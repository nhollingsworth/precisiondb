﻿CREATE TABLE [connects].[CON_imp_customer_test] (
    [internalID] VARCHAR (10) NULL,
    [name]       VARCHAR (75) NULL,
    [class]      VARCHAR (55) NULL,
    [address1]   VARCHAR (55) NULL,
    [address2]   VARCHAR (55) NULL,
    [address3]   VARCHAR (55) NULL,
    [address4]   VARCHAR (55) NULL
);

