﻿




create function [seq_manager].[db_get_currval_value]
	( @schema_name nvarchar(100), @sequence_name nvarchar(100) )
	returns numeric(38)
begin
	--  declare real value variable
	declare @spid int
	set @spid = [seq_manager].[db_get_active_spid]()
	
	--  declare real value variable
	declare @current_value numeric(38, 0)
	--  read real value
	select @current_value = [currval] 
		from [seq_manager].[db_sequence_currvals] 
		where [schema_name] = @schema_name and
			[sequence_name] = @sequence_name and
			[spid] = @spid
			
	return @current_value
end


