﻿

CREATE function [seq_manager].[db_get_active_spid]()
	returns int
as
begin
	if(seq_manager.db_get_context_value_bit(121, 2) = 0)
	begin
		return @@SPID
	end
	
	declare 
		@value binary(128), 
		@rt smallint
	SELECT @value = context_info FROM master.dbo.sysprocesses WHERE spid = @@SPID
	set @rt = cast(substring(@value, 122, 2) as smallint)
	return @rt
end
