﻿


CREATE function [seq_manager].[db_get_curr_sequence_value]
	( @schema_name nvarchar(100), @sequence_name nvarchar(100) )
	returns numeric(38)
begin
----------------------------------------------------------------------------
--  dv(19.04.2005): old implemetation replaced with call to db_get_currval_value function
--
--	declare @retval nvarchar(100)
--	--  call the extended stored procedure
--  -- exec master..xp_ora2ms_sequence 'db_get_curr_sequence_value_impl', @schema_name, @sequence_name, 'SYSDB', 'seq_manager', @retval OUTPUT
--	declare @sessionid smallint
--	set @sessionid = sysdb.seq_manager.db_get_active_spid()
--	declare @ci binary(128)
--	SELECT @ci = context_info FROM master.dbo.sysprocesses WHERE spid = @@SPID
--  exec master..xp_ora2ms_exec @ci, @sessionid, 'SYSDB', 'seq_manager', 'db_get_curr_sequence_value_impl', @schema_name, @sequence_name, @retval OUTPUT
--	if(isnumeric(@retval) = 0) return 0
--  return convert(numeric(38), @retval)
----------------------------------------------------------------------------
	return seq_manager.db_get_currval_value(@schema_name, @sequence_name)
end


