﻿



CREATE function [seq_manager].[db_get_next_sequence_value]
      (
        @schema_name nvarchar(100)
      , @sequence_name nvarchar(100)
      )
returns numeric(38)
begin
	--  variables
    declare @retval numeric(38)
    DECLARE @seq_value BIGINT 
	--  call the extended stored procedure
  -- exec master..xp_ora2ms_sequence 'db_get_next_sequence_value_impl', @schema_name, @sequence_name, 'common', 'seq_manager', @retval OUTPUT
    declare @sessionid smallint
    set @sessionid = seq_manager.db_get_active_spid()
    declare @ci binary(128)
    SELECT  @ci = context_info
    FROM    master.dbo.sysprocesses
    WHERE   spid = @@SPID
    SET @seq_value = 0;
    EXEC seq_manager.db_get_next_sequence_value_impl @schema_name,@sequence_name,@seq_value output
--    exec master..xp_ora2ms_exec @ci, @sessionid, 'common',
--         'seq_manager', 'db_get_next_sequence_value_impl',
--         @schema_name, @sequence_name, @retval OUTPUT
--	if(isnumeric(@retval) = 0) return 0
--  return convert(numeric(38), @retval)
    return seq_manager.db_get_currval_value(@schema_name, @sequence_name)
end


