﻿

create function [seq_manager].[db_get_context_value_bit]
	(@byte_offset int, @bit_no int)
		returns bit
as
begin
	declare 
		@value binary(128), 
		@rt tinyint
		
	SELECT @value = context_info FROM master.dbo.sysprocesses WHERE spid = @@SPID
	set @rt = cast(substring(@value, @byte_offset, 1) as tinyint)

	if(@rt is null) return null
	
	if((@rt & power(2, @bit_no)) <> 0) return 1
	return 0
end
