﻿

--
--  this function is called only from db_get_next_sequence_value_impl
--
CREATE PROCEDURE [seq_manager].[Z_SAVE_db_get_curr_sequence_value_impl]
      (
        @schema_name NVARCHAR(100)
      , @sequence_name NVARCHAR(100)
      , @sequence_value NUMERIC(38) OUTPUT 
      )
AS 
      BEGIN

	--  variables
            DECLARE @initial_value NUMERIC(38)
                  , @current_value NUMERIC(38)
                  , @increment_value NUMERIC(38)
                  , @maxvalue NUMERIC(38)
                  , @is_maxvalue SMALLINT
                  , -- when NOMAXVALUE, then set to 0; otherwise - set to 1 
                    @minvalue NUMERIC(38)
                  , @is_minvalue SMALLINT -- when NOMINVALUE, then set to 0; otherwise - set to 1 

    -- cahnged by Yuri Nogin 7.22.2008
	-- if Sequence doesn't exist then create it
            IF NOT EXISTS ( SELECT  1
                            FROM    seq_manager.db_sequence_list
                            WHERE   schema_name = @schema_name AND
                                    sequence_name = @sequence_name ) 
               INSERT   INTO seq_manager.db_sequence_list
                        (
                          schema_name
                        , sequence_name
                        , initial_value
                        , current_value
                        , increment_value
                        , maxvalue
                        , is_maxvalue
                        , minvalue
                        , is_minvalue
                        , is_cycle
                        , cache_value
                        , is_cachevalue
                        , is_order
                        , last_updated
                        )
               VALUES   (
                          @schema_name
                        , @sequence_name
                        , 1000
                        , 1000
                        , 1
                        , 9223372036854000000
                        , 0
                        , 1
                        , 1
                        , 0
                        , 40
                        , 0
                        , 0
                        , GETDATE()
                        )
		            
	--  get the value
            SELECT  @initial_value = initial_value
                  , @current_value = current_value
                  , @increment_value = ISNULL(increment_value, 1)
                  , @maxvalue = CASE WHEN ( is_maxvalue = 1 )
                                     THEN maxvalue
                                     WHEN increment_value < 0
                                     THEN -1
                                     ELSE 99999999999999999999999999999999999999
                                END
                  , @is_maxvalue = is_maxvalue
                  , @minvalue = CASE WHEN ( is_minvalue = 1 )
                                     THEN minvalue
                                     WHEN increment_value < 0
                                     THEN -99999999999999999999999999999999999999
                                     ELSE 1
                                END
                  , @is_minvalue = is_minvalue
            FROM    seq_manager.db_sequence_list
            WHERE   schema_name = @schema_name AND
                    sequence_name = @sequence_name

	--  the sequence exists?
            IF ( @@ROWCOUNT = 0 ) 
               RAISERROR ( 'Sequence "%s"."%s" not found',
                         16, 1, @schema_name, @sequence_name )
	
	--  is the sequence initialized?
            IF ( @current_value IS NULL ) 
               BEGIN
		--  has initial value?
                     SET @current_value = @initial_value
                     IF ( @current_value IS NULL ) 
                        BEGIN
			-- no initial value
                              IF ( @increment_value >= 0 )
				--  increasing sequence
                                 SET @current_value = @minvalue
                              ELSE
				--  decreasing sequence
                                 SET @current_value = @maxvalue
                        END
		--  store current value 
                     UPDATE seq_manager.db_sequence_list
                     SET    current_value = @current_value
                     WHERE  schema_name = @schema_name AND
                            sequence_name = @sequence_name ;
                            
                     EXEC [seq_manager].[db_update_currval_value] @schema_name,
                          @sequence_name, @current_value
                            
                            
               END

            SET @sequence_value = @current_value
	
--  dv(19.04.2005): update session-specific value of sequence's nextval
      END



