﻿



CREATE PROCEDURE [seq_manager].[db_get_next_sequence_value_impl]
	( @schema_name nvarchar(100), @sequence_name nvarchar(100), @sequence_value numeric(38) output )
as
begin

	--  variables
	declare 
		@current_value numeric(38),
		@increment_value numeric(38),
		@maxvalue numeric(38),
		@is_maxvalue SMALLINT, -- when NOMAXVALUE, then set to 0; otherwise - set to 1 
		@minvalue numeric(38),
		@is_minvalue SMALLINT, -- when NOMINVALUE, then set to 0; otherwise - set to 1 
		@is_cycle SMALLINT

	--  get the current value, initialize it if needed
	EXEC seq_manager.db_get_curr_sequence_value_impl @schema_name, @sequence_name, @current_value OUTPUT
	-- select @current_value = seq_manager.db_get_curr_sequence_value(@schema_name, @sequence_name)

	--  get info
	select 
--			@current_value = current_value,
			@increment_value = isnull(increment_value, 1),
			@maxvalue = case when (is_maxvalue = 1) then maxvalue when increment_value < 0 then -1 else 99999999999999999999999999999999999999 end,
			@is_maxvalue = is_maxvalue,
			@minvalue = case when (is_minvalue = 1) then minvalue when increment_value < 0 then -99999999999999999999999999999999999999 else 1 end,
			@is_minvalue = is_minvalue,
			@is_cycle = is_cycle
---- ks		from seq_manager.db_sequence_list
		from common.db_sequence_list		
		where 
			schema_name = @schema_name and sequence_name = @sequence_name

	--  generate next value
	select @current_value = @current_value + @increment_value
	
	--  check the margins
	if(@increment_value >= 0)
	begin
		--  increasing sequence ?
		if(@current_value > @maxvalue)
		begin
			if(@is_cycle <> 1)
			begin
				raiserror ('Sequence "%s"."%s" out of values', 16, 1, @schema_name, @sequence_name)
				return
			end
			--  set to min value
			set @current_value = @minvalue
		end
	end
	else
	begin
		--  decreasing sequence ?
		if(@current_value < @minvalue)
		begin
			if(@is_cycle <> 1)
			begin
				raiserror ('Sequence "%s"."%s" out of values', 16, 1, @schema_name, @sequence_name)
				return
			end
			--  set to max value
			set @current_value = @maxvalue
		end
	end

	--  store the new value as current
---- ks	UPDATE seq_manager.db_sequence_list
	update common.db_sequence_list	
		set current_value = @current_value
		where schema_name = @schema_name and sequence_name = @sequence_name

	set @sequence_value = @current_value
end




