﻿



CREATE PROCEDURE [seq_manager].[db_update_currval_value]
	( @schema_name nvarchar(100), @sequence_name nvarchar(100), @currval numeric(38, 0) )
as
begin
	if(@currval is not null) 
	begin
	
		--  declare real value variable
		declare @spid int
		set @spid = [seq_manager].[db_get_active_spid]()
		
		--  declare real value variable
		declare @current_value numeric(38, 0)
		--  read real value
		select @current_value = [currval] 
------	ks		from [seq_manager].[db_sequence_currvals] 
			from common.[db_sequence_currvals] 			
			where [schema_name] = @schema_name and
				[sequence_name] = @sequence_name and
				[spid] = @spid
			
		--  set new value
		if(@current_value is null or @current_value < @currval) 
		begin
------	ks		update [seq_manager].[db_sequence_currvals] 
			update common.[db_sequence_currvals] 			
			set [currval] = @currval
			where [schema_name] = @schema_name and
				[sequence_name] = @sequence_name and
				[spid] = @spid
				
			if(@@ROWCOUNT < 1) 
			begin
----  ks				insert into [seq_manager].[db_sequence_currvals] 
				insert into common.[db_sequence_currvals] 				
				([spid], [schema_name], [sequence_name], [currval])
					values(@spid, @schema_name, @sequence_name, @currval)
			end
		end
	end
end

