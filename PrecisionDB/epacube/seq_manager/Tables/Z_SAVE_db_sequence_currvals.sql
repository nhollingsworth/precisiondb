﻿CREATE TABLE [seq_manager].[Z_SAVE_db_sequence_currvals] (
    [spid]          INT           NOT NULL,
    [schema_name]   NVARCHAR (30) NOT NULL,
    [sequence_name] NVARCHAR (30) NOT NULL,
    [currval]       NUMERIC (38)  NOT NULL
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_db_sequence_currvals]
    ON [seq_manager].[Z_SAVE_db_sequence_currvals]([spid] ASC, [schema_name] ASC, [sequence_name] ASC) WITH (FILLFACTOR = 80);

