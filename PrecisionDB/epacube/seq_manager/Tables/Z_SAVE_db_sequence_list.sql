﻿CREATE TABLE [seq_manager].[Z_SAVE_db_sequence_list] (
    [schema_name]     NVARCHAR (30) NULL,
    [sequence_name]   NVARCHAR (30) NULL,
    [initial_value]   NUMERIC (38)  NULL,
    [current_value]   NUMERIC (38)  NULL,
    [increment_value] NUMERIC (38)  NULL,
    [maxvalue]        NUMERIC (38)  NULL,
    [is_maxvalue]     SMALLINT      NULL,
    [minvalue]        NUMERIC (38)  NULL,
    [is_minvalue]     SMALLINT      NULL,
    [is_cycle]        SMALLINT      NULL,
    [cache_value]     NUMERIC (38)  NULL,
    [is_cachevalue]   SMALLINT      NULL,
    [is_order]        SMALLINT      NULL,
    [last_updated]    DATETIME      NULL
);




GO
CREATE TRIGGER [seq_manager].[Z_db_sequence_list_update_currval] ON [seq_manager].[Z_SAVE_db_sequence_list] 
FOR INSERT, UPDATE
AS
	if(UPDATE(current_value))
	begin
		declare 
			@schema nvarchar(100),
			@sequence nvarchar(100),
			@val numeric(38)

		declare cur cursor local for
			select [schema_name], [sequence_name], [current_value] from inserted
		
		open cur
	
		fetch next from cur 
			into @schema, @sequence, @val
		
		while @@FETCH_STATUS = 0
		begin
	
			exec seq_manager.db_update_currval_value @schema, @sequence, @val
	
			fetch next from cur 
				into @schema, @sequence, @val
		end

		close cur 
	end

GO

CREATE TRIGGER [seq_manager].[Z_db_sequence_list_updatedate] ON [seq_manager].[Z_SAVE_db_sequence_list] 
FOR INSERT, UPDATE
AS
  UPDATE [seq_manager].[db_sequence_list] 
    SET last_updated = getdate()
    FROM [seq_manager].[db_sequence_list] t1
    WHERE exists (select 1 from inserted i where i.schema_name = t1.schema_name and i.sequence_name = t1.sequence_name)

GO
CREATE NONCLUSTERED INDEX [IX_db_sequence_list_names]
    ON [seq_manager].[Z_SAVE_db_sequence_list]([schema_name] ASC, [sequence_name] ASC) WITH (FILLFACTOR = 80);

