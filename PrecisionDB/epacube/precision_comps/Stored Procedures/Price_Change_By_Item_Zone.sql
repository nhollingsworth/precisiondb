﻿
-- =============================================
-- Author:		Gary Stone
-- Create date: April 9, 2018
-- Description:	Get Retail Price, Rounded and with Price Points

--	Modifications
--	05-06-2020	URM-1878 IN:865	Isnull added to where clause preventing new prices from being loaded where no price previously existed for the item.	~ ln362
--	06-30-2020	URM-1911 Changes made to component ended up loading retails of null. Put in prevention.
--				Put in code to track users' entries causing changes
--	07-10-2020	Corrected problem with applying Target Margins to parity items.
-- =============================================

CREATE PROCEDURE [precision_comps].[Price_Change_By_Item_Zone]
	@Product_Structure_FK bigint
	, @Zone_Entity_Structure_FK bigint
	, @Price_Multiple_New int = 1
	, @Cost Numeric(18, 5) = Null
	, @New_Retail Numeric(18, 4) = Null
	, @GM_New Numeric(18, 4) = null
	, @URM_TARGET_MARGIN Numeric(18, 4) = null
	, @CONTROL_EFFECTIVE_DATE date
	, @CONTROL_USE_PRICE_POINTS_ROUNDING int = 0
	, @CONTROL_UPDATE_TM_PCT int = 0
	, @CONTROL_APPLY_PARITY_UPDATE int = 0
	, @Column_Change_Name varchar(64) = ''
	, @user varchar(64) = ''

AS

/* 

--precision_comps.[Price_Change_By_Item_Zone] 168528,113232,null,2.8942,0,0.4529,0.4529,2020-08-31,1,1,0,NEW_RETAIL,gstone@epacube.com

	Declare @Product_Structure_FK bigint = 168528
	, @Zone_Entity_Structure_FK bigint = 113232
	, @Price_Multiple_New int = Null
	, @Cost Numeric(18, 5) = 2.8942
	, @New_Retail Numeric(18, 4) = 0
	, @GM_New Numeric(18, 4) = 0.4529
	, @URM_TARGET_MARGIN Numeric(18, 4) = 0.4529
	, @CONTROL_EFFECTIVE_DATE date = cast(getdate() as date)
	, @CONTROL_USE_PRICE_POINTS_ROUNDING int = 1
	, @CONTROL_UPDATE_TM_PCT int = 1
	, @CONTROL_APPLY_PARITY_UPDATE int = 0
	--, @Column_Change_Name varchar(64) = 'GM_NEW'
	--, @Column_Change_Name varchar(64) = 'URM_TARGET_MARGIN'
	, @Column_Change_Name varchar(64) = 'NEW_RETAIL'
	, @user varchar(64) = 'gstone@epacube.com'
*/

Declare @Cutoff as time
Declare @Time varchar(20)
Declare @ind_Requested_Parity_Status int

Set @Cutoff = (select value from epacube.epacube_params WHERE EPACUBE_PARAMS_ID = 112000)

Set @Time = replace(convert(varchar(20), @Cutoff, 22), ':00 ', ' ')

--If cast(getdate() as time) >= @Cutoff and @CONTROL_EFFECTIVE_DATE <= cast(getdate() as date)
--Begin
--	Select
--	(Select	'You have missed today''s cutoff time of ' + @Time + '.<br><br>The values just entered have not been accepted.<br><br>Please change the effective date to tomorrow or later, refresh your screen, and re-enter your changes') 'USER_MSG'
--	, 1 'reset_values'
--	, 10000 'user_msg_timeout_ms'
--	goto eof
--End

insert into precision.user_call_statements
(create_user, ui_name, procedure_called, procedure_parameters, prod_segment_fk, entity_segment_fk)
Select @user 'create_user'
, 'View' 'ui_name'
, '[precision_comps].[Price_Change_By_Item_Zone]' 'procedure_called'
, '' + isnull(cast(@Product_Structure_FK as varchar(24)), 'Null') + ',
 ' + isnull(cast(@Zone_Entity_Structure_FK as varchar(24)), 'Null') + ',
 ' + isnull(cast( @Price_Multiple_New as varchar(24)), 'Null') + ',
 ' + isnull(cast( @Cost as varchar(24)), 'Null') + ',
 ' + isnull(cast( @New_Retail as varchar(24)), 'Null') + ',
 ' + isnull(cast( @GM_New as varchar(24)), 'Null') + ',
 ' + isnull(cast( @URM_TARGET_MARGIN as varchar(24)), 'Null') + ',
 ''' + isnull(cast( @CONTROL_EFFECTIVE_DATE as varchar(24)), 'Null') + ''',
 ' + isnull(cast( @CONTROL_USE_PRICE_POINTS_ROUNDING as varchar(24)), 'Null') + ',
 ' + isnull(cast( @CONTROL_UPDATE_TM_PCT as varchar(24)), 'Null') + ',
 ' + isnull(cast( @CONTROL_APPLY_PARITY_UPDATE as varchar(24)), 'Null') + ',
 ''' + isnull(cast( @Column_Change_Name as varchar(24)), 'Null') + ''',
 ''' + @User + ''' ' 'procedure_parameters'
, @Product_Structure_FK 'prod_segment_fk', @Zone_Entity_Structure_FK 'entity_segment_fk'

If @CONTROL_EFFECTIVE_DATE < cast(getdate() as date)
Begin
		Select @product_structure_fk 'PRODUCT_STRUCTURE_FK', @zone_entity_structure_fk 'ENTITY_STRUCTURE_FK'
		, @URM_TARGET_MARGIN 'URM_TARGET_MARGIN'
		, Null 'NEW_RETAIL'
		, @Price_Multiple_New 'PM_NEW'
		, 'You cannot make this type of change with an effective date of ' + cast(@CONTROL_EFFECTIVE_DATE as varchar(10)) + ' as that is prior to today''s date of ' + cast(cast(getdate() as date) as varchar(10)) 'USER_MSG'
	goto eof
End

	SET NOCOUNT ON;

	If @Price_Multiple_New = 0
	Begin
		Set @Price_Multiple_New = 1
		If @Column_Change_Name = 'PM_NEW'
			Begin
				Set @Column_Change_Name = 'NEW_RETAIL'
				Set @New_Retail = 0
			End
	End

	If @Column_Change_Name = 'PM_NEW' and @New_Retail is null and @GM_New is null
	goto eof

	Set @user = replace(@user, '.', '_')

	If @Column_Change_Name = 'GM_New'
		Set @New_Retail = Null
	else
	if @Column_Change_Name = 'NEW_RETAIL'
		Set @GM_New = Null
	else
	if @Column_Change_Name = 'URM_TARGET_MARGIN'
	Begin
		Set @New_Retail = Null
		Set @GM_New = Null
	end

	Set @Price_Multiple_New = isnull(@Price_Multiple_New, 1)

	Declare @Parity_Pct Numeric(18, 4)
	Declare @Price_Cur Numeric(18, 4)
	Declare @Price_Initial Numeric(18, 2)
	Declare @Price_Final Numeric(18, 2)
	Declare @GP_Final Numeric(18, 4)
	Declare @CHANGE_TOLERENCE Numeric(18, 4) = 0.05
	
	If (@Column_Change_Name = 'URM_TARGET_MARGIN' and @URM_TARGET_MARGIN = 0)
		or (@Column_Change_Name = 'NEW_RETAIL' and @New_Retail = 0)
		or (@Column_Change_Name = 'GM_New' and @GM_New = 0)
	Begin
		Set @Price_Final = Null
		Set @GP_Final = Null
	End
	else
	If @Column_Change_Name = 'URM_TARGET_MARGIN' and isnull(@New_Retail, 0) = 0 and isnull(@GM_New, 0) = 0
	Begin
		Set @Price_Initial = round(Cast((@Cost * @Price_Multiple_New) / (1 - @URM_TARGET_MARGIN) as Numeric(18, 6)) + 0.0049, 2)

		If @CONTROL_USE_PRICE_POINTS_ROUNDING = 0
		Set @Price_Final = @Price_Initial
		else
		Set @Price_Final = (Select [precision].[getretailprices](@zone_entity_structure_fk, @Product_Structure_FK, @Price_Initial, @CONTROL_EFFECTIVE_DATE))

		Set @GP_Final = Cast((@Price_Final - (@Cost * @Price_Multiple_New)) / @Price_Final as Numeric(18, 4))
	end
	else
	If @Column_Change_Name = 'URM_TARGET_MARGIN' and isnull(@New_Retail, 0) <> 0
	Begin
		Set @Price_Final = @New_Retail
		Set @GP_Final = @GM_New
	End
	else
	Begin
		Set @Price_Initial = isnull(@New_Retail, round(Cast((@Cost * @Price_Multiple_New) / (1 - @GM_New) as Numeric(18, 6)) + 0.0049, 2))

		If @CONTROL_USE_PRICE_POINTS_ROUNDING = 0
			Set @Price_Final = @Price_Initial
		else
			Set @Price_Final = (Select [precision].[getretailprices](@zone_entity_structure_fk, @Product_Structure_FK, @Price_Initial, @CONTROL_EFFECTIVE_DATE))

		Set @GP_Final = Cast((@Price_Final - (@Cost * @Price_Multiple_New)) / @Price_Final as Numeric(18, 4))
	End

	--SELECT @Column_Change_Name '@Column_Change_Name', @Price_Multiple_New '@Price_Multiple_New', @Cost '@Cost', @Price_Final '@Price_Final', @GP_Final '@GP_Final', @New_Retail '@New_Retail', @GM_New '@GM_New'

	SET NOCOUNT ON;

	if object_id('tempdb..#pbv') is not null
	drop table #pbv

	if object_id('tempdb..#t_prod') is not null
	drop table #t_prod

	If object_id('tempdb..#zones') is not null
	drop table #zones

		create table #pbv(value numeric(18, 6) null, unit_value numeric(18, 6) null, effective_date date null, pack int null, size_uom varchar(16) null, product_structure_fk bigint null, pricesheet_basis_values_id bigint null, data_name_fk bigint null
		, create_timestamp datetime null, cust_entity_structure_fk bigint null, drank bigint null, pbv_status varchar(128) null, add_to_zone_date date null, change_status int null, cost_change_processed_date date null
		, drank_f bigint null, ind_cost_change_item int null, item varchar(64) null, unique_cost_record_id varchar(64) null, pbv_id bigint identity(1, 1) not null)

		create table #t_prod(item_change_type_cr_fk bigint null, qualified_status_cr_fk bigint null, price_mgmt_type_cr_fk bigint null, pack int null, size_uom varchar(16) null
		, parent_product_structure_fk bigint null, item varchar(64) null, item_description varchar(64) null, product_structure_fk bigint null, entity_structure_fk bigint null, cost_change_effective_date_cur date null
		, cost_change_effective_date date null, case_cost_new numeric(18, 6) null, unit_cost_new numeric(18, 6) null, case_cost_cur numeric(18, 2) null, unit_cost_cur numeric(18, 4) null, price_multiple int null
		, price_multiple_new int null, price_effective_date_cur date null, price_cur numeric(18, 2) null, price_new_initial numeric(18, 2) null, price_new numeric(18, 2) null, tm_pct_cur numeric(18, 4) null
		, urm_target_margin numeric(18, 4) null, gp_new numeric(18, 4) null, gp_cur numeric(18, 4) null, pricesheet_basis_values_fk bigint null, pricesheet_basis_values_fk_cur bigint null, pbv_status varchar(128) null, change_status int null
		, cost_change_processed_date date null, ind_cost_change_item int null, ind_requested_item int null, parity_type varchar(16) null, parity_status varchar(8) null, ind_parity_status int null, ind_parity int null
		, parity_parent_product_structure_fk bigint null, parity_child_product_structure_fk bigint null, parity_parent_item varchar(64) null, parity_child_item varchar(64) null, parity_discount_amt numeric(18, 2) null
		, parity_mult int null, parity_discount_pct numeric(18, 4) null, alias_id bigint null, alias_description varchar(64) null, parity_alias_id bigint null, ind_initial_load int null, load_step int null
		, lg_active varchar(8) null, lg_flag varchar(8) null, use_lg_price_points varchar(8) null, minamt numeric(18, 2) null, zone_id varchar(64) null, zone_name varchar(64) null, zone_sort int null, pricing_steps varchar(128) null
		, related_item_alias_id bigint null, unit_cost_new_actual numeric(18, 4), subgroup_id bigint null, subgroup_description varchar(64) null, subgroup_data_value_fk bigint null, item_sort bigint null, item_group bigint null
		, ind_display_as_reg int null, Digit_DN_FK bigint null, Digit_Adj Numeric(18, 2) Null, price_rounded Numeric(18, 2) Null, ftn_breakdown_parent varchar(8) null, ftn_cost_delta varchar(8) null, stage_for_processing_fk bigint null
		, qualified_status_step varchar(64) Null, unique_cost_record_id varchar(64) null, t_prod_id bigint identity(1, 1) not null)

		create table #zones(entity_structure_fk bigint not null, zone_type varchar(8) null, zone_id varchar(16) not null, zone_name varchar(64) null, daysout int not null, LG_Active varchar(8))

		insert into #zones(entity_structure_fk, zone_type, zone_id, zone_name, daysout, LG_Active)
		select ZONE_SEGMENT_FK, zone_type, zone_id, zone_name, DaysOut, LG_Active from (
		select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL, dv.value 'Zone_Type', eiz.value 'zone_id', eizn.value 'zone_name'
		, ss.ZONE_SEGMENT_FK, cast(ss.attribute_event_data as Numeric(18, 0)) 'DaysOut', ss.effective_date, lga.LG_Active
		, Dense_Rank()over(partition by ss.ZONE_SEGMENT_FK order by ss.effective_date desc, ss.segments_settings_id desc) 'DRank'
		from epacube.segments_settings ss with (nolock)
		inner join epacube.entity_attribute ea with (nolock) on ss.ZONE_SEGMENT_FK = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
		inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
		inner join epacube.entity_identification eiz with (nolock) on ss.ZONE_SEGMENT_FK = eiz.ENTITY_STRUCTURE_FK and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.DATA_NAME_FK = 151110
		left join epacube.ENTITY_IDENT_NONUNIQUE eizn with (nolock) on eiz.ENTITY_STRUCTURE_FK = eizn.ENTITY_STRUCTURE_FK and eizn.DATA_NAME_FK = 140200
		left join
			(	select * from (
				Select ss.ATTRIBUTE_Y_N 'LG_Active', ss.ZONE_SEGMENT_FK, ss.SEGMENTS_SETTINGS_ID
				, dense_rank()over(partition by ss.data_name_fk, ss.ZONE_SEGMENT_FK order by ss.effective_date desc, ss.segments_settings_id desc) DRank
				from epacube.segments_settings ss with (nolock) 
				where ss.data_name_fk = 144862
				and ss.PROD_SEGMENT_FK is null
				and ss.ZONE_SEGMENT_FK is not null 
				and CUST_ENTITY_STRUCTURE_FK is null
				and ss.effective_date <= @CONTROL_EFFECTIVE_DATE
				and ss.RECORD_STATUS_CR_FK = 1
				) a where Drank = 1
			) LGA on ss.ZONE_SEGMENT_FK = lga.ZONE_SEGMENT_FK
		where 1 = 1
		and cust_entity_structure_fk is null
		and ss.data_name_fk = 144868
		and ss.effective_date <= @CONTROL_EFFECTIVE_DATE
		and ss.RECORD_STATUS_CR_FK = 1
		) a where DRank = 1
		and ZONE_SEGMENT_FK = @Zone_Entity_Structure_FK

	insert into #pbv
	(value, unit_value, effective_date, pack, product_structure_fk, pricesheet_basis_values_id, data_name_fk, create_timestamp, cust_entity_structure_fk, DRank, change_status)
	Select * from 
		(
			Select pbv.value, pbv.unit_value, pbv.Effective_Date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, pbv.create_timestamp
			, case when isnull(patt.attribute_event_data, 'W') <> 'W' then pbv.cust_entity_structure_fk end 'Cust_Entity_Structure_FK' 
			, dense_rank()over(partition by pbv.data_name_fk, pbv.product_structure_fk order by pbv.effective_date desc, pbv.pricesheet_basis_values_id) DRank
			, pbv.Change_Status
			from marginmgr.pricesheet_basis_values pbv with (nolock) 
			left join epacube.product_attribute patt with (nolock) on pbv.product_structure_fk = patt.product_structure_fk and patt.data_name_fk = 500000
			where pbv.data_name_fk = 503201 
			and cust_entity_structure_fk is null
			and pbv.product_structure_fk = @Product_Structure_FK
			and cast(PBV.Effective_Date as date) <= @CONTROL_EFFECTIVE_DATE
			and pbv.record_status_cr_fk = 1
		) a where drank = 1

exec [precision].[Pricing_Calcs_Common_Tmp_Tbls]

--Update tp
--set parity_status = 'P'
--, ind_parity_status = 1
--from #t_prod tp
--where alias_id = (select top 1 alias_id from #t_prod where parity_status = 'P')

--Update tp
--set parity_status = 'C'
--, ind_parity_status = 2
--from #t_prod tp
--where alias_id = (select top 1 alias_id from #t_prod where parity_status = 'C')

Set @ind_Requested_Parity_Status = (select top 1 ind_parity_status from #t_prod where ind_requested_item = 1)

If isnull(@CONTROL_APPLY_PARITY_UPDATE, 0) = 0
	Begin
		delete from #t_prod
		where (select count(*) from #t_prod where isnull(ind_parity_status, 0) <> 0) > 0
		and ind_parity_status <> (select top 1 ind_parity_status from #t_prod where ind_requested_item = 1)
	End

update tp
set price_multiple_new = isnull(@Price_Multiple_New, 1)
from #t_prod tp

--	Delete Newly Created Target Margins from Segments Settings
	If @CONTROL_UPDATE_TM_PCT = 1 or @Column_Change_Name = 'URM_TARGET_MARGIN'
	Begin
		Print 'Deleting todays TM(s)'
		Delete SS
		from epacube.segments_settings ss
		inner join #t_prod tp on ss.ZONE_SEGMENT_FK = tp.entity_structure_fk
								and ss.PRODUCT_STRUCTURE_FK = tp.product_structure_fk
		where ss.data_name_fk = 502046
		and ss.EFFECTIVE_DATE = @CONTROL_EFFECTIVE_DATE
		and ss.DATA_SOURCE = 'user update'
		and ss.process_source = 'PRICE CHANGES COMPONENT'
	end

	If @Column_Change_Name = 'URM_TARGET_MARGIN'
	Begin
		Update P
		Set URM_TARGET_MARGIN = Case	when @Column_Change_Name = 'URM_TARGET_MARGIN' and @URM_TARGET_MARGIN = 0 then tm_pct_cur 
		
										else @URM_TARGET_MARGIN end
		from #t_prod P
		inner join 
			(select * from (
							Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
							, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
							from epacube.segments_settings ss with (nolock) 
							inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
							inner join #t_prod p on sp.product_structure_fk = p.product_structure_fk and ss.zone_segment_fk = p.entity_structure_fk
							where ss.data_name_fk = 502046
							and ss.Effective_Date <= @CONTROL_EFFECTIVE_DATE
							and (ss.reinstate_inheritance_date > @CONTROL_EFFECTIVE_DATE or ss.reinstate_inheritance_date is null)
							and ss.RECORD_STATUS_CR_FK = 1
							) a where Drank = 1
			) sz_502046 on p.Product_structure_FK = sz_502046.product_structure_fk and p.entity_structure_fk = sz_502046.zone_segment_fk
		where isnull(p.ind_parity_status, 0) = @ind_Requested_Parity_Status OR (@Column_Change_Name = 'URM_TARGET_MARGIN' and @URM_TARGET_MARGIN = 0)
	End

		If @Column_Change_Name = 'URM_TARGET_MARGIN' and @URM_TARGET_MARGIN = 0
		Begin
			Update pr
			set TARGET_MARGIN = tp.urm_target_margin
			, update_user = 'TM Pct Updated 0'
			from marginmgr.pricesheet_retails pr
			inner join #t_prod tp on pr.product_structure_fk = tp.product_structure_fk and pr.ZONE_ENTITY_STRUCTURE_FK = tp.entity_structure_fk
			where 1 = 1
			and pr.EFFECTIVE_DATE = @CONTROL_EFFECTIVE_DATE
			and data_source = 'user update'
		
		goto eof_zero
		End

	If @Column_Change_Name <> 'URM_TARGET_MARGIN'
	Begin
		Print 'Deleting todays Pricesheet_Retail(s)'
		delete pr from marginmgr.pricesheet_retails pr
		inner join #t_prod tp on pr.PRODUCT_STRUCTURE_FK = isnull(tp.product_structure_fk, tp.parent_product_structure_fk)
							and pr.ZONE_ENTITY_STRUCTURE_FK = tp.entity_structure_fk
		where 1 = 1
		and pr.EFFECTIVE_DATE = @CONTROL_EFFECTIVE_DATE
		and pr.DATA_SOURCE = 'user update'
		and pr.process_source = 'PRICE CHANGES COMPONENT'

		If isnull(@Price_Final, 0) = 0 or (@Column_Change_Name = 'PM_NEW' and @Price_Multiple_New = 0)
		goto eof_zero

		Update tp
		set PRICE_MULTIPLE_NEW = isnull(@price_multiple_new, 1)
		from #t_prod tp

		Update tp
		set price_new = @Price_Final
		, gp_new = @GP_Final
		, URM_TARGET_MARGIN = case	when @Column_Change_Name = 'URM_TARGET_MARGIN' then @URM_TARGET_MARGIN 
									when @CONTROL_UPDATE_TM_PCT = 1 then @GP_Final
									else tp.URM_TARGET_MARGIN end
		from #t_prod tp
		where ind_parity_status = @ind_Requested_Parity_Status

		Update tp
		set price_new = case when @CONTROL_USE_PRICE_POINTS_ROUNDING = 0 then
							case when ind_parity_status = 1 then @Price_Final * (1 - tp.parity_discount_pct) else @Price_Final / (1 - tp.parity_discount_pct) end
						else (Select [precision].[getretailprices](@zone_entity_structure_fk, @Product_Structure_FK
						, case when ind_parity_status = 1 then @Price_Final * (1 - tp.parity_discount_pct) else @Price_Final / (1 - tp.parity_discount_pct) end
						, @CONTROL_EFFECTIVE_DATE)) end
		from #t_prod tp
		where ind_parity_status <> @ind_Requested_Parity_Status
		and ind_parity_status <> 0
		and isnull(@CONTROL_APPLY_PARITY_UPDATE, 0) = 1
		and price_new is null

		Update tp
		Set price_new = (select top 1 price_new from #t_prod tp where ind_parity_status <> @ind_Requested_Parity_Status and ind_parity_status <> 0 and isnull(@CONTROL_APPLY_PARITY_UPDATE, 0) = 1 and price_new is not null)
		from #t_prod tp
		where ind_parity_status <> @ind_Requested_Parity_Status
		and ind_parity_status <> 0
		and isnull(@CONTROL_APPLY_PARITY_UPDATE, 0) = 1
		and price_new is null

		Update tp
		set gp_new = cast((price_new - (unit_cost_new * tp.PRICE_MULTIPLE_NEW)) / price_new as Numeric(18, 4))
		, URM_TARGET_MARGIN = case isnull(@CONTROL_UPDATE_TM_PCT, 0) when 0 then URM_TARGET_MARGIN else cast((price_new - (unit_cost_new * tp.PRICE_MULTIPLE_NEW)) / price_new as Numeric(18, 4)) end
		from #t_prod tp
		where ind_parity_status <> @ind_Requested_Parity_Status
		and ind_parity_status <> 0
		and isnull(@CONTROL_APPLY_PARITY_UPDATE, 0) = 1
		and gp_new is null

		Update tp
		set URM_TARGET_MARGIN = (select top 1 urm_target_margin from #t_prod where ind_parity_status <> @ind_Requested_Parity_Status and ind_parity_status <> 0 and parity_status is not null)
		from #t_prod tp
		where ind_parity_status <> @ind_Requested_Parity_Status
		and ind_parity_status <> 0
		and isnull(@CONTROL_APPLY_PARITY_UPDATE, 0) = 1




		update tp
		set GP_CUR = cast((isnull(price_cur, price_new) - isnull(unit_cost_cur, unit_cost_new)) / isnull(price_cur, price_new) as Numeric(18, 4))
		from #t_prod tp
		where isnull(isnull(price_cur, price_new) , 0) <> 0

	--	Load new retails for use
	Print 'Loading Pricesheet_Retails'
		Insert into marginmgr.PRICESHEET_RETAILS
		(data_name_fk, target_margin, product_structure_fk, zone_entity_structure_fk, price_multiple, price, gm_pct, effective_date, price_type, RETAIL_SOURCE_DATA_NAME_FK, RETAIL_SOURCE_FK, retail_source, create_user, [data_source], [Process_Source])
		select 503301 'data_name_fk', tp.URM_TARGET_MARGIN, tp.product_structure_fk, tp.entity_structure_fk 'zone_entity_structure_fk'
		, isnull(@Price_Multiple_New, 1) 'price_multiple', tp.price_new 'price' --,tp.gm_pct
		, Case when tp.price_new is null then null else Cast((tp.PRICE_NEW - isnull(unit_cost_new, unit_cost_cur))/tp.PRICE_NEW as numeric(18, 4)) end 'gm_pct'
		, @CONTROL_EFFECTIVE_DATE 'effective_date', 1 'price_type', 151110 'RETAIL_SOURCE_DATA_NAME_FK', tp.entity_structure_fk 'retail_source_fk', tp.zone_id 'retail_source', @user 'create_user', 'USER UPDATE' 'data_source'
		, 'PRICE CHANGES COMPONENT' 'Process_Source'
		from #t_prod tp
		where (isnull(tp.price_new, 1) <> isnull(tp.price_cur, 0) or isnull(tp.price_multiple, 1) <> isnull(tp.price_multiple_new, 1))
		and isnull(@New_Retail, 1.00) <> 0
		and tp.price_new is not null
	End

	If @Column_Change_Name = 'URM_TARGET_MARGIN' or @CONTROL_UPDATE_TM_PCT = 1 
	Begin

	Print 'AT URM Target Margin and Control Update TM PCT = 1'
		Update tp
		set gp_new = cast((price_new - (unit_cost_new * tp.PRICE_MULTIPLE_NEW)) / price_new as Numeric(18, 4))
		from #t_prod tp

		If @CONTROL_APPLY_PARITY_UPDATE = 1
		Begin
		--Determine TM pct of parity items
			--Declare @CalcRetail Numeric(18, 2) = 
			--	(Select (select [precision].[getretailprices](@zone_entity_structure_fk, @Product_Structure_FK, calc_retail * multiplier * isnull(Price_Multiple_New, 1), @CONTROL_EFFECTIVE_DATE)
			--	from (
			--										SELECT item, zone_id, parity_discount_pct, parity_status, ind_parity_status, unit_cost_new, isnull(price_new, price_cur) 'retail', urm_target_margin
			--										, cast(round(unit_cost_new / (1 - urm_target_margin), 2) as numeric(18, 2)) 'Calc_Retail' 
			--										, cast(case cast(ind_parity_status as int) when 1 then 1/(1-parity_discount_pct) else 1 - parity_discount_pct end as numeric(18, 4)) 'multiplier'
			--										, Price_Multiple_New
			--										from #t_prod where product_structure_fk = (Select top 1 product_structure_fk from #t_prod where ind_parity_status = @ind_Requested_Parity_Status and parity_discount_pct is not null)
			--									) A
			--	))

			Declare @CalcRetail Numeric(18, 2) = 
					(Select [precision].[getretailprices](@zone_entity_structure_fk, @Product_Structure_FK
					, Case when @Column_Change_Name = 'URM_TARGET_MARGIN' then calc_retail else @Price_Final end
						* multiplier, @CONTROL_EFFECTIVE_DATE)
					from (
														SELECT item, zone_id, parity_discount_pct, parity_status, ind_parity_status, unit_cost_new, isnull(price_new, price_cur) 'retail', urm_target_margin
														, cast(round(unit_cost_new / (1 - urm_target_margin), 2) as numeric(18, 2)) 'Calc_Retail' 
														, cast(case cast(ind_parity_status as int) when 1 then 1/(1-parity_discount_pct) else 1 - parity_discount_pct end as numeric(18, 4)) 'multiplier'
														, Price_Multiple_New
														from #t_prod where product_structure_fk = (Select top 1 product_structure_fk from #t_prod where ind_parity_status = @ind_Requested_Parity_Status and parity_discount_pct is not null)
													) A
					)

			Update TP
			Set URM_Target_Margin = (Select top 1 cast(Round((@CalcRetail - (unit_cost_new * price_multiple_new)) / @CalcRetail, 4) as Numeric(18, 4)) from #t_prod where ind_parity_status <> @ind_Requested_Parity_Status and ind_parity_status <> 0)
			from #t_prod tp
			where ind_parity_status <> @ind_Requested_Parity_Status
			and @Column_Change_Name = 'URM_TARGET_MARGIN'
		
			Declare @MultiParityInd int = (select ind_parity_status from (Select alias_id, ind_parity_status from #t_prod where parity_parent_product_structure_fk is not null or parity_child_product_structure_fk is not null group by alias_id, ind_parity_status) a group by ind_parity_status having count(*) > 1)

			Update tp
			Set URM_Target_Margin = cast(Round((@CalcRetail - (upd.unit_cost_new * tp.price_multiple_new)) / @CalcRetail, 4) as Numeric(18, 4))
			from #t_prod tp
			inner join (Select alias_id, unit_cost_new from #t_prod where (parity_parent_product_structure_fk is not null or parity_child_product_structure_fk is not null) and ind_parity_status = @MultiParityInd and parity_status is not null group by alias_id, unit_cost_new) upd on isnull(tp.alias_id, 0) = isnull(upd.alias_id, 0)
			--where @Column_Change_Name = 'URM_TARGET_MARGIN'

			Update tp 
			Set URM_Target_Margin = cast(Round((@Price_Final - upd.unit_cost_new) / @Price_Final, 4) as Numeric(18, 4))
			from #t_prod tp
			inner join (Select alias_id, unit_cost_new from #t_prod where (parity_parent_product_structure_fk is not null or parity_child_product_structure_fk is not null) and ind_parity_status = @MultiParityInd and parity_status is not null group by alias_id, unit_cost_new) upd on isnull(tp.alias_id, 0) = isnull(upd.alias_id, 0)
			where tp.ind_parity_status = @ind_Requested_Parity_Status
			--where @Column_Change_Name <> 'URM_TARGET_MARGIN'
	
		End

		Update pr
		set TARGET_MARGIN = tp.urm_target_margin
		, update_user = 'TM Pct Updated'
		from marginmgr.pricesheet_retails pr
		inner join #t_prod tp on pr.product_structure_fk = tp.product_structure_fk and pr.ZONE_ENTITY_STRUCTURE_FK = tp.entity_structure_fk
		where 1 = 1
		and pr.EFFECTIVE_DATE = @CONTROL_EFFECTIVE_DATE
		and data_source = 'user update'

		Print 'Loading Target Margins'
--	Load New Target Margins into epaCUBE.Segments_Settings
		Insert into epacube.SEGMENTS_SETTINGS
		(ORG_ENTITY_STRUCTURE_FK, ZONE_ENTITY_STRUCTURE_FK, DATA_NAME_FK, ATTRIBUTE_NUMBER, ATTRIBUTE_EVENT_DATA, PRECEDENCE, RECORD_STATUS_CR_FK, create_user, prod_segment_fk, zone_segment_fk, PRODUCT_STRUCTURE_FK, Effective_Date, ENTITY_DATA_NAME_FK
		, prod_segment_data_name_fk, [data_source], PROCESS_SOURCE)
		Select 1 'Org_entity_structure_fk', tp.entity_structure_fk 'zone_entity_structure_fk', 502046 'data_name_fk', URM_TARGET_MARGIN 'attribute_number', URM_TARGET_MARGIN 'attribute_event_data', 5 'precedence', 1 record_status_cr_fk
		, @user 'create_user', product_structure_fk 'prod_segment_fk', entity_structure_fk 'zone_segment_fk', product_structure_fk 'product_structure_fk', @CONTROL_EFFECTIVE_DATE 'effective_date', 151000 entity_data_name_fk
		, 110103 'prod_segment_data_name_fk', 'USER UPDATE' 'data_source', 'PRICE CHANGES COMPONENT' 'Process_Source'
		from #t_prod tp
		where tp.TM_PCT_CUR <> tp.URM_TARGET_MARGIN
		and tp.URM_TARGET_MARGIN <> 0
		and @Column_Change_Name = case when @Column_Change_Name = 'URM_TARGET_MARGIN' and @URM_TARGET_MARGIN = 0 then '0' else @Column_Change_Name end
		and isnull(tp.gp_new, 0.01) <> 0
	End

	If @Column_Change_Name = 'URM_TARGET_MARGIN'
	Begin
		Print @Column_Change_Name + ' Return1' 
		
		Select item, isnull(tp.PRODUCT_STRUCTURE_FK, tp.PARENT_PRODUCT_STRUCTURE_FK) 'PRODUCT_STRUCTURE_FK', tp.Entity_Structure_FK 'ENTITY_STRUCTURE_FK'
		, tp.URM_TARGET_MARGIN
		, case when isnull(@Price_Multiple_New, 1) = 1 then null else @Price_Multiple_New end 'PM_NEW'
		from #t_prod tp
		inner join #zones z on tp.entity_structure_fk = z.entity_structure_fk
		cross Join (Select cast(ep.value as numeric(18, 2)) 'margin_tol' from epacube.epacube_params ep Where ep.application_scope_fk = 5013 and ep.[name] = 'MARGIN CHANGE TOLERENCE') mt
		where 1 = 1
		and URM_TARGET_MARGIN is not null
			
		goto eof
	End
	Else
	Begin
		Print @Column_Change_Name + ' Return2'

		Select item, isnull(tp.PRODUCT_STRUCTURE_FK, tp.PARENT_PRODUCT_STRUCTURE_FK) 'PRODUCT_STRUCTURE_FK', tp.Entity_Structure_FK 'ENTITY_STRUCTURE_FK'
		, tp.PRICE_NEW 'NEW_RETAIL'--, tp.GP_NEW 'GM_NEW'
		, Case when tp.price_new is null then null else Cast((tp.PRICE_NEW - (isnull(unit_cost_new, unit_cost_cur) * tp.price_multiple_new))/tp.PRICE_NEW as numeric(18, 4)) end  'GM_NEW'
		, tp.URM_TARGET_MARGIN
		, case when isnull(@Price_Multiple_New, 1) = 1 then null else @Price_Multiple_New end 'PM_NEW'
		, Case	when GP_NEW > 0.5 then 'HIGH MARGIN WARNING'
							WHEN Abs(GP_NEW - GP_CUR) <= margin_tol then '' else Cast(Cast(((GP_NEW - GP_CUR) * 100) as Numeric(18, 2)) as varchar(8)) +  '% CHANGE NOT WITHIN ' + Cast(Cast(margin_tol * 100 as Numeric(18, 2)) as varchar(8)) + '% TOLERENCE LEVEL' END 'USER_MSG'
		, Case	when GP_NEW > 0.5 then 1500
							WHEN Abs(GP_NEW - GP_CUR) <= margin_tol then Null else 3000 END 'user_msg_timeout_ms'
		from #t_prod tp
		inner join #zones z on tp.entity_structure_fk = z.entity_structure_fk
		cross Join (Select cast(ep.value as numeric(18, 2)) 'margin_tol' from epacube.epacube_params ep Where ep.application_scope_fk = 5013 and ep.[name] = 'MARGIN CHANGE TOLERENCE') mt
			
		goto eof
	End

eof_pm:
Print 'Return eof_pm'
	Select item, isnull(tp.PRODUCT_STRUCTURE_FK, tp.PARENT_PRODUCT_STRUCTURE_FK) 'PRODUCT_STRUCTURE_FK', tp.Entity_Structure_FK 'ENTITY_STRUCTURE_FK'
	, tp.PRICE_NEW 'NEW_RETAIL', tp.GP_NEW 'GM_NEW', tp.URM_TARGET_MARGIN, case @price_multiple_new when 1 then Null when 0 then case tp.price_multiple_new when 1 then null else tp.price_multiple_new end else @price_multiple_new end 'PM_NEW'
	from #t_prod tp
	inner join #zones z on tp.entity_structure_fk = z.entity_structure_fk
	where 1 = 1

	goto eof

eof_zero:
Print 'Return eof_zero'

	Update P
	Set Price_Multiple = CurPrice.price_multiple
	from #t_prod P
	inner join 
		(Select pr.zone_entity_structure_FK, pr.product_structure_fk, pr.price_multiple, pr.Price, pr.Effective_Date
		, Dense_Rank()over(partition by pr.zone_entity_structure_FK, pr.product_structure_fk order by pr.effective_date desc, pr.Price) DRank_Eff
		from marginmgr.PRICESHEET_RETAILS PR 
		inner join #t_prod P on pr.product_structure_fk = P.Product_structure_FK and pr.zone_entity_structure_FK = P.Entity_Structure_FK
		where 1 = 1
		and pr.zone_entity_structure_FK is not null 
		and pr.effective_date <= @CONTROL_EFFECTIVE_DATE
		group by pr.zone_entity_structure_FK, pr.product_structure_fk, pr.price_multiple, pr.Price, pr.Effective_Date
		) CurPrice on P.PRODUCT_STRUCTURE_FK = CurPrice.Product_Structure_FK and P.Entity_Structure_FK = CurPrice.zone_entity_structure_FK
	where 1 = 1
		and isnull(CurPrice.DRank_Eff, 1) = 1

	If @Column_Change_Name = 'URM_TARGET_MARGIN'
		Select item, isnull(tp.PRODUCT_STRUCTURE_FK, tp.PARENT_PRODUCT_STRUCTURE_FK) 'PRODUCT_STRUCTURE_FK', tp.Entity_Structure_FK 'ENTITY_STRUCTURE_FK'
		, tp.URM_TARGET_MARGIN
		, case when isnull(tp.price_multiple, 1) > 1 then tp.price_multiple_New else null end 'PM_NEW'
		from #t_prod tp
		inner join #zones z on tp.entity_structure_fk = z.entity_structure_fk
		where 1 = 1
	Else
	If @Column_Change_Name <> 'URM_TARGET_MARGIN' and @CONTROL_UPDATE_TM_PCT = 0
		Select item, isnull(tp.PRODUCT_STRUCTURE_FK, tp.PARENT_PRODUCT_STRUCTURE_FK) 'PRODUCT_STRUCTURE_FK', tp.Entity_Structure_FK 'ENTITY_STRUCTURE_FK'
		, tp.PRICE_NEW 'NEW_RETAIL', tp.GP_NEW 'GM_NEW'
		, case when isnull(tp.price_multiple, 1) > 1 then tp.price_multiple_New else null end 'PM_NEW'
		from #t_prod tp
		inner join #zones z on tp.entity_structure_fk = z.entity_structure_fk
		where 1 = 1
	Else
		Select item, isnull(tp.PRODUCT_STRUCTURE_FK, tp.PARENT_PRODUCT_STRUCTURE_FK) 'PRODUCT_STRUCTURE_FK', tp.Entity_Structure_FK 'ENTITY_STRUCTURE_FK'
		, tp.PRICE_NEW 'NEW_RETAIL', tp.GP_NEW 'GM_NEW'
		, tp.TM_PCT_CUR 'URM_TARGET_MARGIN'
		, case when isnull(tp.price_multiple, 1) > 1 then tp.price_multiple_New else null end 'PM_NEW'
		from #t_prod tp
		inner join #zones z on tp.entity_structure_fk = z.entity_structure_fk
		Where 1 = 1

eof:
--select * from #t_prod
