﻿	-- =====================================================
	-- Author:		Gary Stone
	-- Create date: 2019-08-13
	-- Description:	Load Level Gross Deltas into audit table
	-- =====================================================
	CREATE PROCEDURE [precision_comps].[level_gross_audit_items] @price_Change_Effective_Date Date = null
	AS
	set nocount on

	--declare @price_change_effective_date date = '2020-02-14'
	----declare @Marginchanges int = 0

	If @price_change_effective_date is null
	set @price_change_effective_date = cast(getdate() as date)

	delete from Precision_Comps.Level_Gross_Analysis_Weekly where price_change_effective_date = @price_change_effective_date --and ind_margin_changes = @MarginChanges
	delete from Precision_Comps.Level_Gross_Analysis_Weekly_price_Steps where price_change_effective_date = @price_change_effective_date --and ind_margin_changes = @MarginChanges
	delete from Precision_Comps.Level_Gross_Analysis_Mbr_Costs where price_change_effective_date = @price_change_effective_date

	Insert Into Precision_Comps.Level_Gross_Analysis_Weekly
	(
	[Comparison_Class], [change_type], [alias_id], [par_als], [link], [rims_paritm], [zone_id], [par_pitm], [par_citm], [item], [p_prc_c], [rims_p_c], [p_prc_init], [p_add], [p_rnded], [p_prc_new], [rims_pn]
	, [delta], [itm_cst_chg], [p_tm], [rims_tm], [p_cs_cst_C], [p_cs_cst_N], [rims_cs_cst_C], [rims_cs_cst_N], [p_u_cst_C], [p_u_cst_N], [rims_u_cst_C], [rims_u_cst_N], [p_gm_C], [p_gm_N], [rims_gm_C]
	, [rims_gm_N], [cost_change_reason], [last_in_rims], [p_cst_chg_dt], [rims_cst_chg_dt], [related_item_alias_id], [pricing_steps], [pricesheet_retail_changes_id], [rims_Q], [p_Q], [Qual_r_p], [lg_a_f]
	, [level_gross], [lg_activation], [ind_margin_changes], [PRODUCT_STRUCTURE_FK], [PROD_SEGMENT_FK], [ENTITY_STRUCTURE_FK], [ENTITY_DATA_NAME_FK], [DATA_NAME_FK], [Auth_Dt], [Alias_Dt], [Parity_Dt]
	, [lr_cs_cst_C], [lr_cs_cst_N], [lr_prc_C], [lr_prc_N], [ind_display_as_reg], [item_change_type_cr_fk], [price_change_effective_date]
	)
	select 
		'All Matching Items' 'Comparison_Class'
		, case prc.item_change_type_cr_fk when 655 then 'reg' when 661 then 'reg mc' when 656 then 'alias' when 657 then 'alias' when 662 then 'alias mc' end 'change_type'
		, prc.alias_id, prc.parity_alias_id 'par_als', rims.link, rims.item_parity 'rims_paritm', prc.zone 'zone_id'
		, prc.parity_parent_item 'par_pitm', prc.parity_child_item 'par_citm', prc.item
		, prc.price_cur 'p_prc_c', rims.price 'rims_p_c', prc.price_initial 'p_prc_init', prc.digit_adj 'p_add', prc.price_rounded 'p_rnded', prc.price_new 'p_prc_new', rims.new_prc 'rims_pn'
		, rims.new_prc - prc.price_new 'delta'
		, prc.ind_cost_change_item 'itm_cst_chg'
		, prc.target_margin 'p_tm'
		, cast(cast(rims.newtmpct as numeric(18, 4)) / 100 as numeric(18, 4)) 'rims_tm'
		, prc.case_cost_cur  'p_cs_cst_C'
		, prc.case_cost_new 'p_cs_cst_N'
		, rims.extcstcas 'rims_cs_cst_C'
		, rims.newcstcas 'rims_cs_cst_N'
		, prc.unit_cost_cur 'p_u_cst_C'
		, prc.unit_cost_new 'p_u_cst_N'
		, rims.unit_cost_cur 'rims_u_cst_C'
		, rims.unit_cost_new 'rims_u_cst_N'
		, prc.cur_gm_pct 'p_gm_C'
		, prc.new_gm_pct 'p_gm_N'
		, rims.gm_pct_cur 'rims_gm_C'
		, rims.gm_pct_new 'rims_gm_N'
		, case when isnull(prc.change_status, 0) = -1 then prc.cost_change_reason + ' decline' else prc.cost_change_reason end 'cost_change_reason'
		, (select top 1 effective_date from imports.urm_level_gross_audit r with (nolock) where r.product_structure_fk = prc.product_structure_fk and r.zone_entity_structure_fk = prc.zone_entity_structure_fk and r.effective_date < prc.price_change_effective_date and r.newcstcas <> r.extcstcas order by effective_date desc) 'last_in_rims'
		, prc.cost_change_effective_date 'p_cst_chg_dt', rims.new_cost_effective_date 'rims_cst_chg_dt', prc.related_item_alias_id
		, prc.pricing_steps
		, prc.pricesheet_retail_changes_id
		, rims.qual_flg 'rims_Q'
		, case prc.qualified_status_cr_fk when 653 then 'Y' else 'N' end 'p_Q'
		, isnull(rims.qual_flg, '')+ '/' + case prc.qualified_status_cr_fk when 653 then 'Y' when 654 then 'N' else '' end 'Qual_r_p'
		, prc.lg_activation + '/' + prc.level_gross 'lg_a_f'
		, prc.level_gross
		, prc.lg_activation
		, prc.ind_margin_changes
		, prc.PRODUCT_STRUCTURE_FK
		, prc.PRODUCT_STRUCTURE_FK 'PROD_SEGMENT_FK'
		, prc.ENTITY_STRUCTURE_FK
		, 151000 'ENTITY_DATA_NAME_FK'
		, 151110 'DATA_NAME_FK'
		, (select cast(create_timestamp as date) from epacube.product_association pa with (nolock) 
			where pa.data_name_fk = 159450 
			and pa.PRODUCT_STRUCTURE_FK = rims.PRODUCT_STRUCTURE_FK 
			and pa.entity_structure_fk = rims.zone_entity_structure_fk and pa.record_status_cr_fk = 1) 'Auth_Dt'
		, (select cast(create_timestamp as date) from epacube.product_mult_type pmt with (nolock) 
			where pmt.data_name_fk = 159450 
			and pmt.PRODUCT_STRUCTURE_FK = rims.PRODUCT_STRUCTURE_FK 
			and pmt.entity_structure_fk = rims.zone_entity_structure_fk and pmt.record_status_cr_fk = 1) 'Alias_Dt'	
		, (select top 1 cast(create_timestamp as date) from epacube.product_association pa with (nolock) 
			where pa.data_name_fk = 159903 
			and rims.PRODUCT_STRUCTURE_FK in (pa.PRODUCT_STRUCTURE_FK, pa.CHILD_PRODUCT_STRUCTURE_FK)
			and pa.entity_structure_fk = rims.zone_entity_structure_fk and pa.record_status_cr_fk = 1 order by cast(create_timestamp as date) desc) 'Parity_Dt'
		, (select top 1 extcstcas from imports.urm_level_gross_audit r with (nolock) 
			where r.product_structure_fk = prc.product_structure_fk 
			and r.zone_entity_structure_fk = prc.zone_entity_structure_fk 
			and r.effective_date < rims.Effective_Date 
			and r.newcstcas <> r.extcstcas 
			order by effective_date desc) 'lr_cs_cst_C'
		, (select top 1 newcstcas from imports.urm_level_gross_audit r with (nolock) 
			where r.product_structure_fk = prc.product_structure_fk 
			and r.zone_entity_structure_fk = prc.zone_entity_structure_fk 
			and r.effective_date < rims.Effective_Date 
			and r.newcstcas <> r.extcstcas 
			order by effective_date desc) 'lr_cs_cst_N'
		, (select top 1 price from imports.urm_level_gross_audit r with (nolock) 
			where r.product_structure_fk = prc.product_structure_fk 
			and r.zone_entity_structure_fk = prc.zone_entity_structure_fk 
			and r.effective_date < rims.Effective_Date 
			and r.newcstcas <> r.extcstcas 
			order by effective_date desc) 'lr_prc_C'
		, (select top 1 new_prc from imports.urm_level_gross_audit r with (nolock) 
			where r.product_structure_fk = prc.product_structure_fk 
			and r.zone_entity_structure_fk = prc.zone_entity_structure_fk 
			and r.effective_date < rims.Effective_Date 
			and r.newcstcas <> r.extcstcas 
			order by effective_date desc) 'lr_prc_N'
		, prc.ind_display_as_reg
		, prc.item_change_type_cr_fk
		, @price_change_effective_date 'price_change_effective_date'
	from marginmgr.pricesheet_retail_changes prc with (nolock)
	inner join imports.urm_level_gross_audit rims with (nolock) on prc.product_structure_fk = rims.product_structure_fk
																				and prc.entity_structure_fk = rims.zone_entity_structure_fk
																				and prc.price_change_effective_date = rims.effective_date
	where 1 = 1
	and prc.price_change_effective_date = @price_change_effective_date
	--and isnull(prc.ind_margin_changes, 0) = @MarginChanges

	Insert into Precision_Comps.Level_Gross_Analysis_Weekly
	(
	[Comparison_Class], [change_type], [alias_id], [par_als], [link], [rims_paritm], [zone_id], [par_pitm], [par_citm], [item], [p_prc_c], [rims_p_c], [p_prc_init], [p_add], [p_rnded], [p_prc_new], [rims_pn]
	, [delta], [itm_cst_chg], [p_tm], [rims_tm], [p_cs_cst_C], [p_cs_cst_N], [rims_cs_cst_C], [rims_cs_cst_N], [p_u_cst_C], [p_u_cst_N], [rims_u_cst_C], [rims_u_cst_N], [p_gm_C], [p_gm_N], [rims_gm_C]
	, [rims_gm_N], [cost_change_reason], [last_in_rims], [last_in_precision], [p_cst_chg_dt], [rims_cst_chg_dt], [related_item_alias_id], [pricing_steps], [pricesheet_retail_changes_id], [rims_Q], [p_Q], [Qual_r_p], [lg_a_f]
	, [level_gross], [lg_activation], [ind_margin_changes], [PRODUCT_STRUCTURE_FK], [PROD_SEGMENT_FK], [ENTITY_STRUCTURE_FK], [ENTITY_DATA_NAME_FK], [DATA_NAME_FK], [Auth_Dt], [Alias_Dt], [Parity_Dt]
	, [lr_cs_cst_C], [lr_cs_cst_N], [lr_prc_C], [lr_prc_N], [ind_display_as_reg], [item_change_type_cr_fk], [price_change_effective_date]
	)
	select distinct
		'Not In Precision' 'Comparison_Class'
		, case prc.item_change_type_cr_fk when 655 then 'reg' when 661 then 'reg mc' when 656 then 'alias' when 657 then 'alias' when 662 then 'alias mc' end 'change_type'
		, prc.alias_id, prc.parity_alias_id 'par_als', rims.link, rims.item_parity 'rims_paritm', rims.ZONE_NUM 'zone_id'
		, prc.parity_parent_item 'par_pitm', prc.parity_child_item 'par_citm', rims.item
		, prc.price_cur 'p_prc_c', rims.price 'rims_p_c', prc.price_initial 'p_prc_init', prc.digit_adj 'p_add', prc.price_rounded 'p_rnded', prc.price_new 'p_prc_new', rims.new_prc 'rims_pn'
		, rims.new_prc - prc.price_new 'delta'
		, prc.ind_cost_change_item 'itm_cst_chg'
		, prc.target_margin 'p_tm'
		, cast(cast(rims.newtmpct as numeric(18, 4)) / 100 as numeric(18, 4)) 'rims_tm'
		, prc.case_cost_cur  'p_cs_cst_C'
		, prc.case_cost_new 'p_cs_cst_N'
		, rims.extcstcas 'rims_cs_cst_C'
		, rims.newcstcas 'rims_cs_cst_N'
		, prc.unit_cost_cur 'p_u_cst_C'
		, prc.unit_cost_new 'p_u_cst_N'
		, rims.unit_cost_cur 'rims_u_cst_C'
		, rims.unit_cost_new 'rims_u_cst_N'
		, prc.cur_gm_pct 'p_gm_C'
		, prc.new_gm_pct 'p_gm_N'
		, rims.gm_pct_cur 'rims_gm_C'
		, rims.gm_pct_new 'rims_gm_N'
		, (select top 1 case when isnull(r.change_status, 0) = -1 then r.cost_change_reason + ' decline' else r.cost_change_reason end from marginmgr.PRICESHEET_RETAIL_CHANGES r with (nolock) 
			where 1 = 1
			and r.product_structure_fk = rims.product_structure_fk 
			and r.zone_entity_structure_fk = rims.zone_entity_structure_fk 
			order by effective_date desc) 'cost_change_reason'
		, (select top 1 effective_date from imports.urm_level_gross_audit r with (nolock) where r.product_structure_fk = rims.product_structure_fk and r.zone_entity_structure_fk = rims.zone_entity_structure_fk and effective_date < @price_change_effective_date order by effective_date desc) 'last_in_rims'
		, (select top 1 price_change_effective_date from marginmgr.PRICESHEET_RETAIL_CHANGES r with (nolock) 
			where 1 = 1
			and r.product_structure_fk = rims.product_structure_fk 
			and r.zone_entity_structure_fk = rims.zone_entity_structure_fk 
			order by effective_date desc) 'last_in_precision'		, prc.cost_change_effective_date 'p_cst_chg_dt', rims.new_cost_effective_date 'rims_cst_chg_dt', prc.related_item_alias_id
		, prc.pricing_steps
		, prc.pricesheet_retail_changes_id
		, rims.qual_flg 'rims_Q'
		, case prc.qualified_status_cr_fk when 653 then 'Y' else 'N' end 'p_Q'
		, isnull(rims.qual_flg, '')+ '/' + case prc.qualified_status_cr_fk when 653 then 'Y' when 654 then 'N' else '' end 'Qual_r_p'
		, prc.lg_activation + '/' + prc.level_gross 'lg_a_f'
		, prc.level_gross
		, prc.lg_activation
		, prc.ind_margin_changes
		, pi.PRODUCT_STRUCTURE_FK
		, pi.PRODUCT_STRUCTURE_FK 'PROD_SEGMENT_FK'
		, pi.ENTITY_STRUCTURE_FK
		, 151000 'ENTITY_DATA_NAME_FK'
		, 151110 'DATA_NAME_FK'
		, (select cast(create_timestamp as date) from epacube.product_association pa with (nolock) 
			where pa.data_name_fk = 159450 
			and pa.PRODUCT_STRUCTURE_FK = rims.PRODUCT_STRUCTURE_FK 
			and pa.entity_structure_fk = rims.zone_entity_structure_fk and pa.record_status_cr_fk = 1) 'Auth_Dt'
		, (select cast(create_timestamp as date) from epacube.product_mult_type pmt with (nolock) 
			where pmt.data_name_fk = 159450 
			and pmt.PRODUCT_STRUCTURE_FK = rims.PRODUCT_STRUCTURE_FK 
			and pmt.entity_structure_fk = rims.zone_entity_structure_fk and pmt.record_status_cr_fk = 1) 'Alias_Dt'	
		, (select top 1 cast(create_timestamp as date) from epacube.product_association pa with (nolock) 
			where pa.data_name_fk = 159903 
			and rims.PRODUCT_STRUCTURE_FK in (pa.PRODUCT_STRUCTURE_FK, pa.CHILD_PRODUCT_STRUCTURE_FK)
			and pa.entity_structure_fk = rims.zone_entity_structure_fk and pa.record_status_cr_fk = 1 order by cast(create_timestamp as date) desc) 'Parity_Dt'
		, (select top 1 extcstcas from imports.urm_level_gross_audit r with (nolock) 
			where r.product_structure_fk = prc.product_structure_fk 
			and r.zone_entity_structure_fk = prc.zone_entity_structure_fk 
			and r.effective_date < rims.Effective_Date 
			and r.newcstcas <> r.extcstcas 
			order by effective_date desc) 'lr_cs_cst_C'
		, (select top 1 newcstcas from imports.urm_level_gross_audit r with (nolock) 
			where r.product_structure_fk = prc.product_structure_fk 
			and r.zone_entity_structure_fk = prc.zone_entity_structure_fk 
			and r.effective_date < rims.Effective_Date 
			and r.newcstcas <> r.extcstcas 
			order by effective_date desc) 'lr_cs_cst_N'
		, (select top 1 price from imports.urm_level_gross_audit r with (nolock) 
			where r.product_structure_fk = prc.product_structure_fk 
			and r.zone_entity_structure_fk = prc.zone_entity_structure_fk 
			and r.effective_date < rims.Effective_Date 
			and r.newcstcas <> r.extcstcas 
			order by effective_date desc) 'lr_prc_C'
		, (select top 1 new_prc from imports.urm_level_gross_audit r with (nolock) 
			where r.product_structure_fk = prc.product_structure_fk 
			and r.zone_entity_structure_fk = prc.zone_entity_structure_fk 
			and r.effective_date < rims.Effective_Date 
			and r.newcstcas <> r.extcstcas 
			order by effective_date desc) 'lr_prc_N'
		, prc.ind_display_as_reg
		, prc.item_change_type_cr_fk
		, @price_change_effective_date 'price_change_effective_date'
	from imports.urm_level_gross_audit rims with (nolock)
	left join marginmgr.PRICESHEET_RETAIL_CHANGES prc with (nolock) on prc.item = rims.item
																				and prc.zone = rims.ZONE_NUM
																				and prc.price_change_effective_date = rims.Effective_Date
	inner join epacube.PRODUCT_IDENTIFICATION pi with (nolock) on rims.item = pi.value and pi.DATA_NAME_FK = 110100
	inner join epacube.ENTITY_IDENTIFICATION ei with (nolock) on rims.ZONE_NUM = ei.value and ei.ENTITY_DATA_NAME_FK = 151000 and ei.DATA_NAME_FK = 151110
	inner join epacube.entity_attribute ea with (nolock) on ei.ENTITY_STRUCTURE_FK = ea.entity_structure_fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
			inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	where 1 = 1
	and dv.value = 'H'
	--and isnull(prc.ind_margin_changes, 0) = @MarginChanges
	and rims.Effective_Date = @PRICE_CHANGE_EFFECTIVE_DATE
	and rims.newcstcas <> rims.extcstcas
	and prc.PRICESHEET_RETAIL_CHANGES_ID is null

	Insert into Precision_Comps.Level_Gross_Analysis_Weekly
	(
	[Comparison_Class], [change_type], [alias_id], [par_als], [link], [rims_paritm], [zone_id], [par_pitm], [par_citm], [item], [p_prc_c], [rims_p_c], [p_prc_init], [p_add], [p_rnded], [p_prc_new], [rims_pn]
	, [delta], [itm_cst_chg], [p_tm], [rims_tm], [p_cs_cst_C], [p_cs_cst_N], [rims_cs_cst_C], [rims_cs_cst_N], [p_u_cst_C], [p_u_cst_N], [rims_u_cst_C], [rims_u_cst_N], [p_gm_C], [p_gm_N], [rims_gm_C]
	, [rims_gm_N], [cost_change_reason], [last_in_rims], [p_cst_chg_dt], [rims_cst_chg_dt], [related_item_alias_id], [pricing_steps], [pricesheet_retail_changes_id], [rims_Q], [p_Q], [Qual_r_p], [lg_a_f]
	, [level_gross], [lg_activation], [ind_margin_changes], [PRODUCT_STRUCTURE_FK], [PROD_SEGMENT_FK], [ENTITY_STRUCTURE_FK], [ENTITY_DATA_NAME_FK], [DATA_NAME_FK], [Auth_Dt], [Alias_Dt], [Parity_Dt]
	, [lr_cs_cst_C], [lr_cs_cst_N], [lr_prc_C], [lr_prc_N], [ind_display_as_reg], [item_change_type_cr_fk], [price_change_effective_date]
	)
	select 
		'Not In RIMS' 'Comparison_Class'
		, case prc.item_change_type_cr_fk when 655 then 'reg' when 661 then 'reg mc' when 656 then 'alias' when 657 then 'alias' when 662 then 'alias mc' end 'change_type'
		, prc.alias_id, prc.parity_alias_id 'par_als', rims.link, rims.item_parity 'rims_paritm', prc.zone 'zone_id'
		, prc.parity_parent_item 'par_pitm', prc.parity_child_item 'par_citm', prc.item
		, prc.price_cur 'p_prc_c', rims.price 'rims_p_c', prc.price_initial 'p_prc_init', prc.digit_adj 'p_add', prc.price_rounded 'p_rnded', prc.price_new 'p_prc_new', rims.new_prc 'rims_pn'
		, rims.new_prc - prc.price_new 'delta'
		, prc.ind_cost_change_item 'itm_cst_chg'
		, prc.target_margin 'p_tm'
		, cast(cast(rims.newtmpct as numeric(18, 4)) / 100 as numeric(18, 4)) 'rims_tm'
		, prc.case_cost_cur  'p_cs_cst_C'
		, prc.case_cost_new 'p_cs_cst_N'
		, rims.extcstcas 'rims_cs_cst_C'
		, rims.newcstcas 'rims_cs_cst_N'
		, prc.unit_cost_cur 'p_u_cst_C'
		, prc.unit_cost_new 'p_u_cst_N'
		, rims.unit_cost_cur 'rims_u_cst_C'
		, rims.unit_cost_new 'rims_u_cst_N'
		, prc.cur_gm_pct 'p_gm_C'
		, prc.new_gm_pct 'p_gm_N'
		, rims.gm_pct_cur 'rims_gm_C'
		, rims.gm_pct_new 'rims_gm_N'
		, case when isnull(prc.change_status, 0) = -1 then prc.cost_change_reason + ' decline' else prc.cost_change_reason end 'cost_change_reason'
		, (select top 1 effective_date from imports.urm_level_gross_audit r with (nolock) where r.product_structure_fk = prc.product_structure_fk and r.zone_entity_structure_fk = prc.zone_entity_structure_fk and r.effective_date < prc.price_change_effective_date and r.newcstcas <> r.extcstcas order by effective_date desc) 'last_in_rims'
		, prc.cost_change_effective_date 'p_cst_chg_dt', rims.new_cost_effective_date 'rims_cst_chg_dt', prc.related_item_alias_id
		, prc.pricing_steps
		, prc.pricesheet_retail_changes_id
		, rims.qual_flg 'rims_Q'
		, case prc.qualified_status_cr_fk when 653 then 'Y' else 'N' end 'p_Q'
		, isnull(rims.qual_flg, '')+ '/' + case prc.qualified_status_cr_fk when 653 then 'Y' when 654 then 'N' else '' end 'Qual_r_p'
		, prc.lg_activation + '/' + prc.level_gross 'lg_a_f'
		, prc.level_gross
		, prc.lg_activation
		, prc.ind_margin_changes
		, prc.PRODUCT_STRUCTURE_FK
		, prc.PRODUCT_STRUCTURE_FK 'PROD_SEGMENT_FK'
		, prc.ENTITY_STRUCTURE_FK
		, 151000 'ENTITY_DATA_NAME_FK'
		, 151110 'DATA_NAME_FK'
		, (select cast(create_timestamp as date) from epacube.product_association pa with (nolock) 
			where pa.data_name_fk = 159450 
			and pa.PRODUCT_STRUCTURE_FK = rims.PRODUCT_STRUCTURE_FK 
			and pa.entity_structure_fk = rims.zone_entity_structure_fk and pa.record_status_cr_fk = 1) 'Auth_Dt'
		, (select cast(create_timestamp as date) from epacube.product_mult_type pmt with (nolock) 
			where pmt.data_name_fk = 159450 
			and pmt.PRODUCT_STRUCTURE_FK = rims.PRODUCT_STRUCTURE_FK 
			and pmt.entity_structure_fk = rims.zone_entity_structure_fk and pmt.record_status_cr_fk = 1) 'Alias_Dt'	
		, (select top 1 cast(create_timestamp as date) from epacube.product_association pa with (nolock) 
			where pa.data_name_fk = 159903 
			and rims.PRODUCT_STRUCTURE_FK in (pa.PRODUCT_STRUCTURE_FK, pa.CHILD_PRODUCT_STRUCTURE_FK)
			and pa.entity_structure_fk = rims.zone_entity_structure_fk and pa.record_status_cr_fk = 1 order by cast(create_timestamp as date)) 'Parity_Dt'
		, (select top 1 extcstcas from imports.urm_level_gross_audit r with (nolock) 
			where r.product_structure_fk = prc.product_structure_fk 
			and r.zone_entity_structure_fk = prc.zone_entity_structure_fk 
			and r.effective_date < rims.Effective_Date 
			and r.newcstcas <> r.extcstcas 
			order by effective_date desc) 'lr_cs_cst_C'
		, (select top 1 newcstcas from imports.urm_level_gross_audit r with (nolock) 
			where r.product_structure_fk = prc.product_structure_fk 
			and r.zone_entity_structure_fk = prc.zone_entity_structure_fk 
			and r.effective_date < rims.Effective_Date 
			and r.newcstcas <> r.extcstcas 
			order by effective_date desc) 'lr_cs_cst_N'
		, (select top 1 price from imports.urm_level_gross_audit r with (nolock) 
			where r.product_structure_fk = prc.product_structure_fk 
			and r.zone_entity_structure_fk = prc.zone_entity_structure_fk 
			and r.effective_date < rims.Effective_Date 
			and r.newcstcas <> r.extcstcas 
			order by effective_date desc) 'lr_prc_C'
		, (select top 1 new_prc from imports.urm_level_gross_audit r with (nolock) 
			where r.product_structure_fk = prc.product_structure_fk 
			and r.zone_entity_structure_fk = prc.zone_entity_structure_fk 
			and r.effective_date < rims.Effective_Date 
			and r.newcstcas <> r.extcstcas 
			order by effective_date desc) 'lr_prc_N'
		, prc.ind_display_as_reg
		, prc.item_change_type_cr_fk
		, @price_change_effective_date 'price_change_effective_date'
	from marginmgr.PRICESHEET_RETAIL_CHANGES prc with (nolock)
	left join imports.urm_level_gross_audit rims with (nolock) on prc.item = rims.item
																				and prc.zone = rims.ZONE_NUM
																				and prc.price_change_effective_date = rims.Effective_Date
	inner join epacube.entity_attribute ea with (nolock) on prc.ZONE_ENTITY_STRUCTURE_FK = ea.entity_structure_fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
			inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	where 1 = 1
	and prc.price_change_effective_date = @PRICE_CHANGE_EFFECTIVE_DATE
	and prc.ITEM_CHANGE_TYPE_CR_FK <> 656
	--and isnull(prc.ind_margin_changes, 0) = @MarginChanges
	--and prc.unit_cost_cur<> prc.unit_cost_new
	and prc.lg_activation = 'Y'
	and prc.LEVEL_GROSS = 'Y'
	and rims.URM_LEVEL_GROSS_AUDIT_ID is null
	and dv.value = 'H'

--	Update target margin in alias to value used in alias calculations where appropriate
	Update LGAW
	set lgaw.p_tm =prc.target_margin
	from Precision_Comps.Level_Gross_Analysis_Weekly LGAW
	inner join marginmgr.pricesheet_retail_changes prc with (nolock) on lgaw.alias_id = prc.alias_id
																	and prc.item_change_type_cr_fk = 656
																	and lgaw.ENTITY_STRUCTURE_FK = prc.zone_entity_structure_fk
																	and lgaw.price_change_effective_date = prc.price_change_effective_date
	where lgaw.price_change_effective_date = @price_Change_Effective_Date
	and prc.target_margin <> lgaw.p_tm

----Load into separate table Pricing Steps for all transaction
--declare @price_change_effective_date date = '2020-02-14'
--delete from Precision_Comps.Level_Gross_Analysis_Weekly_price_Steps where price_change_effective_date = @price_change_effective_date

	If object_id('tempdb..#t_prod01') is not null
	drop table #t_prod01

	Select 
	prc.alias_id
	, prc.parity_alias_id
	, prc.parity_status
	, prc.zone
	, prc.item
	, isnull(prc.parity_parent_item, prc.parity_child_item) 'parity_itm'
	, prc.related_item_alias_id
	, prc.ind_cost_change_item
	, prc.unit_cost_new 
	, prc.target_margin 
	, prc.price_initial
	, prc.use_lg_price_points
	, prc.digit_adj
	, prc.price_rounded
	, prc.price_new
	, prc.minamt
	, prc.lg_activation
	, prc.level_gross
	, prc.cost_change_reason
	, prc.pricing_steps
	, prc.ind_margin_changes
		, (select cast(create_timestamp as date) from epacube.product_association pa with (nolock) 
			where pa.data_name_fk = 159450 
			and pa.PRODUCT_STRUCTURE_FK = prc.PRODUCT_STRUCTURE_FK 
			and pa.entity_structure_fk = prc.zone_entity_structure_fk and pa.record_status_cr_fk = 1) 'Auth_Dt'
		, (select cast(create_timestamp as date) from epacube.product_mult_type pmt with (nolock) 
			where pmt.data_name_fk = 159450 
			and pmt.PRODUCT_STRUCTURE_FK = prc.PRODUCT_STRUCTURE_FK 
			and pmt.entity_structure_fk = prc.zone_entity_structure_fk and pmt.record_status_cr_fk = 1) 'Alias_Dt'	
		, (select top 1 cast(create_timestamp as date) from epacube.product_association pa with (nolock) 
			where pa.data_name_fk = 159903 
			and prc.PRODUCT_STRUCTURE_FK in (pa.PRODUCT_STRUCTURE_FK, pa.CHILD_PRODUCT_STRUCTURE_FK)
			and pa.entity_structure_fk = prc.zone_entity_structure_fk and pa.record_status_cr_fk = 1 order by cast(create_timestamp as date) desc) 'Parity_Dt'
	, prc.daysout
	, prc.PRODUCT_STRUCTURE_FK
	, prc.PROD_SEGMENT_FK
	, prc.ENTITY_STRUCTURE_FK
	, price_change_effective_date
	, prc.item_change_type_cr_fk
	, prc.pricesheet_retail_changes_id 'pricesheet_retail_changes_fk'
	into #t_prod01
	from marginmgr.PRICESHEET_RETAIL_CHANGES prc
	where 1 = 1
	and prc.price_change_effective_date = @price_change_effective_date

	If object_id('tempdb..#t_prod02') is not null
	drop table #t_prod02

	create table #t_prod02(pricesheet_retail_changes_fk bigint not null, Pricing_Token varchar(64) not null)
	create index idx_o1 on #t_prod02(pricesheet_retail_changes_fk)

	DECLARE 
		@pricesheet_retail_changes_fk bigint
		, @token varchar(max)

		DECLARE cur_pricing CURSOR local for
			SELECT pricesheet_retail_changes_fk, pricing_steps from #t_prod01

				OPEN cur_pricing;
				FETCH NEXT FROM cur_pricing INTO @pricesheet_retail_changes_fk,
												@token
			WHILE @@FETCH_STATUS = 0
			BEGIN

			INSERT INTO #t_prod02
				(pricesheet_retail_changes_fk
				,Pricing_Token
				)


			select
					@pricesheet_retail_changes_fk
					, *
			from utilities.SplitString ( @token, '~' ) ss	 

				FETCH NEXT FROM cur_pricing INTO @pricesheet_retail_changes_fk,
												@token

	END 
	CLOSE cur_pricing
	DEALLOCATE cur_pricing

	Insert into Precision_Comps.Level_Gross_Analysis_Weekly_price_Steps
	(
	[alias_id], [par_als], [zone_id], [item], [par_itm], [rltd_alias], parity_status, [cost_change_reason], [pricing_step], [price], pricing_phase, [pricing_logic], [auth_dt], [alias_dt], [Parity_dt], [item_change_type_cr_fk], [PRODUCT_STRUCTURE_FK], [PROD_SEGMENT_FK], [ENTITY_STRUCTURE_FK], [Level_Gross_Analysis_Weekly_FK], [price_change_effective_date], [ind_margin_changes], PRICESHEET_RETAIL_CHANGES_FK
	, ind_cost_change_item, unit_cost_new, target_margin, price_initial, use_lg_price_points, digit_adj, price_rounded, minamt, lg_activation, level_gross, daysout, create_timestamp
	)
	select
	tp.alias_id
	, tp.parity_alias_id
	, tp.zone
	, tp.item
	, tp.parity_itm
	, tp.related_item_alias_id
	, tp.parity_status
	, tp.cost_change_reason
	, psteps.pricing_step
	, psteps.price

	, case cast(replace(psteps.pricing_step, '_', '') as int)
			when 1 then 'Initial'
			when 10 then 'Parity'
			when 15 then 'Parity'
			when 20 then 'Parity'
			when 22 then 'Parity'
			when 25 then 'Parity'
			when 30 then 'Parity'
			when 35 then 'Parity'

			when 36 then 'Parity'
			when 37 then 'Parity'
			when 38 then 'Parity'

			when 40 then 'Alias'
			when 110 then 'Ntl Alias Parity to PL Item'
			when 115 then 'Ntl Alias Parity to PL Item'
			when 120 then 'Ntl Alias Parity to PL Item'
			when 125 then 'Ntl Alias Parity to PL Item'
			when 130 then 'Ntl Alias Parity to PL Item'
			when 210 then 'PL Alias Parity to Ntl Item'
			when 215 then 'PL Alias Parity to Ntl Item'
			when 220 then 'PL Alias Parity to Ntl Item'
			when 225 then 'PL Alias Parity to Ntl Item'
			when 230 then 'PL Alias Parity to Ntl Item'
			when 310 then 'Aliases in Parity'
			when 315 then 'Aliases in Parity'
			when 320 then 'Aliases in Parity'
			when 325 then 'Aliases in Parity'
			when 330 then 'Aliases in Parity'
			when 657 then 'Aliases in Parity'
			end 'pricing_logic'
	
	, case cast(replace(psteps.pricing_step, '_', '') as int)
			when 1 then 'Set prices all items to ppts and rounding'
			when 10 then 'Set PL to discount from National'
			when 15 then 'Set Ntl to parity differential of PL'
			when 20 then 'Step 15 causes National Price Decline'
			When 22 then 'Re-price PL to parity diff w/ National w 15, 20'
			when 25 then 'Set National New Price to Current'
			when 30 then 'Set PL to discount from National'
			when 35 then 'alias items - max parity price when higher than non-parity items w/o 30'

			when 36 then 'Adjust PL related - Ntl No Inc w/o 15'
			when 37 then 'Adjust PL related - Ntl Inc w/o 15'
			when 38 then 'Adjust PL items in alias to same price w/o 30'

			when 40 then 'Alias Hdr Recs created'
			when 110 then 'Set PL to discount from National'
			when 115 then 'Set Ntl to parity differential of PL'
			when 120 then 'Step 115 causes National Price Decline'
			when 125 then 'Set National New Price to Current'
			when 130 then 'Set PL to discount from National'
			when 210 then 'Set PL to discount from National'
			when 215 then 'Set Ntl to parity differential of PL'
			when 220 then 'Step 215 causes National Price Decline'
			when 225 then 'Set National New Price to Current'
			when 230 then 'Set PL to discount from National'
			when 310 then 'Set PL to discount from National'
			when 315 then 'Set Ntl to parity differential of PL w/o 22, 30, 35-37'
			when 320 then 'Step 315 causes National Price Decline'
			when 325 then 'Set National New Price to Current'
			when 330 then 'Set PL to discount from National'
			when 657 then 'Update alias records to match headers'
			end 'pricing_logic'
	, tp.auth_dt
	, tp.alias_dt
	, tp.Parity_dt
	, tp.item_change_type_cr_fk
	, tp.PRODUCT_STRUCTURE_FK
	, tp.PROD_SEGMENT_FK
	, tp.ENTITY_STRUCTURE_FK
	, 0 'Level_Gross_Analysis_Weekly_FK'
	, tp.price_change_effective_date
	, tp.[ind_margin_changes]
	, TP.PRICESHEET_RETAIL_CHANGES_FK
	, tp.ind_cost_change_item, tp.unit_cost_new, tp.target_margin, tp.price_initial, tp.use_lg_price_points, tp.digit_adj, tp.price_rounded, tp.minamt, tp.lg_activation, tp.level_gross, tp.daysout, getdate()
	from #t_prod01 tp
	left join
	(
		Select 
		PRICESHEET_RETAIL_CHANGES_FK
		, replace(left(pricing_token, pos - 1), '_', '') 'pricing_step'
		, cast(right(pricing_token, len(pricing_token) - pos) as numeric(18, 2)) 'Price'
		from (
		select 
		PRICESHEET_RETAIL_CHANGES_FK
		, pricing_token
		, charindex(':', Pricing_Token) pos
		from #t_prod02
		) A
	) psteps on tp.PRICESHEET_RETAIL_CHANGES_FK = psteps.PRICESHEET_RETAIL_CHANGES_FK

	Insert into Precision_Comps.Level_Gross_Analysis_Mbr_Costs
	(eff_dt, cs_cost, unit_cst, chg, pack, rescind, created, Imprt_dt, update_user, PRODUCT_STRUCTURE_FK, PROD_SEGMENT_FK, pricesheet_basis_values_fk, price_change_effective_date, record_status_cr_fk, unique_cost_record_id)
	select
	pbv.effective_date 'eff_dt'
	, value 'cs_cost'
	, unit_value 'unit_cst'
	, case when isnull(change_status, 0) = 0 then 'Increase' else 'Decline' end 'chg'
	, pack
	, cast(RESCIND_TIMESTAMP as date) 'rescind'
	, cast(create_timestamp as date) 'created'
	, cast(import_timestamp as date) 'Imprt_dt'
	, update_user
	, pbv.PRODUCT_STRUCTURE_FK
	, pbv.PRODUCT_STRUCTURE_FK 'PROD_SEGMENT_FK'
	, pbv.Pricesheet_Basis_Values_ID 'pricesheet_basis_values_fk'
	, @price_change_effective_date 'price_change_effective_date'
	, pbv.record_status_cr_fk
	, pbv.unique_cost_record_id
	from marginmgr.pricesheet_basis_values pbv with (nolock)
	inner join
			(
				Select product_structure_fk
				from marginmgr.PRICESHEET_RETAIL_CHANGES prc with (nolock)
				where price_change_effective_date = @price_change_effective_date
				group by product_structure_fk
				Union
				Select product_structure_fk
				from IMPORTS.URM_LEVEL_GROSS_AUDIT
				where effective_date = @price_change_effective_date
				group by PRODUCT_STRUCTURE_FK
			) prods on pbv.Product_Structure_FK = prods.PRODUCT_STRUCTURE_FK
	where 1 = 1
	and pbv.Cust_Entity_Structure_FK is null
	and pbv.Zone_Entity_Structure_FK is not null
	and pbv.data_name_fk = 503201
	Group by
	pbv.effective_date
	, pbv.value
	, pbv.unit_value
	, case when isnull(pbv.change_status, 0) = 0 then 'Increase' else 'Decline' end
	, pbv.pack
	, cast(pbv.RESCIND_TIMESTAMP as date)
	, cast(pbv.create_timestamp as date)
	, cast(pbv.import_timestamp as date)
	, pbv.update_user
	, pbv.PRODUCT_STRUCTURE_FK
	, pbv.PRODUCT_STRUCTURE_FK
	, pbv.Pricesheet_Basis_Values_ID
	, isnull(pbv.rescind, 0)
	, pbv.record_status_cr_fk
	, pbv.unique_cost_record_id
	order by pbv.product_structure_fk, pbv.effective_date desc, isnull(pbv.rescind, 0)
