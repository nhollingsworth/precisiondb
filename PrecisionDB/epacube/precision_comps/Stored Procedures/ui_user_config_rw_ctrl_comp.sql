﻿
-- ==============================================
-- Author:		Gary Stone
-- Create date: 4/3/2019
-- Description:	Provide user configurations to UI
-- ==============================================
CREATE PROCEDURE [precision_comps].[ui_user_config_rw_ctrl_comp]

@CONTROL_EFFECTIVE_DATE varchar(64)
, @CONTROL_USE_PRICE_POINTS_ROUNDING varchar(8)
, @CONTROL_UPDATE_TM_PCT varchar(8)
, @CONTROL_APPLY_PARITY_UPDATE varchar(8)
, @USERNAME varchar(64)
, @COLUMN_CHANGE_NAME varchar(64)
, @COMPONENT_ID varchar(16)

AS
	SET NOCOUNT ON;

--declare @CONTROL_EFFECTIVE_DATE as varchar(64) = '2019-09-10'
--declare @CONTROL_USE_PRICE_POINTS_ROUNDING as varchar(8) = 'Y'
--declare @CONTROL_UPDATE_TM_PCT as varchar(8) = 'Y'
--declare @CONTROL_APPLY_PARITY_UPDATE as varchar(8) = 'N'
--declare @USERNAME as varchar(64) = 'gstone@epacube.com'
--declare @COLUMN_CHANGE_NAME varchar(64) = 'CONTROL_APPLY_PARITY_UPDATE'
----declare @COLUMN_CHANGE_NAME varchar(64) = 'CONTROL_UPDATE_TM_PCT'
----declare @COLUMN_CHANGE_NAME varchar(64) = 'CONTROL_APPLY_PARITY_UPDATE'
--Declare @COMPONENT_ID varchar(16) = 31560

Set @username = replace(@username, '.', '_')
Declare @ui_name varchar(128) = 'filter_comp'
Declare @field_name varchar(64) = Null
Declare @value varchar(64) = null
Declare @Label varchar(64) = null
Declare @checkbox int = 0
Declare @action int = 1	--@Action; 0 = Read, 1 = Write
Declare @SQL varchar(max)

Set @value = replace(replace(Case @COLUMN_CHANGE_NAME when 'CONTROL_EFFECTIVE_DATE' then @CONTROL_EFFECTIVE_DATE
										when 'CONTROL_USE_PRICE_POINTS_ROUNDING' then @CONTROL_USE_PRICE_POINTS_ROUNDING
										when 'CONTROL_UPDATE_TM_PCT' then @CONTROL_UPDATE_TM_PCT
										when 'CONTROL_APPLY_PARITY_UPDATE' then @CONTROL_APPLY_PARITY_UPDATE end, 'Y', 'yes'), 'N', 'no')

Set @field_name = (select data_name_id from epacube.data_name where name = @COLUMN_CHANGE_NAME)

Update uucr
Set [value] = @value
, Label = @COLUMN_CHANGE_NAME
, update_timestamp = getdate()
from epacube.precision.ui_user_config uucr where 1 = 1 and username = @username and ui_name = @ui_name and field_name = @field_name

select * from epacube.precision.ui_user_config where username = @USERNAME and field_name in (select cast(data_name_fk as varchar(16)) from epacube.ui_comp where UI_COMP_MASTER_FK = @COMPONENT_ID)

Set @SQL = '
Update tbl
Set ' + @COLUMN_CHANGE_NAME + ' =  ' + '''' + replace(replace(@value, 'YES', 'Y'), 'NO', 'N') + '''
from temptbls.tmp_results_'+ @COMPONENT_ID + '_' + @USERNAME + ' tbl'

exec(@SQL)

Set @SQL = '
update tbl
Set CritValue = ' + '''' + @value + '''
, OpExclusion = ' + '''' + @value + '''
from temptbls.tmp_FILTER_' + @username + ' tbl
inner join epacube.epacube.data_name dn on tbl.critDN = dn.DATA_NAME_ID
and dn.name = ''' + @COLUMN_CHANGE_NAME + ''''

exec(@SQL)


Set @SQL = '
update tbl
Set OptionSetting = ' + '''' + @value + '''
from temptbls.tmp_filters_InForce_' + @username + ' tbl
inner join epacube.epacube.data_name dn on tbl.critDN = dn.DATA_NAME_ID
and dn.name = ''' + @COLUMN_CHANGE_NAME + ''''

exec(@SQL)

--select * from temptbls.tmp_Filters_InForce_gstone@epacube_com
--select * from temptbls.tmp_Filter_gstone@epacube_com
