﻿CREATE TABLE [precision_comps].[LEVEL_GROSS_ANALYSIS_WEEKLY_PRICE_STEPS] (
    [alias_id]                                   VARCHAR (16)    NULL,
    [par_als]                                    BIGINT          NULL,
    [zone_id]                                    VARCHAR (128)   NULL,
    [item]                                       VARCHAR (64)    NULL,
    [par_itm]                                    VARCHAR (64)    NULL,
    [rltd_alias]                                 BIGINT          NULL,
    [cost_change_reason]                         VARCHAR (72)    NULL,
    [pricing_step]                               VARCHAR (8000)  NULL,
    [price]                                      NUMERIC (18, 2) NULL,
    [pricing_logic]                              VARCHAR (MAX)   NULL,
    [auth_dt]                                    DATE            NULL,
    [alias_dt]                                   DATE            NULL,
    [Parity_dt]                                  DATE            NULL,
    [item_change_type_cr_fk]                     INT             NULL,
    [PRODUCT_STRUCTURE_FK]                       BIGINT          NULL,
    [PROD_SEGMENT_FK]                            BIGINT          NULL,
    [ENTITY_STRUCTURE_FK]                        BIGINT          NULL,
    [Level_Gross_Analysis_Weekly_FK]             BIGINT          NOT NULL,
    [price_change_effective_date]                DATE            NULL,
    [Level_Gross_Analysis_Weekly_price_Steps_ID] BIGINT          IDENTITY (1, 1) NOT NULL,
    [ind_margin_changes]                         INT             NULL,
    [PRICESHEET_RETAIL_CHANGES_FK]               BIGINT          NULL,
    [pricing_phase]                              VARCHAR (128)   NULL,
    [parity_status]                              VARCHAR (8)     NULL,
    [ind_cost_change_item]                       INT             NULL,
    [unit_cost_new]                              NUMERIC (18, 6) NULL,
    [target_margin]                              NUMERIC (18, 4) NULL,
    [price_initial]                              NUMERIC (18, 2) NULL,
    [use_lg_price_points]                        VARCHAR (8)     NULL,
    [digit_adj]                                  NUMERIC (18, 2) NULL,
    [price_rounded]                              NUMERIC (18, 2) NULL,
    [minamt]                                     NUMERIC (18, 2) NULL,
    [lg_activation]                              VARCHAR (8)     NULL,
    [level_gross]                                VARCHAR (8)     NULL,
    [daysout]                                    INT             NULL,
    [create_timestamp]                           DATETIME        NULL
);




GO
CREATE NONCLUSTERED INDEX [idx_01]
    ON [precision_comps].[Level_Gross_Analysis_Weekly_price_Steps]([Level_Gross_Analysis_Weekly_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_02]
    ON [precision_comps].[Level_Gross_Analysis_Weekly_price_Steps]([PRODUCT_STRUCTURE_FK] ASC, [ENTITY_STRUCTURE_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_03]
    ON [precision_comps].[Level_Gross_Analysis_Weekly_price_Steps]([PROD_SEGMENT_FK] ASC, [ENTITY_STRUCTURE_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [prc_fk]
    ON [precision_comps].[LEVEL_GROSS_ANALYSIS_WEEKLY_PRICE_STEPS]([PRICESHEET_RETAIL_CHANGES_FK] ASC)
    INCLUDE([alias_id], [zone_id], [item], [cost_change_reason], [pricing_step], [price], [pricing_logic], [auth_dt], [alias_dt], [Parity_dt], [PRODUCT_STRUCTURE_FK], [pricing_phase], [parity_status]) WITH (FILLFACTOR = 80);

