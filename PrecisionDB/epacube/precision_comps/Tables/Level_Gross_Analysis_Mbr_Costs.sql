﻿CREATE TABLE [precision_comps].[Level_Gross_Analysis_Mbr_Costs] (
    [eff_dt]                      DATE            NULL,
    [cs_cost]                     NUMERIC (18, 6) NULL,
    [unit_cst]                    NUMERIC (18, 6) NULL,
    [chg]                         VARCHAR (8)     NOT NULL,
    [pack]                        INT             NULL,
    [rescind]                     DATE            NULL,
    [created]                     DATE            NULL,
    [Imprt_dt]                    DATE            NULL,
    [update_user]                 VARCHAR (64)    NULL,
    [PRODUCT_STRUCTURE_FK]        BIGINT          NULL,
    [PROD_SEGMENT_FK]             BIGINT          NULL,
    [pricesheet_basis_values_fk]  BIGINT          NOT NULL,
    [price_change_effective_date] DATE            NULL,
    [record_status_cr_fk]         INT             NULL,
    [unique_cost_record_id]       VARCHAR (64)    NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_01]
    ON [precision_comps].[Level_Gross_Analysis_Mbr_Costs]([PRODUCT_STRUCTURE_FK] ASC, [price_change_effective_date] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [idx_02]
    ON [precision_comps].[Level_Gross_Analysis_Mbr_Costs]([PROD_SEGMENT_FK] ASC, [price_change_effective_date] ASC) WITH (FILLFACTOR = 80);

