﻿CREATE TABLE [precision_comps].[Level_Gross_Analysis_Weekly] (
    [Comparison_Class]               VARCHAR (18)    NOT NULL,
    [change_type]                    VARCHAR (8)     NULL,
    [alias_id]                       VARCHAR (16)    NULL,
    [par_als]                        BIGINT          NULL,
    [link]                           VARCHAR (256)   NULL,
    [rims_paritm]                    VARCHAR (128)   NULL,
    [zone_id]                        VARCHAR (128)   NULL,
    [par_pitm]                       VARCHAR (64)    NULL,
    [par_citm]                       VARCHAR (64)    NULL,
    [item]                           VARCHAR (64)    NULL,
    [p_prc_c]                        MONEY           NULL,
    [rims_p_c]                       VARCHAR (256)   NULL,
    [p_prc_init]                     NUMERIC (18, 2) NULL,
    [p_add]                          NUMERIC (18, 2) NULL,
    [p_rnded]                        NUMERIC (18, 2) NULL,
    [p_prc_new]                      NUMERIC (18, 2) NULL,
    [rims_pn]                        VARCHAR (256)   NULL,
    [delta]                          NUMERIC (19, 2) NULL,
    [itm_cst_chg]                    INT             NULL,
    [p_tm]                           NUMERIC (18, 6) NULL,
    [rims_tm]                        NUMERIC (18, 4) NULL,
    [p_cs_cst_C]                     NUMERIC (18, 6) NULL,
    [p_cs_cst_N]                     NUMERIC (18, 6) NULL,
    [rims_cs_cst_C]                  VARCHAR (256)   NULL,
    [rims_cs_cst_N]                  VARCHAR (256)   NULL,
    [p_u_cst_C]                      NUMERIC (18, 6) NULL,
    [p_u_cst_N]                      NUMERIC (18, 6) NULL,
    [rims_u_cst_C]                   NUMERIC (18, 2) NULL,
    [rims_u_cst_N]                   NUMERIC (18, 2) NULL,
    [p_gm_C]                         NUMERIC (18, 4) NULL,
    [p_gm_N]                         NUMERIC (18, 4) NULL,
    [rims_gm_C]                      NUMERIC (18, 4) NULL,
    [rims_gm_N]                      NUMERIC (18, 4) NULL,
    [cost_change_reason]             VARCHAR (72)    NULL,
    [last_in_rims]                   NVARCHAR (MAX)  NULL,
    [p_cst_chg_dt]                   DATE            NULL,
    [rims_cst_chg_dt]                NVARCHAR (MAX)  NULL,
    [related_item_alias_id]          BIGINT          NULL,
    [pricing_steps]                  VARCHAR (MAX)   NULL,
    [pricesheet_retail_changes_id]   BIGINT          NULL,
    [rims_Q]                         VARCHAR (256)   NULL,
    [p_Q]                            VARCHAR (1)     NOT NULL,
    [Qual_r_p]                       VARCHAR (258)   NOT NULL,
    [lg_a_f]                         VARCHAR (17)    NULL,
    [level_gross]                    VARCHAR (8)     NULL,
    [lg_activation]                  VARCHAR (8)     NULL,
    [ind_margin_changes]             INT             NULL,
    [PRODUCT_STRUCTURE_FK]           BIGINT          NULL,
    [PROD_SEGMENT_FK]                BIGINT          NULL,
    [ENTITY_STRUCTURE_FK]            BIGINT          NULL,
    [ENTITY_DATA_NAME_FK]            INT             NOT NULL,
    [DATA_NAME_FK]                   INT             NOT NULL,
    [Auth_Dt]                        DATE            NULL,
    [Alias_Dt]                       DATE            NULL,
    [Parity_Dt]                      DATE            NULL,
    [lr_cs_cst_C]                    VARCHAR (256)   NULL,
    [lr_cs_cst_N]                    VARCHAR (256)   NULL,
    [lr_prc_C]                       VARCHAR (256)   NULL,
    [lr_prc_N]                       VARCHAR (256)   NULL,
    [ind_display_as_reg]             INT             NULL,
    [item_change_type_cr_fk]         INT             NULL,
    [price_change_effective_date]    DATE            NULL,
    [Level_Gross_Analysis_Weekly_ID] BIGINT          IDENTITY (1, 1) NOT NULL,
    [last_in_precision]              DATE            NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_02]
    ON [precision_comps].[Level_Gross_Analysis_Weekly]([PRODUCT_STRUCTURE_FK] ASC, [ENTITY_STRUCTURE_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_03]
    ON [precision_comps].[Level_Gross_Analysis_Weekly]([PROD_SEGMENT_FK] ASC, [ENTITY_STRUCTURE_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

