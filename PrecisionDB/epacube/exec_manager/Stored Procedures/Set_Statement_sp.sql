﻿-- Stored Procedure










-- =============================================
-- Author:		Yuri Nogin
-- Create date: 1/29/2006
-- Description:	Sets the statement to be executed in the exec_manager.StatementQueue table
-- Modified:    7/27/2008 By Yuri Nogin. Added epa_job_id parameter
-- =============================================
CREATE PROCEDURE [exec_manager].[Set_Statement_sp] 
	-- Add the parameters for the stored procedure here
	@exec_id bigint,
    @statement varchar(max),
    @statement_id bigint output,
    @create_user_id varchar(50) = NULL,
    @host_id varchar(50) = NULL,
	@host_name varchar(50) = NULL,
    @statement_type varchar(50) = 'TSQL',
    @allow_commit smallint = 1, -- 1 not in trigger, 0 in trigger
    @separator  varchar(10) = 'go',
    @epa_job_id BIGINT = NULL 
    
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRY
        DECLARE @MyTableVar table( statement_id bigint);
        if @allow_commit > 0 
           Begin
		        BEGIN TRANSACTION 
           End
       	INSERT into  exec_manager.StatementQueue  
                   (exec_id,statement,create_user_id,
                    host_id,host_name,statement_type,separator,epa_job_id)
        OUTPUT INSERTED.statement_id 
        INTO @MyTableVar
		VALUES( @exec_id,@statement,IsNull(@create_user_id,suser_name()),
                IsNull(@host_id,host_id()),IsNull(@host_name,host_name()),
                @statement_type,@separator,@epa_job_id);
        
        
        if @allow_commit > 0 and XACT_STATE() = 1
           Begin
		    	COMMIT TRANSACTION;
           End
		Select top 1 @statement_id = statement_id from @MyTableVar;
    END TRY
    BEGIN CATCH 
        Exec  exec_monitor.Report_Error_sp @exec_id = @exec_id, @epa_job_id = @epa_job_id;
    END CATCH;

END

















