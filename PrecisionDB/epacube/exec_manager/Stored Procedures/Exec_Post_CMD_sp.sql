﻿





-- =============================================
-- Author:		Yuri Nogin
-- Create date: 11/14/2006
-- Description:	Post executes CMD statement
-- Modified:    7/27/2008 By Yuri Nogin. Added epa_job_id parameter
-- =============================================
CREATE PROCEDURE [exec_manager].[Exec_Post_CMD_sp] 
	-- Add the parameters for the stored procedure here
      @exec_id BIGINT
    , @statement VARCHAR(4000)
    , @JobQueue VARCHAR(MAX) OUTPUT
    , @epa_job_id BIGINT = NULL 
AS 
      BEGIN
            DECLARE @statement_id BIGINT ;
            DECLARE @sExec NVARCHAR(MAX) ;
            DECLARE @job_id_out UNIQUEIDENTIFIER ;
    
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
            SET NOCOUNT ON ;

    -- Insert statements for procedure here
            BEGIN TRY
                  EXEC job_manager.Start_CMD_Job_sp @exec_id = @exec_id,
                       @cmd_statement = @statement,
                       @JobQueue_job_id = @job_id_out OUTPUT,
                       @epa_job_id = @epa_job_id ;
                  SET @JobQueue = @JobQueue +
                      CAST(@job_id_out AS VARCHAR(60)) + ';' ;
	
            END TRY
            BEGIN CATCH
                  EXEC exec_monitor.Report_Error_sp @exec_id = @exec_id, @epa_job_id = @epa_job_id
            END CATCH ;

      END





















