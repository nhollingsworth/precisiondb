﻿


-- =============================================
-- Author:		Yuri Nogin
-- Create date: 5/10/2006
-- Description:	Post executes statement
-- Modified:    7/27/2008 By Yuri Nogin. Added epa_job_id parameter
-- =============================================
CREATE PROCEDURE [exec_manager].[Exec_Post_sp] 
	-- Add the parameters for the stored procedure here
      @exec_id BIGINT
    , @statement VARCHAR(MAX)
    , @JobQueue VARCHAR(MAX) OUTPUT
    , @allow_commit SMALLINT = 1
    , -- 1 not in trigger, 0 in trigger
      @separator VARCHAR(10) = 'go'
    , @epa_job_id BIGINT = NULL    
AS 
      BEGIN
    
    
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
            SET NOCOUNT ON ;

    -- Insert statements for procedure here
            BEGIN TRY
                  DECLARE @statement_id BIGINT ;
                  DECLARE @sExec NVARCHAR(MAX) ;
                  DECLARE @job_id_out UNIQUEIDENTIFIER ;
                  EXEC exec_manager.Set_Statement_sp @exec_id = @exec_id,
                       @statement = @statement,
                       @statement_id = @statement_id OUTPUT,
                       @allow_commit = @allow_commit,
                       @separator = @separator,
                       @epa_job_id = @epa_job_id;
                  IF @statement_id > 0 
                     BEGIN 
                           EXEC job_manager.Start_Job_sp @exec_id = @exec_id,
                                @statement_id = @statement_id,
                                @JobQueue_job_id = @job_id_out OUTPUT,@epa_job_id = @epa_job_id ;
                           SET @JobQueue = @JobQueue +
                               CAST(@job_id_out AS VARCHAR(60)) +
                               ';' ;
                     END ; 
	    --Print 'Job Queue: ' + cast(@job_id_out as varchar(60));
            END TRY
            BEGIN CATCH
                  EXEC exec_monitor.Report_Error_sp @exec_id = @exec_id,@epa_job_id = @epa_job_id
            END CATCH ;

      END






















