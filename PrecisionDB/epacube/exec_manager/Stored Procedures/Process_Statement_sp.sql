﻿


-- =============================================
-- Author:		Yuri Nogin
-- Create date: 3/6/2006
-- Description:	Processes the statement from the exec_manager.StatementQueue table
-- Fixed a bug with an open transaction - YN 4/8/2007
-- Modified:    7/27/2008 By Yuri Nogin. Added epa_job_id parameter
-- =============================================
CREATE PROCEDURE [exec_manager].[Process_Statement_sp] 
	-- Add the parameters for the stored procedure here
      @statement_id BIGINT
    , @allow_commit SMALLINT = 1   -- 1 not in trigger, 0 in trigger
AS 
      BEGIN
	
    -- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
            SET NOCOUNT ON ;

    -- Insert statements for procedure here
            BEGIN TRY
                  DECLARE @statement VARCHAR(MAX) ;
                  DECLARE @create_user_id VARCHAR(50) ;
                  DECLARE @host_id VARCHAR(50) ;
                  DECLARE @host_name VARCHAR(50) ;
                  DECLARE @statement_type VARCHAR(50) ;
                  DECLARE @exec_id BIGINT ;
                  DECLARE @separator VARCHAR(10) ; -- 'go'
                  DECLARE @epa_job_id BIGINT ;
                  DECLARE cur_stmt CURSOR
                          FOR SELECT    [create_user_id]
                                      , [host_id]
                                      , [host_name]
                                      , [exec_id]
                                      , [statement_type]
                                      , [statement]
                                      , [separator]
                                      , [epa_job_id]
                              FROM      exec_manager.StatementQueue
                              WHERE     [statement_id] = @statement_id
                          FOR READ ONLY ;

                  OPEN cur_stmt ;
                  FETCH NEXT FROM cur_stmt INTO @create_user_id, @host_id, @host_name, @exec_id, @statement_type, @statement, @separator ;
                  WHILE @@FETCH_STATUS = 0
                        BEGIN
                              IF UPPER(@statement_type) = 'TSQL' 
                                 BEGIN
                                       EXEC exec_manager.Exec_Immediate_sp @exec_id = @exec_id, @statement = @statement, @allow_commit = @allow_commit, @separator = @separator, @epa_job_id = @epa_job_id
                                 END

                              FETCH NEXT FROM cur_stmt INTO @create_user_id, @host_id, @host_name, @exec_id, @statement_type, @statement, @separator ;
                        END ;

                  CLOSE cur_stmt ;
                  DEALLOCATE cur_stmt ;
		
                  DELETE    exec_manager.StatementQueue
                  WHERE     [statement_id] = @statement_id ;
        
            END TRY
            BEGIN CATCH
                  EXEC exec_monitor.Report_Error_sp @exec_id = @exec_id,
                       @epa_job_id = @epa_job_id
            END CATCH ;

      END























