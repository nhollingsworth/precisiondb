﻿






-- =============================================
-- Author:		Yuri Nogin
-- Create date: 5/23/2006
-- Description:	Waits for all procs fired by (exec_post / job_start) to finish
-- =============================================
CREATE PROCEDURE [exec_manager].[Wait_For_All_Procs_To_Finish_sp] 
	-- Add the parameters for the stored procedure here
	@exec_id bigint,
    @JobQueue varchar(max), 
    @separator  varchar(10) = ';'
AS
BEGIN
       
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- wait 3 seconds to let SQL Server to start the jobs
    waitfor delay '00:00:03.050';
    -- Insert statements for procedure here
    BEGIN TRY
		WHILE (select top 1 1 from msdb.dbo.sysjobs msj INNER JOIN 
               utilities.SplitString(@JobQueue, @separator) lj 
               ON (msj.job_id = lj.ListItem)) = 1
               Begin
                    waitfor delay '00:00:00.050';
               End              

    END TRY
    BEGIN CATCH
		 EXEC exec_monitor.Report_Error_sp @exec_id 
    END CATCH;

END






















