﻿-- Stored Procedure


-- =============================================
-- Author:		Yuri Nogin
-- Create date: 5/10/2006
-- Description:	Inserts the statement into the temp table and executes it
-- Modified:    7/27/2008 By Yuri Nogin. Added epa_job_id parameter
-- =============================================
CREATE PROCEDURE [exec_manager].[Exec_Immediate_sp] 
	-- Add the parameters for the stored procedure here
	@exec_id bigint,
    @statement varchar(max),
    @allow_commit smallint = 1, -- 1 not in trigger, 0 in trigger
    @separator  varchar(10) = 'go',
    @epa_job_id BIGINT = NULL 
AS
BEGIN
    
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRY
            DECLARE @statement_id bigint;
            DECLARE @sExec nvarchar(max);
			DECLARE @status_desc varchar(max);
			DECLARE @time_before datetime;
			DECLARE @time_after datetime;
			DECLARE @elapsed_time numeric(10,4)
			DECLARE @rows_affected varchar(max);
			DECLARE @rows_dml bigint;
			DECLARE @ParmDefinition nvarchar(500);
            IF Cursor_Status('local', 'cur_exec') <= 0
			BEGIN
				DECLARE cur_exec cursor local for SELECT * FROM utilities.SplitString (@statement,@separator);
            END;
    	
        	OPEN cur_exec;
            FETCH NEXT FROM cur_exec Into @sExec;
            WHILE @@FETCH_STATUS = 0
                BEGIN
					Begin Try
                        set @time_before = getdate();
                        -- Main Execute here
                        if ((CHARINDEX('INSERT ',Upper(@sExec)) > 0) OR 
                            (CHARINDEX('UPDATE ',Upper(@sExec)) > 0) OR
                            (CHARINDEX('DELETE ',Upper(@sExec)) > 0))
                            Begin
                                SET @ParmDefinition = N'@rows_out bigint OUTPUT';
                                if right(rtrim(@sExec),1) <> ';'
                                   SET @sExec = @sExec + ';';
                                SET @sExec = @sExec + ' set @rows_out = @@rowcount;'
                                Exec sp_executesql 	@sExec,@ParmDefinition, @rows_out = @rows_dml OUTPUT;
                                SET @rows_affected = ' Rows affected by this DML: ' + cast(ISNULL(@rows_dml,0) as varchar(60));
                            End
                        else
                            Begin
								Exec sp_executesql 	@sExec;
                                SET @rows_affected = '';
                            End
						--		
                        -- check for affected row count if DML
   
						set @time_after = getdate();
						set @elapsed_time = datediff(ms,@time_before,@time_after) / 1000.00;

                        -- report the status of the executed statement
                        Set @status_desc = 'The statement: ' + @sExec + ' - Executed succesfully.' + @rows_affected +
                                           ' Elapsed Time for execution in sec. was: ' + cast(@elapsed_time as varchar(20));
						Exec exec_monitor.Report_Status_sp @exec_id = @exec_id, @status_desc = @status_desc, @epa_job_id = @epa_job_id
					End Try
					Begin Catch
				       	   Set @status_desc = 'The statement: ' + @sExec + ' executed with errors.'
						   Exec exec_monitor.Report_Error_sp @exec_id = @exec_id, @epa_job_id = @epa_job_id;
					End Catch
				    FETCH NEXT FROM cur_exec Into @sExec;
                END;  
                CLOSE cur_exec;
				DEALLOCATE cur_exec;  

    END TRY
    BEGIN CATCH
		EXEC exec_monitor.Report_Error_sp @exec_id = @exec_id, @epa_job_id = @epa_job_id
    END CATCH;

END































