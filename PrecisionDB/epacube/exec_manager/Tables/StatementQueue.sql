﻿CREATE TABLE [exec_manager].[StatementQueue] (
    [statement_id]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [create_dt]      DATETIME      CONSTRAINT [DF_StatementQueue_create_dt] DEFAULT (getdate()) NOT NULL,
    [create_user_id] VARCHAR (50)  CONSTRAINT [DF_StatementQueue_create_user_id] DEFAULT (suser_name()) NOT NULL,
    [host_id]        VARCHAR (50)  CONSTRAINT [DF_StatementQueue_host_id] DEFAULT (host_id()) NOT NULL,
    [host_name]      VARCHAR (50)  CONSTRAINT [DF_StatementQueue_host_name] DEFAULT (host_name()) NOT NULL,
    [exec_id]        BIGINT        NOT NULL,
    [statement_type] VARCHAR (50)  CONSTRAINT [DF_StatementQueue_statement_type] DEFAULT ('TSQL') NOT NULL,
    [statement]      VARCHAR (MAX) NOT NULL,
    [separator]      VARCHAR (10)  CONSTRAINT [DF_StatementQueue_separator] DEFAULT ('go') NULL,
    [epa_job_id]     BIGINT        NULL,
    CONSTRAINT [PK_StatementQueue] PRIMARY KEY CLUSTERED ([statement_id] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_StatementQueue_StatementQueue] FOREIGN KEY ([exec_id]) REFERENCES [exec_monitor].[ExecLog] ([exec_id])
);



