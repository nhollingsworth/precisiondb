﻿
CREATE PROCEDURE [stage].[import_taxonomy_tree]
      (
        @IN_PACKAGE_FK INT
      , @IN_JOB_FK BIGINT
       
      )
AS -- Copyright 2008 epaCUBE, Inc.
  --
  -- Purpose: Import Taxonomy Tree Data.
  --
  -- MODIFICATION HISTORY
  -- Person     Date        Comments
  -- --------- ----------  -------------------------------------------------
  --           10/06/2008  Initial version
  -- MS		   05/13/2014  Added the node display sequence number to the process
  
      DECLARE @l_sysdate DATETIME
            , @ls_stmt VARCHAR(1000)
            , @l_exec_no FLOAT(53)
            , @v_count BIGINT
            , @v_batch_no BIGINT
            , @v_job_class_fk INT
            , @v_job_name VARCHAR(64)
            , @v_data1 VARCHAR(128)
            , @v_data2 VARCHAR(128)
            , @v_data3 VARCHAR(128)
            , @v_job_user VARCHAR(64)
            , @v_out_job_fk BIGINT
            , @v_input_rows BIGINT
            , @v_output_rows BIGINT
            , @v_output_total BIGINT
            , @v_exception_rows BIGINT
            , @v_job_date DATETIME
            , @v_sysdate DATETIME
            , @status_desc VARCHAR(MAX)
            , @taxonomy_tree_id INT 
      SET @l_sysdate = GETDATE()
      SET @v_job_class_fk = 270
      SET @v_job_user = 'IMPORT'
      SET @v_job_date = GETDATE() ;
      SET @v_sysdate = GETDATE()
      BEGIN
            SET @l_exec_no = 0 ;
            SET @ls_stmt = 'started execution of STAGE.IMPORT_TAXONOMY_TREE. ' +
                ' Job_FK: ' +
                CAST(ISNULL(@IN_JOB_FK, 0) AS VARCHAR(16)) +
                ', Package_FK: ' +
                CAST(ISNULL(@IN_PACKAGE_FK, 0) AS VARCHAR(16))
            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                 @exec_id = @l_exec_no OUTPUT ;
            BEGIN TRY
                  SELECT TOP 1
                            @v_job_name = ip.name
                          , @v_data1 = iu.import_filename
                  FROM      import.IMPORT_TAXONOMY_TREE iu 
                  INNER JOIN import.import_package ip
                            ON ( ip.import_package_id = iu.import_package_fk )
                  WHERE     iu.job_fk = @IN_JOB_FK ;
                  
                  EXEC common.job_create @IN_JOB_FK,
                       @V_job_class_fk,
                       @V_JOB_NAME,
                       @V_DATA1,
                       @V_DATA2,
                       @V_DATA3,
                       @V_JOB_USER,
                       @V_OUT_JOB_FK OUTPUT ;
                  IF ( @in_package_fk IS NOT NULL AND
                       @in_job_fk IS NOT NULL
                     ) 
                     BEGIN
        -------------------------------------------------------------------------------------
        --   put comment here
        -------------------------------------------------------------------------------------
                           SELECT   @v_input_rows = COUNT(1)
                           FROM     import.IMPORT_TAXONOMY_TREE
                           WHERE    job_fk = @in_job_fk ;
                           SET @v_output_rows = 0 -- REMOVE THIS LINE AFTER YOU RESOLVE NEXT COMMENTED BLOCK
                           SET @v_exception_rows = @v_input_rows - @v_output_rows ;
                           SET @l_sysdate = GETDATE()
                           EXEC common.job_execution_create @in_job_fk, 'IMPORT Taxonomy Tree  Nodes TO EPACUBE Taxonomy_Node table', @v_input_rows, @v_output_rows, @v_exception_rows, 201, @v_sysdate, @l_sysdate ;
        
        -- Per Kathi the intial row must exist in the Taxonomy_Tree table before this procedure is executed
        -- So all the changes have to be applied to Taxonomy_Node table
               
                           SELECT   @taxonomy_tree_id = tt.taxonomy_tree_id
                           FROM     epacube.TAXONOMY_TREE tt
                           WHERE    tt.NAME LIKE ( SELECT TOP 1
                                                            tree_name
                                                   FROM     import.IMPORT_TAXONOMY_TREE
                                                 )
           
                           IF @@ROWCOUNT = 0 OR
                              @taxonomy_tree_id IS NULL 
                              RAISERROR ( 'Taxonomy Tree Must EXISTS IN the epacube.TAXONOMY_TREE TABLE before the import', -- Message text.
                                        16, -- Severity.
                                        0 -- State.      
                                         ) ;
                                         
                           
                           DECLARE @vm$Delete_Flag CHAR(1)
                                 , @vm$Tree_Name varchar(64)
                                 , @vm$Node_Code varchar(64)
                                 , @vm$Description varchar(255)
                                 , @vm$Level int
                                 , @vm$Parent_Node varchar(128)
                                 , @vm$ActivityCode INT
                                 , @vm$Image varchar(255)
                                 , @vm$import_filename VARCHAR(128)
                                 , @vm$record_no INT
								 , @vm$Display_Seq INT
                            
        
                           DECLARE tax_tree_node cursor local
                                   FOR SELECT   import_filename
                                              , record_no
                                              , Delete_Flag
                                              , Tree_Name
                                              , Node_Code
                                              , Description
                                              , [Level]
                                              , Parent_Node
                                              , [IMAGE]
											  , DISPLAY_SEQ
                                       FROM     import.IMPORT_TAXONOMY_TREE
                                       WHERE    JOB_FK = @in_job_fk
                                       ORDER BY record_no ASC ;
       
                           OPEN tax_tree_node ;
       
        
                           FETCH NEXT FROM tax_tree_node INTO @vm$import_filename, @vm$record_no, @vm$Delete_Flag, @vm$Tree_Name, @vm$Node_Code, @vm$Description, @vm$Level, @vm$Parent_Node, @vm$Image, @vm$Display_Seq ;

                           WHILE @@FETCH_STATUS = 0 
                                 BEGIN
                                       IF @vm$Delete_Flag = 'N' 
                                          SET @vm$ActivityCode = 8003 ;
                                       ELSE 
                                          SET @vm$ActivityCode = 8004 ;
                                       BEGIN TRY
                                             EXEC [synchronizer].[TAXONOMY_NODE_EDITOR] @in_taxonomy_node_id = NULL, @in_taxonomy_node_name = @vm$Node_Code, @in_taxonomy_code = @vm$Node_Code, @in_parent_taxonomy_node_fk = NULL, @in_taxonomy_tree_fk = @taxonomy_tree_id, @in_update_user = 'IMPORT', @in_ui_action_cr_fk = @vm$ActivityCode, @in_image = @vm$Image, @in_level_seq = @vm$Level, @in_parent_taxonomy_node_code = @vm$Parent_Node, @in_description = @vm$Description, @in_display_seq = @vm$Display_Seq
                                       END TRY 
                                       BEGIN CATCH
                                              -- insert data excecptions here
                                             SELECT @status_desc = ERROR_MESSAGE() 
                                             INSERT INTO stage.import_data_exceptions
                                                    (
                                                      package_fk
                                                    , job_fk
                                                    , target_table
                                                    , action_failed
                                                    , error_desc
                                                    )
                                                    SELECT  @IN_PACKAGE_FK
                                                          , @IN_JOB_FK
                                                          , 'EPACUBE.TAXONOMY_NODE'
                                                          , 'INSERT / UPDATE'
                                                          , 'Record No: ' + CAST(@vm$record_no AS VARCHAR(20)) + ' failed in import file: ' + @vm$import_filename + ' with error: ' + @status_desc ;
                                       
                                         
                                       
                                       END CATCH ; 
    
                                       FETCH NEXT FROM tax_tree_node INTO @vm$import_filename, @vm$record_no, @vm$Delete_Flag, @vm$Tree_Name, @vm$Node_Code, @vm$Description, @vm$Level, @vm$Parent_Node, @vm$Image, @vm$Display_Seq ;
                                 END ;
                           CLOSE tax_tree_node ;
                           DEALLOCATE tax_tree_node ;
                           
                           EXEC common.job_execution_create @in_job_fk, 'IMPORT COMPLETE', @v_input_rows, @v_output_total, @v_exception_rows, 201, @v_job_date, @l_sysdate ;
                     END ; -- IF @in_package_fk IS NOT NULL AND @in_job_fk IS NOT NULL
                  SET @status_desc = 'finished execution of STAGE.IMPORT_TAXONOMY_TREE' ;
                  EXEC exec_monitor.report_status_sp @l_exec_no,
                       @status_desc ;
            END TRY
            BEGIN CATCH
                  DECLARE @ErrorMessage NVARCHAR(4000) ;
                  DECLARE @ErrorSeverity INT ;
                  DECLARE @ErrorState INT ;
                  SELECT    @ErrorMessage = 'Execution of STAGE.IMPORT_TAXONOMY_TREE has failed ' + Error_message()
                          , @ErrorSeverity = Error_severity()
                          , @ErrorState = Error_state() ;
                  EXEC exec_monitor.report_error_sp @l_exec_no ;
                  declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
                  RAISERROR ( @ErrorMessage, -- Message text.
                            @ErrorSeverity, -- Severity.
                            @ErrorState -- State.
    ) ;
            END CATCH ;
      END


