﻿





-- Copyright 2008
--
-- Procedure Created by Cindy Vortour
--
--
-- Purpose: To load the entity data into the identification and structure tables 
--           where insert missing ind is turned on.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        02/20/2010   Initial version
-- KS        02/28/2010   Added epacube_search entry
-- CV        05/03/2010   Commented Epacube search
-- CV        10/07/2013     Need to look at level 2 entities
--  
CREATE PROCEDURE [stage].[INSERT_MISSING_ENTITY] (
      @in_job_fk       bigint
   )
AS
BEGIN


/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to load Entities into the STAGE.STG_ENTITY_IDENTIFICATION
--	 table.  
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure STAGE.STG_LOAD_ENTITIES
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--   Uses the import.V_import View looking for those data elements that have table name of entity
--   structures.  
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 datetime;
     DECLARE @epa_job_id		 Int;

     DECLARE @status_desc          varchar(max);
     DECLARE @v_import_package_fk  int
     DECLARE @v_entity_class_cr_fk int

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.INSERT_MISSING_ENTITY', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE()



------
-------------------------------------------------------------------------------------------------
--------   CREATE ANY MISSING ENTITIES THAT ARE FLAGGED IF INSERT_MISSING_IND = 1 
-------------------------------------------------------------------------------------------


	INSERT INTO epacube.ENTITY_STRUCTURE
           (ENTITY_STATUS_CR_FK
           ,DATA_NAME_FK
           ,LEVEL_SEQ
           ,PARENT_ENTITY_STRUCTURE_FK
           ,ENTITY_CLASS_CR_FK
           ,IMPORT_FILENAME
           ,RECORD_NO
           ,JOB_FK
           ,RECORD_STATUS_CR_FK
--           ,CREATE_TIMESTAMP
--           ,UPDATE_TIMESTAMP
           ,UPDATE_LOG_FK
           )
	SELECT 
           ( SELECT CODE_REF_ID
             FROM EPACUBE.CODE_REF  WITH (NOLOCK)
             WHERE CODE_TYPE = 'ENTITY_STATUS'
             AND   CODE = 'ACTIVE' )
		  ,TSEI.ENTITY_DATA_NAME_FK
		  ,isNULL (srec.LEVEL_SEQ, 1)  --[LEVEL_SEQ]
          ,isNULL(srec.PARENT_STRUCTURE_FK, NULL)   --- [PARENT_ENTITY_STRUCTURE_FK]
		  ,TSEI.ENTITY_CLASS_CR_FK
		  ,TSEI.IMPORT_FILENAME
		  ,TSEI.RECORD_NO
		  ,TSEI.JOB_FK
          ,1  -- RECORD STATUS
--          ,@L_SYSDATE
--          ,@L_SYSDATE
          ,TSEI.STG_RECORD_ENTITY_FK
	  FROM #TS_ENTITY_IDENTS TSEI
	  INNER JOIN stage.stg_Record srec
	  on (srec.RECORD_NO = TSEI.RECORD_NO
	  AND srec.JOB_FK = TSEI.Job_fk)
	  inner join epacube.ENTITY_CLASS_TREE ect
	  on (ect.DATA_NAME_FK = tsei.entity_data_name_fk
	  and ect.LEVEL_SEQ = 1
	  and ect.RECORD_STATUS_CR_FK = 1)
	  
-----------------------------------------------------------------------------------------
INSERT INTO epacube.ENTITY_STRUCTURE
           (ENTITY_STATUS_CR_FK
           ,DATA_NAME_FK
           ,LEVEL_SEQ
           ,PARENT_ENTITY_STRUCTURE_FK
           ,ENTITY_CLASS_CR_FK
           ,IMPORT_FILENAME
           ,RECORD_NO
           ,JOB_FK
           ,RECORD_STATUS_CR_FK
           ,CREATE_TIMESTAMP
           ,UPDATE_TIMESTAMP
           ,UPDATE_LOG_FK
           )
	SELECT distinct 
           ( SELECT CODE_REF_ID
             FROM EPACUBE.CODE_REF  WITH (NOLOCK)
             WHERE CODE_TYPE = 'ENTITY_STATUS'
             AND   CODE = 'ACTIVE' )
		  ,TSEI.ENTITY_DATA_NAME_FK
		  ,2  --[LEVEL_SEQ]
          ,(select entity_structure_fk from epacube.entity_identification
          where VALUE = (select value from epacube.epacube_params where NAME = 'DEFAULT REGION'
           and APPLICATION_SCOPE_FK = 10101) ) --- [PARENT_ENTITY_STRUCTURE_FK] 'need region'
		  ,TSEI.ENTITY_CLASS_CR_FK
		  ,TSEI.IMPORT_FILENAME
		  ,TSEI.RECORD_NO
		  ,TSEI.JOB_FK
          ,1  -- RECORD STATUS
          ,@L_SYSDATE
          ,@L_SYSDATE
          ,TSEI.STG_RECORD_ENTITY_FK
	  FROM #TS_ENTITY_IDENTS TSEI
	  INNER JOIN stage.stg_Record srec
	  on (srec.JOB_FK = TSEI.Job_fk)	
 inner join epacube.ENTITY_CLASS_TREE ect
	  on (ect.DATA_NAME_FK = tsei.entity_data_name_fk
	  and ect.LEVEL_SEQ = 2
	  and ect.RECORD_STATUS_CR_FK = 1)  

------------------------------------------------------------------------------------

UPDATE stage.STG_RECORD_ENTITY
SET ENTITY_STRUCTURE_FK = ES.ENTITY_STRUCTURE_ID
   ,ACTIVITY_ACTION_CR_FK   = 144
   ,EVENT_CONDITION_CR_FK = 98   -- YELLOW FOR AUTO INSERT
FROM #TS_ENTITY_IDENTS TSEI
INNER JOIN EPACUBE.ENTITY_STRUCTURE ES
 ON ( ES.UPDATE_LOG_FK = TSEI.STG_RECORD_ENTITY_FK 
 AND  ES.DATA_NAME_FK = TSEI.ENTITY_DATA_NAME_FK
 AND  ES.IMPORT_FILENAME = TSEI.IMPORT_FILENAME 
 AND  ES.JOB_FK = TSEI.JOB_FK 
 AND  ES.RECORD_NO = TSEI.RECORD_NO )
WHERE TSEI.STG_RECORD_ENTITY_FK = stage.STG_RECORD_ENTITY.STG_RECORD_ENTITY_ID



INSERT INTO epacube.ENTITY_IDENTIFICATION
           ([ENTITY_STRUCTURE_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[DATA_NAME_FK]
           ,[VALUE]
           )
SELECT  
            SRECE.ENTITY_STRUCTURE_FK
           ,TSEI.ENTITY_DATA_NAME_FK
           ,TSEI.ENTITY_ID_DATA_NAME_FK
           ,TSEI.ENTITY_ID_VALUE
FROM #TS_ENTITY_IDENTS TSEI
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.DATA_NAME_ID = TSEI.ENTITY_ID_DATA_NAME_FK )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )  
INNER JOIN  stage.STG_RECORD_ENTITY SRECE
  ON ( SRECE.STG_RECORD_ENTITY_ID = TSEI.STG_RECORD_ENTITY_FK )
WHERE DS.TABLE_NAME = 'ENTITY_IDENTIFICATION'



INSERT INTO epacube.ENTITY_IDENT_MULT
           ([ENTITY_STRUCTURE_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[DATA_NAME_FK]
           ,[VALUE]
           )
SELECT 
            SRECE.ENTITY_STRUCTURE_FK
           ,TSEI.ENTITY_DATA_NAME_FK
           ,TSEI.ENTITY_ID_DATA_NAME_FK
           ,TSEI.ENTITY_ID_VALUE
FROM #TS_ENTITY_IDENTS TSEI
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.DATA_NAME_ID = TSEI.ENTITY_ID_DATA_NAME_FK )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )  
INNER JOIN  stage.STG_RECORD_ENTITY SRECE
  ON ( SRECE.STG_RECORD_ENTITY_ID = TSEI.STG_RECORD_ENTITY_FK )
WHERE DS.TABLE_NAME = 'ENTITY_IDENT_MULT'


-------------------------------------------------------------------------------------------
--   MATCH STG_RECORD_ENTITY_IDENT TABLE
-------------------------------------------------------------------------------------

	  EXEC stage.LOAD_STG_RECORD_ENTITY_IDENT_MATCH  @IN_JOB_FK

----      SELECT @v_output_rows = COUNT(1)
----        FROM stage.STG_ENTITY_IDENTIFICATION WITH (NOLOCK)
----       WHERE import_job_fk = @in_job_fk
----
----	  set @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @l_sysdate = getdate()
----      exec common.job_execution_create  @in_job_fk, 'ENTITY IDENTIFICATION',
----                                @v_input_rows, @v_output_rows, @v_exception_rows,
----							 201, @v_sysdate, @l_sysdate  ;
----	  set @v_sysdate = getdate();


---insert into epacube.epacube_search
----
----INSERT INTO [epacube].[EPACUBE_SEARCH]
----			 (ENTITY_CLASS_CR_FK
----			,[EPACUBE_ID]
----			,[SEARCH1]
----           ,[SEARCH2]
----           ,[SEARCH3]
----           ,[SEARCH4]
----           ,[SEARCH5]
----           ,[SEARCH6]) 
----			   
----	SELECT
----			ES.ENTITY_CLASS_CR_FK
----			,ES.ENTITY_STRUCTURE_ID
----			 ,( SELECT CURRENT_DATA 
----              FROM epacube.V_CURRENT_ENTITY_DATA 
----              WHERE DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT ) FROM EPACUBE.EPACUBE_PARAMS EEP
----									 WHERE APPLICATION_SCOPE_FK = ES.ENTITY_CLASS_CR_FK AND NAME = 'SEARCH1' )
----              AND   ENTITY_STRUCTURE_FK = ES.ENTITY_STRUCTURE_ID )              
----           ,( SELECT CURRENT_DATA 
----              FROM epacube.V_CURRENT_ENTITY_DATA 
----              WHERE DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT ) FROM EPACUBE.EPACUBE_PARAMS EEP
----									 WHERE APPLICATION_SCOPE_FK = ES.ENTITY_CLASS_CR_FK AND NAME = 'SEARCH2' )
----              AND   ENTITY_STRUCTURE_FK = ES.ENTITY_STRUCTURE_ID )
----           ,( SELECT CURRENT_DATA 
----              FROM epacube.V_CURRENT_ENTITY_DATA 
----              WHERE DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT ) FROM EPACUBE.EPACUBE_PARAMS EEP
----									 WHERE APPLICATION_SCOPE_FK = ES.ENTITY_CLASS_CR_FK AND NAME = 'SEARCH3' )
----              AND   ENTITY_STRUCTURE_FK = ES.ENTITY_STRUCTURE_ID )
----           ,( SELECT CURRENT_DATA 
----              FROM epacube.V_CURRENT_ENTITY_DATA 
----              WHERE DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT ) FROM EPACUBE.EPACUBE_PARAMS EEP
----									 WHERE APPLICATION_SCOPE_FK = ES.ENTITY_CLASS_CR_FK AND NAME = 'SEARCH4' )
----              AND   ENTITY_STRUCTURE_FK = ES.ENTITY_STRUCTURE_ID )
----           ,( SELECT CURRENT_DATA 
----              FROM epacube.V_CURRENT_ENTITY_DATA 
----              WHERE DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT ) FROM EPACUBE.EPACUBE_PARAMS EEP
----									 WHERE APPLICATION_SCOPE_FK = ES.ENTITY_CLASS_CR_FK AND NAME = 'SEARCH5' )
----              AND   ENTITY_STRUCTURE_FK = ES.ENTITY_STRUCTURE_ID )
----           ,( SELECT CURRENT_DATA 
----              FROM epacube.V_CURRENT_ENTITY_DATA 
----              WHERE DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT ) FROM EPACUBE.EPACUBE_PARAMS EEP
----									 WHERE APPLICATION_SCOPE_FK = ES.ENTITY_CLASS_CR_FK AND NAME = 'SEARCH6' )
----              AND   ENTITY_STRUCTURE_FK = ES.ENTITY_STRUCTURE_ID)
----------			   
----		  FROM epacube.entity_structure es with (NOLOCK)
----			WHERE es.job_fk = @IN_JOB_FK 
----			AND   NOT EXISTS
----	            ( SELECT 1 
----	              FROM EPACUBE.EPACUBE_SEARCH EES WITH (NOLOCK)
----	              WHERE ES.ENTITY_CLASS_CR_FK = EES.ENTITY_CLASS_CR_FK
----	              AND   ES.ENTITY_STRUCTURE_ID = EES.EPACUBE_ID  )
----



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of stage.INSERT_MISSING_ENTITY';
     EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk ;
     
      
      EXEC exec_monitor.print_status_sp @exec_id = @l_exec_no;
   END TRY
   BEGIN CATCH  
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
END


































































