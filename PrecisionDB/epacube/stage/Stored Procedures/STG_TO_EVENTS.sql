﻿









-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- KS        09/14/2008   Initial Version 
-- CV		 06/04/2009   Added update complete timestamp when complete	
-- CV        08/31/2009   Enhancing logging start and end dates


CREATE PROCEDURE [stage].[STG_TO_EVENTS] ( 
        @IN_JOB_FK bigint 
       ,@IN_EXEC_DELETE_JOB_IND SMALLINT = 0     
       ,@IN_DELETE_JOB_IND SMALLINT = 0          
        ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to load import rows into the STAGE.STG_RECORD tables(s)
--	 table.  
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure STAGE.STG_LOAD_ENTITIES
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--   Uses the import.V_import View looking for those data elements that have table name of entity
--   structures.  
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
--     DECLARE @l_sysdate			 DATETIME
--     DECLARE @v_sysdate			 DATETIME
	 DECLARE @V_START_TIMESTAMP  DATETIME
     DECLARE @V_END_TIMESTAMP    DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int     
     	
          

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.STG_TO_EVENTS', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

--     SET @l_sysdate = GETDATE()
SET @V_START_TIMESTAMP = GETDATE()

---Update job table with file name from imported data---------
	update common.job 
	set Data1 = (select top 1 import_filename 
				from import.import_record 
				where job_fk = @in_job_fk)
	where job_id = @in_job_fk

-------------------------------------------------------------------------------------
--   IF JOB_FK Exists then call UNDO
--		BUT pass 1 in second param to SAVE the Import and Job data
-------------------------------------------------------------------------------------

IF @IN_EXEC_DELETE_JOB_IND = 1
        EXEC epacube.UTIL_IMPORT_JOB_DELETE @in_job_fk, @IN_DELETE_JOB_IND



/* ----------------------------------------------------------------------------------- */
--    Assign Next Batch_no to all stg_product_structures within Import Job 
/* ----------------------------------------------------------------------------------- */


	EXEC seq_manager.db_get_next_sequence_value_impl 'common','batch_seq', @v_batch_no output

-------------------------------------------------------------------------------------
--   LOAD STG_RECORD TABLE(S) and Perform Matching
--		loads all stg tables and performs data cleansing matching
-------------------------------------------------------------------------------------

	 EXEC stage.LOAD_STG_RECORD @in_job_fk, @v_batch_no

-- CV-HERE   JOB EXECUTION

      SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      --SET @l_sysdate = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'IMPORT PRODUCTS TO STAGE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
	



-------------------------------------------------------------------------------------
--   Submit Rows are Ready to be Synchronized
--
--   STG_RECORD(s) = BATCH_NO AND <> 99 AND EVENT_ACTION_CR_FK IS NOT NULL
--
--		Create Events and Derived Events
--		Factory(s)  Product, Values, Results and Data Assoc and Entity
--		Perform Event Rules, Validations, and Posting
--
-------------------------------------------------------------------------------------

     SET @V_START_TIMESTAMP = getdate()

	EXEC cleanser.SUBMIT_TO_SYNCHRONIZER @in_job_fk, @v_batch_no

      SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'END SUBMIT TO SYNCHRONIZER',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @v_START_TIMESTAMP, @V_END_TIMESTAMP;



	update common.job
		set job_complete_timestamp = @V_END_TIMESTAMP
		where job_id = @in_job_fk

-------------------------------------------------------------------------------------
-- LAST Step is to Set Job Execution to Complete
-------------------------------------------------------------------------------------
----
----
      SET @V_END_TIMESTAMP = getdate()

      SET @v_input_rows =		( select top 1 input_rows from common.job_execution 
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT PRODUCTS TO STAGE' )

      SET @v_exception_rows =	( select sum(exception_rows)from common.job_execution 
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT PRODUCTS RESOLVED' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE-STAGE TO EVENTS',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @V_START_TIMESTAMP, @v_END_TIMESTAMP;

--------------------------------------------------------------------------------------
	



 SET @status_desc =  'finished execution of stage.STG_TO_EVENTS'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of STAGE.STG_TO_EVENTS has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END







































































































