﻿








-- Copyright 2008
--
-- Procedure Created by Kathi Scott
--
--
-- Purpose: Load ONLY the Unique and Mult Identifiers that belong
--			to the Entity Structures in the STG_RECORD_ENTITY Table
--			( NOTE:  Row Entity will have its idents loaded in Cleansing Tables )
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/05/2007   Initial version
-- CV        06/11/2009   made allowance for All Orgs						  
-- CV		 02/17/2010   added do not import ind logic
-- CV        08/23/2010   commented out old system id code
-- MN		 02/17/2014	  Stripped down all cursors code

CREATE PROCEDURE [stage].[LOAD_STG_RECORD_ENTITY_IDENT] (
      @in_job_fk       bigint
   )
AS
BEGIN


/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to load Entities into the STAGE.STG_RECORD_ENTITY_IDENT
--	 table.  
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure STAGE.STG_LOAD_ENTITIES
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--   Uses the import.V_import View looking for those data elements that have table name of entity
--   structures.  
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @v_import_package_fk   int;

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);
     DECLARE @ls_entity_structure_fk_sql   VARCHAR (1000);
    DECLARE @ls_entity_Value   VARCHAR (256)

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.LOAD_STG_RECORD_ENTITY_IDENT', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @ls_table_name = 'ENTITY_STRUCTURE'

     SET @v_import_package_fk   = ( select top 1 import_package_fk from import.import_record WITH (NOLOCK) 
                                    where job_fk = @in_job_fk )


------------------------------------------------------------------------------------------
--   Capture the Entity Structures and their associated Entity Identifications 
------------------------------------------------------------------------------------------
	SET @status_desc =  'Inserting to stage.LOAD_STG_RECORD_ENTITY_IDENT'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;

      ;WITH IdentityDataStruc as (
 SELECT 
					SRECE.STG_RECORD_ENTITY_ID 
					,SRECE.STG_RECORD_FK 
					,vm.entity_class_cr_fk
					,vm.data_name_fk ENTITY_DATA_NAME_FK
					,vm.ident_data_name_fk ENTITY_ID_DATA_NAME_FK
					,rtrim ( ltrim ([epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name))) ENTITY_ID_VALUE
					,vm.import_column_name COLUMN_NAME
					,( SELECT EI.ENTITY_STRUCTURE_FK FROM EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK) 
                           WHERE EI.ENTITY_DATA_NAME_FK =  vm.data_name_fk
                           AND EI.DATA_NAME_FK = vm.ident_data_name_fk
                           AND EI.VALUE = [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name)
                          ) ENTITY_STRUCTURE_FK
					   , SRECE.JOB_FK
			  FROM import.v_import vm
			  INNER JOIN epacube.data_name dn WITH (NOLOCK)
			    ON ( dn.data_name_id = vm.ident_data_name_fk )
			  INNER JOIN epacube.data_set ds WITH (NOLOCK)
			    ON ( ds.data_set_id = dn.data_set_fk )
			  INNER JOIN  stage.STG_RECORD_ENTITY SRECE  WITH (NOLOCK) 
				ON	 SRECE.ENTITY_DATA_NAME_FK = vm.data_name_fk
					INNER JOIN stage.STG_RECORD SREC  WITH (NOLOCK) 
					 ON ( SREC.STG_RECORD_ID = SRECE.STG_RECORD_FK )
					INNER JOIN IMPORT.IMPORT_RECORD_DATA ID WITH (NOLOCK) 
					 ON ( ID.JOB_FK = SREC.JOB_FK
					 AND  ID.RECORD_NO = SREC.RECORD_NO 
					 AND  ID.SEQ_ID = SRECE.SEQ_ID 
					 AND ISNULL ( ID.DO_NOT_IMPORT_IND, 0 ) <> 1) 
			  WHERE vm.import_package_fk = @v_import_package_fk
			  AND   vm.table_name = 'ENTITY_STRUCTURE'
			  AND   vm.data_name_fk in (
			               select distinct SRECE.entity_data_name_fk
			               from stage.STG_RECORD srec with (nolock)
			               inner join stage.STG_RECORD_ENTITY SRECE with (nolock)
			                  on ( srec.STG_RECORD_id = SRECE.STG_RECORD_fk )
			               where srec.job_fk = @in_job_fk )
              AND   ds.table_name in ( 'ENTITY_STRUCTURE','ENTITY_IDENTIFICATION', 'ENTITY_IDENT_MULT','ENTITY_IDENT_NONUNIQUE' ) --cV added no unique
			  AND [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name) IS NOT NULL
                AND [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name) <> ' '
			   AND ID.JOB_FK = @in_job_fk
)
INSERT INTO stage.STG_RECORD_ENTITY_IDENT
							( STG_RECORD_ENTITY_FK,
							  STG_RECORD_FK,
							  ENTITY_CLASS_CR_FK,							  
							  ENTITY_DATA_NAME_FK,
							  ENTITY_ID_DATA_NAME_FK,
							  ENTITY_ID_VALUE,
							  COLUMN_NAME,
							  ENTITY_STRUCTURE_FK,
							  JOB_FK							  							  
							)
select
					 STG_RECORD_ENTITY_ID,
							  STG_RECORD_FK,
							  ENTITY_CLASS_CR_FK,							  
							  ENTITY_DATA_NAME_FK,
							  ENTITY_ID_DATA_NAME_FK,
							  ENTITY_ID_VALUE,
							  COLUMN_NAME,
							  ENTITY_STRUCTURE_FK,
							  JOB_FK
FROM IdentityDataStruc

--------------------------------------------------------------------
--CREATE THE ENTITY FOR SYSTEM ASSOCIATION
--------------------------------------------------------------------

					----INSERT INTO 
					----[stage].[STG_RECORD_ENTITY_IDENT]
					----		   ([STG_RECORD_ENTITY_FK]
					----		   ,[STG_RECORD_FK]
					----		   ,[ENTITY_CLASS_CR_FK]
					----		   ,[ENTITY_DATA_NAME_FK]
					----		   ,[ENTITY_ID_DATA_NAME_FK]
					----		   ,[ENTITY_ID_VALUE]
					----		   ,[COLUMN_NAME]
					----		   ,[ENTITY_STRUCTURE_FK]
					----		   ,[JOB_FK]
					----		  )
					----(SELECT
					----SRECE.STG_RECORD_ENTITY_ID 
					----,SRECE.STG_RECORD_FK 
					----,(select code_ref_id from epacube.code_ref 
					----where code_type = 'ENTITY_CLASS'
					----and Code = 'SYSTEM')
					----,SRECE.ENTITY_DATA_NAME_FK
					----,(select data_name_id from epacube.data_name
					---- where name = 'SYSTEM CODE')  -- entity id value
					----,IRD.SYSTEM_ID_NAME
					----,'SYSTEM_ID_NAME'
					----,NULL
					----,SRECE.JOB_FK
					----FROM stage.STG_RECORD_ENTITY SRECE  WITH (NOLOCK) 
					----INNER JOIN stage.STG_RECORD SREC  WITH (NOLOCK) 
					---- ON ( SREC.STG_RECORD_ID = SRECE.STG_RECORD_FK )
					----INNER JOIN IMPORT.IMPORT_RECORD_DATA IRD WITH (NOLOCK) 
					---- ON ( IRD.JOB_FK = SREC.JOB_FK
					---- AND  IRD.RECORD_NO = SREC.RECORD_NO 
					---- AND  IRD.SEQ_ID = SRECE.SEQ_ID 
					---- AND ISNULL(IRD.DO_NOT_IMPORT_IND, 0) <> 1) 
					---- WHERE SRECE.COLUMN_NAME = 'SYSTEM_ID_NAME'
					---- AND SREC.JOB_FK = @in_job_fk)

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of stage.LOAD_STG_RECORD_ENTITY_IDENT';
     EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
     
      
      EXEC exec_monitor.print_status_sp @exec_id = @l_exec_no;
   END TRY
   BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;	  
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
END


