﻿







-- Copyright 2012 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- KS        12/05/2012   Initial Version 
-- CV        07/17/2015   Automate break up of file

CREATE PROCEDURE [stage].[IMPORT_TRANSFORMATION_LARGE] ( @IN_JOB_FK bigint ) 
AS BEGIN 


DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME
DECLARE @V_START_TIMESTAMP  DATETIME
DECLARE @V_END_TIMESTAMP    DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int

 DECLARE @v_job_fk             bigint
 DECLARE @v_job_name          varchar(max)
 

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.IMPORT_TRANSFORMATION_LARGE', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp

	 SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      --SET @l_sysdate = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD LARGE FILE BY GROUPS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
	
-------------------------------------------------------------------------------------------------
--   Call the split procedure to set the result type as the file group
--   Group by Group into the IMPORT_RECORD_DATA table with a new JOB_FK
-------------------------------------------------------------------------------------------------

  execute   [import].[SPLIT_RECORD_DATA_LARGE]  @in_job_fk

------------------------------------------------------------------------------------------------------------------------

      DECLARE  @vm$file_group varchar(64)		   

      DECLARE  cur_v_file_group cursor local for
			 SELECT DISTINCT
			        A.file_group_value			         
			  FROM (
				SELECT 
				       CASE WHEN 2 >  1  --- hardcode for now
				       THEN RESULT_TYPE          --- hardcode 
				       ELSE '-999'
				       END AS file_group_value
				FROM  import.IMPORT_RECORD_DATA_LARGE
				WHERE  JOB_FK = @in_job_fk
				GROUP BY RESULT_TYPE
              ) A
			
			  ORDER BY  A.file_group_value ASC


    OPEN cur_v_file_group;
    FETCH NEXT FROM cur_v_file_group INTO   @vm$file_group

------------------------------------------------------------------------------------------
---  LOOP THROUGH IMPORT FILE GROUPINGS
------------------------------------------------------------------------------------------

         WHILE @@FETCH_STATUS = 0 
         BEGIN

          --- PURGE BEFORE PROCESSING
          EXEC  epacube.PURGE_IMPORT_DATA 1


          ---- CREATE A NEW JOB
		  SET @v_job_name = 'LARGE FILE: ' +  @vm$file_group
		  
		
          EXEC [common].[JOB_CREATE] 
               @IN_JOB_FK = NULL,
               @IN_job_class_fk = 100,
               @IN_JOB_NAME = @v_job_name,     ---- CINDY NEED JOB DISPLAY TEXT HERE
               @IN_DATA1 = NULL,
               @IN_DATA2 = NULL,
               @IN_DATA3 = NULL,
               @IN_JOB_USER = NULL,
               @OUT_JOB_FK = @v_job_fk OUTPUT,
               @CLIENT_APP = 'SSIS' ;


         ----  INSERT THE ROWS INTO THE IMPORT_RECORD_DATA TABLE
         EXEC [stage].[IMPORT_RECORD_DATA_FROM_LARGE] @in_job_fk, @v_job_fk, @vm$file_group


---------------------------------------------------------------------------------------------------
-- CALL PROCEDURES JOB BY JOB
---------------------------------------------------------------------------------------------------

 SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record
       WHERE job_fk = @V_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record
       WHERE job_fk = @V_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      --SET @l_sysdate = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @V_job_fk, 'CALL TRANSFORMATION',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


			EXEC  stage.IMPORT_TRANSFORMATION @V_JOB_FK





    FETCH NEXT FROM cur_v_file_group INTO   @vm$file_group

     END --cur_v_file_group
     CLOSE cur_v_file_group;
	 DEALLOCATE cur_v_file_group; 
     


-------------------------------------------------------------------------------------
-- TRUNCATE THE DATA WHEN DONE..... COMMENT FOR NOW WITH TESTING.. 
--   HAVE CINDY DO THIS AT START OF HER SSIS PACKAGE BEFORE INSERTING
-------------------------------------------------------------------------------------

  TRUNCATE TABLE import.IMPORT_RECORD_DATA_LARGE


-------------------------------------------------------------------------------------
-- LOGGING
-------------------------------------------------------------------------------------

 SET @status_desc =  'finished execution of stage.IMPORT_TRANSFORMATION_LARGE'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of stage.IMPORT_TRANSFORMATION_LARGE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END





































































































