﻿
CREATE PROCEDURE [stage].[import_user_data]
      (
        @IN_PACKAGE_FK INT
      , @IN_JOB_FK BIGINT
      )
AS -- Copyright 2006 epaCUBE, Inc.
  --
  -- Purpose: Import User Data.
  --
  -- MODIFICATION HISTORY
  -- Person     Date        Comments
  -- --------- ----------  -------------------------------------------------
  --           05/03/2008  Initial version
  --           05/05/2008  Added logic to propogate data into epacube.USERS table
  --           05/16/2008  Added use of generic stage.IMPORT_DATA_EXCEPTION table
      DECLARE @l_sysdate DATETIME
            , @ls_stmt VARCHAR(1000)
            , @l_exec_no FLOAT(53)
            , @v_count BIGINT
            , @v_batch_no BIGINT
            , @v_job_class_fk INT
            , @v_job_name VARCHAR(64)
            , @v_data1 VARCHAR(128)
            , @v_data2 VARCHAR(128)
            , @v_data3 VARCHAR(128)
            , @v_job_user VARCHAR(64)
            , @v_out_job_fk BIGINT
            , @v_input_rows BIGINT
            , @v_output_rows BIGINT
            , @v_output_total BIGINT
            , @v_exception_rows BIGINT
            , @v_job_date DATETIME
            , @v_sysdate DATETIME
            , @status_desc VARCHAR(MAX)
      SET @l_sysdate = GETDATE()
      SET @v_job_class_fk = 270
      SET @v_job_user = 'IMPORT'
      SET @v_job_date = GETDATE() ;
      SET @v_sysdate = GETDATE()
      BEGIN
            SET @l_exec_no = 0 ;
            SET @ls_stmt = 'started execution of STAGE.IMPORT_USER_DATA. ' +
                ' Job_FK: ' +
                CAST(ISNULL(@IN_JOB_FK, 0) AS VARCHAR(16)) +
                ', Package_FK: ' +
                CAST(ISNULL(@IN_PACKAGE_FK, 0) AS VARCHAR(16))
            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                 @exec_id = @l_exec_no OUTPUT ;
            BEGIN TRY
                  SELECT TOP 1
                            @v_job_name = ip.name
                          , @v_data1 = iu.import_filename
                  FROM      import.import_users iu 
                  INNER JOIN import.import_package ip
                            ON ( ip.import_package_id = iu.import_package_fk )
                  WHERE     iu.job_fk = @IN_JOB_FK
                  EXEC common.job_create @IN_JOB_FK,
                       @V_job_class_fk,
                       @V_JOB_NAME,
                       @V_DATA1,
                       @V_DATA2,
                       @V_DATA3,
                       @V_JOB_USER,
                       @V_OUT_JOB_FK OUTPUT ;
                  IF ( @in_package_fk IS NOT NULL AND
                       @in_job_fk IS NOT NULL
                     ) 
                     BEGIN
        -------------------------------------------------------------------------------------
        --   put comment here
        -------------------------------------------------------------------------------------
                           SELECT   @v_input_rows = COUNT(1)
                           FROM     import.import_users
                           WHERE    job_fk = @in_job_fk ;
                           SET @v_output_rows = 0 -- REMOVE THIS LINE AFTER YOU RESOLVE NEXT COMMENTED BLOCK
                           SET @v_exception_rows = @v_input_rows - @v_output_rows ;
                           SET @l_sysdate = GETDATE()
                           EXEC common.job_execution_create @in_job_fk, 'IMPORT USERS TO EPACUBE USERS tables', @v_input_rows, @v_output_rows, @v_exception_rows, 201, @v_sysdate, @l_sysdate ;
        -- Put the logic to load the data into epacube.USERS table here
        -- Update existing rows first
                           UPDATE   epacube.users
                           SET      password = ( SELECT TOP 1
                                                        ISNULL(s.password, s.user_id)
                                                 FROM   import.import_users s
                                                 WHERE  s.job_fk = @IN_JOB_FK AND
                                                        s.import_package_fk = @IN_PACKAGE_FK AND
                                                        epacube.users.login = s.user_id
                                               )
                                  , FIRST = ( SELECT TOP 1
                                                        s.first_name
                                              FROM      import.import_users s
                                              WHERE     s.job_fk = @IN_JOB_FK AND
                                                        s.import_package_fk = @IN_PACKAGE_FK AND
                                                        epacube.users.login = s.user_id
                                            )
                                  , LAST = ( SELECT TOP 1
                                                    ISNULL(s.last_name, s.first_name)
                                             FROM   import.import_users s
                                             WHERE  s.job_fk = @IN_JOB_FK AND
                                                    s.import_package_fk = @IN_PACKAGE_FK AND
                                                    epacube.users.login = s.user_id
                                           )
                           FROM     import.import_users iu
                           WHERE    ( iu.user_id IS NOT NULL AND
                                      epacube.users.login = iu.user_id AND
                                      iu.job_fk = @IN_JOB_FK AND
                                      iu.import_package_fk = @IN_PACKAGE_FK
                                    ) ;
        
        -- Insert non existent rows into epacube.USERS table
        
                           INSERT   INTO epacube.users
                                    (
                                      login
                                    , password
                                    , FIRST
                                    , LAST
                                    )
                                    SELECT DISTINCT
                                            s.user_id
                                          , ISNULL(s.password, s.user_id)
                                          , s.first_name
                                          , ISNULL(s.last_name, s.first_name) AS "last"
                                    FROM    import.import_users s
                                    WHERE   s.user_id IS NOT NULL AND
                                            s.job_fk = @IN_JOB_FK AND
                                            s.import_package_fk = @IN_PACKAGE_FK AND
                                            NOT EXISTS ( SELECT 1
                                                         FROM   epacube.users d
                                                         WHERE  d.login = s.user_id ) ;
        
        -- Insert non Existent data into epacube.USER_GROUPS table
                           IF OBJECT_ID('tempdb..#t_user_groups') > 0 
                              DROP TABLE #t_user_groups ;
                           CREATE TABLE #t_user_groups
                                  (
                                    users_fk INT
                                  , groups_fk INT
                                  ) ;
                           INSERT   INTO #t_user_groups ( users_fk, groups_fk )
                                    SELECT  s.users_id AS users_fk
                                          , ( SELECT    groups_id
                                              FROM      epacube.groups
                                              WHERE     name = ( SELECT TOP ( 1 )
                                                                        iu.user_group
                                                                 FROM   import.import_users iu
                                                                 WHERE  iu.job_fk = @IN_JOB_FK AND
                                                                        iu.import_package_fk = @IN_PACKAGE_FK AND
                                                                        iu.user_id = s.login
                                                               )
                                            ) AS groups_fk
                                    FROM    epacube.users s 
                                    INNER JOIN import.import_users iu1
                                            ON ( iu1.job_fk = @IN_JOB_FK AND
                                                 iu1.import_package_fk = @IN_PACKAGE_FK AND
                                                 s.login = iu1.user_id
                                               ) ;
        -- check User_Group data for data exceptions
        
                           INSERT   INTO epacube.user_groups ( users_fk, groups_fk )
                                    SELECT  t.users_fk
                                          , t.groups_fk
                                    FROM    #t_user_groups t
                                    WHERE   NOT EXISTS ( SELECT 1
                                                         FROM   epacube.user_groups d
                                                         WHERE  d.users_fk = t.users_fk AND
                                                                d.groups_fk = t.groups_fk ) ;
                           IF OBJECT_ID('tempdb..#t_user_groups') > 0 
                              DROP TABLE #t_user_groups ;
        -- Update the User_Entity_Permissions table 
        -- Insert non existent data into epacube.User_Entity_Permissions
                           IF OBJECT_ID('tempdb..#t_user_entity_permissions') > 0 
                              DROP TABLE #t_user_entity_permissions ;
                           CREATE TABLE #t_user_entity_permissions
                                  (
                                    users_fk INT
                                  , entity_structure_fk BIGINT
                                  ) ;
                           INSERT   INTO #t_user_entity_permissions
                                    (
                                      users_fk
                                    , entity_structure_fk
                                    )
                                    SELECT  u.users_id
                                          , ( SELECT    entity_structure_fk
                                              FROM      epacube.entity_identification
                                              WHERE     VALUE = iu1.user_entity_value AND
                                                        data_name_fk = ( SELECT data_name_id
                                                                         FROM   epacube.data_name
                                                                         WHERE  name = iu1.user_entity_name
                                                                       )
                                            ) AS entity_structure_fk
                                    FROM    epacube.users u 
                                    INNER JOIN import.import_users iu1
                                            ON ( iu1.job_fk = @IN_JOB_FK AND
                                                 iu1.import_package_fk = @IN_PACKAGE_FK AND
                                                 u.login = iu1.user_id
                                               ) ;

        -- insert data excecptions here
                           INSERT   INTO stage.import_data_exceptions
                                    (
                                      package_fk
                                    , job_fk
                                    , target_table
                                    , action_failed
                                    , error_desc
                                    )
                                    SELECT  @IN_PACKAGE_FK
                                          , @IN_JOB_FK
                                          , 'EPACUBE.USER_ENTITY_PERMISSIONS'
                                          , 'INSERT'
                                          , 'MISSING entity_structure_fk FOR USERS_FK: ' + CAST(users_fk AS VARCHAR(12))
                                    FROM    #t_user_entity_permissions
                                    WHERE   entity_structure_fk IS NULL ;

                           INSERT   INTO epacube.user_entity_permissions
                                    (
                                      users_fk
                                    , entity_structure_fk
                                    )
                                    SELECT  users_fk
                                          , entity_structure_fk
                                    FROM    #t_user_entity_permissions t
                                    WHERE   entity_structure_fk IS NOT NULL AND
                                            NOT EXISTS ( SELECT 1
                                                         FROM   epacube.user_entity_permissions d
                                                         WHERE  d.users_fk = t.users_fk AND
                                                                d.entity_structure_fk = t.entity_structure_fk ) ;
                           IF OBJECT_ID('tempdb..#t_user_entity_permissions') > 0 
                              DROP TABLE #t_user_entity_permissions ;
                           EXEC common.job_execution_create @in_job_fk, 'IMPORT COMPLETE', @v_input_rows, @v_output_total, @v_exception_rows, 201, @v_job_date, @l_sysdate ;
                     END ; -- IF @in_package_fk IS NOT NULL AND @in_job_fk IS NOT NULL
                  SET @status_desc = 'finished execution of STAGE.IMPORT_USER_DATA' ;
                  EXEC exec_monitor.report_status_sp @l_exec_no,
                       @status_desc ;
            END TRY
            BEGIN CATCH
                  DECLARE @ErrorMessage NVARCHAR(4000) ;
                  DECLARE @ErrorSeverity INT ;
                  DECLARE @ErrorState INT ;
                  SELECT    @ErrorMessage = 'Execution of STAGE.IMPORT_USER_DATA has failed ' + Error_message()
                          , @ErrorSeverity = Error_severity()
                          , @ErrorState = Error_state() ;
                  EXEC exec_monitor.report_error_sp @l_exec_no ;
                  declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
                  RAISERROR ( @ErrorMessage, -- Message text.
                            @ErrorSeverity, -- Severity.
                            @ErrorState -- State.
    ) ;
            END CATCH ;
      END


