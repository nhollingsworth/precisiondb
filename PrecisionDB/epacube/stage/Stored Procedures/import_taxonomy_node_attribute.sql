﻿
CREATE PROCEDURE [stage].[import_taxonomy_node_attribute]
      (
        @IN_PACKAGE_FK INT
      , @IN_JOB_FK BIGINT
       
      )
AS -- Copyright 2008 epaCUBE, Inc.
  --
  -- Purpose: Import Taxonomy Tree Data.
  --
  -- MODIFICATION HISTORY
  -- Person     Date        Comments
  -- --------- ----------  -------------------------------------------------
  -- YN        10/12/2008  Initial version
  -- MS        05/13/2014  Added processing to handle Attribute and Value
  --                       Display Sequence columns
  -- MS		   05/28/2014  Added the Attribute Type (Pricebook Data Value) processing
  
      DECLARE @l_sysdate DATETIME
            , @ls_stmt VARCHAR(1000)
            , @l_exec_no FLOAT(53)
            , @v_count BIGINT
            , @v_batch_no BIGINT
            , @v_job_class_fk INT
            , @v_job_name VARCHAR(64)
            , @v_data1 VARCHAR(128)
            , @v_data2 VARCHAR(128)
            , @v_data3 VARCHAR(128)
            , @v_job_user VARCHAR(64)
            , @v_out_job_fk BIGINT
            , @v_input_rows BIGINT
            , @v_output_rows BIGINT
            , @v_output_total BIGINT
            , @v_exception_rows BIGINT
            , @v_job_date DATETIME
            , @v_sysdate DATETIME
            , @status_desc VARCHAR(MAX)
            , @taxonomy_tree_id INT
            , @DataSet_TableName VARCHAR(128)
      SET @l_sysdate = GETDATE()
      SET @v_job_class_fk = 270
      SET @v_job_user = 'IMPORT'
      SET @v_job_date = GETDATE() ;
      SET @v_sysdate = GETDATE()
      SET @DataSet_TableName = 'PRODUCT_TAX_ATTRIBUTE'
      BEGIN
            SET @l_exec_no = 0 ;
            SET @ls_stmt = 'started execution of STAGE.IMPORT_TAXONOMY_NODE_ATTRIBUTE. ' +
                ' Job_FK: ' +
                CAST(ISNULL(@IN_JOB_FK, 0) AS VARCHAR(16)) +
                ', Package_FK: ' +
                CAST(ISNULL(@IN_PACKAGE_FK, 0) AS VARCHAR(16))
            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                 @exec_id = @l_exec_no OUTPUT ;
            BEGIN TRY
                  SELECT TOP 1
                            @v_job_name = ip.name
                          , @v_data1 = iu.import_filename
                  FROM      import.IMPORT_TAXONOMY_TREE_ATTRIBUTE iu 
                  INNER JOIN import.import_package ip
                            ON ( ip.import_package_id = iu.import_package_fk )
                  WHERE     iu.job_fk = @IN_JOB_FK ;
                  
                  EXEC common.job_create @IN_JOB_FK,
                       @V_job_class_fk,
                       @V_JOB_NAME,
                       @V_DATA1,
                       @V_DATA2,
                       @V_DATA3,
                       @V_JOB_USER,
                       @V_OUT_JOB_FK OUTPUT ;
                  IF ( @in_package_fk IS NOT NULL AND
                       @in_job_fk IS NOT NULL
                     ) 
                     BEGIN
        -------------------------------------------------------------------------------------
        --   put comment here
        -------------------------------------------------------------------------------------
                           SELECT   @v_input_rows = COUNT(1)
                           FROM     import.IMPORT_TAXONOMY_TREE_ATTRIBUTE
                           WHERE    job_fk = @in_job_fk ;
                           SET @v_output_rows = 0 -- REMOVE THIS LINE AFTER YOU RESOLVE NEXT COMMENTED BLOCK
                           SET @v_exception_rows = @v_input_rows - @v_output_rows ;
                           SET @l_sysdate = GETDATE()
                           EXEC common.job_execution_create @in_job_fk, 'IMPORT Taxonomy Node Attribute TO EPACUBE Taxonomy_Node_Attribute table', @v_input_rows, @v_output_rows, @v_exception_rows, 201, @v_sysdate, @l_sysdate ;
        
        -- Per Kathi the intial row must exist in the Taxonomy_Tree table before this procedure is executed
        -- So all the changes have to be applied to Taxonomy_Node table
               
                           SELECT   @taxonomy_tree_id = tt.taxonomy_tree_id
                           FROM     epacube.TAXONOMY_TREE tt
                           WHERE    tt.NAME LIKE ( SELECT TOP 1
                                                            tree_name
                                                   FROM     import.IMPORT_TAXONOMY_TREE
                                                 )
           
                           IF @@ROWCOUNT = 0 OR
                              @taxonomy_tree_id IS NULL 
                              RAISERROR ( 'Taxonomy Tree Must EXISTS IN the epacube.TAXONOMY_TREE TABLE before the import', -- Message text.
                                        16, -- Severity.
                                        0 -- State.      
                                         ) ;
                                         
                           
                           DECLARE @vm$Delete_Flag CHAR(1)
                                 , @vm$Tree_Name varchar(64)
                                 , @vm$Node_Code varchar(64)
                                 , @vm$Attribute_Name varchar(100)
                                 , @vm$Attribute_Value varchar(256)
                                 , @vm$Data_Type varchar(50)
                                 , @vm$Default_Value varchar(100)
                                 , @vm$Restricted_Lookup varchar(3)
                                 , @vm$Pricebook_Flag INT
                                 , @vm$ActivityCode INT
                                 , @vm$Data_Set_ID INT
                                 , @vm$import_filename VARCHAR(128)
                                 , @vm$record_no INT
								 , @vm$Attribute_Display_Seq INT
								 , @vm$AttrValue_Display_Seq INT
								 , @vm$Attribute_Type VARCHAR(50)
								 , @vm$pricebook_ind_dv_fk BIGINT  
                            
        
                           DECLARE tax_tree_node_attr cursor local
                                   FOR SELECT   import_filename
                                              , record_no
                                              , Delete_Flag
                                              , Tree_Name
                                              , Node_Code
                                              , [Attribute_Name]
                                              , [Attribute_Value]
                                              , [Data_Type]
                                              , [Default_Value]
                                              , [Restricted_Lookup]
                                              , [Pricebook_Flag]
											  , [Attribute_Name_DISPLAY_SEQ]
											  , [Attribute_Value_DISPLAY_SEQ]
											  , [Attribute_Type]
                                       FROM     import.IMPORT_TAXONOMY_TREE_ATTRIBUTE
                                       WHERE    JOB_FK = @in_job_fk
                                       ORDER BY record_no ASC ;
       
                           OPEN tax_tree_node_attr ;
       
        
                           FETCH NEXT FROM tax_tree_node_attr INTO @vm$import_filename, @vm$record_no, @vm$Delete_Flag, @vm$Tree_Name, @vm$Node_Code, @vm$Attribute_Name, @vm$Attribute_Value, @vm$Data_Type, @vm$Default_Value, @vm$Restricted_Lookup, @vm$Pricebook_Flag, @vm$Attribute_Display_Seq, @vm$AttrValue_Display_Seq, @vm$Attribute_Type ;

                           WHILE @@FETCH_STATUS = 0 
                                 BEGIN
                                       IF @vm$Delete_Flag = 'N' 
                                          SET @vm$ActivityCode = 8003 ;
                                       ELSE 
                                          SET @vm$ActivityCode = 8004 ;
                                          
                                       IF @vm$Restricted_Lookup IS NULL OR
                                          LEFT(LTRIM(@vm$Restricted_Lookup), 1) = 'N' 
                                          SET @vm$Restricted_Lookup = 0 ;
                                       ELSE 
                                          SET @vm$Restricted_Lookup = 1 ;

									   --  Determine the Data Value for the Price Book DV
									   IF @vm$Attribute_Type IS NULL
                                          RAISERROR ( 'Attribute Type must exist', -- Message text.
                                                    16, -- Severity.
                                                    0 -- State.      
                                         ) ;

									   SELECT TOP 1
												@vm$pricebook_ind_dv_fk = data_value_id 
												from epacube.DATA_VALUE dv1
												inner join epacube.DATA_NAME dn1
												on dv1.DATA_NAME_FK = dn1.DATA_NAME_ID
												and dn1.NAME = 'PRICEBOOK INDICATOR'
												where dv1.value = LTRIM(@vm$Attribute_Type)

									  IF @vm$pricebook_ind_dv_fk IS NULL or
									      @@ROWCOUNT = 0 
                                          RAISERROR ( 'Attribute Type not found on epacube.data_value Table', -- Message text.
                                                    16, -- Severity.
                                                    0 -- State.      
                                         ) ;
                                          
                                       -- lets get the data_set_id here    
                                       SELECT TOP 1
                                                @vm$Data_Set_ID = DATA_SET_ID
                                       FROM     epacube.DATA_SET
                                       WHERE    data_type_cr_fk = ( SELECT  code_ref_id
                                                                    FROM    epacube.CODE_REF
                                                                    WHERE   code = @vm$Data_Type
                                                                  ) AND
                                                TABLE_NAME = @DataSet_TableName
                                       
                                       IF @vm$Data_Set_ID IS NULL OR
                                          @@ROWCOUNT = 0 
                                          RAISERROR ( 'Data Set Must EXISTS IN the epacube.data_set TABLE before the import', -- Message text.
                                                    16, -- Severity.
                                                    0 -- State.      
                                         ) ;
             
                                       BEGIN TRY
                                                 
                                             EXEC [synchronizer].[TAXONOMY_NODE_ATTRIBUTE_EDITOR] @in_taxonomy_tree_fk = @taxonomy_tree_id, --  int
                                                  @in_taxonomy_node_fk = 0, --  bigint
                                                  @in_taxonomy_node_attribute_id = NULL, --  int
                                                  @in_data_set_fk = @vm$Data_Set_ID, --  int
                                                  @in_data_name = @vm$Attribute_Name, --  varchar(64)
                                                  @in_data_value = @vm$Attribute_Value, --  varchar(64)
												  @in_pricebook_attribute_ind = @vm$Pricebook_Flag,
                                                  --@in_pricebook_attribute_ind = 0, --  smallint
                                                  @in_restricted_data_values_ind = @vm$Restricted_Lookup, --  smallint
                                                  @in_node_code = @vm$Node_Code, -- varcahr(64)
                                                  @in_update_user = 'IMPORT', --  varchar(64)
                                                  @in_ui_action_cr_fk = @vm$ActivityCode, --  int
                                                  @in_default_value = @vm$Default_Value,
												  @in_attribute_display_seq = @vm$Attribute_Display_Seq,
												  @in_attrvalue_display_seq = @vm$AttrValue_Display_Seq,
												  @in_pricebook_ind_dv_fk = @vm$pricebook_ind_dv_fk

                                       END TRY
                                       
                                       BEGIN CATCH
                                              -- insert data excecptions here
                                             SELECT @status_desc = ERROR_MESSAGE() 
                                             INSERT INTO stage.import_data_exceptions
                                                    (
                                                      package_fk
                                                    , job_fk
                                                    , target_table
                                                    , action_failed
                                                    , error_desc
                                                    )
                                                    SELECT  @IN_PACKAGE_FK
                                                          , @IN_JOB_FK
                                                          , 'EPACUBE.TAXONOMY_NODE_ATTRIBUTE'
                                                          , 'INSERT / UPDATE'
                                                          , 'Record No: ' + CAST(@vm$record_no AS VARCHAR(20)) + ' failed in import file: ' + @vm$import_filename + ' with error: ' + @status_desc ;
                                       
                                         
                                       
                                       END CATCH ; 
    
                                       FETCH NEXT FROM tax_tree_node_attr INTO @vm$import_filename, @vm$record_no, @vm$Delete_Flag, @vm$Tree_Name, @vm$Node_Code, @vm$Attribute_Name, @vm$Attribute_Value, @vm$Data_Type, @vm$Default_Value, @vm$Restricted_Lookup, @vm$Pricebook_Flag, @vm$Attribute_Display_Seq, @vm$AttrValue_Display_Seq, @vm$Attribute_Type ;
                                 END ;
                           CLOSE tax_tree_node_attr ;
                           DEALLOCATE tax_tree_node_attr ;
                           
       
        
                           EXEC common.job_execution_create @in_job_fk, 'IMPORT COMPLETE', @v_input_rows, @v_output_total, @v_exception_rows, 201, @v_job_date, @l_sysdate ;
                     END ; -- IF @in_package_fk IS NOT NULL AND @in_job_fk IS NOT NULL
                  SET @status_desc = 'finished execution of STAGE.IMPORT_TAXONOMY_NODE_ATTRIBUTE' ;
                  EXEC exec_monitor.report_status_sp @l_exec_no,
                       @status_desc ;
            END TRY
            BEGIN CATCH
                  DECLARE @ErrorMessage NVARCHAR(4000) ;
                  DECLARE @ErrorSeverity INT ;
                  DECLARE @ErrorState INT ;
                  SELECT    @ErrorMessage = 'Execution of STAGE.IMPORT_TAXONOMY_NODE_ATTRIBUTE has failed ' + Error_message()
                          , @ErrorSeverity = Error_severity()
                          , @ErrorState = Error_state() ;
                  EXEC exec_monitor.report_error_sp @l_exec_no ;
                  declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
                  RAISERROR ( @ErrorMessage, -- Message text.
                            @ErrorSeverity, -- Severity.
                            @ErrorState -- State.
    ) ;
            END CATCH ;
      END


