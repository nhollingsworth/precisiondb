﻿








-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
--
-- Purpose: Load all the Import Data into the Stage Tables
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/05/2007   Initial version
-- CV	     06/11/2009   Trying to fix duplicate creation for package 5					  
-- CV        02/17/2010   Added Do not import ind logic 
-- CV        03/15/2010   Change 'NULL' to '-999'
-- KS        07/15/2010   Removed reference to org_data_name_fk... no org specific idents.
-- CV        01/07/2011   Added new packages for ecl customers
-- CV        06/26/2011   Added update to id_org_value for Resolver to diplay datanames correctly  
-- CV        09/14/2011   tightened down update for customer import packages of entity structure
-- MN		 02/17/2014	  Stripped down all cursors code

CREATE PROCEDURE [stage].[LOAD_STG_RECORD_IDENT] (
      @in_job_fk           bigint
     ,@in_import_package_fk int
   )
AS
BEGIN


/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This procedure is called from LOAD_STG_RECORD
--
--   Its purpose is to attempt to match ONLY the Import Entity Data Name with its 
--   epaCUBE Entity Data Name utilizing the available Imported Identifiers 
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   Resolver Activity is ALWAYS SEEDED to be 1 for Products and 2 for Entities
--	 For Resolver Activities ONLY the job_fk is a parameter;  for cleasner activies
--	 it resides on the Search Activity itself.. ( will want to change that in the future )
--
--   In Cleanser a Display direction is setup for each Activity (see @v_from_data_set & @v_to_data_set) 
--   This direction determines how the data is displayed in the UI and also drives how this
--   procedure MUST insert the matching results into the tables for PROPER display.
--   The default direction currently is (FROM) EPACUBE --> (TO) STAGE to accomodate the UI.
--		The UI ONLY WORKS in one direction right now.  (FROM) EPACUBE --> (TO) STAGE.
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Every Entity Structure's Stg_Entity_Identification has been logged and only the rows
--   whose data name is a UNIQUE Entity Identifier have been matched at this time.
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--   Remove any previously inserted [cleanser].[SEARCH_RESULTS_IDENT] by Job_ID 
--		for all UNMATCHED Entity_Structure_FK's
--   If any Stg_Entity_Identification have a NULL entity_structure_fk and its data name is
--   a UNIQUE Entity Identifier then try again to update the entity_structure_fk
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   Insert any missing STG_Entity_Identification Rows Unique and Non Unique matching
--   into [cleanser].[SEARCH_RESULTS_IDENT] for the Stg_Entity_Structure's Entity Data Name Only.
-------------------------------------------------------------------------------------------------
--   PROCESS STEP3: 
--   If for each Stg_Entity_Structure there is ONLY 1 matching entity_structure_fk in 
--   [cleanser].[SEARCH_RESULTS_IDENT] then mark as MATCHED;  if No entity_structure_fk's 
--	 found at all then mark with the ACTIVITY_ACTION_CR_FK default in the package
-------------------------------------------------------------------------------------------------
--   PROCESS STEP4: 
--   Update Stg Entity Structure with the appropriate entity_structure_fk; stage status 
--   and event action
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   Currently ONLY writing the RESOLVER approach and utilizing the current UI flow
--	 which is (FROM) EPACUBE --> (TO) STAGE becasue that is the ONLY direction the UI works with.
--   When we determine we need to allow for more flexibiltiy we will address it then.
--
--   AWAITING DECENT REQUIREMENTS FROM JAN AND COMPANY.... 
--		DO NOT BELIEVE THAT WE SHOULD AUTO MATCH ON NONUNIQE AT ALL DURING RESOLVER.
--
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500)
	 DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME

     DECLARE @v_batch_no          bigint
	 , @Org_id_entity_value_column VARCHAR(50)

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.LOAD_STG_RECORD_IDENT', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE()

	 SELECT @Org_id_entity_value_column = column_name  from import.import_map_metadata 
					where DATA_NAME_FK = 141000 and IMPORT_PACKAGE_FK =  @in_import_package_fk 

/*
 -- ------------------------------------------------------------------------------------
 --       Insert Identification Rows into stage.STG_RECORD_IDENT
 --			First the Row Data Name Identfications for the Import
 --			Idents driven from the Ident Data Name column 
 --
 -- -----------------------------------------------------------------------------------
 --*/


	 ;WITH IdentificationRows AS 
(SELECT DISTINCT
					SREC.STG_RECORD_ID STG_RECORD_FK, 
						   SREC.ENTITY_CLASS_CR_FK,
						   SREC.ENTITY_DATA_NAME_FK,
						   vm.ident_data_name_fk ID_DATA_NAME_FK,
						   UPPER(RTRIM(LTRIM([epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name)))) ID_VALUE,					 
					ISNULL (immev.ident_data_name_fk, -999 )  ID_ENTITY_DATA_NAME_FK,
					CASE isnull ( dn.entity_data_name_fk, 0 ) WHEN 0 THEN '-999' ELSE UPPER(RTRIM(LTRIM([epacube].[getValueFromImportData](id.import_record_data_id, immev.column_name)))) END  AS ID_ENTITY_VALUE,
					vm.import_column_name COLUMN_NAME,
					SREC.JOB_FK,
					vm.event_source_cr_fk			                                                      
			  FROM import.v_import vm
			  INNER JOIN epacube.data_name dn WITH (NOLOCK)
			    ON ( dn.data_name_id = vm.ident_data_name_fk )
			  INNER JOIN epacube.data_set ds WITH (NOLOCK)
			    ON ( ds.data_set_id = dn.data_set_fk )	
			  LEFT JOIN import.import_map_metadata immev WITH (NOLOCK)
			    ON ( immev.import_package_fk = vm.import_package_fk 
			    AND  immev.data_name_fk = dn.entity_data_name_fk )	
			inner join stage.STG_RECORD SREC  WITH (NOLOCK) 
				ON 	 SREC.ENTITY_DATA_NAME_FK = vm.data_name_fk
					INNER JOIN IMPORT.IMPORT_RECORD_DATA ID WITH (NOLOCK) 
					 ON ( ID.JOB_FK = SREC.JOB_FK
					 AND  ID.RECORD_NO = SREC.RECORD_NO 
					 AND ISNULL ( ID.DO_NOT_IMPORT_IND, 0 ) <> 1)					 		    
			  WHERE vm.import_package_fk = @in_import_package_fk
			  AND   vm.import_column_name IS NOT NULL 
			  AND   vm.table_name IN ( 'ENTITY_STRUCTURE', 'PRODUCT_STRUCTURE' )
			 AND [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name) IS NOT NULL
              AND [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name) <> ' '
			  AND SREC.JOB_FK = @in_job_fk
)
INSERT INTO stage.STG_RECORD_IDENT
							( STG_RECORD_FK,
							  ENTITY_CLASS_CR_FK,							  
							  ENTITY_DATA_NAME_FK,
							  ID_DATA_NAME_FK,
							  ID_VALUE,
							  ID_ENTITY_DATA_NAME_FK,
							  ID_ENTITY_VALUE,	
							  COLUMN_NAME,							  
							  JOB_FK,
							  EVENT_SOURCE_CR_FK					  							  							  
							)
SELECT 
							  STG_RECORD_FK,
							  ENTITY_CLASS_CR_FK,							  
							  ENTITY_DATA_NAME_FK,
							  ID_DATA_NAME_FK,
							  ID_VALUE,
							  ID_ENTITY_DATA_NAME_FK,
							  ID_ENTITY_VALUE,	
							  COLUMN_NAME,							  
							  JOB_FK,
							  EVENT_SOURCE_CR_FK
FROM IdentificationRows
     
	 SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows INSERTED into the STAGE.STG_RECORD_IDENT FROM Temp table IdentificationRows = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk
       END

/*
 -- ------------------------------------------------------------------------------------
 --       Insert Identification Rows into stage.STG_RECORD_IDENT
 --			Next all other Identfication for the Import Record Data
 -- -----------------------------------------------------------------------------------
 --*/

      ;WITH IdentificationRows1 AS (SELECT DISTINCT
					SREC.STG_RECORD_ID STG_RECORD_FK,
						   SREC.ENTITY_CLASS_CR_FK,
						   SREC.ENTITY_DATA_NAME_FK, 
						   vm.data_name_fk ID_DATA_NAME_FK,
						   UPPER(RTRIM(LTRIM([epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name)))) ID_VALUE,
						   ISNULL (immev.ident_data_name_fk, -999 )  ID_ENTITY_DATA_NAME_FK,
					CASE isnull ( dn.entity_data_name_fk, '0' ) WHEN '0' THEN '-999' ELSE UPPER(RTRIM(LTRIM([epacube].[getValueFromImportData](id.import_record_data_id, immev.column_name)))) END  AS ID_ENTITY_VALUE,
					 ISNULL (vm.COLUMN_ORG_DATA_NAME_FK ,-999) ID_ORG_DATA_NAME_FK,
					CASE isnull ( dn.ORG_DATA_NAME_FK, 0 )
					WHEN 0 THEN -999
					ELSE ISNULL((UPPER(RTRIM(LTRIM([epacube].[getValueFromImportData](id.import_record_data_id,@Org_id_entity_value_column))))), -999)
				    END  ID_ORG_VALUE,
					vm.import_column_name COLUMN_NAME,
					SREC.JOB_FK,
					vm.event_source_cr_fk                                                     					                                                    
			  FROM import.v_import vm
			  INNER JOIN epacube.data_name dn WITH (NOLOCK)
			    ON ( dn.data_name_id = vm.data_name_fk )	
			  INNER JOIN epacube.data_set ds WITH (NOLOCK)
			    ON ( ds.data_set_id = dn.data_set_fk )
			  LEFT JOIN import.import_map_metadata immev WITH (NOLOCK)
			    ON ( immev.import_package_fk = vm.import_package_fk 
			    AND  immev.data_name_fk = dn.entity_data_name_fk )   
			INNER JOIN stage.STG_RECORD SREC  WITH (NOLOCK) 
				ON SREC.entity_class_cr_fk = vm.entity_class_cr_fk
					INNER JOIN IMPORT.IMPORT_RECORD_DATA ID WITH (NOLOCK) 
					 ON ( ID.JOB_FK = SREC.JOB_FK
					 AND  ID.RECORD_NO = SREC.RECORD_NO
					 AND ISNULL ( ID.DO_NOT_IMPORT_IND, 0 ) <> 1) 
			  WHERE vm.import_package_fk =  @in_import_package_fk
			  AND   vm.import_column_name IS NOT NULL 			  
			  AND   vm.table_name in ( 'ENTITY_IDENTIFICATION', 'ENTITY_IDENT_MULT', 'ENTITY_IDENT_NONUNIQUE',
                                       'PRODUCT_IDENTIFICATION', 'PRODUCT_IDENT_MULT', 'PRODUCT_IDENT_NONUNIQUE'  )   
				---did not want to load the parent codes here.
				and vm.SOURCE_FIELD_NAME <> 'PARENT_ENTITY' 
				 AND [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name) IS NOT NULL
              AND [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name) <> ' '
			  AND SREC.JOB_FK = @in_job_fk
)
INSERT INTO stage.STG_RECORD_IDENT
							( STG_RECORD_FK,
							  ENTITY_CLASS_CR_FK,							  
							  ENTITY_DATA_NAME_FK,
							  ID_DATA_NAME_FK,
							  ID_VALUE,
							  ID_ENTITY_DATA_NAME_FK,
							  ID_ENTITY_VALUE,
							  ID_ORG_DATA_NAME_FK,
							  ID_ORG_VALUE,
							  COLUMN_NAME,							  
							  JOB_FK,
							  EVENT_SOURCE_CR_FK							  							  							  
							)
SELECT 
							  STG_RECORD_FK,
							  ENTITY_CLASS_CR_FK,							  
							  ENTITY_DATA_NAME_FK,
							  ID_DATA_NAME_FK,
							  ID_VALUE,
							  ID_ENTITY_DATA_NAME_FK,
							  ID_ENTITY_VALUE,
							  ID_ORG_DATA_NAME_FK,
							  ID_ORG_VALUE,
							  COLUMN_NAME,							  
							  JOB_FK,
							  EVENT_SOURCE_CR_FK
FROM IdentificationRows1

	 SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows INSERTED into the STAGE.STG_RECORD_IDENT FROM Temp table IdentificationRows1 = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk
       END

-------------------------------------------------------------------------------------------
--   UPDATE ID_ENTITY_STRUCTURE_FK IN STG_RECORD_IDENT TABLE
-------------------------------------------------------------------------------------
SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'UPDATE ID_ENTITY_STRUCTURE_FK IN STG_RECORD_IDENT TABLE',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

UPDATE stage.STG_RECORD_IDENT
SET ID_ENTITY_STRUCTURE_FK = 
       ( SELECT TOP 1 SRECEI.ENTITY_STRUCTURE_FK
         FROM stage.STG_RECORD_ENTITY_IDENT SRECEI  WITH ( NOLOCK )
         WHERE SRECEI.STG_RECORD_FK = stage.STG_RECORD_IDENT.STG_RECORD_FK
         AND   SRECEI.ENTITY_ID_DATA_NAME_FK = stage.STG_RECORD_IDENT.ID_ENTITY_DATA_NAME_FK
         AND   SRECEI.ENTITY_ID_VALUE = stage.STG_RECORD_IDENT.ID_ENTITY_VALUE )
WHERE ID_ENTITY_DATA_NAME_FK IS NOT NULL
AND   STG_RECORD_FK IN 
        ( SELECT STG_RECORD_ID
          FROM stage.STG_RECORD
          WHERE JOB_FK = @in_Job_fk )

---only for customer import...
If @in_import_package_fk in (241000, 242000, 341000, 342000) --added new ecl packages
BEGIN

UPDATE stage.STG_RECORD_IDENT
SET ID_ENTITY_STRUCTURE_FK = 
       ( SELECT TOP 1 SRECEI.ENTITY_STRUCTURE_FK
         FROM stage.STG_RECORD_ENTITY_IDENT SRECEI  WITH ( NOLOCK )
         WHERE SRECEI.STG_RECORD_FK = stage.STG_RECORD_IDENT.STG_RECORD_FK
         AND SRECEI.ENTITY_DATA_NAME_FK = stage.STG_RECORD_IDENT.ENTITY_DATA_NAME_FK
         AND   SRECEI.ENTITY_ID_DATA_NAME_FK = (select data_name_id
from epacube.data_name where name = 'CUSTOMER CODE'))
WHERE ID_ENTITY_DATA_NAME_FK IS NOT NULL
AND   STG_RECORD_FK IN 
        ( SELECT STG_RECORD_ID
          FROM stage.STG_RECORD
          WHERE JOB_FK = @in_Job_fk )
AND ID_ENTITY_STRUCTURE_FK is NULL

END



-------------------------------------------------------------------------------------------
--   UPDATE ORG_ENTITY_STRUCTURE_FK IN STG_RECORD_IDENT TABLE
---     KS should not have any org specific identifiers except for whse idents
-------------------------------------------------------------------------------------
SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'UPDATE ORG_ENTITY_STRUCTURE_FK IN STG_RECORD_IDENT TABLE',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

UPDATE stage.STG_RECORD_IDENT
SET ORG_ENTITY_STRUCTURE_FK = 
       ( ISNULL (
		   ( SELECT TOP 1 SRECEI.ENTITY_STRUCTURE_FK
			 FROM stage.STG_RECORD_ENTITY_IDENT SRECEI  WITH ( NOLOCK )
			 WHERE SRECEI.STG_RECORD_FK = stage.STG_RECORD_IDENT.STG_RECORD_FK
			 AND   SRECEI.ENTITY_ID_DATA_NAME_FK = stage.STG_RECORD_IDENT.ID_ORG_DATA_NAME_FK
			 AND   SRECEI.ENTITY_ID_VALUE = stage.STG_RECORD_IDENT.ID_ORG_VALUE )
----------  KS 11/02/2009  REPLACED WITH BELOW			 
----------	     , ( CASE ( SELECT ISNULL ( DS.ORG_IND, 0 )
----------				    FROM EPACUBE.DATA_SET DS WITH ( NOLOCK )
----------				    INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
----------					 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
----------				    WHERE DN.DATA_NAME_ID = stage.STG_RECORD_IDENT.ID_DATA_NAME_FK  )
----------			 WHEN 0 THEN 1
----------			 ELSE NULL
	     , ( CASE WHEN ( SELECT ISNULL ( DS.CORP_IND, 0 )
							FROM EPACUBE.DATA_SET DS WITH ( NOLOCK )
							INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
							 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
							WHERE DN.DATA_NAME_ID = stage.STG_RECORD_IDENT.ID_DATA_NAME_FK  ) = 0 
			 THEN -999
			 ELSE ( CASE WHEN ( SELECT ISNULL ( DS.ORG_IND, 0 )
							FROM EPACUBE.DATA_SET DS WITH ( NOLOCK )
							INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
							 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
							WHERE DN.DATA_NAME_ID = stage.STG_RECORD_IDENT.ID_DATA_NAME_FK  ) = 0 
				    THEN 1  --- CAN ASSUME CORP BECAUSE CORP = 1 AND ORG IS NULL
			        ELSE NULL
			        END )
			 END  ) 
          ) ) 			   
WHERE STG_RECORD_FK IN 
        ( SELECT STG_RECORD_ID
          FROM stage.STG_RECORD
          WHERE JOB_FK = @in_Job_fk )
          
--------------------------------------------cindy added for org data...
UPDATE stage.STG_RECORD_IDENT
SET ORG_ENTITY_STRUCTURE_FK = (select epacube.entity_identification.ENTITY_STRUCTURE_FK
from epacube.ENTITY_IDENTIFICATION
where epacube.entity_identification.value = id_org_value
and  epacube.entity_identification.ENTITY_DATA_NAME_FK = id_org_data_name_fk )
WHERE STG_RECORD_FK IN 
        ( SELECT STG_RECORD_ID
          FROM stage.STG_RECORD
          WHERE JOB_FK = @in_Job_fk )



-----------------------------------------------------------------------------
-- jira - 3082--  data needs to be set to -999 for resolver UI to display data name
--------------------------------------------------------------------------------
update stage.stg_record_ident  --- 
         set id_org_value = '-999'
		 where id_org_value is null 
		 

		 update stage.stg_record_ident  --- 
         set ORG_ENTITY_STRUCTURE_FK = '1'
         where ORG_ENTITY_STRUCTURE_FK is null
		 and ID_DATA_NAME_FK in (
		 select DATA_NAME_ID from epacube.data_name where data_set_fk in (
		 select data_set_id from epacube.data_set where org_ind is NULL
		 and TABLE_NAME in( 'ENTITY_IDENTIFICATION', 'ENTITY_IDENT_MULT', 'ENTITY_IDENT_NONUNIQUE',
                                       'PRODUCT_IDENTIFICATION', 'PRODUCT_IDENT_MULT', 'PRODUCT_IDENT_NONUNIQUE'  )))



         


-------- SET @ls_exec = 'BREAK CODE HERE'
--------            INSERT INTO COMMON.T_SQL
--------                ( TS, SQL_TEXT )
--------            VALUES ( GETDATE(), @ls_exec )
--------
--------         exec sp_executesql 	@ls_exec;
--------



-------------------------------------------------------------------------------------------
--   UPDATE STRIPPED_ID_VALUE IN STG_RECORD_IDENT TABLE
-------------------------------------------------------------------------------------
SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'UPDATE STRIPPED_ID_VALUE IN STG_RECORD_IDENT TABLE',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

UPDATE stage.STG_RECORD_IDENT
SET STRIPPED_ID_VALUE = 
       ( SELECT utilities.fn_RegExSplit(stage.STG_RECORD_IDENT.ID_VALUE, '', '\W+') )
----       ( SELECT utilities.fn_CleanString( stage.STG_RECORD_IDENT.ID_VALUE,'','ASC')  )
WHERE STG_RECORD_FK IN 
        ( SELECT STG_RECORD_ID
          FROM stage.STG_RECORD
          WHERE JOB_FK = @IN_JOB_FK )
          
    




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of stage.LOAD_STG_RECORD_IDENT';
     EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
     
      
      EXEC exec_monitor.print_status_sp @exec_id = @l_exec_no;
   END TRY
   BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
END




