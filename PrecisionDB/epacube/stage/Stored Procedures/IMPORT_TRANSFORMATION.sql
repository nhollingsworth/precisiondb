﻿


















-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        10/08/2013   Initial Version 
-- CV        07/17/2015   Add check for large data vs small


CREATE PROCEDURE [stage].[IMPORT_TRANSFORMATION]  
	@IN_JOB_FK bigint, 
	@JobName Varchar(64) = Null

AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to   determine if the import data needs to follow the 
--   product transformation or entity transformation
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure 
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--     
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int 
	 DECLARE @v_import_package_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.IMPORT_TRANSFORMATION', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


--LOOKUP ENTITY CLASS BY PACKAGE
SET @v_import_package_fk = (select distinct import_package_fk 
							from import.import_record_data WITH (NOLOCK)
							where  job_fk = @in_job_fk)

SET @v_entity_class_cr_fk = (select entity_class_cr_fk
						from import.IMPORT_PACKAGE WITH (NOLOCK)
						where import_package_id = @v_import_package_fk)  
						
						
	

--------------------------------------------------------------------------------------
--- NEW TAXONOMY SECTION
---CALL NEW DATA NAME TAX ATTRIBUTE CREATION 
---------------------------------------------------------------------------------------

If @v_import_package_fk  = 193000

BEGIN

EXECUTE  [cleanser].[IMPORT_TAXONOMY_DN_ATTRIBUTE_EVENTS]
   @IN_JOB_FK, @v_import_package_fk
   
  

 END					
					

--------------------------------------------------------------------------------------


--- Check import record data large  count.   If less  than ?
--- move to the import record data and process normal 
--- if greater then run new procedure to split it and run other procedures.
-------------------------------------------------------------------------------------



if 

(Select count(1) from import.import_record_data_large where job_fk = @IN_JOB_FK) >
(select cast(value as int)  from epacube.EPACUBE_PARAMS where name = 'IMPORT FILE SIZE' ) 
 
 
Begin 

EXECUTE [stage].[IMPORT_TRANSFORMATION_LARGE] 
   @IN_JOB_FK

END


--insert into import.IMPORT_RECORD_DATA

if



(Select count(1) from import.import_record_data_large where job_fk = @IN_JOB_FK) <
(select cast(value as int)  from epacube.EPACUBE_PARAMS where name = 'IMPORT FILE SIZE' ) 
 
BEGIN

INSERT INTO [import].[IMPORT_RECORD_DATA]
           ([DO_NOT_IMPORT_IND]
           ,[IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[SEQ_ID]
           ,[JOB_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[RESULT_TYPE]
           ,[SYSTEM_ID_NAME]
           ,[DATA_POSITION1]
           ,[DATA_POSITION2]
           ,[DATA_POSITION3]
           ,[DATA_POSITION4]
           ,[DATA_POSITION5]
           ,[DATA_POSITION6]
           ,[DATA_POSITION7]
           ,[DATA_POSITION8]
           ,[DATA_POSITION9]
           ,[DATA_POSITION10]
           ,[DATA_POSITION11]
           ,[DATA_POSITION12]
           ,[DATA_POSITION13]
           ,[DATA_POSITION14]
           ,[DATA_POSITION15]
           ,[DATA_POSITION16]
           ,[DATA_POSITION17]
           ,[DATA_POSITION18]
           ,[DATA_POSITION19]
           ,[DATA_POSITION20]
           ,[DATA_POSITION21]
           ,[DATA_POSITION22]
           ,[DATA_POSITION23]
           ,[DATA_POSITION24]
           ,[DATA_POSITION25]
           ,[DATA_POSITION26]
           ,[DATA_POSITION27]
           ,[DATA_POSITION28]
           ,[DATA_POSITION29]
           ,[DATA_POSITION30]
           ,[DATA_POSITION31]
           ,[DATA_POSITION32]
           ,[DATA_POSITION33]
           ,[DATA_POSITION34]
           ,[DATA_POSITION35]
           ,[DATA_POSITION36]
           ,[DATA_POSITION37]
           ,[DATA_POSITION38]
           ,[DATA_POSITION39]
           ,[DATA_POSITION40]
           ,[DATA_POSITION41]
           ,[DATA_POSITION42]
           ,[DATA_POSITION43]
           ,[DATA_POSITION44]
           ,[DATA_POSITION45]
           ,[DATA_POSITION46]
           ,[DATA_POSITION47]
           ,[DATA_POSITION48]
           ,[DATA_POSITION49]
           ,[DATA_POSITION50]
           ,[DATA_POSITION51]
           ,[DATA_POSITION52]
           ,[DATA_POSITION53]
           ,[DATA_POSITION54]
           ,[DATA_POSITION55]
           ,[DATA_POSITION56]
           ,[DATA_POSITION57]
           ,[DATA_POSITION58]
           ,[DATA_POSITION59]
           ,[DATA_POSITION60]
           ,[DATA_POSITION61]
           ,[DATA_POSITION62]
           ,[DATA_POSITION63]
           ,[DATA_POSITION64]
           ,[DATA_POSITION65]
           ,[DATA_POSITION66]
           ,[DATA_POSITION67]
           ,[DATA_POSITION68]
           ,[DATA_POSITION69]
           ,[DATA_POSITION70]
           ,[DATA_POSITION71]
           ,[DATA_POSITION72]
           ,[DATA_POSITION73]
           ,[DATA_POSITION74]
           ,[DATA_POSITION75]
           ,[DATA_POSITION76]
           ,[DATA_POSITION77]
           ,[DATA_POSITION78]
           ,[DATA_POSITION79]
           ,[DATA_POSITION80]
           ,[DATA_POSITION81]
           ,[DATA_POSITION82]
           ,[DATA_POSITION83]
           ,[DATA_POSITION84]
           ,[DATA_POSITION85]
           ,[DATA_POSITION86]
           ,[DATA_POSITION87]
           ,[DATA_POSITION88]
           ,[DATA_POSITION89]
           ,[DATA_POSITION90]
           ,[DATA_POSITION91]
           ,[DATA_POSITION92]
           ,[DATA_POSITION93]
           ,[DATA_POSITION94]
           ,[DATA_POSITION95]
           ,[DATA_POSITION96]
           ,[DATA_POSITION97]
           ,[DATA_POSITION98]
           ,[DATA_POSITION99]
           ,[DATA_POSITION100]
           ,[DATA_POSITION101]
           ,[DATA_POSITION102]
           ,[DATA_POSITION103]
           ,[DATA_POSITION104]
           ,[DATA_POSITION105]
           ,[DATA_POSITION106]
           ,[DATA_POSITION107]
           ,[DATA_POSITION108]
           ,[DATA_POSITION109]
           ,[DATA_POSITION110]
           ,[DATA_POSITION111]
           ,[DATA_POSITION112]
           ,[DATA_POSITION113]
           ,[DATA_POSITION114]
           ,[DATA_POSITION115]
           ,[DATA_POSITION116]
           ,[DATA_POSITION117]
           ,[DATA_POSITION118]
           ,[DATA_POSITION119]
           ,[DATA_POSITION120]
           ,[DATA_POSITION121]
           ,[DATA_POSITION122]
           ,[DATA_POSITION123]
           ,[DATA_POSITION124]
           ,[DATA_POSITION125]
           ,[DATA_POSITION126]
           ,[DATA_POSITION127]
           ,[DATA_POSITION128]
           ,[DATA_POSITION129]
           ,[DATA_POSITION130]
           ,[DATA_POSITION131]
           ,[DATA_POSITION132]
           ,[DATA_POSITION133]
           ,[DATA_POSITION134]
           ,[DATA_POSITION135]
           ,[DATA_POSITION136]
           ,[DATA_POSITION137]
           ,[DATA_POSITION138]
           ,[DATA_POSITION139]
           ,[DATA_POSITION140]
           ,[DATA_POSITION141]
           ,[DATA_POSITION142]
           ,[DATA_POSITION143]
           ,[DATA_POSITION144]
           ,[DATA_POSITION145]
           ,[DATA_POSITION146]
           ,[DATA_POSITION147]
           ,[DATA_POSITION148]
           ,[DATA_POSITION149]
           ,[DATA_POSITION150]
           ,[DATA_POSITION151]
           ,[DATA_POSITION152]
           ,[DATA_POSITION153]
           ,[DATA_POSITION154]
           ,[DATA_POSITION155]
           ,[DATA_POSITION156]
           ,[DATA_POSITION157]
           ,[DATA_POSITION158]
           ,[DATA_POSITION159]
           ,[DATA_POSITION160]
           ,[DATA_POSITION161]
           ,[DATA_POSITION162]
           ,[DATA_POSITION163]
           ,[DATA_POSITION164]
           ,[DATA_POSITION165]
           ,[DATA_POSITION166]
           ,[DATA_POSITION167]
           ,[DATA_POSITION168]
           ,[DATA_POSITION169]
           ,[DATA_POSITION170]
           ,[DATA_POSITION171]
           ,[DATA_POSITION172]
           ,[DATA_POSITION173]
           ,[DATA_POSITION174]
           ,[DATA_POSITION175]
           ,[DATA_POSITION176]
           ,[DATA_POSITION177]
           ,[DATA_POSITION178]
           ,[DATA_POSITION179]
           ,[DATA_POSITION180]
           ,[DATA_POSITION181]
           ,[DATA_POSITION182]
           ,[DATA_POSITION183]
           ,[DATA_POSITION184]
           ,[DATA_POSITION185]
           ,[DATA_POSITION186]
           ,[DATA_POSITION187]
           ,[DATA_POSITION188]
           ,[DATA_POSITION189]
           ,[DATA_POSITION190]
           ,[DATA_POSITION191]
           ,[DATA_POSITION192]
           ,[DATA_POSITION193]
           ,[DATA_POSITION194]
           ,[DATA_POSITION195]
           ,[DATA_POSITION196]
           ,[DATA_POSITION197]
           ,[DATA_POSITION198]
           ,[DATA_POSITION199]
           ,[DATA_POSITION200]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
SELECT 
       [DO_NOT_IMPORT_IND]
      ,[IMPORT_FILENAME]
      ,[RECORD_NO]
      ,[SEQ_ID]
      ,@IN_JOB_FK    ----- [JOB_FK]
      ,[IMPORT_PACKAGE_FK]
      ,[EFFECTIVE_DATE]
      ,[END_DATE]
      ,[RESULT_TYPE]
      ,[SYSTEM_ID_NAME]
      ,[DATA_POSITION1]
      ,[DATA_POSITION2]
      ,[DATA_POSITION3]
      ,[DATA_POSITION4]
      ,[DATA_POSITION5]
      ,[DATA_POSITION6]
      ,[DATA_POSITION7]
      ,[DATA_POSITION8]
      ,[DATA_POSITION9]
      ,[DATA_POSITION10]
      ,[DATA_POSITION11]
      ,[DATA_POSITION12]
      ,[DATA_POSITION13]
      ,[DATA_POSITION14]
      ,[DATA_POSITION15]
      ,[DATA_POSITION16]
      ,[DATA_POSITION17]
      ,[DATA_POSITION18]
      ,[DATA_POSITION19]
      ,[DATA_POSITION20]
      ,[DATA_POSITION21]
      ,[DATA_POSITION22]
      ,[DATA_POSITION23]
      ,[DATA_POSITION24]
      ,[DATA_POSITION25]
      ,[DATA_POSITION26]
      ,[DATA_POSITION27]
      ,[DATA_POSITION28]
      ,[DATA_POSITION29]
      ,[DATA_POSITION30]
      ,[DATA_POSITION31]
      ,[DATA_POSITION32]
      ,[DATA_POSITION33]
      ,[DATA_POSITION34]
      ,[DATA_POSITION35]
      ,[DATA_POSITION36]
      ,[DATA_POSITION37]
      ,[DATA_POSITION38]
      ,[DATA_POSITION39]
      ,[DATA_POSITION40]
      ,[DATA_POSITION41]
      ,[DATA_POSITION42]
      ,[DATA_POSITION43]
      ,[DATA_POSITION44]
      ,[DATA_POSITION45]
      ,[DATA_POSITION46]
      ,[DATA_POSITION47]
      ,[DATA_POSITION48]
      ,[DATA_POSITION49]
      ,[DATA_POSITION50]
      ,[DATA_POSITION51]
      ,[DATA_POSITION52]
      ,[DATA_POSITION53]
      ,[DATA_POSITION54]
      ,[DATA_POSITION55]
      ,[DATA_POSITION56]
      ,[DATA_POSITION57]
      ,[DATA_POSITION58]
      ,[DATA_POSITION59]
      ,[DATA_POSITION60]
      ,[DATA_POSITION61]
      ,[DATA_POSITION62]
      ,[DATA_POSITION63]
      ,[DATA_POSITION64]
      ,[DATA_POSITION65]
      ,[DATA_POSITION66]
      ,[DATA_POSITION67]
      ,[DATA_POSITION68]
      ,[DATA_POSITION69]
      ,[DATA_POSITION70]
      ,[DATA_POSITION71]
      ,[DATA_POSITION72]
      ,[DATA_POSITION73]
      ,[DATA_POSITION74]
      ,[DATA_POSITION75]
      ,[DATA_POSITION76]
      ,[DATA_POSITION77]
      ,[DATA_POSITION78]
      ,[DATA_POSITION79]
      ,[DATA_POSITION80]
      ,[DATA_POSITION81]
      ,[DATA_POSITION82]
      ,[DATA_POSITION83]
      ,[DATA_POSITION84]
      ,[DATA_POSITION85]
      ,[DATA_POSITION86]
      ,[DATA_POSITION87]
      ,[DATA_POSITION88]
      ,[DATA_POSITION89]
      ,[DATA_POSITION90]
      ,[DATA_POSITION91]
      ,[DATA_POSITION92]
      ,[DATA_POSITION93]
      ,[DATA_POSITION94]
      ,[DATA_POSITION95]
      ,[DATA_POSITION96]
      ,[DATA_POSITION97]
      ,[DATA_POSITION98]
      ,[DATA_POSITION99]
      ,[DATA_POSITION100]
      ,[DATA_POSITION101]
      ,[DATA_POSITION102]
      ,[DATA_POSITION103]
      ,[DATA_POSITION104]
      ,[DATA_POSITION105]
      ,[DATA_POSITION106]
      ,[DATA_POSITION107]
      ,[DATA_POSITION108]
      ,[DATA_POSITION109]
      ,[DATA_POSITION110]
      ,[DATA_POSITION111]
      ,[DATA_POSITION112]
      ,[DATA_POSITION113]
      ,[DATA_POSITION114]
      ,[DATA_POSITION115]
      ,[DATA_POSITION116]
      ,[DATA_POSITION117]
      ,[DATA_POSITION118]
      ,[DATA_POSITION119]
      ,[DATA_POSITION120]
      ,[DATA_POSITION121]
      ,[DATA_POSITION122]
      ,[DATA_POSITION123]
      ,[DATA_POSITION124]
      ,[DATA_POSITION125]
      ,[DATA_POSITION126]
      ,[DATA_POSITION127]
      ,[DATA_POSITION128]
      ,[DATA_POSITION129]
      ,[DATA_POSITION130]
      ,[DATA_POSITION131]
      ,[DATA_POSITION132]
      ,[DATA_POSITION133]
      ,[DATA_POSITION134]
      ,[DATA_POSITION135]
      ,[DATA_POSITION136]
      ,[DATA_POSITION137]
      ,[DATA_POSITION138]
      ,[DATA_POSITION139]
      ,[DATA_POSITION140]
      ,[DATA_POSITION141]
      ,[DATA_POSITION142]
      ,[DATA_POSITION143]
      ,[DATA_POSITION144]
      ,[DATA_POSITION145]
      ,[DATA_POSITION146]
      ,[DATA_POSITION147]
      ,[DATA_POSITION148]
      ,[DATA_POSITION149]
      ,[DATA_POSITION150]
      ,[DATA_POSITION151]
      ,[DATA_POSITION152]
      ,[DATA_POSITION153]
      ,[DATA_POSITION154]
      ,[DATA_POSITION155]
      ,[DATA_POSITION156]
      ,[DATA_POSITION157]
      ,[DATA_POSITION158]
      ,[DATA_POSITION159]
      ,[DATA_POSITION160]
      ,[DATA_POSITION161]
      ,[DATA_POSITION162]
      ,[DATA_POSITION163]
      ,[DATA_POSITION164]
      ,[DATA_POSITION165]
      ,[DATA_POSITION166]
      ,[DATA_POSITION167]
      ,[DATA_POSITION168]
      ,[DATA_POSITION169]
      ,[DATA_POSITION170]
      ,[DATA_POSITION171]
      ,[DATA_POSITION172]
      ,[DATA_POSITION173]
      ,[DATA_POSITION174]
      ,[DATA_POSITION175]
      ,[DATA_POSITION176]
      ,[DATA_POSITION177]
      ,[DATA_POSITION178]
      ,[DATA_POSITION179]
      ,[DATA_POSITION180]
      ,[DATA_POSITION181]
      ,[DATA_POSITION182]
      ,[DATA_POSITION183]
      ,[DATA_POSITION184]
      ,[DATA_POSITION185]
      ,[DATA_POSITION186]
      ,[DATA_POSITION187]
      ,[DATA_POSITION188]
      ,[DATA_POSITION189]
      ,[DATA_POSITION190]
      ,[DATA_POSITION191]
      ,[DATA_POSITION192]
      ,[DATA_POSITION193]
      ,[DATA_POSITION194]
      ,[DATA_POSITION195]
      ,[DATA_POSITION196]
      ,[DATA_POSITION197]
      ,[DATA_POSITION198]
      ,[DATA_POSITION199]
      ,[DATA_POSITION200]
      ,[UPDATE_TIMESTAMP]
      ,[UPDATE_USER]
  FROM [import].[IMPORT_RECORD_DATA_LARGE]
WHERE  job_fk = @in_job_fk 

END

----------------------------------------------------------------------------------------

SET @v_import_package_fk = (select distinct import_package_fk 
							from import.import_record_data WITH (NOLOCK)
							where  job_fk = @in_job_fk)

SET @v_entity_class_cr_fk = (select entity_class_cr_fk
						from import.IMPORT_PACKAGE WITH (NOLOCK)
						where import_package_id = @v_import_package_fk)  



IF @v_entity_class_cr_fk = 10109
BEGIN 
EXECUTE  [import].[IMPORT_PRODUCT_TRANSFORMATION] 
   @IN_JOB_FK, @JobName


---------------------------------------------------------------------------------------
----   Inactivate Expired results and values according to end date on record
---------------------------------------------------------------------------------------

 -- update epacube.product_values_Z
 -- set RECORD_STATUS_CR_FK = 2
 -- ,UPDATE_TIMESTAMP =  @l_sysdate  
 --where ISNULL(END_DATE,getdate() + 1) <= @l_sysdate

  
 -- update marginmgr.SHEET_RESULTS_Z
 -- set RECORD_STATUS_CR_FK = 2
 -- ,UPDATE_TIMESTAMP =  @l_sysdate  
 -- where ISNULL(END_DATE,getdate() + 1) <= @l_sysdate



END

---------------------------------------------------------------------------------------------
IF @v_entity_class_cr_fk <> 10109
   
   BEGIN

EXECUTE  [import].[ENTITY_TRANSFORMATION] 
   @IN_JOB_FK, @JobName

   END
   


-------------------------------------------------------------------------------------
-- LAST Step is to Set Job Execution to Complete
-------------------------------------------------------------------------------------
----
----

      SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT PRODUCTS TO STAGE' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT PRODUCTS RESOLVED' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------

 SET @status_desc =  'finished execution of stage.IMPORT_TRANSFORMATION'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;

 -- If isnull(@JobName, '') <> ''
	--Begin
	--	Update J
	--	Set Name = @JobName
	--	from common.job J
	--	where Job_ID = @in_job_fk	
	--End
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of stage.IMPORT_TRANSFORMATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
 

END







































































































