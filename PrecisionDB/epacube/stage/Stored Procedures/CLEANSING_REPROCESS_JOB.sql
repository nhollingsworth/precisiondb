﻿










CREATE PROCEDURE [stage].[CLEANSING_REPROCESS_JOB] ( @in_job_fk bigint )
AS
-- Copyright 2006 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott 
--
--
-- Purpose: Reprocess stage data. This procedure is called from the UI.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- KS        09/28/2006   Changed Derived Validation Call to be <> 99 Condition
--                          instead of having any error ( Yellow should go through Derived )
-- KS        10/08/2006   Replaced the DRank by TOP for performance
-- WT        02/22/2007   Added job logging to common.JOB_EXECUTION table (EPA-595).
-- WT        09/28/2007   EPA-1207. Add edit to verify that job id entered by user is valid.
-- 
     
DECLARE 
@l_sysdate datetime,
@ls_stmt varchar(1000),
@l_exec_no bigint,
@v_input_rows bigint,
@v_output_rows bigint,
@v_exception_rows bigint,
@v_input_reprocess bigint,
@v_output_reprocess bigint,
@v_exception_reprocess bigint,
@v_sysdate datetime,
@status_desc varchar(max),     
@v_batch_no bigint,
@ErrorMessage nvarchar(4000),
@ErrorSeverity int,
@ErrorState int

SET @v_sysdate = getdate()
SET @ErrorMessage = NULL
BEGIN
    SET @l_exec_no = 0;
    SET @ls_stmt = 'started execution of stage.CLEANSING_REPROCESS_JOB. '
				 + ' Job_FK: ' + cast(isnull(@in_job_fk,0) as varchar(16))

    EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt
											  ,@exec_id = @l_exec_no OUTPUT;

-- check to make sure job entered
IF @in_job_fk is null
	OR (SELECT count(1) from epacube.common.JOB
		WHERE JOB_ID = @in_job_fk) = 0
BEGIN
	SET @ErrorMessage = 'Invalid Job ID'
    GOTO RAISE_ERROR
END
/* ----------------------------------------------------------------------------------- */
--    Assign Next Batch_no to stg_product_structures in JOB_FK
--       with stage status = UNMATCHED ( 41 ) and DUPL Matched ( 40 )
--       Reset Product_Structurer_fk and  Mfr_entity_structure_fk
--    Call Cleansing Validation then Derive_Stg_Product_Data
/* ----------------------------------------------------------------------------------- */
BEGIN TRY

 		SET @v_batch_no = seq_manager.db_get_next_sequence_value('synchronizer','batch_seq')
  
        UPDATE stage.STG_PRODUCT_STRUCTURE WITH (ROWLOCK)
          SET BATCH_NO = @v_batch_no, 
              PRODUCT_STRUCTURE_FK =  NULL,
              MFR_ENTITY_STRUCTURE_FK = NULL,
              EVENT_CONDITION_CR_FK = 97
        WHERE stg_product_structure_id IN (
          SELECT DISTINCT spsu.stg_product_structure_id
          FROM stage.STG_PRODUCT_STRUCTURE spsu WITH (NOLOCK)
--          LEFT JOIN stage.cleansing_errors ce on ( ce.stg_product_structure_fk = spsu.stg_product_structure_id )
          WHERE spsu.IMPORT_JOB_FK = @IN_JOB_FK
          AND   ( spsu.STAGE_STATUS_CR_FK in ( 40, 41 )    -- DUPL or UNMATCHED
                OR (
                     spsu.STAGE_STATUS_CR_FK in ( 42, 43) -- NEW or MATCHED
                AND  spsu.event_condition_cr_fk <> 97      -- YELLOW or RED CONDITION
                    )
--                  ce.cleansing_errors_id is not null   -- AN ERROR EXISTS
                )
          )


        /* ----------------------------------------------------------------------------------- */
        --    Call Cleansing Validation then Derive_Stg_Product_Data
        /* ----------------------------------------------------------------------------------- */

		SELECT @v_input_rows = COUNT(1)
		  FROM  stage.STG_PRODUCT_STRUCTURE
		  WHERE BATCH_NO = @v_batch_no

        EXEC stage.CLEANSING_VALIDATION @v_batch_no 

        SELECT @v_output_rows = COUNT(1)
          FROM stage.STG_PRODUCT_STRUCTURE
          WHERE import_job_fk = @in_job_fk
	       AND   stage_status_cr_fk <> 41

	    set @v_exception_rows = @v_input_rows - @v_output_rows;
        SET @l_sysdate = getdate()
        exec common.job_execution_create  @in_job_fk, 'REPROCESS COMPLETE',
                                  @v_input_rows, @v_output_rows, @v_exception_rows,
							      201, @v_sysdate, @l_sysdate  ;

SET @status_desc = 'finished execution of stage.CLEANSING_REPROCESS_JOB'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

END TRY
BEGIN CATCH   
		SELECT 
			@ErrorMessage = 'Execution of STAGE.REPROCESS_CLEANSING has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH;
RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END
END



































