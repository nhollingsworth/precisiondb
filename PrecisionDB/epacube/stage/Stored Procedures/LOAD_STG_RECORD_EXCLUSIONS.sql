﻿CREATE PROCEDURE [stage].[LOAD_STG_RECORD_EXCLUSIONS] (
      @in_job_fk       BIGINT
     ,@v_import_package_fk     BIGINT 
   )
AS
BEGIN
      
;with ExclusionData as (
SELECT                  
				   IPE.IMPORT_PACKAGE_EXCLUDE_ID
				   ,ID.JOB_FK
							  ,ID.RECORD_NO
							  ,ID.SEQ_ID
			  FROM import.import_package_exclude ipe WITH (NOLOCK)
			  INNER JOIN import.import_map_metadata imm WITH (NOLOCK)
			    ON ( imm.import_package_fk = ipe.import_package_fk
			    AND  (ipe.exclude_data_name_fk = imm.ident_data_name_fk
				OR ipe.exclude_data_name_fk = imm.data_name_fk ))
				INNER JOIN import.IMPORT_RECORD_DATA ID WITH (NOLOCK)
						  ON ( IPE.IMPORT_PACKAGE_FK = ID.IMPORT_PACKAGE_FK
						  and  cast( dbo.Eval(
					'''' + UPPER(RTRIM(LTRIM([epacube].[getValueFromImportData](id.import_record_data_id, imm.column_name)))) + ''' ' +
					 (select cr.code from epacube.code_ref cr with (nolock)
                   where ipe.operator_cr_fk = cr.code_ref_id
                   and cr.code_type = 'OPERATOR') + 
				    ' ''' + ipe.exclude_value  + '''') as bit) = 1
					)
			  WHERE ipe.import_package_fk = @v_import_package_fk
			  AND   imm.column_name IS NOT NULL 
			   AND IPE.RECORD_STATUS_CR_FK = 1 		
                AND ID.JOB_FK = @in_job_fk	 
)
UPDATE  ird
			SET DO_NOT_IMPORT_IND = 1
			from ExclusionData ed inner join IMPORT.IMPORT_RECORD_DATA ird
			on ird.RECORD_NO = ed.RECORD_NO
			AND ird.SEQ_ID = ed.SEQ_ID
			where ird.JOB_FK = @in_job_fk

                                               
END
