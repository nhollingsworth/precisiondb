﻿






-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
--
-- Purpose: Load all the Import Data into the Stage Tables
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/05/2007   Initial version
-- CV		 02/17/2010   Added Do Not import ind logic						  
-- MN		 02/17/2014	  Stripped down all cursors code
--
CREATE PROCEDURE [stage].[LOAD_STG_RECORD_DATA] (
      @in_job_fk           bigint
     ,@in_import_package_fk int
   )
AS
BEGIN


/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This procedure is called from LOAD_STG_RECORD
--
--   Its purpose is to attempt to match ONLY the Import Entity Data Name with its 
--   epaCUBE Entity Data Name utilizing the available Imported Identifiers 
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   Resolver Activity is ALWAYS SEEDED to be 1 for Products and 2 for Entities
--	 For Resolver Activities ONLY the job_fk is a parameter;  for cleasner activies
--	 it resides on the Search Activity itself.. ( will want to change that in the future )
--
--   In Cleanser a Display direction is setup for each Activity (see @v_from_data_set & @v_to_data_set) 
--   This direction determines how the data is displayed in the UI and also drives how this
--   procedure MUST insert the matching results into the tables for PROPER display.
--   The default direction currently is (FROM) EPACUBE --> (TO) STAGE to accomodate the UI.
--		The UI ONLY WORKS in one direction right now.  (FROM) EPACUBE --> (TO) STAGE.
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Every Entity Structure's Stg_Entity_Identification has been logged and only the rows
--   whose data name is a UNIQUE Entity Identifier have been matched at this time.
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--   Remove any previously inserted [cleanser].[SEARCH_RESULTS_IDENT] by Job_ID 
--		for all UNMATCHED Entity_Structure_FK's
--   If any Stg_Entity_Identification have a NULL entity_structure_fk and its data name is
--   a UNIQUE Entity Identifier then try again to update the entity_structure_fk
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   Insert any missing STG_Entity_Identification Rows Unique and Non Unique matching
--   into [cleanser].[SEARCH_RESULTS_IDENT] for the Stg_Entity_Structure's Entity Data Name Only.
-------------------------------------------------------------------------------------------------
--   PROCESS STEP3: 
--   If for each Stg_Entity_Structure there is ONLY 1 matching entity_structure_fk in 
--   [cleanser].[SEARCH_RESULTS_IDENT] then mark as MATCHED;  if No entity_structure_fk's 
--	 found at all then mark with the ACTIVITY_ACTION_CR_FK default in the package
-------------------------------------------------------------------------------------------------
--   PROCESS STEP4: 
--   Update Stg Entity Structure with the appropriate entity_structure_fk; stage status 
--   and event action
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   Currently ONLY writing the RESOLVER approach and utilizing the current UI flow
--	 which is (FROM) EPACUBE --> (TO) STAGE becasue that is the ONLY direction the UI works with.
--   When we determine we need to allow for more flexibiltiy we will address it then.
--
--   AWAITING DECENT REQUIREMENTS FROM JAN AND COMPANY.... 
--		DO NOT BELIEVE THAT WE SHOULD AUTO MATCH ON NONUNIQE AT ALL DURING RESOLVER.
--
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500)
	 DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME

     DECLARE @v_batch_no          bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.LOAD_STG_RECORD_DATA', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE()
	 SET @V_START_TIMESTAMP = getdate()  -- used in steps


/*
 -- ------------------------------------------------------------------------------------
 --       Insert Identification Rows into stage.STG_RECORD_IDENT
 --			First the Row Data Name Identfications for the Import
 --			Idents driven from the Ident Data Name column 
 --
 -- -----------------------------------------------------------------------------------
 --*/
		  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'Insert Identification Rows into stage.LOAD_STG_RECORD_DATA',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

      ;WITH IdentificationRows AS (
SELECT DISTINCT
				SREC.STG_RECORD_ID
				,vm.data_name_fk
				,dn.NAME DATA_NAME
				,UPPER(RTRIM(LTRIM([epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name)))) DATA_VALUE
				,vm.import_column_name COLUMN_NAME
				,SREC.JOB_FK
				,vm.event_source_cr_fk                                                      
			  FROM import.v_import vm
			  INNER JOIN epacube.data_name dn WITH (NOLOCK)
			    ON ( dn.data_name_id = vm.data_name_fk )
			  INNER JOIN epacube.data_set ds WITH (NOLOCK)
			    ON ( ds.data_set_id = dn.data_set_fk )	
			  INNER JOIN cleanser.SEARCH_JOB_DATA_NAME SADN
			    ON ( sadn.SEARCH_JOB_FK = @in_job_fk
			    AND  sadn.DATA_NAME_FK = dn.data_name_id    )
			  INNER JOIN stage.STG_RECORD SREC  WITH (NOLOCK) 
				ON srec.import_package_fk = vm.import_package_fk
				INNER JOIN IMPORT.IMPORT_RECORD_DATA ID WITH (NOLOCK) 
					 ON ( ID.JOB_FK = SREC.JOB_FK
					 AND  ID.RECORD_NO = SREC.RECORD_NO 
					 AND ISNULL ( ID.DO_NOT_IMPORT_IND, 0 ) <> 1) 
			  WHERE vm.import_package_fk = @in_import_package_fk
			  AND   vm.import_column_name IS NOT NULL 
			  AND [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name) IS NOT NULL
              AND [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name) <> ' '
			  AND SREC.JOB_FK = @in_job_fk
)
INSERT INTO stage.STG_RECORD_DATA
							( STG_RECORD_FK,
							  DATA_NAME_FK,
							  DATA_NAME,
							  DATA_VALUE,
							  COLUMN_NAME,
							  JOB_FK,							  
							  EVENT_SOURCE_CR_FK
							)
select
					STG_RECORD_ID,
							  DATA_NAME_FK,
							  DATA_NAME,
							  DATA_VALUE,
							  COLUMN_NAME,
							  JOB_FK,							  
							  EVENT_SOURCE_CR_FK
FROM IdentificationRows
     

;WITH IdentificationRows1 AS (
SELECT DISTINCT
				SREC.STG_RECORD_ID
				,vm.data_name_fk
				,dn.NAME DATA_NAME
				,UPPER(RTRIM(LTRIM([epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name)))) DATA_VALUE
				,vm.import_column_name COLUMN_NAME
				,SREC.JOB_FK
				,vm.event_source_cr_fk                                                           
			  FROM import.v_import vm
			  INNER JOIN epacube.data_name dn WITH (NOLOCK)
			    ON ( dn.data_name_id = vm.ident_data_name_fk )
			  INNER JOIN epacube.data_set ds WITH (NOLOCK)
			    ON ( ds.data_set_id = dn.data_set_fk )	
			  INNER JOIN cleanser.SEARCH_JOB_DATA_NAME SADN
			    ON ( sadn.SEARCH_JOB_FK = @in_job_fk
			    AND  sadn.DATA_NAME_FK = dn.data_name_id    )
			INNER JOIN stage.STG_RECORD SREC  WITH (NOLOCK)
				ON srec.import_package_fk = vm.import_package_fk 
					INNER JOIN IMPORT.IMPORT_RECORD_DATA ID WITH (NOLOCK) 
					 ON ( ID.JOB_FK = SREC.JOB_FK
					 AND  ID.RECORD_NO = SREC.RECORD_NO
					 AND ISNULL ( ID.DO_NOT_IMPORT_IND, 0 ) <> 1)
			  WHERE vm.import_package_fk = @in_import_package_fk
			  AND   vm.import_column_name IS NOT NULL 
			  AND   ISNULL ( ds.org_ind, 0 ) = 0
			  AND   ISNULL ( ds.ENTITY_STRUCTURE_EC_CR_FK, 0 ) = 0
			  AND [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name) IS NOT NULL
              AND [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name) <> ' '
			  AND SREC.JOB_FK = @in_job_fk
)
INSERT INTO stage.STG_RECORD_DATA
							( STG_RECORD_FK,
							  DATA_NAME_FK,
							  DATA_NAME,
							  DATA_VALUE,
							  COLUMN_NAME,
							  JOB_FK,							  
							  EVENT_SOURCE_CR_FK
							)
select
					STG_RECORD_ID,
							  DATA_NAME_FK,
							  DATA_NAME,
							  DATA_VALUE,
							  COLUMN_NAME,
							  JOB_FK,							  
							  EVENT_SOURCE_CR_FK
FROM IdentificationRows1


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of stage.LOAD_STG_RECORD_DATA';
     EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
     
      
      EXEC exec_monitor.print_status_sp @exec_id = @l_exec_no;
   END TRY
   BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
END




