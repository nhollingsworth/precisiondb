﻿





-- Copyright 2007
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to modify stage identifiers

-- Procedure will be called iteratively for each Stage Record Identifier that is modified.
-- No insertion will be performed, only existing Idents may be updated



-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG       02/12/2009	 Initial Version
-- RG       03/25/2009   EPA-2242 Only update existing values

CREATE PROCEDURE [stage].[EDIT_STAGE_IDENTIFIERS]

    ( @in_stg_record_ident_id BIGINT,      
      @in_stg_record_fk BIGINT,      
      @in_data_name_fk VARCHAR(64),
      @in_data_value VARCHAR(64) ,
      @in_job_fk BIGINT,      
      @in_update_user VARCHAR(64)      
    )
AS /***************  Parameter Defintions  **********************
-- @in_stg_record_data_id BIGINT could be null if addition is needed
-- @in_data_name will update 
***************************************************************/

    BEGIN
        
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT      
		DECLARE @event_source_cr_fk INT

        SET NOCOUNT ON ;
        SET @ErrorMessage = ''
		SET @event_source_cr_fk  = (select code_ref_id 
									from epacube.code_ref
									where code_type = 'EVENT_SOURCE'
									and code = 'USER ENTRY' )
		
        BEGIN TRY 
----------------------------------------------
-- VALIDATIONS
-- #1. Entirety of passed parameters must not be null
----------------------------------------------

----------------------------------------------
--  Must Exist validations
----------------------------------------------
         
         
                    IF @in_stg_record_ident_id IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'Stage Record Ident ID cannot be null'
                                    
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

                    IF @in_data_name_fk IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'Data Name cannot be null'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

					IF @in_data_value IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'Data Value cannot be null'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END
					
					IF @in_update_user IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'User cannot be null'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END
					
					IF @in_stg_record_fk IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'Stage Record FK cannot be null'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

					IF @in_job_fk IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'Job FK cannot be null'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

------------------
-- Must Exist Validations
------------------



--------------------------------------
-- Update stage record ident table with new value, user, timestamp and event_source 
-- based on stg_record_ident, stg_record_fk, id_data_name_fk, and job_id
--------------------------------------

				BEGIN
					UPDATE stage.stg_record_ident
						set  id_value = @in_data_value
							,stripped_id_value = @in_data_value  --cindy added
							,update_user = @in_update_user
							,event_source_cr_fk = @event_source_cr_fk							
							,update_timestamp = getdate()
						where @in_stg_record_ident_id = stg_record_ident_id
						  and @in_stg_record_fk = stg_record_fk
						  and @in_data_name_fk = id_data_name_fk
						  and @in_job_fk = job_fk

--CINDY ADDED SO DATA ON UI IS DISPLAYED CORRECTLY
					UPDATE stage.stg_record_data
						set  data_value = @in_data_value
							,update_user = @in_update_user
							,event_source_cr_fk = @event_source_cr_fk							
							,update_timestamp = getdate()
						where @in_stg_record_fk = stg_record_fk
						  and @in_data_name_fk = data_name_fk
						  and @in_job_fk = job_fk

				END

                 	
        END TRY 


        BEGIN CATCH

            BEGIN
                IF ISNULL(@ErrorMessage, '') = '' 
                    SELECT  @ErrorMessage = ERROR_MESSAGE()
                SET @ErrorSeverity = 16
                SET @ErrorState = 1
                RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState ) 
                RETURN ( -1 )
            END	
        END CATCH 
    END
















































































