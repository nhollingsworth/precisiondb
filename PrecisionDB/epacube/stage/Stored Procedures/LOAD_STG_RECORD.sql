﻿











-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
--
-- Purpose: Load all the Import Data into the Stage Tables
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/05/2007   Initial version
-- CV	     11/12/2009   Added Case statement to select for exclude data name EPA-2665						  
-- CV        12/17/2009   Added to update stg record with parent entity value
-- CV        02/17/2010   Changing Import exclusion logic
-- CV        05/24/2010   Change = to Like for exclusion sql  epa - 2943
-- CV		 03/08/2011	  Add Operator cr fk to exclusion table for like or =
-- CV	     05/29/2011   added the exclude variable to pass in sql
-- CV        06/17/2011   added AND ECT.RECORD_STATUS_CR_FK  = 1 where sub select
-- CV        01/18/2012    Drop exclustion table after curser.
-- CV        04/18/2015   Putting back old way of doing exclusions


CREATE PROCEDURE [stage].[LOAD_STG_RECORD] (
      @in_job_fk       BIGINT
     ,@in_batch_no     BIGINT 
   )
AS
BEGIN


/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to load import rows into the STAGE.STG_RECORD tables(s)
--	 table.  
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure STAGE.STG_LOAD_ENTITIES
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--   Uses the import.V_import View looking for those data elements that have table name of entity
--   structures.  
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @v_sysdate			 DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint;          
     DECLARE @v_exception_rows   bigint;               

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);
	 DECLARE @epa_job_id         bigint;

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int     
     DECLARE @v_import_package_fk    int       
          

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      SET @status_desc  = 'started execution of stage.LOAD_STG_RECORD' + ' Job:: ' + ( CAST  ( ISNULL ( @in_job_fk, 0 )   AS VARCHAR(16) ) )
	  
      EXEC exec_monitor.start_monitor_sp @exec_desc = @status_desc
                                                  ,@epa_job_id = @in_job_fk,  @epa_batch_no = @in_batch_no
												  ,@exec_id = @l_exec_no OUTPUT


     SET @l_sysdate = GETDATE()



/*
 -- ------------------------------------------------------------------------------------
 --       Initialize Variables from TOP Row in Import  ( RECORD_NO = 1 )
 -- -----------------------------------------------------------------------------------
 --*/

-- YN-HERE  CONSTRAINT ON JOB_FK, IMPORT_PACKAGE_FK, ENTITY_CLASS, ENTITY_DATA_NAME, ENTITY_ID_DATA_NAME, ENTITY_ID_VALUE

     SELECT 
            @v_import_package_fk  = ir.import_package_fk
           ,@v_entity_class_cr_fk = ISNULL (( SELECT code_ref_id FROM epacube.CODE_REF WITH (NOLOCK)
											  WHERE  code_type = 'ENTITY_CLASS'
											  AND    code = ir.entity_class  )
										   , 
										   ( SELECT entity_class_cr_fk FROM import.import_package WITH (NOLOCK)
											  WHERE  import_package_id = ir.import_package_fk  ) )
           ,@v_search_activity_fk = ( CASE ISNULL ( IR.SEARCH_ACTIVITY_FK, 0 )
									  WHEN 0 THEN CASE ( ISNULL ( IP.SEARCH_ACTIVITY_FK, 0 ) )
												   WHEN 0 THEN CASE ( ISNULL ( IR.ENTITY_CLASS, 'NULL' ) )
															   WHEN 'NULL' THEN NULL
															   WHEN 'PRODUCT' THEN 1  -- PRODUCT RESOLVER
															   ELSE 2 -- ENTITY RESOLVER
															   END                   
												   ELSE IP.SEARCH_ACTIVITY_FK
												   END								  
									   ELSE IR.SEARCH_ACTIVITY_FK
									   END ) 
     FROM import.IMPORT_RECORD IR WITH (NOLOCK) 
     INNER JOIN import.IMPORT_PACKAGE IP WITH (NOLOCK)
        ON ( IP.IMPORT_PACKAGE_ID = IR.IMPORT_PACKAGE_FK )
     WHERE IR.JOB_FK = @IN_JOB_FK
     AND   IR.RECORD_NO IN 
              ( SELECT TOP 1 RECORD_NO
				 FROM import.IMPORT_RECORD IR WITH (NOLOCK) 
				 WHERE IR.JOB_FK = @IN_JOB_FK )
									   


/*
 -- ------------------------------------------------------------------------------------
 --       Create the cleanser.SEARCH_JOB 
 -- -----------------------------------------------------------------------------------
 --*/


INSERT INTO [cleanser].[SEARCH_JOB]
           ([SEARCH_JOB_ID]
           ,[SEARCH_ACTIVITY_FK]
           ,[NAME]
           ,[IMPORT_BATCH_NO]
           ,[ENTITY_CLASS_CR_FK]
           ,[FROM_DATA_SOURCE_NAME]
           ,[FROM_JOB_FK]
           ,[FROM_FILTER_FK]
           ,[TO_DATA_SOURCE_NAME]
           ,[TO_JOB_FK]
           ,[TO_FILTER_FK]
           ,[THREAD_COUNT]
           ,[TOP_N_ROWS]
           ,[ASSOC_DATA_NAME_FK]
           ,[DNA_CONFIDENCE_WEIGHT]
           ,[ID_CONFIDENCE_WEIGHT]
           ,[SEARCH1_CONFIDENCE_WEIGHT]
           ,[SEARCH2_CONFINDENCE_WEIGHT]
           ,[SEARCH3_CONFIDENCE_WEIGHT])
SELECT @IN_JOB_FK
      ,SACT.SEARCH_ACTIVITY_ID 
      ,SACT.NAME 
      ,@IN_BATCH_NO
      ,@v_entity_class_cr_fk
      ,SACT.FROM_DATA_SOURCE_NAME 
      ,SACT.FROM_JOB_FK 
      ,SACT.FROM_FILTER_FK 
      ,SACT.TO_DATA_SOURCE_NAME 
      ,@IN_JOB_FK   
      ,SACT.TO_FILTER_FK 
      ,SACT.THREAD_COUNT 
      ,SACT.TOP_N_ROWS 
      ,( SELECT IP.MISSING_ASSOC_DATA_NAME_FK
         FROM import.IMPORT_PACKAGE IP WITH (NOLOCK)
         WHERE IP.IMPORT_PACKAGE_ID = @v_import_package_fk  )
      ,SACT.DNA_CONFIDENCE_WEIGHT 
      ,SACT.ID_CONFIDENCE_WEIGHT 
      ,SACT.SEARCH1_CONFIDENCE_WEIGHT 
      ,SACT.SEARCH2_CONFINDENCE_WEIGHT 
      ,SACT.SEARCH3_CONFIDENCE_WEIGHT 
  FROM [cleanser].[SEARCH_ACTIVITY] SACT WITH (NOLOCK)    
  WHERE SACT.SEARCH_ACTIVITY_ID = @v_search_activity_fk


INSERT INTO [cleanser].[SEARCH_JOB_DATA_NAME]
           ([SEARCH_JOB_FK]
           ,[DATA_NAME_FK]
           ,[DATA_NAME_SEQ])
SELECT @IN_JOB_FK
      ,[DATA_NAME_FK]
      ,[DATA_NAME_SEQ]
  FROM [cleanser].[SEARCH_ACTIVITY_DATA_NAME] WITH (NOLOCK)
  WHERE SEARCH_ACTIVITY_FK = @v_search_activity_fk



------------------------------------------------------------------------------------------------------------
--   UPDATE [cleanser].[SEARCH_JOB_DATA_NAME] SEARCH DESCRIPTION BASED ON PACKAGE
--------------------------------------------------------------------------------------------------------------

UPDATE [cleanser].[SEARCH_JOB_DATA_NAME]
SET DATA_NAME_FK = ( SELECT CASE ISNULL (IP.SEARCH_DESC_DN_FK, 0 )
							 WHEN 0 THEN DATA_NAME_FK 
							 ELSE IP.SEARCH_DESC_DN_FK
							 END 
					 FROM import.IMPORT_PACKAGE IP WITH (NOLOCK)
					 WHERE 1 =1
					 AND   IP.IMPORT_PACKAGE_ID = @v_import_package_fk  )
WHERE 1 = 1 					 
AND   SEARCH_JOB_FK = @IN_JOB_FK
AND   DATA_NAME_SEQ = 1  --- DESCRIPTION


------------------------------------------------------------------------------------------------------------
--   UPDATE [cleanser].[SEARCH_JOB_DATA_NAME] SEARCH PRODUCT BASED ON PACKAGE
--------------------------------------------------------------------------------------------------------------

UPDATE [cleanser].[SEARCH_JOB_DATA_NAME]
SET DATA_NAME_FK = ( SELECT CASE ISNULL (IP.SEARCH_PROD_DN_FK, 0 )
							 WHEN 0 THEN DATA_NAME_FK 
							 ELSE IP.SEARCH_PROD_DN_FK
							 END 
					 FROM import.IMPORT_PACKAGE IP WITH (NOLOCK)
					 WHERE 1 =1
					 AND   IP.IMPORT_PACKAGE_ID = @v_import_package_fk  )
WHERE 1 = 1 					 
AND   SEARCH_JOB_FK = @IN_JOB_FK
AND   DATA_NAME_SEQ = 2  --- PRODUCT ID


------------------------------------------------------------------------------------------------------------
--   IMPORT EXCLUSIONS
--------------------------------------------------------------------------------------------------------------

-- YN-HERE  CONSTRAINT FK EXLUDE_DATA_NAME_FK IN IMPORT_MAP_METADATA

--drop temp tables if exists
IF  EXISTS (SELECT * FROM tempdb.sys.objects WHERE NAME = '#TS_IMPORT_EXCLUDE' AND type in (N'U'))
   DROP TABLE dbo.#TS_IMPORT_EXCLUDE

-- create temporary tables and indexes


CREATE TABLE #TS_IMPORT_EXCLUDE  (
	[TS_IMPORT_EXCLUDE_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[IMPORT_PACKAGE_EXCLUDE_FK] [int],
	[JOB_FK] [bigint],		
	[RECORD_NO] [bigint],
    [SEQ_ID] [bigint]
)

 -- ------------------------------------------------------------------------------------
 --       Create Indexes
 -- -----------------------------------------------------------------------------------

CREATE NONCLUSTERED INDEX IDX_TSIE_RNO ON #TS_IMPORT_EXCLUDE
(RECORD_NO ASC )
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


CREATE CLUSTERED INDEX IDX_TSIE_TSIEID ON #TS_IMPORT_EXCLUDE
(TS_IMPORT_EXCLUDE_ID ASC)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


/*
 -- ------------------------------------------------------------------------------------
 --       Insert Rows into #TS_IMPORT_EXCLUDE than are to be EXCLUDED
 -- -----------------------------------------------------------------------------------
 --*/


      DECLARE 
              @vm$exclude_data_name         varchar(64), 
			  @vm$exclude_column_name       varchar(16),               
              @vm$exclude_column_value      varchar(64),
              @vm$exclude_operator_cr_fk    varchar(64),
              @vm$exclude_value				varchar(64)
                                       

      DECLARE  cur_v_import cursor local FOR
			 SELECT DISTINCT (ISNULL (
					(SELECT NAME FROM epacube.DATA_NAME WITH (NOLOCK) 
                               WHERE data_name_id = imm.ident_data_name_fk ),
			        ( SELECT NAME FROM epacube.DATA_NAME WITH (NOLOCK) 
                               WHERE data_name_id = imm.data_name_fk )))
			       ,imm.column_name
			       ,'UPPER(RTRIM(LTRIM( ID.' + imm.column_name + ' )))' 
                   ,(select cr.code from epacube.code_ref cr with (nolock)
                   where ipe.operator_cr_fk = cr.code_ref_id
                   and cr.code_type = 'OPERATOR')
                   ,ipe.exclude_value 
			  FROM import.import_package_exclude ipe WITH (NOLOCK)
			  INNER JOIN import.import_map_metadata imm WITH (NOLOCK)
			    ON ( imm.import_package_fk = ipe.import_package_fk
			    AND  (ipe.exclude_data_name_fk = imm.ident_data_name_fk
				OR ipe.exclude_data_name_fk = imm.data_name_fk ))
			  WHERE ipe.import_package_fk = @v_import_package_fk
			  AND   imm.column_name IS NOT NULL 
			  		  
      
        OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO 
                                          @vm$exclude_data_name
                                         ,@vm$exclude_column_name
                                         ,@vm$exclude_column_value
                                         ,@vm$exclude_operator_cr_fk
                                         ,@vm$exclude_value                                         

         WHILE @@FETCH_STATUS = 0 
         BEGIN
            SET @status_desc =  'stage.LOAD_STG_RECORD: Exclusions - '
                                + @vm$exclude_data_name
                                + ' '
                                + @vm$exclude_column_name

            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;


			TRUNCATE TABLE #TS_IMPORT_EXCLUDE



            SET @ls_exec =
						'INSERT INTO #TS_IMPORT_EXCLUDE
							( IMPORT_PACKAGE_EXCLUDE_FK,
							  JOB_FK,							  
							  RECORD_NO,
							  SEQ_ID
							)
						SELECT IPE.IMPORT_PACKAGE_EXCLUDE_ID
							  ,ID.JOB_FK
							  ,ID.RECORD_NO
							  ,ID.SEQ_ID
						FROM import.IMPORT_PACKAGE_EXCLUDE IPE WITH (NOLOCK)
						INNER JOIN import.IMPORT_RECORD_DATA ID WITH (NOLOCK)
						  ON ( IPE.IMPORT_PACKAGE_FK = ID.IMPORT_PACKAGE_FK AND '
						  --						   
					   + @vm$exclude_column_value
					   + ' '
					   + @vm$exclude_operator_cr_fk
					   + ' ( '''
					   + @vm$exclude_value
					   + ''' )) '
--
               + ' AND IPE.RECORD_STATUS_CR_FK = 1 '		
               + ' AND ISNULL(ID.JOB_FK,-999) = '
               + cast(isnull (@in_job_fk, -999) as varchar(20))



----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), @ls_exec )

         exec sp_executesql 	@ls_exec;


-------------------------------------------------------------------------
--update and flag for not importing
--------------------------------------------------------------------------

            UPDATE IMPORT.IMPORT_RECORD_DATA
			SET DO_NOT_IMPORT_IND = 1
			from #TS_IMPORT_EXCLUDE
			WHERE IMPORT.IMPORT_RECORD_DATA.RECORD_NO = #TS_IMPORT_EXCLUDE.RECORD_NO
			AND IMPORT.IMPORT_RECORD_DATA.SEQ_ID = #TS_IMPORT_EXCLUDE.SEQ_ID
			AND IMPORT.IMPORT_RECORD_DATA.JOB_FK = @in_job_fk



        FETCH NEXT FROM cur_v_import INTO 
                                          @vm$exclude_data_name
                                         ,@vm$exclude_column_name
                                         ,@vm$exclude_column_value
                                         ,@vm$exclude_operator_cr_fk 
                                         ,@vm$exclude_value                                        


     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 
     



IF  EXISTS (SELECT * FROM tempdb.sys.objects WHERE NAME = '#TS_IMPORT_EXCLUDE' AND type in (N'U'))
   DROP TABLE dbo.#TS_IMPORT_EXCLUDE


------------------------------------------------------------------------------------------------------------
--   lOG into JOB_EXECUTION the number of rows Excluded in Import
--------------------------------------------------------------------------------------------------------------

---    CINDY COMMENTING OUT ALL JOB LOGGING FOR NOW... WILL LET YOU DO THIS

----
----    SET @v_input_rows =	(   SELECT COUNT(1)
----							FROM IMPORT.IMPORT_RECORD
----							WHERE JOB_FK = @IN_JOB_FK  )
----                           
----    SET @v_exception_rows = (	SELECT COUNT(1)
----								FROM #TS_IMPORT_EXCLUDE )
----
----    SET @v_output_rows = @v_input_rows - @v_exception_rows
----
----
----    EXEC common.job_execution_create   @in_job_fk, 'IMPORT EXCLUSIONS',
----		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
----										 200, @l_sysdate, @l_sysdate


------------------------------------------------------------------------------------------------------------
--   INSERT INTO STG_RECORD
--------------------------------------------------------------------------------------------------------------



INSERT INTO [stage].[STG_RECORD]
           (
            [IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[IMPORT_PACKAGE_FK]
           ,[JOB_FK]                     
           ,[BATCH_NO]
           ,[IMPORT_BATCH_NO]
		   ,[EFFECTIVE_DATE]
		   ,[END_DATE]           
           ,[ENTITY_CLASS]
           ,[ENTITY_CLASS_CR_FK]           
           ,[ENTITY_DATA_NAME]
           ,[ENTITY_DATA_NAME_FK]           
           ,[ENTITY_ID_COLUMN_NAME]
           ,[ENTITY_ID_DATA_NAME]           
           ,[ENTITY_ID_DATA_NAME_FK]
           ,[ENTITY_ID_VALUE]           
           ,[LEVEL_SEQ]
           ,[PARENT_DATA_NAME_FK]           
           ,[PARENT_ID_COLUMN_NAME]
           ,[PARENT_ID_DATA_NAME_FK] 
,[PARENT_ID_VALUE]          
		   ,[ENTITY_STATUS]
		   ,[PRIORITY]
		   ,[EVENT_PRIORITY_CR_FK]
		   ,[EVENT_SOURCE_CR_FK]		   
		   ,[EVENT_CONDITION_CR_FK]		   
		   ,[ACTIVITY_ACTION_CR_FK]		   		   
		   ,[SEARCH_ACTIVITY_FK]	
		   ,[RESULT_TYPE_CR_FK]	   		   		   
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_USER])
SELECT A.IMPORT_FILENAME
      ,A.RECORD_NO
      ,A.IMPORT_PACKAGE_FK      
      ,A.JOB_FK
      ,@IN_BATCH_NO
      ,@IN_BATCH_NO      
      ,A.EFFECTIVE_DATE
      ,A.END_DATE
      ,A.ENTITY_CLASS
	  ,@V_ENTITY_CLASS_CR_FK         
      ,A.ENTITY_DATA_NAME
	  ,A.ENTITY_DATA_NAME_FK
	  ,( SELECT IMM.COLUMN_NAME
          FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
          WHERE IMM.IMPORT_PACKAGE_FK = A.IMPORT_PACKAGE_FK
          AND   IMM.DATA_NAME_FK = A.ENTITY_DATA_NAME_FK 
          AND   IMM.UNIQUE_SEQ_IND =1) AS ENTITY_ID_COLUMN_NAME   --- added for entity_creatation
      ,A.ENTITY_ID_DATA_NAME
      ,A.ENTITY_ID_DATA_NAME_FK
      ,A.ENTITY_ID_VALUE
	  ,CASE ( ISNULL ( A.ENTITY_CLASS, 'NULL' ) )
       WHEN 'NULL' THEN NULL
       WHEN 'PRODUCT' THEN 1  -- PRODUCT LEVEL SEQ IS ALWAYS SET TO 1 FOR NOW
       ELSE ( SELECT LEVEL_SEQ
              FROM epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
              WHERE ECT.ENTITY_CLASS_CR_FK = @V_ENTITY_CLASS_CR_FK
              AND   ECT.DATA_NAME_FK = A.ENTITY_DATA_NAME_FK 
              AND ECT.RECORD_STATUS_CR_FK  = 1)
       END AS LEVEL_SEQ
	  ,CASE ( ISNULL ( A.ENTITY_CLASS, 'NULL' ) )
       WHEN 'NULL' THEN NULL
       WHEN 'PRODUCT' THEN NULL -- PRODUCT PARENT IS NULL 
       ELSE ( SELECT PARENT_DATA_NAME_FK
              FROM epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
              WHERE ECT.ENTITY_CLASS_CR_FK = @V_ENTITY_CLASS_CR_FK
              AND   ECT.DATA_NAME_FK = A.ENTITY_DATA_NAME_FK 
              AND   ECT.LEVEL_SEQ <> 1 
              AND ECT.RECORD_STATUS_CR_FK  = 1)
       END AS PARENT_DATA_NAME_FK
	  ,CASE ( ISNULL ( A.ENTITY_CLASS, 'NULL' ) )
       WHEN 'NULL' THEN NULL
       WHEN 'PRODUCT' THEN NULL -- PRODUCT PARENT IS NULL 
       ELSE ( SELECT IMM.COLUMN_NAME
              FROM epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
              INNER JOIN IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
                 ON ( IMM.IMPORT_PACKAGE_FK = A.IMPORT_PACKAGE_FK
                 AND  IMM.DATA_NAME_FK = ECT.PARENT_DATA_NAME_FK )	
              WHERE ECT.ENTITY_CLASS_CR_FK = @V_ENTITY_CLASS_CR_FK
              AND   ECT.DATA_NAME_FK = A.ENTITY_DATA_NAME_FK
              AND   ECT.LEVEL_SEQ <> 1 
              AND ECT.RECORD_STATUS_CR_FK  = 1)
       END AS PARENT_IDENT_COLUMN_NAME
	  ,CASE ( ISNULL ( A.ENTITY_CLASS, 'NULL' ) )
       WHEN 'NULL' THEN NULL
       WHEN 'PRODUCT' THEN NULL -- PRODUCT PARENT IS NULL 
       ELSE ( SELECT IMM.IDENT_DATA_NAME_FK
              FROM epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
              INNER JOIN IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
                 ON ( IMM.IMPORT_PACKAGE_FK = A.IMPORT_PACKAGE_FK
                 AND  IMM.DATA_NAME_FK = ECT.PARENT_DATA_NAME_FK )	
              WHERE ECT.ENTITY_CLASS_CR_FK = @V_ENTITY_CLASS_CR_FK
              AND   ECT.DATA_NAME_FK = A.ENTITY_DATA_NAME_FK 
              AND   ECT.LEVEL_SEQ <> 1 
			  AND ECT.RECORD_STATUS_CR_FK  = 1)
       END AS PARENT_ID_DATA_NAME_FK	
,NULL -- PARENT ID VALUE	 
      ,A.ENTITY_STATUS
      ,A.PRIORITY
      ,A.EVENT_PRIORITY_CR_FK
      ,A.EVENT_SOURCE_CR_FK
      ,97  -- GREEN
      ,140 -- UNRESOLVED
      ,@V_SEARCH_ACTIVITY_FK 
      ,A.RESULT_TYPE_CR_FK
      ,@L_SYSDATE
      ,A.UPDATE_USER
FROM (                     
SELECT IR.IMPORT_FILENAME
      ,IR.RECORD_NO
      ,IR.IMPORT_PACKAGE_FK      
      ,IR.JOB_FK
      ,IR.EFFECTIVE_DATE
      ,IR.END_DATE
      ,IR.ENTITY_CLASS
      ,IR.ENTITY_DATA_NAME
	  ,( SELECT DATA_NAME_ID
		 FROM EPACUBE.DATA_NAME WITH (NOLOCK)
		 WHERE NAME = IR.ENTITY_DATA_NAME  )  AS ENTITY_DATA_NAME_FK
	  ,IR.ENTITY_ID_DATA_NAME
	  ,( SELECT DN.DATA_NAME_ID
          FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)
          WHERE DN.NAME = IR.ENTITY_ID_DATA_NAME ) AS ENTITY_ID_DATA_NAME_FK 
	  ,IR.ENTITY_ID_VALUE
      ,IR.ENTITY_STATUS
      ,IR.PRIORITY
      ,( SELECT ISNULL (
			  ( SELECT CODE_REF_ID
				 FROM EPACUBE.CODE_REF WITH (NOLOCK)
				 WHERE CODE_TYPE = 'EVENT_PRIORITY' AND CODE = ISNULL ( IR.PRIORITY, 'NULL' ) ) 
			 , 61  )
		) AS EVENT_PRIORITY_CR_FK
      ,IP.EVENT_SOURCE_CR_FK
--- KS HARDCODE FOR NOW RESULT TYPE      
      ,CASE WHEN ISNULL ( IR.PRIORITY, 'NULL_DATA' ) = 'WIF' THEN 712
       ELSE 711  -- FUTURE
       END AS RESULT_TYPE_CR_FK
      ,ISNULL ( IR.UPDATE_USER, 'EPACUBE' ) AS UPDATE_USER
  FROM import.IMPORT_RECORD IR WITH (NOLOCK)
  INNER JOIN IMPORT.IMPORT_PACKAGE IP WITH (NOLOCK)
     ON ( IP.IMPORT_PACKAGE_ID = IR.IMPORT_PACKAGE_FK )
  WHERE IR.JOB_FK = @IN_JOB_FK
  AND   IR.RECORD_NO IN (
        SELECT DISTINCT IRD.RECORD_NO
  		FROM IMPORT.IMPORT_RECORD_DATA IRD WITH (NOLOCK)
		WHERE  IRD.JOB_FK = IR.JOB_FK
		AND	   ISNULL ( IRD.DO_NOT_IMPORT_IND, 0 ) <> 1 )
----
 ) A



------SELECT DISTINCT A.IMPORT_FILENAME
------      ,A.RECORD_NO
------      ,A.IMPORT_PACKAGE_FK      
------      ,A.JOB_FK
------      ,@IN_BATCH_NO
------      ,@IN_BATCH_NO      
------      ,A.EFFECTIVE_DATE
------      ,A.END_DATE
------      ,A.ENTITY_CLASS
------	  ,@V_ENTITY_CLASS_CR_FK         
------      ,A.ENTITY_DATA_NAME
------	  ,A.ENTITY_DATA_NAME_FK
------	  ,( SELECT IMM.COLUMN_NAME
------          FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
------          WHERE IMM.IMPORT_PACKAGE_FK = A.IMPORT_PACKAGE_FK
------          AND   IMM.DATA_NAME_FK = A.ENTITY_DATA_NAME_FK ) AS ENTITY_ID_COLUMN_NAME
------      ,A.ENTITY_ID_DATA_NAME
------      ,A.ENTITY_ID_DATA_NAME_FK
------      ,A.ENTITY_ID_VALUE
------	  ,CASE ( ISNULL ( A.ENTITY_CLASS, 'NULL' ) )
------       WHEN 'NULL' THEN NULL
------       WHEN 'PRODUCT' THEN 1  -- PRODUCT LEVEL SEQ IS ALWAYS SET TO 1 FOR NOW
------       ELSE ( SELECT LEVEL_SEQ
------              FROM epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
------              WHERE ECT.ENTITY_CLASS_CR_FK = @V_ENTITY_CLASS_CR_FK
------              AND   ECT.DATA_NAME_FK = A.ENTITY_DATA_NAME_FK )
------       END AS LEVEL_SEQ
------	  ,CASE ( ISNULL ( A.ENTITY_CLASS, 'NULL' ) )
------       WHEN 'NULL' THEN NULL
------       WHEN 'PRODUCT' THEN NULL -- PRODUCT PARENT IS NULL 
------       ELSE ( SELECT PARENT_DATA_NAME_FK
------              FROM epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
------              WHERE ECT.ENTITY_CLASS_CR_FK = @V_ENTITY_CLASS_CR_FK
------              AND   ECT.DATA_NAME_FK = A.ENTITY_DATA_NAME_FK 
------              AND   ECT.LEVEL_SEQ <> 1 )
------       END AS PARENT_DATA_NAME_FK
------	  ,CASE ( ISNULL ( A.ENTITY_CLASS, 'NULL' ) )
------       WHEN 'NULL' THEN NULL
------       WHEN 'PRODUCT' THEN NULL -- PRODUCT PARENT IS NULL 
------       ELSE ( SELECT IMM.COLUMN_NAME
------              FROM epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
------              INNER JOIN IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
------                 ON ( IMM.IMPORT_PACKAGE_FK = A.IMPORT_PACKAGE_FK
------                 AND  IMM.DATA_NAME_FK = ECT.PARENT_DATA_NAME_FK )	
------              WHERE ECT.ENTITY_CLASS_CR_FK = @V_ENTITY_CLASS_CR_FK
------              AND   ECT.DATA_NAME_FK = A.ENTITY_DATA_NAME_FK
------              AND   ECT.LEVEL_SEQ <> 1 )
------       END AS PARENT_IDENT_COLUMN_NAME
------	  ,CASE ( ISNULL ( A.ENTITY_CLASS, 'NULL' ) )
------       WHEN 'NULL' THEN NULL
------       WHEN 'PRODUCT' THEN NULL -- PRODUCT PARENT IS NULL 
------       ELSE ( SELECT IMM.IDENT_DATA_NAME_FK
------              FROM epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
------              INNER JOIN IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
------                 ON ( IMM.IMPORT_PACKAGE_FK = A.IMPORT_PACKAGE_FK
------                 AND  IMM.DATA_NAME_FK = ECT.PARENT_DATA_NAME_FK )	
------              WHERE ECT.ENTITY_CLASS_CR_FK = @V_ENTITY_CLASS_CR_FK
------              AND   ECT.DATA_NAME_FK = A.ENTITY_DATA_NAME_FK 
------              AND   ECT.LEVEL_SEQ <> 1 
------			  AND ECT.RECORD_STATUS_CR_FK  = 1)
------       END AS PARENT_ID_DATA_NAME_FK	
------,NULL -- PARENT ID VALUE	 
------      ,A.ENTITY_STATUS
------      ,A.PRIORITY
------      ,A.EVENT_PRIORITY_CR_FK
------      ,A.EVENT_SOURCE_CR_FK
------      ,97  -- GREEN
------      ,140 -- UNRESOLVED
------      ,@V_SEARCH_ACTIVITY_FK 
------      ,A.RESULT_TYPE_CR_FK
------      ,@L_SYSDATE
------      ,A.UPDATE_USER
------FROM (                     
------SELECT IR.IMPORT_FILENAME
------      ,IR.RECORD_NO
------      ,IR.IMPORT_PACKAGE_FK      
------      ,IR.JOB_FK
------      ,IR.EFFECTIVE_DATE
------      ,IR.END_DATE
------      ,IR.ENTITY_CLASS
------      ,IR.ENTITY_DATA_NAME
------	  ,( SELECT DATA_NAME_ID
------		 FROM EPACUBE.DATA_NAME WITH (NOLOCK)
------		 WHERE NAME = IR.ENTITY_DATA_NAME  )  AS ENTITY_DATA_NAME_FK
------	  ,IR.ENTITY_ID_DATA_NAME
------	  ,( SELECT DN.DATA_NAME_ID
------          FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)
------          WHERE DN.NAME = IR.ENTITY_ID_DATA_NAME ) AS ENTITY_ID_DATA_NAME_FK 
------	  ,IR.ENTITY_ID_VALUE
------      ,IR.ENTITY_STATUS
------      ,IR.PRIORITY
------      ,( SELECT ISNULL (
------			  ( SELECT CODE_REF_ID
------				 FROM EPACUBE.CODE_REF WITH (NOLOCK)
------				 WHERE CODE_TYPE = 'EVENT_PRIORITY' AND CODE = ISNULL ( IR.PRIORITY, 'NULL' ) ) 
------			 , 61  )
------		) AS EVENT_PRIORITY_CR_FK
------      ,IP.EVENT_SOURCE_CR_FK
--------- KS HARDCODE FOR NOW RESULT TYPE      
------      ,CASE WHEN ISNULL ( IR.PRIORITY, 'NULL_DATA' ) = 'WIF' THEN 712
------       ELSE 711  -- FUTURE
------       END AS RESULT_TYPE_CR_FK
------      ,ISNULL ( IR.UPDATE_USER, 'EPACUBE' ) AS UPDATE_USER
------  FROM import.IMPORT_RECORD IR WITH (NOLOCK)
------		INNER JOIN IMPORT.IMPORT_RECORD_DATA IRD WITH (NOLOCK)
------		ON (IRD.RECORD_NO = IR.RECORD_NO
------		AND IRD.JOB_FK = IR.JOB_FK
------		AND	ISNULL ( IRD.DO_NOT_IMPORT_IND, 0 ) <> 1 )
------  INNER JOIN IMPORT.IMPORT_PACKAGE IP WITH (NOLOCK)
------     ON ( IP.IMPORT_PACKAGE_ID = IR.IMPORT_PACKAGE_FK )
------  WHERE IR.JOB_FK = @IN_JOB_FK
------ ) A


       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows INSERTED into the STAGE.STG_RECORD table = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk
       END



--------------------------------------------------------------------------------
---UPDATE STG RECORD WITH PARENT ENTITY VALUE
----------------------------------------------------------------------------

declare @v_column_name varchar(50)
declare @v_column_name2 varchar(50)


set @v_column_name = (select distinct parent_id_column_name from stage.stg_record sr 
where parent_id_column_name is not null
and sr.job_fk = @in_job_fk )

set @v_column_name2 = (select distinct entity_id_column_name from stage.stg_record sr 
where parent_id_column_name is not null
and sr.job_fk = @in_job_fk )

	SET @ls_exec =
						'UPDATE STAGE.STG_RECORD
						set PARENT_ID_VALUE = import.import_record_data.'
						+ @v_column_name 
						+ ' 
						from import.import_record_data WITH (NOLOCK)
						where'
						+ ' '
						+ @v_column_name2 
						+ ' = entity_id_value
						and import.import_record_data.JOB_FK = '
						+ cast(isnull (@in_job_fk , -999) as varchar(20))
						+ ' and stage.stg_record.JOB_FK = '
						+ cast(isnull (@in_job_fk , -999) as varchar(20))


----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), @ls_exec )

         exec sp_executesql 	@ls_exec;


---------------------------------------------------------------------------------------
--   LOAD ALL THE ENTITY STRUCTURES ON THE ROW FOR EACH STG_RECORD
---------------------------------------------------------------------------------------

	  EXEC stage.LOAD_STG_RECORD_ENTITY @IN_JOB_FK

----      SELECT @v_output_rows = COUNT(1)
----        FROM stage.STG_ENTITY_IDENTIFICATION WITH (NOLOCK)
----       WHERE import_job_fk = @in_job_fk
----
----	  set @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @l_sysdate = getdate()
----      exec common.job_execution_create  @in_job_fk, 'ENTITY IDENTIFICATION',
----                                @v_input_rows, @v_output_rows, @v_exception_rows,
----							 201, @v_sysdate, @l_sysdate  ;
----	  set @v_sysdate = getdate();


---------------------------------------------------------------------------------------
--   LOAD ALL THE IDENTIFIERS ON THE ROW FOR EACH STG_RECORD
---------------------------------------------------------------------------------------

	  EXEC stage.LOAD_STG_RECORD_IDENT @IN_JOB_FK, @V_IMPORT_PACKAGE_FK

----      SELECT @v_output_rows = COUNT(1)
----        FROM stage.STG_ENTITY_IDENTIFICATION WITH (NOLOCK)
----       WHERE import_job_fk = @in_job_fk
----
----	  set @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @l_sysdate = getdate()
----      exec common.job_execution_create  @in_job_fk, 'ENTITY IDENTIFICATION',
----                                @v_input_rows, @v_output_rows, @v_exception_rows,
----							 201, @v_sysdate, @l_sysdate  ;
----	  set @v_sysdate = getdate();

--UPDATE PARENT ENTITY STRUCTURE
update stage.stg_record
set parent_structure_fk = stage.STG_RECORD_entity_ident.entity_Structure_fk
from stage.stg_record_entity_ident
Where stage.STG_RECORD_entity_ident.entity_id_value = stage.STG_RECORD.parent_id_value
AND stage.STG_RECORD_entity_ident.stg_record_fk = stage.STG_RECORD.stg_record_id
AND stage.STG_RECORD.job_fk = @in_job_fk

---------------------------------------------------------------------------------------
--   LOAD ALL THE SEARCH ACTIVITY DATA NAMES ON THE ROW FOR EACH STG_RECORD
---------------------------------------------------------------------------------------

	  EXEC stage.LOAD_STG_RECORD_DATA @IN_JOB_FK, @V_IMPORT_PACKAGE_FK

----      SELECT @v_output_rows = COUNT(1)
----        FROM stage.STG_ENTITY_IDENTIFICATION WITH (NOLOCK)
----       WHERE import_job_fk = @in_job_fk
----
----	  set @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @l_sysdate = getdate()
----      exec common.job_execution_create  @in_job_fk, 'ENTITY IDENTIFICATION',
----                                @v_input_rows, @v_output_rows, @v_exception_rows,
----							 201, @v_sysdate, @l_sysdate  ;
----	  set @v_sysdate = getdate();



---------------------------------------------------------------------------------------
----   PERFORM DATA CLEANSER MATCHING BY JOB_FK
---------------------------------------------------------------------------------------


      EXEC cleanser.SEARCH_RESULT_MATCH_ACTIVITY    @IN_JOB_FK,  @IN_BATCH_NO, @V_SEARCH_ACTIVITY_FK

----	  set @v_sysdate = getdate();
----      set @v_input_rows = @v_output_rows;
----      SELECT @v_output_rows = COUNT(1)
----        FROM stage.STG_ENTITY_STRUCTURE WITH (NOLOCK)
----       WHERE import_job_fk = @in_job_fk
----	   AND   ACTIVITY_ACTION_CR_FK <> 140
----
----	  set @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @l_sysdate = getdate()
----      exec common.job_execution_create  @in_job_fk, 'ENTITY MATCHING',
----                                @v_input_rows, @v_output_rows, @v_exception_rows,
----							 201, @v_sysdate, @l_sysdate  ;




IF  EXISTS (SELECT * FROM tempdb.sys.objects WHERE NAME = '#TS_IMPORT_EXCLUDE' AND type in (N'U'))
   DROP TABLE dbo.#TS_IMPORT_EXCLUDE



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of stage.LOAD_STG_RECORD';
     EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, 
                                               @epa_job_id = @in_job_fk,  @epa_batch_no = @in_batch_no
                                               
     
      
      EXEC exec_monitor.print_status_sp @exec_id = @l_exec_no;
   END TRY
   BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;
      

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@epa_job_id = @in_job_fk;

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState, -- State.
					@epa_job_id
				    );
   END CATCH;
END





















































