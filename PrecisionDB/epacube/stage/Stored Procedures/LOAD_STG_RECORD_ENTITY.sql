﻿





-- Copyright 2008
--
-- Procedure Created by Kathi Scott
--
--
-- Purpose: To load the data into the STG_ENTITY_IDENTIFICATION table
--			Lookup Entity Structure FK for Unique Data Names only
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/05/2007   Initial version
-- CV		 06/15/2009   uncommented some code that does not create stage data if null						  
-- CV	     07/15/2009   Added new System insert to stg record entity
-- CV        02/17/2010   Added do not import ind logic
-- CV        08/23/2010   Commenting out old system Id code
-- CV        09/08/2013   Adding level 2 to insert missing entity
-- MN		 02/17/2014	  Stripped down all cursors code

CREATE PROCEDURE [stage].[LOAD_STG_RECORD_ENTITY] (
      @in_job_fk       bigint
   )
AS
BEGIN


/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to load Entities into the STAGE.STG_ENTITY_IDENTIFICATION
--	 table.  
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure STAGE.STG_LOAD_ENTITIES
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--   Uses the import.V_import View looking for those data elements that have table name of entity
--   structures.  
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 datetime;
     DECLARE @epa_job_id		 Int;

     DECLARE @status_desc          varchar(max);
     DECLARE @v_import_package_fk  int
     DECLARE @v_entity_class_cr_fk int

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);
	 DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.LOAD_STG_RECORD_ENTITY', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE()
	 SET @V_START_TIMESTAMP = getdate()  -- used in steps



-------------------------------------------------------------------------------------------
--   INSERT STG_RECORD_ENITTY FOR EACH ENTITY FOUND ON THE ROW ( NOT = TO ROW ENTITY )
--   REQUIREMENTS:  THERE SHOULD ONLY BE ONE OF EACH DATA NAME IN THE IMPORT MAP METADATA
--                  AND ONLY THE ENTITY STRUCTURE DATA NAME CAN HAVE A DATA ASSOCIATION 
--
--   FOR IMPORTS WHERE THERE ARE MULTIPLE ENTITIES WITH DATA FOR A SINGLE PRODUCT
--		THEN THE SEQ# ON THE IMPORT_RECORD_DATA WILL MAKE THEM UNIQUE
-------------------------------------------------------------------------------------------


     SET @v_import_package_fk   = ( select top 1 import_package_fk from stage.STG_RECORD WITH (NOLOCK) 
                                    where job_fk = @in_job_fk )

     SET @v_entity_class_cr_fk   = ( select top 1 entity_class_cr_fk from stage.STG_RECORD WITH (NOLOCK) 
                                    where job_fk = @in_job_fk )


;with EntityData as (
SELECT DISTINCT
					 SREC.STG_RECORD_ID
						,ID.SEQ_ID
						,SREC.JOB_FK
						,SREC.IMPORT_PACKAGE_FK
						,140 ACTIVITY_ACTION_CR_FK
						,97  EVENT_CONDITION_CR_FK	
				   ,cast ( vm.entity_class_cr_fk as varchar(16) ) AS entity_class_cr_fk			
				   ,cast ( vm.data_name_fk as varchar(16) ) AS entity_data_name_fk
				     ,isnull( cast ( vm.assoc_data_name_fk as varchar(16) ), '-999') AS assoc_data_name_fk		       
				   ,CASE WHEN ds.entity_class_cr_fk = @v_entity_class_cr_fk THEN 'PARENT' ELSE 'CHILD' END assoc_parent_child
				  ,vm.import_column_name AS column_name
				   ,ISNULL (cast ( vm.insert_missing_ind as varchar(16)), '-999') insert_missing_ind
			FROM import.v_import vm
			left JOIN epacube.data_name dn with (nolock) on ( dn.data_name_id = vm.assoc_data_name_fk )
			left JOIN epacube.data_set ds WITH (NOLOCK) ON (ds.data_set_id = dn.data_set_fk )
			left join STAGE.STG_RECORD SREC WITH (NOLOCK)
			ON srec.import_package_fk = vm.import_package_fk
                   INNER JOIN IMPORT.IMPORT_RECORD_DATA ID WITH (NOLOCK)
                      ON ( ID.JOB_FK = SREC.JOB_FK
                      AND  ID.RECORD_NO = SREC.RECORD_NO
					  AND ISNULL ( ID.DO_NOT_IMPORT_IND, 0 ) <> 1)
			WHERE vm.import_package_fk = @v_import_package_fk
			AND vm.table_name = 'ENTITY_STRUCTURE' 
			and SREC.JOB_FK = @in_job_fk
			AND   SREC.EVENT_CONDITION_CR_FK IN ( 97, 98 ) 
			 AND [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name) IS NOT NULL
                AND [epacube].[getValueFromImportData](id.import_record_data_id, vm.import_column_name) <> ' '
)
INSERT INTO [stage].[STG_RECORD_ENTITY]
					   ([STG_RECORD_FK]
					   ,[SEQ_ID]
					   ,[JOB_FK]
					   ,[IMPORT_PACKAGE_FK]  
					   ,[ACTIVITY_ACTION_CR_FK]
			           ,[EVENT_CONDITION_CR_FK]				   
					   ,[ENTITY_CLASS_CR_FK]
					   ,[ENTITY_DATA_NAME_FK]
					   ,[ASSOC_DATA_NAME_FK]
					   ,[ASSOC_PARENT_CHILD]
					   ,[COLUMN_NAME]
					   ,[INSERT_MISSING_IND]                                 
					   )
select
					STG_RECORD_ID
					,SEQ_ID
					,JOB_FK
					,IMPORT_PACKAGE_FK
					,ACTIVITY_ACTION_CR_FK
					,EVENT_CONDITION_CR_FK 	
				   ,entity_class_cr_fk			
				   ,entity_data_name_fk
				   ,assoc_data_name_fk		       
				   ,assoc_parent_child
				   ,column_name
				   ,insert_missing_ind
FROM EntityData

       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows INSERTED into the STAGE.STG_RECORD_ENTITY table = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk
       END


-------------------------------------------------------------------------------------------
--   LOAD STG_RECORD_ENTITY TABLE WITH SYSTEM ASSOCIATION
-------------------------------------------------------------------------------------

----INSERT INTO [stage].[STG_RECORD_ENTITY]
----           ([STG_RECORD_FK]
----			,[SEQ_ID]
----           ,[ENTITY_CLASS_CR_FK]
----           ,[ENTITY_DATA_NAME_FK]
----           ,[ASSOC_DATA_NAME_FK]
----           ,[ASSOC_PARENT_CHILD]
----           ,[COLUMN_NAME]
----           ,[INSERT_MISSING_IND]                                 
----           ,[ACTIVITY_ACTION_CR_FK]
----           ,[EVENT_CONDITION_CR_FK]
----           ,[JOB_FK]
----           ,[IMPORT_PACKAGE_FK]                      
----           )
----SELECT                        
----			  SREC.STG_RECORD_ID
----			 ,IRD.SEQ_ID
----			 ,(select cr.code_ref_id from epacube.code_ref cr 
----					where cr.code_type = 'ENTITY_CLASS' 
----					and cr.code = 'SYSTEM')
----			 ,(select dn.data_name_id FROM epacube.data_name  dn
----						where dn.name = 'SYSTEM')
----				,(select dn.data_name_id FROM epacube.data_name dn
----						where dn.name = 'PRODUCT SYSTEM')
----			 ,'PARENT'
----			 ,'SYSTEM_ID_NAME'  --IMM.COLUMN_NAME
----			 ,1		-- INSERT MISSING	 
----			 ,140  -- UNMATCHED
----			 ,97  -- GREEN
----			 ,SREC.JOB_FK
----			 ,SREC.IMPORT_PACKAGE_FK
----FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
----INNER JOIN IMPORT.IMPORT_RECORD_DATA IRD WITH (NOLOCK)
---- ON ( IRD.RECORD_NO = SREC.RECORD_NO
----AND ISNULL(IRD.DO_NOT_IMPORT_IND, 0) <> 1
----AND IRD.SYSTEM_ID_NAME is not NULL
----AND IRD.JOB_FK = SREC.JOB_FK
----AND SREC.JOB_FK =  @IN_JOB_FK)

-------------------------------------------------------------------------------------------
--   LOAD STG_RECORD_ENTITY_IDENT TABLE
-------------------------------------------------------------------------------------
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD STG_RECORD_ENTITY_IDENT TABLE',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

	  EXEC stage.LOAD_STG_RECORD_ENTITY_IDENT  @IN_JOB_FK

----      SELECT @v_output_rows = COUNT(1)
----        FROM stage.STG_ENTITY_IDENTIFICATION WITH (NOLOCK)
----       WHERE import_job_fk = @in_job_fk
----
----	  set @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @l_sysdate = getdate()
----      exec common.job_execution_create  @in_job_fk, 'ENTITY IDENTIFICATION',
----                                @v_input_rows, @v_output_rows, @v_exception_rows,
----							 201, @v_sysdate, @l_sysdate  ;
----	  set @v_sysdate = getdate();




            SET @status_desc =  'stage.LOAD_STG_RECORD_ENTITY: inserting - '
                                + '#TS_ENTITY_MATCH'

            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;




-------------------------------------------------------------------------------------------
--   MATCH STG_RECORD_ENTITY_IDENT TABLE
-------------------------------------------------------------------------------------
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'MATCH STG_RECORD_ENTITY_IDENT TABLE - stage.LOAD_STG_RECORD_ENTITY_IDENT_MATCH ',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

	  EXEC stage.LOAD_STG_RECORD_ENTITY_IDENT_MATCH  @IN_JOB_FK

----      SELECT @v_output_rows = COUNT(1)
----        FROM stage.STG_ENTITY_IDENTIFICATION WITH (NOLOCK)
----       WHERE import_job_fk = @in_job_fk
----
----	  set @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @l_sysdate = getdate()
----      exec common.job_execution_create  @in_job_fk, 'ENTITY IDENTIFICATION',
----                                @v_input_rows, @v_output_rows, @v_exception_rows,
----							 201, @v_sysdate, @l_sysdate  ;
----	  set @v_sysdate = getdate();





------------------------------------------------------------------------------------------------------------
--   NEXT AUTO CREATE ANY MISSING ENTITIES THAT ARE ASSOCIATIED BY THEIR UNIQUE IDENTIFIER
--	 THESE ENTITIES MUST BE OF LEVEL 1... NOT SURE HOW WE WOULD HAVE ENOUGH DATA TO DO HIERARCHY
--   USE TEMPORARY TABLE TO ACCUMULATE UNIQUE OCCURANCES TO CREATE A SINGLE ENTITY PER IDENTIFIER
--------------------------------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_ENTITY_IDENTS') is not null
	   drop table #TS_ENTITY_IDENTS;
	   
	-- create temp table
	CREATE TABLE #TS_ENTITY_IDENTS(
		ENTITY_CLASS_CR_FK INT,
		ENTITY_DATA_NAME_FK INT,
		ENTITY_ID_DATA_NAME_FK INT,
		ENTITY_ID_VALUE varchar(256)  NULL,
		STG_RECORD_ENTITY_FK BIGINT,
        IMPORT_FILENAME  varchar(256)  NULL,
        JOB_FK BIGINT NULL,
        RECORD_NO BIGINT NULL
		 )


            SET @status_desc =  'stage.LOAD_STG_RECORD_ENTITY: inserting - '
                                + '#TS_ENTITY_IDENTS'

            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
            
            

-------------------------------------------------------------------------------------------
--   CREATE ANY MISSING ENTITIES THAT ARE FLAGGED IF INSERT_MISSING_IND = 1 
-------------------------------------------------------------------------------------
  	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'CREATE ANY MISSING ENTITIES THAT ARE FLAGGED IF INSERT_MISSING_IND = 1 ',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
										           
	INSERT INTO #TS_ENTITY_IDENTS
           (
            ENTITY_CLASS_CR_FK
           ,ENTITY_DATA_NAME_FK
           ,ENTITY_ID_DATA_NAME_FK
           ,ENTITY_ID_VALUE
           ,STG_RECORD_ENTITY_FK
           ,IMPORT_FILENAME
           ,JOB_FK
           ,RECORD_NO
           )
--
	SELECT 
		 B.ENTITY_CLASS_CR_FK
		,B.ENTITY_DATA_NAME_FK
		,B.ENTITY_ID_DATA_NAME_FK
		,B.ENTITY_ID_VALUE
		,B.STG_RECORD_ENTITY_FK
		,( SELECT SREC.IMPORT_FILENAME
		   FROM stage.STG_RECORD_ENTITY SRECE
		   INNER JOIN stage.STG_RECORD SREC
		     ON ( SREC.STG_RECORD_ID = SRECE.STG_RECORD_FK )
		   WHERE STG_RECORD_ENTITY_ID = B.STG_RECORD_ENTITY_FK )
		,( SELECT SREC.JOB_FK
		   FROM stage.STG_RECORD_ENTITY SRECE
		   INNER JOIN stage.STG_RECORD SREC
		     ON ( SREC.STG_RECORD_ID = SRECE.STG_RECORD_FK )
		   WHERE STG_RECORD_ENTITY_ID = B.STG_RECORD_ENTITY_FK )
		,( SELECT SREC.RECORD_NO
		   FROM stage.STG_RECORD_ENTITY SRECE
		   INNER JOIN stage.STG_RECORD SREC
		     ON ( SREC.STG_RECORD_ID = SRECE.STG_RECORD_FK )
		   WHERE STG_RECORD_ENTITY_ID = B.STG_RECORD_ENTITY_FK )		   
	FROM (    
--
	SELECT DISTINCT
		 A.ENTITY_CLASS_CR_FK
		,A.ENTITY_DATA_NAME_FK
		,A.ENTITY_ID_DATA_NAME_FK
		,A.ENTITY_ID_VALUE
		,( SELECT TOP 1 STG_RECORD_ENTITY_FK
		   FROM stage.STG_RECORD_ENTITY_IDENT
		   WHERE JOB_FK = A.JOB_FK
		   AND   ENTITY_DATA_NAME_FK = A.ENTITY_DATA_NAME_FK
		   AND   ENTITY_ID_DATA_NAME_FK = A.ENTITY_ID_DATA_NAME_FK
		   AND   ENTITY_ID_VALUE = A.ENTITY_ID_VALUE )  AS STG_RECORD_ENTITY_FK
	FROM (    
	SELECT DISTINCT
		 SRECE.JOB_FK
		,SRECE.ENTITY_CLASS_CR_FK
		,SRECEI.ENTITY_DATA_NAME_FK
		,SRECEI.ENTITY_ID_DATA_NAME_FK
		,SRECEI.ENTITY_ID_VALUE
	FROM stage.STG_RECORD_ENTITY SRECE
	INNER JOIN stage.STG_RECORD_ENTITY_IDENT SRECEI
	 ON ( SRECEI.STG_RECORD_ENTITY_FK = SRECE.STG_RECORD_ENTITY_ID 
	 AND  SRECEI.COLUMN_NAME = SRECE.COLUMN_NAME )
	  INNER JOIN EPACUBE.DATA_NAME DN 
	 on (dn.DATA_NAME_ID = SRECEI.entity_id_data_name_fk 
	 and dn.RECORD_STATUS_CR_FK = 1
	 and dn.INSERT_MISSING_VALUES = 1)
	WHERE SRECE.JOB_FK = @IN_JOB_FK 
	AND   SRECE.INSERT_MISSING_IND = 1 
	AND   SRECE.ENTITY_STRUCTURE_FK IS NULL
	AND srece.entity_data_name_fk in (select distinct data_name_fk 
	from epacube.ENTITY_CLASS_TREE 
	where LEVEL_SEQ IN ( 1, 2) 
	and RECORD_STATUS_CR_FK = 1)  ---- want to create these from stage record
	  
AND srece.entity_data_name_fk  not IN (144010, 144020)  ---- want to create these from stage record

	) A
    ) B


------------------------------------------------------------------------------------------
--   CREATE EVENT_STRUCTURE 
--		THEN CREATE THE IDENTIFIERS WITH ITS NEW ASSOCIATED ENTITY_STRUCTURE_FK
------------------------------------------------------------------------------------------


            SET @status_desc =  'stage.LOAD_STG_RECORD_ENTITY: inserting - '
                                + 'ENTITY_STRUCTURE'

            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;

-------------------------------------------------------------------------------------------------
--------   REPLACES THE CODE BELOW AND CREATES ENTITIES THAT ARE FLAGGED 
--------   IF INSERT_MISSING_IND = 1 
-------------------------------------------------------------------------------------------
------
  	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'CREATES ENTITIES THAT ARE FLAGGED IF INSERT_MISSING_IND = 1',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

EXEC [stage].[INSERT_MISSING_ENTITY] @in_job_fk
------
-------------------------------------------------------------------------------------------------
--------   CREATE ANY MISSING ENTITIES THAT ARE FLAGGED IF INSERT_MISSING_IND = 1 
-------------------------------------------------------------------------------------------
------
------
------	INSERT INTO epacube.ENTITY_STRUCTURE
------           (ENTITY_STATUS_CR_FK
------           ,DATA_NAME_FK
------           ,LEVEL_SEQ
------           ,PARENT_ENTITY_STRUCTURE_FK
------           ,ENTITY_CLASS_CR_FK
------           ,IMPORT_FILENAME
------           ,RECORD_NO
------           ,JOB_FK
------           ,RECORD_STATUS_CR_FK
--------           ,CREATE_TIMESTAMP
--------           ,UPDATE_TIMESTAMP
------           ,UPDATE_LOG_FK
------           )
------	SELECT 
------           ( SELECT CODE_REF_ID
------             FROM EPACUBE.CODE_REF  WITH (NOLOCK)
------             WHERE CODE_TYPE = 'ENTITY_STATUS'
------             AND   CODE = 'ACTIVE' )
------		  ,TSEI.ENTITY_DATA_NAME_FK
------		  ,1  --[LEVEL_SEQ]
------          ,NULL   --- [PARENT_ENTITY_STRUCTURE_FK]
------		  ,TSEI.ENTITY_CLASS_CR_FK
------		  ,TSEI.IMPORT_FILENAME
------		  ,TSEI.RECORD_NO
------		  ,TSEI.JOB_FK
------          ,1  -- RECORD STATUS
--------          ,@L_SYSDATE
--------          ,@L_SYSDATE
------          ,TSEI.STG_RECORD_ENTITY_FK
------	  FROM #TS_ENTITY_IDENTS TSEI
------
------
------
------UPDATE stage.STG_RECORD_ENTITY
------SET ENTITY_STRUCTURE_FK = ES.ENTITY_STRUCTURE_ID
------   ,ACTIVITY_ACTION_CR_FK   = 144
------   ,EVENT_CONDITION_CR_FK = 98   -- YELLOW FOR AUTO INSERT
------FROM #TS_ENTITY_IDENTS TSEI
------INNER JOIN EPACUBE.ENTITY_STRUCTURE ES
------ ON ( ES.UPDATE_LOG_FK = TSEI.STG_RECORD_ENTITY_FK 
------ AND  ES.DATA_NAME_FK = TSEI.ENTITY_DATA_NAME_FK
------ AND  ES.IMPORT_FILENAME = TSEI.IMPORT_FILENAME 
------ AND  ES.JOB_FK = TSEI.JOB_FK 
------ AND  ES.RECORD_NO = TSEI.RECORD_NO )
------WHERE TSEI.STG_RECORD_ENTITY_FK = stage.STG_RECORD_ENTITY.STG_RECORD_ENTITY_ID
------
------
------
------INSERT INTO epacube.ENTITY_IDENTIFICATION
------           ([ENTITY_STRUCTURE_FK]
------           ,[ENTITY_DATA_NAME_FK]
------           ,[DATA_NAME_FK]
------           ,[VALUE]
------           )
------SELECT  
------            SRECE.ENTITY_STRUCTURE_FK
------           ,TSEI.ENTITY_DATA_NAME_FK
------           ,TSEI.ENTITY_ID_DATA_NAME_FK
------           ,TSEI.ENTITY_ID_VALUE
------FROM #TS_ENTITY_IDENTS TSEI
------INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
------  ON ( DN.DATA_NAME_ID = TSEI.ENTITY_ID_DATA_NAME_FK )
------INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
------  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )  
------INNER JOIN  stage.STG_RECORD_ENTITY SRECE
------  ON ( SRECE.STG_RECORD_ENTITY_ID = TSEI.STG_RECORD_ENTITY_FK )
------WHERE DS.TABLE_NAME = 'ENTITY_IDENTIFICATION'
------
------
------
------INSERT INTO epacube.ENTITY_IDENT_MULT
------           ([ENTITY_STRUCTURE_FK]
------           ,[ENTITY_DATA_NAME_FK]
------           ,[DATA_NAME_FK]
------           ,[VALUE]
------           )
------SELECT 
------            SRECE.ENTITY_STRUCTURE_FK
------           ,TSEI.ENTITY_DATA_NAME_FK
------           ,TSEI.ENTITY_ID_DATA_NAME_FK
------           ,TSEI.ENTITY_ID_VALUE
------FROM #TS_ENTITY_IDENTS TSEI
------INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
------  ON ( DN.DATA_NAME_ID = TSEI.ENTITY_ID_DATA_NAME_FK )
------INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
------  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )  
------INNER JOIN  stage.STG_RECORD_ENTITY SRECE
------  ON ( SRECE.STG_RECORD_ENTITY_ID = TSEI.STG_RECORD_ENTITY_FK )
------WHERE DS.TABLE_NAME = 'ENTITY_IDENT_MULT'
------

-------------------------------------------------------------------------------------------
--   MATCH STG_RECORD_ENTITY_IDENT TABLE
-------------------------------------------------------------------------------------

----	  EXEC stage.LOAD_STG_RECORD_ENTITY_IDENT_MATCH  @IN_JOB_FK

----      SELECT @v_output_rows = COUNT(1)
----        FROM stage.STG_ENTITY_IDENTIFICATION WITH (NOLOCK)
----       WHERE import_job_fk = @in_job_fk
----
----	  set @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @l_sysdate = getdate()
----      exec common.job_execution_create  @in_job_fk, 'ENTITY IDENTIFICATION',
----                                @v_input_rows, @v_output_rows, @v_exception_rows,
----							 201, @v_sysdate, @l_sysdate  ;
----	  set @v_sysdate = getdate();




-------------------------------------------------------------------------------------------
--   AFTER MATCH OF NEW STG_RECORD_ENTITY 
--		Update all stage.STG_RECORD_ENTITY that also where matched to the NEW ENTITY
-------------------------------------------------------------------------------------------
  	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'Update all stage.STG_RECORD_ENTITY that also where matched to the NEW ENTITY',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

UPDATE stage.STG_RECORD_ENTITY
SET ACTIVITY_ACTION_CR_FK   = 144
   ,EVENT_CONDITION_CR_FK = 98   -- YELLOW FOR AUTO INSERT
WHERE JOB_FK = @IN_JOB_FK
AND   ENTITY_STRUCTURE_FK IN (
			SELECT SRECE.ENTITY_STRUCTURE_FK
			FROM stage.STG_RECORD_ENTITY SRECE WITH (NOLOCK) 
			WHERE SRECE.JOB_FK = @IN_JOB_FK
			AND   ACTIVITY_ACTION_CR_FK = 144 
			AND   ENTITY_STRUCTURE_FK IS NOT NULL )
              


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of stage.LOAD_STG_RECORD_ENTITY';
     EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk ;
     
      
      EXEC exec_monitor.print_status_sp @exec_id = @l_exec_no;
   END TRY
   BEGIN CATCH  
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
END


