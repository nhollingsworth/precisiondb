﻿











-- Copyright 2008
--
-- Procedure Created by Kathi Scott
--
--
-- Purpose: Match ONLY the Unique and Mult Identifiers that belong
--			to the Entity Structures in the STG_RECORD_ENTITY Table
--			( NOTE:  Row Entity will have its idents loaded in Cleansing Tables )
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/05/2007   Initial version
-- KS        02/15/2010   Performance Enhancement...Looked like doing twice the work						  
--							commented out first step.
--
CREATE PROCEDURE [stage].[LOAD_STG_RECORD_ENTITY_IDENT_MATCH] (
      @in_job_fk       BIGINT
   )
AS
BEGIN


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 datetime;     

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);
     DECLARE @ls_entity_structure_fk_sql   VARCHAR (1000);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);
	 DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.LOAD_STG_RECORD_ENTITY_IDENT_MATCH', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE()
	 SET @V_START_TIMESTAMP = getdate()  -- used in steps


     
-------- -- ------------------------------------------------------------------------------------
-------- --      UPDATE stage.STG_RECORD_ENTITY_IDENT'S ENTITY_STRUCTURE_FK WHERE NULL
-------- -- -----------------------------------------------------------------------------------
SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'UPDATE stage.STG_RECORD_ENTITY_IDENT ENTITY_STRUCTURE_FK WHERE NULL',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

UPDATE stage.STG_RECORD_ENTITY_IDENT
SET ENTITY_STRUCTURE_FK = ( CASE DS.TABLE_NAME
						    WHEN 'ENTITY_IDENTIFICATION' THEN
								(	SELECT EI.ENTITY_STRUCTURE_FK FROM EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK) 
									WHERE EI.ENTITY_DATA_NAME_FK = stage.STG_RECORD_ENTITY_IDENT.ENTITY_DATA_NAME_FK
									AND EI.DATA_NAME_FK = stage.STG_RECORD_ENTITY_IDENT.ENTITY_ID_DATA_NAME_FK
									AND EI.VALUE = stage.STG_RECORD_ENTITY_IDENT.ENTITY_ID_VALUE
                                ) 
						    WHEN 'ENTITY_IDENT_MULT' THEN
								(	SELECT EI.ENTITY_STRUCTURE_FK FROM EPACUBE.ENTITY_IDENT_MULT EI WITH (NOLOCK) 
									WHERE EI.ENTITY_DATA_NAME_FK = stage.STG_RECORD_ENTITY_IDENT.ENTITY_DATA_NAME_FK
									AND EI.DATA_NAME_FK = stage.STG_RECORD_ENTITY_IDENT.ENTITY_ID_DATA_NAME_FK
									AND EI.VALUE = stage.STG_RECORD_ENTITY_IDENT.ENTITY_ID_VALUE
                                ) 
                            ELSE NULL
                            END  )
--                          
FROM epacube.DATA_NAME dn WITH (NOLOCK)
INNER JOIN epacube.DATA_SET ds WITH (NOLOCK)
 ON ( ds.data_set_id = dn.data_set_fk )
WHERE stage.STG_RECORD_ENTITY_IDENT.ENTITY_STRUCTURE_FK IS NULL
AND   stage.STG_RECORD_ENTITY_IDENT.ENTITY_ID_DATA_NAME_FK = DN.DATA_NAME_ID 
AND   stage.STG_RECORD_ENTITY_IDENT.JOB_FK = @IN_JOB_FK



---------------------------------------------------------------------
--DMU ADDED THIS BECAUSE THE DATA NAME IS DIFFERENT FOR ALL WHSE AND
--WE NOW SEND IN SX RESULTS FOR WHSE AND WHSE SPECIFIC
---------------------------------------------------------------------
SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'UPDATE stage.STG_RECORD_ENTITY_IDENT WE NOW SEND IN SX RESULTS FOR WHSE AND WHSE SPECIFIC',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

						UPDATE STAGE.STG_RECORD_ENTITY_IDENT
						SET ENTITY_STRUCTURE_FK = 1
						,ENTITY_DATA_NAME_FK = 141099  --Cindy added for DMU imports
						WHERE ENTITY_ID_VALUE in ('All Orgs', 'All Whse')
						AND ENTITY_STRUCTURE_FK is NULL
						AND JOB_FK = @in_job_fk


--NOT SURE WHY THIS IS USED TESTING NOW
--UPDATE STAGE.STG_RECORD_ENTITY
--	SET ENTITY_STRUCTURE_FK = 1
--	FROM STAGE.STG_RECORD_ENTITY_IDENT
--	WHERE STAGE.STG_RECORD_ENTITY.STG_RECORD_ENTITY_ID = STAGE.STG_RECORD_ENTITY_IDENT.STG_RECORD_ENTITY_FK
--	AND STAGE.STG_RECORD_ENTITY.ENTITY_STRUCTURE_FK is NULL
--	AND STAGE.STG_RECORD_ENTITY.JOB_FK = @in_job_fk


-------------------------------------------------------------------------------------------
--   MATCH STG RECORDS ENTITY TO STG_RECORD_ENTITY_IDENT TABLE
--		REQUIRED RESTRICTION !!!!!!
--		ALL IDENTIFIERS USED ARE UNIQUE ( EITHER IDENTIFICATION OR IDENT_MULT )
--
--   Use temporary table to determine matching results
--		
--    ENTITY_MATCH_CNT				Number of All Entities Matching the stg entity structure
--
--   MATCH_RESULT
--    MATCHED:  ( SET 1 TO 7 BASED ON TYPE OF MATCH )
--		1  = SINGLE MATCHED ENTITY STRUCTURE SO IS MATCHED
--		>1 = MORE THAN ONE ENTITY MATCHED SO MISMATCHED
--      0  = NO ENTITIES MATCHED SO IS NEW
--
------------------------------------------------------------------------------------------



--drop temp table if it exists
	IF object_id('tempdb..#TS_ENTITY_MATCH') is not null
	   drop table #TS_ENTITY_MATCH

-- create temporary tables and indexes


CREATE TABLE #TS_ENTITY_MATCH  (
	[TS_ENTITY_MATCH_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[STG_RECORD_ENTITY_FK] [bigint],
	[ENTITY_STRUCTURE_FK] [bigint],
	[ENTITY_MATCH_CNT] [int]
)


CREATE NONCLUSTERED INDEX IDX_TSEM_SES ON #TS_ENTITY_MATCH
(STG_RECORD_ENTITY_FK ASC )
INCLUDE ( [ENTITY_MATCH_CNT], [ENTITY_STRUCTURE_FK] )
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


CREATE CLUSTERED INDEX IDX_TSEM_TSEMID ON #TS_ENTITY_MATCH
(TS_ENTITY_MATCH_ID ASC)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



INSERT INTO #TS_ENTITY_MATCH
  ( STG_RECORD_ENTITY_FK
   ,ENTITY_STRUCTURE_FK
   ,ENTITY_MATCH_CNT 
   )
---
SELECT	
    SRECE.STG_RECORD_ENTITY_ID
   ,( SELECT TOP 1 ENTITY_STRUCTURE_FK
      FROM stage.STG_RECORD_ENTITY_IDENT
      WHERE STG_RECORD_ENTITY_FK = SRECE.STG_RECORD_ENTITY_ID
      AND   ENTITY_STRUCTURE_FK IS NOT NULL )
   ,( SELECT COUNT ( DISTINCT ENTITY_STRUCTURE_FK )
      FROM stage.STG_RECORD_ENTITY_IDENT
      WHERE STG_RECORD_ENTITY_FK = SRECE.STG_RECORD_ENTITY_ID
      AND   ENTITY_STRUCTURE_FK IS NOT NULL )
FROM stage.STG_RECORD_ENTITY  SRECE
WHERE 1 = 1
AND   SRECE.JOB_FK = @IN_JOB_FK



 -- ------------------------------------------------------------------------------------
 --      UPDATE MATCH RESULTS TO THE stage.STG_RECORD_ENTITY
 --  
 --      IF ONE AND ONLY ONE THEN IT IS A MATCH  
 --      0    -- NO MATCHES
 --      > 1  -- MIS MATCH BECAUSE THERE ARE MORE THAN ONE ENTITY MATCHING
 -- -----------------------------------------------------------------------------------
 SET @V_END_TIMESTAMP = getdate()
 EXEC common.job_execution_create  @in_job_fk, 'UPDATE MATCH RESULTS TO THE stage.STG_RECORD_ENTITY',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

UPDATE stage.STG_RECORD_ENTITY
SET ENTITY_STRUCTURE_FK = CASE TSEM.ENTITY_MATCH_CNT
                          WHEN  0  THEN NULL
					      WHEN  1  THEN TSEM.ENTITY_STRUCTURE_FK
					      ELSE  NULL
					      END
   ,ACTIVITY_ACTION_CR_FK =  CASE TSEM.ENTITY_MATCH_CNT
                          WHEN 0 THEN 140  -- UNMATCHED
					      WHEN 1 THEN 142  -- MATCHED
					      ELSE 140         -- UNMATCHED
					      END
   ,UPDATE_TIMESTAMP =  GETDATE()
--
FROM #TS_ENTITY_MATCH TSEM
WHERE TSEM.STG_RECORD_ENTITY_FK = stage.STG_RECORD_ENTITY.STG_RECORD_ENTITY_ID




--drop temp table if it exists
	IF object_id('tempdb..#TS_ENTITY_MATCH') is not null
	   drop table #TS_ENTITY_MATCH





------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of stage.LOAD_STG_RECORD_ENTITY_IDENT_MATCH';
     EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
     
      
      EXEC exec_monitor.print_status_sp @exec_id = @l_exec_no;
   END TRY
   BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
END

















































