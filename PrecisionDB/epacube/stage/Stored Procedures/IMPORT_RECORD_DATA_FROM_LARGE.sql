﻿



-- Copyright 2012
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To insert only a small import_record_data grouping at a time
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        12/05/2012   Initial SQL Version
--

CREATE PROCEDURE [stage].[IMPORT_RECORD_DATA_FROM_LARGE] 
               ( @in_job_fk BIGINT, @V_job_fk BIGINT, @in_file_group VARCHAR(64) )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of stage.IMPORT_RECORD_DATA_FROM_FULL.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;



---------------------------------------------------
-- INSERT STATEMENT
-------    in the future after PAXSON drive off IMPORT_RECORD_FILE_GROUP table
---------------------------------------------------


INSERT INTO [import].[IMPORT_RECORD_DATA]
           ([DO_NOT_IMPORT_IND]
           ,[IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[SEQ_ID]
           ,[JOB_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[RESULT_TYPE]
           ,[SYSTEM_ID_NAME]
           ,[DATA_POSITION1]
           ,[DATA_POSITION2]
           ,[DATA_POSITION3]
           ,[DATA_POSITION4]
           ,[DATA_POSITION5]
           ,[DATA_POSITION6]
           ,[DATA_POSITION7]
           ,[DATA_POSITION8]
           ,[DATA_POSITION9]
           ,[DATA_POSITION10]
           ,[DATA_POSITION11]
           ,[DATA_POSITION12]
           ,[DATA_POSITION13]
           ,[DATA_POSITION14]
           ,[DATA_POSITION15]
           ,[DATA_POSITION16]
           ,[DATA_POSITION17]
           ,[DATA_POSITION18]
           ,[DATA_POSITION19]
           ,[DATA_POSITION20]
           ,[DATA_POSITION21]
           ,[DATA_POSITION22]
           ,[DATA_POSITION23]
           ,[DATA_POSITION24]
           ,[DATA_POSITION25]
           ,[DATA_POSITION26]
           ,[DATA_POSITION27]
           ,[DATA_POSITION28]
           ,[DATA_POSITION29]
           ,[DATA_POSITION30]
           ,[DATA_POSITION31]
           ,[DATA_POSITION32]
           ,[DATA_POSITION33]
           ,[DATA_POSITION34]
           ,[DATA_POSITION35]
           ,[DATA_POSITION36]
           ,[DATA_POSITION37]
           ,[DATA_POSITION38]
           ,[DATA_POSITION39]
           ,[DATA_POSITION40]
           ,[DATA_POSITION41]
           ,[DATA_POSITION42]
           ,[DATA_POSITION43]
           ,[DATA_POSITION44]
           ,[DATA_POSITION45]
           ,[DATA_POSITION46]
           ,[DATA_POSITION47]
           ,[DATA_POSITION48]
           ,[DATA_POSITION49]
           ,[DATA_POSITION50]
           ,[DATA_POSITION51]
           ,[DATA_POSITION52]
           ,[DATA_POSITION53]
           ,[DATA_POSITION54]
           ,[DATA_POSITION55]
           ,[DATA_POSITION56]
           ,[DATA_POSITION57]
           ,[DATA_POSITION58]
           ,[DATA_POSITION59]
           ,[DATA_POSITION60]
           ,[DATA_POSITION61]
           ,[DATA_POSITION62]
           ,[DATA_POSITION63]
           ,[DATA_POSITION64]
           ,[DATA_POSITION65]
           ,[DATA_POSITION66]
           ,[DATA_POSITION67]
           ,[DATA_POSITION68]
           ,[DATA_POSITION69]
           ,[DATA_POSITION70]
           ,[DATA_POSITION71]
           ,[DATA_POSITION72]
           ,[DATA_POSITION73]
           ,[DATA_POSITION74]
           ,[DATA_POSITION75]
           ,[DATA_POSITION76]
           ,[DATA_POSITION77]
           ,[DATA_POSITION78]
           ,[DATA_POSITION79]
           ,[DATA_POSITION80]
           ,[DATA_POSITION81]
           ,[DATA_POSITION82]
           ,[DATA_POSITION83]
           ,[DATA_POSITION84]
           ,[DATA_POSITION85]
           ,[DATA_POSITION86]
           ,[DATA_POSITION87]
           ,[DATA_POSITION88]
           ,[DATA_POSITION89]
           ,[DATA_POSITION90]
           ,[DATA_POSITION91]
           ,[DATA_POSITION92]
           ,[DATA_POSITION93]
           ,[DATA_POSITION94]
           ,[DATA_POSITION95]
           ,[DATA_POSITION96]
           ,[DATA_POSITION97]
           ,[DATA_POSITION98]
           ,[DATA_POSITION99]
           ,[DATA_POSITION100]
           ,[DATA_POSITION101]
           ,[DATA_POSITION102]
           ,[DATA_POSITION103]
           ,[DATA_POSITION104]
           ,[DATA_POSITION105]
           ,[DATA_POSITION106]
           ,[DATA_POSITION107]
           ,[DATA_POSITION108]
           ,[DATA_POSITION109]
           ,[DATA_POSITION110]
           ,[DATA_POSITION111]
           ,[DATA_POSITION112]
           ,[DATA_POSITION113]
           ,[DATA_POSITION114]
           ,[DATA_POSITION115]
           ,[DATA_POSITION116]
           ,[DATA_POSITION117]
           ,[DATA_POSITION118]
           ,[DATA_POSITION119]
           ,[DATA_POSITION120]
           ,[DATA_POSITION121]
           ,[DATA_POSITION122]
           ,[DATA_POSITION123]
           ,[DATA_POSITION124]
           ,[DATA_POSITION125]
           ,[DATA_POSITION126]
           ,[DATA_POSITION127]
           ,[DATA_POSITION128]
           ,[DATA_POSITION129]
           ,[DATA_POSITION130]
           ,[DATA_POSITION131]
           ,[DATA_POSITION132]
           ,[DATA_POSITION133]
           ,[DATA_POSITION134]
           ,[DATA_POSITION135]
           ,[DATA_POSITION136]
           ,[DATA_POSITION137]
           ,[DATA_POSITION138]
           ,[DATA_POSITION139]
           ,[DATA_POSITION140]
           ,[DATA_POSITION141]
           ,[DATA_POSITION142]
           ,[DATA_POSITION143]
           ,[DATA_POSITION144]
           ,[DATA_POSITION145]
           ,[DATA_POSITION146]
           ,[DATA_POSITION147]
           ,[DATA_POSITION148]
           ,[DATA_POSITION149]
           ,[DATA_POSITION150]
           ,[DATA_POSITION151]
           ,[DATA_POSITION152]
           ,[DATA_POSITION153]
           ,[DATA_POSITION154]
           ,[DATA_POSITION155]
           ,[DATA_POSITION156]
           ,[DATA_POSITION157]
           ,[DATA_POSITION158]
           ,[DATA_POSITION159]
           ,[DATA_POSITION160]
           ,[DATA_POSITION161]
           ,[DATA_POSITION162]
           ,[DATA_POSITION163]
           ,[DATA_POSITION164]
           ,[DATA_POSITION165]
           ,[DATA_POSITION166]
           ,[DATA_POSITION167]
           ,[DATA_POSITION168]
           ,[DATA_POSITION169]
           ,[DATA_POSITION170]
           ,[DATA_POSITION171]
           ,[DATA_POSITION172]
           ,[DATA_POSITION173]
           ,[DATA_POSITION174]
           ,[DATA_POSITION175]
           ,[DATA_POSITION176]
           ,[DATA_POSITION177]
           ,[DATA_POSITION178]
           ,[DATA_POSITION179]
           ,[DATA_POSITION180]
           ,[DATA_POSITION181]
           ,[DATA_POSITION182]
           ,[DATA_POSITION183]
           ,[DATA_POSITION184]
           ,[DATA_POSITION185]
           ,[DATA_POSITION186]
           ,[DATA_POSITION187]
           ,[DATA_POSITION188]
           ,[DATA_POSITION189]
           ,[DATA_POSITION190]
           ,[DATA_POSITION191]
           ,[DATA_POSITION192]
           ,[DATA_POSITION193]
           ,[DATA_POSITION194]
           ,[DATA_POSITION195]
           ,[DATA_POSITION196]
           ,[DATA_POSITION197]
           ,[DATA_POSITION198]
           ,[DATA_POSITION199]
           ,[DATA_POSITION200]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
SELECT 
       [DO_NOT_IMPORT_IND]
      ,[IMPORT_FILENAME]
      ,[RECORD_NO]
      ,[SEQ_ID]
      ,@V_job_fk    ----- [JOB_FK]
      ,[IMPORT_PACKAGE_FK]
      ,[EFFECTIVE_DATE]
      ,[END_DATE]
      ,[RESULT_TYPE]
      ,[SYSTEM_ID_NAME]
      ,[DATA_POSITION1]
      ,[DATA_POSITION2]
      ,[DATA_POSITION3]
      ,[DATA_POSITION4]
      ,[DATA_POSITION5]
      ,[DATA_POSITION6]
      ,[DATA_POSITION7]
      ,[DATA_POSITION8]
      ,[DATA_POSITION9]
      ,[DATA_POSITION10]
      ,[DATA_POSITION11]
      ,[DATA_POSITION12]
      ,[DATA_POSITION13]
      ,[DATA_POSITION14]
      ,[DATA_POSITION15]
      ,[DATA_POSITION16]
      ,[DATA_POSITION17]
      ,[DATA_POSITION18]
      ,[DATA_POSITION19]
      ,[DATA_POSITION20]
      ,[DATA_POSITION21]
      ,[DATA_POSITION22]
      ,[DATA_POSITION23]
      ,[DATA_POSITION24]
      ,[DATA_POSITION25]
      ,[DATA_POSITION26]
      ,[DATA_POSITION27]
      ,[DATA_POSITION28]
      ,[DATA_POSITION29]
      ,[DATA_POSITION30]
      ,[DATA_POSITION31]
      ,[DATA_POSITION32]
      ,[DATA_POSITION33]
      ,[DATA_POSITION34]
      ,[DATA_POSITION35]
      ,[DATA_POSITION36]
      ,[DATA_POSITION37]
      ,[DATA_POSITION38]
      ,[DATA_POSITION39]
      ,[DATA_POSITION40]
      ,[DATA_POSITION41]
      ,[DATA_POSITION42]
      ,[DATA_POSITION43]
      ,[DATA_POSITION44]
      ,[DATA_POSITION45]
      ,[DATA_POSITION46]
      ,[DATA_POSITION47]
      ,[DATA_POSITION48]
      ,[DATA_POSITION49]
      ,[DATA_POSITION50]
      ,[DATA_POSITION51]
      ,[DATA_POSITION52]
      ,[DATA_POSITION53]
      ,[DATA_POSITION54]
      ,[DATA_POSITION55]
      ,[DATA_POSITION56]
      ,[DATA_POSITION57]
      ,[DATA_POSITION58]
      ,[DATA_POSITION59]
      ,[DATA_POSITION60]
      ,[DATA_POSITION61]
      ,[DATA_POSITION62]
      ,[DATA_POSITION63]
      ,[DATA_POSITION64]
      ,[DATA_POSITION65]
      ,[DATA_POSITION66]
      ,[DATA_POSITION67]
      ,[DATA_POSITION68]
      ,[DATA_POSITION69]
      ,[DATA_POSITION70]
      ,[DATA_POSITION71]
      ,[DATA_POSITION72]
      ,[DATA_POSITION73]
      ,[DATA_POSITION74]
      ,[DATA_POSITION75]
      ,[DATA_POSITION76]
      ,[DATA_POSITION77]
      ,[DATA_POSITION78]
      ,[DATA_POSITION79]
      ,[DATA_POSITION80]
      ,[DATA_POSITION81]
      ,[DATA_POSITION82]
      ,[DATA_POSITION83]
      ,[DATA_POSITION84]
      ,[DATA_POSITION85]
      ,[DATA_POSITION86]
      ,[DATA_POSITION87]
      ,[DATA_POSITION88]
      ,[DATA_POSITION89]
      ,[DATA_POSITION90]
      ,[DATA_POSITION91]
      ,[DATA_POSITION92]
      ,[DATA_POSITION93]
      ,[DATA_POSITION94]
      ,[DATA_POSITION95]
      ,[DATA_POSITION96]
      ,[DATA_POSITION97]
      ,[DATA_POSITION98]
      ,[DATA_POSITION99]
      ,[DATA_POSITION100]
      ,[DATA_POSITION101]
      ,[DATA_POSITION102]
      ,[DATA_POSITION103]
      ,[DATA_POSITION104]
      ,[DATA_POSITION105]
      ,[DATA_POSITION106]
      ,[DATA_POSITION107]
      ,[DATA_POSITION108]
      ,[DATA_POSITION109]
      ,[DATA_POSITION110]
      ,[DATA_POSITION111]
      ,[DATA_POSITION112]
      ,[DATA_POSITION113]
      ,[DATA_POSITION114]
      ,[DATA_POSITION115]
      ,[DATA_POSITION116]
      ,[DATA_POSITION117]
      ,[DATA_POSITION118]
      ,[DATA_POSITION119]
      ,[DATA_POSITION120]
      ,[DATA_POSITION121]
      ,[DATA_POSITION122]
      ,[DATA_POSITION123]
      ,[DATA_POSITION124]
      ,[DATA_POSITION125]
      ,[DATA_POSITION126]
      ,[DATA_POSITION127]
      ,[DATA_POSITION128]
      ,[DATA_POSITION129]
      ,[DATA_POSITION130]
      ,[DATA_POSITION131]
      ,[DATA_POSITION132]
      ,[DATA_POSITION133]
      ,[DATA_POSITION134]
      ,[DATA_POSITION135]
      ,[DATA_POSITION136]
      ,[DATA_POSITION137]
      ,[DATA_POSITION138]
      ,[DATA_POSITION139]
      ,[DATA_POSITION140]
      ,[DATA_POSITION141]
      ,[DATA_POSITION142]
      ,[DATA_POSITION143]
      ,[DATA_POSITION144]
      ,[DATA_POSITION145]
      ,[DATA_POSITION146]
      ,[DATA_POSITION147]
      ,[DATA_POSITION148]
      ,[DATA_POSITION149]
      ,[DATA_POSITION150]
      ,[DATA_POSITION151]
      ,[DATA_POSITION152]
      ,[DATA_POSITION153]
      ,[DATA_POSITION154]
      ,[DATA_POSITION155]
      ,[DATA_POSITION156]
      ,[DATA_POSITION157]
      ,[DATA_POSITION158]
      ,[DATA_POSITION159]
      ,[DATA_POSITION160]
      ,[DATA_POSITION161]
      ,[DATA_POSITION162]
      ,[DATA_POSITION163]
      ,[DATA_POSITION164]
      ,[DATA_POSITION165]
      ,[DATA_POSITION166]
      ,[DATA_POSITION167]
      ,[DATA_POSITION168]
      ,[DATA_POSITION169]
      ,[DATA_POSITION170]
      ,[DATA_POSITION171]
      ,[DATA_POSITION172]
      ,[DATA_POSITION173]
      ,[DATA_POSITION174]
      ,[DATA_POSITION175]
      ,[DATA_POSITION176]
      ,[DATA_POSITION177]
      ,[DATA_POSITION178]
      ,[DATA_POSITION179]
      ,[DATA_POSITION180]
      ,[DATA_POSITION181]
      ,[DATA_POSITION182]
      ,[DATA_POSITION183]
      ,[DATA_POSITION184]
      ,[DATA_POSITION185]
      ,[DATA_POSITION186]
      ,[DATA_POSITION187]
      ,[DATA_POSITION188]
      ,[DATA_POSITION189]
      ,[DATA_POSITION190]
      ,[DATA_POSITION191]
      ,[DATA_POSITION192]
      ,[DATA_POSITION193]
      ,[DATA_POSITION194]
      ,[DATA_POSITION195]
      ,[DATA_POSITION196]
      ,[DATA_POSITION197]
      ,[DATA_POSITION198]
      ,[DATA_POSITION199]
      ,[DATA_POSITION200]
      ,[UPDATE_TIMESTAMP]
      ,[UPDATE_USER]
  FROM [import].[IMPORT_RECORD_DATA_LARGE]
WHERE  job_fk = @in_job_fk 
AND     RESULT_TYPE = @in_file_group





------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of stage.IMPORT_RECORD_DATA_FROM_LARGE'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of stage.IMPORT_RECORD_DATA_FROM_LARGE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END




















