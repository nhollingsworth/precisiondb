﻿


CREATE
    FUNCTION [stage].[ADD_ID_SEQ] 
      (
        @in_start_string varchar(8000),
        @in_rule_token_fk int
      ) 
      RETURNS varchar(8000)
    AS
BEGIN

        DECLARE 

          @l_count bigint,
          @ls_exec varchar(max),
          @l_exec_no bigint,
          @v_start_string varchar(8000),
          @v_return_string varchar(8000),
          @v_result_string varchar(8000),
          @v_debug_string  varchar (max),
          @v_token_value   varchar (16),
          @v_token_seq int,
          @v_pre_separator char(1),
          @v_substr_length bigint,
          @v_max_length bigint,
          @v_id_seq     bigint       


      SELECT 
          @v_token_seq = stage.RULE_TOKEN.TOKEN_SEQ,
          @v_pre_separator = stage.RULE_TOKEN.PRE_SEPARATOR, 
          @v_substr_length = stage.RULE_TOKEN.SUBSTR_LENGTH
        FROM stage.RULE_TOKEN
        WHERE (stage.RULE_TOKEN.RULE_TOKEN_ID = @in_rule_token_fk)

--
--    IF @v_token_seq > 1
--    AND (  @in_start_string = ''
--		OR @in_start_string is NULL
--		OR @in_start_string = 'NULL'
--        )
--	            RETURN 'NULL START STRING'



--  Start String Operations

   IF (@v_pre_separator = '?')
     SET @v_pre_separator = ' '


   IF @v_pre_separator is NULL
		SET @v_start_string = isnull ( @in_start_string, ' ' )
   ELSE
		SET @v_start_string = isnull ( @in_start_string, ' ' ) + @v_pre_separator       



/*------------------------------------------------------------------------------------
       Result_String = @v_start_string + sequence number
-------------------------------------------------------------------------------------*/

   SET @v_id_seq = seq_manager.db_get_next_sequence_value('common','product_id_seq')

   SET @v_token_value = 
           REPLICATE('0',( @v_substr_length 
                           - LEN ( cast ( @v_id_seq  as varchar ( 64 ) ))))
				+ ( cast ( @v_id_seq  as varchar ( 64 ) ) )



--     SET @v_result_string =  @v_token_value

  SET @v_result_string =  isnull ( ltrim ( rtrim (@v_start_string + @v_token_value ) ) , 'NULL' )


	IF     @v_result_string = ''
		OR @v_result_string is NULL
		OR @v_result_string = 'NULL'
        RETURN ( 'NULL' )
    ELSE
   RETURN ( @v_result_string )


   RETURN ( @v_result_string )
END

