﻿





CREATE
    FUNCTION [stage].[UOM_CONV] 
      (
        @in_uom_new  varchar(32),
        @in_uom_cur  varchar(32)
      ) 
      RETURNS varchar(8000)
    AS
      
      BEGIN

        DECLARE 

          @l_count bigint,
          @ls_exec varchar(max),
          @l_exec_no bigint,
          @v_uom_prim_qty_new numeric ( 18,6 ),
          @v_uom_prim_qty_cur numeric ( 18,6 ),
          @v_uom_convert_mult numeric ( 18,6 )


      SELECT 
          @v_uom_prim_qty_new = primary_qty
        FROM epacube.UOM_CODE
        WHERE uom_code_id =  @in_uom_new


      SELECT 
          @v_uom_prim_qty_cur = primary_qty
        FROM epacube.UOM_CODE
        WHERE uom_code_id =  @in_uom_cur


--  Set Conversion Factor 

		SET @v_uom_convert_mult =  @v_uom_prim_qty_new / @v_uom_prim_qty_cur


   RETURN ( @v_uom_convert_mult )
END














