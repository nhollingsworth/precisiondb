﻿




CREATE FUNCTION [stage].[SKIPPED_PRD_SEQ] (
   @in_data_name_fk       int,
   @in_start_seq          bigint,
   @in_max_seq            bigint
   )
   RETURNS bigint
 AS
--  In Debug Mode return the iterative string @v_debug_string
--  instead of just the normal@v_return_string
 BEGIN
   DECLARE       
   @l_count               bigint,
   @ls_exec               varchar (max),
   @l_exec_no             bigint, 
   @v_return_seq          bigint, 
   @v_count               bigint, 
   @j                     bigint
   

   
  SET  @v_return_seq = NULL
  SET  @J = @in_start_seq

  IF @in_start_seq = 0
		 RETURN ( @v_return_seq )
   

/*************************************************************************************/
--  LOOP THROUGH IDENTIFIERS TO FIND FIRST MISSING SEQ_ID
/*************************************************************************************/


  WHILE ( @J < @in_max_seq )
    BEGIN

	  SET @J = @J + 1

	  SELECT @v_count = count (1) 
	  FROM epacube.product_identification pi 
	  WHERE pi.data_name_fk = @in_data_name_fk
	  AND   pi.value = CAST ( @j as varchar(64) )

	  IF @v_count = 0
	  BEGIN

		 SET @v_return_seq = @j
		 RETURN ( @v_return_seq )

	  END
  

  END
	/*  SEQUENCE Loop */

          
 RETURN ( @v_return_seq  )


END























