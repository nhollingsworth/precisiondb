﻿







  CREATE
    FUNCTION [stage].[ADD_TOKEN] 
      (
        @in_start_string varchar(8000),
        @in_max_length int,
        @in_whole_tokens int,
        @in_rule_token_fk int,
        @in_start_pos     int,
        @in_token_value varchar(8000)
      ) 
      RETURNS varchar(8000)
    AS
      
      BEGIN

        DECLARE 

          @l_count bigint,
          @ls_exec varchar(max),
          @l_exec_no bigint,
          @v_start_string varchar(8000),
          @v_return_string varchar(8000),
          @v_result_string varchar(8000),
          @v_debug_string  varchar (max),
          @v_strip_token varchar(128),
          @v_token_seq int,
          @v_pre_separator char(1),
          @v_remove_spec_chars varchar(64),
          @v_remove_spaces smallint,
          @v_replace_char char(1),
          @v_replacing_char char(1),
          @v_substr_start_pos bigint,
          @v_substr_length bigint,
          @v_max_length bigint       




--select replace('he llo wo' + isnull(null,'') + ' rld',' ','')
--select replace('he llo wo' + isnull(null,'') + ' rld',' ','X')

      SELECT 
          @v_token_seq = stage.RULE_TOKEN.TOKEN_SEQ,
          @v_pre_separator = stage.RULE_TOKEN.PRE_SEPARATOR, 
          @v_remove_spec_chars = stage.RULE_TOKEN.REMOVE_SPEC_CHARS, 
          @v_remove_spaces = stage.RULE_TOKEN.REMOVE_SPACES,
          @v_replace_char = stage.RULE_TOKEN.REPLACE_CHAR, 
          @v_replacing_char = stage.RULE_TOKEN.REPLACING_CHAR, 
          @v_substr_start_pos = stage.RULE_TOKEN.SUBSTR_START_POS, 
          @v_substr_length = stage.RULE_TOKEN.SUBSTR_LENGTH
        FROM stage.RULE_TOKEN
        WHERE (stage.RULE_TOKEN.RULE_TOKEN_ID = @in_rule_token_fk)


	IF     @in_token_value = ''
		OR @in_token_value is NULL
		OR @in_token_value = 'NULL'
	            RETURN 'NULL'
  
	ELSE
    IF @v_token_seq > 1
    AND (  @in_start_string = ''
		OR @in_start_string is NULL
		OR @in_start_string = 'NULL'
        )
	            RETURN 'NULL'

    ELSE
    BEGIN

          SET @l_count = 0

          SET @l_exec_no = 0

          SET @v_strip_token = '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

          IF (@v_pre_separator = '?')
            SET @v_pre_separator = ' '

          IF (@v_replace_char = '?')
            SET @v_replace_char = ' '

          IF (@v_replacing_char = '?')
            SET @v_replacing_char = ' '

          IF (@in_max_length IS NULL)
            SET @v_max_length = 999
          ELSE 
            SET @v_max_length = @in_max_length

--  Start String Operations

          IF @v_token_seq = 1
				SET @v_start_string = isnull ( @in_start_string, ' ' )
          ELSE
          IF @v_pre_separator is NULL
				SET @v_start_string = isnull ( @in_start_string, ' ' )
          ELSE
				SET @v_start_string = isnull ( @in_start_string, ' ' ) + @v_pre_separator       


          SET @v_result_string = ltrim ( rtrim (@in_token_value ))

 

/*------------------------------------------------------------------------------------
       STRIP, REPLACE AND SUBSTRING CHARACTERS FOR STARTING STRING
-------------------------------------------------------------------------------------*/

   IF @in_start_pos > 1
   SET @v_result_string = ( substring ( @v_result_string, @in_start_pos, LEN ( @v_result_string) ) )


   IF (@v_replace_char IS NOT NULL)
    SET @v_result_string = replace(@v_result_string, @v_replace_char, @v_replacing_char)

   IF (ISNULL((@v_remove_spec_chars + '.'), '.') <> '.')
    BEGIN
      SET @v_strip_token = SYSDB.SSMA.SUBSTR3_VARCHAR(@v_strip_token, 1, SYSDB.SSMA.LENGTH_VARCHAR(@v_remove_spec_chars))
      SET @v_result_string = SYSDB.SSMA.TRANSLATE_VARCHAR(@v_result_string, @v_remove_spec_chars, @v_strip_token)
      SET @v_result_string = replace(@v_result_string, '!', '')
    END

   IF (@v_remove_spaces IS NOT NULL)
   SET @v_result_string = ( select replace(@v_result_string + isnull(null,'') ,' ','') )

   IF (@v_substr_length IS NOT NULL)
   BEGIN
    -- move SYSDB.SSMA.SUBSTR3_VARCHAR to common and recompile
    SET @v_result_string = SYSDB.SSMA.SUBSTR3_VARCHAR(@v_result_string, @v_substr_start_pos, @v_substr_length)
    -- move SYSDB.SSMA.LENGTH_VARCHAR to common and recompile
    END



    SET @v_result_string =  isnull ( ltrim ( rtrim (@v_result_string) ) , 'NULL' )


	IF     @v_result_string = ''
		OR @v_result_string is NULL
		OR @v_result_string = 'NULL'
        RETURN ( 'NULL' )

    ELSE
    BEGIN
         IF SYSDB.SSMA.LENGTH_VARCHAR( rtrim ( ltrim (
                  isnull(@v_start_string, '') + isnull(@v_result_string, ''))))
                       <= @v_max_length 
            
				   SET @v_return_string =  ( rtrim ( ltrim (
                          isnull(@v_start_string, '') 
						+ isnull(@v_result_string, ''))))
          ELSE 
            IF (@in_whole_tokens = 1)
              SET @v_return_string = @v_start_string
            ELSE 
                -- move SYSDB.SSMA.SUBSTR3_VARCHAR to common and recompile

              SET @v_return_string = SYSDB.SSMA.SUBSTR3_VARCHAR( ( rtrim ( ltrim ( 
                                            isnull(@v_start_string, '') 
                                          + isnull(@v_result_string, '')))), 
                                         1, ( @v_max_length ) )
--                                         1, ( @v_max_length + 1 ) ) was returning one too many
    END
  
/*************************************************************************************/
--  BUG IN YURI'S FUNCTION SYSDB.SSMA.SUBSTR3_VARCHAR.....
--    Was cutting off one too many characters ... if Max Length was 4 it was making truncating to 3 
--         ADDED 1 TO V_MAX_LENGTH TO TEMPORARILY FIX LENGTH PROBLEM
--  removed the add one because appears to be fixed now.....
/*************************************************************************************/

	END  -- ELSE in_token not NULL


    RETURN ( isnull ( ltrim ( rtrim (@v_return_string) ) , 'NULL' )  )

END




















