﻿CREATE TABLE [stage].[Import_Data_Exceptions] (
    [PACKAGE_FK]       INT           NOT NULL,
    [JOB_FK]           BIGINT        NOT NULL,
    [TARGET_TABLE]     VARCHAR (128) NOT NULL,
    [ACTION_FAILED]    VARCHAR (50)  NOT NULL,
    [ERROR_DESC]       VARCHAR (MAX) NULL,
    [ROW_VALUES]       VARCHAR (MAX) NULL,
    [PK_VALUES]        VARCHAR (MAX) NULL,
    [UPDATE_USER]      VARCHAR (50)  CONSTRAINT [DF_epacube.stage.Import_Data_Exceptions_UPDATE_USER] DEFAULT (suser_name()) NULL,
    [UPDATE_TIMESTAMP] DATETIME      CONSTRAINT [DF_epacube.stage.Import_Data_Exceptions_UPDATE_TIMESTAMP] DEFAULT (getdate()) NULL
);

