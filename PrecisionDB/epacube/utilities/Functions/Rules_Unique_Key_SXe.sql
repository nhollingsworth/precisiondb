﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [utilities].[Rules_Unique_Key_SXe]
(	
@Result_Data_Name_FK as varchar(16)
	, @LevelCode as varchar(8)
	, @Multiplier_Operation_FK as varchar(16)
	, @Discount_Operation_FK as varchar(16)
	, @Promo as varchar(8)
	, @Cust_Segment_Data_Name_FK as varchar(16)
	, @Cust_Segment_FK as varchar(16)
	, @Prod_Segment_Data_Name_FK as Varchar(16)
	, @Prod_Segment_FK as Varchar(16)
	, @Vendor_Segment_Data_Name_FK as varchar(16)
	, @Vendor_Segment_FK as Varchar(16)
	, @Org_Segment_Data_Name_FK as Varchar(16)
	, @Org_Segment_FK as Varchar(16)
	, @UOM_Code_FK as Varchar(16)
	, @Start_Date as date
)
RETURNS
@UniqueKey table
(
  UniqueKey varchar(max)
)
AS
Begin
	Declare @UK as varchar(Max)
	Set @UK = 
			Replace(Replace(Replace(Replace(Replace(
			@Result_Data_Name_FK 
			+ @LevelCode 
			+ @Multiplier_Operation_FK 
			+ @Discount_Operation_FK 
			+ @Promo 
			+ @Cust_Segment_Data_Name_FK
			+ @Cust_Segment_FK 
			+ @Prod_Segment_Data_Name_FK 
			+ @Prod_Segment_FK 
			+ @Vendor_Segment_Data_Name_FK 
			+ @Vendor_Segment_FK 
			+ @Org_Segment_Data_Name_FK 
			+ @Org_Segment_FK 
			+ @UOM_Code_FK 
			+ IsNull(Convert(varchar(8), @Start_Date, 1), '')
			, '''', ''), ' ', ''), 'Null', ''), ',', ''), '"', '')

	INSERT INTO @UniqueKey (UniqueKey) VALUES (@UK)
	Return
End


