﻿



CREATE FUNCTION [utilities].[Translate]
(
  @InputStr  varchar(max),
  @RemoveStr varchar(max),
  @RemoveChar char(1)
)
RETURNS
 varchar(max)

AS
/*----------------------------------------------------------------------
------------------------
' Function       : Translate
'
' Description    : Similar to Oracle's Translate function
'
' Change History :
'
' WHEN        WHO         WHAT
' 6/14/2006    Yuri Nogin  initial version
'-----------------------------------------------------------------------
-----------------------
'-----------------------------------------------------------------------
---------------------*/
BEGIN
  
  DECLARE @CurrentChar  char(1)
  DECLARE @len          bigint
  DECLARE @WorkStr      varchar(max)
  DECLARE @pos          bigint

  
  -- Get the number of chars to replace
  SET @len = len(@RemoveStr)
  SET @WorkStr = @InputStr
  SET @pos = 1

  
    -- Break up the string
    WHILE @len >= @pos
    BEGIN
   
      SET @CurrentChar = SUBSTRING(@RemoveStr,@pos,1)   
      SET @WorkStr = REPLACE(@WorkStr, @CurrentChar, @RemoveChar)
      SET @pos = @pos + 1       
 
    END

  
  RETURN @WorkStr

END
