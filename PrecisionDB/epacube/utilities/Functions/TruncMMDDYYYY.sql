﻿
-- =============================================
-- Author:		Walt Tucholski
-- Create date: 5/15/2006
-- Description:	Returns a datetime variable in mm/dd/yyyy format
-- =============================================
CREATE FUNCTION [utilities].[TruncMMDDYYYY]
  ( @fDate datetime )
RETURNS varchar(10)
AS
BEGIN
  RETURN ( CONVERT(varchar(10),@fDate,101) )
END
