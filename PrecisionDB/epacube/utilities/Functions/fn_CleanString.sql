﻿CREATE FUNCTION [utilities].[fn_CleanString]
(@inpStr NVARCHAR (MAX) NULL, @delim NVARCHAR (10) NULL, @sortStr NVARCHAR (4) NULL)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [SQLCLRCleanser].[SQLCLRCleanser.SQLCLRCleanser].[fn_CleanString]

