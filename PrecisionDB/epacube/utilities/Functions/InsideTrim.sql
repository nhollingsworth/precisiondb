﻿




CREATE FUNCTION [utilities].[InsideTrim]
(
  @List  varchar(max),
  @Delim varchar(10)
)
RETURNS
varchar(max)
AS
/*----------------------------------------------------------------------------------------------
' Function       : InsideTrim
'
' Description    : Split a string using the passed delimeter and concatenates it back
'
' Change History :
'
' WHEN        WHO         WHAT
' 3/3/2006    Yuri Nogin  initial version
'
'--------------------------------------------------------------------------------------------*/
BEGIN
  
  DECLARE @ListItem  varchar(max)
  DECLARE @Pos       int
  DECLARE @OutStr    varchar(max)
 
  SET @OutStr = ''
 
  -- Ensure we have a trailing delimiter
  IF RIGHT(RTRIM(@List),LEN(@Delim)) <> @Delim
    SET @List = LTRIM(RTRIM(@List))+ @Delim

  SET @Pos = CHARINDEX(@Delim, @List, 1)

  IF REPLACE(@List, @Delim, '') <> ''
  BEGIN

    -- Break up the string
    WHILE @Pos > 0
    BEGIN
      
      SET @ListItem = LTRIM(RTRIM(LEFT(@List, @Pos - 1)))
      
      IF @ListItem <> ''
        SET @OutStr = @OutStr + @ListItem + ' '

      SET @List = LTRIM(RTRIM(RIGHT(@List, LEN(@List) - @Pos - LEN(@Delim) + 1)))
      SET @Pos = CHARINDEX(@Delim, @List, 1)
      IF @Pos <= 0 AND LEN(@List) > 0 
         BEGIN
			SET @List = @List + @Delim
            SET @Pos = LEN(@List) + 1
         END 

    END

  END	
  RETURN RTrim(LTrim(@OutStr))

END
