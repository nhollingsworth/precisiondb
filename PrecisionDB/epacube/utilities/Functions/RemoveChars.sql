﻿CREATE FUNCTION [utilities].[RemoveChars]
(
  @InputStr  varchar(max),
  @RemoveChar int
)
RETURNS
 varchar(max)

AS
/*----------------------------------------------------------------------
------------------------
' Function       : RemoveChars
'
' Description    : removes chars bigger than RemoveChar spcified in ASCII number
'
' Change History :
'
' WHEN        WHO         WHAT
' 5/12/2008    Yuri Nogin  initial version
'-----------------------------------------------------------------------
-----------------------
'-----------------------------------------------------------------------
---------------------*/
BEGIN
  
  DECLARE @CurrentChar  char(1)
  DECLARE @len          bigint
  DECLARE @WorkStr      varchar(max)
  DECLARE @pos          bigint

  
  -- Get the number of chars to replace
  SET @len = len(@InputStr)
  SET @WorkStr = @InputStr
  SET @pos = 1

  
    -- Break up the string
    WHILE @len >= @pos
    BEGIN
   
      SET @CurrentChar = SUBSTRING(@InputStr,@pos,1)  
      If (ASCII(@CurrentChar) >=  @RemoveChar)
         SET @WorkStr = REPLACE(@WorkStr, @CurrentChar, '')
      SET @pos = @pos + 1       
 
    END

  
  RETURN @WorkStr

END
