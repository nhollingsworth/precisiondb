﻿




Create FUNCTION [utilities].[SplitStringSeq] (
  @List  varchar(max),
  @Delim varchar(10)
)
RETURNS
@ParsedList table
(
  ListItem varchar(max)
  , Seq Int
)
AS
/*----------------------------------------------------------------------
------------------------
' Function       : SplitString
'
' Description    : Split a string using the passed delimeter - returns a
Table variable
'
' Change History :
'
' WHEN        WHO         WHAT
' 3/3/2006    Yuri Nogin  initial version
'-----------------------------------------------------------------------
-----------------------
'-----------------------------------------------------------------------
---------------------*/
BEGIN
  
  DECLARE @ListItem  varchar(max)
  DECLARE @Pos       int
  DECLARE @Delim_rep     char(1)
  DECLARE @List_rep      varchar(max)
  Declare @i int
  Set @Delim_rep = Char(7) --bell non-visual character
  Set @List_rep = Replace(@List,@Delim,@Delim_rep)
  Set @i = 1
  
  -- Ensure we have a trailing delimiter
  IF RIGHT(RTRIM(@List_rep),LEN(@Delim_rep)) <> @Delim_rep
    SET @List_rep = LTRIM(RTRIM(@List_rep))+ @Delim_rep

  SET @Pos = CHARINDEX(@Delim_rep, @List_rep, 1)

  IF REPLACE(@List_rep, @Delim_rep, '') <> ''
  BEGIN

    -- Break up the string
    WHILE @Pos > 0
    BEGIN
      
      SET @ListItem = LTRIM(RTRIM(LEFT(@List_rep, @Pos - 1)))
      
      IF @ListItem <> ''
        INSERT INTO @ParsedList (ListItem, Seq) VALUES (@ListItem, @i)

      SET @List_rep = LTRIM(RTRIM(RIGHT(@List_rep, LEN(@List_rep) - @Pos - LEN(@Delim_rep) + 1)))
      SET @Pos = CHARINDEX(@Delim_rep, @List_rep, 1)
      IF @Pos <= 0 AND LEN(@List_rep) > 0 
         BEGIN
			SET @List_rep = @List_rep + @Delim_rep
            SET @Pos = LEN(@List_rep) + 1
         END 
	Set @i = @i + 1
    END

  END	
  RETURN

END


