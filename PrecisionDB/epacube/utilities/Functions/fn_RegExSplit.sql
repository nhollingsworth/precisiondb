﻿CREATE FUNCTION [utilities].[fn_RegExSplit]
(@inpstr NVARCHAR (MAX) NULL, @delim NVARCHAR (MAX) NULL, @regexstr NVARCHAR (MAX) NULL)
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [SQLCLRCleanser].[SQLCLRCleanser.SQLCLRCleanser].[fn_RegExSplit]

