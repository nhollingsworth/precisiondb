﻿


CREATE FUNCTION [utilities].[TRUNC](@date_t as DATETIME)
returns DATETIME
begin
    return DATEADD(hh, -DATEPART(hh, @date_t), 
                DATEADD(mi, -DATEPART(mi, @date_t), 
                    DATEADD(ss, -DATEPART(ss, @date_t), 
                    DATEADD(ms, -DATEPART(ms, @date_t), @date_t))));
end

