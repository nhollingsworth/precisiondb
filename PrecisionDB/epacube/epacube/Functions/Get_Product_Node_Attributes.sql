﻿

CREATE FUNCTION [epacube].[Get_Product_Node_Attributes]
    (
      @in_product_structure_fk BIGINT      
    )
RETURNS @TaxTab TABLE
    (
       TAXONOMY_TREE_FK INT 
      ,TAXONOMY_NODE_FK INT    
      ,LEVEL_SEQ   SMALLINT 
      ,TAXONOMY_NODE_ATTRIBUTE_FK  BIGINT 
      ,PRODUCT_STRUCTURE_FK BIGINT 
      ,DATA_NAME_FK   INT 
    )
AS BEGIN


    INSERT  INTO @TaxTab
		SELECT 
		       B.TAXONOMY_TREE_FK
		      ,B.TAXONOMY_NODE_FK      
		      ,B.LEVEL_SEQ
		      ,B.TAXONOMY_NODE_ATTRIBUTE_FK      
		      ,B.PRODUCT_STRUCTURE_FK
		      ,B.DATA_NAME_FK     
		FROM (
		SELECT 
		       A.TAXONOMY_TREE_FK
		      ,A.TAXONOMY_NODE_FK      
		      ,A.LEVEL_SEQ
		      ,A.TAXONOMY_NODE_ATTRIBUTE_FK      
		      ,A.PRODUCT_STRUCTURE_FK
		      ,A.DATA_NAME_FK     
		      ,DENSE_RANK() OVER ( PARTITION BY A.TAXONOMY_TREE_FK, A.PRODUCT_STRUCTURE_FK, A.DATA_NAME_FK
		                    ORDER BY  A.LEVEL_SEQ DESC, A.TAXONOMY_NODE_ATTRIBUTE_FK ) AS DRANK
		FROM (
		SELECT 
		       pc.PRODUCT_STRUCTURE_FK
		      ,tna.DATA_NAME_FK
		      ,tna.TAXONOMY_NODE_ATTRIBUTE_ID AS TAXONOMY_NODE_ATTRIBUTE_FK
		      ,tna.TAXONOMY_NODE_FK
		      ,tt.TAXONOMY_TREE_ID AS TAXONOMY_TREE_FK
		      ,NODE.LEVEL_SEQ
		--
		FROM    epacube.PRODUCT_CATEGORY PC
		        INNER JOIN epacube.TAXONOMY_TREE TT ON ( TT.DATA_NAME_FK = PC.DATA_NAME_FK )
		        CROSS APPLY epacube.Get_Taxonomy_Nodes(pc.PRODUCT_STRUCTURE_FK,  tt.TAXONOMY_TREE_ID) AS node
		        INNER JOIN epacube.TAXONOMY_NODE_ATTRIBUTE tna ON ( tna.TAXONOMY_NODE_FK = node.TAXONOMY_NODE_FK )
		WHERE   pc.product_structure_fk = @in_product_structure_fk
		) A
		) B
		WHERE B.DRANK = 1
		
                            
    RETURN
   END ;
