﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [epacube].[getParamDataNameId]
(
	-- Add the parameters for the function here
    @SCOPE varchar(50)
	,@PARAM varchar(50)
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @id int
	
	if isnumeric(@scope)=1
	begin

		select @id=cast(value as int) from epacube.epacube_params
		where name = @PARAM and application_scope_fk=(cast(@scope as int))

	end
	else

		select @id=cast(value as int) from epacube.epacube_params
		where name = @PARAM and application_scope_fk=(select application_scope_id from epacube.application_scope where scope=@scope)

	-- Return the result of the function
	RETURN @id

END


