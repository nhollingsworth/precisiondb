﻿
-- Function created by Geoff Begley
--
--
-- Purpose: This function is called in the UI in the Product Orgs Editor
-- It returns the current value for a Product Org Attribute in the editing window.
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- GB					  Initial SQL Version
-- RG		 12/17/2007	  EPA-1436 Current values not returning for Product UOM Class types
-- KS		 11/04/2008	  EPA-1950 Include Product_Tax_Attribute, Product_Status, entity_structure_fk
-- KS		 12/21/2008	  
-- GB		 06/08/2009	  EPA-2377 Discriminate associated entity values by specified association, The forth parameter was added after the fact...


CREATE
    FUNCTION [epacube].[getCurrentValue] 
      (
        @in_product_structure_fk         bigint
       ,@in_org_entity_structure_fk      BIGINT   = 1    -- default to CORP entity
       ,@in_data_name_fk                 INT  
       ,@in_entity_structure_fk          BIGINT   = 0    -- default to UNKNOWN entity
      ) 
      RETURNS varchar(256)
    AS
BEGIN

        DECLARE 

          @l_count bigint,
          @ls_exec varchar(max),
          @l_exec_no bigint,
          @v_value varchar(256)


  SET @v_value = 
      ( SELECT 
			  CASE ds.event_type_cr_fk
---
--- KS ADDED 07/31/2009  FOR QUICK FIX FOR EDITOR CALL  
---						 Specifically UI Editor NOT SENDING ENTITY_STRUCTURE_FK on PROD/VND/WHSE
---						 Issue is that Value is on the Prod/WHSE UI.. so have to infer the following
---							When entity_structure_fk is NULL then do not include in WHERE CLAUSE
---							Assumption is that it will be okay because ONLY ASSOC one per WHSE
---
        WHEN 155 THEN 
		CASE WHEN ( ISNULL ( es.entity_structure_id, 0 ) = 0 )
		THEN 			( SELECT EI.VALUE
			              FROM EPACUBE.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)			          
			              INNER JOIN EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
			                ON ( EI.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK
			                AND  EI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
			                                        FROM EPACUBE.EPACUBE_PARAMS EEP
			                                        WHERE EEP.APPLICATION_SCOPE_FK = 100
			                                        AND   EEP.NAME = ( SELECT CODE 
			                                                           FROM epacube.CODE_REF
			                                                           WHERE CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
			             ) )
			            WHERE PAS.RECORD_STATUS_CR_FK = 1
			            AND   PAS.product_structure_fk = ps.product_structure_id
						AND   PAS.data_name_fk = dn.data_name_id 
						AND   PAS.org_entity_structure_fk = eso.entity_structure_id )
        ELSE
						( SELECT EI.VALUE
			              FROM EPACUBE.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)			          
			              INNER JOIN EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
			                ON ( EI.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK
			                AND  EI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
			                                        FROM EPACUBE.EPACUBE_PARAMS EEP
			                                        WHERE EEP.APPLICATION_SCOPE_FK = 100
			                                        AND   EEP.NAME = ( SELECT CODE 
			                                                           FROM epacube.CODE_REF
			                                                           WHERE CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
			             ) )
			            WHERE PAS.RECORD_STATUS_CR_FK = 1
			            AND   PAS.product_structure_fk = ps.product_structure_id
						AND   PAS.data_name_fk = dn.data_name_id 
						AND   PAS.entity_structure_fk = es.entity_structure_id  
						AND   PAS.org_entity_structure_fk = eso.entity_structure_id )
	   END --- ( ISNULL ( es.entity_structure_id, 0 )
---			  			
---------------------------------------------------------------------------------------------			
-- EPA-1950 Modification below	
--   Have to have TOP 1 because the same data name may be on multiple trees	
---------------------------------------------------------------------------------------------
             WHEN 153 THEN ( select TOP 1 attribute_event_data   
								from epacube.PRODUCT_TAX_ATTRIBUTE pat
								where PAT.RECORD_STATUS_CR_FK = 1
								AND   pat.data_name_fk = dn.data_name_id 
								AND   pat.product_structure_fk = ps.product_structure_id   )
----------------------------------- END EPA-1950 Mod
--  
			  WHEN 150 THEN
			     CASE dn.data_name_id 
			     WHEN (select data_name_id from epacube.data_name
						where name = 'EPACUBE STATUS')
				 THEN ( SELECT cr.code
			                     FROM epacube.code_ref cr
			                     WHERE cr.code_ref_id = ps.product_status_cr_fk )
			     ELSE 
				 CASE ds.table_name
				 WHEN 'PRODUCT_CATEGORY'
					   THEN CASE ds.data_type_cr_fk
							WHEN 134 
							THEN  ( select dv.value
									from epacube.product_category pc 
									inner join epacube.data_value dv 
									   on ( dv.data_value_id = pc.data_value_fk )
									where PC.RECORD_STATUS_CR_FK = 1
									AND   pc.data_name_fk = dn.data_name_id
									AND   pc.product_structure_fk = ps.product_structure_id
									and   pc.org_entity_structure_fk = eso.entity_structure_id  
									and   ISNULL ( pc.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 ) 
									)
							WHEN 135 
							THEN  ( select edv.value
									from epacube.product_category pc 
									inner join epacube.entity_data_value edv 
									   on ( edv.entity_data_value_id = pc.entity_data_value_fk )
									where PC.RECORD_STATUS_CR_FK = 1
									AND   pc.data_name_fk = dn.data_name_id
									AND   pc.product_structure_fk = ps.product_structure_id
									and   pc.org_entity_structure_fk = eso.entity_structure_id  
									and   ISNULL ( pc.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 ) 
									)
						   END
				 WHEN 'PRODUCT_ATTRIBUTE'
					   THEN CASE ds.data_type_cr_fk
							WHEN 134 
							THEN  ( select dv.value
									from epacube.product_attribute pa 
									inner join epacube.data_value dv 
									   on ( dv.data_value_id = pa.data_value_fk )
									where PA.RECORD_STATUS_CR_FK = 1
									AND   pa.data_name_fk = dn.data_name_id 
									and	  pa.product_structure_fk = ps.product_structure_id
									and   pa.org_entity_structure_fk = eso.entity_structure_id 									
									and   ISNULL ( pa.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 ) 
									)
							WHEN 135 
							THEN  ( select edv.value
									from epacube.product_attribute pa
									inner join epacube.entity_data_value edv 
									   on ( edv.entity_data_value_id = pa.entity_data_value_fk )
									where PA.RECORD_STATUS_CR_FK = 1
									AND   pa.data_name_fk = dn.data_name_id 
									and	  pa.product_structure_fk = ps.product_structure_id
									and   pa.org_entity_structure_fk = eso.entity_structure_id 									
									and   ISNULL ( pa.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 ) 
									)
							ELSE ( select pa.attribute_event_data
									from epacube.product_attribute pa 
									where PA.RECORD_STATUS_CR_FK = 1
									AND   pa.data_name_fk = dn.data_name_id 
									and	  pa.product_structure_fk = ps.product_structure_id
									and   pa.org_entity_structure_fk = eso.entity_structure_id 									
									and   ISNULL ( pa.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 ) 
									)
						   END
				  WHEN 'PRODUCT_IDENTIFICATION'
						THEN  ( select pi.value
								from epacube.product_identification PI
								where PI.RECORD_STATUS_CR_FK = 1
								AND   pi.data_name_fk = dn.data_name_id 
								and	  pi.product_structure_fk = ps.product_structure_id
								and   pi.org_entity_structure_fk = eso.entity_structure_id 									
								and   ISNULL ( pi.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 ) 
								)
				  WHEN 'PRODUCT_IDENT_MULT'
						THEN  ( select pi.value
								from epacube.product_ident_mult PI
								where PI.RECORD_STATUS_CR_FK = 1
								AND   pi.data_name_fk = dn.data_name_id 
								and	  pi.product_structure_fk = ps.product_structure_id
								and   pi.org_entity_structure_fk = eso.entity_structure_id 									
								and   ISNULL ( pi.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 ) 
								)								
				  WHEN 'PRODUCT_IDENT_NONUNIQUE'
						THEN  ( select pi.value
								from epacube.product_ident_nonunique PI
								where PI.RECORD_STATUS_CR_FK = 1
								AND   pi.data_name_fk = dn.data_name_id 
								and	  pi.product_structure_fk = ps.product_structure_id
								and   pi.org_entity_structure_fk = eso.entity_structure_id 									
								and   ISNULL ( pi.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 ) 
								)																
				  WHEN 'PRODUCT_DESCRIPTION'
						THEN  ( select pd.description
								from epacube.product_description pd
								where PD.RECORD_STATUS_CR_FK = 1
								AND   pd.data_name_fk = dn.data_name_id 
								and	  pd.product_structure_fk = ps.product_structure_id
								and   pd.org_entity_structure_fk = eso.entity_structure_id 									
								and   ISNULL ( pd.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 )
								 )
----------------------------------- END EPA-1436 Mod								
				  WHEN 'PRODUCT_STATUS'
						THEN  ( select dv.value
									from epacube.product_status pst
									inner join epacube.data_value dv 
									   on ( dv.data_value_id = pst.data_value_fk )
									where PST.RECORD_STATUS_CR_FK = 1
								    AND   pst.data_name_fk = dn.data_name_id
									AND   pst.product_structure_fk = ps.product_structure_id
									and   pst.org_entity_structure_fk = eso.entity_structure_id  
									and   ISNULL ( pst.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 ) 
									)
				  END 	
			    END 	
			    
			  WHEN 156 THEN 
			  CASE ds.table_name
---------------------------------------------------------------------------------------------			
-- UOM CODES from TABLE PRODUCT_UOM_CLASS			
---------------------------------------------------------------------------------------------
				  WHEN 'PRODUCT_UOM_CLASS'
						THEN  ( select (select uom_code from epacube.uom_code
									    where uom_code_id = puc.uom_code_fk)
								from epacube.product_uom_class puc
								where PUC.RECORD_STATUS_CR_FK = 1
								AND   puc.data_name_fk = dn.data_name_id 
								and	  puc.product_structure_fk = ps.product_structure_id
								and   puc.org_entity_structure_fk = eso.entity_structure_id 									
								and   ISNULL ( puc.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 ) 
								)
		          ELSE NULL
		          END
			  WHEN 151 THEN
					CASE ds.column_name
                         WHEN 'NET_VALUE1' THEN
                          ( select top 1 cast ( pv.net_value1 as varchar (20) )
							from epacube.product_values pv
							where PV.RECORD_STATUS_CR_FK = 1
							AND   pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   ISNULL ( pv.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 )
							)
                         WHEN 'NET_VALUE2' THEN
                          ( select top 1 cast ( pv.net_value2 as varchar (20) )
							from epacube.product_values pv
							where PV.RECORD_STATUS_CR_FK = 1
							AND   pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( pv.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 )
							)
                         WHEN 'NET_VALUE3' THEN
                          ( select top 1 cast ( pv.net_value3 as varchar (20) )
							from epacube.product_values pv
							where PV.RECORD_STATUS_CR_FK = 1
							AND   pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( pv.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 )
							)
                         WHEN 'NET_VALUE4' THEN
                          ( select top 1 cast ( pv.net_value4 as varchar (20) )
							from epacube.product_values pv
							where PV.RECORD_STATUS_CR_FK = 1
							AND   pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( pv.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 )
							)
                         WHEN 'NET_VALUE5' THEN
                          ( select top 1 cast ( pv.net_value5 as varchar (20) )
							from epacube.product_values pv
							where PV.RECORD_STATUS_CR_FK = 1
							AND   pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( pv.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 )
							)
                         WHEN 'NET_VALUE6' THEN
                          ( select top 1 cast ( pv.net_value6 as varchar (20) )
							from epacube.product_values pv
							where PV.RECORD_STATUS_CR_FK = 1
							AND   pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( pv.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 )
							)
                         WHEN 'NET_VALUE7' THEN
                          ( select top 1 cast ( pv.net_value7 as varchar (20) )
							from epacube.product_values pv
							where PV.RECORD_STATUS_CR_FK = 1
							AND   pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( pv.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 )
							)
                         WHEN 'NET_VALUE8' THEN
                          ( select top 1 cast ( pv.net_value8 as varchar (20) )
							from epacube.product_values pv
							where PV.RECORD_STATUS_CR_FK = 1
							AND   pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( pv.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 )
							)
                         WHEN 'NET_VALUE9' THEN
                          ( select top 1 cast ( pv.net_value9 as varchar (20) )
							from epacube.product_values pv
							where PV.RECORD_STATUS_CR_FK = 1
							AND   pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( pv.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 )
							)
                         WHEN 'NET_VALUE10' THEN
                          ( select top 1 cast ( pv.net_value10 as varchar (20) )
							from epacube.product_values pv
							where PV.RECORD_STATUS_CR_FK = 1
							AND   pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( pv.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 )
							)
                    END
			  WHEN 152 THEN
					CASE ds.column_name
                         WHEN 'NET_VALUE1' THEN
					      ( select top 1 cast ( sr.net_value1 as varchar (20) )
							from marginmgr.sheet_results sr
							where SR.RECORD_STATUS_CR_FK = 1
							AND   sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( sr.entity_structure_fk, 0 ) = ISNULL ( es.entity_structure_id, 0 )
							)
                         WHEN 'NET_VALUE2' THEN
					      ( select top 1 cast ( sr.net_value2 as varchar (20) )
							from marginmgr.sheet_results sr	
							where SR.RECORD_STATUS_CR_FK = 1													
							AND   sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( sr.entity_structure_fk, 0 ) = ISNULL ( sr.entity_structure_fk, 0 )
							)
                         WHEN 'NET_VALUE3' THEN
					      ( select top 1 cast ( sr.net_value3 as varchar (20) )
							from marginmgr.sheet_results sr
							where SR.RECORD_STATUS_CR_FK = 1
							AND   sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( sr.entity_structure_fk, 0 ) = ISNULL ( sr.entity_structure_fk, 0 )
							)
                         WHEN 'NET_VALUE4' THEN
					      ( select top 1 cast ( sr.net_value4 as varchar (20) )
							from marginmgr.sheet_results sr
							where SR.RECORD_STATUS_CR_FK = 1
							AND   sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( sr.entity_structure_fk, 0 ) = ISNULL ( sr.entity_structure_fk, 0 )
							)
                         WHEN 'NET_VALUE5' THEN
					      ( select top 1 cast ( sr.net_value5 as varchar (20) )
							from marginmgr.sheet_results sr
							where SR.RECORD_STATUS_CR_FK = 1
							AND   sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( sr.entity_structure_fk, 0 ) = ISNULL ( sr.entity_structure_fk, 0 )
							)
                         WHEN 'NET_VALUE6' THEN
					      ( select top 1 cast ( sr.net_value6 as varchar (20) )
							from marginmgr.sheet_results sr
							where SR.RECORD_STATUS_CR_FK = 1
							AND   sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( sr.entity_structure_fk, 0 ) = ISNULL ( sr.entity_structure_fk, 0 )
							)
                         WHEN 'NET_VALUE7' THEN
					      ( select top 1 cast ( sr.net_value7 as varchar (20) )
							from marginmgr.sheet_results sr
							where SR.RECORD_STATUS_CR_FK = 1
							AND   sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( sr.entity_structure_fk, 0 ) = ISNULL ( sr.entity_structure_fk, 0 )
							)
                         WHEN 'NET_VALUE8' THEN
					      ( select top 1 cast ( sr.net_value8 as varchar (20) )
							from marginmgr.sheet_results sr
							where SR.RECORD_STATUS_CR_FK = 1
							AND   sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( sr.entity_structure_fk, 0 ) = ISNULL ( sr.entity_structure_fk, 0 )
							)
                         WHEN 'NET_VALUE9' THEN
					      ( select top 1 cast ( sr.net_value9 as varchar (20) )
							from marginmgr.sheet_results sr
							where SR.RECORD_STATUS_CR_FK = 1
							AND   sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( sr.entity_structure_fk, 0 ) = ISNULL ( sr.entity_structure_fk, 0 )
							)
                         WHEN 'NET_VALUE10' THEN
					      ( select top 1 cast ( sr.net_value10 as varchar (20) )
							from marginmgr.sheet_results sr
							where SR.RECORD_STATUS_CR_FK = 1
							AND   sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = eso.entity_structure_id
							and   ISNULL ( sr.entity_structure_fk, 0 ) = ISNULL ( sr.entity_structure_fk, 0 )
							)
                    END
        END
		FROM epacube.data_name dn 
		INNER JOIN epacube.data_set ds on ( dn.data_set_fk = ds.data_set_id )
		INNER JOIN epacube.product_structure ps
		   ON ( dn.record_status_cr_fk = ps.record_status_cr_fk )
		INNER JOIN epacube.entity_structure eso
		   ON ( ps.record_status_cr_fk = eso.record_status_cr_fk )
		INNER JOIN epacube.entity_structure es
		   ON ( ps.record_status_cr_fk = es.record_status_cr_fk )		   
		WHERE dn.data_name_id = @in_data_name_fk
		AND   ps.product_structure_id = @in_product_structure_fk
		AND   eso.entity_structure_id = ISNULL ( @in_org_entity_structure_fk, 1 )
		AND   es.entity_structure_id = ISNULL ( @in_entity_structure_fk, 0 )
   )

   RETURN ( @v_value )
END
