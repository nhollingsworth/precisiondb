﻿
---CINDY FIXED IN QA but not moved to NTSDB since VENDOR and WAREHOUSE 
--- KS.. restructured to be more performant

CREATE
    FUNCTION [epacube].[getEntityIdent] 
      (
        @in_Entity_Structure_ID		bigint

      ) 
      RETURNS varchar(256)
    AS
BEGIN

        DECLARE 

          @l_count bigint,
          @ls_exec varchar(max),
          @l_exec_no bigint,
          @v_class int,
          @v_ident varchar(256)

------------------------------------------------------------------------------------------------------------
--Get Display Default Identification for entity structure input
------------------------------------------------------------------------------------------------------------
	SET @v_ident = 
	(
	
	SELECT CASE ES.ENTITY_CLASS_CR_FK
	WHEN 10101 THEN CASE @in_Entity_Structure_ID
	                WHEN 1 THEN 'ALL WAREHOUSES'
	                ELSE (select ei.value from epacube.entity_identification ei
							 inner join epacube.epacube_params eep on 
							  ( cast ( eep.value as int ) = ei.data_name_fk
								and  eep.name = 'WAREHOUSE' and eep.application_scope_fk = 100 )
							 where entity_structure_fk=@in_entity_structure_id )
	                END 
	WHEN 10102 THEN CASE @in_Entity_Structure_ID
	                WHEN 2 THEN 'ALL SYSTEMS'
	                ELSE (select ei.value from epacube.entity_identification ei
							 inner join epacube.epacube_params eep on 
							  ( cast ( eep.value as int ) = ei.data_name_fk
								and  eep.name = 'SYSTEM' and eep.application_scope_fk = 100 )
							 where entity_structure_fk=@in_entity_structure_id )
	                END 
	
	WHEN 10103 THEN CASE @in_Entity_Structure_ID
	                WHEN 3 THEN 'ALL VENDORS'
	                ELSE (select ei.value from epacube.entity_identification ei
							 inner join epacube.epacube_params eep on 
							  ( cast ( eep.value as int ) = ei.data_name_fk
								and  eep.name = 'VENDOR' and eep.application_scope_fk = 100 )
							 where entity_structure_fk=@in_entity_structure_id )
	                END 
	
	WHEN 10104 THEN CASE @in_Entity_Structure_ID
	                WHEN 4 THEN 'ALL CUSTOMERS'
	                ELSE (select ei.value from epacube.entity_identification ei
							 inner join epacube.epacube_params eep on 
							  ( cast ( eep.value as int ) = ei.data_name_fk
								and  eep.name = 'CUSTOMER' and eep.application_scope_fk = 100 )
							 where entity_structure_fk=@in_entity_structure_id )
	                END 
	
	WHEN 10105 THEN CASE @in_Entity_Structure_ID
	                WHEN 5 THEN 'ALL SALESREPS'
	                ELSE (select ei.value from epacube.entity_identification ei
							 inner join epacube.epacube_params eep on 
							  ( cast ( eep.value as int ) = ei.data_name_fk
								and  eep.name = 'SALESREP' and eep.application_scope_fk = 100 )
							 where entity_structure_fk=@in_entity_structure_id )
	                END
	
	WHEN 10106 THEN CASE @in_Entity_Structure_ID
	                WHEN 6 THEN 'ALL TERRITORIES'
	                ELSE (select ei.value from epacube.entity_identification ei
							 inner join epacube.epacube_params eep on 
							  ( cast ( eep.value as int ) = ei.data_name_fk
								and  eep.name = 'TERRITORY' and eep.application_scope_fk = 100 )
							 where entity_structure_fk=@in_entity_structure_id )
	                END
	
	WHEN 10107 THEN CASE @in_Entity_Structure_ID
	                WHEN 7 THEN 'ALL BUYERS'
	                ELSE (select ei.value from epacube.entity_identification ei
							 inner join epacube.epacube_params eep on 
							  ( cast ( eep.value as int ) = ei.data_name_fk
								and  eep.name = 'BUYER' and eep.application_scope_fk = 100 )
							 where entity_structure_fk=@in_entity_structure_id )
	                END
	
	WHEN 10108 THEN CASE @in_Entity_Structure_ID
	                WHEN 8 THEN 'ALL PRICE MGRS'
	                ELSE (select ei.value from epacube.entity_identification ei
							 inner join epacube.epacube_params eep on 
							  ( cast ( eep.value as int ) = ei.data_name_fk
								and  eep.name = 'PRICE MGR' and eep.application_scope_fk = 100 )
							 where entity_structure_fk=@in_entity_structure_id )
	                END

	WHEN 10111 THEN CASE @in_Entity_Structure_ID
	                WHEN 10 THEN 'ALL PROFIT CENTERS'
	                ELSE (select ei.value from epacube.entity_identification ei
							 inner join epacube.epacube_params eep on 
							  ( cast ( eep.value as int ) = ei.data_name_fk
								and  eep.name = 'PROFIT CENTER' and eep.application_scope_fk = 100 )
							 where entity_structure_fk=@in_entity_structure_id )
	                END
	WHEN 10114 THEN CASE @in_Entity_Structure_ID
	                WHEN 18 THEN 'ALL CUSTOMER ADDRESS TYPE'
	                ELSE (select ei.value from epacube.entity_identification ei
							 inner join epacube.epacube_params eep on 
							  ( cast ( eep.value as int ) = ei.data_name_fk
								and  eep.name = 'CUSTOMER ADDRESS TYPE' and eep.application_scope_fk = 100 )
							 where entity_structure_fk=@in_entity_structure_id )
	                END
	
	ELSE NULL
	END
	-----
	FROM EPACUBE.ENTITY_STRUCTURE ES
	WHERE ES.ENTITY_STRUCTURE_ID = @in_Entity_Structure_ID		
	
	)
	
	
/*	
	
	SET @v_ident = 
        (SELECT 
           CASE ( select entity_class_cr_fk
                  from epacube.entity_structure WITH (NOLOCK)
                  where entity_structure_id = @in_entity_structure_id )
           WHEN (select code_ref_id from epacube.code_ref 
					where code_type = 'ENTITY_CLASS'
					and code = 'WAREHOUSE')   
				THEN CASE ( @in_entity_structure_id )
                     WHEN 1 THEN 'ALL WAREHOUSES'
                     ELSE
                    (select ei.value from epacube.entity_identification ei
                     inner join epacube.epacube_params eep on 
                      ( cast ( eep.value as int ) = ei.data_name_fk
                        and  eep.name = 'WAREHOUSE' and eep.application_scope_fk = 100 )
                     where entity_structure_fk=@in_entity_structure_id )
                END
           WHEN (select code_ref_id from epacube.code_ref 
					where code_type = 'ENTITY_CLASS'
					and code = 'SYSTEM')
				THEN CASE ( @in_entity_structure_id )
                     WHEN (select entity_structure_id from epacube.entity_structure
							where data_name_fk =
							(select data_name_id from epacube.data_name
							where name = 'ALL SYSTEMS')
							and entity_structure_id < 10) --added CV
					 THEN 'ALL SYSTMS'
                     ELSE
					(select ei.value from epacube.entity_identification ei
                     inner join epacube.epacube_params eep on 
                      ( cast ( eep.value as int ) = ei.data_name_fk
                        and  eep.name = 'SYSTEM' and eep.application_scope_fk = 100 )
                     where entity_structure_fk=@in_entity_structure_id )
                END
           WHEN (select code_ref_id from epacube.code_ref 
					where code_type = 'ENTITY_CLASS'
					and code = 'CUSTOMER')
				THEN CASE ( @in_entity_structure_id )
                     WHEN (select entity_structure_id from epacube.entity_structure
							where data_name_fk =
							(select data_name_id from epacube.data_name
							where name = 'ALL CUSTOMERS')
							and entity_structure_id < 10) --added CV
					 THEN 'ALL CUSTOMERS'
                     ELSE
					(select ei.value from epacube.entity_identification ei
                     inner join epacube.epacube_params eep on 
                      ( cast ( eep.value as int ) = ei.data_name_fk
                        and  eep.name = 'CUSTOMER' and eep.application_scope_fk = 100 )
                     where entity_structure_fk=@in_entity_structure_id )
                END
           WHEN (select code_ref_id from epacube.code_ref 
					where code_type = 'ENTITY_CLASS'
					and code = 'VENDOR')
				THEN CASE ( @in_entity_structure_id )
                     WHEN (select entity_structure_id from epacube.entity_structure
							where data_name_fk =
							(select data_name_id from epacube.data_name
							where name = 'ALL VENDORS')
							and entity_structure_id < 10) --added CV
					 THEN 'ALL VENDORS'
                     ELSE
					(select ei.value from epacube.entity_identification ei
                     inner join epacube.epacube_params eep on 
                      ( cast ( eep.value as int ) = ei.data_name_fk
                        and  eep.name = 'VENDOR' and eep.application_scope_fk = 100 )
                     where entity_structure_fk=@in_entity_structure_id )
                END
           WHEN (select code_ref_id from epacube.code_ref 
					where code_type = 'ENTITY_CLASS'
					and code = 'TERRITORY')
				THEN CASE ( @in_entity_structure_id )
                     WHEN (select entity_structure_id from epacube.entity_structure
							where data_name_fk =
							(select data_name_id from epacube.data_name
							where name = 'ALL TERRITORIES')
							and entity_structure_id < 10) --added CV
					 THEN 'ALL TERRITORIES'
                     ELSE
					(select ei.value from epacube.entity_identification ei
                     inner join epacube.epacube_params eep on 
                      ( cast ( eep.value as int ) = ei.data_name_fk
                        and  eep.name = 'TERRITORY' and eep.application_scope_fk = 100 )
                     where entity_structure_fk=@in_entity_structure_id )
                END
           WHEN (select code_ref_id from epacube.code_ref 
					where code_type = 'ENTITY_CLASS'
					and code = 'SALESREP')
				THEN CASE ( @in_entity_structure_id )
                     WHEN (select entity_structure_id from epacube.entity_structure
							where data_name_fk =
							(select data_name_id from epacube.data_name
							where name = 'ALL SALESREPS')
							and entity_structure_id < 10) --added CV
					 THEN 'ALL SALESREPS'
                     ELSE
					(select ei.value from epacube.entity_identification ei
                     inner join epacube.epacube_params eep on 
                      ( cast ( eep.value as int ) = ei.data_name_fk
                        and  eep.name = 'SALESREP' and eep.application_scope_fk = 100 )
                     where entity_structure_fk=@in_entity_structure_id )
                END
		   WHEN (select code_ref_id from epacube.code_ref 
					where code_type = 'ENTITY_CLASS'
					and code = 'BUYER')
				THEN CASE ( @in_entity_structure_id )
                     WHEN (select entity_structure_id from epacube.entity_structure
							where data_name_fk =
							(select data_name_id from epacube.data_name
							where name = 'ALL BUYERS')
							and entity_structure_id < 10) --added CV
					 THEN 'ALL BUYERS'
                     ELSE
					(select ei.value from epacube.entity_identification ei
                     inner join epacube.epacube_params eep on 
                      ( cast ( eep.value as int ) = ei.data_name_fk
                        and  eep.name = 'BUYER' and eep.application_scope_fk = 100 )
                     where entity_structure_fk=@in_entity_structure_id )
                END
        END ) -- IDENTIFICATION

*/

   RETURN ( @v_ident )
END
