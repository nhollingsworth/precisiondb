﻿
-- =============================================
-- Author:		Geoff Begley
-- Create date: 4/28/2008
-- Description:	quick fetch for code from code_ref_id
-- =============================================
CREATE FUNCTION [epacube].[getCodeForId]
(
	@codeRefId int
)
RETURNS varchar(32)
AS
BEGIN
	DECLARE @c varchar(32)

    select @c=code from epacube.code_ref
	where code_ref_id=@codeRefId

	RETURN @c
END

