﻿

/*

KS ---- DO NOT USE THE PROCEDURE FOR NOW... NEED TO REVISIT IT  !!!!




*/



CREATE FUNCTION [epacube].[ADD_BASIS_TOKEN_BY_SUBSTRING] (
   @in_start_string       varchar(8000),
   @in_prefix             varchar (16),
   @in_max_length         int,
   @in_whole_tokens       int,
   @in_rules_action_strfunction_fk      bigint,
   @in_start_pos          int,
   @in_token_value        varchar(8000) )
   RETURNS varchar(8000)
 AS
--  In Debug Mode return the iterative string @v_debug_string
--  instead of just the normal@v_return_string
 BEGIN
   DECLARE       
   @l_count               bigint,
   @ls_exec               varchar (max),
   @l_exec_no             bigint, 
   @ls_tokens             varchar(max),
   @v_start_string        varchar (8000),
   @v_return_token        varchar (8000),
   @v_return_string       varchar (8000),
   @v_result_string       varchar (8000),
   @v_debug_string        varchar (max),
   @v_strip_token         varchar (128),
   @v_string_length       bigint,
   @v_string_complete     bigint,   
   @v_token_seq           int,  
   @v_remove_spec_chars   varchar (64),
   @v_replace_char        char (1),
   @v_replacing_char      char (1),
   @v_substr_start_pos    bigint,
   @v_substr_length       bigint,
   @v_trunc_first_token   smallint,
   @v_max_length		  bigint,
   @j                     tinyint
   

      SELECT 
          @v_token_seq = RASF.BASIS_POSITION,  --- TOKEN_SEQ,
          @v_remove_spec_chars = RASF.REMOVE_SPEC_CHARS_IND,   ---REMOVE_SPEC_CHARS, 
------          @v_remove_spaces = RASF.REMOVE_SPACES_IND,    ---REMOVE_SPACES,        NOT ALLOWED IN SUBSTRING
          @v_replace_char = RASF.REPLACE_CHAR,    ---REPLACE_CHAR, 
          @v_replacing_char = RASF.REPLACING_CHAR,    ---REPLACING_CHAR, 
          @v_substr_start_pos = RASF.SUBSTR_START_POS,    ---SUBSTR_START_POS, 
          @v_trunc_first_token = RASF.TRUNC_FIRST_TOKEN_IND,   --- added here instead of from params
          @v_substr_length = RASF.SUBSTR_LENGTH    ---SUBSTR_LENGTH
        FROM synchronizer.RULES_ACTION_STRFUNCTION RASF
        WHERE RASF.RULES_ACTION_STRFUNCTION_ID = @in_rules_action_strfunction_fk




	IF     @in_token_value = ''
		OR @in_token_value is NULL
		OR @in_token_value = 'NULL'
	            RETURN 'NULL'
  
	ELSE
    IF @v_token_seq > 1
    AND (  @in_start_string = ''
		OR @in_start_string is NULL
		OR @in_start_string = 'NULL'
        )
	            RETURN 'NULL'

    ELSE
    BEGIN


   set @v_strip_token = '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!';
   set @v_string_complete = 0;
   set @l_count = 0;
   set @l_exec_no = 0; 
   set @j = 1;
   set @v_return_string = null;
   set @v_debug_string = null;



    IF @in_prefix = '?'
       set @in_prefix = ' ';
  

    IF @v_replace_char = '?'
       set @v_replace_char = ' ';
    

    IF @v_replacing_char = '?'
       set @v_replacing_char = ' ';
  

	IF @in_max_length is null
	   set @v_max_length = 999;
	ELSE 
       set @v_max_length = @in_max_length;
	
   set @v_strip_token =  substring (@v_strip_token, 1, LEN (@v_remove_spec_chars));


--  Start String Operations

          IF @v_token_seq = 1
				SET @v_start_string = isnull ( @in_start_string, ' ' )
          ELSE
          IF @in_prefix is NULL
				SET @v_start_string = isnull ( @in_start_string, ' ' )
          ELSE
				SET @v_start_string = isnull ( @in_start_string, ' ' ) + @in_prefix       


   SET @v_result_string = LTRIM ( ( RTRIM( @in_token_value ) ) )


/*------------------------------------------------------------------------------------
       STRIP, REPLACE AND SUBSTRING CHARACTERS FOR STARTING STRING
-------------------------------------------------------------------------------------*/

   IF @in_start_pos > 1
   SET @v_result_string = ( substring ( @v_result_string, @in_start_pos, LEN ( @v_result_string) ) )

   IF @v_replace_char IS NOT NULL
      set @v_result_string =
         REPLACE (@v_result_string, @v_replace_char, @v_replacing_char );
      --- if debug on

   IF @v_remove_spec_chars IS NOT NULL
   BEGIN  
      set
      @v_result_string =
         epacube.STR_TRANSLATE_varchar (@v_result_string, @v_remove_spec_chars, @v_strip_token);
      set @v_result_string = REPLACE (@v_result_string, '!', '');
   END;


   IF @v_substr_length IS NOT NULL
   BEGIN
      set @v_result_string =
                 LTRIM (substring (@v_result_string, @v_substr_start_pos, @v_substr_length)) ;
   END



/*------------------------------------------------------------------------------------
       LOOP THROUGH THE INPUT STRING ADDING TOKEN BY TOKEN WHILE < MAX LENGTH
-------------------------------------------------------------------------------------*/

      DECLARE cur_exec cursor for 
       SELECT * FROM utilities.SplitString (@v_result_string,' ');  

      OPEN cur_exec;
      FETCH NEXT FROM cur_exec Into @v_return_token;
      
/*************************************************************************************/
--  BUG IN YURI'S FUNCTION SPLITSTRING.....
--    If the string is a single token it returns NULL value
--         Did the following work around for right now 
/*************************************************************************************/

	IF     @v_return_token = 'NULL'
           RETURN ( 'NULL' )
    ELSE
	IF     @v_return_token = ''
		OR @v_return_token is NULL
      BEGIN

		SET @v_return_string = ( rtrim ( ltrim ( 
                isnull(@v_start_string, 'NULL') + isnull(@v_result_string, 'NULL') )))            


        IF (epacube.STR_LENGTH_VARCHAR( @v_return_string ) <= @v_max_length )
            RETURN ( @v_return_string  ) 
        ELSE
			IF (@in_whole_tokens = 1)
			  RETURN ( @v_start_string )
			ELSE 
              BEGIN
				-- move epacube.STR_SUBSTR3_VARCHAR to common and recompile
			    SET @v_return_string = epacube.STR_SUBSTR3_VARCHAR( ( rtrim ( ltrim ( 
                   isnull(@v_start_string, '') + isnull(@v_result_string, '')))), 1, 
                   ( @v_max_length ) )  
--                   ( @v_max_length + 1 ) )  was returning one too many
                   RETURN ( @v_return_string  )
              END
      END           -- END single token not NULL

/*************************************************************************************/
--  LOOP THROUGH EACH TOKEN FROM FUNCTION SPLITSTRING.....
--    Building Result String one token at a time
--    @v_result_string initialized to @v_start_string
/*************************************************************************************/

      WHILE @@FETCH_STATUS = 0
      BEGIN

		IF @v_string_complete = 0
		BEGIN     

             SET @v_return_token = rtrim ( ltrim ( @v_return_token ) )
	         		 									 
             --   IF @j = 1  then first token.. no space separator -- use pre-separator with @v_start_string
             IF @j = 1
             BEGIN

				IF @v_start_string is NULL
					  OR @v_start_string = ''
						SET @v_string_length = LEN (@v_return_token)
					  ELSE   
						SET @v_string_length = LEN ( @v_start_string + @v_return_token )                 

			     IF @v_string_length  <  ( @v_max_length  ) 
					set @v_return_string = rtrim ( ltrim (
							   ( isnull (@v_start_string, '') + @v_return_token) ))
                 ELSE
			     IF @v_string_length  =  ( @v_max_length  ) 
				 BEGIN
					set @v_return_string = rtrim ( ltrim (
							   ( isnull (@v_start_string, '') + @v_return_token) ))
					set @v_string_complete = 1 -- string complete
				 END
				 ELSE  -- Addition of Token exceeds Max Length and FIRST TOKEN
				 IF @in_whole_tokens =  1   -- then do not split tokens
--					set @v_string_complete = 1  -- string complete
                 BEGIN
					set @v_string_complete = 1  -- string complete
                    IF @v_trunc_first_token = 1   -- trunc token if single token > max length
						IF @v_start_string is NULL
						  OR @v_start_string = ''
							set @v_return_string = rtrim ( ltrim (
							   substring ( @v_return_token,
									   1,
									   @v_max_length
									  ) ))
						ELSE 
							set @v_return_string = rtrim ( ltrim (
							   substring (
									   ( @v_start_string + @v_return_token),
									   1,
									   @v_max_length
									  ) ))
                 END
				 ELSE
				 BEGIN
					IF @v_start_string is NULL
					  OR @v_start_string = ''
						set @v_return_string = rtrim ( ltrim (
						   substring ( @v_return_token,
								   1,
								   @v_max_length
								  ) ))
                    ELSE 
						set @v_return_string = rtrim ( ltrim (
						   substring (
								   ( @v_start_string + @v_return_token),
								   1,
								   @v_max_length
								  ) ))
					set @v_string_complete = 1   -- string complete
				 END
            END
            ELSE     -- Not First Token
            BEGIN
			 SET @v_string_length = (  LEN ( @v_return_string + @v_return_token) + 1 ) -- include space separator

			     IF @v_string_length  <  ( @v_max_length  )
					set @v_return_string = rtrim ( ltrim (
							   (@v_return_string + ' ' + @v_return_token) ))               
                 ELSE
			     IF @v_string_length  =  ( @v_max_length  )
				 BEGIN
					set @v_return_string = rtrim ( ltrim (
							   (@v_return_string + ' ' + @v_return_token) ))               
					set @v_string_complete = 1 -- string complete
				 END
				 ELSE  -- Addition of Token exceeds Max Length
				 IF @in_whole_tokens =  1   -- then do not split tokens
					set @v_string_complete = 1  -- string complete
				 ELSE
				 BEGIN
					set @v_return_string = rtrim ( ltrim (
					   substring (
							   (@v_return_string + ' ' + @v_return_token),
							   1,
							   @v_max_length
							  ) ))
					set @v_string_complete = 1   -- string complete
				 END
             END

			 		              
	  END        -- V_STRING_COMPLETE = 0

	  set @j = @j + 1;  
	  FETCH NEXT FROM cur_exec Into @v_return_token;

  END; -- LOOP


  CLOSE cur_exec;
  DEALLOCATE cur_exec;

          
    SET @v_return_string =  isnull ( ltrim ( rtrim (@v_return_string) ) , 'NULL' )

	IF     @v_return_string = ''
		OR @v_return_string is NULL
		OR @v_return_string = 'NULL'
        RETURN ( 'NULL' )
    ELSE
        RETURN ( @v_return_string  )


 END  -- ELSE in_token not NULL

 RETURN ( @v_return_string  )


END







