﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 11/28/2009
-- Description:	quick fetch for xref from rules
-- =============================================
CREATE FUNCTION [epacube].[getRuleXREFForId]
(
	@RulesId int
)
RETURNS varchar(32)
AS
BEGIN
	DECLARE @x varchar(32)

    select @x=HOST_RULE_XREF from synchronizer.RULES WITH (NOLOCK)
	where Rules_id=@RulesId

	RETURN @x
END

