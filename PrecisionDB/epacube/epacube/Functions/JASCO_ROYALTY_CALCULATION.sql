﻿

/*

The Royalty table should contain the following.
        - Royalty Code   
        - Royalty Description
        - Royalty CalculationMethod
             % or $
        - Royalty Rate
            999.99
        - Royalty Allocation Method
            Gross Sales or Net Sales
Note 1:  In the item record there should a Royalty Flag:  Yes or No
Note 2:  There should also be a supplementary file of Royalty values per item:  Item number and Royalty codes (THIS IS AN ASSOCIATION TO ROYALTY CODES IN THE TABLE ABOVE)
Note 3:  The calculation method should potentially output two royalty summary values by item determined by the royalty types, royalty rates, calc method and alloc method. The output fields should be determined by the allocation method (Gross or Net) and not GE Royalty and Other Royalty.
Note 4:  The calculations should be:
(Total of royalty rates with a $ Calculation Method) + (Landed Cost x Accumulation of all Royalty Rates with a $ Calculation Method) x (Accumulation of all Royalty Rates with a % Calculation Method)
Example:  Royalty Master Table
RType        CMethod     Rate    AMethod
    GE-CE       %        5.00        G   
    DisneyA     %        4.00        G
    Pixar       $         .10        G
    XYZCo       $         .07        G
    GE-HEP      %        4.00        N
    PixarB      $         .18        N
Assume Item 12345 with a landed cost of 2.15 has all of the above Royalty types.
    Gross Royalty = (.10+.07) + ((2.15+.10+.07) X ((5.00+4.00)/100)) = .17+.208 = .38
    Net Royalty = (.18) + ((2.15+.18) X (4.00/100)) = .18+.093 = .27

*/

-- =================================================================
-- Author:		Kathi Scott
-- Create date: 2/22/2012
-- Description:	JASCO Specific Function to Calculate Royalty
--              See Definition above from Sharon
--              Pass in Product, Org, 
--              and Result Data Name = Gross or Net Royalty
--              and lookup Landed Cost Basis Value for Rule
-- =================================================================
CREATE FUNCTION [epacube].[JASCO_ROYALTY_CALCULATION]

      (
        @in_product_structure_fk      bigint,       
        @in_org_entity_structure_fk   bigint,               
        @in_result_data_name_fk       bigint
      ) 
      RETURNS NUMERIC (18,6)
AS
  
  BEGIN

  DECLARE 

      @v_sum_fixed_values numeric (18,6),
      @v_sum_percent_values numeric (18,6),
      @v_landed_cost numeric (18,6),
      @v_royalty numeric (18,6)

--- add filter by qualifed rule on product

   SET @v_sum_fixed_values =
       ISNULL (  
       (  SELECT  SUM ( RA.BASIS1_NUMBER )
          FROM synchronizer.RULES R WITH (NOLOCK)    
          INNER JOIN  synchronizer.RULES_ACTION RA WITH (NOLOCK) 
             ON ( R.RULES_ID = RA.RULES_FK )
          INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
             ON ( R.RULES_ID = RFS.RULES_FK )
          INNER JOIN synchronizer.V_RULE_FILTERS_PROD VRFP 
             ON ( VRFP.RULES_FILTER_FK = RFS.PROD_FILTER_FK 
             AND  VRFP.PRODUCT_STRUCTURE_FK = @in_product_structure_fk )
          WHERE 1 = 1
          AND   RA.RULES_ACTION_OPERATION1_FK = 1021   --- FIXED VALUE
          AND   R.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
          )   
       , 0 )
       

   SET @v_sum_percent_values = 
       ISNULL (
       (  SELECT  SUM ( RA.BASIS1_NUMBER )
          FROM synchronizer.RULES R WITH (NOLOCK)    
          INNER JOIN  synchronizer.RULES_ACTION RA WITH (NOLOCK) 
             ON ( R.RULES_ID = RA.RULES_FK )
          INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
             ON ( R.RULES_ID = RFS.RULES_FK )
          INNER JOIN synchronizer.V_RULE_FILTERS_PROD VRFP 
             ON ( VRFP.RULES_FILTER_FK = RFS.PROD_FILTER_FK 
             AND  VRFP.PRODUCT_STRUCTURE_FK = @in_product_structure_fk )
          WHERE 1 = 1
          AND   RA.RULES_ACTION_OPERATION1_FK = 1061   --- PERCENTAGE
          AND   R.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
          )   
        , 0 )

   SET @v_landed_cost =  
 ISNULL ( 
 (  SELECT  NET_VALUE1
          FROM marginmgr.SHEET_RESULTS SR WITH (NOLOCK)    
          WHERE 1 = 1
          AND   SR.PRODUCT_STRUCTURE_FK = @in_product_structure_fk 
          AND   SR.ORG_ENTITY_STRUCTURE_FK = @in_org_entity_structure_fk  
          AND   SR.DATA_NAME_FK = ( SELECT TOP 1 DN.PARENT_DATA_NAME_FK  --- LOOKUP PARENT OF BASIS DN FROM RULE
									  FROM synchronizer.RULES R WITH (NOLOCK)    
									  INNER JOIN  synchronizer.RULES_ACTION RA WITH (NOLOCK) 
										 ON ( R.RULES_ID = RA.RULES_FK )
									  INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
										 ON ( DN.DATA_NAME_ID = RA.BASIS_CALC_DN_FK )										 
									  INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
										 ON ( R.RULES_ID = RFS.RULES_FK )
									  INNER JOIN synchronizer.V_RULE_FILTERS_PROD VRFP 
										 ON ( VRFP.RULES_FILTER_FK = RFS.PROD_FILTER_FK 
										 AND  VRFP.PRODUCT_STRUCTURE_FK = @in_product_structure_fk )
									  WHERE 1 = 1
									  AND   RA.RULES_ACTION_OPERATION1_FK = 1061   --- PERCENTAGE
									  AND   R.RESULT_DATA_NAME_FK = @in_result_data_name_fk  )
          )
   , 0 ) 
 
   SET @v_royalty = ( @v_sum_fixed_values 
                    + ( @v_landed_cost * ( @v_sum_percent_values / 100 ) )
                    )
   
   RETURN @v_royalty

END




