﻿-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Return taxonomy information JIRA (PRE-110)
-- Get taxonomy information for a given taxonomy node
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE   FUNCTION [epacube].[getTaxonomyWithFilter] 
(
	@TAXONOMY_NODE_ID AS INT,
	@FILTERED_NODES epacube.TAXONOMY_NODE_TYPE READONLY
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE @JSON VARCHAR(MAX) = '{}'

	SET @JSON = (
	SELECT TN.TAXONOMY_NODE_ID TAXONOMY_NODE_ID
		, DV.[VALUE] NODE_NAME
		, TN.PARENT_TAXONOMY_NODE_FK
		, (
			SELECT COUNT(*)
			FROM epacube.TAXONOMY_PRODUCT
			WHERE TAXONOMY_NODE_FK = TN.TAXONOMY_NODE_ID) PRODUCT_COUNT
		, epacube.getTaxonomyNodeSubProductCount(TN.TAXONOMY_NODE_ID) SUB_PRODUCT_COUNT
		, CASE ISNULL(FN.TAXONOMY_NODE_ID, 0)
			WHEN 0 THEN 0
			ELSE 1
		END [FILTER]
		, JSON_QUERY(epacube.getTaxonomyWithFilter(TN.TAXONOMY_NODE_ID, @FILTERED_NODES)) CHILDREN
	FROM epacube.TAXONOMY_NODE TN
		INNER JOIN epacube.DATA_VALUE DV
			ON TN.DATA_VALUE_FK = DV.DATA_VALUE_ID
		LEFT JOIN @FILTERED_NODES FN
			ON TN.TAXONOMY_NODE_ID = FN.TAXONOMY_NODE_ID
	WHERE TN.PARENT_TAXONOMY_NODE_FK = @TAXONOMY_NODE_ID
	FOR JSON PATH);

	RETURN @JSON

END