﻿


CREATE
    FUNCTION [epacube].[getProductMultiplier] 
      (
        @in_product_structure_fk		bigint
       ,@in_result_data_name_fk			int      	   
      ) 
      RETURNS numeric(18,6)
    AS
BEGIN

        DECLARE 

          @l_count bigint,
          @ls_exec varchar(max),
          @l_exec_no bigint,
          @v_value varchar(256),
		  @v_hierarchy varchar(100),
		  @v_rule_id int,
		  @v_Org_Filter_dn varchar(100),
		  @v_PRD_Filter_dn varchar(100),
		  @active int,
		  @ruleID bigint,	
		  @multiplier numeric(18,6),
		  @ColumnName varchar(64),
		  @TableName varchar(64),
		  @CustomSelect varchar(max),
		  @VendorClass int;

	set @active = (select epacube.getCodeRefId('RECORD_STATUS','ACTIVE'))
	set @VendorClass = (select epacube.getCodeRefId('ENTITY_CLASS','VENDOR'))

-- create a table variable to hold all the qualified rules
DECLARE @QualifiedRules TABLE
(
  RulesID bigint,
  RulePrecedence smallint    
)

--process everything from Product Identification for the data name in question first 
--Goodin has expressly stated that they DO NOT care about Org or Supplier filters, so just grab a product
--and the data name and stick it in there

--Identifiers 

Insert into @QualifiedRules(RulesID, RulePrecedence)
select 
r.rules_id, r.RULE_PRECEDENCE
from synchronizer.rules_filter_set rfs
inner join synchronizer.rules r on (r.rules_id = rfs.rules_fk 
									and r.result_data_name_fk = @in_result_data_name_fk
									and r.record_status_cr_fk = @active)
inner join synchronizer.rules_filter rf on (rfs.prod_filter_fk = rf.RULES_FILTER_ID											
											and rf.RECORD_STATUS_CR_FK = @active)
inner join epacube.product_identification pi on (pi.DATA_NAME_FK = rf.DATA_NAME_FK
												 and pi.value = rf.value1
												 and pi.PRODUCT_STRUCTURE_FK = @in_product_structure_fk 
												 and pi.RECORD_STATUS_CR_FK = @active)							

--Do the same for Product Category Data Values
Insert into @QualifiedRules(RulesID, RulePrecedence)	
select 
r.rules_id,
r.RULE_PRECEDENCE	
from synchronizer.rules_filter_set rfs
inner join synchronizer.rules r on (r.rules_id = rfs.rules_fk 
									and r.result_data_name_fk = @in_result_data_name_fk
									and r.record_status_cr_fk = @active)
inner join synchronizer.rules_filter rf on (rfs.prod_filter_fk = rf.RULES_FILTER_ID
											and rf.RECORD_STATUS_CR_FK = @active)
inner join epacube.product_category pc on (pc.DATA_NAME_FK = rf.DATA_NAME_FK												
											and pc.PRODUCT_STRUCTURE_FK = @in_product_structure_fk
										    and pc.RECORD_STATUS_CR_FK = @active)
inner join epacube.data_value pcdv on (pcdv.DATA_VALUE_ID = pc.DATA_VALUE_FK
										and pcdv.DATA_NAME_FK = pc.DATA_NAME_FK
										and rf.value1 = pcdv.value
										and pcdv.RECORD_STATUS_CR_FK = @active)
 

--Do the same for Product Category Entity Data Values
Insert into @QualifiedRules(RulesID, RulePrecedence)	
select 	--distinct
r.rules_id,
r.RULE_PRECEDENCE	
from synchronizer.rules_filter_set rfs
inner join synchronizer.rules r on (r.rules_id = rfs.rules_fk 
									and r.result_data_name_fk = @in_result_data_name_fk
									and r.record_status_cr_fk = @active)
inner join synchronizer.rules_filter rf on (rfs.prod_filter_fk = rf.RULES_FILTER_ID
											and rf.RECORD_STATUS_CR_FK = @active)
inner join epacube.product_category pc on (pc.DATA_NAME_FK = rf.DATA_NAME_FK												
											and pc.PRODUCT_STRUCTURE_FK = @in_product_structure_fk
										    and pc.RECORD_STATUS_CR_FK = @active)
inner join epacube.entity_data_value pcedv on (pcedv.ENTITY_DATA_VALUE_ID = pc.DATA_VALUE_FK
										and pcedv.DATA_NAME_FK = pc.DATA_NAME_FK
										and rf.value1 = pcedv.value
										and pcedv.RECORD_STATUS_CR_FK = @active)								

--Do the same for Product Attribute

Insert into @QualifiedRules(RulesID, RulePrecedence)
select --distinct	
r.rules_id,
r.RULE_PRECEDENCE		
from synchronizer.rules_filter_set rfs
inner join synchronizer.rules r on (r.rules_id = rfs.rules_fk 
									and r.result_data_name_fk = @in_result_data_name_fk
									and r.record_status_cr_fk = @active)
inner join synchronizer.rules_filter rf on (rfs.prod_filter_fk = rf.RULES_FILTER_ID									
											and rf.RECORD_STATUS_CR_FK = @active)
inner join epacube.product_attribute pa on (pa.DATA_NAME_FK = rf.DATA_NAME_FK												
												 and pa.PRODUCT_STRUCTURE_FK = @in_product_structure_fk
												 and pa.attribute_event_data = rf.value1
												 and pa.RECORD_STATUS_CR_FK = @active)

--Do the same for Product Vendor Associations

Insert into @QualifiedRules(RulesID, RulePrecedence)
select --distinct	
r.rules_id,
r.RULE_PRECEDENCE		
from synchronizer.rules_filter_set rfs
inner join synchronizer.rules r on (r.rules_id = rfs.rules_fk 
									and r.result_data_name_fk = @in_result_data_name_fk
									and r.record_status_cr_fk = @active)
inner join synchronizer.rules_filter rf on (rfs.supl_filter_fk = rf.RULES_FILTER_ID									
											and rf.RECORD_STATUS_CR_FK = @active)
inner join epacube.entity_identification ei on (rf.value1 = ei.value
											 and rf.DATA_NAME_FK = ei.data_name_fk
											 and rf.ENTITY_DATA_NAME_FK = ei.ENTITY_DATA_NAME_FK)											
inner join epacube.product_association pa on (pa.entity_structure_fk = ei.entity_structure_fk
											 and pa.entity_class_cr_fk = @VendorClass
											 and pa.product_structure_fk = @in_product_structure_fk)



---select the best one from the temp table

	set @ruleID = (select top 1 RulesID from @QualifiedRules
										   order by RulePrecedence ASC)
--then look up the multiplier

	set @multiplier = (select basis1_number from synchronizer.rules_action
						where rules_fk = @ruleID)
   RETURN ( @multiplier )
END



