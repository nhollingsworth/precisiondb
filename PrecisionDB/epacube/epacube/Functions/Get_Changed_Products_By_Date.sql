﻿
-- Function created by Robert Gannon
--
--
-- Purpose: Purpose of this Function is to grab all products for a specific export
-- where the products have been changed since the last specified date. 
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        12/13/2007  Initial SQL Version

CREATE FUNCTION [epacube].[Get_Changed_Products_By_Date]( @LastExportDate datetime  )
RETURNS @changedExportProducts TABLE 
(
    -- Columns returned by the function
    ProductStructureID bigint PRIMARY KEY NOT NULL    
)
AS
-- Returns the product_structure_fk for all products updated since the last export
BEGIN

INSERT into @changedExportProducts (ProductStructureID)
        
--select a.product_structure_fk from (
(select product_structure_fk 
from epacube.product_description
where update_timestamp > @LastExportDate)

UNION

(select product_structure_fk 
from epacube.product_identification 
where update_timestamp > @LastExportDate)

UNION

(select product_structure_fk 
from epacube.product_BOM
where update_timestamp > @LastExportDate)

UNION

(select product_structure_fk 
from epacube.product_values
where update_timestamp > @LastExportDate)

UNION

(select product_structure_fk 
from epacube.product_UOM_CLASS
where update_timestamp > @LastExportDate)

UNION

(select product_structure_fk 
from epacube.product_properties
where update_timestamp > @LastExportDate)

UNION

(select product_structure_fk 
from epacube.product_mult_type
where update_timestamp > @LastExportDate)

UNION

(select product_structure_fk 
from epacube.product_mult_type pmt
inner join epacube.data_value dv on (dv.data_value_id = pmt.data_value_fk and dv.update_timestamp > @LastExportDate))

UNION

(select product_structure_fk 
from epacube.product_text
where update_timestamp > @LastExportDate)


UNION

(select product_structure_fk 
from epacube.product_UOM
where update_timestamp > @LastExportDate)


UNION

(select product_structure_fk 
from epacube.product_taxonomy
where update_timestamp > @LastExportDate)

UNION

(select product_structure_fk 
from epacube.product_tax_properties
where update_timestamp > @LastExportDate)

UNION

(select product_structure_fk 
from epacube.product_ident_nonunique
where update_timestamp > @LastExportDate)

UNION

---------------Three Queries for Product Category
-- 1. Grab all products with updated Category Setup
-- 2. Grab all products with updated Data Values
-- 3. Grab all products with updated Entity Data Values

-- 1. Grab Product Category
(select product_structure_fk 
from epacube.product_category pc
where pc.update_timestamp > @LastExportDate)

UNION

-- 2. Grab Data Values
select product_structure_fk 
from epacube.product_category pc
inner join epacube.data_value dv on (data_value_id = pc.data_value_fk and dv.update_timestamp > @LastExportDate)

UNION
-- 3. Grab Entity Data Values
select product_structure_fk 
from epacube.product_category pc
inner join epacube.entity_data_value edv on (entity_data_value_id = pc.data_value_fk and edv.update_timestamp > @LastExportDate)

-- End Product Category Set

UNION

-- 
(select product_structure_fk 
from epacube.product_association
where update_timestamp > @LastExportDate)

UNION

---------------Three Queries for Product Attribute
-- 1. Grab all products with updated Attribute Setup
-- 2. Grab all products with updated Data Values
-- 3. Grab all products with updated Entity Data Values

--1. Updated Attribute setup
(select product_structure_fk 
from epacube.product_attribute
where update_timestamp > @LastExportDate)

UNION
--2. Updated Data Values
(select product_structure_fk 
from epacube.product_attribute pa
inner join epacube.data_value dv on (data_value_id = pa.data_value_fk and dv.update_timestamp > @LastExportDate))


UNION
--3. Updated Entity Data Values
(select product_structure_fk 
from epacube.product_attribute pa
inner join epacube.entity_data_value edv on (entity_data_value_id = pa.data_value_fk and edv.update_timestamp > @LastExportDate))

-- End Product Attribute Set

---------------Three Queries for Product Assoc Attribute
-- 1. Grab all products with updated Assoc Attribute Setup
-- 2. Grab all products with updated Data Values
-- 3. Grab all products with updated Entity Data Values

UNION
--1. Updated Attribute setup
(select product_structure_fk 
from epacube.product_association pa
inner join epacube.product_assoc_attribute paa on (paa.product_association_fk = pa.product_association_id and paa.update_timestamp > @LastExportDate))

UNION
--2. Updated Data Values
(select product_structure_fk 
from epacube.product_association pa
inner join epacube.product_assoc_attribute paa on (paa.product_association_fk = pa.product_association_id )
inner join epacube.data_value dv on (paa.data_value_fk = data_value_id and dv.update_timestamp > @LastExportDate))

UNION

--3. Updated Entity Data Values
(select product_structure_fk 
from epacube.product_association pa
inner join epacube.product_assoc_attribute paa on (paa.product_association_fk = pa.product_association_id )
inner join epacube.entity_data_value edv on (paa.data_value_fk = entity_data_value_id and edv.update_timestamp > @LastExportDate))

--End Product Assoc Attribute set
/*
)a
 inner join epacube.product_structure ps on (a.product_structure_fk = product_structure_id)
where ps.record_status_cr_fk = 1
*/
    
    RETURN
END;


