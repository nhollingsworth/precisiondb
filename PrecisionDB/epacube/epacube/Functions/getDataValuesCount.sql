﻿
-- =============================================
-- Author:		Gary Stone
-- Create date: 12/15/2017
-- Determine how many instances of a data value are in use
-- =============================================
CREATE FUNCTION [epacube].[getDataValuesCount]
(
	@Data_Name_FK varchar(32), @Data_Value_FK varchar(32)
)
RETURNS Varchar(Max)
AS
BEGIN
	DECLARE @c Varchar(Max)

    Set @c = 'Select count(*) from ' +
	(Select DNGR.qtbl Qulaified_Table 
	from epacube.DATA_NAME_GROUP_REFS DNGR 
	inner join epacube.data_value dv on dngr.data_name_fk = dv.data_name_fk 
	where dngr.data_name_fk = @Data_Name_FK and dv.data_value_id = @Data_Value_FK)
	+ ' where data_name_fk = ' + @Data_Name_FK + ' and data_value_fk = ' + @Data_Value_FK

	RETURN (@c)
END

