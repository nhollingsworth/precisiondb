﻿-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Return taxonomy information JIRA (PRE-110)
-- Get taxonomy information for a given taxonomy node
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE   FUNCTION [epacube].[getTaxonomyNodeSubProductCount] 
(
	@TAXONOMY_NODE_ID AS INT
)
RETURNS INT
AS
BEGIN
	DECLARE @SUB_PRODUCT_COUNT AS INT;

	WITH CTE_TAXONOMY
	AS (
	SELECT @TAXONOMY_NODE_ID TAXONOMY_NODE_ID
	UNION ALL
	SELECT TN.TAXONOMY_NODE_ID
	FROM CTE_TAXONOMY CT
		INNER JOIN epacube.TAXONOMY_NODE TN
			ON CT.TAXONOMY_NODE_ID = TN.PARENT_TAXONOMY_NODE_FK
	)

	SELECT @SUB_PRODUCT_COUNT = COUNT(*)
	FROM CTE_TAXONOMY CT
		INNER JOIN epacube.TAXONOMY_PRODUCT TP
			ON CT.TAXONOMY_NODE_ID = TP.TAXONOMY_NODE_FK;

	RETURN @SUB_PRODUCT_COUNT

END