﻿
CREATE FUNCTION [epacube].[Get_Entity_CrossRef]
    (
      @in_entity_structure_fk INT
      
    )
RETURNS @EntityCrossRef TABLE
    (    
      [ENTITY_STRUCTURE_FK] BIGINT,
      [CROSSREF_ENTITY_STRUCTURE_FK] BIGINT,
      [ENTITY_DATA_NAME_FK] INT,
      [LEVEL_SEQ] SMALLINT,
      [ENTITY_CLASS_CR_FK] INT
    )
AS
BEGIN


    WITH    EntityHierarchy ( Entity_Structure_fk, Parent_Entity_Structure_FK, Entity_Data_Name_FK, Level_Seq, Entity_Class_CR_FK )
              AS (
-- Anchor member definition
                   SELECT   es.entity_structure_id AS entity_structure_fk,
                            es.Parent_Entity_Structure_FK,
                            es.DATA_NAME_FK AS Entity_Data_Name_fk,
                            ect.Level_Seq,
                            es.entity_class_cr_fk
                   FROM     epacube.ENTITY_STRUCTURE es WITH (NOLOCK) 
                            INNER JOIN epacube.ENTITY_CLASS_TREE ect WITH (NOLOCK) ON ( ect.entity_class_cr_fk = es.entity_class_cr_fk
                                                                     AND  ect.data_name_fk = es.data_name_fk ) 
                   WHERE    es.entity_structure_id = @in_entity_structure_fk
                   UNION ALL
-- Recursive member definition
                   SELECT   ep.entity_structure_id AS entity_structure_fk, 
                            ep.Parent_Entity_Structure_FK,
                            ep.DATA_NAME_FK AS Entity_Data_Name_fk,
                            ect.Level_Seq,
                            ep.entity_class_cr_fk
                   FROM     epacube.ENTITY_STRUCTURE ep WITH (NOLOCK) 
                            INNER JOIN epacube.ENTITY_CLASS_TREE ect WITH (NOLOCK) ON ( ect.entity_class_cr_fk = ep.entity_class_cr_fk
                                                                     AND  ect.data_name_fk = ep.data_name_fk ) 
                            INNER JOIN EntityHierarchy AS eh ON ep.entity_structure_id = eh.Parent_Entity_Structure_FK
                 )
        -- Statement that executes the CTE
INSERT  INTO @EntityCrossRef
        SELECT  @in_entity_structure_fk,
                eh.Entity_Structure_FK,
                eh.Entity_Data_Name_fk,
                eh.Level_Seq,
                eh.entity_class_cr_fk                   
        FROM    EntityHierarchy eh


----        -- Statement that inserts ALL ENTITY
INSERT  INTO @EntityCrossRef
        SELECT  @in_entity_structure_fk,
                es.Entity_Structure_id,
                es.DATA_NAME_FK,
                0,
                es.entity_class_cr_fk                   
        FROM    epacube.entity_structure es
        WHERE   es.entity_class_cr_fk = ( SELECT esx.entity_class_cr_fk
                                          FROM epacube.entity_structure esx WITH (NOLOCK) 
                                          WHERE esx.entity_structure_id = @in_entity_structure_fk )
        AND     es.entity_structure_id < 10
        AND     @in_entity_structure_fk > 10


    RETURN
   END ;
