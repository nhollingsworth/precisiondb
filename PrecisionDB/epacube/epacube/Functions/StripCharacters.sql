﻿
/*
 *  Examples of how to use StripCharacters function
 *	Alphabetic only, strip non-Alphabetic characters
	SELECT epacube.StripCharacters('a1!s2@d3#f4$', '^a-z')

	Get Numeric only, strip non-numeric characters
	SELECT epacube.StripCharacters('a1!s2@d3#f4$', '^.0-9')

	Alphanumeric only, strip non-Alphanumeric characters
	SELECT epacube.StripCharacters('a1!s2@d3#f4$', '^a-z0-9')

	Non-alphanumeric, strip Alphanumeric characters
	SELECT epacube.StripCharacters('a1!s2@d3#f4$', 'a-z0-9')
 */

CREATE FUNCTION [epacube].[StripCharacters]
(
    @String NVARCHAR(MAX), 
    @MatchExpression VARCHAR(255)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
    SET @MatchExpression =  '%['+@MatchExpression+']%'

    WHILE PatIndex(@MatchExpression, @String) > 0
        SET @String = Stuff(@String, PatIndex(@MatchExpression, @String), 1, '')

    RETURN @String

END
