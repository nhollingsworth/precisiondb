﻿








-- Copyright 2007
--
-- Function created by Robert Gannon
--
--
-- Purpose: Purpose of this Function is to grab all products for a specific export
-- where the products have been changed since the last export. Logic is that any 
-- product_structure_fk with an update_timestamp greater than the most recent completion
-- of Export Type X has been "changed".
--
-- Initial version takes data as input, may be changed. May also need data name updates.
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        11/30/2007  Initial SQL Version
-- WT        04/01/2008  EPA-1616. Fix non-performant sql queries.



CREATE FUNCTION [epacube].[Get_Changed_Products]( @LastExportDate datetime  )
RETURNS @changedExportProducts TABLE 
(
    -- Columns returned by the function
    ProductStructureID bigint PRIMARY KEY NOT NULL    
)
AS 
-- Returns the product_structure_fk for all products updated since the last export
BEGIN
    --DECLARE @LastExportDate datetime  
	--DECLARE	@ProductStructureID bigint	       
    -- Find timestamp of most recent export of flavor A
--	SET @LastExportDate = (select  max(je.end_timestamp)jet
--from common.job_execution je
--inner join common.job j on (j.job_id = je.job_fk )
--where job_class_cr_fk = (select code_ref_id from epacube.code_ref 
--						 where code = 'EXPORT' and code_type = 'JOB_CLASS' 
--						 and je.job_status_cr_fk = (select code_ref_id 
--						 from epacube.code_Ref where  code = 'COMPLETE'))
--and j.name = (select name from export.export_package where export_package_id = @in_export_id)
--);

 INSERT into @changedExportProducts (ProductStructureID)
        
select distinct a.product_structure_fk from (
(select product_structure_fk 
from epacube.product_description
where update_timestamp > @LastExportDate)

UNION ALL

(select product_structure_fk 
from epacube.product_identification 
where update_timestamp > @LastExportDate)

UNION ALL

(select product_structure_fk 
from epacube.product_BOM
where update_timestamp > @LastExportDate)

UNION ALL

(select product_structure_fk 
from epacube.product_values
where update_timestamp > @LastExportDate)

UNION ALL

(select product_structure_fk 
from epacube.product_UOM_CLASS
where update_timestamp > @LastExportDate)

UNION ALL

(select product_structure_fk 
from epacube.product_properties
where update_timestamp > @LastExportDate)

UNION ALL

(select product_structure_fk 
from epacube.product_mult_type
where update_timestamp > @LastExportDate)

UNION ALL

(select product_structure_fk 
from epacube.product_mult_type pmt
inner join epacube.data_value dv on (dv.data_value_id = pmt.data_value_fk and dv.update_timestamp > @LastExportDate))

UNION ALL

(select product_structure_fk 
from epacube.product_text
where update_timestamp > @LastExportDate)


UNION ALL

(select product_structure_fk 
from epacube.product_UOM
where update_timestamp > @LastExportDate)


UNION ALL

(select product_structure_fk 
from epacube.product_taxonomy
where update_timestamp > @LastExportDate)

UNION ALL

(select product_structure_fk 
from epacube.product_tax_properties
where update_timestamp > @LastExportDate)

UNION ALL

(select product_structure_fk 
from epacube.product_ident_nonunique
where update_timestamp > @LastExportDate)

UNION ALL

---------------Three Queries for Product Category
-- 1. Grab all products with updated Category Setup
-- 2. Grab all products with updated Data Values
-- 3. Grab all products with updated Entity Data Values

-- 1. Grab Product Category
(select product_structure_fk 
from epacube.product_category pc
where pc.update_timestamp > @LastExportDate)

UNION ALL

-- 2. Grab Data Values
select product_structure_fk 
from epacube.product_category pc
inner join epacube.data_value dv on (data_value_id = pc.data_value_fk and dv.update_timestamp > @LastExportDate)

UNION ALL
-- 3. Grab Entity Data Values
select product_structure_fk 
from epacube.product_category pc
inner join epacube.entity_data_value edv on (entity_data_value_id = pc.data_value_fk and edv.update_timestamp > @LastExportDate)

-- End Product Category Set

UNION ALL

-- 
(select product_structure_fk 
from epacube.product_association
where update_timestamp > @LastExportDate)

UNION ALL 

---------------Three Queries for Product Attribute
-- 1. Grab all products with updated Attribute Setup
-- 2. Grab all products with updated Data Values
-- 3. Grab all products with updated Entity Data Values

--1. Updated Attribute setup
(select product_structure_fk 
from epacube.product_attribute
where update_timestamp > @LastExportDate)

UNION ALL
--2. Updated Data Values
(select product_structure_fk 
from epacube.product_attribute pa
inner join epacube.data_value dv on (data_value_id = pa.data_value_fk and dv.update_timestamp > @LastExportDate))


UNION ALL
--3. Updated Entity Data Values
(select product_structure_fk 
from epacube.product_attribute pa
inner join epacube.entity_data_value edv on (entity_data_value_id = pa.data_value_fk and edv.update_timestamp > @LastExportDate))

-- End Product Attribute Set

---------------Three Queries for Product Assoc Attribute
-- 1. Grab all products with updated Assoc Attribute Setup
-- 2. Grab all products with updated Data Values
-- 3. Grab all products with updated Entity Data Values

UNION ALL
--1. Updated Attribute setup
(select product_structure_fk 
from epacube.product_association pa
inner join epacube.product_assoc_attribute paa on (paa.product_association_fk = pa.product_association_id and paa.update_timestamp > @LastExportDate))

UNION ALL
--2. Updated Data Values
(select product_structure_fk 
from epacube.product_association pa
inner join epacube.product_assoc_attribute paa on (paa.product_association_fk = pa.product_association_id )
inner join epacube.data_value dv on (paa.data_value_fk = data_value_id and dv.update_timestamp > @LastExportDate))

UNION ALL

--3. Updated Entity Data Values
(select product_structure_fk 
from epacube.product_association pa
inner join epacube.product_assoc_attribute paa on (paa.product_association_fk = pa.product_association_id )
inner join epacube.entity_data_value edv on (paa.data_value_fk = entity_data_value_id and edv.update_timestamp > @LastExportDate))

--End Product Assoc Attribute set
)a
 inner join epacube.product_structure ps on (a.product_structure_fk = product_structure_id)
    
    RETURN
END;



