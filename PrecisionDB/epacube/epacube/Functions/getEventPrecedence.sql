﻿
CREATE
    FUNCTION [epacube].[getEventPrecedence] 
      (
        @in_Sheet_Structure			bigint
       ,@in_PRD_Filter_dn			varchar(100)
       ,@in_Cust_filter_dn          varchar(100)
	   ,@in_Org_filter_dn			varchar(100)
	   ,@in_Promo_Flag				varchar(1) --default('N'),

      ) 
      RETURNS varchar(256)
    AS
BEGIN

        DECLARE 

          @l_count bigint,
          @ls_exec varchar(max),
          @l_exec_no bigint,
          @v_value varchar(256),
		  @v_hierarchy varchar(100),
		  @v_rule_id int,
		  @v_Org_Filter_dn varchar(100),
		  @v_PRD_Filter_dn varchar(100)

------------------------------------------------------------------------------------------------------------
--Get the precedence rule id used for sheet 
--*** NOTE:  IF THIS IS NULL THEN USER CAN ENTER PRECEDENCE FROM UI
------------------------------------------------------------------------------------------------------------
	SET @v_rule_id = (SELECT sheet_precedence_rule_fk 
					   FROM marginmgr.sheet_structure 
					   WHERE sheet_structure_id = @in_sheet_structure)
------------------------------------------------------------------------------------------------------------
--Get the hierachy used for sheet_rule_id
---------------------------------------------------------------------------------------------------------
	SET @v_hierarchy = (SELECT name 
					   FROM marginmgr.sheet_precedence_rule 
					   WHERE sheet_precedence_rule_id = @v_rule_id)
-----------------------------------------------------------------------------------------------------------
--Translate Org data for lookup
-------------------------------------------------------------------------------------------------------------
	SET @v_PRD_Filter_dn = (SELECT
							CASE WHEN @in_Prd_filter_dn = 'Product' 
								 THEN 'All Products' 
								 ELSE @in_Prd_filter_dn 
								 END)
-------------------------------------------------------------------------------------------------------------
--Translate product data for lookup
-------------------------------------------------------------------------------------------------------------
	SET @v_Org_Filter_dn = (SELECT
							CASE WHEN @in_Org_filter_dn = 'ALL ORGS' 
								 THEN 'ALL ORGS' 
								 ELSE 'Whse' 
								 END)

------------------------------------------------------------------------------------------------------------
--Now look up precedence  -- added support for CORP also
-------------------------------------------------------------------------------------------------------------

   SET @v_value = ( 
       ISNULL (
         (  SELECT sprl.precedence 
			FROM  marginmgr.sheet_precedence_rule_lookup sprl	
			WHERE sprl.hierarchy = @v_Hierarchy
		AND sprl.prd_filter_dn = @v_PRD_Filter_dn
		AND sprl.Cust_filter_dn = @in_Cust_filter_dn
		AND sprl.Org_filter_dn = @v_Org_filter_dn
		AND sprl.Promo_flag = isnull(@in_Promo_Flag,'N')	
         ),
         (  SELECT sprl.precedence 
			FROM  marginmgr.sheet_precedence_rule_lookup sprl	
			WHERE sprl.hierarchy = @v_Hierarchy
		AND sprl.prd_filter_dn = @v_PRD_Filter_dn
		AND sprl.Cust_filter_dn = @in_Cust_filter_dn
		AND sprl.Org_filter_dn = 'CORP'  -- CONTIGENCY FOR DATA NOT CHANGED YET
		AND sprl.Promo_flag = isnull(@in_Promo_Flag,'N')	
         )
        ) )

   RETURN ( @v_value )
END
