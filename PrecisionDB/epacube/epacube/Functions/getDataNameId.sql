﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [epacube].[getDataNameId]
(
	-- Add the parameters for the function here
	@DATA_NAME varchar(50)
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @id int

	-- Add the T-SQL statements to compute the return value here
	SELECT @id = data_name_id from epacube.data_name where name = @DATA_NAME

	-- Return the result of the function
	RETURN @id

END

