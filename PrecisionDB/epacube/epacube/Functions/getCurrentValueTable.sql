﻿
-- Function created by Geoff Begley
--
--
-- Purpose: This function is called in the UI in the Product Orgs Editor
-- It returns the current value for a Product Org Attribute in the editing window.
-- Used to perform a SQL Inner Join to Gather up all the current values at one time.
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS   				  Initial SQL Version




CREATE
    FUNCTION [epacube].[getCurrentValueTable] 
      (
        @in_product_structure_fk         bigint
       ,@in_org_entity_structure_fk      bigint
       ,@in_data_name_fk                 int
      ) 
 RETURNS  @GCVTab table
(
        product_structure_fk         bigint
       ,org_entity_structure_fk      bigint
       ,data_name_fk                 int
       ,current_value                varchar(256)
)
AS
BEGIN

        DECLARE 

          @l_count bigint,
          @ls_exec varchar(max),
          @l_exec_no bigint,
          @v_value varchar(256)


  SET @v_value =   
      ( SELECT 
			  CASE ds.event_type_cr_fk
--- KS ADDED 155 ASSOCIATIONS
        WHEN 155 THEN ( SELECT EI.VALUE
			              FROM EPACUBE.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)			          
			              INNER JOIN EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
			                ON ( EI.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK
			                AND  EI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
			                                        FROM EPACUBE.EPACUBE_PARAMS EEP
			                                        WHERE EEP.APPLICATION_SCOPE_FK = 100
			                                        AND   EEP.NAME = ( SELECT CODE 
			                                                           FROM epacube.CODE_REF
			                                                           WHERE CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
			             ) )
			            WHERE PAS.product_structure_fk = ps.product_structure_id
						AND   PAS.data_name_fk = dn.data_name_id 
						AND   PAS.org_entity_structure_fk = es.entity_structure_id )
---			  
			  WHEN 150 THEN
				 CASE ds.table_name
				 WHEN 'PRODUCT_CATEGORY'
					   THEN CASE ds.data_type_cr_fk
							WHEN 134 
							THEN  ( select dv.value
									from epacube.product_category pc 
									inner join epacube.data_value dv 
									   on ( dv.data_value_id = pc.data_value_fk )
									where pc.product_structure_fk = ps.product_structure_id
									and   pc.data_name_fk = dn.data_name_id 
									and   pc.org_entity_structure_fk = es.entity_structure_id )
							WHEN 135 
							THEN  ( select edv.value
									from epacube.product_category pc 
									inner join epacube.entity_data_value edv 
									   on ( edv.entity_data_value_id = pc.entity_data_value_fk )
									where pc.product_structure_fk = ps.product_structure_id
									and   pc.data_name_fk = dn.data_name_id 
									and   pc.org_entity_structure_fk = es.entity_structure_id )
						   END
				 WHEN 'PRODUCT_ATTRIBUTE'
					   THEN CASE ds.data_type_cr_fk
							WHEN 134 
							THEN  ( select dv.value
									from epacube.product_attribute pa 
									inner join epacube.data_value dv 
									   on ( dv.data_value_id = pa.data_value_fk )
									where pa.product_structure_fk = ps.product_structure_id
									and   pa.data_name_fk = dn.data_name_id 
									and   pa.org_entity_structure_fk = es.entity_structure_id )
							WHEN 135 
							THEN  ( select edv.value
									from epacube.product_attribute pa
									inner join epacube.entity_data_value edv 
									   on ( edv.entity_data_value_id = pa.entity_data_value_fk )
									where pa.product_structure_fk = ps.product_structure_id
									and   pa.data_name_fk = dn.data_name_id 
									and   pa.org_entity_structure_fk = es.entity_structure_id )
							ELSE ( select pa.attribute_event_data
									from epacube.product_attribute pa 
									where pa.product_structure_fk = ps.product_structure_id
									and   pa.data_name_fk = dn.data_name_id 
									and   pa.org_entity_structure_fk = es.entity_structure_id )
						   END
				  WHEN 'PRODUCT_IDENTIFICATION'
						THEN  ( select pi.value
								from epacube.product_identification pi
								where pi.product_structure_fk = ps.product_structure_id
								and   pi.data_name_fk = dn.data_name_id 
								and   pi.org_entity_structure_fk = es.entity_structure_id )
				  WHEN 'PRODUCT_DESCRIPTION'
						THEN  ( select pd.description
								from epacube.product_description pd
								where pd.product_structure_fk = ps.product_structure_id
								and   pd.data_name_fk = dn.data_name_id 
								and   pd.org_entity_structure_fk = es.entity_structure_id )
---------------------------------------------------------------------------------------------			
-- EPA-1436 Modification below				
---------------------------------------------------------------------------------------------
				  WHEN 'PRODUCT_UOM_CLASS'
						THEN  ( select (select uom_code from epacube.uom_code
									    where uom_code_id = puc.uom_code_fk)
								from epacube.product_uom_class puc
								where puc.product_structure_fk = ps.product_structure_id
								and   puc.data_name_fk = dn.data_name_id 
								and   puc.org_entity_structure_fk = es.entity_structure_id )
----------------------------------- END EPA-1436 Mod
				  END 
			  WHEN 151 THEN
					CASE ds.column_name
                         WHEN 'NET_VALUE1' THEN
                          ( select top 1 cast ( pv.net_value1 as varchar (20) )
							from epacube.product_values pv
							where pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = es.entity_structure_id
							)
                         WHEN 'NET_VALUE2' THEN
                          ( select top 1 cast ( pv.net_value2 as varchar (20) )
							from epacube.product_values pv
							where pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = es.entity_structure_id
							)
                         WHEN 'NET_VALUE3' THEN
                          ( select top 1 cast ( pv.net_value3 as varchar (20) )
							from epacube.product_values pv
							where pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = es.entity_structure_id
							)
                         WHEN 'NET_VALUE4' THEN
                          ( select top 1 cast ( pv.net_value4 as varchar (20) )
							from epacube.product_values pv
							where pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = es.entity_structure_id
							)
                         WHEN 'NET_VALUE5' THEN
                          ( select top 1 cast ( pv.net_value5 as varchar (20) )
							from epacube.product_values pv
							where pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = es.entity_structure_id
							)
                         WHEN 'NET_VALUE6' THEN
                          ( select top 1 cast ( pv.net_value6 as varchar (20) )
							from epacube.product_values pv
							where pv.data_name_fk = dn.parent_data_name_fk
							and   pv.product_structure_fk = ps.product_structure_id
							and   pv.org_entity_structure_fk = es.entity_structure_id
							)
       ----                  WHEN 'NET_VALUE7' THEN
       ----                   ( select top 1 cast ( pv.net_value7 as varchar (20) )
							----from epacube.product_values pv
							----where pv.data_name_fk = dn.parent_data_name_fk
							----and   pv.product_structure_fk = ps.product_structure_id
							----and   pv.org_entity_structure_fk = es.entity_structure_id
							----)
       ----                  WHEN 'NET_VALUE8' THEN
       ----                   ( select top 1 cast ( pv.net_value8 as varchar (20) )
							----from epacube.product_values pv
							----where pv.data_name_fk = dn.parent_data_name_fk
							----and   pv.product_structure_fk = ps.product_structure_id
							----and   pv.org_entity_structure_fk = es.entity_structure_id
							----)
       ----                  WHEN 'NET_VALUE9' THEN
       ----                   ( select top 1 cast ( pv.net_value9 as varchar (20) )
							----from epacube.product_values pv
							----where pv.data_name_fk = dn.parent_data_name_fk
							----and   pv.product_structure_fk = ps.product_structure_id
							----and   pv.org_entity_structure_fk = es.entity_structure_id
							----)
       ----                  WHEN 'NET_VALUE10' THEN
       ----                   ( select top 1 cast ( pv.net_value10 as varchar (20) )
							----from epacube.product_values pv
							----where pv.data_name_fk = dn.parent_data_name_fk
							----and   pv.product_structure_fk = ps.product_structure_id
							----and   pv.org_entity_structure_fk = es.entity_structure_id
							----)
                    END
			  WHEN 152 THEN
					CASE ds.column_name
                         WHEN 'NET_VALUE1' THEN
					      ( select top 1 cast ( sr.net_value1 as varchar (20) )
							from marginmgr.sheet_results sr
							where sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = es.entity_structure_id
							)
                         WHEN 'NET_VALUE2' THEN
					      ( select top 1 cast ( sr.net_value2 as varchar (20) )
							from marginmgr.sheet_results sr
							where sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = es.entity_structure_id
							)
                         WHEN 'NET_VALUE3' THEN
					      ( select top 1 cast ( sr.net_value3 as varchar (20) )
							from marginmgr.sheet_results sr
							where sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = es.entity_structure_id
							)
                         WHEN 'NET_VALUE4' THEN
					      ( select top 1 cast ( sr.net_value4 as varchar (20) )
							from marginmgr.sheet_results sr
							where sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = es.entity_structure_id
							)
                         WHEN 'NET_VALUE5' THEN
					      ( select top 1 cast ( sr.net_value5 as varchar (20) )
							from marginmgr.sheet_results sr
							where sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = es.entity_structure_id
							)
                         WHEN 'NET_VALUE6' THEN
					      ( select top 1 cast ( sr.net_value6 as varchar (20) )
							from marginmgr.sheet_results sr
							where sr.data_name_fk = dn.parent_data_name_fk
							and   sr.product_structure_fk = ps.product_structure_id
							and   sr.org_entity_structure_fk = es.entity_structure_id
							)
       ----                  WHEN 'NET_VALUE7' THEN
					  ----    ( select top 1 cast ( sr.net_value7 as varchar (20) )
							----from marginmgr.sheet_results sr
							----where sr.data_name_fk = dn.parent_data_name_fk
							----and   sr.product_structure_fk = ps.product_structure_id
							----and   sr.org_entity_structure_fk = es.entity_structure_id
							----)
       ----                  WHEN 'NET_VALUE8' THEN
					  ----    ( select top 1 cast ( sr.net_value8 as varchar (20) )
							----from marginmgr.sheet_results sr
							----where sr.data_name_fk = dn.parent_data_name_fk
							----and   sr.product_structure_fk = ps.product_structure_id
							----and   sr.org_entity_structure_fk = es.entity_structure_id
							----)
       ----                  WHEN 'NET_VALUE9' THEN
					  ----    ( select top 1 cast ( sr.net_value9 as varchar (20) )
							----from marginmgr.sheet_results sr
							----where sr.data_name_fk = dn.parent_data_name_fk
							----and   sr.product_structure_fk = ps.product_structure_id
							----and   sr.org_entity_structure_fk = es.entity_structure_id
							----)
       ----                  WHEN 'NET_VALUE10' THEN
					  ----    ( select top 1 cast ( sr.net_value10 as varchar (20) )
							----from marginmgr.sheet_results sr
							----where sr.data_name_fk = dn.parent_data_name_fk
							----and   sr.product_structure_fk = ps.product_structure_id
							----and   sr.org_entity_structure_fk = es.entity_structure_id
							----)
                    END
        END  -- END OF CASE
 ----------                             
		FROM epacube.data_name dn 
		INNER JOIN epacube.data_set ds on ( dn.data_set_fk = ds.data_set_id )
		INNER JOIN epacube.product_structure ps
		   ON ( dn.record_status_cr_fk = ps.record_status_cr_fk )
		INNER JOIN epacube.entity_structure es
		   ON ( ps.record_status_cr_fk = es.record_status_cr_fk )
		WHERE dn.data_name_id = @in_data_name_fk
		AND   ps.product_structure_id = @in_product_structure_fk
		AND   es.entity_structure_id = @in_org_entity_structure_fk
   )


-- Statement that inserts result
insert into @GCVTab
VALUES (@in_product_structure_fk      
       ,@in_org_entity_structure_fk   
       ,@in_data_name_fk              
       ,@v_value )

RETURN 
END
