﻿

-- Copyright 2007
--
-- Function created by Cindy Voutour
--
--
-- Purpose: Purpose of this Function is to grab all product events for a specific export
-- where the products have been changed since the last export. Logic is that any 
-- product_structure_fk with an update_timestamp greater than the most recent completion
-- of Export Type X has been "changed".
--
-- Initial version takes data as input, may be changed. May also need data name updates.
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        02/01/2012  Initial SQL Version
--


CREATE FUNCTION [epacube].[Get_Changed_Products_TAX_ATTRIBUTES]( @FROM_DATE DATETIME )
 RETURNS  @changedExportProducts TABLE 
( 
    -- Columns returned by the function
    ProductStructureID BIGINT NOT NULL    
      )

AS
-- Returns the product_structure_fk, Org_entity_Structure, and Entity_structure_fk
--	 for all products updated since the last export


BEGIN



 INSERT into @changedExportProducts (ProductStructureID )
SELECT DISTINCT a.PRODUCT
FROM (

---- PRODUCT EVENTS 

SELECT edh.product_structure_fk PRODUCT        
 FROM synchronizer.EVENT_DATA_HISTORY EDH WITH (NOLOCK)

INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
										 ON ( DN.DATA_NAME_ID = EDH.DATA_NAME_FK)

 INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
ON (DN.DATA_SET_FK = DS.DATA_SET_ID
AND DS.TABLE_NAME = 'PRODUCT_TAX_ATTRIBUTE')
WHERE 1=1
AND   EDH.EVENT_TYPE_CR_FK IN ( 153)  -- product attributes/uom only
AND EDH.EVENT_ACTION_CR_FK NOT IN (56) --No Change
AND (  
	----Or if in the export config --  
---other things that are not prod tax attibute 
--- item file from Kevin no embeded in 
EDH.EVENT_SOURCE_CR_FK NOT IN (79) --Host Import
OR (	EDH.EVENT_STATUS_CR_FK = 82
AND    EDH.EVENT_SOURCE_CR_FK = 79 )
)
AND   EDH.UPDATE_TIMESTAMP >= @FROM_DATE

)A




   RETURN 
END;
