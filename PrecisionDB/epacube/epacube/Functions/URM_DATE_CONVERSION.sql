﻿
CREATE FUNCTION [epacube].[URM_DATE_CONVERSION]
(
    @String NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

--Declare @ConvDate as date
Declare @ConvDate as Varchar(64)
    
        --SET @ConvDate = Cast(Case When right(left(cast(ltrim(rtrim(@String)) as Numeric(18, 6)), 7), 1) = '.' then '19' + Cast(left(cast(ltrim(rtrim(@String)) as Numeric(18, 6)), 6) as varchar(8)) else '20' + Cast(right(left(cast(ltrim(rtrim(@String)) as Numeric(18, 6)), 7), 6) as varchar(8)) end as date)
		SET @ConvDate = Case When right(left(cast(ltrim(rtrim(@String)) as Numeric(18, 6)), 7), 1) = '.' then '19' + Cast(left(cast(ltrim(rtrim(@String)) as Numeric(18, 6)), 6) as varchar(8)) else '20' + Cast(right(left(cast(ltrim(rtrim(@String)) as Numeric(18, 6)), 7), 6) as varchar(8)) end
    
		if isdate(@ConvDate) = 1
		SET @ConvDate = Cast(@ConvDate as date)
		Else
		SET @ConvDate = Null

	RETURN @ConvDate

END
