﻿
-- Copyright 2007
--
-- Function created by Kathi Scott
--
--
-- Purpose: Purpose of this Function is to grab all product/orgs for a specific export
-- where the products have been changed since the last export. Logic is that any 
-- product_structure_fk with an update_timestamp greater than the most recent completion
-- of Export Type X has been "changed" or "rejected"
--   Note:  Rejected Events from HOST only are also logged in history
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        06/08/2009  Initial SQL Version
--


CREATE FUNCTION [epacube].[Get_Changed_ProdOrgs_By_Export]( @ExportName varchar(256) )
RETURNS @changedExportProducts TABLE 
(
    -- Columns returned by the function
    Product_Structure_FK bigint NOT NULL   
   ,Org_Entity_Structure_FK BIGINT NOT NULL 
)
AS
-- Returns the product_structure_fk for all products updated since the last export
BEGIN


 INSERT into @changedExportProducts (Product_Structure_FK, Org_Entity_Structure_FK)
        
SELECT DISTINCT edh.product_structure_fk, ISNULL (edh.org_entity_structure_fk, 1 )
 FROM synchronizer.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
 INNER JOIN EPACUBE.CONFIG_DATA_NAME CDN WITH (NOLOCK)
										 ON ( CDN.DATA_NAME_FK = EDH.DATA_NAME_FK
                                         AND  CDN.CONFIG_FK = ( SELECT CAST ( VALUE AS INT )
														         FROM epacube.CONFIG CNFJ WITH (NOLOCK)
														         INNER JOIN EPACUBE.CONFIG_PROPERTY CNFJP WITH (NOLOCK)
														         ON ( CNFJP.CONFIG_FK = CNFJ.CONFIG_ID
														         AND  CNFJP.NAME = 'COLUMNS_CONFIG_ID' )
														         WHERE CNFJ.NAME = @ExportName ) )
WHERE 1=1
AND   EDH.UPDATE_TIMESTAMP >= ISNULL ( 
									( SELECT MAX ( JOB.JOB_COMPLETE_TIMESTAMP )
										FROM COMMON.JOB JOB WITH (NOLOCK)
										WHERE NAME = @ExportName  )
                                    , '01/01/2009' )
        

    
    RETURN
END;
