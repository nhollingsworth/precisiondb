﻿

-- =============================================
-- Author:		Gary Stone
-- Create date: 10/26/2017
-- Description:	quick fetch for value from data_value_id
-- =============================================
create FUNCTION [epacube].[getDataValueForID]
(
	@Data_Value_ID bigint
)
RETURNS varchar(64)
AS
BEGIN
	DECLARE @c varchar(64)

		select @c = dv.value from epacube.data_value dv with (nolock) where data_value_id = @Data_value_ID

	RETURN @c
END


