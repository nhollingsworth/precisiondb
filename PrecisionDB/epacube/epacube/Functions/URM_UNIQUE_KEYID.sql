﻿
CREATE FUNCTION [epacube].[URM_UNIQUE_KEYID]
(
    @Data_Name_FK Varchar(64), @Precedence_Customer Varchar(64), @Precedence_Product Varchar(64), @Effective varchar(64)
	, @Prod_Data_Name_FK Varchar(64), @Prod_Segment_FK Varchar(64), @Cust_Data_Name_FK Varchar(64), @Cust_Segment_FK Varchar(64)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

Declare @Unique_KeyID as Varchar(256)

		SET @Unique_KeyID = '[' + @Data_Name_FK + '][' + @Precedence_Customer + isnull(@Precedence_Product, '00') + '][' + Replace(@Effective, '-', '') + '][' + @Prod_Data_Name_FK + '][' + @Prod_Segment_FK + '][' + @Cust_Data_Name_FK
							+ '][' + @Cust_Segment_FK + ']'
    
	RETURN @Unique_KeyID

END

