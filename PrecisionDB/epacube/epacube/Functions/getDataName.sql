﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [epacube].[getDataName]
(
	-- Add the parameters for the function here
	@id int
)
RETURNS varchar(80)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @name varchar(80)

	-- Add the T-SQL statements to compute the return value here
	SELECT @name = name from epacube.data_name where data_name_id = @id

	-- Return the result of the function
	RETURN @name

END


