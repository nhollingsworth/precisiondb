﻿
-- =============================================
-- Author:		Geoff Begley
-- Create date: 4/28/2008
-- Description:	quick fetch for code from code_ref_id
-- =============================================
CREATE FUNCTION [epacube].[getLabelForId]
(
	@dataNameId int
)
RETURNS varchar(32)
AS
BEGIN
	DECLARE @l varchar(32)

    select @l=label from epacube.data_name WITH (NOLOCK)
	where data_name_id=@dataNameId

	RETURN @l
END

