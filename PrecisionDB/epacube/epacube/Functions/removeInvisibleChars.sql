﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 5/19/2009
-- Description:	Remove trailing 0's from Decimal
-- =============================================
create FUNCTION [epacube].[removeInvisibleChars]
(
	@in_str varchar(max)
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @out_str VARCHAR(max)

	SET	@out_str = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@in_str, CHAR(9),''), CHAR(13),''), CHAR(10), '')))
  
	RETURN @out_str
END

