﻿
Create FUNCTION [epacube].[STR_TRANSLATE_varchar] (
@source as varchar(8000),
@from as varchar(8000),
@to as varchar(8000)
) returns varchar(8000)
Begin
  Declare @retval varchar(8000), @LenFrom Integer, @LenTo Integer, @ind Integer, @StrInd Integer
  Set @LenFrom = datalength(@from)
  Set @LenTo = datalength(@to)
  Set @retval = @source

  If datalength(@retval) = 0 Or @LenFrom = 0 Or @LenTo = 0
    Begin
      Set @retval = null
      return @retval
    End

  Set @StrInd = 1
  While @StrInd <= datalength(@retval)
    Begin
      Set @ind = CharIndex(SubString(@retval, @StrInd, 1) COLLATE Latin1_General_BIN, @from COLLATE Latin1_General_BIN)
      If @ind > 0
        Begin
          Set @retval = Stuff(@retval, @StrInd, 1,  SubString(@to, @ind, 1))
          If @ind > @LenTo Set @StrInd = @StrInd - 1
        End
      Set @StrInd = @StrInd + 1
    End  -- While

  return @retval
End
