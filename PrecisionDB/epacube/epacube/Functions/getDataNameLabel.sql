﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [epacube].[getDataNameLabel]
(
	-- Add the parameters for the function here
	@DATA_NAME_ID int
)
RETURNS VARCHAR(64)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @LABEL varchar(64)

	-- Add the T-SQL statements to compute the return value here
	SELECT @LABEL = LABEL from epacube.data_name where data_name_id = @DATA_NAME_ID

	-- Return the result of the function
	RETURN @LABEL

END


