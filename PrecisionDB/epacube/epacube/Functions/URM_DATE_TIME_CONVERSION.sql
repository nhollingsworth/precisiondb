﻿
CREATE FUNCTION [epacube].[URM_DATE_TIME_CONVERSION]
(
    @DateString NVARCHAR(64), @TimeString NVarchar(64) = Null
)
RETURNS datetime
AS
BEGIN

--Declare @DateString NVARCHAR(64)
--Declare @TimeString NVarchar(64)

--Set @DateString = '940718'
--Set @TimeString = '730'

Declare @ConvDate as nvarchar(64), @TimeConv as nvarchar(64), @NewDateTime as datetime, @NewDateTimeVChar as Varchar(64)

  
        SET @ConvDate = Case When right(left(cast(ltrim(rtrim(@DateString)) as Numeric(18, 6)), 7), 1) = '.' then '19' + Cast(left(cast(ltrim(rtrim(@DateString)) as Numeric(18, 6)), 6) as varchar(8)) else '20' + Cast(right(left(cast(ltrim(rtrim(@DateString)) as Numeric(18, 6)), 7), 6) as varchar(8)) end
		Set @TimeConv = Case when isnull(@TimeString, '') = '' then ''
						when isnumeric(@TimeString) = 0 then ''
						else left(Right('000000' + @TimeString, 6), 2) + ':' + substring(Right('000000' + @TimeString, 6), 3, 2) + ':' + Right(Right('000000' + @TimeString, 6), 2) end

		--Set @NewDateTime = Case	when @TimeString = '' then @ConvDate
		--						When isdate(@ConvDate) = 0 then ''
		--						When isdate(@ConvDate + ' ' + @TimeConv) = 0 then @ConvDate
		--						else cast(@ConvDate + ' ' + @TimeConv as datetime) end

		Set @NewDateTimeVChar = Case	when @TimeString = '' then @ConvDate
								When isdate(@ConvDate) = 0 then ''
								When isdate(@ConvDate + ' ' + @TimeConv) = 0 then @ConvDate
								else @ConvDate + ' ' + @TimeConv end

		If isdate(@NewDateTimeVChar) = 1
			Set @NewDateTime = Cast(@NewDateTimeVChar as datetime)
		Else
			Set @NewDateTime = Null

	Return @NewDateTime
END
