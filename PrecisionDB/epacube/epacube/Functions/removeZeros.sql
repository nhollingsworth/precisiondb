﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 5/19/2009
-- Description:	Remove trailing 0's from Decimal
-- =============================================
CREATE FUNCTION [epacube].[removeZeros]
(
	@in_number varchar(32)
)
RETURNS VARCHAR(32)
AS
BEGIN
	DECLARE @out_number VARCHAR(32)
	       ,@v_zeros    VARCHAR(32)

  
set @v_zeros = reverse(substring(@in_number,patindex('%.%',@in_number)+1,len(@in_number)))  
  
SET @out_number = ( SELECT  CASE   
----							WHEN  patindex('%.%[1-9]%',@in_number) = 0 THEN substring(@in_number,1,patindex('%.%',@in_number)-1)  
							WHEN  patindex('%.%',@in_number) = 0 THEN @in_number
							ELSE  CASE WHEN  patindex('%.%[1-9]%',@in_number) = 0 
							           THEN substring(@in_number,1,patindex('%.%',@in_number)-1)  
							           ELSE  substring(@in_number,1,patindex('%.%',@in_number)-1) + '.' +  Reverse(substring(@v_zeros,patindex('%[1-9]%',@v_zeros),len(@v_zeros)))  
							           END
							END  )
  
	RETURN @out_number
END

