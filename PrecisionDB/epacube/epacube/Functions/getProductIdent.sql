﻿


CREATE
    FUNCTION [epacube].[getProductIdent] 
      (
        @in_Product_Structure_ID		bigint

      ) 
      RETURNS varchar(256)
    AS
      
      BEGIN

        DECLARE 

          @l_count bigint,
          @ls_exec varchar(max),
          @l_exec_no bigint,
          @v_class int,
          @v_ident varchar(256)

------------------------------------------------------------------------------------------------------------
--Get Display Default Identification for entity structure input
------------------------------------------------------------------------------------------------------------
	SET @v_ident = 
        (select pi.value from epacube.product_identification pi
                     inner join epacube.epacube_params eep on 
                      ( cast ( eep.value as int ) = pi.data_name_fk
                        and  eep.name = 'PRODUCT' and eep.application_scope_fk = 100 )
                     where product_structure_fk=@in_product_structure_id )

   RETURN ( @v_ident )
END
















