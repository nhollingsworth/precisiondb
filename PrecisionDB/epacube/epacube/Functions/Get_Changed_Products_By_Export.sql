﻿

-- Copyright 2007
--
-- Function created by Robert Gannon
--
--
-- Purpose: Purpose of this Function is to grab all products for a specific export
-- where the products have been changed since the last export. Logic is that any 
-- product_structure_fk with an update_timestamp greater than the most recent completion
-- of Export Type X has been "changed".
--
-- Initial version takes data as input, may be changed. May also need data name updates.
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        12/13/2007  Initial SQL Version
-- WT        04/01/2008  EPA-1616. Fix non-performant sql queries.
-- KS        05/15/2008  EPA-1773. Change Job timestamp from Job table not execution
-- KS		 05/15/2008  EPA-XXXX. Include Rejected Events from Host into Export
-- KS        11/15/2008  V5 Approach Use Event_Data_History Table and added Product,Org and Entity
-- CV		 10/27/2009  EPA - 2626 when results were changed did not export.  Added code below to check for 
--						 parent result and values data name
-- CV        11/19/2009  Added UNION ALL to solve issue of duplicate products exporting on first run.
-- CV        05/05/2010  Added event type of 156 UOM to export
-- CV		 05/11/2010  Added AND EDH.EVENT_SOURCE_CR_FK <> 79 host import EPA-2966
-- CV        02/17/2011  Added EDH.EVENT_ACTION_CR_FK NOT IN (56) --No Change EPA-31640
-- CV        06/11/2015  Add 30 min to timestamp to pickup things just posted.
-- CV        07/28/2015  ADDED TO USE THE ARCHIVE PERIOD IF EXPORT JOB IS MISSING


CREATE FUNCTION [epacube].[Get_Changed_Products_By_Export]( @ExportName varchar(256) )
 RETURNS  @changedExportProducts TABLE 
( 
    -- Columns returned by the function
    ProductStructureID BIGINT NOT NULL    
   ,OrgEntityStructureID BIGINT NOT NULL 
   ,EntityStructureID BIGINT NOT NULL    
)

AS
-- Returns the product_structure_fk, Org_entity_Structure, and Entity_structure_fk
--	 for all products updated since the last export



BEGIN



 INSERT into @changedExportProducts (ProductStructureID, OrgEntityStructureID, EntityStructureID )
SELECT DISTINCT a.PRODUCT, a.ORG, a.ENTITY
FROM (

---- PRODUCT EVENTS 

SELECT edh.product_structure_fk PRODUCT, ISNULL (edh.org_entity_structure_fk, 1) ORG, ISNULL (edh.entity_structure_fk, 0) ENTITY          
 FROM synchronizer.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
 INNER JOIN EPACUBE.CONFIG_DATA_NAME CDN WITH (NOLOCK)
										 ON ( CDN.DATA_NAME_FK = EDH.DATA_NAME_FK
                                         AND  CDN.CONFIG_FK = ( SELECT CAST ( VALUE AS BIGINT )
														         FROM epacube.CONFIG CNFJ WITH (NOLOCK)
														         INNER JOIN EPACUBE.CONFIG_PROPERTY CNFJP WITH (NOLOCK)
														         ON ( CNFJP.CONFIG_FK = CNFJ.CONFIG_ID
														         AND  CNFJP.NAME = 'COLUMNS_CONFIG_ID' )
														         WHERE CNFJ.NAME = @ExportName ) )
WHERE 1=1
AND   EDH.EVENT_TYPE_CR_FK IN ( 150, 156 )  -- product attributes/uom only
AND EDH.EVENT_ACTION_CR_FK NOT IN (56) --No Change
AND (  
	
EDH.EVENT_SOURCE_CR_FK NOT IN (79) --Host Import
OR (	EDH.EVENT_STATUS_CR_FK = 82
AND    EDH.EVENT_SOURCE_CR_FK = 79 )
)
AND   EDH.UPDATE_TIMESTAMP >= ISNULL ( 
									( SELECT MAX ( dateadd(minute,-30,JOB.JOB_COMPLETE_TIMESTAMP) )
										FROM COMMON.JOB JOB WITH (NOLOCK)
										WHERE NAME = @ExportName  )
                                    , cast(getdate() - (select cast(value as int)  from epacube.EPACUBE_PARAMS where name = 'ARCHIVE_PERIOD') as date) )



----TO GET ASSOCIATIONS THAT ARE PART OF EXPORT DATA (IE WAREHOUSE)

UNION ALL

SELECT edh.product_structure_fk PRODUCT, ISNULL (edh.org_entity_structure_fk, 1 ) ORG, ISNULL (edh.entity_structure_fk, 0 ) ENTITY         
 FROM synchronizer.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
ON (DN.DATA_NAME_ID = EDH.DATA_NAME_FK)
 INNER JOIN EPACUBE.CONFIG_DATA_NAME CDN WITH (NOLOCK)
										 ON ( CDN.DATA_NAME_FK = DN.DATA_NAME_ID
                                         AND  CDN.CONFIG_FK = ( SELECT CAST ( VALUE AS BIGINT )
														         FROM epacube.CONFIG CNFJ WITH (NOLOCK)
														         INNER JOIN EPACUBE.CONFIG_PROPERTY CNFJP WITH (NOLOCK)
														         ON ( CNFJP.CONFIG_FK = CNFJ.CONFIG_ID
														         AND  CNFJP.NAME = 'COLUMNS_CONFIG_ID' )
														         WHERE CNFJ.NAME = @ExportName ) )
WHERE 1=1
AND   EDH.EVENT_TYPE_CR_FK IN ( 155 )  -- Assoc
AND   EDH.RECORD_STATUS_CR_FK = 1
AND   EDH.UPDATE_TIMESTAMP >= ISNULL ( 
									( SELECT MAX ( dateadd(minute,-30,JOB.JOB_COMPLETE_TIMESTAMP) )
										FROM COMMON.JOB JOB WITH (NOLOCK)
										WHERE NAME = @ExportName  )
                                    , cast(getdate() - (select cast(value as int)  from epacube.EPACUBE_PARAMS where name = 'ARCHIVE_PERIOD') as date) )
----
--------TO GET SYSTEM ASSOC FUTURE JIRA - 2961 works, but also exports for ICSC changes when associating to a system product
UNION ALL

SELECT edh.product_structure_fk PRODUCT, ISNULL (edh.org_entity_structure_fk, 1 ) ORG, 0  ENTITY         
 FROM synchronizer.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
LEFT JOIN EPACUBE.ENTITY_IDENTIFICATION EI
ON (EI.ENTITY_STRUCTURE_FK = EDH.ENTITY_STRUCTURE_FK
AND EI.DATA_NAME_FK = 142111
AND EI.VALUE in ('PRODUCT', 'CATALOG'))
--AND @ExportName = 'ICSP Export CHANGES'

WHERE 1=1
AND   EDH.EVENT_TYPE_CR_FK IN ( 155 )  -- Assoc
AND   EDH.DATA_NAME_FK = 159200
AND   EDH.NEW_DATA in ('PRODUCT', 'CATALOG')
AND   EDH.UPDATE_TIMESTAMP >= ISNULL ( 
									( SELECT MAX ( dateadd(minute,-30,JOB.JOB_COMPLETE_TIMESTAMP) )
										FROM COMMON.JOB JOB WITH (NOLOCK)
										WHERE NAME = @ExportName  )
                                   , cast(getdate() - (select cast(value as int)  from epacube.EPACUBE_PARAMS where name = 'ARCHIVE_PERIOD') as date) )
----
----UNION ALL
----
----SELECT edh.product_structure_fk PRODUCT, ISNULL (edh.org_entity_structure_fk, 1 ) ORG, 0  ENTITY         
---- FROM synchronizer.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
----LEFT JOIN EPACUBE.ENTITY_IDENTIFICATION EI
----ON (EI.ENTITY_STRUCTURE_FK = EDH.ENTITY_STRUCTURE_FK
----AND EI.DATA_NAME_FK = 142111
----AND EI.VALUE = 'CATALOG')
----AND @ExportName = 'ICSC Export CHANGES'
----
----WHERE 1=1
----AND   EDH.EVENT_TYPE_CR_FK IN ( 155 )  -- Assoc
----AND   EDH.DATA_NAME_FK = 159200
----AND   EDH.NEW_DATA = 'CATALOG'
----AND   EDH.UPDATE_TIMESTAMP >= ISNULL ( 
----									( SELECT MAX ( JOB.JOB_COMPLETE_TIMESTAMP )
----										FROM COMMON.JOB JOB WITH (NOLOCK)
----										WHERE NAME = @ExportName  )
----                                   , '01/01/2009' )

----TO GET VALUES


UNION ALL

SELECT edh.product_structure_fk PRODUCT, ISNULL (edh.org_entity_structure_fk, 1) ORG, ISNULL (edh.entity_structure_fk, 0) ENTITY       
 FROM synchronizer.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
ON (DN.PARENT_DATA_NAME_FK = EDH.DATA_NAME_FK)
 INNER JOIN EPACUBE.CONFIG_DATA_NAME CDN WITH (NOLOCK)
										 ON ( CDN.DATA_NAME_FK = DN.DATA_NAME_ID
                                         AND  CDN.CONFIG_FK = ( SELECT CAST ( VALUE AS BIGINT )
														         FROM epacube.CONFIG CNFJ WITH (NOLOCK)
														         INNER JOIN EPACUBE.CONFIG_PROPERTY CNFJP WITH (NOLOCK)
														         ON ( CNFJP.CONFIG_FK = CNFJ.CONFIG_ID
														         AND  CNFJP.NAME = 'COLUMNS_CONFIG_ID' )
														         WHERE CNFJ.NAME = @ExportName ) )
WHERE 1=1
AND   EDH.EVENT_TYPE_CR_FK IN ( 151 )  -- Values Results
AND EDH.EVENT_ACTION_CR_FK NOT IN (56) --No Change
AND (  
	EDH.EVENT_SOURCE_CR_FK NOT IN (79) --Host Import
OR (	EDH.EVENT_STATUS_CR_FK = 82
AND    EDH.EVENT_SOURCE_CR_FK = 79 )
)
AND   EDH.UPDATE_TIMESTAMP >= ISNULL ( 
									( SELECT MAX ( dateadd(minute,-30,JOB.JOB_COMPLETE_TIMESTAMP))
										FROM COMMON.JOB JOB WITH (NOLOCK)
										WHERE NAME = @ExportName  )
                                    , cast(getdate() - (select cast(value as int)  from epacube.EPACUBE_PARAMS where name = 'ARCHIVE_PERIOD') as date))


----TO GET RESULTS

UNION ALL

SELECT edh.product_structure_fk PRODUCT, ISNULL (edh.org_entity_structure_fk, 1) ORG, ISNULL (edh.entity_structure_fk, 0) ENTITY         
 FROM synchronizer.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
ON (DN.PARENT_DATA_NAME_FK = EDH.DATA_NAME_FK)
 INNER JOIN EPACUBE.CONFIG_DATA_NAME CDN WITH (NOLOCK)
										 ON ( CDN.DATA_NAME_FK = DN.DATA_NAME_ID
                                         AND  CDN.CONFIG_FK = ( SELECT CAST ( VALUE AS BIGINT )
														         FROM epacube.CONFIG CNFJ WITH (NOLOCK)
														         INNER JOIN EPACUBE.CONFIG_PROPERTY CNFJP WITH (NOLOCK)
														         ON ( CNFJP.CONFIG_FK = CNFJ.CONFIG_ID
														         AND  CNFJP.NAME = 'COLUMNS_CONFIG_ID' )
														         WHERE CNFJ.NAME = @ExportName ) )
WHERE 1=1
AND   EDH.EVENT_TYPE_CR_FK IN ( 152 )  -- Sheet Results
AND EDH.EVENT_ACTION_CR_FK NOT IN (56) --No Change
AND (  
EDH.EVENT_SOURCE_CR_FK NOT IN (79) --Host Import
OR (	EDH.EVENT_STATUS_CR_FK = 82
AND    EDH.EVENT_SOURCE_CR_FK = 79 )
)
AND   EDH.UPDATE_TIMESTAMP >= ISNULL ( 
									( SELECT MAX ( dateadd(minute,-30,JOB.JOB_COMPLETE_TIMESTAMP) )
										FROM COMMON.JOB JOB WITH (NOLOCK)
										WHERE NAME = @ExportName  )
                                    ,cast(getdate() - (select cast(value as int)  from epacube.EPACUBE_PARAMS where name = 'ARCHIVE_PERIOD') as date))
) A



   RETURN 
END;

