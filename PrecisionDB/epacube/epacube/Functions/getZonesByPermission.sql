﻿
CREATE FUNCTION [epacube].[getZonesByPermission](@User varchar(64))
returns TABLE 
as Return

--Declare @User varchar(64)
--Set @User = 'gstone@epacube.com'

		Select ei.entity_structure_fk from epacube.entity_identification ei with (nolock)
		where 1 = 1
		and ei.entity_data_name_fk = 151000 and ei.data_name_fk = 151110
		and (
				ei.ENTITY_STRUCTURE_FK in
					(Select entity_structure_fk 
					from epacube.USER_ENTITY_PERMISSIONS UEP with (nolock)
					inner join epacube.users U with (nolock) on uep.users_fk = u.USERS_ID
					where replace([login], '.', '_') = replace(@User, '.', '_'))
		
				or 
			
				ei.entity_structure_fk in
					(Select ei.entity_structure_fk from epacube.entity_identification ei with (nolock)
					inner join epacube.entity_attribute ea with (nolock) on ei.ENTITY_STRUCTURE_FK = ea.ENTITY_STRUCTURE_FK and ea.DATA_NAME_FK = 502060
					inner join epacube.data_value dv with (nolock) on ea.DATA_VALUE_FK = dv.DATA_VALUE_ID
					where 1 = 1
					and dv.value = 'H'
					and replace(@User, '.', '_') in
							(Select replace([login], '.', '_') 
							from epacube.users u with (nolock)
							inner join EPACUBE.USER_GROUPS UG with (nolock) on U.users_id = UG.USERS_FK
							inner join EPACUBE.GROUP_PERMISSIONS GP with (nolock) on UG.groups_FK = GP.groups_fk
							inner join epacube.PERMISSIONS P on GP.Permissions_FK = P.permissions_id
							where replace([login], '.', '_') = replace(@User, '.', '_') 
							and UG.GROUPS_FK in (10021)))

				or 
			
				ei.entity_structure_fk in
					(Select ei.entity_structure_fk from epacube.entity_identification ei with (nolock)
					inner join epacube.entity_attribute ea with (nolock) on ei.ENTITY_STRUCTURE_FK = ea.ENTITY_STRUCTURE_FK and ea.DATA_NAME_FK = 502060
					inner join epacube.data_value dv with (nolock) on ea.DATA_VALUE_FK = dv.DATA_VALUE_ID
					where 1 = 1
					and dv.value = 'R'
					and replace(@User, '.', '_') in
							(Select replace([login], '.', '_') 
							from epacube.users u with (nolock)
							inner join EPACUBE.USER_GROUPS UG with (nolock) on U.users_id = UG.USERS_FK
							inner join EPACUBE.GROUP_PERMISSIONS GP with (nolock) on UG.groups_FK = GP.groups_fk
							inner join epacube.PERMISSIONS P on GP.Permissions_FK = P.permissions_id
							where replace([login], '.', '_') = replace(@User, '.', '_') 
							and UG.GROUPS_FK in (10022)))

				or

				replace(@User, '.', '_') in
					(Select replace([login], '.', '_') 
					from epacube.users u with (nolock)
					inner join EPACUBE.USER_GROUPS UG with (nolock) on U.users_id = UG.USERS_FK
					inner join EPACUBE.GROUP_PERMISSIONS GP with (nolock) on UG.groups_FK = GP.groups_fk
					inner join epacube.PERMISSIONS P on GP.Permissions_FK = P.permissions_id
					where replace([login], '.', '_') = replace(@User, '.', '_') 
					and replace(@User, '.', '_') <> 'gstone@epacube_com'
					and UG.GROUPS_FK in (2))
			)



			
