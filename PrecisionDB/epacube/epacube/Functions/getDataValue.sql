﻿
-- Copyright 2019 epaCUBE, Inc.
--
-- Function created by Neal Hollingsworth
--
-- Purpose:	Get data values JIRA (URM-1201)
-- Get all data value information and return in a 
-- hierarchical JSON format
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE OR ALTER FUNCTION [epacube].[getDataValue](@data_value_id BIGINT) 
RETURNS VARCHAR(MAX)
BEGIN 
   DECLARE @json NVARCHAR(MAX) = '{}' 
   DECLARE @children NVARCHAR(MAX)

	SET @json = 
	(SELECT dv.DATA_VALUE_ID
		, ds.NAME DATA_VALUE_TYPE
		, dn.LABEL DATA_VALUE_LABEL
		, dv.VALUE
		, dv.DESCRIPTION
		, dn.DATA_NAME_ID
		, JSON_QUERY(epacube.getDataValue(dv.DATA_VALUE_ID)) AS CHILDREN 
	FROM epacube.DATA_NAME dn
		INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
		INNER JOIN epacube.DATA_VALUE dv ON dn.DATA_NAME_ID = dv.DATA_NAME_FK
	WHERE dv.RECORD_STATUS_CR_FK = 1 
		AND dv.PARENT_DATA_VALUE_FK = @data_value_id
	FOR JSON PATH);

    RETURN @json
END 
