﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 12/21/2009
-- Description:	quick fetch for code from code_ref_id
-- =============================================
CREATE FUNCTION [epacube].[getUOMForId]
(
	@UOMCodeId int
)
RETURNS varchar(32)
AS
BEGIN
	DECLARE @c varchar(32)

    select @c=uom_code from epacube.uom_code
	where uom_code_id=@UOMCodeId

	RETURN @c
END

