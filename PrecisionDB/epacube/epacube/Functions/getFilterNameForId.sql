﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 11/28/2009
-- Description:	quick fetch for xref from rules
-- =============================================
CREATE FUNCTION [epacube].[getFilterNameForId]
(
	@FilterId int
)
RETURNS varchar(32)
AS
BEGIN
	DECLARE @x varchar(32)

    select @x=NAME from synchronizer.RULES_FILTER WITH (NOLOCK)
	where Rules_Filter_id=@FilterId

	RETURN @x
END

