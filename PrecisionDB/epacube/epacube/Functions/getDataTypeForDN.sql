﻿

-- =============================================
-- Author:		Geoff Begley
-- Create date: 4/28/2008
-- Description:	quick fetch for code from code_ref_id
-- =============================================
Create FUNCTION [epacube].[getDataTypeForDN]
(
	@Data_Name varchar(64)
)
RETURNS varchar(64)
AS
BEGIN
	DECLARE @c varchar(64)

	If Isnumeric(@Data_Name) = 0
		select @c = cr.code 
		from epacube.data_name dn with (nolock) 
		inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id 
		inner join epacube.epacube.code_ref cr with (nolock) on ds.DATA_TYPE_CR_FK = cr.CODE_REF_ID
		where dn.name = @Data_Name
	else
		select @c = cr.code 
		from epacube.data_name dn with (nolock) 
		inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id 
		inner join epacube.epacube.code_ref cr with (nolock) on ds.DATA_TYPE_CR_FK = cr.CODE_REF_ID
		where dn.data_name_id = @Data_Name

	RETURN @c
END


