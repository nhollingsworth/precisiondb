﻿
create FUNCTION [epacube].[GetColumnNameFromPackageFK]
(
    @import_package_fk bigint, 
    @unique_seq_ind tinyint
)
RETURNS VARCHAR(50)
AS
BEGIN

    RETURN ISNULL((select column_name from import.import_map_metadata
								where import_package_fk = @import_package_fk and unique_seq_ind = @unique_seq_ind),-999)

END
