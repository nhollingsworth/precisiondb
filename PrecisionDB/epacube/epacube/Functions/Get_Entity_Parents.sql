﻿
CREATE FUNCTION [epacube].[Get_Entity_Parents] (
      @in_Entity_Structure_FK       bigint
      
   )
 RETURNS  @EntityTab table
(
      [Child_Entity_Structure_FK]   bigint,
      [Parent_Entity_Structure_FK]    bigint,
      [Child_Data_Name_FK]           int,
      [Child_Level_Seq]              int

)
AS
BEGIN


WITH EntityHierarchy (Child_Entity_Structure_FK, Parent_Entity_Structure_FK, 
						Child_Data_Name_FK, Child_Level_Seq )
AS
(
-- Anchor member definition
SELECT es.Entity_Structure_ID, es.Parent_Entity_Structure_FK,
       es.Data_Name_FK, es.Level_Seq
FROM epacube.Entity_Structure AS es
where es.Entity_Structure_ID = @in_Entity_Structure_FK
and   es.entity_status_cr_fk = 10200
UNION ALL
-- Recursive member definition
SELECT es2.Entity_Structure_ID, es2.Parent_Entity_Structure_FK,
       es2.Data_Name_FK, es2.Level_Seq
FROM epacube.Entity_Structure AS es2
INNER JOIN EntityHierarchy AS eh
ON es2.Entity_Structure_ID = eh.Parent_Entity_Structure_FK
where es2.entity_status_cr_fk = 10200
)
-- Statement that executes the CTE
insert into @EntityTab
SELECT ehs.Child_Entity_Structure_FK, ehs.Parent_Entity_Structure_FK, 
       ehs.Child_Data_Name_FK, ehs.Child_Level_Seq
from EntityHierarchy ehs


return
end;
