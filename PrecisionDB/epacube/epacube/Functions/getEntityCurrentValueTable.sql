﻿

-- Function created by Kathi Scott
--
--
-- Purpose: This function is called in the UI in Entity Editor
-- It returns the current value for Entity Data Name.
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS   				  Initial SQL Version




CREATE
    FUNCTION [epacube].[getEntityCurrentValueTable] 
      (
        @in_entity_structure_fk         bigint
       ,@in_data_name_fk                 int
       ,@in_data_value_fk               bigint
      ) 
 RETURNS  @GCVTab table
(
        entity_structure_fk         bigint
       ,data_name_fk                 int
       ,current_value                varchar(256)
)
AS
BEGIN

        DECLARE 

          @l_count bigint,
          @ls_exec varchar(max),
          @l_exec_no bigint,
          @v_value varchar(256)


  SET @v_value = 
      ( SELECT 
		CASE ds.table_name
				 WHEN 'ENTITY_CATEGORY'
					   THEN   ( select dv.value
									from epacube.entity_category ec 
									inner join epacube.data_value dv 
									   on ( dv.data_value_id = ec.data_value_fk )
									where ec.entity_structure_fk = es.entity_structure_id
									and   ec.data_name_fk = dn.data_name_id 
                               )
				 WHEN 'ENTITY_MULT_TYPE'
					   THEN   ISNULL (
							  ( select dv.value
									from epacube.entity_mult_type emt 
									inner join epacube.data_value dv 
									   on ( dv.data_value_id = emt.data_value_fk )
									where emt.entity_structure_fk = es.entity_structure_id
									and   emt.data_name_fk = dn.data_name_id 
                                    and   emt.data_value_fk = isnull ( @in_data_value_fk, 0 )
                               )
                              ,
					          ( select TOP 1 dv.value
									from epacube.entity_mult_type emt 
									inner join epacube.data_value dv 
									   on ( dv.data_value_id = emt.data_value_fk )
									where emt.entity_structure_fk = es.entity_structure_id
									and   emt.data_name_fk = dn.data_name_id 
                               )
                           )
				 WHEN 'ENTITY_ATTRIBUTE'
					   THEN CASE ds.data_type_cr_fk
							WHEN 134 
							THEN  ( select dv.value
									from epacube.entity_attribute ea 
									inner join epacube.data_value dv 
									   on ( dv.data_value_id = ea.data_value_fk )
									where ea.entity_structure_fk = es.entity_structure_id
									and   ea.data_name_fk = dn.data_name_id 
								  )
							ELSE ( select ea.attribute_event_data
									from epacube.entity_attribute ea 
									where ea.entity_structure_fk = es.entity_structure_id
									and   ea.data_name_fk = dn.data_name_id 
								 )
						   END
				  WHEN 'ENTITY_IDENTIFICATION'
						THEN  ( select pi.value
								from epacube.entity_identification pi
								where pi.entity_structure_fk = es.entity_structure_id
								and   pi.data_name_fk = dn.data_name_id 
							  )
        END
		FROM epacube.data_name dn 
		INNER JOIN epacube.data_set ds on ( dn.data_set_fk = ds.data_set_id )
		INNER JOIN epacube.entity_structure es
		   ON ( dn.record_status_cr_fk = es.record_status_cr_fk )
		WHERE dn.data_name_id = @in_data_name_fk
		AND   es.entity_structure_id = @in_entity_structure_fk
   )


-- Statement that inserts result
insert into @GCVTab
VALUES (@in_entity_structure_fk      
       ,@in_data_name_fk              
       ,@v_value )

RETURN 
END


















