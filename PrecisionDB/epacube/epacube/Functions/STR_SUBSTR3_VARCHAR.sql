﻿
CREATE FUNCTION [epacube].[STR_SUBSTR3_VARCHAR](@src as varchar(8000), @pos as int, @sub_len as int)
returns varchar(8000)
begin
    declare 
        @strlen as int

    if( @src is NULL) or (@pos is NULL ) or (@sub_len is NULL) or (@sub_len < 1)
        return NULL;
    select @strlen=DATALENGTH(@src)
    if ABS(@pos) > @strlen
        return NULL;

    if @pos = 0 --Per Oracle specification: if position is 0 - it treated as 1
        select @pos = 1

    if @pos > 0 
    begin
        return SUBSTRING(@src, @pos, @sub_len)
    end
    --negative pos treated as reverse order of substring finding
    return SUBSTRING(@src, @strlen + @pos+1, @sub_len)
end
