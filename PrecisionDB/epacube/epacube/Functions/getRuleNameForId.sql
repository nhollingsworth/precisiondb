﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 11/28/2009
-- Description:	quick fetch for xref from rules
-- =============================================
CREATE FUNCTION [epacube].[getRuleNameForId]
(
	@RulesId int
)
RETURNS varchar(32)
AS
BEGIN
	DECLARE @x varchar(32)

    select @x=NAME from synchronizer.RULES WITH (NOLOCK)
	where Rules_id=@RulesId

	RETURN @x
END

