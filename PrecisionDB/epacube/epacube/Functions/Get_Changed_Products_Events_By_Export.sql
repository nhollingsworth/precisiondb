﻿

-- Copyright 2007
--
-- Function created by Robert Gannon
--
--
-- Purpose: Purpose of this Function is to grab all product events for a specific export
-- where the products have been changed since the last export. Logic is that any 
-- product_structure_fk with an update_timestamp greater than the most recent completion
-- of Export Type X has been "changed".
--
-- Initial version takes data as input, may be changed. May also need data name updates.
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/03/2009  Initial SQL Version
--


CREATE FUNCTION [epacube].[Get_Changed_Products_Events_By_Export]( @ExportName varchar(256) )
RETURNS @changedExportProducts TABLE 
(
    -- Columns returned by the function
    Data_Name_FK BIGINT NOT NULL
   ,Product_Structure_FK BIGINT NOT NULL    
   ,Org_Entity_Structure_FK BIGINT NOT NULL 
   ,Entity_Structure_FK BIGINT NOT NULL    
)
AS
-- Returns the product_structure_fk, Org_entity_Structure, and Entity_structure_fk
--	 for all products updated since the last export
BEGIN


 INSERT into @changedExportProducts (Data_Name_FK, Product_Structure_FK, Org_Entity_Structure_FK, Entity_Structure_FK )

SELECT DISTINCT edh.data_name_fk, edh.product_structure_fk, ISNULL (edh.org_entity_structure_fk, 1 ), ISNULL (edh.entity_structure_fk, 0 )           
 FROM synchronizer.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
 INNER JOIN EPACUBE.CONFIG_DATA_NAME CDN WITH (NOLOCK)
										 ON ( CDN.DATA_NAME_FK = EDH.DATA_NAME_FK
                                         AND  CDN.CONFIG_FK = ( SELECT CAST ( VALUE AS INT )
														         FROM epacube.CONFIG CNFJ WITH (NOLOCK)
														         INNER JOIN EPACUBE.CONFIG_PROPERTY CNFJP WITH (NOLOCK)
														         ON ( CNFJP.CONFIG_FK = CNFJ.CONFIG_ID
														         AND  CNFJP.NAME = 'COLUMNS_CONFIG_ID' )
														         WHERE CNFJ.NAME  in (@ExportName) ) )
WHERE 1=1
AND   EDH.EVENT_TYPE_CR_FK NOT IN ( 153, 154, 155, 159 )
AND   EDH.UPDATE_TIMESTAMP >= ISNULL ( 
									( SELECT MAX ( JOB.JOB_COMPLETE_TIMESTAMP )
										FROM COMMON.JOB JOB WITH (NOLOCK)
										WHERE NAME in (@ExportName ) )
                                    , cast(getdate() - (select cast(value as int)  from epacube.EPACUBE_PARAMS where name = 'ARCHIVE_PERIOD') as date) )
        


----    DECLARE @LastExportDate datetime  
----	--DECLARE	@ProductStructureID bigint	       
----     --Find timestamp of most recent export of flavor A
----	SET @LastExportDate = (
----		select max(j.job_create_timestamp)
----		from common.job j
----		inner join common.job_execution je  on (j.job_id = je.job_fk )
----		where j.name = ISNULL ( @ExportName, 'NULL' )
----		and   job_class_fk  = (select job_class_fk from common.JOB_CLASS
----								 where name = 'EXPORT PRODUCTS'  )
----	    and je.job_status_cr_fk = (select code_ref_id 
----								 from epacube.code_Ref 
----								 where  code_type = 'JOB_STATUS' 
----								 AND    code = 'COMPLETE')
----        )

        
------select distinct a.product_structure_fk from (
------	select product_structure_fk 
------	from epacube.product_description
------	where update_timestamp > @LastExportDate
------
------UNION ALL
------
------	select product_structure_fk 
------	from epacube.product_status
------	where update_timestamp > @LastExportDate
------
------UNION ALL
------
------	select product_structure_fk 
------	from epacube.product_identification 
------	where update_timestamp > @LastExportDate
------
------UNION ALL
------
------	select product_structure_fk 
------	from epacube.product_BOM
------	where update_timestamp > @LastExportDate
------
------UNION ALL
------
------	select product_structure_fk 
------	from epacube.product_values
------	where update_timestamp > @LastExportDate
------
------UNION ALL
------
------	select product_structure_fk 
------	from epacube.product_UOM_CLASS
------	where update_timestamp > @LastExportDate
------
------UNION ALL
------
------	select distinct product_structure_fk 
------	from epacube.product_mult_type pmt
------	where pmt.update_timestamp > @LastExportDate
------    group by  pmt.product_structure_fk       -- must have group by many rows
------
------UNION ALL
------
------	select product_structure_fk 
------	from epacube.product_text
------	where update_timestamp > @LastExportDate
------
------
------UNION ALL
------
------	select product_structure_fk 
------	from epacube.product_UOM
------	where update_timestamp > @LastExportDate
------
------
------UNION ALL
------
------	select product_structure_fk 
------	from epacube.product_ident_nonunique
------	where update_timestamp > @LastExportDate
------
------UNION ALL
------
------	select product_structure_fk 
------	from epacube.product_ident_mult
------	where update_timestamp > @LastExportDate
------
------UNION ALL
------
------	select product_structure_fk 
------	from epacube.product_category pc
------	where pc.update_timestamp > @LastExportDate
------
------UNION ALL
------
-------- 
------
------	select product_structure_fk 
------	from epacube.product_attribute
------	where update_timestamp > @LastExportDate
------
------
------UNION ALL
------
------	select product_structure_fk 
------	from epacube.product_association pa
------    where pa.update_timestamp > @LastExportDate
------
--------  Look for Rejected Events from the Host Systems
------
------UNION ALL
------
------	select distinct product_structure_fk 
------	from synchronizer.event_data ep
------	where ep.update_timestamp > @LastExportDate
------    and   ep.event_source_cr_fk = 79  -- host
------    and   ep.event_status_cr_fk = 82  -- rejected
------    group by  ep.product_structure_fk       -- must have group by many rows
------
------
------)a
------ inner join epacube.product_structure ps on (a.product_structure_fk = product_structure_id)
    
    RETURN
END;

