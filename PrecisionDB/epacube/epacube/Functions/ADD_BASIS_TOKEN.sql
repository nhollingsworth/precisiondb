﻿





  CREATE
    FUNCTION [epacube].[ADD_BASIS_TOKEN] 
      (
        @in_basis_position smallint,       
        @in_start_string varchar(8000),
        @in_prefix       varchar(16),                
        @in_token_value varchar(8000),
        @in_rules_action_strfunction_fk bigint
      ) 
      RETURNS varchar(8000)
    AS
      
      BEGIN

        DECLARE 

          @l_count bigint,
          @ls_exec varchar(max),
          @l_exec_no bigint,
          @v_return_string varchar(8000),
          @v_result_string varchar(8000),
          @v_debug_string  varchar (max),
          @v_strip_token varchar(128),
          @v_token_seq int,
          @v_remove_spec_chars varchar(64),
          @v_remove_spaces smallint,
          @v_replace_char char(1),
          @v_replacing_char char(1),
          @v_replace_string char(1),
          @v_replacing_string char(1),          
          @v_substr_start_pos bigint,
          @v_substr_length bigint




--select replace('he llo wo' + isnull(null,'') + ' rld',' ','')
--select replace('he llo wo' + isnull(null,'') + ' rld',' ','X')

      SELECT 
          @v_remove_spec_chars = RASF.SPECIAL_CHARS_SET, --RASF.REMOVE_SPEC_CHARS_IND,   ---REMOVE_SPEC_CHARS, 
          @v_remove_spaces = RASF.REMOVE_SPACES_IND,    ---REMOVE_SPACES,
          @v_replace_char = RASF.REPLACE_CHAR,    ---REPLACE_CHAR, 
          @v_replacing_char = RASF.REPLACING_CHAR,    ---REPLACING_CHAR, 
          @v_replace_string = RASF.REPLACE_STRING, 
          @v_replacing_string = RASF.REPLACING_STRING, 
          @v_substr_start_pos = RASF.SUBSTR_START_POS,    ---SUBSTR_START_POS, 
          @v_substr_length = RASF.SUBSTR_LENGTH    ---SUBSTR_LENGTH
        FROM synchronizer.RULES_ACTION_STRFUNCTION RASF
        WHERE RASF.RULES_ACTION_STRFUNCTION_ID = @in_rules_action_strfunction_fk

          SET @l_count = 0

          SET @l_exec_no = 0

          SET @v_strip_token = '????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????'

          IF (@in_prefix = '?')
            SET @in_prefix = ' '

          IF (@v_replace_char = '?')
            SET @v_replace_char = ' '

          IF (@v_replacing_char = '?')
            SET @v_replacing_char = ' '


/*------------------------------------------------------------------------------------
       STRIP, REPLACE AND SUBSTRING CHARACTERS FOR STARTING STRING
-------------------------------------------------------------------------------------*/


   IF ( @v_substr_start_pos IS NOT NULL )
		SET @v_result_string = ( substring ( @in_token_value, @v_substr_start_pos
		        ,ISNULL ( @v_substr_length,  LEN ( @in_token_value ) ) ) )
   ELSE
		SET @v_result_string = @in_token_value   --- ltrim ( rtrim (@in_token_value ))

   IF (@v_replace_char IS NOT NULL)
    SET @v_result_string = replace(@v_result_string, @v_replace_char, @v_replacing_char)

   IF (@v_replace_string IS NOT NULL)
    SET @v_result_string = replace(@v_result_string, @v_replace_string, @v_replacing_string)


   IF (ISNULL((@v_remove_spec_chars + '.'), '.') <> '.')
    BEGIN
      SET @v_strip_token = epacube.STR_SUBSTR3_VARCHAR(@v_strip_token, 1, epacube.STR_LENGTH_VARCHAR(@v_remove_spec_chars))
      SET @v_result_string = epacube.STR_TRANSLATE_VARCHAR(@v_result_string, @v_remove_spec_chars, @v_strip_token)
      SET @v_result_string = replace(@v_result_string, '?', '')
    END

   IF (@v_remove_spaces IS NOT NULL)
	SET @v_result_string = ( select replace(@v_result_string + isnull(null,'') ,' ','') )


--------	ALREADY SUBSTING-ING ABOVE
--------
--------   IF (@v_substr_length IS NOT NULL)
--------    BEGIN
--------    -- move epacube.STR_SUBSTR3_VARCHAR to common and recompile
--------    SET @v_result_string = epacube.STR_SUBSTR3_VARCHAR(@v_result_string, @v_substr_start_pos, @v_substr_length)
--------    -- move epacube.STR_LENGTH_VARCHAR to common and recompile
--------    END


   IF @in_prefix is NOT NULL
		SET @v_result_string = @in_prefix + @v_result_string

   IF  @in_start_string IS NULL 
       IF @in_basis_position = 1 
          SET @v_result_string = @v_result_string
       ELSE 
          SET @v_result_string = NULL
   ELSE
        SET @v_result_string = @in_start_string + @v_result_string

   RETURN @v_result_string

END

--------  WAY TOO COMPLICATED NOW THAT WHOLE TOKEN REMVOED ALONG WITH MAX LENGTH FROM SCOPE
--------
--------    IF @in_start_string IS NOT NULL   -- THEN CONCATENATE 
--------    BEGIN
--------     IF epacube.STR_LENGTH_VARCHAR( rtrim ( ltrim (
--------              isnull(@in_start_string, '') + isnull(@v_result_string, ''))))
--------                   <= @v_max_length 
--------        
--------			   SET @v_return_string =  ( rtrim ( ltrim (
--------                      isnull(@in_start_string, '') 
--------					+ isnull(@v_result_string, ''))))
--------      ELSE 
--------        IF (@in_whole_tokens = 1)
--------          SET @v_return_string = @in_start_string   --- can not concatenate will send over max length 
--------        ELSE 
--------            -- move epacube.STR_SUBSTR3_VARCHAR to common and recompile
--------
--------          SET @v_return_string = epacube.STR_SUBSTR3_VARCHAR( ( rtrim ( ltrim ( 
--------                                        isnull(@in_start_string, '') 
--------                                      + isnull(@v_result_string, '')))), 
--------                                     1, ( @v_max_length ) )
----------                                         1, ( @v_max_length + 1 ) ) was returning one too many  
--------			/*************************************************************************************/
--------			--  BUG IN YURI'S FUNCTION epacube.STR_SUBSTR3_VARCHAR.....
--------			--    Was cutting off one too many characters ... if Max Length was 4 it was making truncating to 3 
--------			--         ADDED 1 TO V_MAX_LENGTH TO TEMPORARILY FIX LENGTH PROBLEM
--------			--  removed the add one because appears to be fixed now.....
--------			/*************************************************************************************/
--------
--------   END  -- @in_start_string IS NOT NULL 
--------   ELSE 
--------   IF  @in_basis_position > 1			--- not the first basis in the string concatenation and start string is null
--------		SET @v_result_string = NULL     --- then result will need to be nulled out also

    
----    RETURN ( isnull ( ltrim ( rtrim (@v_return_string) ) ,ltrim ( rtrim (@v_return_string)))) -- 'NULL' )  )





























