﻿



-- =============================================
-- Author:		Leslie Andrews
-- Create date: 1/17/2013
-- Description:	Remove special character from field value 
-- =============================================
Create Function [epacube].[StripSpecs](@myString varchar(500), @invalidChars varchar(100))
RETURNS varchar(500) AS
Begin
    
    While PatIndex('%[' + @invalidChars + ']%', @myString) > 0
        Set @myString = Stuff(@myString, PatIndex('%[' + @invalidChars + ']%', @myString), 1, '')
    Return @myString
End



