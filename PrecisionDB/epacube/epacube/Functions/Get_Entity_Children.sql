﻿
CREATE FUNCTION [epacube].[Get_Entity_Children] (
      @in_Entity_Structure_FK       bigint
      
   )
 RETURNS  @EntityTab table
(
      [Parent_Entity_Structure_FK]   bigint,
      [Child_Entity_Structure_FK]    bigint,
      [Child_Data_Name_FK]           int,
      [Child_Level_Seq]              int

)
AS
BEGIN


WITH EntityHierarchy (Parent_Entity_Structure_FK, Child_Entity_Structure_FK, 
                         Child_Data_Name_FK, Child_Level_Seq)
AS
(
-- Anchor member definition
SELECT es.Parent_Entity_Structure_FK, es.Entity_Structure_ID, 
       es.Data_Name_FK, es.Level_Seq
FROM epacube.Entity_Structure AS es
where es.Entity_Structure_ID = @in_Entity_Structure_FK
and   es.entity_status_cr_fk = 10200
UNION ALL
-- Recursive member definition
SELECT es2.Parent_Entity_Structure_FK, es2.Entity_Structure_ID, 
       es2.Data_Name_FK, es2.Level_Seq
FROM epacube.Entity_Structure AS es2
INNER JOIN EntityHierarchy AS eh
ON es2.Parent_Entity_Structure_FK = eh.Child_Entity_Structure_FK
where es2.entity_status_cr_fk = 10200
)
-- Statement that executes the CTE
insert into @EntityTab
SELECT ehs.Parent_Entity_Structure_FK, ehs.Child_Entity_Structure_FK, 
       ehs.Child_Data_Name_FK, ehs.Child_Level_Seq
from EntityHierarchy ehs


return
end;
