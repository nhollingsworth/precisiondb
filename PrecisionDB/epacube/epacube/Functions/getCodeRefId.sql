﻿
-- =============================================
-- Author:		Geoff Begley
-- Create date: 4/28/2008
-- Description:	find code_ref_id for type and code
-- =============================================
CREATE FUNCTION [epacube].[getCodeRefId]
(
	@type varchar(32)
	,@code varchar(32)
)
RETURNS int
AS
BEGIN
	DECLARE @c int

    select @c=code_ref_id from epacube.code_ref
	where code_type=@type and code=@code

	RETURN @c
END

