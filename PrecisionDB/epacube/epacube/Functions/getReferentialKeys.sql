﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [epacube].[getReferentialKeys]
(
	@DATA_NAME_FK int, @Alias Varchar(64)
)
RETURNS VARCHAR(Max)
AS
BEGIN

--Declare the return variable here
	DECLARE @Keys varchar(Max)

--Declare @DATA_NAME_FK int = 110100, @Alias Varchar(64) = 'PI'

--	-- Add the T-SQL statements to compute the return value here
		Set @Keys = (
		Select 
		@Alias + '.[' + Max(isnull(PRODUCT_STRUCTURE_FK, 'NULL')) + '] ''PRODUCT_STRUCTURE_FK'', ' +
		@Alias + '.[' + Max(isnull(ORG_ENTITY_STRUCTURE_FK, 'NULL')) + '] ''ORG_ENTITY_STRUCTURE_FK'', ' +
		@Alias + '.[' + Max(isnull(CUST_ENTITY_STRUCTURE_FK, 'NULL')) + '] ''CUST_ENTITY_STRUCTURE_FK'', ' +
		@Alias + '.[' + Max(isnull(ENTITY_STRUCTURE_FK, 'NULL')) + '] ''ENTITY_STRUCTURE_FK'', ' +
		@Alias + '.[' + Max(isnull(VENDOR_ENTITY_STRUCTURE_FK, 'NULL')) + '] ''VENDOR_ENTITY_STRUCTURE_FK'', ' +
		@Alias + '.[' + Max(isnull(ZONE_ENTITY_STRUCTURE_FK, 'NULL')) + '] ''ZONE_ENTITY_STRUCTURE_FK'', ' +
		@Alias + '.[' + Max(isnull(COMP_ENTITY_STRUCTURE_FK, 'NULL')) + '] ''COMP_ENTITY_STRUCTURE_FK'', ' +
		@Alias + '.[' + Max(isnull(ASSOC_ENTITY_STRUCTURE_FK, 'NULL')) + '] ''ASSOC_ENTITY_STRUCTURE_FK'', ' +
		@Alias + '.[' + Max(isnull(PROD_DATA_VALUE_FK, 'NULL')) + '] ''PROD_DATA_VALUE_FK'', ' +
		@Alias + '.[' + Max(isnull(CUST_DATA_VALUE_FK, 'NULL')) + '] ''CUST_DATA_VALUE_FK'', ' +
		@Alias + '.[' + Max(isnull(DEPT_ENTITY_DATA_VALUE_FK, 'NULL')) + '] ''DEPT_ENTITY_DATA_VALUE_FK'', ' +
		@Alias + '.[' + Max(isnull(DATA_VALUE_FK, 'NULL')) + '] ''DATA_VALUE_FK'', ' +
		@Alias + '.[' + Max(isnull(ENTITY_DATA_VALUE_FK, 'NULL')) + '] ''ENTITY_DATA_VALUE_FK'', ' 
		--, Max() CUST_ENTITY_STRUCTURE_FK
		--, Max() ENTITY_STRUCTURE_FK
		--, Max() VENDOR_ENTITY_STRUCTURE_FK
		--, Max() ZONE_ENTITY_STRUCTURE_FK
		--, Max() COMP_ENTITY_STRUCTURE_FK
		--, Max() ASSOC_ENTITY_STRUCTURE_FK 
		--, Max() PROD_DATA_VALUE_FK
		--, Max() CUST_DATA_VALUE_FK
		--, Max() DEPT_ENTITY_DATA_VALUE_FK
		--, Max() DATA_VALUE_FK
		--, Max() ENTITY_DATA_VALUE_FK
		from (
		Select Table_Name, data_name_fk
		, Case Local_Column when 'PRODUCT_STRUCTURE_FK' then Local_Column end 'PRODUCT_STRUCTURE_FK'
		, Case Local_Column when 'ORG_ENTITY_STRUCTURE_FK' then Local_Column end 'ORG_ENTITY_STRUCTURE_FK'
		, Case Local_Column when 'CUST_ENTITY_STRUCTURE_FK' then Local_Column end 'CUST_ENTITY_STRUCTURE_FK'
		, Case when Data_Name_FK in (144010, 144020) and Local_Column = 'Value' then 'Entity_Structure_FK' when Local_Column = 'Entity_Structure_FK' then local_column end 'Entity_Structure_FK'
		, Case Local_Column when 'VENDOR_ENTITY_STRUCTURE_FK' then Local_Column end 'VENDOR_ENTITY_STRUCTURE_FK'
		, Case Local_Column when 'ZONE_ENTITY_STRUCTURE_FK' then Local_Column end 'ZONE_ENTITY_STRUCTURE_FK'
		, Case Local_Column when 'COMP_ENTITY_STRUCTURE_FK' then Local_Column end 'COMP_ENTITY_STRUCTURE_FK'
		, Case Local_Column when 'ASSOC_ENTITY_STRUCTURE_FK' then Local_Column end 'ASSOC_ENTITY_STRUCTURE_FK'
		, Case Local_Column when 'PROD_DATA_VALUE_FK' then Local_Column end 'PROD_DATA_VALUE_FK'
		, Case Local_Column when 'CUST_DATA_VALUE_FK' then Local_Column end 'CUST_DATA_VALUE_FK'
		, Case Local_Column when 'DEPT_ENTITY_DATA_VALUE_FK' then Local_Column end 'DEPT_ENTITY_DATA_VALUE_FK'
		, Case Local_Column when 'DATA_VALUE_FK' then Local_Column end 'DATA_VALUE_FK'
		, Case Local_Column when 'ENTITY_DATA_VALUE_FK' then Local_Column end 'ENTITY_DATA_VALUE_FK'
		from EPACUBE.DATA_NAME_GROUP_REFERENCES 
		where	1 = 1
		--and	(Local_Column like '%Structure%' 
		--		OR Local_column like '%Data_Value_FK'
		--		or primary_column like '%Structure%')
		and data_name_fk = @data_name_fk
		) A group by Table_Name, data_name_fk
		) 

	Return Replace(@Keys, @Alias + '.[Null]', 'NULL')
End
