﻿
CREATE FUNCTION [epacube].[STR_LENGTH_VARCHAR] (@s varchar(8000))
returns int
as 
begin
  return len(replace(@s, ' ', '.'))
end
