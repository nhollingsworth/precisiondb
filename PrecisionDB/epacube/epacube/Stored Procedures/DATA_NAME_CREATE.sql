﻿


-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create entities JIRA (URM-1188)
-- 1. Get the next DATA_NAME_ID
-- 2. Parse the JSON object and insert the new record into the DATA_NAME table
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------


CREATE PROCEDURE [epacube].[DATA_NAME_CREATE] 
	@data_name AS NVARCHAR(MAX)
	,@user VARCHAR(64)
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @new_data_name_id BIGINT;

	SET NOCOUNT ON;

	--***************************************************************************************************************************************
	--Example data name structure
	--{
	--	"dataName": "NEW_CUSTOMER_DATA_NAME_TEST",
	--	"label": "New Customer Data Name Test",
	--	"shortName": "Test",
	--	"userEditable": 1,
	--	"dataSetId": 44000
	--}
	--***************************************************************************************************************************************

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.DATA_NAME_CREATE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	SELECT @new_data_name_id = MAX(data_name_id) + 1 FROM epacube.DATA_NAME

	BEGIN TRY
		BEGIN TRANSACTION;

			--Add the new data name
			INSERT INTO epacube.DATA_NAME (DATA_NAME_ID, [NAME], LABEL, SHORT_NAME, DATA_SET_FK, NAME_ORIGINAL, USER_EDITABLE)
			SELECT @new_data_name_id, DATA_NAME, LABEL, SHORT_NAME, DATA_SET_ID, DATA_NAME, USER_EDITABLE
			FROM OPENJSON(@data_name)
			WITH (
				DATA_NAME VARCHAR(64) '$.dataName',
				LABEL VARCHAR(64) '$.label',
				SHORT_NAME VARCHAR(32) '$.shortName',
				USER_EDITABLE SMALLINT '$.userEditable',
				DATA_SET_ID INT '$.dataSetId'
			);

		COMMIT TRANSACTION;

		SET @ls_stmt = 'A new data name was added with DATA_NAME_ID ' + CAST(@new_data_name_id AS VARCHAR(20)) + '.'; 
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		SELECT @new_data_name_id;
					
	END TRY

	BEGIN CATCH
		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 
						
		SET @ls_stmt = 'Execution of epacube.DATA_NAME_CREATE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
				
	END CATCH

END

