﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Delete product associations (URM-1202)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[PRODUCT_ASSOCIATION_DELETE]
	@association_to_delete NVARCHAR(MAX)
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	SET NOCOUNT ON;

	--***************************************************************************************************************************************
	--Example product association delete structure
	--{
	--	"PRODUCT_ASSOCIATION_ID":[ 615534, 615533, 615532 ]
	--}
	--***************************************************************************************************************************************

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.PRODUCT_ASSOCIATION_DELETE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY
		BEGIN TRANSACTION;

			--Delete selected product associations
			DELETE 
			FROM epacube.PRODUCT_ASSOCIATION
			WHERE PRODUCT_ASSOCIATION_ID IN (
				SELECT VALUE AS PRODUCT_ASSOCIATION_ID
				FROM OPENJSON(@association_to_delete, '$.PRODUCT_ASSOCIATION_ID'));

		COMMIT TRANSACTION;

		SET @ls_stmt = 'Finished execution of epacube.PRODUCT_ASSOCIATION_DELETE.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 

		SET @ls_stmt = 'Execution of epacube.PRODUCT_ASSOCIATION_DELETE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END
