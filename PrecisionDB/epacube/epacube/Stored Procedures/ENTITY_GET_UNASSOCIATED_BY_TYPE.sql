﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get unassociated entities by association type (URM-1187)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[ENTITY_GET_UNASSOCIATED_BY_TYPE]
	@product_association_type_id INT,
	@product_id VARCHAR(128) = '%',
	@entity_id VARCHAR(128) = '%'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.ENTITY_GET_UNASSOCIATED_BY_TYPE. Parameters: @product_association_type_id = ' + CAST(@product_association_type_id AS VARCHAR(20)) + ', @product_id = ' + @product_id + ', @entity_id = ' + @entity_id
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT EI.ENTITY_STRUCTURE_FK	AS ENTITY_STRUCTURE_ID
			, EI.VALUE					AS [ENTITY_ID]
			, EIN.VALUE					AS [ENTITY]
		FROM epacube.ENTITY_IDENTIFICATION EI
			INNER JOIN epacube.ENTITY_IDENT_NONUNIQUE EIN
				ON EI.ENTITY_STRUCTURE_FK = EIN.ENTITY_STRUCTURE_FK
		WHERE EI.ENTITY_STRUCTURE_FK NOT IN (
			SELECT PA.ENTITY_STRUCTURE_FK
			FROM epacube.PRODUCT_ASSOCIATION PA
				INNER JOIN epacube.PRODUCT_IDENTIFICATION PID
					ON PA.PRODUCT_STRUCTURE_FK = PID.PRODUCT_STRUCTURE_FK
			WHERE PA.DATA_NAME_FK = @product_association_type_id
				AND PID.VALUE LIKE @product_id)
			AND EI.RECORD_STATUS_CR_FK = 1
			AND EI.VALUE LIKE @entity_id
		FOR JSON PATH, ROOT('data');

		SET @ls_stmt = 'Finished execution of epacube.ENTITY_GET_UNASSOCIATED_BY_TYPE.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH
		SET @ls_stmt = 'Execution of epacube.ENTITY_GET_UNASSOCIATED_BY_TYPE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END 

