﻿
-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get all media with types JIRA (PRE-97)
-- 1. Return media and types in JSON format
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------


CREATE   PROCEDURE [epacube].[MEDIA_GET_ALL] 
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	SET NOCOUNT ON;

	BEGIN TRY
		--Initialize log
		SET @ls_stmt = 'Started execution of epacube.MEDIA_GET_ALL.'
		EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
			@exec_id = @l_exec_no OUTPUT;

		SELECT dv.[VALUE] 'type'
			, media.MEDIA_MANAGEMENT_ID 'Id'
			, media.[FILE_NAME] 'fileName'
			, media.[URL] 'URL'
		FROM epacube.DATA_VALUE dv
			INNER JOIN epacube.MEDIA_MANAGEMENT media
				ON dv.DATA_VALUE_ID = media.DATA_VALUE_FK
		ORDER BY 'type'
		FOR JSON AUTO;

		SET @ls_stmt = 'Finished execution of epacube.MEDIA_GET_ALL.';
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;
	
	END TRY

	BEGIN CATCH

		SET @ls_stmt = 'Execution of epacube.MEDIA_GET_ALL has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

	END CATCH
END