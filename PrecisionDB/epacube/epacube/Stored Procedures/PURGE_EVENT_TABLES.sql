﻿






--					Move to history data from Event tables provided the status is final, and either the
--						job is older than the grace period, or the job is null and the event's
--						update timestamp is older than the grace period
--					Move to archive data from history tables provided
--					 event is older than derived archive period

--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG		 03/30/2010  EPA-2898 Breaking up parent procedure
-- KS        06/02/2010  EPA-3011 Removing the the notion of "Completed Jobs" and using instead the criteria below:
--							EVENT DATA Event Effective Date <= @purge_date_max ( getdate() - 'PURGE IMPORT JOBS AFTER' )
--							and Event Status is completed ( ie Approved, Pre_Posted, or Rejected ) 
-- CV        02/21/2011   EPA-3189 Uncommented Delete of Event Calc tables. 
-- CV        03/18/2011   Decided to clear the event calc tables differently (it's own procedure) 

CREATE PROCEDURE [epacube].[PURGE_EVENT_TABLES] @purge_immediate_ind INT = 0 
AS
BEGIN

        DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @job_timestamp DATETIME
		DECLARE @grace_days INT         
        DECLARE @purge_date_max DATETIME        
        DECLARE	@epacube_Params_Grace_Period int
		DECLARE @calculated_Grace_Period int
		DECLARE @display_Grace_Period varchar(20)
		DECLARE @archivePeriod int
		DECLARE @archive_date DATETIME
		DECLARE @defaultArchivePeriod int
		DECLARE @appscope INT
--------		DECLARE @job_Complete_Status_Code int		
--------		DECLARE @importProductsJobClass INT
--------		DECLARE @importProductTaxonomyJobClass INT
--------		DECLARE @importEntityJobClass INT

				
		BEGIN TRY
            SET NOCOUNT ON ;
            SET @l_exec_no = 0
            SET @l_sysdate = GETDATE()
			SET @grace_days = 7
			
			SET @appscope = (select application_scope_id
							 from epacube.application_scope with (NOLOCK)
							 where scope = 'APPLICATION')  --100
			SET @epacube_Params_Grace_Period = (select CAST ( eep.value as int )
												from epacube.epacube_params eep with (NOLOCK)
												where  eep.name = 'PURGE IMPORT JOBS AFTER'
												and application_scope_fk = @appscope)
			SET @calculated_Grace_Period = CASE @purge_immediate_ind
			                               WHEN 1 THEN 0
			                               ELSE (isnull(@epacube_Params_Grace_Period,@grace_days))
			                               END 						
			SET @purge_date_max = DATEADD(day, -@calculated_Grace_Period, @l_sysdate)
			SET @display_Grace_Period = CAST(ISNULL(@calculated_Grace_Period, 0) AS VARCHAR(20))						
			SET @defaultArchivePeriod = 30  --- KS changed from 90 to thirty
			SET @archivePeriod = CAST(isnull((select CAST ( value as int ) from epacube.epacube_params with (NOLOCK)
										  where NAME = 'ARCHIVE_PERIOD'
										  and application_scope_fk = @appscope),@defaultArchivePeriod) AS VARCHAR(20))
			SET @archive_date = DATEADD(day, -@archivePeriod, @l_sysdate)										  

--------			SET @job_Complete_Status_Code = epacube.getCodeRefId('JOB_STATUS','COMPLETE')							 
--------			SET @importProductsJobClass = (select job_class_id from common.job_class
--------											where name = 'IMPORT PRODUCTS')
--------			SET @importProductTaxonomyJobClass = (select job_class_id from common.job_class
--------											where name = 'IMPORT PRODUCT TAXONOMY')
--------			SET @importEntityJobClass = (select job_class_id from common.job_class
--------											where name = 'IMPORT ENTITY')
----------------------------------------------------------
--logging
----------------------------------------------------------
		
SET @ls_stmt = 'started execution of epacube.PURGE_EVENT_TABLES. '
                + 'Grace days = '
                + @display_Grace_Period + ' days.'
            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;


			DECLARE @FinalEventStatuses TABLE
				(
					CodeRefID bigint
				)

			INSERT INTO @FinalEventStatuses(CodeRefID)

					select code_ref_id 
					from epacube.code_ref
					where code_type = 'EVENT_STATUS'		
					and code in ('APPROVED','REJECTED','PRE POST')	



--------------------------------------------------------------------
						
--------			DECLARE @CompletedImportJobs TABLE
--------				(
--------					JobID bigint
--------				)
--------
--------			INSERT INTO @CompletedImportJobs (JobID)
--------
--------					select distinct job_fk 
--------					from common.job_execution with (NOLOCK)
--------					where job_status_cr_fk = @job_Complete_Status_Code
--------					and job_fk in (select job_id from common.job j with (NOLOCK)
--------								   inner join import.import_record on (job_fk = job_id)
--------									and j.job_class_fk in (@importProductsJobClass,@importProductTaxonomyJobClass, @importEntityJobClass ) ) 						
					
--------			DECLARE @CompletedGraceJobs TABLE
--------				(
--------					JobID bigint
--------				)
--------
--------			INSERT INTO @CompletedGraceJobs (JobID)
--------
--------					select distinct job_fk 
--------					from common.job_execution with (NOLOCK)
--------					where job_status_cr_fk = @job_Complete_Status_Code					
--------					and job_fk in (select job_id from common.job j with (NOLOCK) where
--------									 utilities.TRUNC(j.job_complete_timestamp) 
--------														<= 
--------									utilities.TRUNC(@purge_date_max)
--------									and job_id in (select JobID from @CompletedImportJobs))								 
							 
------------------------------ begin sync event tables
					
                				
--EVENT DATA ERRORS
                    DELETE  FROM synchronizer.event_data_errors
                    WHERE   event_fk IN (
                            SELECT  event_id
                            FROM    synchronizer.event_data WITH ( NOLOCK )
                            WHERE   event_status_cr_fk IN ( SELECT CodeRefId 
															from @FinalEventStatuses)
                            AND     utilities.TRUNC(event_effective_date)
														<= 
									utilities.TRUNC(@purge_date_max) )
																																													
--------							AND		(import_job_fk in (select JobID from @CompletedGraceJobs )		
--------									 or (import_job_fk is null )
--------											AND utilities.TRUNC(update_timestamp)
--------														<= 
--------												utilities.TRUNC(@purge_date_max))))

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from synchronizer.event_data_errors table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

--EVENT DATA RULES
					DELETE  FROM synchronizer.event_data_rules
                    WHERE   event_fk IN (
                            SELECT  event_id
                            FROM    synchronizer.event_data WITH ( NOLOCK )
                            WHERE   event_status_cr_fk IN ( SELECT CodeRefId 
															from @FinalEventStatuses)
                            AND     utilities.TRUNC(event_effective_date)
														<= 
									utilities.TRUNC(@purge_date_max) )
						

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from synchronizer.event_data_rules table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

----------------------------------------------------------------------------------------------------------------------------------

					DELETE  FROM synchronizer.event_data
                    WHERE   event_status_cr_fk IN ( SELECT CodeRefId 
													from @FinalEventStatuses)
                            AND     utilities.TRUNC(event_effective_date)
														<= 
									utilities.TRUNC(@purge_date_max) 
																																													
--------							AND		(import_job_fk in (select JobID from @CompletedGraceJobs )		
--------									 or (import_job_fk is null )
--------											AND utilities.TRUNC(update_timestamp)
--------														<= 
--------												utilities.TRUNC(@purge_date_max))))

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows moved from synchronizer.event_data table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END




--ARCHIVE EVENTS FROM HISTORY                        
					INSERT INTO synchronizer.EVENT_DATA_ARCHIVE (
		 EVENT_TYPE_CR_FK
		,EVENT_ENTITY_CLASS_CR_FK 
		,EPACUBE_ID    
		,IMPORT_JOB_FK     
		,IMPORT_PACKAGE_FK 
		,DATA_NAME_FK	
	    ,PRODUCT_STRUCTURE_FK
	    ,ORG_ENTITY_STRUCTURE_FK
	    ,ENTITY_STRUCTURE_FK    
		,ENTITY_CLASS_CR_FK     
		,ENTITY_DATA_NAME_FK    
        ,NEW_DATA         
        ,CURRENT_DATA     
		,DATA_VALUE_FK    
		,ENTITY_DATA_VALUE_FK
        ,NET_VALUE1_NEW      
        ,NET_VALUE2_NEW      
        ,NET_VALUE3_NEW      
        ,NET_VALUE4_NEW      
        ,NET_VALUE5_NEW      
        ,NET_VALUE6_NEW      
        --,NET_VALUE7_NEW      
        --,NET_VALUE8_NEW      
        --,NET_VALUE9_NEW      
        --,NET_VALUE10_NEW     
        ,NET_VALUE1_CUR      
        ,NET_VALUE2_CUR      
        ,NET_VALUE3_CUR     
        ,NET_VALUE4_CUR     
        ,NET_VALUE5_CUR     
        ,NET_VALUE6_CUR     
        --,NET_VALUE7_CUR     
        --,NET_VALUE8_CUR     
        --,NET_VALUE9_CUR     
        --,NET_VALUE10_CUR    
		,PERCENT_CHANGE1	 
		,PERCENT_CHANGE2	
		,PERCENT_CHANGE3	
		,PERCENT_CHANGE4	
		,PERCENT_CHANGE5	
		,PERCENT_CHANGE6	
		--,PERCENT_CHANGE7	
		--,PERCENT_CHANGE8	
		--,PERCENT_CHANGE9	
		--,PERCENT_CHANGE10
        ,VALUE_UOM_CODE_FK 
        ,ASSOC_UOM_CODE_FK 
		,UOM_CONVERSION_FACTOR 
		,END_DATE_NEW          
        ,END_DATE_CUR          
		,EFFECTIVE_DATE_CUR    
		,EVENT_EFFECTIVE_DATE 
		,VALUE_EFFECTIVE_DATE
        ,RELATED_EVENT_FK	
        ,RULES_FK			
		,EVENT_ACTION_CR_FK   
		,EVENT_STATUS_CR_FK   
		,EVENT_CONDITION_CR_FK
		,EVENT_PRIORITY_CR_FK   
		,EVENT_SOURCE_CR_FK   
		,RESULT_TYPE_CR_FK	 
		,SINGLE_ASSOC_IND     
		,TABLE_ID_FK          
		,BATCH_NO			
		,IMPORT_BATCH_NO     
        ,JOB_CLASS_FK		
        ,IMPORT_DATE         
        ,IMPORT_FILENAME     
        ,STG_RECORD_FK       
		,RECORD_STATUS_CR_FK 
        ,UPDATE_TIMESTAMP	
        ,UPDATE_USER		    
     )
SELECT 
		 EVENT_TYPE_CR_FK
		,EVENT_ENTITY_CLASS_CR_FK 
		,EPACUBE_ID    
		,IMPORT_JOB_FK     
		,IMPORT_PACKAGE_FK 
		,DATA_NAME_FK	
	    ,PRODUCT_STRUCTURE_FK
	    ,ORG_ENTITY_STRUCTURE_FK
	    ,ENTITY_STRUCTURE_FK    
		,ENTITY_CLASS_CR_FK     
		,ENTITY_DATA_NAME_FK    
        ,NEW_DATA         
        ,CURRENT_DATA     
		,DATA_VALUE_FK    
		,ENTITY_DATA_VALUE_FK
        ,NET_VALUE1_NEW      
        ,NET_VALUE2_NEW      
        ,NET_VALUE3_NEW      
        ,NET_VALUE4_NEW      
        ,NET_VALUE5_NEW      
        ,NET_VALUE6_NEW      
        --,NET_VALUE7_NEW      
        --,NET_VALUE8_NEW      
        --,NET_VALUE9_NEW      
        --,NET_VALUE10_NEW     
        ,NET_VALUE1_CUR      
        ,NET_VALUE2_CUR      
        ,NET_VALUE3_CUR     
        ,NET_VALUE4_CUR     
        ,NET_VALUE5_CUR     
        ,NET_VALUE6_CUR     
        --,NET_VALUE7_CUR     
        --,NET_VALUE8_CUR     
        --,NET_VALUE9_CUR     
        --,NET_VALUE10_CUR    
		,PERCENT_CHANGE1	 
		,PERCENT_CHANGE2	
		,PERCENT_CHANGE3	
		,PERCENT_CHANGE4	
		,PERCENT_CHANGE5	
		,PERCENT_CHANGE6	
		--,PERCENT_CHANGE7	
		--,PERCENT_CHANGE8	
		--,PERCENT_CHANGE9	
		--,PERCENT_CHANGE10
        ,VALUE_UOM_CODE_FK 
        ,ASSOC_UOM_CODE_FK 
		,UOM_CONVERSION_FACTOR 
		,END_DATE_NEW          
        ,END_DATE_CUR          
		,EFFECTIVE_DATE_CUR    
		,EVENT_EFFECTIVE_DATE 
		,VALUE_EFFECTIVE_DATE
        ,RELATED_EVENT_FK	
        ,RULES_FK			
		,EVENT_ACTION_CR_FK   
		,EVENT_STATUS_CR_FK   
		,EVENT_CONDITION_CR_FK
		,EVENT_PRIORITY_CR_FK   
		,EVENT_SOURCE_CR_FK   
		,RESULT_TYPE_CR_FK	 
		,SINGLE_ASSOC_IND     
		,TABLE_ID_FK          
		,BATCH_NO			
		,IMPORT_BATCH_NO     
        ,JOB_CLASS_FK		
        ,IMPORT_DATE         
        ,IMPORT_FILENAME     
        ,STG_RECORD_FK       
		,RECORD_STATUS_CR_FK 
        ,UPDATE_TIMESTAMP	
        ,UPDATE_USER	
         FROM synchronizer.event_data_history
                    WHERE @archivePeriod IS NOT NULL --- KS added this to denote Archive Period specified if NULL then DO NOT archive
                    AND   utilities.TRUNC(event_effective_date)
														<= 
												utilities.TRUNC(@archive_date)

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows archived into synchronizer.event_data_archive table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END                        

												
					DELETE FROM synchronizer.event_data_history			
					 WHERE utilities.TRUNC(event_effective_date)
														<= 
												utilities.TRUNC(@archive_date)									

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from synchronizer.event_data_history table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END                        


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

            SET @status_desc = 'finished execution of SYNCHRONIZER.epacube.PURGE_EVENT_TABLES'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


  END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.PURGE_EVENT_TABLES has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH
		  
    END








