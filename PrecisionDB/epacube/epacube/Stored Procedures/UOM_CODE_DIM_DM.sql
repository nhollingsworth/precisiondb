﻿



-- Copyright 2007
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to create/modify/activate/inactivate UOM Dimensions
-- preliminary version has no validation
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        09/21/2007   Initial SQL Version


CREATE PROCEDURE [epacube].[UOM_CODE_DIM_DM] 
( @in_uom_code_dim_id int,
  @in_uom_code varchar (64),
  @in_uom_description varchar (64),
  @in_uom_code_dim_type_cr_fk int,
  @in_lower_qty numeric(18,6),
  @in_lower_uom_code_dim varchar (64), 
  @in_primary_qty numeric(18,6),
  @in_record_status_cr_fk int,
  @in_display_seq int,
  @in_ui_action_cr_fk int,
  @in_update_user varchar(64) )
												
AS

/***************  Parameter Defintions  **********************
CURRENTLY USED IN THE DATA MASTER EDITOR....

***************************************************************/

BEGIN

DECLARE @l_sysdate				datetime
DECLARE @ls_stmt				varchar (1000)
DECLARE @l_exec_no				bigint
DECLARE @l_rows_processed		bigint
DECLARE @v_count				bigint
DECLARE @v_rule_data_name_fk    bigint
DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int

SET NOCOUNT ON;
SET @l_sysdate = getdate ()
set @ErrorMessage = null


-------------------------------------------------------------------------------------
--   Peform UI Delete Action 
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = 8004
BEGIN
	DELETE FROM epacube.UOM_CODE_DIM
	WHERE UOM_CODE_DIM_ID = @in_uom_code_dim_id
	return (0)
END -- IF @in_ui_action_cr_fk = 8004

-------------------------------------------------------------------------------------
--   Peform UI Activate Action 
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = 8007  -- activate
BEGIN

	UPDATE epacube.uom_code_dim
	       set record_status_cr_fk = 1
           ,update_timestamp = getdate()
		   ,update_user = @in_update_user
	       where uom_code_dim_id = @in_uom_code_dim_id	
		   return (0)
END -- IF @in_ui_action_cr_fk = 8007

-------------------------------------------------------------------------------------
--   Peform UI Inactivate Action 
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = 8005  -- inactivate
BEGIN

	UPDATE epacube.uom_code_dim
	       set record_status_cr_fk = 2
		   ,update_timestamp = getdate()
		   ,update_user = @in_update_user
	       where uom_code_dim_id = @in_uom_code_dim_id
		   return (0)
END -- IF @in_ui_action_cr_fk = 8005
-------------------------------------------------------------------------------------
-- Perform Server-side validation 
-- Lower 1 and 2 UOM codes must be either an existing, valid UOM code, 
-- or the user-entered uom_code
-------------------------------------------------------------------------------------
IF @in_ui_action_cr_fk in (8003,8006) --8003=create,8006=change
BEGIN

	IF  @in_uom_code IS NULL
    BEGIN
		SET @ErrorMessage = 'Please enter UOM Dimension'
        GOTO RAISE_ERROR
	END

	select @ErrorMessage = 'Invalid UOM Dimension specified for Lower UOM Dimension'
	from epacube.UOM_CODE_DIM
	where (@in_lower_uom_code_dim not in (select UOM_CODE from epacube.UOM_CODE_DIM))
	and (@in_lower_uom_code_dim not in (@in_uom_code))

	IF @ErrorMessage is not null
	BEGIN
		GOTO RAISE_ERROR
    END

-------------------------------------------------------------------------------------
-- Lower UOM type and specified type must match
--@in_uom_code_dim_type_cr_fk 
-------------------------------------------------------------------------------------
	select @ErrorMessage = 'UOM Dimension type must match type specified for lower UOM'
	from epacube.UOM_CODE_DIM
	where (@in_uom_code_dim_type_cr_fk  not in 
		(select uom_code_dim_type_cr_fk 
		 from epacube.uom_code_dim 
	     where uom_code = @in_lower_uom_code_dim ))
	
	IF @ErrorMessage is not null
	BEGIN
		GOTO RAISE_ERROR
    END
-------------------------------------------------------------------------------------
--   If the lower 1 or 2 UOM is equal to the UOM Code input, then the 
--   associated lower qty must be 1
-------------------------------------------------------------------------------------
	 IF ((@in_lower_uom_code_dim = @in_uom_code) and (@in_lower_qty not in (1)))
	 BEGIN
		SET	@ErrorMessage = 'Lower Qty must equal one if the Lower UOM Dimension is equal to the UOM Dimension'
		GOTO RAISE_ERROR
	 END
--	 IF ((@in_lower2_uom_code = @in_uom_code) and (@in_lower2_qty not in (1)))
--	 BEGIN
--		SET	@ErrorMessage = 'Lower Qty 2 must equal one if the Lower 2 UOM is equal to the UOM Code'
--		GOTO RAISE_ERROR
--	 END


-------------------------------------------------------------------------------------
-- Perform UI Create Action
-------------------------------------------------------------------------------------


  IF @in_ui_action_cr_fk = 8003 --create	
  BEGIN

-------------------------------------------------------------------------------------
--   Check for Duplicate Prior to Insert
-------------------------------------------------------------------------------------

    select @ErrorMessage = 'Duplicate UOM Dimension. UOM Dimension ID = ' + 
             	+ CAST(isnull(UCD.UOM_CODE_DIM_ID,0) AS VARCHAR(30))
    from epacube.uom_code_dim UCD
    where upper(ucd.uom_code) = upper(@in_uom_code)

			 IF @@rowcount > 0
					GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
--   Edits passed Insert Row
-------------------------------------------------------------------------------------

    INSERT INTO epacube.UOM_CODE_DIM
    (
	 uom_code_dim_type_cr_fk,
	 uom_code,
     uom_description,
     lower_qty,
     lower_uom_code_dim,     
     primary_qty,	      
     display_seq,
     record_status_cr_fk,	 
	 update_user,
     create_timestamp
    )
    SELECT
	@in_uom_code_dim_type_cr_fk,
	upper(@in_uom_code),
    upper(@in_uom_description),
    @in_lower_qty,
    upper(@in_lower_uom_code_dim),    
    @in_primary_qty,        
    @in_display_seq,
    @in_record_status_cr_fk,	
	@in_update_user,
    getdate()
  END --- IF @in_ui_action_cr_fk = 8003


-------------------------------------------------------------------------------------
--   Perform UI Change Action
-------------------------------------------------------------------------------------
  IF @in_ui_action_cr_fk = 8006 --change
  BEGIN

    select @ErrorMessage = 'UOM Dimension ' 
				+ upper(@in_uom_code)
				+ ' already exists. UOM Dimension ID = ' + 
             	+ CAST(isnull(UCD.UOM_CODE_DIM_ID,0) AS VARCHAR(30))
    from epacube.uom_code_dim UCD
    where upper(ucd.uom_code) = upper(@in_uom_code)
	  and ucd.uom_code_dim_id <> @in_uom_code_dim_id

			 IF @@rowcount > 0
					GOTO RAISE_ERROR
	
	UPDATE epacube.uom_code_dim
		set uom_code = upper(@in_uom_code)
			,uom_description = upper (@in_uom_description)
			,lower_qty = @in_lower_qty
			,lower_uom_code_dim = @in_lower_uom_code_dim			
			,primary_qty = @in_primary_qty
			,display_seq = @in_display_seq
			,record_status_cr_fk = @in_record_status_cr_fk
			,update_user = @in_update_user			
			,update_timestamp = getdate()
		where uom_code_dim_id = @in_uom_code_dim_id


  End -- IF @in_ui_action_cr_fk = 8006

End --IF @in_ui_action_cr_fk in (8003,8006) -validation block





RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END







































