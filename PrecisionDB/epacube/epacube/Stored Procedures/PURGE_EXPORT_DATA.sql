﻿





-- Copyright 2011 epaCUBE, Inc.
--
-- Procedure created by Robert Gannon
--
--
-- 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG		  01/13/2011   EPA-3136  Purging Export Data

CREATE PROCEDURE [epacube].[PURGE_EXPORT_DATA] 
AS
BEGIN

        DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
       

		DECLARE @export_grace_period int
		DECLARE @calculated_export_grace_period datetime
		DECLARE @display_export_grace_period varchar(20)
		

        BEGIN TRY
            set @export_grace_period = (select cast(value as int) from epacube.epacube_params
							where name = 'EXPORT_GRACE_PERIOD'
							and application_scope_fk = (select application_scope_id from epacube.application_scope
							where scope = 'PRODUCT EXPORT'));
							
			set @calculated_export_grace_period = DATEADD(day, -@export_grace_period, getdate())											  
			SET @display_export_grace_period = CAST(ISNULL(@calculated_export_grace_Period, 0) AS VARCHAR(20))
							
            SET @ls_stmt = 'started execution of epacube.PURGE_EXPORT_DATA. '
                + 'Grace days = '
                + @display_export_grace_period + ' days.'
            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;
                
		
----------------------------------------------------------------------------		                
-- Deletion based on date calculation
----------------------------------------------------------------------------

			DELETE from export.export_product_200
			where utilities.Trunc(@calculated_export_grace_period)  >  utilities.Trunc(effective_date)

        END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.PURGE_EXPORT_DATA has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH
                
END                
			

