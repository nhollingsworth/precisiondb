﻿









-- Copyright 2008
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: To determine any special sql for specific Export taxonomy trees
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        04/09/2014   Initial SQL Version
-- GS        02/05/2015   Gary added the ablity to pass a paramater to dynamic sql


CREATE PROCEDURE [epacube].[EXECUTE_UTIL_SPECIAL_SQL_gs_params] ( @IN_JOB_FK bigint ) 
               

AS
BEGIN

 DECLARE @ls_exec            nvarchar (max);
 DECLARE @l_exec_no          bigint;
 DECLARE @l_rows_processed   bigint;
 DECLARE @l_sysdate			 DATETIME
 
 DECLARE  @ls_stmt			  VARCHAR(MAX)
 DECLARE  @v_count			  BIGINT

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;

SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of Epacube.EXECUTE_UTIL_SPECIAL_SQL ' 
              
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;




------------------------------------------------------------------------------------------
---  Perform Dynamic SQL on ACTION 
------------------------------------------------------------------------------------------
---Need to loop by seq id

DECLARE 
			  @vm$package_id        varchar(10),   
              @vm$seq_id            varchar(10), 
              @vm$Action1_sql       varchar(Max),
			  @vm$Action2_sql       varchar(Max)

 DECLARE  cur_v_import cursor local for
			SELECT Import_package_fk, SEQ_ID, ACTION1_SQL, ACTION2_SQL 
						FROM EPACUBE.UTIL_SPECIAL_SQL with (nolock)
						WHERE Record_status_cr_fk = 1
						AND import_package_fk  = 
						(select top 1 import_package_fk from import.import_record_data where job_fk = @IN_JOB_FK)
						order by seq_id asc
--    

 OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO
		 @vm$package_id ,
				@vm$seq_id, 
				@vm$Action1_sql,
				@vm$Action2_sql

WHILE @@FETCH_STATUS = 0 
         BEGIN
    
            SET @status_desc =  'Export_Special_SQL: inserting - '
								+ @vm$package_id
                                + ' '
                                + @vm$seq_id
                                + ' '
                                + @vm$Action1_sql
								+ ' '
                                + ISNULL (@vm$Action2_sql, ' AND 1 = 1')


            Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @IN_JOB_FK ;


------------------------------------------------------------------------------------------
---  Perform Dynamic SQL on ACTION1 and need to figure out how to pass params here.. if procedure needs them..
------------------------------------------------------------------------------------------

--      --Determine what parameters are in use by query being called. This has to be expanded for more comprehensive use but will do for now where Job_FK is the only potential parameter.

		Declare @vm$Param Varchar(64)
		Declare @Params Varchar(256)

		Declare params cursor local for
			SELECT P.NAME FROM SYS.OBJECTS O
			INNER JOIN SYS.all_parameters P ON O.object_id = P.object_id
			WHERE @vm$Action1_sql like '%' + O.NAME + '%'
			ORDER BY P.PARAMETER_ID

		Open params
			FETCH NEXT FROM params INTO @vm$Param

			WHILE @@FETCH_STATUS = 0 
					 BEGIN

					 Set @Params = @vm$Param + ', '

		 FETCH NEXT FROM params INTO @vm$Param

		 END --params LOOP

		WHILE @@FETCH_STATUS = 0 
			 CLOSE params;
			 DEALLOCATE params; 

		Set @Params = left(@vm$Param, len(@vm$Param) - 2)

		Set @Params = Case when @Params like '%Job_FK%' then @IN_JOB_FK else Null end


		SET @ls_exec = @vm$Action1_sql 
						
						+ ' ' +
						ISNULL (@vm$Action2_sql, '') + Case when isnull(@Params, '') > '' then ' ' + @Params else Null end

----                    
----
---- ( SELECT ISNULL(ACTION2_SQL, ' AND 1 = 1' ) FROM Import.import_package_sql
----						WHERE Import_package_FK = @in_import_package_fk 
----						AND Record_status_cr_fk = 1  )


----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), ISNULL(@ls_exec,'UTIL SPECIAL SQL PART') )

      EXEC sp_executesql 	@ls_exec;

----

 FETCH NEXT FROM cur_v_import INTO 
										  @vm$package_id,
										  @vm$seq_id,
										  @vm$action1_sql,
										  @vm$Action2_sql

 END --cur_v_import LOOP

WHILE @@FETCH_STATUS = 0 
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of EPACUBE.UTIL_SPECAIL_SQL'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of EPACUBE.UTIL_SPECIAL_SQL has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024) 
		 
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');

			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END

























































