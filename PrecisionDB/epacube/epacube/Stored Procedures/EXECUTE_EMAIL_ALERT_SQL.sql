﻿







-- Copyright 2008
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: To determine any special sql for emailing
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        09/17/2012   Initial SQL Version

CREATE PROCEDURE [epacube].[EXECUTE_EMAIL_ALERT_SQL] 

AS
BEGIN
DECLARE @pscmd varchar (200)
DECLARE @result int
SET @pscmd = 'C:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe –file "C:\kr\EMail_Alerts\Powershell\email_alerts.ps1"' 
EXEC @result = xp_cmdshell @pscmd , NO_OUTPUT

End
