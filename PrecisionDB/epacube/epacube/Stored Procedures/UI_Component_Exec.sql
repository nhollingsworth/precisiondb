﻿
CREATE procedure [epacube].[UI_COMPONENT_EXEC] @UserID varchar(64), @UI_Comp_Master_FK Varchar(Max), @Parameters varchar(max) = '', @UI_RETURN_SQL int = 0, @UI_DN_Source int = 1

as 
		------/***********************************************************
		------	Written By  : Gary Stone
		------	February, 2017
		------	Updated 11/29/2017 to cover user-defined reports as well
		--------*/----------------------------------------------------------
--Declare @UserID Varchar(64) = 'gstone@epacube.com'
--Declare @UI_COMP_MASTER_FK Varchar(Max) =  31560 --31568--31562--31556--21488--21493--'110100~144020~503201~502045'
--Declare @Parameters varchar(max) = ''
--Declare @UI_RETURN_SQL int = 1
--Declare @UI_DN_Source int = 0

Set @parameters = replace(@Parameters, '~', '')
Set @UserID = Replace(@UserID, '.', '_')

Declare @SQL_Library_FK int
Set @SQL_Library_FK = isnull((Select sql_library_fk from epacube.ui_comp_master where UI_COMP_MASTER_ID = @UI_Comp_Master_FK), 0)

Declare @t_results varchar(64) = ''
Set @t_results = Case when @t_results = '' then 'TempTbls.tmp_RESULTS_' + Case when @UI_COMP_MASTER_FK like '%~%' then '1' else @UI_COMP_MASTER_FK end + '_' + @UserID else @t_results end

If @SQL_Library_FK <> 0
	Begin
		Exec [epacube].[UI_Component_Exec_Sql_Lib] @t_results, @UserID, @UI_Comp_Master_FK, @SQL_Library_fk, @Parameters, @UI_RETURN_SQL
		goto eop
	End

Set NoCount ON
SET ANSI_WARNINGS Off

If object_id('tempdb..#UI_COMP_MASTER') is not null
Drop table #UI_COMP_MASTER

If object_id('tempdb..#UI_COMP') is not null
Drop table #UI_COMP

If object_id('tempdb..#UI_COMP_Levels') is not null
Drop table #UI_COMP_Levels

Declare @DNsource Varchar(64) = ''
Declare @DNsourceID Varchar(64) = ''
Declare @FiltersInForce Varchar(128)

CREATE TABLE #UI_COMP_MASTER(
	[UI_COMP_MASTER_ID] [bigint] NOT NULL,
	[TOP_N_ROWS] [int] NULL,
	[RECORD_STATUS_CR_FK] [int] NOT NULL)

CREATE TABLE #UI_COMP(
	UI_COMP_ID bigint identity(1, 1) Not Null, 
	[UI_COMP_MASTER_FK] [bigint] NOT NULL,
	[DATA_NAME_FK] [int] NULL,
	[SEQ] [int] NULL,
	[RECORD_STATUS_CR_FK] [int] NOT NULL
)

CREATE TABLE #UI_COMP_Levels(
	--[UI_COMP_FK] [bigint] NOT NULL,
	[UI_COMP_MASTER_FK] [bigint] NOT NULL,
	[DATA_NAME_FK] [int] NOT NULL,
	[DATA_LEVEL_CR_FK] [int] NOT NULL,
	--[COLUMN_TYPE] [varchar](64) NULL,
	[ENTITY_CLASS_CR_FK] [int] NULL,
	[RECORD_STATUS_CR_FK] [int] NULL
) ON [PRIMARY]

If @UI_Comp_Master_FK like '%~%'
Begin
	Set @UI_Comp_Master_FK = Case when right(@UI_Comp_Master_FK, 1) <> '~' then @UI_Comp_Master_FK + '~' else @UI_Comp_Master_FK end
	Insert into #UI_COMP_MASTER
	([UI_COMP_MASTER_ID], [TOP_N_ROWS], [RECORD_STATUS_CR_FK])
	Select 1, 0, 1

	Insert into #UI_COMP
	([UI_COMP_MASTER_FK], [DATA_NAME_FK], [SEQ], [RECORD_STATUS_CR_FK])
	select	1 'UI_COMP_MASTER_FK', ss.ListItem, SS.SEQ, 1 'RECORD_STATUS_CR_FK' from common.utilities.SplitStringSeq( @UI_Comp_Master_FK, '~' ) ss
End
Else
If @UI_DN_Source = 1
Begin
	Set @DNsource = 'epacube.ui_comp'
	Set @DNsourceID = 'UI_Comp_Master_FK'

	Insert into #UI_COMP_MASTER
	([UI_COMP_MASTER_ID], [TOP_N_ROWS], [RECORD_STATUS_CR_FK])
	Select [UI_COMP_MASTER_ID], [TOP_N_ROWS], [RECORD_STATUS_CR_FK] from epacube.ui_comp_master where ui_comp_master_id = @UI_Comp_Master_FK

	Insert into #UI_COMP
	([UI_COMP_MASTER_FK], [DATA_NAME_FK], [SEQ], [RECORD_STATUS_CR_FK])
	Select [UI_COMP_MASTER_FK], [DATA_NAME_FK], [SEQ], [RECORD_STATUS_CR_FK] from epacube.ui_comp where ui_comp_master_fk = @UI_Comp_Master_FK and RECORD_STATUS_CR_FK = 1 and data_name_fk not in (select data_name_fk from #UI_COMP)

	Insert into #UI_COMP_Levels
	([UI_COMP_MASTER_FK], [DATA_NAME_FK], [DATA_LEVEL_CR_FK], [ENTITY_CLASS_CR_FK], [RECORD_STATUS_CR_FK])
	Select [UI_COMP_MASTER_FK], [DATA_NAME_FK], [DATA_LEVEL_CR_FK], [ENTITY_CLASS_CR_FK], [RECORD_STATUS_CR_FK] from epacube.ui_comp_levels where ui_comp_master_fk = @UI_Comp_Master_FK and RECORD_STATUS_CR_FK = 1 and data_name_fk not in (select data_name_fk from #UI_COMP_Levels)
End
Else
Begin
	Set @DNsource = 'epacube.ui_reports_master'
	Set @DNsourceID = 'UI_Reports_Master_FK'

	Insert into #UI_COMP
	([UI_COMP_MASTER_FK], [DATA_NAME_FK], [SEQ], [RECORD_STATUS_CR_FK])
	Select [UI_Reports_Master_FK], [DATA_NAME_FK], [SEQ], [RECORD_STATUS_CR_FK] from epacube.ui_reports where ui_reports_master_fk = @UI_Comp_Master_FK and RECORD_STATUS_CR_FK = 1 and data_name_fk not in (select data_name_fk from #UI_COMP)
End

--Declare @top_n_rows varchar(16) = '10' --isnull((select top_n_rows from #UI_COMP_MASTER), 5000)  -- # is per data_level_cr_fk; 0 returns all
Declare @AllowanceCalcDate varchar(64) = ''
Declare @AllowanceCriteria varchar(Max) = ''
Declare @AllowanceOption Varchar(16) = ''
Declare @Consolidate_Results int = 1
Declare @Entity_Class_CR_FK varchar(64) = ''
Declare @WHERE varchar(Max) = 'Where 1 = 1 '
Declare @WHERE_IsNotNull VARCHAR(Max) = ''
Declare @VirtualWhereClause Varchar(Max) = ''
Declare @WhereOption varchar(Max) = ''
Declare @WhereDRank Varchar(Max) = ''
Declare @PBV_DNFK varchar(16) = ''
Declare @DN_Label Varchar(64) = ''
Declare @KeyCols Varchar(Max) = ''
Declare @SqlEXEC Varchar(Max) = ''
Declare @Structure_Table_In_Use int = 0
Declare @BT_Status int = 0
Declare @ST_Status int = 0
Declare @Prod_Status int = 0
Declare @Vendor_Status int = 0
Declare @Cust_Status int = 0
Declare @Zone_Status int = 0
Declare @t_prod varchar(64) = ''
Declare @t_vendor varchar(64) = ''
Declare @t_cust varchar(64) = ''
Declare @t_zone varchar(64) = ''
--Declare @t_results varchar(64) = ''
Declare @From VARCHAR(MAX) = ''
Declare @Auths_Ind int = 0
Declare @Auths_Join Varchar(16) = ''
Declare @ind_PROD_DV int = 0
Declare @ind_CUST_DV int = 0
Declare @ind_PS_FK int = 0
Declare @ind_ES_FK int = 0
Declare @ind_OES_FK int = 0
Declare @ind_CES_FK int = 0
Declare @ind_VES_FK int = 0
Declare @ind_ZES_FK int = 0
Declare @ind_COMP_ES_FK int = 0
Declare @ind_ASSOC_ES_FK int = 0
Declare @ind_Dept_EDV int = 0
Declare @ind_DL_CR_FK int = 0
Declare @ind_Rec_Ingredient_XREF int = 0
Declare @ind_CHILD_PS_FK int = 0
Declare @ind_PARENT_ES_FK int = 0
Declare @SettingsAliasZone Varchar(16) = ''
Declare @SettingsAliasCust Varchar(16) = ''
Declare @KeyColsPSFK Varchar(Max) = 'NULL ''Product_Structure_FK'', '
Declare @KeyColsCESFK Varchar(Max) = 'NULL ''Cust_Entity_Structure_FK'', '
Declare @KeyColsVESFK Varchar(Max) = 'NULL ''Vendor_Entity_Structure_FK'', '
Declare @KeyColsZESFK Varchar(Max) = 'NULL ''Zone_Entity_Structure_FK'', '
Declare @KeyColsCOMPFK Varchar(Max) = 'NULL ''COMP_Entity_Structure_FK'', '
Declare @KeyColsASSOCFK Varchar(Max) = 'NULL ''ASSOC_Entity_Structure_FK'', '
Declare @KeyColsDV Varchar(Max) = 'NULL ''Data_Value_FK'', '
Declare @KeyColsPRODDV Varchar(Max) = 'NULL ''Prod_Data_Value_FK'', '
Declare @KeyColsCUSTDV Varchar(Max) = 'NULL ''CUST_Data_Value_FK'', '
Declare @KeyColsDEPTeDV Varchar(Max) = 'NULL ''DEPT_Entity_Data_Value_FK'', '
Declare @KeyColsSETTINGSDV Varchar(Max) = 'NULL ''Settings_Data_Value_FK'', '
Declare @KeyColsDLCRFK varchar(Max) =  'NULL ''Data_Level_CR_FK'', '
Declare @Temp_Prod_Exists int = 0
Declare @Temp_Cust_Exists int = 0
Declare @Temp_Vendor_Exists int = 0
Declare @Temp_Zone_Exists int = 0
Declare @Cust_Structure_Table_In_Use Varchar(Max)
Declare @Qtbl Varchar(128) = ''
Declare @DN_FK Varchar(16) = ''
Declare @Alias Varchar(16) = ''
Declare @Select Varchar(Max) =  'Select '
Declare @Primary_Column varchar(64) = ''
Declare @Local_Column varchar(64) = ''
Declare @Alias_DV varchar(16) = ''
Declare @SEQ int = 0
Declare @Vendor_Join varchar(16) = ''
Declare @From_Cust Varchar(Max) = ''
Declare @Data_Level_CR_FK varchar(16) = ''
Declare @Data_level_join varchar(max) = ''
Declare @Precedence varchar(8) = ''
Declare @Item_Data_Name_FK varchar(64) = ''
Declare @EDV_SQL_Parent Varchar(Max) = ''
Declare @Data_Column varchar(Max) = ''
Declare @Virtual_Action_Type varchar(64) = ''
Declare @Virtual_Column_Check varchar(Max) = ''
Declare @V_Ref_Data_Name_FK varchar(64) = ''
Declare @QTbl_DL varchar(128) = ''
Declare @Alias_SPPSH varchar(16) = ''
Declare @From1 VARCHAR(MAX) = ''
Declare @SQL_Results varchar(Max) = ''
Declare @ColName varchar(64), @CursorSQL varchar(Max)
Declare @NullColumn int = 0
Declare @FKs Varchar(Max) = ''

Declare @Cust_Structure varchar(Max) = ''
Declare @Primary_Cust_Structure_Column varchar(64) = ''
Declare @Vendor_Entity_Structure_FK varchar(64) = ''
Declare @Zone_Entity_Structure_FK varchar(64) = ''

Declare @Product_Structure_Column Varchar(64) = ''
Declare @Org_Structure_Column Varchar(64) = ''
Declare @Customer_Structure_Column Varchar(64) = ''
Declare @Vendor_Structure_Column Varchar(64) = ''
Declare @Zone_Structure_Column Varchar(64) = ''
Declare @CompStatus int = 1
Declare @CompMessage Varchar(128) = ''
Declare @ind_Recipe_Ing_Comp int = 0
Declare @SQLpbv Varchar(Max)
Declare @ind_Get_Basis_Values int = 0
Declare @VendCustJoinText Varchar(256)
Declare @ExcludeSel int = 0
Declare @Data_Type varchar(64) = ''
Declare @Date_Type Varchar(32) = ''

Declare @FromPBV Varchar(Max) = ''
Declare @SQLDnames Varchar(Max) = ''
Declare @ReturnSet1 Varchar(Max) = ''
Declare @ReturnSet2 Varchar(Max) = ''
Declare @WhereDataLevel_CR_FK Varchar(64) = ''
Declare @VendorTypeFilter Varchar(64) = ''
Declare @VendorItemTypeFilter Varchar(64) = Null
Declare @dv_lookup int = 0
Declare @edv_lookup int = 0
Declare @Attribute_Parent_DN_FK varchar(16) = ''
Declare @PBV Varchar(Max) = ''
Declare @MPR Varchar(Max) = ''
Declare @Zones varchar(16) = ''

	Set @CompStatus = isnull((Select top 1 record_status_cr_fk from #ui_comp_master), 1)

	If @CompStatus = 0
	Begin

		Set @CompMessage = Case @CompStatus When 0 then 'This Component No Longer Exists'
											When 2 then 'This Component is INACTIVE'
											When 3 then 'This component is under construction' 
											else '' end
		Print(@CompMessage)
		--GoTo EOP
	End
	
	--Set @UserID = Replace(@UserID, '.', '_')

	IF Object_ID('tempdb..#Filters') is not null
	drop table #Filters

	Create Table #Filters(CritDN Varchar(64) Null, CritValue Varchar(128) Null, OpExclusion Varchar(8) Null, OptionSetting Varchar(128) Null
	, OptionValue Varchar(64) Null, WhereOption Varchar(Max) Null, WhereDRank Varchar(64), V_Ref_Data_Name_FK bigint null, Data_Type varchar(16) Null
	, Date_Type varchar(16) Null)

	Set @FiltersInForce = 'TempTbls.tmp_Filters_InForce_' + @UserID

	Exec('Insert into #Filters Select * from ' + @FiltersInForce)

	Set @Zones = (select top 1 critvalue from #filters where critdn = 151110)

	IF Object_ID('tempdb..#CodeRefs') is not null
	drop table #CodeRefs
	Create Table #CodeRefs(Code_Type Varchar(64) Not Null, Code_Ref_ID Varchar(128) Not Null)

	Exec('Insert into #CodeRefs
	(Code_Type, Code_Ref_ID) 
	Select CritDN
	, CritValue
	from TempTbls.tmp_FILTER_' + @UserID + ' where critdn like ''%cr_%''')

	If (Select count(*) from #CodeRefs where Code_Type = 'cr_Data_Level') <> 0
	Set @WhereDataLevel_CR_FK = 'where isnull(Data_Level_CR_FK, 0) in (0, ' + (Select Code_Ref_ID from #CodeRefs where Code_Type = 'cr_Data_Level') + ')'
	else Set @WhereDataLevel_CR_FK = ''

	Set @VendorItemTypeFilter = (Select Code_Ref_ID from #CodeRefs where Code_Type = 'cr_Vendor_Item_Type')

	If (Select count(*) from #CodeRefs where Code_Type = 'cr_vendor_type') <> 0
	Set @VendorTypeFilter = ' and sv.vendor_type_cr_Fk in (' + (Select Code_Ref_ID from #CodeRefs where Code_Type = 'cr_vendor_type') + ')'
	Else Set @VendorTypeFilter = ''



	If Object_ID('tempdb..#PBV') is not null
	drop table #PBV

	If object_id('tempdb..#CursorDataLevels') is not null
	drop table #CursorDataLevels

	If Object_ID('tempdb..#DNs') is not null
	drop table #DNs

	create table #DNs(Data_Name_FK bigint not null, Seq int not null, Data_Level_CR_FK int null, NullColumn int null, Data_Source Varchar(65) Null, Entity_Class_CR_FK int null, Reference_Data_Name_FK bigint Null, FilterValue Varchar(128) Null, OpExclusion Varchar(8) Null, ExcludeSel int null, VirtualWhereClause varchar(Max) Null, Data_Type varchar(16) Null, Date_Type varchar(16) Null)
	create index idx_dns on #DNs(Data_Name_FK, Data_Level_CR_FK)

	Set @Item_Data_Name_FK = (Select value from epacube.epacube_params where application_scope_fk = 10109 and name = 'search2')

--load components to #DNs
	Insert into #DNs(Data_Name_FK, Seq, Data_Level_CR_FK, NullColumn, Data_Source, Entity_Class_CR_FK, Reference_Data_Name_FK, FilterValue, OpExclusion)
	Select *, 'Component', Null, Null, Null, Null 
	from (
			select UC.Data_Name_FK, Rank() over(order by UC.ui_comp_id + isnull((Select Max(Seq) from #DNs), 0)) 'Seq', 0 Data_Level_CR_FK, 0 NullColumn 
			from #ui_comp UC with (nolock) 
			inner join epacube.DATA_NAME_GROUP_REFS DNGR on UC.data_name_fk = DNGR.data_name_fk
			where 1 = 1
				
				and isnull(UC.data_name_fk, 0) <> 0 
				and UC.record_status_CR_FK = 1) a

--add pos depts
	If (select count(*) from #DNs where data_name_fk = 502049) > 0 and (select count(*) from #DNs where data_name_fk = 141124) = 0
		Insert into #DNS(Data_Name_FK, Seq, Data_Level_CR_FK, NullColumn, Data_Source) Select 141124, isnull((Select seq from #DNs where data_name_fk = 502049), 0) + 5, 0 Data_Level_CR_FK, 1 NullColumn, 'Component' 'Data_Source'
	Else
		Update DNs
		Set Seq = isnull((Select seq from #DNs where data_name_fk = 502049), 0) + 5
		from #DNs DNs where data_name_fk = 141124

--add data levels
	Insert into #DNs
	(Data_Name_FK, Seq, Data_Level_CR_FK, NullColumn, Data_Source, Entity_Class_CR_FK, Reference_Data_Name_FK, FIlterValue, OpExclusion)
	Select *, Null, Null from (
	Select DNs.Data_Name_FK, DNS.Seq, dls.DATA_LEVEL_CR_FK
		, Null NullColumn
		, Data_Source, Null ECFK, Null Rev_DN_FK
	from #DNs DNs
	cross join (Select data_level_cr_fk from #UI_COMP_LEVELS with (nolock) where RECORD_STATUS_CR_FK = 1
	group by data_level_cr_fk) DLs
	left join #UI_COMP_LEVELS UCL with (nolock) on ucl.RECORD_STATUS_CR_FK = 1
	and dns.data_name_fk = ucl.data_name_fk and dls.DATA_LEVEL_CR_FK = ucl.DATA_LEVEL_CR_FK) A 
	group by Data_Name_FK, Seq, DATA_LEVEL_CR_FK, NullColumn, Data_Source, ECFK, Rev_DN_FK
	order by Data_level_CR_FK, Seq

--add data names from filters with options buttons
	Update DNs
	Set Entity_Class_CR_FK = R.entity_class_cr_fk
	, data_type = r.data_type
	, date_type = r.date_type
	from #DNs DNs inner join epacube.data_name_group_Refs R on DNs.Data_Name_FK = R.data_name_fk

	Insert into #DNs
	(Data_Name_FK, Seq, Data_Level_CR_FK, NullColumn, Data_Source, Entity_Class_CR_FK, Reference_Data_Name_FK, FilterValue, ExcludeSel)
	Select
	f.critdn 'Data_Name_FK'
	, (Select Max(Seq) from #DNs) + 1 'Seq'
	, 0 'Data_Level_CR_FK'
	, 0 'NullColumn'
	, 'Filters' 'Data_Source'
	, DNGR.Entity_Class_CR_FK
	, dngr.V_Ref_Data_Name_FK 'Reference_Data_Name_FK'
	, replace(f.OptionSetting, '`', '') 'filtervalue'
	, Case when dngr.data_type <> 'date' and replace(f.OptionSetting, '`', '') = 'all' then 0 else 1 end 'ExcludeSel'
	from #Filters F
	inner join epacube.DATA_NAME_GROUP_REFS dngr on f.critdn = dngr.Data_Name_FK
	left join #DNs DNs on f.critdn = DNs.data_name_fk
	where 1 = 1
	and f.OptionSetting is not null
	and DNS.data_name_fk is null
	and dngr.Entity_Class_CR_FK in (select entity_class_cr_fk from #DNs)
	and isnull((Select Exclude_forced_DNs from epacube.ui_comp_master where ui_comp_master_id = @UI_COMP_MASTER_FK), 0) <> 1

--select * from #DNs where data_name_fk = 550001
-----------------------------------
--add data names for joins to support virtual data name References to other data names that are not part of the component

	Insert into #DNs
	(Data_Name_FK, Seq, Data_Level_CR_FK, NullColumn, Data_Source, Entity_Class_CR_FK, ExcludeSel)
	select distinct r2.data_name_fk, (Select Max(Seq) from #DNs) + 1 'Seq', DLevels.Data_Level_CR_FK 'Data_Level_CR_FK', 0 'NullColumn', 'Referential' 'Data_Source', r2.Entity_Class_CR_FK, 1 'ExcludeSel'
	from #DNs DN 
	inner join epacube.data_name_group_refs dngr on DN.data_name_fk = DNGR.Data_Name_FK
	inner join epacube.data_name_group_refs r2 on dngr.V_Ref_Data_Name_FK = r2.Data_Name_FK
	Cross join (select data_level_cr_fk from #DNs group by data_level_cr_fk) DLevels
	where 1 = 1 
	and r2.data_name_fk not in (select data_name_fk from #DNs)

--add data names that virtual data names are dependent upon
	Insert into #DNs
	(Data_Name_FK, Seq, Data_Level_CR_FK, NullColumn, Data_Source, Entity_Class_CR_FK, ExcludeSel)
	Select distinct
	dngr.Data_Name_FK, (Select Max(Seq) from #DNs) + 1 'Seq', DLevels.Data_Level_CR_FK 'Data_Level_CR_FK'--, isnull(DNGR.data_level_cr_fk, 0) 'Data_Level_CR_FK'
	, 0 'NullColumn', 'dependence' 'Data_Source', DNGR.Entity_Class_CR_FK, 1 'ExcludeSel' --dnv.exclude_select_dependent 'ExcludeSel'
	from #DNs dn
	inner join epacube.data_name_virtual dnv on dn.data_name_fk = dnv.data_name_fk
	inner join epacube.data_name_group_refs dngr on DNGR.Data_Name_FK in (Select * from common.utilities.splitstring((select dependent_data_name_fk from epacube.data_name_virtual where data_name_fk = dnv.data_name_fk), ','))
	Cross join (select data_level_cr_fk from #DNs group by data_level_cr_fk) DLevels
	where	dnv.dependent_data_name_fk is not null
			and dngr.data_name_fk not in (select data_name_fk from #DNs)

--Select epacube.getdatanamelabel(data_name_fk) DNL, * from #DNs
-----------------------------------

	Update DNs
	Set NullColumn = 0
	from #DNs DNS 
	where 1 = 1
	and (
		(data_name_fk = 141124 and data_level_cr_fk in (Select data_level_cr_fk from #DNS where data_name_fk = 502049 and NullColumn = 0)) 
		OR
		(data_name_fk in (select data_name_fk from [epacube].[DATA_NAME_GROUP_REFS] with (nolock) where isnull(data_level_cr_fk, 0) = 0) and data_name_fk not in (141124, 502049))
		OR
		(data_level_cr_fk = 510 and data_name_fk in (select data_name_fk from [epacube].[DATA_NAME_GROUP_REFS] with (nolock) where isnull(data_level_cr_fk, 0) = 510))
		)

	Update DNs
	Set NullColumn = 1
	from #DNs DNS 
	left join epacube.data_name_group_refs r with (nolock) on dns.data_name_fk = r.data_name_fk and (isnull(dns.data_level_cr_fk, 0) = isnull(r.data_level_cr_fk, 0) or isnull(r.data_level_cr_fk, 0) = 0)
	where 1 = 1
	and r.table_name is null
	AND (Select count(*) from #DNs where isnull(Data_Level_CR_FK, 0) <> 0) > 0

	Update DNs
	Set Entity_Class_CR_FK = R.entity_class_cr_fk
	, data_type = r.data_type
	, date_type = r.date_type
	from #DNs DNs inner join epacube.data_name_group_Refs R on DNs.Data_Name_FK = R.data_name_fk --and (isnull(DNs.Data_Level_CR_FK, 0) = isnull(R.Data_Level_CR_FK, 0) or isnull(R.Data_Level_CR_FK, 0) = 0 or isnull(DNs.Data_Level_CR_FK, 0) = 0)

	Update DNs
	Set [Reference_Data_Name_FK] = DNV.[Reference_Data_Name_FK]
	, FilterValue = case when dns.filtervalue is null then F.CritValue else dns.filtervalue end
	, OpExclusion = F.OpExclusion
	, VirtualWhereClause = f.WhereOption
	--, VirtualWhereClause = Case when isnull(DNs.date_type, '') <> 'effective' then f.WhereOption end
	from #DNs DNs inner join epacube.DATA_NAME_Virtual DNV with (nolock) on DNs.Data_Name_FK = DNV.Data_Name_FK
	left join #Filters F on DNV.DATA_NAME_FK = F.CritDN
	
	Update DNs
	Set Seq = a.NewSeq
	from #DNs DNs inner join
	(	Select data_name_fk, Seq, data_source, excludesel
		, Rank()over(order by case	when data_source = 'dependence' then 1
									when data_source = 'filters' and isnull(ExcludeSel, 0) = 0 then 2
									when data_source = 'component' then 3 else 4 end
							, seq
							, data_level_cr_fk
							, entity_class_cr_fk desc
							, data_name_fk) 'NewSeq'
		from #DNs
	) a on DNs.data_name_fk = a.data_name_fk

	Set @AllowanceCriteria = isnull((Select top 1 WhereOption from #Filters where V_Ref_Data_Name_FK = 503207 and OptionValue is not null and epacube.getdatatypefordn(critdn) = 'Date'), '')
		--(Select top 1 OptionSetting from #Filters where V_Ref_Data_Name_FK = 503207 and date_type = 'effective') )

	Set @AllowanceOption = (Select top 1 OptionSetting from #Filters where V_Ref_Data_Name_FK = 503207 and date_type = 'effective')

	Set @BT_Status = isnull((Select top 1 1 from EPACUBE.DATA_NAME_GROUP_REFS R1 with (nolock) inner join #DNs DNs with (nolock) on R1.Data_Name_FK = DNs.Data_Name_FK where R1.data_name_fk = 144010), 0)

	Set @ST_Status = isnull((Select top 1 1 from EPACUBE.DATA_NAME_GROUP_REFS R1 with (nolock) inner join #DNs DNs with (nolock) on R1.Data_Name_FK = DNs.Data_Name_FK where R1.data_name_fk = 144020), 0)

	Set @Prod_Status = isnull((Select top 1 1 from EPACUBE.DATA_NAME_GROUP_REFS R1 with (nolock) inner join #DNs DNs with (nolock) on R1.Data_Name_FK = DNs.Data_Name_FK where R1.Entity_Class_CR_FK = 10109 or r1.ind_PROD_DV = 1), 0)

	Set @Cust_Status =  isnull((
						Select top 1 1 from EPACUBE.DATA_NAME_GROUP_REFS R1 with (nolock) 
						inner join #DNs DNs with (nolock) on R1.Data_Name_FK = DNs.Data_Name_FK
						left join #ui_comp_levels ucl with (nolock) on r1.data_name_fk = ucl.data_name_fk 
																				and r1.data_level_cr_fk = ucl.data_level_cr_fk 
						where ((1 = 1
						and R1.Entity_Class_CR_FK = 10104 and ind_es_fk = 1)
						or ind_ces_fk = 1
						Or R1.Table_Name like '%Product_Recipe%'
						) and isnull(ucl.record_status_cr_fk, 0) <> 2
----Testing this out 4/30/18
						--and (select count(*) from #DNs where Entity_Class_CR_FK = 10104) <> 0
----
						)
						, 0)

	Set @t_prod = Case when @t_prod = '' then 'TempTbls.tmp_PROD_' + @UserID else @t_prod end
	Set @t_vendor = Case when @t_vendor = '' then 'TempTbls.tmp_VENDOR_' + @UserID else @t_vendor end
	Set @t_cust = Case when @t_cust = '' then 'TempTbls.tmp_CUST_' + @UserID else @t_cust end
	Set @t_zone = Case when @t_zone = '' then 'TempTbls.tmp_ZONE_' + @UserID else @t_zone end
	--Set @t_results = Case when @t_results = '' then 'TempTbls.tmp_RESULTS_' + Case when @UI_COMP_MASTER_FK like '%~%' then '1' else @UI_COMP_MASTER_FK end + '_' + @UserID else @t_results end


	If Object_ID(@t_results) is not null
	Exec('drop table ' + @t_results)

	if object_id(@t_prod) is not null
	Set @Temp_Prod_Exists = 1

	if object_id(@t_cust) is not null
	Set @Temp_Cust_Exists = 1

	if object_id(@t_vendor) is not null
	Set @Temp_Vendor_Exists = 1

	if object_id(@t_zone) is not null
	Set @Temp_Zone_Exists = 1

	Set @Vendor_Join = Case when @Temp_Vendor_Exists = 1 and @Vendor_Status = 1 then '
	inner' else 'Left' end

	If @Temp_Zone_Exists = 1
	Set @Cust_Status = 0

	Set @Vendor_Status =	isnull((Select top 1 1 from EPACUBE.DATA_NAME_GROUP_REFS R1 with (nolock) inner join #DNs DNs with (nolock) on R1.Data_Name_FK = DNs.Data_Name_FK where R1.Entity_Class_CR_FK = 10103), 0)
	
	Set @Zone_Status =	isnull((Select top 1 1 from EPACUBE.DATA_NAME_GROUP_REFS R1 with (nolock) inner join #DNs DNs with (nolock) on R1.Data_Name_FK = DNs.Data_Name_FK where R1.Entity_Class_CR_FK = 10117), 0)

	Set @Auths_Ind = Case	When @Temp_Zone_Exists = 1 and (@Temp_prod_Exists = 0 or @Temp_Cust_Exists = 0) then 0
							When (Select count(*) from #filters where V_Ref_Data_Name_FK = 159905 and isnull(OptionSetting, '') = 'no') <> 0 then 2
							When (Select count(*) from #filters where V_Ref_Data_Name_FK = 159905 and isnull(OptionSetting, '') = 'yes') <> 0 then 1 
							else 0 
							end

--Select @Auths_Ind '@Auths_Ind'
	Set @Auths_Join = Case @Auths_Ind when 2 then 'left' else 'inner' end

	Set @Cust_Structure_Table_In_Use = (Select  isnull(Max(Case when r.table_name not like '%Segment%' and r.table_name not like '%Settings%' then 1 else 0 end) , 0) 
										from EPACUBE.DATA_NAME_GROUP_REFS R with (nolock) 
										inner join #ui_comp UC with (nolock) on R.Data_Name_FK = UC.Data_Name_FK 
										inner join #DNs DNs on R.data_name_fk = DNs.data_name_fk and (isnull(R.data_level_cr_fk, 0) = isnull(DNS.Data_Level_CR_FK , 0) or isnull(R.data_level_cr_fk, 0) = 0) 
										where UC.record_status_CR_FK = 1 and R.Customer_structure_column is not null)

	Set @ind_Recipe_Ing_Comp = isnull((select Max(ind_Rec_Ingredient_XRef) from EPACUBE.DATA_NAME_GROUP_REFS R1 with (nolock) inner join #DNS DNS on R1.Data_Name_FK = DNS.Data_Name_FK), 0)

	Set @ind_Get_Basis_Values = isnull((select top 1 1 from EPACUBE.DATA_NAME_GROUP_REFS R1 with (nolock) inner join #DNS DNS on R1.Data_Name_FK = DNS.Data_Name_FK where QTbl = '#PBV'), 0)

--Check for Header Components

	If (select count(*) from #ui_comp uc with (nolock) 
	inner join epacube.data_name dn with (nolock) on uc.data_name_fk = dn.data_name_id
	inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id
	and ds.COLUMN_NAME = 'data_value_fk'
	) = 1

	and

	(select count(*) from #ui_comp uc with (nolock) 
	inner join epacube.data_name dn with (nolock) on uc.data_name_fk = dn.data_name_id
	inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id
	) = 1
	
	Begin
		Select @DN_FK = uc.data_name_fk
		, @Data_Column = case when isnull(dv.[description], '') <> '' then dv.value + '-' + dv.[description] else dv.value end, @Alias = dn.name
		from #ui_comp uc
		inner join epacube.data_name dn on uc.data_name_fk = dn.DATA_NAME_ID
		left join #Filters F on uc.data_name_fk = f.critdn
		left join epacube.data_value dv on uc.data_name_fk = dv.data_name_fk and f.CritValue = dv.VALUE
		where 1 = 1
		and uc.DATA_NAME_FK not in (110100, 110103, 144020)

		Exec('If Object_ID(''' + @T_Results + ''') is not null drop table ' + @t_Results + '; Select ' + @DN_FK + ' ''DN_' + @DN_FK + ''', ''' + @Data_Column + ''' ''' + @Alias + ''' into ' + @t_results)

		goto eop

	end

	Declare @Prod_Alias varchar(64)
	Declare @Cust_Alias varchar(64)
	Declare @Vendor_Alias varchar(64)
	Declare @Zone_Alias varchar(64)

	Set @Prod_Alias = Case when @Temp_Prod_Exists = 1 and @Prod_Status = 1 then 't_PROD' when @Temp_Prod_Exists = 1 and @Prod_Status = 0 then 'PA_159905' else 'PI' end
	Set @CUST_Alias = Case when @Temp_CUST_Exists = 1 then 't_CUST'  when @Cust_Status = 1 then 'eic' else '' end
	Set @VENDOR_Alias = Case when @Temp_VENDOR_Exists = 1 and @Vendor_Status = 1 then 't_VENDOR' else 'eiv' end
	Set @ZONE_Alias = Case when @Temp_ZONE_Exists = 1 and @ZONE_Status = 1 then 't_ZONE' else 'eiz' end

	Set @Primary_Cust_Structure_Column = Case when @Temp_CUST_Exists = 1 then 'cust_entity_structure_fk' else 'entity_structure_fk' end
	Set @Vendor_Entity_Structure_FK = Case when @Temp_Vendor_Exists = 1 then 'vendor_entity_structure_fk' else 'entity_structure_fk' end
	Set @ZONE_Entity_Structure_FK = Case when @Temp_Zone_Exists = 1 then 'zone_entity_structure_fk' else 'entity_structure_fk' end

	Set @Cust_Structure = 
	Case 
	When @BT_Status = 1 and @ST_Status = 1 and @Cust_Structure_Table_In_Use = 1 then '			
		' + @Auths_Join + ' join epacube.epacube.entity_structure esc_p with (Nolock) on ' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = esc_p.entity_structure_id
		' + @Auths_Join + ' join epacube.epacube.entity_identification eic_p with (nolock) on esc_p.entity_structure_id = eic_p.entity_structure_fk and esc_p.data_name_fk = eic_p.entity_data_name_fk and esc_p.data_name_fk = 144010
		' + @Auths_Join + ' join epacube.epacube.entity_structure esc_c with (nolock) on eic_p.entity_structure_fk = esc_c.parent_entity_structure_fk and esc_c.data_name_fk = 144020
		' + @Auths_Join + ' join epacube.epacube.entity_identification eic with (nolock) on esc_c.entity_structure_id = eic.entity_structure_fk'
	When @BT_Status = 1 and @Cust_Structure_Table_In_Use = 1 then '
		' + @Auths_Join + ' join epacube.epacube.entity_structure esc_p with (Nolock) on ' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = esc_p.entity_structure_id
		' + @Auths_Join + ' join epacube.epacube.entity_identification eic with (nolock) on esc_p.entity_structure_id = eic.entity_structure_fk and esc_p.data_name_fk = eic.entity_data_name_fk and esc_p.data_name_fk = 144010 '			
	When @ST_Status = 1 and @Cust_Structure_Table_In_Use = 1 then '
		' + @Auths_Join + ' join epacube.epacube.entity_structure ESC with (nolock) on ' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = esc.entity_structure_id 
		' + @Auths_Join + ' Join epacube.entity_identification eic with (nolock) on esc.entity_structure_id = eic.entity_structure_fk and esc.data_name_fk = eic.entity_data_name_fk and esc.data_name_fk in (144010, 144020) '
	When @BT_Status = 1 and @ST_Status = 1 and @Temp_Prod_Exists = 1 and @Cust_Structure_Table_In_Use = 0 then '			
		' + @Auths_Join + ' join epacube.epacube.entity_structure esc_p with (Nolock) on pa_159905.entity_structure_fk = esc_p.entity_structure_id
		' + @Auths_Join + ' join epacube.epacube.entity_identification eic_p with (nolock) on esc_p.entity_structure_id = eic_p.entity_structure_fk and esc_p.data_name_fk = eic_p.entity_data_name_fk and esc_p.data_name_fk = 144010
		' + @Auths_Join + ' join epacube.epacube.entity_structure esc_c with (nolock) on eic_p.entity_structure_fk = esc_c.parent_entity_structure_fk and esc_c.data_name_fk = 144020
		' + @Auths_Join + ' join epacube.epacube.entity_identification eic with (nolock) on esc_c.entity_structure_id = eic.entity_structure_fk'
	When @BT_Status = 1 and @Temp_Prod_Exists = 1 and @Cust_Structure_Table_In_Use = 0 then '
		' + @Auths_Join + ' join epacube.epacube.entity_structure esc_p with (Nolock) on pa_159905.entity_structure_fk = esc_p.entity_structure_id
		' + @Auths_Join + ' join epacube.epacube.entity_identification eic with (nolock) on esc_p.entity_structure_id = eic.entity_structure_fk and esc_p.data_name_fk = eic.entity_data_name_fk and esc_p.data_name_fk = 144010 '			
	When @ST_Status = 1 and @Temp_Prod_Exists = 1 and @Cust_Structure_Table_In_Use = 0 then '
		' + @Auths_Join + ' join epacube.epacube.entity_structure ESC with (nolock) on pa_159905.entity_structure_fk = esc.entity_structure_id 
		' + @Auths_Join + ' Join epacube.entity_identification eic with (nolock) on esc.entity_structure_id = eic.entity_structure_fk and esc.data_name_fk = eic.entity_data_name_fk and esc.data_name_fk in (144010, 144020) '
	else ''
	end

	Set @From_Cust = 
	Case 
	When @Temp_Cust_Exists = 0 and @BT_Status = 1 and @ST_Status = 1 and @Cust_Structure_Table_In_Use = 1 then '			
		From epacube.epacube.entity_structure esc_p with (Nolock) 
		' + @Auths_Join + ' join epacube.epacube.entity_identification eic_p with (nolock) on esc_p.entity_structure_id = eic_p.entity_structure_fk and esc_p.data_name_fk = eic_p.entity_data_name_fk and esc_p.data_name_fk = 144010
		' + @Auths_Join + ' join epacube.epacube.entity_structure esc_c with (nolock) on eic_p.entity_structure_fk = esc_c.parent_entity_structure_fk and esc_c.data_name_fk = 144020
		' + @Auths_Join + ' join epacube.epacube.entity_identification eic with (nolock) on esc_c.entity_structure_id = eic.entity_structure_fk'
	When @Temp_Cust_Exists = 0 and @BT_Status = 1 and @Cust_Structure_Table_In_Use = 1 then '
		From epacube.epacube.entity_structure esc_p with (Nolock) 
		' + @Auths_Join + ' join epacube.epacube.entity_identification eic with (nolock) on esc_p.entity_structure_id = eic.entity_structure_fk and esc_p.data_name_fk = eic.entity_data_name_fk and esc_p.data_name_fk = 144010 '			
	When @Temp_Cust_Exists = 0 and @ST_Status = 1 and @Cust_Structure_Table_In_Use = 1 then '
		From epacube.epacube.entity_structure ESC with (nolock) 
		' + @Auths_Join + ' Join epacube.entity_identification eic with (nolock) on esc.entity_structure_id = eic.entity_structure_fk and esc.data_name_fk = eic.entity_data_name_fk and esc.data_name_fk in (144010, 144020) '
	else ''
	end

	Set @From = 
	Case when @Temp_Prod_Exists = 1 and @Prod_Status = 1 then '
		From ' + @t_prod + ' t_prod with (nolock) '
	when (@Temp_Prod_Exists = 0 and @Prod_Status = 1) then '
		From epacube.product_structure ps with (nolock)
		inner join epacube.product_identification pi with (nolock) on ps.product_structure_id = pi.product_structure_fk and pi.data_name_fk = ' + @Item_Data_Name_FK
	When @Temp_Cust_Exists = 1 and @Cust_Status = 1 then  '
		From ' + @t_cust + ' t_cust with (nolock) '
	when @Temp_Cust_Exists = 0 and @Cust_Status = 1 then '
	' +	@From_Cust

	when  @Temp_Vendor_Exists = 1 and @Vendor_Status = 1 then '
		From ' + @t_vendor + ' t_vendor with (nolock) '
	when @Temp_Vendor_Exists = 0 and @Vendor_Status = 1 then '
		From epacube.entity_structure es with (nolock)
		inner join epacube.entity_identification eiv with (nolock) on es.entity_structure_id = eiv.entity_structure_fk and es.data_name_fk = 143000 '
	When @Temp_Zone_Exists = 1 and @Zone_Status = 1 then '
		From ' + @t_Zone + ' t_zone with (nolock) '
	when @Temp_Zone_Exists = 0 and @Zone_Status = 1 then '
		From epacube.entity_structure es with (nolock)
		inner join epacube.entity_identification eiz with (nolock) on es.entity_structure_id = eiz.entity_structure_fk and es.data_name_fk = 151000 '
	else
	''
	end

	Set @From = @From +
		Case when @Zones = 'UNASSIGNED' and @Zone_Status = 1 then '
		cross join (Select entity_structure_fk from epacube.entity_identification with (nolock) where data_name_fk = 151110 and entity_data_name_fk = 151000 --and ENTITY_STRUCTURE_FK between 19172 and 19190
		) cj
		left join epacube.epacube.product_association pa_159450 with (nolock) on t_PROD.product_structure_fk = pa_159450.product_structure_fk and pa_159450.data_name_fk = 159450  and pa_159450.ENTITY_STRUCTURE_FK = cj.ENTITY_STRUCTURE_FK
		left join TempTbls.tmp_ZONE_gstone@epacube_com t_zone on pa_159450.entity_structure_fk = t_zone.zone_entity_structure_fk
		
		'
		when @Temp_Prod_Exists = 1 and @Prod_Status = 1 and @Temp_Zone_Exists = 1 and @Zone_Status = 1 then '
		inner join epacube.epacube.product_association pa_159450 with (nolock) on t_PROD.product_structure_fk = pa_159450.product_structure_fk and pa_159450.data_name_fk = 159450 
		inner join ' + @t_zone + ' t_zone on pa_159450.entity_structure_fk = t_zone.zone_entity_structure_fk'

				when @Temp_Prod_Exists = 0 and @Prod_Status = 1 and @Temp_Zone_Exists = 1 and @Zone_Status = 1 then '
		inner join epacube.epacube.product_association pa_159450 with (nolock) on ps.product_structure_id = pa_159450.product_structure_fk and pa_159450.data_name_fk = 159450 
		inner join ' + @t_zone + ' t_zone on pa_159450.entity_structure_fk = t_zone.zone_entity_structure_fk'
		else '' end

--Select @Prod_Status '@Prod_Status', @Temp_Prod_Exists '@Temp_Prod_Exists', @Cust_Status '@Cust_Status', @Temp_Cust_Exists '@Temp_Cust_Exists', @Vendor_Status '@Vendor_Status', @Temp_Vendor_Exists '@Temp_Vendor_Exists', @From '@From', @BT_Status '@BT_Status', @ST_Status '@ST_Status', @Cust_Structure_Table_In_Use '@Cust_Structure_Table_In_Use', @Auths_Ind '@Auths_Ind'

--When product is required in component, and any oher data that is required by filter or component
Set @VendCustJoinText = 
	Case When @From like '%' + @Primary_Cust_Structure_Column + '%' then '
	and ((' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = sv.cust_entity_structure_fk and sv.data_level_cr_fk = 508) or (sv.data_level_cr_fk = 518)) '
	else '
	and sv.data_level_cr_fk = 518 ' end

--@Temp_Zone_Exists = 1 and (@Temp_prod_Exists = 0 or @Temp_Cust_Exists = 0)

--Select @Prod_Status '@Prod_Status', @Cust_Status '@Cust_Status', @Zone_Status '@Zone_Status', @Auths_Ind '@Auths_Ind', @Temp_Zone_Exists '@Temp_Zone_Exists', @Temp_prod_Exists '@Temp_prod_Exists', @Temp_Cust_Exists '@Temp_Cust_Exists'

	Set @From = @From +
		--Case when @Prod_Status = 1 then
		Case when @Prod_Status = 1 and (@Cust_Status <> 0 or @Auths_Ind <> 0) then
			Case when (@Temp_Cust_Exists = 1 and @Cust_Status = 1) or (@Temp_Cust_Exists = 1 and @Auths_Ind = 1) then '
		' + @Auths_Join + ' join epacube.product_association PA_159905 with (nolock) on '+ @Prod_Alias +'.product_structure_fk = pa_159905.product_structure_fk and pa_159905.data_name_fk = 159905 ' + Case when @Auths_Ind = 2 then ' and pa_159905.entity_structure_fk in (select cust_entity_structure_fk from ' + @t_cust + ' with (nolock)) ' else '' end + '
		' + Case when @Auths_Ind = 0 then 'left' else @Auths_Join end + ' join ' + @t_cust + ' t_cust with (nolock) on pa_159905.entity_structure_fk = t_cust.cust_entity_structure_fk 
		' + @Cust_Structure
		When @Temp_Cust_Exists = 0 and @Cust_Status = 1 then '
		' + @Auths_Join + ' join epacube.product_association PA_159905 with (nolock) on '+ @Prod_Alias +'.product_structure_fk = pa_159905.product_structure_fk and pa_159905.data_name_fk = 159905 '
		+ Replace(@Cust_Structure, ' ESC with (nolock) on eic.[entity_structure_fk]', ' ESC with (nolock) on pa_159905.[entity_structure_fk]')
		else '' end

		+	Case when @Temp_Vendor_Exists = 1 and @Vendor_Status = 1 then '
		' + @Vendor_Join + ' join epacube.segments_vendor sv with (nolock) on '+ @Prod_Alias +'.product_structure_fk = sv.product_structure_fk ' + @VendorTypeFilter + '
							' + @VendCustJoinText + '
		' + @Vendor_Join + ' join ' + @t_vendor + ' t_vendor with (nolock) on sv.vendor_entity_structure_fk = t_vendor.vendor_entity_structure_fk ' + @VendorTypeFilter
				when @Temp_Vendor_Exists = 0 and @Vendor_Status = 1 and @Cust_Status = 1 then

		 + @Vendor_Join + ' join epacube.epacube.segments_vendor sv with (nolock) on '+ @Prod_Alias +'.product_structure_fk = sv.product_structure_fk 		
			and isnull(sv.cust_entity_structure_fk, 0) = case data_level_cr_fk when 508 then ' + @Cust_Alias + '.' + @Primary_Cust_Structure_Column + ' else 0 end and sv.vendor_item_type_CR_FK in (' + isnull(@VendorItemTypeFilter, '611') + ') ' + @VendorTypeFilter	
	
				when @Temp_Vendor_Exists = 0 and @Vendor_Status = 1 then '
		' + @Vendor_Join + ' join epacube.epacube.segments_vendor sv with (nolock) on '+ @Prod_Alias +'.product_structure_fk = sv.product_structure_fk '  + @VendorTypeFilter + '
							' + @VendCustJoinText + '
		' + @Vendor_Join + ' join epacube.entity_identification eiv with (nolock) on sv.vendor_entity_structure_fk = eiv.entity_structure_fk and eiv.data_name_fk = 143110 '
				else '' end

--When Prod data is not required by component and any other data is required by filter or component
		when @Prod_Status = 0 then
			Case when @Temp_Vendor_Exists = 1 and @Vendor_Status = 1 and @Temp_Prod_Exists = 1 then '
		' + @Vendor_Join + ' join epacube.segments_vendor sv with (nolock) on ((' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = sv.cust_entity_structure_fk and sv.data_level_cr_fk = 508) or sv.data_level_cr_fk = 518) and sv.product_structure_fk in (select Product_structure_fk from ' + @t_prod + ' with (nolock))
		' + @Vendor_Join + ' join ' + @t_vendor + ' t_vendor with (nolock) on sv.vendor_entity_structure_fk = t_vendor.vendor_entity_structure_fk ' + @VendorTypeFilter

			When @Temp_Vendor_Exists = 0 and @Vendor_Status = 1 and @Cust_Status = 1 and @Temp_Prod_Exists = 1  then '
		inner join epacube.product_association pa_159905 with (nolock) on ' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = pa_159905.entity_structure_fk
		inner join ' + @t_Prod + ' t_prod with (nolock) on pa_159905.product_structure_fk = t_prod.product_structure_fk
		inner join epacube.segments_vendor sv with (nolock) on (	(sv.data_level_cr_fk = 518 and t_prod.product_structure_fk = sv.product_structure_fk) or
																	(sv.data_level_Cr_fk = 508 and t_prod.product_structure_fk = sv.product_structure_fk and ' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = sv.cust_entity_structure_fk)	)
																	' + @VendorTypeFilter + '
		inner join epacube.entity_identification eiv with (nolock) on sv.vendor_entity_structure_fk = eiv.entity_structure_fk and eiv.entity_data_name_fk = 143000 and eiv.data_name_fk = 143110 ' 

			when @Temp_Vendor_Exists = 0 and @Vendor_Status = 1 and @Cust_Status = 1 then '
		inner join (Select Cust_Entity_Structure_FK, Vendor_Entity_Structure_FK from epacube.epacube.segments_vendor sv with (nolock) 
		where sv.data_level_cr_fk = 508 ' +  @VendorTypeFilter + '
			' + Case when @Temp_Cust_Exists = 1 then '
		and cust_entity_structure_fk in (Select cust_entity_structure_fk from ' + @t_cust + ' with (nolock)) Group by Cust_Entity_Structure_FK, Vendor_Entity_Structure_FK ' else '' end + '
		) sv on ' + Case @Cust_Status when 1 then @Cust_Alias else @Vendor_Alias end + '.entity_structure_fk = sv.' + Case @Cust_Status when 1 then 'Cust' else 'Vendor' end + '_entity_structure_fk ' 

			When @Temp_Vendor_Exists = 0 and @Vendor_Status = 1 and @Cust_Status = 0 and @Temp_Prod_Exists = 1 then '
		inner join (Select Product_Structure_FK, Vendor_Entity_Structure_FK from epacube.epacube.segments_vendor sv with (nolock) 
		where sv.data_level_cr_fk = 518 ' +  @VendorTypeFilter + '
		and product_structure_fk in (select Product_structure_fk from ' + @t_prod + ' with (nolock)) Group by Product_Structure_FK, Vendor_Entity_Structure_FK ) sv on eiv.entity_structure_fk = sv.Vendor_entity_structure_fk '
			
			when @Temp_Vendor_Exists = 0 and @Vendor_Status = 1 then '
			' + Case when @Temp_Prod_Exists = 1 then '
		inner join (Select Product_Structure_FK, Vendor_Entity_Structure_FK from epacube.epacube.segments_vendor sv with (nolock) 
		where sv.data_level_cr_fk = 518 ' +  @VendorTypeFilter + '
		and product_structure_fk in (select Product_structure_fk from ' + @t_prod + ' with (nolock)) Group by Product_Structure_FK, Vendor_Entity_Structure_FK) sv on eiv.entity_structure_fk = sv.Vendor_entity_structure_fk '  else '' end
			else '' end
		else '' end

	Set @From = @From + Case when (Select top 1 1 from EPACUBE.DATA_NAME_GROUP_REFS R1 with (nolock) 
					inner join #ui_comp UC1 with (nolock) on R1.Data_Name_FK = UC1.Data_Name_FK where UC1.record_status_CR_FK = 1 and R1.Table_Name like '%Product_Recipe%') = 1 then '
		inner join epacube.segments_customer sc with (nolock) on sc.data_name_fk = 502045 and sc.cust_entity_structure_fk = ' + 
			Case When @From like '%pa_159905%' and @From not like '%' + @Cust_Alias + '%' then 'pa_159905' else @Cust_Alias end + '.[' + @Primary_Cust_Structure_Column + ']
		left join epacube.Product_recipe pr_503999 with (nolock) on ' + @prod_alias + '.Product_Structure_FK = pr_503999.Product_Structure_FK and pr_503999.chain_id_data_value_fk =  sc.CUST_SEGMENT_FK ' else '' end
							
	If isnull((select Max(ind_Rec_Ingredient_XRef) from EPACUBE.DATA_NAME_GROUP_REFS R1 with (nolock) inner join #DNS DNS on R1.Data_Name_FK = DNS.Data_Name_FK), 0) = 1
	Begin
		Set @From = @From + '
		inner join epacube.DATA_VALUE dv_ing with (nolock) on pr_503999.chain_id_data_value_fk = dv_ing.DATA_VALUE_ID
		inner join epacube.product_recipe_ingredient PRI_504031 with (nolock) on pr_503999.product_recipe_ID = PRI_504031.product_recipe_fk and pr_503999.CHAIN_ID_DATA_VALUE_FK = PRI_504031.CHAIN_ID_DATA_VALUE_FK
		INNER JOIN EPACUBE.PRODUCT_RECIPE_INGREDIENT_XREF PRIX_504030 with (nolock)	on PRI_504031.chain_id_data_value_fk = PRIX_504030.chain_id_data_value_fk 
																				and PRI_504031.product_recipe_ingredient_xref_fk = PRIX_504030.product_recipe_ingredient_xref_id
		inner join epacube.data_value CIDV_504030 with (nolock) on PRIX_504030.data_value_fk = CIDV_504030.DATA_VALUE_ID
		inner join epacube.PRODUCT_IDENTIFICATION pi_504030 with (nolock) on PRIX_504030.product_structure_fk = pi_504030.PRODUCT_STRUCTURE_FK and pi_504030.DATA_NAME_FK = ' + @Item_Data_Name_FK + '
		inner join epacube.product_description pd_504030 with (nolock) on PRIX_504030.product_structure_fk = pd_504030.PRODUCT_STRUCTURE_FK and pd_504030.DATA_NAME_FK = 110401 '
	End

	--If isnull(@ind_Get_Basis_Values, 0) > 0 
	--Begin
	--	If object_id('tempdb..#PBV') is not null
	--	drop table #PBV

	--	If object_id('tempdb..#DNames') is not null
	--	drop table #DNames
		
	--	If @UI_RETURN_SQL = 0 or @UI_DN_Source <> 1
	--	Begin
	--		Select top 1 Cast(Null as int) Rnk
	--		, [PriceSheetID], [Effective_Date], [End_Date], [Product_Structure_FK], [Org_Entity_Structure_FK], [Cust_Entity_Structure_FK], [Vendor_Entity_Structure_FK], [Zone_Type_CR_FK], [Zone_Entity_Structure_FK], [Data_Name_FK], [Basis_Position], [Value], [UOM_CODE_FK], [Pack], [SPC_UOM_FK], [Calculated_Value], [Promo], [CO_ENTITY_STRUCTURE_FK], cast(Null as int) 'Record_Status_CR_FK', Cast(Null as Numeric(18, 4)) 'Unit_Value', Cast(Null as bigint) PBV_ID
	--		into #PBV 
	--		from marginmgr.PRICESHEET_BASIS_VALUES PBV with (nolock) 
	--		where 1 = 2

	--		create index idx_pbv on #PBV(data_name_fk, product_structure_fk, cust_entity_structure_fk, vendor_entity_structure_fk, effective_date, Rnk)
	--	End

	--Create Table #DNames(DN_FK Varchar(64) Null, Data_Name_FK bigint Null, OptionSetting varchar(128) Null, OptionValue varchar(64) Null, Data_Column Varchar(Max) NUll, WhereOption Varchar(Max) Null, WhereDRank varchar(64) Null)

	--	If @UI_Comp_Master_FK like '%~%'
	--	Begin
	--		Set @SQLDnames = '
	--Insert into #DNames
	--Select isnull(r.v_ref_data_name_fk, r.data_Name_FK) DN_FK, r.Data_Name_FK, F.OptionSetting, F.OptionValue, r.DATA_COLUMN, F.WhereOption, F.WhereDRank
	--from EPACUBE.DATA_NAME_GROUP_REFS R with (nolock)
	--inner join ' + @FiltersInForce + ' F on r.data_name_fk = F.CritDN and f.OptionSetting is not null
	--where 1 = 1 
	--and r.Table_Name in (''Pricesheet_Basis_Values'', ''#PBV'')
	--and r.data_name_fk in (' + Left(Replace(@UI_COMP_MASTER_FK, '~', ','), Len(@UI_COMP_MASTER_FK) - 1) + ')'

	--	End
	--	Else
	--		Set @SQLDnames = '
	--Insert into #DNames
	--Select isnull(r.v_ref_data_name_fk, r.data_Name_FK) DN_FK, r.Data_Name_FK, F.OptionSetting, F.OptionValue, r.DATA_COLUMN, F.WhereOption, F.WhereDRank
	--from EPACUBE.DATA_NAME_GROUP_REFS R with (nolock)
	--inner join #ui_comp uc with (nolock) on r.data_name_fk = uc.data_name_fk
	--inner join ' + @FiltersInForce + ' F on r.data_name_fk = F.CritDN and f.OptionSetting is not null
	--where 1 = 1 
	--and r.Table_Name in (''Pricesheet_Basis_Values'', ''#PBV'')
	--and uc.record_status_cr_fk = 1'
		
	--	Exec(@SQLDnames)

	--	DECLARE PBVfilters Cursor local for
	--	select DN_FK, isnull(WhereOption, '') WhereOption, isnull(WhereDRank, '') WhereDRank from #DNames 

	--	OPEN PBVfilters;
	--	FETCH NEXT FROM PBVfilters INTO @PBV_DNFK, @WhereOption, @WhereDRank
	--	WHILE @@FETCH_STATUS = 0

	--		Begin--Fetch
		
	--		If @ind_Recipe_Ing_Comp = 0
	--		Begin
	--			Set @SQLpbv = '
	--Insert into #PBV
	--Select A.* from (
	--Select Dense_Rank() over(partition by pbv.data_name_fk, pbv.product_structure_fk, pbv.cust_entity_structure_fk, pbv.vendor_entity_structure_fk order by pbv.effective_date desc) DRank
	--, pbv.[PriceSheetID], pbv.[Effective_Date], pbv.[End_Date], pbv.[Product_Structure_FK], pbv.[Org_Entity_Structure_FK], pbv.[Cust_Entity_Structure_FK], pbv.[Vendor_Entity_Structure_FK], pbv.[Zone_Type_CR_FK], pbv.[Zone_Entity_Structure_FK], pbv.[Data_Name_FK], pbv.[Basis_Position], pbv.[Value], pbv.[UOM_CODE_FK], pbv.[Pack], pbv.[SPC_UOM_FK], pbv.[Calculated_Value], pbv.[Promo], pbv.[CO_ENTITY_STRUCTURE_FK], pbv.[RECORD_STATUS_CR_FK]
	--, pbv.unit_value, pbv.Pricesheet_Basis_Values_ID
	--from marginmgr.PRICESHEET_BASIS_VALUES pbv with (nolock) ' + Case when @Temp_Prod_Exists = 1 then '
	--inner join ' + @t_prod + ' t_prod with (nolock) on pbv.Product_Structure_FK = t_prod.product_structure_fk ' else '' end + Case when @Temp_Cust_Exists = 1 then '
	--left join ' + @t_cust + ' t_cust with (nolock) on pbv.Cust_Entity_Structure_FK = t_cust.Cust_Entity_Structure_FK or pbv.Cust_Entity_Structure_FK is null ' else '' end + Case when @Temp_Vendor_Exists = 1 then '
	--inner join ' + @t_Vendor + ' t_vendor with (nolock) on pbv.Vendor_Entity_Structure_FK = t_vendor.Vendor_Entity_Structure_FK ' else '' end + '
	--inner join #DNames DNames on pbv.data_name_fk = DNames.DN_FK
	--' + @WhereOption + '
	--UNION
	--Select Dense_Rank() over(partition by pbv.data_name_fk, pbv.product_structure_fk, pbv.cust_entity_structure_fk, pbv.vendor_entity_structure_fk order by pbv.effective_date desc) DRank
	--, pbv.[PriceSheetID], pbv.[Effective_Date], pbv.[End_Date], pbv.[Product_Structure_FK], pbv.[Org_Entity_Structure_FK], pbv.[Cust_Entity_Structure_FK], pbv.[Vendor_Entity_Structure_FK], pbv.[Zone_Type_CR_FK], pbv.[Zone_Entity_Structure_FK], pbv.[Data_Name_FK], pbv.[Basis_Position], pbv.[Value], pbv.[UOM_CODE_FK], pbv.[Pack], pbv.[SPC_UOM_FK], pbv.[Calculated_Value], pbv.[Promo], pbv.[CO_ENTITY_STRUCTURE_FK], pbv.[RECORD_STATUS_CR_FK]
	--, pbv.unit_value, pbv.Pricesheet_Basis_Values_ID
	--from marginmgr.PRICESHEET_BASIS_VALUES pbv with (nolock) ' + Case when @Temp_Prod_Exists = 1 then '
	--inner join ' + @t_prod + ' t_prod with (nolock) on pbv.Product_Structure_FK = t_prod.product_structure_fk ' else '' end + Case when @Temp_Cust_Exists = 1 then '
	--left join ' + @t_cust + ' t_cust with (nolock) on pbv.Cust_Entity_Structure_FK = t_cust.Cust_Entity_Structure_FK or (pbv.Cust_Entity_Structure_FK is null and Zone_Entity_Structure_FK = 19195) or pbv.Cust_Entity_Structure_FK is null ' else '' end + Case when @Temp_Vendor_Exists = 1 then '
	--inner join ' + @t_Vendor + ' t_vendor with (nolock) on pbv.Vendor_Entity_Structure_FK = t_vendor.Vendor_Entity_Structure_FK or pbv.Vendor_Entity_Structure_FK is null ' else '' end + '
	--inner join #DNames DNames on pbv.data_name_fk = DNames.DN_FK
	--' + @WhereOption + '
	--) A ' + @WhereDRank + '
	--'
	--		End
	--		Else
	--		If @ind_Recipe_Ing_Comp = 1
	--		Begin
	--			Set @SQLpbv = '
	--Insert into #PBV
	--Select A.* from (
	--Select Dense_Rank() over(partition by pbv.data_name_fk, pbv.product_structure_fk, pbv.cust_entity_structure_fk, pbv.vendor_entity_structure_fk order by pbv.effective_date desc) Rnk
	--, pbv.[PriceSheetID], pbv.[Effective_Date], pbv.[End_Date], pbv.[Product_Structure_FK], pbv.[Org_Entity_Structure_FK], pbv.[Cust_Entity_Structure_FK], pbv.[Vendor_Entity_Structure_FK], pbv.[Zone_Type_CR_FK], pbv.[Zone_Entity_Structure_FK], pbv.[Data_Name_FK], pbv.[Basis_Position], pbv.[Value], pbv.[UOM_CODE_FK], pbv.[Pack], pbv.[SPC_UOM_FK], pbv.[Calculated_Value], pbv.[Promo], pbv.[CO_ENTITY_STRUCTURE_FK], pbv.[RECORD_STATUS_CR_FK]
	--, pbv.unit_value, pbv.Pricesheet_Basis_Values_ID
	--from marginmgr.PRICESHEET_BASIS_VALUES pbv with (nolock) 
	--inner join epacube.PRODUCT_RECIPE_INGREDIENT_XREF XRef on pbv.Product_Structure_FK = xref.PRODUCT_STRUCTURE_FK
	--inner join epacube.product_recipe_ingredient PRI on XRef.product_recipe_ingredient_XRef_ID = PRI.product_recipe_ingredient_XRef_FK and XRef.chain_id_data_value_fk = PRI.chain_id_data_value_fk
	--inner join epacube.product_recipe PRec on PRI.Product_recipe_fk = PRec.product_recipe_id and PRI.chain_id_data_value_fk = PRec.chain_id_data_value_fk ' + Case when @Temp_Prod_Exists = 1 then '
	--inner join ' + @t_prod + ' t_prod with (nolock) on PRec.Product_Structure_FK = t_prod.product_structure_fk ' else '' end + Case when @Temp_Cust_Exists = 1 then '
	--inner join ' + @t_cust + ' t_cust with (nolock) on pbv.Cust_Entity_Structure_FK = t_cust.Cust_Entity_Structure_FK or (pbv.Cust_Entity_Structure_FK is null and Zone_Entity_Structure_FK = 19195) ' else '' end + Case when @Temp_Vendor_Exists = 1 then '
	--inner join ' + @t_Vendor + ' t_vendor with (nolock) on pbv.Vendor_Entity_Structure_FK = t_vendor.Vendor_Entity_Structure_FK ' else '' end + '
	--inner join #DNames DNames on pbv.data_name_fk = DNames.DN_FK
	--' + @WhereOption + ') A ' + @WhereDRank + '
	--'
	--		End

	--		If @UI_RETURN_SQL = 0 or @UI_DN_Source <> 1
	--		Exec(@SQLpbv)

	--		FETCH NEXT FROM PBVfilters INTO @PBV_DNFK, @WhereOption, @WhereDRank
	--		End--Fetch
				
	--		--If @UI_RETURN_SQL = 0		
	--		--create index idx_pbv on #PBV(data_name_fk, product_structure_fk, cust_entity_structure_fk, vendor_entity_structure_fk, effective_date, Rnk)

	--	Close PBVfilters;
	--	Deallocate PBVfilters;
	
	--End	--end of If isnull(@ind_Get_Basis_Values, 0) > 0 

	Set @KeyColsPSFK =	Case when @Temp_Prod_Exists = 1 and @Prod_Status = 1 then 't_PROD.Product_Structure_FK' + ' ''Product_Structure_FK'', ' else @KeyColsPSFK end
	Set @KeyColsCESFK = Case when @Temp_Cust_Exists = 1 and @Cust_Status = 1 then 't_CUST.CUST_Entity_Structure_FK' + ' ''CUST_Entity_Structure_FK'', ' 
							 when @Temp_Cust_Exists = 0 and @Cust_Status = 1 then 'eic.Entity_Structure_FK' + ' ''CUST_Entity_Structure_FK'', ' else @KeyColsCESFK end
	Set @KeyColsVESFK = Case when @Temp_Vendor_Exists = 1 and @Vendor_status = 1 then 't_VENDOR.VENDOR_Entity_Structure_FK' + ' ''VENDOR_Entity_Structure_FK'', ' 
							 when @Temp_Vendor_Exists = 0 and @Vendor_status = 1 then 'eiv.Entity_Structure_FK' + ' ''VENDOR_Entity_Structure_FK'', ' else @KeyColsVESFK end

	Create Table #CursorDataLevels(Data_Level_CR_FK int Null, Precedence int null)
	Insert into #CursorDataLevels
		Select Data_Level_CR_FK, precedence from (
		Select isnull(R.data_level_cr_fk, 0) Data_Level_CR_FK, isnull(precedence, 0) Precedence from epacube.DATA_NAME_GROUP_REFS R with (nolock)
		inner join #DNs DNs on R.data_name_fk = DNs.Data_Name_FK and isnull(R.data_level_cr_fk, 0) = isnull(DNs.data_level_cr_fk, 0)
		Group by isnull(R.data_level_cr_fk, 0), isnull(R.precedence, 0)
		) A
		Group by Data_Level_CR_FK, precedence
		having ((count(*) > 1 and Min(isnull(Data_level_CR_FK, 0)) <> 0)) or count(*) = 1
		order by isnull(precedence, 0) desc

	If (Select count(*) from #CursorDataLevels where data_level_CR_FK <> 0) > 0
	Begin
		Delete from #DNs where data_level_cr_fk = 0
		Delete from #CursorDataLevels where data_level_cr_fk = 0
	End

	DECLARE Data_Levels Cursor local for
	Select * from #CursorDataLevels
	order by Precedence desc

		OPEN Data_Levels;
		FETCH NEXT FROM Data_Levels INTO @Data_Level_CR_FK, @Precedence
		WHILE @@FETCH_STATUS = 0

		Begin

	Set @KeyColsPSFK = 'NULL ''Product_Structure_FK'', '
	Set @KeyColsCESFK = 'NULL ''Cust_Entity_Structure_FK'', '
	Set @KeyColsVESFK = 'NULL ''Vendor_Entity_Structure_FK'', '
	Set @KeyColsZESFK = 'NULL ''Zone_Entity_Structure_FK'', '
	Set @KeyColsCOMPFK = 'NULL ''COMP_Entity_Structure_FK'', '
	Set @KeyColsASSOCFK = 'NULL ''ASSOC_Entity_Structure_FK'', '
	Set @KeyColsDV = 'NULL ''Data_Value_FK'', '
	Set @KeyColsPRODDV = 'NULL ''Prod_Data_Value_FK'', '
	Set @KeyColsCUSTDV = 'NULL ''CUST_Data_Value_FK'', '
	Set @KeyColsDEPTeDV = 'NULL ''DEPT_Entity_Data_Value_FK'', '
	Set @KeyColsSETTINGSDV = 'NULL ''Settings_Data_Value_FK'', '
	Set @KeyColsDLCRFK = 'NULL ''Data_Level_CR_FK'', '

	Set @Cust_Alias = Case when @From like '%pa_159905%' and @from not like '%eic.%' and @from not like '%t_cust.%' then 'pa_159905' else @Cust_Alias end

	If Object_ID('tempdb..#CursorDNs') is not null
	drop table #CursorDNs

		--Declare @Data_level_CR_FK int
		--Set @Data_level_CR_FK = 0

		Select
		R.Qtbl '@Qtbl', DN.data_name_ID '@DN_FK'
		, R.Alias '@Alias'
		, R.Primary_Column '@Primary_Column', R.Local_Column '@Local_Column', Case when R.Data_Column = '' then ' ' else R.Data_Column end '@Data_Column', R.Alias_DV '@Alias_DV', DN.Name '@DN_Label', R.Entity_Class_CR_FK '@Entity_Class_CR_FK'
		, DNs.seq '@SEQ', R.ind_PS_FK '@ind_PS_FK', R.ind_ES_FK '@ind_ES_FK', R.ind_OES_FK '@ind_OES_FK'
		, Case isnull( R.V_Ref_Data_Name_FK, DN.Data_Name_ID) when 503201 then 0 else R.ind_CES_FK end '@ind_CES_FK'
		, R.ind_VES_FK '@ind_VES_FK'
		, Case isnull( R.V_Ref_Data_Name_FK, DN.Data_Name_ID) when 503201 then 0 else R.ind_ZES_FK end '@ind_ZES_FK'
		, R.ind_COMP_ES_FK '@ind_COMP_ES_FK', R.ind_ASSOC_ES_FK '@ind_ASSOC_ES_FK', R.ind_DL_CR_FK '@ind_DL_CR_FK', R.ind_Prod_DV '@ind_Prod_DV', R.ind_Cust_DV '@ind_Cust_DV', R.ind_Dept_EDV '@ind_Dept_EDV'
		, R.JoinSQL '@EDV_SQL_Parent'
		, R.VirtualActionType '@Virtual_Action_Type', R.VirtualColumnCheck '@Virtual_Column_Check', R.V_Ref_Data_Name_FK '@V_Ref_Data_Name_FK', DNs.NullColumn '@NullColumn'
		, Product_Structure_Column '@Product_Structure_Column', Org_Structure_Column '@Org_Structure_Column'
		, Case isnull( R.V_Ref_Data_Name_FK, DN.Data_Name_ID) when 503201 then Null else Customer_Structure_Column end '@Customer_Structure_Column'
		, Vendor_Structure_Column '@Vendor_Structure_Column'
		, Case isnull( R.V_Ref_Data_Name_FK, DN.Data_Name_ID) when 503201 then Null else Zone_Structure_Column end '@Zone_Structure_Column'
		, isnull(R.ind_Rec_Ingredient_XREF, 0) '@ind_Rec_Ingredient_XREF', ind_CHILD_PS_FK '@ind_CHILD_PS_FK', ind_PARENT_ES_FK '@ind_PARENT_ES_FK'
		, isnull(ExcludeSel, 0) '@ExcludeSel', isnull(DNs.VirtualWhereClause, '') '@VirtualWhereClause', dns.date_type '@Date_Type', r.ind_dv_lookup '@dv_lookup', r.ind_edv_lookup '@edv_lookup', r.Attribute_Parent_DN_FK '@Attribute_Parent_DN_FK'
		into #CursorDNs
		from #DNs DNs
		inner join epacube.data_name DN with (nolock) on DNs.Data_Name_FK = DN.data_Name_ID
		left join EPACUBE.DATA_NAME_GROUP_REFS R with (nolock) on DNs.Data_Name_FK = R.data_name_fk and (isnull(DNs.data_level_CR_FK, 0) = isnull(R.data_level_CR_FK, 0) or r.nullcolumn = 1 or (isnull(r.data_level_cr_fk, 0) = 0 and DNs.Data_Level_CR_FK <> 0 and DNs.NullColumn = 0) or (@Data_level_CR_FK = 0))
		left join #ui_comp_levels ucl with (nolock) on R.data_name_fk = ucl.data_name_fk and r.data_level_cr_fk = ucl.data_level_cr_fk
		where 1 = 1
		and DNs.data_level_cr_fk = @Data_Level_CR_FK
		and isnull(ucl.record_status_cr_fk, 0) <> 2
		and DN.data_name_ID in (select data_name_fk from epacube.data_name_group_refs)
		and R.Entity_Class_CR_FK <> 10101
		Order by DNs.seq

		DECLARE Tbl Cursor local for
		
		--Declare @Temp_Cust_Exists int
		--Set @Temp_Cust_Exists = 0

		Select * 
--		into #t1 
		from #CursorDNs --order by [@qtbl]
			Where [@SEQ] not in (Select [@SEQ] from #CursorDNs where [@Customer_Structure_Column] is not null and (select count(*) from #CursorDNs where [@Entity_Class_CR_FK] = 10104) = 0 and @Temp_Cust_Exists = 0)
			and [@SEQ] not in (Select [@SEQ] from #CursorDNs where [@Customer_Structure_Column] is null and [@Zone_Structure_Column] is Null and [@QTbl] = 'SYNCHRONIZER.[RULES_SALES_PROGRAMS]' and (select count(*) from #CursorDNs where [@Entity_Class_CR_FK] = 10104) = 0 and @Temp_Cust_Exists = 0)
		order by [@seq]
--order by [@DN_FK]

	OPEN Tbl;

	FETCH NEXT FROM Tbl INTO @Qtbl, @DN_FK, @Alias, @Primary_Column, @Local_Column, @Data_Column, @Alias_DV, @DN_Label, @Entity_Class_CR_FK, @SEQ, @ind_PS_FK, @ind_ES_FK, @ind_OES_FK, @ind_CES_FK, @ind_VES_FK, @ind_ZES_FK, @ind_COMP_ES_FK, @ind_ASSOC_ES_FK, @ind_DL_CR_FK, @ind_Prod_DV, @ind_Cust_DV, @ind_Dept_EDV, @EDV_SQL_Parent, @Virtual_Action_Type, @Virtual_Column_Check, @V_Ref_Data_Name_FK, @NullColumn, @Product_Structure_Column, @Org_Structure_Column, @Customer_Structure_Column, @Vendor_Structure_Column, @Zone_Structure_Column, @ind_Rec_Ingredient_XREF, @ind_CHILD_PS_FK, @ind_PARENT_ES_FK, @ExcludeSel, @VirtualWhereClause, @Date_Type, @dv_lookup, @edv_lookup, @Attribute_Parent_DN_FK
	WHILE @@FETCH_STATUS = 0

	Begin

	If (@Entity_Class_CR_FK = 10104 or @From like '%159905%') and @Where not like '%' + @Primary_Cust_Structure_Column + '%'
		Set @Where = @Where + ' and ' + @Cust_Alias + '.' + @Primary_Cust_Structure_Column + ' is not null'
--Test Code 4/30/2018
	--If (@Entity_Class_CR_FK = 10104 or @From like '%Product_Association%') and @Where not like '%' + @Primary_Cust_Structure_Column + '%'
	--	Set @Where = @Where + ' and ' + @Cust_Alias + '.' + @Primary_Cust_Structure_Column + ' is not null'
	
	If @Entity_Class_CR_FK = 10109 and @Where not like '%Product_Structure_fk%'
	Set @Where = @Where + ' and ' + @Prod_Alias + '.Product_Structure_fk is not null' + Case @Zones when 'UNASSIGNED' then ' and pa_159450.entity_structure_fk is null' else '' end
	
	--Set @Where = @Where + @VirtualWhereClause + ' '

	If @NullColumn = 1
	Begin

		Set @Select = @Select + '
			' + 'Null ''' + @DN_Label + ''', '
		Set @From = @From
	End
	Else
	Begin

If @Virtual_Action_Type is not null or @Qtbl in ('PBV', '#PBV', 'marginmgr.[pricesheet_basis_values]')
goto VirtualDNsFrom1

----Get all the @From1 tables and joins

	Set @VENDOR_Alias = Case when @From + @From1 like '%t_VENDOR%' then 't_VENDOR' when @From + @From1 like '%eiv.%' then 'eiv'	else 'SV' end
	Set @Vendor_Entity_Structure_FK = Case when @From + @From1 like '%eiv.%' then 'entity_structure_fk' else 'vendor_entity_structure_fk' end

		Set @Data_level_join =  Case when @Data_Level_CR_FK = 0 or @ind_DL_CR_FK = 0 or @Qtbl = 'EPACUBE.[PRODUCT_ATTRIBUTE]' then '' else ' and ' + @Alias + '.[Data_Level_CR_FK] = ' + @Data_Level_CR_FK + ' ' end
							+	
							Case when @ind_Dept_EDV = 0 then '' When @From like '%502049%' then ' and edv_502049.entity_data_value_id = ' + @Alias + '.[dept_entity_data_value_fk] ' else '' end 
							+
							Case when @Qtbl like '%Segments_Contacts%' then ' and ' + @Alias + '.[entity_class_cr_fk] = ' + @Entity_Class_CR_FK else '' end
							+
							Case when @ind_dept_EDV = 0 then '' when @From + @From1 like '%141130%' then ' and edv_141130.entity_data_value_id = ' + @Alias + '.DEPT_entity_data_value_fk ' else '' end


		Set @From1 = @From1 + Case when @Data_Level_CR_FK in (502, 503) and @From1 not like '%ei_z%' then '
		left join epacube.segments_product sp with (nolock) on ' + @Prod_Alias + '.PRODUCT_STRUCTURE_FK = sp.PRODUCT_STRUCTURE_FK and sp.data_name_fk = 501045
		left join epacube.[epacube].[SEGMENTS_ZONE_ASSOCIATION] sza with (nolock) on sp.data_value_fk = sza.prod_data_value_fk and ' + Case when @From + @From1 not like '%' + @Cust_Alias + '%' then '' else @cust_alias + '.[' + @Primary_Cust_Structure_Column + '] = sza.cust_entity_structure_fk and ' end + 'sza.data_level_cr_fk = 503
		left join epacube.entity_identification ei_z with (nolock) on sza.zone_entity_structure_fk = ei_z.entity_structure_fk ' 
		when @Data_Level_CR_FK = 501 and @From1 not like '%ei_z%' then '
		left join epacube.segments_product sp with (nolock) on ' + @Prod_Alias + '.PRODUCT_STRUCTURE_FK = sp.PRODUCT_STRUCTURE_FK and sp.data_name_fk = 501045
		left join epacube.[epacube].[SEGMENTS_ZONE_ASSOCIATION] sza with (nolock) on sp.data_value_fk = sza.prod_data_value_fk and ' + Case when @From + @From1 not like '%' + @Cust_Alias + '%' then '' else @cust_alias + '.[' + @Primary_Cust_Structure_Column + '] = sza.cust_entity_structure_fk and ' end + 'sza.data_level_cr_fk = 503
		left join epacube.[epacube].[SEGMENTS_ZONE_ASSOCIATION] sza_501 with (nolock) on sza.prod_data_value_fk = sza_501.prod_data_value_fk and sza_501.data_level_cr_fk = 501 and sza.zone_entity_structure_fk = sza_501.zone_entity_structure_fk
		left join epacube.entity_identification ei_z with (nolock) on sza_501.zone_entity_structure_fk = ei_z.entity_structure_fk ' 
		else '' end

		--If (@From1 like '%' + @QTbl + '%' and @From1 like '%' + @Alias + '%') or (@From1 like '%' + @QTbl + '%' and @From1 like '%' + @Alias_DV + '%')
--		Begin
--			Set @From1 = @From1
--		End
--else
--Temp Fix for Advisory Board Demo
		IF @QTbl = 'EPACUBE.[SEGMENTS_VENDOR]' and @Data_Level_CR_FK in (508, 518) and @From + @From1 like '%' + @Cust_Alias + '%' and @From + @From1 like '%' + @Prod_Alias  + '%' --and @Temp_Prod_Exists = 1 and @Temp_Cust_Exists = 1
		Begin

		If @Cust_Alias in ('eic', 'pa_159905')
			Set @Customer_Structure_Column = 'Entity_Structure_FK'
		Set @From1 = @From1 + '
		left join ' + @Qtbl + ' ' + @Alias + ' with (nolock) on ' + @Prod_Alias + '.[product_structure_fk] = ' + @Alias + '.[Product_Structure_FK]
			' + Case when @Data_Level_CR_FK = 508 then ' and ' + @Cust_Alias + '.' + @Customer_Structure_Column + ' = ' + @Alias + '.[Cust_Entity_Structure_FK] ' else '' end + '
		and ' + @Alias + '.Data_Name_FK = ' + @DN_FK + ' and ' + @Alias + '.[Data_Level_CR_FK] = ' + @Data_level_CR_FK
--Select @Cust_Alias, @Customer_Structure_Column, @Alias
		End
Else
-----
		If ltrim(rtrim(@QTbl)) in ('EPACUBE.[SETTINGS_STORE_ATTRIBUTE]')
		Begin
			Set @From1 = @From1 + '
	' + 'Left Join ' + @QTbl + ' ' + @Alias + ' with (nolock) on SS_' + @Attribute_Parent_DN_FK + '.Settings_Store_ID = ' + @Alias + '.Settings_Store_FK and ' + @Alias + '.data_name_fk = ' + @DN_FK

--	Left Join epacube.Settings_Store SS_' + @DN_FK + ' with (nolock) on ' + @Cust_Alias + '.' + Case when @Cust_Alias = 't_cust' then 'Cust_Entity_Structure_FK' else 'entity_structure_fk' end + ' = SS_' + @DN_FK + '.cust_entity_structure_fk and ss_' + @DN_FK + '.Data_Name_FK = ' + @DN_FK + '

--Select @From1 '@From1', @DN_FK '@DN_FK', @Cust_Alias '@Cust_Alias', @Cust_Structure '@Cust_Structure', @QTbl '@QTbl', @Alias '@Alias'
		End
Else

		IF (@ind_PS_FK = 1 or @ind_ES_FK = 1 or @ind_CES_FK = 1 or @ind_VES_FK = 1) and ltrim(rtrim(@QTbl)) 
			not in ('EPACUBE.[SEGMENTS_SETTINGS]', 'EPACUBE.[PRODUCT_ASSOCIATION]', 'MARGINMGR.[PRICESHEET_BASIS_VALUES]', 'EPACUBE.[ENTITY_DATA_VALUE]', 'EPACUBE.[SETTINGS_POS_PROD_STORE_HEADER]', '#PBV', 'EPACUBE.[PRODUCT_RECIPE_ATTRIBUTE]'
					, 'MARGINMGR.[PRICESHEET_RETAILS]', 'MARGINMGR.[PRICESHEET_RETAILS_SPECIAL]')
		and @QTbl not like '%Product_Recipe%'
		and (@ind_Recipe_Ing_Comp = 0 or @QTbl <> 'EPACUBE.[PRODUCT_ATTRIBUTE]')
		and @From + @From1 not like '% ' + @Alias + ' %'

		Begin

			Set @From1 = @From1 + '
		left join ' + @Qtbl + ' ' + @Alias + ' with (nolock) on ' + 
				Case	When @Entity_Class_CR_FK = 10109 or isnull(@Product_Structure_Column, '') = 'Product_Structure_FK' then @Prod_Alias + '.[product_structure_fk] '

						when @Entity_Class_CR_FK = 10104 and @Temp_Cust_Exists = 1 then 't_CUST.[Cust_Entity_Structure_FK] ' 
						When @Entity_Class_CR_FK = 10104 and @Temp_Cust_Exists = 0 and @Auths_Ind = 1 then 'pa_159905.[entity_structure_fk] '
						When @Entity_Class_CR_FK = 10104 and @Temp_Cust_Exists = 0 and @Cust_Status = 1 then 'eic.[Entity_Structure_FK] ' 
						
						when @Entity_Class_CR_FK = 10117 and @Zones = 'UNASSIGNED' then 'cj.entity_structure_fk'
						When @Entity_Class_CR_FK = 10117 and @Temp_ZONE_Exists = 1 and @ZONE_Status = 1 and @Prod_Status = 1 then 'pa_159450.entity_structure_fk '
						When @Entity_Class_CR_FK = 10117 and @Temp_ZONE_Exists = 1 and @ZONE_Status = 1 and @Prod_Status = 0 then 't_zone.zone_entity_structure_fk '
						When @Entity_Class_CR_FK = 10117 and @Temp_ZONE_Exists = 0 and @ZONE_Status = 1 then 'eiz.[Entity_Structure_FK] '

						when @Entity_Class_CR_FK = 10103 and @Temp_Vendor_Exists = 1 then 't_VENDOR.[Vendor_Entity_Structure_FK] ' 
						when @Entity_Class_CR_FK = 10103 and @Vendor_Status = 1 then @VENDOR_Alias + '.' +  @Vendor_Entity_Structure_FK + ' '
				else '' end + ' = ' + @Alias + '.[' + Case	when @Entity_Class_CR_FK = 10109 then 'Product_Structure_FK' 
															when @Entity_Class_CR_FK = 10104 then 
																	Case when @Alias in ('eic', 'pa_159905', 'EI_' + @DN_FK) or @Alias like '%EIN_%' then 'Entity_Structure_FK' else 'Cust_Entity_Structure_FK' end 
															When @Entity_Class_CR_FK = 10103 then @Vendor_Structure_Column 
															When @Entity_Class_CR_FK = 10117 then 'entity_structure_fk' 
															end + '] ' 
				+ Case when @DN_FK in (144010, 144020) then '' else ' and ' + @Alias + '.Data_Name_FK = ' + @DN_FK  end + ' ' 
				+ Case when @DN_FK in (500030) then ' and ' + @Alias + '.global_pack_code = ''E''' else '' end + ' '
--				+ Case when @DN_FK in (500030) then ' and ' + @Alias + '.Precedence = 1 and ' + @Alias + '.global_pack_code = ''E''' else '' end + ' '
--added 10/16/2017
				+ Case when @DN_FK in (500019) then ' and ' + @Alias + '.' + @Customer_Structure_Column  + Case when @Cust_Status = 0 then ' is Null ' else ' = ' + @Cust_Alias + '.' + @Primary_Cust_Structure_Column end else '' end + ' '
------------------
				+  @Data_level_join

				
--Select @From1 '@From1', @Data_Column '@Data_Column', @Alias '@Alias', @Alias_DV '@Alias_DV', @QTbl '@QTbl', @ind_PS_FK '@ind_PS_FK', @ind_ES_FK '@ind_ES_FK', @ind_CES_FK '@ind_CES_FK', @ind_VES_FK '@ind_VES_FK', @Entity_Class_CR_FK '@Entity_Class_CR_FK'
--, @Temp_ZONE_Exists '@Temp_ZONE_Exists' , @ZONE_Status '@ZONE_Status', @Data_level_join '@Data_level_join'
		End
else
		If ltrim(rtrim(@QTbl)) in ('EPACUBE.[SETTINGS_POS_PROD_STORE_HEADER]')
		goto MassivePOSDataFrom1
else
		If ltrim(rtrim(@QTbl)) in ('EPACUBE.[SEGMENTS_SETTINGS]') and isnull(@V_Ref_Data_Name_FK, 0) = 0
		Begin

			Set @SettingsAliasZone = 'sz_' + @DN_FK
			Set @SettingsAliasCust = 'ss_' + @DN_FK

			Set @From1 = @from1 + Case when @Temp_Zone_Exists = 1 and @Zone_Status = 1 then '
			left join 
			(
				select * from (
				Select ss.' + @Data_Column + ', sp.product_structure_fk, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK, case ss.PROD_SEGMENT_DATA_NAME_FK when 110103 then ''Item'' when 501045 then ''SG'' when 501044 then ''Grp'' else dn.label end ''Item_Class_Level'', isnull(dv.value, '''') ''Item_Class_ID'', isnull(dv.[description], '''') ''Item_Class_Description''
				, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
				from epacube.segments_settings ss with (nolock) 
				inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
				' + Case @Zones when 'UNASSIGNED' then '--' else '' end + 'inner join ' + @t_zone + ' t_zone with (nolock) on ss.ZONE_SEGMENT_FK = t_zone.zone_entity_structure_fk
				inner join ' + @t_prod + ' t_prod with (nolock) on sp.PRODUCT_STRUCTURE_FK = t_prod.PRODUCT_STRUCTURE_FK
				left join epacube.data_value dv with (nolock) on ss.PROD_SEGMENT_FK = dv.data_value_id and ss.prod_segment_data_name_fk = dv.data_name_fk
				left join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
				where ss.data_name_fk = ' + @DN_FK + ' and ZONE_SEGMENT_FK is not null and CUST_ENTITY_STRUCTURE_FK is null
				) a where Drank = 1
			)  ' + @SettingsAliasZone + ' on t_prod.product_structure_fk = ' + @SettingsAliasZone + '.product_structure_fk and ' + @SettingsAliasZone + '.ZONE_SEGMENT_FK = ' + Case @Zones when 'UNASSIGNED' then 'cj.entity_structure_fk' else 't_zone.zone_entity_structure_fk' end + '
			'
										when @Temp_Cust_Exists = 1 and @Cust_Status = 1 and @ind_PROD_DV = 1 then  '
			left join 
			(
				select * from (
				Select ss.' + @Data_Column + ', sp.product_structure_fk, ss.cust_segment_fk, ss.SEGMENTS_SETTINGS_ID
				, dense_rank()over(partition by ss.data_name_fk, ss.cust_segment_fk, ss.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
				from epacube.segments_settings ss with (nolock) 
				inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
				inner join ' + @t_cust + ' t_cust with (nolock) on ss.cust_entity_structure_fk = t_cust.cust_entity_structure_fk
				inner join ' + @t_prod + ' t_prod with (nolock) on sp.PRODUCT_STRUCTURE_FK = t_prod.PRODUCT_STRUCTURE_FK
				where ss.data_name_fk = ' + @DN_FK + '
				and cust_data_value_fk is not null
				) a where Drank = 1
			)  ' + @SettingsAliascust + ' on t_prod.product_structure_fk = ' + @SettingsAliasCust + '.product_structure_fk and ' + @SettingsAliasCust + '.cust_segment_fk = t_cust.cust_entity_structure_fk
			'
										when @Temp_Prod_Exists = 1 and @Prod_Status = 1 and @ind_PROD_DV = 1 then '
			

			'
			end

/*

			

			Set @From1 = @From1 + Case when @ind_PROD_DV = 1 and @Data_Level_CR_FK <> 502 then '
		left join epacube.segments_product ' + @SettingsAliasProd + ' with (nolock) on ' + @Prod_Alias + '.Product_Structure_FK = ' + @SettingsAliasProd + '.Product_Structure_FK and ' + @SettingsAliasProd + '.data_name_fk = 501045 ' + '
		left join epacube.segments_settings ' + @Alias + ' with (nolock) on ' + @SettingsAliasProd + '.data_value_FK = ' + @Alias + '.prod_data_value_fk and ' + @Alias + '.Data_Name_FK = ' + @DN_FK + ' ' + @Data_level_join
			when @ind_ces_fk = 1 and @Data_Level_CR_FK <> 502 then '
		left join epacube.segments_settings ' + @Alias + ' with (nolock) on ' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = ' + @Alias + '.cust_entity_structure_FK and ' + @Alias + '.Data_Name_FK = ' + @DN_FK + ' ' + @Data_level_join
			else '' end

			Set @From1 = @From1 + Case when @ind_Prod_DV = 1 and @ind_CES_FK = 1 and @data_level_cr_fk not in (501, 502) then '
		and ' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = ' + @Alias + '.cust_entity_structure_FK ' else '' end

			Set @From1 = @From1 + Case when @ind_ZES_FK = 1 and @Data_Level_CR_FK in (501, 503) then '
		and ' + @Alias + '.Zone_Entity_Structure_FK = sza.zone_entity_structure_FK ' 
				when @ind_ZES_FK = 1 and @Data_Level_CR_FK = 502 then '
		left join epacube.segments_settings ' + @Alias + ' with (nolock) on ' + @Alias + '.zone_entity_structure_fk = ei_z.entity_structure_fk and ' + @Alias + '.Data_Name_FK = ' + @DN_FK + ' and '+ @Alias + '.data_level_cr_fk = 502 '
			else '' end
*/
		End

else
		If ltrim(rtrim(@QTbl)) in ('EPACUBE.[ENTITY_DATA_VALUE]') and isnull(@EDV_SQL_Parent, '') <> ''
		Begin

			Set @From1 = @From1 + '
			' + @EDV_SQL_Parent
		End
else
		IF @Qtbl = 'EPACUBE.[PRODUCT_ASSOCIATION]' and @from + @From1 not like '%' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + '%' and @ind_CHILD_PS_FK = 1 and (@Temp_Zone_Exists = 1 or @Temp_Cust_Exists = 1)
		Begin
			Set @From1 = @From1 + '
		left join epacube.product_association ' + @Alias + ' with (nolock) on ' + @Prod_Alias + '.product_structure_fk = ' + @Alias + '.product_structure_fk and ' + @Alias + '.data_name_fk = ' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + '
			and isnull(' + @Alias + '.entity_structure_fk, 0) = ' + Case when @Temp_Zone_Exists = 1 then 't_zone.zone_entity_structure_fk' when @Temp_Cust_Exists = 1 then '0' end + '
		left join epacube.product_identification ' + replace(@Alias, 'pa_', 'pi_') + ' with (nolock) on ' + @Alias + '.CHILD_PRODUCT_STRUCTURE_FK = ' + replace(@Alias, 'pa_', 'pi_') + '.PRODUCT_STRUCTURE_FK and ' + replace(@Alias, 'pa_', 'pi_') + '.data_name_fk = ' + @Item_Data_Name_FK + '
		left join epacube.product_description ' + replace(@Alias, 'pa_', 'pd_') + ' with (nolock) on ' + @Alias + '.CHILD_PRODUCT_STRUCTURE_FK = ' + replace(@Alias, 'pa_', 'pd_') + '.PRODUCT_STRUCTURE_FK and ' + replace(@Alias, 'pa_', 'pd_') + '.data_name_fk = 110401 '

--SELECT @Alias '@Alias', @Prod_Alias '@Prod_Alias', @V_Ref_Data_Name_FK '@V_Ref_Data_Name_FK', @DN_FK '@DN_FK', @Temp_Zone_Exists '@Temp_Zone_Exists', @Temp_Cust_Exists '@Temp_Cust_Exists', @Item_Data_Name_FK '@Item_Data_Name_FK'

		End
else
		IF @Qtbl = 'EPACUBE.[PRODUCT_ASSOCIATION]' and @from + @From1 not like '%' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + '%' and @ind_CHILD_PS_FK = 1 and @Temp_Zone_Exists = 0 and @Temp_Cust_Exists = 0
		Begin
			Set @From1 = @From1 + '
		left join epacube.product_association ' + @Alias + ' with (nolock) on ' + @Prod_Alias + '.product_structure_fk = ' + @Alias + '.product_structure_fk and ' + @Alias + '.data_name_fk = ' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + '
		left join epacube.product_identification ' + replace(@Alias, 'pa_', 'pi_') + ' with (nolock) on ' + @Alias + '.CHILD_PRODUCT_STRUCTURE_FK = ' + replace(@Alias, 'pa_', 'pi_') + '.PRODUCT_STRUCTURE_FK and ' + replace(@Alias, 'pa_', 'pi_') + '.data_name_fk = ' + @Item_Data_Name_FK + '
		left join epacube.product_description ' + replace(@Alias, 'pa_', 'pd_') + ' with (nolock) on ' + @Alias + '.CHILD_PRODUCT_STRUCTURE_FK = ' + replace(@Alias, 'pa_', 'pd_') + '.PRODUCT_STRUCTURE_FK and ' + replace(@Alias, 'pa_', 'pd_') + '.data_name_fk = 110401 '

--SELECT @From1 '@From1', @Alias '@Alias', @Prod_Alias '@Prod_Alias', @V_Ref_Data_Name_FK '@V_Ref_Data_Name_FK', @DN_FK '@DN_FK', @Temp_Zone_Exists '@Temp_Zone_Exists', @Temp_Cust_Exists '@Temp_Cust_Exists', @Item_Data_Name_FK '@Item_Data_Name_FK', @ind_CHILD_PS_FK '@ind_CHILD_PS_FK'

		End



else
		IF @Qtbl = 'EPACUBE.[PRODUCT_ASSOCIATION]' and @from + @From1 not like '%' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + '%' and @ind_CHILD_PS_FK = 0 and @from + @From1 like '%Segments_Vendor SV %' and @Entity_Class_CR_FK = 10103
		Begin
			Set @From1 = @From1 + '
		left join epacube.product_association ' + @Alias + ' with (nolock) on ' + @Prod_Alias + '.product_structure_fk = ' + @Alias + '.product_structure_fk and ' + @Alias + '.data_name_fk = ' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + '
		and sv.vendor_entity_structure_fk = ' + @Alias + '.entity_structure_fk '
		End
Else
	If @DN_FK = 504033
	set @From1 = @From1 + '
		left join epacube.product_recipe_procedures prp_504033 on pr_503999.product_recipe_id = prp_504033.product_recipe_fk '
else
		If @QTbl like '%PRODUCT_RECIPE%' or (@ind_Recipe_Ing_Comp = 1 and @QTbl like '%Product_Attribute%')
		Begin
			Set @From1 = @From1 + Case	when @ind_Rec_Ingredient_XRef = 0 and @QTbl = 'epacube.[product_recipe_attribute]' then '
		left join epacube.product_recipe_attribute ' + @Alias + ' with (nolock) on pr_503999.product_recipe_id = ' + @Alias + '.product_recipe_fk and ' + @Alias + '.data_name_fk = ' + @DN_FK
										When @ind_Rec_Ingredient_XRef = 1 and @QTbl = 'epacube.[product_recipe_attribute]' then '
		left join epacube.product_recipe_attribute ' + @Alias + ' with (nolock) on pr_503999.product_recipe_id = ' + @Alias + '.product_recipe_fk and PRIX_504030.product_structure_fk = ' + @Alias + '.Product_Structure_FK and '+ @Alias + '.data_name_fk = ' + @DN_FK
										When @ind_Recipe_Ing_Comp = 1 and @QTbl like '%Product_Attribute%' then '
		left join epacube.product_attribute ' + @Alias + ' with (nolock) on PRIX_504030.product_structure_fk = ' + @Alias + '.Product_Structure_FK and '+ @Alias + '.data_name_fk = ' + @DN_FK
								else '' end	
		End
Else
VirtualDNsFrom1:

--Select * from epacube.data_name where name like '%item_type%'
--Select * from epacube.product_attribute where data_name_fk = 500000
--Select * from epacube.product_identification where PRODUCT_STRUCTURE_FK not in (Select PRODUCT_STRUCTURE_FK from epacube.product_attribute where data_name_fk = 500000)

		If ltrim(rtrim(@QTbl)) in ('MARGINMGR.[PRICESHEET_BASIS_VALUES]', '#PBV') and @FromPBV not like '%' + @Alias + '%' and (@Temp_Zone_Exists = 1 or @Temp_Cust_Exists = 1)
		BEGIN
		--Set @PBV = '
		--	(
		--		Select * from (
		--		Select pbv.value, pbv.unit_value, pbv.Effective_Date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, patt.attribute_event_data ''item_type''
		--		, case when patt.attribute_event_data <> ''W'' then pbv.cust_entity_structure_fk end ''Cust_Entity_Structure_FK'' 
		--		, dense_rank()over(partition by pbv.data_name_fk, pbv.product_structure_fk, case when patt.attribute_event_data <> ''W'' then pbv.cust_entity_structure_fk else '''' end order by pbv.effective_date desc, pbv.pricesheet_basis_values_id desc) DRank
		--		from marginmgr.pricesheet_basis_values pbv with (nolock) 
		--		inner join ' + @t_prod + ' t_prod with (nolock) on pbv.PRODUCT_STRUCTURE_FK = t_prod.PRODUCT_STRUCTURE_FK
		--		inner join epacube.product_attribute patt with (nolock) on pbv.product_structure_fk = patt.product_structure_fk and patt.data_name_fk = 500000
		--		where pbv.data_name_fk = ' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + ' and cust_entity_structure_fk is null
		--		' + isnull((Select top 1 [@VirtualWhereClause] from #CursorDNs where isnull([@VirtualWhereClause] , '') <> '' and [@V_Ref_Data_Name_fk] in (@V_Ref_Data_Name_FK, @DN_FK) and isnull([@date_type], '') <> '' order by [@seq]), '') + '
		--		) a where DRank = 1
		--	)  '
--taking out item_type daata
		Set @PBV = '
			(
				Select * from (
				Select pbv.value, pbv.unit_value, pbv.Effective_Date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk
				, dense_rank()over(partition by pbv.data_name_fk, pbv.product_structure_fk order by pbv.effective_date desc, pbv.pricesheet_basis_values_id desc) DRank
				from marginmgr.pricesheet_basis_values pbv with (nolock) 
				inner join ' + @t_prod + ' t_prod with (nolock) on pbv.PRODUCT_STRUCTURE_FK = t_prod.PRODUCT_STRUCTURE_FK
				where pbv.data_name_fk = ' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + ' and cust_entity_structure_fk is null
				' + isnull((Select top 1 [@VirtualWhereClause] from #CursorDNs where isnull([@VirtualWhereClause] , '') <> '' and [@V_Ref_Data_Name_fk] in (@V_Ref_Data_Name_FK, @DN_FK) and isnull([@date_type], '') <> '' order by [@seq]), '') + '
				) a where DRank = 1
			)  '

			If @ind_Recipe_Ing_Comp = 0
				Set @FromPBV = @FromPBV + '
		left join ' + @PBV + ' ' + @Alias + ' on ' + @Alias + '.Data_Name_FK = ' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + '
			' + Case when @Prod_Status = 1 then 'and ' + @Prod_Alias + '.[Product_Structure_FK] = ' + @Alias + '.Product_Structure_FK ' else '' end + '
			' + Case when @Cust_Status = 1 then 'and ' + '
					' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = case when item_type <> ''W'' then PBV_503201.cust_entity_structure_fk else t_cust.Cust_Entity_Structure_FK end'
--					' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = ' + @Alias + '.Cust_Entity_Structure_FK '-- + '
					--' + 'or (' + @Alias + '.Cust_Entity_Structure_FK is null and ' + @Alias + '.Zone_Entity_Structure_FK = 19195) ' + '
					--' + 'or ' + @Alias + '.Cust_Entity_Structure_FK is null ' 
				else '' end + '
			' + Case when @Vendor_Status = 1 then 'and (' + @Vendor_Alias + '.[' + @Vendor_Entity_Structure_FK + '] = ' + @Alias + '.VENDOR_Entity_Structure_FK or ' + @Alias + '.VENDOR_Entity_Structure_FK is null) ' else '' end
			Else
			If @ind_Recipe_Ing_Comp = 1
				Set @FromPBV = @FromPBV + '
		left join ' + @PBV + ' PBV_503201 with (nolock) on PBV_503201.Data_Name_FK = 503201 and PRIX_504030.[Product_Structure_FK] = PBV_503201.Product_Structure_FK and (t_CUST.[cust_entity_structure_fk] = PBV_503201.Cust_Entity_Structure_FK or PBV_503201.Cust_Entity_Structure_FK is null)' -- and PBV_503201.Rnk = 1

	
		END
--Else
--		If ltrim(rtrim(@QTbl)) in ('MARGINMGR.[PRICESHEET_BASIS_VALUES]', '#PBV') and @FromPBV not like '%' + @Alias + '%' and @Temp_Cust_Exists = 1
--		BEGIN
--		Set @PBV = '
--			(
--				Select * from (
--				Select pbv.value, pbv.unit_value, pbv.Effective_Date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk
--				, case when patt.attribute_event_data <> ''W'' or 1 = ' + cast(@Cust_Status as varchar(8)) + ' then pbv.cust_entity_structure_fk end ''Cust_Entity_Structure_FK'' 
--				, dense_rank()over(partition by pbv.data_name_fk, pbv.product_structure_fk, case when patt.attribute_event_data <> ''W'' or 1 = ' + cast(@Cust_Status as varchar(8)) + ' then pbv.cust_entity_structure_fk else '''' end order by pbv.effective_date desc, pbv.update_timestamp desc) DRank
--				from marginmgr.pricesheet_basis_values pbv with (nolock) 
--				inner join ' + @t_prod + ' t_prod with (nolock) on pbv.PRODUCT_STRUCTURE_FK = t_prod.PRODUCT_STRUCTURE_FK
--				inner join epacube.product_attribute patt with (nolock) on pbv.product_structure_fk = patt.product_structure_fk and patt.data_name_fk = 500000
--				where pbv.data_name_fk = ' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + ' and cust_entity_structure_fk is null
--				' + isnull((Select top 1 [@VirtualWhereClause] from #CursorDNs where isnull([@VirtualWhereClause] , '') <> '' and [@V_Ref_Data_Name_fk] in (@V_Ref_Data_Name_FK, @DN_FK) and isnull([@date_type], '') <> '' order by [@seq]), '') + '
--				) a where DRank = 1
--			)  '

--			If @ind_Recipe_Ing_Comp = 0
--				Set @FromPBV = @FromPBV + '
--		left join ' + @PBV + ' ' + @Alias + ' on ' + @Alias + '.Data_Name_FK = ' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + '
--			' + Case when @Prod_Status = 1 then 'and ' + @Prod_Alias + '.[Product_Structure_FK] = ' + @Alias + '.Product_Structure_FK ' else '' end + '
--			' + Case when @Cust_Status = 1 then 'and (' + '
--					' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = ' + @Alias + '.Cust_Entity_Structure_FK ' + '
--					' + 'or (' + @Alias + '.Cust_Entity_Structure_FK is null and ' + @Alias + '.Zone_Entity_Structure_FK = 19195) ' + '
--					' + 'or ' + @Alias + '.Cust_Entity_Structure_FK is null ' else '' end + '
--			' + Case when @Vendor_Status = 1 then 'and (' + @Vendor_Alias + '.[' + @Vendor_Entity_Structure_FK + '] = ' + @Alias + '.VENDOR_Entity_Structure_FK or ' + @Alias + '.VENDOR_Entity_Structure_FK is null) ' else '' end
--			Else
--			If @ind_Recipe_Ing_Comp = 1
--				Set @FromPBV = @FromPBV + '
--		left join ' + @PBV + ' PBV_503201 with (nolock) on PBV_503201.Data_Name_FK = 503201 and PRIX_504030.[Product_Structure_FK] = PBV_503201.Product_Structure_FK and (t_CUST.[cust_entity_structure_fk] = PBV_503201.Cust_Entity_Structure_FK or PBV_503201.Cust_Entity_Structure_FK is null)' -- and PBV_503201.Rnk = 1

--		END
Else
		If ltrim(rtrim(@QTbl)) in ('MARGINMGR.[PRICESHEET_RETAILS]') and @From1 not like '%' + @Alias + '%' and @Cust_Status <> 1
		Begin
		Set @MPR = '
			(
				Select * from (
				Select MPR.PRICE, MPR.product_structure_fk, MPR.zone_entity_structure_fk ''zone_segment_fk'', MPR.cust_entity_structure_fk, MPR.pricesheet_retails_id, MPR.Effective_Date, MPR.data_name_fk, MPR.PRICE_MULTIPLE
				,Dense_Rank()over(partition by MPR.product_structure_fk, MPR.zone_entity_structure_fk , MPR.cust_entity_structure_fk order by MPR.effective_date desc, mpr.pricesheet_retails_id desc) DRank
				from MARGINMGR.[PRICESHEET_RETAILS] MPR with (nolock)
				' + Case when @Temp_Zone_Exists = 0 then '--' else '' end + ' inner join ' + @t_zone + ' t_zone with (nolock) on MPR.zone_entity_structure_fk  = t_zone.zone_entity_structure_fk
				inner join ' + @t_prod + ' t_prod with (nolock) on MPR.PRODUCT_STRUCTURE_FK = t_prod.PRODUCT_STRUCTURE_FK
				where 1 = 1 and MPR.data_name_fk = ' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + '
				and MPR.cust_entity_structure_fk is null
				' + isnull((Select top 1 [@VirtualWhereClause] from #CursorDNs where isnull([@VirtualWhereClause] , '') <> '' and [@V_Ref_Data_Name_fk] in (@V_Ref_Data_Name_FK, @DN_FK) and isnull([@date_type], '') <> '' order by [@seq]), '') + '
				) a where DRank = 1
			) '
			Set @From1 = @From1 + '
		left join ' + @MPR + ' ' + @Alias + ' on ' + @Prod_Alias + '.[Product_Structure_FK] = ' + @Alias + '.Product_Structure_FK  ' + Case when @Zone_Status = 1 then ' and t_zone.ZONE_ENTITY_STRUCTURE_FK = ' + @Alias + '.Zone_Segment_FK' else '' end


		End
Else
		If ltrim(rtrim(@QTbl)) in ('MARGINMGR.[PRICESHEET_RETAILS]') and @From1 not like '%' + @Alias + '%' and @Cust_Status = 1
		Begin
		Set @MPR = '
			(
				Select * from (
				Select MPR.PRICE, MPR.product_structure_fk, MPR.zone_entity_structure_fk ''zone_segment_fk'', MPR.cust_entity_structure_fk, MPR.pricesheet_retails_id, MPR.Effective_Date, MPR.data_name_fk, MPR.PRICE_MULTIPLE
				,Dense_Rank()over(partition by MPR.product_structure_fk, MPR.zone_segment_fk, MPR.cust_entity_structure_fk order by MPR.effective_date desc, mpr.pricesheet_retails_id desc) DRank
				from MARGINMGR.[PRICESHEET_RETAILS] MPR with (nolock)
				inner join ' + @t_cust + ' t_cust with (nolock) on MPR.cust_SEGMENT_FK = t_cust.cust_entity_structure_fk
				inner join ' + @t_prod + ' t_prod with (nolock) on MPR.PRODUCT_STRUCTURE_FK = t_prod.PRODUCT_STRUCTURE_FK
				where 1 = 1 and MPR.data_name_fk = ' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + '
				and MPR.cust_entity_structure_fk is not null
				' + isnull((Select top 1 [@VirtualWhereClause] from #CursorDNs where isnull([@VirtualWhereClause] , '') <> '' and [@V_Ref_Data_Name_fk] in (@V_Ref_Data_Name_FK, @DN_FK) and isnull([@date_type], '') <> '' order by [@seq]), '') + '
				) a where DRank = 1
			) '
			Set @From1 = @From1 + '
		left join ' + @MPR + ' ' + @Alias + ' on ' + @Prod_Alias + '.[Product_Structure_FK] = ' + @Alias + '.Product_Structure_FK  and t_cust.CUST_ENTITY_STRUCTURE_FK = ' + @Alias + '.Cust_Entity_Structure_FK'


		End
Else		
		If ltrim(rtrim(@QTbl)) in ('SYNCHRONIZER.[RULES_SALES_PROGRAMS]') and @From1 not like '%SC_144020%'
		Begin
			Set @AllowanceCalcDate = isnull((Select top 1 OptionValue from #Filters where V_Ref_Data_Name_FK = 503207 and OptionValue is not null), Cast(Cast(getdate() as date) as varchar(64)))
			Set @From1 = @From1 + '

		left join epacube.epacube.segments_customer sc_144020 with (nolock) on ' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = ' + 'sc_144020.Cust_Entity_Structure_FK ' + '
			' + 'and sc_144020.entity_data_name_fk = 144020 ' + '
			' + 'and sc_144020.data_name_fk = 144111 ' + '
		left join epacube.synchronizer.rules_sales_programs rsp with (nolock) on t_prod.PRODUCT_STRUCTURE_FK = rsp.Prod_Segment_FK ' + '
			' + 'and sc_144020.CUST_SEGMENT_FK = rsp.Cust_Segment_FK ' + '
			and rsp.Result_data_name_fk between 503204 and 503206 ' + '
		' + Case	when @FromPBV like '%PBV_503201%' and @date_type = 'effective' then
		'	and rsp.Effective_Date >= PBV_503201.[EFFECTIVE_DATE] ' else '' end + '
		' + Case	when @AllowanceOption in ('on') then 
						Case when @FromPBV like '%PBV_503201%' and @date_type = 'effective' then
		'	and isnull(rsp.End_Date, ''' + @AllowanceCalcDate + ''') >= PBV_503201.[EFFECTIVE_DATE]' else '' end + '
		' + ' and ((''' + @AllowanceCalcDate + ''' between rsp.Effective_Date and isnull(rsp.End_Date, ''' + @AllowanceCalcDate + ''') ' + '
		' + ' and rsp.Result_data_name_fk between 503204 and 503206)
		 or (rsp.Result_data_name_fk between 503204 and 503206 and isnull(rsp.qty_remaining, 0) > 0))'
					when @AllowanceOption in ('after') then
						Case when @FromPBV like '%PBV_503201%' and @date_type = 'effective' then 
		'	and isnull(rsp.End_Date, ''' + @AllowanceCalcDate + ''') >= PBV_503201.[EFFECTIVE_DATE]' else '' end + '
		' + ' and (((''' + @AllowanceCalcDate + ''' <= rsp.End_Date) ' + '
		' + ' and rsp.Result_data_name_fk between 503204 and 503206)
		or (rsp.Result_data_name_fk between 503204 and 503206 and isnull(rsp.qty_remaining, 0) > 0))' 
					when @AllowanceOption in ('before') then 
						Case when @FromPBV like '%PBV_503201%' and @date_type = 'effective' then
		'	and isnull(rsp.End_Date, ''' + @AllowanceCalcDate + ''') >= PBV_503201.[EFFECTIVE_DATE]' else '' end + '
		' + ' and (((''' + @AllowanceCalcDate + ''' >= rsp.effective_Date) ' + '
		' + ' and rsp.Result_data_name_fk between 503204 and 503206)
		or (rsp.Result_data_name_fk between 503204 and 503206 and isnull(rsp.qty_remaining, 0) > 0))' 		
		else '' end
		End
Else
		If ltrim(rtrim(@QTbl)) in ('MARGINMGR.[PRICESHEET_RETAILS_SPECIAL]') and @From1 not like '% PRS.%'
		Begin
			Set @From1 = @From1 + '
		left join epacube.marginmgr.pricesheet_retails_special prs with (nolock) on rsp.RULES_SALES_PROGRAMS_ID = prs.Rules_Sales_Programs_FK ' + '
			' + 'and rsp.Prod_Segment_FK = prs.product_structure_fk '
		End
Else
		if isnull(@Alias_DV, '') <> '' and @from not like '%' + @Alias_DV + '%' --and @Select not like '%' + @Alias_DV + '%'
		Begin

			Set @From1 = @From1 + 
			Case	When @Data_Column like '%Data_Value_FK' and @Data_Column <> 'Entity_Data_Value_FK' then '
		left join epacube.data_value ' + isnull(@Alias_DV, '') + ' with (nolock) on ' + @Alias + '.data_value_fk = ' + isnull(@Alias_DV, '') + '.data_Value_ID'
					When @Data_Column = 'Entity_Data_Value_FK' and isnull(@EDV_SQL_Parent, '') = '' then '
		left join epacube.entity_data_value ' + isnull(@Alias_DV, '') + ' with (nolock) on ' + isnull(@Alias_DV, '') + '.entity_structure_fk = ' + @Cust_Alias + '.' + Case when @Cust_Alias ='t_Cust' then 'Cust_Entity_Structure_FK' else 'entity_structure_fk' end  + ' and ' + @Alias_DV + '.data_name_fk = ' + @DN_FK 
			+ ' and ' + @Alias + '.entity_data_value_fk = ' + @Alias_DV + '.entity_Data_Value_ID'
		else '' end
		End
Else

MassivePOSDataFrom1:
		If ltrim(rtrim(@QTbl)) in ('EPACUBE.[SETTINGS_POS_PROD_STORE_HEADER]')
		Begin
			Set @Alias_SPPSH = @Alias 
			Set @Alias = Replace(@Alias, 'SPPSH_', 'SPPSV_')

			Set @From1 = @From1 + Case when @From + @From1 like '%' + @Cust_Alias + '%' then  '
				left join epacube.settings_pos_Prod_Store_Header ' + @Alias_SPPSH + ' with (nolock) on '+ @Prod_Alias + '.[Product_Structure_FK] = ' + @Alias_SPPSH + '.[Product_Structure_FK] 
				and ' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '] = ' + @Alias_SPPSH + '.[' + @Customer_Structure_Column + '] ' + ' ' + Replace(@Data_level_join, 'SPPSV_', 'SPPSH_') + ' 
				left join epacube.SETTINGS_POS_PROD_STORE_VALUES ' + @Alias + ' with (nolock) on ' + @Alias_SPPSH + '.[Settings_POS_Prod_Store_Header_ID] = ' + @Alias + '.[Settings_POS_Prod_Store_Header_FK] and ' + @Alias + '.[Data_Name_FK] = ' + @DN_FK
				Else '
				left join epacube.settings_pos_Prod_Store_Header ' + @Alias_SPPSH + ' with (nolock) on '+ @Prod_Alias + '.[Product_Structure_FK] = ' + @Alias_SPPSH + '.[Product_Structure_FK] 
				left join epacube.SETTINGS_POS_PROD_STORE_VALUES ' + @Alias + ' with (nolock) on ' + @Alias_SPPSH + '.[Settings_POS_Prod_Store_Header_ID] = ' + @Alias + '.[Settings_POS_Prod_Store_Header_FK] and ' + @Alias + '.[Data_Name_FK] = ' + @DN_FK
				End
		goto MassivePOSDataSelect
		End

----Get Columns for main body - Select Statements

--Select @Select '@Select', @Alias '@Alias', @DN_FK '@DN_FK', @Virtual_Action_Type '@Virtual_Action_Type', @Virtual_Column_Check '@Virtual_Column_Check', @Data_Column '@Data_Column', @ExcludeSel '@ExcludeSel'

		If @Select like '%' + @Alias + '.' + @Data_Column + '%' or @Select like '%' + @Alias_DV + '.' + @Data_Column + '%' or @ExcludeSel = 1
		Begin
			Set @Select = @Select
		End
Else
		IF @Qtbl = 'EPACUBE.[SEGMENTS_SETTINGS]' AND @Temp_Zone_Exists = 1 AND @Zone_Status = 1 and isnull(@Virtual_Action_Type, '') = ''   --and @ind_CHILD_PS_FK = 1
		Begin
			Set @Select = @Select + '
			' + @SettingsAliasZone + '.[' + @Data_Column + '] ''' + @DN_Label + ''', '
		End
Else
		IF @Qtbl = 'EPACUBE.[PRODUCT_ASSOCIATION]' and @ind_CHILD_PS_FK = 1
		Begin
			Set @Select = @Select + replace(@Alias, 'pa_', 'pi_') + '.Value + ' + ''': ''' + ' + ' + replace(@Alias, 'pa_', 'pd_') + '.[Description] ''' + @DN_Label + ''', '
		End
Else
		If (@QTbl like '%PRODUCT_RECIPE%' or (@ind_Recipe_Ing_Comp = 1 and @QTbl like '%Product_Attribute%')) and isnull(@Virtual_Action_Type, '') = ''
		Begin
			Set @Select = @Select + Case when @Primary_Column = 'Data_Value_FK' then @Alias_DV + '.Value' else @Alias + '.[' + @Data_Column + ']' end + ' ''' + @DN_Label + ''', '
		End
Else
		If isnull(@Virtual_Action_Type, '') in ('REFERENCE', 'SQLcode')
		Begin
			Set @Select = @Select + '
			' + @Data_Column + ' ''' + @DN_Label + ''', '
		End
Else
		If isnull(@Virtual_Action_Type, '') <> '' and isnull(@Virtual_Column_Check, '') <> '' and isnull(@Virtual_Column_Check, '') <> isnull(@Data_Column, '')
		Begin

			Set @Select = @Select + '
			' + @Data_Column + ' ''' + @DN_Label + ''', '
		End
Else		
		If @Data_Column not like '%Data_Value_FK%' and (isnull(@Virtual_Action_Type, '') <> 'WhenRecordExists' or isnull(@Virtual_Action_Type, '') = '')
		 and @Qtbl <> 'EPACUBE.[PRODUCT_ASSOCIATION]'
		Begin

			Set @Select = @Select + '
			' + @Alias + '.[' + @Data_Column + '] ''' + @DN_Label + ''', '
		End
Else
		If @Data_Column not like '%Data_Value_FK%' and @Virtual_Action_Type = 'WhenRecordExists'
		Begin
			Set @Select = @Select + 
			Case when @Entity_Class_CR_FK = 10103 then '
			Case when isnull(' + @Alias + '.[' + @Data_Column + '], 0) = 0 then ''N'' else ''Y'' end ''' + @DN_Label + ''', '
			else '
			Case when isnull(' + @Cust_Alias + '.[' + @Primary_Cust_Structure_Column + '], 0) = 0 then ''N'' else ''Y'' end ''' + @DN_Label + ''', '
			end 
		end
else
		if isnull(@Alias_DV, '') <> '' and @from not like '%' + @Alias_DV + '%' --and @ExcludeSel = 0 --and @Select not like '%' + @Alias_DV + '%' 
		Begin
			Set @Select = @Select + '
			' + @Alias_DV + '.[Value] + Case when isnull(' + @Alias_DV + '.[Description], '''') = '''' then '''' else '': '' + isnull(' + @Alias_DV + '.[Description], '''')  end ''' + @DN_Label + ''', '

--Select @DN_FK, @Data_Column, @EDV_SQL_Parent, @From1, @Data_Level_Join, @Alias, @Alias_DV

		End

MassivePOSDataSelect:
		If ltrim(rtrim(@QTbl)) in ('EPACUBE.[SETTINGS_POS_PROD_STORE_HEADER]')
		Begin
			--Set @Alias_SPPSH = @Alias 
			--Set @Alias = Replace(@Alias, 'SPPSH_', 'SPPSV_')

			If @Select not like '%' + @Alias + '.[' + @Data_Column + '] ''' + '%'
				Set @Select = @Select + '
				' + @Alias + '.[' + @Data_Column + '] ''' + @DN_Label + ''', '

		End

	Set @WHERE_IsNotNull = @WHERE_IsNotNull + Case	when @ind_Dept_EDV = 1 then Case when @Alias_SPPSH <> '' then @Alias_SPPSH else @Alias end + '.[DEPT_entity_data_value_fk] is not null or ' 
													--When isnull(@Virtual_Action_Type, '') = '' and isnull(@Alias, '') <> '' and @From + @From1 like '%' + @Alias + '%' then @Alias + '.[' + @Data_Column + '] is not null or '
													else '' end
											--+ Case when @QTbl = '#PBV' and @WHERE_IsNotNull not like '%' + @Alias + '%' then @Alias + '.[value] is not null and ' + @Alias + '.data_name_fk = ' + @V_Ref_Data_Name_FK + ' and ' + @Alias + '.rnk = 1 and ' else '' end

		--If @Temp_Prod_Exists = 1 and @Prod_Status = 1 and @KeyColsPSFK = 'NULL ''Product_Structure_FK'', '
		--	Set @KeyColsPSFK = 't_prod.Product_Structure_FK' + ' ''Product_Structure_FK'', '
		--Else If @ind_PS_FK = 1 and @Prod_Status = 1 and @KeyColsPSFK = 'NULL ''Product_Structure_FK'', '
		--	Set @KeyColsPSFK = @Alias + '.' + @Product_Structure_Column + ' ''Product_Structure_FK'', '

		If @Temp_Prod_Exists = 1 and @Prod_Status = 1 and @KeyColsPSFK like '%NULL ''Product_Structure_FK'', %'
			Set @KeyColsPSFK = 't_prod.Product_Structure_FK' + ' ''Product_Structure_FK'', t_prod.Product_Structure_FK' + ' ''Prod_Segment_FK'','
		Else If @ind_PS_FK = 1 and @Prod_Status = 1 and @KeyColsPSFK like '%NULL ''Product_Structure_FK'', %'
			Set @KeyColsPSFK = @Alias + '.' + @Product_Structure_Column + ' ''Product_Structure_FK'', ' + @Alias + '.' + @Product_Structure_Column + ' ''Product_Segment_FK'', '
	
		If @Temp_Cust_Exists = 1 and @Cust_Status = 1 and @KeyColsCESFK = 'NULL ''Cust_Entity_Structure_FK'', '
			Set @KeyColsCESFK = 't_cust.[Cust_Entity_Structure_FK] ''Cust_Entity_Structure_FK'', '
		Else If @From like '%pa_159905.%' and @KeyColsCESFK = 'NULL ''Cust_Entity_Structure_FK'', '
			Set @KeyColsCESFK = 'pa_159905.[Entity_Structure_FK] ''Cust_Entity_Structure_FK'', '
		Else If @From like '%eic.%' and @KeyColsCESFK = 'NULL ''Cust_Entity_Structure_FK'', '
			Set @KeyColsCESFK = 'eic.[Entity_Structure_FK] ''Cust_Entity_Structure_FK'', '

		If  @KeyColsVESFK = 'NULL ''Vendor_Entity_Structure_FK'', ' and @Vendor_Entity_Structure_FK <> '' and @DN_FK like '143%'
			Set @KeyColsVESFK = @Alias + '.[' + @Vendor_Structure_Column + '] ''Vendor_Entity_Structure_FK'', '

		If @Zones = 'UNASSIGNED'
				Set @KeyColsZESFK = 'cj.[Entity_Structure_FK] ''Zone_Entity_Structure_FK'', '
			else If @Temp_ZONE_Exists = 1 and @Zone_Status = 1 and @Prod_Status = 1 and @KeyColsZESFK = 'NULL ''Zone_Entity_Structure_FK'', '
				Set @KeyColsZESFK = 'pa_159450.[Entity_Structure_FK] ''Zone_Entity_Structure_FK'', '
			else If @Temp_ZONE_Exists = 1 and @Zone_Status = 1 and @Prod_Status = 0 and @KeyColsZESFK = 'NULL ''Zone_Entity_Structure_FK'', '
				Set @KeyColsZESFK = 't_zone.[Zone_Entity_Structure_FK] ''Zone_Entity_Structure_FK'', '
			else if @Temp_ZONE_Exists = 1 and @Zone_Status = 0 and @KeyColsZESFK = 'NULL ''Zone_Entity_Structure_FK'', '
				Set @KeyColsZESFK = '(select top 1 zone_entity_structure_fk from ' + @t_zone + ') ''Zone_Entity_Structure_FK'', '
			else If @Temp_ZONE_Exists = 0 and @Zone_Status = 1 and @KeyColsZESFK = 'NULL ''Zone_Entity_Structure_FK'', '
				Set @KeyColsZESFK = 'eiz.[Entity_Structure_FK] ''Zone_Entity_Structure_FK'', '
			Else If @ind_ZES_FK = 1 and @KeyColsZESFK = 'NULL ''Zone_Entity_Structure_FK'', ' and isnull(@Alias, '') <> '' and @from1 like '%ei_z%'
				Set @KeyColsZESFK = 'ei_z.[Entity_Structure_FK] ''Zone_Entity_Structure_FK'', '

		If @ind_COMP_ES_FK = 1 and @KeyColsCOMPFK = 'NULL ''COMP_Entity_Structure_FK'', '
			Set @KeyColsCOMPFK = @Alias + '.[COMP_Entity_Structure_FK] ''COMP_Entity_Structure_FK'', '

		If @ind_ASSOC_ES_FK = 1 and @KeyColsASSOCFK = 'NULL ''ASSOC_Entity_Structure_FK'', '
			Set @KeyColsASSOCFK = @Alias + '.[ASSOC_Entity_Structure_FK] ''ASSOC_Entity_Structure_FK'', '

		If @ind_PROD_DV = 1 and @KeyColsPRODDV = 'NULL ''Prod_Data_Value_FK'', '
			Set @KeyColsPRODDV = @Alias + '.[Prod_Data_Value_FK] ''Prod_Data_Value_FK'', '

		If @ind_CUST_DV = 1 and @KeyColsCUSTDV = 'NULL ''CUST_Data_Value_FK'', '
			Set @KeyColsCUSTDV = @Alias + '.[CUST_Data_Value_FK] ''CUST_Data_Value_FK'', '

		If @ind_DEPT_eDV = 1 and @KeyColsDEPTeDV = 'NULL ''DEPT_Entity_Data_Value_FK'', '
			Set @KeyColsDEPTeDV = Case when isnull(@Alias_SPPSH, '') <> '' then @Alias_SPPSH else @Alias end + '.[DEPT_Entity_Data_Value_FK] ''DEPT_Entity_Data_Value_FK'', '

		If @Primary_Column = 'Data_Value_FK' and @KeyColsDV = 'NULL ''Data_Value_FK'', '
			Set @KeyColsDV = @Alias + '.[Data_Value_FK] ''Data_Value_FK'', '

		If @QTbl like '%Segments_Settings%' and @KeyColsSETTINGSDV = 'NULL ''Settings_Data_Value_FK'', '
			Set @KeyColsSETTINGSDV = @Alias + '.[Data_Value_FK] ''Settings_Data_Value_FK'', '

		If @ind_DL_CR_FK = 1 and @Data_Level_CR_FK <> 0--and @KeyColsDLCRFK =  'NULL ''Data_Level_CR_FK'', '
			Set @KeyColsDLCRFK = '' + @Data_Level_CR_FK + Case when isnull(@Data_Level_CR_FK, 0) <> 0 then ' Data_Level_CR_FK, (Select code from epacube.code_ref with (nolock) where code_ref_id = ' + @Data_Level_CR_FK + ') ''' else ' Data_Level_CR_FK, ''' end + 'Data Level'', '

	End

		Set @FKs = @FKs + Case	When @ExcludeSel = 1 then ''
								When (@QTbl not like '%Product_Recipe%' and @FKs like '%' + @Alias + '.' + Replace(Replace(@QTbl, 'epacube.[', ''), ']', '') + '_ID ''' + '%') or (@FKs like '%DN_' + @DN_FK + '%') then ''
								When isnull(@Alias, '') = '' then 'NULL DN_' + @DN_FK + ', '
								When @QTbl = 'epacube.[Entity_Data_Value]'-- or @QTbl like '%[Data_Value]%') 
									and isnull(@Alias_DV, '') <> '' then @Alias_DV + '.' + Replace(Replace(@QTbl, 'epacube.[', ''), ']', '') + '_ID ''' +  'DN_' + @DN_FK + ''', '
								When @QTbl = 'epacube.[segments_settings]' and @Temp_Zone_Exists = 1 and @Zone_Status = 1 then @SettingsAliasZone + 
									 '.[' + Replace(Replace(Replace(Replace(@QTbl, 'epacube.[', ''), 'MarginMGR.[', ''), 'Synchronizer.[', ''),  ']', '') + '_ID] ''' +  'DN_' + @DN_FK + ''', ' 
								else @Alias + '.[' + Replace(Replace(Replace(Replace(@QTbl, 'epacube.[', ''), 'MarginMGR.[', ''), 'Synchronizer.[', ''),  ']', '') + '_ID] ''' +  'DN_' + @DN_FK + ''', ' 
						end

		Set @FKs = Replace(Replace(Replace(@FKs, '#PBV', 'Pricesheet_Basis_Values'), '_HEADER_ID', '_HEADER_FK'), 'SPPSH_', 'SPPSV_')

--Select @Select '@Select', @from '@From', @From1 '@From1'

		FETCH NEXT FROM Tbl INTO @Qtbl, @DN_FK, @Alias, @Primary_Column, @Local_Column, @Data_Column, @Alias_DV, @DN_Label, @Entity_Class_CR_FK, @SEQ, @ind_PS_FK, @ind_ES_FK, @ind_OES_FK, @ind_CES_FK, @ind_VES_FK, @ind_ZES_FK, @ind_COMP_ES_FK, @ind_ASSOC_ES_FK, @ind_DL_CR_FK, @ind_Prod_DV, @ind_Cust_DV, @ind_Dept_EDV, @EDV_SQL_Parent, @Virtual_Action_Type, @Virtual_Column_Check, @V_Ref_Data_Name_FK, @NullColumn, @Product_Structure_Column, @Org_Structure_Column, @Customer_Structure_Column, @Vendor_Structure_Column, @Zone_Structure_Column, @ind_Rec_Ingredient_XREF, @ind_CHILD_PS_FK, @ind_PARENT_ES_FK, @ExcludeSel, @VirtualWhereClause, @Date_Type, @dv_lookup, @edv_lookup, @Attribute_Parent_DN_FK

		If @From1 like '%' + @Alias + '.[' + @Primary_Column + ']' + '%' and @Primary_Column like '%Structure%'
		Set @Alias = @Alias + '_1'
	
		End

--Select @Select '@Select', @from '@From', @From1 '@From1'

		Set @KeyColsDV = Case when @KeyColsSETTINGSDV = 'NULL ''Settings_Data_Value_FK'', ' then @KeyColsDV else Replace(@KeyColsSETTINGSDV, '''Settings_Data_Value_FK''', '''Data_Value_FK''') end
		Set @KeyCols = @KeyColsPSFK + @KeyColsCESFK + @KeyColsVESFK + @KeyColsZESFK + @KeyColsCOMPFK + @KeyColsASSOCFK --+ @KeyColsDV + @KeyColsPRODDV + @KeyColsCUSTDV + @KeyColsDEPTeDV + @KeyColsDLCRFK

		Close Tbl;
		Deallocate Tbl;

		Set @Select = Upper(rtrim(ltrim(@Select)))

		Set @Select = Left(@Select, Len(@Select) - 1)

		Set @Select = Right(@Select, Len(@Select) - charindex(' ', @Select))

		Set @FKs = @FKs + Case	When @FKs like '%' + @KeyColsDLCRFK + '%' then '' else @KeyColsDLCRFK end

		Set @KeyCols = upper(isnull(@KeyCols, '') + isnull(@FKs, ''))

		Set @WHERE_IsNotNull = Case when @WHERE_IsNotNull <> '' then '(' + Left(@WHERE_IsNotNull, Len(@WHERE_IsNotNull) - 3) + ')' else '' end

--Select @Select '@Select', @from '@From', @From1 '@From1', @FKs '@FKs', @KeyCols '@KeyCols', @Where '@Where', @WHERE_IsNotNull '@WHERE_IsNotNull'

		--Set @SqlEXEC = '
		--' +	Replace(Replace(('Select ' + Case When @top_n_rows = 0 then '' else 'top ' + @top_n_rows + ' ' end + @KeyCols + @Select + @From + @FromPBV + @From1 + '
		--' + Case	when	@Where <> 'Where 1 = 1 ' and @WHERE_IsNotNull <> '' then
		--					@Where + ' and ' + @WHERE_IsNotNull
		--			When	@WHERE_IsNotNull <> '' then
		--					' Where ' + @WHERE_IsNotNull else @Where end
		--), '  ', ' '), '  ', ' ')

		Set @SqlEXEC = '
		' +	Replace(Replace(('Select ' + @KeyCols + @Select + @From + @FromPBV + @From1 + '
		' + Case	when	@Where <> 'Where 1 = 1 ' and @WHERE_IsNotNull <> '' then
							@Where + ' and ' + @WHERE_IsNotNull
					When	@WHERE_IsNotNull <> '' then
							' Where ' + @WHERE_IsNotNull else @Where end
		), '  ', ' '), '  ', ' ')

	IF @Consolidate_Results = 1 and @UI_RETURN_SQL <> 1
	Begin
		Begin Try

						If Object_ID(@t_results) is null
						Begin
							Set @SQL_Results = 'Select * into ' + @t_results + ' from ('
								+ @SqlEXEC
								+ ') a where 1 = 2'
							exec(@SQL_Results)

								DECLARE NullValues Cursor local for
								Select '[' + column_name + ']', data_type from epacube.INFORMATION_SCHEMA.columns where table_schema + '.' + table_name = '' + @t_Results + '' and (is_nullable <> 'YES' or data_type <> 'varchar')
				
									OPEN NullValues;
									FETCH NEXT FROM NullValues INTO @ColName, @Data_Type
									WHILE @@FETCH_STATUS = 0

									Begin

									Set @CursorSQL = 'Alter Table ' + @t_Results + ' Alter Column ' + @ColName + ' ' + Case when @Data_Type like '%Date%' then @Data_Type else 'Varchar(128)' end + ' Null'

									Exec(@CursorSQL)

									FETCH NEXT FROM NullValues INTO @ColName, @Data_Type

								End

								Close NullValues;
								Deallocate NullValues;

							Set @SQL_Results = 'Insert into ' + @t_results + ' select * from ('
								+ @SqlEXEC
								+ ') a'
							Exec(@SQL_Results)

							goto endoffetch
						End
						Else
						Begin
							Set @SQL_Results = 'Insert into ' + @t_results + ' select * from ('
								+ @SqlEXEC
								+ ') a'
							Exec(@SQL_Results)
						End
		End Try
		Begin Catch
			Print 'Results are not available for this component'
		goto eop
		End Catch
	End

	If @UI_RETURN_SQL <> 0
	Begin
		Set @ReturnSet1 = '
	Declare @UserID Varchar(64) = ' + '''' + @UserID + '''' + '
	Declare @UI_COMP_MASTER_FK Varchar(64) = ' + '''' + cast(@UI_COMP_MASTER_FK as varchar(64)) + '''' + '
	If Object_ID(''tempdb..#PBV'') is not null
	drop table #PBV
	If Object_ID(''tempdb..#DNames'') is not null
	Drop Table #DNames'

		If isnull(@ind_Get_Basis_Values, 0) > 0 
		Begin

			Set @ReturnSet1 = @ReturnSet1 + '
	Select top 1 Cast(Null as int) Rnk
	, [PriceSheetID], [Effective_Date], [End_Date], [Product_Structure_FK], [Org_Entity_Structure_FK], [Cust_Entity_Structure_FK], [Vendor_Entity_Structure_FK], [Zone_Type_CR_FK], [Zone_Entity_Structure_FK], [Data_Name_FK], [Basis_Position], [Value], [UOM_CODE_FK], [Pack], [SPC_UOM_FK], [Calculated_Value], [Promo], [CO_ENTITY_STRUCTURE_FK], Cast(Null as int) ''Record_Status_CR_FK'', Cast(Null as Numeric(18, 4)) ''Unit_Value'', Cast(Null as bigint) PBV_ID
	into #PBV from marginmgr.PRICESHEET_BASIS_VALUES PBV with (nolock) where 1 = 2' + '
	' + '
	create index idx_pbv on #PBV(data_name_fk, product_structure_fk, cust_entity_structure_fk, vendor_entity_structure_fk, effective_date, Rnk)
	
	Create Table #DNames(DN_FK Varchar(64) Null, Data_Name_FK bigint Null, OptionSetting varchar(128) Null, OptionValue varchar(64) Null, Data_Column Varchar(Max) NUll, WhereOption Varchar(Max) Null, WhereDRank varchar(64) Null)
	'

		If @UI_Comp_Master_FK like '%~%'
			Set @ReturnSet1 = @ReturnSet1 + ' 
	' + @SQLDnames

			Else
			Set @ReturnSet1 = @ReturnSet1 + ' 
	' +	Replace(@SQLDnames, '#ui_comp', @DNsource) + ' and ' + @DNsourceID + ' = ' + @UI_COMP_MASTER_FK
		
			Set @ReturnSet1 = @ReturnSet1 + ' 
	' + isnull(@SQLpbv, '')
		End

Set @ReturnSet1 = ''

	Declare @OrderBy Varchar(Max)
	Set @OrderBy = isnull((Select ordering from epacube.ui_comp_master where ui_comp_master_id = @UI_Comp_Master_FK), '')
	Set @OrderBy = Case @OrderBy when '' then '' else 'Order by ' + @OrderBy end

	Set @ReturnSet2 = @ReturnSet2 + '
	' + 'Select * from (' + @SqlEXEC + ') A 
		' + @WhereDataLevel_CR_FK + '
		' + @OrderBy + '
	' + '
	-------**** Filters In Force ****------------
	--Select * from ' + @FiltersInForce + '
	---------------------------------------------
	' 
--Select @SqlEXEC '@SqlEXEC', @WhereDataLevel_CR_FK '@WhereDataLevel_CR_FK', @OrderBy '@OrderBy', @ReturnSet1 '@ReturnSet1', @ReturnSet2 '@ReturnSet2'

		If @UI_RETURN_SQL = 1 
		--	Select @ReturnSet1 + @ReturnSet2 as SQLpreview
		--else
		Begin
			Print @ReturnSet1
			Print @ReturnSet2
		End
 
	goto endoffetch
	End
	
	If @Consolidate_Results <> 1
	Begin Try
		Set @SqlEXEC = Replace(@SqlEXEC, 'Select ', 'Select Distinct ')
		Exec(@SqlEXEC)
	End Try

	Begin Catch

		If object_id('tempdb..#Vars') is not null
		drop table #Vars

		select Cast(@Auths_Ind as varchar(64)) '@Auths_Ind'
		,  Cast(@Auths_Join as varchar(64)) '@Auths_Join'
		,  Cast(@Data_level_join as varchar(64)) '@Data_level_join'
		,  Cast(@Temp_Prod_Exists as varchar(64)) '@Temp_Prod_Exists'
		,  Cast(@t_prod as varchar(64)) '@t_prod'
		,  Cast(@Prod_Status as varchar(64)) '@Prod_Status'
		,  Cast(@Prod_Alias as varchar(64)) '@Prod_Alias'
		,  Cast(@Temp_Cust_Exists as varchar(64)) '@Temp_Cust_Exists'
		,  Cast(@t_cust as varchar(64)) '@t_cust'
		,  Cast(@Cust_Status as varchar(64)) '@Cust_Status'
		,  Cast(@Cust_Alias as varchar(64)) '@Cust_Alias'
		,  Cast(@Primary_Cust_Structure_Column as varchar(64)) '@Primary_Cust_Structure_Column'
		,  Cast(@Cust_Structure_Table_In_Use as varchar(64)) '@Cust_Structure_Table_In_Use'
		,  Cast(@Temp_Vendor_Exists as varchar(64)) '@Temp_Vendor_Exists'
		,  Cast(@t_vendor as varchar(64)) '@t_vendor'
		,  Cast(@Vendor_Status as varchar(64)) '@Vendor_Status'
		,  Cast(@Vendor_Alias as varchar(64)) '@Vendor_Alias'
		,  Cast(@Vendor_Entity_Structure_FK as varchar(64)) '@Vendor_Entity_Structure_FK'
		into #Vars

		Select * from #Vars

		select @SqlEXEC

	End Catch

endoffetch:

	Set @Select = 'Select '
	Set @From1 = ''
	Set @KeyCols = ''
	Set @FKs = ''
	Set @Seq = 0
	Set @WHERE_IsNotNull = ''
	Set @Alias_SPPSH = ''

	FETCH NEXT FROM Data_Levels INTO @Data_Level_CR_FK, @Precedence

End

Close Data_Levels;
Deallocate Data_Levels;

Begin Try
	If @Consolidate_Results = 1 and @UI_RETURN_SQL <> 1
	Begin

		If (Select count(*) from epacube.INFORMATION_SCHEMA.columns where table_schema = 'TempTbls' and table_name = replace(@t_results, 'TempTbls.', '') and column_name = 'Data Level') = 0
		--Or @UI_COMP_Master_FK = 21336
		Begin
			--If exists(Select * from epacube.INFORMATION_SCHEMA.columns where table_name = @t_results and column_name = 'Data Level_cr_fk')
			--Exec('Alter table ' + @t_results + ' drop column [data_level_cr_fk]')

			If exists(Select * from epacube.INFORMATION_SCHEMA.columns where table_name = @t_results and column_name = 'Data Level')
			Exec('Update ' + @t_results + ' Set [Data Level] = Null, [data_level_cr_fk] = Null')
		End

		Exec [epacube].[UI_Component_Consolidate] @t_results

		If (Select count(*) from epacube.INFORMATION_SCHEMA.columns where table_schema = 'TempTbls' and table_name = replace(@t_results, 'TempTbls.', '')) > 1
		Begin
			If Object_id(@t_results + '_1') is not null
			Exec('Drop table ' + @t_results + '_1')

			Exec('Select distinct * into ' + @t_results + '_1 from ' + @t_results)
			Exec('Truncate Table ' + @t_results)
			
			If isnull(@WhereDataLevel_CR_FK, '') <> ''
				Exec('Insert into ' + @t_results + ' select * from ' + @t_results + '_1' + ' ' + @WhereDataLevel_CR_FK)
				else
				Exec('Insert into ' + @t_results + ' select * from ' + @t_results + '_1')
			Exec('drop table ' + @t_results + '_1')
		End
		
		Exec('Alter Table ' + @t_Results + ' Add RESULTS_ID int identity(1, 1) Not Null')
		--Exec('Select distinct * from ' + @t_results)

		set @t_results = Replace(@t_results, 'TempTbls.', '')

		exec('
		Select dn.data_name_id, isc.column_name
		--, case when dn.data_name_id is null or isc.column_name in (''Data_Level_CR_FK'', ''ROW_STATUS'', ''RESULTS_ID'') then 0 else 1 end ''Display_Column''
		, case when isc.column_name like ''%_fk'' or isc.column_name in (''Data_Level_CR_FK'', ''ROW_STATUS'', ''RESULTS_ID'', ''ind_discontinued'', ''V_UPC_PRECEDENCE'') then 0 else 1 end ''Display_Column''
		, isnull(ucm.is_control, isnull(uc.editable, 0)) ''editable''
		, Null ''reference_data_name_fk''
		, Null ''reference_column''
		, Null ''stored_proc''
		, Null ''ui_config''
				, case	when isc.column_name = ''DESCRIPTION_LINE_1'' then ''[{"condition":"var1!=null","var1":"ind_discontinued","color":"red","text-decoration":"underline"}]'' 
				else null end ''ui_config_conditional''
		from INFORMATION_SCHEMA.COLUMNS isc with (nolock)
		left join epacube.data_name dn with (nolock) on isc.column_name = dn.name
		left join epacube.UI_COMP uc with (nolock) on dn.data_name_id = uc.data_name_fk and uc.ui_comp_master_fk = ' + @UI_Comp_Master_FK + '
		left join epacube.ui_comp_master ucm with (nolock) on uc.ui_comp_master_fk = ucm.ui_comp_master_id
		where isc.table_schema = ''TempTbls'' and isc.table_name = ''' + @t_results + '''
		and isc.column_name not like ''DN_%''
		order by isc.ordinal_position')
	End
End Try
Begin Catch
	Print 'Results are not available for this component: Duplicate columns May be the issue.'
End Catch

eop:
Declare @top_n_rows int = isnull((select top_n_rows from epacube.UI_COMP_MASTER where ui_comp_master_id = @UI_Comp_Master_FK), 5000) 

Set @top_n_rows = Case when @top_n_rows > 10000 then 10000 else @top_n_rows end

If @t_results not like 'TempTbls.%'
	Set @t_results = 'TempTbls.' + @t_results
		
		If object_id('tempdb..#RowCnt') is not null
		drop table #RowCnt

		Create table #RowCnt(Records Int Not Null)
				
		Declare @RowCnt varchar(Max)
		Declare @RecordCount int

		Set @RowCnt = 'Insert into #RowCnt select count(1) ''Records'' from ' + @t_results
		Exec(@RowCnt)

		Set @RecordCount = (select isnull(records, 0) from #RowCnt)

		If @RecordCount > @top_n_rows
			exec [epacube].[UI_Component_Exec_Top_Rows] @t_results, @RecordCount, @top_n_rows

----exec('select * from ' + @t_results)

