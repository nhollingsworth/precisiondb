﻿






CREATE PROCEDURE [epacube].[UTIL_REMOVE_EVENTS_81_82_83] 
        @IN_JOB_FK BIGINT
       
    AS
BEGIN     



---------- EVENT DATA

delete from synchronizer.event_data_errors 
where event_fk in (
select event_id from synchronizer.event_data
where import_job_fk = @in_job_fk
and event_status_cr_fk in (83,82,81))


delete from synchronizer.event_data_rules
where event_fk in (
select event_id from synchronizer.event_data
where import_job_fk = @in_job_fk
and event_status_cr_fk in (83,82,81))

delete from synchronizer.event_data
where import_job_fk = @in_job_fk
and event_status_cr_fk in (83,82,81)




END













