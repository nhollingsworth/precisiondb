﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Delete taxonomy node JIRA (PRE-160)
-- Use a recursive CTE to get the node and all of its children
-- Load the node ids to a temp table and delete from all appropriate tables
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[TAXONOMY_NODE_DELETE]
	@taxonomy_node_id INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @child_node_count VARCHAR(10);
	DECLARE @product_count VARCHAR(10);
	DECLARE @taxonomy_node_name VARCHAR(256);
	DECLARE @data_value_id BIGINT;


	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.TAXONOMY_NODE_DELETE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	SELECT @taxonomy_node_name = DV.[VALUE]
		, @data_value_id = DV.DATA_VALUE_ID
	FROM epacube.TAXONOMY_NODE TN
		INNER JOIN epacube.DATA_VALUE DV
			ON TN.DATA_VALUE_FK = DV.DATA_VALUE_ID
	WHERE TN.TAXONOMY_NODE_ID = @taxonomy_node_id;

	BEGIN TRY
		
		DROP TABLE IF EXISTS #TAX_NODES_TO_DELETE;
			
		--Get node and its children
		WITH CTE_TAX_FAMILY AS (
			SELECT TAXONOMY_NODE_ID
			FROM epacube.TAXONOMY_NODE
			WHERE TAXONOMY_NODE_ID = @taxonomy_node_id
			UNION ALL
			SELECT TN.TAXONOMY_NODE_ID
			FROM epacube.TAXONOMY_NODE TN
				INNER JOIN CTE_TAX_FAMILY CTETF 
					ON CTETF.TAXONOMY_NODE_ID = TN.PARENT_TAXONOMY_NODE_FK
		)
			
		SELECT TAXONOMY_NODE_ID
		INTO #TAX_NODES_TO_DELETE
		FROM CTE_TAX_FAMILY 

		-- Get child node count
		SELECT @child_node_count = COUNT(TAXONOMY_NODE_ID)
		FROM #TAX_NODES_TO_DELETE
		WHERE TAXONOMY_NODE_ID <> @taxonomy_node_id

		-- Get product count
		SELECT @product_count = COUNT(TAXONOMY_PRODUCT_ID)
		FROM epacube.TAXONOMY_PRODUCT TP
			INNER JOIN #TAX_NODES_TO_DELETE TNTD
				ON TP.TAXONOMY_NODE_FK = TNTD.TAXONOMY_NODE_ID

		IF (@child_node_count > 0 OR @product_count > 0)
			SELECT 'There are ' + @child_node_count + ' child nodes and ' + @product_count + ' associated products that need to be removed before this node can be deleted' AS USER_MSG
				, 0 SUCCESS;

		ELSE

			BEGIN TRANSACTION; 
				
				-- Delete taxonomy node attribute data values
				DELETE 
				FROM epacube.TAXONOMY_NODE_ATTRIBUTE_DV
				WHERE TAXONOMY_NODE_ATTRIBUTE_FK IN (
					SELECT TAXONOMY_NODE_ATTRIBUTE_ID
					FROM epacube.TAXONOMY_NODE_ATTRIBUTE
					WHERE TAXONOMY_NODE_FK = @taxonomy_node_id)
				
				-- Delete taxonomy node attributes
				DELETE 
				FROM epacube.TAXONOMY_NODE_ATTRIBUTE
				WHERE TAXONOMY_NODE_FK = @taxonomy_node_id
				
				-- Delete taxonomy node
				DELETE
				FROM epacube.TAXONOMY_NODE
				WHERE TAXONOMY_NODE_ID = @taxonomy_node_id

			COMMIT TRANSACTION;

			SELECT 'Taxonomy node ' + @taxonomy_node_name + ' successfully deleted' USER_MSG, 1 SUCCESS;
		
		SET @ls_stmt = 'Finished execution of epacube.TAXONOMY_NODE_DELETE.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 

		SET @ls_stmt = 'Execution of epacube.TAXONOMY_NODE_DELETE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END
