﻿






CREATE PROCEDURE [epacube].[UTIL_DELETE_PRICESHEETS] 
        @IN_PRODUCT_FILE_ID Varchar(64) = NULL
       ,@IN_UCC Varchar(64) = NULL
       ,@IN_MFR_SHORTNAME VARCHAR(64) = NULL
       ,@IN_PRICE_LINE VARCHAR(64) = NULL
       ,@IN_ORG VARCHAR(64) 
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        07/11/2011  Initial SQL Version
-- CV        07/28/2011  Add Product Id as paramater to pass
 
 
  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        
 DECLARE @V_ORG_ENTITY_STRUCTURE_FK  Int
 
 
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of epacube.UTL_DELETE_PRICESHEETS. '
                + 'UCC = '
                + isnull(@IN_UCC, 'NULL') 
                + ' Shortname = '
                + isNull(@IN_MFR_SHORTNAME, 'NULL')
                + ' PriceLine = '
                + isnull(@IN_PRICE_LINE, 'NULL')
                + '  For WHSE/TERR = '
                + @IN_ORG 

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
 

                
                
                

---------------------------------------------------------------------------------------
----CREATE TEMP TABLE FOR PRODUCT LIST-----
---------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb.. #TS_REMOVE_PROD_ASSOC') is not null
	   drop table  #TS_REMOVE_PROD_ASSOC;


-- create temporary tables and indexes

CREATE TABLE #TS_REMOVE_PROD_ASSOC 
 ([PRODUCT_STRUCTURE_FK]  int Not NULL)
 
 
 ------------------------------------------------------
-------if user passes the Product id
-------------------------------------------------------

insert into #TS_REMOVE_PROD_ASSOC
(PRODUCT_STRUCTURE_FK)
(select pi.product_structure_fk from epacube.PRODUCT_IDENTIFICATION pi with (nolock)
where pi.DATA_NAME_FK = 310101
and pi.VALUE = @IN_PRODUCT_FILE_ID)

------------------------------------------------------
-------if user passes the ucc
-------------------------------------------------------

insert into #TS_REMOVE_PROD_ASSOC
(PRODUCT_STRUCTURE_FK)
(select pa.product_structure_fk from epacube.PRODUCT_ATTRIBUTE pa with (nolock)
inner join epacube.data_value dv with (nolock)
on (pa.data_value_fk = dv.DATA_VALUE_ID
and dv.DATA_NAME_FK in (813906,823904)
and dv.VALUE = @IN_UCC))

--insert into #TS_REMOVE_PROD_ASSOC
--(PRODUCT_STRUCTURE_FK)
--(select pi.product_structure_fk from epacube.PRODUCT_identification pi with (nolock)
--where  pi.DATA_NAME_FK = 110105
--and pi.VALUE = @IN_UCC)

------insert into #TS_REMOVE_PROD_ASSOC
------(PRODUCT_STRUCTURE_FK)
------(select pc.product_structure_fk from epacube.PRODUCT_CATEGORY pc with (nolock)
------inner join epacube.data_value dv with (nolock)
------on (pc.data_value_fk = dv.DATA_VALUE_ID
------and dv.DATA_NAME_FK = 110908
------and dv.VALUE = @IN_UCC))

------WHICH UCC DATA NAME?
------110105	EAN UCC13
------110908	PRODUCT UCC
------113902	MFR UCC
------813906	IDW MFRr UCC  ----- michelle verified
------820805	TS MFR REGISTERED UCC FLAG
------823904	TS MFR UCC   ----- michelle verified


----------------------------------------------------------
-----if user passes the Mfr short name
----------------------------------------------------------
insert into #TS_REMOVE_PROD_ASSOC
(PRODUCT_STRUCTURE_FK)
(select distinct
product_structure_fk 
from epacube.PRODUCT_ATTRIBUTE 
where ATTRIBUTE_EVENT_DATA = @IN_MFR_SHORTNAME
and DATA_NAME_FK = 10000006)

---send to michelle
------------WHAT SHORT NAME ARE WE PASSING?
----------823903	TS PFMS MFR SHORTNAME
----------843201	TS MFR SHORTNAME
----------10000006	VEND SHORT NAME  ---------verifed from michelle
----------10000132	IDW2 SELLER SHORT NAME


--------------------------------------------------------------------------------
---if user passes the price line
-----------------------------------------------------------------------------------

-----select to get products
insert into #TS_REMOVE_PROD_ASSOC
(PRODUCT_STRUCTURE_FK)
(select pc.product_structure_fk from epacube.PRODUCT_CATEGORY pc with (nolock)
inner join epacube.data_value dv with (nolock)
on (pc.data_value_fk = dv.DATA_VALUE_ID
and dv.DATA_NAME_FK = 310903
and dv.VALUE = @IN_PRICE_LINE))

--310903	ECL PRICE LINE   --- verified by michelle
--10000005	VEND PRICE LINE
---------------------------------------------------------
----CREATE INDEX
------------------------------------------------------------

CREATE CLUSTERED INDEX IDX_TSPA_TSPSFK ON #TS_REMOVE_PROD_ASSOC
(PRODUCT_STRUCTURE_FK ASC)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

------------------------------------------------------
----- select to get org
------------------------------------------------------


	set @V_ORG_ENTITY_STRUCTURE_FK = (select entity_structure_fk 
									from epacube.entity_identification with (nolock)
									where DATA_NAME_fk = 141111
									and ENTITY_DATA_NAME_FK = 141000
									and value = @IN_ORG)


------------------------------------------------------------------------------------------------------------------
---Once we have the product_structure and the org_entity_structure
--- remove these entries from the following tables.
------------------------------------------------------------------------------------------------------------------


DELETE
FROM EPACUBE.PRODUCT_CATEGORY
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			
			   SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_category table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

DELETE
FROM EPACUBE.PRODUCT_ATTRIBUTE
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			 
			 SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_attribute table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


DELETE
FROM EPACUBE.PRODUCT_TEXT
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			
			 SET @l_rows_processed = @@ROWCOUNT
                   
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_text table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

DELETE
FROM EPACUBE.PRODUCT_UOM_CLASS
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			 
			 SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_Uom_Class table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

DELETE
FROM EPACUBE.PRODUCT_DESCRIPTION
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			
			 SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_description table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END



DELETE
FROM EPACUBE.PRODUCT_VALUES
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			 
			 SET @l_rows_processed = @@ROWCOUNT
                   
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_values table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
                        
                        
DELETE
FROM MARGINMGR.PRICESHEET   -----Historical price sheet stuff
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			 
			 SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from pricesheet table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

DELETE
FROM marginmgr.SHEET_RESULTS_VALUES_FUTURE
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			
			 SET @l_rows_processed = @@ROWCOUNT
                   
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_results table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
			
			
DELETE
FROM EPACUBE.PRODUCT_ASSOCIATION
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			AND DATA_NAME_FK = 159100
			
			 SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_associations table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
                        
-----------REMOVE ANY PENDING EVENTS FOR THE PRODUCT /ORG COMBINTAION
--first remove the errors

                    
DELETE
FROM synchronizer.EVENT_DATA_ERRORS
WHERE EVENT_FK in (SELECT EVENT_ID FROM synchronizer.EVENT_DATA 
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			AND EVENT_STATUS_CR_FK = 80)
			
			
			 SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from Event Errors table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


DELETE 
FROM synchronizer.EVENT_DATA_RULES where EVENT_FK in 
(select event_id FROM synchronizer.EVENT_DATA
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			AND EVENT_STATUS_CR_FK = 80)
			
                        
DELETE
FROM synchronizer.EVENT_DATA
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			AND EVENT_STATUS_CR_FK = 80
			
			 SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from Event Data table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END




-------------------------------------------------------------------------------------------------------------

  SET @status_desc = 'finished execution of SYNCHRONIZER.epacube.UTL_DELETE_PRICESHEETS'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.UTL_DELETE_PRICESHEETS has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END














