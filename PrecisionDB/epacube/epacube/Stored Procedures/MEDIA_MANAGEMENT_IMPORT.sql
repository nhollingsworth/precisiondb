﻿-- =============================================
-- Author:		Neal Hollingsworth
-- Create date: 4/15/2020
-- Description:	Import media management data
-- =============================================
CREATE   PROCEDURE [epacube].[MEDIA_MANAGEMENT_IMPORT]
	@in_job_fk BIGINT
AS
BEGIN
	DECLARE @image_data_value_id INT;
	DECLARE @document_data_value_id INT;
	DECLARE @video_data_value_id INT;
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	SET @ls_stmt = 'Started execution of epacube.MEDIA_MANAGEMENT_IMPORT.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT,
		@epa_job_id = @in_job_fk;

	SELECT @image_data_value_id = DATA_VALUE_ID FROM epacube.DATA_VALUE WHERE DATA_NAME_FK = 400100 AND VALUE = 'Image';
	SELECT @document_data_value_id = DATA_VALUE_ID FROM epacube.DATA_VALUE WHERE DATA_NAME_FK = 400100 AND VALUE = 'Document';
	SELECT @video_data_value_id = DATA_VALUE_ID FROM epacube.DATA_VALUE WHERE DATA_NAME_FK = 400100 AND VALUE = 'Video';

	BEGIN try
		-- insert new image records
		INSERT INTO epacube.MEDIA_MANAGEMENT(DATA_VALUE_FK, URL, FILE_NAME)
		SELECT @image_data_value_id
			, mi.URL
			, mi.FILE_NAME
		FROM stage.MEDIA_IMAGES mi
			left join epacube.MEDIA_MANAGEMENT mm
				on mi.URL = mm.URL
		WHERE mm.URL is null

		-- insert new document records
		INSERT INTO epacube.MEDIA_MANAGEMENT(DATA_VALUE_FK, URL, FILE_NAME)
		SELECT @document_data_value_id
			, md.URL
			, md.FILE_NAME
		FROM stage.MEDIA_DOCUMENTS md
			left join epacube.MEDIA_MANAGEMENT mm
				on md.URL = mm.URL
		WHERE mm.URL is null

		-- insert new video records
		INSERT INTO epacube.MEDIA_MANAGEMENT(DATA_VALUE_FK, URL, FILE_NAME)
		SELECT @video_data_value_id
			, mv.URL
			, mv.FILE_NAME
		FROM stage.MEDIA_VIDEOS mv
			left join epacube.MEDIA_MANAGEMENT mm
				on mv.URL = mm.URL
		WHERE mm.URL is null


		  DECLARE @V_END_TIMESTAMP       DATETIME
		   DECLARE @l_sysdate			 DATETIME

		   SET @V_END_TIMESTAMP = getdate()

      exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                0, 0, 0,
								201, @l_sysdate, @V_END_TIMESTAMP;



		SET @ls_stmt = 'Finished execution of epacube.MEDIA_MANAGEMENT_IMPORT.';
		EXEC exec_monitor.Report_Status_sp @exec_id = @l_exec_no, @status_desc = @ls_stmt, @epa_job_id = @in_job_fk;
	END TRY

	BEGIN CATCH

		SET @ls_stmt = 'Execution of epacube.MEDIA_MANAGEMENT_IMPORT has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @exec_id = @l_exec_no, @status_desc = @ls_stmt, @epa_job_id = @in_job_fk;

		EXEC exec_monitor.Report_Error_sp @exec_id = @l_exec_no, @epa_job_id = @in_job_fk;

	END CATCH
END