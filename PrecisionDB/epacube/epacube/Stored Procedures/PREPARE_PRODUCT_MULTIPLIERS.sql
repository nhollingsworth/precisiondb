﻿






-- Copyright 2013 epaCUBE, Inc.
--
-- Procedure created by Robert Gannon
--
--
-- 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        02/13/2008  EPA-3552. Initial SQL Version




-------------------------------------------------------------------
-- This procedure is somewhat customer-specific. It does not take
-- into account associations for Product Identification, Category
-- or Description per customer spec. These tables are explicitly joined with the 
-- Rules Filters tables. 
--
-- The rest of the qualifying rules are (will be) determined based on dynamic sql generation
-- excluding Product Ident, Category, and Description
--
-- The expectation is that this procedure will be run at night, and populate a table
-- with the product and the multipliers, as trying to do this as part of a query was nowhere
-- close to performant, even with only the Ident, Category (twice; once for data values, once for entity data values)
-- and Description tables.
--
-------------------------------------------------------------------



CREATE PROCEDURE [epacube].[PREPARE_PRODUCT_MULTIPLIERS]
AS
BEGIN

        DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @job_timestamp DATETIME
		DECLARE @StdCostDataNameId INT
		DECLARE @ReplCostDataNameId INT
		DECLARE @BasePriceDataNameId INT
		DECLARE @active int
		DECLARE @ruleID bigint
		DECLARE @multiplier numeric(18,6)
		DECLARE @appscope int
		DECLARE @product_structure_fk bigint
		
		

        BEGIN TRY
            SET NOCOUNT ON ;
            SET @l_exec_no = 0
            SET @l_sysdate = GETDATE()			

			set @active = (select epacube.getCodeRefId('RECORD_STATUS','ACTIVE'))
						
			
			SET @appscope = (select application_scope_id
							 from epacube.application_scope with (NOLOCK)
							 where scope = 'APPLICATION')

			SET @StdCostDataNameId = (select epacube.getDataNameId('SX STD'))
			
			SET @ReplCostDataNameId = (select epacube.getDataNameId('SX REPL COST'))

			SET @BasePriceDataNameId = (select epacube.getDataNameId('SX BASE'))			
							
            SET @ls_stmt = 'started execution of epacube.PREPARE_PRODUCT_MULTIPLIERS.'                

            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;
                
            ---First, Zap everything
			TRUNCATE TABLE epacube.product_multipliers


Insert into epacube.product_multipliers(PRODUCT_STRUCTURE_FK, REPL_COST_MULTIPLIER, BASE_PRICE_MULTIPLIER, STD_COST_MULTIPLIER, UPDATE_TIMESTAMP)
select product_structure_id,
epacube.getProductMultiplier(product_structure_id,@ReplCostDataNameId),
epacube.getProductMultiplier(product_structure_id,@BasePriceDataNameId),
epacube.getProductMultiplier(product_structure_id,@StdCostDataNameId),
getdate()
from epacube.product_structure

	
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

            SET @status_desc = 'finished execution of epacube.PREPARE_PRODUCT_MULTIPLIERS'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;

        END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of epacube.PREPARE_PRODUCT_MULTIPLIERS has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH
		  
    END


