﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create new taxonomy tree JIRA (URM-1209)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[TAXONOMY_TREE_DELETE]
	@taxonomy_tree_id BIGINT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.TAXONOMY_TREE_DELETE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		BEGIN TRANSACTION; 

			DELETE
			FROM epacube.TAXONOMY_PRODUCT
			WHERE TAXONOMY_NODE_FK IN (
				SELECT TAXONOMY_NODE_ID
				FROM epacube.TAXONOMY_NODE
				WHERE TAXONOMY_TREE_FK = @taxonomy_tree_id)
			
			DELETE
			FROM epacube.TAXONOMY_NODE
			WHERE TAXONOMY_TREE_FK = @taxonomy_tree_id;
		
			DELETE
			FROM epacube.TAXONOMY_TREE
			WHERE TAXONOMY_TREE_ID = @taxonomy_tree_id; 

		COMMIT TRANSACTION;
		
		SET @ls_stmt = 'Finished execution of epacube.TAXONOMY_TREE_DELETE.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 

		SET @ls_stmt = 'Execution of epacube.TAXONOMY_TREE_DELETE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END
