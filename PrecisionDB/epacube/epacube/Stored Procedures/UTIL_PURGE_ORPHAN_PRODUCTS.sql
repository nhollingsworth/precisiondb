﻿

/* Copyright 2009
--
-- Procedure created by Anonymous
--
--
-- Purpose: Called as part of a nightly routine, from nightly purge procedure,
			 to remove all "orphan" products.
--  An orphan product is a essentially a neglected product structure id with nothing in any 
	associated tables, ie the user clicked the plus button but didn't mean to, etc.
	
	This procedure defines a table variable and uses it to store all the products with a status 
	of Setup and no foreign key references. Then all product structure id's fitting this criteria
	are deleted.

-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
--	RG		 07/14/2009	 EPA-2444; updating for V5.X
--  RG	     08/20/2009  EPA-2444; switching code reference/dangling events.
--- CV       05/16/2014  Found that we are purging product structures that are NEWLY Created and data in resolver.
						--adding to purge after so many days
--  RG		 03/23/2015  Adding Product Multipliers

*/

CREATE OR ALTER PROCEDURE [epacube].[UTIL_PURGE_ORPHAN_PRODUCTS] 
    AS
BEGIN


DECLARE @archivePeriod int
DECLARE @archive_date DATETIME
DECLARE @defaultArchivePeriod int

SET @defaultArchivePeriod = 30 

SET @archivePeriod = CAST(isnull((select CAST ( value as int ) from epacube.epacube_params with (NOLOCK)
										  where NAME = 'ARCHIVE_PERIOD'),@defaultArchivePeriod) AS VARCHAR(20))
SET @archive_date = DATEADD(day, -@archivePeriod, getdate()		)						  

										 
-- Parameterize code ref id
DECLARE @SetupCodeRefID int;
set @SetupCodeRefID = epacube.getCodeRefId('PRODUCT_STATUS','SETUP')


-- table variable to store products we will purge; only one column and set should
-- be small enough to not warrant a temp table
DECLARE @OrphanProducts TABLE
(ProductStructureID bigint)

-- populate Orphan Products table with all products set to setup that have no foreign key 
-- references
insert into @OrphanProducts
			(ProductStructureID)
	select DISTINCT ps.product_structure_id 
	from epacube.product_structure ps 
	where ps.product_status_cr_fk = @SetupCodeRefID
	AND utilities.TRUNC(ps.CREATE_TIMESTAMP) <=  utilities.TRUNC(@archive_date)		 
	AND   ps.PRODUCT_STRUCTURE_ID NOT IN
      (
  	   SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_association
        ON ( CHILD_PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 	
	
	   SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_association
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 	
	   
	   SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_ATTRIBUTE
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 
	
	   SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_category 
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 
	
       SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_description
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 
	

       SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_identification
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 

       SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_ident_MULT
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 

	   SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_multipliers
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 
       
       SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_ident_NONUNIQUE
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 

		SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_mult_type
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL                                      

       SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE
       INNER JOIN epacube.product_STATUS
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 

       SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_TAX_ATTRIBUTE
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 
		
		SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_text
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 		

         SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.PRODUCT_UOM_PACK
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID  
	   UNION ALL

       SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.PRODUCT_UOM_SIZE
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 
		
		SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_uom_class
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 

		SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN epacube.product_values
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
       UNION ALL 

--		SELECT product_structure_fk
--       FROM  EPACUBE.PRODUCT_STRUCTURE 
--       INNER JOIN marginmgr.mm_calc_analysis
--        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
--       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
--       UNION ALL 
--
--		SELECT product_structure_fk
--       FROM  EPACUBE.PRODUCT_STRUCTURE 
--       INNER JOIN marginmgr.mm_calc_cust_stats
--        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
--       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
--       UNION ALL 
--
--		SELECT product_structure_fk
--       FROM  EPACUBE.PRODUCT_STRUCTURE 
--       INNER JOIN marginmgr.mm_calc_stats
--        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
--       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
--       UNION ALL 
--
--		SELECT product_structure_fk
--       FROM  EPACUBE.PRODUCT_STRUCTURE 
--       INNER JOIN marginmgr.mm_inventory_data
--        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
--       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
--       UNION ALL 
--
--		SELECT product_structure_fk
--       FROM  EPACUBE.PRODUCT_STRUCTURE 
--       INNER JOIN marginmgr.mm_product_movement
--        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
--       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
--       UNION ALL 		
--	
--		SELECT product_structure_fk
--       FROM  EPACUBE.PRODUCT_STRUCTURE 
--       INNER JOIN marginmgr.mm_sales_summary
--        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
--       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
--       UNION ALL 
--
--		SELECT product_structure_fk
--       FROM  EPACUBE.PRODUCT_STRUCTURE 
--       INNER JOIN marginmgr.mm_watchlist
--        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
--       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
--       UNION ALL 
--
--		SELECT product_structure_fk
--       FROM  EPACUBE.PRODUCT_STRUCTURE 
--       INNER JOIN marginmgr.sheet_exclusion
--        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
--       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
--       UNION ALL 
--
--		SELECT product_structure_fk
--       FROM  EPACUBE.PRODUCT_STRUCTURE 
--       INNER JOIN marginmgr.sheet_historical
--        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
--       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
--       UNION ALL 

       SELECT product_structure_fk
       FROM  EPACUBE.PRODUCT_STRUCTURE 
       INNER JOIN marginmgr.SHEET_RESULTS_VALUES_FUTURE
        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
      

--		SELECT product_structure_fk
--       FROM  EPACUBE.PRODUCT_STRUCTURE 
--       INNER JOIN stage.stg_mm_host_sales
--        ON ( PRODUCT_STRUCTURE_FK = PRODUCT_STRUCTURE_ID )
--       WHERE PRODUCT_STATUS_CR_FK = @SetupCodeRefID         
--       UNION ALL 
		

       
   )
   
---Define a table variable for the creation events and any 
-- event history to be deleted

DECLARE @OrphanProductEvents TABLE
(EventID bigint)

INSERT INTO @OrphanProductEvents
(EventID)
select event_id from synchronizer.event_data
where product_structure_fk in (select ProductStructureID from @OrphanProducts)

   
------------------------------
-- Delete Any Event References. There was probably an event(s) created when the 
-- orphan product was created. Do not want bad FK references.
	  
      DELETE SYNCHRONIZER.EVENT_DATA_ERRORS
	  WHERE event_fk in (select EventID from @OrphanProductEvents)

	  DELETE SYNCHRONIZER.EVENT_DATA_RULES
	  WHERE event_fk in (select EventID from @OrphanProductEvents)

	  DELETE SYNCHRONIZER.EVENT_DATA_HISTORY
	  WHERE event_id in (select EventID from @OrphanProductEvents)
	  
	  DELETE SYNCHRONIZER.EVENT_DATA_HISTORY_HEADER
	  WHERE event_fk in (select EventID from @OrphanProductEvents)
	  	  
	  DELETE SYNCHRONIZER.EVENT_DATA_HISTORY_DETAILS
	  WHERE event_id in (select EventID from @OrphanProductEvents)

	  DELETE SYNCHRONIZER.EVENT_DATA
	  WHERE event_id in (select EventID from @OrphanProductEvents)

-- Finally, delete all product structure ids that have been determined to be orphans

      DELETE epacube.product_structure
      WHERE product_structure_id in (select ProductStructureID from @OrphanProducts)

END

