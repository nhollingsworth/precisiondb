﻿-- =============================================
-- Author:		<Gary Stone>
-- Create date: <November 17, 2012>
-- Description:	<Dynamic eMail Notification>
-- =============================================
CREATE PROCEDURE [epacube].[EMAIL_DYNAMIC_FORMATTED_CALL]
	@Recipients Varchar(Max), @subject Varchar(Max), @Hdr Varchar(Max), @SubSelect Varchar(Max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
IF object_id('tempdb..##email') is not null
Drop Table ##email	

Declare @tableHTML nvarchar(max)
Declare @SQLcd Varchar(Max)
Set @SQLcd = 'Select * into ##email from ' + @SubSelect + 'A '
Exec(@SQLcd)

Declare @CN as Varchar(64)
Declare @ColsLbls as Varchar(MAX)
Declare @ColsTD as Varchar(MAX)

Set @ColsLbls = ''
Set @ColsTD = ''
Set @CN = ''
Set @tableHTML = ''

DECLARE  HTMLtbl cursor local for
			select COLUMN_NAME from tempdb.INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '##email' order by ORDINAL_POSITION

 OPEN HTMLtbl;
        FETCH NEXT FROM HTMLtbl INTO @CN

WHILE @@FETCH_STATUS = 0 
         BEGIN

	Set @ColsLbls	=	@ColsLbls + '<th>' + @CN + '</th>'
	Set @ColsTD		=	@ColsTD	 + 'td = ' + @CN + ', '''','

	FETCH NEXT FROM HTMLtbl INTO @CN
 END 

WHILE @@FETCH_STATUS = 0 
     CLOSE HTMLtbl;
	 DEALLOCATE HTMLtbl; 

Set @ColsTD = LEFT(@ColsTD, Len(@ColsTD) - 1)

Set @tableHTML = 
	'N''<H1>' + @Hdr +
	N'<table border = "1">' +
	N'<tr>' + @ColsLbls + '</tr>''' + ' + ' +
	'CAST( (Select	' + @ColsTD + '
					from ##email
	for XML Path(''tr''), Type
	) as NVarchar(Max)) +
	N''</table>'

Declare @Stmt Varchar(Max)
Set @Stmt =
'
Declare @Recipients Varchar(Max)
Declare @body Varchar(Max)
Declare @subject Varchar(Max)
Declare @tableHTML Varchar(Max)
Declare @from_address Varchar(Max)

Set @Recipients = ''' + @Recipients + '''
Set @subject = ''' + @subject + '''
Set @tableHTML = ' + @tableHTML + '''

EXEC msdb.dbo.sp_send_dbmail  
	@recipients = @recipients,
	@body = @tableHTML,
	@body_format = ''HTML'',
	@query_result_header = 1,
	@subject = @subject;'
	
EXEC(@Stmt)
	
IF object_id('tempdb..##email') is not null
Drop Table ##email	

END
