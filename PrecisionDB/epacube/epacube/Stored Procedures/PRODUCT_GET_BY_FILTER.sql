﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get product information JIRA (URM-1186)
-- Get all product information for given filter and return in JSON format
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
	--CV	09/23/2020	 working on logging

CREATE OR ALTER PROCEDURE [epacube].[PRODUCT_GET_BY_FILTER] 
	@product_id VARCHAR(128),
	@product_id_non_unique VARCHAR(256),
	@product_description VARCHAR(256)
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.PRODUCT_GET_BY_FILTER. Parameters: @product_id = ' + @product_id
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		DECLARE @field_list AS TABLE (DATA_NAME_ID INT, [NAME] VARCHAR(64), [LABEL] VARCHAR(64));
		DECLARE @dynamic_fields AS VARCHAR(MAX);
			
		INSERT INTO @field_list
		SELECT DATA_NAME_ID
			, dn.[NAME]
			, [LABEL]
		FROM epacube.data_name dn
			INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
			INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
		WHERE dn.RECORD_STATUS_CR_FK = 1
			AND cr.CODE = 'PRODUCT'
			AND dn.USER_EDITABLE = 1

		SELECT @dynamic_fields = COALESCE(@dynamic_fields + ', ', '') + QUOTENAME([name])
		FROM @field_list

		CREATE TABLE #FILTERED_PRODUCTS (
			PRODUCT_STRUCTURE_ID BIGINT PRIMARY KEY
		);

		IF @product_id IS NOT NULL
			BEGIN
				INSERT INTO #FILTERED_PRODUCTS (PRODUCT_STRUCTURE_ID)
				SELECT DISTINCT PRODUCT_STRUCTURE_FK
				FROM epacube.PRODUCT_IDENTIFICATION pid
					LEFT JOIN #FILTERED_PRODUCTS fp
						ON pid.PRODUCT_STRUCTURE_FK = fp.PRODUCT_STRUCTURE_ID
				WHERE fp.PRODUCT_STRUCTURE_ID IS NULL
					AND [VALUE] LIKE '%' + @product_id + '%'
			END
		
		IF @product_id_non_unique IS NOT NULL
			BEGIN
				INSERT INTO #FILTERED_PRODUCTS (PRODUCT_STRUCTURE_ID)
				SELECT DISTINCT ps.PRODUCT_STRUCTURE_ID
				FROM epacube.PRODUCT_STRUCTURE ps
					LEFT JOIN #FILTERED_PRODUCTS fp
						ON ps.PRODUCT_STRUCTURE_ID = fp.PRODUCT_STRUCTURE_ID
				WHERE ps.PRODUCT_STRUCTURE_ID IN (
					SELECT PRODUCT_STRUCTURE_FK
					FROM epacube.PRODUCT_IDENTIFICATION 
					WHERE [VALUE] LIKE '%' + @product_id_non_unique + '%'
					UNION
					SELECT PRODUCT_STRUCTURE_FK
					FROM epacube.PRODUCT_IDENT_NONUNIQUE
					WHERE [VALUE] LIKE '%' + @product_id_non_unique + '%'
					UNION
					SELECT PRODUCT_STRUCTURE_FK
					FROM epacube.PRODUCT_ATTRIBUTE
					WHERE ATTRIBUTE_CHAR LIKE '%' + @product_id_non_unique + '%'
						AND DATA_NAME_FK = (
							SELECT DATA_NAME_ID 
							FROM epacube.DATA_NAME
							WHERE NAME = 'VENDOR PRODUCT NUMBER'))
					AND fp.PRODUCT_STRUCTURE_ID IS NULL
			END

		IF @product_description IS NOT NULL
			BEGIN
				INSERT INTO #FILTERED_PRODUCTS (PRODUCT_STRUCTURE_ID)
				SELECT DISTINCT PRODUCT_STRUCTURE_FK
				FROM epacube.PRODUCT_DESCRIPTION pd
					LEFT JOIN #FILTERED_PRODUCTS fp
						ON pd.PRODUCT_STRUCTURE_FK = fp.PRODUCT_STRUCTURE_ID
				WHERE fp.PRODUCT_STRUCTURE_ID IS NULL
					AND [DESCRIPTION] LIKE '%' + @product_description + '%'
			END

		SELECT *
		INTO #Product
		FROM (
			SELECT pp.PRODUCT_STRUCTURE_ID, dn.[NAME], pid.[VALUE]
			FROM epacube.DATA_NAME dn
				INNER JOIN epacube.PRODUCT_IDENTIFICATION pid ON dn.DATA_NAME_ID = pid.DATA_NAME_FK
				INNER JOIN #FILTERED_PRODUCTS pp ON pid.PRODUCT_STRUCTURE_FK = pp.PRODUCT_STRUCTURE_ID
				INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
				INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
			WHERE dn.RECORD_STATUS_CR_FK = 1
				AND cr.CODE = 'PRODUCT'
			UNION
			SELECT pp.PRODUCT_STRUCTURE_ID, dn.[NAME], pin.[DESCRIPTION]
			FROM epacube.DATA_NAME dn
				INNER JOIN epacube.PRODUCT_DESCRIPTION pin ON dn.DATA_NAME_ID = pin.DATA_NAME_FK
				INNER JOIN #FILTERED_PRODUCTS pp ON pin.PRODUCT_STRUCTURE_FK = pp.PRODUCT_STRUCTURE_ID
				INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
				INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
			WHERE dn.RECORD_STATUS_CR_FK = 1
				AND cr.CODE = 'PRODUCT') AS product;
 
		--Build the Dynamic Pivot Table Query
		DECLARE @table_structure AS VARCHAR(MAX);

		IF OBJECT_ID('tempdb.dbo.Product_Pivot', 'U') IS NOT NULL
			DROP TABLE tempdb.dbo.Product_Pivot; 

		SET @table_structure = 'SELECT ' + @dynamic_fields  + ', PRODUCT_STRUCTURE_ID epaCUBE_ID' +
		' INTO tempdb.dbo.Product_Pivot FROM #Product x PIVOT ( MAX(VALUE) FOR [NAME] in (' + @dynamic_fields + ') ) p ';
		
		EXECUTE(@table_structure);

		SELECT (
				SELECT *
				FROM @field_list
				FOR JSON PATH
			) [fields]
			, (
				SELECT *
				FROM (
					SELECT *
					FROM tempdb.dbo.Product_Pivot) p
				FOR JSON PATH
			) [data]
		FOR JSON PATH;

		SET @ls_stmt = 'Finished execution of epacube.PRODUCT_GET_BY_FILTER.';
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		SET @ls_stmt = 'Execution of epacube.PRODUCT_GET_BY_FILTER has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

	END CATCH
END


