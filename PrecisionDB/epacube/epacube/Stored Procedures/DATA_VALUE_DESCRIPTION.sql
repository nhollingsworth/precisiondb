﻿




-- Copyright 2006
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to modify the description for a data value
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG		 11/15/2007	 EPA-427/Initial SQL Version
-- RG		 11/18/2013  EPA-3520 turned off uppercase for Taxonomy DNs

CREATE PROCEDURE [epacube].[DATA_VALUE_DESCRIPTION] 
( 
  @in_type_flag smallint, -- 1 = Entity Data Value, 0 = Data Value
  @in_data_value_id bigint,
  @in_description varchar (128),  
  @in_update_user varchar(64) )
												
AS

/***************  Parameter Defintions  **********************
CURRENTLY USED IN THE DATA MASTER EDITOR....
***************************************************************/

BEGIN

DECLARE @out_entity_structure_fk bigint
DECLARE @MyTableVar				 table(out_entity_structure_fk bigint)
DECLARE @Cannot_delete_msg		 varchar (100)
declare @l_sysdate				 datetime
DECLARE @ErrorMessage			 nvarchar(4000)
DECLARE @ErrorSeverity			 int
DECLARE @ErrorState				 int

SET NOCOUNT ON;
SET @l_sysdate = getdate ()
set @ErrorMessage = null

-------------------------------------------------------------------------------------
--Ensure relevant keys/flags were passed
-------------------------------------------------------------------------------------
 IF  @in_data_value_id IS NULL 
     BEGIN
		SET @ErrorMessage = 'Data Value ID is NULL'
        GOTO RAISE_ERROR
	 END

 IF  @in_type_flag IS NULL 
     BEGIN
		SET @ErrorMessage = 'Type Flag is NULL'
        GOTO RAISE_ERROR
	 END

	IF @in_type_flag in (1) --ENTITY DATA VALUE
	BEGIN

-------------------------------------------------------------------------------------
--   Perform validation on passed data value id
-------------------------------------------------------------------------------------
	IF  @in_data_value_id not in (select entity_data_value_id 
							   from epacube.entity_data_value) 
	  BEGIN
			SET @ErrorMessage = 'Entity Data Value ID is unknown'
		    GOTO RAISE_ERROR
	  END

-------------------------------------------------------------------------------------
--	Set up table variable of Taxonomy-specific Data Names to govern uppercase conversion
-------------------------------------------------------------------------------------
	DECLARE @TaxonomyDataNames TABLE
	(
	TAXONOMY_DNIDS int
	)

	INSERT INTO @TaxonomyDataNames
	select 
	data_name_id
	from epacube.data_name
	where (data_set_fk in (select data_set_id 
						   from epacube.data_set
						   where table_name = 'PRODUCT_TAX_ATTRIBUTE')					 
		   or					 
		   (name like 'TAXONOMY NODE%') )



-------------------------------------------------------------------------------------
--   Perform ENTITY DATA VALUE EDIT, with taxonomy checks for uppercase
-------------------------------------------------------------------------------------

		--For Entity Data Values that are not Taxonomy Data Names
		UPDATE epacube.ENTITY_DATA_VALUE
			SET DESCRIPTION = upper(@in_description)
			   ,UPDATE_USER = @in_update_user
			   ,UPDATE_TIMESTAMP = @l_sysdate
			WHERE ENTITY_DATA_VALUE_ID = @in_data_value_id
			and DATA_NAME_FK not in (select TAXONOMY_DNIDS from @TaxonomyDataNames)

		--For Entity Data Values that are Taxonomy Data Names
		UPDATE epacube.ENTITY_DATA_VALUE
			SET DESCRIPTION = @in_description
			   ,UPDATE_USER = @in_update_user
			   ,UPDATE_TIMESTAMP = @l_sysdate
			WHERE ENTITY_DATA_VALUE_ID = @in_data_value_id
			and DATA_NAME_FK in (select TAXONOMY_DNIDS from @TaxonomyDataNames)
     END --END ENTITY DATA VALUE UPDATE 


	IF @in_type_flag in (0) --DATA VALUE
	BEGIN

-------------------------------------------------------------------------------------
--   Perform validation on passed data value id
-------------------------------------------------------------------------------------
		IF  @in_data_value_id not in (select data_value_id 
						    		   from epacube.data_value) 
		BEGIN
			SET @ErrorMessage = 'Data Value ID is unknown'
		    GOTO RAISE_ERROR
		END
-------------------------------------------------------------------------------------
--   Perform DATA VALUE EDIT, with taxonomy checks for uppercase
-------------------------------------------------------------------------------------

		--For Data Values that are not Taxonomy Data Names
		UPDATE epacube.DATA_VALUE
			SET DESCRIPTION = upper(@in_description)
			   ,UPDATE_USER = @in_update_user
		       ,UPDATE_TIMESTAMP = @l_sysdate
		WHERE DATA_VALUE_ID = @in_data_value_id
		and DATA_NAME_FK not in (select TAXONOMY_DNIDS from @TaxonomyDataNames)

		--For Data Values that are Taxonomy Data Names
		UPDATE epacube.DATA_VALUE
			SET DESCRIPTION = @in_description
			   ,UPDATE_USER = @in_update_user
		       ,UPDATE_TIMESTAMP = @l_sysdate
		WHERE DATA_VALUE_ID = @in_data_value_id
		and DATA_NAME_FK in (select TAXONOMY_DNIDS from @TaxonomyDataNames)

	 END --End entity data value edit

RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END --End procedure




































