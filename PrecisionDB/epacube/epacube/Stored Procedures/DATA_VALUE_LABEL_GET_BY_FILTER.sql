﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get data value types JIRA (URM-1201)
-- Get all data value tpe information and return in JSON format
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE OR ALTER PROCEDURE [epacube].[DATA_VALUE_LABEL_GET_BY_FILTER]
	@data_value_type INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	BEGIN TRY

		--Initialize log
		SET @ls_stmt = 'Started execution of epacube.DATA_VALUE_LABEL_GET_BY_FILTER.'
		EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
			@exec_id = @l_exec_no OUTPUT;

		SELECT DISTINCT dn.DATA_NAME_ID
			, dn.LABEL DATA_VALUE_LABEL
		FROM epacube.DATA_NAME dn
			INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
			INNER JOIN epacube.DATA_VALUE dv ON dn.DATA_NAME_ID = dv.DATA_NAME_FK
		WHERE ds.DATA_SET_ID = @data_value_type
		FOR JSON PATH;

		SET @ls_stmt = 'Finished execution of epacube.DATA_VALUE_LABEL_GET_BY_FILTER.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH
		SET @ls_stmt = 'Execution of epacube.DATA_VALUE_LABEL_GET_BY_FILTER has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END
