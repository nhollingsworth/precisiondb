﻿




-- Copyright 2014
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Provides report of User Database Backup and Index Rebuild
-- Maintenance Plan executions in the past week. 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        01/16/2014   Initial SQL Version

CREATE PROCEDURE [epacube].[EMAIL_MAINTENANCE_PLAN_REPORT] 
--(  )
												
AS

/***************  Parameter Defintions  **********************
CURRENTLY USED IN THE DATA MASTER EDITOR....

***************************************************************/

BEGIN

DECLARE @l_sysdate				datetime
DECLARE @ls_stmt				varchar (1000)
DECLARE @l_exec_no				bigint
DECLARE @l_rows_processed		bigint
DECLARE @v_count				bigint
DECLARE @v_rule_data_name_fk    bigint
DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int

SET NOCOUNT ON;
SET @l_sysdate = getdate ()
set @ErrorMessage = null

---------Fetch Customer name for use in Subject
DECLARE @customerName varchar(128)
set @customerName = (select VALUE from epacube.epacube_params
where name = 'EPACUBE CUSTOMER')

---Create Email Subject based on customer name
DECLARE @emailSubject varchar(256);
set @emailSubject = @customerName + ' Weekly Maintenance Plan Report';

---Fetch recipients from operator
DECLARE @reportRecipients varchar(MAX);
set @reportRecipients = (select email_address from msdb.dbo.sysoperators where name = 'epaCube Support')

--Create a table for results
declare @tableHTML NVARCHAR(MAX);

--Store query as variable with formatting
set @tableHTML = N'<H1>' + @customerName+ ' Maintenance Plan Report</H1>' +
N'<table border="1">' +
N'<tr><th style="font-size:20px;background-color:#CCCCFF;"><b>Job Name</b></th><th width="80px" style="font-size:20px;background-color:#CCCCFF;"><b>Job Date</b></th><th style="font-size:20px;background-color:#CCCCFF;"><b>Message</b></th></tr>' +
CAST ((select td = substring(jobs.name,1,charindex('.', jobs.name)), '',
			  td = (substring(cast(jobHistory.run_date as varchar(8)),5,2) + '-' +
					substring(cast(jobHistory.run_date as varchar(8)),7,2) + '-' +			  
					substring(cast(jobHistory.run_date as varchar(8)),1,4) 
				   ) , '',			  
			  td = substring(message,1,charindex('.', message))
			  from msdb.dbo.sysjobhistory jobHistory
			  inner join msdb.dbo.sysjobs jobs on (jobHistory.job_id = jobs.job_id)
			  where jobHistory.step_name = '(Job outcome)'
			  and (jobs.name like '%purge%'
						or
					jobs.name like '%backup%')				   				  
			  and jobs.name <> 'syspolicy_purge_history'
			  and jobs.name not like '%system%'
			   and( convert(datetime, substring(cast(run_date as varchar(9)), 3, 7), 120)			  
					> dateadd(dd,-7,getdate())    
--			  and (DATEFROMPARTS(
--substring(cast(jobHistory.run_date as varchar(8)),1,4) --Year
--   ,substring(cast(jobHistory.run_date as varchar(8)),5,2) --Month
--   ,substring(cast(jobHistory.run_date as varchar(8)),7,2) --Day
--) > dateadd(dd,-7,getdate())
--)
)
 FOR XML PATH('tr'), TYPE
)  AS NVARCHAR(MAX))  +
N'</table>';


-----Call email stored proc
exec msdb.dbo.sp_send_dbmail
 @recipients = @reportRecipients
,@subject = @emailSubject
,@execute_query_database = 'msdb'
,@body = @tableHTML
,@body_format = 'HTML'


RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END








































