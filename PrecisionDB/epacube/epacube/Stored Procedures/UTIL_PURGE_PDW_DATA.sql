﻿





CREATE PROCEDURE [epacube].[UTIL_PURGE_PDW_DATA] 
        @IN_PRICE_LINE Varchar(64) = NULL
       ,@IN_IDW_SELLER_ID Varchar(64) = NULL
      
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        12/14/2012  Initial SQL Version
-- CV        01/04/2013  add delete of PDW ID

 
 
  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        
 ----DECLARE @V_ORG_ENTITY_STRUCTURE_FK  Int
 
 
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of epacube.UTL_PURGE_PDW_DATA. '
               
                + ' IDW SELLER ID = '
                + isNull(@IN_IDW_SELLER_ID, 'NULL')
                + ' PriceLine = '
                + isnull(@IN_PRICE_LINE, 'NULL')
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
 

                
                
                

---------------------------------------------------------------------------------------
----CREATE TEMP TABLE FOR PRODUCT LIST-----
---------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb.. #TS_REMOVE_PROD_ASSOC') is not null
	   drop table  #TS_REMOVE_PROD_ASSOC;


-- create temporary tables and indexes

CREATE TABLE #TS_REMOVE_PROD_ASSOC 
 ([PRODUCT_STRUCTURE_FK]  int Not NULL)
 
 
 select distinct dnp.DATA_NAME_FK, dn.NAME, 
 ds.TABLE_NAME from epacube.DATA_NAME_PROPERTIES dnp WITH (NOLOCK)
 inner join epacube.DATA_NAME dn WITH (NOLOCK)
 on (dn.DATA_NAME_ID = dnp.DATA_NAME_FK)
 inner join epacube.DATA_SET ds WITH (NOLOCK)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK
 AND ds.TABLE_NAME not in  ('product_identification', 'PRODUCT_IDENT_NONUNIQUE'))
  where dnp.NAME = 'PDW'
 order by ds.TABLE_NAME
 
--------------------------------------------------------------------------------
---if user passes the price line
-----------------------------------------------------------------------------------

-----select to get products
insert into #TS_REMOVE_PROD_ASSOC
(PRODUCT_STRUCTURE_FK)
(select pc.product_structure_fk from epacube.PRODUCT_CATEGORY pc with (nolock)
inner join epacube.data_value dv with (nolock)
on (pc.data_value_fk = dv.DATA_VALUE_ID
and dv.DATA_NAME_FK = 310903
and dv.VALUE = @IN_PRICE_LINE))

--310903	ECL PRICE LINE   


----------------------------------------------------------
-----if user passes the Mfr seller id
----------------------------------------------------------
insert into #TS_REMOVE_PROD_ASSOC
(PRODUCT_STRUCTURE_FK)
(select distinct
product_structure_fk 
from epacube.PRODUCT_ATTRIBUTE 
where ATTRIBUTE_EVENT_DATA = @IN_IDW_SELLER_ID
and DATA_NAME_FK = 3810804)


---------------------------------------------------------
----CREATE INDEX
------------------------------------------------------------

CREATE CLUSTERED INDEX IDX_TSPA_TSPSFK ON #TS_REMOVE_PROD_ASSOC
(PRODUCT_STRUCTURE_FK ASC)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

------------------------------------------------------
----- select to get org
------------------------------------------------------


------------------------------------------------------------------------------------------------------------------
---Once we have the product_structure
--- remove these entries from the following tables.
------------------------------------------------------------------------------------------------------------------

DELETE
FROM EPACUBE.PRODUCT_IDENTIFICATION
WHERE DATA_NAME_FK in (310102, 333810104, 3810107)
AND PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			
					
			   SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_identification table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END



DELETE
FROM EPACUBE.PRODUCT_CATEGORY
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND DATA_NAME_FK in ( select distinct dnp.DATA_NAME_FK 
 from epacube.DATA_NAME_PROPERTIES dnp WITH (NOLOCK)
 inner join epacube.DATA_NAME dn WITH (NOLOCK)
 on (dn.DATA_NAME_ID = dnp.DATA_NAME_FK)
 inner join epacube.DATA_SET ds WITH (NOLOCK)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK
 AND ds.TABLE_NAME = 'PRODUCT_CATEGORY')
  where dnp.NAME = 'PDW')
 
			
			   SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_category table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

DELETE
FROM EPACUBE.PRODUCT_ATTRIBUTE
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND DATA_NAME_FK in ( select distinct dnp.DATA_NAME_FK 
 from epacube.DATA_NAME_PROPERTIES dnp WITH (NOLOCK)
 inner join epacube.DATA_NAME dn WITH (NOLOCK)
 on (dn.DATA_NAME_ID = dnp.DATA_NAME_FK)
 inner join epacube.DATA_SET ds WITH (NOLOCK)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK
 AND ds.TABLE_NAME = 'PRODUCT_ATTRIBUTE')
  where dnp.NAME = 'PDW')
 
			 
			 SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_attribute table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


DELETE
FROM EPACUBE.PRODUCT_TEXT
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND DATA_NAME_FK in ( select distinct dnp.DATA_NAME_FK 
 from epacube.DATA_NAME_PROPERTIES dnp WITH (NOLOCK)
 inner join epacube.DATA_NAME dn WITH (NOLOCK)
 on (dn.DATA_NAME_ID = dnp.DATA_NAME_FK)
 inner join epacube.DATA_SET ds WITH (NOLOCK)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK
 AND ds.TABLE_NAME = 'PRODUCT_TEXT')
  where dnp.NAME = 'PDW')
 
			
			 SET @l_rows_processed = @@ROWCOUNT
                   
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_text table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

DELETE
FROM EPACUBE.PRODUCT_UOM_CLASS
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND DATA_NAME_FK in ( select distinct dnp.DATA_NAME_FK 
 from epacube.DATA_NAME_PROPERTIES dnp WITH (NOLOCK)
 inner join epacube.DATA_NAME dn WITH (NOLOCK)
 on (dn.DATA_NAME_ID = dnp.DATA_NAME_FK)
 inner join epacube.DATA_SET ds WITH (NOLOCK)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK
 AND ds.TABLE_NAME = 'PRODUCT_UOM_CLASS')
  where dnp.NAME = 'PDW')
 
			 
			 SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_Uom_Class table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

DELETE
FROM EPACUBE.PRODUCT_DESCRIPTION
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND DATA_NAME_FK in ( select distinct dnp.DATA_NAME_FK 
 from epacube.DATA_NAME_PROPERTIES dnp WITH (NOLOCK)
 inner join epacube.DATA_NAME dn WITH (NOLOCK)
 on (dn.DATA_NAME_ID = dnp.DATA_NAME_FK)
 inner join epacube.DATA_SET ds WITH (NOLOCK)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK
 AND ds.TABLE_NAME = 'PRODUCT_DESCRIPTION')
  where dnp.NAME = 'PDW')
 
			
			 SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_description table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END



DELETE
FROM EPACUBE.PRODUCT_VALUES
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND DATA_NAME_FK in ( select distinct dnp.DATA_NAME_FK 
 from epacube.DATA_NAME_PROPERTIES dnp WITH (NOLOCK)
 inner join epacube.DATA_NAME dn WITH (NOLOCK)
 on (dn.DATA_NAME_ID = dnp.DATA_NAME_FK)
 inner join epacube.DATA_SET ds WITH (NOLOCK)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK
 AND ds.TABLE_NAME = 'PRODUCT_VALUES')
  where dnp.NAME = 'PDW')
 
			 
			 SET @l_rows_processed = @@ROWCOUNT
                   
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_values table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
                        
                        
----DELETE
----FROM MARGINMGR.PRICESHEET   -----Historical price sheet stuff
----WHERE PRODUCT_STRUCTURE_FK IN (
----			SELECT PRODUCT_STRUCTURE_FK 
----			FROM #TS_REMOVE_PROD_ASSOC)
----			AND ORG_ENTITY_STRUCTURE_FK = @V_ORG_ENTITY_STRUCTURE_FK
			 
----			 SET @l_rows_processed = @@ROWCOUNT
                    
----                        BEGIN
----                            SET @status_desc = 'Total rows deleted from pricesheet table = '
----                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
----                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
----                                @status_desc
----                        END

DELETE
FROM marginmgr.SHEET_RESULTS_VALUES_FUTURE
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND DATA_NAME_FK in ( select distinct dnp.DATA_NAME_FK 
 from epacube.DATA_NAME_PROPERTIES dnp WITH (NOLOCK)
 inner join epacube.DATA_NAME dn WITH (NOLOCK)
 on (dn.DATA_NAME_ID = dnp.DATA_NAME_FK)
 inner join epacube.DATA_SET ds WITH (NOLOCK)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK
 AND ds.TABLE_NAME = 'SHEET_RESULTS')
  where dnp.NAME = 'PDW')
 
			
			 SET @l_rows_processed = @@ROWCOUNT
                   
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_results table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
			
			
DELETE
FROM EPACUBE.PRODUCT_ASSOCIATION
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND DATA_NAME_FK in ( select distinct dnp.DATA_NAME_FK 
 from epacube.DATA_NAME_PROPERTIES dnp WITH (NOLOCK)
 inner join epacube.DATA_NAME dn WITH (NOLOCK)
 on (dn.DATA_NAME_ID = dnp.DATA_NAME_FK)
 inner join epacube.DATA_SET ds WITH (NOLOCK)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK
 AND ds.TABLE_NAME = 'PRODUCT_ASSOCIATION')
  where dnp.NAME = 'PDW')
 
  
  
  DELETE
FROM EPACUBE.PRODUCT_STATUS
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND DATA_NAME_FK in ( select distinct dnp.DATA_NAME_FK 
 from epacube.DATA_NAME_PROPERTIES dnp WITH (NOLOCK)
 inner join epacube.DATA_NAME dn WITH (NOLOCK)
 on (dn.DATA_NAME_ID = dnp.DATA_NAME_FK)
 inner join epacube.DATA_SET ds WITH (NOLOCK)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK
 AND ds.TABLE_NAME = 'PRODUCT_STATUS')
  where dnp.NAME = 'PDW')
 

 
			
			 SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from product_Status table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
                        
-----------REMOVE ANY PENDING EVENTS FOR THE PRODUCT /ORG COMBINTAION
--first remove the errors

                    
DELETE
FROM synchronizer.EVENT_DATA_ERRORS
WHERE EVENT_FK in (SELECT EVENT_ID FROM synchronizer.EVENT_DATA 
WHERE EVENT_STATUS_CR_FK = 80
AND PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND DATA_NAME_FK in ( select distinct dnp.DATA_NAME_FK 
 from epacube.DATA_NAME_PROPERTIES dnp WITH (NOLOCK)
 inner join epacube.DATA_NAME dn WITH (NOLOCK)
 on (dn.DATA_NAME_ID = dnp.DATA_NAME_FK)
 inner join epacube.DATA_SET ds WITH (NOLOCK)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK)
 and dnp.NAME = 'PDW')
			)
			
			
			 SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from Event Errors table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


                        
DELETE
FROM synchronizer.EVENT_DATA
WHERE PRODUCT_STRUCTURE_FK IN (
			SELECT PRODUCT_STRUCTURE_FK 
			FROM #TS_REMOVE_PROD_ASSOC)
			AND DATA_NAME_FK in ( select distinct dnp.DATA_NAME_FK 
 from epacube.DATA_NAME_PROPERTIES dnp WITH (NOLOCK)
 inner join epacube.DATA_NAME dn WITH (NOLOCK)
 on (dn.DATA_NAME_ID = dnp.DATA_NAME_FK)
 inner join epacube.DATA_SET ds WITH (NOLOCK)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK)
 and dnp.NAME = 'PDW')
 and EVENT_STATUS_CR_FK = 80 
			
			 SET @l_rows_processed = @@ROWCOUNT
                    
                        BEGIN
                            SET @status_desc = 'Total rows deleted from Event Data table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END




-------------------------------------------------------------------------------------------------------------

  SET @status_desc = 'finished execution of SYNCHRONIZER.epacube.UTL_PURGE_PDW_DATA'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.UTL_PURGE_PDW_DATA has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END














