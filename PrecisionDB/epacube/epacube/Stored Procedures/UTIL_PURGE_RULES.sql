﻿






-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
--
-- Purpose: Purge Data Names that have a status of Purge
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/21/2010   Initial version
--						  
--
--
CREATE PROCEDURE [epacube].[UTIL_PURGE_RULES] 
AS  
BEGIN


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @status_desc        varchar(max);

----  ASSUMES THAT ALL THE FOLLOWING DATA IS CLEARED PRIOR TO RUNNING
------IMPORT, STAGE, EVENT, EXPORT, RULES, ENTITY, AND PRODUCT TABLES 
----  SO NOT CHECKING FOR EXISTANCE OF DATA_NAMES IN THESE TABLES           

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      SET @status_desc  = 'started execution of epacube.UTIL_PURGE_RULES' 
	  
      EXEC exec_monitor.start_monitor_sp @exec_desc = @status_desc
                                                  ,@epa_job_id = NULL,  @epa_batch_no = NULL
												  ,@exec_id = @l_exec_no OUTPUT


     SET @l_sysdate = GETDATE()




	DELETE 
	FROM synchronizer.RULES_FILTER_SET
	WHERE RULES_FK IN 
	(
	SELECT RULES_ID
	FROM synchronizer.RULES
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from synchronizer.RULES_FILTER_SET table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END




-------------------------------------------------------------------------------------
--   RULE TABLES - MAIN FOR NOW
-------------------------------------------------------------------------------------


	DELETE 
	FROM synchronizer.RULES_FILTER_SET
	WHERE RULES_FK IN 
	(
	SELECT RULES_ID
	FROM synchronizer.RULES
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from synchronizer.RULES_FILTER_SET table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END


	DELETE 
	FROM synchronizer.RULES_ERRORS
	WHERE RULES_FK IN 
	(
	SELECT RULES_ID
	FROM synchronizer.RULES
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from synchronizer.RULES_ERRORS table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END


	DELETE 
	FROM synchronizer.RULES_ACTION_OPERANDS
	WHERE RULES_ACTION_FK IN 
	(
	SELECT RULES_ACTION_ID
	FROM synchronizer.RULES R
	INNER JOIN synchronizer.RULES_ACTION RA 
	 ON ( R.RULES_ID = RA.RULES_FK )
	WHERE R.RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from synchronizer.RULES_ACTION_OPERANDS table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END


	DELETE 
	FROM synchronizer.RULES_ACTION
	WHERE RULES_FK IN 
	(
	SELECT RULES_ID
	FROM synchronizer.RULES
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from synchronizer.RULES_ACTION table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END





-------------------------------------------------------------------------------------
--   RULE TABLES -- OTHERS LATER  synchronizer.RULES_ATTRIBUTE  synchronizer.RULES_CONTRACT
-------------------------------------------------------------------------------------




-------------------------------------------------------------------------------------
--  ADD MARGIN TABLES LATER
-------------------------------------------------------------------------------------



------------------------------------------------------------------------------------------
--   REMOVE COLUMNS MARKED WTIH RECORD_STATUS_CR_FK = 0 FOR PURGE 
------------------------------------------------------------------------------------------

		
	DELETE 
	FROM synchronizer.RULES
	WHERE  record_status_cr_fk = 0

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from synchronizer.RULES table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END
	   

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of epacube.UTIL_PURGE_RULES';
     Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, 
                                               @epa_job_id = NULL,  @epa_batch_no = NULL
                                               
     
      
      Exec exec_monitor.print_status_sp @exec_id = @l_exec_no;
   END TRY
   BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;
      

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = NULL;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState, -- State.
					NULL 
				    );
   END CATCH;
END

















































