﻿
-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get all media with types and there associations by PRODUCT_STRUCTURE_ID JIRA (PRE-97)
-- 1. Return media, types, and associations in JSON format
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------


CREATE   PROCEDURE [epacube].[PRODUCT_GET_BY_MEDIA]
	@media_management_id INT,
	@product_id VARCHAR(128) = NULL,
	@product_id_non_unique VARCHAR(256) = NULL,
	@product_description VARCHAR(256) = NULL
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.PRODUCT_GET_BY_MEDIA. Parameters: @media_management_id = ' + CAST(@media_management_id AS VARCHAR(10)) + ', @product_id = ' + @product_id + ', @product_indentifier_non_unique = ' + @product_id_non_unique + ', @product_description = ' + @product_description
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	 BEGIN TRY

		DECLARE @field_list AS TABLE (DATA_NAME_ID INT, [NAME] VARCHAR(64), [LABEL] VARCHAR(64));
		DECLARE @dynamic_fields AS VARCHAR(MAX);

		INSERT INTO @field_list
		SELECT DATA_NAME_ID
			, dn.[NAME]
			, [LABEL]
		FROM epacube.data_name dn
			INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
			INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
		WHERE dn.RECORD_STATUS_CR_FK = 1
			AND cr.CODE = 'PRODUCT'
			AND dn.USER_EDITABLE = 1

		SELECT @dynamic_fields = COALESCE(@dynamic_fields + ', ', '') + QUOTENAME([name])
		FROM @field_list

		DROP TABLE IF EXISTS #FILTERED_PRODUCTS;

		CREATE TABLE #FILTERED_PRODUCTS (
			PRODUCT_STRUCTURE_ID BIGINT PRIMARY KEY
		);

		INSERT INTO #FILTERED_PRODUCTS (PRODUCT_STRUCTURE_ID)
		SELECT DISTINCT PRODUCT_STRUCTURE_FK
		FROM epacube.PRODUCT_MEDIA_ASSOCIATION
		WHERE MEDIA_MANAGEMENT_FK = @media_management_id

		IF @product_id IS NOT NULL
			BEGIN
				INSERT INTO #FILTERED_PRODUCTS (PRODUCT_STRUCTURE_ID)
				SELECT DISTINCT pma.PRODUCT_STRUCTURE_FK
				FROM epacube.PRODUCT_IDENTIFICATION pid
					INNER JOIN epacube.PRODUCT_MEDIA_ASSOCIATION pma
						ON pid.PRODUCT_STRUCTURE_FK = pma.PRODUCT_STRUCTURE_FK
					LEFT JOIN #FILTERED_PRODUCTS fp
						ON pid.PRODUCT_STRUCTURE_FK = fp.PRODUCT_STRUCTURE_ID
				WHERE fp.PRODUCT_STRUCTURE_ID IS NULL
					AND pma.MEDIA_MANAGEMENT_FK = @media_management_id
					AND [VALUE] LIKE '%' + @product_id + '%'
			END
		
		IF @product_id_non_unique IS NOT NULL
			BEGIN
				INSERT INTO #FILTERED_PRODUCTS (PRODUCT_STRUCTURE_ID)
				SELECT DISTINCT ps.PRODUCT_STRUCTURE_ID
				FROM epacube.PRODUCT_STRUCTURE ps
					INNER JOIN epacube.PRODUCT_MEDIA_ASSOCIATION pma
						ON ps.PRODUCT_STRUCTURE_ID = pma.PRODUCT_STRUCTURE_FK
					LEFT JOIN #FILTERED_PRODUCTS fp
						ON ps.PRODUCT_STRUCTURE_ID = fp.PRODUCT_STRUCTURE_ID
				WHERE fp.PRODUCT_STRUCTURE_ID IS NULL
					AND pma.MEDIA_MANAGEMENT_FK = @media_management_id
					AND ps.PRODUCT_STRUCTURE_ID IN (
						SELECT PRODUCT_STRUCTURE_FK
						FROM epacube.PRODUCT_IDENTIFICATION 
						WHERE [VALUE] LIKE '%' + @product_id_non_unique + '%'
						UNION
						SELECT PRODUCT_STRUCTURE_FK
						FROM epacube.PRODUCT_IDENT_NONUNIQUE
						WHERE [VALUE] LIKE '%' + @product_id_non_unique + '%'
						UNION
						SELECT PRODUCT_STRUCTURE_FK
						FROM epacube.PRODUCT_ATTRIBUTE
						WHERE ATTRIBUTE_CHAR LIKE '%' + @product_id_non_unique + '%'
							AND DATA_NAME_FK = (
								SELECT DATA_NAME_ID 
								FROM epacube.DATA_NAME
								WHERE NAME = 'VENDOR PRODUCT NUMBER'))
			END

		IF @product_description IS NOT NULL
			BEGIN
				INSERT INTO #FILTERED_PRODUCTS (PRODUCT_STRUCTURE_ID)
				SELECT DISTINCT pd.PRODUCT_STRUCTURE_FK
				FROM epacube.PRODUCT_DESCRIPTION pd
					INNER JOIN epacube.PRODUCT_MEDIA_ASSOCIATION pma
						ON pd.PRODUCT_STRUCTURE_FK = pma.PRODUCT_STRUCTURE_FK
					LEFT JOIN #FILTERED_PRODUCTS fp
						ON pd.PRODUCT_STRUCTURE_FK = fp.PRODUCT_STRUCTURE_ID
				WHERE fp.PRODUCT_STRUCTURE_ID IS NULL
					AND pma.MEDIA_MANAGEMENT_FK = @media_management_id
					AND [DESCRIPTION] LIKE '%' + @product_description + '%'
			END

		DROP TABLE IF EXISTS #Product;

		SELECT *
		INTO #Product
		FROM (
			SELECT pp.PRODUCT_STRUCTURE_ID, dn.[NAME], pid.[VALUE]
			FROM epacube.DATA_NAME dn
				INNER JOIN epacube.PRODUCT_IDENTIFICATION pid ON dn.DATA_NAME_ID = pid.DATA_NAME_FK
				INNER JOIN #FILTERED_PRODUCTS pp ON pid.PRODUCT_STRUCTURE_FK = pp.PRODUCT_STRUCTURE_ID
				INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
				INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
			WHERE dn.RECORD_STATUS_CR_FK = 1
				AND cr.CODE = 'PRODUCT'
			UNION
			SELECT pp.PRODUCT_STRUCTURE_ID, dn.[NAME], pin.[DESCRIPTION]
			FROM epacube.DATA_NAME dn
				INNER JOIN epacube.PRODUCT_DESCRIPTION pin ON dn.DATA_NAME_ID = pin.DATA_NAME_FK
				INNER JOIN #FILTERED_PRODUCTS pp ON pin.PRODUCT_STRUCTURE_FK = pp.PRODUCT_STRUCTURE_ID
				INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
				INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
			WHERE dn.RECORD_STATUS_CR_FK = 1
				AND cr.CODE = 'PRODUCT') AS product;
 
		--Build the Dynamic Pivot Table Query
		DECLARE @table_structure AS VARCHAR(MAX);

		IF OBJECT_ID('tempdb.dbo.Product_Pivot', 'U') IS NOT NULL
			DROP TABLE tempdb.dbo.Product_Pivot; 

		SET @table_structure = 'SELECT ' + @dynamic_fields  + ', PRODUCT_STRUCTURE_ID epaCUBE_ID' +
		' INTO tempdb.dbo.Product_Pivot FROM #Product x PIVOT ( MAX(VALUE) FOR [NAME] in (' + @dynamic_fields + ') ) p ';
		
		EXECUTE(@table_structure);

		SELECT (
				SELECT *
				FROM @field_list
				FOR JSON PATH
			) [fields]
			, (
				SELECT *
				FROM (
					SELECT *
					FROM tempdb.dbo.Product_Pivot) p
				FOR JSON PATH
			) [data]
		FOR JSON PATH;

		SET @ls_stmt = 'Finished execution of epacube.PRODUCT_GET_BY_MEDIA.';
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;
	
	 END TRY

	BEGIN CATCH

		SET @ls_stmt = 'Execution of epacube.PRODUCT_GET_BY_MEDIA has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

	END CATCH
END