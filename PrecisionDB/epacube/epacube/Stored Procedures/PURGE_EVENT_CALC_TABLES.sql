﻿









--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV		 03/18/2011  Initial Create
-- RG		 11/05/2013	 SUP-2465 not null in job check; set appscope
-- 

CREATE PROCEDURE [epacube].[PURGE_EVENT_CALC_TABLES] @purge_immediate_ind INT = 0 
AS
BEGIN

        DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @job_timestamp DATETIME
		DECLARE @archivePeriod int
		DECLARE @archive_date DATETIME
		DECLARE @defaultArchivePeriod int
		DECLARE @appscope INT
				
		BEGIN TRY
            SET NOCOUNT ON ;
            SET @l_exec_no = 0
            SET @l_sysdate = GETDATE()
			SET @appscope = (select APPLICATION_SCOPE_ID from epacube.APPLICATION_SCOPE where scope = 'APPLICATION')
			SET @defaultArchivePeriod = 30  --- KS changed from 90 to thirty
			SET @archivePeriod = CAST(isnull((select CAST ( value as int ) from epacube.epacube_params with (NOLOCK)
										  where NAME = 'ARCHIVE_PERIOD'
										  and application_scope_fk = @appscope),@defaultArchivePeriod) AS VARCHAR(20))
			SET @archive_date = DATEADD(day, -@archivePeriod, @l_sysdate)										  


----------------------------------------------------------
--logging
----------------------------------------------------------

IF @purge_immediate_ind = 0 

BEGIN
		
SET @ls_stmt = 'started execution of epacube.PURGE_EVENT_CALC_TABLES. '
                + 'Archive Period = '
                --+ @archivePeriod + ' days.'
            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;

--------------------------------------------------------------------------------------------
--  CREATE TEMP TABLE FOR JOBS TO BE REMOVED
--		REMOVE JOBS THAT JOB_DATE > ARCHIVE_PERIOD ( IF ARCHIVE_PERIOD IS NULL USE 30 DAYS )
--------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_REMOVE_JOBS') is not null
	   drop table #TS_REMOVE_JOBS;

	   
	-- create temp table
	CREATE TABLE #TS_REMOVE_JOBS(
				JOB_FK BIGINT NULL
				)

INSERT INTO #TS_REMOVE_JOBS
	( JOB_FK )
	SELECT EC.JOB_FK
	FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)	
	WHERE utilities.TRUNC(EC.RESULT_EFFECTIVE_DATE) 
	                <=
		  utilities.TRUNC(@archive_date)									
    AND EC.JOB_FK NOT IN (
				SELECT DISTINCT IMPORT_JOB_FK
				FROM synchronizer.event_data
				where IMPORT_JOB_FK is not null
				UNION ALL
				SELECT DISTINCT IMPORT_JOB_FK
				FROM synchronizer.EVENT_DATA_HISTORY 
				where IMPORT_JOB_FK is not null
				)
				


--------------------------------------------------------------------
						
--------			
----------CALC TABLES


					DELETE  FROM synchronizer.event_Calc_errors
                    WHERE   EVENT_FK IN (select EVENT_ID FROM synchronizer.EVENT_CALC
				WHERE JOB_FK  IN (
                               SELECT JOB_FK
                               FROM #TS_REMOVE_JOBS ))
																																																				

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows moved from synchronizer.event_Calc_errors table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


	
		DELETE  FROM synchronizer.EVENT_CALC_RULES_BASIS
                    WHERE  EVENT_RULES_FK IN  (select EVENT_RULES_ID 
                    FROM synchronizer.EVENT_CALC_RULES
                    WHERE   EVENT_FK IN 
                     ( select EVENT_ID FROM synchronizer.EVENT_CALC
				WHERE JOB_FK  IN (
                               SELECT JOB_FK
                               FROM #TS_REMOVE_JOBS )))
																																																				

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows moved from synchronizer.event_Calc_rules_basis table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

			DELETE  FROM synchronizer.EVENT_CALC_RULES
                    WHERE   EVENT_FK IN ( select EVENT_ID FROM synchronizer.EVENT_CALC
				WHERE JOB_FK  IN (
                               SELECT JOB_FK
                               FROM #TS_REMOVE_JOBS ))
																																																				

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows moved from synchronizer.event_Calc_rules table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


																				
				DELETE  FROM synchronizer.EVENT_CALC
                        WHERE JOB_FK  IN (
                               SELECT JOB_FK
                               FROM #TS_REMOVE_JOBS )																																

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows moved from synchronizer.event_Calc table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


				
--drop temp table if it exists
	IF object_id('tempdb..#TS_REMOVE_JOBS') is not null
	   drop table #TS_REMOVE_JOBS;

END 

If @purge_immediate_ind = 1

BEGIN

TRUNCATE TABLE Synchronizer.event_calc_rules_basis
DELETE FROM Synchronizer.event_calc_rules
TRUNCATE TABLE Synchronizer.event_calc_errors
DELETE FROM Synchronizer.event_calc

END
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

            SET @status_desc = 'finished execution of SYNCHRONIZER.epacube.PURGE_EVENT_CALC_TABLES'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


  END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.PURGE_EVENT_CALC_TABLES has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH
		  
    END









