﻿









-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Jan DeForest
--
--
-- Purpose: Removing Products from ICSC Catalog
--			
--			
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        02/28/2015   Initial SQL Version
--
--

CREATE PROCEDURE [epacube].[UTIL_ACTIVATE_MM_RULES] 
      
AS
BEGIN



DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint
DECLARE @v_import_package_fk   int  
DECLARE @v_NULLING_IND   int 
DECLARE @v_filename_count INT
DECLARE @epa_job_id			 INT


DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)

DECLARE @v_last_job_fk     bigint
DECLARE @v_job_date   datetime
DECLARE @v_out_job_fk BIGINT


BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of epacube.Util_activate_MM_RULES - from HOLD status', 
                                                @epa_job_id = NULL, @epa_batch_no =NULL,
												@exec_id = @l_exec_no OUTPUT;
												
SET @l_sysdate = GETDATE()

-----------------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------------
  
                  EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'job_seq', @v_job_fk output
    
    SET @v_job_class_fk  = 100        --- PRODUCT MAINTENANCE
    SET @v_job_user = 'EPACUBE'
    SET @v_job_name = 'ACTIVATE MM RULES'
    SET @v_job_date = @l_sysdate
    SET @v_data1   = NULL
    SET @v_data2    = NULL
    
    EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
										   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

    EXEC common.job_execution_create   @v_job_fk, 'START ACTIVATE MM RULES',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  
----------------------------------------------------------------------------------------------  
					
					Update R Set Record_status_cr_fk = 1 from synchronizer.rules r 
					where result_data_name_fk in (111401, 111501, 111502, 111602) 
					and record_status_cr_fk = 3

-------------------------------------------------------------------------------------------------


EXEC common.job_execution_create   @v_job_fk, 'ACTIATION COMPLETE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @l_sysdate, @l_sysdate  
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of epacube.UTIL_ACTIVATE_MM_RULES - from HOLD'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = NULL,  @epa_batch_no = NULL
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of epacube.UTIL_ACTIVATE_MM_RULES - from HOLD has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END



































































































































































































