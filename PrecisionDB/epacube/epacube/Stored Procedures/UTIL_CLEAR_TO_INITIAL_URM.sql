﻿








-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        06/11/2013   Initial Version 
-- CV        09/15/2018   Modify for URM



CREATE PROCEDURE [epacube].[UTIL_CLEAR_TO_INITIAL_URM] 
    
        
AS BEGIN 

-----------------------------------------------------------------------------------------------
--                   *********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
--     DECLARE @l_sysdate                DATETIME
--     DECLARE @v_sysdate                DATETIME
       DECLARE @V_START_TIMESTAMP  DATETIME
     DECLARE @V_END_TIMESTAMP    DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
        DECLARE @v_exception_rows        bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
       DECLARE @time_after datetime;
       DECLARE @elapsed_time numeric(10,4);
       DECLARE @rows_affected varchar(max);
       DECLARE @rows_dml bigint;
       DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int     
       
          

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of Epacube.UTIL_CLEAR_TO_INITIAL', 
                                                                                  @exec_id = @l_exec_no OUTPUT,
                                                                                  @epa_job_id = NULL;

--     SET @l_sysdate = GETDATE()
SET @V_START_TIMESTAMP = GETDATE()



--------------------------------------------------------------------------
---USE THIS SCRIPT TO START OVER WITH PRODUCT IMPORTS
---- Does not clear entity data that has been loaded
---- Does not clear data values or rules or seed data.
----  ONLY ALLOWS FOR FRESH PRODUCT IMPORTS
--------------------------------------------------------------------------
 
truncate table  [IMPORTS].[URM_ITEM_BREAKDOWN]
truncate table  [IMPORTS].[URM_ITEM_CLASS]
truncate table  [IMPORTS].[URM_ITEM_NEW]
truncate table  [IMPORTS].[URM_ZONE]
truncate table  [IMPORTS].[URM_ZONE_ITEM]
truncate table  [IMPORTS].[URM_ZONE_ITEM_CLASS]
truncate table  [IMPORTS].[URM_ZONE_ITEM_PARITY]
truncate table  [IMPORTS].[URM_ZONE_LINK]
truncate table  [IMPORTS].[URM_ZONE_PRICE_POINTS]
truncate table  [IMPORTS].[URM_ZONE_RETAILS]
truncate table  [IMPORTS].[URM_ZONE_RETAILS_WEEKLY]


truncate table  [IMPORTS].[URM_LEVEL_GROSS_ZONE]
 
truncate table   [import].[URM_COST_CHANGES_RETAILER]
--
truncate table cleanser.SEARCH_JOB_DATA_NAME
truncate table cleanser.SEARCH_JOB_RESULTS_BEST
truncate table cleanser.SEARCH_JOB_RESULTS_IDENT
truncate table cleanser.SEARCH_JOB_RESULTS_TASK
truncate table cleanser.SEARCH_JOB_RESULTS_TASK_WORK
truncate table cleanser.SEARCH_JOB_from_to_match
truncate table cleanser.SEARCH_JOB_STRING_WORK
delete from cleanser.search_JOB_string
DBCC CHECKIDENT ( 'cleanser.search_job_string',reseed,1000)
DELETE FROM cleanser.search_job
--


TRUNCATE TABLE import.IMPORT_SXE_CUSTOMERS
TRUNCATE TABLE import.IMPORT_SXE_PDSC
TRUNCATE TABLE import.IMPORT_SXE_PDSR
TRUNCATE TABLE import.IMPORT_SXE_PDSV
TRUNCATE TABLE import.IMPORT_RECORD_DATA
TRUNCATE TABLE import.IMPORT_RECORD_DATA_LARGE
TRUNCATE TABLE import.IMPORT_RECORD
TRUNCATE TABLE import.IMPORT_RECORD_NAME_VALUE
TRUNCATE TABLE import.IMPORT_TAXONOMY_TREE
TRUNCATE TABLE import.IMPORT_TAXONOMY_TREE_ATTRIBUTE
TRUNCATE TABLE stage.Import_Data_Exceptions
TRUNCATE TABLE import.IMPORT_USERS
TRUNCATE TABLE import.IMPORT_RULES
TRUNCATE TABLE import.IMPORT_RULES_DATA
TRUNCATE TABLE import.IMPORT_RULES_ERRORS
TRUNCATE TABLE import.IMPORT_SXE_SALES_TRANSACTIONS

--------STAGE

TRUNCATE TABLE stage.STG_RECORD_ERRORS
DELETE FROM STAGE.STG_RECORD_ENTITY_IDENT
TRUNCATE TABLE STAGE.STG_RECORD_IDENT
DELETE FROM STAGE.STG_RECORD_ENTITY
TRUNCATE TABLE STAGE.STG_RECORD_DATA
TRUNCATE TABLE STAGE.STG_RECORD_STRUCTURE
DELETE FROM stage.STG_RECORD
TRUNCATE TABLE stage.Import_Data_Exceptions

DBCC CHECKIDENT ( 'STAGE.STG_RECORD_ENTITY_IDENT', RESEED, 1000 )
DBCC CHECKIDENT ( 'STAGE.STG_RECORD_ENTITY', RESEED, 1000 )
DBCC CHECKIDENT ( 'stage.STG_RECORD', RESEED, 1000 )
--
 

---- PRODUCTS ONLY....  NO ENTITIES

TRUNCATE TABLE epacube.PRODUCT_IDENTIFICATION
DBCC CHECKIDENT ( 'epacube.PRODUCT_IDENTIFICATION', RESEED, 1000 )
TRUNCATE TABLE epacube.PRODUCT_DESCRIPTION
DBCC CHECKIDENT ( 'epacube.PRODUCT_DESCRIPTION', RESEED, 1000 )
TRUNCATE TABLE epacube.PRODUCT_CATEGORY
DBCC CHECKIDENT ( 'epacube.PRODUCT_CATEGORY', RESEED, 1000 )
TRUNCATE TABLE epacube.PRODUCT_TEXT
DBCC CHECKIDENT ( 'epacube.PRODUCT_TEXT', RESEED, 1000 )
TRUNCATE TABLE epacube.PRODUCT_UOM
TRUNCATE TABLE epacube.PRODUCT_ATTRIBUTE
DBCC CHECKIDENT ( 'epacube.PRODUCT_ATTRIBUTE', RESEED, 1000 )
TRUNCATE TABLE epacube.PRODUCT_TAX_ATTRIBUTE

TRUNCATE TABLE epacube.segments_product
DBCC CHECKIDENT ( 'epacube.segments_product', RESEED, 1000 )
truncate table epacube.product_gtin
DBCC CHECKIDENT ( 'epacube.PRODUCT_gtin', RESEED, 1000 )
truncate table epacube.product_mult_type  
DBCC CHECKIDENT ( 'epacube.PRODUCT_mult_type', RESEED, 1000 )

TRUNCATE TABLE epacube.PRODUCT_UOM_CLASS
DBCC CHECKIDENT ( 'epacube.PRODUCT_UOM_CLASS', RESEED, 1000 )
TRUNCATE TABLE epacube.PRODUCT_STATUS
DBCC CHECKIDENT ( 'epacube.PRODUCT_STATUS', RESEED, 1000 )
TRUNCATE TABLE epacube.PRODUCT_IDENT_NONUNIQUE
DBCC CHECKIDENT ( 'epacube.PRODUCT_IDENT_NONUNIQUE', RESEED, 1000 )
 


DELETE FROM epacube.PRODUCT_ASSOCIATION
DBCC CHECKIDENT ( 'epacube.PRODUCT_ASSOCIATION', RESEED, 1000 )
--delete from epacube.PRODUCT_ASSOCIATION where DATA_NAME_FK in ( 159450, 159903)

--select distinct dn.name, dn.DATA_NAME_ID from epacube.PRODUCT_ASSOCIATION pa
--inner join epacube.data_name dn on dn.DATA_NAME_ID = pa.DATA_NAME_FK

delete from epacube.data_value where DATA_NAME_FK in (  500019)

truncate table    epacube.segments_settings
truncate table [epacube].[SETTINGS_POS_PROD_STORE_VALUES]
truncate table [epacube].[SETTINGS_POS_PROD_STORE_HEADER]


----delete from  epacube.segments_settings where 1=1 
----and  DATA_NAME_FK in (144897, 144898, 144899, 144901, 144902, 144903, 144904,144905,144906, 144907, 144868,144862, 144908,  144876, 500210, 500211, 500212, 144870,144871,144872,144875,144891,500015,
----502046)
---- and ZONE_ENTITY_STRUCTURE_FK is not NULL
---- and CUST_ENTITY_STRUCTURE_FK is NULL

---- delete from epacube.SEGMENTS_SETTINGS where PROD_SEGMENT_FK is Not NULL 

 truncate table marginmgr.pricesheet_retails

--Delete from epacube.marginmgr.pricesheet_retails where data_name_fk = 503301 
--and zone_segment_fk is not null and cust_entity_structure_fk is null

-------
truncate table marginmgr.pricesheet_basis_values

--delete from marginmgr.pricesheet_basis_values where Zone_Entity_Structure_FK is not Null
--and Cust_Entity_Structure_FK is NULL and Data_Name_FK = 503201





DELETE FROM synchronizer.EVENT_DATA_ERRORS
TRUNCATE TABLE synchronizer.EVENT_DATA_HISTORY
truncate table  [synchronizer].[EVENT_DATA_HISTORY_DETAILS]
truncate table [synchronizer].[EVENT_DATA_HISTORY_HEADER]
TRUNCATE TABLE synchronizer.EVENT_DATA_ARCHIVE
TRUNCATE TABLE synchronizer.EVENT_DATA_RULES
DELETE FROM  synchronizer.EVENT_CALC_ERRORS
DELETE FROM synchronizer.EVENT_CALC_RULES
DELETE FROM  synchronizer.EVENT_CALC_RULES_BASIS
DELETE FROM synchronizer.EVENT_CALC
DELETE FROM synchronizer.EVENT_DATA

DBCC CHECKIDENT ( 'synchronizer.EVENT_DATA_RULES', RESEED, 1000 )
DBCC CHECKIDENT ( 'synchronizer.EVENT_DATA_ERRORS', RESEED, 1000 )
DBCC CHECKIDENT ( 'synchronizer.EVENT_DATA', RESEED, 1000 )

DBCC CHECKIDENT ( 'synchronizer.EVENT_CALC_RULES', RESEED, 1000 )
DBCC CHECKIDENT ( 'synchronizer.EVENT_CALC_RULES_BASIS', RESEED, 1000 )
DBCC CHECKIDENT ( 'synchronizer.EVENT_CALC_ERRORS', RESEED, 1000 )
DBCC CHECKIDENT ( 'synchronizer.EVENT_CALC', RESEED, 1000 )

 


------

--
TRUNCATE TABLE COMMON.JOB_PARAMS
TRUNCATE TABLE COMMON.JOB_FILTERS
TRUNCATE TABLE COMMON.JOB_ERRORS
DELETE FROM COMMON.JOB_EXECUTION
DELETE FROM COMMON.JOB
--
truncate table epacube.SEGMENTS_VENDOR 
truncate table epacube.PRODUCT_RECIPE_INGREDIENT
delete from epacube.PRODUCT_RECIPE_INGREDIENT_XREF
truncate table epacube.PRODUCT_RECIPE_ATTRIBUTE
truncate table epacube.PRODUCT_UOM_SIZE
delete from epacube.PRODUCT_RECIPE
truncate table epacube.PRODUCT_BOM
truncate table epacube.PRODUCT_UOM_Pack

DELETE FROM epacube.PRODUCT_STRUCTURE
DBCC CHECKIDENT ( 'epacube.PRODUCT_STRUCTURE', RESEED, 1000 )
--



TRUNCATE TABLE COMMON.T_SQL

TRUNCATE TABLE common.db_sequence_currvals

UPDATE common.db_sequence_list
SET initial_value = 1001,
    current_value = 1001
WHERE sequence_name IN ( 'job_seq', 'batch_seq' )

UPDATE common.db_sequence_list
SET initial_value = 1000001,
    current_value = 1000001
WHERE sequence_name IN ( 'product_id_seq' )


--


TRUNCATE TABLE job_manager.JobQueue
TRUNCATE TABLE exec_monitor.StatusLog
delete from exec_manager.StatementQueue
DBCC CHECKIDENT ( 'exec_manager.StatementQueue', RESEED, 1 )
DELETE FROM exec_monitor.ExecLog
DBCC CHECKIDENT ( 'exec_monitor.ExecLog', RESEED, 1 )

-----------------------------------------------------------------------------------------------------

END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
         DECLARE @ErrorSeverity INT;
         DECLARE @ErrorState INT;

              SELECT 
                     @ErrorMessage = 'Execution of STAGE.STG_TO_EVENTS has failed ' + ERROR_MESSAGE(),
                     @ErrorSeverity = ERROR_SEVERITY(),
                     @ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = NULL;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
                               @ErrorSeverity, -- Severity.
                               @ErrorState -- State.
                               );
   END CATCH;


END
































