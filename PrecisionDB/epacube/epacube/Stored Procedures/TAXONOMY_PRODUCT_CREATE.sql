﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Assign product to a taxonomy node JIRA (URM-1209)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[TAXONOMY_PRODUCT_CREATE]
	@taxonomy_product VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @taxonomy_node_id INT;

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.TAXONOMY_PRODUCT_CREATE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	IF OBJECT_ID('tempdb.dbo.#Taxonomy_Product', 'U') IS NOT NULL
		DROP TABLE #Taxonomy_Product;
	
	SELECT *
	INTO #Taxonomy_Product
	FROM OPENJSON(@taxonomy_product)
	WITH (
		TAXONOMY_NODE_ID INT '$.TAXONOMY_NODE_ID'
	) tp
	CROSS APPLY OPENJSON(@taxonomy_product, '$.data')
		WITH (
			PRODUCT_STRUCTURE_ID BIGINT '$.PRODUCT_STRUCTURE_ID'
		);

	SELECT @taxonomy_node_id = TAXONOMY_NODE_ID FROM #Taxonomy_Product;
	
	BEGIN TRY

		BEGIN TRANSACTION;
		
			INSERT INTO epacube.TAXONOMY_PRODUCT (PRODUCT_STRUCTURE_FK, DATA_NAME_FK, TAXONOMY_NODE_FK) 
			SELECT PRODUCT_STRUCTURE_ID
				, 190901 DATA_NAME_ID
				, TAXONOMY_NODE_ID
			FROM #Taxonomy_Product;

		COMMIT TRANSACTION;

		SELECT 'Products successfully added' USER_MSG, 1 SUCCESS;
		
		SET @ls_stmt = 'Finished execution of epacube.TAXONOMY_PRODUCT_CREATE.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 

		SET @ls_stmt = 'Execution of epacube.TAXONOMY_PRODUCT_CREATE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END
