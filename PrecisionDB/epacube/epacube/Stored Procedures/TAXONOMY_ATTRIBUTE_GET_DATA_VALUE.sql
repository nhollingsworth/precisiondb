﻿-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get product attribute data value information JIRA (PRE-144)
-- Get all product attribute data name information and return in JSON format
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE   PROCEDURE [epacube].[TAXONOMY_ATTRIBUTE_GET_DATA_VALUE]
	@DATA_NAME_ID INT
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.TAXONOMY_ATTRIBUTE_GET_DATA_VALUE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT (
			SELECT DV.DATA_VALUE_ID
				, DV.[VALUE]
			FROM epacube.DATA_VALUE DV
				INNER JOIN epacube.DATA_NAME DN
					ON DV.DATA_NAME_FK = DN.DATA_NAME_ID
				INNER JOIN epacube.DATA_SET DS
					ON DN.DATA_SET_FK = DS.DATA_SET_ID
			WHERE DS.TABLE_NAME = 'PRODUCT_TAX_ATTRIBUTE'
				AND DN.RECORD_STATUS_CR_FK = 1
				AND DN.DATA_NAME_ID = @DATA_NAME_ID
			ORDER BY DV.[VALUE]
			FOR JSON PATH) [DATA]
		FOR JSON PATH, WITHOUT_ARRAY_WRAPPER

		SET @ls_stmt = 'Finished execution of epacube.TAXONOMY_ATTRIBUTE_GET_DATA_VALUE.';
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		SET @ls_stmt = 'Execution of epacube.TAXONOMY_ATTRIBUTE_GET_DATA_VALUE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

	END CATCH
END