﻿
CREATE PROCEDURE [epacube].[UTIL_URM DATA_NAMES_RECORD_CNTS] 
       
    AS
BEGIN     
 
-- MODIFICATION HISTORY
-- Person		Date		Comments
-- ---------	----------	------------------------------------------
-- GHS			1/29/2017	Identifies record counts for all active data names in the system by data_name_id, table_name, and column_name
--							Data is loaded into table dbo.AAA_Table_Counts_By_DN
 
If object_id('dbo.AAA_Table_Counts_By_DN') is not null
drop table dbo.AAA_Table_Counts_By_DN

Create Table dbo.AAA_Table_Counts_By_DN(Data_Name_ID bigint null, Data_Name varchar(128) null, Label Varchar(64) null, Column_Name varchar(64) Null, Table_Name varchar(128) Null, DN_Status varchar(16) Null, Record_Count Int Null)


	Declare @DN varchar(64), @DN_Name varchar(64), @Label varchar(64), @ColName Varchar(64), @TblName varchar(64), @DN_Status varchar(16), @RecCount int, @SQL Varchar(Max), @SQLu Varchar(Max), @Schema Varchar(64)

	DECLARE Fltr Cursor local for
		Select
			DN.Data_Name_ID
			, DN.Name
			, DN.Label
			, ds.column_name
			, ds.table_name
			, Case when conv.old_dn is not null then 'Old DN' else '' end DN_Status
			, ist.TABLE_SCHEMA
			from epacube.epacube.data_name dn with (nolock)
			inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id
			inner join epacube.INFORMATION_SCHEMA.tables ist on ds.table_name = ist.TABLE_NAME
			Left join dbo.AAA_DN_DS CONV on dn.data_name_id = conv.old_dn
			where dn.record_status_cr_fk = 1
			and ds.table_name is not null
			and ds.table_name in 
			(
			select distinct ist.table_name
			from epacube.epacube.data_name dn with (nolock)
			inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id
			inner join epacube.INFORMATION_SCHEMA.columns ist on ds.table_name = ist.TABLE_NAME
			where dn.record_status_cr_fk = 1
			and ist.COLUMN_NAME = 'Data_Name_FK'
			)
			order by ds.table_name, ds.column_name

					 OPEN Fltr;
					 FETCH NEXT FROM Fltr INTO @DN, @DN_Name, @Label, @ColName, @TblName, @DN_Status, @Schema
					 WHILE @@FETCH_STATUS = 0
				Begin

				Set @SQL = 'Insert into dbo.AAA_Table_Counts_By_DN (Data_Name_ID, Data_Name, Label, Column_Name, Table_Name, DN_Status)
				 Select ' + @DN + ', ''' + @DN_Name + ''', ''' + @Label + ''', ''' + @ColName + ''', ''' + @Schema + '.' + @TblName + ''', ''' + @DN_Status + ''''
				--from epacube.[' + @TblName + '] where data_name_fk = ' + @DN

				exec(@SQL)

				Set @SQLu = 'Update TN 
					Set Record_Count = (Select count(*) from [' + @Schema + '].[' + @TblName + '] where data_name_fk = ' + @DN + ') from dbo.AAA_Table_Counts_By_DN TN where data_name_id = ' + @DN

				exec(@SQLu)

			FETCH NEXT FROM Fltr INTO @DN, @DN_Name, @Label, @ColName, @TblName, @DN_Status, @Schema
			End

			Close Fltr;
			Deallocate Fltr;

END

