﻿--A_epaMAUI_A_SP2_eMail_Notification_SB

-- =============================================
-- Author:		Gary Stone
-- Create date: April, 2013
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [epacube].[eMail_Notification_Thru_Service_Broker]
	 @Job_FK Int, @Body varchar(128), @subject varchar(128),  @eMailAddress Varchar(64)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Declare @ServiceBrokerCall nvarchar(4000)
	SET @ServiceBrokerCall = 
		'EXEC msdb.dbo.sp_send_dbmail ' 
		+ '@recipients = ''' + @eMailAddress + ''', '
		+ '@body = ''' + @body + ''', '
		+ '@subject = ''' + @subject + ''' ;'

	EXEC queue_manager.enqueue
		@service_name = N'TargetEpaService', --  nvarchar(50)
		@cmd_text = @ServiceBrokerCall, --  nvarchar(4000)
		@epa_job_id = @Job_FK, --  int
		@cmd_type = N'SQL' --  nvarchar(10)
END
