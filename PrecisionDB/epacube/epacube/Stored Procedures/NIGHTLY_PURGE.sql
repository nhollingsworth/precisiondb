﻿








CREATE or ALTER PROCEDURE [epacube].[NIGHTLY_PURGE] AS
BEGIN
      DECLARE 
        /* Copyright 2005, epaCUBE, Inc. */
        /* $Header: /sdi/development/db/update/1.06.06.03/proc_sheet_calc_insert_results.sql,v 1.1 2005/05/11 18:43:34 cnoe Exp $ 
		

			Purpose: This procedure is called as part of the nightly routine from the 
					 Nightly Synchronizer procedure. Its purpose in life is to purge
					 all products flagged as such, all the approved events,
					 and then to call the purge orphan products
					 procedure.


			Modification History

			Name____Date___________________Mod_Comments______________________________________
			CV   10/16/2008   added delete from product association table   
			CV   10/30/2008   Added delete from marginmgr.sheet_calculation 
			RG	 07/14/2009   V5-ification
			RG	 10/12/2009   Adding deletion of Approved Events
            CV   11/13/2009   Commented the removal of event data history for approved status
                              We need the history for exporting the data.  Future dated events were
                               approving and then getting removed therefore not getting exported.
			CV   08/18/2010   Added the order data history to purge table list
			CV   03/08/2011   Added the event data history and archive to purge table list epa-3191
            CV   04/13/2012	  Added Marginmrg.pricesheet to list
            CV   08/28/2012   Added call to Util Purge Sql	
            CV   11/23/2012   Adding logging so we can track products that are being purged.
			CV   01/20/2013   Add to remove orphaned entity structures created by UI.
			CV   07/09/2013   Added to remove inactive entity associations.
			RG   10/21/2013   Adding Product_Multipliers table to nightly purge.
            CV   11/18/2013   Needed to add clean up of entity associations table
			CV   09/23/2020   Change name of product uom table

*/
        @l_sysdate datetime,
        @ls_stmt varchar(1000),
        @l_exec_no bigint,
        @status_desc varchar(max),     
        @v_batch_no bigint, 
        @v_do_not_approve smallint, 
        @v_approve_event smallint, 
		@PurgeCodeRefID int,
        @ApprovedStatus int

		--SUP-5575
           insert into common.t_sql
		   (ts, sql_text)
		   values(getdate(), 'Starting Purge')





		 SET @l_sysdate = getdate()
		set @PurgeCodeRefID = epacube.getCodeRefId('PRODUCT_STATUS','PURGE')
		set @ApprovedStatus = epacube.getCodeRefId('EVENT_STATUS','APPROVED')
       -- EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of EPACUBE.NIGHTLY_PURGE' , 
										--		@exec_id = @l_exec_no OUTPUT;
        
		DECLARE @ProductsFlaggedPurge TABLE
			(	
				ProductStructureID bigint
			)
		
		INSERT INTO @ProductsFlaggedPurge (ProductStructureID)
			select product_structure_id
			from epacube.product_structure 
			where product_status_cr_fk = @PurgeCodeRefID

		DECLARE @EventsFlaggedPurge TABLE
			(	
				EventID bigint
			)
		
		INSERT INTO @EventsFlaggedPurge (EventID)
			select event_id
			from synchronizer.event_data
			where product_structure_fk in (select ProductStructureID 
										   from @ProductsFlaggedPurge)
										   
		
--------------------------------------------------------------------------------------------------------
--Add logging to see what is being purged
--------------------------------------------------------------------------------------------------------
		
		INSERT INTO [epacube].[Nightly_Purge_Log]
           ([Product_structure_fk]
           ,[Product_id]
           ,[Date_Purged])
           (select ProductStructureID
           ,(select value from epacube.PRODUCT_IDENTIFICATION pi where pi.PRODUCT_STRUCTURE_FK = ProductStructureID
           and pi.data_name_fk = 110100)
           ,GETDATE()
           from @ProductsFlaggedPurge          )           


        /* ----------------------------------------------------------------------------------- */
        --    Procedure for all the Nightly Housekeeping 
        /* ----------------------------------------------------------------------------------- */
BEGIN TRY



delete epacube.product_association
where  Child_product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_association
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_attribute
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_category
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_description
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_ident_mult
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_ident_nonunique
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_identification
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_mult_type
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_status
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_tax_attribute
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_text
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_uom_size
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_uom_pack
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_uom_class
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)

delete epacube.product_values
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)


---temp SUP-5575 changes
--SUP-5575
           insert into common.t_sql
		   (ts, sql_text)
		   values(getdate(), 'pmult')



delete epacube.product_multipliers
where  product_structure_fk in 
(select ProductStructureID from @ProductsFlaggedPurge)
---temp SUP-5575 changes
--SUP-5575
           insert into common.t_sql
		   (ts, sql_text)
		   values(getdate(), 'done pmult')
------------------------------------------------------
--
--delete marginmgr.order_data_history
--where  product_structure_fk in
--(select ProductStructureID from @ProductsFlaggedPurge) 


--delete marginmgr.mm_calc_analysis
--where  product_structure_fk in
--(select ProductStructureID from @ProductsFlaggedPurge)       
--        
--delete marginmgr.mm_calc_cust_stats
--where  product_structure_fk in
--(select ProductStructureID from @ProductsFlaggedPurge)               
--
--delete marginmgr.mm_calc_stats
--where  product_structure_fk in
--(select ProductStructureID from @ProductsFlaggedPurge)             
--
--delete marginmgr.mm_inventory_data
--where  product_structure_fk in
--(select ProductStructureID from @ProductsFlaggedPurge)       
--        
--delete marginmgr.mm_product_movement
--where  product_structure_fk in
--(select ProductStructureID from @ProductsFlaggedPurge)       
--        			
--delete marginmgr.mm_sales_summary
--where  product_structure_fk in
--(select ProductStructureID from @ProductsFlaggedPurge)       
--        
--delete marginmgr.mm_watchlist
--where  product_structure_fk in
--(select ProductStructureID from @ProductsFlaggedPurge)       
--        
--delete marginmgr.sheet_exclusion
--where  product_structure_fk in
--(select ProductStructureID from @ProductsFlaggedPurge)       
--        
--delete marginmgr.sheet_historical
--where  product_structure_fk in
--(select ProductStructureID from @ProductsFlaggedPurge)               

        delete marginmgr.SHEET_RESULTS_VALUES_FUTURE
        where  product_structure_fk in
       (select ProductStructureID from @ProductsFlaggedPurge) 

       -- delete marginmgr.pricesheet
       -- where  product_structure_fk in
       --(select ProductStructureID from @ProductsFlaggedPurge)        
        
--
--		delete stage.stg_mm_host_sales
--        where  product_structure_fk in
--       (select ProductStructureID from @ProductsFlaggedPurge)       
        
		
	    delete synchronizer.event_data_errors
        where  event_fk in
       (select EventID from @EventsFlaggedPurge) 
        
		delete synchronizer.event_data_rules
        where  event_fk in
       (select EventID from @EventsFlaggedPurge)			      
        
		delete synchronizer.event_data
        where  event_id in
       (select EventID from @EventsFlaggedPurge) 
               
		delete synchronizer.event_data_history
        where  product_structure_fk in
       (select ProductStructureID from @ProductsFlaggedPurge) 

	   	delete synchronizer.EVENT_DATA_HISTORY_DETAILS
        where event_data_history_header_fk in  ( select event_data_history_header_id from synchronizer.EVENT_DATA_HISTORY_HEADER
        where  product_structure_fk in
       (select ProductStructureID from @ProductsFlaggedPurge) )


	   delete synchronizer.EVENT_DATA_HISTORY_HEADER
        where  product_structure_fk in
       (select ProductStructureID from @ProductsFlaggedPurge) 
       
       delete synchronizer.EVENT_DATA_ARCHIVE
        where  product_structure_fk in
       (select ProductStructureID from @ProductsFlaggedPurge)       


---temp SUP-5575 changes
--SUP-5575
           insert into common.t_sql
		   (ts, sql_text)
		   values(getdate(), 'about to do pstruct')

		delete epacube.product_structure
		where  product_structure_id in
       (select ProductStructureID from @ProductsFlaggedPurge)

	   ---temp SUP-5575 changes
--SUP-5575
           insert into common.t_sql
		   (ts, sql_text)
		   values(getdate(), 'done pstruct')

---------------------------------------------------------------------------------------
----   Delete Approved Events
---------------------------------------------------------------------------------------
	    delete synchronizer.event_data_errors
        where  event_fk in
				(select event_id From synchronizer.event_data with (NOLOCK)
				 where event_status_cr_fk = 
				 epacube.getCodeRefId('EVENT_STATUS','APPROVED')) 

        
		delete synchronizer.event_data_rules
        where  event_fk in
				(select event_id From synchronizer.event_data with (NOLOCK)
				 where event_status_cr_fk = 
				 epacube.getCodeRefId('EVENT_STATUS','APPROVED'))
		
------		delete synchronizer.event_data_history
------        where  event_id in
------				(select event_id From synchronizer.event_data with (NOLOCK)
------				 where event_status_cr_fk = 
------				 epacube.getCodeRefId('EVENT_STATUS','APPROVED'))       
        
		delete synchronizer.event_data
        where  event_status_cr_fk  = 
				 epacube.getCodeRefId('EVENT_STATUS','APPROVED')
---------------------------------------------------------------------------------------
----   Call other PURGE routines
---------------------------------------------------------------------------------------
---temp SUP-5575 changes
--SUP-5575
           insert into common.t_sql
		   (ts, sql_text)
		   values(getdate(), 'about to purge orphan prods')
EXEC epacube.UTIL_PURGE_ORPHAN_PRODUCTS

---temp SUP-5575 changes
--SUP-5575
           insert into common.t_sql
		   (ts, sql_text)
		   values(getdate(), 'orphans gone')


---removes orphaned enity structures.

delete from epacube.ENTITY_ASSOCIATION_ATTRIBUTE where RECORD_STATUS_CR_FK = 2
delete from epacube.ENTITY_ASSOCIATION where RECORD_STATUS_CR_FK = 2

delete from epacube.ENTITY_ASSOCIATION 
where CHILD_ENTITY_STRUCTURE_FK not in (
select entity_structure_fk from epacube.ENTITY_IDENTIFICATION with (nolock))

delete from epacube.ENTITY_ASSOCIATION 
where PARENT_ENTITY_STRUCTURE_FK not in (
select entity_structure_fk from epacube.ENTITY_IDENTIFICATION with (nolock)) 

delete from epacube.ENTITY_ATTRIBUTE
where ENTITY_STRUCTURE_FK not in (
select entity_structure_fk from epacube.ENTITY_IDENTIFICATION with (nolock)) 

delete from epacube.ENTITY_IDENT_NONUNIQUE where entity_structure_fk not in 
(select distinct entity_Structure_fk from epacube.entity_Identification
where entity_structure_fk > 1000)
and entity_Structure_fk > 1000

delete from epacube.segments_contacts where cust_entity_structure_fk not in 
(select distinct entity_Structure_fk from epacube.entity_Identification
where entity_structure_fk > 1000)
and cust_entity_Structure_fk > 1000

delete from epacube.segments_customer where  entity_structure_fk not in 
(select distinct entity_Structure_fk from epacube.entity_Identification
where entity_structure_fk > 1000)
and  entity_Structure_fk > 1000

delete from epacube.SEGMENTS_RULE_SCHEME where cust_entity_structure_fk not in 
(select distinct entity_Structure_fk from epacube.entity_Identification
where entity_structure_fk > 1000)
and CUST_ENTITY_STRUCTURE_FK > 1000

--delete from epacube.entity_structure where entity_structure_id not in 
--(select distinct entity_Structure_fk from epacube.entity_Identification
--where entity_structure_fk > 1000)
--and entity_Structure_id > 1000


---------------------------------------------------------------------------------------

EXEC [epacube].[EXECUTE_UTIL_PURGE_SQL] 

---------------------------------------------------------------------------------------
----   End of Processing Nightly Purge
---------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of SYNCHRONIZER.NIGHTLY_PURGE';
     --Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.NIGHTLY_PURGE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


        -- EXEC exec_monitor.Report_Error_sp @l_exec_no;
        declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
END








