﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create entities JIRA (URM-1201)
-- 1. Parse the JSON object into temp table and apply updates
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[DATA_VALUE_UPDATE]
	@data_value NVARCHAR(MAX)
	,@user VARCHAR(64)
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	
	SET NOCOUNT ON;

	--***************************************************************************************************************************************
	--Example entity structure
	--{
	--	"DATA_VALUE_ID": 101,
	--	"VALUE": "101",
	--	"DESCRIPTION": "SALAD DRSS-ENV-WF DIP MIX 1 OZ",
	--	"INSERT_MISSING_VALUES": 1
	--}
	--***************************************************************************************************************************************

    --Initialize log
	SET @ls_stmt = 'Started execution of epacube.DATA_VALUE_UPDATE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		IF OBJECT_ID('tempdb.dbo.#Data_Value', 'U') IS NOT NULL
			DROP TABLE #Data_Value;
		
		SELECT *
		INTO #Data_Value
		FROM OPENJSON(@data_value)
			WITH (
				DATA_VALUE_ID BIGINT '$.DATA_VALUE_ID',
				[VALUE] VARCHAR(256) '$.VALUE',
				[DESCRIPTION] VARCHAR(256) '$.DESCRIPTION',
				INSERT_MISSING_VALUES SMALLINT '$.INSERT_MISSING_VALUES'
			)
		
		BEGIN TRANSACTION;
		 
			UPDATE dv
			SET dv.[VALUE] = src.[VALUE]
				, dv.[DESCRIPTION] = src.[DESCRIPTION]
				, dv.UPDATE_TIMESTAMP = CONVERT(CHAR(23), GETDATE(), 121)
				, dv.UPDATE_USER = @user
			FROM epacube.DATA_VALUE dv 
				INNER JOIN #Data_Value src ON dv.DATA_VALUE_ID = src.DATA_VALUE_ID;

			UPDATE dn
			SET dn.INSERT_MISSING_VALUES = src.INSERT_MISSING_VALUES
				, dn.UPDATE_TIMESTAMP = CONVERT(CHAR(23), GETDATE(), 121)
				, dn.UPDATE_USER = @user
			FROM epacube.DATA_NAME dn 
				INNER JOIN epacube.DATA_VALUE dv ON dn.DATA_NAME_ID = dv.DATA_NAME_FK
				INNER JOIN #Data_Value src ON dv.DATA_VALUE_ID = src.DATA_VALUE_ID;

		COMMIT TRANSACTION;

		SET @ls_stmt = 'Finished execution of epacube.DATA_VALUE_UPDATE.'; 
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 
						
		SET @ls_stmt = 'Execution of epacube.DATA_VALUE_UPDATE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

	END CATCH
END
