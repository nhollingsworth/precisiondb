﻿-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Copy taxonomy node JIRA (PRE-140)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[TAXONOMY_NODE_COPY]
	@taxonomy_tree_id INT
	,@taxonomy_node_id INT
	,@parent_taxonomy_node_id INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @data_value_id_local BIGINT;
	DECLARE @taxonomy_node_id_local INT;
	DECLARE @json VARCHAR(MAX);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.TAXONOMY_NODE_COPY.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT @data_value_id_local = DATA_VALUE_FK
		FROM epacube.TAXONOMY_NODE
		WHERE TAXONOMY_NODE_ID = @taxonomy_node_id;
		
		BEGIN TRANSACTION;
				
			INSERT INTO epacube.TAXONOMY_NODE (TAXONOMY_TREE_FK, PARENT_TAXONOMY_NODE_FK, DATA_VALUE_FK)
			SELECT @taxonomy_tree_id, @parent_taxonomy_node_id, @data_value_id_local;

			SELECT @taxonomy_node_id_local = SCOPE_IDENTITY();

		COMMIT TRANSACTION;
		
		SET @json = (
			SELECT (
				SELECT TAXONOMY_NODE_ID
					, [VALUE] NODE_NAME
					, 0 PRODUCT_COUNT
					, 0 SUB_PRODUCT_COUNT
					, '[]' CHILDREN
				FROM epacube.TAXONOMY_NODE TN
					INNER JOIN epacube.DATA_VALUE DV
						ON TN.DATA_VALUE_FK = DV.DATA_VALUE_ID
				WHERE TAXONOMY_NODE_ID = @taxonomy_node_id_local
				FOR JSON PATH) [DATA]
			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER);

		SELECT REPLACE(@json, '"[]"', '[]');

		SET @ls_stmt = 'Finished execution of epacube.TAXONOMY_NODE_COPY.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 

		SET @ls_stmt = 'Execution of epacube.TAXONOMY_NODE_COPY has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END