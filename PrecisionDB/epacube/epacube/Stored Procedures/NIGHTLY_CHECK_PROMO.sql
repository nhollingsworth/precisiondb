﻿






-- Copyright 2006 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        04/30/2014   Initial SQL Version
-- CV        05/08/2014   Seperate insert to event for each data name
-- CV        06/11/2014   tightened down to check data not already in events.
-- CV        02/03/2015   added temp table as a first pass.



CREATE PROCEDURE [epacube].[NIGHTLY_CHECK_PROMO] 
AS
BEGIN
        DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @job_timestamp DATETIME

		
		

        BEGIN TRY
            SET NOCOUNT ON ;
            SET @l_exec_no = 0
            SET @l_sysdate = GETDATE()
										  

							
            SET @ls_stmt = 'started execution of epacube.Nightly Create_Promo. '
                 EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;


--------------------------------------------------------------------------------------------------------------
-----Added because we are getting duplicates in the future table.. which causes a mess down stream
--------------------------------------------------------------------------------------------------------------

                
update  marginmgr.SHEET_RESULTS_VALUES_FUTURE
set RECORD_STATUS_CR_FK = 3
where SHEET_RESULTS_VALUES_FUTURE_ID in (
  select a.SHEET_RESULTS_VALUES_FUTURE_ID
  from (
      select pas.SHEET_RESULTS_VALUES_FUTURE_ID,
      DENSE_RANK() over (Partition By pas.product_structure_Fk, 
                                     
									  pas.org_entity_structure_fk,
									  pas.data_name_fk
									 ,pas.effective_date
									 ,pas.end_date
									 --,pas.calc_job_fk
									 ,pas.net_value1
									 ,pas.net_value2
									 ,pas.net_value3
									 ,pas.net_value4
									 ,pas.net_value5
									 ,pas.net_value6
      Order by pas.sheet_results_values_future_id asc ) as drank
      from  marginmgr.SHEET_RESULTS_VALUES_FUTURE pas with (nolock)
	  where 1=1

  ) a
  where a.drank > 1
)



delete from marginmgr.SHEET_RESULTS_VALUES_FUTURE where RECORD_STATUS_CR_FK = 3
------------------------------------------------------------------------------------------------------------
DECLARE @v_Future_eff_days int
DECLARE @v_batch_no   bigint

SET @v_FUTURE_EFF_DAYS = 
(select value from epacube.epacube_params where name = 'FUTURE_EFF_DAYS')

EXEC seq_manager.db_get_next_sequence_value_impl 'common','batch_seq', @v_batch_no output

--------------------------------------------------------------------------------------------------------------
--Add a check to see if there are any Promo's or not if Not do not do this event creation.
--------------------------------------------------------------------------------------------------------------
-----for testing
--	select * from marginmgr.SHEET_RESULTS_VALUES_FUTURE
--where PRODUCT_STRUCTURE_FK =7538 and ORG_ENTITY_STRUCTURE_FK = 11793					
--and DATA_NAME_FK in (  100100500)	

--select * from marginmgr.SHEET_RESULTS_VALUES_FUTURE
--where PRODUCT_STRUCTURE_FK = 7538 and ORG_ENTITY_STRUCTURE_FK = 11793				
--and DATA_NAME_FK in (  100100500)	
----4365 4367
----4369


--drop temp table if it exists
	IF object_id('tempdb..#TS_PRODUCT_PROMO') is not null
	   drop table #TS_PRODUCT_PROMO;
	   
	   
	-- create temp table
	CREATE TABLE #TS_PRODUCT_PROMO(
	SHEET_RESULTS_VALUES_FUTURE_ID BIGINT,
	[DATA_NAME_FK] [int] NULL,
	[PRODUCT_STRUCTURE_FK] [bigint] NULL,
	[ORG_ENTITY_STRUCTURE_FK] [bigint] NULL,
	[EFFECTIVE_DATE] [date] NULL,
	[END_DATE] [date] NULL,
	[NET_VALUE1] [numeric](18, 6) NULL,
	[NET_VALUE2] [numeric](18, 6) NULL,
	[VALUE_UOM_CODE_FK] [int] NULL,
	[ASSOC_UOM_CODE_FK] [int] NULL,
	[UOM_CONVERSION_FACTOR] [numeric](18, 6) NULL,
	[VALUE_EFFECTIVE_DATE] [datetime] NULL,
	[PREV_PRODUCT_UOM_CLASS_FK] [bigint] NULL,
	[PREV_EFFECTIVE_DATE] [datetime] NULL,
	[PREV_END_DATE] [datetime] NULL,
	[PREV_UPDATE_TIMESTAMP] [datetime] NULL,
	[RECORD_STATUS_CR_FK] [int] NULL,
	[CREATE_TIMESTAMP] [datetime] NULL,
	[UPDATE_TIMESTAMP] [datetime] NULL)



INSERT INTO #TS_PRODUCT_PROMO(
SHEET_RESULTS_VALUES_FUTURE_ID,
                  [DATA_NAME_FK] ,
	[PRODUCT_STRUCTURE_FK],
	[ORG_ENTITY_STRUCTURE_FK] ,
	[EFFECTIVE_DATE],
	[END_DATE] ,
	[NET_VALUE1] ,
	[NET_VALUE2] ,
	[VALUE_UOM_CODE_FK],
	[ASSOC_UOM_CODE_FK],
	[UOM_CONVERSION_FACTOR] ,
	[VALUE_EFFECTIVE_DATE] 
	)
( 
select  f.SHEET_RESULTS_VALUES_FUTURE_ID, f.[DATA_NAME_FK] ,
	f.[PRODUCT_STRUCTURE_FK],
	f.[ORG_ENTITY_STRUCTURE_FK] ,
	f.[EFFECTIVE_DATE],
	f.[END_DATE] ,
	f.[NET_VALUE1] ,
	f.[NET_VALUE2] ,
	f.[VALUE_UOM_CODE_FK],
	f.[ASSOC_UOM_CODE_FK],
	f.[UOM_CONVERSION_FACTOR] ,
	f.[VALUE_EFFECTIVE_DATE] 

  from marginmgr.SHEET_RESULTS_VALUES_FUTURE  F
	inner Join [marginmgr].[SHEET_RESULTS] SRV with (nolock) 
	on F.data_name_fk = SRV.DATA_NAME_FK
	and F.PRODUCT_STRUCTURE_FK = SRV.PRODUCT_STRUCTURE_FK
	and F.ORG_ENTITY_STRUCTURE_FK = srv.ORG_ENTITY_STRUCTURE_FK
	--and cast(f.Effective_Date as Date) = cast(SRV.EFFECTIVE_DATE as date)
	and F.NET_VALUE1 = srv.NET_VALUE1
	and F.NET_VALUE2 = srv.NET_VALUE2
	and cast(F.END_DATE as date) = cast(srv.END_DATE as Date)
		where 1 = 1
AND SRV.DATA_NAME_FK =  100100500	
AND F.DATA_NAME_FK =  100100500	
and F.RECORD_STATUS_CR_FK = 1
AND utilities.TRUNC(F.EFFECTIVE_DATE) - @v_FUTURE_EFF_DAYS  = srv.EFFECTIVE_DATE)

--------------------------------------------------------------------------------------------------

-----100100500	JS_PROMO_SX_RESULTS	Promo SX Results


INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           
           ,[NET_VALUE1_NEW]
           ,[NET_VALUE2_NEW]
        
           ,[VALUE_UOM_CODE_FK]
           ,[ASSOC_UOM_CODE_FK]
           ,[UOM_CONVERSION_FACTOR]
		   ,[END_DATE_NEW]
           
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[TABLE_ID_FK]
         ,BATCH_NO
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
  (SELECT distinct 152
  ,10109
  ,getdate()
  ,F.PRODUCT_STRUCTURE_FK
  ,F.DATA_NAME_FK
  ,F.PRODUCT_STRUCTURE_FK
  ,F.ORG_ENTITY_STRUCTURE_FK
  
  ,F.NET_VALUE1
  ,F.NET_VALUE2
 
  ,F.VALUE_UOM_CODE_FK
  ,F.ASSOC_UOM_CODE_FK
  ,F.UOM_CONVERSION_FACTOR
  ,F.END_DATE
  ,97
  ,88
  ,66
  ,71
  ,51
  ,SHEET_RESULTS_VALUES_FUTURE_ID
  ,@v_batch_no
    ,getdate()
  ,'NIGHTLY_CHECK_PROMO'
    from #TS_PRODUCT_PROMO  F
	 
where f.SHEET_RESULTS_VALUES_FUTURE_ID in 
	(SELECT  A.SHEET_RESULTS_VALUES_FUTURE_ID
			FROM (
                SELECT
                FSR.SHEET_RESULTS_VALUES_FUTURE_ID,
				( DENSE_RANK() OVER ( PARTITION BY  fSR.Product_STRUCTURE_FK, FSR.org_entity_structure_fk, FSR.data_name_fk
											ORDER BY fsr.net_value1 asc, fsr.net_Value2 asc ) 
                     ) AS DRANK
					from #TS_PRODUCT_PROMO FSR WITH (NOLOCK) 
					
where FSR.DATA_NAME_FK =  100100500	
and FSR.RECORD_STATUS_CR_FK = 1
--AND utilities.TRUNC(FSR.EFFECTIVE_DATE) - @v_FUTURE_EFF_DAYS >= utilities.TRUNC(GETDATE())
AND FSR.SHEET_RESULTS_VALUES_FUTURE_ID not in (select distinct TABLE_ID_FK from synchronizer.event_data 
where DATA_NAME_FK =  100100500 and UPDATE_USER = 'NIGHTLY_CHECK_PROMO')
)A
  WHERE A.DRANK = 1 
)
)

  





--------------------------------------------------------------------------------------

----100100520	JS_PROMO_END_USER	Promo End User Group

--drop temp table if it exists
	IF object_id('tempdb..#TS_PRODUCT_PROMO_END') is not null
	   drop table #TS_PRODUCT_PROMO_END;
	   
	   
	-- create temp table
	CREATE TABLE #TS_PRODUCT_PROMO_END(
	SHEET_RESULTS_VALUES_FUTURE_ID BIGINT,
	[DATA_NAME_FK] [int] NULL,
	[PRODUCT_STRUCTURE_FK] [bigint] NULL,
	[ORG_ENTITY_STRUCTURE_FK] [bigint] NULL,
	[EFFECTIVE_DATE] [date] NULL,
	[END_DATE] [date] NULL,
	[NET_VALUE1] [numeric](18, 6) NULL,
	[NET_VALUE2] [numeric](18, 6) NULL,
	[VALUE_UOM_CODE_FK] [int] NULL,
	[ASSOC_UOM_CODE_FK] [int] NULL,
	[UOM_CONVERSION_FACTOR] [numeric](18, 6) NULL,
	[VALUE_EFFECTIVE_DATE] [datetime] NULL,
	[PREV_PRODUCT_UOM_CLASS_FK] [bigint] NULL,
	[PREV_EFFECTIVE_DATE] [datetime] NULL,
	[PREV_END_DATE] [datetime] NULL,
	[PREV_UPDATE_TIMESTAMP] [datetime] NULL,
	[RECORD_STATUS_CR_FK] [int] NULL,
	[CREATE_TIMESTAMP] [datetime] NULL,
	[UPDATE_TIMESTAMP] [datetime] NULL)



INSERT INTO #TS_PRODUCT_PROMO_END(
SHEET_RESULTS_VALUES_FUTURE_ID,
                  [DATA_NAME_FK] ,
	[PRODUCT_STRUCTURE_FK],
	[ORG_ENTITY_STRUCTURE_FK] ,
	[EFFECTIVE_DATE],
	[END_DATE] ,
	[NET_VALUE1] ,
	[NET_VALUE2] ,
	[VALUE_UOM_CODE_FK],
	[ASSOC_UOM_CODE_FK],
	[UOM_CONVERSION_FACTOR] ,
	[VALUE_EFFECTIVE_DATE] 
	)
( 
select  f.SHEET_RESULTS_VALUES_FUTURE_ID, f.[DATA_NAME_FK] ,
	f.[PRODUCT_STRUCTURE_FK],
	f.[ORG_ENTITY_STRUCTURE_FK] ,
	f.[EFFECTIVE_DATE],
	f.[END_DATE] ,
	f.[NET_VALUE1] ,
	f.[NET_VALUE2] ,
	f.[VALUE_UOM_CODE_FK],
	f.[ASSOC_UOM_CODE_FK],
	f.[UOM_CONVERSION_FACTOR] ,
	f.[VALUE_EFFECTIVE_DATE] 

  from marginmgr.SHEET_RESULTS_VALUES_FUTURE  F
	inner Join [marginmgr].[SHEET_RESULTS] SRV with (nolock) 
	on F.data_name_fk = SRV.DATA_NAME_FK
	and F.PRODUCT_STRUCTURE_FK = SRV.PRODUCT_STRUCTURE_FK
	and F.ORG_ENTITY_STRUCTURE_FK = srv.ORG_ENTITY_STRUCTURE_FK
	--and cast(f.Effective_Date as Date) = cast(SRV.EFFECTIVE_DATE as date)
	and F.NET_VALUE1 = srv.NET_VALUE1
	and F.NET_VALUE2 = srv.NET_VALUE2
	and cast(F.END_DATE as date) = cast(srv.END_DATE as Date)
		where 1 = 1
AND SRV.DATA_NAME_FK =  100100520	
AND F.DATA_NAME_FK =  100100520	
and F.RECORD_STATUS_CR_FK = 1
AND utilities.TRUNC(F.EFFECTIVE_DATE) -@v_FUTURE_EFF_DAYS  = srv.EFFECTIVE_DATE)



INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           
           ,[NET_VALUE1_NEW]
           ,[NET_VALUE2_NEW]
        
           ,[VALUE_UOM_CODE_FK]
           ,[ASSOC_UOM_CODE_FK]
           ,[UOM_CONVERSION_FACTOR]
		   ,[END_DATE_NEW]
           
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[TABLE_ID_FK]
         ,BATCH_NO
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
  (SELECT distinct 152
  ,10109
  ,getdate()
  ,F.PRODUCT_STRUCTURE_FK
  ,F.DATA_NAME_FK
  ,F.PRODUCT_STRUCTURE_FK
  ,F.ORG_ENTITY_STRUCTURE_FK
  
  ,F.NET_VALUE1
  ,F.NET_VALUE2
 
  ,F.VALUE_UOM_CODE_FK
  ,F.ASSOC_UOM_CODE_FK
  ,F.UOM_CONVERSION_FACTOR
  ,F.END_DATE
  ,97
  ,88
  ,66
  ,71
  ,51
  ,SHEET_RESULTS_VALUES_FUTURE_ID
  ,@v_batch_no
    ,getdate()
  ,'NIGHTLY_CHECK_PROMO'
    from #TS_PRODUCT_PROMO_END  F
	 
where f.SHEET_RESULTS_VALUES_FUTURE_ID in 
	(SELECT  A.SHEET_RESULTS_VALUES_FUTURE_ID
			FROM (
                SELECT
                FSR.SHEET_RESULTS_VALUES_FUTURE_ID,
				( DENSE_RANK() OVER ( PARTITION BY  fSR.Product_STRUCTURE_FK, FSR.org_entity_structure_fk, FSR.data_name_fk
											ORDER BY fsr.net_value1 asc, fsr.net_Value2 asc ) 
                     ) AS DRANK
					from #TS_PRODUCT_PROMO_END FSR WITH (NOLOCK) 
					
where FSR.DATA_NAME_FK =  100100520	
and FSR.RECORD_STATUS_CR_FK = 1
--AND utilities.TRUNC(FSR.EFFECTIVE_DATE) - @v_FUTURE_EFF_DAYS >= utilities.TRUNC(GETDATE())
AND FSR.SHEET_RESULTS_VALUES_FUTURE_ID not in (select distinct TABLE_ID_FK from synchronizer.event_data 
where DATA_NAME_FK =  100100520 and UPDATE_USER = 'NIGHTLY_CHECK_PROMO')
)A
  WHERE A.DRANK = 1 
)
)

  


----------------------------------------------------------------------------------
-- CALL APPROVE EVENTS           
------------------------------------------------------------------------------------

EXECUTE [synchronizer].[EVENT_POSTING]   @v_batch_no

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

            SET @status_desc = 'finished execution of epacube.Nightly Check_Promo'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;

        END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.Nightly Check Promo has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            EXEC exec_monitor.email_error_sp @l_exec_no,
                'dbexceptions@epacube.com' ;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH
		  
    END







