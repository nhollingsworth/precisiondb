﻿


-- Copyright 2011
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: To Remove data once exported
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        12/03/2012   Initial SQL Version

--
CREATE PROCEDURE [epacube].[Z_PAXSON_REMOVE_PRIORUPC_AFTER_EXPORT] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @ls_exec2            varchar(100)
DECLARE  @ls_exec3             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint
DECLARE @test varchar(50)

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime
DECLARE  @v_import_name       varchar (50)
DECLARE  @v_schema_name       varchar (50)
DECLARE  @v_Future_eff_days int

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.PURGE_INACTIVE_AFTER_EXPORT.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;


----------------------------------------------------------------------
---USED TO GET CHANGES EXPORT NAMES --
    -- first cursor --
----------------------------------------------------------------------

 DECLARE 
              @v_export_name           varchar(256)
              

      DECLARE  cur_v_export_name cursor local for
				SELECT  CNFJ.NAME 
					FROM epacube.CONFIG CNFJ WITH (NOLOCK)
					where  cnfj.CONFIG_CLASS_CR_FK= 10401
					and CONFIG_ID in (	select CONFIG_FK from epacube.CONFIG_DATA_NAME where DATA_NAME_FK =  4104001)




      
        OPEN cur_v_export_name;
        FETCH NEXT FROM cur_v_export_name INTO  
			  @v_export_name         
			  
			  											  
         WHILE @@FETCH_STATUS = 0 
         BEGIN
         
-------------------------------------------------------------------------------------                     										 
---Secound loop GET PRODUCTS THAT HAVE -999999999 
-- or NULL values and are a part of an export
---DELETE from epacube tables and event history if they have been exported  
------------------------------------------------------------------------------------------
   
   DECLARE 
             
              @vm$product_structure_fk     varchar(54),              
              @vm$entity_structure_fk        varchar(54),
              @vm$Org_entity_structure_fk         varchar(54),
              @vm$Event_id							int

      DECLARE  cur_v_Products cursor local for
							SELECT DISTINCT					 
							 
							 ed.product_structure_fk, 
							 isnull(ed.entity_structure_fk,0)
							 , ed.org_entity_structure_fk
							 ,event_id
							from synchronizer.EVENT_DATA_HISTORY ED
							where ed.DATA_NAME_FK = 4104007
							and ED.EVENT_STATUS_CR_FK = 81
							AND   ED.UPDATE_TIMESTAMP < ISNULL ( 
									( SELECT MAX ( JOB.JOB_COMPLETE_TIMESTAMP )
										FROM COMMON.JOB JOB WITH (NOLOCK)
										WHERE NAME = @v_export_name), '01/01/2009' )
										

OPEN cur_v_products;
        FETCH NEXT FROM cur_v_products INTO  
			               	
              @vm$product_structure_fk,                   
              @vm$entity_structure_fk,        
              @vm$Org_entity_structure_fk,
              @vm$Event_id
											  											  																			  
											  
											  
         WHILE @@FETCH_STATUS = 0 
         BEGIN
       

-------------------------------------------------------------------------------------

BEGIN

DELETE FROM epacube.PRODUCT_DESCRIPTION where PRODUCT_STRUCTURE_FK =@vm$product_structure_fk
AND data_name_fk =  4104007
						 				 
			 				 
 INSERT INTO COMMON.T_SQL
                ( TS,  SQL_TEXT )
            VALUES ( GETDATE(), ISNULL ( @ls_exec, 'Host data exported' ) )
            

   END
-------------------------------------------------------------------------------------
--Once data has been removed from epacube tables need to archive the event history 
-- so the event does not qualify next time around
-------------------------------------------------------------------------------------
	INSERT INTO synchronizer.EVENT_DATA_ARCHIVE (
		 EVENT_TYPE_CR_FK
		,EVENT_ENTITY_CLASS_CR_FK 
		,EPACUBE_ID    
		,IMPORT_JOB_FK     
		,IMPORT_PACKAGE_FK 
		,DATA_NAME_FK	
	    ,PRODUCT_STRUCTURE_FK
	    ,ORG_ENTITY_STRUCTURE_FK
	    ,ENTITY_STRUCTURE_FK    
		,ENTITY_CLASS_CR_FK     
		,ENTITY_DATA_NAME_FK    
        ,NEW_DATA         
        ,CURRENT_DATA     
		,DATA_VALUE_FK    
		,ENTITY_DATA_VALUE_FK
        ,NET_VALUE1_NEW      
        ,NET_VALUE2_NEW      
        ,NET_VALUE3_NEW      
        ,NET_VALUE4_NEW      
        ,NET_VALUE5_NEW      
        ,NET_VALUE6_NEW      
        --,NET_VALUE7_NEW      
        --,NET_VALUE8_NEW      
        --,NET_VALUE9_NEW      
        --,NET_VALUE10_NEW     
        ,NET_VALUE1_CUR      
        ,NET_VALUE2_CUR      
        ,NET_VALUE3_CUR     
        ,NET_VALUE4_CUR     
        ,NET_VALUE5_CUR     
        ,NET_VALUE6_CUR     
        --,NET_VALUE7_CUR     
        --,NET_VALUE8_CUR     
        --,NET_VALUE9_CUR     
        --,NET_VALUE10_CUR    
		,PERCENT_CHANGE1	 
		,PERCENT_CHANGE2	
		,PERCENT_CHANGE3	
		,PERCENT_CHANGE4	
		,PERCENT_CHANGE5	
		,PERCENT_CHANGE6	
		--,PERCENT_CHANGE7	
		--,PERCENT_CHANGE8	
		--,PERCENT_CHANGE9	
		--,PERCENT_CHANGE10
        ,VALUE_UOM_CODE_FK 
        ,ASSOC_UOM_CODE_FK 
		,UOM_CONVERSION_FACTOR 
		,END_DATE_NEW          
        ,END_DATE_CUR          
		,EFFECTIVE_DATE_CUR    
		,EVENT_EFFECTIVE_DATE 
		,VALUE_EFFECTIVE_DATE
        ,RELATED_EVENT_FK	
        ,RULES_FK			
		,EVENT_ACTION_CR_FK   
		,EVENT_STATUS_CR_FK   
		,EVENT_CONDITION_CR_FK
		,EVENT_PRIORITY_CR_FK   
		,EVENT_SOURCE_CR_FK   
		,RESULT_TYPE_CR_FK	 
		,SINGLE_ASSOC_IND     
		,TABLE_ID_FK          
		,BATCH_NO			
		,IMPORT_BATCH_NO     
        ,JOB_CLASS_FK		
        ,IMPORT_DATE         
        ,IMPORT_FILENAME     
        ,STG_RECORD_FK       
		,RECORD_STATUS_CR_FK 
        ,UPDATE_TIMESTAMP	
        ,UPDATE_USER		    
     )
SELECT 
		 EVENT_TYPE_CR_FK
		,EVENT_ENTITY_CLASS_CR_FK 
		,EPACUBE_ID    
		,IMPORT_JOB_FK     
		,IMPORT_PACKAGE_FK 
		,DATA_NAME_FK	
	    ,PRODUCT_STRUCTURE_FK
	    ,ORG_ENTITY_STRUCTURE_FK
	    ,ENTITY_STRUCTURE_FK    
		,ENTITY_CLASS_CR_FK     
		,ENTITY_DATA_NAME_FK    
        ,NEW_DATA         
        ,CURRENT_DATA     
		,DATA_VALUE_FK    
		,ENTITY_DATA_VALUE_FK
        ,NET_VALUE1_NEW      
        ,NET_VALUE2_NEW      
        ,NET_VALUE3_NEW      
        ,NET_VALUE4_NEW      
        ,NET_VALUE5_NEW      
        ,NET_VALUE6_NEW      
        --,NET_VALUE7_NEW      
        --,NET_VALUE8_NEW      
        --,NET_VALUE9_NEW      
        --,NET_VALUE10_NEW     
        ,NET_VALUE1_CUR      
        ,NET_VALUE2_CUR      
        ,NET_VALUE3_CUR     
        ,NET_VALUE4_CUR     
        ,NET_VALUE5_CUR     
        ,NET_VALUE6_CUR     
        --,NET_VALUE7_CUR     
        --,NET_VALUE8_CUR     
        --,NET_VALUE9_CUR     
        --,NET_VALUE10_CUR    
		,PERCENT_CHANGE1	 
		,PERCENT_CHANGE2	
		,PERCENT_CHANGE3	
		,PERCENT_CHANGE4	
		,PERCENT_CHANGE5	
		,PERCENT_CHANGE6	
		--,PERCENT_CHANGE7	
		--,PERCENT_CHANGE8	
		--,PERCENT_CHANGE9	
		--,PERCENT_CHANGE10
        ,VALUE_UOM_CODE_FK 
        ,ASSOC_UOM_CODE_FK 
		,UOM_CONVERSION_FACTOR 
		,END_DATE_NEW          
        ,END_DATE_CUR          
		,EFFECTIVE_DATE_CUR    
		,EVENT_EFFECTIVE_DATE 
		,VALUE_EFFECTIVE_DATE
        ,RELATED_EVENT_FK	
        ,RULES_FK			
		,EVENT_ACTION_CR_FK   
		,EVENT_STATUS_CR_FK   
		,EVENT_CONDITION_CR_FK
		,EVENT_PRIORITY_CR_FK   
		,EVENT_SOURCE_CR_FK   
		,RESULT_TYPE_CR_FK	 
		,SINGLE_ASSOC_IND     
		,TABLE_ID_FK          
		,BATCH_NO			
		,IMPORT_BATCH_NO     
        ,JOB_CLASS_FK		
        ,IMPORT_DATE         
        ,IMPORT_FILENAME     
        ,STG_RECORD_FK       
		,RECORD_STATUS_CR_FK 
        ,UPDATE_TIMESTAMP	
        ,UPDATE_USER	
         FROM synchronizer.event_data_history 
         where EVENT_ID = @vm$Event_id
                   
										

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows archived into synchronizer.event_data_archive table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END                        

												
					DELETE FROM synchronizer.event_data_history	 
					where EVENT_ID = @vm$Event_id	
					 

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from synchronizer.event_data_history table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END                        

            
             

FETCH NEXT FROM cur_v_products INTO 	
											  @vm$product_structure_fk,                   
											  @vm$entity_structure_fk,        
											  @vm$Org_entity_structure_fk,
											  @vm$Event_id


     END --cur_v_data_name LOOP
     CLOSE cur_v_products;
	 DEALLOCATE cur_v_products; 
	 
--End second loop	 
------------------------------------------------------------------------------------------             
   

        FETCH NEXT FROM cur_v_export_name INTO  @v_export_name           
											  


     END --cur_v_data_name LOOP
     CLOSE cur_v_export_name;
	 DEALLOCATE cur_v_export_name; 
     
--End first loop

   
    

----------------------------------------------------------------------
---USED TO GET CHANGES EXPORT NAMES --
    -- first cursor --
----------------------------------------------------------------------

              
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of EPACUBE.PAXSON_REMOVE_PRIORUPC_AFTER_EXPORT'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of EPACUBE.PAXSON_REMOVE_PRIORUPC_AFTER_EXPORT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END