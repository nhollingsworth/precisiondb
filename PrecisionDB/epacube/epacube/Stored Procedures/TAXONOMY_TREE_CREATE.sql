﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create new taxonomy tree JIRA (URM-1209)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[TAXONOMY_TREE_CREATE]
	@data_value_id BIGINT = NULL
	,@taxonomy_tree_name VARCHAR(256) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @data_value_id_local BIGINT;

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.TAXONOMY_TREE_CREATE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		IF @data_value_id IS NOT NULL
			BEGIN

				BEGIN TRANSACTION; 

					INSERT INTO epacube.TAXONOMY_TREE (DATA_VALUE_FK)
					SELECT @data_value_id;

				COMMIT TRANSACTION;
			
			END;

		ELSE
			BEGIN 
				BEGIN TRANSACTION; 

					INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, [VALUE], [DESCRIPTION])
					SELECT 190901, @taxonomy_tree_name, @taxonomy_tree_name;

					SELECT @data_value_id_local = SCOPE_IDENTITY();

					INSERT INTO epacube.TAXONOMY_TREE (DATA_VALUE_FK)
					SELECT @data_value_id_local;

				COMMIT TRANSACTION;
			END;

		SELECT (
			SELECT TT.TAXONOMY_TREE_ID
				, DV.[VALUE] [NAME]
			FROM epacube.TAXONOMY_TREE TT
				INNER JOIN epacube.DATA_VALUE DV
					ON TT.DATA_VALUE_FK = DV.DATA_VALUE_ID
			WHERE TT.RECORD_STATUS_CR_FK = 1
			FOR JSON PATH) CHILDREN
		FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		
		SET @ls_stmt = 'Finished execution of epacube.TAXONOMY_TREE_CREATE.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 

		SET @ls_stmt = 'Execution of epacube.TAXONOMY_TREE_CREATE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END
