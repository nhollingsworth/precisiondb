﻿




CREATE PROCEDURE [epacube].[UI_Component_Complete] @UI_Comp_Master_FK Varchar(16)

as 

--/***********************************************************
--  Written By  : Gary Stone			2017
--*/----------------------------------------------------------

set nocount on

--Declare @UI_Comp_Master_FK Varchar(16) = 21506

	If object_id('tempdb..#DLevels') is not null
	drop table #DLevels

	Create table #DLevels(UI_Comp_Master_FK bigint Not Null, data_name_fk bigint not null, data_level_cr_fk int not null, data_level varchar(64) not null, table_schema varchar(64) not null, table_name varchar(98) not null, Precedence int null, Entity_Class_CR_FK int not null)

	Insert into #DLevels
	Select @UI_Comp_Master_FK, r.data_name_fk, r.data_level_cr_fk, cr.code 'data_level', r.table_schema, r.table_name, cr.PRECEDENCE, r.Entity_Class_CR_FK
	from EPACUBE.DATA_NAME_GROUP_REFS R
	inner join epacube.code_ref cr with (nolock)  on isnull(r.Data_Level_CR_FK, 0) = isnull(cr.CODE_REF_ID, 0)
	where 1 = 1
	and data_level_cr_FK is not null 
	and data_name_fk in (select data_name_fk from epacube.UI_COMP where UI_Comp_Master_FK = @UI_Comp_Master_FK)
	and entity_class_cr_fk in (Select ds.entity_class_cr_fk from epacube.data_set ds with (nolock)
							inner join epacube.data_name dn with (nolock) on ds.data_set_id = dn.DATA_SET_FK
							where ltrim(rtrim(table_name)) not in ('SEGMENTS_CONTACTS')
							and dn.data_name_id in (select data_name_fk from epacube.UI_COMP UC where ui_comp_master_fk = @UI_Comp_Master_FK group by data_name_fk) group by ds.entity_class_cr_fk)
	and r.table_name <> 'PRODUCT_ATTRIBUTE'
	group by  r.data_name_fk, r.data_level_cr_fk, cr.code, r.table_schema, r.table_name, cr.PRECEDENCE, r.Entity_Class_CR_FK

	If object_id('tempdb..#CompDataLevel') is not null
	drop table #CompDataLevel

	Create table #CompDataLevel(Data_Name_ID bigint Null, DATA_LEVEL_CR_FK int null)

	Declare @DN_FK varchar(64), @Table_Schema Varchar(64), @TblName varchar(64), @Data_Level_CR_FK Varchar(64), @UI_COMP_ID Varchar(16), @SQL varchar(Max)

	DECLARE CompBldr Cursor local for
	Select data_name_fk, table_schema, table_name
	from #DLevels 
	group by data_name_fk, table_schema, table_name order by data_name_fk


				OPEN CompBldr;
					FETCH NEXT FROM CompBldr INTO @DN_FK, @Table_Schema, @TblName
					WHILE @@FETCH_STATUS = 0
			Begin

			If @TblName in ('SETTINGS_POS_PROD_STORE_HEADER', ' EPACUBE.[SETTINGS_POS_PROD_STORE_HEADER]')
				Begin
					Set @SQL = ' Insert into #CompDataLevel
								Select * from
								(
								 Select top 1 ' + @DN_FK + ' Data_Name_FK, Data_Level_CR_FK ' + ' from [epacube].[SETTINGS_POS_PROD_STORE_HEADER] sppsh with (nolock)
											inner join [epacube].[SETTINGS_POS_PROD_STORE_VALUES] sppsv with (nolock) on sppsh.[SETTINGS_POS_PROD_STORE_HEADER_ID] = sppsv.[SETTINGS_POS_PROD_STORE_HEADER_FK]
											where sppsv.data_name_fk = ' + @DN_FK
															+ ' and sppsh.data_level_cr_fk is not null) a'
				End
				Else
				Begin
					Set @SQL = ' Insert into #CompDataLevel Select ' + @DN_FK + ' Data_Name_FK, Data_Level_CR_FK ' + ' from [' + @Table_Schema + '].[' + @TblName + '] where data_name_fk = ' + @DN_FK
								+ ' and data_level_cr_fk is not null Group by data_name_fk, data_level_cr_fk'
				End


			eXEC(@SQL)


		FETCH NEXT FROM CompBldr INTO @DN_FK, @Table_Schema, @TblName
		End

		Close CompBldr;
		Deallocate CompBldr;

	Delete UCL from [epacube].[UI_COMP_LEVELS] ucl 
	LEFT JOIN #CompDataLevel cdl ON UCL.DATA_NAME_FK = CDL.DATA_NAME_ID AND UCL.DATA_LEVEL_CR_FK = CDL.DATA_LEVEL_CR_FK
	where UCL.UI_COMP_MASTER_FK = UI_Comp_Master_FK
			AND CDL.DATA_NAME_ID IS NULL
			AND CDL.DATA_LEVEL_CR_FK IS NULL
			and ucl.UI_COMP_MASTER_FK = @UI_COMP_MASTER_FK

	Insert into [epacube].[UI_COMP_LEVELS]
	(UI_Comp_FK, UI_Comp_Master_FK, Data_Name_FK, Data_Level_CR_FK, entity_class_cr_fk, Record_Status_CR_FK)
	Select UIC.UI_Comp_ID
	, UIC.UI_Comp_Master_FK
	, UIC.Data_Name_FK
	, r.Data_Level_CR_FK
	, R.entity_class_cr_fk
	, 1 Record_Status_CR_FK		
	from #DLevels R
	inner join [epacube].[UI_COMP] UIC on R.data_name_FK = UIC.DATA_NAME_FK
	left join #CompDataLevel DL on R.data_name_FK = DL.Data_Name_ID and R.data_level_cr_fk = dl.DATA_LEVEL_CR_FK
	LEFT JOIN EPACUBE.UI_COMP_LEVELS UCL ON UIC.DATA_NAME_FK = UCL.DATA_NAME_FK AND DL.DATA_LEVEL_CR_FK = UCL.DATA_LEVEL_CR_FK and uic.UI_COMP_MASTER_FK = ucl.UI_COMP_MASTER_FK
	where uic.UI_COMP_MASTER_FK = @UI_COMP_MASTER_FK
		AND UCL.UI_COMP_LEVELS_ID IS NULL
	Order by UIC.data_name_FK, isnull(r.precedence, 0) desc

	Update uc
	Set editable = Case when isnull(dn.system_protected, 0) = 0 and isnull(dnv.VIRTUAL_ACTION_TYPE_CR_FK, 0) in (0, 10512, 10513) then 1 else 0 end
	from epacube.ui_comp uc
	inner join epacube.data_name dn on uc.DATA_NAME_FK = dn.DATA_NAME_ID
	left join epacube.data_name_virtual dnv on dn.data_name_id = dnv.DATA_NAME_FK
	where uc.UI_COMP_MASTER_FK = @UI_COMP_MASTER_FK

--Put in place for advisory board meeting
	Update UCL
	Set record_status_cr_fk = 2
	from epacube.ui_comp_levels ucl 
	where ui_comp_master_fk in (21338, 21336) 
	and data_level_cr_fk not in (507, 509, 501)

	update uc
	set editable = case when data_name_fk in (500001, 550001) then 1 else 0  end
	from epacube.ui_comp uc where ui_comp_master_fk = 21335

	update uc
	set editable = case when data_name_fk in (503201) then 1 else 0 end
	from epacube.ui_comp uc where ui_comp_master_fk = 21336
--Put in place for advisory board meeting

--temp update until UI handles

	Update UCM
	Set Record_status_cr_fk = 1
	from epacube.ui_comp_master UCM where ui_comp_master_id = @UI_Comp_Master_FK

