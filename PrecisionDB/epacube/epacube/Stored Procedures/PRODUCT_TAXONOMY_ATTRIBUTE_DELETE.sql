﻿
-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Delete product attribute (PRE-110)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[PRODUCT_TAXONOMY_ATTRIBUTE_DELETE]
	@PRODUCT_TAX_ATTRIBUTE_ID BIGINT
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @DATA_NAME VARCHAR(64);

	SET NOCOUNT ON;

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.PRODUCT_TAX_ATTRIBUTE_DELETE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT @DATA_NAME = [LABEL]
		FROM epacube.PRODUCT_TAX_ATTRIBUTE PTA
			INNER JOIN epacube.DATA_NAME DN
				ON PTA.DATA_NAME_FK = DN.DATA_NAME_ID
		WHERE PTA.PRODUCT_TAX_ATTRIBUTE_ID = @PRODUCT_TAX_ATTRIBUTE_ID

		BEGIN TRANSACTION;

			--Delete selected product attribute
			DELETE 
			FROM epacube.PRODUCT_TAX_ATTRIBUTE
			WHERE PRODUCT_TAX_ATTRIBUTE_ID = @PRODUCT_TAX_ATTRIBUTE_ID;

		COMMIT TRANSACTION;

		SELECT 'Product attribute ' + @DATA_NAME + ' successfully deleted' USER_MSG, 1 SUCCESS;

		SET @ls_stmt = 'Finished execution of epacube.PRODUCT_TAX_ATTRIBUTE_DELETE.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 

		SET @ls_stmt = 'Execution of epacube.PRODUCT_TAX_ATTRIBUTE_DELETE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END