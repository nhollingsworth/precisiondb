﻿






CREATE PROCEDURE [epacube].[Z_PAXSON_CHECK_UPC_CHANGE]  ( @IN_JOB_FK bigint ) 
         
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        11/30/2012  Initial SQL Version
-- CV        01/4/2012   Added batch no for approve event 
-- CV        02/19/2013  check for duplicates and remove them
-- CV        09/23/2013  Added code block for changes of cat num and ucc at same time. 
-- CV        10/14/2013  Added call to ced analysis report
 
  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @v_batch_no   bigint
        
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT
        
 
 
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of epacube.PAXSON_CHECK_UPC_CHANGE. '
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT
											  ,@epa_job_id = @v_job_fk;
 

                
    ----              EXEC seq_manager.db_get_next_sequence_value_impl 'common',
    ----                'job_seq', @v_job_fk output
    
    ----SET @v_job_class_fk  = 100        --- PRODUCT MAINTENANCE
    ----SET @v_job_user = 'EPACUBE'
    ----SET @v_job_name = 'CED UPC CHANGE'
    ----SET @v_job_date = @l_sysdate
    ----SET @v_data1   = 'LAST JOB:: '
    ----                 + CAST ( ISNULL ( @v_last_job_fk, -999 )  AS VARCHAR(32) )
    ----                 + ' - '
    ----                 + CAST ( ISNULL ( @v_last_job_date, '01/01/2000' ) AS VARCHAR(64) )
    ----SET @v_data2   = @v_last_job_date
    
    ----EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
				----						   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

    ----EXEC common.job_execution_create   @v_job_fk, 'START CED NET CATALOG WITH UPC DIFF',
		  ----                               @v_input_rows, @v_output_rows, @v_exception_rows,
				----						 200, @l_sysdate, @l_sysdate  
  

                

---------------------------------------------------------------------------------------
----CREATE TEMP TABLE FOR PRODUCT LIST-----
---------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_UPC_CHANGE') is not null
	   drop table #TS_UPC_CHANGE;
	   
	      
					CREATE TABLE #TS_UPC_DIFF 
					 (PRODUCT_STRUCTURE_FK bigint Not NULL
					 ,ENTITY_STRUCTURE_FK bigint
					 ,CEDNETCATALOG varchar(256)
					 ,UPC Varchar(256)
					 ,JOB_FK int)


------------------------------------------------------------------------------
-----Inserts product structure for products that are missing CEDNETCATALOG
------------------------------------------------------------------------------
							
							insert into #TS_UPC_DIFF
							(PRODUCT_STRUCTURE_FK
							,ENTITY_STRUCTURE_FK 
							,CEDNETCATALOG 
							,UPC 
							,JOB_FK)
							------
							(select ed.PRODUCT_STRUCTURE_FK, 
							ed.ENTITY_STRUCTURE_FK,
							pd.DESCRIPTION, 
							ed.CURRENT_DATA, 
							@IN_JOB_FK
							from synchronizer.EVENT_DATA_HISTORY ed  WITH (NOLOCK)
							left join epacube.PRODUCT_DESCRIPTION pd WITH (NOLOCK)
							On (pd.PRODUCT_STRUCTURE_FK = ed.PRODUCT_STRUCTURE_FK
							and pd.DATA_NAME_FK = 4104001)  ---ced net description
							where ed.DATA_NAME_FK = 110102  --- upc event
							and ed.EVENT_ACTION_CR_FK = 52 ---change
							and ed.IMPORT_JOB_FK = @IN_JOB_FK
							)

	 SET @l_rows_processed = @@ROWCOUNT

	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows - with UPC Differences = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @v_job_fk
       END


------------------------------------------------------------------------------------------------
  -- Create events 
  ------------------------------------------------------------------------------------------------------ 
  --need batch to run approve event
  EXEC seq_manager.db_get_next_sequence_value_impl 'common','batch_seq', @v_batch_no output



INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
		   ,ENTITY_STRUCTURE_FK
          
           ,[NEW_DATA]
           ,[CURRENT_DATA]
                    
           ,[SEARCH1]
           ,[SEARCH2]
           ,[SEARCH3]
           ,[SEARCH4]
           ,[SEARCH5]
           ,[SEARCH6]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[EVENT_REASON_CR_FK]
           ,BATCH_NO
           ,[IMPORT_JOB_FK]
           
           ,[TABLE_ID_FK]
          
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
           ( SELECT 
							150
						   ,10109
						   ,GETDATE()
						   ,TMP.product_structure_fk
						   ,4104007	--------CED NET DESC UPC PRIOR
						   ,TMP.PRODUCT_STRUCTURE_FK
						   ,1
				          ,TMP.ENTITY_STRUCTURE_FK
           ,left(TMP.CEDNETCATALOG + space(34), 34) + ':' +  TMP.UPC --NEW DATA
           ,NULL --CURRENT_DATA, varchar,>
           
                 
           ,NULL  --SEARCH1, varchar(128),>
           ,NULL --<SEARCH2, varchar(128),>
           ,NULL --<SEARCH3, varchar(128),>
           ,NULL --SEARCH4, varchar(128),>
           ,NULL --<SEARCH5, varchar(128),>
           ,NULL --<SEARCH6, varchar(128),>
           ,97 --EVENT_CONDITION_CR_FK, int,>
           ,87 --EVENT_STATUS_CR_FK, int,>
           ,66  --EVENT_PRIORITY_CR_FK, int,>
           ,72  --EVENT_SOURCE_CR_FK, int,>
           ,51 --<EVENT_ACTION_CR_FK, int,>
           ,144  --EVENT_REASON_CR_FK, int,>
           ,@v_batch_no--batch_no
           ,TMP.JOB_FK
           
           
           ,NULL  --table id
         
           ,1
           ,GETDATE()
           ,'UPC CHANGE'
           from  #TS_UPC_DIFF TMP)


		SET @l_rows_processed = @@ROWCOUNT

	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total Events inserted- with UPC Differences = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @v_job_fk
       END


------------------------------------------------------------------------------------------------------
----CREATE EVENT ERRORS
--------------------------------------------------------------------------------------------------------

----INSERT INTO [synchronizer].[EVENT_DATA_ERRORS]
----           ([EVENT_FK]
----           ,[RULE_FK]
----           ,[EVENT_CONDITION_CR_FK]
----           ,[DATA_NAME_FK]
----           ,[ERROR_NAME]
----           ,[DATA_NAME]
----           ,[ERROR_DATA1]
----           ,[ERROR_DATA2]
----           --,[ERROR_DATA3]
----           --,[ERROR_DATA4]
----           --,[ERROR_DATA5]
----           --,[ERROR_DATA6]
----           ,[UPDATE_TIMESTAMP])
----     (SELECT
----           ED.EVENT_ID
----           ,NULL
----           ,98
----           ,110102
----           ,'DIFFERENT UPC''S FOR SAME CED NET CATALOG'
----           ,'UPC'
----           ,TMP.CEDNETCATALOG
----           ,tmp.UPC
----           --,<ERROR_DATA3, varchar(256),>
----           --,<ERROR_DATA4, varchar(256),>
----           --,<ERROR_DATA5, varchar(256),>
----           --,<ERROR_DATA6, varchar(256),>
----           ,getdate()
----		   from synchronizer.event_data ed with (nolock)
----		   inner join #TS_UPC_DIFF tmp WITH (NOLOCK)
----		   on (tmp.PRODUCT_STRUCTURE_FK = ed.PRODUCT_STRUCTURE_FK)
----		   where ed.import_job_fk = @v_job_fk)





           
            EXEC common.job_execution_create   @v_job_fk, 'CEDNETDESC UPC PRIOR EVENTS CREATED',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  
  
 
           
-------------------------------------------------------------------------------

EXECUTE [synchronizer].[EVENT_POSTING_IMPORT] @v_batch_no

--EXECUTE [synchronizer].[EVENT_APPROVE_UI] 
--------pass 0 to not call service broker
-------------------------------------------------------------------------------

---remove duplicates because of entity change

update epacube.product_identification
set record_status_cr_fk = 2
where product_identification_id in (
  select a.product_identification_id
  from (
      select pas.product_identification_id,
      DENSE_RANK() over (Partition By pas.product_structure_fk, 
                                      PAS.data_name_FK,
                                      pas.value,
									  pas.org_entity_structure_fk
									 -- pas.entity_structure_fk
      Order by pas.product_identification_id desc ) as drank
      from epacube.product_identification pas with (nolock)
  ) a
  where a.drank > 1
)


delete  
from epacube.product_identification
where record_status_cr_fk = 2

update epacube.PRODUCT_IDENT_NONUNIQUE
set record_status_cr_fk = 2
where PRODUCT_IDENT_NONUNIQUE_id in (
  select a.PRODUCT_IDENT_NONUNIQUE_id
  from (
      select pas.PRODUCT_IDENT_NONUNIQUE_id,
      DENSE_RANK() over (Partition By pas.product_structure_fk, 
                                      PAS.data_name_FK,
									  pas.value,
pas.org_entity_structure_fk 
--pas.entity_structure_fk
      Order by pas.PRODUCT_IDENT_NONUNIQUE_id desc ) as drank
      from epacube.PRODUCT_IDENT_NONUNIQUE pas
  ) a
  where a.drank > 1
)


delete  
from epacube.PRODUCT_IDENT_NONUNIQUE 
where record_status_cr_fk = 2

--- PRODUCT ASSOCIATION

update epacube.PRODUCT_ASSOCIATION
set record_status_cr_fk = 2
where product_association_id in (
  select a.product_association_id
  from (
      select pas.product_association_id,
      DENSE_RANK() over (Partition By pas.product_structure_fk, 
      pas.data_name_fk,
                                                      pas.org_entity_structure_fk
                                                      --PAS.ENTITY_STRUCTURE_FK
      Order by pas.product_association_id desc ) as drank
      from epacube.PRODUCT_ASSOCIATION pas
  ) a
  where a.drank > 1
)


delete  
from epacube.PRODUCT_ASSOCIATION
where record_status_cr_fk = 2


--- PRODUCT CATEGORY

update epacube.PRODUCT_category
set record_status_cr_fk = 2
where product_category_id in (
  select a.product_category_id
  from (
      select pas.product_category_id,
      DENSE_RANK() over (Partition By pas.product_structure_fk, 
                                                      pas.org_entity_structure_fk, 
                                                      --PAS.ENTITY_STRUCTURE_FK,
pas.data_name_fk --, pas.data_value_fk
      Order by pas.product_category_id desc ) as drank
      from epacube.PRODUCT_category pas
  ) a
  where a.drank > 1
)

delete  
from epacube.PRODUCT_category
where record_status_cr_fk = 2

--- PRODUCT ATTRIBUTE

update epacube.PRODUCT_attribute
set record_status_cr_fk = 2
where product_attribute_id in (
  select a.product_attribute_id
  from (
      select pas.product_attribute_id,
      DENSE_RANK() over (Partition By pas.product_structure_fk, 
                                                      pas.org_entity_structure_fk, 
                                                      --PAS.ENTITY_STRUCTURE_FK,
pas.data_name_fk  --, pas.attribute_event_data
      Order by pas.product_attribute_id desc ) as drank
      from epacube.PRODUCT_attribute pas
  ) a
  where a.drank > 1
)


delete  
from epacube.PRODUCT_attribute
where record_status_cr_fk = 2

----- added code block to remove if the cat number changes.  so the value is different and it changed ucc.
delete from epacube.product_identification  where PRODUCT_STRUCTURE_FK in (
select product_structure_fk from epacube.product_association with (nolock) where  data_name_fk = 159300
and PRODUCT_STRUCTURE_FK in (
select a.issues  from (
select PRODUCT_STRUCTURE_FK issues, count(1) number from epacube.PRODUCT_IDENTIFICATION with (nolock) where DATA_NAME_FK = 820103
group by PRODUCT_STRUCTURE_FK
having count(1) > 1) A))
and ENTITY_STRUCTURE_FK not in (
select entity_structure_fk from epacube.product_association with (nolock) where  data_name_fk = 159300
and PRODUCT_STRUCTURE_FK in (
select a.issues  from (
select PRODUCT_STRUCTURE_FK issues, count(1) number from epacube.PRODUCT_IDENTIFICATION with (nolock) where DATA_NAME_FK = 820103
group by PRODUCT_STRUCTURE_FK
having count(1) > 1) A))


-------------------------------------------------------------------------------------------------------------------

         
EXEC common.job_execution_create   @v_job_fk, 'IMPORT COMPLETE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @l_sysdate, @l_sysdate  
  
--------------------------------------------------------------------------------------------------------------------

EXECUTE  [dbo].[GenerateCEDInputAnalysisRpt] 
@IN_JOB_FK

-------------------------------------------------------------------------------------------------------------

  SET @status_desc = 'finished execution of SYNCHRONIZER.epacube.PAXSON_CHECK_UPC_CHANGE'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @v_job_fk ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.PAXSON_CHECK_UPC_CHANGE has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END