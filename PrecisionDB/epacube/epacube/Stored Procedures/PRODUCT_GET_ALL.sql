﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get product information JIRA (URM-1186)
-- Get all product information and return in JSON format
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[PRODUCT_GET_ALL] 
	@page INT = 1,
	@recs_per_page INT = 1000
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @first_rec INT;
	DECLARE @last_rec INT;
	DECLARE @total_rec INT;

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.PRODUCT_GET_ALL.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		DECLARE @field_list AS TABLE (DATA_NAME_ID INT, [NAME] VARCHAR(64), LABEL VARCHAR(64));
		DECLARE @dynamic_fields AS VARCHAR(MAX);
			
		INSERT INTO @field_list
		SELECT DATA_NAME_ID, dn.[NAME], LABEL
		FROM epacube.data_name dn
			INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
			INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
		WHERE dn.RECORD_STATUS_CR_FK = 1
			AND cr.CODE = 'PRODUCT'
			AND USER_EDITABLE = 1

		SELECT @total_rec = COUNT(*)
		FROM epacube.PRODUCT_STRUCTURE
		WHERE RECORD_STATUS_CR_FK = 1
		
		SELECT @dynamic_fields = COALESCE(@dynamic_fields + ', ', '') + QUOTENAME([name])
		FROM @field_list

		SELECT @first_rec = (@page - 1) * @recs_per_page
		SELECT @last_rec = (@page * @recs_per_page + 1)

		IF OBJECT_ID('tempdb.dbo.#Paged_Products', 'U') IS NOT NULL
			DROP TABLE #Paged_Products;
			
		SELECT TOP (@last_rec - 1) ROW_NUMBER() OVER( ORDER BY PRODUCT_STRUCTURE_ID ) ROW_NUM
			, PRODUCT_STRUCTURE_ID
		INTO #Paged_Products
		FROM epacube.PRODUCT_STRUCTURE
		WHERE RECORD_STATUS_CR_FK = 1
		ORDER BY PRODUCT_STRUCTURE_ID 

		IF OBJECT_ID('tempdb.dbo.#Product', 'U') IS NOT NULL
			DROP TABLE #Product; 

		SELECT *
		INTO #Product
		FROM (
			SELECT pp.ROW_NUM, pp.PRODUCT_STRUCTURE_ID, dn.[NAME], pid.[VALUE]
			FROM epacube.DATA_NAME dn
				INNER JOIN epacube.PRODUCT_IDENTIFICATION pid ON dn.DATA_NAME_ID = pid.DATA_NAME_FK
				INNER JOIN #Paged_Products pp ON pid.PRODUCT_STRUCTURE_FK = pp.PRODUCT_STRUCTURE_ID
				INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
				INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
			WHERE dn.RECORD_STATUS_CR_FK = 1
				AND cr.CODE = 'PRODUCT'
			UNION
			SELECT pp.ROW_NUM, pp.PRODUCT_STRUCTURE_ID, dn.[NAME], pin.[DESCRIPTION]
			FROM epacube.DATA_NAME dn
				INNER JOIN epacube.PRODUCT_DESCRIPTION pin ON dn.DATA_NAME_ID = pin.DATA_NAME_FK
				INNER JOIN #Paged_Products pp ON pin.PRODUCT_STRUCTURE_FK = pp.PRODUCT_STRUCTURE_ID
				INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
				INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
			WHERE dn.RECORD_STATUS_CR_FK = 1
				AND cr.CODE = 'PRODUCT') AS product
		WHERE ROW_NUM > @first_rec
			AND ROW_NUM < @last_rec  
		ORDER BY PRODUCT_STRUCTURE_ID;
 
		--Build the Dynamic Pivot Table Query
		DECLARE @table_structure AS VARCHAR(MAX);

		IF OBJECT_ID('tempdb.dbo.Product_Pivot', 'U') IS NOT NULL
			DROP TABLE tempdb.dbo.Product_Pivot; 

		SET @table_structure = 'SELECT ' + @dynamic_fields  + ', PRODUCT_STRUCTURE_ID epaCUBE_ID' +
		' INTO tempdb.dbo.Product_Pivot FROM #Product x PIVOT ( MAX(VALUE) FOR [NAME] in (' + @dynamic_fields + ') ) p ';
		
		EXECUTE(@table_structure);

		SELECT @total_rec AS TOTAL_PRODUCTS
			,(
				SELECT *
				FROM @field_list
				FOR JSON PATH
			) [fields]
			, (
				SELECT *
				FROM (
					SELECT *
					FROM tempdb.dbo.Product_Pivot) p
				FOR JSON PATH
			) [data]
		FOR JSON PATH;

		SET @ls_stmt = 'Finished execution of epacube.PRODUCT_GET_ALL.';
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		SET @ls_stmt = 'Execution of epacube.PRODUCT_GET_ALL has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

	END CATCH
END


