﻿
-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create entities JIRA (URM-1159)
-- Get all entity information for the given type
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[ENTITY_GET_BY_TYPE] 
	-- Add the parameters for the stored procedure here
	@entity_type VARCHAR(32)
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @dynamic_fields AS VARCHAR(MAX);

	BEGIN TRY

		--Initialize log
		SET @ls_stmt = 'Started execution of epacube.ENTITY_GET_BY_TYPE.'
		EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
			@exec_id = @l_exec_no OUTPUT;
			
		IF OBJECT_ID('tempdb.dbo.#Data_Set', 'U') IS NOT NULL
			DROP TABLE #Data_Set; 
		
		--Get data set information
		SELECT DATA_SET_ID, ds.[NAME] DATA_SET_NAME, TABLE_NAME, dcr.CODE DATA_TYPE 
		INTO #Data_Set
		FROM epacube.DATA_SET ds
			INNER JOIN epacube.CODE_REF ecr ON ds.ENTITY_CLASS_CR_FK = ecr.CODE_REF_ID
			INNER JOIN epacube.CODE_REF dcr ON ds.DATA_TYPE_CR_FK = dcr.CODE_REF_ID
		WHERE ds.RECORD_STATUS_CR_FK = 1
			AND ecr.CODE = @entity_type

		IF OBJECT_ID('tempdb.dbo.#Data_Name', 'U') IS NOT NULL
			DROP TABLE #Data_Name; 

		--Get data name information
		SELECT DISTINCT DATA_SET_FK, DATA_NAME_ID, dn.[NAME] DATA_NAME, LABEL, SHORT_NAME, dn.USER_EDITABLE, UI_CONFIG, 
			CASE WHEN dv.DATA_VALUE_ID IS NULL THEN 0 ELSE 1 END AS DATA_VALUE
		INTO #Data_Name
		FROM epacube.DATA_NAME dn
			INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
			INNER JOIN epacube.CODE_REF ecr ON ds.ENTITY_CLASS_CR_FK = ecr.CODE_REF_ID
			LEFT JOIN epacube.DATA_VALUE dv ON dn.DATA_NAME_ID = dv.DATA_NAME_FK
		WHERE dn.RECORD_STATUS_CR_FK = 1
			AND ecr.CODE = @entity_type
	
		--Create comma delimited list of data names for pivoting below
		SELECT @dynamic_fields = COALESCE(@dynamic_fields + ', ', '') + QUOTENAME([DATA_NAME])
		FROM #Data_Name

		SET @ls_stmt = 'Get entity information for type ' + @entity_type
		EXEC exec_monitor.Report_Status_sp @l_exec_no,
			@ls_stmt

		IF OBJECT_ID('tempdb.dbo.#Entity_Type_Data_Values', 'U') IS NOT NULL
			DROP TABLE #Entity_Type_Data_Values;
			
		CREATE TABLE #Entity_Type_Data_Values (
			ENTITY_STRUCTURE_ID BIGINT,
			[NAME] VARCHAR(64),
			[VALUE] VARCHAR(64)
		); 
		
		--Get entity type specific data values
		IF @entity_type = 'CUSTOMER'
			INSERT INTO #Entity_Type_Data_Values (ENTITY_STRUCTURE_ID, [NAME], [VALUE])
			SELECT sc.CUST_ENTITY_STRUCTURE_FK ENTITY_STRUCTURE_ID, dn.[NAME], COALESCE(dv.[VALUE], '') +  COALESCE(': ' + dv.[DESCRIPTION], '') [VALUE]
			FROM epacube.DATA_VALUE dv
				INNER JOIN epacube.SEGMENTS_CUSTOMER sc on dv.data_value_id = sc.CUST_SEGMENT_FK
				INNER JOIN epacube.DATA_NAME dn on dv.DATA_NAME_FK = dn.DATA_NAME_ID
			WHERE dv.RECORD_STATUS_CR_FK = 1
				AND dn.USER_EDITABLE = 1
		ELSE
			INSERT INTO #Entity_Type_Data_Values (ENTITY_STRUCTURE_ID, [NAME], [VALUE])
			SELECT ea.ENTITY_STRUCTURE_FK, dn.[NAME], dv.[VALUE]
			FROM epacube.DATA_VALUE dv
				INNER JOIN epacube.ENTITY_ATTRIBUTE ea ON dv.DATA_VALUE_ID = ea.DATA_VALUE_FK AND ea.DATA_NAME_FK = 502060
				INNER JOIN epacube.DATA_NAME dn ON dv.DATA_NAME_FK = dn.DATA_NAME_ID
				INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
				INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
			WHERE cr.CODE = @entity_type
				AND dv.RECORD_STATUS_CR_FK = 1
				AND dn.USER_EDITABLE = 1

		IF OBJECT_ID('tempdb.dbo.#Entity', 'U') IS NOT NULL
			DROP TABLE #Entity; 
		
		--Get parent entity info
		SELECT *
		INTO #Entity
		FROM (
			SELECT es.ENTITY_STRUCTURE_ID, dn.[NAME], ei.[VALUE]
			FROM epacube.DATA_NAME dn
				INNER JOIN epacube.ENTITY_IDENTIFICATION ei ON dn.DATA_NAME_ID = ei.DATA_NAME_FK
				INNER JOIN epacube.ENTITY_STRUCTURE es ON ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
				INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
				INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
			WHERE cr.CODE = @entity_type
				AND es.PARENT_ENTITY_STRUCTURE_FK IS NULL
				AND ei.RECORD_STATUS_CR_FK = 1
			UNION
			SELECT es.ENTITY_STRUCTURE_ID, dn.[NAME], ein.[VALUE]
			FROM epacube.DATA_NAME dn
				INNER JOIN epacube.ENTITY_IDENT_NONUNIQUE ein ON dn.DATA_NAME_ID = ein.DATA_NAME_FK
				INNER JOIN epacube.ENTITY_STRUCTURE es ON ein.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
				INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
				INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
			WHERE cr.CODE = @entity_type
				AND es.PARENT_ENTITY_STRUCTURE_FK IS NULL
				AND ein.RECORD_STATUS_CR_FK = 1
			UNION
			SELECT ENTITY_STRUCTURE_ID, [NAME], [VALUE]
			FROM #Entity_Type_Data_Values) AS src;

		IF OBJECT_ID('tempdb.dbo.Entity_Pivot', 'U') IS NOT NULL
			DROP TABLE tempdb.dbo.Entity_Pivot; 
 
		--Flatten the recordset by pivoting on the DATA_NAME.NAME field
		DECLARE @table_structure AS VARCHAR(MAX);
		  
		SET @table_structure = 'SELECT ENTITY_STRUCTURE_ID, ' + @dynamic_fields  +  
		' INTO tempdb.dbo.Entity_Pivot FROM #Entity x PIVOT ( MAX(VALUE) FOR [NAME] in (' + @dynamic_fields + ') ) p ';  

		EXECUTE(@table_structure);

		IF OBJECT_ID('tempdb.dbo.#Entity_Children', 'U') IS NOT NULL
			DROP TABLE #Entity_Children; 

		--Get related child entity info
		SELECT *
		INTO #Entity_Children
		FROM (
			SELECT es.ENTITY_STRUCTURE_ID, es.PARENT_ENTITY_STRUCTURE_FK, dn.[NAME], ei.[VALUE]
			FROM epacube.DATA_NAME dn
				INNER JOIN epacube.ENTITY_IDENTIFICATION ei ON dn.DATA_NAME_ID = ei.DATA_NAME_FK
				INNER JOIN epacube.ENTITY_STRUCTURE es ON ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
				INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
				INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
			WHERE cr.CODE = @entity_type
				AND es.PARENT_ENTITY_STRUCTURE_FK IS NOT NULL
				AND ei.RECORD_STATUS_CR_FK = 1
			UNION
			SELECT es.ENTITY_STRUCTURE_ID, es.PARENT_ENTITY_STRUCTURE_FK, dn.[NAME], ein.[VALUE]
			FROM epacube.DATA_NAME dn
				INNER JOIN epacube.ENTITY_IDENT_NONUNIQUE ein ON dn.DATA_NAME_ID = ein.DATA_NAME_FK
				INNER JOIN epacube.ENTITY_STRUCTURE es ON ein.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
				INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
				INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
			WHERE cr.CODE = @entity_type
				AND es.PARENT_ENTITY_STRUCTURE_FK IS NOT NULL
				AND ein.RECORD_STATUS_CR_FK = 1) src;

		IF OBJECT_ID('tempdb.dbo.Entity_Children_Pivot', 'U') IS NOT NULL
			DROP TABLE tempdb.dbo.Entity_Children_Pivot; 
 
		--Flatten the recordset by pivoting on the DATA_NAME.NAME field
		DECLARE @child_table_structure AS VARCHAR(MAX);

		SET @child_table_structure = 'SELECT ENTITY_STRUCTURE_ID, PARENT_ENTITY_STRUCTURE_FK, ' + @dynamic_fields +  
		' INTO tempdb.dbo.Entity_Children_Pivot FROM #Entity_Children x PIVOT ( MAX(VALUE) FOR [NAME] in (' + @dynamic_fields + ') ) p ';  
		
		EXECUTE(@child_table_structure);

		--Return a hierarchical JSON structure
		SELECT @entity_type entityType
			, (
				SELECT ds.*
					, (
						SELECT DATA_NAME_ID, DATA_NAME, LABEL, SHORT_NAME, USER_EDITABLE, UI_CONFIG, DATA_VALUE
						FROM #Data_Name dn
						WHERE dn.DATA_SET_FK = ds.DATA_SET_ID
						FOR JSON PATH
					) [data_name]
				FROM #Data_Set ds
				FOR JSON PATH
			) [data_set]
			, (
				SELECT ep.*
					, (
							SELECT ecp.*
							FROM tempdb.dbo.Entity_Children_Pivot ecp
							WHERE ecp.PARENT_ENTITY_STRUCTURE_FK = ep.ENTITY_STRUCTURE_ID
							FOR JSON PATH
						) [children]
				FROM tempdb.dbo.Entity_Pivot ep
				FOR JSON PATH
			) [data]
		FOR JSON PATH;

		SET @ls_stmt = 'Finished execution of epacube.ENTITY_GET_BY_TYPE.';
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		SET @ls_stmt = 'Execution of epacube.ENTITY_GET_BY_TYPE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

	END CATCH

END
