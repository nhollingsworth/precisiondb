﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Creat new product associations (URM-1202)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[PRODUCT_ASSOCIATION_CREATE]
	@association NVARCHAR(MAX)
	,@user VARCHAR(64)
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	SET NOCOUNT ON;

	--***************************************************************************************************************************************
	--Example product association structure
	--{
	--	"data": [
	--		{
	--			"associationId": 500023,
	--			"productStructureId": 8709,
	--			"organizationId": 8709,
	--			"entityStructureId": 1002
	--		}
	--	]
	--}
	--***************************************************************************************************************************************

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.PRODUCT_ASSOCIATION_CREATE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY
		BEGIN TRANSACTION;

			IF OBJECT_ID('tempdb.dbo.#Association', 'U') IS NOT NULL
				DROP TABLE #Association;

			SELECT *
			INTO #Association
			FROM OPENJSON(@association, '$.data')
			WITH (
				DATA_NAME_FK INT '$.associationId',
				PRODUCT_STRUCTURE_FK BIGINT '$.productStructureId',
				ORG_ENTITY_STRUCTURE_ID BIGINT '$.organizationId',
				ENTITY_STRUCTURE_ID BIGINT '$.entityStructureId'
			);

			--Create new product association					
			INSERT INTO epacube.PRODUCT_ASSOCIATION (DATA_NAME_FK, PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, ENTITY_CLASS_CR_FK, ENTITY_STRUCTURE_FK, RECORD_STATUS_CR_FK, CREATE_USER)
			SELECT a.DATA_NAME_FK, a.PRODUCT_STRUCTURE_FK, a.ORG_ENTITY_STRUCTURE_ID, es.ENTITY_CLASS_CR_FK, a.ENTITY_STRUCTURE_ID, 1, @user
			FROM #Association a
				INNER JOIN epacube.ENTITY_STRUCTURE es ON a.ENTITY_STRUCTURE_ID = es.ENTITY_STRUCTURE_ID;

		COMMIT TRANSACTION;

		SET @ls_stmt = 'Finished execution of epacube.PRODUCT_ASSOCIATION_CREATE.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 

		SET @ls_stmt = 'Execution of epacube.PRODUCT_ASSOCIATION_CREATE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END
