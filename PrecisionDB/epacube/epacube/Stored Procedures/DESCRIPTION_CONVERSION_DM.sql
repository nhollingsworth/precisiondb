﻿



-- Copyright 2013
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to create Description Conversions
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        09/09/2013   Initial SQL Version

CREATE PROCEDURE [epacube].[DESCRIPTION_CONVERSION_DM] 
( 
  @in_lookup_text varchar(128),
  @in_vendor_text varchar (128),
  @in_type_dn_id int,      
  @in_value1_edv_fk bigint,
  @in_value2_edv_fk bigint,
  @in_part_format_dn_id int,  
  @in_remove_special_char char(1),
  @in_category_edv_fk bigint,
  @in_display_seq int,  
  @in_update_user varchar(64) )
											


AS

/***************  Parameter Defintions  **********************

***************************************************************/

BEGIN

DECLARE @l_sysdate				datetime
DECLARE @ls_stmt				varchar (1000)
DECLARE @l_exec_no				bigint
DECLARE @l_rows_processed		bigint
DECLARE @v_count				bigint
DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int

DECLARE @PrimaryVendorDNID		int
DECLARE @PrimaryVendorDNLabel	varchar(64)
DECLARE @VendorEntityStructureID bigint
DECLARE @lookup_dv_fk			bigint
DECLARE @ValidationRows			int


SET NOCOUNT ON;
SET @l_sysdate = getdate ()
set @ErrorMessage = null

set @PrimaryVendorDNID = (select value from epacube.epacube_params
						  where APPLICATION_SCOPE_FK = (select APPLICATION_SCOPE_ID from epacube.APPLICATION_SCOPE
				                						where scope = 'APPLICATION')
						  and name = 'VENDOR')

set @PrimaryVendorDNLabel = (select epacube.getDataNameLabel(@PrimaryVendorDNID))


-------------------------------------------------------------------------------------
--   Peform UI Insertion or Modification
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
-- Perform Server-side validation 
-- 1. Valid Vendor
-- 2. Valid Lookup
-- 3. Not a Duplicate Description Conversion
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--1.	Make Sure Vendor is valid and look up Entity Structure FK
-------------------------------------------------------------------------------------
IF @in_vendor_text is not null
BEGIN
select ENTITY_STRUCTURE_FK from epacube.ENTITY_IDENTIFICATION
where value = @in_vendor_text
and data_name_fk = @PrimaryVendorDNID

			 
			 IF @@rowcount = 0	
				BEGIN			
					set @ErrorMessage = 'No ' + @PrimaryVendorDNLabel + ' matching ' + @in_vendor_text + '; Please enter a valid ' +@PrimaryVendorDNLabel 
					GOTO RAISE_ERROR					
				END
END

-------------------------------------------------------------------------------------
--2.   Make sure Lookup is valid and look up the Entity Data Value ID
-------------------------------------------------------------------------------------
IF @in_lookup_text is not null
BEGIN
select DATA_VALUE_ID from epacube.DATA_VALUE
where value = @in_lookup_text
--and data_name_fk = @PrimaryVendorDNID

			  
			 IF @@rowcount = 0				
				BEGIN
				set @ErrorMessage = 'No lookup' + ' matching ' + @in_lookup_text + '; Please enter a valid Lookup'
					GOTO RAISE_ERROR					
				END
END
-------------------------------------------------------------------------------------
--3.   Check for Duplicate Prior to Insert
-------------------------------------------------------------------------------------

BEGIN

set @lookup_dv_fk = (select top 1 DATA_VALUE_ID from epacube.DATA_VALUE
					 where value = @in_lookup_text)

set @VendorEntityStructureID = (select ENTITY_STRUCTURE_FK from epacube.ENTITY_IDENTIFICATION
								where value = @in_vendor_text
								and data_name_fk = @PrimaryVendorDNID)



    select * from epacube.DESCRIPTION_CONVERSION
	where LOOKUP_DATA_VALUE_FK = @lookup_dv_fk
	and VENDOR_ENTITY_STRUCTURE_FK = (select entity_structure_fk from epacube.ENTITY_IDENTIFICATION
									  where value = @in_vendor_text
									  and data_name_fk = @PrimaryVendorDNID)
	and DATA_NAME_FK = @in_type_dn_id
	and PART_FORMAT_DN_FK = @in_part_format_dn_id
	and ENTITY_DATA_VALUE1_FK = @in_value1_edv_fk
	and ENTITY_DATA_VALUE2_FK = @in_value2_edv_fk
	and REMOVE_SPECIAL_CHAR = @in_remove_special_char
	and DATA_CATEGORY_EDV_FK = @in_category_edv_fk
	
			
			 IF @@ROWCOUNT > 0
			 BEGIN
					set @ErrorMessage = 'Duplicate Description Conversion.'
					GOTO RAISE_ERROR					
			 END

END
-------------------------------------------------------------------------------------
--   Insert Row
-------------------------------------------------------------------------------------
IF @ErrorMessage is null


BEGIN
INSERT INTO epacube.DESCRIPTION_CONVERSION
(LOOKUP_DATA_VALUE_FK, VENDOR_ENTITY_STRUCTURE_FK, DATA_NAME_FK, ENTITY_DATA_VALUE1_FK, PART_FORMAT_DN_FK, ENTITY_DATA_VALUE2_FK
, REMOVE_SPECIAL_CHAR, DATA_CATEGORY_EDV_FK, DISPLAY_SEQ, RECORD_STATUS_CR_FK, CREATE_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_USER)
VALUES
( @lookup_dv_fk, @VendorEntityStructureID, @in_type_dn_id, @in_value1_edv_fk, @in_part_format_dn_id, @in_value2_edv_fk
, @in_remove_special_char, @in_category_edv_fk, @in_display_seq, 1, getdate(), getdate(), @in_update_user)

END



RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END


