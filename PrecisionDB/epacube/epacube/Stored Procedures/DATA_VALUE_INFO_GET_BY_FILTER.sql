﻿
-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get data values JIRA (URM-1201)
-- Get data value information based on filter and return in a hierarchical JSON format
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE OR ALTER PROCEDURE [epacube].[DATA_VALUE_INFO_GET_BY_FILTER]
	@data_value_type INT = NULL,
	@data_name_id INT = NULL,
	@value VARCHAR(256) = NULL,
	@data_value_description VARCHAR(256) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	BEGIN TRY

		-- Initialize log
		SET @ls_stmt = 'Started execution of epacube.DATA_VALUE_INFO_GET_BY_FILTER.'
		EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
			@exec_id = @l_exec_no OUTPUT;

		SELECT dv.DATA_VALUE_ID
			, ds.NAME DATA_VALUE_TYPE
			, dn.LABEL DATA_VALUE_LABEL
			, dv.VALUE
			, dv.DESCRIPTION
			, dn.DATA_NAME_ID
			, dn.INSERT_MISSING_VALUES
			, JSON_QUERY(epacube.getDataValue(dv.DATA_VALUE_ID)) AS CHILDREN 
		FROM epacube.DATA_NAME dn
			INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
			INNER JOIN epacube.DATA_VALUE dv ON dn.DATA_NAME_ID = dv.DATA_NAME_FK
		WHERE (@data_value_type IS NULL
				OR ds.DATA_SET_ID = @data_value_type)
			AND (@data_name_id IS NULL
				OR dn.DATA_NAME_ID = @data_name_id)
			AND (@value IS NULL
				OR dv.VALUE = @value)
			AND (@data_value_description IS NULL 
				OR dv.DESCRIPTION = @data_value_description)
		FOR JSON PATH;

		SET @ls_stmt = 'Finished execution of epacube.DATA_VALUE_INFO_GET_BY_FILTER.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH
		SET @ls_stmt = 'Execution of epacube.DATA_VALUE_INFO_GET_BY_FILTER has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END
