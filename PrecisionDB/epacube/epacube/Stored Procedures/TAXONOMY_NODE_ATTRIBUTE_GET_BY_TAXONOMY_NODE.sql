﻿-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get product attribute information JIRA (PRE-158)
-- Get all product attribute information and return in JSON format
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE   PROCEDURE [epacube].[TAXONOMY_NODE_ATTRIBUTE_GET_BY_TAXONOMY_NODE]
	@taxonomy_node_id INT
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.TAXONOMY_ATTRIBUTE_GET_BY_TAXONOMY_NODE_ID.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT (
			SELECT TNA.TAXONOMY_NODE_ATTRIBUTE_ID
				, DN.DATA_NAME_ID
				, DN.[LABEL] AS [NAME]
				, (
					SELECT DV.DATA_VALUE_ID
						, DV.[VALUE]
						, CASE TNA.RESTRICTED_DATA_VALUES_IND 
							WHEN 1 THEN 'TRUE'
							ELSE 'FALSE'
						END AS RESTRICTED
					FROM epacube.TAXONOMY_NODE_ATTRIBUTE_DV TNAD
						INNER JOIN epacube.DATA_VALUE DV
							ON TNAD.DATA_VALUE_FK = DV.DATA_VALUE_ID
					WHERE TNAD.TAXONOMY_NODE_ATTRIBUTE_FK = TNA.TAXONOMY_NODE_ATTRIBUTE_ID
					FOR JSON PATH) CHILDREN
			FROM epacube.TAXONOMY_NODE TN
				INNER JOIN epacube.TAXONOMY_NODE_ATTRIBUTE TNA
					ON TN.TAXONOMY_NODE_ID = TNA.TAXONOMY_NODE_FK
				INNER JOIN epacube.DATA_NAME DN
					ON TNA.DATA_NAME_FK = DN.DATA_NAME_ID
			WHERE TN.TAXONOMY_NODE_ID = @taxonomy_node_id
			FOR JSON PATH) [DATA]
		FOR JSON PATH, WITHOUT_ARRAY_WRAPPER;

		SET @ls_stmt = 'Finished execution of epacube.TAXONOMY_ATTRIBUTE_GET_BY_TAXONOMY_NODE_ID.';
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		SET @ls_stmt = 'Execution of epacube.TAXONOMY_ATTRIBUTE_GET_BY_TAXONOMY_NODE_ID has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

	END CATCH
END