﻿







CREATE PROCEDURE [epacube].[UTIL_SearchStringInCode] (@s varchar(400),
@flag char(1))

as 

/***********************************************************

  Written By  : yousef ekhtiari
  email          :y_ekhtiari@yahoo.com
  Date           : 10 January 2006
  Description : Returns the name of   stored procedures 
which contain whole or any part of  tokens in a string


  
USAGE:

Parameters: (1) string to search for, (2) flag (use w) 

@flag='a' means any part of  tokens
@flag='w' whole  part of  tokens

exec Usp_SearchInProc  @s='#tmp_result   "yousef  ekhtiari"    ',@flag='w'
exec Usp_SearchInProc  @s='#tmp_result   "yousef  ekhtiari"  lol  ',@flag='a'


***********************************************************/
set nocount on
create table #Sarg   (s varchar(100))
declare 
		@pos int,
		@sSQL varchar(8000),
		 @dbname as sysname,
		@where as  varchar(8000)
	if @flag not in ('w','a')
	begin
		raiserror('Invalid use of @flag',16,1)
		return	
	end
set @s=ltrim(ltrim(@s))+' '

while len(@s)>0
begin
	if left(@s,1)='"'
		begin
			set  @pos=CHARINDEX('"',@s,2)
			insert  #Sarg values( ltrim(replace( left(@s,CHARINDEX('"',@s,2) ) ,'"','')))
	
		end 
	else
		begin
			set  @pos=CHARINDEX(' ',@s,2)
			insert  #Sarg values(  ltrim(left(@s,CHARINDEX(' ',@s,2))))
	
		end
	set @s=ltrim(stuff(@s,1,@pos ,''))

end
declare db cursor 
for  SELECT [name] 
FROM [master].[dbo].[sysdatabases]
--where sid<>0x01
--where sid = 0x01 and dbid in (5,6)
where name in ('common','epacube')

open db
fetch next from db into @dbname
while @@fetch_status=0
begin
	print '----------------'+ @dbname+'-------------'

		set @sSQL='SELECT  distinct [name]
		FROM  '+@dbname+'.[dbo].[sysobjects] o
		inner join  '+@dbname+'.[dbo].[syscomments] c on o.id=c.id
--		where xtype=''p''
		where xtype in (''p'',''pc'',''v'',''tf'',''fn'',''if'',''fs'',''ft'')
		and name not like ''dt_%'''
if @flag='a'
	set @sSQL=@sSQL+' and   exists(
						select * from #Sarg
						where 
						 ltrim(rtrim(text)) like  ''%''+ltrim(rtrim(s))+''%'')'
else if @flag='w'
		begin
			set @where=''

			select @where=@where+' and  patindex( ''%'+replace(ltrim(rtrim(s)),'''','''''' )+'%'', text)>0'
			from   #Sarg 
			
			set @sSQL=@sSQL+@where

		end

	
	exec(@sSQL)
fetch next from db into @dbname
end
close db
deallocate db
drop table  #Sarg









