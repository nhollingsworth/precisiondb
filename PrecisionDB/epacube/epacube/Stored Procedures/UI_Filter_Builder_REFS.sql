﻿--USE [epacube]
--GO
--/****** Object:  StoredProcedure [epacube].[UI_Filter_Builder]    Script Date: 2/22/2017 10:13:00 AM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE PROCEDURE [epacube].[UI_Filter_Builder_REFS] @UserID varchar(64), @Data_Name_FK varchar(64), @Criteria varchar(Max) = Null, @FilterResultsExec int = 0, @FilterResultsPrint int = 1, @QueryExec int = 0, @QueryScript Varchar(Max) OUTPUT

as 

/*
--/************************************************************************************************************************************************
--  Written By  : Gary Stone
	February, 2017
Parameters
@FilterResultsExec - 1 will process results of for the filter data name requested
@FilterResultsPrint - 1 will generate the query to use with the filter data name requested, but will not display any results
@QueryExec - 0 will do nothing; 1 will display the code used for each of the Prod, Cust, and Vendor tables to be generated; 2 will run the query

The appropriate Prod, Cust, and Vendor tables will be generated regardless of the options chosen above.

! = NOT
~ = group separator - replaces ^
| = OR
= = Criteria Value

--*/-----------------------------------------------------------------------------------------------------------------------------------------------
*/

set nocount on
--Declare @UserID Varchar(8) = 78, @Data_Name_FK varchar(32) = '501045', @Criteria varchar(Max) = '110401=%cortisone%~501045=3130030~159905=1~144010=YOKES FOODS INC~144020=~144112=~520013=~143112=THE ART OF GOOD FOOD~'   --'144010=YOKES FOODS INC~'--Null-- 
--Declare @UserID Varchar(8) = 78, @Data_Name_FK varchar(32) = '501045', @Criteria varchar(Max) = '110401=%cortisone%~501045=3130030~'
--Declare @UserID Varchar(8) = 78, @Data_Name_FK varchar(32) = '502045', @Criteria varchar(Max) = '110401=%cortisone%~501045=3130030~'
--Declare @UserID Varchar(8) = 78, @Data_Name_FK varchar(32) = '144020', @Criteria varchar(Max) = '110401=%cortisone%~501045=3130030~159905=1~144010=YOKES FOODS INC~'   --'144010=YOKES FOODS INC~'--Null-- 
--Declare @UserID Varchar(8) = 78, @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = '159905=1~144010=YOKES FOODS INC~'--Null-- 
--Declare @UserID Varchar(8) = 78, @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = '144010=YOKES FOODS INC~110401=%cortisone%~501045=3130030~'--Null-- 
--Declare @UserID Varchar(8) = 78, @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = '110100=%D~'
--Declare @UserID Varchar(8) = 78, @Data_Name_FK varchar(32) = '144120', @Criteria varchar(Max) = Null--'110100=%D~'
--Declare @UserID Varchar(8) = 78, @Data_Name_FK varchar(32) = '502045', @Criteria varchar(Max) = '144010=BARNEYS SOOPER MARKET INC~159905=1~110401=%cereal%~'
--Declare @UserID Varchar(8) = 78, @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = '144112=Ros%~502045=42~159905=1~' --502045=BARNEYS SOOPER MARKET INC~159905=1~'
--Declare @UserID Varchar(8) = 78, @Data_Name_FK varchar(32) = '143110', @Criteria varchar(Max) = '110100=111018D~', @FilterResults int = 1, @CompQuery int = 0
--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = '110100=233877D~144020=9670~143110=1017012~', @FilterResultsExec int = 0, @FilterResultsPrint int = 0, @QueryExec int = 2
--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = '501045=1000001~144020=7155~143110=1800103~159905=1~', @FilterResultsExec int = 0, @FilterResultsPrint int = 1, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = 'URM SUBGROUP=1000001~CUSTOMER SHIP TO=7155~VENDOR NUMBER=1800103~AUTHORIZED=1~', @FilterResultsExec int = 0, @FilterResultsPrint int = 1, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(32) = '144010', @Criteria varchar(Max) = '144020=~', @FilterResultsExec int = 0, @FilterResultsPrint int = 1, @QueryExec int = 2, @QueryScript Varchar(Max)
--Declare @UserID Varchar(8) = 'gstone2', @Data_Name_FK varchar(Max) = '', @Criteria varchar(Max) = '144020=~110100=111018D~159905=1~', @FilterResultsExec int = 0, @FilterResultsPrint int = 1, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(Max) = '144010~144020~110103~144120', @Criteria varchar(Max) = '110100=233877D~144020=9670~143110=1017012~550001=1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(Max) = '0', @Criteria varchar(Max) = '110100=233877D~144020=9670~143110=1017012~550001=1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(Max) = '110100~', @Criteria varchar(Max) = '143110=1017012~550001=1~144020=7155~550020=1~110401=%Cereal%~550100=!1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(Max) = '110401~', @Criteria varchar(Max) = '144020=7155~143110=1017012~550001=1~550020=1~110401=%Cereal%~550100=1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(Max) = '110401~', @Criteria varchar(Max) = '143110=1017012~550001=!1~550020=1~110401=%Cereal%~550100=1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(Max) = '110401~', @Criteria varchar(Max) = '143110=1017012~144020=7155~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(Max) = '144120~110100~501045~502045~', @Criteria varchar(Max) = '550001=1~144020=7141~550100=1~550020=1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(Max) = '502045~', @Criteria varchar(Max) = '550001=!1~550020=1~110401=%Cereal%~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)

--Declare @UserID Varchar(8) = 'gstone3', @Data_Name_FK varchar(Max) = 'All', @Criteria varchar(Max) = '550001=1~144020=7141~550100=1~550020=1~110100=15055D~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)

--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(Max) = 'all', @Criteria varchar(Max) = '110401=%cereal%~110100=420151W~550001=1~550020=1~550100=1~144020=7120~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)

--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(Max) = 'All', @Criteria varchar(Max) = '110100=14969w~144020=2020~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)

--Declare @UserID Varchar(8) = 'gstone', @Data_Name_FK varchar(Max) = 'ALL~', @Criteria varchar(Max) = '110100=14969w~144020=2020', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)


Set @UserID = Replace(@UserID, '.', '_')

If right(@Data_Name_FK, 1) <> '~' and @Data_Name_FK not like 'ALL%'
Set @Data_Name_FK = @Data_Name_FK + '~'

If @Data_Name_FK = 'All~'
Set @Data_Name_FK = 'All'

IF right(@Criteria, 1) <> '~'
Set @Criteria = @Criteria + '~'

If object_id('tempdb..#Tbl') is not null
drop table #Tbl

Create table #Tbl([SQL] varchar(Max) Null, [Where] Varchar(Max) Null)

If object_id('tempdb..#Param3') is not null
drop table #Param3

Declare @POS int
Declare @StartPOS int
Declare @FilterToken varchar(192)
Declare @CritDN varchar(64)
Declare @CritValue Varchar(128)
Declare @OpExclusion varchar(8)
Declare @TmpTbl varchar(64)
Declare @FilterCode varchar(Max)
Declare @Entity_Class_CR_FK varchar(64)
Declare @SQL varchar(Max)
Declare @WHERE varchar(Max)
Declare @DN_Type varchar(32)
Declare @Structure_FK varchar(64)
Declare @Local_Structure_FK Varchar(64)
Declare @Primary_Structure_FK Varchar(64)
Declare @AuthSQL Varchar(Max)
Declare @DN_Label Varchar(64)
Declare @KeyCols Varchar(Max)
Declare @SqlEXEC Varchar(Max)
Declare @From VARCHAR(MAX), @Qtbl Varchar(128), @DN_FK Varchar(16), @Alias Varchar(16), @Select Varchar(Max), @Primary_Column varchar(64), @Local_Column varchar(64), @Alias_DV varchar(16), @Operator Varchar(8), @OpExcl Varchar(8), @R_EC_CR_FK Varchar(16)
Declare @VendorJoin Varchar(Max), @VendorCols Varchar(Max)
Declare @Lookup_Values int
Declare @BT_Status int
Declare @ST_Status int
Declare @Op varchar(16) 
Declare @DN_String Varchar(Max) = ''
Declare @DN_POS int = 1
Declare @DN_StartPOS int = 1
Declare @DN_Length int = 1
Declare @Auths_Ind int = 1
Declare @Auths_Join varchar(64) = ''
Declare @SQLcodeStart Varchar(Max) = ''
Declare @SQLcodeIn Varchar(Max) = ''
Declare @SQLcodeNotIn Varchar(Max) = ''
Declare @VirtualColumnCheck Varchar(Max) = ''
Declare @DNtmp varchar(16) = ''
Declare @FILTER_ENTITY_CLASS_CR_FK varchar(64) = ''
Declare @COLUMN_ENTITY_CLASS_CR_FK varchar(64) = ''
Declare @Product_Structure_Column varchar(64) = ''
Declare @VirtualJoin varchar(64) = ''
Declare @ImmedAlias varchar(64) = ''
Declare @Ref_DN_FK varchar(64) = ''
Declare @Customer_Structure_Column varchar(64) = ''
Declare @Vendor_Structure_Column varchar(64) = ''
Declare @VirtualAction varchar(64) = ''
Declare @Pass int = 1
Declare @Virtual_DN_FK varchar(64) = ''
Declare @Cust_Structure_FK_Init varchar(64) = ''
Declare @Item_Data_Name_FK varchar(64) = ''
Declare @PA_Alias varchar(64) = ''
Declare @All_Filters varchar(Max) = ''
Declare @CurrentFilterDN_FK varchar(16) = ''
Declare @Max_Pass varchar(8) = ''
Declare @Prod_Alias varchar(64) = ''
Declare @ind_VirtualColumnCheck int = 0
Declare @Where_Column Varchar(256)
Declare @Data_Level_CR_FK int

Set @Item_Data_Name_FK = (Select value from epacube.epacube_params where application_scope_fk = 10109 and name = 'search2')
Set @Prod_Alias = 'pi_' + @Item_Data_Name_FK
	Set @QueryScript = ''

	If @Data_Name_FK = 'All'
	Begin
		DECLARE AllFilters Cursor local for
			Select Data_Name_fk 
			from epacube.ui_filter 
			where isnull(data_name_fk, 0) <> 0 
				and record_status_cr_fk = 1
				--and isnull(checkbox_y_n, 0) = 0 

			OPEN AllFilters;
			FETCH NEXT FROM AllFilters INTO @CurrentFilterDN_FK
			WHILE @@FETCH_STATUS = 0

				Begin--Fetch

				Set @All_Filters = @All_Filters + @CurrentFilterDN_FK + '~'

			FETCH NEXT FROM AllFilters INTO @CurrentFilterDN_FK
				End--Fetch
			Close AllFilters;
			Deallocate AllFilters;

			Set @Data_Name_FK = @All_Filters
		End

	If @Data_Name_FK like '%~%'
	Begin
		Set @DN_String = @Data_Name_FK
		Set @Data_Name_FK = '0'
	End

If @Data_Name_FK = '0' and isnull(@Criteria, '') <> ''
Begin
	Set @Lookup_Values = 1
end
Else
Begin
	Set @Data_Name_FK = Case when isnumeric(@Data_Name_FK) = 1 then @Data_Name_FK else (Select data_name_id from epacube.data_name where name = '' + @Data_Name_FK + '') end
	Set @Lookup_Values = isnull((Select top 1 Lookup_Values from epacube.ui_filter where data_name_fk = @Data_Name_FK), 0)

	If isnull(@Criteria, '') = ''
		GoTo Last_Step
End

Create Table #Param3(CritDN varchar(Max) Not Null, CritValue varchar(max) Not Null, OpExclusion varchar(8)) --, Lookup_Field varchar(8))

	Set @StartPOS = 1

	If isnull(@UserID, '0') = '0'
	Set @UserID = Cast(@@SPID as varchar(8))

	If object_id('TempTbls.tmp_PROD_' + @UserID) is not null
		Exec('Drop Table TempTbls.tmp_PROD_' + @UserID)

	If object_id('TempTbls.tmp_CUST_' + @UserID) is Not null
		Exec('Drop Table TempTbls.tmp_CUST_' + @UserID)

	If object_id('TempTbls.tmp_VENDOR_' + @UserID) is Not null
		Exec('Drop Table TempTbls.tmp_VENDOR_' + @UserID)

	If object_id('TempTbls.tmp_FILTER_' + @UserID) is not null
		Exec('Drop Table TempTbls.tmp_FILTER_' + @UserID)

	Exec('Create Table TempTbls.tmp_FILTER_' + @UserID + '(CritDN varchar(64) Not Null, CritValue varchar(128) Not Null, OpExclusion varchar(8))')

	If isnull(@Criteria, '') = ''
	Set @Criteria = @Data_Name_FK + '=' + '' + '~'

	If @QueryExec <> 0
	Begin
		Set @FilterResultsPrint = 0
		Set @FilterResultsExec = 0
	End
		
	Set @VendorJoin = ''

	While charindex('~', @Criteria, @StartPOS) <> 0 
		Begin

			Set @POS = charindex('~', @Criteria, @StartPOS)
			Set @FilterToken = substring(@Criteria, @StartPOS, @POS - @StartPOS)


			Set @StartPOS = @POS + 1		

			Set @CritDN = left(@FilterToken,charindex('=', @FilterToken) - 1)
			Set @CritDN = Case when isnumeric(@CritDN) = 1 then @CritDN else (Select data_name_id from epacube.data_name where name = '' + @CritDN + '') end
			Set @CritValue = reverse(left(reverse(@FilterToken),charindex('=', reverse(@FilterToken)) - 1))

		If @CritDN Not In (Select data_name_fk from epacube.ui_filter) --or @CritDN = 159905
			goto NextVal

			If @CritValue like '%!%' 
				Begin
					Set @OpExclusion = '!'
					Set @CritValue = Right(@CritValue, Len(@CritValue)-1)
				end
				Else
				Begin
					Set @OpExclusion = ''
				End

			Insert into #Param3
			Select @CritDN, @CritValue, @OpExclusion--, Null 

			If @Criteria not like '%' + @Data_Name_FK + '%'
			Insert into #Param3
			Select @Data_Name_FK, '', ''
			where @Data_Name_FK not in (Select CritDN from #Param3)

NextVal:
	End

	Exec('Insert into TempTbls.tmp_FILTER_' + @UserID + ' Select * from #Param3')

	If (Select count(*) from #Param3 where isnull(CritValue, '') <> '') = 0
	GoTo Last_Step

	Set @BT_Status = Case when (Select count(*) from #Param3 where critdn = 144010) > 0 then 1 else 0 end
	Set @ST_Status = Case when (Select count(*) from #Param3 where critdn = 144020) > 0 then 1 else 0 end

	DECLARE Entity_Class Cursor local for
	Select r.ENTITY_CLASS_CR_FK
	from EPACUBE.DATA_NAME_GROUP_REFS R
	inner join epacube.epacube.ui_filter uif on r.data_name_fk = uif.Data_Name_FK
	inner join #Param3 P3 on uif.data_name_fk = P3.CritDN
	where 1 = 1
		and P3.CritDN <> 159905
		and r.table_name <> 'SEGMENTS_CONTACTS'
	group by r.ENTITY_CLASS_CR_FK
	Order by Case when r.ENTITY_CLASS_CR_FK = 10109 then 2 else 1 end

		OPEN Entity_Class;
		FETCH NEXT FROM Entity_Class INTO @Entity_Class_CR_FK
		WHILE @@FETCH_STATUS = 0

		Begin
		
		Set @Select = 'Select '
		Set @Where = 'Where 1 = 1 '
		Set @From = ''
		Set @KeyCols = ''

		Set @From = Case 
			when @Entity_Class_CR_FK = 10109 then ' From epacube.epacube.product_structure PS with (nolock) '
												+ 'Inner join epacube.product_identification ' + @Prod_Alias + ' with (nolock) on ps.product_structure_id = ' + @Prod_Alias + '.product_structure_fk and ' + @Prod_Alias + '.data_name_fk = ' + @Item_Data_Name_FK
			When @Entity_Class_CR_FK = 10103 then ' From epacube.epacube.entity_structure ESV with (nolock) '
												+ 'Inner Join epacube.entity_identification eiv with (nolock) on esv.entity_structure_id = eiv.entity_structure_fk and esv.data_name_fk = eiv.entity_data_name_fk and esv.data_name_fk in (143000) '
	

			When @Entity_Class_CR_FK = 10104 and @BT_Status = 1 and @ST_Status = 1 then
													' From epacube.epacube.entity_structure esc_p with (Nolock)
													Inner join epacube.epacube.entity_identification eic_p with (nolock) on esc_p.entity_structure_id = eic_p.entity_structure_fk and esc_p.data_name_fk = eic_p.entity_data_name_fk and esc_p.data_name_fk = 144010
													inner join epacube.epacube.entity_structure esc_c with (nolock) on eic_p.entity_structure_fk = esc_c.parent_entity_structure_fk and esc_c.data_name_fk = 144020
													inner join epacube.epacube.entity_identification eic with (nolock) on esc_c.entity_structure_id = eic.entity_structure_fk 
													'
			When @Entity_Class_CR_FK = 10104 and @BT_Status = 1 then
													' From epacube.epacube.entity_structure esc_p with (Nolock)
													Inner join epacube.epacube.entity_identification eic with (nolock) on esc_p.entity_structure_id = eic.entity_structure_fk and esc_p.data_name_fk = eic.entity_data_name_fk and esc_p.data_name_fk = 144010
													'
												else ' From epacube.epacube.entity_structure ESC with (nolock) '
												+ 'Inner Join epacube.entity_identification eic with (nolock) on esc.entity_structure_id = eic.entity_structure_fk and esc.data_name_fk = eic.entity_data_name_fk and esc.data_name_fk in (144010, 144020) 
												' end

		Set @TmpTbl = 'TempTbls.tmp_'	+ Case @Entity_Class_CR_FK when 10103 then 'VENDOR_' when 10104 then 'CUST_' when 10109 then 'PROD_' end + @UserID
		Set @Structure_FK = Case @Entity_Class_CR_FK when 10103 then 'VENDOR_ENTITY_STRUCTURE_FK' when 10104 then 'CUST_ENTITY_STRUCTURE_FK' when 10109 then 'PRODUCT_STRUCTURE_FK' end

		If ((Select count(*) from EPACUBE.DATA_NAME_GROUP_REFS R inner join epacube.epacube.ui_filter uif on r.data_name_fk = uif.Data_Name_FK inner join #Param3 P3 on uif.data_name_fk = P3.CritDN where r.table_name <> 'SEGMENTS_CONTACTS' and r.entity_class_cr_fk = 10103) > 0 And
			((Select count(*) from EPACUBE.DATA_NAME_GROUP_REFS R inner join epacube.epacube.ui_filter uif on r.data_name_fk = uif.Data_Name_FK inner join #Param3 P3 on uif.data_name_fk = P3.CritDN where r.table_name <> 'SEGMENTS_CONTACTS' and r.entity_class_cr_fk = 10104) > 0 OR
			(Select count(*) from EPACUBE.DATA_NAME_GROUP_REFS R inner join epacube.epacube.ui_filter uif on r.data_name_fk = uif.Data_Name_FK inner join #Param3 P3 on uif.data_name_fk = P3.CritDN where r.table_name <> 'SEGMENTS_CONTACTS' and r.entity_class_cr_fk = 10109) > 0)) And
			@VendorJoin = ''
		Begin
			Set @VendorJoin = ' inner join epacube.epacube.segments_vendor sv with (nolock) on t_vendor.vendor_entity_structure_fk = sv.VENDOR_ENTITY_STRUCTURE_FK and sv.data_level_cr_fk = 518 '
		end
		If @Entity_Class_CR_FK = 10104
			Set @VendorJoin = @VendorJoin + ' inner join TempTbls.tmp_CUST_' + @UserID + ' t_cust with (nolock) on sv.cust_entity_structure_fk = t_cust.cust_entity_structure_fk '
		If @Entity_Class_CR_FK = 10109
			Set @VendorJoin = @VendorJoin + ' inner join TempTbls.tmp_PROD_' + @UserID + ' t_prod with (nolock) on sv.product_structure_fk = t_prod.product_structure_fk '

--select @Entity_Class_CR_FK, @BT_Status, @ST_Status

			DECLARE Tbl Cursor local for

--select * from #Param3
--Declare @Entity_Class_CR_FK int
--Set @Entity_Class_CR_FK = 10104

--select * from epacube.DATA_NAME_VIRTUAL
--select * from EPACUBE.DATA_NAME_GROUP_REFS where data_name_fk in (select data_name_fk from epacube.DATA_NAME_VIRTUAL)

Select
r.Qtbl, r.data_name_fk, r.Alias, isnull(RV.Primary_Column, R.Primary_Column) 'Primary_Column'
--, Case when isnull(R.VirtualActionType, '') = 'CheckBoxTransform' then 'and ' + r.Alias + '.Record_Status_CR_FK = 1' else R.Data_Column end 'Local_Column'
, R.Data_Column 'Local_Column'
, R.Alias_DV, R.DN_Label, p3.CritValue, r.Entity_Class_CR_FK
--, Case when charindex('%', P3.CritValue) = 0 then ' = ' else ' like ' end Operator
--, Case when isnull(P3.OpExclusion, '') = '' then '' else ' Not ' end OpExcl
, Case when charindex('%', P3.CritValue) <> 0 then ' like ' when isnull(P3.OpExclusion, '') <> '' then ' <> ' else ' = ' end Operator
, Case when isnull(P3.OpExclusion, '') <> '' and charindex('%', P3.CritValue) <> 0 then ' Not ' else '' end OpExcl
, Case when R.entity_class_cr_fk = 10109 then R.[Product_Structure_Column] else 'Entity_Structure_FK' end 'Primary_Structure_Column'
--, R.[Local_Structure_Column]
, R.DATA_COLUMN
, Case when isnull(r.VirtualColumnCheck, '') <> '' then 1 else 0 end ind_VirtualColumnCheck
, isnull(R.VirtualActionType, '') --= 'CheckBoxTransform'
from EPACUBE.DATA_NAME_GROUP_REFS  R
			inner join epacube.epacube.ui_filter uif on r.data_name_fk = uif.Data_Name_FK
			inner join #Param3 P3 on uif.data_name_fk = P3.CritDN
			left join EPACUBE.DATA_NAME_GROUP_REFS RV on R.V_Ref_Data_Name_FK = RV.Data_Name_FK and isnull(R.data_level_CR_FK, 0) = isnull(RV.data_level_CR_FK, 0)
			where	1 = 1
					and isnull(r.V_Ref_Data_Name_FK, 0) <> 159905
					and r.entity_class_cr_fk in (@Entity_Class_CR_FK)
				OPEN Tbl;
				FETCH NEXT FROM Tbl INTO @Qtbl, @DN_FK, @Alias, @Primary_Column, @Local_Column, @Alias_DV, @DN_Label, @CritValue, @R_EC_CR_FK, @Operator, @OpExcl, @Primary_Structure_FK, @Local_Structure_FK, @ind_VirtualColumnCheck, @VirtualAction
				WHILE @@FETCH_STATUS = 0

				Begin
				Set @Alias = Case When @Entity_Class_CR_FK = 10104 and @BT_Status = 1 and  @ST_Status = 0 then Replace(@Alias, 'EIC_P', 'EIC') else @Alias end
--Select @Alias
					Set @KeyCols = Case 
					When @KeyCols not like '%' + @Primary_Structure_FK + '%' then Case @Entity_Class_CR_FK when 10104 then 'EIC' when 10103 then 'EIV' else @Prod_Alias end + '.[' + @Primary_Structure_FK + '] ''' + @Local_Structure_FK + ''',  ' 
					else @KeyCols + '' end

				If @Local_Structure_FK in ('Product_Structure_FK', 'Cust_Entity_Structure_fk', 'Vendor_Entity_Structure_FK', 'Entity_Structure_FK') and ((@R_EC_CR_FK = 10124 and @Entity_Class_CR_FK in (10103, 10104)) or @R_EC_CR_FK <> 10124) and @DN_FK not in (144010, 144020)
					and @From not like '%' + @Alias + '%'
					Begin
						Set @From = @From + Case when @ind_VirtualColumnCheck = 0 then '
							inner join ' + @Qtbl + ' ' + @Alias + ' on ' + Case @Entity_Class_CR_FK when 10109 then '' + @Prod_Alias + '.[' when 10104 then 'EIC.['  when 10103 then 'EIV.[' end + @Primary_Structure_FK  + '] = ' + @Alias + '.[' + @Local_Structure_FK + '] and ' +
							@Alias + '.Data_Name_FK = ' + @DN_FK
							Else '
							inner join ' + @Qtbl + ' ' + @Alias + ' on ' + Case @Entity_Class_CR_FK when 10109 then '' + @Prod_Alias + '.[' when 10104 then 'EIC.['  when 10103 then 'EIV.[' end + @Primary_Structure_FK  + '] = ' + @Local_Structure_FK + ' and ' +
							@Alias + '.Data_Name_FK = ' + @DN_FK
							End
					End
					
				If @Local_Column not in ('Data_Value_FK', 'Entity_Data_Value_FK', 'Record_status_cr_fk') and @Local_Column not like '%Structure%' and ((@R_EC_CR_FK = 10124 and @Entity_Class_CR_FK in (10103, 10104)) or @R_EC_CR_FK <> 10124)
					Begin

					Set @Select = @Select + Case when @ind_VirtualColumnCheck = 0 then @Alias + '.[' + @Local_Column + '] ' else @Local_Column end + ' ''' + @DN_Label + ''', '
					Set @Where = @Where + Case when isnull(@CritValue, '') <> '' then ' and ' + 
						Case	when @ind_VirtualColumnCheck = 1 and @VirtualAction = 'CheckBoxTransform' then @Alias + '.Record_Status_CR_FK ' + @OpExcl + ' ' + @Operator + ' ' + @CritValue
								when @ind_VirtualColumnCheck = 1 then @Local_Column
								else @Alias + '.[' + @Local_Column + '] ' + @OpExcl + ' ' + @Operator + '''' + @CritValue + '''' end
						else @Where end 

						--Case when @ind_VirtualColumnCheck = 0 then @Alias + '.[' + @Local_Column + '] ' 
						--when @VirtualAction = 'CheckBoxTransform' then @Alias + '.Record_Status_CR_FK ' + @OpExcl + ' ' + @Operator + '''' + @CritValue + ''''
						--else  @Local_Column end + @OpExcl + ' ' + @Operator + '''' + @CritValue + '''' else @Where end
--Select @Select '@Select', @ind_VirtualColumnCheck '@ind_VirtualColumnCheck', @Alias '@Alias', @Local_Column '@Local_Column', @DN_Label '@DN_Label', @OpExcl '@OpExcl', @Operator '@Operator', @CritValue '@CritValue', @OpExcl + ' ' + @Operator + '''' + @CritValue + ''''
					End

				if @Primary_Column in ('data_value_fk', 'entity_data_value_fk') and ((@R_EC_CR_FK = 10124 and @Entity_Class_CR_FK in (10103, 10104)) or @R_EC_CR_FK <> 10124)
					Begin
						Set @From = @From + '
							Inner join ' + Case @Primary_Column when 'Data_Value_FK' then 'epacube.data_value' else 'epacube.entity_data_value' end + ' ' + @Alias_DV + ' on ' + @Alias + '.[' + @Local_Column + '] = ' + @Alias_DV + '.[' + Replace(@Primary_Column, '_FK', '_ID') + '] '
						Set @Select = @Select + @Alias_DV + '.[value] ''' + @DN_Label + ''', ' + @Alias_DV + '.[Description] ''' + @DN_Label + '_Description'', '
						Set @Where = Case when isnull(@CritValue, '') <> '' then @Where + ' and ' + @Alias_DV + '.[Value] ' + @OpExcl + ' ' + @Operator +  '''' + @CritValue + '''' else @Where end
						Set @KeyCols = Case When @KeyCols not like '%' + @Local_Column then @KeyCols + @Alias + '.[' + @Local_Column + '] ''' + @Local_Column + ''',  ' else '' end
--select @Prod_Alias, @Select, @Alias, @Local_Column, @DN_Label, @Alias_DV
					End

				If @Qtbl like '%SEGMENTS_CONTACTS%' and @R_EC_CR_FK = 10124 and @From like '%' + @Alias + '%' and  @Entity_Class_CR_FK in (10103, 10104)
					
					Set @From = @From + ' and ' + @Alias + '.entity_class_cr_fk = ' + @Entity_Class_CR_FK

				FETCH NEXT FROM Tbl INTO @Qtbl, @DN_FK, @Alias, @Primary_Column, @Local_Column, @Alias_DV, @DN_Label, @CritValue, @R_EC_CR_FK, @Operator, @OpExcl, @Primary_Structure_FK, @Local_Structure_FK, @ind_VirtualColumnCheck, @VirtualAction

				If @from like '%' + @Alias + '.[' + @Primary_Column + ']' + '%' and @Primary_Column like '%Structure%'--and @Alias not in ('esc_p', 'esc_c', 'esc', 'eic_p', 'eic_c', 'eic') --and @Select not like '%' + @Alias + '.[' + @Local_Column + ']%'
				Set @Alias = @Alias + '_1'
				
			End
			
			Close Tbl;
			Deallocate Tbl;
--Select @DN_FK, @Select 

		Set @Select = Left(@Select, Len(rtrim(@Select)) - 1)
		Set @Select = Right(@Select, Len(@Select) - 6)
		Set @Select = 'Select ' + @KeyCols + @Select

		Set @SqlEXEC = Replace(Replace((@Select + @From + ' ' + @Where), '  ', ' '), '  ', ' ')

		If @QueryExec = 1
			Print(@SqlEXEC)
		Else
		If @QueryExec = 2
			Exec(@SqlEXEC)

		Set @SqlEXEC = 'Select distinct ' + @Local_Structure_FK + '
		from (' + @SqlEXEC + ') A'

--Select @Select, @From, @QueryExec, @Alias, @Prod_Alias

--print(@SqlEXEC)
	If (Select count(*) from #Param3 P3 inner join EPACUBE.DATA_NAME_GROUP_REFS R on p3.CritDN = R.data_name_fk where r.entity_class_cr_fk = @Entity_Class_CR_FK and isnull(p3.CritValue, '') not in ('', '0')) > 0
	Begin
		Exec('If Object_ID(''' + @TmpTbl + ''') is null
		Begin	
		Create Table ' + @TmpTbl + '(' + @Structure_FK + ' bigint not null);
		Create index idx_st on ' + @TmpTbl + '(' + @Structure_FK + ')
		End')

		Set @SqlEXEC = 'Insert into ' + @TmpTbl + ' (' + @Structure_FK + ') ' + @SqlEXEC

--Select @SqlEXEC
--print(@SqlEXEC)
		exec(@SqlEXEC)
	End
		FETCH NEXT FROM Entity_Class INTO @Entity_Class_CR_FK
		End

	Close Entity_Class;
	Deallocate Entity_Class;

------get filter code

	Set @DN_String = Case	when @DN_String <> '' and right(ltrim(rtrim(@DN_String)), 1) <> '~' then ltrim(rtrim(@DN_String)) + '~' 
							when @DN_String = '' and @Data_Name_FK <> 0 and @Data_Name_FK not like '%~' then @Data_Name_FK + '~'
					 else @DN_String end

	While charindex('~', @DN_String, @DN_StartPOS) <> 0 
		Begin

			Set @DN_POS = charindex('~', @DN_String, @DN_StartPOS)
			Set @DN_Length = @DN_POS - @DN_StartPOS
			Set @Data_Name_FK = Substring(@DN_String, @DN_StartPOS, @DN_Length)

			Set @DN_StartPOS = @DN_POS + 1

			Set @Data_Name_FK = Case when isnumeric(@Data_Name_FK) = 1 then @Data_Name_FK else (Select data_name_id from epacube.data_name where name = '' + @Data_Name_FK + '') end

	If object_id('tempdb..#DNFltrCursor') is not null
	drop table #DNFltrCursor
--Select * from #DNFltrCursor order by pass Select * from EPACUBE.DATA_NAME_GROUP_REFs where data_name_fk in (110100, 550100, 550020)

	--Declare @Data_Name_FK varchar(16)
	--Set @Data_name_fk = 110100 --550001 --550020 --550020, 550100, 110100

--Select * from #DNFltrCursor
--Select @Data_Name_FK

		Select  *, Rank()over(order by Case when isnull(Virtual_Data_Name_FK, Data_Name_FK) = @Data_Name_FK then 1 When Filter_Entity_Class_CR_FK = Column_Entity_Class_CR_FK and qtbl not like '%product_association%' then 2 when qtbl like '%product_association%' then 3 else data_name_fk end) Pass
		into #DNFltrCursor
		from (
		Select Lookup_Values, Alias_DV, Local_Column, QTbl, Filter_Entity_Class_CR_FK, Column_Entity_Class_CR_FK, DN_Label, Max(Vendor_Structure_Column) 'Vendor_Structure_Column', DN_Type, Op, VirtualJoin, Alias, Ref_DN_FK
			, Max(Customer_Structure_Column) 'Customer_Structure_Column', Max(Product_Structure_Column) 'Product_Structure_Column', VirtualActionType, VirtualColumnCheck, Virtual_Data_Name_FK, Data_Name_FK, Where_Column
		from (Select  
		isnull(Uf.Lookup_Values, 0) 'Lookup_Values'
		, isnull(r.ALIAS_DV, '') 'Alias_DV'
		, R.Data_Column Local_Column
		,  Replace(ltrim(rtrim(R.QTbl)), 'Entity_Structure', 'Entity_Identification') QTbl
		, (Select top 1 Entity_class_cr_fk from EPACUBE.DATA_NAME_GROUP_REFS where data_name_fk = @data_name_fk and entity_class_cr_fk is not null) FILTER_ENTITY_CLASS_CR_FK
		, R.Entity_Class_CR_FK Column_entity_Class_CR_FK
		, R.DN_Label
		, r.Vendor_Structure_Column
		, Case when isnull(dnv.REFERENCE_DATA_NAME_FK, r.data_name_fk) in (144010, 144020) then 'Entity_Data_Name_FK' else 'Data_Name_FK' end 'DN_Type'
		, Case	when uf.checkbox_y_n = 1 and isnull((select OpExclusion from #param3 p3a where p3a.critdn = r.V_Ref_Data_Name_FK), '') like '%!%' then ' not in ' else ' in ' end Op
		, Case	when dnv.REFERENCE_DATA_NAME_FK is not null and isnull((select OpExclusion from #param3 p3a where p3a.critdn = dnv.REFERENCE_DATA_NAME_FK), '0') = '0' then ' left '
				when dnv.REFERENCE_DATA_NAME_FK is not null and isnull((select OpExclusion from #param3 p3a where p3a.critdn = dnv.REFERENCE_DATA_NAME_FK), '0') <> '0' then ' inner ' else '0' end VirtualJoin
		, R.Alias
		, isnull(dnv.REFERENCE_DATA_NAME_FK, r.data_name_fk) Ref_DN_FK
		, r.Customer_Structure_Column
		, r.Product_Structure_Column 
		, r.VirtualActionType
		--, ' ' + Replace(isnull(r.VirtualColumnCheck, ''), DNV.data_name_FK, dnv.REFERENCE_DATA_NAME_FK) VirtualColumnCheck
		, Case when isnull(R.VirtualActionType, '') = 'CheckBoxTransform' then ' and ' + r.Alias + '.Record_Status_CR_FK = 1' when isnull(R.VirtualActionType, '') = 'WhenRecordExists' then '' else R.Data_Column end 'VirtualColumnCheck'
		, DNV.data_name_FK Virtual_Data_Name_FK
		, isnull(dnv.REFERENCE_DATA_NAME_FK, r.data_name_fk) data_name_fk
		, Case when isnull(R.VirtualActionType, '') = 'CheckBoxTransform' then ' and ' + r.Alias + '.Record_Status_CR_FK = 1' when isnull(R.VirtualActionType, '') = 'WhenRecordExists' then '' else R.Data_Column end 'Where_Column'
		from EPACUBE.DATA_NAME_GROUP_REFS R
		left join #Param3 P3 on r.Data_Name_FK = P3.critdn
		left join epacube.DATA_NAME_VIRTUAL DNV on R.data_name_fk = DNV.data_Name_FK --in (dnv.data_name_fk, dnv.REFERENCE_DATA_NAME_FK) and DNV.data_name_fk in (select critdn from #Param3) --and DNV.ACTION_TYPE in ('CheckBoxTransform', 'RecordStatusCheck')
		left join epacube.ui_filter uf on r.data_name_fk = uf.Data_Name_FK
		Where 1 = 1
			and (
				(dnv.REFERENCE_DATA_NAME_FK is not null and r.data_name_fk = dnv.data_name_fk and isnull(P3.critvalue, '') <> '')
				or r.data_name_fk = @Data_Name_FK
				)
		) A  
		Group by Lookup_Values, Alias_DV, Local_Column, QTbl, Filter_Entity_Class_CR_FK, Column_Entity_Class_CR_FK, DN_Label, DN_Type, Op, VirtualJoin, Alias, Ref_DN_FK
			, VirtualActionType, VirtualColumnCheck, Virtual_Data_Name_FK, Data_Name_FK, Where_Column
		) B Order by pass 

	DECLARE DNFltrCode Cursor local for
	Select * from #DNFltrCursor order by pass

		Set @SQLcodeStart = ''
		Set @SQLcodeIn = ''
		Set @SQLcodeNotIn = ''

		OPEN DNFltrCode;
		Set @Max_Pass = @@CURSOR_ROWS;
		
		FETCH NEXT FROM DNFltrCode INTO @Lookup_Values, @Alias_DV, @Local_Column, @QTbl, @FILTER_ENTITY_CLASS_CR_FK, @Column_Entity_Class_CR_FK, @DN_Label, @Vendor_Structure_Column, @DN_Type, @Op, @VirtualJoin, @Alias, @Ref_DN_FK, @Customer_Structure_Column, @Product_Structure_Column, @VirtualAction, @VirtualColumnCheck, @Virtual_DN_FK, @DNtmp, @Where_Column, @Pass
		WHILE @@FETCH_STATUS = 0

		Begin--Fetch

		IF (select [epacube].getDataTypeForDN(@Data_Name_FK)) = 'Date'
		goto nextfilter

		If @Filter_Entity_Class_CR_FK = 10109
		Begin

			Set @ImmedAlias = Case when Object_ID('TempTbls.tmp_PROD_' + @UserID) is not null then 't_prod' else @Prod_Alias end

			Set @SQLcodeStart = Case when @SQLcodeStart <> '' then @SQLcodeStart else 
				Case when @Pass = 1 and Object_ID('TempTbls.tmp_PROD_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK and isnull(@VirtualAction, '') = 'CheckBoxTransform' then
		'(Select ' + Replace(@Local_Column, @Prod_Alias + '.', '') + ' ''KEY'', ' + Replace(@Local_Column, @Prod_Alias + '.', '') + '  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @Ref_DN_FK
					when @Pass = 1 and Object_ID('TempTbls.tmp_PROD_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
		'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + ']  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @Ref_DN_FK					
					when @Pass = 1 and Object_ID('TempTbls.tmp_PROD_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
		'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + ']  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @Ref_DN_FK
				else '' end		End

			Set @SQLcodeIn = @SQLcodeIn + 
				Case	when @SQLcodeIn = '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform')  and (@Pass <> 1 or @Max_Pass = 1) and Object_ID('TempTbls.tmp_PROD_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		and [product_structure_fk] in (select ' + @ImmedAlias + '.[product_structure_fk] from TempTbls.tmp_PROD_' + @UserID + ' ' + @ImmedAlias + ' '
						when @SQLcodeIn = '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and (@Pass <> 1  or @Max_Pass = 1) and Object_ID('TempTbls.tmp_PROD_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then ' 
		and product_structure_fk in (select ' + @Prod_Alias + '.product_structure_Fk from epacube.product_identification ' + @ImmedAlias + ' with (nolock) '
				else '' end

--Select @SQLcodeIn '@SQLcodeIn', @op '@op', @VirtualAction '@VirtualAction', @Pass '@Pass', @Max_Pass '@Max_Pass', @COLUMN_ENTITY_CLASS_CR_FK '@COLUMN_ENTITY_CLASS_CR_FK', @FILTER_ENTITY_CLASS_CR_FK '@FILTER_ENTITY_CLASS_CR_FK' 

			Set @SQLcodeIn = @SQLcodeIn + 
				Case	when @SQLcodeIn <> '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and @Pass <> 1 and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @ImmedAlias + '.[Product_Structure_FK] = ' + @Alias + '.[Product_Structure_FK] and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + Case when @Op = ' in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end 
		+ Case when @SQLcodeIn not like '%' + @Item_Data_Name_FK + '%' and @ImmedAlias <> 't_prod' then ' and ' + @Prod_Alias + '.data_name_fk = ' + @Item_Data_Name_FK else '' end
				else '' end

			Set @SQLcodeIn = @SQLcodeIn + 
				Case	when @SQLcodeIn <> '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join TempTbls.tmp_CUST_' + @UserID + ' t_cust with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = t_cust.cust_entity_structure_fk '
					else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and(@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK 
					and @SQLcodeIn like '%t_cust%'	then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = t_cust.cust_entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + Case when @Op = ' in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end
					else '' end

		Set @SQLcodeNotIn = @SQLcodeNotIn + 
			Case	when @SQLcodeNotIn = '' and @op = ' not in ' and Object_ID('TempTbls.tmp_PROD_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK and @SQLcodeIn not like '%' + @Alias + '%'	then '
		and [product_structure_fk] not in (select ' + @ImmedAlias + '.[product_structure_fk] from TempTbls.tmp_PROD_' + @UserID + ' ' + @ImmedAlias + ' '
					when @SQLcodeNotIn = '' and @op = ' not in ' and Object_ID('TempTbls.tmp_PROD_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK and @SQLcodeIn not like '%' + @Alias + '%'	 then '
		and product_structure_fk not in (select ' + @Prod_Alias + '.product_structure_Fk from epacube.product_identification ' + @ImmedAlias + ' with (nolock) '
			else '' end

		Set @SQLcodeNotIn = @SQLcodeNotIn + 
			Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' +@VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @ImmedAlias + '.[Product_Structure_FK] = ' + @Alias + '.[Product_Structure_FK] and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + @VirtualColumnCheck
					else '' end
		Set @SQLcodeNotIn = @SQLcodeNotIn + 
		+	Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join TempTbls.tmp_CUST_' + @UserID + ' t_cust with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = t_cust.cust_entity_structure_fk '
				else '' end

		Set @SQLcodeNotIn = @SQLcodeNotIn + 
			Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and isnull(@VirtualAction, '') = 'CheckBoxTransform' and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK 
					and @SQLcodeNotIn like '%t_cust%' then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = t_cust.cust_entity_structure_fk and ' + @DN_Type + ' = ' + @Ref_DN_FK + @VirtualColumnCheck
				else '' end

	End
	Else
	If @Filter_Entity_Class_CR_FK = 10104
	Begin

		Set @ImmedAlias = Case when Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null then 't_cust' else 'eic' end
		Set @Cust_Structure_FK_Init = Case when @Pass = 1 then @Customer_Structure_Column else @Cust_Structure_FK_Init end
		Set @PA_Alias = Case when @Qtbl like '%product_association%' then @Alias else @PA_Alias end

		Set @SQLcodeStart = Case when @SQLcodeStart <> '' then @SQLcodeStart else 
			Case when @Pass = 1 and Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK and isnull(@VirtualAction, '') = 'CheckBoxTransform' then
			'(Select ' + Replace(@Local_Column, @Alias + '.', '') + ' ''KEY'', ' + Replace(@Local_Column, @Alias + '.', '') + '  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @Ref_DN_FK
			when @Pass = 1 and Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
			'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + ']  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @Ref_DN_FK
				when @Pass = 1 and Object_ID('TempTbls.tmp_CUST_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
			'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + ']  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @Ref_DN_FK
			else '' end			End

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn = '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform')  and (@Pass <> 1 or @Max_Pass = 1) and Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null then '
		and [' + @Cust_Structure_FK_Init + '] in (select ' + @ImmedAlias + '.[Cust_Entity_Structure_FK] from TempTbls.tmp_CUST_' + @UserID + ' ' + @ImmedAlias + ' '
					when @SQLcodeIn = '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and (@Pass <> 1 or @Max_Pass = 1) and Object_ID('TempTbls.tmp_CUST_' + @UserID) is null then '
		and [' + @Cust_Structure_FK_Init + '] in (select eic.entity_structure_fk from epacube.entity_identification ' + @ImmedAlias + ' with (nolock) '
			else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and @Pass <> 1 and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @ImmedAlias + '.[' 
				+ Case when @ImmedAlias = 'eic' then 'entity_structure_fk' else 'cust_entity_structure_fk' end
				 + '] = ' + @Alias + '.[' + @Customer_Structure_Column + '] and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + Case when @Op = ' in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end 
			else '' end
							
		Set @SQLcodeIn = @SQLcodeIn + --Product_Association is included here
			Case	when @SQLcodeIn <> '' and(@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null 
					and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK 
					then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = t_cust.cust_entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK 
			+ Case when @Op = ' in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end
			else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and(@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is null 
					and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = eic.entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK 
				+ Case when @Op = ' in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end
			else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and @op = ' in ' and @Pass <> 1 and @Product_Structure_Column <> '0' and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK
					and @SQLcodeIn not like '%t_prod%' and @SQLcodeIn not like '%epacube.product_identification pi%'
					and @SQLcodeIn like '%PRODUCT_ASSOCIATION%'
					then '
			' + @VirtualJoin + 'join ' + Case	when Object_ID('TempTbls.tmp_Prod_' + @UserID) is not null then 'TempTbls.tmp_Prod_' + @UserID + ' t_PROD with (nolock) on ' + @Alias + '.[Product_Structure_FK] = t_prod.product_structure_fk '
												when Object_ID('TempTbls.tmp_Prod_' + @UserID) is null then 'epacube.product_identification ' + @Prod_Alias + ' with (nolock) on ' + @PA_Alias + '.[Product_Structure_FK] = ' + @Prod_Alias + '.product_structure_fk and ' + @Prod_Alias + '.data_name_fk = ' 
												+ @Item_Data_Name_FK + ' ' + @VirtualColumnCheck
										 else '' end
			else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and @Pass <> 1 and @Product_Structure_Column <> '0' and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK
					and (@SQLcodeIn like '%t_prod%' or @SQLcodeIn like '%epacube.product_identification pi%' or @SQLcodeIn like '%PRODUCT_ASSOCIATION%')
					and @qtbl not like '%PRODUCT_ASSOCIATION%'
					then '
			' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + Case when @SQLcodeIn like '%t_prod%' then 't_prod' when @SQLcodeIn like '%PI_%' then @Prod_Alias when @SQLcodeIn like '%Product_Association%' then  @PA_Alias end 
			+ '.[' + @Product_Structure_Column + '] = ' + @Alias + '.[Product_Structure_FK] and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + ' ' + Case when @Op = ' in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end  									
				 --else '123321123' 
				 else ''
				 end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn = '' and @op = ' not in ' and Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null then '
	and [' + @Cust_Structure_FK_Init +'] not in (select ' + @ImmedAlias + '.[cust_entity_structure_fk] from TempTbls.tmp_CUST_' + @UserID + ' ' + @ImmedAlias + ' '
				when @SQLcodeNotIn = '' and @op = ' not in ' and Object_ID('TempTbls.tmp_CUST_' + @UserID) is null then '
	and [' + @Cust_Structure_FK_Init +'] not in (select eic.entity_structure_Fk from epacube.entity_identification ' + @ImmedAlias + ' with (nolock) '
		else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Pass <> 1 and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
	' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @ImmedAlias + '.[' + Case when @ImmedAlias = 'eic' then 'entity_structure_fk' else 'cust_entity_structure_fk' end + '] = ' + @Alias + '.[' + @Customer_Structure_Column + '] and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + ' ' + @VirtualColumnCheck
		else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK 
				then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = t_cust.cust_entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + ' ' + @VirtualColumnCheck
			else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> ''and @op = ' not in ' and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = eic.entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + ' ' + @VirtualColumnCheck
			else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Pass <> 1 and @Product_Structure_Column <> '0' and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK
				and @SQLcodeNotIn not like '%t_prod%' and @SQLcodeNotIn not like '%epacube.product_identification pi%'  
				then '
		' + @VirtualJoin + 'join ' + Case	when Object_ID('TempTbls.tmp_Prod_' + @UserID) is not null then 'TempTbls.tmp_Prod_' + @UserID + ' t_PROD with (nolock) on ' + @Alias + '.[Product_Structure_FK] = t_prod.product_structure_fk '
											when Object_ID('TempTbls.tmp_Prod_' + @UserID) is null then 'epacube.product_identification ' +  @Prod_Alias + ' with (nolock) on ' + @PA_Alias + '.[Product_Structure_FK] = ' + @Prod_Alias + '.product_structure_fk and ' + @Prod_Alias + '.data_name_fk = ' + @Item_Data_Name_FK + ' ' + @VirtualColumnCheck
									 else '' end
		else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> '' and @Pass <> 1 and @Product_Structure_Column <> '0' and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK
				and (@SQLcodeNotIn like '%t_prod%' or @SQLcodeNotIn like '%epacube.product_identification pi%' or @SQLcodeNotIn like '%EPACUBE.[PRODUCT_ASSOCIATION]%')
				and @qtbl not like '%PRODUCT_ASSOCIATION%'
				then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + Case when @SQLcodeNotIn like '%t_prod%' then 't_prod' when @SQLcodeNotIn like '%PI_%' then @Prod_Alias when @SQLcodeNotIn like '%Product_Association%' then  @PA_Alias end + '.[' + @Product_Structure_Column + '] = ' + @Alias + '.[Product_Structure_FK] and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + ' ' + Case when @Op = ' not in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end  									
			 else '' end
	
	End
	
	Else
	If @Filter_Entity_Class_CR_FK = 10103
	Begin

		Set @SQLcodeStart = Case when Object_ID('TempTbls.tmp_Vendor_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
	'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + '] ''LABEL'' from epacube.' + @QTbl + ' where data_name_fk = ' + @Data_Name_FK 
	+ ' and ' + @Vendor_Structure_Column + ' in (select t_Vendor.Vendor_Entity_Structure_fk from TempTbls.tmp_Vendor_' + @UserID + ' t_Vendor with (nolock)'
								 when Object_ID('TempTbls.tmp_Vendor_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
	'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + '] ''LABEL'' from epacube.' + @QTbl + ' where data_name_fk = ' + @Data_Name_FK
	else isnull(@SQLcodeStart, '')
	End

	End
NextFilter:		

	FETCH NEXT FROM DNFltrCode INTO @Lookup_Values, @Alias_DV, @Local_Column, @QTbl, @FILTER_ENTITY_CLASS_CR_FK, @Column_Entity_Class_CR_FK, @DN_Label, @Vendor_Structure_Column, @DN_Type, @Op, @VirtualJoin, @Alias, @Ref_DN_FK, @Customer_Structure_Column, @Product_Structure_Column, @VirtualAction, @VirtualColumnCheck, @Virtual_DN_FK, @DNtmp, @Where_Column, @Pass
	End--Fetch
	Close DNFltrCode;
	Deallocate DNFltrCode;

		Set @SQLcodeIn = @SQLcodeIn + Case when @SQLcodeIn <> '' then  ')' else '' end
		Set @SQLcodeNotIn = @SQLcodeNotIn + Case when @SQLcodeNotIn <> '' then  ')' else '' end

	If @Lookup_Values <> 1
	Begin
		Set @SQLcodeIn = ''
		Set @SQLcodeNotIn = ''
	End

		Set @Filtercode = ( @SQLcodeStart + '
		' + @SQLcodeIn + '
		' + @SQLcodeNotIn + ')')

	If @Alias_DV <> ''
		Begin
			Set @POS = charindex(' from ', @Filtercode)
			Set @Filtercode = Right(@Filtercode, Len(@Filtercode) - @POS)
			Set @Filtercode = 'Select data_value_fk ' + @Filtercode
			Set @Filtercode = 
				'(select value ''KEY'', [Description] ''LABEL'' from epacube.data_value where data_value_id in (' + @Filtercode + ')' 
		End

		If ltrim(rtrim(@Filtercode)) > ' '
		Begin
			Set @FilterCode = '((((((' + ltrim(rtrim(@FilterCode)) + '))))))'
			Set @FilterCode = Replace(Replace(@FilterCode, '(((((((', ''), ')))))))', '')
		End				

		If Len(@FilterCode) > 10
		Set @QueryScript = @QueryScript + 'Select ' + @Data_Name_FK + ' ''Data_Name_FK'', ''' + Replace(@FilterCode, '''', '''''') + ''' ''SQL_Query'' Union
'
	End --End of While Loop

	Set @QueryScript = Case when Right(ltrim(rtrim(@QueryScript)), 7) like '%Union%' then Left(ltrim(rtrim(@QueryScript)), Len(ltrim(rtrim(@QueryScript))) - 7) else @QueryScript end


	If @FilterResultsPrint = 1
	Begin
		Select @QueryScript
		
		--Print(@QueryScript)

		goto EOP
		
	end

	If @FilterResultsExec = 1
	Begin
		Exec(@QueryScript)
		GoTo EOP
	end
	Else
	If @FilterResultsExec = 2
	Begin
		Exec(@Filtercode)
		
		goto EOP
		
	end
Last_Step:
	
	--Select @QueryScript = Case when @Lookup_Values = 2 then Replace(Replace(Replace((Select initialization_sql from epacube.ui_filter where data_name_fk = @Data_Name_FK), ' .', '.'), '(', ''), ')', '') else '' end
	
EOP:
