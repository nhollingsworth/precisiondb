﻿


-- Copyright 2007
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to create/modify/activate/inactivate/delete UOM Aliases
-- preliminary version has no validation
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        09/24/2007   Initial SQL Version

 CREATE PROCEDURE [epacube].[UOM_CODE_DIM_ALIAS_DM] 
( @in_uom_code_dim_alias_id int,
  @in_uom_code_dim_fk int,
  @in_uom_code varchar (64),
  @in_display_seq int,  
  @in_record_status_cr_fk int,  
  @in_ui_action_cr_fk int,
  @in_update_user varchar(64) )
												
AS

/***************  Parameter Defintions  **********************

***************************************************************/

BEGIN

DECLARE @l_sysdate				datetime
DECLARE @ls_stmt				varchar (1000)
DECLARE @l_exec_no				bigint
DECLARE @l_rows_processed		bigint
DECLARE @v_count				bigint
DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int

SET NOCOUNT ON;
SET @l_sysdate = getdate ()
set @ErrorMessage = null


-------------------------------------------------------------------------------------
--   Peform UI Delete Action 
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = 8004
BEGIN
	DELETE FROM epacube.UOM_CODE_DIM_ALIAS
	WHERE UOM_CODE_DIM_ALIAS_ID = @in_uom_code_dim_alias_id
	return (0)
END -- IF @in_ui_action_cr_fk = 8004

-------------------------------------------------------------------------------------
--   Peform UI Activate Action 
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = 8007  -- activate
BEGIN
	UPDATE epacube.uom_code_dim_alias
	       set record_status_cr_fk = 1
           ,update_timestamp = getdate()
		   ,update_user = @in_update_user
	       where uom_code_dim_alias_id = @in_uom_code_dim_alias_id	
		   return (0)
END -- IF @in_ui_action_cr_fk = 8007

-------------------------------------------------------------------------------------
--   Peform UI Inactivate Action 
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = 8005  -- inactivate
BEGIN

	UPDATE epacube.uom_code_dim_alias
	       set record_status_cr_fk = 2
		   ,update_timestamp = getdate()
		   ,update_user = @in_update_user
	       where uom_code_dim_alias_id = @in_uom_code_dim_alias_id		
		   return (0)
END -- IF @in_ui_action_cr_fk = 8005
-------------------------------------------------------------------------------------
-- Perform Server-side validation 
-- UOM Alias must be entered, must not be a duplicate alias
-------------------------------------------------------------------------------------
IF @in_ui_action_cr_fk in (8003,8006) --8003=create,8006=change
BEGIN

	IF  @in_uom_code IS NULL
    BEGIN
		SET @ErrorMessage = 'Please enter UOM Dimension Alias'
        GOTO RAISE_ERROR
	END	

-------------------------------------------------------------------------------------
-- Perform UI Create Action
-------------------------------------------------------------------------------------


  IF @in_ui_action_cr_fk = 8003 --create	
  BEGIN

-------------------------------------------------------------------------------------
--   Check for Duplicate Prior to Insert
-------------------------------------------------------------------------------------

    select @ErrorMessage = 'Duplicate UOM Dim Alias. UOM Dim Alias ID = ' + 
             	+ CAST(isnull(UCDA.UOM_CODE_DIM_ALIAS_ID,0) AS VARCHAR(30))
    from epacube.uom_code_DIM_alias UCDA
    where upper(ucda.uom_code) = upper(@in_uom_code)

			 IF @@rowcount > 0
					GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
--   Edits passed Insert Row
-------------------------------------------------------------------------------------

    INSERT INTO epacube.UOM_CODE_DIM_ALIAS
    (
	 uom_code_dim_fk,
     uom_code,     	      
     display_seq,
     record_status_cr_fk,
	 create_timestamp,
	 update_timestamp,
	 update_user     
    )
    SELECT
	@in_uom_code_dim_fk,
    upper(@in_uom_code),
    @in_display_seq,
    @in_record_status_cr_fk,    
    getdate(),                
	getdate(),
	@in_update_user    
  END --- IF @in_ui_action_cr_fk = 8003


-------------------------------------------------------------------------------------
--   Perform UI Change Action
-------------------------------------------------------------------------------------
  IF @in_ui_action_cr_fk = 8006 --change
  BEGIN

    select @ErrorMessage = 'UOM Dim Alias ' 
				+ upper(@in_uom_code)
				+ ' already exists. UOM Dim Alias ID = ' + 
             	+ CAST(isnull(UCDA.UOM_CODE_DIM_ALIAS_ID,0) AS VARCHAR(30))
    from epacube.uom_code_dim_alias UCDA
    where upper(ucda.uom_code) = upper(@in_uom_code)
	  and ucda.uom_code_dim_fk <> @in_uom_code_dim_fk

			 IF @@rowcount > 0
					GOTO RAISE_ERROR
	
	UPDATE epacube.uom_code_dim_alias
		set uom_code = upper(@in_uom_code)
			,uom_code_dim_fk = @in_uom_code_dim_fk			
			,display_seq = @in_display_seq
			,record_status_cr_fk = @in_record_status_cr_fk
			,update_user = @in_update_user			
			,update_timestamp = getdate()
		where uom_code_dim_alias_id = @in_uom_code_dim_alias_id


  End -- IF @in_ui_action_cr_fk = 8006

End --IF @in_ui_action_cr_fk in (8003,8006) -validation block





RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END







































