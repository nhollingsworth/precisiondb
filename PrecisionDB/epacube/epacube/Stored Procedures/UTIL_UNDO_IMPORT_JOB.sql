﻿







-- Copyright 2011
--
-- Procedure Called from Synchronizer UI to submit events for approval
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -----------------------------------------------
-- KS        09/27/2011   Original SQL Version 
--

CREATE PROCEDURE [epacube].[UTIL_UNDO_IMPORT_JOB] 
        @IN_JOB_FK BIGINT
AS
BEGIN

DECLARE @l_sysdate    datetime
DECLARE @ls_stmt      varchar (1000)
DECLARE @ls_queue     nvarchar (4000)
DECLARE @l_exec_no    bigint
DECLARE @l_rows_processed    BIGINT

DECLARE @v_batch_no   bigint
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

DECLARE @status_desc  varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

BEGIN TRY
SET NOCOUNT ON
SET @l_sysdate = getdate ()

SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of epacube.UTIL_UNDO_IMPORT_JOB', 
												@exec_id = @l_exec_no OUTPUT;


----------------------------------------------------------------------------------------------------
--  First UNDO all the Approved Events from the Import JOB 
--        Assign Batch_No  ( but only APPROVED from import job )
--        Then call synchronizer.EVENT_POSTING_UNDO
---------------------------------------------------------------------------------------------------


	EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'batch_seq', @v_batch_no output

    UPDATE synchronizer.EVENT_DATA_HISTORY
    SET BATCH_NO = @v_batch_no
    WHERE EVENT_STATUS_CR_FK = 81
    AND   IMPORT_JOB_FK = @IN_JOB_FK 


	EXEC synchronizer.EVENT_POSTING_UNDO @v_batch_no


----------------------------------------------------------------------------------------------------
--  Next Remove all the Events but leave the Event History
---------------------------------------------------------------------------------------------------


--------------   EVENTS
DELETE 
FROM synchronizer.event_data_errors
where EVENT_FK IN (select event_id from synchronizer.event_data WITH (NOLOCK)
where import_job_fk = @in_job_fk)


DELETE 
FROM synchronizer.event_data_rules
where EVENT_FK IN (select event_id from synchronizer.event_data WITH (NOLOCK)
where import_job_fk = @in_job_fk)


DELETE
FROM synchronizer.EVENT_DATA 
WHERE IMPORT_JOB_FK = @in_job_fk


 
----------------------------------------------------------------------------------------------------
--  Next Remove all the Stage Data
---------------------------------------------------------------------------------------------------


DELETE 
FROM cleanser.SEARCH_JOB
WHERE SEARCH_JOB_ID = @in_job_fk

DELETE 
FROM cleanser.SEARCH_JOB_DATA_NAME
WHERE SEARCH_JOB_FK = @in_job_fk

DELETE
FROM cleanser.SEARCH_JOB_FROM_TO_MATCH
WHERE SEARCH_JOB_FK = @in_job_fk

DELETE
FROM cleanser.SEARCH_JOB_RESULTS_BEST
WHERE SEARCH_JOB_FK = @in_job_fk


DELETE
FROM cleanser.SEARCH_JOB_RESULTS_IDENT
WHERE SEARCH_JOB_FK = @in_job_fk


DELETE
FROM cleanser.SEARCH_JOB_RESULTS_TASK
WHERE SEARCH_JOB_FK = @in_job_fk


DELETE
FROM cleanser.SEARCH_JOB_RESULTS_TASK_WORK
WHERE SEARCH_JOB_FK = @in_job_fk


DELETE
FROM cleanser.SEARCH_JOB_STRING
WHERE SEARCH_JOB_FK = @in_job_fk


DELETE
FROM cleanser.SEARCH_JOB_STRING_WORK
WHERE SEARCH_JOB_FK = @in_job_fk


DELETE 
FROM stage.STG_RECORD_IDENT
WHERE JOB_FK = @in_job_fk

DELETE
FROM stage.STG_RECORD_ENTITY_IDENT
WHERE JOB_FK = @in_job_fk


DELETE
FROM stage.STG_RECORD_ENTITY
WHERE JOB_FK = @in_job_fk


DELETE
FROM stage.STG_RECORD_DATA
WHERE JOB_FK = @in_job_fk


DELETE
FROM stage.STG_RECORD_ERRORS
WHERE JOB_FK = @in_job_fk


DELETE
FROM stage.STG_RECORD_STRUCTURE 
WHERE STG_RECORD_FK IN 
  ( SELECT STG_RECORD_ID
	FROM stage.STG_RECORD WITH (NOLOCK)
	WHERE JOB_FK = @in_job_fk )


DELETE
FROM stage.STG_RECORD
WHERE JOB_FK = @in_job_fk


----------------------------------------------------------------------------------------------------
--  Next Remove all the Import Data
---------------------------------------------------------------------------------------------------

		DELETE
		FROM import.IMPORT_RECORD_DATA
		WHERE JOB_FK = @in_job_fk

		DELETE
		FROM import.IMPORT_RECORD_NAME_VALUE
		WHERE JOB_FK = @in_job_fk

		DELETE
		FROM import.IMPORT_RECORD
		WHERE JOB_FK = @in_job_fk


----------------------------------------------------------------------------------------------------
--  Last Step remove the JOB
---------------------------------------------------------------------------------------------------

		delete from common.job_errors 
		where job_fk = @in_job_fk

		delete from common.job_execution 
		where job_fk = @in_job_fk

		delete from common.job
		where job_id = @in_job_fk



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of epacube.UTIL_UNDO_IMPORT_JOB'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;


END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of epacube.UTIL_UNDO_IMPORT_JOB has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END



















