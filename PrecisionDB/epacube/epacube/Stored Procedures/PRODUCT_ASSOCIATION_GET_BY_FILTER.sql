﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get all product associations for a given product (URM-1187)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[PRODUCT_ASSOCIATION_GET_BY_FILTER]
	@product_id VARCHAR(128),
	@product_association_type_id VARCHAR(10) = '%',
	@entity_id VARCHAR(128) = '%'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.PRODUCT_ASSOCIATION_GET_BY_FILTER. Parameters: @product_id = ' + @product_id + ', @product_association_type_id = ' + @product_association_type_id + ', @entity_id = ' + @entity_id
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT PA.PRODUCT_ASSOCIATION_ID	AS ASSOCIATION_ID 
			, PADN.DATA_NAME_ID				AS ASSOCIATION_TYPE_ID
			, PADN.LABEL					AS ASSOCIATION
			, PA.ENTITY_STRUCTURE_FK		AS ENTITY_STRUCTURE_ID
			, EI.VALUE						AS [ENTITY_ID]
			, EIN.VALUE						AS ENTITY
			, PA.ORG_ENTITY_STRUCTURE_FK	AS ORGANIZATION_ID
			, OEI.VALUE						AS ORGANIZATION
		FROM epacube.PRODUCT_ASSOCIATION AS PA
			INNER JOIN (
				SELECT P.PRODUCT_STRUCTURE_FK, P.VALUE
				FROM epacube.PRODUCT_IDENTIFICATION P
					INNER JOIN epacube.DATA_NAME DN
						ON P.DATA_NAME_FK = DN.DATA_NAME_ID
							AND DN.NAME = 'PRODUCT_ID') PID
				ON PA.PRODUCT_STRUCTURE_FK = PID.PRODUCT_STRUCTURE_FK 
			INNER  JOIN epacube.DATA_NAME AS PADN 
				ON PA.DATA_NAME_FK = PADN.DATA_NAME_ID
			LEFT JOIN epacube.ENTITY_IDENTIFICATION AS EI 
				ON PA.ENTITY_STRUCTURE_FK = EI.ENTITY_STRUCTURE_FK
			LEFT JOIN epacube.ENTITY_IDENT_NONUNIQUE AS EIN 
				ON PA.ENTITY_STRUCTURE_FK = EIN.ENTITY_STRUCTURE_FK
			LEFT JOIN epacube.ENTITY_IDENTIFICATION AS OEI 
				ON PA.ORG_ENTITY_STRUCTURE_FK = OEI.ENTITY_STRUCTURE_FK
		WHERE PID.VALUE = @product_id
			AND PA.DATA_NAME_FK LIKE @product_association_type_id
			AND EI.VALUE LIKE @entity_id
		FOR JSON PATH, ROOT('data');
		
		SET @ls_stmt = 'Finished execution of epacube.PRODUCT_ASSOCIATION_GET_BY_FILTER.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH
		SET @ls_stmt = 'Execution of epacube.PRODUCT_ASSOCIATION_GET_BY_FILTER has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END 

