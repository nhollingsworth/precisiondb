﻿







-- Copyright 2006 epaCUBE, Inc.
--
-- Procedure created by Walt Tucholski
--
--
-- 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- WT        02/13/2008  EPA-1540. Initial SQL Version
-- CV        10/30/2008  Added call to epacube.nightly purge
-- RG		 05/28/2009	 SUP-308
-- RG		 08/21/2009	 V5-modification
-- RG		 03/30/2010  Incorporation of Event History + Event Archive
-- RG		 04/13/2010  EPA-2898
-- CV        04/28/2010  EPA-2942 - leave common Jobs out there
-- CV        05/25/2010  Switched order of which purge procedures are called.  import last
-- KS        06/02/2010  EPA-3011 Removing the the notion of "Completed Jobs" and using instead the criteria below:
--							EVENT DATA Event Effective Date <= @purge_date_max ( getdate() - 'PURGE IMPORT JOBS AFTER' )
--							    and Event Status is completed ( ie Approved, Pre_Posted, or Rejected ) 
--							STAGE DATA Cleansed Indicator = 1 AND Import Date <= @purge_date_max ( getdate() - 'PURGE IMPORT JOBS AFTER' )
--							IMPORT DATA Any row in the Import Tables in which a Stage Record does not exist. 
--                       Also changed @grace_days to variable instead of parameter and set default to 7 days
--						 Also added a new procedure parameter @purge_immediate_ind and default to 0 ( or OFF )
--                       If @purge_immediate_ind = 1 then @purge_date_max = getdate() 
--                       Last enhancement is to remove all Jobs that are older than archive period.
-- RG		 01/14/2011  EPA-3136 Adding call to purge export data older than grace period
-- CV        03/18/2011  Added call to new procedure purge event calc table
-- CV        01/28/2013  Added to not remove from the common.job table jobs under 1000 - paxson needs a header job for exports.
-- CV        07/30/2012  Do not remove MM jobs from Common Job table.  Needed in MAUI Jira -
-- CV		 06/04/2014  Added is null to job id select.  was causing us to leave jobs because event data has null for import job id

CREATE PROCEDURE [epacube].[PURGE_IMPORT_DATA] @purge_immediate_ind INT = 0 
AS
BEGIN

        DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @job_timestamp DATETIME

		DECLARE @grace_days INT         
        DECLARE @purge_date_max DATETIME        
        DECLARE	@epacube_Params_Grace_Period int
		DECLARE @calculated_Grace_Period int
		DECLARE @display_Grace_Period varchar(20)
		DECLARE @archivePeriod int
		DECLARE @archive_date DATETIME
		DECLARE @defaultArchivePeriod int
		DECLARE @appscope INT
		

        BEGIN TRY
            SET NOCOUNT ON ;
            SET @l_exec_no = 0
            SET @l_sysdate = GETDATE()
			SET @grace_days = 7
						
			
			SET @appscope = (select application_scope_id
							 from epacube.application_scope with (NOLOCK)
							 where scope = 'APPLICATION')
			SET @epacube_Params_Grace_Period = (select CAST ( eep.value as int )
												from epacube.epacube_params eep with (NOLOCK)
												where  eep.name = 'PURGE IMPORT JOBS AFTER'
												and application_scope_fk = @appscope)
			SET @calculated_Grace_Period = CASE @purge_immediate_ind
			                               WHEN 1 THEN 0
			                               ELSE (isnull(@epacube_Params_Grace_Period,@grace_days))
			                               END 						
			SET @purge_date_max = DATEADD(day, -@calculated_Grace_Period, @l_sysdate)
			SET @display_Grace_Period = CAST(ISNULL(@calculated_Grace_Period, 0) AS VARCHAR(20))						
			SET @defaultArchivePeriod = 30  --- KS changed from 90 to thirty
			SET @archivePeriod = CAST(isnull((select CAST ( value as int ) from epacube.epacube_params with (NOLOCK)
										  where NAME = 'ARCHIVE_PERIOD'
										  and application_scope_fk = @appscope),@defaultArchivePeriod) AS VARCHAR(20))
			SET @archive_date = DATEADD(day, -@archivePeriod, @l_sysdate)										  

							
            SET @ls_stmt = 'started execution of epacube.PURGE_IMPORT_DATA. '
                + 'Grace days = '
                + @display_Grace_Period + ' days.'
            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;
                
                
			
-- call subroutine to purge event tables
			EXEC EPACUBE.PURGE_EVENT_TABLES	@purge_immediate_ind
			
-- call subroutine to purge event tables
			EXEC EPACUBE.PURGE_EVENT_CALC_TABLES	@purge_immediate_ind

-- call subroutine to purge stage/cleanser tables
			EXEC EPACUBE.PURGE_STAGE_TABLES @purge_immediate_ind          

-- call subroutine to purge data from import tables
			EXEC EPACUBE.PURGE_IMPORT_TABLES @purge_immediate_ind
			
-- call subroutine to purge data from export 200 table
			EXEC EPACUBE.PURGE_EXPORT_DATA 


--------------------------------------------------------------------------------------------
--  CREATE TEMP TABLE FOR JOBS TO BE REMOVED
--		REMOVE JOBS THAT JOB_DATE > ARCHIVE_PERIOD ( IF ARCHIVE_PERIOD IS NULL USE 30 DAYS )
--------------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_REMOVE_JOBS') is not null
	   drop table #TS_REMOVE_JOBS;

	   
	-- create temp table
	CREATE TABLE #TS_REMOVE_JOBS(
				JOB_FK BIGINT NULL
				)

	INSERT INTO #TS_REMOVE_JOBS
	( JOB_FK )
	SELECT J.JOB_ID
	FROM common.JOB J WITH (NOLOCK)	
	WHERE utilities.TRUNC(J.JOB_CREATE_TIMESTAMP) 
	                <=
		  utilities.TRUNC(@archive_date)	
    And j.job_class_FK not in (460, 480, 481,630)					
    AND J.JOB_ID NOT IN (
				SELECT DISTINCT ISNULL(JOB_FK,0)
				FROM stage.STG_RECORD
				UNION ALL
				SELECT DISTINCT ISNULL(IMPORT_JOB_FK,0)
				FROM synchronizer.EVENT_DATA 
				)

	CREATE CLUSTERED INDEX IDX_TSRJ ON #TS_REMOVE_JOBS
	(JOB_FK ASC )
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



--cleanser.SEARCH_JOB_DATA_NAME
					DELETE  FROM cleanser.SEARCH_JOB_DATA_NAME
                    WHERE   SEARCH_JOB_FK IN (
                               SELECT JOB_FK
                               FROM #TS_REMOVE_JOBS )

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from cleanser.SEARCH_JOB_DATA_NAME table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


--cleanser.SEARCH_JOB
					DELETE  FROM cleanser.SEARCH_JOB
                    WHERE   SEARCH_JOB_ID IN (
                               SELECT JOB_FK
                               FROM #TS_REMOVE_JOBS )

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from cleanser.SEARCH_JOB table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


--common.JOB_ERRORS
					DELETE  FROM common.JOB_ERRORS
                    WHERE   JOB_FK IN (
                               SELECT JOB_FK
                               FROM #TS_REMOVE_JOBS )

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from common.JOB_ERRORS table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


--common.JOB_EXECUTION
					DELETE  FROM common.JOB_EXECUTION
                    WHERE   JOB_FK IN (
                               SELECT JOB_FK
                               FROM #TS_REMOVE_JOBS )

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from common.JOB_EXECUTION table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


--common.JOB
					DELETE  FROM common.JOB
                    WHERE   JOB_ID IN (
                               SELECT JOB_FK
                               FROM #TS_REMOVE_JOBS )
                     AND JOB_ID > 1000          

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from common.JOB table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


--drop temp table if it exists
	IF object_id('tempdb..#TS_REMOVE_JOBS') is not null
	   drop table #TS_REMOVE_JOBS;

	
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

            SET @status_desc = 'finished execution of epacube.PURGE_IMPORT_DATA'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;

        END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.PURGE_IMPORT_DATA has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH
		  
    END








