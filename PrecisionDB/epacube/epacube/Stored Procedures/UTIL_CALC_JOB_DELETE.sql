﻿





CREATE PROCEDURE [epacube].[UTIL_CALC_JOB_DELETE] 
        @IN_JOB_FK BIGINT
    AS
BEGIN     



-------------------------------------------------------------------------------------
--  DELETE PREVIOUS JOB ROWS
-------------------------------------------------------------------------------------


	----------------  EVENT DATA
	DELETE 
	FROM [synchronizer].[EVENT_DATA_RULES]
	WHERE EVENT_FK IN (
			SELECT EVENT_ID
			FROM [synchronizer].[EVENT_DATA]  WITH (NOLOCK)
			WHERE IMPORT_JOB_FK = @in_job_fk   )


	DELETE 
	FROM [synchronizer].[EVENT_DATA_ERRORS]
	WHERE EVENT_FK IN (
			SELECT EVENT_ID
			FROM [synchronizer].[EVENT_DATA]  WITH (NOLOCK)
			WHERE IMPORT_JOB_FK = @in_job_fk   )

	DELETE 
	FROM [synchronizer].[EVENT_DATA]
	WHERE IMPORT_JOB_FK = @in_job_fk 

	DELETE 
	FROM [synchronizer].[EVENT_DATA_HISTORY]
	WHERE IMPORT_JOB_FK = @in_job_fk 

   

    ---------------- EVENT CALC
	DELETE 
	FROM [synchronizer].[EVENT_CALC_RULES_BASIS]
	WHERE EVENT_RULES_FK IN (
			SELECT EVENT_RULES_ID
			FROM [synchronizer].[EVENT_CALC_RULES] ECR WITH (NOLOCK)
			INNER JOIN [synchronizer].[EVENT_CALC] EC  WITH (NOLOCK)
			 ON ( ECR.EVENT_FK = EC.EVENT_ID )
			WHERE JOB_FK = @in_job_fk   )


	DELETE 
	FROM [synchronizer].[EVENT_CALC_RULES]
	WHERE EVENT_FK IN (
			SELECT EVENT_ID
			FROM [synchronizer].[EVENT_CALC]  WITH (NOLOCK)
			WHERE JOB_FK = @in_job_fk   )


	DELETE 
	FROM [synchronizer].[EVENT_CALC_ERRORS]
	WHERE EVENT_FK IN (
			SELECT EVENT_ID
			FROM [synchronizer].[EVENT_CALC]  WITH (NOLOCK)
			WHERE JOB_FK = @in_job_fk   )

	    
	DELETE 
	FROM [synchronizer].[EVENT_CALC]
	WHERE JOB_FK = @in_job_fk 


------------- JOB DATA

		delete from common.job_execution 
		where job_fk = @in_job_fk

		delete from common.job
		where job_id = @in_job_fk



END













