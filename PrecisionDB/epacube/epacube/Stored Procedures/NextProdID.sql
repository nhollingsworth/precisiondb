﻿
-- =============================================
-- Author:		<Gary Stone>
-- Create date: <December 3, 2007>
-- Description:	<Starting with the specified Product ID, return the first sequential void, or 0 if none exists.>
-- =============================================
CREATE PROCEDURE [epacube].[NextProdID]
	-- Add the parameters for the stored procedure here
	(@Prod_ID   int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

Select top 1 prods.NxtID from 
(
SELECT 
(Select min(Cast(value as BigInt)) value from epacube.product_identification where isnumeric(value) = 1 and data_name_fk = 100 and cast(value as bigint) >= @Prod_ID)
+
(Select count(*) from epacube.product_identification where isnumeric(value) = 1 and Cast(value as BigInt) < 
	Cast(ID_Seq.value as BigInt) and Cast(value as Bigint) > @Prod_ID and data_name_fk = 100 ) as NxtID
FROM epacube.product_identification AS ID_Seq where isnumeric(value) = 1 and Cast(value as Bigint) > @Prod_ID and data_name_fk = 100
union all select 0
) prods
where prods.NxtID not in (select Cast(value as BigInt) from epacube.product_identification where data_name_fk = 100 and isnumeric(value) = 1 and 
Cast(value as bigint) >= @Prod_ID
)

END
