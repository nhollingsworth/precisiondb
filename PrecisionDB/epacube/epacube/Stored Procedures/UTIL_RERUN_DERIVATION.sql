﻿






CREATE PROCEDURE [epacube].[UTIL_RERUN_DERIVATION] 
        @in_import_job_fk bigint,
        @in_data_name_fk bigint
    AS
    BEGIN
 
      DECLARE 
        @l_sysdate datetime,
        @ls_stmt varchar(1000),
        @l_exec_no bigint,
        @l_rows_processed bigint,
        @v_count  int,
        @v_batch_no  int


		DECLARE @status_desc				varchar (max)
		DECLARE @ErrorMessage				nvarchar(4000)
		DECLARE @ErrorSeverity				int
		DECLARE @ErrorState					int


  BEGIN TRY

	SET NOCOUNT ON
    SET @l_sysdate = getdate()
    SET @l_exec_no = 0;
    SET @ls_stmt = 'started execution of epacube.UTIL_RERUN_DERIVATION '
				 + ' JOB_FK: ' + ( CAST ( @in_import_job_fk AS VARCHAR(16)) ) 
				 + ' DATA_NAME_FK: ' + ( CAST ( @in_data_name_fk  AS VARCHAR(16)) ) 
    EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt
											  ,@exec_id = @l_exec_no OUTPUT;



------------------------------------------------------------------------------------------
--  Set Stage Product Structure Batch
------------------------------------------------------------------------------------------
    SET @v_batch_no = 0;
	EXEC seq_manager.db_get_next_sequence_value_impl 'synchronizer','batch_seq', @v_batch_no OUTPUT;

    UPDATE stage.stg_product_structure
    SET batch_no = @v_batch_no
    WHERE stg_product_structure_id in (
           select sps.stg_product_structure_id
           from stage.stg_product_structure sps
           inner join stage.stg_product_data spd on 
             ( spd.stg_product_structure_fk = sps.stg_product_structure_id )
           where sps.import_job_fk = @in_import_job_fk
           and   spd.data_name_fk = 704  -- PRICE GROUP 1
           )

--		  SELECT product_structure_fk
--		  FROM  epacube.synchronizer.event_product
--		  WHERE import_job_fk = @in_import_job_fk
--		  AND   data_name_fk = @in_data_name_fk  )

   SET @l_rows_processed = @@ROWCOUNT
   
   IF @l_rows_processed > 0
   BEGIN
		SET @ls_stmt = 'Batch No Set >> total rows assigned = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt
   END


 -- ------------------------------------------------------------------------------------
 --      Remove all Previous Created Derivations for stg products in batch
 -- -----------------------------------------------------------------------------------


	DELETE 
	FROM stage.STG_PRODUCT_DATA
	WHERE event_source_cr_fk = 72   -- DERIVED VALUES
    AND   data_name_fk = @in_data_name_fk
    AND   stg_product_structure_fk in (
			 select sps.stg_product_structure_id
			 from stage.stg_product_structure sps WITH (NOLOCK)
			 where batch_no = @v_batch_no )

   SET @l_rows_processed = @@ROWCOUNT
   
   IF @l_rows_processed > 0
   BEGIN
		SET @ls_stmt = 'stage.STG_PRODUCT_DATA >> total rows DELETED = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt
   END

-- ------------------------------------------------------------------------------------
--    Execute Derivation
-- -----------------------------------------------------------------------------------

	 EXEC STAGE.DERIVE_STG_PRODUCT_DATA_LOAD  @V_BATCH_NO, @IN_DATA_NAME_FK

	SELECT @v_COUNT = COUNT(1)
	FROM stage.STG_PRODUCT_DATA
	WHERE event_source_cr_fk = 72   -- DERIVED VALUES
    AND   data_name_fk = @in_data_name_fk
    AND   stg_product_structure_fk in (
			 select sps.stg_product_structure_id
			 from stage.stg_product_structure sps WITH (NOLOCK)
			 where batch_no = @v_batch_no )

   
	 SET @ls_stmt = 'STAGE.DERIVE_STG_PRODUCT_DATA_LOAD >> total rows DERIVED = '
			+ CAST(isnull(@v_COUNT,0) AS VARCHAR(30))
	 EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt


------------------------------------------------------------------------------------------
--  Remove Events
------------------------------------------------------------------------------------------


      DELETE 
      FROM synchronizer.event_product_errors
      WHERE event_fk in (
		  SELECT event_id
		  FROM  epacube.synchronizer.event_product
		  WHERE import_job_fk = @in_import_job_fk
		  AND   data_name_fk = @in_data_name_fk  )
      
	   SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @ls_stmt = 'synchronizer.event_product_errors >> total rows DELETED = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt
	   END

      DELETE 
      FROM synchronizer.event_product
      WHERE import_job_fk = @in_import_job_fk
      AND   data_name_fk = @in_data_name_fk

	   SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @ls_stmt = 'synchronizer.event_product >> total rows DELETED = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt
	   END

-------------------------------------------------------------------------------------
--   Create the Product Events
-------------------------------------------------------------------------------------


-- Product Category
      INSERT   INTO synchronizer.EVENT_PRODUCT 
               (data_name_fk
			   ,event_effective_date
			   ,product_structure_fk
			   ,new_data
			   ,current_data
			   ,last_update_timestamp
			   ,search1
			   ,search2
			   ,search3
			   ,search4
			   ,search5
			   ,search6
			   ,event_condition_cr_fk
			   ,event_status_cr_fk
			   ,event_priority_cr_fk
			   ,event_source_cr_fk
			   ,event_action_cr_fk
--			   ,approve_event			   
			   ,import_job_fk
			   ,import_date
               ,import_package_fk
	  	       ,stage_table_id_fk
			   ,batch_no
			   ,table_id_fk
			   ,data_value_fk
			   ,mfr_entity_structure_fk
               ,host_connector_fk
			   ,update_timestamp
			   ,update_user
               )
      SELECT spd.data_name_fk
			,spd.effective_date
	        ,SPS.product_structure_fk
			,spd.data_value
            ,CASE ds.data_type_cr_fk
             WHEN 134 THEN ( SELECT top 1 VALUE FROM epacube.data_value WHERE data_value_id = pc.data_value_fk )
             WHEN 135 THEN ( SELECT top 1 VALUE FROM epacube.entity_data_value WHERE entity_data_value_id = pc.entity_data_value_fk )
             ELSE null
             END
		    ,pc.update_timestamp
 		,isnull ( 
	 		 ( SELECT top 1 ei.VALUE FROM epacube.entity_identification ei, 
                  stage.stg_product_structure spsx, epacube.data_name dn
		   		  WHERE spsx.stg_product_structure_id = sps.stg_product_structure_id
				  AND   spsx.mfr_entity_structure_fk = ei.entity_structure_fk
				  AND   ei.data_name_fk = dn.data_name_id
                  AND   spsx.batch_no = @v_batch_no
				  AND   dn.NAME = 'VENDOR NUMBER' ),
	 		 ( SELECT top 1 ei.VALUE FROM epacube.entity_identification ei, 
                  epacube.product_structure ps, epacube.data_name dn
		   		  WHERE ps.product_structure_id = sps.product_structure_fk
				  AND   ps.mfr_entity_structure_fk = ei.entity_structure_fk
				  AND   ei.data_name_fk = dn.data_name_id
				  AND   dn.NAME = 'VENDOR NUMBER' )
		     )
		,isnull ( 
	 		 ( SELECT top 1 ei.VALUE FROM epacube.entity_identification ei, 
                  stage.stg_product_structure spsx, epacube.data_name dn
		   		  WHERE spsx.stg_product_structure_id = sps.stg_product_structure_id
				  AND   spsx.mfr_entity_structure_fk = ei.entity_structure_fk
				  AND   ei.data_name_fk = dn.data_name_id
                  AND   spsx.batch_no = @v_batch_no
				  AND   dn.NAME = 'MFR PREFIX' ),
	 		 ( SELECT top 1 ei.VALUE FROM epacube.entity_identification ei, 
                  epacube.product_structure ps, epacube.data_name dn
		   		  WHERE ps.product_structure_id = sps.product_structure_fk
				  AND   ps.mfr_entity_structure_fk = ei.entity_structure_fk
				  AND   ei.data_name_fk = dn.data_name_id
				  AND   dn.NAME = 'MFR PREFIX' )
			)
	 		,isnull (
			 ( SELECT top 1 spd.data_value FROM stage.stg_product_data spd
		   		  WHERE spd.stg_product_structure_fk = sps.stg_product_structure_id
				  AND   spd.data_name = 'SX PRODUCT ID'  ),
			 ( SELECT top 1 pi.VALUE FROM epacube.product_identification pi, epacube.data_name dn
		   		  WHERE pi.product_structure_fk = sps.product_structure_fk
				  AND   pi.data_name_fk = dn.data_name_id
				  AND   dn.NAME = 'SX PRODUCT ID'  )
			)
	 		,isnull (
			 ( SELECT top 1 spd.data_value FROM stage.stg_product_data spd
		   		  WHERE spd.stg_product_structure_fk = sps.stg_product_structure_id
				  AND   spd.data_name = 'MFR CATALOG NUMBER' ),
			 ( SELECT top 1 pi.VALUE FROM epacube.product_identification pi, epacube.data_name dn
		   		  WHERE pi.product_structure_fk = sps.product_structure_fk
				  AND   pi.data_name_fk = dn.data_name_id
				  AND   dn.NAME = 'MFR CATALOG NUMBER'  )
			)
	 		,isnull (
			 ( SELECT top 1 spd.data_value FROM stage.stg_product_data spd
		   		  WHERE spd.stg_product_structure_fk = sps.stg_product_structure_id
				  AND   spd.data_name = 'UPC' ),
			 ( SELECT top 1 pi.VALUE FROM epacube.product_identification pi, epacube.data_name dn
		   		  WHERE pi.product_structure_fk = sps.product_structure_fk
				  AND   pi.data_name_fk = dn.data_name_id
				  AND   dn.NAME = 'UPC'  )
			)
	 		,isnull (
			 ( SELECT top 1 spd.data_value FROM stage.stg_product_data spd
		   		  WHERE spd.stg_product_structure_fk = sps.stg_product_structure_id
				  AND   spd.data_name = 'SX DESCRIPTION LINE 1' ),
			 ( SELECT top 1 pd.description FROM epacube.product_description pd, epacube.data_name dn
		   		  WHERE pd.product_structure_fk = sps.product_structure_fk
				  AND   pd.data_name_fk = dn.data_name_id
				  AND   dn.NAME = 'SX DESCRIPTION LINE 1' )
			)
			,97  --green
			,CASE isnull ( ep.event_id, 0 )
			 WHEN 0 THEN 80  --pending
			 ELSE 83  --pre_pending
			 END
            ,61  --standard
			,spd.event_source_cr_fk
			,CASE isnull ( sps.event_action_cr_fk, 0 )
			 WHEN 54 THEN 54
			 ELSE ( CASE isnull ( pc.product_category_id, 0 )
			        WHEN 0 THEN 51
				    ELSE 52
					END )
			 END
--			,NULL  -- APPROVE_EVENT	
			,sps.import_job_fk
			,NULL  --sps.import_date
            ,sps.import_package_fk
            ,spd.stg_product_data_id
			,sps.batch_no
			,pc.product_category_id
            , CASE ds.data_type_cr_fk
              WHEN 134 THEN
                          ( SELECT top 1 dv.data_value_id FROM epacube.DATA_VALUE dv
                            WHERE dv.data_name_fk = dn.data_name_id 
                            AND   dv.value = spd.data_value )
              WHEN 135 THEN
                          ( SELECT top 1 edv.entity_data_value_id FROM epacube.ENTITY_DATA_VALUE edv
                            WHERE edv.data_name_fk = dn.data_name_id 
                            AND   edv.value = spd.data_value
                            AND   edv.entity_structure_fk = sps.mfr_entity_structure_fk )
			  ELSE NULL
              END   -- data value fk
            ,sps.MFR_ENTITY_STRUCTURE_FK  -- FOR ALL EVENTS sps.MFR_ENTITY_STRUCTURE_FK
            ,sps.host_connector_fk
			,@l_sysdate
			,sps.update_user
	      FROM  stage.stg_product_data spd
		  INNER JOIN epacube.data_name dn ON ( dn.data_name_id = spd.data_name_fk )
		  INNER JOIN epacube.data_set ds ON ( ds.data_set_id = dn.data_set_fk  )
		  INNER JOIN stage.stg_product_structure sps ON ( sps.stg_product_structure_id = spd.stg_product_structure_fk )
		  LEFT JOIN epacube.product_category pc ON ( pc.product_structure_fk = sps.product_structure_fk 
		                                           AND  spd.data_name_fk = pc.data_name_fk )
          LEFT JOIN synchronizer.EVENT_PRODUCT ep ON ( ep.product_structure_fk = sps.product_structure_fk
		                                          AND  ep.data_name_fk = spd.data_name_fk
--												  AND  ep.event_status_cr_fk = 80 )
												  AND  ep.event_status_cr_fk in ( 80, 83, 84, 85 ) )
		  WHERE sps.batch_no = @v_batch_no
		  AND   ds.table_name = 'PRODUCT_CATEGORY'
          AND   spd.data_value is not null
          AND   spd.data_value <> ''
		  AND   (    
                  (  ds.data_type_cr_fk = 134 
                     AND ( isnull ( spd.data_value, 'NULL' ) <> 
                           isnull ( ( SELECT dv.VALUE FROM epacube.data_value dv WHERE dv.data_value_id = pc.data_value_fk ), 'NULL' ) )
                  )OR
                  (  ds.data_type_cr_fk = 135 
                     AND ( isnull ( spd.data_value, 'NULL' ) <> 
                           isnull ( ( SELECT edv.VALUE FROM epacube.entity_data_value edv WHERE edv.entity_data_value_id = pc.entity_data_value_fk ), 'NULL' ) )
                  ) 
                )                
		  AND   isnull ( spd.data_value, 'NULL' ) <> isnull ( ep.new_data, 'NULL')  	  
		  ;

	   SET @l_rows_processed = @@ROWCOUNT
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'PRODUCT_CATEGORY Events Created '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END



------------------------------------------------------------------------------------------
--   LOG COMPLETION
------------------------------------------------------------------------------------------

	SELECT @v_COUNT = COUNT(1)
	  FROM  epacube.synchronizer.event_product
	  WHERE import_job_fk = @in_import_job_fk
	  AND   data_name_fk = @in_data_name_fk  

	 SET @ls_stmt = 'COMPLETE >> total Events Created = '
			+ CAST(isnull(@v_COUNT,0) AS VARCHAR(30))
	 EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of epacube.UTIL_RERUN_DERIVATION '
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


END TRY
BEGIN CATCH   


		SELECT 
			@ErrorMessage = 'Execution of epacube.UTIL_RERUN_DERIVATION  has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END







































