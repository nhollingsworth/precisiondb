﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create product attribute JIRA (URM-1201)
-- Evaluate the data type and insert the record into the PRODUCT_ATTRIBUTES table
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[PRODUCT_ATTRIBUTE_CREATE] 
	@product_structure_id BIGINT
	,@data_name_id INT
	,@data_type VARCHAR(32)
	,@data_value_id BIGINT = NULL
	,@value VARCHAR(256) = NULL
	,@user VARCHAR(64)
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @att_char VARCHAR(256);
	DECLARE @att_num NUMERIC(18,6);
	DECLARE @att_date DATETIME;
	DECLARE @att_yn CHAR(1);
	DECLARE @att_evt VARCHAR(256);

	SET NOCOUNT ON;

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.PRODUCT_ATTRIBUTE_CREATE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	IF @data_value_id IS NULL AND @value IS NULL
		RAISERROR('Parameters @data_value_id and @value cannot both be NULL. Please provide a value for one of the parameters.', 16, 1);

	IF @data_type = 'CHARACTER' SET @att_char = @value;
	IF @data_type = 'NUMBER' SET @att_num = CAST(@value AS NUMERIC(18,6));
	IF @data_type = 'DATE' SET @att_date = CAST(@value AS DATETIME);
	IF @data_type = 'Y/N' SET @att_yn = CAST(@value AS CHAR(1));
	IF @data_type = 'DATA VALUE' SELECT @att_evt = VALUE FROM epacube.DATA_VALUE WHERE DATA_VALUE_ID = @data_value_id

    BEGIN TRY
		BEGIN TRANSACTION;
			
			INSERT INTO epacube.PRODUCT_ATTRIBUTE (PRODUCT_STRUCTURE_FK, DATA_NAME_FK, ATTRIBUTE_CHAR, ATTRIBUTE_NUMBER, ATTRIBUTE_DATE, ATTRIBUTE_Y_N, DATA_VALUE_FK, ATTRIBUTE_EVENT_DATA, CREATE_USER)
			VALUES(@product_structure_id, @data_name_id, @att_char, @att_num, @att_date, @att_yn, @data_value_id, @att_evt, @user);

			SELECT SCOPE_IDENTITY() AS NEW_PRODUCT_ATTRIBUTE_ID;

		COMMIT TRANSACTION;

		SET @ls_stmt = 'Finished execution of epacube.PRODUCT_ATTRIBUTE_CREATE.'; 
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 
						
		SET @ls_stmt = 'Execution of epacube.PRODUCT_ATTRIBUTE_CREATE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

	END CATCH
END
