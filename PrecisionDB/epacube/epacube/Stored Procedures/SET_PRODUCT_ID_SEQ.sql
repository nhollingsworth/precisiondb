﻿




  CREATE PROCEDURE [epacube].[SET_PRODUCT_ID_SEQ] 
        @in_seq bigint
    AS
     
    BEGIN
 DECLARE 
    @l_sysdate datetime,
    @ls_stmt varchar(1000),
    @l_exec_no bigint,
	@status_desc		 varchar (max)

SET @l_sysdate = getdate()
SET NOCOUNT ON;
SET @l_exec_no = 0;
--
DELETE FROM seq_manager.db_sequence_currvals
where schema_name = 'common'
and sequence_name = 'product_id_seq'

UPDATE seq_manager.db_sequence_list
SET  initial_value = @in_seq - 1
	,current_value = @in_seq - 1
where schema_name = 'common'
and sequence_name = 'product_id_seq'

END




