﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Delete taxonomy products JIRA (URM-1209)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[TAXONOMY_PRODUCT_DELETE]
	@taxonomy_product VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @taxonomy_node_id INT;

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.TAXONOMY_PRODUCT_DELETE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	IF OBJECT_ID('tempdb.dbo.#Taxonomy_Product_Delete', 'U') IS NOT NULL
		DROP TABLE #Taxonomy_Product_Delete;
	
	SELECT *
	INTO #Taxonomy_Product_Delete
	FROM OPENJSON(@taxonomy_product)
	WITH (
		TAXONOMY_NODE_ID INT '$.TAXONOMY_NODE_ID'
	) tp
	CROSS APPLY OPENJSON(@taxonomy_product, '$.data')
		WITH (
			PRODUCT_STRUCTURE_ID BIGINT '$.PRODUCT_STRUCTURE_ID'
		);

	SELECT @taxonomy_node_id = TAXONOMY_NODE_ID FROM #Taxonomy_Product_Delete;

	BEGIN TRY

		BEGIN TRANSACTION; 

			DELETE
			FROM epacube.TAXONOMY_PRODUCT
			WHERE TAXONOMY_PRODUCT_ID IN ( 
				SELECT TAXONOMY_PRODUCT_ID
				FROM epacube.TAXONOMY_PRODUCT TP
					INNER JOIN #Taxonomy_Product_Delete TPD
						ON TP.TAXONOMY_NODE_FK = TPD.TAXONOMY_NODE_ID
							AND TP.PRODUCT_STRUCTURE_FK = TPD.PRODUCT_STRUCTURE_ID);

		COMMIT TRANSACTION;

		SELECT TN.TAXONOMY_NODE_ID
			, DV.VALUE NODE_NAME
			, PARENT_TAXONOMY_NODE_FK
			, (
				SELECT COUNT(*)
				FROM epacube.TAXONOMY_PRODUCT
				WHERE TAXONOMY_NODE_FK = TN.TAXONOMY_NODE_ID) PRODUCT_COUNT
			, epacube.getTaxonomyNodeSubProductCount(TN.TAXONOMY_NODE_ID) SUB_PRODUCT_COUNT
			, JSON_QUERY(epacube.getTaxonomy(TAXONOMY_NODE_ID)) CHILDREN
		FROM epacube.TAXONOMY_NODE TN
			INNER JOIN epacube.DATA_VALUE DV
				ON TN.DATA_VALUE_FK = DV.DATA_VALUE_ID
		WHERE TN.TAXONOMY_NODE_ID = @taxonomy_node_id
		FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		
		SET @ls_stmt = 'Finished execution of epacube.TAXONOMY_PRODUCT_DELETE.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 

		SET @ls_stmt = 'Execution of epacube.TAXONOMY_PRODUCT_DELETE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END
