﻿



-- Copyright 2012 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Synchronize Product Standard Data for the Paxon Project
--          This will maintain the epaCUBE side Synchronization Table
--           that will be utilized to maintain the SRP [dbo].[StandardProduct]
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- KS        12/19/2012   Initial Version 
--

/*

Notes for Discussion::

Logic for Insert/Update/Delete
Indexes
1.	Primary Key Clustered
a.	ProductID, ASC
2.	Nonclustered Index
a.	MfrCode, ASC
b.	CatalogNo, ASC
Added Index on PDMProductID to facilite performance

Logic for Insert/Update/Delete
Unique business key =  PDMProductID or UPC
If business key exists in incoming data set and NOT in dbo.StandardProduct table, perform an insert:
•	Insert a record by setting the following columns
o	PDMProductID 
o	MfrCode, CatalogNo, Description, ListPrice, Col1Price, Col2Price, Col3Price, DistPrice, UOM, UPC, CommCode, MfrUCCNum, ItemNum, Zone, UNSPSC, PSCashDiscount, LegacyUpdateStatus, CountryOfOrigin, PercentMfdgInUSA
o	MfrPGC – if multiple group codes exists, value in field = ‘*’.  If product only has one Mfr. Group Code, set column = Mfr. Group Code 1
o	IsActive – set to 1
o	LastModifiedBy – set to user running the stored proc. Need to provide SQL job with an epaCUBE related username to run under.  
o	LastModifiedTime – set to current date/time UTC
o	CreatedTime –set to current date/time UTC
If business key does not exist in incoming data set and exists in dbo.StandardProduct table, perform a delete:
•	Update the record by setting the following columns
o	IsActive – set to 0
o	LastModifiedBy – set to user running the stored proc. Need to provide SQL job with an epaCUBE related username to run under.  
o	LastModifiedTime – set to current date/time UTC
If business key exists in incoming data set and exists in dbo.StandardProduct table, perform an update:
•	Compare the values in the following columns to the values in the incoming data set.  
o	MfrCode, CatalogNo, Description, ListPrice, Col1Price, Col2Price, Col3Price, DistPrice, UOM, UPC, CommCode, MfrUCCNum, ItemNum, Zone, UNSPSC, PSCashDiscount, CountryOfOrigin, PercentMfdgInUSA
•	MfrPGC – if value in incoming data set = ‘*’ and column = ‘*’ then leave column as is.  If value in incoming data set is not ‘*’ and value in column is null or different, set column to value in incoming data 
•	LegacyUpdateStatus – if value in incoming data set =’Q’, then set IsActive to 0
	If value is null for a column in the incoming data set and value is NOT null for the column in the table, DO NOT update column in table
	If value is not null for a column in the incoming data set and value for the column in the table is either null or different from incoming value, set the values in the table equal to the values in the incoming data set for the columns above
o	IsActive – set to 1 if currently 0 (do not set if legacy_update_status = Q)
o	LastModifiedBy – set to user running the stored proc.  Need to provide SQL job with an epaCUBE related username to run under.  
o	LastModifiedTime – set to current date/time UTC
Note: Given the short delivery time, the data in the view will be all products in epaCUBE as opposed to a more efficient solution of only pulling in products that have updated values since the last update was exported. 
Not in Scope
•	Deletion of rows
•	Logging of errors 


*/

CREATE PROCEDURE [epacube].[Synchronize_SRP_StandardProduct]
    @in_job_fk bigint, @in_effective_date datetime, @in_initialize_ind tinyint

--  
AS BEGIN 

     DECLARE @v_count_rows            bigint;               
	 DECLARE @v_LastModifiedTime      datetime;
	 DECLARE @v_last_effective_date   datetime;

SET NOCOUNT ON;

BEGIN TRY


------------------------------------------------------------------------------------------
--   CREATE  #TS_PDMProductID for Changes Only 
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_PDMProductStructure') is not null
	   drop table #TS_PDMProductStructure;
	  
	   
	-- create temp table
	CREATE TABLE #TS_PDMProductStructure (
		PDMProductStructureFK  BIGINT NULL
		 )

--------------------------------------------------------------------------------------------
--  CREATE INDEXES
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TS_PDMProductStructure ON #TS_PDMProductStructure 
	(PDMProductStructureFK ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



/*

-------------------------------------------------------------------------------------
--   Update Product Standard Rows
-------------------------------------------------------------------------------------

TRUNCATE TABLE #TS_PDMProductStructure
                
INSERT INTO #TS_PDMProductStructure(
	PDMProductStructureFK )    	    	    
------
(select product_structure_id 
from epacube.epacube.product_structure with (nolock)
where update_timestamp > @in_last_effective_date)

UNION

(select product_structure_fk 
from epacube.epacube.product_description with (nolock)
where update_timestamp > @in_last_effective_date)

UNION

(select product_structure_fk 
from epacube.epacube.product_identification with (nolock)
where update_timestamp > @in_last_effective_date)

UNION

(select product_structure_fk 
from epacube.epacube.product_ident_nonunique with (nolock)
where update_timestamp > @in_last_effective_date)

UNION

(select product_structure_fk 
from epacube.epacube.product_values with (nolock)
where update_timestamp > @in_last_effective_date)

UNION

(select product_structure_fk 
from epacube.epacube.product_UOM_CLASS with (nolock)
where update_timestamp > @in_last_effective_date)

UNION

(select product_structure_fk 
from epacube.epacube.product_ident_nonunique with (nolock)
where update_timestamp > @in_last_effective_date)


UNION

(select product_structure_fk 
from epacube.epacube.product_category pc with (nolock)
where pc.update_timestamp > @in_last_effective_date)


UNION

(select product_structure_fk 
from epacube.epacube.product_attribute with (nolock)
where update_timestamp > @in_last_effective_date)

UNION

(select product_structure_fk 
from epacube.epacube.product_association with (nolock)
where update_timestamp > @in_last_effective_date)

-- end of insert



UPDATE epacube.epacube.product_structure 
SET update_timestamp = @v_LastModifiedTime
WHERE 1=1
and   product_status_cr_fk = 100   ---- Active
and   product_structure_id in (
SELECT 
	PDMProductStructureFK
FROM #TS_PDMProductStructure  )


SET @v_count_rows = ( select count(1)
                 from #TS_PDMProductStructure  )


-------------------------------------------------------------------------------------
--   End of Procedure
-------------------------------------------------------------------------------------


------drop temp table if it exists
	IF object_id('tempdb..#TS_PDMProductStructure') is not null
	   drop table #TS_PDMProductStructure;


------------------------------------------------------------------------------------------
--   CREATE  #TS_StdProduct
------------------------------------------------------------------------------------------


----  drop temp table if it exists
	IF object_id('tempdb..#TS_StdProduct') is not null
	   drop table #TS_StdProduct;
	   
	   
	-- create temp table
	CREATE TABLE #TS_StdProduct (
	    [TS_StdProductID]  bigint IDENTITY(1000,1) NOT NULL,
		[PDMProductID] [bigint] NOT NULL,
		[MfrCode] [varchar](5) NOT NULL,
		[CatalogNo] [varchar](20) NOT NULL,
		[MfrPGC] [varchar](20) NULL,
		[Description] [varchar](50) NULL,
		[ListPrice] [decimal](12, 4) NULL,
		[Col1Price] [decimal](12, 4) NULL,
		[Col2Price] [decimal](12, 4) NULL,
		[Col3Price] [decimal](12, 4) NULL,
		[DistPrice] [decimal](12, 4) NULL,
		[UOM] [char](1) NULL,
		[UPC] [char](11) NULL,
		[CommCode] [varchar](4) NULL,
		[MfrUCCNum] [char](6) NULL,
		[ItemNum] [char](5) NULL,
		[Zone] [char](3) NOT NULL,
		[UNSPSC] [char](8) NULL,
		[PSCashDiscount] [decimal](3, 1) NULL,
		[LegacyUpdateStatus] [char](4) NULL,
		[CountryOfOrigin] [varchar](256) NULL,
		[PercentMfdgInUSA] [decimal](5, 2) NULL,
		[product_status_cr_fk]     int NULL,
		[SRP_ProductId]    [int] NULL
		 )


--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSQR_TSQRID ON #TS_StdProduct
	(TS_StdProductID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TS_StdProduct_1] ON #TS_StdProduct
	(PDMProductID ASC,
	 Zone ASC,
	 product_status_cr_fk ASC
	)
	INCLUDE ( TS_StdProductID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TS_StdProduct_2] ON #TS_StdProduct
	(SRP_ProductID ASC,
	 product_status_cr_fk ASC
	)
	INCLUDE ( TS_StdProductID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)





------------------------------------------------------------------------------------------
---  Loop through MfrCodes
------------------------------------------------------------------------------------------


      DECLARE @vm$MfrEntity     bigint
             ,@vm$MfrCode       varchar(64)
			
			DECLARE  cur_v_mfr cursor local for
			SELECT DISTINCT pas.entity_structure_fk, mfrcode.value
			FROM epacube.epacube.product_structure ps with (nolock)
			inner join epacube.epacube.product_identification pi with (nolock) -- epaCUBE PDM Product ID
			on ( pi.product_structure_fk = ps.product_structure_id
			and  pi.data_name_fk = 110100 ) -- PRODUCT_ID
			---  System Assoc of 'PRODUCT' is required
			inner join epacube.epacube.product_association pasym with (nolock)
			   on (pasym.data_name_fk = 159200
			   and pasym.product_structure_fk = ps.product_structure_id 
			   and pasym.org_entity_structure_fk = 1
			   and pasym.entity_structure_fk = ( select ei.entity_structure_fk
													from epacube.epacube.entity_identification ei with (nolock)
													where ei.entity_data_name_fk = 142000
													and   ei.value = 'PRODUCT' )  )
			--- MFR Assoc is required
			inner join epacube.epacube.product_association pas  with (nolock)  -- must have mfr association
			on ( pas.product_structure_fk = ps.product_structure_id
			and  pas.data_name_fk = 159300  )  --Product/Mfr Association
			--  MfrCode is required
			inner join  epacube.epacube.entity_identification mfrcode with (nolock)
			on ( mfrcode.data_name_fk = 143110
			and  mfrcode.entity_structure_fk = pas.entity_structure_fk )
			WHERE 1=1
				and   (   isnull(@in_initial_ind, 0 ) = 1
				or ps.update_timestamp > @in_last_effective_date )  
			ORDER BY  MfrCode.value ASC

                    
    OPEN cur_v_mfr;
    FETCH NEXT FROM cur_v_mfr INTO  @vm$MfrEntity, @vm$MfrCode


------------------------------------------------------------------------------------------
---  LOOP THROUGH BASIS STACK OF QUALIFED RULES DATA
------------------------------------------------------------------------------------------


         WHILE @@FETCH_STATUS = 0 
         BEGIN

		INSERT INTO [paxson].[dbo].[job_execution]
				   ([JOB_FK]
				   ,[description]
				   ,[START_TIMESTAMP] )
			 VALUES
				   (@in_job_fk
				   ,'StandardProduct :: Temp Table for MfrCode:: ' +  @vm$MfrCode
				   ,GETDATE ()
					)


-------------------------------------------------------------------------------------
--   Insert Temp Table
-------------------------------------------------------------------------------------


Synchronize_StandardProduct


    TRUNCATE TABLE #TS_StdProduct 

	INSERT INTO #TS_StdProduct 
           ([PDMProductID]
           ,[MfrCode]
           ,[CatalogNo]
           ,[MfrPGC]
           ,[description]
           ,[ListPrice]
           ,[Col1Price]
           ,[Col2Price]
           ,[Col3Price]
           ,[DistPrice]
           ,[UOM]
           ,[UPC]
           ,[CommCode]
           ,[MfrUCCNum]
           ,[ItemNum]
           ,[Zone]
           ,[UNSPSC]
           ,[PSCashDiscount]
           ,[LegacyUpdateStatus]
           ,[CountryOfOrigin]
           ,[PercentMfdgInUSA]
		   ,[product_status_cr_fk]
		   ,[SRP_ProductId]         
		 )
     SELECT 
		  pi.value as  PDMProductID
		 ,substring(mfrcode.value,1,5) as MfrCode 
		 ,substring(catno.value,1,20) as CatalogNo
		 ,(CASE when (select count(1) 
					  from epacube.epacube.entity_data_value edv with (nolock)
					  inner join epacube.epacube.product_category pc with (nolock)
					   on ( pc.entity_data_value_fk = edv.entity_data_value_id )
					  where pc.data_name_fk in (100143901, 100143902,100143903)
					  and   pc.product_structure_fk = ps.product_structure_id
					  and   pc.org_entity_structure_fk = 1
					  and   pc.entity_structure_fk = pas.entity_structure_fk 
					  and   edv.value <> '-999999999' ) > 1 then '*'
				else (select case when isnull ( edv.value, '-999999999' )  = '-999999999' then NULL 
							 else case when len(edv.value) > 20 then null else edv.value end
							 end
					  from epacube.epacube.entity_data_value edv with (nolock)
					  inner join epacube.epacube.product_category pc with (nolock)
					   on ( pc.entity_data_value_fk = edv.entity_data_value_id )
						where pc.data_name_fk = 100143901 
						and   pc.product_structure_fk = ps.product_structure_id
						and   pc.org_entity_structure_fk = 1
						and   pc.entity_structure_fk = pas.entity_structure_fk )
			   end  ) as MfrPGC
		,( select case when isnull (pd.description, '-999999999' ) = '-999999999' then NULL else pd.description end
		  from epacube.epacube.product_description pd  with (nolock)
		  where pd.data_name_fk = 4104001
		  and   pd.product_structure_fk = ps.product_structure_id
		  and   pd.org_entity_structure_fk = 1
		  and   pd.entity_structure_fk is null )  as Description
		,pv.net_value5 as ListPrice
		,pv.net_value4 as Col1Price
		,pv.net_value3 as Col2Price
		,pv.net_value2 as Col3Price
		,pv.net_value1 as DistPrice
		,( select case when isnull(uom.uom_code, '-999999999' ) = '-999999999' then NULL else uom.uom_code end 
		   from epacube.epacube.product_uom_class  puc  with (nolock)
		   inner join epacube.epacube.uom_code uom on uom.uom_code_id = puc.uom_code_fk 
		   where puc.data_name_fk = 110606
		   and   puc.product_structure_fk = ps.product_structure_id
		   and   puc.org_entity_structure_fk = 1
		   and   puc.entity_structure_fk is null ) as UOM
		,upc.value as UPC
		,( select case when isnull (dv.value, '-999999999') = '-999999999' then NULL else dv.value end  
		   from  epacube.epacube.product_category pc with (nolock)
		   inner join epacube.epacube.data_value dv with (nolock)
		   on dv.data_value_id = pc.data_value_fk
		   where pc.data_name_fk = 110902
		   and   pc.product_structure_fk = ps.product_structure_id
		   and   pc.org_entity_structure_fk = 1
		   and   pc.entity_structure_fk is null )  as CommCode
		,( select case when isnull (edv.value, '-999999999') = '-999999999' then NULL else edv.value end  
		   from epacube.epacube.product_category pc with (nolock)
		   inner join epacube.epacube.entity_data_value edv with (nolock) 
			 on  edv.entity_data_value_id = pc.entity_data_value_fk 
		   where pc.data_name_fk = 113902
		   and   pc.product_structure_fk = ps.product_structure_id
		   and   pc.org_entity_structure_fk = 1
		   and   pc.entity_structure_fk = pas.entity_structure_fk )  as MfrUCCNum
		,( select case when isnull (pinu.value, '-999999999') = '-999999999' then NULL else pinu.value end
		  from epacube.epacube.product_ident_nonunique pinu with (nolock)
		  where pinu.data_name_fk = 820101
		  and   pinu.product_structure_fk = ps.product_structure_id
		  and   pinu.org_entity_structure_fk = 1
		  and   pinu.entity_structure_fk is null )  as ItemNum
		,substring(zone.value,1,3) as Zone
		,( select case when isnull (dv.value, '-999999999') = '-999999999' then NULL else dv.value end  
		   from  epacube.epacube.product_category pc with (nolock)
		   inner join epacube.epacube.data_value dv with (nolock)
		   on dv.data_value_id = pc.data_value_fk
		   where pc.data_name_fk = 110901
		   and   pc.product_structure_fk = ps.product_structure_id
		   and   pc.org_entity_structure_fk = 1
		   and   pc.entity_structure_fk is null )  as UNSPSC
		,( select case when isnull (dv.value, '-999999999') = '-999999999' then NULL else dv.value end  
		   from  epacube.epacube.product_attribute pa with (nolock)
		   inner join epacube.epacube.data_value dv with (nolock)
		   on dv.data_value_id = pa.data_value_fk
		   where pa.data_name_fk =  110819
		   and   pa.product_structure_fk = ps.product_structure_id
		   and   pa.org_entity_structure_fk = 1
		   and   pa.entity_structure_fk is null )  as PSCashDiscount
		,( select case when isnull (dv.value, '-999999999') = '-999999999' then NULL else dv.value end  
		   from  epacube.epacube.product_status psts with (nolock)
		   inner join epacube.epacube.data_value dv with (nolock)
		   on dv.data_value_id = psts.data_value_fk
		   where psts.data_name_fk = 820201
		   and   psts.product_structure_fk = ps.product_structure_id
		   and   psts.org_entity_structure_fk = 1
		   and   psts.entity_structure_fk is null ) as LegacyUpdateStatus
		,( select case when isnull (pa.attribute_event_data, '-999999999') = '-999999999' then NULL else pa.attribute_event_data end  
		   from  epacube.epacube.product_attribute pa with (nolock)
		   where pa.data_name_fk =   820816
		   and   pa.product_structure_fk = ps.product_structure_id
		   and   pa.org_entity_structure_fk = 1
		   and   pa.entity_structure_fk is null ) as CountryOfOrigin
		,( select case when isnull (pa.attribute_event_data, '-999999999') = '-999999999' then NULL else pa.attribute_event_data end  
		   from  epacube.epacube.product_attribute pa with (nolock)
		   where pa.data_name_fk =  820817
		   and   pa.product_structure_fk = ps.product_structure_id
		   and   pa.org_entity_structure_fk = 1
		   and   pa.entity_structure_fk is null )  as PercentMfdgInUSA
		,case when catno.value = '999999999'
		      then 105  -- hold
			  else  case when mfrcode.value = '999999999' 
			             then 105   -- hold
						 else ps.product_status_cr_fk  
						 end
			  end as product_status_cr_fk
		,srp.ProductId as SRP_ProductId          
FROM epacube.epacube.product_structure ps with (nolock)
inner join epacube.epacube.product_identification pi with (nolock) -- epaCUBE PDM Product ID
on ( pi.product_structure_fk = ps.product_structure_id
and  pi.data_name_fk = 110100 ) -- PRODUCT_ID
---  System Assoc of 'PRODUCT' is required
inner join epacube.epacube.product_association pasym with (nolock)
   on (pasym.data_name_fk = 159200
   and pasym.product_structure_fk = ps.product_structure_id 
   and pasym.org_entity_structure_fk = 1
   and pasym.entity_structure_fk = ( select ei.entity_structure_fk
										from epacube.epacube.entity_identification ei with (nolock)
										where ei.entity_data_name_fk = 142000
										and   ei.value = 'PRODUCT' )  )
--- MFR Assoc is required
inner join epacube.epacube.product_association pas  with (nolock)  -- must have mfr association
on ( pas.product_structure_fk = ps.product_structure_id
and  pas.data_name_fk = 159300  )  --Product/Mfr Association
--  MfrCode is required
inner join  epacube.epacube.entity_identification mfrcode with (nolock)
on ( mfrcode.data_name_fk = 143110
and  mfrcode.entity_structure_fk = pas.entity_structure_fk )
--- Catalog Number is required 
INNER JOIN epacube.epacube.product_identification catno with (nolock)
  on  ( catno.data_name_fk = 820103    ---- CatalogNo Must Exist otherwise do not use
  and   catno.product_structure_fk = ps.product_structure_id
  and   catno.org_entity_structure_fk = 1
  and   catno.entity_structure_fk =  pas.entity_structure_fk )  -- MfrCode    --- = pas.entity_structure_fk
--- UPC is required 
INNER JOIN epacube.epacube.product_identification upc with (nolock)
  on  ( upc.data_name_fk = 110102    ---- UPC Must Exist otherwise do not use
  and   upc.product_structure_fk = ps.product_structure_id
  and   upc.org_entity_structure_fk = 1
  and   upc.entity_structure_fk is null )  
---  Zone Assoc is required
inner join epacube.epacube.product_association paszn with (nolock)
   on (paszn.data_name_fk = 159100
   and paszn.product_structure_fk = ps.product_structure_id )
inner join epacube.epacube.entity_identification zone
on  ( zone.entity_structure_fk = paszn.org_entity_structure_fk
and   zone.data_name_fk = 141111  ) 
--- Zone pricing is optional
left join epacube.epacube.product_values pv  with (nolock)  
on (  pv.data_name_fk = 822100  --- TS Columns
and   pv.product_structure_fk = ps.product_structure_id 
and   pv.org_entity_structure_fk = zone.entity_structure_fk )
------- check against already existing SRP Rows
LEFT JOIN [paxson].[dbo].[StandardProduct] srp with (nolock)
							on  srp.PDMProductID = pi.value 
							and srp.zone = zone.value
-----
WHERE 1 = 1
and   ps.update_timestamp > @in_last_effective_date  


--- rows that exist only in SRP Table need to be marked IsActive = false
	INSERT INTO #TS_StdProduct (
		 PDMProductID           
		,Zone                   
		,product_status_cr_fk   
		,MfrCode                
		,CatalogNo              
		,SRP_ProductId          
		 )
	 SELECT 
		 PDMProductID         
		,Zone                
		,NULL    -- no status         
		,MfrCode       
		,CatalogNo                   
		,ProductId    
	from paxson.dbo.StandardProduct
	where MfrCode = @vm$MfrCode
	and   ProductId not in (  select SRP_ProductId
			                   from #TS_StdProduct  )


--------------------------------------------------------------------------------------------
--  Log completion of Temp Table Inserts for MfrCode
--------------------------------------------------------------------------------------------

    SET @v_count_rows = ( select count(1) from #TS_StdProduct )

	UPDATE [paxson].[dbo].[job_execution]
		SET   end_timestamp = GETDATE()
		     ,data = @v_count_rows
		WHERE job_fk = @in_job_fk
		AND   description = ( 'StandardProduct :: Temp Table for MfrCode:: ' +  @vm$MfrCode )



-------------------------------------------------------------------------------------
--   Update Product Standard Rows
-------------------------------------------------------------------------------------

SET @v_count_rows = ( select count(1) from #TS_StdProduct tsp
                      where  1=1
					  and    tsp.SRP_ProductId is not null
					  and    tsp.product_status_cr_fk = 100   )   --- active products only


INSERT INTO [paxson].[dbo].[job_execution]
           ([JOB_FK]
           ,[description]
		   ,[DATA]
           ,[START_TIMESTAMP] )
     VALUES
           (@in_job_fk
           ,'StandardProduct :: Updates for MfrCode:: ' +  @vm$MfrCode
		   ,CAST ( @v_count_rows as varchar(16) )
		   ,GETDATE ()
            )

IF @v_count_rows > 0
UPDATE [dbo].[StandardProduct]
   SET [MfrCode] = tsp.MfrCode
      ,[CatalogNo] = tsp.CatalogNo
      ,[MfrPGC] = tsp.MfrPGC
      ,[description] = tsp.description
      ,[ListPrice] = tsp.ListPrice
      ,[Col1Price] = tsp.Col1Price
      ,[Col2Price] = tsp.Col2Price
      ,[Col3Price] = tsp.Col3Price
      ,[DistPrice] = tsp.DistPrice
      ,[UOM] = tsp.UOM
      ,[UPC] = tsp.UPC
      ,[CommCode] = tsp.CommCode
      ,[MfrUCCNum] = tsp.MfrUCCNum
      ,[ItemNum] = tsp.ItemNum
      ,[Zone] = tsp.Zone
      ,[UNSPSC] = tsp.UNSPSC
      ,[PSCashDiscount] = tsp.PSCashDiscount
      ,[LegacyUpdateStatus] = tsp.LegacyUpdateStatus
      ,[CountryOfOrigin] = tsp.CountryOfOrigin
      ,[PercentMfdgInUSA] = tsp.PercentMfdgInUSA
      ,[IsActive] = 1 -----  Active
      ,[LastModifiedBy] = @in_update_user
      ,[LastModifiedTime] = @v_effective_date
FROM  #TS_StdProduct tsp
WHERE 1 = 1
and   [dbo].[StandardProduct].ProductId = tsp.SRP_ProductId 
and   tsp.product_status_cr_fk = 100 


UPDATE [paxson].[dbo].[job_execution]
 SET   end_timestamp = GETDATE()
 WHERE job_fk = @in_job_fk
 AND   description = ( 'StandardProduct :: Updates for MfrCode:: ' +  @vm$MfrCode )



-------------------------------------------------------------------------------------
--   Inactivate Product Standard Rows
-------------------------------------------------------------------------------------

SET @v_count_rows = ( select count(1) from #TS_StdProduct tsp
                      where  1=1
					  and    tsp.SRP_ProductId is not null
					  and    tsp.product_status_cr_fk  IN ( 101, 102, 103, 106 )  )  ---- In Active Statuses


INSERT INTO [paxson].[dbo].[job_execution]
           ([JOB_FK]
           ,[description]
		   ,[DATA]
           ,[START_TIMESTAMP] )
     VALUES
           (@in_job_fk
           ,'StandardProduct :: InActivate for MfrCode:: ' +  @vm$MfrCode
		   ,CAST ( @v_count_rows as varchar(16) )
		   ,GETDATE ()
            )

IF @v_count_rows > 0
UPDATE [paxson].[dbo].[StandardProduct]
   SET 
       [IsActive] = 0 -----  InActivate
      ,[LastModifiedBy] = @in_update_user
      ,[LastModifiedTime] = @in_effective_date
WHERE 1=1
and   ProductId IN (  select tsp.SRP_ProductId
                      from #TS_StdProduct tsp
                      where  1=1
					  and    tsp.SRP_ProductId is not null
					  and    tsp.product_status_cr_fk  IN ( 101, 102, 103, 106 )  )  ---- In Active Statuses


UPDATE [paxson].[dbo].[job_execution]
 SET   end_timestamp = GETDATE()
 WHERE job_fk = @in_job_fk
 AND   description = ( 'StandardProduct :: InActivate for MfrCode:: ' +  @vm$MfrCode )



-------------------------------------------------------------------------------------
--   Insert Product Standard Rows
-------------------------------------------------------------------------------------


SET @v_count_rows = ( select count(1) from #TS_StdProduct tsp
                      where  1=1
					  and    tsp.SRP_ProductId is null
					  and    tsp.product_status_cr_fk  = 100  )  ---- Active Status ONLY



INSERT INTO [paxson].[dbo].[job_execution]
           ([job_fk]
           ,[description]
		   ,[data]
           ,[start_timestamp] )
     VALUES
           (@in_job_fk
           ,'StandardProduct :: Inserts for MfrCode:: ' +  @vm$MfrCode
		   ,CAST ( @v_count_rows as varchar(16) )
		   ,GETDATE ()
            )

IF @v_count_rows > 0
INSERT INTO [paxson].[dbo].[StandardProduct]
           ([PDMProductID]
           ,[MfrCode]
           ,[CatalogNo]
           ,[MfrPGC]
           ,[description]
           ,[ListPrice]
           ,[Col1Price]
           ,[Col2Price]
           ,[Col3Price]
           ,[DistPrice]
           ,[UOM]
           ,[UPC]
           ,[CommCode]
           ,[MfrUCCNum]
           ,[ItemNum]
           ,[Zone]
           ,[UNSPSC]
           ,[PSCashDiscount]
           ,[LegacyUpdateStatus]
           ,[CountryOfOrigin]
           ,[PercentMfdgInUSA]
           ,[IsActive]
           ,[LastModifiedBy]
           ,[CreatedTime])
SELECT 
       tsp.PDMProductID
      ,tsp.MfrCode
      ,tsp.CatalogNo
      ,tsp.MfrPGC
      ,tsp.description
      ,tsp.ListPrice
      ,tsp.Col1Price
      ,tsp.Col2Price
      ,tsp.Col3Price
      ,tsp.DistPrice
      ,tsp.UOM
      ,tsp.UPC
      ,tsp.CommCode
      ,tsp.MfrUCCNum
      ,tsp.ItemNum
      ,tsp.Zone
      ,tsp.UNSPSC
      ,tsp.PSCashDiscount
      ,tsp.LegacyUpdateStatus
      ,tsp.CountryOfOrigin
      ,tsp.PercentMfdgInUSA
      ,1
      ,@in_update_user
      ,@in_effective_date
 FROM #TS_StdProduct tsp
WHERE 1 = 1
and    tsp.SRP_ProductId is null
and    tsp.product_status_cr_fk  = 100 


UPDATE [paxson].[dbo].[job_execution]
 SET   end_timestamp = GETDATE()
 WHERE job_fk = @in_job_fk
 AND   description = ( 'StandardProduct :: Inserts for MfrCode:: ' +  @vm$MfrCode )





-------------------------------------------------------------------------------------
--   Data Error Logging  -- PDMProductID exists in SRP table but not epacube
-------------------------------------------------------------------------------------


INSERT INTO [dbo].[job_errors]
           ([job_fk]
           ,[description]
           ,[data]
           )
SELECT DISTINCT
     @in_job_fk
	,'PDMProductID exists in SRP table but not epacube '
	,tsp.PDMProductID
FROM #TS_StdProduct tsp
WHERE tsp.SRP_ProductId is not null
AND   tsp.product_status_cr_fk is null   -- no status



-------------------------------------------------------------------------------------
--   Data Error Logging  -- CatalogNo is set to be Nulled in epaCUBE
-------------------------------------------------------------------------------------

INSERT INTO [dbo].[job_errors]
           ([job_fk]
           ,[description]
           ,[data]
           )
SELECT DISTINCT
     @in_job_fk
	,'CatalogNo is to be NULLed out in epaCUBE for PDMProductID '
	,tsp.PDMProductID
FROM #TS_StdProduct tsp
WHERE tsp.CatalogNo = '999999999'


-------------------------------------------------------------------------------------
--   Data Error Logging  -- MfrCode is set to be Nulled in epaCUBE
-------------------------------------------------------------------------------------

INSERT INTO [dbo].[job_errors]
           ([job_fk]
           ,[description]
           ,[data]
           )
SELECT DISTINCT
     @in_job_fk
	,'MfrCode is to be NULLed out in epaCUBE for PDMProductID '
	,tsp.PDMProductID
FROM #TS_StdProduct tsp
WHERE tsp.MfrCode = '999999999'  




------------------------------------------------------------------------------------------
---  GET NEXT MfrCode 
------------------------------------------------------------------------------------------

     FETCH NEXT FROM cur_v_mfr INTO  @vm$MfrEntity, @vm$MfrCode

     END --cur_v_mfr  LOOP
     CLOSE cur_v_mfr
	 DEALLOCATE cur_v_mfr; 
     


----------------------------------------------------------------------------------------------
------  Drop Temp Tables
----------------------------------------------------------------------------------------------


----  drop temp table if it exists
	IF object_id('tempdb..#TS_StdProduct') is not null
	   drop table #TS_StdProduct;



-------------------------------------------------------------------------------------
--   End of Procedure
-------------------------------------------------------------------------------------

UPDATE [paxson].[dbo].[job_execution]
 SET   end_timestamp = GETDATE()
 WHERE job_fk = @in_job_fk
 AND   description = 'Synchronize_epaCUBE_to_Paxson_StandardProduct'

*/
      
END TRY
BEGIN CATCH   

      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of [dbo].[Synchronize_epaCUBE_to_Paxson_StandardProduct] has failed ' + ISNULL ( ERROR_MESSAGE(), 'Unknown Error'),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		INSERT INTO [dbo].[job_execution]
				   ([job_fk]
				   ,[description]
				   ,[start_timestamp]
				   )
			 VALUES
				   (@in_job_fk
				   ,@ErrorMessage
				   ,getdate()
				    )

         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );

   END CATCH;


END

