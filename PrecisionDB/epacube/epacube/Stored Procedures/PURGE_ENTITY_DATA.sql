﻿







-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Purge ENTITY data that is marked for purge on the entity structure table.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        01/01/2008  Initial SQL Version
-- CV        02/18/2013  Delete entity data if not assoicated with products





CREATE PROCEDURE [epacube].[PURGE_ENTITY_DATA]

AS
BEGIN

DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int
DECLARE @ENTITY$ENTITY_STRUCTURE_ID bigint
DECLARE @ENTITY$ENTITY_CLASS_FK int
DECLARE @ENTITY$ENTITY_IDENT varchar(128)

BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0
SET @l_sysdate = getdate()

SET @ls_stmt = 'started execution of epacube.PURGE_ENTITY_DATA'
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
							   			  @exec_id   = @l_exec_no OUTPUT;

--drop temp table if it exists
IF object_id('tempdb..#TS_ENTITY') is not null
   drop table #TS_ENTITY
---- create temp table
CREATE TABLE #TS_ENTITY(
	ENTITY_STRUCTURE_ID bigint NOT NULL,
    ENTITY_CLASS_CR_FK bigint NOT NULL )
--
INSERT into #TS_ENTITY (ENTITY_STRUCTURE_ID, ENTITY_CLASS_CR_FK)

SELECT distinct es.entity_structure_id, es.entity_class_cr_fk
  FROM epacube.ENTITY_structure es
    WHERE  es.entity_status_cr_fk = 10202   -- PURGE
	and ENTITY_STRUCTURE_ID not in (select distinct ENTITY_STRUCTURE_fk from epacube.PRODUCT_ASSOCIATION)
  


 ------------------------------------------------------------------------------------------------------------------
---DELETE FROM ENTITY TABLES
------------------------------------------------------------------------------------------------------------------

	DELETE FROM epacube.ENTITY_attribute
	WHERE  ENTITY_structure_fk in 
	(
	SELECT ENTITY_structure_id
	FROM #TS_ENTITY 
	)

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows deleted from epacube.ENTITY_attribute table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END

--------------------------------------------------------------------------------------

	DELETE FROM epacube.ENTITY_category
	WHERE  ENTITY_structure_fk in 
	(
	SELECT ENTITY_structure_id
	FROM #TS_ENTITY 
	)

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows deleted from epacube.ENTITY_category table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END

----------------------------------------------------------------------------------------------------
	
	DELETE FROM epacube.ENTITY_ident_MULT
	WHERE  ENTITY_structure_fk in 
	(
	SELECT ENTITY_structure_id
	FROM #TS_ENTITY 
	)

-------------------------------------------------------------------------------------------------------

	DELETE FROM epacube.ENTITY_ident_nonunique
	WHERE  ENTITY_structure_fk in 
	(
	SELECT ENTITY_structure_id
	FROM #TS_ENTITY 
	)

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows deleted from epacube.ENTITY_ident_nonunique table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END

	------------------------------------------------------------------------------------------------

	DELETE FROM epacube.ENTITY_identification
	WHERE  ENTITY_structure_fk in 
	(
	SELECT ENTITY_structure_id
	FROM #TS_ENTITY 
	)

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows deleted from epacube.ENTITY_identification table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END

	----------------------------------------------------------------------------------------------------------

	DELETE FROM epacube.ENTITY_mult_type
	WHERE  ENTITY_structure_fk in 
	(
	SELECT ENTITY_structure_id
	FROM #TS_ENTITY 
	)

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows deleted from epacube.ENTITY_mult_type table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END

----
-------------------------------------------------------------------------------------------------------	

	DELETE FROM epacube.ENTITY_association
	WHERE  1 =1
    AND Parent_entity_structure_fk in 
			(
			SELECT ENTITY_structure_id
			FROM #TS_ENTITY 
			)
	OR Child_entity_structure_fk in 
			(
			SELECT ENTITY_structure_id
			FROM #TS_ENTITY 
			)

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows deleted from epacube.ENTITY_association table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END

	------------------------------------------------------------------------------------------------------
	
	--DELETE FROM epacube.ENTITY_DATA_VALUE
	--WHERE  ENTITY_STRUCTURE_FK in 
	--(
	--SELECT ENTITY_structure_id
	--FROM #TS_ENTITY ps
	--)

 --   SET @l_rows_processed = @@ROWCOUNT
 --   IF @l_rows_processed > 0
 --   BEGIN
	--	SET @status_desc = 'Total rows deleted from epacube.ENTITY_DATA_VALUE = '
	--		+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
	--	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
 --   END


	DELETE FROM epacube.ENTITY_structure
	WHERE  ENTITY_structure_id in 
	(
	SELECT ENTITY_structure_id
	FROM #TS_ENTITY ps
	)

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows deleted from epacube.ENTITY_structure table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END




--drop temp table if it exists
IF object_id('tempdb..#TS_ENTITY') is not null
   drop table #TS_ENTITY;

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

	SET @status_desc = 'finished execution of epacube.PURGE_ENTITY_DATA'
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.epacube.PURGE_ENTITY_DATA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END



















































































































