﻿










-- Copyright 2006 epaCUBE, Inc.
--
-- Procedure created by Epacube
--
--
-- 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV       01/20/2014   Initial SQL Version - adding a job

CREATE PROCEDURE [epacube].[RUN_PURGE_IMPORT_DATA_QUARTZ] @purge_immediate_ind INT = 1 
AS
BEGIN

        DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @job_timestamp DATETIME

DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

		

        BEGIN TRY
            SET NOCOUNT ON ;
            SET @l_exec_no = 0
            SET @l_sysdate = GETDATE()
		
						
			
				  

							
            SET @ls_stmt = 'started execution of epacube.RUN_PURGE_IMPORT_DATA_QUARTZ. '
                
            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;
                
-------------------------------------------------------------------------------
   ---create job
------------------------------------------------------------------------------             
                  EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'job_seq', @v_job_fk output
   

    SET @v_job_class_fk  = 100       --- USER EXECUTED JOBS
    SET @v_job_user = 'EPACUBE'
    SET @v_job_name = 'PURGE IMPORT & STAGE DATA'
    SET @v_job_date = @l_sysdate
    SET @v_data1   = NULL
    SET @v_data2   = @v_last_job_date
    
    EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
										   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

    EXEC common.job_execution_create   @v_job_fk, 'START PURGE IMPORT & STAGE DATA',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  



                
			
-- call subroutine to purge stage/cleanser tables
			EXEC EPACUBE.PURGE_STAGE_TABLES 1         

-- call subroutine to purge data from import tables
			EXEC EPACUBE.PURGE_IMPORT_TABLES 1
			


--------------------------------------------------------------------------------------------
--  CREATE TEMP TABLE FOR JOBS TO BE REMOVED
--		REMOVE JOBS THAT JOB_DATE > ARCHIVE_PERIOD ( IF ARCHIVE_PERIOD IS NULL USE 30 DAYS )
--------------------------------------------------------------------------------------------

	EXEC common.job_execution_create   @v_job_fk, 'COMPLETED PURGE IMPORT & STAGE DATA',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @l_sysdate, @l_sysdate  
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

            SET @status_desc = 'finished execution of epacube.RUN_PURGE_IMPORT_DATA_QUARTZ'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;

        END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.RUN_PURGE_IMPORT_DATA_QUARTZ has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH
		  
    END











