﻿
CREATE PROCEDURE [epacube].[UI_Component_Consolidate] @Tbl varchar(96)

as 
--/***********************************************************
----	Written By  : Gary Stone
----	May, 2017
----*/----------------------------------------------------------

Set NoCount ON

	--Declare @Tbl Varchar(96)
	--Set @Tbl = 'temptbls.tmp_results_21335_gstone'
	
	Declare @SQL1 Varchar(Max) = ''
	Declare @SQL2 Varchar(Max) = 'Select '
	Declare @SQL3 Varchar(Max) = ''

	Declare @ColName Varchar(96)
	Declare @Pos varchar(8)
	Declare @DateColumn varchar(96) = ''

	If object_id('tempdb..#T1') is not null
	Drop table #T1

	If object_id('tempdb..#T2') is not null
	Drop table #T2

	Create Table #T1(ColName varchar(96) Null, Records int null)

	DECLARE SingleRecConsolidation Cursor local for
	Select column_name, ordinal_position from epacube.INFORMATION_SCHEMA.COLUMNS where table_schema + '.' + Table_Name = @Tbl
	order by ordinal_position

		OPEN SingleRecConsolidation;
		FETCH NEXT FROM SingleRecConsolidation INTO @ColName, @Pos
		WHILE @@FETCH_STATUS = 0

		Begin

		Set @SQL1 = 'Insert into #T1 Select  ''' + @ColName + ''', count(1) from (Select [' + @ColName + '] from ' + @Tbl + ' where [' + @ColName + '] is not null group by [' + @ColName + ']) A '
		Exec(@SQL1) 

		If @ColName not in ('Data_Level_CR_FK', 'Data Level')
		Set @SQL2 = @SQL2 + '(Select top 1 [' + @ColName + '] from ' + @Tbl + ' where [' + @ColName + '] is not null) ''' + @ColName + ''', '

		If @ColName like '%DATE%' 
		and isnull(	(Select ds.table_name 
					from epacube.data_name DN1 with (nolock) 
					inner join epacube.data_name_virtual dnv with (nolock) on DN1.data_name_id = dnv.DATA_NAME_FK
					inner join epacube.data_name dn with (nolock) on dnv.REFERENCE_DATA_NAME_FK = dn.data_name_id
					inner join epacube.data_set ds with (nolock) on dn.DATA_SET_FK = ds.DATA_SET_ID where dn1.name = @ColName)
					,
					(Select DS.Table_name 
					from epacube.data_name dn with (nolock) 
					inner join epacube.data_set ds with (nolock) on dn.DATA_SET_FK = ds.DATA_SET_ID where dn.name = @ColName)
					) in ('PRICESHEET_BASIS_VALUES', 'PRICESHEET_PURCHASE_ALLOWANCES', 'PRICESHEET_PROMOTIONAL_ALLOWANCES')
			and @DateColumn = ''
		Set @DateColumn = '[' + @ColName + ']'
		
	FETCH NEXT FROM SingleRecConsolidation INTO @ColName, @Pos

	End

	Close SingleRecConsolidation;
	Deallocate SingleRecConsolidation;

If isnull((Select Max(Records) from #T1 where ColName not in ('Data_Level_CR_FK', 'Data Level')), 0) < 2
Begin
	Set @SQL2 = Left(@SQL2, Len(@SQL2) - 1) + ' into #T2; Drop table ' + @Tbl + ' Select * into ' + @Tbl+ ' from #T2'
	
	Exec(@SQL2)
End
	
	Exec('Alter Table ' + @Tbl + ' Add [ROW_STATUS] Varchar(64) Null')
		
	If @DateColumn <> ''
	Begin
		Set @SQL3 = '
		Update T
		Set Row_Status = Case when Cast(' + @DateColumn + ' as date) > Getdate() then ''FUTURE'' else ''PREVIOUS'' end
		from ' + @Tbl + ' T

		Update T
		Set Row_Status = ''CURRENT''
		from ' + @Tbl + ' T where ' + @DateColumn + ' = (Select top 1 ' + @DateColumn + ' from ' + @Tbl + ' with (nolock) where Cast(' + @DateColumn + ' as date) <= Getdate() order by Cast(' + @DateColumn + ' as date) desc)
		'
		Exec(@SQL3)
	End
--End

	If object_id('tempdb..#T1') is not null
	Drop table #T1

	If object_id('tempdb..#T2') is not null
	Drop table #T2
