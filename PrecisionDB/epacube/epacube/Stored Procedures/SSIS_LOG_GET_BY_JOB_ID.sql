﻿
-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get SSIS Log Information JIRA (PRE-116)
-- Get SSIS log execution information by job id
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------

CREATE   PROCEDURE [epacube].[SSIS_LOG_GET_BY_JOB_ID]
	@in_job_id BIGINT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @job_id_char VARCHAR(255);
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	SET @job_id_char = CAST(@in_job_id AS VARCHAR(255));

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.SSIS_LOG_GET_BY_JOB_ID for job ' +  @job_id_char;
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT @in_job_id job_id
			, ex.execution_id
			, ex.package_name
			, CASE 
				   WHEN (om.message_source_type = 10) THEN 'ISServerExec' 
				   WHEN (om.message_source_type = 20) THEN 'Transact-SQL stored procedure'
				   ELSE em.message_source_name
			   END AS task_name
			, CAST(om.message_time AS DATETIME2(0)) AS ssis_run_datetime
			, CAST(DATEADD(SECOND, DATEDIFF(SECOND, o.start_time, o.end_time), 0) AS TIME(0)) AS ssis_run_duration
			, em.event_name
			, om.message
		FROM SSISDB.internal.event_messages em WITH (NOLOCK)
			INNER JOIN SSISDB.internal.operation_messages om WITH (NOLOCK)
				ON em.event_message_id = om.operation_message_id
			INNER JOIN SSISDB.internal.operations o WITH (NOLOCK) 
				ON em.operation_id = o.operation_id
			INNER JOIN SSISDB.internal.executions ex WITH (NOLOCK)
				ON o.operation_id = ex.execution_id
		WHERE o.operation_id = (
			SELECT MAX(operation_id)
			FROM SSISDB.internal.event_message_context
			WHERE context_source_name = 'vJobID'
				AND property_value = @in_job_id)
			AND em.event_name NOT LIKE '%Validate%'
			AND em.event_name NOT LIKE '%Warning%'
		ORDER BY ssis_run_datetime

		SET @ls_stmt = 'Finished execution of epacube.SSIS_LOG_GET_BY_JOB_ID for job' + @job_id_char; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH
		SET @ls_stmt = 'Execution of epacube.SSIS_LOG_GET_BY_JOB_ID has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END