﻿
-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create entities JIRA (URM-1201)
-- 1. Parse the JSON object into temp table and insert the records
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE OR ALTER PROCEDURE [epacube].[DATA_VALUE_CREATE] 
	@data_value NVARCHAR(MAX)
	,@user VARCHAR(64)
AS
BEGIN
	DECLARE @data_value_id_str VARCHAR(255);
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @return_message VARCHAR(1000);

	SET NOCOUNT ON;

	--***************************************************************************************************************************************
	--Example data value structure
	--{
	--  "DATA_NAME_FK": 50500,
	--	"VALUE": "TEST",
	--	"DESCRIPTION": "TEST DESCRIPTION",
	--	"PARENT_DATA_VALUE_FK": 1
	--}
	--***************************************************************************************************************************************

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.DATA_VALUE_CREATE';
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, @exec_id = @l_exec_no OUTPUT;

	IF OBJECT_ID('tempdb.dbo.#Data_Value', 'U') IS NOT NULL
		DROP TABLE #Data_Value;

	SELECT *
	INTO #Data_Value
	FROM OPENJSON(@data_value)
	WITH (
		DATA_NAME_FK INT '$.DATA_NAME_FK',
		VALUE VARCHAR(256) '$.VALUE',
		DESCRIPTION VARCHAR(256) '$.DESCRIPTION',
		PARENT_DATA_VALUE_FK BIGINT '$.PARENT_DATA_VALUE_FK'
	);

    BEGIN TRY
		BEGIN TRANSACTION;
			
			INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, PARENT_DATA_VALUE_FK, CREATE_USER)
			SELECT DATA_NAME_FK, VALUE, DESCRIPTION, PARENT_DATA_VALUE_FK, @user
			FROM #Data_Value;

			SET @data_value_id_str = SCOPE_IDENTITY();

		COMMIT TRANSACTION;

		SET @ls_stmt = 'End execution of epacube.DATA_VALUE_CREATE - DATA_VALUE ' + @data_value_id_str + ' successfully created';
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		SET @return_message = 'Successfully created DATA_VALUE ' + @data_value_id_str;
		
		SELECT @return_message AS RETURN_MESSAGE; 

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
			END 
						
		SET @ls_stmt = 'Execution of epacube.DATA_VALUE_CREATE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

		SET @return_message = 'Cannot create DATA_VALUE';
		
		SELECT @return_message AS RETURN_MESSAGE; 

	END CATCH
END
