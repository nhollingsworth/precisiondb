﻿





CREATE PROCEDURE [epacube].[UTIL_MERGE_PRODUCT_ID] 
         
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        07/13/2011  Initial SQL Version
 
  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @v_batch_no   bigint
        
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT
        
 
 
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of epacube.UTL_MERGE_PRODUCT_ID. '
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
 

                
                  EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'job_seq', @v_job_fk output
    
    SET @v_job_class_fk  = 100        --- PRODUCT MAINTENANCE
    SET @v_job_user = 'EPACUBE'
    SET @v_job_name = 'MERGE PRODUCT ID'
    SET @v_job_date = @l_sysdate
    SET @v_data1   = 'LAST JOB:: '
                     + CAST ( ISNULL ( @v_last_job_fk, -999 )  AS VARCHAR(32) )
                     + ' - '
                     + CAST ( ISNULL ( @v_last_job_date, '01/01/2000' ) AS VARCHAR(64) )
    SET @v_data2   = @v_last_job_date
    
    EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
										   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

    EXEC common.job_execution_create   @v_job_fk, 'START MERGE PRODUCT ID ',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  

                

---------------------------------------------------------------------------------------
----CREATE TEMP TABLE FOR PRODUCT LIST-----
---------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_MERGE_PRODUCT_ID') is not null
	   drop table #TS_MERGE_PRODUCT_ID;
	   
	      
					CREATE TABLE #TS_MERGE_PRODUCT_ID 
					 (PRODUCT_STRUCTURE_FK bigint Not NULL
					 ,JOB_FK int)


----------------------------------------------------------
-----Inserts product structure for products to be merged
----------------------------------------------------------
							insert into #TS_MERGE_PRODUCT_ID
							(PRODUCT_STRUCTURE_FK
							,JOB_FK)
							(select
							product_structure_fk 
							,@V_OUT_JOB_FK
							from epacube.PRODUCT_ATTRIBUTE 
							where DATA_NAME_FK = (select DATA_name_id from epacube.data_name
where Name = 'REPLACING PRODUCT ID')
)
	
    
    
--- Create events  one for updating product id one for creating 110834

INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
          
           ,[NEW_DATA]
           ,[CURRENT_DATA]
                    
           ,[SEARCH1]
           ,[SEARCH2]
           ,[SEARCH3]
           ,[SEARCH4]
           ,[SEARCH5]
           ,[SEARCH6]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[EVENT_REASON_CR_FK]
           
           ,[IMPORT_JOB_FK]
           
           ,[TABLE_ID_FK]
          
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
           ( SELECT 
							150
						   ,10109
						   ,GETDATE()
						   ,TMP.product_structure_fk
						   ,110100
						   ,TMP.PRODUCT_STRUCTURE_FK
						   ,1
				          
           ,(select pa.attribute_event_data from epacube.PRODUCT_ATTRIBUTE pa 
           where pa.PRODUCT_STRUCTURE_FK = TMP.PRODUCT_STRUCTURE_FK
           and pa.DATA_NAME_FK = (select DATA_name_id from epacube.data_name
where Name = 'REPLACING PRODUCT ID')) --NEW DATA
           ,(select pi.value from epacube.PRODUCT_IDENTIFICATION pi 
           where pi.PRODUCT_STRUCTURE_FK = TMP.PRODUCT_STRUCTURE_FK 
           and pi.DATA_NAME_FK = 110100) --CURRENT_DATA, varchar,>
           
                 
           ,NULL  --SEARCH1, varchar(128),>
           ,NULL --<SEARCH2, varchar(128),>
           ,NULL --<SEARCH3, varchar(128),>
           ,NULL --SEARCH4, varchar(128),>
           ,NULL --<SEARCH5, varchar(128),>
           ,NULL --<SEARCH6, varchar(128),>
           ,97 --EVENT_CONDITION_CR_FK, int,>
           ,86 --EVENT_STATUS_CR_FK, int,>
           ,66  --EVENT_PRIORITY_CR_FK, int,>
           ,71  --EVENT_SOURCE_CR_FK, int,>
           ,52 --<EVENT_ACTION_CR_FK, int,>
           ,145  --Merge ---EVENT_REASON_CR_FK, int,>
           
           ,TMP.JOB_FK
           
           
           ,(select pi.PRODUCT_IDENTIFICATION_ID from epacube.PRODUCT_IDENTIFICATION pi 
           where pi.PRODUCT_STRUCTURE_FK = TMP.PRODUCT_STRUCTURE_FK 
           and pi.DATA_NAME_FK = 110100)  --table id
         
           ,1
           ,GETDATE()
           ,'EPACUBE'
           from #TS_MERGE_PRODUCT_ID TMP )
           
            EXEC common.job_execution_create   @v_job_fk, 'PRODUCT ID EVENTS CREATED',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  
  
 
           
-------------------------------------------------------------------------------

EXECUTE [synchronizer].[EVENT_APPROVE_UI] 0
--pass 0 to not call service broker
-------------------------------------------------------------------------------
         
  --ONLY upon approval do we set 110834  


           
           DECLARE 
                              
               @v_Attribute_id bigint,                                           
               @v_current_data varchar(64),
               @v_product_structure_fk bigint

	DECLARE cur_v_data CURSOR local for
		SELECT  pa.PRODUCT_ATTRIBUTE_ID,  
				ed.CURRENT_DATA,
				ed.product_structure_fk
		   from epacube.PRODUCT_ATTRIBUTE pa with (nolock)
		   inner join synchronizer.EVENT_DATA ed with (nolock)
		   on (pa.PRODUCT_STRUCTURE_FK = ed.PRODUCT_STRUCTURE_FK
		   and ed.EVENT_STATUS_CR_FK = 81
		   and ed.DATA_NAME_FK = 110100
		   and ed.IMPORT_JOB_FK = @V_OUT_JOB_FK)
		   and pa.DATA_NAME_FK = (select DATA_name_id from epacube.data_name
where Name = 'REPLACING PRODUCT ID')
		
         OPEN cur_v_data;
         FETCH NEXT FROM cur_v_data INTO	@v_Attribute_id,                                           
											@v_current_data,
											@v_product_structure_fk
               
      WHILE @@FETCH_STATUS = 0
      BEGIN

				---remove exsisting prior product ids before creating new ones.
				delete from epacube.PRODUCT_ATTRIBUTE 
				where DATA_NAME_FK = (select DATA_name_id from epacube.data_name
where Name = 'PRIOR PRODUCT ID') 
				 and PRODUCT_STRUCTURE_FK = @v_product_structure_fk
				 
				 

						UPDATE epacube.PRODUCT_ATTRIBUTE 
						set DATA_NAME_FK = (select DATA_name_id from epacube.data_name
where Name = 'PRIOR PRODUCT ID') 
						,ATTRIBUTE_CHAR = @v_current_data
						,ATTRIBUTE_EVENT_DATA = @v_current_data
						where PRODUCT_ATTRIBUTE_ID = @v_Attribute_id
   


 FETCH NEXT FROM cur_v_data Into	@v_Attribute_id,                                           
									@v_current_data,
									@v_product_structure_fk


END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_data
DEALLOCATE cur_v_data    

EXEC common.job_execution_create   @v_job_fk, 'COMPLETED MERGE PRODUCT ID',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @l_sysdate, @l_sysdate  
  


-------------------------------------------------------------------------------------------------------------

  SET @status_desc = 'finished execution of SYNCHRONIZER.epacube.UTL_MERGE_PRODUCT_ID'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.UTL_DELETE_MERGE_PRODUCT_ID has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END














