﻿





-- Copyright 2011
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to create/modify/activate/inactivate Import Exclusions
-- 
-- Validations: A duplicate package/name/value combination may not be created
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        01/09/2011   Initial SQL Version


CREATE PROCEDURE [epacube].[IMPORT_DATA_EXCLUSION_DM] 
( @in_import_package_exclude_id int,
  @in_import_package_fk int,
  @in_exclude_data_name_fk int,
  @in_operator_cr_fk int,
  @in_exclude_value varchar(128),
  @in_ui_action_cr_fk int )
												
AS

/***************  Parameter Defintions  **********************
CURRENTLY USED IN THE DATA MASTER EDITOR....

***************************************************************/

BEGIN

DECLARE @l_sysdate				datetime
DECLARE @ls_stmt				varchar (1000)
DECLARE @l_exec_no				bigint
DECLARE @l_rows_processed		bigint
DECLARE @v_count				bigint

DECLARE @deleteUIActionCodeRef  int
DECLARE @createUIActionCodeRef  int
DECLARE @changeUIActionCodeRef  int
DECLARE @activateUIActionCodeRef  int
DECLARE @inactivateUIActionCodeRef  int


DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int

SET NOCOUNT ON;
SET @l_sysdate = getdate ()
set @ErrorMessage = null
set @deleteUIActionCodeRef = epacube.getCodeRefId('UI_ACTION','DELETE')
set @createUIActionCodeRef = epacube.getCodeRefId('UI_ACTION','CREATE')
set @changeUIActionCodeRef = epacube.getCodeRefId('UI_ACTION','CHANGE')
set @inactivateUIActionCodeRef = epacube.getCodeRefId('UI_ACTION','INACTIVATE')
set @activateUIActionCodeRef = epacube.getCodeRefId('UI_ACTION','ACTIVATE')


-------------------------------------------------------------------------------------
--   Peform UI Delete Action 
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = @deleteUIActionCodeRef
BEGIN
	DELETE FROM IMPORT.IMPORT_PACKAGE_EXCLUDE
	WHERE IMPORT_PACKAGE_EXCLUDE_ID = @in_import_package_exclude_id
	return (0)
END 

-------------------------------------------------------------------------------------
--   Peform UI Activate Action 
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = @activateUIActionCodeRef  
BEGIN

	UPDATE IMPORT.IMPORT_PACKAGE_EXCLUDE
	       set record_status_cr_fk = epacube.getCodeRefId('RECORD_STATUS','ACTIVE')
				 ,update_timestamp = getdate()		   
	       where IMPORT_PACKAGE_EXCLUDE_ID = @in_import_package_exclude_id
		   return (0)
END 

-------------------------------------------------------------------------------------
--   Peform UI Inactivate Action 
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = @inactivateUIActionCodeRef
BEGIN

	UPDATE IMPORT.IMPORT_PACKAGE_EXCLUDE
	       set record_status_cr_fk = epacube.getCodeRefId('RECORD_STATUS','INACTIVE')
			   ,update_timestamp = getdate()		  
	       where IMPORT_PACKAGE_EXCLUDE_ID = @in_import_package_exclude_id
		   return (0)
END -- IF @in_ui_action_cr_fk = 8005
-------------------------------------------------------------------------------------
-- Perform Server-side validation 
-- May not have duplicate Package/DataName/Value combos
-------------------------------------------------------------------------------------
IF @in_ui_action_cr_fk in (@createUIActionCodeRef,@changeUIActionCodeRef)
BEGIN

-------------------------------------------------------------------------------------
--   Check for attempt to create a duplicate
-------------------------------------------------------------------------------------

    select @ErrorMessage = 'Duplicate Import Exclusion; this Package/Field/Value combination already exists. '              	
    from import.import_package_exclude ipe
    where ipe.exclude_value = @in_exclude_value
		  and ipe.exclude_data_name_fk = @in_exclude_data_name_fk
		  and ipe.import_package_fk = @in_import_package_fk		  

			 IF @@rowcount > 0
					GOTO RAISE_ERROR




-------------------------------------------------------------------------------------
-- Perform UI Create Action
-------------------------------------------------------------------------------------


  IF @in_ui_action_cr_fk = @createUIActionCodeRef 
  BEGIN

    INSERT INTO import.import_package_exclude
    (
	 import_package_fk,
     exclude_data_name_fk,
     operator_cr_fk,
     exclude_value,     
     record_status_cr_fk,     
     create_timestamp,	      
     update_timestamp
    )
    SELECT
	@in_import_package_fk,
    @in_exclude_data_name_fk,
    @in_operator_cr_fk,
    @in_exclude_value,
    epacube.getCodeRefId('RECORD_STATUS','ACTIVE'),    
    getdate(),
    getdate()
  END --- End Creation


-------------------------------------------------------------------------------------
--   Perform UI Change Action
-------------------------------------------------------------------------------------
  IF @in_ui_action_cr_fk = @changeUIActionCodeRef 
  BEGIN
	
	UPDATE import.import_package_exclude
		set import_package_fk = @in_import_package_fk
			,exclude_data_name_fk = @in_exclude_data_name_fk
			,exclude_value = @in_exclude_value	
			,operator_cr_fk = @in_operator_cr_fk										
			,update_timestamp = getdate()
		where import_package_exclude_id = @in_import_package_exclude_id


  End  -- End Change Action

End --End Change/Create Actions and validation block





RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END







































