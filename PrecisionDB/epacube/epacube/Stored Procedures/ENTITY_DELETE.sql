﻿
-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Delete entity JIRA (PRE-6)
-- 1. Check to see if the entity is being referenced by other tables
-- 2. If there are no references then delete the entity
-- 3. If there are references then return a message to the application noting the referencing tables
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE   PROCEDURE [epacube].[ENTITY_DELETE]
	@entity_structure_id BIGINT,
	@entity_identification_id BIGINT,
	@entity_ident_nonunique_id BIGINT
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @lookup_count INT;
	DECLARE @i INT = 1;
	DECLARE @statement VARCHAR(1000);
	DECLARE @entity_references VARCHAR(1000);
	DECLARE @return_message VARCHAR(1000);

	SET NOCOUNT ON;
	
	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.ENTITY_DELETE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;
	
	IF OBJECT_ID('tempdb.dbo.#Lookups', 'U') IS NOT NULL
			DROP TABLE #Lookups;
			
	CREATE TABLE #Lookups (
		ID INT IDENTITY(1, 1),
		LOOKUP_STATEMENT VARCHAR(MAX)
	); 

	-- Dynamically create a list of queries to check to see if the data value is in use
	INSERT INTO #Lookups (LOOKUP_STATEMENT)
	SELECT DISTINCT 
		'INSERT INTO #Results (TABLE_NAME) SELECT TOP 1 ''' + Table_Name + ''' TABLE_NAME FROM ' + QTbl + ' WHERE ' + Local_Column + ' = ' + CAST(@entity_ident_nonunique_id AS VARCHAR(255))
	FROM epacube.data_name_group_refs
	WHERE Primary_Column = 'ENTITY_IDENT_NONUNIQUE_FK'
		AND DATA_COLUMN LIKE '%ENTITY_IDENT_NONUNIQUE_FK'
	UNION
	SELECT DISTINCT 
		'INSERT INTO #Results (TABLE_NAME) SELECT TOP 1 ''' + Table_Name + ''' TABLE_NAME FROM ' + QTbl + ' WHERE ' + Local_Column + ' = ' + CAST(@entity_identification_id AS VARCHAR(255))
	FROM epacube.data_name_group_refs
	WHERE Primary_Column = 'ENTITY_IDENTIFICATION_FK'
		AND DATA_COLUMN LIKE '%ENTITY_IDENTIFICATION_FK'
	UNION
	SELECT DISTINCT 
		'INSERT INTO #Results (TABLE_NAME) SELECT TOP 1 ''' + Table_Name + ''' TABLE_NAME FROM ' + QTbl + ' WHERE ' + Local_Column + ' = ' + CAST(@entity_structure_id AS VARCHAR(255))
	FROM epacube.data_name_group_refs
	WHERE Primary_Column = 'ENTITY_STRUCTURE_FK'
		AND DATA_COLUMN LIKE '%ENTITY_STRUCTURE_FK';

	SELECT @lookup_count = COUNT(*)
	FROM #Lookups;

	IF OBJECT_ID('tempdb.dbo.#Results', 'U') IS NOT NULL
			DROP TABLE #Results;
			
	CREATE TABLE #Results (
		TABLE_NAME VARCHAR(64)
	); 

	-- Execute each lookup statement and return the table name if there is a matching data value
	WHILE @i <= @lookup_count
	BEGIN
		SELECT @statement = LOOKUP_STATEMENT
		FROM #Lookups
		WHERE ID = @i;

		EXEC (@statement);
		
		SET @i = @i + 1;
	END

	-- Put all matching tables into a comma delimited list
	SELECT @entity_references =
	STUFF((SELECT ', ' + TABLE_NAME
		FROM #Results
	FOR XML PATH('')), 1, 2, '');

	-- If the data value is not in use then delete
	IF @entity_references IS NULL
	
		BEGIN TRY
			BEGIN TRANSACTION;

				DELETE 
				FROM epacube.ENTITY_IDENT_NONUNIQUE
				WHERE ENTITY_IDENT_NONUNIQUE_ID = @entity_ident_nonunique_id;
				
				DELETE 
				FROM epacube.ENTITY_IDENTIFICATION
				WHERE ENTITY_IDENTIFICATION_ID = @entity_identification_id;
				
				DELETE 
				FROM epacube.ENTITY_STRUCTURE
				WHERE ENTITY_STRUCTURE_ID = @entity_structure_id;

			COMMIT TRANSACTION;

		END TRY

		BEGIN CATCH

			IF (@@TRANCOUNT > 0)
				BEGIN
					ROLLBACK TRANSACTION;
					PRINT 'Error detected, all changes reversed';
				END 
						
			SET @ls_stmt = 'Execution of epacube.ENTITY_DELETE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

			EXEC exec_monitor.Report_Error_sp @l_exec_no;

		END CATCH

	ELSE
		
		SET @return_message = 'Cannot delete ENTITY ' + CAST(@entity_structure_id AS VARCHAR(20)) + ' because it is referenced by table(s) ' + @entity_references;
		
		SELECT @return_message AS RETURN_MESSAGE; 
END