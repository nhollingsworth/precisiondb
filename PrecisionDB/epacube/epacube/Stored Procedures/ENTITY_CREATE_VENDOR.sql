﻿
-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create entities JIRA (URM-1159)
-- 1. Determine if the parent already exists
-- 2. If the parent exists create child record associated to the parent record
-- 3. If the parent does not exist, create the parent record and then create the child record associated to the parent record 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------


CREATE PROCEDURE [epacube].[ENTITY_CREATE_VENDOR] 
	@exec_id BIGINT
	,@criteria AS XML
	,@user VARCHAR(64)
	
AS
BEGIN
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @new_parent_count INT = 0;
	DECLARE @new_child_count INT = 0;
	DECLARE @new_parent_entity_structure_id AS BIGINT;
	DECLARE @new_parent_entity_identification_id AS BIGINT;
	DECLARE @new_child_entity_structure_id BIGINT;
	DECLARE @new_child_entity_identification_id BIGINT;
	DECLARE @i INT = 0;

	SET NOCOUNT ON;

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.ENTITY_CREATE_VENDOR.'
	EXEC exec_monitor.Report_Status_sp @exec_id,
		@ls_stmt;

	--***************************************************************************************************************************************
	--Example vendor entity structure
	--
	-- '<root>
	--		<h1 h1Code="777777" h1Name="ABC Beverage">
	--		</h1>
	--	</root>'
	--
	--***************************************************************************************************************************************	
	
	
	--The vendor entity structure is not currently utilizing a parent/child hierarchy but the associated parent/child code 
	--has been left in place in case it is needed in the future.				
	
	SET @ls_stmt = 'Begin creation of new VENDOR entity.'
	EXEC exec_monitor.Report_Status_sp @exec_id,
		@ls_stmt


	IF OBJECT_ID('tempdb.dbo.#Vendor', 'U') IS NOT NULL
		DROP TABLE #Vendor; 

	SELECT t.c.value('@h1Code', 'VARCHAR(128)') AS h1_code
		, t.c.value('@h1Name', 'VARCHAR(128)') AS h1_name
	INTO #Vendor
	FROM @criteria.nodes('/root[1]/h1') t(c)

	--Check for new parents
	SELECT @new_parent_count = COUNT(DISTINCT h1_code)
	FROM #Vendor c
		LEFT JOIN epacube.ENTITY_IDENTIFICATION ei
			ON c.h1_code = ei.[VALUE]
		LEFT JOIN epacube.ENTITY_STRUCTURE es
			ON ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
				AND es.DATA_NAME_FK LIKE '143%'

	IF @new_parent_count > 0
		BEGIN
			--***************************************************************************************************************************************
			--Begin Parent Block
			
			SET @i = 1;
					
			PRINT '143.101.' + CAST(@i AS VARCHAR(2)) + ' - ' + CAST(@new_parent_count AS VARCHAR(2)) + ' new parent vendor(s) found'

			DECLARE @vendor_parent_code AS VARCHAR(128);
			DECLARE @vendor_parent_name AS VARCHAR(128);
			DECLARE @vendor_parent_cursor AS CURSOR;

			SET @vendor_parent_cursor = CURSOR FOR
			SELECT DISTINCT h1_code, h1_name
			FROM #Vendor;

			OPEN @vendor_parent_cursor;

			FETCH NEXT FROM @vendor_parent_cursor INTO @vendor_parent_code, @vendor_parent_name;

			WHILE @@FETCH_STATUS = 0
				BEGIN
					BEGIN TRY
						BEGIN TRANSACTION;
							--Create the parent entity structure
							INSERT INTO epacube.ENTITY_STRUCTURE (ENTITY_STATUS_CR_FK, DATA_NAME_FK, LEVEL_SEQ, ENTITY_CLASS_CR_FK, RECORD_STATUS_CR_FK, CREATE_USER)
							VALUES (10200, 143000, 1, 10103, 1, @user);

							SET @new_parent_entity_structure_id = SCOPE_IDENTITY();

							PRINT '143.102.' + CAST(@i AS VARCHAR(2)) + ' - New parent entity structure created with ID: ' + CAST(@new_parent_entity_structure_id AS CHAR(20)); 
					
							--Add the parent unique code
							INSERT INTO epacube.ENTITY_IDENTIFICATION (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, [VALUE], RECORD_STATUS_CR_FK, CREATE_USER)
							VALUES (@new_parent_entity_structure_id, 143000, 143111, @vendor_parent_code, 1, @user);

							SET @new_parent_entity_identification_id = SCOPE_IDENTITY();

							PRINT '143.103.' + CAST(@i AS VARCHAR(2)) + ' - New parent unique code added with ID: ' + CAST(@new_parent_entity_identification_id AS CHAR(20));

							--Add the parent non-unique name
							INSERT INTO epacube.ENTITY_IDENT_NONUNIQUE (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, [VALUE], RECORD_STATUS_CR_FK, CREATE_USER)
							VALUES (@new_parent_entity_structure_id, 143000, 143112, @vendor_parent_name, 1, @user);

							SET @new_parent_entity_identification_id = SCOPE_IDENTITY();

							PRINT '143.104.' + CAST(@i AS VARCHAR(2)) + ' - New parent non-unique name added with ID: ' + CAST(@new_parent_entity_identification_id AS CHAR(20));
						
						COMMIT TRANSACTION;

						SET @ls_stmt = 'A new vendor was added with id ENTITY_STRUCTURE_ID ' + CAST(@new_parent_entity_structure_id AS VARCHAR(20)) + '.'; 
						EXEC exec_monitor.Report_Status_sp @exec_id, @ls_stmt;
					
						SET @i = @i + 1;

						FETCH NEXT FROM @vendor_parent_cursor INTO @vendor_parent_code, @vendor_parent_name;
					END TRY

					BEGIN CATCH
						IF (@@TRANCOUNT > 0)
							BEGIN
								ROLLBACK TRANSACTION;
								PRINT 'Error detected, all changes reversed';
							END 
					
						CLOSE @vendor_parent_cursor;
						DEALLOCATE @vendor_parent_cursor;
						
						PRINT 'ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE() 

						SET @ls_stmt = 'Execution of epacube.ENTITY_CREATE_VENDOR has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
						EXEC exec_monitor.Report_Status_sp @exec_id, @ls_stmt;

						EXEC exec_monitor.Report_Error_sp @exec_id;
				
					END CATCH

				END

			SET @ls_stmt = 'Finished execution of epacube.ENTITY_CREATE_VENDOR.';
			EXEC exec_monitor.Report_Status_sp @exec_id, @ls_stmt;

			CLOSE @vendor_parent_cursor;
			DEALLOCATE @vendor_parent_cursor;
				
			--End Parent Block
			--***************************************************************************************************************************************
		END

END
