﻿-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Delete data values JIRA (PRE-7)
-- If data value is in use return message that it cannot be deleted
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE OR ALTER PROCEDURE [epacube].[DATA_VALUE_DELETE]
	@data_value_id BIGINT
AS
BEGIN
	DECLARE @data_value_id_str VARCHAR(255);
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @return_message VARCHAR(1000);

	SET NOCOUNT ON;

	--Convert ID to string for logging
	SET @data_value_id_str = CAST(@data_value_id AS VARCHAR(255));
	
	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.DATA_VALUE_DELETE - Delete DATA_VALUE ' + @data_value_id_str;
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, @exec_id = @l_exec_no OUTPUT;
	
	BEGIN TRY
		BEGIN TRANSACTION;

			DELETE 
			FROM epacube.DATA_VALUE
			WHERE DATA_VALUE_ID = @data_value_id;

		COMMIT TRANSACTION;

		SET @ls_stmt = 'End execution of epacube.DATA_VALUE_DELETE - DATA_VALUE ' + @data_value_id_str + ' successfully deleted';
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

		SET @return_message = 'Successfully deleted DATA_VALUE ' + @data_value_id_str;
		
		SELECT @return_message AS RETURN_MESSAGE; 

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
			END 
						
		SET @ls_stmt = 'Execution of epacube.DATA_VALUE_DELETE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

		SET @return_message = 'Cannot delete DATA_VALUE ' + @data_value_id_str + ' because it is in use';
		
		SELECT @return_message AS RETURN_MESSAGE; 

	END CATCH

END
