﻿






-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG		 04/12/2010  Breaking out from parent procedure
-- RG		 04/23/2010  EPA-2934 unable to reprocess if import data is gone; check cleansed indicator
-- CV        05/26/2010  Added distinct to select and logging
-- KS        06/02/2010  EPA-3011 Removing the the notion of "Completed Jobs" and using instead the criteria below:
--							IMPORT DATA Any row in the Import Tables in which a Stage Record does not exist. 
-- CV        04/11/2011  Add insert of jobs that are orphaned because stage data removed prior
-- CV        05/10/2012  Added tables to the orphaned delete section name value and record

CREATE PROCEDURE [epacube].[PURGE_IMPORT_TABLES] @purge_immediate_ind INT = 0 		
AS BEGIN		
		DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
--------		DECLARE @job_Complete_Status_Code int
--------		DECLARE @importProductsJobClass INT
--------		DECLARE @importProductTaxonomyJobClass INT
--------		DECLARE @importEntityJobClass INT	
        
		DECLARE @grace_days INT         
        DECLARE @purge_date_max DATETIME        
        DECLARE	@epacube_Params_Grace_Period int
		DECLARE @calculated_Grace_Period int
		DECLARE @display_Grace_Period varchar(20)
		DECLARE @appscope INT		

		BEGIN TRY
            SET NOCOUNT ON ;
            SET @l_exec_no = 0	
            SET @l_sysdate = getdate()
			SET @grace_days = 7            

			SET @appscope = (select application_scope_id
							 from epacube.application_scope with (NOLOCK)
							 where scope = 'APPLICATION')
			SET @epacube_Params_Grace_Period = (select CAST ( eep.value as int )
												from epacube.epacube_params eep with (NOLOCK)
												where  eep.name = 'PURGE IMPORT JOBS AFTER'
												and application_scope_fk = @appscope)
			SET @calculated_Grace_Period = CASE @purge_immediate_ind
			                               WHEN 1 THEN 0
			                               ELSE (isnull(@epacube_Params_Grace_Period,@grace_days))
			                               END 						
			SET @purge_date_max = DATEADD(day, -@calculated_Grace_Period, @l_sysdate)
			SET @display_Grace_Period = CAST(ISNULL(@calculated_Grace_Period, 0) AS VARCHAR(20))						

            
--------		SET @job_Complete_Status_Code = epacube.getCodeRefId('JOB_STATUS','COMPLETE')
--------		SET @importProductsJobClass = (select job_class_id from common.job_class
--------											where name = 'IMPORT PRODUCTS')
--------		SET @importProductTaxonomyJobClass = (select job_class_id from common.job_class
--------											where name = 'IMPORT PRODUCT TAXONOMY')
--------		SET @importEntityJobClass = (select job_class_id from common.job_class
--------											where name = 'IMPORT ENTITY')	
			
-------------------------------------------------------------------------

SET @ls_stmt = 'started execution of epacube.PURGE_IMPORT_TABLES. '
                + 'Grace days = '
                + @display_Grace_Period + ' days.'
               
            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;
                
----------------------------------------------------------------------------

--adding logic to deal with EPA-2934 - check to make sure it's cleansed
----------			DECLARE @CompletedCleanserJobs TABLE
----------
----------				(
----------				CLEANSER_JOB_ID bigint
----------				)
----------
----------			INSERT INTO @CompletedCleanserJobs (CLEANSER_JOB_ID)
----------
----------			select distinct search_job_fk from cleanser.search_job_results_best with (NOLOCK)
----------			inner join common.job_execution with (NOLOCK) 
----------on (search_job_fk = job_fk 
----------and job_status_cr_fk = @job_Complete_Status_Code)
----------			where search_job_fk not in 
----------					(select distinct search_job_fk
----------							from cleanser.search_job_results_best with (NOLOCK)
----------							where isnull(cleansed_indicator,0) <> 1)	
----------			
----------			-- Table Variable will be used to reference COMPLETE IMPORT PRODUCT Jobs
----------			
----------			DECLARE @CompletedImportJobs TABLE
----------				(
----------					JobID bigint
----------				)
----------
----------			INSERT INTO @CompletedImportJobs (JobID)
----------
----------					select distinct job_fk 
----------					from common.job_execution with (NOLOCK)
----------					where job_status_cr_fk = @job_Complete_Status_Code
----------					and job_fk in (select job_id from common.job j with (NOLOCK)
----------								   inner join import.import_record WITH (NOLOCK)
----------									on (job_fk = job_id)
----------									and j.job_class_fk in (@importProductsJobClass,@importProductTaxonomyJobClass, @importEntityJobClass ) ) 
----------					and job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)		
----------			
----------			SET @ls_stmt = 'Purging All Import Data from Import Tables related to any Product, Taxonomy, or Entity import jobs with status of COMPLETE '                
----------            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
----------                @exec_id = @l_exec_no OUTPUT ;




--------------------------------------------------------------------------------------------
--  CREATE TEMP TABLE FOR IMPORT ROWS TO BE REMOVED
--------------------------------------------------------------------------------------------
--drop temp table if it exists
	IF object_id('tempdb..#TS_REMOVE_IMPORT_ROWS') is not null
	   drop table #TS_REMOVE_IMPORT_ROWS;

	   
	-- create temp table
	CREATE TABLE #TS_REMOVE_IMPORT_ROWS(
				JOB_FK BIGINT NULL,
				RECORD_NO BIGINT NULL
				)

	INSERT INTO #TS_REMOVE_IMPORT_ROWS
	( JOB_FK, RECORD_NO )
	SELECT IREC.JOB_FK, IREC.RECORD_NO
	FROM import.IMPORT_RECORD IREC WITH (NOLOCK)
	LEFT JOIN stage.STG_RECORD SREC WITH (NOLOCK)
	 ON ( SREC.JOB_FK = IREC.JOB_FK
	 AND  SREC.RECORD_NO = IREC.RECORD_NO )
	WHERE SREC.STG_RECORD_ID IS NULL
	AND   utilities.TRUNC(IREC.effective_date) 
	                <=
		   utilities.TRUNC(@purge_date_max) 



	CREATE NONCLUSTERED INDEX IDX_TSRIR_J_RN ON #TS_REMOVE_IMPORT_ROWS
	(JOB_FK ASC
	,RECORD_NO ASC )
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


--IMPORT RECORD DATA
					DELETE  FROM import.import_record_data
                    WHERE   IMPORT_RECORD_DATA_ID IN (
                               SELECT IMPORT_RECORD_DATA_ID
                               FROM import.IMPORT_RECORD_DATA IRECD WITH (NOLOCK)
                               INNER JOIN #TS_REMOVE_IMPORT_ROWS TSRIR
                                ON ( TSRIR.JOB_FK = IRECD.JOB_FK
                                AND  TSRIR.RECORD_NO = IRECD.RECORD_NO )
							    )

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from import.import_record_data table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

--Delete orphaned Rows because of do not import ind or worked cleanser data

		DELETE 	FROM import.IMPORT_RECORD_data 
				where JOB_FK not in 
				(select distinct job_fk from stage.STG_RECORD SREC WITH (NOLOCK))
				AND   utilities.TRUNC(effective_date) 
	                <= utilities.TRUNC(@purge_date_max) 

        DELETE 	FROM import.IMPORT_RECORD_NAME_VALUE 
				where JOB_FK not in 
				(select distinct job_fk from stage.STG_RECORD SREC WITH (NOLOCK))
				AND   utilities.TRUNC(effective_date) 
	                <= utilities.TRUNC(@purge_date_max) 

        DELETE 	FROM import.IMPORT_RECORD
				where JOB_FK not in 
				(select distinct job_fk from stage.STG_RECORD SREC WITH (NOLOCK))
				AND   utilities.TRUNC(effective_date) 
	                <= utilities.TRUNC(@purge_date_max) 

			SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total Orphaned rows deleted from import.import_record_data table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


--IMPORT RECORD NAME VALUE
					DELETE  FROM import.import_record_name_value
                    WHERE   IMPORT_RECORD_NAME_VALUE_ID IN (
                               SELECT IMPORT_RECORD_NAME_VALUE_ID
                               FROM import.IMPORT_RECORD_NAME_VALUE IRECNV WITH (NOLOCK)
                               INNER JOIN #TS_REMOVE_IMPORT_ROWS TSRIR
                                ON ( TSRIR.JOB_FK = IRECNV.JOB_FK
                                AND  TSRIR.RECORD_NO = IRECNV.RECORD_NO )
							    )							

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from import.import_record_name_value table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END


--IMPORT RECORD
					DELETE  FROM import.import_record
                    WHERE   IMPORT_RECORD_ID IN (
                               SELECT IMPORT_RECORD_ID
                               FROM import.IMPORT_RECORD IREC WITH (NOLOCK)
                               INNER JOIN #TS_REMOVE_IMPORT_ROWS TSRIR
                                ON ( TSRIR.JOB_FK = IREC.JOB_FK
                                AND  TSRIR.RECORD_NO = IREC.RECORD_NO )
							    )

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from import.import_record table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END






/*

KS WILL ADD THESE IN A FUTURE PURGE


---------------start import tables----------------------------------
					
--IMPORT ORDER DATA
					DELETE  FROM import.import_order_data
                    WHERE   job_fk in (select JobID from @CompletedImportJobs ) 
							

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from import.import_order_data table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END  

--IMPORT RULES
					DELETE  FROM import.import_rules
                    WHERE   job_fk in (select JobID from @CompletedImportJobs ) 
							

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from import.import_rules table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
--IMPORT SXE CUSTOMERS
					DELETE  FROM import.IMPORT_SXE_CUSTOMERS
                    WHERE   job_fk in (select JobID from @CompletedImportJobs ) 
							

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from import.sxe_customers table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

--IMPORT SXE PDSC
					DELETE  FROM import.import_sxe_pdsc
                    WHERE   job_fk in (select JobID from @CompletedImportJobs ) 
							

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from import.import_sxe_pdsc table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

--IMPORT SXE PDSR
					DELETE  FROM import.import_sxe_pdsr
                    WHERE   job_fk in (select JobID from @CompletedImportJobs ) 
							

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from import.import_sxe_pdsr table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

--IMPORT SXE SALES TRANSACTIONS
					DELETE  FROM import.import_sxe_sales_transactions
                    WHERE   job_fk in (select JobID from @CompletedImportJobs ) 
							

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from import.import_sxe_sales_transactions table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

--IMPORT TAXONOMY TREE
					DELETE  FROM import.import_taxonomy_tree
                    WHERE   job_fk in (select JobID from @CompletedImportJobs ) 							

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from import.import_taxonomy_tree table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

--IMPORT TAXONOMY TREE ATTRIBUTE
					DELETE  FROM import.import_taxonomy_tree_attribute
                    WHERE   job_fk in (select JobID from @CompletedImportJobs ) 							

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from import.import_taxonomy_tree_attribute table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END		
                        
--IMPORT USERS
					DELETE  FROM import.import_users
                    WHERE   job_fk in (select JobID from @CompletedImportJobs ) 							

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from import.import_taxonomy_tree_attribute table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END	

------  JOB DELETIONS 

--IMPORT DATA EXCEPTIONS
					DELETE  FROM stage.import_data_exceptions
                    WHERE   job_fk in (select JobID from @CompletedImportJobs ) 
							

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from stage.import_data_exceptions table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END           

*/



--drop temp table if it exists
	IF object_id('tempdb..#TS_REMOVE_IMPORT_ROWS') is not null
	   drop table #TS_REMOVE_IMPORT_ROWS;


------------- LOGGING

            SET @status_desc = 'finished execution of epacube.PURGE_IMPORT_TABLES'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;





        END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.PURGE_IMPORT_TABLES has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH
		  
    END





