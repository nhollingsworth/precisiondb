﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Return taxonomy tree information JIRA (URM-1209)
-- Get all taxonomy trees
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[TAXONOMY_TREE_GET_ALL]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.TAXONOMY_GET_TREE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT (
			SELECT TT.TAXONOMY_TREE_ID
				, DV.[VALUE] [NAME]
			FROM epacube.TAXONOMY_TREE TT
				INNER JOIN epacube.DATA_VALUE DV
					ON TT.DATA_VALUE_FK = DV.DATA_VALUE_ID
			WHERE TT.RECORD_STATUS_CR_FK = 1
			FOR JSON PATH) CHILDREN
		FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		
		SET @ls_stmt = 'Finished execution of epacube.TAXONOMY_GET_TREE.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH
		SET @ls_stmt = 'Execution of epacube.TAXONOMY_GET_TREE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END
