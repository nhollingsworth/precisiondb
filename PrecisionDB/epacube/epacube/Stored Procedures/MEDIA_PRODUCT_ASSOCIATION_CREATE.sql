﻿

-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create product media association JIRA (PRE-120)
-- 1. Parse the JSON object and insert the new record into the PRODUCT_MEDIA_ASSOCIATION table
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------


CREATE   PROCEDURE [epacube].[MEDIA_PRODUCT_ASSOCIATION_CREATE] 
	-- Add the parameters for the stored procedure here
	@media_product_association AS NVARCHAR(MAX)
	,@user VARCHAR(64)
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @media_management_fk INT;

	SET NOCOUNT ON;

	--***************************************************************************************************************************************
	--Example data name structure
	--{
	--	"mediaManagementId": 1,
	--	"productStructureId": 1001
	--}
	--***************************************************************************************************************************************

    --Initialize log
	SET @ls_stmt = 'Started execution of epacube.MEDIA_PRODUCT_ASSOCIATION_CREATE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	IF OBJECT_ID('tempdb.dbo.#Media_Product_Association', 'U') IS NOT NULL
	DROP TABLE #Media_Product_Association;

	SELECT *
	INTO #Media_Product_Association
	FROM OPENJSON(@media_product_association)
	WITH (
		MEDIA_MANAGEMENT_FK VARCHAR(6) '$.mediaManagementId',
		PRODUCT_STRUCTURE_FK VARCHAR(64) '$.productStructureId'
	);

	SELECT @media_management_fk = MEDIA_MANAGEMENT_FK FROM #Media_Product_Association

	SET @ls_stmt = 'Begin creation of new media product association.'
	EXEC exec_monitor.Report_Status_sp @l_exec_no,
		@ls_stmt

	BEGIN TRY
		BEGIN TRANSACTION;
			
			--Create new product media association					
			INSERT INTO epacube.PRODUCT_MEDIA_ASSOCIATION (MEDIA_MANAGEMENT_FK, PRODUCT_STRUCTURE_FK, CREATE_USER)
			SELECT tmpa.MEDIA_MANAGEMENT_FK
				, tmpa.PRODUCT_STRUCTURE_FK
				, @user
			FROM #Media_Product_Association tmpa
				LEFT JOIN epacube.PRODUCT_MEDIA_ASSOCIATION pma
					ON tmpa.MEDIA_MANAGEMENT_FK = pma.MEDIA_MANAGEMENT_FK
						AND tmpa.PRODUCT_STRUCTURE_FK = pma.PRODUCT_STRUCTURE_FK
			WHERE pma.PRODUCT_MEDIA_ASSOCIATION_ID IS NULL;

			--Remove unselected media associations
			DELETE
			FROM epacube.PRODUCT_MEDIA_ASSOCIATION
			WHERE MEDIA_MANAGEMENT_FK = @media_management_fk
				AND PRODUCT_STRUCTURE_FK NOT IN (
					SELECT PRODUCT_STRUCTURE_FK
					FROM #Media_Product_Association);

		COMMIT TRANSACTION;

		SET @ls_stmt = 'Finished execution of epacube.MEDIA_PRODUCT_ASSOCIATION_CREATE.'; 
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;
			
	END TRY

	BEGIN CATCH
		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 
						
		SET @ls_stmt = 'Execution of epacube.MEDIA_PRODUCT_ASSOCIATION has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
				
	END CATCH
END