﻿

CREATE PROCEDURE [epacube].[DATA_NAME_GROUP_REFS_FULL_REFRESH]

as 

If Object_ID('tempdb..#ColRels1') is not null 
Drop table #ColRels1
If Object_ID('tempdb..#ColRels2') is not null 
Drop table #ColRels2
If Object_ID('tempdb..#ColRels3') is not null 
Drop table #ColRels3
If Object_ID('tempdb..#ColRels4') is not null 
Drop table #ColRels4
If Object_ID('tempdb..#ColRels4_POS') is not null 
Drop table #ColRels4_POS
If Object_ID('tempdb..#DN') is not null 
Drop table #DN
If Object_ID('tempdb..#POS') is not null 
Drop table #POS

Create Table #ColRels1(Data_Name_FK bigint null, Table_Schema Varchar(64), Table_Name Varchar(64) Null, QTbl Varchar(128), Primary_Column Varchar(64) Null, Local_Column Varchar(64) Null)
Create Table #ColRels2(Table_Schema Varchar(64), Table_Name Varchar(64) Null, QTbl Varchar(128), Data_Name_FK bigint Null, Data_Level_CR_FK int Null, Primary_Column Varchar(64) Null, Local_Column Varchar(64) Null)
Create Table #ColRels3		(Table_Schema Varchar(64), Table_Name Varchar(64) Null, QTbl Varchar(128), Data_Name_FK bigint Null, Data_Level_CR_FK int Null, Primary_Column Varchar(64) Null, Local_Column Varchar(64) Null, Entity_Class_CR_FK Int Null, DATA_COLUMN VARCHAR(Max) nULL, ALIAS VARCHAR(16) NULL, ALIAS_DV VARCHAR(16) NULL, ZONE_TYPE_CR_FK INT NULL, Create_Timestamp datetime Null)
Create Table #ColRels4		(Table_Schema Varchar(64), Table_Name Varchar(64) Null, QTbl Varchar(128), Data_Name_FK bigint Null, Data_Level_CR_FK int Null, Primary_Column Varchar(64) Null, Local_Column Varchar(64) Null, Entity_Class_CR_FK Int Null, DATA_COLUMN VARCHAR(Max) nULL, ALIAS VARCHAR(16) NULL, ALIAS_DV VARCHAR(16) NULL, ZONE_TYPE_CR_FK INT NULL, Create_Timestamp datetime Null)
Create Table #ColRels4_POS	(Table_Schema Varchar(64), Table_Name Varchar(64) Null, QTbl Varchar(128), Data_Name_FK bigint Null, Data_Level_CR_FK int Null, Primary_Column Varchar(64) Null, Local_Column Varchar(64) Null, Entity_Class_CR_FK Int Null, DATA_COLUMN VARCHAR(Max) nULL, ALIAS VARCHAR(16) NULL, ALIAS_DV VARCHAR(16) NULL, ZONE_TYPE_CR_FK INT NULL, Create_Timestamp datetime Null)


Insert into #ColRels1
Select 
Data_Name_FK, Table_Schema, table_name, Q_Tbl, Primary_Column, isnull( [Local Column], Primary_Column) 'Local Column'
from (

Select distinct
DN.Data_Name_ID Data_Name_FK
, Upper(Table_Schema) Table_Schema
, ltrim(rtrim(Upper(isc.table_name))) table_name
, Upper(isc.Table_Schema + '.[' +  isc.table_name + ']') Q_Tbl
, Upper(Case	when isc.column_name like 'ORG_Entity_Structure%' then 'Entity_Structure_FK' 
									when isc.column_name like '%Entity_Structure%' then 'Entity_Structure_FK' 
									when isc.column_name like '%Product_Structure%' then 'Product_Structure_FK' 
									When isc.column_name like '%entity_data_Value_FK' then 'Entity_Data_Value_FK'
									When isc.column_name like '%data_value_fk' then 'Data_Value_FK'
									else isc.column_name end) 'Primary_Column'
, dv.column_name 'Local Column' 
from epacube.data_name dn with (nolock)
inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id 
inner join epacube.data_group dg on dn.data_group_fk = dg.data_group_id 
inner join INFORMATION_SCHEMA.columns ISC with (nolock) on ds.table_name = isc.table_name
left join 
	(

		Select 'Data_Level_CR_FK' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from epacube.data_name dn with (nolock) inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.data_group dg with (nolock) on dn.data_group_fk = dg.data_group_id inner join INFORMATION_SCHEMA.columns ISC with (nolock) on ds.table_name = isc.table_name --and ds.column_name = isc.column_name
		where isc.column_name = 'Data_Level_CR_FK' group by isc.TABLE_NAME, isc.column_name
		Union
			Select 'Zone_Type_CR_FK' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from epacube.data_name dn with (nolock) inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.data_group dg with (nolock) on dn.data_group_fk = dg.data_group_id inner join INFORMATION_SCHEMA.columns ISC with (nolock) on ds.table_name = isc.table_name --and ds.column_name = isc.column_name
			where isc.column_name = 'Zone_Type_CR_FK' group by isc.TABLE_NAME, isc.column_name
			Union
			Select 'Structure' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from  epacube.data_name dn with (nolock) inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.data_group dg with (nolock) on dn.data_group_fk = dg.data_group_id inner join INFORMATION_SCHEMA.columns ISC with (nolock) on ds.table_name = isc.table_name --and ds.column_name = isc.column_name
			where isc.column_name like '%Structure%' group by isc.TABLE_NAME, isc.column_name
		Union
			Select 'Data_Value' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from  epacube.data_name dn with (nolock) inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.data_group dg with (nolock) on dn.data_group_fk = dg.data_group_id inner join INFORMATION_SCHEMA.columns ISC with (nolock) on ds.table_name = isc.table_name --and ds.column_name = isc.column_name
			where isc.column_name like '%Data_Value%' and isc.column_name not in ('data_value_fk', 'entity_data_value_fk') group by isc.TABLE_NAME, isc.column_name

		Union
			Select 'SRP' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from  epacube.data_name dn with (nolock) inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.data_group dg with (nolock) on dn.data_group_fk = dg.data_group_id inner join INFORMATION_SCHEMA.columns ISC with (nolock) on ds.table_name = isc.table_name --and ds.column_name = isc.column_name
			where isc.column_name = 'SRP' group by isc.TABLE_NAME, isc.column_name

		Union
			Select 'Parent' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from  epacube.data_name dn with (nolock) inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.data_group dg with (nolock) on dn.data_group_fk = dg.data_group_id inner join INFORMATION_SCHEMA.columns ISC with (nolock) on ds.table_name = isc.table_name --and ds.column_name = isc.column_name
			where isc.column_name like '%Parent%' group by isc.TABLE_NAME, isc.column_name
		Union
			Select 'Value' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from  epacube.data_name dn with (nolock) inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.data_group dg with (nolock) on dn.data_group_fk = dg.data_group_id inner join INFORMATION_SCHEMA.columns ISC with (nolock) on ds.table_name = isc.table_name --and ds.column_name = isc.column_name
			where (isc.column_name like '%Value%' and isc.column_name not like '%_FK' and isc.column_name not like '%_ID'and isc.column_name not like '%HOST%' and isc.column_name <> 'Calculated_Value')
			or isc.column_name like 'Attribute%' or isc.column_Name = 'description'
			group by isc.TABLE_NAME, isc.column_name
		Union
			Select isc.column_name SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from  epacube.data_name dn with (nolock) inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.data_group dg with (nolock) on dn.data_group_fk = dg.data_group_id inner join INFORMATION_SCHEMA.columns ISC with (nolock) on ds.table_name = isc.table_name --and ds.column_name = isc.column_name
			where dn.data_name_id in (500030)
			group by isc.TABLE_NAME, isc.column_name

	) DV on isc.table_name = dv.table_name and isc.column_name = dv.column_name
	Where isc.column_name not in ('BASIS_POSITION', 'CREATE_TIMESTAMP','CUSTOMER_PRECEDENCE', 'DATA_NAME_FK', 'ENTITY_DATA_NAME_FK', 'DISCOUNT_OPERAND','DISPLAY_SEQ','DSPLAY_SEQ','EFFECTIVE_DATE','END_DATE','ENTITY_CLASS_CR_FK'
	,'ENTITY_STATUS_CR_FK','IMPORT_FILENAME','JOB_FK','LAST_IMPORT_FILENAME', 'LAST_IMPORT_JOB_FK', 'LAST_IMPORT_TIMESTAMP', 'LEVEL_SEQ', 'MINIMUM_QTY', 'PRECEDENCE', 'PRICE_OPERAND', 'PROD_SEGMENT_FK', 'ATTRIBUTE_EVENT_DATA'
	, 'PRODUCT_PRECEDENCE', 'PROMO', 'QUANTITY_MULTIPLE', 'RANK', 'RECORD_NO', 'SETTINGS_POS_PROD_STORE_HEADER_FK', 'SOURCE_LEVEL_SEQ', 'SPC_UOM_FK', 'TAXONOMY_NODE_FK', 'UOM_CODE_FK', 'UPDATE_LOG_FK', 'UPDATE_TIMESTAMP', 'UPDATE_USER')	 
	And isc.table_name not in ('SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES')
	and dn.RECORD_STATUS_CR_FK = 1
group by Data_Name_ID, isc.table_schema, isc.TABLE_NAME, isc.column_name, dv.column_name
) B 

Select v.data_name_fk, count(*) Records into #POS from [epacube].[SETTINGS_POS_PROD_STORE_VALUES] V
group by v.DATA_NAME_FK

Insert into #ColRels1
Select 
Data_Name_FK, Table_Schema, table_name, Q_Tbl, Primary_Column, isnull( [Local Column], Primary_Column) 'Local Column'
from (
Select distinct
DN.Data_Name_ID Data_Name_FK
, Upper(Table_Schema) Table_Schema
, ltrim(rtrim(Upper(isc.table_name))) table_name
, Upper(isc.Table_Schema + '.[' +  isc.table_name + ']') Q_Tbl
, Upper(Case	when isc.column_name like 'ORG_Entity_Structure%' then 'Entity_Structure_FK' 
									when isc.column_name like '%Entity_Structure%' then 'Entity_Structure_FK' 
									when isc.column_name like '%Product_Structure%' then 'Product_Structure_FK' 
									When isc.column_name like '%prod_data_Value_FK' then 'Data_Value_FK'
									When isc.column_name like '%entity_data_Value_FK' then 'Entity_Data_Value_FK'
									When isc.column_name like '%data_value_fk' then 'Data_Value_FK'
									When isc.column_name like '%DEPT_ENTITY_DATA_VALUE_FK' then 'Entity_Data_Value_FK'
									else isc.column_name end) 'Primary_Column'
, dv.column_name 'Local Column' 
from epacube.data_name dn with (nolock)
inner join #POS dg on dn.data_name_id = dg.data_name_fk
inner join INFORMATION_SCHEMA.columns ISC with (nolock) on isc.table_name in ('SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES')
left join 
	(Select 'Data_Level_CR_FK' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from epacube.data_name dn with (nolock) inner join #POS POS with (nolock) on dn.data_name_id = POS.DATA_NAME_FK inner join INFORMATION_SCHEMA.columns ISC with (nolock) on isc.table_name in ('SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES')
	where isc.column_name = 'Data_Level_CR_FK' group by isc.TABLE_NAME, isc.column_name
	Union
		Select 'Zone_Type_CR_FK' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from epacube.data_name dn with (nolock) inner join #POS POS with (nolock) on dn.data_name_id = POS.DATA_NAME_FK inner join INFORMATION_SCHEMA.columns ISC with (nolock) on isc.table_name in ('SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES')
		where isc.column_name = 'Zone_Type_CR_FK' group by isc.TABLE_NAME, isc.column_name
		Union
		Select 'Structure' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from epacube.data_name dn with (nolock) inner join #POS POS with (nolock) on dn.data_name_id = POS.DATA_NAME_FK inner join INFORMATION_SCHEMA.columns ISC with (nolock) on isc.table_name in ('SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES')
		where isc.column_name like '%Structure%' group by isc.TABLE_NAME, isc.column_name
	Union
		Select 'Data_Value' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from epacube.data_name dn with (nolock) inner join #POS POS with (nolock) on dn.data_name_id = POS.DATA_NAME_FK inner join INFORMATION_SCHEMA.columns ISC with (nolock) on isc.table_name in ('SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES')
		where isc.column_name like '%Data_Value%' and isc.column_name not in ('data_value_fk', 'entity_data_value_fk') group by isc.TABLE_NAME, isc.column_name
	Union
		Select 'Parent' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from epacube.data_name dn with (nolock) inner join #POS POS with (nolock) on dn.data_name_id = POS.DATA_NAME_FK inner join INFORMATION_SCHEMA.columns ISC with (nolock) on isc.table_name in ('SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES')
		where isc.column_name like '%Parent%' group by isc.TABLE_NAME, isc.column_name
	Union
		Select 'Value' SearchString, Upper(isc.table_name) table_name, Upper(isc.column_name) Column_Name from epacube.data_name dn with (nolock) inner join #POS POS with (nolock) on dn.data_name_id = POS.DATA_NAME_FK inner join INFORMATION_SCHEMA.columns ISC with (nolock) on isc.table_name in ('SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES')
		where (isc.column_name like '%Value%' and isc.column_name not like '%_FK' and isc.column_name not like '%_ID'and isc.column_name not like '%HOST%' and isc.column_name <> 'Calculated_Value')
		or isc.column_name like 'Attribute%' or isc.column_Name = 'description'
		group by isc.TABLE_NAME, isc.column_name

	) DV on isc.table_name = dv.table_name and isc.column_name = dv.column_name
	Where isc.column_name not in ('BASIS_POSITION', 'CREATE_TIMESTAMP','CUSTOMER_PRECEDENCE', 'DATA_NAME_FK', 'ENTITY_DATA_NAME_FK', 'DISCOUNT_OPERAND','DISPLAY_SEQ','DSPLAY_SEQ','EFFECTIVE_DATE','END_DATE','ENTITY_CLASS_CR_FK'
	,'ENTITY_STATUS_CR_FK','IMPORT_FILENAME','JOB_FK','LAST_IMPORT_FILENAME', 'LAST_IMPORT_JOB_FK', 'LAST_IMPORT_TIMESTAMP', 'LEVEL_SEQ', 'MINIMUM_QTY', 'PRECEDENCE', 'PRICE_OPERAND', 'PROD_SEGMENT_FK'
	, 'PRODUCT_PRECEDENCE', 'PROMO', 'QUANTITY_MULTIPLE', 'RANK', 'RECORD_NO', 'SETTINGS_POS_PROD_STORE_HEADER_FK', 'SOURCE_LEVEL_SEQ', 'SPC_UOM_FK', 'TAXONOMY_NODE_FK', 'UOM_CODE_FK', 'UPDATE_LOG_FK', 'UPDATE_TIMESTAMP', 'UPDATE_USER'
	, 'DATA_LEVEL', 'RECORD_STATUS_CR_FK', 'SETTINGS_POS_PROD_STORE_VALUES_ID', 'SETTINGS_POS_PROD_STORE_HEADER_ID')
	and dn.RECORD_STATUS_CR_FK = 1	 
group by Data_Name_ID, isc.table_schema, isc.TABLE_NAME, isc.column_name, dv.column_name
) B

	Declare @DN_FK Varchar(16), @Schema varchar(64), @Tbl Varchar(128), @QTbl Varchar(128), @PrimCol varchar(64), @LclCol varchar(64), @Recs int, @SQL varchar(Max)

	DECLARE GetDataLevels2 Cursor local for
	Select distinct * from #ColRels1 where table_name in (select table_name from #ColRels1 where local_column = 'data_level_cr_fk')	and table_name not in ('SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES', 'SEGMENTS_COMPETITOR')

	Set @SQL = ''

	OPEN GetDataLevels2;
		FETCH NEXT FROM GetDataLevels2 INTO @DN_FK, @Schema, @Tbl, @QTbl, @PrimCol, @LclCol
		WHILE @@FETCH_STATUS = 0
		Begin

		Set @SQL = 'Insert into #ColRels2 Select distinct ''' + @Schema + ''' ''Table_Schema'', ''' + @Tbl + ''' ''Table_Name'','' '   + @QTbl + ''' ''Q_Tbl'', Data_Name_FK, Data_Level_CR_FK ''Data_Level_CR_FK'', ''' 
				+ @PrimCol + ''' ''Primary_Column'', ''' + isnull(@LclCol, @PrimCol) + ''' ''Local_Column'' from ' + @QTbl + ' with (nolock) where data_name_fk = ' + @DN_FK
		
	Exec(@SQL)
		FETCH NEXT FROM GetDataLevels2 INTO @DN_FK, @Schema, @Tbl, @QTbl, @PrimCol, @LclCol
		End
	Close GetDataLevels2;
	Deallocate GetDataLevels2;

	Declare @3DN_FK Varchar(16), @3Schema varchar(64), @3Tbl Varchar(128), @3QTbl Varchar(128), @3PrimCol varchar(64), @3LclCol varchar(64), @3Recs int, @3DL_CR_FK varchar(16), @3SQL varchar(Max), @ind_Zone_Type int

	DECLARE GetDataLevels3 Cursor local for
	Select distinct CR1.*, CR2.Data_Level_CR_FK 
	, Case (Select count(*) 
	from #ColRels1 CR1a 
	inner join #ColRels2 CR2a on CR1a.Data_Name_FK = CR2a.Data_Name_FK 
	where CR1a.Primary_Column = 'Zone_Type_CR_FK' and CR1a.qtbl = cr1.qtbl and cr1a.data_name_fk = cr1.data_name_fk) when 0 then 0 else 1 end 'ind_Zone_Type'
	from #ColRels1 CR1 inner join #ColRels2 CR2 on CR1.Data_Name_FK = CR2.Data_Name_FK where CR1.Primary_Column <> 'DATA_LEVEL_CR_FK'
	and CR1.Primary_Column <> 'DATA_LEVEL' and cr1.Table_Name not in ('SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES')
			
	Set @3SQL = ''

	OPEN GetDataLevels3;
		FETCH NEXT FROM GetDataLevels3 INTO @3DN_FK, @3Schema, @3Tbl, @3QTbl, @3PrimCol, @3LclCol, @3DL_CR_FK, @ind_Zone_Type
		WHILE @@FETCH_STATUS = 0
		Begin
	
	Set @3SQL = 
	Case when @3Tbl = 'SEGMENTS_CONTACTS' then
	'Insert into #ColRels3 Select top 1 ''' + @3Schema + ''' ''Table_Schema'', ''' + @3Tbl + ''' ''Table_Name'','' '   + @3QTbl + ''' ''Q_Tbl'', Data_Name_FK, Data_Level_CR_FK ''Data_Level_CR_FK''
		, ''' + @3PrimCol + ''' ''Primary_Column'', ''' + @3LclCol + ''' ''Local_Column'', Entity_Class_CR_FK, NULL DATA_COLUMN, NULL Alias, NULL ALIAS_DV, Null Zone_Type_CR_FK, getdate() CREATE_TIMESTAMP
		from ' + @3QTbl + ' with (nolock) where data_name_fk = ' + @3DN_FK + ' and Data_Level_CR_FK = ' + @3DL_CR_FK + '
		and [' + Isnull(@3LclCol, @3PrimCol) + '] is not null order by data_name_fk'

	When @ind_Zone_Type = 1 then
	'Insert into #ColRels3 Select top 1 ''' + @3Schema + ''' ''Table_Schema'', ''' + @3Tbl + ''' ''Table_Name'','' '   + @3QTbl + ''' ''Q_Tbl'', Data_Name_FK, Data_Level_CR_FK ''Data_Level_CR_FK''
		, ''' + @3PrimCol + ''' ''Primary_Column'', ''' + @3LclCol + ''' ''Local_Column'', NULL Entity_Class_CR_FK, NULL DATA_COLUMN, NULL Alias, NULL ALIAS_DV, Zone_Type_CR_FK, getdate() CREATE_TIMESTAMP
		from ' + @3QTbl + ' with (nolock) where data_name_fk = ' + @3DN_FK + ' and Data_Level_CR_FK = ' + @3DL_CR_FK + '
		and [' + Isnull(@3LclCol, @3PrimCol) + '] is not null order by data_name_fk'
	Else
	'Insert into #ColRels3 Select top 1 ''' + @3Schema + ''' ''Table_Schema'', ''' + @3Tbl + ''' ''Table_Name'','' '   + @3QTbl + ''' ''Q_Tbl'', Data_Name_FK, Data_Level_CR_FK ''Data_Level_CR_FK''
		, ''' + @3PrimCol + ''' ''Primary_Column'', ''' + @3LclCol + ''' ''Local_Column'', NULL Entity_Class_CR_FK, NULL DATA_COLUMN, NULL Alias, NULL ALIAS_DV, Null Zone_Type_CR_FK, getdate() CREATE_TIMESTAMP 
		from ' + @3QTbl + ' with (nolock) where data_name_fk = ' + @3DN_FK + ' and Data_Level_CR_FK = ' + @3DL_CR_FK + '
		and [' + Isnull(@3LclCol, @3PrimCol) + '] is not null order by data_name_fk'
	End
	Exec(@3SQL)
		FETCH NEXT FROM GetDataLevels3 INTO @3DN_FK, @3Schema, @3Tbl, @3QTbl, @3PrimCol, @3LclCol, @3DL_CR_FK, @ind_Zone_Type
		End
	Close GetDataLevels3;
	Deallocate GetDataLevels3;

	Declare @4DN_FK Varchar(16), @4Schema varchar(64), @4Tbl Varchar(128), @4QTbl Varchar(128), @4PrimCol varchar(64), @4LclCol varchar(64), @4Recs int, @4SQL varchar(Max)

	DECLARE GetDataLevels4 Cursor local for

	Select distinct * from #ColRels1 
	--where table_name in ('rules_sales_programs')
	where	(
			((Table_Name not in (Select distinct Table_Name from #ColRels2) and Table_Name not in ('SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES', 'PRODUCT_RECIPE')
			and primary_column not like '%_ID'))  or (Table_Name = 'Product_Recipe' and Primary_column = 'product_recipe_id')
			)
	or data_name_fk in (502046, 500015, 500019)

	Set @4SQL = ''

	OPEN GetDataLevels4;
		FETCH NEXT FROM GetDataLevels4 INTO @4DN_FK, @4Schema, @4Tbl, @4QTbl, @4PrimCol, @4LclCol
		WHILE @@FETCH_STATUS = 0
		Begin

		Set @4SQL = 'Insert into #ColRels4 Select top 1 ''' + @4Schema + ''' ''Table_Schema'', ''' + @4Tbl + ''' ''Table_Name'','' '   + @4QTbl + ''' ''Q_Tbl'', Data_Name_FK, NULL ''Data_Level_CR_FK''
		, ''' + @4PrimCol + ''' ''Primary_Column'', ''' + Isnull(@4LclCol, @4PrimCol) + ''' ''Local_Column'', NULL Entity_Class_CR_FK, NULL DATA_COLUMN, NULL Alias, NULL ALIAS_DV, Null Zone_Type_CR_FK, getdate() CREATE_TIMESTAMP 
		from ' + @4QTbl + ' with (nolock) where data_name_fk = ' + @4DN_FK + ' And [' + Isnull(@4LclCol, @4PrimCol) + '] is not null'

	Exec(@4SQL)
		FETCH NEXT FROM GetDataLevels4 INTO @4DN_FK, @4Schema, @4Tbl, @4QTbl, @4PrimCol, @4LclCol
		End
	Close GetDataLevels4;
	Deallocate GetDataLevels4;

	Declare @4_POS_DN_FK Varchar(16), @4_POS_Schema varchar(64), @4_POS_Tbl Varchar(128), @4_POS_QTbl Varchar(128), @4_POS_PrimCol varchar(64), @4_POS_LclCol varchar(64), @4_POS_Recs int, @4_POS_SQL varchar(Max)

	DECLARE GetDataLevels4_POS Cursor local for
	Select distinct * from #ColRels1 where Table_Name not in (Select distinct Table_Name from #ColRels2) and Table_Name in ('SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES')

	Set @4_POS_SQL = ''

	OPEN GetDataLevels4_POS;
		FETCH NEXT FROM GetDataLevels4_POS INTO @4_POS_DN_FK, @4_POS_Schema, @4_POS_Tbl, @4_POS_QTbl, @4_POS_PrimCol, @4_POS_LclCol
		WHILE @@FETCH_STATUS = 0
		Begin

		Set @4_POS_SQL = 'Insert into #ColRels4_POS Select ''' + @4_POS_Schema + ''' ''Table_Schema'', ''' + Replace(@4_POS_Tbl, 'VALUES', 'HEADER') + ''' ''Table_Name'','' '   + REPLACE(@4_POS_QTbl, 'VALUES', 'HEADER') + ''' ''Q_Tbl'', Data_Name_FK, Data_Level_CR_FK ''Data_Level_CR_FK''
		, ''' + @4_POS_PrimCol + ''' ''Primary_Column'', ''' + Isnull(@4_POS_LclCol, @4_POS_PrimCol) + ''' ''Local_Column'', NULL Entity_Class_CR_FK, NULL DATA_COLUMN, NULL Alias, NULL ALIAS_DV, Null Zone_Type_CR_FK, getdate() CREATE_TIMESTAMP 
		from 
		(Select top 1 
			SETTINGS_POS_PROD_STORE_HEADER_ID, DATA_LEVEL_CR_FK, DEPT_ENTITY_DATA_VALUE_FK, PROD_DATA_VALUE_FK, PRODUCT_STRUCTURE_FK, CUST_ENTITY_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK
			, RESULT_DATA_NAME_FK, DATA_NAME_FK, ATTRIBUTE_CHAR, ATTRIBUTE_NUMBER, ATTRIBUTE_DATE, ATTRIBUTE_Y_N, DATA_VALUE_FK, ENTITY_DATA_VALUE_FK, ATTRIBUTE_EVENT_DATA, PRECEDENCE
			, sppsv.create_user, sppsv.update_user
			from [epacube].[SETTINGS_POS_PROD_STORE_HEADER] sppsh with (nolock)
			inner join [epacube].[SETTINGS_POS_PROD_STORE_VALUES] sppsv with (nolock) on sppsh.[SETTINGS_POS_PROD_STORE_HEADER_ID] = sppsv.[SETTINGS_POS_PROD_STORE_HEADER_FK]
			and sppsv.data_name_fk = ' + @4_POS_DN_FK + '
			) A
		where [' + @4_POS_LclCol + '] is not null' 

	Exec(@4_POS_SQL)
		FETCH NEXT FROM GetDataLevels4_POS INTO @4_POS_DN_FK, @4_POS_Schema, @4_POS_Tbl, @4_POS_QTbl, @4_POS_PrimCol, @4_POS_LclCol
		End
	Close GetDataLevels4_POS;
	Deallocate GetDataLevels4_POS;

-----***-----

IF OBJECT_ID('TEMPDB..#ColRels') IS NOT NULL
drop table #ColRels
Select * into #ColRels from
(Select * from #ColRels3 Union
Select * from #ColRels4 Union
Select * from #ColRels4_POS) a

Alter Table #ColRels Add [DN_Name] Varchar(96) Null
Alter Table #ColRels Add [DN_Label] Varchar(64) Null

Update C
Set DN_Name = dn.name
, DN_Label = dn.label
from #ColRels C
inner join epacube.data_name dn on c.data_name_fk = dn.data_name_id

Update C
Set QTbl = ltrim(rtrim(QTbl))
from #ColRels C

Update C
Set entity_class_cr_fk = ds.entity_class_cr_fk 
from #ColRels C
inner join epacube.data_name dn on c.data_name_fk = dn.data_name_id
inner join epacube.data_set ds on dn.data_set_fk = ds.DATA_SET_ID
where c.Entity_Class_CR_FK is null

--Update C
--Set Data_Column = Case 
--					When Replace(c.table_name, 'SETTINGS_POS_PROD_STORE_HEADER', 'SETTINGS_POS_PROD_STORE_VALUES') in (Select Table_Name from epacube.INFORMATION_SCHEMA.columns where Column_Name = 'Attribute_Event_Data' group by table_name) then 'Attribute_Event_Data'
--					Else c.local_column end
--from #ColRels C
--inner join epacube.data_name dn on c.data_name_fk = dn.data_name_id
--inner join epacube.data_set ds on dn.data_set_fk = ds.DATA_SET_ID
--where (c.primary_column = ds.COLUMN_NAME or c.Local_Column = ds.COLUMN_NAME) --and c.table_name = 'SETTINGS_POS_PROD_STORE_HEADER'

Update C
Set Data_Column = case	when c.QTbl in ('EPACUBE.[SETTINGS_POS_PROD_STORE_HEADER]') and c.local_column <> c.Primary_column then Null 
						when c.QTbl in ('EPACUBE.[SETTINGS_POS_PROD_STORE_HEADER]') and c.local_column = 'DATA_VALUE_FK' then 'ATTRIBUTE_EVENT_DATA'
						--when c.qtbl in ('EPACUBE.[SETTINGS_POS_STORE]', 'EPACUBE.[PRODUCT_ATTRIBUTE]') and c.local_column not like '%Attribute%' then 'ATTRIBUTE_EVENT_DATA'
						------when c.table_name in (select table_name from epacube.INFORMATION_SCHEMA.columns where column_name = 'Attribute_Event_Data') and c.local_column not like '%Attribute%' then 'ATTRIBUTE_EVENT_DATA'
						else c.local_column end
from #ColRels C
inner join epacube.data_name dn on c.data_name_fk = dn.data_name_id
inner join epacube.data_set ds on dn.data_set_fk = ds.DATA_SET_ID
where (c.primary_column = ds.COLUMN_NAME or c.Local_Column = ds.COLUMN_NAME) --and c.table_name = 'SETTINGS_POS_PROD_STORE_HEADER'

--Select 
--case	when c.QTbl in ('EPACUBE.[SETTINGS_POS_PROD_STORE_HEADER]') and c.local_column <> c.Primary_column then Null 
--						when c.QTbl in ('EPACUBE.[SETTINGS_POS_PROD_STORE_HEADER]') and c.local_column = 'DATA_VALUE_FK' then 'ATTRIBUTE_EVENT_DATA'
--						when c.qtbl = 'EPACUBE.[SETTINGS_POS_STORE]' and c.local_column not like '%Attribute%' then 'ATTRIBUTE_EVENT_DATA'
--						else c.local_column end Data_Col
--, 
--select 
--c.*
--from #ColRels C
--inner join epacube.data_name dn on c.data_name_fk = dn.data_name_id
--inner join epacube.data_set ds on dn.data_set_fk = ds.DATA_SET_ID
--where (c.primary_column = ds.COLUMN_NAME or c.Local_Column = ds.COLUMN_NAME)
--and 1 = 1
----and Data_Level_CR_FK = 509
--and Data_Name_FK = 141060 
--and c.table_name = 'SETTINGS_POS_STORE'
--and Data_Column is not null
----and table_name like '%SETTINGS_POS_PROD_STORE%'

--Select * from #ColRels where data_name_fk = 141060
--select * from epacube.data_name where data_name_id = 141060
--select * from epacube.data_set where data_set_id = 44921
--Select * from epacube.data_set where data_set_id between 44920 and 44930
--Update DN set data_set_fk = 44926 from epacube.data_name DN where data_name_id = 141060

--Select * from epacube.DATA_NAME_GROUP_REFS where table_name = 'SETTINGS_POS_STORE'
--and data_name_fk not in 
--(select data_name_fk
--from #ColRels C
--inner join epacube.data_name dn on c.data_name_fk = dn.data_name_id
--inner join epacube.data_set ds on dn.data_set_fk = ds.DATA_SET_ID
--where (c.primary_column = ds.COLUMN_NAME or c.Local_Column = ds.COLUMN_NAME)
--and 1 = 1
--and c.table_name = 'SETTINGS_POS_STORE'
--and Data_Column is not null)


--Update C
--Set Data_Column = 'ATTRIBUTE_EVENT_DATA'
--from #ColRels C
--where data_name_fk not in (select data_name_fk from #ColRels where data_column is not null)
--and table_name like '%SETTINGS_POS_PROD_STORE%' and local_column in ('ATTRIBUTE_EVENT_DATA')

--Update C
--Set Data_Column = 'ATTRIBUTE_EVENT_DATA'
--from #ColRels C
--where data_name_fk not in (select data_name_fk from #ColRels where data_column is not null)
--and table_name = 'SETTINGS_POS_STORE' and local_column like '%data_value%' 

Update C
Set Data_Column = 'ENTITY_DATA_VALUE_FK'
from #ColRels C
where (data_name_fk = 141124 and local_column = 'value') or (data_name_fk = 502049 and local_column = 'ENTITY_DATA_VALUE_FK')

Update C
Set Data_Column = 'PRODUCT_ASSOCIATION_ID'
from #ColRels C where table_name = 'PRODUCT_ASSOCIATION' and Local_Column = 'PRODUCT_STRUCTURE_FK'

Update C
Set Data_Level_CR_FK = 510
FROM #ColRels C 
where 1 = 1
and table_name = 'Product_attribute'

	Declare @TN Varchar(64), @Alias varchar(16), @DN varchar(16), @SQL_TA varchar(Max), @POS int

	DECLARE TblAlias Cursor local for
	Select distinct ltrim(rtrim(table_name)) from #ColRels

	Set @SQL_TA = ''

	OPEN TblAlias;
		FETCH NEXT FROM TblAlias INTO @TN
		WHILE @@FETCH_STATUS = 0
		Begin
			Set @POS = 0
			Set @Alias = ''
			Set @Alias = Left(@TN, 1)
				While charindex('_', @TN, @POS) <> 0
				Begin

					Set @POS = charindex('_', @TN, @POS) + 1
					Set @Alias = @Alias + Substring(@TN, @POS , 1)
					
				End

		Update R
		Set Alias = @Alias + '_' + Cast(R.data_name_fk as varchar(16))
		from #ColRels R where R.Table_Name = @TN and data_column is not null

		FETCH NEXT FROM TblAlias INTO @TN
		End
	Close TblAlias;
	Deallocate TblAlias;

	Declare @Data_Column1 Varchar(64), @AliasDV1 varchar(16), @DN1 varchar(16), @SQL_TA1 varchar(Max), @POS1 int, @Data_Column_Local1 Varchar(64)

	DECLARE TblAliasDV Cursor local for
	Select distinct Data_Name_FK, ltrim(rtrim(Replace(Data_Column, '_FK', ''))), Data_Column from #ColRels where Data_Column like '%Data_Value_FK%'

	Set @SQL_TA1 = ''

	OPEN TblAliasDV;
		FETCH NEXT FROM TblAliasDV INTO @DN1, @Data_Column1, @Data_Column_Local1
		WHILE @@FETCH_STATUS = 0
		Begin
			Set @POS1 = 0
			Set @AliasDV1 = ''
			Set @AliasDV1 = Left(@Data_Column1, 1)
				While charindex('_', @Data_Column1, @POS1) <> 0
				Begin

					Set @POS1 = charindex('_', @Data_Column1, @POS1) + 1
					Set @AliasDV1 = @AliasDV1 + Substring(@Data_Column1, @POS1 , 1)
				End

		Update R
		Set Alias_DV = @AliasDV1 + '_' + Cast(R.data_name_fk as varchar(16))
		from #ColRels R where R.Data_Name_fk = @DN1 and R.Data_Column = @Data_Column_Local1

		FETCH NEXT FROM TblAliasDV INTO @DN1, @Data_Column1, @Data_Column_Local1
		End
	Close TblAliasDV;
	Deallocate TblAliasDV;

Update C
Set Alias = Case when data_name_fk = 141124 then 'EDV_' else 'SS_' end + Cast(Data_Name_FK as varchar(16))
, Alias_DV = 'EDV_' + Cast(Data_Name_FK as varchar(16))
, Data_Column = 'ENTITY_DATA_VALUE_FK'
from #ColRels C where data_name_fk in (141124, 141130, 502049) and data_column is not null

delete from #ColRels where data_name_fk = 144010
Insert into #ColRels 
(Table_Schema, Table_Name, QTbl, Data_Name_FK, Data_Level_CR_FK, Primary_Column, Local_Column, Entity_Class_CR_FK, Create_Timestamp, Alias, Data_Column, DN_Label, DN_Name)
Select 'EPACUBE', 'ENTITY_STRUCTURE', 'EPACUBE.[ENTITY_STRUCTURE]', 144010, NULL, 'VALUE', 'VALUE', 10104, GETDATE(), 'EIC_P', 'VALUE', EPACUBE.getDataNameLabel(144010), EPACUBE.getDataName(144010) UNION
Select 'EPACUBE', 'ENTITY_STRUCTURE', 'EPACUBE.[ENTITY_STRUCTURE]', 144010, NULL, 'ENTITY_STRUCTURE_FK', 'ENTITY_STRUCTURE_FK', 10104, GETDATE(), Null, Null, EPACUBE.getDataNameLabel(144010), EPACUBE.getDataName(144010)

delete from #ColRels where data_name_fk = 144020
Insert into #ColRels 
(Table_Schema, Table_Name, QTbl, Data_Name_FK, Data_Level_CR_FK, Primary_Column, Local_Column, Entity_Class_CR_FK, Create_Timestamp, Alias, Data_Column, DN_Label, DN_Name)
Select 'EPACUBE', 'ENTITY_IDENTIFICATION', 'EPACUBE.[ENTITY_IDENTIFICATION]', 144020, NULL, 'VALUE', 'VALUE', 10104, GETDATE(), 'EIC', 'VALUE', EPACUBE.getDataNameLabel(144020), EPACUBE.getDataName(144020) Union
Select 'EPACUBE', 'ENTITY_IDENTIFICATION', 'EPACUBE.[ENTITY_IDENTIFICATION]', 144020, NULL, 'ENTITY_STRUCTURE_FK', 'ENTITY_STRUCTURE_FK', 10104, GETDATE(), Null, Null, EPACUBE.getDataNameLabel(144020), EPACUBE.getDataName(144020)

Alter table #ColRels Add EVENT_TYPE_CR_FK INT Null
Alter table #ColRels Add Entity_Structure_EC_CR_FK bigint null
Alter table #ColRels Add Product_Structure_Column varchar(64) Null
Alter table #ColRels Add Org_Structure_Column varchar(64) Null
Alter table #ColRels Add Customer_Structure_Column varchar(64) Null
Alter table #ColRels Add Vendor_Structure_Column varchar(64) Null
Alter table #ColRels Add Zone_Structure_Column varchar(64) Null
Alter table #ColRels Add [ind_PS_FK] int null
Alter table #ColRels Add [ind_ES_FK] int null
Alter table #ColRels Add [ind_OES_FK] int null
Alter table #ColRels Add [ind_CES_FK] int null
Alter table #ColRels Add [ind_VES_FK] int null
Alter table #ColRels Add [ind_ZES_FK] int null
Alter table #ColRels Add [ind_COMP_ES_FK] int null
Alter table #ColRels Add [ind_ASSOC_ES_FK] int null
Alter table #ColRels Add [ind_DL_CR_FK] int null
Alter table #ColRels Add [ind_PROD_DV] int null
Alter table #ColRels Add [ind_CUST_DV] int null
Alter table #ColRels Add [ind_DEPT_EDV] int null
Alter table #ColRels Add [ind_DV_LOOKUP] int null
Alter table #ColRels Add [ind_EDV_LOOKUP] int null
Alter table #ColRels Add ind_REC_INGREDIENT_XREF INT Null

Alter table #ColRels Add [ind_CHILD_PS_FK] int null
Alter table #ColRels Add [ind_PARENT_ES_FK] int null

Alter table #ColRels Add [JoinSQL] Varchar(Max) null
Alter table #ColRels Add [VirtualActionType] Varchar(64) null
Alter table #ColRels Add [VirtualActionType_CR_FK] Int null
Alter table #ColRels Add [VirtualColumnCheck] Varchar(Max) null
Alter table #ColRels Add [V_Ref_Data_Name_FK] Varchar(64) null
Alter table #ColRels Add [NullColumn] int null
Alter table #ColRels Add [Precedence] int null

Update C
Set ind_PS_FK = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'Product_Structure_FK') > 0 then 1 else 0 end
, [ind_ES_FK] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'Entity_Structure_FK') > 0 then 1 else 0 end
, [ind_OES_FK] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'Org_Entity_Structure_FK') > 0 then 1 else 0 end
, [ind_CES_FK] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'Cust_Entity_Structure_FK') > 0 then 1 else 0 end
, [ind_VES_FK] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'Vendor_Entity_Structure_FK') > 0 then 1 else 0 end
, [ind_ZES_FK] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'Zone_Entity_Structure_FK') > 0 
	or c.Data_Name_FK = 159450 then 1 else 0 end
, [ind_COMP_ES_FK] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'Comp_Entity_Structure_FK') > 0 then 1 else 0 end
, [ind_ASSOC_ES_FK] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'Assoc_Entity_Structure_FK') > 0 then 1 else 0 end
, [ind_DL_CR_FK] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and isnull(C1.data_level_cr_fk, 0) <> 0) > 0 then 1 else 0 end
, [ind_PROD_DV] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'Prod_Data_Value_FK') > 0 then 1 else 0 end
, [ind_CUST_DV] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'Cust_Data_Value_FK') > 0 then 1 else 0 end
, [ind_DEPT_EDV] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'Dept_Entity_Data_Value_FK') > 0 then 1 else 0 end
, [ind_DV_LOOKUP] = Case when (select count(*) from #ColRels C1 inner join epacube.data_name DN with (nolock) on C1.data_Name_FK = DN.data_name_id inner join epacube.data_set DS with (nolock) on dn.data_set_fk = ds.data_set_id
							where C1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.data_column is not null and ds.COLUMN_NAME = 'Data_Value_FK') > 0 then 1 else 0 end
, [ind_EDV_LOOKUP] = Case when (select count(*) from #ColRels C1 inner join epacube.data_name DN with (nolock) on C1.data_Name_FK = DN.data_name_id inner join epacube.data_set DS with (nolock) on dn.data_set_fk = ds.data_set_id
							where C1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.data_column is not null and ds.COLUMN_NAME = 'Entity_Data_Value_FK') > 0 then 1 else 0 end
, [ind_CHILD_PS_FK] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'CHILD_Product_Structure_FK') > 0 then 1 else 0 end
, [ind_PARENT_ES_FK] = Case when (select count(*) from #ColRels C1 where c1.data_name_fk = C.Data_Name_FK and isnull(c.data_level_cr_fk, 0) = isnull(c1.data_level_cr_fk, 0) and C1.local_column = 'Parent_Entity_Structure_FK') > 0 then 1 else 0 end
from #ColRels C where data_column is not null

Update C
Set ind_REC_INGREDIENT_XREF = Case When data_name_fk in 
	(
		select A.data_Name_FK
		from epacube.Product_recipe Prec
		inner join epacube.PRODUCT_IDENTIFICATION pi on pi.PRODUCT_STRUCTURE_FK = PRec.product_structure_fk and pi.DATA_NAME_FK = 110100
		left join epacube.PRODUCT_DESCRIPTION pd on pd.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK and pd.DATA_NAME_FK = 110401
			inner join epacube.product_recipe_ingredient PRI with (nolock) on prec.product_recipe_ID = pri.product_recipe_fk and PRec.CHAIN_ID_DATA_VALUE_FK = PRI.CHAIN_ID_DATA_VALUE_FK
			INNER JOIN EPACUBE.PRODUCT_RECIPE_INGREDIENT_XREF PRIX with (nolock)	on pri.chain_id_data_value_fk = prix.chain_id_data_value_fk 
																					and pri.product_recipe_ingredient_xref_fk = prix.product_recipe_ingredient_xref_id
			inner join epacube.data_value dv_504031 on PRIX.data_value_fk = dv_504031.DATA_VALUE_ID
		inner join epacube.product_recipe_attribute a on a.product_structure_fk = PRIX.product_structure_fk and PRI.product_recipe_fk = a.product_recipe_fk
		group by a.data_name_fk
	) then 1 else 0 end
from #ColRels C where 1 = 1 and c.data_column is not null

--Select C1.Data_Name_FK, DS.Column_Name
--from #ColRels C1
--inner join epacube.data_name DN with (nolock) on C1.data_Name_FK = DN.data_name_id
--inner join epacube.data_set DS with (nolock) on dn.data_set_fk = ds.data_set_id
--where ds.COLUMN_NAME like '%Data_Value%' and C1.data_column is not null

delete C1
from #ColRels C1
inner join #ColRels C2 on c1.data_name_fk = c2.data_name_fk and isnull(c1.data_level_cr_fk, 0) = isnull(c2.data_level_cr_fk, 0)
where c1.local_column = 'data_value_fk' and c2.local_column <> 'data_value_fk' and c2.local_column like '%data_value_fk%' and c1.DATA_COLUMN is not null and c2.DATA_COLUMN is not null

Update C
Set Precedence = isnull(cr.precedence, 0)
from #ColRels C
inner join epacube.code_ref cr on isnull(c.Data_Level_CR_FK, 0) = cr.CODE_REF_ID and cr.CODE_TYPE = 'DATA_LEVEL'
where isnull(c.Data_Level_CR_FK, 0) <> 0 and data_column is not null

Update C
Set JoinSQL =  'LEFT JOIN EPACUBE.ENTITY_DATA_VALUE EDV_' + cast(C.data_name_fk as varchar(16)) + ' WITH (NOLOCK) ON EDV_' + cast(dn.data_name_id as varchar(16)) + '.PARENT_ENTITY_DATA_VALUE_FK = EDV_' + cast(C.data_name_fk as varchar(16)) + '.ENTITY_DATA_VALUE_ID '
from #ColRels C
inner join epacube.data_name dn on c.data_name_fk = dn.PARENT_DATA_NAME_FK
where 1 = 1
and ltrim(rtrim(c.QTbl)) = 'EPACUBE.[ENTITY_DATA_VALUE]' 
and c.data_column is not null
and dn.SOURCE_DATA_DN_FK is not null

--Update C
--Set Local_Structure_Column = 
--(Select top 1 C1.local_column from #ColRels C1 with (nolock) where C1.Data_Name_FK = C.Data_Name_FK
--		and C1.Entity_Class_CR_FK = C.Entity_Class_CR_FK
--		and C1.Local_Column = 
--			Case	when C1.entity_class_cr_fk = 10109 and C.ind_PS_FK = 1 and C1.local_column = 'Product_Structure_FK' then 'Product_Structure_FK'
--					when C1.entity_class_cr_fk = 10103 and C.Ind_VES_FK = 1 and C1.local_column = 'Vendor_Entity_Structure_FK' then 'Vendor_Entity_Structure_FK'
--					when C1.entity_class_cr_fk = 10103 and C.Ind_ES_FK = 1 and C1.local_column = 'Entity_Structure_FK' then 'Entity_Structure_FK'
--					when C1.entity_class_cr_fk = 10104 and C.Ind_CES_FK = 1 and C1.local_column = 'Cust_Entity_Structure_FK' then 'Cust_Entity_Structure_FK'
--					when C1.entity_class_cr_fk = 10104 and C.Ind_ES_FK = 1 and C1.local_column = 'Entity_Structure_FK' then 'Entity_Structure_FK' end) 
--from #ColRels C where C.DATA_COLUMN is not null

Update C
Set Product_Structure_Column = (Select top 1 C1.local_column from #ColRels C1 with (nolock) where C1.Data_Name_FK = C.Data_Name_FK and C1.local_column = 'Product_Structure_FK')
from #ColRels C where C.DATA_COLUMN is not null and C.ind_PS_FK = 1

Update C
Set Customer_Structure_Column = (Select top 1 C1.local_column from #ColRels C1 with (nolock) where C1.Data_Name_FK = C.Data_Name_FK and (C1.local_column = 'Cust_Entity_Structure_FK' or (C1.local_column = 'entity_structure_fk' and c1.entity_class_cr_fk in (10104, 10109))) order by C1.local_column)
from #ColRels C where C.DATA_COLUMN is not null and (C.ind_CES_FK = 1 or (C.ind_ES_FK = 1 and C.Entity_Class_CR_FK in (10104, 10109)))

Update C
Set Vendor_Structure_Column = (Select top 1 C1.local_column from #ColRels C1 with (nolock) where C1.Data_Name_FK = C.Data_Name_FK and (C1.local_column = 'Vendor_Entity_Structure_FK' or (C1.local_column = 'entity_structure_fk' and c1.entity_class_cr_fk in (10103))) order by C1.local_column)
from #ColRels C where C.DATA_COLUMN is not null and (C.ind_VES_FK = 1 or (C.ind_ES_FK = 1 and C.Entity_Class_CR_FK = 10103))

Update C
Set Zone_Structure_Column = (Select top 1 C1.local_column from #ColRels C1 with (nolock) where C1.Data_Name_FK = C.Data_Name_FK and (C1.local_column = 'Zone_Entity_Structure_FK' or (C1.local_column = 'entity_structure_fk' and c1.entity_class_cr_fk in (10117))) order by C1.local_column)
from #ColRels C where C.DATA_COLUMN is not null and (C.ind_ZES_FK = 1 or (C.ind_ES_FK = 1 and C.Entity_Class_CR_FK = 10117))

Update C
Set Table_Name = '#PBV'
, QTbl =  '#PBV'
, Table_Schema = Null
from #ColRels C where Table_Name = 'PRICESHEET_BASIS_VALUES'

--Alter table #ColRels add [Virtual_Exclude_Select_Dependent] bigint null
Alter table #ColRels add [Virtual_Where_Clause] varchar(max) null

Alter table #ColRels add [Data_Type] varchar(64) null
Alter table #ColRels add [Date_Type] varchar(32) null

Update C
Set data_type = epacube.getdatatypefordn(data_name_fk)
from #ColRels C where data_column is not null

Update C
Set date_type = Case when Data_column like '%Effective_Date%' then 'effective' else 'other' end
from #ColRels C where data_type = 'Date' and data_column is not null

Insert into #ColRels
(Table_Name, QTbl, Data_Name_FK, Entity_Class_CR_FK, Data_Column, Alias, DN_Name, DN_Label, VirtualActionType, VirtualActionType_CR_FK, VirtualColumnCheck, V_Ref_Data_Name_FK, Product_Structure_Column, Customer_Structure_Column, Vendor_Structure_Column, Zone_Structure_Column, ind_PS_fk, ind_ES_fk, ind_OES_fk, ind_CES_fk, ind_VES_fk, ind_ZES_fk, ind_COMP_ES_fk, ind_ASSOC_ES_fk, ind_DL_CR_fk, ind_PROD_DV, ind_CUST_DV, ind_DEPT_EDV, NullColumn, [Virtual_Where_Clause], Data_Type, Date_Type)
Select Case c1.Table_Name when 'PRICESHEET_BASIS_VALUES' then '#PBV' else c1.Table_Name end Table_Name, Case c1.Table_Name when 'PRICESHEET_BASIS_VALUES' then '#PBV' else c1.QTbl end QTbl
, dnv.data_name_fk, c1.Entity_Class_CR_FK
, Case	when cr.code	= 'REFERENCE' then dnv.column_Check 
		when cr.code	= 'REFERENCE' and epacube.getdatatypefordn(dn.data_name_id) = 'Date' then 'cast(' + c1.Alias + '.[' + dnv.column_Check + '] as date) '
		when cr.code	in ('REFERENCE', 'SQLcode', 'WhenRecordExists') then dnv.column_Check 
		when data_column like '%Structure_ID' then 'value' 
		when isnull(cr.code, '') = 'CheckBoxTransform' and isnull(Column_Check, '') like '%Record_Status_CR_FK%' then 'Replace(Replace(' + C1.Alias + '.Record_Status_CR_FK, 2, ''N''), 1, ''Y'')'
	else Data_Column end Data_Column
, dnv.VIRTUAL_ALIAS 
, dnv.VIRTUAL_COLUMN_NAME, dnv.COLUMN_LABEL, cr.code, cr.code_ref_id, dnv.COLUMN_CHECK, dnv.REFERENCE_DATA_NAME_FK
, C1.Product_Structure_Column, C1.Customer_Structure_Column, C1.Vendor_Structure_Column, C1.Zone_Structure_Column
, Isnull(ind_PS_fk, 0), Isnull(ind_ES_fk, 0), Isnull(ind_OES_fk, 0), Isnull(ind_CES_fk, 0), Isnull(ind_VES_fk, 0), Isnull(ind_ZES_fk, 0), Isnull(ind_COMP_ES_fk, 0), Isnull(ind_ASSOC_ES_fk, 0), Isnull(ind_DL_CR_fk, 0), Isnull(ind_PROD_DV, 0), Isnull(ind_CUST_DV, 0), Isnull(ind_DEPT_EDV, 0), Isnull(NullColumn, 0)
--, dnv.exclude_select_dependent
, dnv.[Virtual_Where_Clause]
, epacube.getdatatypefordn(dn.data_name_id) 'data_type'
, Case epacube.getdatatypefordn(dn.data_name_id) when 'Date' then
	Case when dn.name like '%effective%' then 'effective' else 'other' end else null end 'date_type'
from epacube.data_name_virtual dnv
inner join epacube.code_ref cr on dnv.virtual_action_type_cr_fk = cr.code_ref_id
inner join epacube.data_name dn on dnv.data_name_fk = dn.DATA_NAME_ID
inner join #ColRels C1 on dnv.REFERENCE_DATA_NAME_FK = C1.data_name_fk and C1.data_column is not null
order by dnv.data_name_fk

Update C
Set EVENT_TYPE_CR_FK =	Case when QTbl in ('EPACUBE.[SETTINGS_POS_PROD_STORE_HEADER]', 'EPACUBE.[PRODUCT_IDENTIFICATION]') then 150 
when QTbl in ('#PBV') then 158
when QTbl in ('EPACUBE.[PRODUCT_ASSOCIATION]') THEN 155
when QTbl in ('EPACUBE.[SEGMENTS_SETTINGS]') then 154 else ds.EVENT_TYPE_CR_FK end
from #ColRels C inner join epacube.data_name dn on c.data_name_fk = dn.data_name_id inner join epacube.data_set ds on dn.data_set_fk = ds.data_set_id where ds.EVENT_TYPE_CR_FK is not null and c.data_column is not null

--Select c.table_name, c.qtbl, r.table_name 'rRN', r.qtbl 'rQTBL', c.data_name_fk, epacube.getdatanamelabel(c.data_name_fk) DataNameLabel, c.data_Column dc1, isnull(c.event_type_cr_fk, 0) C_ET, isnull(r.event_type_cr_fk, 0) R_ET, c.entity_class_cr_fk C_ECCRFK, r.entity_class_cr_fk R_ECCRFD
--from #ColRels C
--inner join epacube.data_name_group_refs r on c.data_name_fk = r.data_name_fk and isnull(c.data_level_cr_fk, 0) = isnull(r.data_level_cr_fk, 0) where c.data_column is not  null
--and isnull(c.event_type_cr_fk, 0) <> isnull(r.event_type_cr_fk, 0)

Update C
Set ind_REC_INGREDIENT_XREF = 1
from #ColRels C where (data_name_fk in (504030, 504031)
or data_name_fk in (select data_name_FK from epacube.data_name_virtual where REFERENCE_DATA_NAME_FK in (504030, 504031))) and DATA_COLUMN is not null

Update C
Set ind_PS_FK = isnull(ind_PS_FK, 0)
, [ind_ES_FK] = isnull([ind_ES_FK], 0)
, [ind_OES_FK] = isnull([ind_OES_FK], 0)
, [ind_CES_FK] = isnull([ind_CES_FK], 0)
, [ind_VES_FK] = isnull([ind_VES_FK], 0)
, [ind_ZES_FK] = isnull([ind_ZES_FK], 0)
, [ind_COMP_ES_FK] = isnull([ind_COMP_ES_FK], 0)
, [ind_ASSOC_ES_FK] = isnull([ind_ASSOC_ES_FK], 0)
, [ind_DL_CR_FK] = isnull([ind_DL_CR_FK], 0)
, [ind_PROD_DV] = isnull([ind_PROD_DV], 0)
, [ind_CUST_DV] = isnull([ind_CUST_DV], 0)
, [ind_DEPT_EDV] = isnull([ind_DEPT_EDV], 0)
, [ind_DV_LOOKUP] = isnull([ind_DV_LOOKUP], 0)
, [ind_EDV_LOOKUP] = isnull([ind_EDV_LOOKUP], 0)
, [ind_CHILD_PS_FK] = isnull([ind_CHILD_PS_FK], 0)
, [ind_PARENT_ES_FK] = isnull([ind_PARENT_ES_FK], 0)
from #ColRels C where data_column is not null

Update CR
Set Entity_Class_CR_FK = 10103
from #ColRels CR where v_ref_data_name_fk = 159300

--Update CR
--Set Data_Column = [local_column]
--FROM #COLRELS CR WHERE DATA_COLUMN = 'ATTRIBUTE_EVENT_DATA' AND LOCAL_COLUMN IN ('DATA_VALUE_FK', 'ENTITY_DATA_VALUE_FK')

Alter table #ColRels Add Attribute_Parent_DN_FK bigint null

Update CR
Set Attribute_Parent_DN_FK = ss1.parent_DN_FK
from #ColRels CR
inner join 
	(Select ss.data_name_fk parent_DN_FK, ssa.data_name_fk from epacube.settings_store_attribute ssa inner join epacube.settings_store ss on ssa.settings_store_fk = ss.settings_store_id group by ss.data_name_fk, ssa.data_name_fk) SS1 on CR.data_name_fk = ss1.data_name_fk
where CR.DATA_COLUMN is not null

Update CR
Set Entity_Structure_EC_CR_FK = ds.Entity_Structure_EC_CR_FK
from #ColRels CR
inner join epacube.data_name dn with (nolock) on CR.data_name_fk = dn.data_name_id
inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id
where ds.ENTITY_STRUCTURE_EC_CR_FK is not null

Insert into #ColRels
(table_schema, table_name, qtbl, data_name_fk, primary_column, local_column, data_column, alias, dn_name, dn_label)
Select 'CONTROL', 'CONTROL', 'CONTROL', DATA_NAME_ID, LABEL, LABEL, LABEL, 'CTRL_' + CAST(DATA_NAME_ID AS VARCHAR(16)), NAME, LABEL
FROM EPACUBE.DATA_NAME WHERE DATA_NAME_ID BETWEEN 540000 AND 541000

Drop table EPACUBE.DATA_NAME_GROUP_REFS
Select * 
into EPACUBE.DATA_NAME_GROUP_REFS 
from #ColRels where data_column is not null

CREATE INDEX IDX_DNGR ON EPACUBE.DATA_NAME_GROUP_REFS(DATA_NAME_FK)

Update DNGR
Set Data_column = 'NULL'
from epacube.DATA_NAME_GROUP_REFS DNGR where Data_column = ''
