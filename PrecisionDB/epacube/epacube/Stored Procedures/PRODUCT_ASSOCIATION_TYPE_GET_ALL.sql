﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get all product association types (URM-1187)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[PRODUCT_ASSOCIATION_TYPE_GET_ALL]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.PRODUCT_ASSOCIATION_TYPE_GET_ALL.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT DATA_NAME_ID PRODUCT_ASSOCIATION_TYPE_ID
			, DN.NAME
			, LABEL
			, DN.SHORT_NAME
		FROM epacube.DATA_NAME DN
			INNER JOIN epacube.DATA_SET DS 
				ON DN.DATA_SET_FK = DS.DATA_SET_ID
		WHERE DS.TABLE_NAME = 'PRODUCT_ASSOCIATION'
			AND DN.RECORD_STATUS_CR_FK = 1
		FOR JSON PATH, ROOT('data');

		SET @ls_stmt = 'Finished execution of epacube.PRODUCT_ASSOCIATION_TYPE_GET_ALL.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH
		SET @ls_stmt = 'Execution of epacube.PRODUCT_ASSOCIATION_TYPE_GET_ALL has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END 

