﻿






-- Copyright 2006 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        05/28/2014   Initial SQL Version

      


CREATE PROCEDURE [epacube].[REMOVE_NULL_SHEET_RESULTS_EVENTS] 
AS
BEGIN
        DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @job_timestamp DATETIME

		
		

        BEGIN TRY
            SET NOCOUNT ON ;
            SET @l_exec_no = 0
            SET @l_sysdate = GETDATE()
										  

							
            SET @ls_stmt = 'started execution of epacube.REMOVE NULL_Sheet Results EVENTS. '
                 EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;
                


------------------------------------------------------------------------------------------------------------
---Flow is causing Sheet results that normally would be rejected because of NULL values to go on HOLD
---We need to remove the errors and remove the events.
------------------------------------------------------------------------------------------------------------
---insert events into temp table

IF object_id('tempdb..#TS_EVENT_ID') is not null
   drop table #TS_EVENT_ID

CREATE TABLE #TS_EVENT_ID(
		[TS_EVENTS_FK] [bigint] NOT NULL)


		iNSERT into #TS_EVENT_ID
		(TS_EVENTS_FK)
(select event_fk from synchronizer.EVENT_DATA_ERRORS 
where ERROR_NAME = 'EVENT REJECTED:: ALL VALUES are NULL')

delete from synchronizer.EVENT_DATA_rules where event_fk in (
select event_fk from synchronizer.EVENT_DATA_ERRORS 
where ERROR_NAME = 'EVENT REJECTED:: ALL VALUES are NULL')


delete from synchronizer.EVENT_DATA_ERRORS where event_fk in (
select event_fk from synchronizer.EVENT_DATA_ERRORS 
where ERROR_NAME = 'EVENT REJECTED:: ALL VALUES are NULL')




delete from synchronizer.event_data where 1 = 1 -- and EVENT_STATUS_CR_FK = 82
and event_id in (select ts_events_fk from #TS_EVENT_ID)




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

            SET @status_desc = 'finished execution of epacube.REMOVE NULL_Sheet Results_EVENTS'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;

        END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.REMOVE Sheet Results EVENTS has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


              EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
      
		  
    END







