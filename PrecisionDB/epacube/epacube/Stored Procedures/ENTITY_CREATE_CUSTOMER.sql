﻿
-- Copyright 2018 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create entities JIRA (URM-1159)
-- 1. Determine if the parent already exists
-- 2. If the parent exists create child record associated to the parent record
-- 3. If the parent does not exist, create the parent record and then create the child record associated to the parent record 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------


CREATE PROCEDURE [epacube].[ENTITY_CREATE_CUSTOMER] 
	@exec_id BIGINT
	,@criteria AS XML
	,@user VARCHAR(64)
	
AS
BEGIN
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @new_parent_count INT = 0;
	DECLARE @new_child_count INT = 0;
	DECLARE @new_parent_entity_structure_id AS BIGINT;
	DECLARE @new_parent_entity_identification_id AS BIGINT;
	DECLARE @new_child_entity_structure_id BIGINT;
	DECLARE @new_child_entity_identification_id BIGINT;
	DECLARE @i INT = 0;

	SET NOCOUNT ON;

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.ENTITY_CREATE_CUSTOMER.'
	EXEC exec_monitor.Report_Status_sp @exec_id,
		@ls_stmt;

	--***************************************************************************************************************************************
	--Example customer entity structure
	--
	-- '<root>
	--		<h1 h1Code="12345" h1Name="John''s Foods">
	--			<h2 h2Code="122334" h2Name="Nob Hill Store"></h2>
	--			<h2 h2Code="133445" h2Name="Norwood Store"></h2>
	--		</h1>
	--	</root>'
	--
	--***************************************************************************************************************************************	
					
	SET @ls_stmt = 'Begin creation of new CUSTOMER entity.'
	EXEC exec_monitor.Report_Status_sp exec_id,
		@ls_stmt

	IF OBJECT_ID('tempdb.dbo.#Customer', 'U') IS NOT NULL
		DROP TABLE #Customer; 

	SELECT t.c.value('parent::*/@h1Code', 'VARCHAR(128)') AS h1_code
		, t.c.value('parent::*/@h1Name', 'VARCHAR(128)') AS h1_name
		, t.c.value('@h2Code', 'VARCHAR(128)') AS h2_code
		, t.c.value('@h2Name', 'VARCHAR(128)') AS h2_name
	INTO #customer
	FROM @criteria.nodes('/root[1]/h1/h2') t(c)

	--Check for new parents
	SELECT @new_parent_count = COUNT(DISTINCT h1_code)
	FROM #customer c
		LEFT JOIN epacube.ENTITY_IDENTIFICATION ei
			ON c.h1_code = ei.[VALUE]
		LEFT JOIN epacube.ENTITY_STRUCTURE es
			ON ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
				AND es.DATA_NAME_FK LIKE '144%'

	IF @new_parent_count > 0
		BEGIN
			--***************************************************************************************************************************************
			--Begin Parent Block
					
			SET @i = 1;
					
			PRINT '144.101.' + CAST(@i AS VARCHAR(2)) + ' - ' + CAST(@new_parent_count AS VARCHAR(2)) + ' new parent customer(s) found'

			DECLARE @customer_parent_code AS VARCHAR(128);
			DECLARE @customer_parent_name AS VARCHAR(128);
			DECLARE @customer_parent_cursor AS CURSOR;

			SET @customer_parent_cursor = CURSOR FOR
			SELECT DISTINCT h1_code, h1_name
			FROM #customer;

			OPEN @customer_parent_cursor;

			FETCH NEXT FROM @customer_parent_cursor INTO @customer_parent_code, @customer_parent_name;

			WHILE @@FETCH_STATUS = 0
				BEGIN
					BEGIN TRY
						BEGIN TRANSACTION;
							--Create the parent entity structure
							INSERT INTO epacube.ENTITY_STRUCTURE (ENTITY_STATUS_CR_FK, DATA_NAME_FK, LEVEL_SEQ, ENTITY_CLASS_CR_FK, RECORD_STATUS_CR_FK, CREATE_USER)
							VALUES (10200, 144010, 1, 10104, 1, @user);

							SET @new_parent_entity_structure_id = SCOPE_IDENTITY();

							PRINT '144.102.' + CAST(@i AS VARCHAR(2)) + ' - New parent entity structure created with ID: ' + CAST(@new_parent_entity_structure_id AS CHAR(20)); 
					
							--Add the parent unique code
							INSERT INTO epacube.ENTITY_IDENTIFICATION (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, [VALUE], RECORD_STATUS_CR_FK, CREATE_USER)
							VALUES (@new_parent_entity_structure_id, 144010, 144111, @customer_parent_code, 1, @user);

							SET @new_parent_entity_identification_id = SCOPE_IDENTITY();

							PRINT '144.103.' + CAST(@i AS VARCHAR(2)) + ' - New parent unique code added with ID: ' + CAST(@new_parent_entity_identification_id AS CHAR(20));

							--Add the parent non-unique name
							INSERT INTO epacube.ENTITY_IDENT_NONUNIQUE (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, [VALUE], RECORD_STATUS_CR_FK, CREATE_USER)
							VALUES (@new_parent_entity_structure_id, 144010, 144112, @customer_parent_name, 1, @user);

							SET @new_parent_entity_identification_id = SCOPE_IDENTITY();

							PRINT '144.104.' + CAST(@i AS VARCHAR(2)) + ' - New parent non-unique name added with ID: ' + CAST(@new_parent_entity_identification_id AS CHAR(20));
						
						COMMIT TRANSACTION;

						SET @ls_stmt = 'A matching parent was found with id ' + CAST(@new_parent_entity_structure_id AS VARCHAR(20)) + '. A new child was created with id ' + CAST(@new_child_entity_identification_id AS VARCHAR(20)); 
						EXEC exec_monitor.Report_Status_sp exec_id, @ls_stmt;
					
						SET @i = @i + 1;

						FETCH NEXT FROM @customer_parent_cursor INTO @customer_parent_code, @customer_parent_name;
					END TRY

					BEGIN CATCH
						IF (@@TRANCOUNT > 0)
							BEGIN
								ROLLBACK TRANSACTION;
								PRINT 'Error detected, all changes reversed';
							END 
					
						CLOSE @customer_parent_cursor;
						DEALLOCATE @customer_parent_cursor;
						
						PRINT 'ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE() 

						SET @ls_stmt = 'Execution of epacube.ENTITY_CREATE_CUSTOMER has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
						EXEC exec_monitor.Report_Status_sp exec_id, @ls_stmt;

						EXEC exec_monitor.Report_Error_sp exec_id;
				
					END CATCH

				END

			SET @ls_stmt = 'Finished execution of epacube.ENTITY_CREATE_CUSTOMER.';
			EXEC exec_monitor.Report_Status_sp exec_id, @ls_stmt;

			CLOSE @customer_parent_cursor;
			DEALLOCATE @customer_parent_cursor;
				
			--End Parent Block
			--***************************************************************************************************************************************
		END
			
	--Check for new children
	SELECT @new_child_count = COUNT(h2_code)
	FROM #Customer c
		LEFT JOIN epacube.ENTITY_IDENTIFICATION ei
			ON c.h2_code = ei.[VALUE]
	WHERE ei.ENTITY_IDENTIFICATION_ID IS NULL
			
	IF @new_child_count > 0
		BEGIN
			--***************************************************************************************************************************************
			--Begin Child Block
					
			SET @i = 1;
					
			PRINT '144.201.' + CAST(@i AS VARCHAR(2)) + ' - ' + CAST(@new_child_count AS VARCHAR(2)) + ' new child customer(s) found'

			DECLARE @child_code AS VARCHAR(128);
			DECLARE @child_name AS VARCHAR(128);
			DECLARE @parent_id AS BIGINT;
			DECLARE @child_cursor AS CURSOR;

			SET @child_cursor = CURSOR FOR
			SELECT c.h2_code
				, c.h2_name
				, es.ENTITY_STRUCTURE_ID
			FROM #customer c
				INNER JOIN epacube.ENTITY_IDENTIFICATION ei
					ON c.h1_code = ei.[VALUE]
				INNER JOIN epacube.ENTITY_STRUCTURE es
					ON ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
			WHERE es.DATA_NAME_FK LIKE '144%' 

			OPEN @child_cursor;

			FETCH NEXT FROM @child_cursor INTO @child_code, @child_name, @parent_id;

			WHILE @@FETCH_STATUS = 0
				BEGIN
					BEGIN TRY
						BEGIN TRANSACTION;
							--Create the child entity structure
							INSERT INTO epacube.ENTITY_STRUCTURE (ENTITY_STATUS_CR_FK, DATA_NAME_FK, LEVEL_SEQ, PARENT_ENTITY_STRUCTURE_FK, ENTITY_CLASS_CR_FK, RECORD_STATUS_CR_FK, CREATE_USER)
							VALUES (10200, 144020, 1, @parent_id, 10104, 1, @user);

							SET @new_child_entity_structure_id = SCOPE_IDENTITY();

							PRINT '144.202.' + CAST(@i AS VARCHAR(2)) + ' - New child entity structure added with ID: ' + CAST(@new_child_entity_structure_id AS CHAR(20));

							--Add the child unique code
							INSERT INTO epacube.ENTITY_IDENTIFICATION (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, [VALUE], RECORD_STATUS_CR_FK, CREATE_USER)
							VALUES (@new_child_entity_structure_id, 144020, 144111, @child_code, 1, @user);

							SET @new_child_entity_identification_id = SCOPE_IDENTITY();

							PRINT '144.203.' + CAST(@i AS VARCHAR(2)) + ' - New child unique code added with ID: ' + CAST(@new_child_entity_identification_id AS CHAR(20));

							--Add the child non-unique name
							INSERT INTO epacube.ENTITY_IDENT_NONUNIQUE (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, [VALUE], RECORD_STATUS_CR_FK, CREATE_USER)
							VALUES (@new_child_entity_structure_id, 144020, 144112, @child_name, 1, @user);

							SET @new_child_entity_identification_id = SCOPE_IDENTITY();

							PRINT '144.204.' + CAST(@i AS VARCHAR(2)) + ' - New child non-unique name added with ID: ' + CAST(@new_child_entity_identification_id AS CHAR(20));
						
						COMMIT TRANSACTION;
						
						SET @ls_stmt = 'A matching parent was found with id ' + CAST(@parent_id AS VARCHAR(20)) + '. A new child was created with id ' + CAST(@new_child_entity_identification_id AS VARCHAR(20)) 
						EXEC exec_monitor.Report_Status_sp exec_id, @ls_stmt;
								
						SET @i = @i + 1;
								
						FETCH NEXT FROM @child_cursor INTO @child_code, @child_name, @parent_id;

					END TRY
					
					BEGIN CATCH
						IF (@@TRANCOUNT > 0)
							BEGIN
								ROLLBACK TRANSACTION;
								PRINT 'Error detected, all changes reversed';
							END
						
							CLOSE @child_cursor;
							DEALLOCATE @child_cursor; 
					
							PRINT 'ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE(); 

							SET @ls_stmt = 'Execution of epacube.ENTITY_CREATE_CUSTOMER has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
							EXEC exec_monitor.Report_Status_sp exec_id, @ls_stmt;

							EXEC exec_monitor.Report_Error_sp exec_id;
					END CATCH
					
				END
					
			CLOSE @child_cursor;
			DEALLOCATE @child_cursor;

			SET @ls_stmt = 'Finished execution of epacube.ENTITY_CREATE_CUSTOMER.'
			EXEC exec_monitor.Report_Status_sp exec_id, @ls_stmt;
					
			--End Child Block	
			--***************************************************************************************************************************************	
		END

END
