﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create entities JIRA (URM-1159)
-- Get all entity information based on the ID given
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[ENTITY_GET_BY_ID] 
	-- Add the parameters for the stored procedure here
	@entity_structure_id BIGINT
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @entity_id_conv AS VARCHAR(20);
	DECLARE @entity_type_string AS VARCHAR(20);
	DECLARE @is_parent AS TINYINT;
	DECLARE @child_table_structure AS VARCHAR(MAX);

	BEGIN TRY

		--Initialize log
		SET @ls_stmt = 'Started execution of epacube.ENTITY_GET_BY_ID.'
		EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
			@exec_id = @l_exec_no OUTPUT;

		SET @entity_id_conv = CAST(@entity_structure_id AS VARCHAR(20));
			
		SET @ls_stmt = 'Get entity information for ID ' + @entity_id_conv
		EXEC exec_monitor.Report_Status_sp @l_exec_no,
			@ls_stmt
		
		SELECT @entity_type_string = cr.CODE
			, @is_parent = 
				CASE
					WHEN PARENT_ENTITY_STRUCTURE_FK IS NULL THEN 1
					ELSE 0
				END
		FROM epacube.ENTITY_STRUCTURE es 
			INNER JOIN epacube.DATA_NAME dn ON es.DATA_NAME_FK = dn.DATA_NAME_ID 
			INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
			INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
		WHERE ENTITY_STRUCTURE_ID = @entity_structure_id 

		DECLARE @field_list AS TABLE (DATA_NAME_ID INT, [NAME] VARCHAR(64), LABEL VARCHAR(64));
		DECLARE @dynamic_fields AS VARCHAR(MAX);

		INSERT INTO @field_list
		SELECT DATA_NAME_ID, dn.[NAME], LABEL
		FROM epacube.DATA_NAME dn
			INNER JOIN epacube.DATA_SET ds ON dn.DATA_SET_FK = ds.DATA_SET_ID
			INNER JOIN epacube.CODE_REF cr ON ds.ENTITY_CLASS_CR_FK = cr.CODE_REF_ID
		WHERE dn.RECORD_STATUS_CR_FK = 1
			AND cr.CODE = @entity_type_string
			AND USER_EDITABLE = 1

		SELECT @dynamic_fields = COALESCE(@dynamic_fields + ', ', '') + QUOTENAME([NAME])
		FROM @field_list
		
		IF @is_parent = 1
			BEGIN

				IF OBJECT_ID('tempdb.dbo.#Entity_Parent', 'U') IS NOT NULL
					DROP TABLE #Entity_Parent; 

				SELECT *
				INTO #Entity_Parent
				FROM (
					SELECT es.ENTITY_STRUCTURE_ID, dn.[NAME], ei.[VALUE]
					FROM epacube.DATA_NAME dn
						INNER JOIN epacube.ENTITY_IDENTIFICATION ei ON dn.DATA_NAME_ID = ei.DATA_NAME_FK
						INNER JOIN epacube.ENTITY_STRUCTURE es ON ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
					WHERE ENTITY_STRUCTURE_FK = @entity_structure_id
						AND es.PARENT_ENTITY_STRUCTURE_FK IS NULL
						AND ei.RECORD_STATUS_CR_FK = 1
					UNION
					SELECT es.ENTITY_STRUCTURE_ID, dn.[NAME], ein.[VALUE]
					FROM epacube.DATA_NAME dn
						INNER JOIN epacube.ENTITY_IDENT_NONUNIQUE ein ON dn.DATA_NAME_ID = ein.DATA_NAME_FK
						INNER JOIN epacube.ENTITY_STRUCTURE es ON ein.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
					WHERE ENTITY_STRUCTURE_FK = @entity_structure_id
						AND es.PARENT_ENTITY_STRUCTURE_FK IS NULL
						AND ein.RECORD_STATUS_CR_FK = 1) AS src;

				IF OBJECT_ID('tempdb.dbo.Entity_Parent_Pivot', 'U') IS NOT NULL
					DROP TABLE tempdb.dbo.Entity_Parent_Pivot; 
 
				--Build the Dynamic Pivot Table Query
				DECLARE @table_structure AS VARCHAR(MAX);
		  
				SET @table_structure = 'SELECT ENTITY_STRUCTURE_ID AS id, ' + @dynamic_fields  +  
				' INTO tempdb.dbo.Entity_Parent_Pivot FROM #Entity_Parent x PIVOT ( MAX(VALUE) FOR [NAME] in (' + @dynamic_fields + ') ) p ';
	
				EXECUTE(@table_structure);

				IF OBJECT_ID('tempdb.dbo.#Entity_Children', 'U') IS NOT NULL
					DROP TABLE #Entity_Children; 

				SELECT *
				INTO #Entity_Children
				FROM (
					SELECT es.ENTITY_STRUCTURE_ID, es.PARENT_ENTITY_STRUCTURE_FK, dn.[NAME], ei.[VALUE]
					FROM epacube.DATA_NAME dn
						INNER JOIN epacube.ENTITY_IDENTIFICATION ei ON dn.DATA_NAME_ID = ei.DATA_NAME_FK
						INNER JOIN epacube.ENTITY_STRUCTURE es ON ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
					WHERE PARENT_ENTITY_STRUCTURE_FK = @entity_structure_id
						AND ei.RECORD_STATUS_CR_FK = 1
					UNION
					SELECT es.ENTITY_STRUCTURE_ID, es.PARENT_ENTITY_STRUCTURE_FK, dn.[NAME], ein.[VALUE]
					FROM epacube.DATA_NAME dn
						INNER JOIN epacube.ENTITY_IDENT_NONUNIQUE ein ON dn.DATA_NAME_ID = ein.DATA_NAME_FK
						INNER JOIN epacube.ENTITY_STRUCTURE es ON ein.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
					WHERE PARENT_ENTITY_STRUCTURE_FK = @entity_structure_id
						AND ein.RECORD_STATUS_CR_FK = 1) src;

				IF OBJECT_ID('tempdb.dbo.Entity_Children_Pivot', 'U') IS NOT NULL
					DROP TABLE tempdb.dbo.Entity_Children_Pivot; 
 
				--Build the Dynamic Pivot Table Query

				SET @child_table_structure = 'SELECT ENTITY_STRUCTURE_ID AS id, PARENT_ENTITY_STRUCTURE_FK AS fk, ' + @dynamic_fields +  
				' INTO tempdb.dbo.Entity_Children_Pivot FROM #Entity_Children x PIVOT ( MAX(VALUE) FOR [NAME] in (' + @dynamic_fields + ') ) p ';  
		
				EXECUTE(@child_table_structure);

				SELECT @entity_type_string entityType
					, (
						SELECT *
						FROM @field_list
						FOR JSON PATH
					) [fields]
					, (
						SELECT ep.*
							, (
									SELECT ecp.*
									FROM tempdb.dbo.Entity_Children_Pivot ecp
									WHERE ecp.fk = ep.id
									FOR JSON PATH
								) [children]
						FROM tempdb.dbo.Entity_Parent_Pivot ep
						FOR JSON PATH
					) [data]
				FOR JSON PATH;
			END
		ELSE
			BEGIN
				IF OBJECT_ID('tempdb.dbo.#Entity', 'U') IS NOT NULL
					DROP TABLE #Entity; 

				SELECT *
				INTO #Entity
				FROM (
					SELECT es.ENTITY_STRUCTURE_ID, es.PARENT_ENTITY_STRUCTURE_FK, dn.[NAME], ei.[VALUE]
					FROM epacube.DATA_NAME dn
						INNER JOIN epacube.ENTITY_IDENTIFICATION ei ON dn.DATA_NAME_ID = ei.DATA_NAME_FK
						INNER JOIN epacube.ENTITY_STRUCTURE es ON ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
					WHERE ENTITY_STRUCTURE_FK = @entity_structure_id
						AND es.PARENT_ENTITY_STRUCTURE_FK IS NOT NULL
						AND ei.RECORD_STATUS_CR_FK = 1
					UNION
					SELECT es.ENTITY_STRUCTURE_ID, es.PARENT_ENTITY_STRUCTURE_FK, dn.[NAME], ein.[VALUE]
					FROM epacube.DATA_NAME dn
						INNER JOIN epacube.ENTITY_IDENT_NONUNIQUE ein ON dn.DATA_NAME_ID = ein.DATA_NAME_FK
						INNER JOIN epacube.ENTITY_STRUCTURE es ON ein.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID
					WHERE ENTITY_STRUCTURE_FK = @entity_structure_id
						AND es.PARENT_ENTITY_STRUCTURE_FK IS NOT NULL
						AND ein.RECORD_STATUS_CR_FK = 1) src;

				IF OBJECT_ID('tempdb.dbo.Entity_Pivot', 'U') IS NOT NULL
					DROP TABLE tempdb.dbo.Entity_Pivot; 
 
				--Build the Dynamic Pivot Table Query
				SET @child_table_structure = 'SELECT ENTITY_STRUCTURE_ID AS id, PARENT_ENTITY_STRUCTURE_FK AS fk, ' + @dynamic_fields +  
				' INTO tempdb.dbo.Entity_Pivot FROM #Entity x PIVOT ( MAX(VALUE) FOR [NAME] in (' + @dynamic_fields + ') ) p ';  
		
				EXECUTE(@child_table_structure);

				SELECT @entity_type_string entityType
					, (
						SELECT *
						FROM @field_list
						FOR JSON PATH
					) [fields]
					, (
						SELECT ecp.*
						FROM tempdb.dbo.Entity_Pivot ecp
						FOR JSON PATH
					) [data]
				FOR JSON PATH;
			END

		SET @ls_stmt = 'Finished execution of epacube.ENTITY_GET_BY_ID.';
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		SET @ls_stmt = 'Execution of epacube.ENTITY_GET_BY_ID has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

	END CATCH

END
