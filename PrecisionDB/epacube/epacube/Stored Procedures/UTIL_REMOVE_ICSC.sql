﻿



-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Jan DeForest
--
--
-- Purpose: Removing Products from ICSC Catalog
--			
--			
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- JD        06/08/2012   Initial SQL Version
-- CV        10/03/2013    Add Create Job for logging to UI
--

CREATE PROCEDURE [epacube].[UTIL_REMOVE_ICSC] 
      
AS
BEGIN



DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint
DECLARE @v_import_package_fk   int  
DECLARE @v_NULLING_IND   int 
DECLARE @v_filename_count INT
DECLARE @epa_job_id			 INT
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime
DECLARE @v_out_job_fk BIGINT

DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int




BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of epacube.Util_remove_icsc', 
                                                @epa_job_id = NULL, @epa_batch_no =NULL,
												@exec_id = @l_exec_no OUTPUT;
												
SET @l_sysdate = GETDATE()
--------------------------------------
----adding create job for UI visibility
--------------------------------------
select * from common.job_class
                
                  EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'job_seq', @v_job_fk output
   

    SET @v_job_class_fk  = 910        --- HOUSEKEEPING
    SET @v_job_user = 'EPACUBE'
    SET @v_job_name = 'REMOVE ICSC'
    SET @v_job_date = @l_sysdate
    SET @v_data1   = 'LAST JOB:: '
                     + CAST ( ISNULL ( @v_last_job_fk, -999 )  AS VARCHAR(32) )
                     + ' - '
                     + CAST ( ISNULL ( @v_last_job_date, '01/01/2000' ) AS VARCHAR(64) )
    SET @v_data2   = @v_last_job_date
    
    EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
										   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

    EXEC common.job_execution_create   @v_job_fk, 'START REMOVE ICSC',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  

-----------------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------------

-- SQL's to remove ICSC data from epaCUBE
-- Process is to:
--		1) Set epaCUBE Status = HOLD on products to be removed from SX Catalog
--		2) Run ICSC REMOVE Export
--		3) Find products where epaCUBE Status = Hold AND product not also in ICSP and/or ICSW
--		   Set epaCUBE Status on these products to PURGE.  Nightly Synchronizer job will purge them daily.
--		4) Find products where epaCUBE Status = Hold AND product IS in ICSP and/or ICSW	
--		   Remove System Association = CATALOG and then set epaCUBE Status back to ACTIVE.	

-- ADD NIGHTLY PURGE ROUTINE ALSO



-- 3) Find Hold status products in ICSC ONLY and set epaCUBE status to PURGE
update epacube.product_structure
set product_status_cr_fk = 102
where product_structure_id in (
	select distinct pas1.product_structure_fk
	from epacube.product_association pas1
	where pas1.data_name_fk = 159200
	and pas1.entity_structure_fk in (
		select ei1.entity_structure_fk from epacube.entity_identification ei1
		where ei1.data_name_fk = 142111 and ei1.value = 'CATALOG')
	and pas1.product_structure_fk in (
		select product_structure_id from epacube.product_structure
		where product_status_cr_fk = 105)
	and 
	( pas1.product_structure_fk not in (
		select pas2.product_structure_fk from epacube.product_association pas2
		where pas2.data_name_fk = 159200
		and pas2.entity_structure_fk in (
			select ei2.entity_structure_fk from epacube.entity_identification ei2
			where ei2.data_name_fk = 142111 and ei2.value = 'PRODUCT'))
	and pas1.product_structure_fk not in (
		select distinct pas3.product_structure_fk from epacube.product_association pas3
		where pas3.data_name_fk = 159100))
)



-- 4) Find products where epaCUBE Status = Hold AND product IS in ICSP and/or ICSW	
--	  Remove Catalog System Association 
delete from epacube.product_association
where data_name_fk = 159200
and entity_structure_fk in ( 
	select entity_structure_fk from epacube.entity_identification
	where data_name_fk = 142111 and value = 'CATALOG')
and product_structure_fk in (
	select distinct pas1.product_structure_fk
	from epacube.product_association pas1
	where pas1.data_name_fk = 159200
	and pas1.entity_structure_fk in (
		select ei1.entity_structure_fk from epacube.entity_identification ei1
		where ei1.data_name_fk = 142111 and ei1.value = 'CATALOG')
	and pas1.product_structure_fk in (
		select product_structure_id from epacube.product_structure
		where product_status_cr_fk = 105)
	and (pas1.product_structure_fk in (
		select pas2.product_structure_fk from epacube.product_association pas2
		where pas2.data_name_fk = 159200
		and pas2.entity_structure_fk in (
			select ei2.entity_structure_fk from epacube.entity_identification ei2
			where ei2.data_name_fk = 142111 and ei2.value = 'PRODUCT'))
	or pas1.product_structure_fk in (
		select distinct pas3.product_structure_fk from epacube.product_association pas3
		where pas3.data_name_fk = 159100))
)

-- Set epaCUBE Status back to ACTIVE
update epacube.product_structure
set product_status_cr_fk = 100
where product_structure_id in (
	select distinct pas1.product_structure_fk
	from epacube.product_association pas1
--	where pas1.data_name_fk = 159200
--	and pas1.entity_structure_fk in (
--		select ei1.entity_structure_fk from epacube.entity_identification ei1
--		where ei1.data_name_fk = 142111 and ei1.value = 'CATALOG')
	where pas1.product_structure_fk in (
		select product_structure_id from epacube.product_structure
		where product_status_cr_fk = 105)
	and (pas1.product_structure_fk in (
		select pas2.product_structure_fk from epacube.product_association pas2
		where pas2.data_name_fk = 159200
		and pas2.entity_structure_fk in (
			select ei2.entity_structure_fk from epacube.entity_identification ei2
			where ei2.data_name_fk = 142111 and ei2.value = 'PRODUCT'))
	or pas1.product_structure_fk in (
		select distinct pas3.product_structure_fk from epacube.product_association pas3
		where pas3.data_name_fk = 159100))
)

  EXEC common.job_execution_create   @v_job_fk, 'START PURGING DATA',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate 

-- 5) Run purge procedure
exec epacube.nightly_purge


    EXEC common.job_execution_create   @v_job_fk, 'JOB COMPLETE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate 

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of epacube.UTIL_REMOVE_ICSC'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = NULL,  @epa_batch_no = NULL
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of epacube.UTIL_REMOVE_ICSC has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END






































































































































































































