﻿-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get product attribute data name information JIRA (PRE-110)
-- Get all product attribute data name information and return in JSON format
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE   PROCEDURE [epacube].[TAXONOMY_ATTRIBUTE_GET_DATA_NAME]
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.TAXONOMY_ATTRIBUTE_GET_DATA_NAME.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT DATA_NAME_ID
			, CR.CODE AS DATA_TYPE
			, LABEL
		FROM epacube.DATA_NAME DN
			INNER JOIN epacube.DATA_SET DS
				ON DN.DATA_SET_FK = DS.DATA_SET_ID
			INNER JOIN epacube.CODE_REF CR
				ON DS.DATA_TYPE_CR_FK = CR.CODE_REF_ID
		WHERE DS.TABLE_NAME = 'PRODUCT_TAX_ATTRIBUTE'
			AND DN.RECORD_STATUS_CR_FK = 1
		ORDER BY LABEL
		FOR JSON PATH, ROOT('DATA')

		SET @ls_stmt = 'Finished execution of epacube.TAXONOMY_ATTRIBUTE_GET_DATA_NAME.';
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		SET @ls_stmt = 'Execution of epacube.TAXONOMY_ATTRIBUTE_GET_DATA_NAME has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

	END CATCH
END