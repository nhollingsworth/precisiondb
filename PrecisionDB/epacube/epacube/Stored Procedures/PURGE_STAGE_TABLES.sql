﻿



--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG		 04/12/2010  Breaking out of parent procedure
-- RG		 04/14/2010  EPA-2989 fixing poor null logic
-- CV        05/26/2010  Added Distinct to select statement
-- KS        06/02/2010  EPA-3011 Removing the the notion of "Completed Jobs" and using instead the criteria below:
--							STAGE DATA Cleansed Indicator = 1 AND Import Effective Date <= @purge_date_max ( getdate() - 'PURGE IMPORT JOBS AFTER' )
--

CREATE PROCEDURE [epacube].[PURGE_STAGE_TABLES] @purge_immediate_ind INT = 0 
AS
BEGIN
		
		
		
		DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        
		DECLARE @grace_days INT         
        DECLARE @purge_date_max DATETIME        
        DECLARE	@epacube_Params_Grace_Period int
		DECLARE @calculated_Grace_Period int
		DECLARE @display_Grace_Period varchar(20)
		DECLARE @appscope INT		

------		DECLARE @job_Complete_Status_Code INT
		
		BEGIN TRY
            SET NOCOUNT ON ;
            SET @l_exec_no = 0
            SET @l_sysdate = GETDATE()
			SET @grace_days = 7
			
			SET @appscope = (select application_scope_id
							 from epacube.application_scope with (NOLOCK)
							 where scope = 'APPLICATION')
			SET @epacube_Params_Grace_Period = (select CAST ( eep.value as int )
												from epacube.epacube_params eep with (NOLOCK)
												where  eep.name = 'PURGE IMPORT JOBS AFTER'
												and application_scope_fk = @appscope)
			SET @calculated_Grace_Period = CASE @purge_immediate_ind
			                               WHEN 1 THEN 0
			                               ELSE (isnull(@epacube_Params_Grace_Period,@grace_days))
			                               END 						
			SET @purge_date_max = DATEADD(day, -@calculated_Grace_Period, @l_sysdate)
			SET @display_Grace_Period = CAST(ISNULL(@calculated_Grace_Period, 0) AS VARCHAR(20))						

--------			SET @job_Complete_Status_Code = epacube.getCodeRefId('JOB_STATUS','COMPLETE')

----------------------------------------------- begin stage /clansertables

SET @ls_stmt = 'started execution of epacube.PURGE_STAGE_TABLES. '
                + 'Grace days = '
                + @display_Grace_Period + ' days.'
                
            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;



--------			DECLARE @CompletedCleanserJobs TABLE
--------
--------				(
--------				CLEANSER_JOB_ID bigint
--------				)
--------
--------			INSERT INTO @CompletedCleanserJobs (CLEANSER_JOB_ID)
--------
--------			select distinct search_job_fk 
--------from cleanser.search_job_results_best WITH (NOLOCK)
--------			inner join common.job_execution WITH (NOLOCK)
-------- on (search_job_fk = job_fk and job_status_cr_fk = @job_Complete_Status_Code)
--------			where search_job_fk not in 
--------							(select distinct search_job_fk
--------							from cleanser.search_job_results_best WITH (NOLOCK)
--------							where isnull(cleansed_indicator,0) <> 1)

			                                  
--STG RECORD DATA
--------                    DELETE  FROM stage.stg_record_data
--------                    WHERE   job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)                            

                   DELETE 
                   FROM stage.STG_RECORD_DATA
                   WHERE STG_RECORD_FK IN (
							SELECT SJRB.BEST_SEARCH_FK
							FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
							INNER JOIN stage.STG_RECORD SREC WITH (NOLOCK )
							 ON ( SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK )
							WHERE ISNULL ( SJRB.CLEANSED_INDICATOR, 0 ) = 1
							AND   utilities.TRUNC(SREC.effective_date) 
							                <=
								   utilities.TRUNC(@purge_date_max) 
                        )                   

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from stage.stg_record_data table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

--STG RECORD STRUCTURE
--------					DELETE  FROM stage.stg_record_structure
--------                    WHERE   job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)
                            
                   DELETE 
                   FROM stage.STG_RECORD_STRUCTURE
                   WHERE STG_RECORD_FK IN (
							SELECT SJRB.BEST_SEARCH_FK
							FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
							INNER JOIN stage.STG_RECORD SREC WITH (NOLOCK )
							 ON ( SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK )
							WHERE ISNULL ( SJRB.CLEANSED_INDICATOR, 0 ) = 1
							AND   utilities.TRUNC(SREC.effective_date) 
							                <=
								   utilities.TRUNC(@purge_date_max) 
                        )                   

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from stage.stg_record_structure table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
--STG RECORD ENTITY IDENT
--------                    DELETE  FROM stage.stg_record_entity_ident
--------                    WHERE   job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)

                   DELETE 
                   FROM stage.STG_RECORD_ENTITY_IDENT
                   WHERE STG_RECORD_FK IN (
							SELECT SJRB.BEST_SEARCH_FK
							FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
							INNER JOIN stage.STG_RECORD SREC WITH (NOLOCK )
							 ON ( SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK )
							WHERE ISNULL ( SJRB.CLEANSED_INDICATOR, 0 ) = 1
							AND   utilities.TRUNC(SREC.effective_date) 
							                <=
								   utilities.TRUNC(@purge_date_max) 
                        )                   

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from stage.stg_record_entity_ident table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
--STG RECORD ENTITY					
--------					DELETE  FROM stage.stg_record_entity
--------                    WHERE   job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)

                   DELETE 
                   FROM stage.STG_RECORD_ENTITY
                   WHERE STG_RECORD_FK IN (
							SELECT SJRB.BEST_SEARCH_FK
							FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
							INNER JOIN stage.STG_RECORD SREC WITH (NOLOCK )
							 ON ( SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK )
							WHERE ISNULL ( SJRB.CLEANSED_INDICATOR, 0 ) = 1
							AND   utilities.TRUNC(SREC.effective_date) 
							                <=
								   utilities.TRUNC(@purge_date_max) 
                        )                   

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from stage.stg_record_entity table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
                        
--STG RECORD IDENT	
--------					DELETE  FROM stage.stg_record_ident
--------                    WHERE   job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)
                   DELETE 
                   FROM stage.STG_RECORD_IDENT
                   WHERE STG_RECORD_FK IN (
							SELECT SJRB.BEST_SEARCH_FK
							FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
							INNER JOIN stage.STG_RECORD SREC WITH (NOLOCK )
							 ON ( SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK )
							WHERE ISNULL ( SJRB.CLEANSED_INDICATOR, 0 ) = 1
							AND   utilities.TRUNC(SREC.effective_date) 
							                <=
								   utilities.TRUNC(@purge_date_max) 
                        )                   

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from stage.stg_record_ident table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
--STG RECORD ERRORS
--------					DELETE  FROM stage.stg_record_errors
--------                    WHERE   job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)

                   DELETE 
                   FROM stage.STG_RECORD_ERRORS
                   WHERE STG_RECORD_FK IN (
							SELECT SJRB.BEST_SEARCH_FK
							FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
							INNER JOIN stage.STG_RECORD SREC WITH (NOLOCK )
							 ON ( SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK )
							WHERE ISNULL ( SJRB.CLEANSED_INDICATOR, 0 ) = 1
							AND   utilities.TRUNC(SREC.effective_date) 
							                <=
								   utilities.TRUNC(@purge_date_max) 
                        )                   

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from stage.stg_record_errors table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
                        
-------------------------------------------------------
-- Now for the Cleanser tables:	             
-------------------------------------------------------
--SEARCH JOB FROM TO MATCH
--------					DELETE  FROM cleanser.search_job_from_to_match
--------                    WHERE   search_job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)

                   DELETE 
                   FROM cleanser.SEARCH_JOB_FROM_TO_MATCH
                   WHERE TO_SEARCH_FK IN (
							SELECT SJRB.BEST_SEARCH_FK
							FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
							INNER JOIN stage.STG_RECORD SREC WITH (NOLOCK )
							 ON ( SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK )
							WHERE ISNULL ( SJRB.CLEANSED_INDICATOR, 0 ) = 1
							AND   utilities.TRUNC(SREC.effective_date) 
							                <=
								   utilities.TRUNC(@purge_date_max) 
                        )                   
                            
                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from cleanser.search_job_from_to_match table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
                        
-- SEARCH JOB RESULTS IDENT
--------					DELETE  FROM cleanser.search_job_results_ident
--------                    WHERE   search_job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)
                            
                   DELETE 
                   FROM cleanser.SEARCH_JOB_RESULTS_IDENT
                   WHERE TO_SEARCH_FK IN (
							SELECT SJRB.BEST_SEARCH_FK
							FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
							INNER JOIN stage.STG_RECORD SREC WITH (NOLOCK )
							 ON ( SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK )
							WHERE ISNULL ( SJRB.CLEANSED_INDICATOR, 0 ) = 1
							AND   utilities.TRUNC(SREC.effective_date) 
							                <=
								   utilities.TRUNC(@purge_date_max) 
                        )                   

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from cleanser.search_job_results_ident table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
                                                

--SEARCH JOB RESULTS BEST
--------					DELETE  FROM cleanser.search_job_results_best
--------                    WHERE   search_job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)
                            
                   DELETE 
                   FROM cleanser.SEARCH_JOB_RESULTS_BEST
                   WHERE BEST_SEARCH_FK IN (
							SELECT SJRB.BEST_SEARCH_FK
							FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
							INNER JOIN stage.STG_RECORD SREC WITH (NOLOCK )
							 ON ( SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK )
							WHERE ISNULL ( SJRB.CLEANSED_INDICATOR, 0 ) = 1
							AND   utilities.TRUNC(SREC.effective_date) 
							                <=
								   utilities.TRUNC(@purge_date_max) 
                        )                   


                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from cleanser.search_job_results_best table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

-- Finally, delete primary keys from stage.STG_RECORD table  
--		WHERE NO cleanser.search_job_results_best ROW EXISTS


--STG RECORD	
----					DELETE  FROM stage.stg_record
----                    WHERE   job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)

                   DELETE 
                   FROM stage.STG_RECORD
                   WHERE STG_RECORD_ID NOT IN (
							SELECT SJRB.BEST_SEARCH_FK
							FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
                        )                   
                            

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from stage.stg_record table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

                        
/*

KS  JOBS ARE TO BE DELETED IN THE MAIN CALLING PROCEDURE PURGE_IMPORT_TABLES 
    STRING AND TASK TABLES ARE NOT CURRENTLY USED


--SEARCH JOB RESULTS TASK WORK
--------					DELETE  FROM cleanser.search_job_results_task_work
--------                    WHERE   search_job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)
                            
                   DELETE 
                   FROM cleanser.SEARCH_JOB_RESULTS_TASK_WORK
                   WHERE BEST_SEARCH_FK IN (
							SELECT SJRB.BEST_SEARCH_FK
							FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
							INNER JOIN stage.STG_RECORD SREC WITH (NOLOCK )
							 ON ( SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK )
							WHERE ISNULL ( SJRB.CLEANSED_INDICATOR, 0 ) = 1
							AND   utilities.TRUNC(SREC.effective_date) 
							                <=
								   utilities.TRUNC(@purge_date_max) 
                        )                   

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from cleanser.search_job_results_task_work table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END
                        
--SEARCH JOB RESULTS TASK
--------					DELETE  FROM cleanser.search_job_results_task
--------                    WHERE   search_job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)

                   DELETE 
                   FROM cleanser.SEARCH_JOB_RESULTS_TASK
                   WHERE BEST_SEARCH_FK IN (
							SELECT SJRB.BEST_SEARCH_FK
							FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
							INNER JOIN stage.STG_RECORD SREC WITH (NOLOCK )
							 ON ( SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK )
							WHERE ISNULL ( SJRB.CLEANSED_INDICATOR, 0 ) = 1
							AND   utilities.TRUNC(SREC.effective_date) 
							                <=
								   utilities.TRUNC(@purge_date_max) 
                        )                   
                            
                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from cleanser.search_job_results_task table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END

                        
--SEARCH JOB STRING WORK
					DELETE  FROM cleanser.SEARCH_JOB_STRING_WORK
                    WHERE   search_job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)
                            

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from cleanser.search_job_string_work table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END      
                        
--SEARCH JOB STRING
					DELETE  FROM cleanser.SEARCH_JOB_STRING
                    WHERE   search_job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)
                            

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from cleanser.search_job_string table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END  
                        
                        
--SEARCH JOB DATA NAME
					DELETE  FROM cleanser.SEARCH_JOB_DATA_NAME
                    WHERE   search_job_fk in (select CLEANSER_JOB_ID from @CompletedCleanserJobs)
                            

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from cleanser.search_job_data_name table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END  
                        
                                                                                      
*/ 


------------------------------ end stage tables

------- End Purging Stage tables
 SET @status_desc = 'finished execution of epacube.PURGE_STAGE_TABLES'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;

        END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.PURGE_STAGE_TABLES has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH
		  
    END






