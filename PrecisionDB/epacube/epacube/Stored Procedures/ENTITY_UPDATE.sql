﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create entities JIRA (URM-1185)
-- 1. Parse the JSON object into a temp table
-- 2. Determine the tables that need to be updated
-- 3. Dynamically generate the update statement(s)
-- 4. Execute the update statement(s) to update the entity 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[ENTITY_UPDATE]
	-- Add the parameters for the stored procedure here
	@entity NVARCHAR(MAX)
	,@user VARCHAR(64)
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @update_statement NVARCHAR(MAX);
	DECLARE @update_count INT;
	DECLARE @updates_complete INT;
	
	SET NOCOUNT ON;

	--***************************************************************************************************************************************
	--Example entity structure
	--{
	--	"entityStructureId": 45034,
	--	"data": [
	--		{
	--			"dataNameId": 144111,
	--			"dataValue": "18405"
	--		},
	--		{
	--			"dataNameId": 144112,
	--			"dataValue": "WHEATBERRIES BAKESHOP (B)"
	--		}
	--	]
	--}
	--***************************************************************************************************************************************

    --Initialize log
	SET @ls_stmt = 'Started execution of epacube.ENTITY_UPDATE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;


	DECLARE @updates TABLE (
		UPDATE_ID INT IDENTITY(1,1),
		UPDATE_STATEMENT NVARCHAR(MAX)
	);

	WITH Entity_CTE (ENTITY_STRUCTURE_ID, DATA_NAME_ID, DATA_VALUE) 
	AS (
		SELECT *
		FROM OPENJSON(@entity)
			WITH (
				ENTITY_STRUCTURE_ID BIGINT '$.entityStructureId'
			) e
		CROSS APPLY OPENJSON(@entity, '$.data')
			WITH (
				DATA_NAME_ID INT '$.dataNameId',
				DATA_VALUE VARCHAR(128) '$.dataValue'
			)
	)

	,Entity_With_Table_CTE (ENTITY_STRUCTURE_ID, DATA_NAME_ID, DATA_VALUE, TABLE_NAME)
	AS ( 
		SELECT e.ENTITY_STRUCTURE_ID
			, e.DATA_NAME_ID
			, e.DATA_VALUE
			, 'epacube.' + ds.TABLE_NAME
		FROM Entity_CTE e
			INNER JOIN epacube.DATA_NAME dn
				ON e.DATA_NAME_ID = dn.DATA_NAME_ID
			INNER JOIN epacube.DATA_SET ds
				ON dn.DATA_SET_FK = ds.DATA_SET_ID
	)

	INSERT INTO @updates (UPDATE_STATEMENT)
	SELECT 'UPDATE ' + TABLE_NAME + ' SET [VALUE] = ''' + DATA_VALUE + ''' , UPDATE_TIMESTAMP = ''' + CONVERT(CHAR(23), GETDATE(), 121) + ''' WHERE ENTITY_STRUCTURE_FK = ' + CAST(ENTITY_STRUCTURE_ID AS VARCHAR(20)) + ' AND DATA_NAME_FK = ' + CAST(DATA_NAME_ID AS VARCHAR(10))
	FROM Entity_With_Table_CTE;

	SELECT @update_count = COUNT(*) FROM @updates;
	SET @updates_complete = 1;

	WHILE @updates_complete <= @update_count
	BEGIN TRY
		BEGIN TRANSACTION

			SELECT @update_statement = UPDATE_STATEMENT
			FROM @updates
			WHERE UPDATE_ID = @updates_complete;

			EXEC(@update_statement);

			SET @updates_complete = @updates_complete + 1;

		COMMIT TRANSACTION

		SET @ls_stmt = 'Update statement completed successfully: ' + @update_statement; 
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 
						
		SET @ls_stmt = 'Execution of epacube.ENTITY_UPDATE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

		RAISERROR('Fatal error occurred in the ENTITY_UPDATE process.', 20, -1) WITH LOG;

	END CATCH
END
