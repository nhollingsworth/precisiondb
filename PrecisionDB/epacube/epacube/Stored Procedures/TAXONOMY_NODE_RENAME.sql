﻿-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Rename taxonomy node JIRA (PRE-142)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE   PROCEDURE [epacube].[TAXONOMY_NODE_RENAME]
	@taxonomy_node_id INT
	,@taxonomy_node_name VARCHAR(256)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @data_value_id_local BIGINT;

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.TAXONOMY_NODE_RENAME.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT @data_value_id_local = DATA_VALUE_FK
		FROM epacube.TAXONOMY_NODE
		WHERE TAXONOMY_NODE_ID = @taxonomy_node_id

		BEGIN TRANSACTION;

			UPDATE epacube.DATA_VALUE 
			SET [VALUE] = @taxonomy_node_name
				, [DESCRIPTION] = @taxonomy_node_name
			WHERE DATA_VALUE_ID = @data_value_id_local;


		COMMIT TRANSACTION;

		SET @ls_stmt = 'Finished execution of epacube.TAXONOMY_NODE_RENAME.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 

		SET @ls_stmt = 'Execution of epacube.TAXONOMY_NODE_RENAME has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END