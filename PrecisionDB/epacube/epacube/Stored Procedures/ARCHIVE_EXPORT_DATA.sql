﻿




CREATE PROCEDURE [epacube].[ARCHIVE_EXPORT_DATA] (@in_package_fk int, @in_job_fk bigint)

AS
BEGIN
-- Copyright 2006 epaCUBE, Inc.
--
-- Procedure created by Walt Tucholski 
--
--
-- Purpose: Archive export data to history table. Called from SSIS packages.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- WT        10/13/2006   Creation date
-- 

DECLARE 
@l_sysdate datetime,
@ls_stmt varchar(1000),
@l_exec_no float(53),
@status_desc varchar(max)
     
  SET @l_exec_no = 0;
  EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of epacube.ARCHIVE_EXPORT_DATA', 
											@exec_id = @l_exec_no OUTPUT;

BEGIN TRY


IF @in_package_fk IS NOT NULL AND @in_job_fk IS NOT NULL 
BEGIN

--- Archive the exported data to history table and remove from export_product_200 table

	INSERT INTO export.export_product_200_history 
	SELECT ep2.*
		FROM export.export_product_200 ep2
		WHERE ep2.job_fk = @in_job_fk
		  and ep2.export_package_fk = @in_package_fk

	IF @@ROWCOUNT > 0
		DELETE FROM export.export_product_200
		WHERE job_fk = @in_job_fk
		  and export_package_fk = @in_package_fk

END; -- IF @in_package_fk IS NOT NULL AND @in_job_fk IS NOT NULL

SET @status_desc =  'finished execution of epacube.ARCHIVE_EXPORT_DATA';
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of epacube.ARCHIVE_EXPORT_DATA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END











































