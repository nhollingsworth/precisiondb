﻿






--					Move to Archive Event history from Event data from Event tables 

--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV		 09/27/2010  To help with Big AB load
--
--

CREATE PROCEDURE [epacube].[ARCHIVE_EVENT_DATA_HISTORY_DELETE] 
AS
BEGIN

        DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @job_timestamp DATETIME

				
----------------------------------------------------------
--logging
----------------------------------------------------------
		
SET @ls_stmt = 'started execution of epacube.ARCHIVE_EVENT_DATA_HISTORY_DELETE. '
                
            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;


--ARCHIVE EVENTS FROM HISTORY                        
					INSERT INTO synchronizer.EVENT_DATA_ARCHIVE (
		 EVENT_TYPE_CR_FK
		,EVENT_ENTITY_CLASS_CR_FK 
		,EPACUBE_ID    
		,IMPORT_JOB_FK     
		,IMPORT_PACKAGE_FK 
		,DATA_NAME_FK	
	    ,PRODUCT_STRUCTURE_FK
	    ,ORG_ENTITY_STRUCTURE_FK
	    ,ENTITY_STRUCTURE_FK    
		,ENTITY_CLASS_CR_FK     
		,ENTITY_DATA_NAME_FK    
        ,NEW_DATA         
        ,CURRENT_DATA     
		,DATA_VALUE_FK    
		,ENTITY_DATA_VALUE_FK
        ,NET_VALUE1_NEW      
        ,NET_VALUE2_NEW      
        ,NET_VALUE3_NEW      
        ,NET_VALUE4_NEW      
        ,NET_VALUE5_NEW      
        ,NET_VALUE6_NEW      
        --,NET_VALUE7_NEW      
        --,NET_VALUE8_NEW      
        --,NET_VALUE9_NEW      
        --,NET_VALUE10_NEW     
        ,NET_VALUE1_CUR      
        ,NET_VALUE2_CUR      
        ,NET_VALUE3_CUR     
        ,NET_VALUE4_CUR     
        ,NET_VALUE5_CUR     
        ,NET_VALUE6_CUR     
        --,NET_VALUE7_CUR     
        --,NET_VALUE8_CUR     
        --,NET_VALUE9_CUR     
        --,NET_VALUE10_CUR    
		,PERCENT_CHANGE1	 
		,PERCENT_CHANGE2	
		,PERCENT_CHANGE3	
		,PERCENT_CHANGE4	
		,PERCENT_CHANGE5	
		,PERCENT_CHANGE6	
		--,PERCENT_CHANGE7	
		--,PERCENT_CHANGE8	
		--,PERCENT_CHANGE9	
		--,PERCENT_CHANGE10
        ,VALUE_UOM_CODE_FK 
        ,ASSOC_UOM_CODE_FK 
		,UOM_CONVERSION_FACTOR 
		,END_DATE_NEW          
        ,END_DATE_CUR          
		,EFFECTIVE_DATE_CUR    
		,EVENT_EFFECTIVE_DATE 
		,VALUE_EFFECTIVE_DATE
        ,RELATED_EVENT_FK	
        ,RULES_FK			
		,EVENT_ACTION_CR_FK   
		,EVENT_STATUS_CR_FK   
		,EVENT_CONDITION_CR_FK
		,EVENT_PRIORITY_CR_FK   
		,EVENT_SOURCE_CR_FK   
		,RESULT_TYPE_CR_FK	 
		,SINGLE_ASSOC_IND     
		,TABLE_ID_FK          
		,BATCH_NO			
		,IMPORT_BATCH_NO     
        ,JOB_CLASS_FK		
        ,IMPORT_DATE         
        ,IMPORT_FILENAME     
        ,STG_RECORD_FK       
		,RECORD_STATUS_CR_FK 
        ,UPDATE_TIMESTAMP	
        ,UPDATE_USER		    
     )
SELECT 
		 EVENT_TYPE_CR_FK
		,EVENT_ENTITY_CLASS_CR_FK 
		,EPACUBE_ID    
		,IMPORT_JOB_FK     
		,IMPORT_PACKAGE_FK 
		,DATA_NAME_FK	
	    ,PRODUCT_STRUCTURE_FK
	    ,ORG_ENTITY_STRUCTURE_FK
	    ,ENTITY_STRUCTURE_FK    
		,ENTITY_CLASS_CR_FK     
		,ENTITY_DATA_NAME_FK    
        ,NEW_DATA         
        ,CURRENT_DATA     
		,DATA_VALUE_FK    
		,ENTITY_DATA_VALUE_FK
        ,NET_VALUE1_NEW      
        ,NET_VALUE2_NEW      
        ,NET_VALUE3_NEW      
        ,NET_VALUE4_NEW      
        ,NET_VALUE5_NEW      
        ,NET_VALUE6_NEW      
        --,NET_VALUE7_NEW      
        --,NET_VALUE8_NEW      
        --,NET_VALUE9_NEW      
        --,NET_VALUE10_NEW     
        ,NET_VALUE1_CUR      
        ,NET_VALUE2_CUR      
        ,NET_VALUE3_CUR     
        ,NET_VALUE4_CUR     
        ,NET_VALUE5_CUR     
        ,NET_VALUE6_CUR     
        --,NET_VALUE7_CUR     
        --,NET_VALUE8_CUR     
        --,NET_VALUE9_CUR     
        --,NET_VALUE10_CUR    
		,PERCENT_CHANGE1	 
		,PERCENT_CHANGE2	
		,PERCENT_CHANGE3	
		,PERCENT_CHANGE4	
		,PERCENT_CHANGE5	
		,PERCENT_CHANGE6	
		--,PERCENT_CHANGE7	
		--,PERCENT_CHANGE8	
		--,PERCENT_CHANGE9	
		--,PERCENT_CHANGE10
        ,VALUE_UOM_CODE_FK 
        ,ASSOC_UOM_CODE_FK 
		,UOM_CONVERSION_FACTOR 
		,END_DATE_NEW          
        ,END_DATE_CUR          
		,EFFECTIVE_DATE_CUR    
		,EVENT_EFFECTIVE_DATE 
		,VALUE_EFFECTIVE_DATE
        ,RELATED_EVENT_FK	
        ,RULES_FK			
		,EVENT_ACTION_CR_FK   
		,EVENT_STATUS_CR_FK   
		,EVENT_CONDITION_CR_FK
		,EVENT_PRIORITY_CR_FK   
		,EVENT_SOURCE_CR_FK   
		,RESULT_TYPE_CR_FK	 
		,SINGLE_ASSOC_IND     
		,TABLE_ID_FK          
		,BATCH_NO			
		,IMPORT_BATCH_NO     
        ,JOB_CLASS_FK		
        ,IMPORT_DATE         
        ,IMPORT_FILENAME     
        ,STG_RECORD_FK       
		,RECORD_STATUS_CR_FK 
        ,UPDATE_TIMESTAMP	
        ,UPDATE_USER	
         FROM synchronizer.event_data_history
                   
-----REMOVE ALL EVENTS
												
					DELETE FROM synchronizer.event_data_history			
				
-------------------------------------------------------------------------------- 									

                    SET @l_rows_processed = @@ROWCOUNT
                    IF @l_rows_processed > 0 
                        BEGIN
                            SET @status_desc = 'Total rows deleted from synchronizer.event_data_history table = '
                                + CAST(ISNULL(@l_rows_processed, 0) AS VARCHAR(30))
                            EXEC exec_monitor.Report_Status_sp @l_exec_no,
                                @status_desc
                        END                        


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

            SET @status_desc = 'finished execution of SYNCHRONIZER.epacube.ARCHIVE_EVENT_HISTORY_DELETE'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;





            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.ARCHIVE_EVENT_HISTORY_DELETE has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
			set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;

		  
    END








