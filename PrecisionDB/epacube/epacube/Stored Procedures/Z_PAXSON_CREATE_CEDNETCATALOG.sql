﻿






CREATE PROCEDURE [epacube].[Z_PAXSON_CREATE_CEDNETCATALOG] ( @IN_JOB_FK bigint ) 
         
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        10/22/2012  Initial SQL Version
-- CV		 11/22/2012	 Now include changes not only new values
-- CV        12/11/2012  Change base data name to 820103
-- CV        01/20/2013  Add check for isAlternet and isGeneric Flags 
-- CV        07/25/2013  added inner join to product structure to make sure it exist still
-- CV        08/02/2013  added call to remove -999999999 values
 
  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @v_batch_no   bigint
        
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT
        
 
 
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of epacube.Paxson_Create_CEDNETCATALOG. '
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT
											  ,@epa_job_id = @v_job_fk;

											  
	EXEC seq_manager.db_get_next_sequence_value_impl 'common','batch_seq', @v_batch_no output
 

                
                  EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'job_seq', @v_job_fk output
    
    SET @v_job_class_fk  = 100        --- PRODUCT MAINTENANCE
    SET @v_job_user = 'EPACUBE'
    SET @v_job_name = 'DERIVE CEDNETCATALOG'
    SET @v_job_date = @l_sysdate
    SET @v_data1   = 'LAST JOB:: '
                     + CAST ( ISNULL ( @v_last_job_fk, -999 )  AS VARCHAR(32) )
                     + ' - '
                     + CAST ( ISNULL ( @v_last_job_date, '01/01/2000' ) AS VARCHAR(64) )
    SET @v_data2   = @v_last_job_date
    
    EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
										   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

    EXEC common.job_execution_create   @v_job_fk, 'START DERIVE CEDNETCATALOG ',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  

                

---------------------------------------------------------------------------------------
----CREATE TEMP TABLE FOR PRODUCT LIST-----
---------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_MISSING_CEDNETCATALOG') is not null
	   drop table #TS_MISSING_CEDNETCATALOG;
	   
	      
					CREATE TABLE #TS_MISSING_CEDNETCATALOG 
					 (PRODUCT_STRUCTURE_FK bigint Not NULL
					 ,ENTITY_STRUCTURE_FK bigint
					 ,PFMS_CATALOG varchar(150)
					 ,HAS_FRACTION int
					 ,REMOVE_FRACTION int
					 ,REMOVE_DASHES int
					 ,REMOVE_SLASHES int
					 ,NEW_DATA varchar(150)
					 ,JOB_FK int)

					
----------------------------------------------------------------------------
--CREATE INDEXES
---------------------------------------------------------------------------


	CREATE NONCLUSTERED INDEX [IDX_TSMC_ESF] ON #TS_MISSING_CEDNETCATALOG 
	(ENTITY_STRUCTURE_FK ASC
		)
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

    CREATE NONCLUSTERED INDEX [IDX_TSMC_RD_HF_ND] ON #TS_MISSING_CEDNETCATALOG 
	(REMOVE_DASHES ASC
	,HAS_FRACTION ASC
	,NEW_DATA ASC
		)
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)
	
	CREATE NONCLUSTERED INDEX [IDX_TSMC_ND] ON #TS_MISSING_CEDNETCATALOG 
	(NEW_DATA ASC
		)
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

　 CREATE NONCLUSTERED INDEX [IDX_TSMC_RD_HF] ON #TS_MISSING_CEDNETCATALOG 
	(REMOVE_SLASHES ASC
	,HAS_FRACTION ASC
		)
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


------------------------------------------------------------------------------
-----Inserts product structure for products that are missing CEDNETCATALOG
------------------------------------------------------------------------------
							
							insert into #TS_MISSING_CEDNETCATALOG
							(PRODUCT_STRUCTURE_FK
							,ENTITY_STRUCTURE_FK
							,PFMS_CATALOG
							,HAS_FRACTION
							,REMOVE_FRACTION
							,REMOVE_DASHES
							,REMOVE_SLASHES
							,NEW_DATA
							,JOB_FK)
							(select
							pa.product_structure_fk 
							,pa.ENTITY_STRUCTURE_FK
							,(select value from epacube.PRODUCT_IDENTIFICATION pin where pin.DATA_NAME_FK = 820103
							and pin.PRODUCT_STRUCTURE_FK = pa.product_structure_fk)
							,Case when (select value from epacube.PRODUCT_IDENTIFICATION pin where pin.DATA_NAME_FK = 820103
							and pin.PRODUCT_STRUCTURE_FK = pa.product_structure_fk) like '%/%' then 1 else NULL end-- has fraction
							,NULL
							,NULL-- has dashes
							,NULL -- remove slashes
							,NULL -- new_data
							,@V_OUT_JOB_FK
							--- 
								from epacube.PRODUCT_ASSOCIATION pa  with (nolock)
								inner join epacube.PRODUCT_CATEGORY pc with (nolock)
								on (pa.PRODUCT_STRUCTURE_FK = pc.PRODUCT_STRUCTURE_FK
								and pc.DATA_NAME_FK = 4104006)  --- CED Mfr Type
								inner join epacube.DATA_VALUE dv with (nolock)
								on (dv.DATA_VALUE_ID = pc.DATA_VALUE_FK
								and value = 'BRANDED') 
								inner join epacube.PRODUCT_ATTRIBUTE pat with (nolock)
								on (pat.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
								and pat.DATA_NAME_FK  = 100140801 --CED PFMS CATALOG
								and pat.ATTRIBUTE_EVENT_DATA = 'TRADE SERVICE')
							    left join epacube.product_attribute paf with (nolock)
								on (paf.product_structure_fk = pa.product_structure_fk
								and paf.data_name_fk not in (100110801,100110802)   --isalternate isgeneric flag
								and isnull(paf.attribute_event_data,'N') ='N' )
								where pa.DATA_NAME_FK = 159300 --PRIMARY VENDOR
																AND pa.PRODUCT_STRUCTURE_FK Not in (
							 select distinct PRODUCT_STRUCTURE_FK
							 from epacube.PRODUCT_IDENT_NONUNIQUE with (nolock) where DATA_NAME_FK = 4101001))

						
	

	 SET @l_rows_processed = @@ROWCOUNT

	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows insert- with Missing CED_Net_Catalog = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @v_job_fk
       END


---------------------------------------------------------------------------------------------
-----Inserts product structure for products that have changes from trade import job
----------------------------------------------------------------------------------------------
							
							insert into #TS_MISSING_CEDNETCATALOG
							(PRODUCT_STRUCTURE_FK
							,ENTITY_STRUCTURE_FK
							,PFMS_CATALOG
							,JOB_FK)
							 select edh.product_structure_fk
							 ,edh.entity_structure_fk
							 ,NEW_DATA
						     ,@V_OUT_JOB_FK
							 from synchronizer.EVENT_DATA_HISTORY edh with (nolock)
							 inner join epacube.PRODUCT_STRUCTURE ps with (nolock)
							 on (ps.PRODUCT_STRUCTURE_ID = edh.PRODUCT_STRUCTURE_FK)
							 left join epacube.product_attribute paf with (nolock)
								on (paf.product_structure_fk = edh.product_structure_fk
								and paf.data_name_fk not in (100110801,100110802)   --isalternate isgeneric flag
								and isnull(paf.attribute_event_data,'N') ='N' )
							 where  import_job_fk = @IN_JOB_FK 
							 and edh.data_name_fk = 820103
							 and edh.EVENT_STATUS_CR_FK = 81

	 
							 
							 
	 SET @l_rows_processed = @@ROWCOUNT

	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows insert - with change or new CED_Net_Catalog = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @v_job_fk
       END

---------------------------------------------------------------------------------------------------------------------
---  check if dashes need to be removed per vendor and slashes need to be removed per vendor
---------------------------------------------------------------------------------------------------------------------   
  --DASHES
   
    UPDATE #TS_MISSING_CEDNETCATALOG
	set REMOVE_DASHES = 1 
	where ENTITY_STRUCTURE_FK in (
	select entity_structure_fk from epacube.entity_attribute where DATA_NAME_FK = 100143802  --CED REMOVE DASHES FLAG
								and ATTRIBUTE_EVENT_DATA = 'Y')

	SET @l_rows_processed = @@ROWCOUNT

	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows remove dashes = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @v_job_fk
       END


---SLASHES

	UPDATE #TS_MISSING_CEDNETCATALOG
	set REMOVE_SLASHES = 1 
	where ENTITY_STRUCTURE_FK in (
	select entity_structure_fk from epacube.entity_attribute where DATA_NAME_FK = 100143819	--CED REMOVE SLASHES FLAG
								and ATTRIBUTE_EVENT_DATA = 'Y')

								SET @l_rows_processed = @@ROWCOUNT

	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows remove slashes = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @v_job_fk
       END


---FRACTION FLAG

	UPDATE #TS_MISSING_CEDNETCATALOG
	set REMOVE_FRACTION = 1 
	where ENTITY_STRUCTURE_FK in (
	select entity_structure_fk from epacube.entity_attribute where DATA_NAME_FK = 100143801  ---'CED ADD DASH TO FRACTION FLAG'
								and ATTRIBUTE_EVENT_DATA = 'Y') 
								SET @l_rows_processed = @@ROWCOUNT

	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows with Fractions = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @v_job_fk
       END



								
------------------------------------------------------------------------------------------------------------------------
--1 - remove dashes
-------------------------------------------------------------------------------------------------------------------------

    Update #TS_MISSING_CEDNETCATALOG
	SET NEW_DATA = replace(PFMS_CATALOG,'-','')
	where isnull(REMOVE_DASHES,0) = 1

--------------------------------------------------------------------------------------------------------------------------
--2 - No fractions and dashes remove
--------------------------------------------------------------------------------------------------------------------------

    Update #TS_MISSING_CEDNETCATALOG
	SET NEW_DATA = PFMS_CATALOG
	where REMOVE_DASHES is NULL
	and REMOVE_FRACTION is NULL
	and NEW_DATA is Null

---------------------------------------------------------------------------------------------------------------------------
--3 - Fraction work
---------------------------------------------------------------------------------------------------------------------------

-- The beginning fraction cannot start as the first character 
-- The character before and after the “/” (beginning fraction) must be numeric and 1 thru 9 
-- The beginning fraction cannot have two or more digits in BOTH the numerator and denominator
-- The numerator must be less than the denominator in the ending fraction (a fraction could be 9/9, 9/99) 
--· Dash cannot already be present in front of the beginning fraction 
--Examples where a ‘-‘ would be inserted: 
--· AB/C1/2XYZ would be AB/C-1/2XYZ · ABC11/2XYZ would be ABC1-1/2XYZ · ABC8/32XYZ would be ABC-8/32XYZ 

--select EPACUBE.PAXSON_INSERTDASH('LaA1/038x')
--select EPACUBE.PAXSON_INSERTDASH('ABC11/2XYZ')
--select EPACUBE.PAXSON_INSERTDASH('ABC8/32XYZ')

--1/2XYZ would stay 1/2XYZ (beginning fraction is the first character)· ABC19/2XYZ would stay ABC19/2XYZ (the numerator is greater than the denominator in the ending fraction)· ABC1-1/2XYZ would stay ABC1-1/2XYZ (dash already present in the front of the beginning fraction)· ABC18/32XYZ would stay ABC18/32XYZ  
--select EPACUBE.PAXSON_INSERTDASH('1/2XYZ')
--select EPACUBE.PAXSON_INSERTDASH('ABC19/2XYZ')
--select EPACUBE.PAXSON_INSERTDASH('ABC1-1/2XYZ')
--select EPACUBE.PAXSON_INSERTDASH('ABC18/32XYZ')


	-- Insert - Logic - Starts
	-- Recursive function FN_INSERTDASH is called from here to update the / into the PFMS_Catalog
	
	UPDATE #TS_MISSING_CEDNETCATALOG
	SET NEW_DATA = EPACUBE.PAXSON_INSERTDASH(TMP.NEW_DATA)
	FROM #TS_MISSING_CEDNETCATALOG TMP WHERE TMP.HAS_FRACTION = '1'
	AND TMP.REMOVE_FRACTION = 1
	AND CHARINDEX('/', TMP.NEW_DATA, 1) > 1 AND  CHARINDEX('/+/', TMP.NEW_DATA) < 1   

-----------------------------------------------------------------------------------------------------------------------
--4--REMOVE SLASHES WORK
-----------------------------------------------------------------------------------------------------------------------	
	
	UPDATE #TS_MISSING_CEDNETCATALOG
	SET NEW_DATA  =  REPLACE(NEW_DATA, '/','') 
	Where isnull(REMOVE_SLASHES, 0) = 1
	and isnull(HAS_FRACTION, 0) <> 1
	
------------------------------------------------------------------------------------------------------------------------    
--5-- Create events  
-----------------------------------------------------------------------------------------------------------------------
EXEC seq_manager.db_get_next_sequence_value_impl 'common','batch_seq', @v_batch_no output


INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
          
           ,[NEW_DATA]
           ,[CURRENT_DATA]
                    
           ,[SEARCH1]
           ,[SEARCH2]
           ,[SEARCH3]
           ,[SEARCH4]
           ,[SEARCH5]
           ,[SEARCH6]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[EVENT_REASON_CR_FK]
           ,BATCH_NO
           ,[IMPORT_JOB_FK]
           
           ,[TABLE_ID_FK]
          
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
           ( SELECT Distinct
							150
						   ,10109
						   ,GETDATE()
						   ,TMP.product_structure_fk
						   ,4101001
						   ,TMP.PRODUCT_STRUCTURE_FK
						   ,1
				          
           ,tmp.NEW_DATA --NEW DATA
           ,NULL --CURRENT_DATA, varchar,
           ,NULL  --SEARCH1, varchar(128),>
           ,NULL --<SEARCH2, varchar(128),>
           ,NULL --<SEARCH3, varchar(128),>
           ,NULL --SEARCH4, varchar(128),>
           ,NULL --<SEARCH5, varchar(128),>
           ,NULL --<SEARCH6, varchar(128),>
           ,97 --EVENT_CONDITION_CR_FK, int,>
           ,80 --EVENT_STATUS_CR_FK, int,>
           ,66  --EVENT_PRIORITY_CR_FK, int,>
           ,72  --EVENT_SOURCE_CR_FK----- RULE DERIVED
           ,51 --<EVENT_ACTION_CR_FK New Value
           ,144  --ADD? ---EVENT_REASON_CR_FK, int,
		   ,@v_batch_no
           ,TMP.JOB_FK         
           ,NULL --table id
           ,1
           ,GETDATE()
           ,'EPACUBE'
           from  #TS_MISSING_CEDNETCATALOG TMP )

		SET @l_rows_processed = @@ROWCOUNT

	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total CED_Net_Catalog Events Created = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @v_job_fk
       END


------------------------------------------------------------------------------------------------
-- Logging
------------------------------------------------------------------------------------------------

         
            EXEC common.job_execution_create   @v_job_fk, 'CEDNETCATALOG EVENTS CREATED',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  
  
----------------------------------------------------------------------------------
-- CALL APPROVE EVENTS           
------------------------------------------------------------------------------------

EXECUTE [synchronizer].[EVENT_POSTING_IMPORT] @v_batch_no

-------------------------------------------------------------------------------------
-- Call in order to remove the -999999999 values from posted data

EXECUTE  [synchronizer].[PURGE_INACTIVE_AFTER_EXPORT] 

-------------------------------------------------------------------------------
         
  
EXEC common.job_execution_create   @v_job_fk, 'IMPORT COMPLETE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @l_sysdate, @l_sysdate  
  


-------------------------------------------------------------------------------------------------------------

  SET @status_desc = 'finished execution of SYNCHRONIZER.epacube.Paxson_Create_CEDNETCATALOG'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @v_job_fk ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.Paxson_Create_CEDNETCATALOG has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END