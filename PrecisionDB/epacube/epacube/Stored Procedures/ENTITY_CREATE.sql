﻿
-- Copyright 2018 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Create entities JIRA (URM-1159)
-- 1. Parse the JSON object into a temp table
-- 2. Lookup the default variables based on the entity type and the hierarchy
-- 3. Create a new entity structure and return the entity structure id
-- 4. Add the entity identification unique and non-unique values for the new entity structure 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------


CREATE PROCEDURE [epacube].[ENTITY_CREATE] 
	@entity AS NVARCHAR(MAX)
	,@user VARCHAR(64)
	,@new_entity_structure_id BIGINT OUTPUT
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @entity_type VARCHAR(64);
	DECLARE @hierarchy VARCHAR(6);
	DECLARE @parent_entity_structure_id BIGINT;
	DECLARE @new_child_entity_structure_id BIGINT;
	DECLARE @entity_data_name BIGINT;

	SET NOCOUNT ON;

	--***************************************************************************************************************************************
	--Example entity structure
	--{
	--	"entityType": "Customer",
	--	"hierarchy": "Parent",
	--	"parentId": "",
	--	"data": [
	--		{
	--			"dataNameId": 144111,
	--			"dataValue": "12345"
	--		},
	--		{
	--			"dataNameId": 144112,
	--			"dataValue": "Johns Foods"
	--		}
	--	]
	--}
	--***************************************************************************************************************************************

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.ENTITY_CREATE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;
		

	IF OBJECT_ID('tempdb.dbo.#Entity', 'U') IS NOT NULL
	DROP TABLE #Entity;

	SELECT *
	INTO #Entity
	FROM OPENJSON(@entity)
	WITH (
		ENTITY_TYPE VARCHAR(64) '$.entityType',
		HIERARCHY VARCHAR(6) '$.hierarchy',
		PARENT_ID BIGINT '$.parentId'
	) e
	CROSS APPLY OPENJSON(@entity, '$.data')
		WITH (
			DATA_NAME_ID INT '$.dataNameId',
			DATA_VALUE VARCHAR(128) '$.dataValue'
		);

	SELECT DISTINCT @entity_type = ENTITY_TYPE,
		@hierarchy = HIERARCHY,
		@parent_entity_structure_id = NULLIF(PARENT_ID, 0)
	FROM #Entity

	SET @ls_stmt = 'Begin creation of new ' + @entity_type + ' entity.'
	EXEC exec_monitor.Report_Status_sp @l_exec_no,
		@ls_stmt

	BEGIN TRY
		BEGIN TRANSACTION;

			SET @entity_data_name = 
				CASE @entity_type
					WHEN 'Customer' THEN
						CASE @hierarchy
							WHEN 'Parent' THEN 144010
							WHEN 'Child' THEN 144020
						END
					WHEN 'Warehouse' THEN 
						CASE @hierarchy 
							WHEN 'Parent' THEN 141030
							WHEN 'Child' THEN 141000
						END 
					WHEN 'Zone' THEN 151000
					WHEN 'Vendor' THEN 143000
				END;
			
			--Create the entity structure					
			INSERT INTO epacube.ENTITY_STRUCTURE (ENTITY_STATUS_CR_FK, DATA_NAME_FK, LEVEL_SEQ, PARENT_ENTITY_STRUCTURE_FK, ENTITY_CLASS_CR_FK, CREATE_USER)
			SELECT 10200, @entity_data_name, 1, @parent_entity_structure_id, ENTITY_CLASS_CR_FK, @user
			FROM epacube.DATA_SET
			WHERE [NAME] = @entity_type;

			SET @new_entity_structure_id = SCOPE_IDENTITY();

			--Add the unique code
			INSERT INTO epacube.ENTITY_IDENTIFICATION (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, [VALUE], CREATE_USER)
			SELECT @new_entity_structure_id, @entity_data_name, e.DATA_NAME_ID, DATA_VALUE, @user
			FROM #Entity e
				INNER JOIN epacube.DATA_NAME dn
					ON e.DATA_NAME_ID = dn.DATA_NAME_ID
				INNER JOIN epacube.DATA_SET ds
					ON dn.DATA_SET_FK = ds.DATA_SET_ID
			WHERE ds.TABLE_NAME = 'ENTITY_IDENTIFICATION';

			--Add the non-unique name
			INSERT INTO epacube.ENTITY_IDENT_NONUNIQUE (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, [VALUE], CREATE_USER)
			SELECT @new_entity_structure_id, @entity_data_name, e.DATA_NAME_ID, DATA_VALUE, @user
			FROM #Entity e
				INNER JOIN epacube.DATA_NAME dn
					ON e.DATA_NAME_ID = dn.DATA_NAME_ID
				INNER JOIN epacube.DATA_SET ds
					ON dn.DATA_SET_FK = ds.DATA_SET_ID
			WHERE ds.TABLE_NAME = 'ENTITY_IDENT_NONUNIQUE';

		COMMIT TRANSACTION;

		SET @ls_stmt = 'A new entity was added with ENTITY_STRUCTURE_ID ' + CAST(@new_entity_structure_id AS VARCHAR(20)) + '.'; 
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		SELECT @new_entity_structure_id;
					
	END TRY

	BEGIN CATCH
		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 
						
		SET @ls_stmt = 'Execution of epacube.ENTITY_CREATE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
				
	END CATCH

END
