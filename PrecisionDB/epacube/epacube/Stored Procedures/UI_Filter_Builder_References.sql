﻿--USE [epacube]
--GO
--/****** Object:  StoredProcedure [epacube].[UI_Filter_Builder]    Script Date: 2/22/2017 10:13:00 AM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE PROCEDURE [epacube].[UI_Filter_Builder_References] @UserID varchar(64), @Data_Name_FK varchar(64), @Criteria varchar(Max) = Null, @FilterResultsExec int = 0, @FilterResultsPrint int = 1, @QueryExec int = 0, @QueryScript Varchar(Max) OUTPUT

as 

/*
--/************************************************************************************************************************************************
--  Written By  : Gary Stone
	February, 2017
Parameters
@FilterResultsExec - 1 will process results of for the filter data name requested
@FilterResultsPrint - 1 will generate the query to use with the filter data name requested, but will not display any results
@QueryExec - 0 will do nothing; 1 will display the code used for each of the Prod, Cust, and Vendor tables to be generated; 2 will run the query

The appropriate Prod, Cust, and Vendor tables will be generated regardless of the options chosen above.

! = NOT
~ = group separator - replaces ^
| = OR
= = Criteria Value

--*/-----------------------------------------------------------------------------------------------------------------------------------------------
*/

set nocount on
--Declare @UserID Varchar(64) = 78, @Data_Name_FK varchar(32) = '501045', @Criteria varchar(Max) = '110401=%cortisone%~501045=3130030~159905=1~144010=YOKES FOODS INC~144020=~144112=~520013=~143112=THE ART OF GOOD FOOD~'   --'144010=YOKES FOODS INC~'--Null-- 
--Declare @UserID Varchar(64) = 78, @Data_Name_FK varchar(32) = '501045', @Criteria varchar(Max) = '110401=%cortisone%~501045=3130030~'
--Declare @UserID Varchar(64) = 78, @Data_Name_FK varchar(32) = '502045', @Criteria varchar(Max) = '110401=%cortisone%~501045=3130030~'
--Declare @UserID Varchar(64) = 78, @Data_Name_FK varchar(32) = '144020', @Criteria varchar(Max) = '110401=%cortisone%~501045=3130030~159905=1~144010=YOKES FOODS INC~'   --'144010=YOKES FOODS INC~'--Null-- 
--Declare @UserID Varchar(64) = 78, @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = '159905=1~144010=YOKES FOODS INC~'--Null-- 
--Declare @UserID Varchar(64) = 78, @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = '144010=YOKES FOODS INC~110401=%cortisone%~501045=3130030~'--Null-- 
--Declare @UserID Varchar(64) = 78, @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = '110100=%D~'
--Declare @UserID Varchar(64) = 78, @Data_Name_FK varchar(32) = '144120', @Criteria varchar(Max) = Null--'110100=%D~'
--Declare @UserID Varchar(64) = 78, @Data_Name_FK varchar(32) = '502045', @Criteria varchar(Max) = '144010=BARNEYS SOOPER MARKET INC~159905=1~110401=%cereal%~'
--Declare @UserID Varchar(64) = 78, @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = '144112=Ros%~502045=42~159905=1~' --502045=BARNEYS SOOPER MARKET INC~159905=1~'
--Declare @UserID Varchar(64) = 78, @Data_Name_FK varchar(32) = '143110', @Criteria varchar(Max) = '110100=111018D~', @FilterResults int = 1, @CompQuery int = 0
--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = '110100=233877D~144020=9670~143110=1017012~', @FilterResultsExec int = 0, @FilterResultsPrint int = 0, @QueryExec int = 2
--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = '501045=1000001~144020=7155~143110=1800103~159905=1~', @FilterResultsExec int = 0, @FilterResultsPrint int = 1, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(32) = '110100', @Criteria varchar(Max) = 'URM SUBGROUP=1000001~CUSTOMER SHIP TO=7155~VENDOR NUMBER=1800103~AUTHORIZED=1~', @FilterResultsExec int = 0, @FilterResultsPrint int = 1, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(32) = '144010', @Criteria varchar(Max) = '144020=~', @FilterResultsExec int = 0, @FilterResultsPrint int = 1, @QueryExec int = 2, @QueryScript Varchar(Max)
--Declare @UserID Varchar(64) = 'gstone2', @Data_Name_FK varchar(Max) = '', @Criteria varchar(Max) = '144020=~110100=111018D~159905=1~', @FilterResultsExec int = 0, @FilterResultsPrint int = 1, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(Max) = '144010~144020~110103~144120', @Criteria varchar(Max) = '110100=233877D~144020=9670~143110=1017012~550001=1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(Max) = '0', @Criteria varchar(Max) = '110100=233877D~144020=9670~143110=1017012~550001=1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(Max) = '110100~', @Criteria varchar(Max) = '143110=1017012~550001=1~144020=7155~550020=1~110401=%Cereal%~550100=!1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(Max) = '110401~', @Criteria varchar(Max) = '144020=7155~143110=1017012~550001=1~550020=1~110401=%Cereal%~550100=1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(Max) = '110401~', @Criteria varchar(Max) = '143110=1017012~550001=!1~550020=1~110401=%Cereal%~550100=1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(Max) = '110401~', @Criteria varchar(Max) = '143110=1017012~144020=7155~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(Max) = '144120~110100~501045~502045~', @Criteria varchar(Max) = '550001=1~144020=7141~550100=1~550020=1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)
--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(Max) = '502045~', @Criteria varchar(Max) = '550001=!1~550020=!1~110401=%Cereal%~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)

--Declare @UserID Varchar(64) = 'gstone', @Data_Name_FK varchar(Max) = 'All', @Criteria varchar(Max) = '550001=1~144020=7141~550100=1~550020=1~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)

--Declare @UserID Varchar(64) = 'gstone@epacube.com', @Data_Name_FK varchar(Max) = '110100', @Criteria varchar(Max) = '500019=101~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)

--Declare @UserID Varchar(64) = 'gstone@epacube.com', @Data_Name_FK varchar(Max) = '144020', @Criteria varchar(Max) = '144020=395~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)

Set @UserID = Replace(@UserID, '.', '_')

If object_id('tempdb..#Tbl') is not null
drop table #Tbl

Create table #Tbl([SQL] varchar(Max) Null, [Where] Varchar(Max) Null)

If object_id('tempdb..#Param3') is not null
drop table #Param3

Declare @POS int
Declare @StartPOS int
Declare @FilterToken varchar(192)
Declare @CritDN varchar(64)
Declare @CritValue Varchar(128)
Declare @OpExclusion varchar(8)
Declare @TmpTbl varchar(64)
Declare @FilterCode varchar(Max)
Declare @Entity_Class_CR_FK varchar(64)
Declare @SQL varchar(Max)
Declare @WHERE varchar(Max)
Declare @DN_Type varchar(32)
Declare @Structure_FK varchar(64)
Declare @Local_Structure_FK Varchar(64)
Declare @Primary_Structure_FK Varchar(64)
Declare @AuthSQL Varchar(Max)
Declare @DN_Label Varchar(64)
Declare @KeyCols Varchar(Max)
Declare @SqlEXEC Varchar(Max)
Declare @From VARCHAR(MAX), @Qtbl Varchar(128), @DN_FK Varchar(16), @Alias Varchar(16), @Select Varchar(Max), @Primary_Column varchar(64), @Local_Column varchar(64), @Alias_DV varchar(16), @Operator Varchar(8), @OpExcl Varchar(8), @R_EC_CR_FK Varchar(16)
Declare @VendorJoin Varchar(Max), @VendorCols Varchar(Max)
Declare @Lookup_Values int
Declare @BT_Status int
Declare @ST_Status int
Declare @Op varchar(16) 
Declare @DN_String Varchar(Max) = ''
Declare @DN_POS int = 1
Declare @DN_StartPOS int = 1
Declare @DN_Length int = 1
Declare @Auths_Ind int = 1
Declare @Auths_Join varchar(64) = ''
Declare @SQLcodeStart Varchar(Max) = ''
Declare @SQLcodeIn Varchar(Max) = ''
Declare @SQLcodeNotIn Varchar(Max) = ''
Declare @VirtualColumnCheck Varchar(Max) = ''
Declare @DNtmp varchar(16) = ''
Declare @FILTER_ENTITY_CLASS_CR_FK varchar(64) = ''
Declare @COLUMN_ENTITY_CLASS_CR_FK varchar(64) = ''
Declare @Prod_Structure_Column varchar(64) = ''
Declare @VirtualJoin varchar(64) = ''
Declare @ImmedAlias varchar(64) = ''
Declare @Ref_DN_FK varchar(64) = ''
Declare @Cust_Structure_Column varchar(64) = ''
Declare @VirtualAction varchar(64) = ''
Declare @Pass int = 1
Declare @Virtual_DN_FK varchar(64) = ''
Declare @Cust_Structure_FK_Init varchar(64) = ''
Declare @Item_Data_Name_FK varchar(64) = ''
Declare @PA_Alias varchar(64) = ''
Declare @All_Filters varchar(Max) = ''
Declare @CurrentFilterDN_FK varchar(16) = ''
Declare @Max_Pass varchar(8)

Set @Item_Data_Name_FK = (Select value from epacube.epacube_params where application_scope_fk = 10109 and name = 'search2')

	Set @QueryScript = ''

	--If @Data_Name_FK = 'All'
	--Begin
	--	DECLARE AllFilters Cursor local for
	--		Select Data_Name_fk from epacube.ui_filter where isnull(checkbox_y_n, 0) = 0 and isnull(data_name_fk, 0) <> 0 and record_status_cr_fk = 1

	--		OPEN AllFilters;
	--		FETCH NEXT FROM AllFilters INTO @CurrentFilterDN_FK
	--		WHILE @@FETCH_STATUS = 0

	--			Begin--Fetch

	--			Set @All_Filters = @All_Filters + @CurrentFilterDN_FK + '~'

	--		FETCH NEXT FROM AllFilters INTO @CurrentFilterDN_FK
	--			End--Fetch
	--		Close AllFilters;
	--		Deallocate AllFilters;

	--		Set @Data_Name_FK = @All_Filters
	--	End

	If @Data_Name_FK like '%~%'
	Begin
		Set @DN_String = @Data_Name_FK
		Set @Data_Name_FK = '0'
	End

If @Data_Name_FK = '0' and isnull(@Criteria, '') <> ''
Begin
	Set @Lookup_Values = 1
end
Else
Begin
	Set @Data_Name_FK = Case when isnumeric(@Data_Name_FK) = 1 then @Data_Name_FK else (Select data_name_id from epacube.data_name where name = '' + @Data_Name_FK + '') end
	Set @Lookup_Values = isnull((Select top 1 Lookup_Values from epacube.ui_filter where data_name_fk = @Data_Name_FK), 0)
	If @Lookup_Values = 2
	Begin
		Set @Criteria = Null
		goto last_step
	End
	else
	--If @Lookup_Values = 0 or isnull(@Criteria, '') = ''
	If isnull(@Criteria, '') = ''
		GoTo Last_Step
End

Create Table #Param3(CritDN varchar(Max) Not Null, CritValue varchar(max) Not Null, OpExclusion varchar(8)) --, Lookup_Field varchar(8))

	Set @StartPOS = 1

	If isnull(@UserID, '0') = '0'
	Set @UserID = Cast(@@SPID as varchar(8))

	If object_id('TempTbls.tmp_PROD_' + @UserID) is not null
		Exec('Drop Table TempTbls.tmp_PROD_' + @UserID)

	If object_id('TempTbls.tmp_CUST_' + @UserID) is Not null
		Exec('Drop Table TempTbls.tmp_CUST_' + @UserID)

	If object_id('TempTbls.tmp_VENDOR_' + @UserID) is Not null
		Exec('Drop Table TempTbls.tmp_VENDOR_' + @UserID)

	If object_id('TempTbls.tmp_FILTER_' + @UserID) is not null
		Exec('Drop Table TempTbls.tmp_FILTER_' + @UserID)

	Exec('Create Table TempTbls.tmp_FILTER_' + @UserID + '(CritDN varchar(64) Not Null, CritValue varchar(128) Not Null, OpExclusion varchar(8))')

	If isnull(@Criteria, '') = ''
	Set @Criteria = @Data_Name_FK + '=' + '' + '~'

	If @QueryExec <> 0
	Begin
		Set @FilterResultsPrint = 0
		Set @FilterResultsExec = 0
	End
		
	Set @VendorJoin = ''

	While charindex('~', @Criteria, @StartPOS) <> 0 
		Begin

			Set @POS = charindex('~', @Criteria, @StartPOS)
			Set @FilterToken = substring(@Criteria, @StartPOS, @POS - @StartPOS)


			Set @StartPOS = @POS + 1		

			Set @CritDN = left(@FilterToken,charindex('=', @FilterToken) - 1)
			Set @CritDN = Case when isnumeric(@CritDN) = 1 then @CritDN else (Select data_name_id from epacube.data_name where name = '' + @CritDN + '') end
			Set @CritValue = reverse(left(reverse(@FilterToken),charindex('=', reverse(@FilterToken)) - 1))

--Put in place for advisory board meeting of 5/18/17
			
			Set @CritValue = Case When @CritDN = '110100' and isnumeric(right(@CritValue, 1)) = 1 then Left(@CritValue, Len(@CritValue) - 1) + Case Right(@CritValue, 1) when '0' then 'D' else 'W' end else @CritValue end

--End Advisory Board Meeting Code

		If @CritDN Not In (Select data_name_fk from epacube.ui_filter) --or @CritDN = 159905
			goto NextVal

			If @CritValue like '%!%' 
				Begin
					Set @OpExclusion = '!'
					Set @CritValue = Right(@CritValue, Len(@CritValue)-1)
				end
				Else
				Begin
					Set @OpExclusion = ''
				End

			Set @CritValue = Case ltrim(rtrim(@CritValue)) when 'on' then '1' else @CritValue end

			Insert into #Param3
			Select @CritDN, @CritValue, @OpExclusion--, Null 

			If @Criteria not like '%' + @Data_Name_FK + '%'
			Insert into #Param3
			Select @Data_Name_FK, '', ''
			where @Data_Name_FK not in (Select CritDN from #Param3)

NextVal:
	End

	Exec('Insert into TempTbls.tmp_FILTER_' + @UserID + ' Select * from #Param3')

	If (Select count(*) from #Param3 where isnull(CritValue, '') <> '') = 0
	GoTo Last_Step

	Set @BT_Status = Case when (Select count(*) from #Param3 where critdn = 144010) > 0 then 1 else 0 end
	Set @ST_Status = Case when (Select count(*) from #Param3 where critdn = 144020) > 0 then 1 else 0 end

	DECLARE Entity_Class Cursor local for
	Select r.ENTITY_CLASS_CR_FK
	from EPACUBE.z_DATA_NAME_GROUP_REFERENCES R
	inner join epacube.epacube.ui_filter uif on r.data_name_fk = uif.Data_Name_FK
	inner join #Param3 P3 on uif.data_name_fk = P3.CritDN
	where 1 = 1
		and P3.CritDN <> 159905
		and r.table_name <> 'SEGMENTS_CONTACTS'
	group by r.ENTITY_CLASS_CR_FK
	Order by Case when r.ENTITY_CLASS_CR_FK = 10109 then 2 else 1 end

		OPEN Entity_Class;
		FETCH NEXT FROM Entity_Class INTO @Entity_Class_CR_FK
		WHILE @@FETCH_STATUS = 0

		Begin
		
		Set @Select = 'Select '
		Set @Where = 'Where 1 = 1 '
		Set @From = ''
		Set @KeyCols = ''

		Set @From = Case 
			when @Entity_Class_CR_FK = 10109 then ' From epacube.epacube.product_structure PS with (nolock) '
												+ 'Inner join epacube.product_identification pi with (nolock) on ps.product_structure_id = pi.product_structure_fk and pi.data_name_fk = ' + @Item_Data_Name_FK
			When @Entity_Class_CR_FK = 10103 then ' From epacube.epacube.entity_structure ESV with (nolock) '
												+ 'Inner Join epacube.entity_identification eiv with (nolock) on esv.entity_structure_id = eiv.entity_structure_fk and esv.data_name_fk = eiv.entity_data_name_fk and esv.data_name_fk in (143000) '
	

			When @Entity_Class_CR_FK = 10104 and @BT_Status = 1 and @ST_Status = 1 then
													' From epacube.epacube.entity_structure esc_p with (Nolock)
													Inner join epacube.epacube.entity_identification eic_p with (nolock) on esc_p.entity_structure_id = eic_p.entity_structure_fk and esc_p.data_name_fk = eic_p.entity_data_name_fk and esc_p.data_name_fk = 144010
													inner join epacube.epacube.entity_structure esc_c with (nolock) on eic_p.entity_structure_fk = esc_c.parent_entity_structure_fk and esc_c.data_name_fk = 144020
													inner join epacube.epacube.entity_identification eic with (nolock) on esc_c.entity_structure_id = eic.entity_structure_fk 
													'
			When @Entity_Class_CR_FK = 10104 and @BT_Status = 1 then
													' From epacube.epacube.entity_structure esc_p with (Nolock)
													Inner join epacube.epacube.entity_identification eic with (nolock) on esc_p.entity_structure_id = eic.entity_structure_fk and esc_p.data_name_fk = eic.entity_data_name_fk and esc_p.data_name_fk = 144010
													'
												else ' From epacube.epacube.entity_structure ESC with (nolock) '
												+ 'Inner Join epacube.entity_identification eic with (nolock) on esc.entity_structure_id = eic.entity_structure_fk and esc.data_name_fk = eic.entity_data_name_fk and esc.data_name_fk in (144020) --(144010, 144020) 
												' end

		Set @TmpTbl = 'TempTbls.tmp_'	+ Case @Entity_Class_CR_FK when 10103 then 'VENDOR_' when 10104 then 'CUST_' when 10109 then 'PROD_' end + @UserID
		Set @Structure_FK = Case @Entity_Class_CR_FK when 10103 then 'VENDOR_ENTITY_STRUCTURE_FK' when 10104 then 'CUST_ENTITY_STRUCTURE_FK' when 10109 then 'PRODUCT_STRUCTURE_FK' end

		If ((Select count(*) from EPACUBE.z_DATA_NAME_GROUP_REFERENCES R inner join epacube.epacube.ui_filter uif on r.data_name_fk = uif.Data_Name_FK inner join #Param3 P3 on uif.data_name_fk = P3.CritDN where r.table_name <> 'SEGMENTS_CONTACTS' and r.entity_class_cr_fk = 10103) > 0 And
			((Select count(*) from EPACUBE.z_DATA_NAME_GROUP_REFERENCES R inner join epacube.epacube.ui_filter uif on r.data_name_fk = uif.Data_Name_FK inner join #Param3 P3 on uif.data_name_fk = P3.CritDN where r.table_name <> 'SEGMENTS_CONTACTS' and r.entity_class_cr_fk = 10104) > 0 OR
			(Select count(*) from EPACUBE.z_DATA_NAME_GROUP_REFERENCES R inner join epacube.epacube.ui_filter uif on r.data_name_fk = uif.Data_Name_FK inner join #Param3 P3 on uif.data_name_fk = P3.CritDN where r.table_name <> 'SEGMENTS_CONTACTS' and r.entity_class_cr_fk = 10109) > 0)) And
			@VendorJoin = ''
		Begin
			Set @VendorJoin = ' inner join epacube.epacube.segments_vendor sv with (nolock) on t_vendor.vendor_entity_structure_fk = sv.VENDOR_ENTITY_STRUCTURE_FK and sv.data_level_cr_fk = 518 '
		end
		If @Entity_Class_CR_FK = 10104
			Set @VendorJoin = @VendorJoin + ' inner join TempTbls.tmp_CUST_' + @UserID + ' t_cust with (nolock) on sv.cust_entity_structure_fk = t_cust.cust_entity_structure_fk '
		If @Entity_Class_CR_FK = 10109
			Set @VendorJoin = @VendorJoin + ' inner join TempTbls.tmp_PROD_' + @UserID + ' t_prod with (nolock) on sv.product_structure_fk = t_prod.product_structure_fk '

			DECLARE Tbl Cursor local for
			Select r.Qtbl, r.data_name_fk, r.Alias, Primary_Column, Local_Column, Alias_DV, R.DN_Label, p3.CritValue, r.Entity_Class_CR_FK
			, Case when charindex('%', P3.CritValue) = 0 then ' = ' else ' like ' end Operator
			, Case when isnull(P3.OpExclusion, '') = '' then '' else ' Not ' end OpExcl
			, (Select top 1 R1.Primary_column from EPACUBE.z_DATA_NAME_GROUP_REFERENCES R1 with (nolock) where R1.Data_Name_FK = R.Data_Name_FK and R1.entity_class_CR_FK = @Entity_Class_CR_FK
				and R1.Primary_column in ('Cust_Entity_Structure_FK', 'Vendor_Entity_Structure_FK', 'Product_Structure_FK', 'Entity_Structure_FK') and R1.local_column not in ('Org_Entity_Structure_FK')) 'Primary_Structure_Column'
			, (Select top 1 R1.local_column from EPACUBE.z_DATA_NAME_GROUP_REFERENCES R1 with (nolock) where R1.Data_Name_FK in (Select critdn from #Param3) and R1.entity_class_CR_FK = @Entity_Class_CR_FK
				and R1.local_column in ('Cust_Entity_Structure_FK', 'Vendor_Entity_Structure_FK', 'Product_Structure_FK', 'Entity_Structure_FK') order by len(local_column) desc) 'Local_Structure_Column'
			from EPACUBE.z_DATA_NAME_GROUP_REFERENCES R
			inner join epacube.epacube.ui_filter uif on r.data_name_fk = uif.Data_Name_FK
			inner join #Param3 P3 on uif.data_name_fk = P3.CritDN
			where	--P3.CritDN <> 159905
					P3.CritDN not in (159905)
					and P3.CritDN not in (select data_name_fk from epacube.epacube.DATA_NAME_VIRTUAL with (nolock) where REFERENCE_DATA_NAME_FK in (503201, 503207, 503301, 503309)) --Virtual Effective Dates
					and r.entity_class_cr_fk in (@Entity_Class_CR_FK)
					and R.local_column not like '%Segment_FK'
					and R.Local_Column not in ('RECORD_STATUS_CR_FK', 'price_per')
			group by  r.Qtbl, r.data_name_fk, r.Alias, Primary_Column, Local_Column, Alias_DV, R.DN_Label, p3.CritValue, isnull(P3.OpExclusion, ''), r.Entity_Class_CR_FK
			Order by Case when R.Primary_Column like '%Data_Value_FK' then 9 when R.Primary_Column like '%Structure%' then 1 else 5 end

				OPEN Tbl;
				FETCH NEXT FROM Tbl INTO @Qtbl, @DN_FK, @Alias, @Primary_Column, @Local_Column, @Alias_DV, @DN_Label, @CritValue, @R_EC_CR_FK, @Operator, @OpExcl, @Primary_Structure_FK, @Local_Structure_FK
				WHILE @@FETCH_STATUS = 0

				Begin
				Set @Alias = Case When @Entity_Class_CR_FK = 10104 and @BT_Status = 1 and  @ST_Status = 0 then Replace(@Alias, 'EIC_P', 'EIC') else @Alias end

					Set @KeyCols = Case 
					When @KeyCols not like '%' + @Primary_Structure_FK + '%' then Case @Entity_Class_CR_FK when 10104 then 'EIC' when 10103 then 'EIV' else 'PI' end + '.[' + @Primary_Structure_FK + '] ''' + @Local_Structure_FK + ''',  ' 
					else @KeyCols + '' end

				If @Local_Column in ('Product_Structure_FK', 'Cust_Entity_Structure_fk', 'Vendor_Entity_Structure_FK', 'Entity_Structure_FK') and ((@R_EC_CR_FK = 10124 and @Entity_Class_CR_FK in (10103, 10104)) or @R_EC_CR_FK <> 10124) and @DN_FK not in (144010, 144020)
					Begin
						Set @From = @From + '
							inner join ' + @Qtbl + ' ' + @Alias + ' on ' + Case @Entity_Class_CR_FK when 10109 then 'PI.[' when 10104 then 'EIC.['  when 10103 then 'EIV.[' end + @Primary_Column  + '] = ' + @Alias + '.[' + @Local_Column + '] and ' +
							@Alias + '.Data_Name_FK = ' + @DN_FK
					End
					
				If @Local_Column not in ('Data_Value_FK', 'Entity_Data_Value_FK', 'Record_status_cr_fk') and @Local_Column not like '%Structure%' and ((@R_EC_CR_FK = 10124 and @Entity_Class_CR_FK in (10103, 10104)) or @R_EC_CR_FK <> 10124)
					Begin
					Set @Select = @Select + @Alias + '.[' + @Local_Column + '] ''' + @DN_Label + ''', '
					Set @Where = Case when isnull(@CritValue, '') <> '' then @Where + ' and ' + @Alias + '.[' + @Local_Column + '] ' + @OpExcl + ' ' + @Operator + '''' + @CritValue + '''' else @Where end
					End
				if @Primary_Column in ('data_value_fk', 'entity_data_value_fk') and ((@R_EC_CR_FK = 10124 and @Entity_Class_CR_FK in (10103, 10104)) or @R_EC_CR_FK <> 10124)
					Begin
						Set @From = @From + '
							Inner join ' + Case @Primary_Column when 'Data_Value_FK' then 'epacube.data_value' else 'epacube.entity_data_value' end + ' ' + @Alias_DV + ' on ' + @Alias + '.[' + @Local_Column + '] = ' + @Alias_DV + '.[' + Replace(@Primary_Column, '_FK', '_ID') + '] '
						Set @Select = @Select + @Alias_DV + '.[value] ''' + @DN_Label + ''', ' + @Alias_DV + '.[Description] ''' + @DN_Label + '_Description'''' '
						Set @Where = Case when isnull(@CritValue, '') <> '' then @Where + ' and ' + @Alias_DV + '.[Value] ' + @OpExcl + ' ' + @Operator +  '''' + @CritValue + '''' else @Where end
						Set @KeyCols = Case When @KeyCols not like '%' + @Local_Column then @KeyCols + @Alias + '.[' + @Local_Column + '] ''' + @Local_Column + ''',  ' else '' end
					End

				If @Qtbl like '%SEGMENTS_CONTACTS%' and @R_EC_CR_FK = 10124 and @From like '%' + @Alias + '%' and  @Entity_Class_CR_FK in (10103, 10104)
					
					Set @From = @From + ' and ' + @Alias + '.entity_class_cr_fk = ' + @Entity_Class_CR_FK

				FETCH NEXT FROM Tbl INTO @Qtbl, @DN_FK, @Alias, @Primary_Column, @Local_Column, @Alias_DV, @DN_Label, @CritValue, @R_EC_CR_FK, @Operator, @OpExcl, @Primary_Structure_FK, @Local_Structure_FK

				If @from like '%' + @Alias + '.[' + @Primary_Column + ']' + '%' and @Primary_Column like '%Structure%'--and @Alias not in ('esc_p', 'esc_c', 'esc', 'eic_p', 'eic_c', 'eic') --and @Select not like '%' + @Alias + '.[' + @Local_Column + ']%'
				Set @Alias = @Alias + '_1'

			End
			
			Close Tbl;
			Deallocate Tbl;

		Set @Select = Left(@Select, Len(rtrim(@Select)) - 1)
		Set @Select = Right(@Select, Len(@Select) - 6)
		Set @Select = 'Select ' + @KeyCols + @Select

		Set @SqlEXEC = Replace(Replace((@Select + @From + ' ' + @Where), '  ', ' '), '  ', ' ')

		If @QueryExec = 1
			Print(@SqlEXEC)
		Else
		If @QueryExec = 2
			Exec(@SqlEXEC)

		Set @SqlEXEC = 'Select distinct ' + @Local_Structure_FK + '
		from (' + @SqlEXEC + ') A'

--print(@SqlEXEC)
	If (Select count(*) from #Param3 P3 inner join EPACUBE.z_DATA_NAME_GROUP_REFERENCES R on p3.CritDN = R.data_name_fk where r.entity_class_cr_fk = @Entity_Class_CR_FK and isnull(p3.CritValue, '') not in ('', '0')) > 0
	Begin
		Exec('If Object_ID(''' + @TmpTbl + ''') is null
		Begin	
		Create Table ' + @TmpTbl + '(' + @Structure_FK + ' bigint not null);
		Create index idx_st on ' + @TmpTbl + '(' + @Structure_FK + ')
		End')

		Set @SqlEXEC = 'Insert into ' + @TmpTbl + ' (' + @Structure_FK + ') ' + @SqlEXEC

--print(@SqlEXEC)
		exec(@SqlEXEC)
	End
		FETCH NEXT FROM Entity_Class INTO @Entity_Class_CR_FK
		End

	Close Entity_Class;
	Deallocate Entity_Class;

------get filter code

	Set @DN_String = Case	when @DN_String <> '' and right(ltrim(rtrim(@DN_String)), 1) <> '~' then ltrim(rtrim(@DN_String)) + '~' 
							when @DN_String = '' and @Data_Name_FK <> 0 and @Data_Name_FK not like '%~' then @Data_Name_FK + '~'
					 else @DN_String end

	While charindex('~', @DN_String, @DN_StartPOS) <> 0 
		Begin

			Set @DN_POS = charindex('~', @DN_String, @DN_StartPOS)
			Set @DN_Length = @DN_POS - @DN_StartPOS
			Set @Data_Name_FK = Substring(@DN_String, @DN_StartPOS, @DN_Length)

			Set @DN_StartPOS = @DN_POS + 1

			Set @Data_Name_FK = Case when isnumeric(@Data_Name_FK) = 1 then @Data_Name_FK else (Select data_name_id from epacube.data_name where name = '' + @Data_Name_FK + '') end

	If object_id('tempdb..#DNFltrCursor') is not null
	drop table #DNFltrCursor

		Select *, Rank()over(order by Case when Data_Name_FK = @Data_Name_FK then 1 When Filter_Entity_Class_CR_FK = Column_Entity_Class_CR_FK then 2 when qtbl like '%product_association%' then 3 else  data_name_fk end) Pass
		into #DNFltrCursor
		from (
		Select distinct R.Local_Column,  Replace(ltrim(rtrim(R.QTbl)), 'Entity_Structure', 'Entity_Identification') QTbl
		, (Select top 1 Entity_class_cr_fk from EPACUBE.z_DATA_NAME_GROUP_REFERENCES where data_name_fk = @data_name_fk and entity_class_cr_fk is not null) FILTER_ENTITY_CLASS_CR_FK
		, R.Entity_Class_CR_FK Column_entity_Class_CR_FK
		, R.DN_Label
		, Case when R.Data_Name_FK in (144010, 144020) then 'Entity_Structure_FK' else
			(Select top 1 R1.local_column from EPACUBE.z_DATA_NAME_GROUP_REFERENCES R1 with (nolock) where R1.Data_Name_FK = R.Data_Name_FK and R1.entity_class_CR_FK = R.Entity_Class_CR_FK and R1.local_column like '%Structure%' 
			and R1.local_column not in ('Org_Entity_Structure_FK', 'Dept_Entity_Structure_FK', 'Zone_Entity_Structure_FK') order by local_column desc) end 'Structure_Column'
		, Case when R.Data_Name_FK in (144010, 144020) then 'Entity_Data_Name_FK' else 'Data_Name_FK' end 'DN_Type'
		, ' in ' Op
--		, Case	when uf.checkbox_y_n = 1 and isnull((select OpExclusion from #param3 p3a where p3a.critdn = dnv.data_name_fk), '') like '%!%' then ' not in ' else ' in ' end Op --and DNV.ACTION_TYPE = 'CheckBoxTransform' 
		, Case	when DNV.DATA_NAME_VIRTUAL_ID is not null and isnull((select OpExclusion from #param3 p3a where p3a.critdn = dnv.data_name_fk), '0') = '0' then ' left '
				when DNV.DATA_NAME_VIRTUAL_ID is not null and isnull((select OpExclusion from #param3 p3a where p3a.critdn = dnv.data_name_fk), '0') <> '0' then ' inner ' else '0' end VirtualJoin
		, isnull(DNV.Virtual_Alias, R.Alias) Alias
		, isnull(DNV.REFERENCE_DATA_NAME_FK, r.data_name_fk) Ref_DN_FK
		, isnull((Select top 1 local_column from EPACUBE.z_DATA_NAME_GROUP_REFERENCES where data_name_fk = isnull(DNV.REFERENCE_DATA_NAME_FK, R.data_name_fk) and local_column in ('entity_structure_fk', 'cust_entity_structure_fk')
				and local_column <> 'Org_Entity_Structure_FK'), '0') 'Cust_Structure_Column'
		, isnull((Select top 1 primary_column from EPACUBE.z_DATA_NAME_GROUP_REFERENCES where data_name_fk = DNV.REFERENCE_DATA_NAME_FK and primary_column in ('product_structure_fk')), '0') 'Prod_Structure_Column'
		, Null 'Action_Type' --DNV.ACTION_TYPE
		, ' ' + isnull(DNV.Column_Check, '') VirtualColumnCheck
		, dnv.data_name_FK Virtual_Data_Name_FK
		, r.data_name_fk
		--, (select r.local_column where data_name_fk = @Data_name_FK)
		from EPACUBE.z_DATA_NAME_GROUP_REFERENCES R
		left join #Param3 P3 on r.Data_Name_FK = P3.critdn
		left join epacube.DATA_NAME_VIRTUAL DNV on R.data_name_fk = dnv.REFERENCE_DATA_NAME_FK and DNV.data_name_fk in (select critdn from #Param3) --and DNV.ACTION_TYPE in ('CheckBoxTransform', 'RecordStatusCheck')
		left join epacube.ui_filter uf on dnv.data_name_fk = uf.Data_Name_FK
		Where 1 = 1
			and ((local_column not like '%structure%' and local_column not in ('RECORD_STATUS_CR_FK', 'price_per') and local_column not like '%_Segment_FK'
			and (R.data_name_FK = @Data_Name_FK or r.data_name_fk = dnv.REFERENCE_DATA_NAME_FK)))
		) A  Order by pass 

	DECLARE DNFltrCode Cursor local for
	Select * from #DNFltrCursor order by pass

	If (Select count(*) from #DNFltrCursor where pass = 1 and data_name_fk in (144010, 144020, @Item_Data_Name_FK) and data_name_fk <> isnull(Virtual_Data_Name_FK, data_Name_FK)) <> 0
	Begin
		Insert into #DNFltrCursor
		Select Local_Column, QTbl, Filter_Entity_Class_CR_FK, COLUMN_ENTITY_CLASS_CR_FK, DN_Label, Structure_Column, DN_Type, Op, VirtualJoin, case when column_entity_class_cr_fk = 10109 then 'po' else 'eic' end Alias, Ref_DN_FK, Cust_Structure_Column, Prod_Structure_Column, Action_Type, '' VirtualColumnCheck, Null Virtual_Data_Name_FK, data_name_fk, 0 Pass
		from #DNFltrCursor where pass = 1

		Update DNF
		Set Pass = Pass + 1 from #DNFltrCursor DNF
	End

		Set @SQLcodeStart = ''
		Set @SQLcodeIn = ''
		Set @SQLcodeNotIn = ''

		OPEN DNFltrCode;
		Set @Max_Pass = @@CURSOR_ROWS;
		
		FETCH NEXT FROM DNFltrCode INTO @Local_Column, @QTbl, @FILTER_ENTITY_CLASS_CR_FK, @Column_Entity_Class_CR_FK, @DN_Label, @Local_Structure_FK, @DN_Type, @Op, @VirtualJoin, @Alias, @Ref_DN_FK, @Cust_Structure_Column, @Prod_Structure_Column, @VirtualAction, @VirtualColumnCheck, @Virtual_DN_FK, @DNtmp, @Pass
		WHILE @@FETCH_STATUS = 0

		Begin--Fetch
		If @Filter_Entity_Class_CR_FK = 10109
		Begin

			Set @ImmedAlias = Case when Object_ID('TempTbls.tmp_PROD_' + @UserID) is not null then 't_prod' else 'pi' end

			Set @SQLcodeStart = Case when @SQLcodeStart <> '' then @SQLcodeStart else 
				Case when @Pass = 1 and Object_ID('TempTbls.tmp_PROD_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
		'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + ']  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @Ref_DN_FK
					when @Pass = 1 and Object_ID('TempTbls.tmp_PROD_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
		'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + ']  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @Ref_DN_FK
				else '' end			End
			Set @SQLcodeIn = @SQLcodeIn + 
				Case	when @SQLcodeIn = '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform')  and (@Pass <> 1 or @Max_Pass = 1) and Object_ID('TempTbls.tmp_PROD_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		and [product_structure_fk] in (select ' + @ImmedAlias + '.[product_structure_fk] from TempTbls.tmp_PROD_' + @UserID + ' ' + @ImmedAlias + ' '
						when @SQLcodeIn = '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and (@Pass <> 1  or @Max_Pass = 1) and Object_ID('TempTbls.tmp_PROD_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then ' 
		and product_structure_fk in (select pi.product_structure_Fk from epacube.product_identification ' + @ImmedAlias + ' with (nolock) '
				else '' end
			Set @SQLcodeIn = @SQLcodeIn + 
				Case	when @SQLcodeIn <> '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and @Pass <> 1 and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @ImmedAlias + '.[Product_Structure_FK] = ' + @Alias + '.[Product_Structure_FK] and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + Case when @Op = ' in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end 
		+ Case when @SQLcodeIn not like '%' + @Item_Data_Name_FK + '%' and @ImmedAlias = 'pi' then ' and pi.data_name_fk = ' + @Item_Data_Name_FK else '' end
				else '' end
			Set @SQLcodeIn = @SQLcodeIn + 
				Case	when @SQLcodeIn <> '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and @Pass <> 1 and @Cust_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join TempTbls.tmp_CUST_' + @UserID + ' t_cust with (nolock) on ' + @Alias + '.[' + @Cust_Structure_Column + '] = t_cust.cust_entity_structure_fk '
					else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and(@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and @Pass <> 1 and @Cust_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK 
					and @SQLcodeIn like '%t_cust%'	then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Cust_Structure_Column + '] = t_cust.cust_entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + Case when @Op = ' in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end
					else '' end

	--If @SQLcodeStart + @SQLcodeIn like '%' + @DNtmp + '%'
	--goto NextFilter

		Set @SQLcodeNotIn = @SQLcodeNotIn + 
			Case	when @SQLcodeNotIn = '' and @op = ' not in ' and Object_ID('TempTbls.tmp_PROD_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK and @SQLcodeIn not like '%' + @Alias + '%'	then '
		and [product_structure_fk] not in(select ' + @ImmedAlias + '.[product_structure_fk] from TempTbls.tmp_PROD_' + @UserID + ' ' + @ImmedAlias + ' '
					when @SQLcodeNotIn = '' and @op = ' not in ' and Object_ID('TempTbls.tmp_PROD_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK and @SQLcodeIn not like '%' + @Alias + '%'	 then '
		and product_structure_fk not in (select pi.product_structure_Fk from epacube.product_identification ' + @ImmedAlias + ' with (nolock) '
			else '' end

		Set @SQLcodeNotIn = @SQLcodeNotIn + 
			Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' +@VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @ImmedAlias + '.[Product_Structure_FK] = ' + @Alias + '.[Product_Structure_FK] and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + @VirtualColumnCheck
					else '' end
		Set @SQLcodeNotIn = @SQLcodeNotIn + 
		+	Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Cust_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join TempTbls.tmp_CUST_' + @UserID + ' t_cust with (nolock) on ' + @Alias + '.[' + @Cust_Structure_Column + '] = t_cust.cust_entity_structure_fk '
				else '' end

		Set @SQLcodeNotIn = @SQLcodeNotIn + 
			Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and isnull(@VirtualAction, '') = 'CheckBoxTransform' and @Pass <> 1 and @Cust_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK 
					and @SQLcodeNotIn like '%t_cust%' then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Cust_Structure_Column + '] = t_cust.cust_entity_structure_fk and ' + @DN_Type + ' = ' + @Ref_DN_FK + @VirtualColumnCheck
				else '' end

	End
	Else
	If @Filter_Entity_Class_CR_FK = 10104
	Begin

		Set @ImmedAlias = Case when Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null then 't_cust' else 'eic' end
		Set @Cust_Structure_FK_Init = Case when @Pass = 1 then @Cust_Structure_Column else @Cust_Structure_FK_Init end
		Set @PA_Alias = Case when @Qtbl like '%product_association%' then @Alias else @PA_Alias end

		Set @SQLcodeStart = Case when @SQLcodeStart <> '' then @SQLcodeStart else 
			Case when @Pass = 1 and Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
			'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + ']  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @Ref_DN_FK
				when @Pass = 1 and Object_ID('TempTbls.tmp_CUST_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
			'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + ']  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @Ref_DN_FK
			else '' end			End

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn = '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform')  and (@Pass <> 1 or @Max_Pass = 1) and Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null then '
		and [' + @Cust_Structure_FK_Init + '] in (select ' + @ImmedAlias + '.[Cust_Entity_Structure_FK] from TempTbls.tmp_CUST_' + @UserID + ' ' + @ImmedAlias + ' '
					when @SQLcodeIn = '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and (@Pass <> 1 or @Max_Pass = 1) and Object_ID('TempTbls.tmp_CUST_' + @UserID) is null then '
		and [' + @Cust_Structure_FK_Init + '] in (select eic.entity_structure_fk from epacube.entity_identification ' + @ImmedAlias + ' with (nolock) '
			else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and (@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and @Pass <> 1 and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @ImmedAlias + '.[' 
				+ Case when @ImmedAlias = 'eic' then 'entity_structure_fk' else 'cust_entity_structure_fk' end
				 + '] = ' + @Alias + '.[' + @Cust_Structure_Column + '] and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + Case when @Op = ' in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end 
			else '' end
							
		Set @SQLcodeIn = @SQLcodeIn + --Product_Association is included here
			Case	when @SQLcodeIn <> '' and(@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and @Pass <> 1 and @Cust_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null 
					and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK 
					then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Cust_Structure_Column + '] = t_cust.cust_entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK 
			+ Case when @Op = ' in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end
			else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and(@op = ' in ' or isnull(@VirtualAction, '') = 'CheckBoxTransform') and @Pass <> 1 and @Cust_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is null 
					and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Cust_Structure_Column + '] = eic.entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK 
				+ Case when @Op = ' in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end
			else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and @op = ' in ' and @Pass <> 1 and @Prod_Structure_Column <> '0' and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK
					and @SQLcodeIn not like '%t_prod%' and @SQLcodeIn not like '%epacube.product_identification pi%'
					and @SQLcodeIn like '%PRODUCT_ASSOCIATION%'
					then '
			' + @VirtualJoin + 'join ' + Case	when Object_ID('TempTbls.tmp_Prod_' + @UserID) is not null then 'TempTbls.tmp_Prod_' + @UserID + ' t_PROD with (nolock) on ' + @Alias + '.[Product_Structure_FK] = t_prod.product_structure_fk '
												when Object_ID('TempTbls.tmp_Prod_' + @UserID) is null then 'epacube.product_identification pi with (nolock) on ' + @PA_Alias + '.[Product_Structure_FK] = pi.product_structure_fk and pi.data_name_fk = ' 
												+ @Item_Data_Name_FK + ' ' + @VirtualColumnCheck
										 else '' end
			else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and @Pass <> 1 and @Prod_Structure_Column <> '0' and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK
					and (@SQLcodeIn like '%t_prod%' or @SQLcodeIn like '%epacube.product_identification pi%' or @SQLcodeIn like '%PRODUCT_ASSOCIATION%')
					and @qtbl not like '%PRODUCT_ASSOCIATION%'
					then '
			' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + Case when @SQLcodeIn like '%t_prod%' then 't_prod' when @SQLcodeIn like '%PI %' then 'PI' when @SQLcodeIn like '%Product_Association%' then  @PA_Alias end 
			+ '.[' + @Prod_Structure_Column + '] = ' + @Alias + '.[Product_Structure_FK] and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + ' ' + Case when @Op = ' in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end  									
				 --else '123321123' 
				 else ''
				 end

	--If @SQLcodeStart + @SQLcodeIn like '%' + @DNtmp + '%'
	--Begin

	--goto NextFilter
	--end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn = '' and @op = ' not in ' and Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null then '
	and [' + @Cust_Structure_FK_Init +'] not in (select ' + @ImmedAlias + '.[cust_entity_structure_fk] from TempTbls.tmp_CUST_' + @UserID + ' ' + @ImmedAlias + ' '
				when @SQLcodeNotIn = '' and @op = ' not in ' and Object_ID('TempTbls.tmp_CUST_' + @UserID) is null then '
	and [' + @Cust_Structure_FK_Init +'] not in (select eic.entity_structure_Fk from epacube.entity_identification ' + @ImmedAlias + ' with (nolock) '
		else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Pass <> 1 and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
	' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @ImmedAlias + '.[' + Case when @ImmedAlias = 'eic' then 'entity_structure_fk' else 'cust_entity_structure_fk' end + '] = ' + @Alias + '.[' + @Cust_Structure_Column + '] and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + ' ' + @VirtualColumnCheck
		else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Pass <> 1 and @Cust_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK 
				then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Cust_Structure_Column + '] = t_cust.cust_entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + ' ' + @VirtualColumnCheck
			else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> ''and @op = ' not in ' and @Pass <> 1 and @Cust_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Cust_Structure_Column + '] = eic.entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + ' ' + @VirtualColumnCheck
			else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Pass <> 1 and @Prod_Structure_Column <> '0' and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK
				and @SQLcodeNotIn not like '%t_prod%' and @SQLcodeNotIn not like '%epacube.product_identification pi%'  
				then '
		' + @VirtualJoin + 'join ' + Case	when Object_ID('TempTbls.tmp_Prod_' + @UserID) is not null then 'TempTbls.tmp_Prod_' + @UserID + ' t_PROD with (nolock) on ' + @Alias + '.[Product_Structure_FK] = t_prod.product_structure_fk '
											when Object_ID('TempTbls.tmp_Prod_' + @UserID) is null then 'epacube.product_identification pi with (nolock) on ' + @PA_Alias + '.[Product_Structure_FK] = pi.product_structure_fk and pi.data_name_fk = ' + @Item_Data_Name_FK + ' ' + @VirtualColumnCheck
									 else '' end
		else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> '' and @Pass <> 1 and @Prod_Structure_Column <> '0' and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK
				and (@SQLcodeNotIn like '%t_prod%' or @SQLcodeNotIn like '%epacube.product_identification pi%' or @SQLcodeNotIn like '%EPACUBE.[PRODUCT_ASSOCIATION]%')
				and @qtbl not like '%PRODUCT_ASSOCIATION%'
				then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + Case when @SQLcodeNotIn like '%t_prod%' then 't_prod' when @SQLcodeNotIn like '%PI %' then 'PI' when @SQLcodeNotIn like '%Product_Association%' then  @PA_Alias end + '.[' + @Prod_Structure_Column + '] = ' + @Alias + '.[Product_Structure_FK] and ' + @Alias + '.' + @DN_Type + ' = ' + @Ref_DN_FK + ' ' + Case when @Op = ' not in ' then @VirtualColumnCheck else replace(@VirtualColumnCheck, '=', '<>') end  									
			 else '' end
	
	End
	
	Else
	If @Filter_Entity_Class_CR_FK = 10103
	Begin

		Set @SQLcodeStart = Case when Object_ID('TempTbls.tmp_Vendor_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
	'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + '] ''LABEL'' from epacube.' + @QTbl + ' where data_name_fk = ' + @Data_Name_FK 
	+ ' and ' + @Local_Structure_FK + ' in (select t_Vendor.Vendor_Entity_Structure_fk from TempTbls.tmp_Vendor_' + @UserID + ' t_Vendor with (nolock)'
								 when Object_ID('TempTbls.tmp_Vendor_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
	'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + '] ''LABEL'' from epacube.' + @QTbl + ' where data_name_fk = ' + @Data_Name_FK
--	+ ' and ' + @Local_Structure_FK + ' in (select Entity_Structure_fk from epacube.entity_identification eiv with (nolock) where data_name_fk = 143110 ))'
	else isnull(@SQLcodeStart, '')
	End

	End
NextFilter:		

	FETCH NEXT FROM DNFltrCode INTO @Local_Column, @QTbl, @FILTER_ENTITY_CLASS_CR_FK, @Column_Entity_Class_CR_FK, @DN_Label, @Local_Structure_FK, @DN_Type, @Op, @VirtualJoin, @Alias, @Ref_DN_FK, @Cust_Structure_Column, @Prod_Structure_Column, @VirtualAction, @VirtualColumnCheck, @Virtual_DN_FK, @DNtmp, @Pass
	End--Fetch
	Close DNFltrCode;
	Deallocate DNFltrCode;

		Set @SQLcodeIn = @SQLcodeIn + Case when @SQLcodeIn <> '' then  ')' else '' end
		Set @SQLcodeNotIn = @SQLcodeNotIn + Case when @SQLcodeNotIn <> '' then  ')' else '' end

		Set @Filtercode = ( @SQLcodeStart + '
		' + @SQLcodeIn + '
		' + @SQLcodeNotIn + ')')

If (select count(*) from EPACUBE.z_DATA_NAME_GROUP_REFERENCES R where data_name_fk = @Data_Name_FK and alias_dv is not null) > 0
						Begin
							Set @POS = charindex(' from ', @Filtercode)
							Set @Filtercode = Right(@Filtercode, Len(@Filtercode) - @POS)
							Set @Filtercode = 'Select data_value_fk ' + @Filtercode
							Set @Filtercode = 
								'(select value ''KEY'', [Description] ''LABEL'' from epacube.data_value where data_value_id in (' + @Filtercode + ')' End

		If ltrim(rtrim(@Filtercode)) > ' '
		Begin
			Set @FilterCode = '((((((' + ltrim(rtrim(@FilterCode)) + '))))))'
			Set @FilterCode = Replace(Replace(@FilterCode, '(((((((', ''), ')))))))', '')
		End				--Set @FilterCode = Right(ltrim(rtrim(@FilterCode)), Len(ltrim(rtrim(@FilterCode)) - 1))

		If Len(@FilterCode) > 10
		Set @QueryScript = @QueryScript + 'Select ' + @Data_Name_FK + ' ''Data_Name_FK'', ''' + Replace(@FilterCode, '''', '''''') + ''' ''SQL_Query'' Union
'
	End --End of While Loop

	Set @QueryScript = Case when Right(ltrim(rtrim(@QueryScript)), 7) like '%Union%' then Left(ltrim(rtrim(@QueryScript)), Len(ltrim(rtrim(@QueryScript))) - 7) else @QueryScript end


	If @FilterResultsPrint = 1
	Begin
		Select @QueryScript
		
		--Print(@QueryScript)

		goto EOP
		
	end

	If @FilterResultsExec = 1
	Begin
		Exec(@QueryScript)
		GoTo EOP
	end
	Else
	If @FilterResultsExec = 2
	Begin
		Exec(@Filtercode)
		
		goto EOP
		
	end
Last_Step:
	
	--Select @QueryScript = Case when @Lookup_Values = 2 then Replace(Replace(Replace((Select initialization_sql from epacube.ui_filter where data_name_fk = @Data_Name_FK), ' .', '.'), '(', ''), ')', '') else '' end
	
EOP:

If Object_ID('TempTbls.tmp_Prod_' + @UserID) is not null 
	Begin
		Declare @S0 table(v nvarchar(max))
		Declare @S1 varchar(max)
		Declare @S2 Varchar(max)
		Declare @Cnt int

		Declare @Cst int
		Set @Cst = Case when Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null then 1 else 0 end

		Set @S1 = 'Select isnull((Select count(*) from TempTbls.tmp_Prod_' + @UserID + '), 0)'

		Insert into @S0
		Exec(@S1)

		Set @Cnt = (Select top 1 * from @S0)

		If @Cnt = 0
		Begin
			Set @S2 = (Select TOP 1 'Insert into epacube.TempTbls.tmp_Prod_' + @UserID + ' Select distinct t1.product_structure_fk from ' + isc.table_schema + '.' + isc.table_name + ' T1 inner join epacube.data_value dv on t1.data_value_fk = dv.data_value_id ' + '
			' + Case when @Cst = 1 then ' inner join epacube.TempTbls.tmp_Cust_' + @UserID + ' C1 on T1.entity_structure_fk = C1.cust_entity_structure_fk ' else '' end + '
			' + 'where dv.data_name_fk = ' + F.critDN + ' and dv.value = ''' + F.critvalue + ''' and T1.Product_Structure_FK is not null ' + '
			' + Case when @CST = 0 then ' and t1.entity_structure_fk is null' else '' end
			from #Param3 F with (nolock)
			inner join epacube.data_name dn with (nolock) on F.CritDN = dn.DATA_NAME_ID
			inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id
			inner join epacube.INFORMATION_SCHEMA.columns isc with (nolock) on ds.table_name = isc.table_name and ds.COLUMN_NAME = isc.COLUMN_NAME
			left join epacube.data_value dv with (nolock) on f.CritDN = dv.data_name_fk and f.CritValue = dv.VALUE
			where ds.column_name = 'Data_Value_FK' Order by F.CritDN)

			Exec(@S2)
		End
End

--If Object_ID('TempTbls.tmp_Prod_' + @UserID) is not null 
--	Begin
--		Declare @S0 table(v nvarchar(max))
--		Declare @S1 varchar(max)
--		Declare @S2 Varchar(max)
--		Declare @Cnt int

--		Set @S1 = 'Select isnull((Select count(*) from TempTbls.tmp_Prod_' + @UserID + '), 0)'

--		Insert into @S0
--		Exec(@S1)

--		Set @Cnt = (Select top 1 * from @S0)

--		If @Cnt = 0
--		Begin
--			Set @S2 = (Select 'Insert into epacube.TempTbls.tmp_Prod_' + @UserID + ' Select distinct t1.product_structure_fk from ' + isc.table_schema + '.' + isc.table_name + ' T1 inner join epacube.data_value dv on t1.data_value_fk = dv.data_value_id where dv.data_name_fk = ' + F.critDN + ' and dv.value = ''' + F.critvalue + ''' and T1.Product_Structure_FK is not null'
--			from #Param3 F with (nolock)
--			inner join epacube.data_name dn with (nolock) on F.CritDN = dn.DATA_NAME_ID
--			inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id
--			inner join epacube.INFORMATION_SCHEMA.columns isc with (nolock) on ds.table_name = isc.table_name and ds.COLUMN_NAME = isc.COLUMN_NAME
--			left join epacube.data_value dv with (nolock) on f.CritDN = dv.data_name_fk and f.CritValue = dv.VALUE
--			where ds.column_name = 'Data_Value_FK')

--			Exec(@S2)
--		End
--End
