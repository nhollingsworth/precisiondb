﻿


















-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
--
-- Purpose: Purge Data Names that have a status of Purge
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/21/2010   Initial version
--						  
--
--
CREATE PROCEDURE [epacube].[UTIL_PURGE_DATA_NAME] 
AS  
BEGIN


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @status_desc        varchar(max);

----  ASSUMES THAT ALL THE FOLLOWING DATA IS CLEARED PRIOR TO RUNNING
------IMPORT, STAGE, EVENT, EXPORT, RULES, ENTITY, AND PRODUCT TABLES 
----  SO NOT CHECKING FOR EXISTANCE OF DATA_NAMES IN THESE TABLES           

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      SET @status_desc  = 'started execution of epacube.UTIL_PURGE_DATA_NAME' 
	  
      EXEC exec_monitor.start_monitor_sp @exec_desc = @status_desc
                                                  ,@epa_job_id = NULL,  @epa_batch_no = NULL
												  ,@exec_id = @l_exec_no OUTPUT


     SET @l_sysdate = GETDATE()



-------------------------------------------------------------------------------------
--   Import Map Metadata
-------------------------------------------------------------------------------------


	DELETE 
	FROM import.IMPORT_MAP_METADATA
	WHERE DATA_NAME_FK IN 
	(
	SELECT DATA_NAME_ID
	FROM epacube.DATA_NAME
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from import.IMPORT_MAP_METADATA table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END



	DELETE 
	FROM import.IMPORT_MAP_METADATA
	WHERE IDENT_DATA_NAME_FK IN 
	(
	SELECT DATA_NAME_ID
	FROM epacube.DATA_NAME
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from import.IMPORT_MAP_METADATA table by IDENT_DN = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END




	DELETE 
	FROM import.IMPORT_MAP_METADATA
	WHERE ASSOC_DATA_NAME_FK IN 
	(
	SELECT DATA_NAME_ID
	FROM epacube.DATA_NAME
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from import.IMPORT_MAP_METADATA table by ASSOC_DN = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END



-------------------------------------------------------------------------------------
--   Config Tables
-------------------------------------------------------------------------------------

	DELETE 
	FROM epacube.CONFIG_DATA_NAME
	WHERE DATA_NAME_FK IN 
	(
	SELECT DATA_NAME_ID
	FROM epacube.DATA_NAME
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from epacube.CONFIG_DATA_NAME table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END


	DELETE 
	FROM epacube.CONFIG
	WHERE DATA_NAME_FK IN 
	(
	SELECT DATA_NAME_ID
	FROM epacube.DATA_NAME
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from epacube.CONFIG table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END


	DELETE 
	FROM epacube.DATA_NAME_PROPERTIES
	WHERE DATA_NAME_FK IN 
	(
	SELECT DATA_NAME_ID
	FROM epacube.DATA_NAME
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from epacube.DATA_NAME_PROPERTIES table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END




-------------------------------------------------------------------------------------
--   Rules
-------------------------------------------------------------------------------------


	UPDATE synchronizer.RULES
	SET RECORD_STATUS_CR_FK = 0  -- PURGE
	WHERE RESULT_DATA_NAME_FK IN 
	(
	SELECT DATA_NAME_ID
	FROM epacube.DATA_NAME
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows marked for Purge from synchronizer.RULES by RESULT_DN = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END



	UPDATE synchronizer.RULES
	SET RECORD_STATUS_CR_FK = 0  -- PURGE
	WHERE TRIGGER_EVENT_DN_FK IN 
	(
	SELECT DATA_NAME_ID
	FROM epacube.DATA_NAME
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows marked for Purge from synchronizer.RULES by TRIGGER_DN = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END



	UPDATE synchronizer.RULES
	SET RECORD_STATUS_CR_FK = 0  -- PURGE
	WHERE RULES_ID IN (
	SELECT RA.RULES_FK
	FROM synchronizer.RULES_ACTION RA
	INNER JOIN epacube.DATA_NAME DN
	 ON ( DN.DATA_NAME_ID = RA.BASIS1_DN_FK )
	WHERE DN.RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows marked for Purge from synchronizer.RULES by BASIS1_DN = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END



	UPDATE synchronizer.RULES
	SET RECORD_STATUS_CR_FK = 0  -- PURGE
	WHERE RULES_ID IN (
	SELECT RA.RULES_FK
	FROM synchronizer.RULES_ACTION RA
	INNER JOIN epacube.DATA_NAME DN
	 ON ( DN.DATA_NAME_ID = RA.BASIS2_DN_FK )
	WHERE DN.RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows marked for Purge from synchronizer.RULES by BASIS2_DN = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END






-------------------------------------------------------------------------------------
--   EXECUTE REMOVE RULES 
-------------------------------------------------------------------------------------

   EXEC  epacube.UTIL_PURGE_RULES

-------------------------------------------------------------------------------------
--   Data Values
-------------------------------------------------------------------------------------

	DELETE 
	FROM epacube.DATA_VALUE
	WHERE DATA_NAME_FK IN 
	(
	SELECT DATA_NAME_ID
	FROM epacube.DATA_NAME
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from epacube.DATA_VALUE table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END


-------------------------------------------------------------------------------------
--   Permissions
-------------------------------------------------------------------------------------

	DELETE 
	FROM epacube.PERMISSION_DATA_NAMES
	WHERE DATA_NAME_FK IN 
	(
	SELECT DATA_NAME_ID
	FROM epacube.DATA_NAME
	WHERE RECORD_STATUS_CR_FK = 0
	) 

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from epacube.PERMISSION_DATA_NAMES table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END



------------------------------------------------------------------------------------------
--   REMOVE COLUMNS MARKED WTIH RECORD_STATUS_CR_FK = 0 FOR PURGE 
------------------------------------------------------------------------------------------

		
	DELETE 
	FROM epacube.DATA_NAME
	WHERE  record_status_cr_fk = 0

    SET @l_rows_processed = @@ROWCOUNT
    IF @l_rows_processed > 0
    BEGIN
		SET @status_desc = 'Total rows cleared from epacube.DATA_NAME table = '
			+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(20))
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
    END
	   

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of epacube.UTIL_PURGE_DATA_NAME';
     Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, 
                                               @epa_job_id = NULL,  @epa_batch_no = NULL
                                               
     
      
      Exec exec_monitor.print_status_sp @exec_id = @l_exec_no;
   END TRY
   BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;
      

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = NULL;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState, -- State.
					NULL 
				    );
   END CATCH;
END




















































