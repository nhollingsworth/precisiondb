﻿







-- Copyright 2006 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        03/23/2014   Initial SQL Version
-- CV        04/28/2014   Adding logic to check Promo data name is least amount in Sheet Results. 
-- CV        04/30/2014   Remove logic to it's own procedure
      


CREATE PROCEDURE [epacube].[NIGHTLY_ARCHIVE_SHEET_RESULTS] 
AS
BEGIN
        DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @job_timestamp DATETIME

		
		

        BEGIN TRY
            SET NOCOUNT ON ;
            SET @l_exec_no = 0
            SET @l_sysdate = GETDATE()
										  

							
            SET @ls_stmt = 'started execution of epacube.Nightly Sheet Results Archive. '
                 EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;
                


                
--		Create a stored procedure to run with NIGHTLY SYNCHRONIZER so when there are multiple rows for the same data name,
-- product_structure and whse, as soon as the effective date has passed, 
--we will move it out of marginmgr.sheet_results_values_future and put it in the marginmgr.sheet_results_values_ARCHIVE table.

--Note:  Johnstone may have two promos with effective dates/end dates that overlap so this archive process 
--needs to consider these and only archive when the End Date is less than today's date. 
-- This is so both will be available for the calculation job until such time that one of the two becomes expired.

--The reason for moving this out of the future table once the effective date has been recognized is because the corresponding 
--event should have moved from FUTURE to APPROVED.  The data for this event will show as CURRENT data 
--from marginmgr.SHEET_RESULTS_VALUES_FUTURE table.

--The archive table is the same as the future table as far as columns and unique key.

INSERT INTO [marginmgr].[SHEET_RESULTS_VALUES_ARCHIVE]
           ([DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_CLASS_CR_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[NET_VALUE1]
           ,[NET_VALUE2]
           ,[NET_VALUE3]
           ,[NET_VALUE4]
           ,[NET_VALUE5]
           ,[NET_VALUE6]
           ,[NET_VALUE7]
           ,[NET_VALUE8]
           ,[NET_VALUE9]
           ,[NET_VALUE10]
           ,[PREV_VALUE1]
           ,[PREV_VALUE2]
           ,[PREV_VALUE3]
           ,[PREV_VALUE4]
           ,[PREV_VALUE5]
           ,[PREV_VALUE6]
           ,[PREV_VALUE7]
           ,[PREV_VALUE8]
           ,[PREV_VALUE9]
           ,[PREV_VALUE10]
           ,[PERCENT_CHANGE1]
           ,[PERCENT_CHANGE2]
           ,[PERCENT_CHANGE3]
           ,[PERCENT_CHANGE4]
           ,[PERCENT_CHANGE5]
           ,[PERCENT_CHANGE6]
           ,[PERCENT_CHANGE7]
           ,[PERCENT_CHANGE8]
           ,[PERCENT_CHANGE9]
           ,[PERCENT_CHANGE10]
           ,[VALUE_UOM_CODE_FK]
           ,[ASSOC_UOM_CODE_FK]
           ,[UOM_CONVERSION_FACTOR]
           ,[VALUE_EFFECTIVE_DATE]
           ,[PREV_PRODUCT_UOM_CLASS_FK]
           ,[PREV_EFFECTIVE_DATE]
           ,[PREV_END_DATE]
           ,[PREV_UPDATE_TIMESTAMP]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK]
           ,[UPDATE_USER]
           ,[PRICESHEET_NAME])
(
select [DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_CLASS_CR_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[NET_VALUE1]
           ,[NET_VALUE2]
           ,[NET_VALUE3]
           ,[NET_VALUE4]
           ,[NET_VALUE5]
           ,[NET_VALUE6]
           ,[NET_VALUE7]
           ,[NET_VALUE8]
           ,[NET_VALUE9]
           ,[NET_VALUE10]
           ,[PREV_VALUE1]
           ,[PREV_VALUE2]
           ,[PREV_VALUE3]
           ,[PREV_VALUE4]
           ,[PREV_VALUE5]
           ,[PREV_VALUE6]
           ,[PREV_VALUE7]
           ,[PREV_VALUE8]
           ,[PREV_VALUE9]
           ,[PREV_VALUE10]
           ,[PERCENT_CHANGE1]
           ,[PERCENT_CHANGE2]
           ,[PERCENT_CHANGE3]
           ,[PERCENT_CHANGE4]
           ,[PERCENT_CHANGE5]
           ,[PERCENT_CHANGE6]
           ,[PERCENT_CHANGE7]
           ,[PERCENT_CHANGE8]
           ,[PERCENT_CHANGE9]
           ,[PERCENT_CHANGE10]
           ,[VALUE_UOM_CODE_FK]
           ,[ASSOC_UOM_CODE_FK]
           ,[UOM_CONVERSION_FACTOR]
           ,[VALUE_EFFECTIVE_DATE]
           ,[PREV_PRODUCT_UOM_CLASS_FK]
           ,[PREV_EFFECTIVE_DATE]
           ,[PREV_END_DATE]
           ,[PREV_UPDATE_TIMESTAMP]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK]
           ,[UPDATE_USER]
           ,[PRICESHEET_NAME] from marginmgr.SHEET_RESULTS_VALUES_FUTURE where EFFECTIVE_DATE  < getdate() 
and isnull(end_date, getdate())  < = getdate())


delete 
from marginmgr.SHEET_RESULTS_VALUES_FUTURE where EFFECTIVE_DATE  < getdate() 
and isnull(end_date, getdate())  < = getdate()
------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

            SET @status_desc = 'finished execution of epacube.Nightly Sheet Result Active'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;

        END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of SYNCHRONIZER.epacube.Nightly Sheet Result Archive has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH
		  
    END








