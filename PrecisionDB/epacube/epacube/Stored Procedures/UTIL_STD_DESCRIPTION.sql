﻿











-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott 
--
--
-- Purpose: Description Parseing
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        08/12/2013   Initial Version 
-- CV        08/16/2013   Tighen up code for loop
-- CV        09/06/2013   Use New table 



CREATE PROCEDURE [epacube].[UTIL_STD_DESCRIPTION] ( @IN_JOB_FK bigint ) 
    
        
AS BEGIN 

-----------------------------------------------------------------------------------------------
--                   *********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
       DECLARE @V_START_TIMESTAMP  DATETIME
     DECLARE @V_END_TIMESTAMP    DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
        DECLARE @v_exception_rows        bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
       DECLARE @time_after datetime;
       DECLARE @elapsed_time numeric(10,4);
       DECLARE @rows_affected varchar(max);
       DECLARE @rows_dml bigint;
       DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int     
     DECLARE @v_lookup_value1   varchar(50)
     DECLARE @v_lookup_value2   Varchar(50) 
	 DECLARE @v_lookup_value3   Varchar(50)
       
          

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of Epacube.UTIL_STD_DESCRIPTION', 
                                                                                  @exec_id = @l_exec_no OUTPUT,
                                                                                  @epa_job_id = NULL;

--     SET @l_sysdate = GETDATE()
SET @V_START_TIMESTAMP = GETDATE()
SET @v_lookup_value1 = 'ECL BUY LINE'
SET @v_lookup_value2 = 'VENDOR CODE'
SET @v_lookup_value3 = 'VENDOR NUMBER'  --- not using right now.



--------------------------------------------------------------------------
----ADDED NEW values to map metdata for the package 309000
---- position 97 = system for completeness rule set using import sql
---- position 98 = buyline  data value id
---- positon 99 = vendor entity structure fk
---- position 100 = vendor Name - used in putting back together description
--------------------------------------------------------------------------

update import.import_record_data
set data_position98 = a.buyline
,data_position99 = a.vendor
,data_position100 = a.vendor_name
from  (
select distinct  ird.import_record_data_id, dv.data_value_id buyline, ei.entity_structure_fk vendor
,ei.value  vendor_name
from import.import_record_data ird with (nolock)
inner join epacube.data_value dv with (nolock)
on (ird.data_position8 = dv.value
and dv.data_name_fk = (select data_name_id from epacube.data_Name with (nolock) 
									where name = @v_lookup_value1) ) --'ECL BUY LINE'310904
inner join epacube.DESCRIPTION_CONVERSION bvc with (nolock)
on (dv.data_value_id = bvc.lookup_data_value_fk)
inner join epacube.entity_identification ei with (nolock)
on (ei.entity_structure_fk = bvc.vendor_entity_Structure_fk
and ei.data_name_fk = (select data_name_id from epacube.data_Name with (nolock) 
									where name = @v_lookup_value2))  --'VENDOR CODE'143111)
where ird.job_fk = @in_job_fk) a 
where a.import_record_data_id = import.import_record_data.import_record_data_id


--------------------------------------------------------------------------
----ADDED NEW RULE SO CAN USE EXISTING CODE BASE
---- position 8 = buyline  -310904
---- position 3 = description
---- setting data position 101 to the stripped description
--------------------------------------------------------------------------
-----FIRST REMOVE SPECIAL CHARACTERS   --Added rule ---2000000001 =REMOVE SPECIAL CHARACTERS - HAJOCA DESC
--to modify the characters to remove. update the rules  currently set to this ,!""
--select * from synchronizer.RULES_ACTION_STRFUNCTION where rules_action_fk = 2000000002

update import.import_record_data
set data_position101 = (SELECT epacube.ADD_BASIS_TOKEN 
										 ( 1, NULL, NULL, data_position3, 2000000001 ) )
where job_fk = @IN_JOB_FK

-- 
----------------------------------------------------------------------------------------------------------------	
---Start working the Description - need to do word conversion first 
--- if word conversion is found in description.  Remove it so that Abbreviations are  trying to use
--- values from word conversion.. 
-----------------------------------------------------------------------------------------------------------------

TRUNCATE TABLE [dbo].[Temp_Description_Work]


INSERT INTO [dbo].[Temp_Description_Work]
           ([import_record_data_fk]
           ,[Job_fk]
           ,[Vendor_entity_structure_Fk]
           ,[Imported_Description]
		   ,[New_Desc]
           ,[Search_Word]
           ,[Search_Description]
           ,[Data_Category]
           ,[Display_seq])
select distinct
ird.import_record_data_id
,ird.job_fk
 ,cast(ird.data_position99 as int) entity_id
,ird.data_position101
,replace(ird.data_position101,dvsearchword.value,'') NEW_DESC  ---need to remove so abbreviations don't look at it.
,dvsearchword.value Searchword
,dvsearchword.description description
,dvcat.value
,orderdesc.display_seq 
--select * 
from import.import_record_data ird with (nolock)
inner join epacube.DESCRIPTION_CONVERSION bvc with (nolock)
on (bvc.record_status_cr_fk = 1
and cast(ird.data_position98 as int) = bvc.lookup_data_value_fk
and cast(ird.data_Position99 as int) = bvc.vendor_entity_structure_fk
and bvc.data_name_fk = (select data_name_id from epacube.data_name where name = 'Word Conversion'))
inner join epacube.entity_data_value dvsearchword with (nolock)
on (dvsearchword.entity_data_value_id = bvc.entity_data_value1_Fk
and ird.data_position101 like ('%'+ dvsearchword.value + '%'))
INNER join epacube.DESCRIPTION_CONVERSION orderdesc with (nolock)
on (orderdesc.record_status_cr_fk = 1
--and orderdesc.entity_data_value1_Fk = bvc.data_category_edv_fk
and orderdesc.vendor_entity_structure_fk = bvc.vendor_entity_structure_fk
and orderdesc.data_name_fk = (select data_name_id from epacube.data_name where name = 'DESC CONVERSION'))
INNER JOIN epacube.entity_data_value dvcat with (nolock)
on (dvcat.entity_data_value_id = orderdesc.entity_data_value1_fk)
and ird.job_fk =  @in_job_fk


----
----select distinct
----ird.import_record_data_id
----,ird.job_fk
---- ,cast(ird.data_position99 as int) entity_id
----,ird.data_position101
----,replace(ird.data_position101,bvc.data_value1,'') NEW_DESC
----,bvc.data_value1 Searchword
----,bvc.data_value1_description
----,bvc.data_category
----,orderdesc.display_seq 
----
----from import.import_record_data ird with (nolock)
----inner join epacube.DESCRIPTION_CONVERSION bvc with (nolock)
----on (bvc.record_status_cr_fk = 1
----and cast(ird.data_position98 as int) = bvc.Buyline_data_value_fk
----and cast(ird.data_Position99 as int) = bvc.vendor_entity_structure_fk
----and bvc.data_name = 'Word Conversion'
----and ird.data_position101 like ('%'+ bvc.data_value1 + '%'))
----inner join epacube.DESCRIPTION_CONVERSION orderdesc with (nolock)
----on (orderdesc.record_status_cr_fk = 1
----and orderdesc.data_value1 = bvc.data_category
----and orderdesc.vendor_entity_structure_fk = bvc.vendor_entity_structure_fk
----and orderdesc.data_name = 'DESC CONVERSION')
----and ird.job_fk = @in_job_fk

------------------------------------------------------------------------------
---update import record data with the latest stripped description using 
--- data position 102 and if there was nothing to strip then set the value to original
--- starting data position to parse through for other pieces
-------------------------------------------------------------------------------

update import.import_record_data
set data_position102 = a.new_desc
from (select tmp.new_desc new_desc, tmp.import_record_data_fk
from dbo.temp_description_work tmp )a
where import_record_data_id = a.import_record_data_fk

update import.import_record_data
set data_position102 = data_position101
where data_position102 is NULL
and job_fk = @in_job_fk
--------------------------------------------------------------------------------------
---Start working on the description - Abbreviations
--------------------------------------------------------------------------------------

INSERT INTO [dbo].[Temp_Description_Work]
           ([import_record_data_fk]
           ,[Job_fk]
           ,[Vendor_entity_structure_Fk]
           ,[Imported_Description]
		   ,[New_Desc]
           ,[Search_Word]
           ,[Search_Description]
           ,[Data_Category]
           ,[Display_seq])
select distinct
ird.import_record_data_id
,ird.job_fk
 ,cast(ird.data_position99 as int) entity_id
,ird.data_position101
,replace(ird.data_position101,dvsearchword.value,'') NEW_DESC  ---need to remove so abbreviations don't look at it.
,dvsearchword.value Searchword
,dvsearchword.description description
,dvcat.value
,orderdesc.display_seq 

from import.import_record_data ird with (nolock)
inner join epacube.DESCRIPTION_CONVERSION bvc with (nolock)
on (bvc.record_status_cr_fk = 1
and cast(ird.data_position98 as int) = bvc.lookup_data_value_fk
and cast(ird.data_Position99 as int) = bvc.vendor_entity_structure_fk
and bvc.data_name_fk = (select data_name_id from epacube.data_name where name = 'ABBREVIATION'))
inner join epacube.entity_data_value dvsearchword with (nolock)
on (dvsearchword.entity_data_value_id = bvc.entity_data_value1_Fk
and ird.data_position102 like ('% '+ dvsearchword.value + ' %'))
inner join epacube.DESCRIPTION_CONVERSION orderdesc with (nolock)
on (orderdesc.record_status_cr_fk = 1
and orderdesc.entity_data_value1_Fk = bvc.data_category_edv_fk
and orderdesc.vendor_entity_structure_fk = bvc.vendor_entity_structure_fk
and orderdesc.data_name_fk = (select data_name_id from epacube.data_name where name = 'DESC CONVERSION'))
inner JOIN epacube.entity_data_value dvcat with (nolock)
on (dvcat.entity_data_value_id = orderdesc.entity_data_value1_fk)
and ird.job_fk = @in_job_fk
order by ird.import_record_data_id


--select 
--ird.import_record_data_id
--,ird.job_fk
-- ,cast(ird.data_position99 as int) entity_id
--,ird.data_position101
--,replace(ird.data_position102,bvc.data_value1,'') NEW_DESC
--,bvc.data_value1 Searchword
--,bvc.data_value1_description
--,bvc.data_category
--,orderdesc.display_seq 
--
--from import.import_record_data ird with (nolock)
--inner join epacube.DESCRIPTION_CONVERSION bvc with (nolock)
--on (bvc.record_status_cr_fk = 1
--and cast(ird.data_position98 as int) = bvc.Buyline_data_value_fk
--and cast(ird.data_Position99 as int) = bvc.vendor_entity_structure_fk
--and bvc.data_name = 'ABBREVIATION'
--and ird.data_position102 like ('% '+ bvc.data_value1 + ' %'))
--inner join epacube.DESCRIPTION_CONVERSION orderdesc with (nolock)
--on (orderdesc.record_status_cr_fk = 1
--and orderdesc.data_value1 = bvc.data_category
--and orderdesc.vendor_entity_structure_fk = bvc.vendor_entity_structure_fk
--and orderdesc.data_name = 'DESC CONVERSION')
-------
--where ird.job_fk = @in_job_fk
--order by ird.import_record_data_id


----call import sql for part number conversion if necessary

update import.import_record_data 
set data_position200 = substring(data_position4,1,5) + '-'+ substring(data_position4,6,1)
+ '-'+ substring(data_position4,7,1)+ '-'+ substring(data_position4,8,1)+ '-'+ substring(data_position4,9,2)
from import.import_record_data 
where  len(data_position4) = 10
and data_position100 = 'CONBRACO'
and job_fk = @in_job_fk

update import.import_record_data 
set data_position200 = substring(data_position4,1,3) + '-'+ substring(data_position4,5,1)
+ '-'+ substring(data_position4,6,1)+ '-'+ substring(data_position4,8,1)+ '-'+ substring(data_position4,7,2)
from import.import_record_data 
where len(data_position4) = 8
and data_position100 = 'CONBRACO'
and job_fk = @in_job_fk

--------------------------------------------------------------------------------------------------------------
--- Next add Loop Not sure how many part number versions we could have per vendor
--- loop because we don't know how many different part formats we have to deal with
--------------------------------------------------------------------------------------------------------------
DECLARE @v_Parts varchar(50)
                


	DECLARE cur_v_import CURSOR local for
		select  distinct dn.name  
from epacube.DESCRIPTION_CONVERSION bvcpart WITH (nolock)
inner join epacube.data_name dn with (nolock)
on (bvcpart.data_name_fk = dn.data_name_id)
inner join dbo.temp_description_work tdw with (nolock) 
on (bvcpart.vendor_entity_structure_fk = tdw.vendor_entity_structure_fk
and bvcpart.record_Status_cr_fk = 1)
inner join import.import_record_data ird with (nolock)
on (ird.import_record_data_id = tdw.import_record_data_fk
and cast(ird.data_position98 as int) = bvcpart.lookup_data_value_fk
and cast(ird.data_Position99 as int) = tdw.vendor_entity_structure_fk)
inner join epacube.entity_data_value dv with (nolock)
on (dv.value = 'PART'
and dv.entity_structure_fk = bvcpart.vendor_entity_structure_fk
and dv.data_name_fk = (select data_name_id from epacube.data_name where name = 'DATA CATEGORY'))
where bvcpart.data_category_edv_fk = dv.entity_data_value_id


--select * from epacube.entity_data_value where value = 'part'



         OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO 
				@v_parts

                
			   

      WHILE @@FETCH_STATUS = 0
      BEGIN

INSERT INTO [dbo].[Temp_Description_Work]
           ([import_record_data_fk]
           ,[Job_fk]
           ,[Vendor_entity_structure_Fk]
           ,[Imported_Description]
		   ,[New_Desc]
           ,[Search_Word]
          ,[Search_Description]
           ,[Data_Category]
           ,[Display_seq])
select 
ird.import_record_data_id
,ird.job_fk
,bvc.vendor_entity_structure_fk
,ird.data_position102
,ird.data_position200 --new_desc
,dv.value
,replace(ird.data_position200,'-', ' ' )  search_desc
,dn.name
,0
-------
from import.import_record_data ird with (nolock)
inner join epacube.DESCRIPTION_CONVERSION bvc with (nolock)
on (bvc.record_status_cr_fk = 1
and cast(ird.data_position98 as int) = bvc.lookup_data_value_fk
and cast(ird.data_Position99 as int) = bvc.vendor_entity_structure_fk)
inner join epacube.data_name dn with (nolock)
on (dn.name = @v_Parts
and dn.data_name_id = bvc.data_name_fk)
inner join epacube.entity_data_value dv with (nolock) 
on (dv.entity_data_value_id = bvc.entity_data_value1_fk)
where ird.data_position4 like (dv.value)
and ird.job_fk = @in_job_fk  
   
and ird.import_record_data_id not in (    
select import_record_data_fk from dbo.temp_description_work where display_seq = 0)


--
--INSERT INTO [dbo].[Temp_Description_Work]
--           ([import_record_data_fk]
--           ,[Job_fk]
--           ,[Vendor_entity_structure_Fk]
--           ,[Imported_Description]
--		   ,[New_Desc]
--           ,[Search_Word]
--          ,[Search_Description]
--           ,[Data_Category]
--           ,[Display_seq])
--select 
--ird.import_record_data_id
--,ird.job_fk
--,bvc.vendor_entity_structure_fk
--,ird.data_position102
--,substring(ird.data_position102,
--PATINDEX('%' + dv.value,ird.data_position102) ---start  --- the 3 needs to be a variable how to find that first space????
--			,len(ird.data_position102)- charindex('-', reverse(ird.data_position102),1)  ) new_desc
--,dv.value
--,replace (substring(ird.data_position102,
--PATINDEX('%' + dv.value,ird.data_position102) ---start  the 3 needs to be a variable how to find that first space????
--			,len(ird.data_position102)- charindex('-', reverse(ird.data_position102),1)),'-',' ')   search_desc
--,dn.name
--,0
---------
--from import.import_record_data ird with (nolock)
--inner join epacube.DESCRIPTION_CONVERSION bvc with (nolock)
--on (bvc.record_status_cr_fk = 1
--and cast(ird.data_position98 as int) = bvc.lookup_data_value_fk
--and cast(ird.data_Position99 as int) = bvc.vendor_entity_structure_fk)
--inner join epacube.data_name dn with (nolock)
--on (dn.name = @v_Parts
--and dn.data_name_id = bvc.data_name_fk)
--inner join epacube.entity_data_value dv with (nolock) 
--on (dv.entity_data_value_id = bvc.entity_data_value1_fk)
--where ird.data_position102 like ('%' + dv.value)
----outer left join dbo.temp_description_work tdw with (nolock)
----on ird.import_record_data_id = tdw.import_record_data_fk 
----and tdw display_seq = 0)
--and ird.job_fk = @in_job_fk  
--   
--and ird.import_record_data_id not in (    
--select import_record_data_fk from dbo.temp_description_work where display_seq = 0)
--

-----old code
----select 
----ird.import_record_data_id
----,ird.job_fk
----,bvc.vendor_entity_structure_fk
----,ird.data_position102
----,substring(ird.data_position102,
----CHARINDEX('K-', ird.data_position102, 1)---start
----			,len(ird.data_position102)- charindex('-', reverse(ird.data_position102),1)  ) new_desc
----,bvc.data_value1
----,replace (substring(ird.data_position102,
----CHARINDEX('K-', ird.data_position102, 1)---start
----			,len(ird.data_position102)- charindex('-', reverse(ird.data_position102),1)),'-',' ')   search_desc
----,bvc.data_name
----,0
-----------
----
----from import.import_record_data ird with (nolock)
----inner join epacube.DESCRIPTION_CONVERSION bvc with (nolock)
----on (bvc.record_status_cr_fk = 1
----and cast(ird.data_position98 as int) = bvc.Buyline_data_value_fk
----and cast(ird.data_Position99 as int) = bvc.vendor_entity_structure_fk
----and bvc.data_name = @v_Parts)
----where ird.data_position102 like ('%' + bvc.data_value1)
------outer left join dbo.temp_description_work tdw with (nolock)
------on ird.import_record_data_id = tdw.import_record_data_fk 
------and tdw display_seq = 0)
----and ird.job_fk = @in_job_fk  
----   
----and ird.import_record_data_id not in (    
----select import_record_data_fk from dbo.temp_description_work where display_seq = 0)



------------------------------------------


INSERT INTO [dbo].[Temp_Description_Work]
           ([import_record_data_fk]
           ,[Job_fk]
           ,[Vendor_entity_structure_Fk]
           ,[Imported_Description]
		   ,[New_Desc]
           ,[Search_Word]
          ,[Search_Description]
           ,[Data_Category]
           ,[Display_seq])
select distinct
tdw.import_record_data_fk
,tdw.job_fk
,tdw.vendor_entity_structure_fk
,tdw.imported_description
,tdw.search_description
,dv4.value search_word
,dv4.description 
,dn.name
,bvc4.display_seq
------
--select bvc1.data_name_fk, bvc2.*
from epacube.DESCRIPTION_CONVERSION  bvc1 with (nolock)
inner join epacube.entity_data_value dv1 with (nolock)
on (bvc1.entity_data_value1_fk = dv1.entity_data_value_id)
inner join epacube.data_name dn1 with (nolock)
on (dn1.data_name_id = bvc1.data_name_fk)
------- this brings in the pieces
inner join epacube.DESCRIPTION_CONVERSION  bvc2 with (nolock)
on (bvc2.record_status_cr_fk = 1)
inner join epacube.entity_data_value dv2 with (nolock)
on (dv2.data_name_fk = bvc1.data_name_fk
and dv2.value = dn1.name)
inner join epacube.data_name dn
on (dn.data_name_id = bvc2.part_format_dn_fk)
----- this get the values we need to scan for in the parts
inner join epacube.DESCRIPTION_CONVERSION  bvc3 with (nolock)
on (bvc3.record_status_cr_fk = 1
and bvc2.part_format_dn_Fk = bvc3.data_name_fk)
----This gets seq id for parts positions
inner join epacube.entity_data_value dv4 with (nolock)
on (dv4.entity_data_value_id = bvc3.entity_data_value1_fk)
inner join epacube.DESCRIPTION_CONVERSION  bvc4 with (nolock)
on (bvc4.record_status_cr_fk = 1
and bvc4.entity_data_value1_fk = bvc2.data_category_edv_fk)----
-----this gets the part format from the working table to check
inner join dbo.temp_description_work tdw with (nolock) 
on (tdw.data_category =  @v_parts
and tdw.search_description like '%'+ dv4.value + '%')
--and tdw.search_description like '% '+ dv4.value + ' %')
where bvc1.record_status_cr_fk = 1
and bvc1.vendor_entity_structure_Fk = tdw.vendor_entity_structure_fk



-------------------------old  code
--------select distinct
--------tdw.import_record_data_fk
--------,tdw.job_fk
--------,tdw.vendor_entity_structure_fk
--------,tdw.imported_description
--------,tdw.search_description
--------,bvc3.data_value1
--------,bvc3.data_value1_description
--------,bvc1.data_value1_description
--------,bvc4.display_seq
--------------
--------from epacube.DESCRIPTION_CONVERSION  bvc1 with (nolock)
--------inner join epacube.DESCRIPTION_CONVERSION  bvc2 with (nolock)
--------on (bvc2.record_status_cr_fk = 1
--------and bvc1.data_value1 = bvc2.data_name)
--------inner join epacube.DESCRIPTION_CONVERSION  bvc3 with (nolock)
--------on (bvc3.record_status_cr_fk = 1
--------and bvc1.data_value1_description = bvc3.data_name)
--------inner join epacube.DESCRIPTION_CONVERSION  bvc4 with (nolock)
--------on (bvc4.record_status_cr_fk = 1
--------and bvc4.data_value1 = bvc1.data_category)
----------
--------inner join dbo.temp_description_work tdw with (nolock) 
--------on (tdw.data_category =  @v_parts
--------and tdw.search_description like '% '+ bvc3.data_value1 + ' %')
------------
--------where bvc1.record_status_cr_fk = 1





 FETCH NEXT FROM cur_v_import Into 
				@v_parts

                


END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_import
DEALLOCATE cur_v_import    

-----------------------------------------------------------------------------------
--Clean up  --- because of duplicates because of possible multiple part formats
------------------------------------------------------------------------------------

update dbo.temp_description_work
set display_seq = 999
where temp_desc_id in (
  select a.temp_desc_id
  from (
      select pas.temp_desc_id,
      DENSE_RANK() over (Partition By import_record_data_fk
										,search_word
      Order by pas.display_seq desc ) as drank
      from dbo.temp_description_work pas with (nolock)
  ) a
  where a.drank > 1
)


delete from dbo.temp_description_work where display_seq = 999

------------------------------------------------------------------------------------------------
----  Loop to put the desciptions back together by the record data id and the display seq from working table
-------------------------------------------------------------------------------------------------

DECLARE @v_record_Id varchar(50)
                    


	DECLARE cur_v_import CURSOR local for
		select distinct import_record_data_fk from dbo.temp_description_work


         OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO 
				@v_record_Id 
                
			   

      WHILE @@FETCH_STATUS = 0
      BEGIN
DECLARE @List VARCHAR(5000)

SELECT @List = COALESCE(@List + ' ' + a.description, a.description) from
(select distinct tdw.search_description description
,tdw.display_seq seq
,tdw.import_record_data_fk
from dbo.temp_description_work  tdw with (nolock)
inner join epacube.DESCRIPTION_CONVERSION  bvc with (nolock)
on (bvc.record_status_Cr_fk = 1
and bvc.display_seq = tdw.display_seq
and bvc.vendor_entity_structure_fk = tdw.vendor_entity_structure_fk)
inner join import.import_record_data ird  with (nolock)
on (ird.import_record_data_id = tdw.import_record_data_fk)
where tdw.import_record_data_fk = @v_record_Id
and tdw.display_seq <> 0
) a
order by a.seq asc

------set the vendor for record and then the listed values
---Position105 is the Hajoca Description
update import.import_record_data
set data_position105 = data_position100 + @list
where import_record_data_id = @v_record_Id

           
SET @LIST = ''

 FETCH NEXT FROM cur_v_import Into 
				@v_record_Id
                


END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_import
DEALLOCATE cur_v_import    

--------------------------------------------------------------------------------------------------------
----More Clean up..  Remove Data for Description if the description only contains the Vendor
-----------------------------------------------------------------------------------------------------

update import.import_record_data 
set data_position105 = NULL
where data_position105 = data_position100
and job_fk = @in_job_Fk





END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
         DECLARE @ErrorSeverity INT;
         DECLARE @ErrorState INT;

              SELECT 
                     @ErrorMessage = 'Execution of epacube.Utl_std_description has failed ' + ERROR_MESSAGE(),
                     @ErrorSeverity = ERROR_SEVERITY(),
                     @ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = NULL;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
                               @ErrorSeverity, -- Severity.
                               @ErrorState -- State.
                               );
   END CATCH;


END



































