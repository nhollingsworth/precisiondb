﻿-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get product attribute information JIRA (PRE-158)
-- Get all product attribute information and return in JSON format
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[PRODUCT_TAXONOMY_ATTRIBUTE_GET_BY_PRODUCT]
	@product_structure_id BIGINT
AS
BEGIN
	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.PRODUCT_TAXONOMY_ATTRIBUTE_GET_BY_PRODUCT.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT (
			SELECT PRODUCT_TAX_ATTRIBUTE_ID
				, DN.[LABEL] AS [NAME]
				, DV.[VALUE]
			FROM epacube.PRODUCT_TAX_ATTRIBUTE PA
				INNER JOIN epacube.DATA_NAME DN
					ON PA.DATA_NAME_FK = DN.DATA_NAME_ID
				LEFT JOIN epacube.DATA_VALUE DV 
					ON PA.DATA_VALUE_FK = DV.DATA_VALUE_ID
			WHERE PRODUCT_STRUCTURE_FK = @product_structure_id
			FOR JSON PATH) [DATA]
		FOR JSON PATH, WITHOUT_ARRAY_WRAPPER;

		SELECT *
		FROM epacube.PRODUCT_TAX_ATTRIBUTE

		SET @ls_stmt = 'Finished execution of epacube.PRODUCT_TAXONOMY_ATTRIBUTE_GET_BY_PRODUCT.';
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		SET @ls_stmt = 'Execution of epacube.PRODUCT_TAXONOMY_ATTRIBUTE_GET_BY_PRODUCT has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;

	END CATCH
END