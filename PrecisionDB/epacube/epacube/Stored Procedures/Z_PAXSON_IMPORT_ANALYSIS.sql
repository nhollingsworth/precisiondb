﻿







CREATE PROCEDURE [epacube].[Z_PAXSON_IMPORT_ANALYSIS] ( @IN_JOB_FK bigint ) 
         
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        05/14/2013  Initial SQL Version

 
  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @v_batch_no   bigint
        
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT
        
 
 
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of epacube.Paxson_IMPORT_ANALYSIS. '
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT
											  ,@epa_job_id = @v_job_fk;

											  
	--EXEC seq_manager.db_get_next_sequence_value_impl 'common','batch_seq', @v_batch_no output
 

                
 --                 EXEC seq_manager.db_get_next_sequence_value_impl 'common',
 --                   'job_seq', @v_job_fk output
    
 --   SET @v_job_class_fk  = 100        --- PRODUCT MAINTENANCE
 --   SET @v_job_user = 'EPACUBE'
 --   SET @v_job_name = 'DERIVE CEDNETCATALOG'
 --   SET @v_job_date = @l_sysdate
 --   SET @v_data1   = 'LAST JOB:: '
 --                    + CAST ( ISNULL ( @v_last_job_fk, -999 )  AS VARCHAR(32) )
 --                    + ' - '
 --                    + CAST ( ISNULL ( @v_last_job_date, '01/01/2000' ) AS VARCHAR(64) )
 --   SET @v_data2   = @v_last_job_date
    
 --   EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
	--									   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

 --   EXEC common.job_execution_create   @v_job_fk, 'START DERIVE CEDNETCATALOG ',
	--	                                 @v_input_rows, @v_output_rows, @v_exception_rows,
	--									 200, @l_sysdate, @l_sysdate  
  
                

---------------------------------------------------------------------------------------
----SQL -----LOOK AT JIRA 3515
---------------------------------------------------------------------------------------


--select * from import.import_record_data
--where job_fk = 1607

--select * from import.IMPORT_MAP_METADATA where IMPORT_PACKAGE_FK = 802000
---------------------------------------------------------------------------------------
---put all counts up here by MFR
----------------------------------------------------------------------------

----number of products by MFR  --- all new and changed for this job, also total UPC
select  distinct  ein1.value MFR_CODE, '1-TOTAL CURRENT RECORDS' Description , ISNULL(Records,0) Records  
from epacube.ENTITY_IDENT_NONUNIQUE ein1
left join 
(
select ei.value  MFR_CODE ,'1-TOTAL CURRENT RECORDS' Description, count(1) Records
from epacube.product_association pa
inner join epacube.entity_ident_nonunique ei 
on (pa.entity_structure_fk = ei.entity_structure_fk
and Pa.data_name_fk = 159300)
and ei.DATA_NAME_FK =100143113
group by ei.value) tmp on ein1.value = tmp.MFR_CODE
where  ein1.DATA_NAME_FK = 100143113

UNION

select  distinct  ein1.value MFR_CODE, '2-CHANGE IN RECORDS COUNT SINCE LAST IMPORT' Description , ISNULL(Records,0) Records  
from epacube.ENTITY_IDENT_NONUNIQUE ein1
left join 
(
select  DATA_POSITION197 MFR_CODE ,'2-CHANGE IN RECORDS COUNT SINCE LAST IMPORT' Description, count(1) Records
from import.import_record_data
where job_fk = 1607
group by DATA_POSITION197) tmp on ein1.value = tmp.MFR_CODE
where  ein1.DATA_NAME_FK = 100143113

--UNION
--select ein.value ,'2-NEW PRODUCTS' Description,  count(1)
--from synchronizer.EVENT_DATA_history ed
--inner join epacube.DATA_NAME dn
--on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
--inner join epacube.PRODUCT_ASSOCIATION pa
--on (pa.PRODUCT_STRUCTURE_FK = ed.PRODUCT_STRUCTURE_FK
--and pa.DATA_NAME_FK = 159300)x
--inner join epacube.ENTITY_IDENT_NONUNIQUE ein
--on (ein.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK
--and ein.DATA_NAME_FK = 100143113)
--where ed.IMPORT_JOB_FK = 1607
--and EVENT_STATUS_CR_FK = 83
--and ed.DATA_NAME_FK = 149000
--group by ein.value

UNION
--UPC added
select  distinct  ein1.value MFR_CODE, '3-NUMBER OF NEW UPCS' Description , ISNULL(Records,0) Records  
from epacube.ENTITY_IDENT_NONUNIQUE ein1
left join 
(
select  ein.value MFR_CODE, '3-NUMBER OF NEW UPCS' Description,  count(1) Records
from synchronizer.EVENT_DATA_history ed
inner  join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
inner join epacube.PRODUCT_ASSOCIATION pa
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein
on (ein.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
where ed.IMPORT_JOB_FK = 1607
and ed.DATA_NAME_FK = 110102
and EVENT_ACTION_CR_FK = 52
group by ein.value) tmp on ein1.value = tmp.MFR_CODE
where  ein1.DATA_NAME_FK = 100143113

--UPC dropped Total number of UPCs that were inactive (Legacy_update_status = Q)
UNION
select  distinct  ein1.value MFR_CODE, '4-UPCS DROPPED' Description , ISNULL(Records,0) Records  
from epacube.ENTITY_IDENT_NONUNIQUE ein1
left join 
(
select ein.value MFR_CODE, '4-UPCS DROPPED' description,   count(1) Records
from synchronizer.EVENT_DATA_history ed
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
inner join epacube.PRODUCT_ASSOCIATION pa
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENTIFICATION ei 
on (pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
where ed.IMPORT_JOB_FK = 1607
and ed.DATA_NAME_FK =  820201
and ed.new_data = 'Q'
and EVENT_ACTION_CR_FK = 52
group by ein.value) tmp on ein1.value = tmp.MFR_CODE
where  ein1.DATA_NAME_FK = 100143113

UNION
--add those mfr code with 0 count
select  distinct  ein1.value MFR_CODE, '5-UPCS BLANK' Description , ISNULL(Records,0) Records  
from epacube.ENTITY_IDENT_NONUNIQUE ein1
left join 
(
select ein.value MFR_CODE, '5-UPCS BLANK' description,   count(1) Records
from synchronizer.EVENT_DATA ed
inner join synchronizer.event_data_errors ede
on (ede.event_fk = ed.event_id
and ede.error_name = 'PRODUCT COMPLETENESS::UPC' )
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
inner join epacube.PRODUCT_ASSOCIATION pa
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENTIFICATION ei 
on (pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
where ed.IMPORT_JOB_FK = 1607
and ed.data_name_fk = 159200
group by ein.value) tmp on ein1.value = tmp.MFR_CODE
where  ein1.DATA_NAME_FK = 100143113


UNION
--need 0 count records.
select  distinct  ein1.value MFR_CODE, '6-UPCS DUPLICATED' Description , ISNULL(Records,0) Records  
from epacube.ENTITY_IDENT_NONUNIQUE ein1
left join 
(select ein.value MFR_CODE, '6-UPCS DUPLICATED' description,   count(1) Records
from synchronizer.EVENT_DATA ed
inner join synchronizer.event_data_errors ede
on (ede.event_fk = ed.event_id
and ede.error_name = 'THIS UNIQUE IDENTIFIER ALREADY EXISTS.' )
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
inner join epacube.PRODUCT_ASSOCIATION pa
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENTIFICATION ei 
on (pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
where ed.IMPORT_JOB_FK = 1607
--and ed.data_name_fk =159300  ---association event
and ed.data_name_fk =110102  --upc event
group by ein.value) tmp on ein1.value = tmp.MFR_CODE
where  ein1.DATA_NAME_FK = 100143113

UNION

---MFR CODES CHANGE
select  distinct  ein1.value MFR_CODE, '7-MFR CODES CHANGED' Description , ISNULL(Records,0) Records  
from epacube.ENTITY_IDENT_NONUNIQUE ein1
left join 
(
select   ein.value MFR_CODE, '7-MFR CODES CHANGED' Description, count(1) Records
from synchronizer.EVENT_DATA_history ed
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
inner join epacube.PRODUCT_ASSOCIATION pa
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENTIFICATION ei 
on (pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
where ed.IMPORT_JOB_FK = 1607
and ed.DATA_NAME_FK in  ( 100143901, 100143902, 100143903)
and EVENT_ACTION_CR_FK = 52
group by ein.value) tmp on ein1.value = tmp.MFR_CODE
where  ein1.DATA_NAME_FK = 100143113

order by MFR_CODE, Description asc


------------------------------------------------------
---MFR's NOT INCLUDED IN THIS IMPORT
------------------------------------------------------
select distinct Value from epacube.ENTITY_IDENT_NONUNIQUE where DATA_NAME_FK = 100143113
and value not in (select Distinct DATA_POSITION197 from import.import_record_data
where job_fk = 1607)

------------------------------------
----NEW ITEMS FOR THIS IMPORT
------------------------------------

select distinct dn.DATA_NAME_ID, name, event_status_cr_fk, count(1)
from synchronizer.EVENT_DATA ed
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
where ed.IMPORT_JOB_FK = 1607
and EVENT_STATUS_CR_FK = 83
group by dn.DATA_NAME_ID, dn.name, event_status_cr_fk

union

select distinct dn.DATA_NAME_ID, dn.name, event_status_cr_fk, count(1)
from synchronizer.EVENT_DATA_history ed
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
where ed.IMPORT_JOB_FK = 1607
and EVENT_STATUS_CR_FK = 83
group by dn.DATA_NAME_ID, dn.name, event_status_cr_fk

-------------------------new items by mfr code

select distinct ein.value , count(1)
from synchronizer.EVENT_DATA_history ed
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
inner join epacube.PRODUCT_ASSOCIATION pa
on (pa.PRODUCT_STRUCTURE_FK = ed.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein
on (ein.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
where ed.IMPORT_JOB_FK = 1607
and EVENT_STATUS_CR_FK = 83
and ed.DATA_NAME_FK = 149000
group by ein.value


---------------------------------------------------------------
---CHANGE EVENTS THAT AUTO APPROVED
---------------------------------------------------------------

select distinct dn.DATA_NAME_ID, dn.name, event_status_cr_fk, count(1)
from synchronizer.EVENT_DATA_history ed
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
where ed.IMPORT_JOB_FK = 1607
and EVENT_STATUS_CR_FK = 81
group by dn.DATA_NAME_ID, dn.name, event_status_cr_fk

-----------------------------------------------------------------
---DEEPER LOOK AT UPC EVENTS
-----------------------------------------------------------------
--changed UPC's
------------------------------------------------------------
select  dn.name, count(1)
from synchronizer.EVENT_DATA_history ed
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
where ed.IMPORT_JOB_FK = 1607
and ed.DATA_NAME_FK = 110102
and EVENT_ACTION_CR_FK = 52
group by dn.name

---------------------------------------------------------------------
--- UPC CHANGE by MFR in this import
---------------------------------------------------------------------
select   ein.value, count(1)
from synchronizer.EVENT_DATA_history ed
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
inner join epacube.PRODUCT_ASSOCIATION pa
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENTIFICATION ei 
on (pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
where ed.IMPORT_JOB_FK = 1607
and ed.DATA_NAME_FK = 110102
and EVENT_ACTION_CR_FK = 52
group by ein.value

-----------------------------------------------------------------
--- UPC ADDED AS NEW 
------------------------------------------------------------------
select   ein.value, count(1)
from synchronizer.EVENT_DATA_history ed
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
inner join epacube.PRODUCT_ASSOCIATION pa
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENTIFICATION ei 
on (pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
where ed.IMPORT_JOB_FK = 1607
and ed.DATA_NAME_FK = 110102
and EVENT_ACTION_CR_FK = 51
group by ein.value

select  DATA_POSITION99, count(1) from import.import_record_data
where job_fk = 1607
and DATA_POSITION99 is NULL
group by DATA_POSITION99


----------------------------------------
-----MFR GROUPCODE CHANGES
----------------------------------------
select  dn.name, count(1)
from synchronizer.EVENT_DATA_history ed
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
where ed.IMPORT_JOB_FK = 1607
and ed.DATA_NAME_FK in ( 100143901, 100143902, 100143903)
and EVENT_ACTION_CR_FK = 52
group by dn.name

-------------------------------------------------------
--MFR with GROUP CODE CHANGES
-------------------------------------------------------

select   ein.value, count(1)
from synchronizer.EVENT_DATA_history ed
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
inner join epacube.PRODUCT_ASSOCIATION pa
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENTIFICATION ei 
on (pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
where ed.IMPORT_JOB_FK = 1607
and ed.DATA_NAME_FK in  ( 100143901, 100143902, 100143903)
and EVENT_ACTION_CR_FK = 52
group by ein.value
-------------------------------------------------------------------------------------------------------------

  SET @status_desc = 'finished execution of epacube.Paxson_Import_analysis'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @v_job_fk ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of epacube.Paxson_Import_analysis has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END