﻿-- Copyright 2019 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Get all product associations by the given type (URM-1187)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[PRODUCT_ASSOCIATION_GET_BY_TYPE]
	@product_association_type_id INT,
	@page INT = 1,
	@recs_per_page INT = 1000
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @first_rec INT;
	DECLARE @last_rec INT;
	DECLARE @total_rec INT;
	DECLARE @filter_name VARCHAR(64);
	DECLARE @filter_value VARCHAR(64);

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.PRODUCT_ASSOCIATION_GET_BY_TYPE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT @total_rec = COUNT(PRODUCT_ASSOCIATION_ID)
		FROM epacube.PRODUCT_ASSOCIATION
		WHERE DATA_NAME_FK = @product_association_type_id;
		
		SELECT @first_rec = COALESCE((@page - 1) * @recs_per_page, 0);
		SELECT @last_rec = COALESCE((@page * @recs_per_page + 1), 0);
		
		SELECT @total_rec AS TOTAL_ASSOCIATIONS
			,(
			SELECT TOP (@last_rec - 1)
				PRODUCT_STRUCTURE_ID
				, PRODUCT_ID
				, ASSOCIATION_ID
				, ASSOCIATION
				, ENTITY_STRUCTURE_ID
				, ORGANIZATION_ID
				, ORGANIZATION 
			FROM (
				SELECT ROW_NUMBER() OVER(ORDER BY PRODUCT_ASSOCIATION_ID ASC)	AS ROW_NUM
					, PA.PRODUCT_STRUCTURE_FK									AS PRODUCT_STRUCTURE_ID
					, PID.VALUE													AS PRODUCT_ID
					, PADN.DATA_NAME_ID											AS ASSOCIATION_ID
					, PADN.LABEL												AS ASSOCIATION
					, PA.ENTITY_STRUCTURE_FK									AS ENTITY_STRUCTURE_ID
					, PA.ORG_ENTITY_STRUCTURE_FK								AS ORGANIZATION_ID
					, OEI.VALUE													AS ORGANIZATION
				FROM epacube.PRODUCT_ASSOCIATION AS PA
					INNER JOIN (
						SELECT P.PRODUCT_STRUCTURE_FK, P.VALUE
						FROM epacube.PRODUCT_IDENTIFICATION P
							INNER JOIN epacube.DATA_NAME DN
								ON P.DATA_NAME_FK = DN.DATA_NAME_ID
									AND DN.NAME = 'PRODUCT_ID') PID
						ON PA.PRODUCT_STRUCTURE_FK = PID.PRODUCT_STRUCTURE_FK 
					INNER  JOIN epacube.DATA_NAME AS PADN 
						ON PA.DATA_NAME_FK = PADN.DATA_NAME_ID
					INNER JOIN epacube.ENTITY_IDENTIFICATION AS OEI 
						ON PA.ORG_ENTITY_STRUCTURE_FK = OEI.ENTITY_STRUCTURE_FK
				WHERE PA.DATA_NAME_FK = @product_association_type_id) src
			WHERE ROW_NUM > CAST(@first_rec AS VARCHAR(20)) AND ROW_NUM < CAST(@last_rec AS VARCHAR(20))
			FOR JSON PATH
			) [data]
		FOR JSON PATH;

		SET @ls_stmt = 'Finished execution of epacube.PRODUCT_ASSOCIATION_GET_BY_TYPE.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH
		SET @ls_stmt = 'Execution of epacube.PRODUCT_ASSOCIATION_GET_BY_TYPE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END 

