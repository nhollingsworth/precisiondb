﻿
CREATE PROCEDURE [epacube].[UI_Component_Renumber] @UI_Comp_Master_FK bigint

as 
--/***********************************************************
----	Written By  : Gary Stone
----	May, 2017
----*/----------------------------------------------------------

If object_id('tempdb..#UIC') is not null
drop table #UIC

Select * into #UIC from epacube.ui_comp where ui_comp_master_fk = @UI_Comp_Master_FK
Delete from epacube.ui_comp where ui_comp_master_fk = @UI_Comp_Master_FK

Insert into epacube.ui_comp 
(UI_COMP_MASTER_FK, DATA_NAME_FK, COLUMN_NO, SEQ, RESULTS_ORDER_SEQ, EDITABLE, RECORD_STATUS_CR_FK, GROUP_BY_COL_NAME)
Select 
UI_COMP_MASTER_FK, DATA_NAME_FK, COLUMN_NO, SEQ, RESULTS_ORDER_SEQ, EDITABLE, RECORD_STATUS_CR_FK, GROUP_BY_COL_NAME
from #UIC order by seq
