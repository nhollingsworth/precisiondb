﻿--USE [epacube]
--GO
--/****** Object:  StoredProcedure [epacube].[UI_Filter_Builder]    Script Date: 2/22/2017 10:13:00 AM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE procedure [epacube].[UI_FILTER_BUILDER] @UserID varchar(96), @Data_Name_FK varchar(Max), @Criteria varchar(Max) = Null, @FilterResultsExec int = 0, @FilterResultsPrint int = 1, @QueryExec int = 0, @QueryScript Varchar(Max) OUTPUT, @View_ID int = Null, @Component_ID int = Null

as 

/*
--/************************************************************************************************************************************************
--  Written By  : Gary Stone
	February, 2017
Parameters
@FilterResultsExec - 1 will query sql code for the filter data name requested; 2 will execute that query
@FilterResultsPrint - 1 will generate the query to use with the filter data name requested, but will not display any results
@QueryExec - 0 will do nothing; 1 will display the code used for each of the Prod, Cust, and Vendor tables to be generated; 2 will run the query

The appropriate Filter, Prod, Cust, and Vendor tables will be generated regardless of the options chosen above.

! = NOT
~ = group separator - replaces ^
| = OR
= = Criteria Value
` = Back-tick separates options setting from date when date criteria is specified

--*/-----------------------------------------------------------------------------------------------------------------------------------------------
*/
set nocount on
--Declare @UserID Varchar(96) = 'gstone@epacube.com', @Data_Name_FK varchar(Max) = 'all', @Criteria varchar(Max) = '110100=800128w~151110=30130~540000=`2019-12-23~540002=no~', @FilterResultsExec int = 1, @FilterResultsPrint int = 0, @QueryExec int = 0, @QueryScript Varchar(Max)

--'gstone@epacube.com','all','110100=800128w~151110=30130~540000=`2019-12-23~540002=no~',1,0,0,null,4497,null

--exec epacube.ui_filter_builder 'gstone@epacube.com','all','110100=61839,8145,8942~151110=30001,30003,30011,30130,30597,30600,40001,40004,70001,80001~540000=`2019-07-19~540001=yes~540002=yes~540004=yes~',1,0,0

	--If Object_ID('dbo.data_test') is not null
	--drop table dbo.data_test

	--Select @UserID '@UserID', @Data_Name_FK '@Data_Name_FK', @Criteria '@Criteria' into dbo.data_test

Declare @EmptyCriteria int = 0
Set Ansi_Warnings Off

If @Criteria = '~' and Isnumeric(Replace(@Data_Name_FK, '~', '')) = 1
Begin
	Set @EmptyCriteria = 1
	Set @Criteria = Replace(@Data_Name_FK, '~', '') + '=~'
End

Set @UserID = Replace(@UserID, '.', '_')

If right(@Data_Name_FK, 1) <> '~' and @Data_Name_FK not like 'ALL%'
Set @Data_Name_FK = @Data_Name_FK + '~'

If @Data_Name_FK = 'All~'
Set @Data_Name_FK = 'All'

IF right(@Criteria, 1) <> '~'
Set @Criteria = @Criteria + '~'

If object_id('tempdb..#Tbl') is not null
drop table #Tbl

Create table #Tbl([SQL] varchar(Max) Null, [Where] Varchar(Max) Null)

If object_id('tempdb..#Param3') is not null
drop table #Param3

Declare @POS int
Declare @StartPOS int
Declare @FilterToken varchar(192)
Declare @CritDN varchar(Max)
Declare @CritValue Varchar(Max)
Declare @OpExclusion varchar(64)
Declare @TmpTbl varchar(64)
Declare @FilterCode varchar(Max)
Declare @Entity_Class_CR_FK varchar(64)
Declare @SQL varchar(Max)
Declare @WHERE varchar(Max)
Declare @DN_Type varchar(32)
Declare @Structure_FK varchar(64)
Declare @Data_Column Varchar(64)
Declare @Primary_Structure_FK Varchar(64)
Declare @AuthSQL Varchar(Max)
Declare @DN_Label Varchar(64)
Declare @KeyCols Varchar(Max)
Declare @SqlEXEC Varchar(Max)
Declare @From VARCHAR(MAX), @Qtbl Varchar(128), @DN_FK Varchar(16), @Alias Varchar(16), @Select Varchar(Max), @Primary_Column varchar(64), @Local_Column varchar(64), @Alias_DV varchar(16), @Operator Varchar(8), @OpExcl Varchar(64), @R_EC_CR_FK Varchar(16)
Declare @VendorJoin Varchar(Max), @VendorCols Varchar(Max)
Declare @Lookup_Values int
Declare @BT_Status int
Declare @ST_Status int
Declare @Op varchar(16) 
Declare @DN_String Varchar(Max) = ''
Declare @DN_POS int = 1
Declare @DN_StartPOS int = 1
Declare @DN_Length int = 1
Declare @Auths_Ind int = 1
Declare @Auths_Join varchar(64) = ''
Declare @SQLcodeStart Varchar(Max) = ''
Declare @SQLcodeIn Varchar(Max) = ''
Declare @SQLcodeNotIn Varchar(Max) = ''
Declare @DNtmp varchar(16) = ''
Declare @FILTER_ENTITY_CLASS_CR_FK varchar(64) = ''
Declare @COLUMN_ENTITY_CLASS_CR_FK varchar(64) = ''
Declare @Product_Structure_Column varchar(64) = ''
Declare @VirtualJoin varchar(64) = ''
Declare @ImmedAlias varchar(64) = ''
Declare @Ref_DN_FK varchar(64) = ''
Declare @Customer_Structure_Column varchar(64) = ''
Declare @Vendor_Structure_Column varchar(64) = ''
Declare @Pass int = 1
Declare @Cust_Structure_FK_Init varchar(64) = ''
Declare @Item_Data_Name_FK varchar(64) = ''
Declare @PA_Alias varchar(64) = ''
Declare @All_Filters varchar(Max) = ''
Declare @CurrentFilterDN_FK varchar(16) = ''
Declare @Max_Pass varchar(8) = ''
Declare @Prod_Alias varchar(64) = ''
Declare @Where_Column Varchar(256)
Declare @Data_Level_CR_FK int
Declare @CRcnt int = 1, @CodeRefPosStart int = 0, @CodeRefPosEnd int = 0, @CodeRef Varchar(Max) = '', @CodeRefStringP3 Varchar(Max) = '', @CodeRefValueP3 Varchar(Max)
Declare @WhereDataLevel_CR_FK Varchar(64) = ''
Declare @VendorTypeFilter Varchar(64) = ''
Declare @VendorItemTypeFilter Varchar(64) = Null
Declare @DV_OrderBy varchar(128) = ''
Declare @OrderBy varchar(max) = ''
Declare @V_Ref_Data_Name_FK varchar(64) = Null

Set @Item_Data_Name_FK = (Select value from epacube.epacube_params with (nolock) where application_scope_fk = 10109 and name = 'search2')
Set @Prod_Alias = 'pi_' + @Item_Data_Name_FK
	Set @QueryScript = ''

	If @Data_Name_FK = 'All'
	Begin
		DECLARE AllFilters Cursor local for
			Select Data_Name_fk from epacube.ui_filter with (nolock) where options is null and isnull(data_name_fk, 0) <> 0 and record_status_cr_fk = 1
			and data_name_fk in (select data_name_fk from epacube.epacube.DATA_NAME_GROUP_REFS with (nolock) where V_Ref_Data_Name_FK is null)

			OPEN AllFilters;
			FETCH NEXT FROM AllFilters INTO @CurrentFilterDN_FK
			WHILE @@FETCH_STATUS = 0

				Begin--Fetch

				Set @All_Filters = @All_Filters + @CurrentFilterDN_FK + '~'

			FETCH NEXT FROM AllFilters INTO @CurrentFilterDN_FK
				End--Fetch
			Close AllFilters;
			Deallocate AllFilters;

			Set @Data_Name_FK = @All_Filters
		End

	--Select @CodeRefPosStart, @CodeRefPosend, @Criteria, @CodeRef

	If @Data_Name_FK like '%~%'
	Begin
		Set @DN_String = @Data_Name_FK
		Set @Data_Name_FK = '0'
	End

	Set @Data_Name_FK = Case when isnumeric(@Data_Name_FK) = 1 then @Data_Name_FK else (Select data_name_id from epacube.data_name with (nolock) where name = '' + @Data_Name_FK + '') end
	Set @Lookup_Values = isnull((Select top 1 Lookup_Values from epacube.ui_filter with (nolock) where data_name_fk = @Data_Name_FK), 0)

	--If isnull(@Criteria, '') = ''
	--	GoTo Last_Step

	Create Table #Param3(CritDN varchar(Max) Not Null, CritValue varchar(max) Null, OpExclusion varchar(64)) 

	While @CRcnt <= (Select count(*) from epacube.ui_filter where Record_Status_CR_FK = 1 and code_type is not null)
	Begin
		If @Criteria like '%~cr_%'
		Begin
			Set @CodeRefPosStart = charindex('~cr_', @Criteria)
			Set @CodeRefPosEnd = Charindex('~', @Criteria, @CodeRefPosStart + 1)-- - 1
			Set @CodeRef = Substring(@Criteria, @CodeRefPosStart + 1, @CodeRefPosEnd - @CodeRefPosStart)
			Set @CodeRefStringP3 = Left(@CodeRef, CHARINDEX('=', @CodeRef) - 1)
			Set @CodeRefValueP3 = Replace(Reverse(Left(Reverse(@CodeRef), CHARINDEX('=', Reverse(@CodeRef)) - 1)), '~', '')
			Set @Criteria = Replace(@Criteria, @CodeRef, '')
			
			Insert into #Param3(CritDN, CritValue)
			Select @CodeRefStringP3, @CodeRefValueP3

		End
			Set @CRcnt = @CRcnt + 1
	End

	Set @StartPOS = 1

	--If isnull(@UserID, '0') = '0'
	--Set @UserID = Cast(@@SPID as varchar(8))

	--If object_id('TempTbls.tmp_PROD_' + @UserID) is not null
	--	Exec('Drop Table TempTbls.tmp_PROD_' + @UserID)

	--If object_id('TempTbls.tmp_CUST_' + @UserID) is Not null
	--	Exec('Drop Table TempTbls.tmp_CUST_' + @UserID)

	--If object_id('TempTbls.tmp_VENDOR_' + @UserID) is Not null
	--	Exec('Drop Table TempTbls.tmp_VENDOR_' + @UserID)

	--If object_id('TempTbls.tmp_FILTER_' + @UserID) is not null
	--	Exec('Drop Table TempTbls.tmp_FILTER_' + @UserID)

	--If object_id('TempTbls.tmp_ZONE_' + @UserID) is not null
	--	Exec('Drop Table TempTbls.tmp_ZONE_' + @UserID)


	Declare @tbl varchar(128)
	
	DECLARE Tmps Cursor local for
			Select table_name from INFORMATION_SCHEMA.tables where TABLE_SCHEMA = 'temptbls' and table_name like '%_' + @UserID

			OPEN Tmps;
			FETCH NEXT FROM Tmps INTO @tbl
			WHILE @@FETCH_STATUS = 0

				Begin--Fetch

				exec('drop table epacube.temptbls.[' + @tbl + ']')

			FETCH NEXT FROM Tmps INTO @tbl
				End--Fetch
			Close Tmps;
			Deallocate Tmps;





	Exec('Create Table TempTbls.tmp_FILTER_' + @UserID + '(CritDN varchar(Max) Not Null, CritValue varchar(Max) Null, OpExclusion varchar(64))')

	If isnull(@Criteria, '') = ''
	Set @Criteria = @Data_Name_FK + '=' + '' + '~'

	If @QueryExec <> 0
	Begin
		Set @FilterResultsPrint = 0
		Set @FilterResultsExec = 0
	End
		
	Set @VendorJoin = ''

	While charindex('~', @Criteria, @StartPOS) <> 0 
		Begin

			Set @POS = charindex('~', @Criteria, @StartPOS)
			Set @FilterToken = substring(@Criteria, @StartPOS, @POS - @StartPOS)


			Set @StartPOS = @POS + 1		

			Set @CritDN = left(@FilterToken,charindex('=', @FilterToken) - 1)
			Set @CritDN = Case when isnumeric(@CritDN) = 1 then @CritDN else (Select data_name_id from epacube.data_name with (nolock) where name = '' + @CritDN + '') end
			Set @CritValue = reverse(left(reverse(@FilterToken),charindex('=', reverse(@FilterToken)) - 1))

			If @CritDN Not In (Select data_name_fk from epacube.ui_filter with (nolock)) --or @CritDN = 159905
			goto NextVal

			If @CritValue like '%`%' and epacube.getdatatypefordn(@CritDN) <> 'date'
				Begin
					Set @OpExclusion = '!'
					Set @CritValue = Right(@CritValue, Len(@CritValue)-charindex('`', @CritValue))
				end
				Else
				Begin
					Set @OpExclusion = ''
				End

			Set @CritValue = Case when @CritValue = '' then Null else @CritValue end
			Set @OpExclusion = Case when @OpExclusion = '' then Null else @OpExclusion end
			

	Declare @ItmNmbr varchar(64)
	Declare @NewCritDv varchar(128) = ''

	If @CritDN = 110100
	Begin
	DECLARE Itm Cursor local for
			select ss.ListItem
			from common.utilities.SplitString ( @CritValue, ',' ) ss	 

			OPEN Itm;
			FETCH NEXT FROM Itm INTO @ItmNmbr
			WHILE @@FETCH_STATUS = 0

				Begin--Fetch

				Set @NewCritDV = @NewCritDV + Case when isnumeric(@ItmNmbr) = 1 then @ItmNmbr + 'w,' else @ItmNmbr + ',' end

			FETCH NEXT FROM Itm INTO @ItmNmbr
			Set @CritValue = left(@NewCritDV, len(@NewCritDV) - 1)
				End--Fetch
			Close Itm;
			Deallocate Itm;

	End

			Insert into #Param3
			Select @CritDN, case when @critDN = 110100 and isnumeric(@CritValue) = 1 then @CritValue + 'w' else @CritValue end, @OpExclusion

			If @Criteria not like '%' + @Data_Name_FK + '%'
			Insert into #Param3
			Select @Data_Name_FK, Null, Null
			where @Data_Name_FK not in (Select CritDN from #Param3)

NextVal:
	End

	Update P3
	Set OpExclusion =	Case	when epacube.getdatatypefordn(critdn) = 'date' then
									Case	When left(critvalue, 1) = '`' then Null
											when critvalue like '%`%' then left(CritValue, charindex('`', CritValue)-1)
											else Null
									end
								when critvalue like '%`%' then left(CritValue, charindex('`', CritValue)-1)
								when uf.Options is not null and uf.Options <> 'yes:~no~all' and critvalue not like '%`%' then critvalue
						else OpExclusion end
	, CritValue =		Case	when epacube.getdatatypefordn(critdn) = 'date' then
									Case	when isdate(replace(critvalue, '`', '')) = 1 then replace(critvalue, '`', '')
											when critvalue like '%`%' and left(CritValue, charindex('`', CritValue)-1) = 'all' then null
											when critvalue = 'ignore' then null --like '%`%' and left(CritValue, charindex('`', CritValue)-1) = 'all' then null
											when critvalue like '%`%'  and reverse(left(Reverse(CritValue), charindex('`', reverse(CritValue)) - 1)) <> '' then reverse(left(Reverse(CritValue), charindex('`', reverse(CritValue)) - 1))
											else Null 
									end
								when critvalue like '%`%' then Null
						else CritValue end
	from #Param3 P3
	inner join epacube.ui_filter UF with (nolock) on P3.critdn = UF.Data_Name_FK
	where UF.options is not null
	and isnumeric(critdn) = 1

	Update P3
	Set CritValue =	right('00000' + cast(CritValue as varchar(8)), 5)
	from #Param3 P3
	where critdn in (550011, 550012)

	--If (Select count(*) from #Param3 where critdn in (550011, 550012)) > 0 and (Select count(*) from #Param3 where critdn = 550010) = 0
	--insert into #Param3(CritDN, CritValue)
	--Select 550010, 0

	Exec('Insert into TempTbls.tmp_FILTER_' + @UserID + ' Select * from #Param3')

------********

	--If (Select count(*) from #Param3 where isnull(CritValue, '') <> '') = 0
	--GoTo Last_Step

	Set @BT_Status = Case when (Select count(*) from #Param3 where critdn = '144010') > 0 then 1 else 0 end
	Set @ST_Status = Case when (Select count(*) from #Param3 where critdn = '144020') > 0 then 1 else 0 end

	Declare @NullEntityClassCR_FK bigint = 10109


	DECLARE Entity_Class Cursor local for
	Select isnull(r.ENTITY_CLASS_CR_FK, @NullEntityClassCR_FK) 'ENTITY_CLASS_CR_FK'
	from EPACUBE.DATA_NAME_GROUP_REFS R with (nolock)
	inner join epacube.epacube.ui_filter uif with (nolock) on r.data_name_fk = uif.Data_Name_FK
	inner join #Param3 P3 on uif.data_name_fk = P3.CritDN
	where 1 = 1
		and isnumeric(P3.CritDN) = 1
		and P3.CritDN <> 159450 ---replaced 159905 on 7/16/2020 with consolidation of authorized fields to 1.
		and r.table_name <> 'SEGMENTS_CONTACTS'
	group by isnull(r.ENTITY_CLASS_CR_FK, @NullEntityClassCR_FK)
	Order by Case when isnull(r.ENTITY_CLASS_CR_FK, @NullEntityClassCR_FK) = 10109 then 2 else 1 end

		OPEN Entity_Class;
		FETCH NEXT FROM Entity_Class INTO @Entity_Class_CR_FK
		WHILE @@FETCH_STATUS = 0

		Begin
		
		Set @Select = 'Select '
		Set @Where = 'Where 1 = 1 '
		Set @From = ''
		--Set @KeyCols = ''

		Set @From = Case 
			when @Entity_Class_CR_FK = 10109 then ' 
		From epacube.epacube.product_structure PS with (nolock) 
		Inner join epacube.product_identification ' + @Prod_Alias + ' with (nolock) on ps.product_structure_id = ' + @Prod_Alias + '.product_structure_fk and ' + @Prod_Alias + '.data_name_fk = ' + @Item_Data_Name_FK
			When @Entity_Class_CR_FK = 10103 then ' 
		From epacube.epacube.entity_structure ESV with (nolock)
		Inner Join epacube.entity_identification eiv with (nolock) on esv.entity_structure_id = eiv.entity_structure_fk and esv.data_name_fk = eiv.entity_data_name_fk and esv.data_name_fk in (143000) '
	
			When @Entity_Class_CR_FK = 10117 then ' 
		From epacube.epacube.entity_structure ESZ with (nolock) 
		Inner Join epacube.entity_identification eiz with (nolock) on esz.entity_structure_id = eiz.entity_structure_fk and esz.data_name_fk = eiz.entity_data_name_fk and esz.data_name_fk in (151000) 
		Inner join epacube.epacube.entity_ident_nonunique einz with (nolock) on eiz.entity_structure_fk = einz.entity_structure_fk and eiz.entity_data_name_fk = einz.entity_data_name_fk and einz.data_name_fk = 140200 '

			When @Entity_Class_CR_FK = 10104 and @BT_Status = 1 and @ST_Status = 1 then '
		From epacube.epacube.entity_structure esc_p with (Nolock)
		Inner join epacube.epacube.entity_identification eic_p with (nolock) on esc_p.entity_structure_id = eic_p.entity_structure_fk and esc_p.data_name_fk = eic_p.entity_data_name_fk and esc_p.data_name_fk = 144010
		inner join epacube.epacube.entity_structure esc_c with (nolock) on eic_p.entity_structure_fk = esc_c.parent_entity_structure_fk and esc_c.data_name_fk = 144020
		inner join epacube.epacube.entity_identification eic with (nolock) on esc_c.entity_structure_id = eic.entity_structure_fk '

			When @Entity_Class_CR_FK = 10104 and @BT_Status = 1 then '
		From epacube.epacube.entity_structure esc_p with (Nolock)
		Inner join epacube.epacube.entity_identification eic with (nolock) on esc_p.entity_structure_id = eic.entity_structure_fk and esc_p.data_name_fk = eic.entity_data_name_fk and esc_p.data_name_fk = 144010 '
			else ' 
		From epacube.epacube.entity_structure ESC with (nolock)
		Inner Join epacube.entity_identification eic with (nolock) on esc.entity_structure_id = eic.entity_structure_fk and esc.data_name_fk = eic.entity_data_name_fk and esc.data_name_fk in (144020) ' end

		Set @TmpTbl = 'TempTbls.tmp_'	+ Case @Entity_Class_CR_FK when 10103 then 'VENDOR_' when 10104 then 'CUST_' when 10109 then 'PROD_' when 10117 then 'ZONE_' end + @UserID
		Set @Structure_FK = Case @Entity_Class_CR_FK when 10103 then 'VENDOR_ENTITY_STRUCTURE_FK' when 10104 then 'CUST_ENTITY_STRUCTURE_FK' when 10109 then 'PRODUCT_STRUCTURE_FK' when 10117 then 'ZONE_ENTITY_STRUCTURE_FK' end

		If ((Select count(*) from EPACUBE.DATA_NAME_GROUP_REFS R with (nolock) inner join epacube.epacube.ui_filter uif with (nolock) on r.data_name_fk = uif.Data_Name_FK inner join #Param3 P3 on uif.data_name_fk = P3.CritDN where isnumeric(P3.CritDN) = 1 and r.table_name <> 'SEGMENTS_CONTACTS' and r.entity_class_cr_fk = 10103) > 0 And
			((Select count(*) from EPACUBE.DATA_NAME_GROUP_REFS R with (nolock) inner join epacube.epacube.ui_filter uif with (nolock) on r.data_name_fk = uif.Data_Name_FK inner join #Param3 P3 on uif.data_name_fk = P3.CritDN where isnumeric(P3.CritDN) = 1 and r.table_name <> 'SEGMENTS_CONTACTS' and r.entity_class_cr_fk = 10104) > 0 OR
			(Select count(*) from EPACUBE.DATA_NAME_GROUP_REFS R with (nolock) inner join epacube.epacube.ui_filter uif with (nolock) on r.data_name_fk = uif.Data_Name_FK inner join #Param3 P3 on uif.data_name_fk = P3.CritDN where isnumeric(P3.CritDN) = 1 and r.table_name <> 'SEGMENTS_CONTACTS' and r.entity_class_cr_fk = 10109) > 0)) And
			@VendorJoin = ''
		Begin
			Set @VendorJoin = ' 
		inner join epacube.epacube.segments_vendor sv with (nolock) on t_vendor.vendor_entity_structure_fk = sv.VENDOR_ENTITY_STRUCTURE_FK and sv.data_level_cr_fk = 518 '
		end
		If @Entity_Class_CR_FK = 10104
			Set @VendorJoin = @VendorJoin + ' 
		inner join TempTbls.tmp_CUST_' + @UserID + ' t_cust with (nolock) on sv.cust_entity_structure_fk = t_cust.cust_entity_structure_fk '
		If @Entity_Class_CR_FK = 10109
			Set @VendorJoin = @VendorJoin + ' 
		inner join TempTbls.tmp_PROD_' + @UserID + ' t_prod with (nolock) on sv.product_structure_fk = t_prod.product_structure_fk '

		If @Entity_Class_CR_FK = 10109 and object_id('Temptbls.tmp_ZONE_' + @UserID) is not null
			Set @From = @From + '
		' + case when (Select OpExclusion from #Param3 where critdn = 151110) = '!' or @CritValue = 'UNASSIGNED' then 'left' else 'inner' end + ' join epacube.product_association pa_159450 with (nolock) on ps.product_structure_id = pa_159450.PRODUCT_STRUCTURE_FK and pa_159450.data_name_fk = 159450
			and pa_159450.entity_structure_fk ' + case when (Select OpExclusion from #Param3 where critdn = 151110) = '!' or @CritValue = 'UNASSIGNED' then 'not in' else 'in' end + ' (select zone_entity_structure_fk from temptbls.tmp_zone_' + @UserID + ' t_zone with (nolock))'

Set @KeyCols = ''
Declare @KeyColFKS varchar(Max)
DECLARE Tbl Cursor local for
Select distinct
r.Qtbl, r.data_name_fk, r.Alias, R.Primary_Column
, Case when r.V_Ref_Data_Name_FK is null then R.DATA_COLUMN else Replace(R.DATA_COLUMN, R.Alias + '.', '') end 'Local_Column'
, R.Alias_DV, replace(R.DN_Label, ' #', '') 'DN_Label', p3.CritValue, r.Entity_Class_CR_FK
, Case when charindex('%', P3.CritValue) <> 0 then ' like ' when CritDN = '151110' then '=' when isnull(P3.OpExclusion, '') <> '' then ' <> ' else ' = ' end Operator
, Case when isnull(P3.OpExclusion, '') <> '' and charindex('%', P3.CritValue) <> 0 then ' Not ' else '' end OpExcl
, Case when R.entity_class_cr_fk = 10109 then isnull(R.[Product_Structure_Column], 'Product_Structure_FK') else 'Entity_Structure_FK' end 'Primary_Structure_Column'
, Case when r.V_Ref_Data_Name_FK is null then R.DATA_COLUMN else Replace(R.DATA_COLUMN, R.Alias + '.', '') end 'DATA_COLUMN'
, r.V_Ref_Data_Name_FK
from EPACUBE.DATA_NAME_GROUP_REFS  R with (nolock)
			inner join epacube.epacube.ui_filter uif with (nolock) on r.data_name_fk = uif.Data_Name_FK
			inner join #Param3 P3 on uif.data_name_fk = P3.CritDN
			where	1 = 1
					and isnumeric(isnull(P3.CritDN, 0)) = 1 
					and r.entity_class_cr_fk in (@Entity_Class_CR_FK)
					and (r.V_Ref_Data_Name_FK is null or r.V_Ref_Data_Name_FK = 500030)

				OPEN Tbl;
				FETCH NEXT FROM Tbl INTO @Qtbl, @DN_FK, @Alias, @Primary_Column, @Local_Column, @Alias_DV, @DN_Label, @CritValue, @R_EC_CR_FK, @Operator, @OpExcl, @Primary_Structure_FK, @Data_Column, @V_Ref_Data_Name_FK--, @ind_VirtualColumnCheck, @VirtualAction
				WHILE @@FETCH_STATUS = 0

				Begin

				Set @Alias = Case When @Entity_Class_CR_FK = 10104 and @BT_Status = 1 and  @ST_Status = 0 then Replace(@Alias, 'EIC_P', 'EIC') When @Entity_Class_CR_FK = 10117 then 'EIZ' else @Alias end
--Select @Alias
					Set @KeyCols = Case 
					When @KeyCols not like '%' + @Primary_Structure_FK + '%' then Case @Entity_Class_CR_FK when 10104 then 'EIC' when 10103 then 'EIV' when 10117 then 'EIZ' else @Prod_Alias end + '.[' + @Primary_Structure_FK + '] ''' + @Structure_FK + ''',  ' 
					else @KeyCols + '' end

--select @Primary_Structure_FK '@Primary_Structure_FK', @Structure_FK '@Structure_FK', @KeyCols '@KeyCols'

--select @KeyCols '@KeyCols', @Primary_Structure_FK '@Primary_Structure_FK', @Structure_FK '@Structure_FK', @KeyColFKS '@KeyColFKS'

				If @Primary_Structure_FK in ('Product_Structure_FK', 'Cust_Entity_Structure_fk', 'Vendor_Entity_Structure_FK', 'Entity_Structure_FK') and ((@R_EC_CR_FK = 10124 and @Entity_Class_CR_FK in (10103, 10104)) or @R_EC_CR_FK <> 10124) and @DN_FK not in (144010, 144020)
					and @From not like '%' + @Alias + '%'
					Begin
						Set @From = @From + '
		inner join ' + @Qtbl + ' ' + @Alias + ' on ' + Case @Entity_Class_CR_FK when 10109 then '' + @Prod_Alias + '.[' when 10104 then 'EIC.['  when 10103 then 'EIV.['  when 10117 then 'EIZ.[' end + @Primary_Structure_FK  + '] = ' + @Alias + '.[' 
							+ case when @Qtbl like '%Segments%' then @Structure_FK else @Primary_Structure_FK end + '] and ' + @Alias + '.Data_Name_FK = ' + isnull(@V_Ref_Data_Name_FK, @DN_FK) + '
			'				+	Case when isnull(@V_Ref_Data_Name_FK, @DN_FK) = 500030 then ' and ' + @Alias + '.GLOBAL_PACK_CODE = ''E'''  else '' end
--			'				+	Case when isnull(@V_Ref_Data_Name_FK, @DN_FK) = 500030 then ' and ' + @Alias + '.GLOBAL_PACK_CODE = ''E'' and ' + @Alias + '.PRECEDENCE = 1' else '' end

--Select @from

					End

				If @Local_Column not in ('Data_Value_FK', 'Entity_Data_Value_FK', 'Record_status_cr_fk') and @Local_Column not like '%Structure%' and ((@R_EC_CR_FK = 10124 and @Entity_Class_CR_FK in (10103, 10104)) or @R_EC_CR_FK <> 10124)
					Begin

					Set @Select = @Select + @Alias + '.' + @Local_Column + ' ''' + @DN_Label + ''', '
					--Set @Where = @Where + Case when isnull(@CritValue, '') <> '' then ' and ' + @Alias + '.[' + @Local_Column + '] ' + @OpExcl + ' ' + @Operator + '''' + @CritValue + ''''
					Set @Where = @Where + Case	when isnull(@CritValue, '') like '%,%' then ' and ' + @Alias + '.[' + @Local_Column + '] ' + @OpExcl + ' in (' + '''' + Replace(@CritValue, ',', ''', ''') + '''' + ')'
												when isnull(@CritValue, '') <> '' then ' and ' + @Alias + '.[' + @Local_Column + '] ' + @OpExcl + ' ' + @Operator + '''' + @CritValue + ''''
						else '' end 
--Select @Select
					End

				if @Primary_Column in ('data_value_fk', 'entity_data_value_fk') and ((@R_EC_CR_FK = 10124 and @Entity_Class_CR_FK in (10103, 10104)) or @R_EC_CR_FK <> 10124)
					Begin
						Set @From = @From + '
		Inner join ' + Case @Primary_Column when 'Data_Value_FK' then 'epacube.data_value' else 'epacube.entity_data_value' end + ' ' + @Alias_DV + ' on ' + @Alias + '.[' + @Local_Column + '] = ' + @Alias_DV + '.[' + Replace(@Primary_Column, '_FK', '_ID') + '] '
						Set @Select = @Select + @Alias_DV + '.[value] ''' + @DN_Label + ''', ' + @Alias_DV + '.[Description] ''' + @DN_Label + '_Description'', '
						Set @Where = Case when isnull(@CritValue, '') <> '' then @Where + ' and ' + @Alias_DV + '.[Value] ' + @OpExcl + ' ' + @Operator +  '''' + @CritValue + '''' else @Where end
						Set @KeyCols = @KeyCols + Case When @KeyCols not like '%' + @Local_Column then @Alias + '.[' + @Local_Column + '] ''' + @Alias_DV + '_' + @Local_Column + ''',  ' else '' end

--select @Prod_Alias '@Prod_Alias', @Select '@Select', @Alias '@Alias', @Local_Column '@Local_Column', @DN_Label '@DN_Label', @Alias_DV '@Alias_DV'
--Select @Select
					End
					
--Select @Select
				If @Qtbl like '%SEGMENTS_CONTACTS%' and @R_EC_CR_FK = 10124 and @From like '%' + @Alias + '%' and  @Entity_Class_CR_FK in (10103, 10104)
					
					Set @From = @From + ' and ' + @Alias + '.entity_class_cr_fk = ' + @Entity_Class_CR_FK

				FETCH NEXT FROM Tbl INTO @Qtbl, @DN_FK, @Alias, @Primary_Column, @Local_Column, @Alias_DV, @DN_Label, @CritValue, @R_EC_CR_FK, @Operator, @OpExcl, @Primary_Structure_FK, @Data_Column, @V_Ref_Data_Name_FK--, @ind_VirtualColumnCheck, @VirtualAction

				If @from like '%' + @Alias + '.[' + @Primary_Column + ']' + '%' and @Primary_Column like '%Structure%'--and @Alias not in ('esc_p', 'esc_c', 'esc', 'eic_p', 'eic_c', 'eic') --and @Select not like '%' + @Alias + '.[' + @Local_Column + ']%'
				Set @Alias = @Alias + '_1'
				
			End
			
			Close Tbl;
			Deallocate Tbl;


			If @Entity_Class_CR_FK = 10109 and (Select count(*) from #Param3 where critdn = 151110 and (OpExclusion = '!' OR CritValue = 'UNASSIGNED')) <> 0
					Set @Where = @Where + ' and pa_159450.product_association_id is null'
			else
			If @Entity_Class_CR_FK = 10109 and (Select count(*) from #Param3 where critdn = 151110) <> 0
					Set @Where = @Where + ' and pa_159450.product_association_id is not null'

--Select @Select, @From, @QueryExec, @Alias, @Prod_Alias

	If Len(@Select) >= 7
		Set @Select = Left(@Select, Len(rtrim(@Select)) - 1)

		Set @Select = Right(@Select, Len(@Select) - 6)
		Set @Select = 'Select ' + Case when @DN_FK = 151110 then '' else 'distinct ' end + @KeyCols + @Select

		Set @SqlEXEC = Replace(Replace((@Select + @From + ' ' + @Where), '  ', ' '), '  ', ' ')

		Set @OrderBy = case when @DN_FK = 151110 then ' order by Cast(ascii(left(einz.value, 1)) as varchar(16)) + right(''000'' + ltrim(rtrim(reverse(left(Reverse(einz.value), charindex('' '', Reverse(einz.value)) - 1)))), 3) + ''0'' Desc' else '' end

		If @QueryExec = 1
			Print(@SqlEXEC + @OrderBy)
		Else
		If @QueryExec = 2
			Exec(@SqlEXEC + @OrderBy )

		Set @SqlEXEC = 'Select ' + @Structure_FK + '
		from (' + @SqlEXEC + ') A'


--Select @Entity_Class_CR_FK '@Entity_Class_CR_FK', @Select '@Select', @EmptyCriteria '@EmptyCriteria', @TmpTbl '@TmpTbl'

	If (Select count(*) from #Param3 P3 inner join EPACUBE.DATA_NAME_GROUP_REFS R with (nolock) on p3.CritDN = R.data_name_fk where isnumeric(P3.CritDN) = 1 and r.entity_class_cr_fk = @Entity_Class_CR_FK) > 0 -- and isnull(p3.CritValue, '') not in ('', '0')) > 0
	and @Select <> 'Select ' and @EmptyCriteria <> 1

	Begin
		Exec('If Object_ID(''' + @TmpTbl + ''') is null
		Begin	
		Create Table ' + @TmpTbl + '(' + @Structure_FK + ' bigint not null);
		Create index idx_st on ' + @TmpTbl + '(' + @Structure_FK + ')
		End')

		Set @SqlEXEC = 'Insert into ' + @TmpTbl + ' (' + @Structure_FK + ') ' + @SqlEXEC

--Select @SqlEXEC
--print(@SqlEXEC)
--Select @Select, @From, @QueryExec, @Alias, @Prod_Alias
	
	EXEC(@SqlEXEC)
	

	End
		FETCH NEXT FROM Entity_Class INTO @Entity_Class_CR_FK
		End

	Close Entity_Class;
	Deallocate Entity_Class;


----get filter code

	Set @DN_String = Case	when @DN_String <> '' and right(ltrim(rtrim(@DN_String)), 1) <> '~' then ltrim(rtrim(@DN_String)) + '~' 
							when @DN_String = '' and @Data_Name_FK <> 0 and @Data_Name_FK not like '%~' then @Data_Name_FK + '~'
					 else @DN_String end

	While charindex('~', @DN_String, @DN_StartPOS) <> 0 
		Begin

			Set @DN_POS = charindex('~', @DN_String, @DN_StartPOS)
			Set @DN_Length = @DN_POS - @DN_StartPOS
			Set @Data_Name_FK = Substring(@DN_String, @DN_StartPOS, @DN_Length)

			Set @DN_StartPOS = @DN_POS + 1

			Set @Data_Name_FK = Case when isnumeric(@Data_Name_FK) = 1 then @Data_Name_FK else (Select data_name_id from epacube.data_name with (nolock) where name = '' + @Data_Name_FK + '') end

	If object_id('tempdb..#DNFltrCursor') is not null
	drop table #DNFltrCursor
--Select * from #DNFltrCursor order by pass Select * from EPACUBE.DATA_NAME_GROUP_REFs where data_name_fk in (110100, 550100, 550020)

	--Declare @Data_Name_FK varchar(16)
	--Set @Data_name_fk = 110100 --550001 --550020 --550020, 550100, 110100

--Select * from #DNFltrCursor
--Select @Data_Name_FK

		Select  *, Rank()over(order by Case when Data_Name_FK = @Data_Name_FK then 1 When Filter_Entity_Class_CR_FK = Column_Entity_Class_CR_FK and qtbl not like '%product_association%' then 2 when qtbl like '%product_association%' then 3 else data_name_fk end) Pass
		into #DNFltrCursor
		from (
		Select Lookup_Values, Alias_DV, Local_Column, QTbl, Filter_Entity_Class_CR_FK, Column_Entity_Class_CR_FK, DN_Label, Max(Vendor_Structure_Column) 'Vendor_Structure_Column', DN_Type, Op, VirtualJoin
			, Alias
			, Max(Customer_Structure_Column) 'Customer_Structure_Column', Max(Product_Structure_Column) 'Product_Structure_Column'
			, Data_Name_FK
		from (Select  
		isnull(Uf.Lookup_Values, 0) 'Lookup_Values'
		, isnull(r.ALIAS_DV, '') 'Alias_DV'
		, R.Data_Column Local_Column
		,  Replace(ltrim(rtrim(R.QTbl)), 'Entity_Structure', 'Entity_Identification') QTbl
		, (Select top 1 Entity_class_cr_fk from EPACUBE.DATA_NAME_GROUP_REFS with (nolock) where data_name_fk = @data_name_fk and entity_class_cr_fk is not null) FILTER_ENTITY_CLASS_CR_FK
		, R.Entity_Class_CR_FK Column_entity_Class_CR_FK
		, R.DN_Label
		, r.Vendor_Structure_Column
		, Case when r.data_name_fk in (144010, 144020) then 'Entity_Data_Name_FK' else 'Data_Name_FK' end 'DN_Type'
		, Case	when p3.OpExclusion like '%!%' then ' not in ' else ' in ' end Op
		, ' inner ' 'VirtualJoin'
		, R.Alias
		, r.Customer_Structure_Column
		, r.Product_Structure_Column 
		, r.data_name_fk data_name_fk
		from EPACUBE.DATA_NAME_GROUP_REFS R with (nolock)
		left join #Param3 P3 on cast(r.Data_Name_FK as varchar(64)) = P3.critdn
		left join epacube.ui_filter uf with (nolock) on r.data_name_fk = uf.Data_Name_FK
		Where 1 = 1
			and isnumeric(isnull(P3.CritDN, 0)) = 1
			and r.data_name_fk = @Data_Name_FK
			and r.V_Ref_Data_Name_FK is null
		) A  
		Group by Lookup_Values, Alias_DV, Local_Column, QTbl, Filter_Entity_Class_CR_FK, Column_Entity_Class_CR_FK, DN_Label, DN_Type, Op, VirtualJoin
		, Alias, Data_Name_FK
		) B Order by pass 

--select * from EPACUBE.DATA_NAME_GROUP_REFS where data_name_fk = 151110
--select @DN_String

	DECLARE DNFltrCode Cursor local for
	Select * from #DNFltrCursor order by pass

		Set @SQLcodeStart = ''
		Set @SQLcodeIn = ''
		Set @SQLcodeNotIn = ''

		OPEN DNFltrCode;
		Set @Max_Pass = @@CURSOR_ROWS;

		FETCH NEXT FROM DNFltrCode INTO @Lookup_Values, @Alias_DV, @Local_Column, @QTbl, @FILTER_ENTITY_CLASS_CR_FK, @Column_Entity_Class_CR_FK, @DN_Label, @Vendor_Structure_Column, @DN_Type, @Op, @VirtualJoin, @Alias, @Customer_Structure_Column, @Product_Structure_Column, @DNtmp, @Pass
		WHILE @@FETCH_STATUS = 0

		Begin--Fetch

		IF (select [epacube].getDataTypeForDN(@Data_Name_FK)) = 'Date'
		goto nextfilter

		If @Filter_Entity_Class_CR_FK = 10109
		Begin

			Set @ImmedAlias = Case when Object_ID('TempTbls.tmp_PROD_' + @UserID) is not null then 't_prod' else @Prod_Alias end

			Set @SQLcodeStart = 
				Case	when @SQLcodeStart <> '' then @SQLcodeStart 
						when @Alias_DV <> '' and @Pass = 1 and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
		'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + ']  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @DNtmp					
						when @Alias_DV = '' and @Pass = 1 and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
		--'(Select ' + @Alias + '.[' + @Local_Column + '] ''KEY'', ' + @Alias + '.[' + @Local_Column + ']' + ' + '': '' + pd.[description] ''LABEL'' from epacube.' + @QTbl + ' ' + @Alias + ' with (nolock) inner join epacube.product_description pd with (nolock) on ' + @Alias + '.product_structure_fk = pd.product_structure_fk and pd.data_name_fk = 110401 where ' + @Alias + '.' + @DN_Type + ' = ' + @DNtmp
		'(Select ' + @Alias + '.[' + @Local_Column + '] ''KEY'', ' + 'pd.[description] ''LABEL'' from epacube.' + @QTbl + ' ' + @Alias + ' with (nolock) inner join epacube.product_description pd with (nolock) on ' + @Alias + '.product_structure_fk = pd.product_structure_fk and pd.data_name_fk = 110401 where ' + @Alias + '.' + @DN_Type + ' = ' + @DNtmp											
				
				else '' end		

				Set @SQLcodeIn = @SQLcodeIn + 
				Case	when @SQLcodeIn = '' and @op = ' in ' and (@Pass <> 1 or @Max_Pass = 1) and Object_ID('TempTbls.tmp_PROD_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		and ' + case when @Alias_DV <> '' then '' else @Alias + '.' end + '[product_structure_fk] in (select ' + @ImmedAlias + '.[product_structure_fk] from TempTbls.tmp_PROD_' + @UserID + ' ' + @ImmedAlias + ' '
						when @SQLcodeIn = '' and @op = ' in 'and (@Pass <> 1  or @Max_Pass = 1) and Object_ID('TempTbls.tmp_PROD_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then ' 
		and ' + case when @Alias_DV <> '' then '' else @Alias + '.' end + 'product_structure_fk in (select ' + @Prod_Alias + '.product_structure_Fk from epacube.product_identification ' + @ImmedAlias + ' with (nolock) '
				else '' end

--select @SQLcodeStart, @SQLcodeIn '@SQLcodeIn', @Alias_DV '@Alias_DV', @Alias '@Alias'
--Select @SQLcodeIn '@SQLcodeIn', @op '@op', @VirtualAction '@VirtualAction', @Pass '@Pass', @Max_Pass '@Max_Pass', @COLUMN_ENTITY_CLASS_CR_FK '@COLUMN_ENTITY_CLASS_CR_FK', @FILTER_ENTITY_CLASS_CR_FK '@FILTER_ENTITY_CLASS_CR_FK' 

			Set @SQLcodeIn = @SQLcodeIn + 
				Case	when @SQLcodeIn <> '' and @op = ' in ' and @Pass <> 1 and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @ImmedAlias + '.[Product_Structure_FK] = ' + @Alias + '.[Product_Structure_FK] and ' + @Alias + '.' + @DN_Type + ' = ' + @DNtmp
		+ Case when @SQLcodeIn not like '%' + @Item_Data_Name_FK + '%' and @ImmedAlias <> 't_prod' then ' and ' + @Prod_Alias + '.data_name_fk = ' + @Item_Data_Name_FK else '' end
				else '' end

			Set @SQLcodeIn = @SQLcodeIn + 
				Case	when @SQLcodeIn <> '' and @op = ' in ' and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join TempTbls.tmp_CUST_' + @UserID + ' t_cust with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = t_cust.cust_entity_structure_fk '
					else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and @op = ' in ' and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK 
					and @SQLcodeIn like '%t_cust%'	then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = t_cust.cust_entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @DNtmp
					else '' end

		Set @SQLcodeNotIn = @SQLcodeNotIn + 
			Case	when @SQLcodeNotIn = '' and @op = ' not in ' and Object_ID('TempTbls.tmp_PROD_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK and @SQLcodeIn not like '%' + @Alias + '%'	then '
		and [product_structure_fk] not in (select ' + @ImmedAlias + '.[product_structure_fk] from TempTbls.tmp_PROD_' + @UserID + ' ' + @ImmedAlias + ' '
					when @SQLcodeNotIn = '' and @op = ' not in ' and Object_ID('TempTbls.tmp_PROD_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK and @SQLcodeIn not like '%' + @Alias + '%'	 then '
		and product_structure_fk not in (select ' + @Prod_Alias + '.product_structure_Fk from epacube.product_identification ' + @ImmedAlias + ' with (nolock) '
			else '' end

		Set @SQLcodeNotIn = @SQLcodeNotIn + 
			Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' +@VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @ImmedAlias + '.[Product_Structure_FK] = ' + @Alias + '.[Product_Structure_FK] and ' + @Alias + '.' + @DN_Type + ' = ' + @DNtmp
					else '' end
		Set @SQLcodeNotIn = @SQLcodeNotIn + 
		+	Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join TempTbls.tmp_CUST_' + @UserID + ' t_cust with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = t_cust.cust_entity_structure_fk '
				else '' end

		Set @SQLcodeNotIn = @SQLcodeNotIn + 
			Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK 
					and @SQLcodeNotIn like '%t_cust%' then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = t_cust.cust_entity_structure_fk and ' + @DN_Type + ' = ' + @DNtmp
				else '' end

	End
	Else
	If @Filter_Entity_Class_CR_FK = 10104
	Begin

		Set @ImmedAlias = Case when Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null then 't_cust' else 'eic' end
		Set @Cust_Structure_FK_Init = Case when @Pass = 1 then @Customer_Structure_Column else @Cust_Structure_FK_Init end
		Set @PA_Alias = Case when @Qtbl like '%product_association%' then @Alias else @PA_Alias end

		Set @SQLcodeStart = Case when @SQLcodeStart <> '' then @SQLcodeStart else 
			Case 
				when @Pass = 1 and Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
			'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + ']  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @DNtmp
				when @Pass = 1 and Object_ID('TempTbls.tmp_CUST_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
			'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + ']  ''LABEL'' from epacube.' + @QTbl + ' with (nolock) where ' + @DN_Type + ' = ' + @DNtmp
			else '' end			End

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn = '' and @op = ' in ' and (@Pass <> 1 or @Max_Pass = 1) and Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null then '
		and [' + @Cust_Structure_FK_Init + '] in (select ' + @ImmedAlias + '.[Cust_Entity_Structure_FK] from TempTbls.tmp_CUST_' + @UserID + ' ' + @ImmedAlias + ' '
					when @SQLcodeIn = '' and @op = ' in ' and (@Pass <> 1 or @Max_Pass = 1) and Object_ID('TempTbls.tmp_CUST_' + @UserID) is null then '
		and [' + @Cust_Structure_FK_Init + '] in (select eic.entity_structure_fk from epacube.entity_identification ' + @ImmedAlias + ' with (nolock) '
			else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and @op = ' in ' and @Pass <> 1 and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @ImmedAlias + '.[' 
				+ Case when @ImmedAlias = 'eic' then 'entity_structure_fk' else 'cust_entity_structure_fk' end
				 + '] = ' + @Alias + '.[' + @Customer_Structure_Column + '] and ' + @Alias + '.' + @DN_Type + ' = ' + @DNtmp
			else '' end
							
		Set @SQLcodeIn = @SQLcodeIn + --Product_Association is included here
			Case	when @SQLcodeIn <> '' and @op = ' in ' and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null 
					and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK 
					then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = t_cust.cust_entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @DNtmp 
			else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and @op = ' in ' and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is null 
					and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = eic.entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @DNtmp 
			else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and @op = ' in ' and @Pass <> 1 and @Product_Structure_Column <> '0' and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK
					and @SQLcodeIn not like '%t_prod%' and @SQLcodeIn not like '%epacube.product_identification pi%'
					and @SQLcodeIn like '%PRODUCT_ASSOCIATION%'
					then '
			' + @VirtualJoin + 'join ' + Case	when Object_ID('TempTbls.tmp_Prod_' + @UserID) is not null then 'TempTbls.tmp_Prod_' + @UserID + ' t_PROD with (nolock) on ' + @Alias + '.[Product_Structure_FK] = t_prod.product_structure_fk '
												when Object_ID('TempTbls.tmp_Prod_' + @UserID) is null then 'epacube.product_identification ' + @Prod_Alias + ' with (nolock) on ' + @PA_Alias + '.[Product_Structure_FK] = ' + @Prod_Alias + '.product_structure_fk and ' + @Prod_Alias + '.data_name_fk = ' 
												+ @Item_Data_Name_FK
										 else '' end
			else '' end

		Set @SQLcodeIn = @SQLcodeIn + 
			Case	when @SQLcodeIn <> '' and @Pass <> 1 and @Product_Structure_Column <> '0' and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK
					and (@SQLcodeIn like '%t_prod%' or @SQLcodeIn like '%epacube.product_identification pi%' or @SQLcodeIn like '%PRODUCT_ASSOCIATION%')
					and @qtbl not like '%PRODUCT_ASSOCIATION%'
					then '
			' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + Case when @SQLcodeIn like '%t_prod%' then 't_prod' when @SQLcodeIn like '%PI_%' then @Prod_Alias when @SQLcodeIn like '%Product_Association%' then  @PA_Alias end 
			+ '.[' + @Product_Structure_Column + '] = ' + @Alias + '.[Product_Structure_FK] and ' + @Alias + '.' + @DN_Type + ' = ' + @DNtmp  									
				 else ''
				 end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn = '' and @op = ' not in ' and Object_ID('TempTbls.tmp_CUST_' + @UserID) is not null then '
	and [' + @Cust_Structure_FK_Init +'] not in (select ' + @ImmedAlias + '.[cust_entity_structure_fk] from TempTbls.tmp_CUST_' + @UserID + ' ' + @ImmedAlias + ' '
				when @SQLcodeNotIn = '' and @op = ' not in ' and Object_ID('TempTbls.tmp_CUST_' + @UserID) is null then '
	and [' + @Cust_Structure_FK_Init +'] not in (select eic.entity_structure_Fk from epacube.entity_identification ' + @ImmedAlias + ' with (nolock) '
		else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Pass <> 1 and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then '
	' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @ImmedAlias + '.[' + Case when @ImmedAlias = 'eic' then 'entity_structure_fk' else 'cust_entity_structure_fk' end + '] = ' + @Alias + '.[' + @Customer_Structure_Column + '] and ' + @Alias + '.' + @DN_Type + ' = ' + @DNtmp
		else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK 
				then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = t_cust.cust_entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @DNtmp
			else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> ''and @op = ' not in ' and @Pass <> 1 and @Customer_Structure_Column <> '0' and Object_ID('TempTbls.tmp_Cust_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + @Alias + '.[' + @Customer_Structure_Column + '] = eic.entity_structure_fk and ' + @Alias + '.' + @DN_Type + ' = ' + @DNtmp
			else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> '' and @op = ' not in ' and @Pass <> 1 and @Product_Structure_Column <> '0' and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK
				and @SQLcodeNotIn not like '%t_prod%' and @SQLcodeNotIn not like '%epacube.product_identification pi%'  
				then '
		' + @VirtualJoin + 'join ' + Case	when Object_ID('TempTbls.tmp_Prod_' + @UserID) is not null then 'TempTbls.tmp_Prod_' + @UserID + ' t_PROD with (nolock) on ' + @Alias + '.[Product_Structure_FK] = t_prod.product_structure_fk '
											when Object_ID('TempTbls.tmp_Prod_' + @UserID) is null then 'epacube.product_identification ' +  @Prod_Alias + ' with (nolock) on ' + @PA_Alias + '.[Product_Structure_FK] = ' + @Prod_Alias + '.product_structure_fk and ' + @Prod_Alias + '.data_name_fk = ' + @Item_Data_Name_FK
									 else '' end
		else '' end

	Set @SQLcodeNotIn = @SQLcodeNotIn + 
		Case	when @SQLcodeNotIn <> '' and @Pass <> 1 and @Product_Structure_Column <> '0' and @COLUMN_ENTITY_CLASS_CR_FK <> @FILTER_ENTITY_CLASS_CR_FK
				and (@SQLcodeNotIn like '%t_prod%' or @SQLcodeNotIn like '%epacube.product_identification pi%' or @SQLcodeNotIn like '%EPACUBE.[PRODUCT_ASSOCIATION]%')
				and @qtbl not like '%PRODUCT_ASSOCIATION%'
				then '
		' + @VirtualJoin + 'join ' + @QTbl + ' ' + @Alias + ' with (nolock) on ' + Case when @SQLcodeNotIn like '%t_prod%' then 't_prod' 
			when @SQLcodeNotIn like '%PI_%' then @Prod_Alias when @SQLcodeNotIn like '%Product_Association%' then  @PA_Alias end + '.[' + @Product_Structure_Column + '] = ' + @Alias + '.[Product_Structure_FK] and ' +
			 @Alias + '.' + @DN_Type + ' = ' + @DNtmp									
			 else '' end
	
	End
	
	Else
	If @Filter_Entity_Class_CR_FK = 10103
	Begin
			Set @SQLcodeStart = Case when Object_ID('TempTbls.tmp_Vendor_' + @UserID) is not null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
		'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + '] ''LABEL'' from epacube.' + @QTbl + ' where data_name_fk = ' + @Data_Name_FK 
		+ ' and ' + @Vendor_Structure_Column + ' in (select t_Vendor.Vendor_Entity_Structure_fk from TempTbls.tmp_Vendor_' + @UserID + ' t_Vendor with (nolock)'
									 when Object_ID('TempTbls.tmp_Vendor_' + @UserID) is null and @COLUMN_ENTITY_CLASS_CR_FK = @FILTER_ENTITY_CLASS_CR_FK then
		'(Select [' + @Local_Column + '] ''KEY'', [' + @Local_Column + '] ''LABEL'' from epacube.' + @QTbl + ' where data_name_fk = ' + @Data_Name_FK
		else isnull(@SQLcodeStart, '')
		End
	End
	Else
	If @Filter_Entity_Class_CR_FK = 10117
	Begin
		Set @SQLcodeStart = 
		'(Select EIZ.VALUE ''KEY'', ein.value ''LABEL'' From epacube.epacube.entity_structure ESZ with (nolock) 
		Inner Join epacube.entity_identification eiz with (nolock) on esz.entity_structure_id = eiz.entity_structure_fk and esz.data_name_fk = eiz.entity_data_name_fk and esz.data_name_fk in (151000) 
		Inner Join epacube.ENTITY_IDENT_NONUNIQUE ein with (nolock) on esz.entity_structure_id = ein.ENTITY_STRUCTURE_FK and ein.data_name_fk = 140200
		--inner join [epacube].[getZonesByPermission](''' +  Replace(@UserID, '_com', '.com') + ''') A1 on eiz.entity_structure_fk = a1.entity_structure_fk
		inner join [epacube].[getEntitiesByPermission](''' +  Replace(@UserID, '_com', '.com') + ''', 151110) A1 on eiz.entity_structure_fk = a1.entity_structure_fk
		Where 1 = 1
		' + ' Union Select ''UNASSIGNED'' ''VALUE'', ''UNASSIGNED'' ''description''
		'
	End

NextFilter:		

	FETCH NEXT FROM DNFltrCode INTO @Lookup_Values, @Alias_DV, @Local_Column, @QTbl, @FILTER_ENTITY_CLASS_CR_FK, @Column_Entity_Class_CR_FK, @DN_Label, @Vendor_Structure_Column, @DN_Type, @Op, @VirtualJoin, @Alias, @Customer_Structure_Column, @Product_Structure_Column, @DNtmp, @Pass
	End--Fetch
	Close DNFltrCode;
	Deallocate DNFltrCode;

	--Declare @ZES_FK varchar(16)
	--Set @ZES_FK = isnull((Select top 1 entity_structure_fk from epacube.epacube.ENTITY_IDENTIFICATION eiz with (nolock) inner join #param3 p3 on eiz.value = p3.CritValue and eiz.entity_data_name_fk = 151000 and eiz.data_name_fk = 151110), 0)

	--If @ZES_FK <> 0 and @Product_Structure_Column = 'Product_Structure_FK'
	--	Set @SQLcodeIn = @SQLcodeIn + ') 
	--' + ' and ' + Case when @SQLcodeIn like '%' + @Alias + '%' then @Alias + '.' else '' end + 'product_structure_fk in (select pa_159450.product_structure_Fk from epacube.product_association pa_159450 with (nolock) 
	--											where pa_159450.data_name_fk = 159450 and pa_159450.entity_structure_fk = ' + @ZES_FK

	If (select count(*) from #Param3 where critdn = 151110) <> 0 and @Product_Structure_Column = 'Product_Structure_FK'
		Set @SQLcodeIn = @SQLcodeIn + ') 
	' + ' and ' + Case when @SQLcodeIn like '%' + @Alias + '%' then @Alias + '.' else '' end + 'product_structure_fk in (select pa_159450.product_structure_Fk from epacube.product_association pa_159450 with (nolock) 
												inner join TempTbls.tmp_ZONE_' + @UserID + ' t_zone on pa_159450.entity_structure_fk = t_zone.zone_entity_structure_fk
												where pa_159450.data_name_fk = 159450'
	
		Set @SQLcodeIn = @SQLcodeIn + Case when @SQLcodeIn <> '' then  ')' else '' end
		Set @SQLcodeNotIn = @SQLcodeNotIn + Case when @SQLcodeNotIn <> '' then  ')' else '' end

	If @Lookup_Values <> 1
	Begin
		Set @SQLcodeIn = ''
		Set @SQLcodeNotIn = ''
	End

		Set @Filtercode = ( @SQLcodeStart + '
		' + @SQLcodeIn + '
		' + @SQLcodeNotIn + ')')

	If @Alias_DV <> ''
		Begin
			Set @POS = charindex(' from ', @Filtercode)
			Set @Filtercode = Right(@Filtercode, Len(@Filtercode) - @POS)
			Set @Filtercode = 'Select data_value_fk ' + @Filtercode
			Set @Filtercode = 
				'(select value ''KEY'', [Description] ''LABEL'' from epacube.data_value where data_value_id in (' + @Filtercode + ')' 
		End
	If @Lookup_Values = 1
		Begin
			If ltrim(rtrim(@Filtercode)) > ' '
			Begin
				Set @FilterCode = '((((((' + ltrim(rtrim(@FilterCode)) + '))))))'
				Set @FilterCode = Replace(Replace(@FilterCode, '(((((((', ''), ')))))))', '')
			End				

			If Len(@FilterCode) > 10
			Begin

				Set @FilterCode = Case	when @FilterCode not like '%pd.[description]%%' then
											--Replace(Replace(Replace(@FilterCode, '''Key''', '''value'''), '[Description] ''label''', '[Value] + '': '' + [description]' + ' ''description'''), '''label''', '''description''')
										Replace(Replace(Replace(@FilterCode, '''Key''', '''value'''), '[Description] ''label''', '[description]' + ' ''description'''), '''label''', '''description''')
										else 
											Replace(Replace(@FilterCode, '''Key''', '''value'''), '''label''', '''description''')
									end

				Set @QueryScript = @QueryScript + ' Select ' + @Data_Name_FK + ' ''Data_Name_FK'', ''' + Replace(@FilterCode, '''', '''''') + ''' ''SQL_Query'' Union '
			End
		End
		Else
		If @Lookup_Values = 2
			Begin
				Set @DV_OrderBy = 'Order by ' + Case when (select count(*) from epacube.data_value where data_name_fk = @DN_FK and isnumeric(value) <> 1) > 0 then ' value' else ' cast(value as int)' end

				--Set @QueryScript = @QueryScript + 'Select ' + @Data_Name_FK + ' ''Data_Name_FK'', ''Select Value ''''value'''', value + '''': '''' + Description ''''description'''' from epacube.data_value where data_name_fk = ' + @Data_Name_FK + ' ' + @DV_OrderBy + ''' ''SQL_Query'' Union '
				Set @QueryScript = @QueryScript + 'Select ' + @Data_Name_FK + ' ''Data_Name_FK'', ''Select Value ''''value'''', Description ''''description'''' from epacube.data_value where data_name_fk = ' + @Data_Name_FK + ' ' + @DV_OrderBy + ''' ''SQL_Query'' Union '			
			
			End
	End --End of While Loop

	Set @QueryScript = Case when Right(ltrim(rtrim(@QueryScript)), 5) = 'Union' then Left(ltrim(rtrim(@QueryScript)), Len(ltrim(rtrim(@QueryScript))) - 5) else @QueryScript end
	
	If @FilterResultsPrint = 1
	Begin
		Select @QueryScript
		
		--Print(@QueryScript)

		goto EOP
		
	end

	If @FilterResultsExec = 1
	Begin

		Set @QueryScript = case when isnull(@QueryScript, '') = '' then 'Select ' + @Data_Name_FK + ' Data_Name_FK, ''Select Cast(0 as int) value, Cast(null as varchar(256)) '' SQL_Query' else @QueryScript end

		exec(@QueryScript)
		GoTo EOP
	end
	Else
	If @FilterResultsExec = 2
	Begin
	Select @Filtercode
		Exec(@Filtercode)
		
		goto EOP
		
	end
Last_Step:
	
EOP:

--If Object_ID('TempTbls.tmp_Prod_' + @UserID) is not null 
--	Begin
--		Declare @S0 table(v nvarchar(max))
--		Declare @S1 varchar(max)
--		Declare @S2 Varchar(max)
--		Declare @Cnt int

--		Declare @Cst int
--		Set @Cst = Case when Object_ID('TempTbls.tmp_Cust_' + @UserID) is not null then 1 else 0 end

--		Set @S1 = 'Select isnull((Select count(*) from TempTbls.tmp_Prod_' + @UserID + '), 0)'

--		Insert into @S0
--		Exec(@S1)

--		Set @Cnt = (Select top 1 * from @S0)

--		If @Cnt = 0
--		Begin
--			Set @S2 = (Select TOP 1 'Insert into epacube.TempTbls.tmp_Prod_' + @UserID + ' Select distinct t1.product_structure_fk from ' + isnull(isc.table_schema, '') + '.' + isnull(isc.table_name, '') + ' T1 inner join epacube.data_value dv on t1.data_value_fk = dv.data_value_id ' + '
--			' + Case when @Cst = 1 then ' inner join epacube.TempTbls.tmp_Cust_' + @UserID + ' C1 on T1.entity_structure_fk = C1.cust_entity_structure_fk ' else '' end + '
--			' + 'where dv.data_name_fk = ' + F.critDN + ' and dv.value = ''' + F.critvalue + ''' and T1.Product_Structure_FK is not null ' + '
--			' + Case when @CST = 0 then ' and t1.entity_structure_fk is null' else '' end
--			from #Param3 F with (nolock)
--			inner join epacube.data_name dn with (nolock) on F.CritDN = dn.DATA_NAME_ID
--			inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id
--			inner join epacube.INFORMATION_SCHEMA.columns isc with (nolock) on ds.table_name = isc.table_name and ds.COLUMN_NAME = isc.COLUMN_NAME
--			left join epacube.data_value dv with (nolock) on f.CritDN = dv.data_name_fk and f.CritValue = dv.VALUE
--			where ds.column_name = 'Data_Value_FK' Order by F.CritDN)

--			Exec(@S2)
--		End
--End

	--Declare @UserID varchar(64) = 'gstone@epacube_com'

	Declare @FiltersInForce Varchar(64)
	Set @FiltersInForce = 'TempTbls.tmp_Filters_InForce_' + @UserID

	Exec('if object_id(''' + @FiltersInForce + ''') is not null
	drop table ' + @FiltersInForce)

	Exec('
	Create Table ' + @FiltersInForce + ' (CritDN Varchar(64) Null, CritValue Varchar(Max) Null, OpExclusion Varchar(8) Null, OptionSetting Varchar(128) Null
	, OptionValue Varchar(64) Null, WhereOption Varchar(Max) Null, WhereDRank Varchar(64), V_Ref_Data_Name_FK bigint null, Data_Type varchar(16) Null
	, Date_Type varchar(16) Null)')

	Exec('Insert into ' + @FiltersInForce + '
	' + '(CritDN, CritValue, OpExclusion, OptionSetting, OptionValue) 
	Select CritDN
	, Case when UF.options is not null then Null else CritValue end ''CritValue''
	, Case when OpExclusion <> ''!'' then Null else OpExclusion end ''OpExclusion''
	, Case When UF.options is not null then
		case	when CritValue in (''yes'', ''no'', ''ignore'') then CritValue
				when isnull(OpExclusion, '''') <> '''' then OpExclusion
				else (select default_value from epacube.ui_filter where data_name_fk = CritDN) end end ''OptionSetting''
				
				--When UF.options like ''%on:%'' then ''on''
				--When UF.options like ''%after:%'' then ''after''
				--When UF.options like ''%before:%'' then ''before''
				--When UF.options like ''%all:%'' then ''all''
				--When UF.options like ''%yes:%'' then ''yes''
				--When UF.options like ''%no:%'' then ''no'' 
				--else (select default_value from epacube.ui_filter where data_name_fk = CritDN) end end ''OptionSetting''
				--else ''all'' end  End ''OptionSetting''
	
	, Case when UF.options is not null then
		Case	when epacube.getdatatypefordn(critdn) = ''Date'' and isnull(critvalue, '''') = '''' then cast(cast(getdate() as date) as varchar(64))
				when epacube.getdatatypefordn(critdn) = ''Date'' and isdate(critvalue) = 1 then critvalue
				when epacube.getdatatypefordn(critdn) = ''Date'' and critvalue like ''%`%'' then reverse(left(Reverse(CritValue), charindex(''`'', reverse(CritValue)) - 1))
		end
		end OptionValue
	from TempTbls.tmp_FILTER_' + @UserID + ' F inner join epacube.ui_filter UF on F.critdn = cast(UF.data_name_fk as varchar(64)) 
	where UF.options is not null or isnull(critvalue, '''') <> ''''') 

	Exec('
	Insert into ' + @FiltersInForce + '
	' + '(CritDN, OptionSetting, OptionValue)
	Select
	data_name_fk
	, OptionSetting
	, OptionValue
	from (
	Select 
	uf.data_name_fk
	--, Case	when options like ''%on:%'' then ''on''
	--		when options like ''%before:%'' then ''before''
	--		when options like ''%after:%'' then ''after''
	--		when options like ''%all:%'' then ''all'' 
	--		when options like ''%yes:%'' then ''yes'' 
	--		when options like ''%no:%'' then ''no''
	--		else (select default_value from epacube.ui_filter where data_name_fk = uf.data_name_fk)
	--		else ''all'' 
	--		end ''OptionSetting''
	, uf.default_value ''OptionSetting''
	, Case when epacube.getdatatypefordn(uf.data_name_fk) = ''Date'' then Cast(Cast(getdate() as date) as varchar(64))
		end ''OptionValue''
	from epacube.ui_filter UF
	left join ' + @FiltersInForce + ' f on uf.data_name_fk = f.CritDN
	where 1 = 1
	and f.critdn is null 
	and uf.options is not null
	and uf.Record_Status_CR_FK = 1
	) a')

	--Exec('
	--Update F
	--Set WhereOption = Case when isnull(r.date_type, '''') <> ''effective'' then null else
	--					Case isnull(f.OptionSetting, '''')	when ''all'' then ''where 1 = 1 and DNames.DN_FK = '' + isnull(r.v_ref_data_name_fk, r.data_Name_FK)
	--													when ''before'' then ''where 1 = 1 and DNames.DN_FK = '' + isnull(r.v_ref_data_name_fk, r.data_Name_FK) + '' and '' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' <= '''''' + f.OptionValue + ''''''''
	--													when ''on'' then ''where 1 = 1 and DNames.DN_FK = '' +  + isnull(r.v_ref_data_name_fk, r.data_Name_FK) + '' and '' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' <= '''''' + f.OptionValue + ''''''''
	--													when ''after'' then ''where 1 = 1 and DNames.DN_FK = '' + isnull(r.v_ref_data_name_fk, r.data_Name_FK) + '' and '' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' >= '''''' + f.OptionValue + '''''''' end
	--				 end
	--, WhereDRank = Case when isnull(r.date_type, '''') <> ''effective'' then null
	--					when isnull(f.OptionSetting, '''')	= ''on'' then ''where 1 = 1 and DRank = 1'' end
	--, V_Ref_Data_Name_FK = r.V_Ref_Data_Name_FK
	--, OptionValue = case when F.OptionSetting = ''All'' then Null else f.OptionValue end
	--, data_type = isnull(r.data_type, ''''), date_type = isnull(r.date_type, '''')
	--from ' + @FiltersInForce + ' F
	--inner join epacube.DATA_NAME_GROUP_REFS r on F.critdn = r.data_name_fk
	--where r.qtbl in (''marginmgr.[pricesheet_basis_values]'', ''#PBV'')')

	Exec('
	Update F
	Set WhereOption = Case when isnull(r.date_type, '''') <> ''effective'' then null else
						Case isnull(f.OptionSetting, '''')	when ''ignore'' then ''''
														when ''before'' then ''and '' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' <= '''''' + f.OptionValue + ''''''''
														when ''on'' then ''and '' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' <= '''''' + f.OptionValue + ''''''''
														when ''after'' then ''and '' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' >= '''''' + f.OptionValue + '''''''' end
					 end
	, WhereDRank = Case when isnull(r.date_type, '''') <> ''effective'' then null
						when isnull(f.OptionSetting, '''')	= ''on'' then ''where 1 = 1 and DRank = 1'' end
	, V_Ref_Data_Name_FK = r.V_Ref_Data_Name_FK
	, OptionValue = case when F.OptionSetting = ''ignore'' then Null else f.OptionValue end
	, data_type = isnull(r.data_type, ''''), date_type = isnull(r.date_type, '''')
	from ' + @FiltersInForce + ' F
	inner join epacube.DATA_NAME_GROUP_REFS r on F.critdn = r.data_name_fk
	where r.qtbl in (''marginmgr.[pricesheet_basis_values]'', ''#PBV'')')

	Exec('
	' + 'Update F
	Set WhereOption = Case when isnull(r.date_type, '''') <> ''effective'' then null else
						Case isnull(f.OptionSetting, '''')	when ''ignore'' then Null
														when ''before'' then '' and ('' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' <= '''''' + f.OptionValue + ''''''''
														when ''on'' then '' and ('' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' <= '''''' + f.OptionValue + ''''''''
														when ''after'' then '' and ('' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' >= '''''' + f.OptionValue + '''''''' 
														+ '' or '''''' + f.OptionValue + '''''' between '' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' and ''
														+ Replace(replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), ''''), ''Effective_Date'', ''End_Date'') 
														end
					 end
	, WhereDRank = Case when isnull(r.date_type, '''') <> ''effective'' then null
						when isnull(f.OptionSetting, '''')	= ''on'' then ''where 1 = 1 and DRank = 1'' end
	, V_Ref_Data_Name_FK = r.V_Ref_Data_Name_FK
	, OptionValue = case when F.OptionSetting = ''ignore'' then Null else f.OptionValue end
	, data_type = isnull(r.data_type, ''''), date_type = isnull(r.date_type, '''')
	from ' + @FiltersInForce + ' F
	inner join epacube.DATA_NAME_GROUP_REFS r on F.critdn = r.data_name_fk
	where r.qtbl in (''SYNCHRONIZER.[RULES_SALES_PROGRAMS]'')')

	Exec('
	' + 'Update F
	Set WhereOption = Case when isnull(r.date_type, '''') <> ''effective'' then null else
						Case isnull(f.OptionSetting, '''')	when ''ignore'' then ''''
														when ''before'' then '' and '' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' <= '''''' + f.OptionValue + ''''''''
														when ''on'' then '' and '''''' + f.OptionValue + '''''' between '' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' and isnull(''
														+ Replace(replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), ''''), ''Effective_Date'', ''End_Date'') + '', cast(getdate() as date))''
														when ''after'' then '' and ('' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' >= '''''' + f.OptionValue + '''''''' 
														+ '' or '''''' + f.OptionValue + '''''' between '' + replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), '''') + '' and isnull(''
														+ Replace(replace(r.data_column, ''_'' + isnull(r.v_ref_data_name_fk, r.data_Name_FK), ''''), ''Effective_Date'', ''End_Date'') + '', cast(getdate() as date)))'' 
														end
					 end
	, WhereDRank = Case when isnull(r.data_type, '''') <> ''Date'' then null
						when isnull(f.OptionSetting, '''')	= ''on'' then ''where 1 = 1 and DRank = 1'' end
	, V_Ref_Data_Name_FK = r.V_Ref_Data_Name_FK
	, OptionValue = case when F.OptionSetting = ''ignore'' then Null else f.OptionValue end
	, data_type = isnull(r.data_type, ''''), date_type = isnull(r.date_type, '''')
	from ' + @FiltersInForce + ' F
	inner join epacube.DATA_NAME_GROUP_REFS r on F.critdn = r.data_name_fk
	where r.qtbl not in (''marginmgr.[pricesheet_basis_values]'', ''#PBV'', ''SYNCHRONIZER.[RULES_SALES_PROGRAMS]'')')

	Exec('
	' + 'Update F
	Set WhereOption = Case isnull(f.OptionSetting, '''')	when ''ignore'' then ''''
														when ''before'' then '' and cast('' + r.data_column + '' as date) <= '''''' + f.OptionValue + ''''''''
														when ''on'' then '' and cast('' + r.data_column + '' as date) = '''''' + f.OptionValue + ''''''''
														when ''after'' then '' and cast('' + r.data_column + ''as date) >= '''''' + f.OptionValue + ''''''''
														end
	, data_type = isnull(r.data_type, ''''), date_type = isnull(r.date_type, '''')
	from ' + @FiltersInForce + ' F
	inner join epacube.DATA_NAME_GROUP_REFS r on F.critdn = r.data_name_fk
	where R.DATA_TYPE = ''DATE'' AND R.DATE_TYPE = ''other''')

	Exec('
	' + 'Update F
	Set WhereOption = case f.OptionSetting	when ''yes'' then r.virtual_where_clause
											when ''no'' then Replace(replace(virtual_where_clause, ''= 1'', ''= 2''), ''is not null'', ''is null'') end 
	, data_type = isnull(r.data_type, ''''), date_type = isnull(r.date_type, '''')
	from ' + @FiltersInForce + ' F
	inner join epacube.DATA_NAME_GROUP_REFS r on F.critdn = r.data_name_fk
	where r.virtual_where_clause is not null')

	Exec('
	Delete From TempTbls.tmp_FILTER_' + @UserID + ' where isnull(OpExclusion, '''') = ''ignore'';
	Delete from ' + @FiltersInForce + ' where isnull(OptionSetting, '''') = ''ignore''')

	Exec('
	If (select count(*) from temptbls.tmp_Filters_InForce_gstone@epacube_com where CritDN = 500010 and CritValue in (''1'')) > 0
	Begin
		Delete from TempTbls.tmp_FILTER_' + @UserID + ' where CritDN in (select data_name_fk from epacube.ui_filter where options is not null);
		Delete from ' + @FiltersInForce + ' where CritDN in (select data_name_fk from epacube.ui_filter where options is not null)
	End
	')

	Exec('
	delete from [precision].[ui_user_config] where ui_name = ''filter_comp'' and username = ''' + @UserID + '''
	Insert into [precision].[ui_user_config]
	(username, ui_name, field_name, value, Exclusion, date_value, create_timestamp, label)
	select ''' + @UserID + ''', ''filter_comp'' ''ui_name'', FIF.CritDN ''field_name'', isnull(FIF.CritValue, FIF.OptionSetting) ''value'', FIF.OpExclusion, FIF.OptionValue, getdate()
		, isnull(Case when isnull(dv.description, '''') <> '''' then dv.value + '': '' + dv.description else dv.value end, ein.value) ''label''
	from '+ @FiltersInForce + ' FIF with (nolock)
	left join epacube.data_value dv with (nolock) on FIF.CritDN = dv.data_name_fk and FIF.CritValue = dv.value
	left join epacube.entity_identification ei with (nolock) on FIF.CritDN = ei.DATA_NAME_FK and isnull(FIF.CritValue, FIF.OptionSetting) = ei.value
	left join epacube.ENTITY_IDENT_NONUNIQUE ein with (nolock) on ei.entity_structure_fk = ein.entity_structure_fk and ei.entity_data_name_fk = ein.ENTITY_DATA_NAME_FK and right(ein.data_name_fk, 3) = 112
	Union 
	Select  ''' + @UserID + ''', ''filter_comp'' ''ui_name'', P3.CritDN ''field_name'', P3.opexclusion ''value'', Null ''OpExclusion'', P3.critvalue ''date_value'', getdate()
		, isnull(Case when isnull(dv.description, '''') <> '''' then dv.value + '': '' + dv.description else dv.value end, ein.value) ''label''
	from #param3 P3 
	left join epacube.data_value dv with (nolock) on P3.CritDN = dv.data_name_fk and P3.CritValue = dv.value
	left join epacube.entity_identification ei with (nolock) on P3.CritDN = ei.DATA_NAME_FK and P3.CritValue = ei.value
	left join epacube.ENTITY_IDENT_NONUNIQUE ein with (nolock) on ei.entity_structure_fk = ein.entity_structure_fk and ei.entity_data_name_fk = ein.ENTITY_DATA_NAME_FK and right(ein.data_name_fk, 3) = 112
	where opexclusion = ''ignore'''
	)
