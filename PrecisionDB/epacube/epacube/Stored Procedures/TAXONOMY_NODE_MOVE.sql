﻿-- Copyright 2020 epaCUBE, Inc.
--
-- Procedure created by Neal Hollingsworth
--
-- Purpose:	Move taxonomy node JIRA (PRE-140)
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
CREATE PROCEDURE [epacube].[TAXONOMY_NODE_MOVE]
	@taxonomy_node_id INT
	,@parent_taxonomy_node_id INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @l_exec_no BIGINT = 0;
	DECLARE @ls_stmt VARCHAR(1000);
	DECLARE @parent_taxonomy_node_id_original INT;

	--Initialize log
	SET @ls_stmt = 'Started execution of epacube.TAXONOMY_NODE_MOVE.'
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
		@exec_id = @l_exec_no OUTPUT;

	BEGIN TRY

		SELECT @parent_taxonomy_node_id_original = PARENT_TAXONOMY_NODE_FK
		FROM epacube.TAXONOMY_NODE
		WHERE TAXONOMY_NODE_ID = @taxonomy_node_id;
		
		BEGIN TRANSACTION;

			UPDATE epacube.TAXONOMY_NODE
			SET PARENT_TAXONOMY_NODE_FK = @parent_taxonomy_node_id
			WHERE TAXONOMY_NODE_ID = @taxonomy_node_id

		COMMIT TRANSACTION;
		
		SELECT TN.TAXONOMY_NODE_ID
			, DV.VALUE NODE_NAME
			, @parent_taxonomy_node_id PARENT_TAXONOMY_NODE_FK
			, @parent_taxonomy_node_id_original PARENT_TAXONOMY_NODE_FK_ORIGINAL
			, (
				SELECT COUNT(*)
				FROM epacube.TAXONOMY_PRODUCT
				WHERE TAXONOMY_NODE_FK = TN.TAXONOMY_NODE_ID) PRODUCT_COUNT
			, epacube.getTaxonomyNodeSubProductCount(TN.TAXONOMY_NODE_ID) SUB_PRODUCT_COUNT
			, JSON_QUERY(epacube.getTaxonomy(TAXONOMY_NODE_ID)) CHILDREN
		FROM epacube.TAXONOMY_NODE TN
			INNER JOIN epacube.DATA_VALUE DV
				ON TN.DATA_VALUE_FK = DV.DATA_VALUE_ID
		WHERE TN.TAXONOMY_NODE_ID = @taxonomy_node_id
		FOR JSON PATH, WITHOUT_ARRAY_WRAPPER

		SET @ls_stmt = 'Finished execution of epacube.TAXONOMY_NODE_MOVE.'; 
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

	END TRY

	BEGIN CATCH

		IF (@@TRANCOUNT > 0)
			BEGIN
				ROLLBACK TRANSACTION;
				PRINT 'Error detected, all changes reversed';
			END 

		SET @ls_stmt = 'Execution of epacube.TAXONOMY_NODE_MOVE has failed with ErrorNumber: ' + CAST(ERROR_NUMBER() AS CHAR(4)) + ' ErrorSeverity: ' + CAST(ERROR_SEVERITY() AS CHAR(2))+ ' ErrorState: ' + CAST(ERROR_STATE() AS CHAR(1)) + ' ErrorProcedure: ' + ERROR_PROCEDURE() + ' ErrorLine: ' + CAST(ERROR_LINE() AS VARCHAR(20)) + ' ErrorMessage: ' + ERROR_MESSAGE();
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @ls_stmt;

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
	END CATCH

END