﻿
CREATE procedure [epacube].[UI_COMPONENT_EXEC_SQL_LIB] @t_results varchar(64), @UserID varchar(64), @UI_Comp_Master_FK Varchar(Max) = 0, @SQL_Library_fk int =  0, @Parameters varchar(max) = '', @UI_RETURN_SQL int = 0

as 
		------/***********************************************************
		------	Written By  : Gary Stone
		------	February, 2017
		------	Created 4/13/2019 eo enable components from SQL Library
		--------*/----------------------------------------------------------
	--Declare @t_results varchar(64)= 'temptbls.tmp_results_'
	--Declare @UserID varchar(64) = 'gstone@epacube.com'
	--Declare @ui_comp_master_fk Varchar(16) = 31562
	--Declare @SQL_Library_fk int = 4
	--Declare @UI_RETURN_SQL int = 0
	--Declare @Parameters varchar(max) = ''

Set NoCount ON

	Set @UserID = Replace(@UserID, '.', '_')
	Declare @TblFilters varchar(Max) = '[TempTbls].[tmp_Filters_InForce_' + @UserID + ']'
	--Declare @t_results varchar(Max) = '[TempTbls].[tmp_results_' + @ui_comp_master_fk + '_' + @UserID + ']' 
	Declare @TblProd varchar(Max) = '[TempTbls].[tmp_PROD_' + @UserID + ']' 
	Declare @TblCust varchar(Max) = '[TempTbls].[tmp_CUST_' + @UserID + ']' 
	Declare @TblZone varchar(Max) = '[TempTbls].[tmp_ZONE_' + @UserID + ']' 

	Declare @ExecSQL varchar(Max)

	Declare @DN as varchar(64)
	Declare @Nm as varchar(64)
	Declare @Val as varchar(64)
	Declare @DataType int
	Declare @Sql varchar(Max)
	Declare @SqlDN varchar(Max)
	Declare @SqlCol varchar(Max)
	Declare @Sql_hdr varchar(Max)
	Declare @Editable varchar(Max)

	--Declare @top_n_rows varchar(16) = 'top ' + isnull((select cast(top_n_rows as varchar(16)) from epacube.ui_comp_master where UI_COMP_MASTER_ID = @UI_Comp_Master_FK), '5000')
	
	Exec('If Object_ID(''' + @t_results + ''') is not null drop table ' + @t_results)

	Set @SQL = ''
	Set @SQLCol = ''
	Set @SQLDN = 'Select '
	Set @Sql_hdr = 'Select '

	Set @ExecSQL = Replace(Replace(Replace(Replace(Replace((Select SQL_Code from precision.sql_library where sql_library_id = @SQL_Library_fk)
					, '@TblFilters', @TblFilters)
					, '@ui_comp_master_fk', @ui_comp_master_fk)
					, '@TblProd', @TblProd)
					, '@TblCust', @TblCust)
					, '@TblZone', @TblZone)

	If @ExecSQL like 'Declare % Cursor for%'
	Begin
		Set @Editable = cast((select isnull(is_control, 0) from epacube.ui_comp_master where UI_COMP_MASTER_ID = @ui_comp_master_fk) as varchar(8))

		exec(@ExecSQL)

		OPEN Results;
		FETCH NEXT FROM Results INTO @DN, @Nm, @Val, @DataType
		WHILE @@FETCH_STATUS = 0

			Begin--Fetch
			
			Set @SqlDN = @SqlDN + @DN + ' ''DN_' + @DN + ''', '

			Set @SqlCol = @SqlCol + '''' + @val + '''' + ' ''' + @Nm + ''', '

			Set @Sql_hdr = @Sql_hdr + @DN + ' ''Data_Name_ID'', ''' + @Nm + ''' ''Column_name'', 1 ''Display_Column'', ' + @Editable + ' ''editable'' Union Select '

			FETCH NEXT FROM Results INTO @DN, @Nm, @Val, @DataType
			End--Fetch
		
		Close Results;
		Deallocate Results;

		Set @SqlCol = Left(@SqlCol, Len(@SqlCol) - 1)
		Set @Sql_hdr = Left(@Sql_hdr, Len(@Sql_hdr) - 13)		
		
		Set @Sql = @SqlDN + @SqlCol + ' into ' + @t_results

		Exec(@SQL)
		--Exec(@Sql_hdr)
		If @UI_RETURN_SQL = 0
			Exec(@Sql_hdr)
		else
			Print(@ExecSQL)
			print(@Sql_hdr)

	End
	else 
	Begin
		
		
--		Set @ExecSQL = Replace(Replace(@ExecSQL, '@Initialize', 'Select ' + @top_n_rows + ' * into ' + @t_results + ' from'), '@ParamReplace', @parameters)
		Set @ExecSQL = Replace(Replace(@ExecSQL, '@Initialize', 'Select * into ' + @t_results + ' from'), '@ParamReplace', @parameters)

		exec(@ExecSQL)

		Set @Sql_hdr = '
		Select dn.data_name_id, isc.column_name
		--, case when ucuf.ui_comp_user_fields_id is not null then 1 when dn.data_name_id is null or isc.column_name in (''Data_Level_CR_FK'', ''ROW_STATUS'', ''RESULTS_ID'') then 0 else 1 end ''Display_Column''
		, case when ucuf.ui_comp_user_fields_id is not null then 1 --when dn.data_name_id is null 
			when isc.column_name like ''%_fk'' or isc.column_name in (''Data_Level_CR_FK'', ''ROW_STATUS'', ''RESULTS_ID'', ''DRANK'', ''ind_discontinued'', ''t_prod_id'') then 0 else 1 end ''Display_Column''
		, case when (select isnull(is_control, 0) from epacube.epacube.ui_comp_master where ui_comp_master_id = ' + @UI_Comp_Master_FK + ') = 1 then 1 else isnull(isnull(ucuf.editable, uc.editable), 0) end ''editable''
		, ucuf.reference_data_name_fk
		, ucuf.reference_column
		, ucuf.stored_proc
		, ucuf.ui_config
		, case	when isc.column_name = ''DESCRIPTION_LINE_1'' then ''[{"condition":"var1!=null","var1":"ind_discontinued","color":"red","text-decoration":"underline"}]'' 
				else null end ''ui_config_conditional''
		from INFORMATION_SCHEMA.COLUMNS isc with (nolock)
		left join epacube.data_name dn with (nolock) on isc.column_name = dn.name
		left join epacube.UI_COMP uc with (nolock) on dn.data_name_id = uc.data_name_fk and uc.ui_comp_master_fk = ' + @UI_Comp_Master_FK + '
		left join epacube.ui_comp_user_fields ucuf with (nolock) on isc.COLUMN_NAME = ucuf.column_name and ucuf.ui_comp_master_fk = ' + @UI_Comp_Master_FK + '
		where isc.table_schema = ''TempTbls'' and isc.table_name = ''tmp_results_' + @ui_comp_master_fk + '_' + @UserID + '''
		and isc.column_name not like ''DN_%''
		order by isc.ordinal_position'

		If @UI_RETURN_SQL = 0
			exec(@Sql_hdr)
		else
			If len(@ExecSQL) < 8001
				Print(@ExecSQL)
			else
			begin
				Print(left(@ExecSQL, 8000))
				print(substring(@ExecSQL, 8001, 8000))
				print(substring(@ExecSQL, 16001, 8000))
			End

	End
