﻿







CREATE PROCEDURE [epacube].[UTIL_IMPORT_JOB_REMOVE_ALL_IMP_STG_ED] 
        @IN_JOB_FK BIGINT
    AS
BEGIN     

set @IN_JOB_FK = (select distinct Job_fk from import.import_record_data where job_fk <1488)

 
--------------   EVENTS
DELETE 
FROM synchronizer.event_data_errors
where EVENT_FK IN (select event_id from synchronizer.event_data WITH (NOLOCK)
where import_job_fk = @in_job_fk)


DELETE 
FROM synchronizer.event_data_rules
where EVENT_FK IN (select event_id from synchronizer.event_data WITH (NOLOCK)
where import_job_fk = @in_job_fk)


DELETE
FROM synchronizer.EVENT_DATA 
WHERE IMPORT_JOB_FK = @in_job_fk

----------------------------------------------------

 

------------- STAGE DATA


DELETE 
FROM cleanser.SEARCH_JOB
WHERE SEARCH_JOB_ID = @in_job_fk

DELETE 
FROM cleanser.SEARCH_JOB_DATA_NAME
WHERE SEARCH_JOB_FK = @in_job_fk

DELETE
FROM cleanser.SEARCH_JOB_FROM_TO_MATCH
WHERE SEARCH_JOB_FK = @in_job_fk

DELETE
FROM cleanser.SEARCH_JOB_RESULTS_BEST
WHERE SEARCH_JOB_FK = @in_job_fk


DELETE
FROM cleanser.SEARCH_JOB_RESULTS_IDENT
WHERE SEARCH_JOB_FK = @in_job_fk


DELETE
FROM cleanser.SEARCH_JOB_RESULTS_TASK
WHERE SEARCH_JOB_FK = @in_job_fk


DELETE
FROM cleanser.SEARCH_JOB_RESULTS_TASK_WORK
WHERE SEARCH_JOB_FK = @in_job_fk


DELETE
FROM cleanser.SEARCH_JOB_STRING
WHERE SEARCH_JOB_FK = @in_job_fk


DELETE
FROM cleanser.SEARCH_JOB_STRING_WORK
WHERE SEARCH_JOB_FK = @in_job_fk


DELETE 
FROM stage.STG_RECORD_IDENT
WHERE JOB_FK = @in_job_fk

DELETE
FROM stage.STG_RECORD_ENTITY_IDENT
WHERE JOB_FK = @in_job_fk


DELETE
FROM stage.STG_RECORD_ENTITY
WHERE JOB_FK = @in_job_fk


DELETE
FROM stage.STG_RECORD_DATA
WHERE JOB_FK = @in_job_fk


DELETE
FROM stage.STG_RECORD_ERRORS
WHERE JOB_FK = @in_job_fk


DELETE
FROM stage.STG_RECORD_STRUCTURE 
WHERE STG_RECORD_FK IN 
  ( SELECT STG_RECORD_ID
	FROM stage.STG_RECORD WITH (NOLOCK)
	WHERE JOB_FK = @in_job_fk )


DELETE
FROM stage.STG_RECORD
WHERE JOB_FK = @in_job_fk


------------- IMPORT DATA

		DELETE
		FROM import.IMPORT_RECORD_DATA
		WHERE JOB_FK = @in_job_fk

		DELETE
		FROM import.IMPORT_RECORD_NAME_VALUE
		WHERE JOB_FK = @in_job_fk

		DELETE
		FROM import.IMPORT_RECORD
		WHERE JOB_FK = @in_job_fk

------------- JOB DATA

		delete from common.job_execution 
		where job_fk = @in_job_fk

		delete from common.job
		where job_id = @in_job_fk



END














