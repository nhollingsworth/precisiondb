﻿










-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Remove old entity specific data when product changes MFR code
--          Called at end of import process so not to leave duplicate data.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        08/21/2014   Initial Version 
-- CV		 01/29/2015   Fixed copy mistake product text table all other tables.



CREATE PROCEDURE [epacube].[UTIL_CLEANUP_AFTER_ENTITY_CHANGE] 
    
        
AS BEGIN 

-----------------------------------------------------------------------------------------------
--                   *********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
--     DECLARE @l_sysdate                DATETIME
--     DECLARE @v_sysdate                DATETIME
       DECLARE @V_START_TIMESTAMP  DATETIME
     DECLARE @V_END_TIMESTAMP    DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
        DECLARE @v_exception_rows        bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
       DECLARE @time_after datetime;
       DECLARE @elapsed_time numeric(10,4);
       DECLARE @rows_affected varchar(max);
       DECLARE @rows_dml bigint;
       DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int     
       
          

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of Epacube.UTIL_CLEANUP_AFTER_ENTITY_CHANGE', 
                                                                                  @exec_id = @l_exec_no OUTPUT,
                                                                                  @epa_job_id = NULL;

--     SET @l_sysdate = GETDATE()
SET @V_START_TIMESTAMP = GETDATE()

----sanity check on datanames getting selected.

------select ds.TABLE_NAME, dn.DATA_NAME_ID,dn.name, ds.* from epacube.data_name dn with (nolock)
------inner join epacube.DATA_SET ds with (nolock)
------on (ds.DATA_SET_ID = dn.DATA_SET_FK
--------and dn.ORG_DATA_NAME_FK is NULL
------and ds.entity_structure_ec_cr_fk = 10103)
------where dn.ENTITY_DATA_NAME_FK = 143000
------and dn.RECORD_STATUS_CR_FK = 1
------and ds.TABLE_NAME ='PRODUCT_ASSOCIATION'



--drop temp table if it exists
	IF object_id('tempdb..#TS_PRODUCT_ASSOCS') is not null
	   drop table #TS_PRODUCT_ASSOCS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_PRODUCT_ASSOCS(
    [RECORD_NO] [int] IDENTITY(1,1) NOT NULL,
	[PRODUCT_STRUCTURE_FK]	BIGINT,
	[ENTITY_STRUCTURE_FK] bigint
	)


INSERT INTO #TS_PRODUCT_ASSOCS(
                   PRODUCT_STRUCTURE_FK,
					ENTITY_STRUCTURE_FK)
              ( 
				select distinct product_structure_fk, entity_structure_fk 
				from epacube.PRODUCT_ASSOCIATION with (nolock)
				where DATA_NAME_FK in (
				select  dn.DATA_NAME_ID from epacube.data_name dn with (nolock)
				inner join epacube.DATA_SET ds with (nolock)
				on (ds.DATA_SET_ID = dn.DATA_SET_FK
				--and dn.ORG_DATA_NAME_FK is NULL
				and ds.entity_structure_ec_cr_fk = 10103)
				where dn.ENTITY_DATA_NAME_FK = 143000
				and dn.RECORD_STATUS_CR_FK = 1
				and ds.TABLE_NAME ='PRODUCT_ASSOCIATION'))
				


-----------------------------------------------------------------------------------
--- PRODUCT CATEGORY
-----------------------------------------------------------------------------------

update pc
set record_status_cr_fk = 2 
from epacube.PRODUCT_CATEGORY pc
inner join #TS_PRODUCT_ASSOCS temp
on pc.PRODUCT_STRUCTURE_FK = temp.product_structure_fk
and DATA_NAME_FK in(
select dn.DATA_NAME_ID
 from epacube.data_name dn with (nolock)
inner join epacube.DATA_SET ds with (nolock)
on (ds.DATA_SET_ID = dn.DATA_SET_FK
and dn.ORG_DATA_NAME_FK is NULL
and ds.entity_structure_ec_cr_fk = 10103)
where dn.ENTITY_DATA_NAME_FK = 143000
and dn.RECORD_STATUS_CR_FK = 1
and ds.TABLE_NAME ='PRODUCT_CATEGORY')
and pc.entity_structure_fk not in (select entity_structure_fk from #TS_PRODUCT_ASSOCS
where PRODUCT_STRUCTURE_FK = pc.PRODUCT_STRUCTURE_FK) 


delete  
from epacube.PRODUCT_category
where record_status_cr_fk = 2

EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Category delete', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;

---------------------------------------------------------------------------------------
------- PRODUCT ATTRIBUTE
---------------------------------------------------------------------------------------
update pa
set record_status_cr_fk = 2 
from epacube.PRODUCT_ATTRIBUTE pa
inner join #TS_PRODUCT_ASSOCS temp
on pa.PRODUCT_STRUCTURE_FK = temp.product_structure_fk
and DATA_NAME_FK in(
select dn.DATA_NAME_ID
 from epacube.data_name dn with (nolock)
inner join epacube.DATA_SET ds with (nolock)
on (ds.DATA_SET_ID = dn.DATA_SET_FK
and dn.ORG_DATA_NAME_FK is NULL
and ds.entity_structure_ec_cr_fk = 10103)
where dn.ENTITY_DATA_NAME_FK = 143000
and dn.RECORD_STATUS_CR_FK = 1
and ds.TABLE_NAME ='PRODUCT_ATTRIBUTE')
and pa.entity_structure_fk not in (select entity_structure_fk from #TS_PRODUCT_ASSOCS
where PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK)

delete  
from epacube.PRODUCT_attribute
where record_status_cr_fk = 2

EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Attribute delete', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;

---------------------------------------------------------------------------------------
------Product Description
---------------------------------------------------------------------------------------
update pt
set record_status_cr_fk = 2 
from epacube.PRODUCT_DESCRIPTION pt
inner join #TS_PRODUCT_ASSOCS temp
on pt.PRODUCT_STRUCTURE_FK = temp.product_structure_fk
and DATA_NAME_FK in(
select dn.DATA_NAME_ID
 from epacube.data_name dn with (nolock)
inner join epacube.DATA_SET ds with (nolock)
on (ds.DATA_SET_ID = dn.DATA_SET_FK
and dn.ORG_DATA_NAME_FK is NULL
and ds.entity_structure_ec_cr_fk = 10103)
where dn.ENTITY_DATA_NAME_FK = 143000
and dn.RECORD_STATUS_CR_FK = 1
and ds.TABLE_NAME ='PRODUCT_DESCRIPTION')
and pt.entity_structure_fk not in (select entity_structure_fk from #TS_PRODUCT_ASSOCS
where PRODUCT_STRUCTURE_FK = pt.PRODUCT_STRUCTURE_FK)


delete  
from epacube.PRODUCT_description
where record_status_cr_fk = 2


EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Description delete', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;


---------------------------------------------------------------------------------------
------Product text
---------------------------------------------------------------------------------------

update pt
set record_status_cr_fk = 2 
from epacube.PRODUCT_TEXT pt
inner join #TS_PRODUCT_ASSOCS temp
on pt.PRODUCT_STRUCTURE_FK = temp.product_structure_fk
and DATA_NAME_FK in(
select dn.DATA_NAME_ID
 from epacube.data_name dn with (nolock)
inner join epacube.DATA_SET ds with (nolock)
on (ds.DATA_SET_ID = dn.DATA_SET_FK
and dn.ORG_DATA_NAME_FK is NULL
and ds.entity_structure_ec_cr_fk = 10103)
where dn.ENTITY_DATA_NAME_FK = 143000
and dn.RECORD_STATUS_CR_FK = 1
and ds.TABLE_NAME ='PRODUCT_TEXT')
and pt.entity_structure_fk not in (select entity_structure_fk from #TS_PRODUCT_ASSOCS
where PRODUCT_STRUCTURE_FK = pt.PRODUCT_STRUCTURE_FK) 

delete  
from epacube.PRODUCT_TEXT
where record_status_cr_fk = 2

EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Text delete', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;
---------------------------------------------------------------------------------------
------Product Status
---------------------------------------------------------------------------------------
update pt
set record_status_cr_fk = 2 
from epacube.PRODUCT_STATUS pt
inner join #TS_PRODUCT_ASSOCS temp
on pt.PRODUCT_STRUCTURE_FK = temp.product_structure_fk
and DATA_NAME_FK in(
select dn.DATA_NAME_ID
 from epacube.data_name dn with (nolock)
inner join epacube.DATA_SET ds with (nolock)
on (ds.DATA_SET_ID = dn.DATA_SET_FK
and dn.ORG_DATA_NAME_FK is NULL
and ds.entity_structure_ec_cr_fk = 10103)
where dn.ENTITY_DATA_NAME_FK = 143000
and dn.RECORD_STATUS_CR_FK = 1
and ds.TABLE_NAME ='PRODUCT_STATUS')
and pt.entity_structure_fk not in (select entity_structure_fk from #TS_PRODUCT_ASSOCS
where PRODUCT_STRUCTURE_FK = pt.PRODUCT_STRUCTURE_FK)

delete  
from epacube.PRODUCT_STATUS
where record_status_cr_fk = 2

EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Status delete', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;

---------------------------------------------------------------------------------------
------Product UOM Class
---------------------------------------------------------------------------------------
update pt
set record_status_cr_fk = 2 
from epacube.PRODUCT_UOM_CLASS pt
inner join #TS_PRODUCT_ASSOCS temp
on pt.PRODUCT_STRUCTURE_FK = temp.product_structure_fk
and DATA_NAME_FK in(
select dn.DATA_NAME_ID
 from epacube.data_name dn with (nolock)
inner join epacube.DATA_SET ds with (nolock)
on (ds.DATA_SET_ID = dn.DATA_SET_FK
and dn.ORG_DATA_NAME_FK is NULL
and ds.entity_structure_ec_cr_fk = 10103)
where dn.ENTITY_DATA_NAME_FK = 143000
and dn.RECORD_STATUS_CR_FK = 1
and ds.TABLE_NAME ='PRODUCT_UOM_CLASS')
and pt.entity_structure_fk not in (select entity_structure_fk from #TS_PRODUCT_ASSOCS
where PRODUCT_STRUCTURE_FK = pt.PRODUCT_STRUCTURE_FK)

delete  
from epacube.PRODUCT_UOM_CLASS
where record_status_cr_fk = 2

EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product UOM Class delete', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;
--------------------------------------------------------------------------------------
------- PRODUCT_IDENT_MULT
---------------------------------------------------------------------------------------
update pt
set record_status_cr_fk = 2 
from epacube.PRODUCT_IDENT_MULT pt
inner join #TS_PRODUCT_ASSOCS temp
on pt.PRODUCT_STRUCTURE_FK = temp.product_structure_fk
and DATA_NAME_FK in(
select dn.DATA_NAME_ID
 from epacube.data_name dn with (nolock)
inner join epacube.DATA_SET ds with (nolock)
on (ds.DATA_SET_ID = dn.DATA_SET_FK
and dn.ORG_DATA_NAME_FK is NULL
and ds.entity_structure_ec_cr_fk = 10103)
where dn.ENTITY_DATA_NAME_FK = 143000
and dn.RECORD_STATUS_CR_FK = 1
and ds.TABLE_NAME ='PRODUCT_IDENT_MULT')
and pt.entity_structure_fk not in (select entity_structure_fk from #TS_PRODUCT_ASSOCS
where PRODUCT_STRUCTURE_FK = pt.PRODUCT_STRUCTURE_FK)


delete  
from epacube.PRODUCT_IDENT_MULT
where record_status_cr_fk = 2

EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Ident Mult delete', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;
---------------------------------------------------------------------------------------
------- PRODUCT_IDENT_NONUNIQUE
---------------------------------------------------------------------------------------
 update pt
set record_status_cr_fk = 2 
from epacube.PRODUCT_IDENT_NONUNIQUE pt
inner join #TS_PRODUCT_ASSOCS temp
on pt.PRODUCT_STRUCTURE_FK = temp.product_structure_fk
and DATA_NAME_FK in(
select dn.DATA_NAME_ID
 from epacube.data_name dn with (nolock)
inner join epacube.DATA_SET ds with (nolock)
on (ds.DATA_SET_ID = dn.DATA_SET_FK
and dn.ORG_DATA_NAME_FK is NULL
and ds.entity_structure_ec_cr_fk = 10103)
where dn.ENTITY_DATA_NAME_FK = 143000
and dn.RECORD_STATUS_CR_FK = 1
and ds.TABLE_NAME ='PRODUCT_IDENT_NONUNIQUE')
and pt.entity_structure_fk not in (select entity_structure_fk from #TS_PRODUCT_ASSOCS
where PRODUCT_STRUCTURE_FK = pt.PRODUCT_STRUCTURE_FK)

delete  
from epacube.PRODUCT_IDENT_NONUNIQUE
where record_status_cr_fk = 2

EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Ident NonUnique delete', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;
---------------------------------------------------------------------------------------
------- PRODUCT IDENTIFICATION
---------------------------------------------------------------------------------------
update pt
set record_status_cr_fk = 2 
from epacube.PRODUCT_IDENTIFICATION pt
inner join #TS_PRODUCT_ASSOCS temp
on pt.PRODUCT_STRUCTURE_FK = temp.product_structure_fk
and DATA_NAME_FK in(
select dn.DATA_NAME_ID
 from epacube.data_name dn with (nolock)
inner join epacube.DATA_SET ds with (nolock)
on (ds.DATA_SET_ID = dn.DATA_SET_FK
and dn.ORG_DATA_NAME_FK is NULL
and ds.entity_structure_ec_cr_fk = 10103)
where dn.ENTITY_DATA_NAME_FK = 143000
and dn.RECORD_STATUS_CR_FK = 1
and ds.TABLE_NAME ='PRODUCT_IDENTIFICATION')
and pt.entity_structure_fk not in (select entity_structure_fk from #TS_PRODUCT_ASSOCS
where PRODUCT_STRUCTURE_FK = pt.PRODUCT_STRUCTURE_FK)

delete  
from epacube.PRODUCT_IDENTIFICATION
where record_status_cr_fk = 2

EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Ident update', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;
-----------------------------------------------------------------------------------------------------
 EXEC exec_monitor.start_monitor_sp @exec_desc = 'finish execution of Epacube.UTIL_CLEANUP_AFTER_ENTITY_CHANGE', 
                                                                                  @exec_id = @l_exec_no OUTPUT,
                                                                                  @epa_job_id = NULL;

END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
         DECLARE @ErrorSeverity INT;
         DECLARE @ErrorState INT;

              SELECT 
                     @ErrorMessage = 'Execution of epacube.utl_cleanup_after_entity_change has failed ' + ERROR_MESSAGE(),
                     @ErrorSeverity = ERROR_SEVERITY(),
                     @ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = NULL;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
                               @ErrorSeverity, -- Severity.
                               @ErrorState -- State.
                               );
   END CATCH;


END


































