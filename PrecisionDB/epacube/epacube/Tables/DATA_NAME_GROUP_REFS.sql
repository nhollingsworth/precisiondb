﻿CREATE TABLE [epacube].[DATA_NAME_GROUP_REFS] (
    [DATA_NAME_GROUP_REFS_ID]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [Table_Schema]              VARCHAR (64)  NULL,
    [Table_Name]                VARCHAR (64)  NULL,
    [QTbl]                      VARCHAR (128) NULL,
    [Data_Name_FK]              BIGINT        NULL,
    [Data_Level_CR_FK]          INT           NULL,
    [Primary_Column]            VARCHAR (64)  NULL,
    [Local_Column]              VARCHAR (64)  NULL,
    [Entity_Class_CR_FK]        INT           NULL,
    [DATA_COLUMN]               VARCHAR (MAX) NULL,
    [ALIAS]                     VARCHAR (16)  NULL,
    [ALIAS_DV]                  VARCHAR (16)  NULL,
    [ZONE_TYPE_CR_FK]           INT           NULL,
    [Create_Timestamp]          DATETIME      NULL,
    [DN_Name]                   VARCHAR (96)  NULL,
    [DN_Label]                  VARCHAR (64)  NULL,
    [EVENT_TYPE_CR_FK]          INT           NULL,
    [Entity_Structure_EC_CR_FK] BIGINT        NULL,
    [Product_Structure_Column]  VARCHAR (64)  NULL,
    [Org_Structure_Column]      VARCHAR (64)  NULL,
    [Customer_Structure_Column] VARCHAR (64)  NULL,
    [Vendor_Structure_Column]   VARCHAR (64)  NULL,
    [Zone_Structure_Column]     VARCHAR (64)  NULL,
    [ind_PS_FK]                 INT           NULL,
    [ind_ES_FK]                 INT           NULL,
    [ind_OES_FK]                INT           NULL,
    [ind_CES_FK]                INT           NULL,
    [ind_VES_FK]                INT           NULL,
    [ind_ZES_FK]                INT           NULL,
    [ind_COMP_ES_FK]            INT           NULL,
    [ind_ASSOC_ES_FK]           INT           NULL,
    [ind_DL_CR_FK]              INT           NULL,
    [ind_PROD_DV]               INT           NULL,
    [ind_CUST_DV]               INT           NULL,
    [ind_DEPT_EDV]              INT           NULL,
    [ind_DV_LOOKUP]             INT           NULL,
    [ind_EDV_LOOKUP]            INT           NULL,
    [ind_REC_INGREDIENT_XREF]   INT           NULL,
    [ind_CHILD_PS_FK]           INT           NULL,
    [ind_PARENT_ES_FK]          INT           NULL,
    [JoinSQL]                   VARCHAR (MAX) NULL,
    [VirtualActionType]         VARCHAR (64)  NULL,
    [VirtualActionType_CR_FK]   INT           NULL,
    [VirtualColumnCheck]        VARCHAR (MAX) NULL,
    [V_Ref_Data_Name_FK]        VARCHAR (64)  NULL,
    [NullColumn]                INT           NULL,
    [Precedence]                INT           NULL,
    [Virtual_Where_Clause]      VARCHAR (MAX) NULL,
    [Data_Type]                 VARCHAR (64)  NULL,
    [Date_Type]                 VARCHAR (32)  NULL,
    [Attribute_Parent_DN_FK]    BIGINT        NULL,
    CONSTRAINT [PK_DATA_NAME_GROUP_REFS] PRIMARY KEY CLUSTERED ([DATA_NAME_GROUP_REFS_ID] ASC) WITH (FILLFACTOR = 80)
);




GO
CREATE NONCLUSTERED INDEX [IDX_DNGR]
    ON [epacube].[DATA_NAME_GROUP_REFS]([Data_Name_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

