﻿CREATE TABLE [epacube].[TAXONOMY_NODE_ATTRIBUTE_DV] (
    [TAXONOMY_NODE_ATTRIBUTE_DV_ID] INT      IDENTITY (1000, 1) NOT NULL,
    [TAXONOMY_NODE_ATTRIBUTE_FK]    INT      NULL,
    [DATA_VALUE_FK]                 BIGINT   NULL,
    [RECORD_STATUS_CR_FK]           INT      CONSTRAINT [DF_TAX_ATTR_DV_DF__7EF6D905] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]              DATETIME CONSTRAINT [DF_TAX_ATTR_DV_DF__7FEAFD3E] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]              DATETIME CONSTRAINT [DF_TAX_ATTR_DV_DF__00DF2177] DEFAULT (getdate()) NULL,
    [UPDATE_LOG_FK]                 BIGINT   NULL,
    [DISPLAY_SEQ]                   INT      NULL,
    CONSTRAINT [PK_TAX_ATTR_DV] PRIMARY KEY CLUSTERED ([TAXONOMY_NODE_ATTRIBUTE_DV_ID] ASC),
    CONSTRAINT [FK_TNADV_DVF_DV] FOREIGN KEY ([DATA_VALUE_FK]) REFERENCES [epacube].[DATA_VALUE] ([DATA_VALUE_ID]),
    CONSTRAINT [FK_TNADV_TNAF_TNA] FOREIGN KEY ([TAXONOMY_NODE_ATTRIBUTE_FK]) REFERENCES [epacube].[TAXONOMY_NODE_ATTRIBUTE] ([TAXONOMY_NODE_ATTRIBUTE_ID])
);

