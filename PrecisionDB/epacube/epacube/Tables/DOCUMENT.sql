﻿CREATE TABLE [epacube].[DOCUMENT] (
    [document_id]         INT            IDENTITY (1, 1) NOT NULL,
    [doc_type_CR_FK]      INT            NULL,
    [entity_structure_fk] BIGINT         NULL,
    [user_fk]             BIGINT         NULL,
    [name]                NVARCHAR (64)  NOT NULL,
    [description]         NVARCHAR (256) NULL,
    [xml]                 NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_DOCUMENT] PRIMARY KEY CLUSTERED ([document_id] ASC),
    CONSTRAINT [FK_script_CODE_REF] FOREIGN KEY ([doc_type_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID])
);

