﻿CREATE TABLE [epacube].[DATA_SET] (
    [DATA_SET_ID]               INT            NOT NULL,
    [NAME]                      VARCHAR (64)   NULL,
    [TABLE_NAME]                VARCHAR (64)   NULL,
    [COLUMN_NAME]               VARCHAR (64)   NULL,
    [DATA_TYPE_CR_FK]           INT            NULL,
    [ENTITY_CLASS_CR_FK]        INT            NULL,
    [CORP_IND]                  SMALLINT       NULL,
    [ORG_IND]                   SMALLINT       NULL,
    [PROD_CHILD_IND]            SMALLINT       NULL,
    [ENTITY_STRUCTURE_EC_CR_FK] INT            NULL,
    [EVENT_TYPE_CR_FK]          INT            NULL,
    [EXTRACTOR]                 VARCHAR (2000) NULL,
    [RECORD_STATUS_CR_FK]       INT            CONSTRAINT [DF__DATA_SET__RECOR__0E6E26BF] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]          DATETIME       CONSTRAINT [DF__DATA_SET__CREAT__0F624AF8] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]          DATETIME       CONSTRAINT [DF__DATA_SET_UPDAT__10566F31] DEFAULT (getdate()) NULL,
    [UPDATE_LOG_FK]             BIGINT         NULL,
    CONSTRAINT [PK_DATA_SET] PRIMARY KEY CLUSTERED ([DATA_SET_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_DATA_SET_8_734169911__K1]
    ON [epacube].[DATA_SET]([DATA_SET_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_DATA_SET_8_734169911__K2_K1]
    ON [epacube].[DATA_SET]([NAME] ASC, [DATA_SET_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_DATA_SET_7_672265700__K1_K6_5]
    ON [epacube].[DATA_SET]([DATA_TYPE_CR_FK] ASC, [DATA_SET_ID] ASC, [ENTITY_CLASS_CR_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_DATA_SET_7_672265700__K1_5_6]
    ON [epacube].[DATA_SET]([DATA_TYPE_CR_FK] ASC, [ENTITY_CLASS_CR_FK] ASC, [DATA_SET_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_DATA_SET_8_1213559707__K4]
    ON [epacube].[DATA_SET]([COLUMN_NAME] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_DATA_SET_8_1213559707__K3_K1]
    ON [epacube].[DATA_SET]([TABLE_NAME] ASC, [DATA_SET_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_IDX_DS_TUNE_TN_DSI__ECCF]
    ON [epacube].[DATA_SET]([TABLE_NAME] ASC, [DATA_SET_ID] ASC, [ENTITY_CLASS_CR_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_TUNE_DS_DSI_ETC_OI]
    ON [epacube].[DATA_SET]([ENTITY_STRUCTURE_EC_CR_FK] ASC, [DATA_SET_ID] ASC, [EVENT_TYPE_CR_FK] ASC, [ORG_IND] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_TUNE_DATA_SET_DSI]
    ON [epacube].[DATA_SET]([COLUMN_NAME] ASC, [DATA_SET_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_DATA_SET_10_1213559707__K10_K1_K3]
    ON [epacube].[DATA_SET]([EVENT_TYPE_CR_FK] ASC, [DATA_SET_ID] ASC, [TABLE_NAME] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_TUNE_DS_DSI_ICI]
    ON [epacube].[DATA_SET]([CORP_IND] ASC, [DATA_SET_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_TUNE_DS_ECF_TN]
    ON [epacube].[DATA_SET]([ENTITY_CLASS_CR_FK] ASC, [TABLE_NAME] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_DATA_SET_10_1213559707__K1_K9_K8]
    ON [epacube].[DATA_SET]([DATA_SET_ID] ASC, [ENTITY_STRUCTURE_EC_CR_FK] ASC, [ORG_IND] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_DATA_SET_10_1213559707__K9]
    ON [epacube].[DATA_SET]([ENTITY_STRUCTURE_EC_CR_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_DATA_SET_10_1213559707__K1_K3_K5_K8_9]
    ON [epacube].[DATA_SET]([ENTITY_STRUCTURE_EC_CR_FK] ASC, [DATA_SET_ID] ASC, [TABLE_NAME] ASC, [DATA_TYPE_CR_FK] ASC, [ORG_IND] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [DS_20100925_EVENT_SUMMARY_DRILLDOWN_1]
    ON [epacube].[DATA_SET]([COLUMN_NAME] ASC, [DATA_SET_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [tune_event_import_sheet_results_DSI_04_12_1012]
    ON [epacube].[DATA_SET]([CORP_IND] ASC, [DATA_TYPE_CR_FK] ASC, [ENTITY_CLASS_CR_FK] ASC, [ENTITY_STRUCTURE_EC_CR_FK] ASC, [EVENT_TYPE_CR_FK] ASC, [ORG_IND] ASC, [TABLE_NAME] ASC, [DATA_SET_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

