﻿CREATE TABLE [epacube].[CONTROLS] (
    [Controls_ID]            INT           IDENTITY (1, 1) NOT NULL,
    [Company_ID]             INT           NULL,
    [CO_ENTITY_STRUCTURE_FK] BIGINT        NULL,
    [CONTROL_NAME]           VARCHAR (64)  NOT NULL,
    [CONTROL_VALUE]          VARCHAR (64)  NULL,
    [CONTROL_DATA_NAME_FK]   BIGINT        NULL,
    [CONTROL_PRECEDENCE]     INT           NULL,
    [CONTROL_CR_FK]          INT           NULL,
    [RULE_TYPE_CR_FK]        INT           NULL,
    [SCHEDULE1]              INT           NULL,
    [SCHEDULE2]              INT           NULL,
    [CONTROL2_DATA_NAME_FK]  BIGINT        NULL,
    [CONTROL2_CR_FK]         NCHAR (10)    NULL,
    [CONTROL_DESCRIPTION]    VARCHAR (256) NULL,
    [RESULT_DATA_NAME_FK]    BIGINT        NULL,
    [RULE_PRECEDENCE]        INT           NULL,
    [RULES_SEGMENT_FK_NAME]  VARCHAR (32)  NULL,
    [RECORD_STATUS_CR_FK]    INT           NULL,
    [ERP]                    VARCHAR (16)  NULL,
    [CONTROL_SOURCE]         VARCHAR (64)  NULL,
    CONSTRAINT [PK_CONTROLS] PRIMARY KEY NONCLUSTERED ([Controls_ID] ASC) WITH (FILLFACTOR = 80)
);




GO
CREATE CLUSTERED INDEX [ClusteredIndex-20160920-222920]
    ON [epacube].[controls]([CONTROL_NAME] ASC, [CONTROL_DATA_NAME_FK] ASC, [RESULT_DATA_NAME_FK] ASC);

