﻿CREATE TABLE [epacube].[PRODUCT_RECIPE_PROCEDURES] (
    [PRODUCT_RECIPE_PROCEDURES_ID] BIGINT        IDENTITY (1000, 1) NOT NULL,
    [PRODUCT_RECIPE_FK]            BIGINT        NULL,
    [DATA_NAME_FK]                 BIGINT        NULL,
    [SEQ]                          INT           NULL,
    [PROCEDURE_NUMBER]             INT           NULL,
    [VALUE]                        VARCHAR (256) NULL,
    [RECORD_STATUS_CR_FK]          INT           NULL,
    [CREATE_TIMESTAMP]             DATETIME      NULL,
    [UPDATE_TIMESTAMP]             DATETIME      NULL,
    [UPDATE_USER]                  VARCHAR (50)  NULL,
    CONSTRAINT [PK_PRODUCT_RECIPE_PROCEDURES] PRIMARY KEY CLUSTERED ([PRODUCT_RECIPE_PROCEDURES_ID] ASC) WITH (FILLFACTOR = 80)
);



