﻿CREATE TABLE [epacube].[UI_COMP_MASTER] (
    [UI_COMP_MASTER_ID]        BIGINT         IDENTITY (1000, 1) NOT NULL,
    [COMPONENT_NAME]           VARCHAR (64)   NOT NULL,
    [COMPONENT_DESCRIPTION]    VARCHAR (MAX)  NULL,
    [GRID]                     INT            NULL,
    [COLUMNS]                  INT            NULL,
    [UI_COMP_GROUPING_FK]      BIGINT         NULL,
    [TOP_N_ROWS]               INT            CONSTRAINT [DF_UI_COMP_MASTER_TOP_N_ROWS] DEFAULT ((100)) NULL,
    [RECORD_STATUS_CR_FK]      INT            CONSTRAINT [DF_UI_COMP_MASTER_RECORD_STATUS_CR_FK] DEFAULT ((3)) NOT NULL,
    [PIVOT_AGGREGATE]          VARCHAR (50)   NULL,
    [ORDERING]                 VARCHAR (1000) NULL,
    [Exclude_Forced_DNs]       INT            CONSTRAINT [DF_UI_COMP_MASTER_Exclude_Forced_DNs] DEFAULT ((0)) NULL,
    [COMP_CHECKBOX]            INT            NULL,
    [Is_Control]               INT            NULL,
    [SQL_Library_FK]           INT            NULL,
    [PROD_SEGMENT_FK_FROM_COL] VARCHAR (64)   NULL,
    [StorProc]                 VARCHAR (128)  NULL,
    [Parameters]               VARCHAR (MAX)  NULL,
    [ind_Save]                 INT            NULL,
    [COMPONENT_LABEL]          VARCHAR (64)   NULL,
    CONSTRAINT [PK_UI_COMP_MASTER] PRIMARY KEY CLUSTERED ([UI_COMP_MASTER_ID] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_nm]
    ON [epacube].[UI_COMP_MASTER]([COMPONENT_NAME] ASC) WITH (FILLFACTOR = 80);

