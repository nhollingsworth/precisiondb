﻿CREATE TABLE [epacube].[PROMOS_CONTROLS] (
    [PROMOS_CONTROLS_ID]              BIGINT       IDENTITY (1000, 1) NOT NULL,
    [DATA_NAME_FK]                    BIGINT       NOT NULL,
    [Data_Value_FK]                   BIGINT       NULL,
    [Value]                           VARCHAR (16) NULL,
    [Parent_Data_Value_FK]            BIGINT       NULL,
    [COST_ZONE_ENTITY_STRUCTURE_FK]   BIGINT       NULL,
    [RETAIL_ZONE_ENTITY_STRUCTURE_FK] BIGINT       NULL,
    [CUST_ENTITY_STRUCTURE_FK]        BIGINT       NULL,
    [PRECEDENCE]                      INT          NULL,
    [AD_GROUP_ACCT]                   VARCHAR (8)  NULL
);

