﻿CREATE TABLE [epacube].[UI_FILTER_HEADER] (
    [UI_FILTER_HEADER_ID]               INT          IDENTITY (1000, 1) NOT NULL,
    [FILTER_HEADER]                     VARCHAR (64) NOT NULL,
    [SEQUENCE]                          INT          NULL,
    [OPEN_EXPANDED]                     INT          NULL,
    [URL_LINK_FROM_UI_FILTER_HEADER_FK] INT          NULL,
    [PARENT_FILTER_HEADER_FK]           INT          NULL,
    [IS_CONTROL]                        INT          NULL,
    CONSTRAINT [PK_UI_FILTER_HEADER] PRIMARY KEY CLUSTERED ([UI_FILTER_HEADER_ID] ASC)
);

