﻿CREATE TABLE [epacube].[UI_COMP_LINK] (
    [UI_COMP_LINK_ID]           BIGINT        IDENTITY (10000, 1) NOT NULL,
    [UI_COMP_MASTER_FK]         BIGINT        NOT NULL,
    [LINK_TO_UI_COMP_MASTER_FK] BIGINT        NULL,
    [LINK_SQL]                  VARCHAR (MAX) NULL,
    [LINK_NAME]                 VARCHAR (64)  NOT NULL,
    [DATA_NAME_FK]              BIGINT        NULL,
    [ICON]                      VARCHAR (64)  NULL,
    [TOOLTIP]                   VARCHAR (64)  NULL,
    [RECORD_STATUS_CR_FK]       INT           NOT NULL,
    [LINK_TYPE_CR_FK]           INT           DEFAULT ((10520)) NULL,
    [ACTION_SQL]                VARCHAR (MAX) NULL,
    [TEMPLATE]                  VARCHAR (MAX) NULL,
    CONSTRAINT [PK_UI_COMP_LINK] PRIMARY KEY CLUSTERED ([UI_COMP_LINK_ID] ASC) WITH (FILLFACTOR = 80)
);



