﻿CREATE TABLE [epacube].[Nightly_Purge_Log] (
    [Product_structure_fk] BIGINT        NOT NULL,
    [Product_id]           VARCHAR (256) NULL,
    [Date_Purged]          DATETIME      NULL,
    [Comment]              VARCHAR (256) NULL
);

