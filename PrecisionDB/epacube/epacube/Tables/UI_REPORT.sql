﻿CREATE TABLE [epacube].[UI_REPORT] (
    [UI_REPORT_ID]     INT            NOT NULL,
    [NAME]             VARCHAR (80)   NOT NULL,
    [DESCRIPTION]      VARCHAR (512)  NULL,
    [owner_login]      VARCHAR (80)   NULL,
    [permission]       VARCHAR (80)   NULL,
    [fitlers_criteria] VARCHAR (256)  NULL,
    [VIEW_FK]          INT            NULL,
    [content]          NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([UI_REPORT_ID] ASC)
);

