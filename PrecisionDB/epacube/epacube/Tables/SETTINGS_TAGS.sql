﻿CREATE TABLE [epacube].[SETTINGS_TAGS] (
    [SETTINGS_TAGS_ID]           BIGINT          IDENTITY (1000, 1) NOT NULL,
    [DATA_LEVEL_CR_FK]           INT             NULL,
    [DATA_LEVEL]                 VARCHAR (64)    NULL,
    [PROD_DATA_VALUE_FK]         BIGINT          NULL,
    [CUST_DATA_VALUE_FK]         BIGINT          NULL,
    [RESULT_DATA_NAME_FK]        BIGINT          NULL,
    [ORG_ENTITY_STRUCTURE_FK]    BIGINT          NULL,
    [CUST_ENTITY_STRUCTURE_FK]   BIGINT          NULL,
    [VENDOR_ENTITY_STRUCTURE_FK] BIGINT          NULL,
    [ZONE_TYPE_CR_FK]            INT             NULL,
    [ZONE_ENTITY_STRUCTURE_FK]   BIGINT          NULL,
    [DATA_NAME_FK]               BIGINT          NOT NULL,
    [ATTRIBUTE_CHAR]             VARCHAR (256)   NULL,
    [ATTRIBUTE_NUMBER]           NUMERIC (18, 6) NULL,
    [ATTRIBUTE_DATE]             DATETIME        NULL,
    [ATTRIBUTE_Y_N]              CHAR (1)        NULL,
    [DATA_VALUE_FK]              BIGINT          NULL,
    [ENTITY_DATA_VALUE_FK]       BIGINT          NULL,
    [ATTRIBUTE_EVENT_DATA]       VARCHAR (256)   NULL,
    [PRECEDENCE]                 INT             NULL,
    [RECORD_STATUS_CR_FK]        INT             NULL,
    [UPDATE_TIMESTAMP]           DATETIME        CONSTRAINT [DF_SETTINGS_TAGS_UPDATE_TIMESTAMP] DEFAULT (getdate()) NOT NULL,
    [CREATE_TIMESTAMP]           DATETIME        CONSTRAINT [DF_SETTINGS_TAGS_CREATE_TIMESTAMP] DEFAULT (getdate()) NOT NULL,
    [UPDATE_USER]                VARCHAR (50)    NULL,
    [CREATE_USER]                VARCHAR (50)    NULL,
    CONSTRAINT [PK_SETTINGS_TAGS] PRIMARY KEY CLUSTERED ([SETTINGS_TAGS_ID] ASC) WITH (FILLFACTOR = 80)
);



