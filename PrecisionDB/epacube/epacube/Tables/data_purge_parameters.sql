﻿CREATE TABLE [epacube].[data_purge_parameters] (
    [purge_class]       VARCHAR (64)  NOT NULL,
    [data_name_fk]      BIGINT        NULL,
    [import_table_name] VARCHAR (128) NULL,
    [purge_sql]         VARCHAR (MAX) NULL,
    [number_days]       INT           NULL,
    [criteria]          VARCHAR (MAX) NULL,
    [minimum_instances] INT           NULL,
    [date_basis]        VARCHAR (64)  NULL,
    [org_ind]           INT           NULL
);

