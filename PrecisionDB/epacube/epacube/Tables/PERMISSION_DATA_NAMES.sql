﻿CREATE TABLE [epacube].[PERMISSION_DATA_NAMES] (
    [permission_data_name_id] INT IDENTITY (1000, 1) NOT NULL,
    [permissions_fk]          INT NOT NULL,
    [data_name_fk]            INT NOT NULL,
    [data_restriction_cr_fk]  INT CONSTRAINT [PERMISSION_DATA_NAMES_data_restriction_cr_fk_default] DEFAULT ((4)) NULL,
    CONSTRAINT [PK_permission_data_names] PRIMARY KEY CLUSTERED ([permission_data_name_id] ASC),
    CONSTRAINT [FK_permission_data_names_DATA_NAME] FOREIGN KEY ([data_name_fk]) REFERENCES [epacube].[DATA_NAME] ([DATA_NAME_ID]),
    CONSTRAINT [FK_permission_data_names_PERMISSIONS] FOREIGN KEY ([permissions_fk]) REFERENCES [epacube].[PERMISSIONS] ([PERMISSIONS_ID])
);

