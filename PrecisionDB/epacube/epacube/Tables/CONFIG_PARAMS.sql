﻿CREATE TABLE [epacube].[CONFIG_PARAMS] (
    [config_param_id] INT           NOT NULL,
    [scope]           VARCHAR (64)  CONSTRAINT [DF_config_params_category] DEFAULT ('SYSTEM') NOT NULL,
    [name]            VARCHAR (64)  NOT NULL,
    [value]           VARCHAR (MAX) NULL,
    CONSTRAINT [PK_CONFIG_PARAMS] PRIMARY KEY CLUSTERED ([config_param_id] ASC)
);

