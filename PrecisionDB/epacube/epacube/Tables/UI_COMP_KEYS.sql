﻿CREATE TABLE [epacube].[UI_COMP_KEYS] (
    [UI_COMP_KEYS_ID]            INT           IDENTITY (1, 1) NOT NULL,
    [UI_Comp_Master_FK]          BIGINT        NULL,
    [Data_Level_CR_FK]           VARCHAR (64)  NULL,
    [QTbl]                       VARCHAR (128) NULL,
    [Data_Column]                VARCHAR (64)  NULL,
    [Cust_Entity_Structure_FK]   VARCHAR (64)  NULL,
    [Product_Structure_FK]       VARCHAR (64)  NULL,
    [Vendor_Entity_Structure_FK] VARCHAR (64)  NULL,
    [Zone_Entity_Structure_FK]   VARCHAR (64)  NULL,
    [Prod_Data_Value_FK]         VARCHAR (64)  NULL,
    [Cust_Data_Value_FK]         VARCHAR (64)  NULL,
    [Dept_Entity_Data_Value_FK]  VARCHAR (64)  NULL,
    [Comp_Entity_Structure_FK]   VARCHAR (64)  NULL,
    CONSTRAINT [PK_UI_COMP_KEYS] PRIMARY KEY CLUSTERED ([UI_COMP_KEYS_ID] ASC) WITH (FILLFACTOR = 80)
);



