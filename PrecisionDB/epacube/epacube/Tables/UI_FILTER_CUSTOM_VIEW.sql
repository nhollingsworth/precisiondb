﻿CREATE TABLE [epacube].[UI_FILTER_CUSTOM_VIEW] (
    [ui_filter_custom_view_id] INT IDENTITY (1, 1) NOT NULL,
    [ui_view_fk]               INT NOT NULL,
    [ui_filter_fk]             INT NOT NULL,
    [record_status_cr_fk]      INT CONSTRAINT [DF_ui_filter_custom_view_record_status_cr_fk] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_UI_FILTER_CUSTOM_VIEW] PRIMARY KEY CLUSTERED ([ui_filter_custom_view_id] ASC) WITH (FILLFACTOR = 80)
);



