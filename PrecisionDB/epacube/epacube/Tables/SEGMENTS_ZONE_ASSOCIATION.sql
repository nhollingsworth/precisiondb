﻿CREATE TABLE [epacube].[SEGMENTS_ZONE_ASSOCIATION] (
    [SEGMENTS_ZONE_ASSOCIATION_ID] BIGINT IDENTITY (1, 1000) NOT NULL,
    [Data_Name_FK]                 BIGINT NOT NULL,
    [Entity_Data_Name_FK]          BIGINT NULL,
    [Data_level_CR_FK]             INT    NOT NULL,
    [Cust_Entity_Structure_FK]     BIGINT NULL,
    [Zone_Entity_Structure_FK]     BIGINT NOT NULL,
    [Prod_Data_Value_FK]           BIGINT NULL,
    CONSTRAINT [PK_SEGMENTS_ZONE_ASSOCIATION] PRIMARY KEY CLUSTERED ([SEGMENTS_ZONE_ASSOCIATION_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_CIC]
    ON [epacube].[SEGMENTS_ZONE_ASSOCIATION]([Data_level_CR_FK] ASC, [Cust_Entity_Structure_FK] ASC, [Zone_Entity_Structure_FK] ASC, [Prod_Data_Value_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_IC]
    ON [epacube].[SEGMENTS_ZONE_ASSOCIATION]([Data_level_CR_FK] ASC, [Prod_Data_Value_FK] ASC, [Zone_Entity_Structure_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_cust]
    ON [epacube].[SEGMENTS_ZONE_ASSOCIATION]([Data_level_CR_FK] ASC, [Zone_Entity_Structure_FK] ASC, [Cust_Entity_Structure_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

