﻿CREATE TABLE [epacube].[PROMOS_CONTROLS_2] (
    [PROMOS_CONTROLS_ID]              BIGINT      IDENTITY (1000, 1) NOT NULL,
    [DATA_LEVEL_CR_FK]                INT         NULL,
    [DATA_NAME_FK]                    BIGINT      NOT NULL,
    [AD_GROUP_DV_FK]                  BIGINT      NOT NULL,
    [AD_ZONE_DV_FK]                   BIGINT      NULL,
    [BUYER_ACCESS]                    VARCHAR (8) NULL,
    [INTERFACE_TO_SALES]              VARCHAR (8) NULL,
    [INTERFACE_TO_MISC]               VARCHAR (8) NULL,
    [PRINT_CHECKSTAND_LIST]           VARCHAR (8) NULL,
    [PRINT_LARGE_SIGNS]               VARCHAR (8) NULL,
    [EFFECTIVE_DATE_ADJUSTMENT]       INT         NULL,
    [TERMINATION_DATE_ADJUSTMENT]     INT         NULL,
    [MAX_ZONES]                       INT         NULL,
    [COST_ZONE_ENTITY_STRUCTURE_FK]   BIGINT      NULL,
    [RETAIL_ZONE_ENTITY_STRUCTURE_FK] BIGINT      NULL,
    [CUST_ENTITY_STRUCTURE_FK]        BIGINT      NULL,
    [PRECEDENCE]                      INT         NULL,
    [AD_GROUP_ACCT]                   VARCHAR (8) NULL
);

