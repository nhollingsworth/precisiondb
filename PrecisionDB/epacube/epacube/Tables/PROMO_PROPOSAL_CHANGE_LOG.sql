﻿CREATE TABLE [epacube].[PROMO_PROPOSAL_CHANGE_LOG] (
    [PROMO_PROPOSAL_CHANGE_LOG_ID] BIGINT       IDENTITY (1, 1) NOT NULL,
    [PROMO_PROPOSAL_HEADER_FK]     BIGINT       NOT NULL,
    [PROPOSAL_ID]                  VARCHAR (50) NULL,
    [COLUMN_NAME]                  VARCHAR (50) NULL,
    [CHANGE_FROM]                  VARCHAR (50) NULL,
    [CHANGE_TO]                    VARCHAR (50) NULL,
    [JOB_FK]                       BIGINT       NULL,
    [CREATE_DATE]                  DATETIME     NULL
);

