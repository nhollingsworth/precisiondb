﻿CREATE TABLE [epacube].[PRODUCT_MULTIPLIERS] (
    [PRODUCT_MULTIPLIERS_ID] BIGINT          IDENTITY (1000, 1) NOT NULL,
    [PRODUCT_STRUCTURE_FK]   BIGINT          NOT NULL,
    [REPL_COST_MULTIPLIER]   NUMERIC (18, 6) NULL,
    [BASE_PRICE_MULTIPLIER]  NUMERIC (18, 6) NULL,
    [STD_COST_MULTIPLIER]    NUMERIC (18, 6) NULL,
    [UPDATE_TIMESTAMP]       DATETIME        CONSTRAINT [DF__PROD_MULT__UPDATE_TS] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_PRODUCT_MULTIPLIERS_ID] PRIMARY KEY CLUSTERED ([PRODUCT_MULTIPLIERS_ID] ASC),
    CONSTRAINT [FK_PM_PSF_PS] FOREIGN KEY ([PRODUCT_STRUCTURE_FK]) REFERENCES [epacube].[PRODUCT_STRUCTURE] ([PRODUCT_STRUCTURE_ID]),
    CONSTRAINT [PM_PSF_UC] UNIQUE NONCLUSTERED ([PRODUCT_STRUCTURE_FK] ASC)
);

