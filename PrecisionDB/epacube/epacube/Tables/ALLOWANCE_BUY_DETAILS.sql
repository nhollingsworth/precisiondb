﻿CREATE TABLE [epacube].[ALLOWANCE_BUY_DETAILS] (
    [ALLOWANCE_BUY_DETAILS_ID]     BIGINT          IDENTITY (1000, 1) NOT NULL,
    [RESULT_DATA_NAME_FK]          BIGINT          NULL,
    [BASIS]                        VARCHAR (50)    NULL,
    [BASIS_CALC_DN_FK]             BIGINT          NULL,
    [OPERATION1]                   VARCHAR (50)    NULL,
    [RULES_ACTION_OPERATION1_FK]   BIGINT          NULL,
    [VALUE]                        NUMERIC (18, 4) NULL,
    [QTY_COMMITTED]                NUMERIC (18, 2) NULL,
    [PROD_SEGMENT_DATA_NAME_FK]    BIGINT          NULL,
    [PROD_SEGMENT_FK]              BIGINT          NULL,
    [ALLOWANCE_TYPE_CR_FK]         VARCHAR (14)    NULL,
    [START_DATE]                   DATE            NULL,
    [END_DATE]                     DATE            NULL,
    [ALLOWANCE_PROGRAM_TYPE]       VARCHAR (15)    NULL,
    [ALLOWANCE_PROGRAM_TYPE_CR_FK] INT             NULL,
    [ALLOWANCE_BUY_HEADER_FK]      BIGINT          NULL,
    [CREATE_TIMESTAMP]             DATETIME        NULL,
    [CREATE_USER]                  VARCHAR (64)    NULL,
    [UPDATE_TIMESTAMP]             DATETIME        NULL,
    [UPDATE_USER]                  VARCHAR (64)    NULL,
    [RECORD_STATUS_CR_FK]          INT             NULL,
    CONSTRAINT [PK_ALLOWANCE_BUY_DETAILS] PRIMARY KEY CLUSTERED ([ALLOWANCE_BUY_DETAILS_ID] ASC) WITH (FILLFACTOR = 80)
);



