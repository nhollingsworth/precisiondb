﻿CREATE TABLE [epacube].[CONFIG] (
    [CONFIG_ID]          BIGINT        IDENTITY (90000000, 1) NOT NULL,
    [PARENT_CONFIG_FK]   BIGINT        NULL,
    [CONFIG_CLASS_CR_FK] INT           NULL,
    [DATA_NAME_FK]       INT           NULL,
    [NAME]               VARCHAR (50)  NOT NULL,
    [DESCRIPTION]        VARCHAR (600) NULL,
    [CREATE_TIMESTAMP]   DATETIME      NULL,
    [UPDATE_TIMESTAMP]   DATETIME      NULL,
    [UPDATE_USER]        VARCHAR (64)  NULL,
    [UPDATE_LOG_FK]      INT           NULL,
    CONSTRAINT [PK_CONFIG] PRIMARY KEY CLUSTERED ([CONFIG_ID] ASC),
    CONSTRAINT [FK_CONFIG_CCCF_CR] FOREIGN KEY ([CONFIG_CLASS_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID])
);



