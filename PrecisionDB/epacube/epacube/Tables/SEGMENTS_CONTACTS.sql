﻿CREATE TABLE [epacube].[SEGMENTS_CONTACTS] (
    [Segments_Contacts_ID]       BIGINT        IDENTITY (1, 1000) NOT NULL,
    [Data_Level_CR_FK]           INT           NULL,
    [Data_Level]                 VARCHAR (64)  NULL,
    [Data_Name_FK]               BIGINT        NULL,
    [Value]                      VARCHAR (128) NULL,
    [Data_Value_FK]              BIGINT        NULL,
    [Cust_Entity_Structure_FK]   BIGINT        NULL,
    [Cust_Data_Value_FK]         BIGINT        NULL,
    [Segments_Customer_FK]       BIGINT        NULL,
    [Vendor_Entity_Structure_FK] BIGINT        NULL,
    [Segments_Vendor_FK]         BIGINT        NULL,
    [Comp_Entity_Structure_FK]   BIGINT        NULL,
    [Entity_Class_CR_FK]         INT           NULL,
    [Update_Timestamp]           DATETIME      NULL,
    [Create_Timestamp]           DATETIME      NULL,
    [Update_User]                VARCHAR (64)  NULL,
    [Create_User]                VARCHAR (50)  NULL,
    CONSTRAINT [PK_SEGMENTS_CONTACTS] PRIMARY KEY CLUSTERED ([Segments_Contacts_ID] ASC) WITH (FILLFACTOR = 80)
);



