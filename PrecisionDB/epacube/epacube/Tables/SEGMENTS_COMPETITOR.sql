﻿CREATE TABLE [epacube].[SEGMENTS_COMPETITOR] (
    [SEGMENTS_COMPETITOR_ID]   BIGINT          IDENTITY (1000, 1) NOT NULL,
    [DATA_LEVEL_CR_FK]         INT             NULL,
    [DATA_LEVEL]               VARCHAR (64)    NULL,
    [COMP_ENTITY_STRUCTURE_FK] BIGINT          NULL,
    [ENTITY_DATA_VALUE_FK]     BIGINT          NULL,
    [DATA_NAME_FK]             BIGINT          NULL,
    [VALUE]                    VARCHAR (64)    NULL,
    [Description]              VARCHAR (64)    NULL,
    [RESULT_DATA_NAME_FK]      BIGINT          NULL,
    [PRODUCT_STRUCTURE_FK]     BIGINT          NULL,
    [COMPETITOR_UPC]           VARCHAR (16)    NULL,
    [Competitor_Description]   VARCHAR (64)    NULL,
    [Competitor_Size]          VARCHAR (16)    NULL,
    [URM_UPC]                  VARCHAR (16)    NULL,
    [REVIEWED]                 VARCHAR (1)     NULL,
    [EFFECTIVE_DATE]           DATE            NULL,
    [PRICE_MULTIPLE]           INT             NULL,
    [PRICE]                    NUMERIC (18, 6) NULL,
    [UPDATE_TIMESTAMP]         DATETIME        NULL,
    [CREATE_TIMESTAMP]         DATETIME        NULL,
    [UPDATE_USER]              VARCHAR (50)    NULL,
    [CREATE_USER]              VARCHAR (50)    NULL,
    [RECORD_STATUS_CR_FK]      INT             NULL,
    [data_value_fk]            BIGINT          NULL,
    CONSTRAINT [PK_SEGMENTS_COMPETITOR] PRIMARY KEY CLUSTERED ([SEGMENTS_COMPETITOR_ID] ASC) WITH (FILLFACTOR = 80)
);



