﻿CREATE TABLE [epacube].[DATA_TABLE_PROPERTIES] (
    [DATA_TABLE_PROPERTIES_ID] INT            NOT NULL,
    [TABLE_NAME]               VARCHAR (64)   NULL,
    [NAME]                     VARCHAR (32)   NULL,
    [VALUE]                    NVARCHAR (MAX) NULL,
    [VALUE2]                   NVARCHAR (MAX) NULL,
    [RECORD_STATUS_CR_FK]      INT            CONSTRAINT [DF__DATA_TBL_PROP__RECOR__0E6E26BF] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]         DATETIME       CONSTRAINT [DF__DATA_TBL_PROP__CREAT__0F624AF8] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]         DATETIME       CONSTRAINT [DF__DATA_TBL_PROP_UPDAT__10566F31] DEFAULT (getdate()) NULL,
    [UPDATE_LOG_FK]            BIGINT         NULL,
    CONSTRAINT [PK_DATA_TABLE_PROPERTIES] PRIMARY KEY CLUSTERED ([DATA_TABLE_PROPERTIES_ID] ASC)
);

