﻿CREATE TABLE [epacube].[PROMO_PROPOSAL_ITEM_ALLOW] (
    [ALLOWANCE_AMOUNT]             MONEY           NULL,
    [PROMO_PROPOSAL_HEADER_FK]     BIGINT          NULL,
    [PROPOSAL_ID]                  VARCHAR (16)    NULL,
    [PRODUCT_STRUCTURE_FK]         BIGINT          NULL,
    [ALLOWANCE_TYPE]               VARCHAR (14)    NULL,
    [START_DATE]                   DATE            NULL,
    [END_DATE]                     DATE            NULL,
    [PERCENT_OFF]                  NUMERIC (18, 4) NULL,
    [PERCENT_OFF_BASIS]            VARCHAR (64)    NULL,
    [PROGRAM_TYPE]                 VARCHAR (15)    NULL,
    [CREATE_DATE]                  DATETIME        NULL,
    [CREATE_USER]                  VARCHAR (64)    NULL,
    [PROMO_PROPOSAL_ITEM_ALLOW_ID] BIGINT          IDENTITY (1000, 1) NOT NULL
);

