﻿CREATE TABLE [epacube].[PRODUCT_UOM_SIZE] (
    [PRODUCT_UOM_SIZE_ID]  BIGINT          IDENTITY (1000, 1) NOT NULL,
    [PRODUCT_STRUCTURE_FK] BIGINT          NOT NULL,
    [VALUE]                NUMERIC (18, 4) NOT NULL,
    [PRIMARY_UOM_CODE_FK]  INT             NOT NULL,
    [RECORD_STATUS_CR_FK]  INT             CONSTRAINT [DF__PRODUCT_PUS__RECOR__1209AD79] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]     DATETIME        CONSTRAINT [DF__PRODUCT_PUS__CREAT__12FDD1B2] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]     DATETIME        CONSTRAINT [DF__PRODUCT_PUS__UPDAT__13F1F5EB] DEFAULT (getdate()) NULL,
    [UPDATE_LOG_FK]        BIGINT          NULL,
    CONSTRAINT [PK_PRODUCT_UOM_SIZE] PRIMARY KEY CLUSTERED ([PRODUCT_UOM_SIZE_ID] ASC),
    CONSTRAINT [FK_PUS_PSF_PS] FOREIGN KEY ([PRODUCT_STRUCTURE_FK]) REFERENCES [epacube].[PRODUCT_STRUCTURE] ([PRODUCT_STRUCTURE_ID]),
    CONSTRAINT [FK_PUS_UCF_UC] FOREIGN KEY ([PRIMARY_UOM_CODE_FK]) REFERENCES [epacube].[UOM_CODE] ([UOM_CODE_ID]),
    CONSTRAINT [PUS_DNF_PSF_OESF_EESF_UC] UNIQUE NONCLUSTERED ([PRIMARY_UOM_CODE_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC)
);

