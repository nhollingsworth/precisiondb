﻿CREATE TABLE [epacube].[UI_COMP_USER_FIELDS] (
    [ui_comp_user_fields_id] BIGINT        IDENTITY (1, 1) NOT NULL,
    [ui_comp_master_fk]      BIGINT        NOT NULL,
    [column_name]            VARCHAR (64)  NOT NULL,
    [Reference_Data_name_fk] BIGINT        NULL,
    [reference_column]       VARCHAR (64)  NULL,
    [stored_proc]            VARCHAR (256) NULL,
    [editable]               INT           NULL,
    [UI_CONFIG]              VARCHAR (MAX) NULL,
    CONSTRAINT [PK_UI_COMP_USER_FIELDS] PRIMARY KEY CLUSTERED ([ui_comp_user_fields_id] ASC) WITH (FILLFACTOR = 80)
);



