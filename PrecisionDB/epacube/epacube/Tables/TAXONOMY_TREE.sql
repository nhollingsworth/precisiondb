﻿CREATE TABLE [epacube].[TAXONOMY_TREE] (
    [TAXONOMY_TREE_ID]    INT      IDENTITY (1000, 1) NOT NULL,
    [DATA_VALUE_FK]       BIGINT   NOT NULL,
    [RECORD_STATUS_CR_FK] INT      CONSTRAINT [DF_TAX_TREE_17F790F9] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]    DATETIME CONSTRAINT [DF_TAX_TREE_CREAT__18EBB532] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]    DATETIME CONSTRAINT [DF_TAX_TREE_UPDAT__19DFD96B] DEFAULT (getdate()) NULL,
    [UPDATE_LOG_FK]       BIGINT   NULL,
    [DISPLAY_SEQ]         INT      NULL,
    CONSTRAINT [PK_TAX_TREE] PRIMARY KEY CLUSTERED ([TAXONOMY_TREE_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_TT_DVF_DV] FOREIGN KEY ([DATA_VALUE_FK]) REFERENCES [epacube].[DATA_VALUE] ([DATA_VALUE_ID])
);




GO


