﻿CREATE TABLE [epacube].[UI_FILTER] (
    [UI_Filter_ID]            BIGINT        IDENTITY (1, 1) NOT NULL,
    [Data_Name_FK]            BIGINT        NOT NULL,
    [V_Ref_Data_Name_FK]      BIGINT        NULL,
    [Text_Mask]               VARCHAR (16)  NULL,
    [Code_Type]               VARCHAR (64)  NULL,
    [Code_Records_to_Display] INT           NULL,
    [Sequence]                INT           NULL,
    [UI_Filter_Header_FK]     INT           NULL,
    [Radio]                   VARCHAR (64)  NULL,
    [Options]                 VARCHAR (64)  NULL,
    [Default_Value]           VARCHAR (64)  NULL,
    [Comp_Lookup_CR_FK]       INT           NULL,
    [Lookup_Values]           INT           NULL,
    [COMMENTS]                VARCHAR (MAX) NULL,
    [Record_Status_CR_FK]     BIGINT        NOT NULL,
    [ENTITY_CLASS_CR_FK]      INT           NULL,
    [ind_Multi_Select]        INT           NULL,
    [Zero_Pad]                INT           NULL,
    CONSTRAINT [PK_UI_FILTER] PRIMARY KEY CLUSTERED ([UI_Filter_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_UI_FILTER] FOREIGN KEY ([UI_Filter_Header_FK]) REFERENCES [epacube].[UI_FILTER_HEADER] ([UI_FILTER_HEADER_ID])
);



