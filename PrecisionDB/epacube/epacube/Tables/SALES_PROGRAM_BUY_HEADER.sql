﻿CREATE TABLE [epacube].[SALES_PROGRAM_BUY_HEADER] (
    [SALES_PROGRAM_BUY_HEADER_ID]       BIGINT        IDENTITY (1000, 1) NOT NULL,
    [SALES_PROGRAM_CLASS]               VARCHAR (50)  NULL,
    [SALES_PROGRAM_CLASS_CR_FK]         INT           NULL,
    [REBATE_ID]                         VARCHAR (50)  NULL,
    [PROPOSAL_ID]                       VARCHAR (50)  NULL,
    [START_DATE]                        DATE          NULL,
    [END_DATE]                          DATE          NULL,
    [SALES_PROGRAM]                     VARCHAR (64)  NULL,
    [SALES_PROGRAM_DV_FK]               BIGINT        NULL,
    [PAYMENT_METHOD]                    VARCHAR (50)  NULL,
    [VENDOR_ID]                         VARCHAR (50)  NULL,
    [VENDOR_ENTITY_STRUCTURE_FK]        BIGINT        NULL,
    [PROPOSAL_GROUPING]                 VARCHAR (50)  NULL,
    [BILLING_MOVEMENT_SOURCE]           VARCHAR (50)  NULL,
    [TERMINATION_DATE_OVERRIDE]         VARCHAR (50)  NULL,
    [COMMENT]                           VARCHAR (256) NULL,
    [THEME]                             VARCHAR (256) NULL,
    [PERFORMANCE_REQUIREMENT]           VARCHAR (256) NULL,
    [PRESENTING_ENTITY]                 VARCHAR (50)  NULL,
    [PRESENTING_ENTITY_STRUCTURE_FK]    BIGINT        NULL,
    [PROMO_DOLLARS_ENTITY]              VARCHAR (50)  NULL,
    [PROMO_DOLLARS_ENTITY_STRUCTURE_FK] BIGINT        NULL,
    [BILL_BACK_ENTITY]                  VARCHAR (50)  NULL,
    [BILL_BACK_ENTITY_STRUCTURE_FK]     BIGINT        NULL,
    [CREATE_TIMESTAMP]                  DATETIME      NULL,
    [CREATE_USER]                       VARCHAR (50)  NULL,
    [UPDATE_TIMESTAMP]                  DATETIME      NULL,
    [UPDATE_USER]                       VARCHAR (50)  NULL,
    [RECORD_STATUS_CR_FK]               INT           NULL,
    [tPromo_ID]                         VARCHAR (64)  NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_sfh1]
    ON [epacube].[SALES_PROGRAM_BUY_HEADER]([SALES_PROGRAM_BUY_HEADER_ID] ASC, [REBATE_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

