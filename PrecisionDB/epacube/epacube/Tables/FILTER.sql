﻿CREATE TABLE [epacube].[FILTER] (
    [FILTER_ID]         INT            IDENTITY (1000, 1) NOT NULL,
    [PARENT_FILTER_FK]  INT            NULL,
    [FILTER_TYPE_CR_FK] INT            NULL,
    [LOGIN]             VARCHAR (64)   NULL,
    [NAME]              VARCHAR (64)   NULL,
    [DESCRIPTION]       VARCHAR (2000) NULL,
    [DOCUMENT]          VARCHAR (MAX)  NULL,
    [SCRIPT]            VARCHAR (256)  NULL,
    [ID_SQL]            VARCHAR (MAX)  NULL,
    [CREATE_TIMESTAMP]  DATETIME       CONSTRAINT [DF_FILTER_CREATE_TIMESTAMP] DEFAULT (getdate()) NULL,
    [ID_COLUMN]         VARCHAR (50)   NULL,
    [COLUMNS]           VARCHAR (MAX)  NULL,
    [TARGET]            VARCHAR (256)  NULL,
    [JOINS]             VARCHAR (MAX)  NULL,
    [CRITERIA]          VARCHAR (MAX)  NULL,
    [SPECS]             VARCHAR (256)  NULL,
    CONSTRAINT [PK_FILTER] PRIMARY KEY CLUSTERED ([FILTER_ID] ASC),
    CONSTRAINT [CK_FILTER_UNK] UNIQUE NONCLUSTERED ([LOGIN] ASC, [NAME] ASC, [SCRIPT] ASC, [FILTER_TYPE_CR_FK] ASC)
);

