﻿CREATE TABLE [epacube].[PRODUCT_RECIPE_INGREDIENT] (
    [PRODUCT_RECIPE_INGREDIENT_ID]      BIGINT          IDENTITY (1000, 1) NOT NULL,
    [PRODUCT_RECIPE_FK]                 BIGINT          NULL,
    [CHAIN_ID_DATA_VALUE_FK]            BIGINT          NULL,
    [PRODUCT_RECIPE_INGREDIENT_XREF_FK] BIGINT          NULL,
    [SEQ]                               BIGINT          NULL,
    [DATA_NAME_FK]                      INT             NULL,
    [VALUE]                             NUMERIC (18, 4) NULL,
    [RECORD_STATUS_CR_FK]               INT             NULL,
    [CREATE_USER]                       VARCHAR (50)    NULL,
    [CREATE_TIMESTAMP]                  DATETIME        NULL,
    [UPDATE_TIMESTAMP]                  DATETIME        NULL,
    [UPDATE_USER]                       VARCHAR (50)    NULL,
    CONSTRAINT [PK_PRODUCT_RECIPE_INGREDIENT] PRIMARY KEY CLUSTERED ([PRODUCT_RECIPE_INGREDIENT_ID] ASC),
    CONSTRAINT [FK_PRI_CHDV] FOREIGN KEY ([CHAIN_ID_DATA_VALUE_FK]) REFERENCES [epacube].[DATA_VALUE] ([DATA_VALUE_ID]),
    CONSTRAINT [FK_PRI_DN] FOREIGN KEY ([DATA_NAME_FK]) REFERENCES [epacube].[DATA_NAME] ([DATA_NAME_ID]),
    CONSTRAINT [FK_PRI_PRF] FOREIGN KEY ([PRODUCT_RECIPE_FK]) REFERENCES [epacube].[PRODUCT_RECIPE] ([PRODUCT_RECIPE_ID]),
    CONSTRAINT [FK_PRI_PRIXF] FOREIGN KEY ([PRODUCT_RECIPE_INGREDIENT_XREF_FK]) REFERENCES [epacube].[PRODUCT_RECIPE_INGREDIENT_XREF] ([PRODUCT_RECIPE_INGREDIENT_XREF_ID])
);

