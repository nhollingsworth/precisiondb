﻿CREATE TABLE [epacube].[ALLOWANCE_BUY_ASSIGNMENTS] (
    [ALLOWANCE_BUY_ASSIGNMENTS_ID] BIGINT       IDENTITY (1000, 1) NOT NULL,
    [ALLOWANCE_BUY_HEADER_FK]      BIGINT       NOT NULL,
    [CUST_SEGMENT_DATA_NAME_FK]    BIGINT       NULL,
    [CUST_SEGMENT_FK]              BIGINT       NULL,
    [ALLOWANCE_TYPE_CR_FK]         INT          NULL,
    [ALLOWANCE_PROGRAM_TYPE]       VARCHAR (64) NULL,
    [ALLOWANCE_PROGRAM_TYPE_CR_FK] INT          NULL,
    [PROPOSAL_AMOUNT_LUMP_SUM]     MONEY        NULL,
    [PROPOSAL_AMOUNT_BY_CASE]      MONEY        NULL,
    [PROMO_DOLLARS]                MONEY        NULL,
    [PROPOSAL_USED]                VARCHAR (8)  NULL,
    CONSTRAINT [PK_ALLOWANCE_BUY_ASSIGNMENTS] PRIMARY KEY CLUSTERED ([ALLOWANCE_BUY_ASSIGNMENTS_ID] ASC) WITH (FILLFACTOR = 80)
);



