﻿CREATE TABLE [epacube].[PRODUCT_IDENT_NONUNIQUE] (
    [PRODUCT_IDENT_NONUNIQUE_ID] BIGINT        IDENTITY (1000, 1) NOT NULL,
    [PRODUCT_STRUCTURE_FK]       BIGINT        NOT NULL,
    [DATA_NAME_FK]               INT           NOT NULL,
    [VALUE]                      VARCHAR (128) NOT NULL,
    [ORG_ENTITY_STRUCTURE_FK]    BIGINT        CONSTRAINT [DF_PRODUCT_IDENT_NONUNIQUE_ORG_ENTITY_STRUCTURE_FK] DEFAULT ((1)) NOT NULL,
    [ENTITY_STRUCTURE_FK]        BIGINT        NULL,
    [RECORD_STATUS_CR_FK]        INT           CONSTRAINT [DF_PRODUCT_IDENT_NONUNIQUE_RECORD_STATUS_CR_FK] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]           DATETIME      CONSTRAINT [DF_PRODUCT_IDENT_NONUNIQUE_CREATE_TIMESTAMP] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]           DATETIME      CONSTRAINT [DF_PRODUCT_IDENT_NONUNIQUE_UPDATE_TIMESTAMP] DEFAULT (getdate()) NULL,
    [UPDATE_LOG_FK]              BIGINT        NULL,
    [END_DATE]                   DATETIME      NULL,
    CONSTRAINT [PK_PRODUCT_IDENT_NONUNIQUE] PRIMARY KEY CLUSTERED ([PRODUCT_IDENT_NONUNIQUE_ID] ASC),
    CONSTRAINT [FK_PIN_ESF_ES] FOREIGN KEY ([ENTITY_STRUCTURE_FK]) REFERENCES [epacube].[ENTITY_STRUCTURE] ([ENTITY_STRUCTURE_ID]),
    CONSTRAINT [FK_PINU_DN] FOREIGN KEY ([DATA_NAME_FK]) REFERENCES [epacube].[DATA_NAME] ([DATA_NAME_ID]),
    CONSTRAINT [FK_PINU_OESF_ES] FOREIGN KEY ([ORG_ENTITY_STRUCTURE_FK]) REFERENCES [epacube].[ENTITY_STRUCTURE] ([ENTITY_STRUCTURE_ID]),
    CONSTRAINT [FK_PINU_PSF_PS] FOREIGN KEY ([PRODUCT_STRUCTURE_FK]) REFERENCES [epacube].[PRODUCT_STRUCTURE] ([PRODUCT_STRUCTURE_ID]),
    CONSTRAINT [PINU_20100215_UC_CHECK] UNIQUE NONCLUSTERED ([DATA_NAME_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC, [ORG_ENTITY_STRUCTURE_FK] ASC, [ENTITY_STRUCTURE_FK] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_PIN_UT_PSF]
    ON [epacube].[PRODUCT_IDENT_NONUNIQUE]([UPDATE_TIMESTAMP] ASC, [PRODUCT_STRUCTURE_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_PINU_DNF_PSF_OESF_EESF_ICD]
    ON [epacube].[PRODUCT_IDENT_NONUNIQUE]([VALUE] ASC, [DATA_NAME_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC, [ORG_ENTITY_STRUCTURE_FK] ASC, [ENTITY_STRUCTURE_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_PRODUCT_IDENT_NONUNIQUE_10_1207219601__K2_K3_5_7]
    ON [epacube].[PRODUCT_IDENT_NONUNIQUE]([ENTITY_STRUCTURE_FK] ASC, [ORG_ENTITY_STRUCTURE_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC, [DATA_NAME_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE STATISTICS [PINU_20110302_POST_DERIVE_STAT1]
    ON [epacube].[PRODUCT_IDENT_NONUNIQUE]([DATA_NAME_FK], [PRODUCT_IDENT_NONUNIQUE_ID], [PRODUCT_STRUCTURE_FK]);

