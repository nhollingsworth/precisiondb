﻿


CREATE 
-- alter 
view [epacube].[v_entity_assoc_w_pending]
as
select 
ea.parent_entity_structure_fk
,ea.entity_association_id
,null event_id
,ea.org_entity_structure_fk
,ea.child_entity_structure_fk
,ea.data_name_fk associationDataNameId
,null event_condition_cr_fk
,null condition
,null errors
  from epacube.entity_association ea WITH (NOLOCK)
 where ea.record_status_cr_fk=1
 
union 
select 
ed.entity_structure_fk --I assume this is the parent?
,null entity_association_id
,ed.event_id
,ed.org_entity_structure_fk
,ed.entity_structure_fk child_entity_structure_fk--probably have to look this one up?
,ed.data_name_fk associationDataNameId
,ed.event_condition_cr_fk
,(select code from epacube.code_ref where code_Ref_id=ed.event_condition_cr_fk) condition
,(select count(1) errors from synchronizer.event_data_errors where event_fk=ed.event_id) errors
  from synchronizer.event_data ed WITH (NOLOCK)
 where ed.event_type_cr_fk=epacube.getCodeRefId('EVENT_TYPE','ENTITY_ASSOC') 
   and ed.event_status_cr_fk=epacube.getCodeRefId('EVENT_STATUS','PENDING')



