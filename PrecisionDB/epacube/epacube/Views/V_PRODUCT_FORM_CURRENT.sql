﻿
CREATE VIEW [epacube].[V_PRODUCT_FORM_CURRENT]
AS
-- SET ANSI_NULLS OFF
select 
cpd.CURRENT_DATA,
(select description from epacube.data_value where data_value_id=cpd.current_data_dv_fk) DV_DESCRIPTION,
-- Look for  a pending event value for this data_name 
cpd.DATA_NAME_FK,
vdn.DATA_NAME,
vdn.LABEL,
cpd.net_value1_cur,
cpd.net_value2_cur,
cpd.net_value3_cur,
cpd.net_value4_cur,
cpd.net_value5_cur,
cpd.net_value6_cur,
cpd.PRODUCT_STRUCTURE_FK,
cpd.ENTITY_STRUCTURE_FK,
cpd.ORG_ENTITY_STRUCTURE_FK
  from epacube.v_current_product_all cpd
       inner join epacube.v_data_names vdn on (vdn.dnid=cpd.data_name_fk)
