﻿CREATE VIEW [epacube].[V_EXPORT_TAXONOMY_TREE_STRUCTURE]
AS
SELECT
taxonomy_tree_fk as EPA_CAT_ID 
           ,CATALOG_NAME
           ,LANGUAGE_CODE
           ,TAXONOMY_NODE_fk AS EPA_CTG_ID
           ,CATEGORY
           ,DESCRIPTION
           ,DISPLAY_SEQ
           ,LEVEL_SEQ AS TAX_LEVEL
           ,PARENT_TAXONOMY_NODE_fk as EPA_PARENT_CTG_ID
           ,[PARENT_NAME]
           ,[IMAGE]
           ,[COMMENT]
           ,[STATUS]
           ,[CUSTOM1]
           ,[CUSTOM2]
           ,[CUSTOM3]
           ,[CUSTOM4]
           ,[CUSTOM5]      
FROM        [epacube].[dbo].[EXPORT.EXPORT_TAXONOMY_TREE]

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TN"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 271
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TT"
            Begin Extent = 
               Top = 6
               Left = 309
               Bottom = 114
               Right = 513
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dntt"
            Begin Extent = 
               Top = 6
               Left = 551
               Bottom = 114
               Right = 800
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tnp"
            Begin Extent = 
               Top = 114
               Left = 280
               Bottom = 222
               Right = 513
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dvp"
            Begin Extent = 
               Top = 114
               Left = 551
               Bottom = 222
               Right = 755
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         W', @level0type = N'SCHEMA', @level0name = N'epacube', @level1type = N'VIEW', @level1name = N'V_EXPORT_TAXONOMY_TREE_STRUCTURE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'idth = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'epacube', @level1type = N'VIEW', @level1name = N'V_EXPORT_TAXONOMY_TREE_STRUCTURE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'epacube', @level1type = N'VIEW', @level1name = N'V_EXPORT_TAXONOMY_TREE_STRUCTURE';

