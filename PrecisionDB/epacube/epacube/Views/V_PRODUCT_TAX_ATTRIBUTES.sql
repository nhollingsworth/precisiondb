﻿


CREATE view [epacube].[V_PRODUCT_TAX_ATTRIBUTES]
AS
SELECT distinct pc.product_structure_fk,
                tt.Taxonomy_Tree_ID AS taxonomy_tree_fk,
                pc.data_value_fk AS Tree_Assignment_DV_FK,
                --( SELECT taxonomy_node_id FROM epacube.taxonomy_node 
                --  WHERE tax_node_dv_fk = pc.data_value_fk ) 
				tn.TAXONOMY_NODE_ID  
				  AS Node_of_Assignment_Taxonomy_Node_fk,
                tna.TAXONOMY_NODE_FK AS Node_of_Attribute_Taxonomy_Node_fk, 
                tna.Taxonomy_Node_Attribute_ID AS taxonomy_node_attribute_fk,
                dn.data_name_id AS data_name_fk,
                dn.[NAME] AS data_name,
                tna.DEFAULT_NODE_VALUE,
                tna.RESTRICTED_DATA_VALUES_IND ,
                dn.data_set_fk
---
        FROM    epacube.PRODUCT_CATEGORY pc
                INNER JOIN epacube.taxonomy_tree tt ON ( tt.tax_node_dn_fk = pc.data_name_fk )
                CROSS APPLY epacube.Get_Product_Node_Attributes (pc.PRODUCT_STRUCTURE_FK ) AS node
                INNER JOIN epacube.TAXONOMY_NODE_ATTRIBUTE tna 
                      ON ( tna.TAXONOMY_NODE_ATTRIBUTE_ID = node.taxonomy_node_attribute_fk 
							--and tna.TAXONOMY_NODE_FK = pc.TAXONOMY_NODE_FK 
							)
                INNER JOIN epacube.DATA_NAME DN ON ( DN.DATA_NAME_ID = TNA.DATA_NAME_FK )  
				INNER JOIN epacube.taxonomy_node tn on (tn.TAX_NODE_DV_FK = pc.DATA_VALUE_FK and tn.TAXONOMY_TREE_FK = tt.TAXONOMY_TREE_ID)


