﻿CREATE VIEW [epacube].[V_DATA_NAMES] as
  select dn.data_name_id dnid
    , ds.data_set_id dsid
    , gr.Data_Name_FK grdnid
    , dnentity.data_name_id ednid
    , dnorg.data_name_id odnid
    , dnparent.data_name_id pdnid
    , dg.data_group_id dgid
    , dg.data_group
    , dn.name data_name
    , dn.label
    , dn.HEADING
    , dn.SHORT_NAME
    , data_type.code datatype
    , event_type.code event_type
    , entity_class.code entity_class
    , grlevel.code data_level
    , ds.name set_name
    , gr.Table_Schema table_schema
    , ds.table_name
    , ds.column_name
    , gr.alias
    , ds.corp_ind corp
    , ds.org_ind org
    , ds.entity_structure_ec_cr_fk
    , entity_structure_class.code ES_ENTITY_CLASS
    , dnentity.name entity_dn_name
    , dnorg.name org_dn_name
    , dnparent.name parent_name
    , dn.SYSTEM_PROTECTED
    , dn.CHECKBOX
    , dn.TOOL_TIP
    , dn.POST_IND
    , dn.UI_CONFIG
  from epacube.data_name dn
    inner join epacube.data_set ds on (ds.data_set_id=dn.data_set_fk)
    left join epacube.DATA_NAME_GROUP_REFS gr on (gr.Data_Name_FK=dn.DATA_NAME_ID)
    left join epacube.code_Ref data_type on (data_type.code_Ref_id=ds.data_type_cr_fk)
    left join epacube.code_Ref event_type on (event_type.code_Ref_id=ds.event_type_cr_fk)
    left join epacube.code_Ref entity_class on (entity_class.code_Ref_id=ds.entity_class_cr_fk)
    left join epacube.code_Ref grlevel on (grlevel.code_Ref_id=gr.Data_Level_CR_FK)
    left join epacube.code_Ref entity_structure_class on (entity_structure_class.code_Ref_id=ds.entity_structure_ec_cr_fk)
    left join epacube.data_name dnparent on (dnparent.data_name_id=dn.parent_data_name_fk)
    left join epacube.data_name dnentity on (dnentity.data_name_id=dn.entity_data_name_fk)
    left join epacube.data_name dnorg on (dnorg.data_name_id=dn.org_data_name_fk)
    left join epacube.data_group dg on (dg.data_group_id=dn.data_group_fk)
  where dn.record_status_cr_fk!=epacube.getCoderefId('RECORD_STATUS','INACTIVE')