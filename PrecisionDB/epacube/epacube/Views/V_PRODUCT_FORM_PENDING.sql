﻿CREATE VIEW EPACUBE.V_PRODUCT_FORM_PENDING
AS
SELECT 
ed.event_id,
crEventCondition.code event_condition, crEventType.code event_type, 
crEventEntityClass.code event_entity_class, vdn.data_name, 
(select count(1) from synchronizer.event_data_errors where event_fk=ed.event_id) errors,
ed.new_data,
ed.net_value1_new,
ed.net_value2_new,
ed.net_value3_new,
ed.net_value4_new,
ed.net_value5_new,
ed.net_value6_new,
ed.net_value7_new,
ed.net_value8_new,
ed.net_value9_new,
ed.net_value10_new,
ed.event_type_cr_fk,
ed.epacube_id,
ed.product_structure_fk,
ed.org_entity_structure_fk,
ed.entity_structure_fk,
ed.data_name_fk
 FROM synchronizer.event_data ed
      inner join epacube.v_data_names vdn on (vdn.dnid=ed.data_name_fk) 
      inner join epacube.code_ref crEventType on (crEventType.code_Ref_id=ed.event_type_cr_fk)
      inner join epacube.code_ref crEventEntityClass on (crEventEntityClass.code_Ref_id=ed.event_type_cr_fk)
      inner join epacube.code_ref crEventCondition on (crEventCondition.code_Ref_id=ed.event_condition_cr_fk)
where ed.event_status_cr_fk=80
