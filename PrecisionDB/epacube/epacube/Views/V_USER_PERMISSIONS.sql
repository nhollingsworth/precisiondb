﻿--drop view epacube.V_USER_PERMISSIONS

CREATE VIEW [epacube].[V_USER_PERMISSIONS]
AS
SELECT u.USERS_ID
       ,g.GROUPS_ID
       ,p.permissions_id
       ,u.LOGIN
       ,g.NAME AS group_name
       ,p.NAME AS permission_name
  FROM epacube.USERS AS u 
  INNER JOIN epacube.USER_GROUPS AS ug ON ug.USERS_FK = u.USERS_ID 
  INNER JOIN epacube.GROUPS AS g ON ug.GROUPS_FK = g.GROUPS_ID 
  INNER JOIN epacube.GROUP_PERMISSIONS AS gp ON g.GROUPS_ID = gp.GROUPS_FK 
  INNER JOIN epacube.PERMISSIONS AS p ON p.PERMISSIONS_ID = gp.PERMISSIONS_FK