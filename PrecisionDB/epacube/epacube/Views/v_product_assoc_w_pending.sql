﻿

CREATE 
-- alter 
view [epacube].[v_product_assoc_w_pending]
as
select 
pa.product_structure_fk
,pa.product_association_id
,null event_id
,pa.org_entity_structure_fk
,pa.entity_structure_fk
,pa.data_name_fk associationDataNameId
,null event_condition_cr_fk
,null condition
,null errors
  from epacube.product_association pa WITH (NOLOCK)
 where pa.record_status_cr_fk=1
 
union 
select 
ed.product_structure_fk
,null product_association_id
,ed.event_id
,ed.org_entity_structure_fk
,ed.entity_structure_fk
,ed.data_name_fk associationDataNameId
,ed.event_condition_cr_fk
,(select code from epacube.code_ref where code_Ref_id=ed.event_condition_cr_fk) condition
,(select count(1) errors from synchronizer.event_data_errors where event_fk=ed.event_id) errors
  from synchronizer.event_data ed WITH (NOLOCK)
 where ed.event_type_cr_fk=epacube.getCodeRefId('EVENT_TYPE','ASSOC') 
   and ed.event_status_cr_fk=epacube.getCodeRefId('EVENT_STATUS','PENDING')


