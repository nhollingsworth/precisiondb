﻿CREATE VIEW EPACUBE.V_PRODUCT_FORM_PENDING_ERRORS
AS
SELECT ede.event_errors_id, ede.event_fk,
	   crEventCondition.code event_condition,
       r.name RULE_NAME,
       ede.error_name,ede.error_data1,ede.error_data2,
       ede.error_data3,ede.error_data4,ede.error_data5,ede.error_data6
  from synchronizer.event_data_errors ede 
       inner join epacube.code_Ref crEventCondition on (crEventCondition.code_ref_id=ede.event_condition_cr_fk)
       left join synchronizer.rules r on (ede.rule_fk=r.rules_id)       
 
 




