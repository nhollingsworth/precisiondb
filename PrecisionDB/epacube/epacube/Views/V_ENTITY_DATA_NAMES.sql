﻿CREATE VIEW [epacube].[V_ENTITY_DATA_NAMES] AS
select 
dn.data_name_id dnid
,cr_data_type.code data_type
,cr_entity_class.code_ref_id entity_class_id
,cr_entity_class.code entity_class
,dn.name
,ds.name dsname 
,ds.table_name
,ds.column_name
,dn.label
,dn.tool_tip
,dn.heading
,dn.description
,dn.USER_EDITIBLE
,dn.USER_EDITIBLE_VALUES
,dn.POST_IND
,dn.org_data_name_fk
,dn.entity_data_name_fk
  from epacube.data_name dn, epacube.data_set ds, epacube.code_ref cr_event_type,epacube.code_ref cr_data_type,epacube.code_ref cr_entity_class
 where dn.data_set_fk=ds.data_set_id
   and cr_event_type.code_ref_id=ds.EVENT_TYPE_CR_FK
   and cr_data_type.code_ref_id=ds.DATA_TYPE_CR_FK
   and cr_entity_class.code_ref_id=ds.ENTITY_CLASS_CR_FK
   and cr_event_type.code='ENTITY' 
   and dn.name not like 'ALL %' 
   and dn.record_status_cr_fk=1
   and ds.record_status_cr_fk=1
--order by ds.name, dn.name

-- select distinct 