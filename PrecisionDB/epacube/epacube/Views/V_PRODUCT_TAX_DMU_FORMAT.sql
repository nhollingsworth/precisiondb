﻿

CREATE VIEW [epacube].[V_PRODUCT_TAX_DMU_FORMAT]
AS
 select 'BSH_STORE' Tax_Tree, pi.value Product,dv.VALUE TAX_NODE, tn.TAXONOMY_NODE_ID Tax_node_id
,dn.Name Tax_Name
, ptal.attribute_event_data Tax_Value
, 'data_value_fk' Data_Value, 'A' Remove_prod_from_Node
,pta.ATTRIBUTE_EVENT_DATA 'storefront_flag'
from epacube.product_identification pi 
inner join epacube.PRODUCT_CATEGORY pc on pc.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK and pc.DATA_NAME_FK = 190901
inner join epacube.DATA_VALUE dv on dv.DATA_VALUE_ID = pc.DATA_VALUE_FK
inner join epacube.TAXONOMY_NODE tn on tn.TAX_NODE_DV_FK = dv.DATA_VALUE_ID
inner join epacube.PRODUCT_TAX_ATTRIBUTE ptal with (nolock)
on pi.PRODUCT_STRUCTURE_FK = ptal.PRODUCT_STRUCTURE_FK and pi.data_name_fk = 110100  
and ptal.DATA_NAME_FK <> 990007 
inner join epacube.data_name dn  on ptal.data_name_FK = dn.data_name_ID
inner join epacube.PRODUCT_TAX_ATTRIBUTE pta with (nolock)
on pi.PRODUCT_STRUCTURE_FK = pta.PRODUCT_STRUCTURE_FK and pi.data_name_fk = 110100
and pta.DATA_NAME_FK = 990007


