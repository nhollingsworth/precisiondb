﻿






CREATE VIEW [epacube].[V_PRODUCT_TAX_DMU_FORMAT_2]
AS
   select distinct epa.Tax_Tree, epa.product Prod_ID ,epa.tax_node, epa.Tax_node_id,  d.tax_name data_name, d.tax_value data_value 
  ,epa.Data_Value Data_Type, epa.Remove_prod_from_Node  
  from epacube.V_product_tax_dmu_format epa with (nolock)
 inner join dbo.bolt_tax_data2 d with (nolock)
 on epa.product = d.product
 and d.tax_value <> ' ' 




