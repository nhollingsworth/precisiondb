﻿
CREATE VIEW [epacube].[V_EXPORT_DATA_NAME]
(
DATA_NAME_FK,
EXPORT_DATE
)
AS
----- SXE EXPORT CHANGES
SELECT DISTINCT
CDN.DATA_NAME_FK,
MAX ( J.JOB_COMPLETE_TIMESTAMP )
FROM common.job J
INNER JOIN epacube.config C ON ( C.NAME = J.NAME )
INNER JOIN epacube.config_property CP ON ( CP.CONFIG_FK = C.CONFIG_ID
                                      AND  CP.NAME = 'COLUMNS_CONFIG_ID' )
INNER JOIN epacube.config CC ON ( cc.config_id = CAST ( CP.VALUE AS BIGINT ) ) 
INNER JOIN epacube.config_data_name CDN ON ( CDN.config_fk = cc.config_id )
WHERE J.NAME IN ( 'ICSC Export CHANGES',  'ICSP Export CHANGES', 'ICSW Export CHANGES' )
GROUP BY CDN.DATA_NAME_FK
-----
----- ECLIPSE EXPORT CHANGES
-----
UNION ALL
SELECT 
100,
'01/01/2999'
---- END