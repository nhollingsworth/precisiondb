﻿

/*
*/
CREATE VIEW [epacube].[V_CURRENT_SHEET_RESULTS]
AS
SELECT                PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, ENTITY_STRUCTURE_FK, DATA_NAME_FK, 
                       NET_VALUE1 AS NET_VALUE1_CUR,
                       NET_VALUE2 AS NET_VALUE2_CUR,                       
                       NET_VALUE3 AS NET_VALUE3_CUR,                       
                       NET_VALUE4 AS NET_VALUE4_CUR,                       
                       NET_VALUE5 AS NET_VALUE5_CUR,                       
                       NET_VALUE6 AS NET_VALUE6_CUR,                       
                       NET_VALUE7 AS NET_VALUE7_CUR,                       
                       NET_VALUE8 AS NET_VALUE8_CUR,                       
                       NET_VALUE9 AS NET_VALUE9_CUR,                       
                       NET_VALUE10 AS NET_VALUE10_CUR,                       
                       ASSOC_UOM_CODE_FK, VALUE_UOM_CODE_FK,
                      EFFECTIVE_DATE AS EFFECTIVE_DATE_CUR, END_DATE AS END_DATE_CUR, 
					  -- SHEET_RESULTS_ID AS TABLE_ID_FK, 
                      UPDATE_LOG_FK, RECORD_STATUS_CR_FK,
                      710 AS RESULT_TYPE_CR_FK, NULL CUR_RESULT_TYPE_CR_FK,
                      NULL AS AUDIT_FILENAME
FROM                   marginmgr.SHEET_RESULTS_VALUES_FUTURE AS SR WITH (NOLOCK)
--------------UNION ALL--- EVENTS... ?? WHY ??? 
----------UNION ALL
----------SELECT                PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, ENTITY_STRUCTURE_FK, DATA_NAME_FK, 
----------                      NET_VALUE1_CUR, NET_VALUE2_CUR, NET_VALUE3_CUR, NET_VALUE4_CUR, NET_VALUE5_CUR, NET_VALUE6_CUR, 
----------                      NET_VALUE7_CUR, NET_VALUE8_CUR, NET_VALUE9_CUR, NET_VALUE10_CUR, ASSOC_UOM_CODE_FK, VALUE_UOM_CODE_FK,
----------                      EVENT_EFFECTIVE_DATE AS EFFECTIVE_DATE_CUR, 
----------                      END_DATE_CUR, EVENT_ID AS TABLE_ID_FK, NULL AS UPDATE_LOG_FK, RECORD_STATUS_CR_FK,
----------                      RESULT_TYPE_CR_FK, CUR_RESULT_TYPE_CR_FK, AUDIT_FILENAME
----------FROM         synchronizer.EVENT_DATA AS ESR WITH (NOLOCK)
----------WHERE     (RESULT_TYPE_CR_FK = 710)
