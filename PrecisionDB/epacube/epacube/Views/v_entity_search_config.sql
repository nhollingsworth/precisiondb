﻿
CREATE view
[epacube].[v_entity_search_config]
as  
select 
s.scope, crec.code entity_class, ep.name param_name 
,dn.data_name, dn.dnid data_name_id
,dn.table_name
,dn.column_name
,s.application_scope_id
,crec.code_ref_id entity_class_cr_fk
  from epacube.application_scope s
 inner join epacube.code_ref crec 
    on (crec.code_ref_id=s.application_scope_id and 
        crec.code_type='ENTITY_CLASS')
 inner join epacube.epacube_params ep on (s.application_scope_id=ep.application_scope_fk) 
 inner join epacube.v_data_names dn on (isnumeric(ep.value)=1 and dn.dnid=(cast (ep.value as int)))

