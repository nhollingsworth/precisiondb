﻿
--USE [epacube]
--GO

--/****** Object:  View [cleanser].[V_SEARCH_BEST_ES]    Script Date: 4/2/2019 2:58:47 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--drop VIEW precision_reports.v_level_gross_this_week

CREATE VIEW [epacube].[v_level_gross_this_week]
AS
	Select 
	prc.Price_Mgmt_Type_CR_FK
	, Case prc.Price_Mgmt_Type_CR_FK when 650 then 'Level Gross' else 'Non Level Gross' end 'Price_Mgmt_Type'
	, prc.Item_Change_Type_CR_FK
	, case prc.item_change_type_cr_fk when 656 then 'Alias'
										when 657 then 'Alias Item'
										when 655 then 'Regular Item'
										when 662 then 'Margin Changes Alias'
										when 661 then 'Margin Changes Regular Item'
		end 'item_change_type'
	, prc.Qualified_Status_CR_FK
	, Case prc.Qualified_Status_CR_FK when 653 then 'Qualified' else 'Unqualified' end 'Qualified Status'
	, PRICESHEET_RETAIL_CHANGES_ID
	, PRC.ALIAS_ID
	, PRC.Parity_PL_Parent_Structure_FK
	, PRC.Parity_Size_Parent_Structure_FK
	, PRC.Parity_Alias_PL_Parent_Structure_FK
	, PRC.Parity_Alias_SIZE_Parent_Structure_FK
	, PRC.PRODUCT_STRUCTURE_FK
	, PRC.ZONE_ENTITY_STRUCTURE_FK
	, PRC.ZONE
	, PRC.ZONE_NAME
	, PRC.SUBGROUP_ID
	, PRC.SUBGROUP_DESCRIPTION
	, ITEM
	, ITEM_DESCRIPTION
	, PRC.STORE_PACK
	, prc.SIZE_UOM
	, PRC.CUR_COST
	, PRC.PRICE_MULTIPLE_CUR
	, PRC.PRICE_CUR
	, PRC.CUR_GM_PCT
	, PRC.COST_CHANGE
	, PRC.PRICE_CHANGE
	, PRC.NEW_COST
	, PRC.PRICE_MULTIPLE_NEW
	, PRC.PRICE_NEW
	, PRC.NEW_GM_PCT
	, PRC.TARGET_MARGIN
	, PRC.TM_SOURCE
	, PRC.SRP_ADJUSTED
	, PRC.GM_ADJUSTED
	, PRC.TM_ADJUSTED
	, PRC.HOLD_CUR_PRICE
	, PRC.REVIEWED
	, CONVERT(VARCHAR(8), PRC.PRICE_CHANGE_EFFECTIVE_DATE, 1) PRICE_CHANGE_EFFECTIVE_DATE
	, PRC.UPDATE_USER_PRICE
	, PRC.UPDATE_USER_SET_TM
	, PRC.UPDATE_USER_HOLD
	, PRC.UPDATE_USER_REVIEWED
	, PRC.ftn_COST_DELTA
	, PRC.IND_MARGIN_TOL
	, pi_c.value + ' ' + pd.[description] + ' ' + ISNULL(pa_500001.attribute_event_data, '') + '/' + ISNULL(pa_500002.attribute_event_data, '') ' BREAKDOWN_ITEM'
	, pb.qty ' BREAKDOWN_QTY'
	, PB.STATE ' BREAKDOWN_STATE'
	FROM MARGINMGR.PRICESHEET_RETAIL_CHANGES PRC with (nolock)
	inner join epacube.ENTITY_ATTRIBUTE ea with (nolock) on prc.ZONE_ENTITY_STRUCTURE_FK = ea.ENTITY_STRUCTURE_FK and ea.data_name_fk = 502060
	inner join epacube.data_value dv with (nolock) on ea.DATA_VALUE_FK = dv.data_value_id and dv.value in ('H', 'R')
	left join epacube.epacube.product_bom pb with (nolock) on prc.product_structure_fk = pb.product_structure_fk and pb.data_name_fk = 500213
	left join epacube.epacube.product_identification pi_c with (nolock) on pb.child_product_structure_fk = pi_c.product_structure_fk and pi_c.data_name_fk = 110100
	left join epacube.epacube.product_description pd with (nolock) on pi_c.product_structure_fk = pd.product_structure_fk and pd.data_name_fk = 110401
	left join [epacube].[PRODUCT_ATTRIBUTE] pa_500001 with (nolock) on pi_c.Product_Structure_FK = pa_500001.PRODUCT_STRUCTURE_FK and pa_500001.data_name_fk = 500001
	left join [epacube].[PRODUCT_ATTRIBUTE] pa_500002 with (nolock) on pi_c.Product_Structure_FK = pa_500002.PRODUCT_STRUCTURE_FK and pa_500002.data_name_fk = 500002
	where 1 = 1
	and cast(prc.PRICE_CHANGE_EFFECTIVE_DATE as date) = CAST(getdate() + 6 - datepart(dw, getdate()) AS DATE)
	--and prc.ZONE_ENTITY_STRUCTURE_FK = 113231

