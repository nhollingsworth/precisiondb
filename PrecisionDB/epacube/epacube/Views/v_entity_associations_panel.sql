﻿
-- create 
CREATE
view [epacube].[v_entity_associations_panel]
as select 
 (select id_value from epacube.v_entity_identification vei where vei.es_id=w.org_entity_structure_fk 
     and vei.ei_dn_id=epacube.getParamDataNameId(epacube.getCodeRefId('ENTITY_CLASS','WAREHOUSE'),'SEARCH2')) AS whIdValue
,(select id_value from epacube.v_entity_identification vei where vei.es_id=w.org_entity_structure_fk 
     and vei.ei_dn_id=epacube.getParamDataNameId(epacube.getCodeRefId('ENTITY_CLASS','WAREHOUSE'),'SEARCH6')) AS whDescription
,(select id_value from epacube.v_entity_identification vei where vei.es_id=w.entity_structure_fk 
     and vei.ei_dn_id=epacube.getParamDataNameId(assocDataName.entity_structure_ec_cr_fk,'SEARCH2')) AS classifiedIdValue
,(select id_value from epacube.v_entity_identification vei where vei.es_id=w.entity_structure_fk 
     and vei.ei_dn_id=epacube.getParamDataNameId(assocDataName.entity_structure_ec_cr_fk,'SEARCH6')) AS classifiedDescription
,w.product_structure_fk productStructureId
,w.product_association_id productAssociationId
,w.event_id eventId
,w.org_entity_structure_fk whEntityStructureId
,w.entity_structure_fk classifiedEntityStructureId
,w.associationDataNameId
,w.event_condition_cr_fk
,w.condition
,w.errors
,assocDataName.dnid assocDataNameId
,assocDataName.data_name assocName
,assocDataName.es_entity_class entityClass
,assocDataName.entity_structure_ec_cr_fk app_sc_id
  from epacube.v_product_assoc_w_pending w WITH (NOLOCK)
       inner join epacube.v_data_names assocDataName WITH (NOLOCK) on (assocDataName.dnid=w.associationDataNameId)


