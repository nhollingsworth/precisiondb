﻿CREATE view [epacube].[v_user_groups]
as
select * 
 from epacube.user_groups ug
 inner join epacube.users u on (ug.users_fk=u.users_id)
 inner join epacube.groups g on (ug.groups_fk=g.groups_id)