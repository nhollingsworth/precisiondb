﻿

CREATE view [epacube].[V_PRODUCT_TAX_ATTRIBUTE_CURRENT_VALUES]
AS
SELECT  pc.product_structure_fk,
            tt.Taxonomy_Tree_ID AS taxonomy_tree_fk,
            pc.data_value_fk AS Tree_Assignment_DV_FK,
            ( SELECT taxonomy_node_id FROM epacube.taxonomy_node 
              WHERE data_value_fk = pc.data_value_fk ) AS Node_of_Assignment_Taxonomy_Node_fk,
            tna.TAXONOMY_NODE_FK AS Node_of_Attribute_Taxonomy_Node_fk, 
            pta.Product_Tax_Attribute_ID AS product_tax_attribute_fk,                    
            tna.Taxonomy_Node_Attribute_ID AS taxonomy_node_attribute_fk,
            dn.data_name_id AS data_name_fk,
            dn.[NAME] AS data_name,
			CASE WHEN ISNULL(pta.ATTRIBUTE_EVENT_DATA, '') <> '' 
			     THEN pta.ATTRIBUTE_EVENT_DATA
			     ELSE tna.DEFAULT_NODE_VALUE						    
            END AS current_data,                    
            CASE WHEN ISNULL(pta.ATTRIBUTE_EVENT_DATA, '') <> '' 
                 THEN pta.DATA_VALUE_FK
                 ELSE tna.DATA_VALUE_FK
            END AS CURENT_DATA_DV_FK,    
---- PTA: Value came from Product_Tax_Attribute Table ( related to node on tree )
---- NNA: Value came from Product Tax_Attribue Table ( with no node associated )
---- DTA: Value came from default on Taxonomy_Node_Attribute Table;
            CASE ISNULL(tna.TAXONOMY_NODE_FK, 0 ) 
                 WHEN 0 THEN ( CASE WHEN ISNULL(pta.ATTRIBUTE_EVENT_DATA, '') <> '' 
                                    THEN 'NNA'
								    ELSE (CASE WHEN ISNULL(tna.DEFAULT_NODE_VALUE, '') <> '' 
								          THEN 'DNA'
								          ELSE NULL
								          END )
								    END
						     )
					    ELSE 'PTA'
					    END		AS ATTRIBUTE_SOURCE,                  
            tna.DEFAULT_NODE_VALUE,
            tna.RESTRICTED_DATA_VALUES_IND ,
            dn.data_set_fk
---
-----
from epacube.taxonomy_tree tt
left JOIN epacube.product_category pc
  ON ( pc.data_name_fk = tt.DATA_NAME_FK )
CROSS APPLY epacube.Get_Product_Node_Attributes (pc.PRODUCT_STRUCTURE_FK ) AS node
INNER JOIN epacube.TAXONOMY_NODE_ATTRIBUTE tna 
      ON ( tna.TAXONOMY_NODE_ATTRIBUTE_ID = node.taxonomy_node_attribute_fk )
INNER JOIN epacube.DATA_NAME DN ON ( DN.DATA_NAME_ID = TNA.DATA_NAME_FK )  
LEFT JOIN epacube.PRODUCT_TAX_ATTRIBUTE	pta 
      ON ( pta.product_structure_fk = pc.PRODUCT_STRUCTURE_FK )
