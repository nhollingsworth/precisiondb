﻿
CREATE view 
[epacube].[v_entity_identification]
as 
select 
es.entity_structure_id es_id
,ei.entity_data_name_fk ei_entity_dn_id
,es.data_name_fk es_dn_id
,ei.data_name_fk ei_dn_id
,eccr.code es_entity_class  
,esedn.name es_entity_data_name
,esdn.name es_data_name
,eidn.name ei_data_name
,eidn.LABEL id_label
,ei.value id_value
,ein.value id_name
  from epacube.entity_structure es
       inner join epacube.entity_identification ei on (ei.entity_Structure_fk=es.entity_Structure_id)
       inner join epacube.code_ref eccr on (eccr.CODE_REF_ID=es.ENTITY_CLASS_CR_FK)
       inner join epacube.data_name esdn on (esdn.data_name_id=es.data_name_fk)
       inner join epacube.data_name eidn on (eidn.data_name_id=ei.data_name_fk)
       inner join epacube.data_name esedn on (esedn.data_name_id=ei.entity_data_name_fk)
	   left join epacube.ENTITY_IDENT_NONUNIQUE ein on es.entity_structure_id = ein.ENTITY_STRUCTURE_FK and ei.ENTITY_DATA_NAME_FK = ein.entity_data_name_fk and right(ein.data_name_fk, 3) = '112'
where es.record_Status_cr_fk=1 and ei.record_Status_cr_fk=1
