﻿

CREATE VIEW [epacube].[V_PRODUCT_VALUES_RESULTS] AS
SELECT 
sr.product_structure_fk product_structure_id
,sr.sheet_results_id vr_id
,srdn.dnid
,srdn.label 
--will replace with inner join
,(select code from epacube.code_ref
 where code_ref_id = (select product_status_cr_fk from epacube.product_structure
					  where product_structure_id = sr.product_structure_fk)) PRODUCT_STATUS
--
,(select id_value from epacube.v_entity_identification vei 
   where es_id=sr.entity_structure_fk and ei_data_name='VENDOR NUMBER') VENDOR_NUMBER
,(select id_value from epacube.v_entity_identification vei 
   where es_id=sr.entity_structure_fk and ei_dn_id=epacube.getParamDataNameId('APPLICATION','CUSTOMER')) CUSTOMER
,(select pi.value from epacube.product_identification pi 
   where pi.product_structure_fk=sr.product_structure_fk
     and pi.entity_structure_fk=sr.entity_structure_fk 
     and pi.data_name_fk=epacube.getDataNameId('VENDOR PART NUMBER')) VPN
,(select pi.value from epacube.product_identification pi 
   where pi.product_structure_fk=sr.product_structure_fk
     and pi.data_name_fk=epacube.getParamDataNameId('APPLICATION','PRODUCT')) PRODUCT
,(select pi.value from epacube.product_identification pi 
   where pi.product_structure_fk=sr.product_structure_fk
     and pi.data_name_fk=epacube.getDataNameId('UPC')) UPC
,isnull(
 (select pd.description from epacube.product_description pd
   where pd.product_structure_fk=sr.product_structure_fk
     and pd.data_name_fk=epacube.getParamDataNameId('APPLICATION','DESCRIPTION'))
 ,(select pd.description from epacube.product_description pd
   where pd.product_structure_fk=sr.product_structure_fk
     and pd.data_name_fk=epacube.getDataNameId('DESCRIPTION LINE 1')
   )
   ) DESCRIPTION
,(select uc.uom_code from epacube.product_uom_class puc
  inner join epacube.uom_code uc on (uc.uom_code_id=puc.uom_code_fk)
  where puc.product_structure_fk=sr.product_Structure_fk
    and puc.org_entity_structure_fk=sr.org_entity_structure_fk
    and puc.entity_Structure_fk=sr.entity_structure_fk
    and puc.data_name_fk = (select uom_class_data_name_fk from epacube.data_name where data_name_id=sr.data_name_fk)) UOM 
,(select ei.value from epacube.entity_identification ei 
   where entity_structure_fk=sr.org_entity_structure_fk and ei.data_name_fk=epacube.getParamDataNameId('APPLICATION','WAREHOUSE')) WAREHOUSE
,(select count(1) from synchronizer.event_data esr 
   where event_status_cr_fk=epacube.getCodeRefId('EVENT_STATUS','PENDING') 
     and esr.product_structure_fk=sr.product_structure_fk
     and esr.event_type_cr_fk = epacube.getCodeRefId('EVENT_TYPE','RESULTS')
  ) pending_events
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE1' )
   WHERE DNX.PARENT_DATA_NAME_FK = sr.DATA_NAME_FK ) AS COLUMN1
,sr.net_value1
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE2' )
   WHERE DNX.PARENT_DATA_NAME_FK = sr.DATA_NAME_FK ) AS COLUMN2
,sr.net_value2
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE3' )
   WHERE DNX.PARENT_DATA_NAME_FK = sr.DATA_NAME_FK ) AS COLUMN3
,sr.net_value3
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE4' )
   WHERE DNX.PARENT_DATA_NAME_FK = sr.DATA_NAME_FK ) AS COLUMN4
,sr.net_value4
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE5' )
   WHERE DNX.PARENT_DATA_NAME_FK = sr.DATA_NAME_FK ) AS COLUMN5
,sr.net_value5
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE6' )
   WHERE DNX.PARENT_DATA_NAME_FK = sr.DATA_NAME_FK ) AS COLUMN6
,sr.net_value6
,sr.end_date
,sr.effective_date effective_date
,sr.update_timestamp
,sr.record_status_cr_fk srstatusfk
,sr.record_status_cr_fk psstatusfk
,NULL AS values_sheet_name
,NULL QTY_PER
,(select UOM_CODE from epacube.UOM_CODE
  where UOM_CODE_ID = sr.VALUE_UOM_CODE_FK) UOM_CODE
FROM marginmgr.sheet_results sr WITH (NOLOCK)
LEFT JOIN epacube.v_data_names srdn WITH (NOLOCK) on (sr.data_name_fk=srdn.dnid and (srdn.EVENT_TYPE='RESULTS'))

            
                    UNION


SELECT 
pv.product_structure_fk product_structure_id
,pv.product_values_id vr_id
,pvdn.dnid
,pvdn.label 
,(select code from epacube.code_ref
 where code_ref_id = (select product_status_cr_fk from epacube.product_structure
					  where product_structure_id = pv.product_structure_fk)) PRODUCT_STATUS
,(select id_value from epacube.v_entity_identification vei 
   where es_id=pv.entity_structure_fk and ei_data_name='VENDOR NUMBER') VENDOR_NUMBER
,(select id_value from epacube.v_entity_identification vei 
   where es_id=pv.entity_structure_fk and ei_dn_id=epacube.getParamDataNameId('APPLICATION','CUSTOMER')) CUSTOMER
,(select pi.value from epacube.product_identification pi 
   where pi.product_structure_fk=pv.product_structure_fk 
     and pi.entity_structure_fk=pv.entity_structure_fk 
     and pi.data_name_fk=epacube.getDataNameId('VENDOR PART NUMBER')) VPN
,(select pi.value from epacube.product_identification pi 
   where pi.product_structure_fk=pv.product_structure_fk 
     and pi.data_name_fk=epacube.getParamDataNameId('APPLICATION','PRODUCT')) PRODUCT
,(select pi.value from epacube.product_identification pi 
   where pi.product_structure_fk=pv.product_structure_fk 
     and pi.data_name_fk=epacube.getDataNameId('UPC')) UPC
,isnull(
 (select pd.description from epacube.product_description pd
   where pd.product_structure_fk=pv.product_structure_fk
     and pd.data_name_fk=epacube.getParamDataNameId('APPLICATION','DESCRIPTION'))
 ,(select pd.description from epacube.product_description pd
   where pd.product_structure_fk=pv.product_structure_fk
     and pd.data_name_fk=epacube.getDataNameId('DESCRIPTION LINE 1')
   )
   ) DESCRIPTION
,(select uc.uom_code from epacube.product_uom_class puc
  inner join epacube.uom_code uc on (uc.uom_code_id=puc.uom_code_fk)
  where puc.product_structure_fk=pv.product_Structure_fk
    and puc.org_entity_structure_fk=pv.org_entity_structure_fk
    and puc.entity_Structure_fk=pv.entity_structure_fk
    and puc.data_name_fk = (select uom_class_data_name_fk from epacube.data_name where data_name_id=pv.data_name_fk)) UOM 
,(select ei.value from epacube.entity_identification ei 
   where entity_structure_fk=pv.org_entity_structure_fk and ei.data_name_fk=epacube.getParamDataNameId('APPLICATION','WAREHOUSE')) WAREHOUSE
,(select count(1) from synchronizer.event_data epv 
   where event_status_cr_fk=epacube.getCodeRefId('EVENT_STATUS','PENDING') 
     and epv.product_structure_fk=pv.product_structure_fk
     and epv.event_type_cr_fk = epacube.getCodeRefId('EVENT_TYPE','VALUES')
  ) pending_events
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE1' )
   WHERE DNX.PARENT_DATA_NAME_FK = pv.DATA_NAME_FK ) AS COLUMN1
,pv.net_value1
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE2' )
   WHERE DNX.PARENT_DATA_NAME_FK = pv.DATA_NAME_FK ) AS COLUMN2
,pv.net_value2
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE3' )
   WHERE DNX.PARENT_DATA_NAME_FK = pv.DATA_NAME_FK ) AS COLUMN3
,pv.net_value3
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE4' )
   WHERE DNX.PARENT_DATA_NAME_FK = pv.DATA_NAME_FK ) AS COLUMN4
,pv.net_value4
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE5' )
   WHERE DNX.PARENT_DATA_NAME_FK = pv.DATA_NAME_FK ) AS COLUMN5
,pv.net_value5
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE6' )
   WHERE DNX.PARENT_DATA_NAME_FK = pv.DATA_NAME_FK ) AS COLUMN6
   ,pv.net_value6
,pv.end_date
,pv.value_effective_date effective_date
,pv.update_timestamp
,pv.record_status_cr_fk statusfk
,pv.record_status_cr_fk pvstatusfk
,VALUES_SHEET_NAME AS values_sheet_name
,NULL QTY_PER
,(select UOM_CODE from epacube.UOM_CODE
  where UOM_CODE_ID = pv.VALUE_UOM_CODE_FK) UOM_CODE
 FROM  epacube.product_values pv WITH (NOLOCK)
LEFT JOIN epacube.v_data_names pvdn WITH (NOLOCK) on (pv.data_name_fk=pvdn.dnid and (pvdn.EVENT_TYPE='VALUES'))

UNION
select 
psh.product_structure_fk product_structure_id
, psh.pricesheet_id vr_id
, pshdn.dnid
, pshdn.label
,(select code from epacube.code_ref
 where code_ref_id = (select product_status_cr_fk from epacube.product_structure
					  where product_structure_id = psh.product_structure_fk)) PRODUCT_STATUS
,(select id_value from epacube.v_entity_identification vei 
   where es_id=psh.entity_structure_fk and ei_data_name='VENDOR NUMBER') VENDOR_NUMBER
,(select id_value from epacube.v_entity_identification vei 
   where es_id=psh.entity_structure_fk and ei_dn_id=epacube.getParamDataNameId('APPLICATION','CUSTOMER')) CUSTOMER
,(select pi.value from epacube.product_identification pi 
   where pi.product_structure_fk=psh.product_structure_fk 
     and pi.entity_structure_fk=psh.entity_structure_fk 
     and pi.data_name_fk=epacube.getDataNameId('VENDOR PART NUMBER')) VPN
,(select pi.value from epacube.product_identification pi 
   where pi.product_structure_fk=psh.product_structure_fk 
     and pi.data_name_fk=epacube.getParamDataNameId('APPLICATION','PRODUCT')) PRODUCT
,(select pi.value from epacube.product_identification pi 
   where pi.product_structure_fk=psh.product_structure_fk 
     and pi.data_name_fk=epacube.getDataNameId('UPC')) UPC
,isnull(
 (select pd.description from epacube.product_description pd
   where pd.product_structure_fk=psh.product_structure_fk
     and pd.data_name_fk=epacube.getParamDataNameId('APPLICATION','DESCRIPTION'))
 ,(select pd.description from epacube.product_description pd
   where pd.product_structure_fk=psh.product_structure_fk
     and pd.data_name_fk=epacube.getDataNameId('DESCRIPTION LINE 1')
   )
   ) DESCRIPTION
,(select uc.uom_code from epacube.product_uom_class puc
  inner join epacube.uom_code uc on (uc.uom_code_id=puc.uom_code_fk)
  where puc.product_structure_fk=psh.product_Structure_fk
    and puc.org_entity_structure_fk=psh.org_entity_structure_fk
    and puc.entity_Structure_fk=psh.entity_structure_fk
    and puc.data_name_fk = (select uom_class_data_name_fk from epacube.data_name where data_name_id=psh.data_name_fk)) UOM 
,(select ei.value from epacube.entity_identification ei 
   where entity_structure_fk=psh.org_entity_structure_fk and ei.data_name_fk=epacube.getParamDataNameId('APPLICATION','WAREHOUSE')) WAREHOUSE
,(select count(1) from synchronizer.event_data epsh 
   where event_status_cr_fk=epacube.getCodeRefId('EVENT_STATUS','PENDING') 
     and epsh.product_structure_fk=psh.product_structure_fk
     and epsh.event_type_cr_fk = epacube.getCodeRefId('EVENT_TYPE','VALUES')
  ) pending_events
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE1' )
   WHERE DNX.PARENT_DATA_NAME_FK = psh.DATA_NAME_FK ) AS COLUMN1
,psh.net_value1
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE2' )
   WHERE DNX.PARENT_DATA_NAME_FK = psh.DATA_NAME_FK ) AS COLUMN2
,psh.net_value2
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE3' )
   WHERE DNX.PARENT_DATA_NAME_FK = psh.DATA_NAME_FK ) AS COLUMN3
,psh.net_value3
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE4' )
   WHERE DNX.PARENT_DATA_NAME_FK = psh.DATA_NAME_FK ) AS COLUMN4
,psh.net_value4
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE5' )
   WHERE DNX.PARENT_DATA_NAME_FK = psh.DATA_NAME_FK ) AS COLUMN5
,psh.net_value5
,( SELECT DNX.LABEL
   FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
   INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK
													AND  DSX.COLUMN_NAME = 'NET_VALUE6' )
   WHERE DNX.PARENT_DATA_NAME_FK = psh.DATA_NAME_FK ) AS COLUMN6
   ,psh.net_value6
,psh.end_date
,psh.effective_date effective_date
,psh.update_timestamp
,psh.record_status_cr_fk statusfk
,psh.record_status_cr_fk pshstatusfk
,PRICESHEET_NAME AS values_sheet_name
,PRICESHEET_QTY_PER QTY_PER
,(select UOM_CODE from epacube.UOM_CODE
  where UOM_CODE_ID = psh.VALUE_UOM_CODE_FK) UOM_CODE
FROM  marginmgr.pricesheet psh
LEFT JOIN epacube.v_data_names pshdn WITH (NOLOCK) on (psh.data_name_fk=pshdn.dnid and (pshdn.EVENT_TYPE='PRICESHEET'))

