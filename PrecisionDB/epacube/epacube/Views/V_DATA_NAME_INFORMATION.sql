﻿
CREATE VIEW [epacube].[V_DATA_NAME_INFORMATION]
( 
  DATA_NAME_FK
 ,NAME
 ,DATA_SET_FK
 ,TABLE_NAME
 ,DATA_TYPE
 ,CORP_IND
 ,ORG_IND
 ,RELATED_ENTITY_CLASS
 ,DATA_NAME_ENTITY_CLASS       
 ,DATA_TYPE_CR_FK    
 ,ENTITY_CLASS_CR_FK  
 ,ENTITY_STRUCTURE_EC_CR_FK  
 ,RECORD_STATUS_CR_FK
)
AS
SELECT 
  DN.DATA_NAME_ID AS DATA_NAME_FK
 ,DN.NAME
 ,DN.DATA_SET_FK
 ,DS.TABLE_NAME
 ,( SELECT CODE FROM EPACUBE.CODE_REF WHERE CODE_REF_ID = DS.DATA_TYPE_CR_FK ) DATA_TYPE
 ,DS.CORP_IND
 ,DS.ORG_IND
 ,( SELECT CODE FROM EPACUBE.CODE_REF WHERE CODE_REF_ID = ENTITY_STRUCTURE_EC_CR_FK )
      AS RELATED_ENTITY_CLASS
 ,( SELECT CODE FROM EPACUBE.CODE_REF WHERE CODE_REF_ID = ENTITY_CLASS_CR_FK )
      AS DATA_NAME_ENTITY_CLASS       
 ,DS.DATA_TYPE_CR_FK    
 ,DS.ENTITY_CLASS_CR_FK  
 ,DS.ENTITY_STRUCTURE_EC_CR_FK  
 ,DN.RECORD_STATUS_CR_FK
FROM epacube.DATA_NAME DN
INNER JOIN EPACUBE.DATA_SET DS ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
