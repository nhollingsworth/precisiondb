﻿CREATE VIEW [epacube].[V_USER_DATA_PERMISSIONS]
AS
select up.* 
          ,pdn.data_name_fk
          ,(select name from epacube.data_name where data_name_id=pdn.data_name_fk) data_name
  from epacube.v_user_permissions up
 inner join epacube.permission_data_names pdn on  (up.permissions_id=pdn.permissions_fk)