﻿

-- =============================================
-- Author:		Yuri Nogin
-- Create date: 5/9/2006
-- Description:	Selects all status info for specified exec_id
-- =============================================
CREATE PROCEDURE [exec_monitor].[Select_Status_sp]
(
	
	@exec_id bigint
)
AS
BEGIN
	-- Declare the return variable here
	SELECT  convert(varchar(50),create_dt,109) 
                                  + ' - User: ' + ltrim(rtrim(a.create_user_id)) 
                                  + ' - Host ID: ' +  ltrim(rtrim(a.host_id)) 
                                  + ' - Host Name: ' + ltrim(rtrim(a.host_name)) 
                                  + ' - Status: ' + ltrim(rtrim(a.status_desc))
                                  + ' - EpaCube Job ID: ' +  isnull(cast(epa_job_id as varchar(40)),'Not Specified') 
								  FROM [exec_monitor].[StatusLog] a
								  WHERE [exec_id] = @exec_id order by create_dt;

END


