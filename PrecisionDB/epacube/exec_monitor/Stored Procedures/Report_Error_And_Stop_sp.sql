﻿


-- =============================================
-- Author:		Yuri Nogin
-- Create date: 5/9/2006
-- Description:	Saves the error info in the exec_monitor.ErrLog table
-- Modified:    7/27/2008 By Yuri Nogin. Added epa_job_id parameter
-- =============================================
CREATE PROCEDURE [exec_monitor].[Report_Error_And_Stop_sp] 
	-- Add the parameters for the stored procedure here
	@exec_id bigint,
    @user_error_desc varchar(4000) = NULL,
    @create_user_id varchar(50) = NULL,
    @host_id varchar(50) = NULL,
	@host_name varchar(50) = NULL,
    @error_number int = NULL,
	@error_line int = NULL,
    @error_severity int = NULL,
    @error_procedure varchar(126) = NULL,
    @error_messsage varchar(4000) = NULL,
    @allow_commit smallint = 1 , -- 1 not in trigger, 0 in trigger
    @epa_job_id BIGINT = NULL 
AS
BEGIN
   DECLARE @Error_State INT;
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    EXEC exec_monitor.Report_Error_sp @exec_id ,
    @user_error_desc,
    @create_user_id,
    @host_id,
	@host_name,
    @error_number,
	@error_line,
    @error_severity,
    @error_procedure,
    @error_messsage,
    @allow_commit,
    @epa_job_id;
    SET @Error_State = ERROR_STATE();
    RAISERROR (@error_messsage, -- Message text.
               @error_severity, -- Severity.
               @Error_State -- State.
               );

END









