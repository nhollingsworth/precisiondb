﻿



-- =============================================
-- Author:		Yuri Nogin
-- Create date: 5/9/2006
-- Description:	Prints all status info for specified exec_id
-- =============================================
CREATE PROCEDURE [exec_monitor].[Print_Status_sp]
(
	
	@exec_id bigint
)
AS
BEGIN
    BEGIN TRY
	-- Declare the return variable here
	DECLARE @Line varchar(max);
    DECLARE cur_stmt cursor  for SELECT  convert(varchar(50),create_dt,109) 
                                  + Char(9) + Char(13) + ' - User: ' + ltrim(rtrim(a.create_user_id)) 
                                  + Char(9) + Char(13) + ' - Host ID: ' +  ltrim(rtrim(a.host_id)) 
                                  + Char(9) + Char(13) + ' - Host Name: ' + ltrim(rtrim(a.host_name)) 
                                  + Char(9) + Char(13) + ' - Information: ' + ltrim(rtrim(a.status_desc))
                                  + Char(9) + Char(13) + ' - EpaCube Job ID: ' +  isnull(cast(epa_job_id as varchar(40)),'Not Specified') 
								  FROM [exec_monitor].[StatusLog] a
								  WHERE [exec_id] = @exec_id order by create_dt
                                  FOR READ ONLY;

	OPEN cur_stmt;
    FETCH NEXT FROM cur_stmt INTO @Line;

    WHILE @@FETCH_STATUS = 0
		BEGIN
		    Print @Line;
			FETCH NEXT FROM cur_stmt INTO @Line;
		END;
    CLOSE cur_stmt;
	DEALLOCATE cur_stmt;
    END TRY
    BEGIN CATCH
		 EXEC exec_monitor.Report_Error_sp @exec_id = @exec_id
    END CATCH;

		
END




