﻿







-- =============================================
-- Author:		Yuri Nogin
-- Create date: 5/9/2006
-- Description:	Prints all error info for specified exec_id
-- =============================================
CREATE PROCEDURE [exec_monitor].[Print_Error_sp]
(
	
	@exec_id bigint
)
AS
BEGIN
    BEGIN TRY
	-- Declare the return variable here
	DECLARE @Line varchar(max);
    DECLARE cur_stmt cursor  for SELECT  convert(varchar(50),create_dt,109) 
                                 + Char(9) + Char(13) + ' - User Message: ' + isnull(user_error_desc,'Not Specified') 
                                 + Char(9) + Char(13) + ' - Error Number: ' + CAST(error_number as varchar(40)) 
                                 + Char(9) + Char(13) + ' - Error Line: ' +  cast(error_line as varchar(40)) 
                                 + Char(9) + Char(13) + ' - Error Severity: ' + cast(error_severity as varchar(20)) 
                                 + Char(9) + Char(13) + ' - Error Procedure: ' +  ltrim(rtrim(isnull(error_procedure,'N/A'))) 
                                 + Char(9) + Char(13) + ' - Error Message: ' +  ltrim(rtrim(error_messsage))
                                 + Char(9) + Char(13) + ' - EpaCube Job ID: ' +  isnull(cast(epa_job_id as varchar(40)),'Not Specified') 
								  FROM [exec_monitor].[ErrLog]
								  WHERE [exec_id] = @exec_id order by create_dt
                                  FOR READ ONLY;

	OPEN cur_stmt;
    FETCH NEXT FROM cur_stmt INTO @Line;

    WHILE @@FETCH_STATUS = 0
		BEGIN
		    Print @Line;
			FETCH NEXT FROM cur_stmt INTO @Line;
		END;
    CLOSE cur_stmt;
	DEALLOCATE cur_stmt;
    END TRY
    BEGIN CATCH
		 EXEC exec_monitor.Report_Error_sp @exec_id = @exec_id
    END CATCH;

		
END








