﻿





-- =============================================
-- Author:		Yuri Nogin
-- Create date: 5/9/2006
-- Description:	Emails all status info for specified exec_id
-- =============================================
CREATE PROCEDURE [exec_monitor].[Email_Status_sp]
(
	
	@exec_id bigint,
    @recipients varchar(max)
)
AS
BEGIN
    BEGIN TRY
	-- Declare the return variable here
	DECLARE @subject varchar(1000);
    DECLARE @Line nvarchar(max);
    DECLARE @body nvarchar(max);
    DECLARE cur_stmt cursor  for SELECT  isnull(convert(varchar(50),create_dt,109) 
                                  + Char(9) + Char(13) + ' - User: ' + ltrim(rtrim(a.create_user_id)) 
                                  + Char(9) + Char(13) + ' - Host ID: ' +  ltrim(rtrim(a.host_id)) 
                                  + Char(9) + Char(13) + ' - Host Name: ' + ltrim(rtrim(a.host_name)) 
                                  + Char(9) + Char(13) + ' - Information: ' + ltrim(rtrim(a.status_desc)),'')
                                  + Char(9) + Char(13) + ' - EpaCube Job ID: ' +  isnull(cast(epa_job_id as varchar(40)),'Not Specified') 
								  FROM [exec_monitor].[StatusLog] a
								  WHERE [exec_id] = @exec_id order by create_dt
                                  FOR READ ONLY;

	SET @body = '';
    SET @Line = '';
    OPEN cur_stmt;
    FETCH NEXT FROM cur_stmt INTO @Line;

    WHILE @@FETCH_STATUS = 0
		BEGIN
		    Set @body  = @body + @Line + Char(10) + Char(13);
			FETCH NEXT FROM cur_stmt INTO @Line;
		END;
    CLOSE cur_stmt;
	DEALLOCATE cur_stmt;
	Set @subject = 'Status Info for Execution No ' + cast(@exec_id as varchar(20)) + ' From SQL Server: ' + @@ServerName;
    
EXEC msdb.dbo.sp_send_dbmail
    @recipients = @recipients,
    @body = @body,
    @subject = @subject,
    @importance = 'Normal'; 
END TRY
BEGIN CATCH
		 EXEC exec_monitor.Report_Error_sp @exec_id = @exec_id
END CATCH;
   
    
END






