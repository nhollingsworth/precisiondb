﻿





-- =============================================
-- Author:		Yuri Nogin
-- Create date: 5/9/2006
-- Description:	Selects all error info for specified exec_id
-- =============================================
CREATE PROCEDURE [exec_monitor].[Select_Error_sp]
(
	
	@exec_id bigint
)
AS
BEGIN
	 SELECT  convert(varchar(50),create_dt,109) + ' - ' + ' User Message: ' + isnull(user_error_desc,'Not Specified') 
                                 + ' - Error Number: ' + CAST(error_number as varchar(40)) 
                                 + ' - Error Line: ' +  cast(error_line as varchar(40)) 
                                 + ' - Error Severity: ' + cast(error_severity as varchar(20)) 
                                 + ' - Error Procedure: ' +  ltrim(rtrim(isnull(error_procedure,'N/A'))) 
                                 + ' - Error Message: ' +  ltrim(rtrim(error_messsage))
                                 + ' - EpaCube Job ID: ' +  isnull(cast(epa_job_id as varchar(40)),'Not Specified') 
								  FROM [exec_monitor].[ErrLog]
								  WHERE [exec_id] = @exec_id order by create_dt;

END






