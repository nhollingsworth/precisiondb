﻿
-- Stored Procedure










-- =============================================
-- Author:		Yuri Nogin
-- Create date: 1/29/2006
-- Description:	Saves the error info in the exec_monitor.ErrLog table
-- Modified:    7/27/2008 By Yuri Nogin. Added epa_job_id parameter
-- =============================================
CREATE PROCEDURE [exec_monitor].[Report_Error_sp] 
	-- Add the parameters for the stored procedure here
	@exec_id bigint,
    @user_error_desc varchar(4000) = 'UNSPECIFIED',
    @create_user_id varchar(50) = NULL,
    @host_id varchar(50) = NULL,
	@host_name varchar(50) = NULL,
    @error_number int = NULL,
	@error_line int = NULL,
    @error_severity int = NULL,
    @error_procedure varchar(126) = NULL,
    @error_messsage varchar(4000) = NULL,
    @allow_commit smallint = 1, -- 1 not in trigger, 0 in trigger
    @epa_batch_no BIGINT = NULL, 
    @epa_job_id BIGINT = NULL     
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRY
       if @allow_commit > 0 
       Begin
			BEGIN TRANSACTION
       End
        set @create_user_id = IsNull(@create_user_id,suser_name());
        set @host_id = IsNull(@host_id,host_id());
		set @host_name = IsNull(@host_name,host_name());
        set @error_number = IsNull(@error_number,error_number());
        set @error_line = IsNull(@error_line,error_line());
        set @error_severity = IsNull(@error_severity,error_severity());        
        set @error_procedure = IsNull(@error_procedure,error_procedure());
        set @error_messsage = IsNull(@error_messsage,error_message());

		INSERT into exec_monitor.ErrLog 
                   (exec_id,create_user_id,
                    host_id,host_name,
                    user_error_desc,error_number,
                    error_line,error_severity,
                    error_procedure,error_messsage,epa_job_id)
		VALUES     (@exec_id,@create_user_id,
                    @host_id,@host_name,
                    @user_error_desc,@error_number,
                    @error_line,@error_severity,
                    @error_procedure,@error_messsage,@epa_job_id);
           if @allow_commit > 0 
           Begin
			COMMIT TRANSACTION;
           End
    END TRY
    BEGIN CATCH
           if @allow_commit > 0 and XACT_STATE() <> 0
           Begin
              	ROLLBACK;
           End
    END CATCH;
END











