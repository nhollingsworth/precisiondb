﻿
-- Stored Procedure







-- =============================================
-- Author:		Yuri Nogin
-- Create date: 1/29/2006
-- Description:	Saves the status message in the exec_monitor.StatusLog table
-- Modified:    7/27/2008 By Yuri Nogin. Added epa_job_id parameter
-- =============================================
CREATE PROCEDURE [exec_monitor].[Report_Status_sp] 
	-- Add the parameters for the stored procedure here
	@exec_id bigint,
    @status_desc varchar(max),
    @create_user_id varchar(50) = NULL,
    @host_id varchar(50) = NULL,
	@host_name varchar(50) = NULL,
    @allow_commit smallint = 1, -- 1 not in trigger, 0 in trigger
	@epa_job_id BIGINT = NULL,
	@epa_batch_no BIGINT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRY
           if @allow_commit > 0 
           Begin
				BEGIN TRANSACTION
           End
		INSERT into exec_monitor.StatusLog (exec_id,create_user_id,host_id,host_name,status_desc,epa_job_id, epa_batch_no)
		VALUES( @exec_id,
                IsNull(@create_user_id,suser_name()),
                IsNull(@host_id,host_id()),
                IsNull(@host_name,host_name()),
                @status_desc,
                @epa_job_id,
                @epa_batch_no);
           if @allow_commit > 0 
           Begin
				COMMIT TRANSACTION;
           End
    END TRY
    BEGIN CATCH
		   if @allow_commit > 0 
           Begin
				ROLLBACK;
           End
    END CATCH;
END








