﻿








-- =============================================
-- Author:		Yuri Nogin
-- Create date: 1/29/2006
-- Description:	Creates a new Exec ID in the exec_monitor.ExecLog table
-- Modified:    7/27/2008 By Yuri Nogin. Added epa_job_id parameter
-- =============================================
CREATE PROCEDURE [exec_monitor].[Start_Monitor_sp] 
	-- Add the parameters for the stored procedure here
    @exec_desc varchar(256) = null,
    @exec_id bigint output,
    @create_user_id varchar(50) = NULL,
    @host_id varchar(50) = NULL,
	@host_name varchar(50) = NULL,
    @allow_commit smallint = 1, -- 1 not in trigger, 0 in trigger
    @epa_job_id BIGINT = NULL, 
    @epa_batch_no BIGINT = NULL     
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    BEGIN TRY
       DECLARE @MyTableVar table( exec_id bigint);
       if @allow_commit > 0 
       Begin
			Begin Transaction
       End
		INSERT into exec_monitor.ExecLog (create_user_id,host_id,host_name,exec_desc,epa_job_id,epa_batch_no)
        OUTPUT INSERTED.exec_id 
        INTO @MyTableVar
		VALUES( IsNull(@create_user_id,suser_name()),
                IsNull(@host_id,host_id()),
                IsNull(@host_name,host_name())
                ,@exec_desc,@epa_job_id,@epa_batch_no);
           if @allow_commit > 0 
           Begin
				COMMIT Transaction;
           End
		Select top 1 @exec_id = exec_id from @MyTableVar;
    END TRY
    BEGIN CATCH
		   if @allow_commit > 0 
           Begin
				ROLLBACK;
           End
		Set @exec_id = -99;
    END CATCH;

   
END









