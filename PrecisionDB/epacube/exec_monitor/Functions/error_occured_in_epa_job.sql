﻿-- =============================================
-- Author:		Yuri Nogin
-- Create date: 5/9/2006
-- Description:	Returns 1 if error occured otherwise 0
-- =============================================
CREATE  FUNCTION [exec_monitor].[error_occured_in_epa_job]
(
	
	@epa_job_id bigint
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResVar bit;
    DECLARE cur_stmt cursor  for SELECT    TOP 1 1
								  FROM [exec_monitor].[ErrLog]
								  WHERE [epa_job_id] = @epa_job_id FOR READ ONLY;

	OPEN cur_stmt;
    FETCH NEXT FROM cur_stmt INTO @ResVar;
    CLOSE cur_stmt;
	DEALLOCATE cur_stmt;
		

	-- Return the result of the function
	RETURN isnull(@ResVar,0)

END
