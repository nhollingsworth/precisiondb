﻿CREATE TABLE [exec_monitor].[ErrLog] (
    [exec_id]         BIGINT         NOT NULL,
    [create_dt]       DATETIME       CONSTRAINT [DF_ErrLog_create_dt] DEFAULT (getdate()) NOT NULL,
    [create_user_id]  VARCHAR (50)   CONSTRAINT [DF_ErrLog_create_user_id] DEFAULT (suser_name()) NOT NULL,
    [host_id]         VARCHAR (50)   CONSTRAINT [DF_ErrLog_host_id] DEFAULT (host_id()) NOT NULL,
    [host_name]       VARCHAR (50)   CONSTRAINT [DF_ErrLog_host_name] DEFAULT (host_name()) NOT NULL,
    [error_number]    INT            CONSTRAINT [DF_ErrLog_error_number] DEFAULT (error_number()) NULL,
    [error_line]      INT            CONSTRAINT [DF_ErrLog_error_line] DEFAULT (error_line()) NULL,
    [error_severity]  INT            CONSTRAINT [DF_ErrLog_error_severity] DEFAULT (error_severity()) NULL,
    [error_procedure] VARCHAR (126)  CONSTRAINT [DF_ErrLog_error_procedure] DEFAULT (error_procedure()) NULL,
    [error_messsage]  VARCHAR (4000) CONSTRAINT [DF_ErrLog_error_messsage] DEFAULT (error_message()) NULL,
    [user_error_desc] VARCHAR (4000) NULL,
    [epa_job_id]      BIGINT         NULL,
    [epa_batch_no]    BIGINT         NULL,
    CONSTRAINT [FK_ErrLog_ExecLog] FOREIGN KEY ([exec_id]) REFERENCES [exec_monitor].[ExecLog] ([exec_id])
);




GO
CREATE NONCLUSTERED INDEX [IX_ErrLog]
    ON [exec_monitor].[ErrLog]([exec_id] ASC) WITH (FILLFACTOR = 80);

