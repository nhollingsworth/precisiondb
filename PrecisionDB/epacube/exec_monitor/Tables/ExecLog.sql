﻿CREATE TABLE [exec_monitor].[ExecLog] (
    [exec_id]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [create_dt]      DATETIME      CONSTRAINT [DF_ExecLog_create_dt] DEFAULT (getdate()) NOT NULL,
    [create_user_id] VARCHAR (50)  CONSTRAINT [DF_ExecLog_create_user_id] DEFAULT (suser_name()) NOT NULL,
    [host_id]        VARCHAR (50)  CONSTRAINT [DF_ExecLog_host_id] DEFAULT (host_id()) NOT NULL,
    [host_name]      VARCHAR (50)  CONSTRAINT [DF_ExecLog_host_name] DEFAULT (host_name()) NOT NULL,
    [exec_desc]      VARCHAR (MAX) NULL,
    [epa_job_id]     BIGINT        NULL,
    [epa_batch_no]   BIGINT        NULL,
    CONSTRAINT [PK_ExecLog] PRIMARY KEY CLUSTERED ([exec_id] ASC) WITH (FILLFACTOR = 80)
);



