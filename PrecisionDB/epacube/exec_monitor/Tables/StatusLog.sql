﻿CREATE TABLE [exec_monitor].[StatusLog] (
    [exec_id]        BIGINT        NOT NULL,
    [create_dt]      DATETIME      CONSTRAINT [DF_StatusLog_create_dt] DEFAULT (getdate()) NOT NULL,
    [create_user_id] VARCHAR (50)  CONSTRAINT [DF_StatusLog_create_user_id] DEFAULT (suser_name()) NOT NULL,
    [host_id]        VARCHAR (50)  CONSTRAINT [DF_StatusLog_host_id] DEFAULT (host_id()) NOT NULL,
    [host_name]      VARCHAR (50)  CONSTRAINT [DF_StatusLog_host_name] DEFAULT (host_name()) NOT NULL,
    [status_desc]    VARCHAR (MAX) NULL,
    [epa_job_id]     BIGINT        NULL,
    [epa_batch_no]   BIGINT        NULL,
    CONSTRAINT [FK_StatusLog_ExecLog] FOREIGN KEY ([exec_id]) REFERENCES [exec_monitor].[ExecLog] ([exec_id])
);



