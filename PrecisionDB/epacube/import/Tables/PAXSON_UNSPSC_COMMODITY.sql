﻿CREATE TABLE [import].[PAXSON_UNSPSC_COMMODITY] (
    [COMM_CODE]            VARCHAR (50)  NULL,
    [CommodityDescription] VARCHAR (256) NULL,
    [UNSPSC]               VARCHAR (256) NULL,
    [UNSPSCTitle]          VARCHAR (256) NULL
);

