﻿CREATE TABLE [import].[IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE] (
    [IMPORT_FILENAME]                         VARCHAR (100)   NULL,
    [Job_FK]                                  BIGINT          NULL,
    [transtype]                               NVARCHAR (64)   NULL,
    [orderno]                                 NVARCHAR (64)   NULL,
    [ordersuf]                                NVARCHAR (MAX)  NULL,
    [lineno]                                  NVARCHAR (MAX)  NULL,
    [custno]                                  NVARCHAR (64)   NULL,
    [custrebamt]                              NVARCHAR (MAX)  NULL,
    [pder rebrecno]                           NVARCHAR (MAX)  NULL,
    [pdsr refer]                              NVARCHAR (MAX)  NULL,
    [pdsr contract]                           NVARCHAR (64)   NULL,
    [invoicedt]                               DATETIME        NULL,
    [line discount]                           NVARCHAR (MAX)  NULL,
    [netamt]                                  NVARCHAR (MAX)  NULL,
    [otherdisc]                               NVARCHAR (MAX)  NULL,
    [pdrecno]                                 NVARCHAR (MAX)  NULL,
    [pdsc refer]                              NVARCHAR (MAX)  NULL,
    [pdsc contract]                           NVARCHAR (MAX)  NULL,
    [prccostmult]                             NUMERIC (18, 6) NULL,
    [price]                                   NVARCHAR (64)   NULL,
    [priceoverfl]                             NVARCHAR (8)    NULL,
    [pricetype]                               NVARCHAR (MAX)  NULL,
    [priceunit]                               NVARCHAR (MAX)  NULL,
    [prod]                                    NVARCHAR (64)   NULL,
    [prodcat]                                 NVARCHAR (MAX)  NULL,
    [prodcost]                                NVARCHAR (64)   NULL,
    [prodline]                                NVARCHAR (MAX)  NULL,
    [qtyship]                                 NUMERIC (18, 6) NULL,
    [salesterr]                               NVARCHAR (MAX)  NULL,
    [slsrepout]                               NVARCHAR (64)   NULL,
    [shipto]                                  NVARCHAR (64)   NULL,
    [unit]                                    NVARCHAR (MAX)  NULL,
    [unitconv]                                NVARCHAR (MAX)  NULL,
    [user1]                                   NVARCHAR (MAX)  NULL,
    [user10]                                  NVARCHAR (MAX)  NULL,
    [user11]                                  NVARCHAR (MAX)  NULL,
    [user12]                                  NVARCHAR (MAX)  NULL,
    [user13]                                  NVARCHAR (MAX)  NULL,
    [user14]                                  NVARCHAR (MAX)  NULL,
    [user15]                                  NVARCHAR (MAX)  NULL,
    [user16]                                  NVARCHAR (MAX)  NULL,
    [user17]                                  NVARCHAR (MAX)  NULL,
    [user18]                                  NVARCHAR (MAX)  NULL,
    [user19]                                  NVARCHAR (MAX)  NULL,
    [user2]                                   NVARCHAR (MAX)  NULL,
    [user20]                                  NVARCHAR (MAX)  NULL,
    [user21]                                  NVARCHAR (MAX)  NULL,
    [user22]                                  NVARCHAR (MAX)  NULL,
    [user23]                                  NVARCHAR (MAX)  NULL,
    [user24]                                  NVARCHAR (MAX)  NULL,
    [user3]                                   NVARCHAR (MAX)  NULL,
    [user4]                                   NVARCHAR (MAX)  NULL,
    [user5]                                   NVARCHAR (MAX)  NULL,
    [user6]                                   NVARCHAR (MAX)  NULL,
    [user7]                                   NVARCHAR (MAX)  NULL,
    [user8]                                   NVARCHAR (MAX)  NULL,
    [user9]                                   NVARCHAR (MAX)  NULL,
    [vendno]                                  NVARCHAR (64)   NULL,
    [vendrebamt]                              NVARCHAR (MAX)  NULL,
    [pder rebrecno 2]                         NVARCHAR (MAX)  NULL,
    [pdsr refer 2]                            NVARCHAR (MAX)  NULL,
    [pdsr contract 2]                         NVARCHAR (128)  NULL,
    [whse]                                    NVARCHAR (64)   NULL,
    [custom1]                                 NVARCHAR (MAX)  NULL,
    [custom2]                                 NVARCHAR (MAX)  NULL,
    [custom3]                                 NVARCHAR (MAX)  NULL,
    [custom4]                                 NVARCHAR (MAX)  NULL,
    [custom5]                                 NVARCHAR (MAX)  NULL,
    [custom6]                                 NVARCHAR (MAX)  NULL,
    [custom7]                                 NVARCHAR (MAX)  NULL,
    [custom8]                                 NVARCHAR (MAX)  NULL,
    [custom9]                                 NVARCHAR (MAX)  NULL,
    [custom10]                                NVARCHAR (MAX)  NULL,
    [SALESREP_IN]                             NVARCHAR (MAX)  NULL,
    [ORDER_TAKER]                             NVARCHAR (MAX)  NULL,
    [SPECIAL_STOCK_IND]                       VARCHAR (16)    NULL,
    [CoNo]                                    NVARCHAR (MAX)  NULL,
    [AveUsage]                                NVARCHAR (MAX)  NULL,
    [OrderType]                               NVARCHAR (MAX)  NULL,
    [ShipVia]                                 NVARCHAR (MAX)  NULL,
    [RushFl]                                  NVARCHAR (MAX)  NULL,
    [CostOverFl]                              NVARCHAR (MAX)  NULL,
    [IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE_ID] BIGINT          IDENTITY (1, 1) NOT NULL,
    [Product_Structure_FK]                    BIGINT          NULL,
    [Org_Entity_Structure_FK]                 BIGINT          NULL,
    [Cust_Entity_Structure_FK]                BIGINT          NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_slstranact]
    ON [import].[IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE]([prod] ASC, [custno] ASC, [shipto] ASC, [whse] ASC, [invoicedt] ASC, [vendno] ASC, [transtype] ASC, [prccostmult] ASC, [slsrepout] ASC, [SPECIAL_STOCK_IND] ASC, [orderno] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_slstran]
    ON [import].[IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE]([transtype] ASC, [invoicedt] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_SALES_DPS]
    ON [import].[IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE]([prod] ASC, [qtyship] ASC, [prodcost] ASC, [prccostmult] ASC, [price] ASC, [vendno] ASC, [invoicedt] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_PROD_DPS]
    ON [import].[IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE]([prod] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

