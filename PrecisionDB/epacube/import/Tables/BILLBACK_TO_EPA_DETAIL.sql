﻿CREATE TABLE [import].[BILLBACK_TO_EPA_DETAIL] (
    [BILLBACK_TO_EPA_DETAIL_ID] BIGINT       IDENTITY (1000, 1) NOT NULL,
    [PROPOSAL_ID]               VARCHAR (50) NULL,
    [NS_ITEM_ID]                VARCHAR (50) NULL,
    [STORE_PACK]                VARCHAR (50) NULL,
    [ALLOWANCE_TYPE]            VARCHAR (50) NULL,
    [START_DATE]                VARCHAR (50) NULL,
    [END_DATE]                  VARCHAR (50) NULL,
    [PERCENT_OFF]               VARCHAR (50) NULL,
    [PERCENT_BASIS]             VARCHAR (50) NULL,
    [ALLOWANCE_AMT]             VARCHAR (50) NULL,
    [PROGRAM_TYPE]              VARCHAR (50) NULL,
    [CREATE_TIMESTAMP]          DATETIME     NULL,
    [CREATE_USER]               VARCHAR (50) NULL,
    [UPDATE_TIMESTAMP]          DATETIME     NULL,
    [UPDATE_USER]               VARCHAR (50) NULL,
    [PRODUCT_STRUCTURE_FK]      BIGINT       NULL,
    [JOB_FK]                    NCHAR (10)   NULL
);

