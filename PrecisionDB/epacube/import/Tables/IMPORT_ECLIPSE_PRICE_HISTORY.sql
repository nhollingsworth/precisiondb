﻿CREATE TABLE [import].[IMPORT_ECLIPSE_PRICE_HISTORY] (
    [ProductID]               VARCHAR (128) NULL,
    [PriceSheetID]            VARCHAR (128) NULL,
    [EffectiveDate]           VARCHAR (128) NULL,
    [PriceUOM]                VARCHAR (128) NULL,
    [PricePer]                VARCHAR (128) NULL,
    [ListPrice]               VARCHAR (128) NULL,
    [TSColumn1]               VARCHAR (50)  NULL,
    [TSColumn2]               VARCHAR (50)  NULL,
    [TSColumn3]               VARCHAR (128) NULL,
    [TSCost]                  VARCHAR (128) NULL,
    [REPLCost]                VARCHAR (128) NULL,
    [STDCost]                 VARCHAR (128) NULL,
    [BurdenCost]              VARCHAR (128) NULL,
    [IMPA]                    VARCHAR (128) NULL,
    [BranchType]              VARCHAR (50)  NULL,
    [Export_date]             VARCHAR (128) NULL,
    [JOB_FK]                  BIGINT        NULL,
    [IMPORT_FILENAME]         VARCHAR (256) NULL,
    [PRODUCT_STRUCTURE_FK]    BIGINT        NULL,
    [ORG_ENTITY_STRUCTURE_FK] BIGINT        NULL,
    [ERROR_ON_IMPORT_IND]     INT           NULL
);

