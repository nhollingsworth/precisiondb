﻿CREATE TABLE [import].[IMPORT_ECLIPSE_VELOCITY] (
    [Matrix_ID]       VARCHAR (128) NULL,
    [Class]           VARCHAR (128) NULL,
    [Group]           VARCHAR (128) NULL,
    [Customer_Rank]   VARCHAR (128) NULL,
    [Cust_Vel1]       VARCHAR (128) NULL,
    [Cust_Vel2]       VARCHAR (128) NULL,
    [Cust_Vel3]       VARCHAR (128) NULL,
    [Cust_Vel4]       VARCHAR (128) NULL,
    [Cust_Vel5]       VARCHAR (128) NULL,
    [Cust_Vel6]       VARCHAR (128) NULL,
    [JOB_FK]          BIGINT        NULL,
    [IMPORT_FILENAME] VARCHAR (128) NULL
);

