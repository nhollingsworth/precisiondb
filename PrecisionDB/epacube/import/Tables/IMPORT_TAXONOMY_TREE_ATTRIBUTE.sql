﻿CREATE TABLE [import].[IMPORT_TAXONOMY_TREE_ATTRIBUTE] (
    [IMPORT_FILENAME]             VARCHAR (128) NOT NULL,
    [RECORD_NO]                   INT           NOT NULL,
    [JOB_FK]                      BIGINT        NOT NULL,
    [IMPORT_PACKAGE_FK]           INT           NOT NULL,
    [Delete_Flag]                 VARCHAR (3)   NULL,
    [Tree_Name]                   VARCHAR (100) NULL,
    [Node_Code]                   VARCHAR (100) NULL,
    [Attribute_Name]              VARCHAR (100) NULL,
    [Attribute_Value]             VARCHAR (125) NULL,
    [Data_Type]                   VARCHAR (50)  NULL,
    [Default_Value]               VARCHAR (100) NULL,
    [Restricted_Lookup]           VARCHAR (3)   NULL,
    [Pricebook_Flag]              VARCHAR (3)   NULL,
    [Source]                      VARCHAR (50)  NULL,
    [UPDATE_TIMESTAMP]            DATETIME      NULL,
    [CREATE_TIMESTAMP]            DATETIME      NULL,
    [UPDATE_USER]                 VARCHAR (64)  NULL,
    [Attribute_Name_DISPLAY_SEQ]  INT           NULL,
    [Attribute_Value_DISPLAY_SEQ] INT           NULL,
    [Attribute_Type]              VARCHAR (50)  NULL
);

