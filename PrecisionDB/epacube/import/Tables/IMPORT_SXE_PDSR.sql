﻿CREATE TABLE [import].[IMPORT_SXE_PDSR] (
    [IMPORT_FILENAME]       VARCHAR (100)  NULL,
    [JOB_FK]                INT            NULL,
    [Typecd]                NVARCHAR (256) NULL,
    [Hierarchy]             NVARCHAR (256) NULL,
    [contractno]            NVARCHAR (256) NULL,
    [custno]                NVARCHAR (256) NULL,
    [custrebty]             NVARCHAR (256) NULL,
    [startdt]               NVARCHAR (256) NULL,
    [enddt]                 NVARCHAR (256) NULL,
    [dropshipty]            NVARCHAR (256) NULL,
    [levelcd]               NVARCHAR (256) NULL,
    [margincostty]          NVARCHAR (256) NULL,
    [pricesheet]            NVARCHAR (256) NULL,
    [pricetype]             NVARCHAR (256) NULL,
    [product]               NVARCHAR (256) NULL,
    [prodcat]               NVARCHAR (256) NULL,
    [prodline]              NVARCHAR (256) NULL,
    [rebateamt]             NVARCHAR (256) NULL,
    [rebatecd]              NVARCHAR (256) NULL,
    [rebatecostty]          NVARCHAR (256) NULL,
    [rebatepct]             NVARCHAR (256) NULL,
    [rebatety]              NVARCHAR (256) NULL,
    [rebcalcty]             NVARCHAR (256) NULL,
    [rebdowntoty]           NVARCHAR (256) NULL,
    [rebrecno]              NVARCHAR (50)  NULL,
    [rebsubty]              NVARCHAR (256) NULL,
    [refer]                 NVARCHAR (256) NULL,
    [shipto]                NVARCHAR (50)  NULL,
    [user1]                 VARCHAR (256)  NULL,
    [user2]                 VARCHAR (256)  NULL,
    [user3]                 VARCHAR (256)  NULL,
    [user4]                 VARCHAR (256)  NULL,
    [user5]                 VARCHAR (256)  NULL,
    [user6]                 VARCHAR (256)  NULL,
    [user7]                 VARCHAR (256)  NULL,
    [user8]                 VARCHAR (256)  NULL,
    [user9]                 VARCHAR (256)  NULL,
    [vendno ]               NVARCHAR (50)  NULL,
    [whse]                  VARCHAR (256)  NULL,
    [custom1]               VARCHAR (256)  NULL,
    [custom2]               VARCHAR (256)  NULL,
    [custom3]               VARCHAR (256)  NULL,
    [custom4]               VARCHAR (256)  NULL,
    [custom5]               VARCHAR (256)  NULL,
    [custom6]               VARCHAR (256)  NULL,
    [custom7]               VARCHAR (256)  NULL,
    [custom8]               VARCHAR (256)  NULL,
    [custom9]               VARCHAR (256)  NULL,
    [custom10]              VARCHAR (256)  NULL,
    [pricesheetto]          VARCHAR (256)  NULL,
    [priceeffectivedate]    NVARCHAR (50)  NULL,
    [priceeffectivedateto ] NVARCHAR (50)  NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_sxe_pdsr1]
    ON [import].[IMPORT_SXE_PDSR]([contractno] ASC, [dropshipty] ASC, [enddt] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_pdr]
    ON [import].[IMPORT_SXE_PDSR]([whse] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

