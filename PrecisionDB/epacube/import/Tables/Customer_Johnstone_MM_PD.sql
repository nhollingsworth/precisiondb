﻿CREATE TABLE [import].[Customer_Johnstone_MM_PD] (
    [Mfg Part #]                  VARCHAR (50)    NULL,
    [Vendor #]                    VARCHAR (50)    NULL,
    [Vendor Name]                 VARCHAR (50)    NULL,
    [Contract #]                  VARCHAR (50)    NULL,
    [Start Date]                  VARCHAR (50)    NULL,
    [3 Digit Store #]             VARCHAR (50)    NULL,
    [6 Digit Store #]             VARCHAR (50)    NULL,
    [Johnstone Stock #]           VARCHAR (50)    NULL,
    [Store Price]                 MONEY           NULL,
    [Standard Claimback Type]     INT             NULL,
    [Fixed Claimback Amount]      MONEY           NULL,
    [DS Multiplier]               NUMERIC (18, 6) NULL,
    [DC Multiplier]               NUMERIC (18, 6) NULL,
    [Import_Filename]             VARCHAR (256)   NULL,
    [Job_FK]                      INT             NULL,
    [CreateRecordType]            VARCHAR (256)   NULL,
    [Customer_Johnstone_MM_PD_ID] BIGINT          IDENTITY (1, 1) NOT NULL
);

