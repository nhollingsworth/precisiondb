﻿CREATE TABLE [import].[PAXSON_PC_MFR_PROFILE] (
    [PCNbr]               VARCHAR (100) NULL,
    [MFR_UCC_NUM]         VARCHAR (100) NULL,
    [AuthDistributorFlag] VARCHAR (100) NULL,
    [SpecialVndrFlag]     VARCHAR (100) NULL,
    [TypeRestrictionCode] VARCHAR (100) NULL,
    [Spaces]              VARCHAR (100) NULL,
    [SpecPriceZone1]      VARCHAR (100) NULL,
    [SpecPriceZone2]      VARCHAR (100) NULL,
    [SpecPriceZone3]      VARCHAR (100) NULL,
    [SpecPriceZone4]      VARCHAR (100) NULL,
    [SpecPriceZone5]      VARCHAR (100) NULL,
    [SpecPriceZone6]      VARCHAR (100) NULL,
    [SpecPriceZone7]      VARCHAR (100) NULL,
    [SpecPriceZone8]      VARCHAR (100) NULL,
    [SpecPriceZone9]      VARCHAR (100) NULL,
    [SpecPriceZone10]     VARCHAR (100) NULL,
    [Undefined_Col]       VARCHAR (100) NULL
);

