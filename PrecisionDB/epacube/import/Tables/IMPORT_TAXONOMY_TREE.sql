﻿CREATE TABLE [import].[IMPORT_TAXONOMY_TREE] (
    [IMPORT_FILENAME]   VARCHAR (128) NOT NULL,
    [RECORD_NO]         INT           NOT NULL,
    [JOB_FK]            BIGINT        NOT NULL,
    [IMPORT_PACKAGE_FK] INT           NOT NULL,
    [Delete_Flag]       VARCHAR (3)   NULL,
    [Tree_Name]         VARCHAR (100) NULL,
    [Node_Code]         VARCHAR (100) NULL,
    [Description]       VARCHAR (100) NULL,
    [Level]             SMALLINT      NULL,
    [Parent_Node]       VARCHAR (100) NULL,
    [Image]             VARCHAR (200) NULL,
    [UPDATE_TIMESTAMP]  DATETIME      CONSTRAINT [DF_IMPORT_TAXONOMY_TREE_UPD_TIMESTAMP] DEFAULT (getdate()) NULL,
    [CREATE_TIMESTAMP]  DATETIME      CONSTRAINT [DF_IMPORT_TAXONOMY_TREE_INS_TIMESTAMP] DEFAULT (getdate()) NULL,
    [UPDATE_USER]       VARCHAR (64)  NULL,
    [DISPLAY_SEQ]       INT           NULL,
    CONSTRAINT [PK_IMPORT_TAXONOMY_TREE] PRIMARY KEY CLUSTERED ([IMPORT_FILENAME] ASC, [RECORD_NO] ASC) WITH (FILLFACTOR = 80)
);

