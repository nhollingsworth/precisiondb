﻿CREATE TABLE [import].[IMPORT_SXE_CUSTOMERS_LAST_IMPORT] (
    [CUST_NO]          VARCHAR (256) NULL,
    [CITY]             VARCHAR (256) NULL,
    [CLASS]            VARCHAR (256) NULL,
    [COUNTRY]          VARCHAR (256) NULL,
    [CUST_TYPE]        VARCHAR (256) NULL,
    [CUST_PRICE_TYPE]  VARCHAR (128) NULL,
    [DISC_CODE]        VARCHAR (256) NULL,
    [JOB_DESC]         VARCHAR (256) NULL,
    [CUST_NAME]        VARCHAR (256) NULL,
    [PRICE_CUST_NO]    VARCHAR (256) NULL,
    [PRICE_CODE]       VARCHAR (256) NULL,
    [REBATE_TYPE]      VARCHAR (128) NULL,
    [ROUTE]            VARCHAR (256) NULL,
    [SALESREP_OUT]     VARCHAR (256) NULL,
    [SALESREP_IN]      VARCHAR (256) NULL,
    [TERRITORY]        VARCHAR (256) NULL,
    [SHIP_TO]          VARCHAR (256) NULL,
    [SIC_CODE1]        VARCHAR (256) NULL,
    [SIC_CODE2]        VARCHAR (256) NULL,
    [SIC_CODE3]        VARCHAR (256) NULL,
    [STATE]            VARCHAR (256) NULL,
    [USER1]            VARCHAR (256) NULL,
    [USER10]           VARCHAR (256) NULL,
    [USER11]           VARCHAR (256) NULL,
    [USER12]           VARCHAR (256) NULL,
    [USER13]           VARCHAR (256) NULL,
    [USER14]           VARCHAR (256) NULL,
    [USER15]           VARCHAR (256) NULL,
    [USER16]           VARCHAR (256) NULL,
    [USER17]           VARCHAR (256) NULL,
    [USER18]           VARCHAR (256) NULL,
    [USER19]           VARCHAR (256) NULL,
    [USER2]            VARCHAR (256) NULL,
    [USER20]           VARCHAR (256) NULL,
    [USER21]           VARCHAR (256) NULL,
    [USER22]           VARCHAR (256) NULL,
    [USER23]           VARCHAR (256) NULL,
    [USER24]           VARCHAR (256) NULL,
    [USER3]            VARCHAR (256) NULL,
    [USER4]            VARCHAR (256) NULL,
    [USER5]            VARCHAR (256) NULL,
    [USER6]            VARCHAR (256) NULL,
    [USER7]            VARCHAR (256) NULL,
    [USER8]            VARCHAR (256) NULL,
    [USER9]            VARCHAR (256) NULL,
    [WAREHOUSE_ID]     VARCHAR (256) NULL,
    [ZIP_CODE]         VARCHAR (256) NULL,
    [CUSTOM1]          VARCHAR (256) NULL,
    [CUSTOM2]          VARCHAR (256) NULL,
    [CUSTOM3]          VARCHAR (256) NULL,
    [CUSTOM4]          VARCHAR (256) NULL,
    [CUSTOM5]          VARCHAR (256) NULL,
    [CUSTOM6]          VARCHAR (256) NULL,
    [CUSTOM7]          VARCHAR (256) NULL,
    [CUSTOM8]          VARCHAR (256) NULL,
    [CUSTOM9]          VARCHAR (256) NULL,
    [CUSTOM10]         VARCHAR (256) NULL,
    [DSO]              VARCHAR (256) NULL,
    [STATUS]           VARCHAR (50)  NULL,
    [COMPANY_NO]       VARCHAR (50)  NULL,
    [CREATE_TIMESTAMP] DATETIME      CONSTRAINT [DF_IMPORT_SXE_CUSTOMERS_LAST_IMPORT_CREATE_TIMESTAMP] DEFAULT (getdate()) NULL,
    [CREATE_USER]      VARCHAR (50)  CONSTRAINT [DF_IMPORT_SXE_CUSTOMERS_LAST_IMPORT_CREATE_USER] DEFAULT ('IMPORT') NULL,
    [IMPORT_FILE_NAME] VARCHAR (128) NULL,
    [JOB_FK]           BIGINT        NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_cust_sx]
    ON [import].[IMPORT_SXE_CUSTOMERS_LAST_IMPORT]([STATUS] ASC, [CUST_NO] ASC, [SHIP_TO] ASC, [CUST_PRICE_TYPE] ASC, [REBATE_TYPE] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

