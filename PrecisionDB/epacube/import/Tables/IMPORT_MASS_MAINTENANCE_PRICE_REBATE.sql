﻿CREATE TABLE [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] (
    [VendorPartNumber]                        NVARCHAR (96)   NULL,
    [VendorNo]                                NVARCHAR (96)   NULL,
    [VendorName]                              NVARCHAR (96)   NULL,
    [VendorPartNumber_OnFile]                 NVARCHAR (96)   NULL,
    [VendorNo_OnFile]                         NVARCHAR (96)   NULL,
    [VendorName_OnFile]                       NVARCHAR (96)   NULL,
    [ContractNo]                              NVARCHAR (96)   NULL,
    [StartDate]                               SMALLDATETIME   NOT NULL,
    [Cust_Data_Name_FK]                       INT             NOT NULL,
    [Product_Data_Name_FK]                    INT             NOT NULL,
    [CustValueShort]                          NVARCHAR (16)   NULL,
    [CustValue]                               NVARCHAR (96)   NOT NULL,
    [ProductValue]                            NVARCHAR (96)   NOT NULL,
    [Price_For_Calculation]                   MONEY           NULL,
    [StoreSellPrice]                          MONEY           NULL,
    [CorpSellPrice]                           MONEY           NULL,
    [ClaimBackType]                           INT             NULL,
    [ClaimBackAction]                         VARCHAR (96)    NULL,
    [ClaimBackFromDN_FK]                      BIGINT          NULL,
    [ClaimBackEndValue]                       MONEY           NULL,
    [CreateRecordType]                        NVARCHAR (32)   NOT NULL,
    [DS Multiplier]                           NUMERIC (18, 6) NULL,
    [DC Multiplier]                           NUMERIC (18, 6) NULL,
    [DataValidationStatus]                    INT             NULL,
    [MassCreateStatus]                        INT             NULL,
    [Create_Timestamp]                        SMALLDATETIME   NULL,
    [Update_Timestamp]                        SMALLDATETIME   NULL,
    [Create_User]                             NVARCHAR (96)   NULL,
    [Update_User]                             NVARCHAR (96)   NULL,
    [Action_Status]                           INT             NULL,
    [Customer_Johnstone_MM_PD_FK]             INT             NULL,
    [Import_FileName]                         NVARCHAR (192)  NOT NULL,
    [Job_FK]                                  BIGINT          NULL,
    [IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID] INT             IDENTITY (2000000000, 1) NOT NULL,
    [Transferred_To_ERP]                      INT             NULL,
    [Transferred_To_ERP_Timestamp]            DATETIME        NULL,
    [CustomerName]                            VARCHAR (96)    NULL,
    CONSTRAINT [PK_IMPORT_PRICE_REBATE_MASS_MAINTENANCE] PRIMARY KEY CLUSTERED ([StartDate] ASC, [Cust_Data_Name_FK] ASC, [Product_Data_Name_FK] ASC, [CustValue] ASC, [ProductValue] ASC, [CreateRecordType] ASC, [Import_FileName] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_MM]
    ON [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE]([CreateRecordType] ASC, [VendorNo] ASC, [IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_vnd_jb]
    ON [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE]([VendorNo] ASC, [Job_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_status]
    ON [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE]([DataValidationStatus] ASC, [Action_Status] ASC, [StartDate] ASC, [CustValue] ASC, [ProductValue] ASC, [CreateRecordType] ASC, [Customer_Johnstone_MM_PD_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

