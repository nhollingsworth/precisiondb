﻿CREATE TABLE [import].[IMPORT_GTIN] (
    [IMPORT_GTIN_ID]       INT           IDENTITY (1000, 1) NOT NULL,
    [ITEM_SURR]            VARCHAR (256) NULL,
    [GPCPCKCDE]            VARCHAR (256) NULL,
    [GTITMNBR]             VARCHAR (256) NULL,
    [PRIOR_KEY]            VARCHAR (256) NULL,
    [DSCDATE]              VARCHAR (256) NULL,
    [DISC_RSN]             VARCHAR (256) NULL,
    [UPC_CODE]             VARCHAR (256) NULL,
    [UPCCHKDIG]            VARCHAR (256) NULL,
    [GPC_MFG]              VARCHAR (256) NULL,
    [DTE_CRT]              VARCHAR (256) NULL,
    [TME_CRT]              VARCHAR (256) NULL,
    [USR_CRT]              VARCHAR (256) NULL,
    [DTE_UPD]              VARCHAR (256) NULL,
    [TME_UPD]              VARCHAR (256) NULL,
    [USR_UPD]              VARCHAR (256) NULL,
    [PRODUCT_STRUCTURE_FK] BIGINT        NULL,
    [JOB_FK]               BIGINT        NULL,
    [DO_NOT_IMPORT]        INT           NULL,
    CONSTRAINT [PK_IMPORT_GTIN] PRIMARY KEY CLUSTERED ([IMPORT_GTIN_ID] ASC)
);

