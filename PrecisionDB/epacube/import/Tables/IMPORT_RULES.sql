﻿CREATE TABLE [import].[IMPORT_RULES] (
    [IMPORT_RULES_ID]                   BIGINT         IDENTITY (1, 1) NOT NULL,
    [JOB_FK]                            INT            NULL,
    [RECORD_NO]                         BIGINT         NULL,
    [IMPORT_FILENAME]                   VARCHAR (256)  NULL,
    [IMPORT_PACKAGE_FK]                 VARCHAR (16)   NULL,
    [RULES_STRUCTURE_NAME]              VARCHAR (64)   NULL,
    [RULES_SCHEDULE_NAME]               VARCHAR (64)   NULL,
    [RULE_NAME]                         VARCHAR (128)  NULL,
    [SHORT_NAME]                        VARCHAR (32)   NULL,
    [RESULT_DATA_NAME]                  VARCHAR (64)   NULL,
    [RESULT_DATA_NAME2]                 VARCHAR (64)   NULL,
    [RESULT_PRICESHEET]                 NVARCHAR (128) NULL,
    [RESULT_PRICESHEET_DATE_IND]        SMALLINT       NULL,
    [EFFECTIVE_DATE]                    VARCHAR (64)   NULL,
    [END_DATE]                          VARCHAR (64)   NULL,
    [TRIGGER_EVENT_TYPE]                VARCHAR (64)   NULL,
    [TRIGGER_EVENT_DN]                  VARCHAR (64)   NULL,
    [TRIGGER_OPERATOR]                  VARCHAR (64)   NULL,
    [TRIGGER_EVENT_VALUE1]              VARCHAR (64)   NULL,
    [TRIGGER_EVENT_VALUE2]              VARCHAR (64)   NULL,
    [TRIGGER_IMPORT_PACKAGE]            VARCHAR (64)   NULL,
    [TRIGGER_EVENT_SOURCE]              VARCHAR (64)   NULL,
    [TRIGGER_EVENT_ACTION]              VARCHAR (64)   NULL,
    [APPLY_TO_ORG_CHILDREN_IND]         VARCHAR (16)   NULL,
    [PROD_FILTER_DN]                    VARCHAR (64)   NULL,
    [PROD_FILTER_VALUE1]                VARCHAR (64)   NULL,
    [PROD_FILTER_VALUE2]                VARCHAR (64)   NULL,
    [PROD_FILTER_OPERATOR]              VARCHAR (32)   NULL,
    [PROD_FILTER_ASSOC_DN]              VARCHAR (64)   NULL,
    [ORG_FILTER_DN]                     VARCHAR (64)   NULL,
    [ORG_FILTER_ENTITY_DN]              VARCHAR (64)   NULL,
    [ORG_FILTER_VALUE1]                 VARCHAR (64)   NULL,
    [ORG_FILTER_VALUE2]                 VARCHAR (64)   NULL,
    [ORG_FILTER_OPERATOR]               VARCHAR (32)   NULL,
    [ORG_FILTER_ASSOC_DN]               VARCHAR (64)   NULL,
    [CUST_FILTER_DN]                    VARCHAR (64)   NULL,
    [CUST_FILTER_ENTITY_DN]             VARCHAR (64)   NULL,
    [CUST_FILTER_VALUE1]                VARCHAR (64)   NULL,
    [CUST_FILTER_VALUE2]                VARCHAR (64)   NULL,
    [CUST_FILTER_OPERATOR]              VARCHAR (32)   NULL,
    [CUST_FILTER_ASSOC_DN]              VARCHAR (64)   NULL,
    [SUPL_FILTER_DN]                    VARCHAR (64)   NULL,
    [SUPL_FILTER_ENTITY_DN]             VARCHAR (64)   NULL,
    [SUPL_FILTER_VALUE1]                VARCHAR (64)   NULL,
    [SUPL_FILTER_VALUE2]                VARCHAR (64)   NULL,
    [SUPL_FILTER_OPERATOR]              VARCHAR (32)   NULL,
    [SUPL_FILTER_ASSOC_DN]              VARCHAR (64)   NULL,
    [RULE_PRECEDENCE]                   VARCHAR (4)    NULL,
    [HOST_PRECEDENCE_LEVEL_DESCRIPTION] VARCHAR (128)  NULL,
    [HOST_RULE_XREF]                    VARCHAR (128)  NULL,
    [REFERENCE]                         VARCHAR (6128) NULL,
    [CONTRACT_NO]                       VARCHAR (128)  NULL,
    [RULES_CONTRACT_IND]                VARCHAR (16)   NULL,
    [GROUP_NAME]                        VARCHAR (64)   NULL,
    [DISPLAY_SEQ]                       VARCHAR (16)   CONSTRAINT [DF__IMPORT_RU__DISPL__64B351D5] DEFAULT ((999)) NULL,
    [RULE_RESULT_TYPE]                  VARCHAR (32)   NULL,
    [RULE_STATUS]                       VARCHAR (32)   NULL,
    [SEED_RULE_IND]                     VARCHAR (16)   NULL,
    [RULE_TYPE]                         VARCHAR (32)   NULL,
    [UNIQUE_KEY1]                       VARCHAR (256)  NULL,
    [SYSTEM_ID_DN]                      VARCHAR (64)   NULL,
    [SYSTEM_ID_VALUE]                   VARCHAR (64)   NULL,
    [RULES_FK]                          VARCHAR (16)   NULL,
    [RESULT_DATA_NAME_FK]               INT            NULL,
    [EVENT_ACTION_CR_FK]                VARCHAR (16)   NULL,
    [ERROR_ON_IMPORT_IND]               VARCHAR (16)   NULL,
    [RECORD_STATUS_CR_FK]               VARCHAR (16)   CONSTRAINT [DF__IMPORT_RU__RECOR__65A7760E] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]                  VARCHAR (64)   CONSTRAINT [DF__IMPORT_RU__CREAT__669B9A47] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]                  VARCHAR (64)   CONSTRAINT [DF__IMPORT_RU__UPDAT__678FBE80] DEFAULT (getdate()) NULL,
    [UPDATE_USER]                       VARCHAR (64)   NULL,
    [UPDATE_LOG_FK]                     VARCHAR (16)   NULL,
    [CREATE_USER]                       VARCHAR (64)   NULL,
    CONSTRAINT [PK_IMPORT_RULES] PRIMARY KEY CLUSTERED ([IMPORT_RULES_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_IR_20110128_ECL_MTX_IMPORT]
    ON [import].[IMPORT_RULES]([ERROR_ON_IMPORT_IND] ASC, [IMPORT_RULES_ID] ASC, [JOB_FK] ASC, [RULE_NAME] ASC, [RESULT_DATA_NAME] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_IR_20110128_RULES_CLEANSER]
    ON [import].[IMPORT_RULES]([ERROR_ON_IMPORT_IND] ASC, [IMPORT_RULES_ID] ASC, [JOB_FK] ASC, [RULES_CONTRACT_IND] ASC, [CONTRACT_NO] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_rl_job]
    ON [import].[IMPORT_RULES]([JOB_FK] ASC, [HOST_RULE_XREF] ASC, [UNIQUE_KEY1] ASC, [RESULT_DATA_NAME_FK] ASC, [RULES_FK] ASC, [ERROR_ON_IMPORT_IND] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

