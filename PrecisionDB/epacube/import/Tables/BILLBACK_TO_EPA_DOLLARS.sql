﻿CREATE TABLE [import].[BILLBACK_TO_EPA_DOLLARS] (
    [BILLBACK_TO_EPA_DOLLARS_ID] BIGINT       IDENTITY (1000, 1) NOT NULL,
    [PROPOSAL_ID]                VARCHAR (50) NULL,
    [AD_GROUP]                   VARCHAR (50) NULL,
    [PROGRAM_TYPE]               VARCHAR (50) NULL,
    [PROPOSAL_AMT_LUMP_SUM]      VARCHAR (50) NULL,
    [PROPOSAL_AMT_BY_CASE]       VARCHAR (50) NULL,
    [CREATE_TIMESTAMP]           DATETIME     NULL,
    [CREATE_USER]                VARCHAR (50) NULL,
    [UPDATE_TIMESTAMP]           DATETIME     NULL,
    [UPDATE_USER]                VARCHAR (50) NULL,
    [JOB_FK]                     BIGINT       NULL
);

