﻿CREATE TABLE [import].[URM_COST_CHANGES_RETAILER] (
    [ITEM_SURROGATE]        VARCHAR (64)    NOT NULL,
    [EFFECTIVE_DATE]        VARCHAR (64)    NOT NULL,
    [SUR_TME]               VARCHAR (50)    NULL,
    [MEMBER_PRICE]          NUMERIC (18, 4) NOT NULL,
    [RESCIND]               VARCHAR (1)     NULL,
    [DTE_CRT]               VARCHAR (50)    NULL,
    [TME_CRT]               VARCHAR (50)    NULL,
    [USR_CRT]               VARCHAR (50)    NULL,
    [DTE_UPD]               VARCHAR (50)    NULL,
    [TME_UPD]               VARCHAR (50)    NULL,
    [USR_UPD]               VARCHAR (50)    NULL,
    [JOB_FK]                BIGINT          NULL,
    [IMPORT_TIMESTAMP]      DATETIME        CONSTRAINT [DF_URM_COST_CHANGES_RETAILER_CREATE_TIMESTAMP] DEFAULT (getdate()) NOT NULL,
    [unique_cost_record_id] VARCHAR (64)    NULL
);

