﻿CREATE TABLE [import].[description_conversion] (
    [BUYLINE]                 NVARCHAR (255) NULL,
    [VENDOR]                  NVARCHAR (255) NULL,
    [DATA_NAME]               NVARCHAR (255) NULL,
    [DATA_VALUE1]             NVARCHAR (255) NULL,
    [DATA_VALUE1_DESCRIPTION] NVARCHAR (255) NULL,
    [PART_FORMAT_DN]          NVARCHAR (255) NULL,
    [DATA_VALUE2]             NVARCHAR (255) NULL,
    [DATA_VALUE2_DESCRIPTION] NVARCHAR (255) NULL,
    [REMOVE_SPECIAL_CHAR]     NVARCHAR (255) NULL,
    [DATA_CATEGORY]           NVARCHAR (255) NULL,
    [DISPLAY_SEQ]             NVARCHAR (255) NULL
);

