﻿CREATE TABLE [import].[IMPORT_ECLIPSE_PRICE_HISTORY_FUTURE] (
    [EffectiveDate]  VARCHAR (128) NULL,
    [ProductID]      VARCHAR (128) NULL,
    [Description]    VARCHAR (128) NULL,
    [ListPrice]      VARCHAR (128) NULL,
    [TSColumn3]      VARCHAR (128) NULL,
    [TSCost]         VARCHAR (128) NULL,
    [REPLCost]       VARCHAR (128) NULL,
    [STDCost]        VARCHAR (128) NULL,
    [BurdenCost]     VARCHAR (128) NULL,
    [Last_Sale_date] VARCHAR (128) NULL,
    [Sales_QTY]      BIGINT        NULL
);

