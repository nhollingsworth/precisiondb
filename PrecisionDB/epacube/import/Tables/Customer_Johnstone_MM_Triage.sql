﻿CREATE TABLE [import].[Customer_Johnstone_MM_Triage] (
    [epaRules_ID]                BIGINT          NULL,
    [Rule_ID]                    VARCHAR (128)   NULL,
    [EFFECTIVE_DATE]             DATETIME        NULL,
    [Product_ID]                 VARCHAR (64)    NULL,
    [Product_Description]        VARCHAR (256)   NULL,
    [Store_ID]                   VARCHAR (64)    NULL,
    [Store_Name]                 VARCHAR (128)   NULL,
    [Vendor_ID]                  VARCHAR (128)   NULL,
    [Whse_ID]                    VARCHAR (16)    NULL,
    [Store_Status]               VARCHAR (6)     NULL,
    [Parent_Entity_Structure_FK] BIGINT          NULL,
    [Matching New Record]        INT             NULL,
    [New Records To Create]      INT             NULL,
    [Parent Stores New]          INT             NULL,
    [Child Stores New]           INT             NULL,
    [Unique Products New]        INT             NULL,
    [PD Record Sell Price]       NUMERIC (18, 6) NULL,
    [New Sell Price]             NUMERIC (18, 6) NULL,
    [PD Record Rebate Amount]    NUMERIC (18, 6) NULL,
    [New Rebate Amount]          NUMERIC (18, 6) NULL,
    [CreateRecordType]           VARCHAR (16)    NULL,
    [Create_User]                INT             NULL,
    [Create_Timestamp]           DATETIME        NOT NULL,
    [Job_FK]                     INT             NULL,
    [Record_Status_CR_FK]        INT             CONSTRAINT [DF_Customer_Johnstone_MM_Triage_Record_Status_CR_FK] DEFAULT ((1)) NULL
);


GO
CREATE CLUSTERED INDEX [ClusteredIndex-20140508-100228]
    ON [import].[Customer_Johnstone_MM_Triage]([Job_FK] ASC, [Vendor_ID] ASC, [Product_ID] ASC);


GO
CREATE NONCLUSTERED INDEX [idx_triage]
    ON [import].[Customer_Johnstone_MM_Triage]([epaRules_ID] ASC, [Vendor_ID] ASC, [Product_ID] ASC, [Store_ID] ASC, [Whse_ID] ASC, [Store_Status] ASC, [CreateRecordType] ASC, [Job_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

