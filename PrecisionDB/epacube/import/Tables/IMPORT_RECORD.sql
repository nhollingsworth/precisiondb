﻿CREATE TABLE [import].[IMPORT_RECORD] (
    [IMPORT_RECORD_ID]    BIGINT        IDENTITY (10000, 1) NOT NULL,
    [IMPORT_FILENAME]     VARCHAR (128) NOT NULL,
    [RECORD_NO]           INT           NOT NULL,
    [JOB_FK]              BIGINT        NOT NULL,
    [IMPORT_PACKAGE_FK]   INT           NOT NULL,
    [EFFECTIVE_DATE]      DATETIME      CONSTRAINT [DF_IMPORT_200_EFF_DATE] DEFAULT (getdate()) NULL,
    [END_DATE]            DATETIME      NULL,
    [ENTITY_CLASS]        VARCHAR (64)  NULL,
    [ENTITY_DATA_NAME]    VARCHAR (64)  NULL,
    [ENTITY_ID_DATA_NAME] VARCHAR (64)  NULL,
    [ENTITY_ID_VALUE]     VARCHAR (64)  NULL,
    [ENTITY_STATUS]       VARCHAR (32)  NULL,
    [SYSTEM_ID_VALUE]     VARCHAR (64)  NULL,
    [PRIORITY]            VARCHAR (32)  NULL,
    [SEARCH_ACTIVITY_FK]  INT           NULL,
    [UPDATE_TIMESTAMP]    DATETIME      CONSTRAINT [DF_IMPORT_200_UPDATE_TIMESTAMP] DEFAULT (getdate()) NULL,
    [UPDATE_USER]         VARCHAR (64)  CONSTRAINT [DF_IMPORT_200_UPDATE_USER] DEFAULT ('IMPORT') NULL,
    CONSTRAINT [PK_I200] PRIMARY KEY CLUSTERED ([IMPORT_RECORD_ID] ASC),
    CONSTRAINT [IR_J_RN] UNIQUE NONCLUSTERED ([JOB_FK] ASC, [RECORD_NO] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IR_20100316_LOAD_STAGE]
    ON [import].[IMPORT_RECORD]([IMPORT_RECORD_ID] ASC, [JOB_FK] ASC, [IMPORT_PACKAGE_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IREC_20100602_PURGE_IMPORT_TABLES]
    ON [import].[IMPORT_RECORD]([IMPORT_RECORD_ID] ASC, [JOB_FK] ASC, [RECORD_NO] ASC, [EFFECTIVE_DATE] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [Idx_import_record_JobFK]
    ON [import].[IMPORT_RECORD]([RECORD_NO] ASC, [ENTITY_ID_VALUE] ASC, [JOB_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [Idx_import_record_JobFK_EntityIDValue]
    ON [import].[IMPORT_RECORD]([JOB_FK] ASC, [ENTITY_ID_VALUE] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

