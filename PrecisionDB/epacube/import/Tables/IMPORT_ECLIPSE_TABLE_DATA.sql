﻿CREATE TABLE [import].[IMPORT_ECLIPSE_TABLE_DATA] (
    [IMPORT_FILENAME]     VARCHAR (128) NOT NULL,
    [RECORD_NO]           BIGINT        NOT NULL,
    [IMPORT_PACKAGE_FK]   BIGINT        NULL,
    [JOB_FK]              BIGINT        NULL,
    [EFFECTIVE_DATE]      DATETIME      NULL,
    [DATA_NAME]           VARCHAR (50)  NULL,
    [VALUE1]              VARCHAR (50)  NULL,
    [VALUE2]              VARCHAR (50)  NULL,
    [ERROR_ON_IMPORT_IND] SMALLINT      NULL
);

