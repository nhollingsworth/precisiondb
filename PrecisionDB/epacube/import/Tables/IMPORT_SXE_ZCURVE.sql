﻿CREATE TABLE [import].[IMPORT_SXE_ZCURVE] (
    [IMPORT_FILENAME] VARCHAR (256) NULL,
    [JOB_FK]          BIGINT        NULL,
    [toscode]         VARCHAR (50)  NULL,
    [rebatety]        VARCHAR (50)  NULL,
    [startdt]         VARCHAR (50)  NULL,
    [enddt]           VARCHAR (50)  NULL,
    [begsellmult2]    VARCHAR (50)  NULL,
    [endsellmult2]    VARCHAR (50)  NULL,
    [splitseg2]       VARCHAR (50)  NULL,
    [begdgm%seg2]     VARCHAR (50)  NULL,
    [endsellmult3]    VARCHAR (50)  NULL,
    [splitseg3]       VARCHAR (50)  NULL,
    [areamult]        VARCHAR (50)  NULL,
    [user1]           VARCHAR (50)  NULL,
    [user2]           VARCHAR (50)  NULL,
    [user3]           VARCHAR (50)  NULL,
    [user4]           VARCHAR (50)  NULL,
    [user5]           VARCHAR (50)  NULL,
    [user6]           VARCHAR (50)  NULL,
    [user7]           VARCHAR (50)  NULL,
    [user8]           VARCHAR (50)  NULL,
    [user9]           VARCHAR (50)  NULL,
    [custom1]         VARCHAR (50)  NULL,
    [custom2]         VARCHAR (50)  NULL,
    [custom3]         VARCHAR (50)  NULL,
    [custom4]         VARCHAR (50)  NULL,
    [custom5]         VARCHAR (50)  NULL,
    [custom6]         VARCHAR (50)  NULL,
    [custom7]         VARCHAR (50)  NULL,
    [custom8]         VARCHAR (50)  NULL,
    [custom9]         VARCHAR (50)  NULL,
    [custom10]        VARCHAR (50)  NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_zcurve]
    ON [import].[IMPORT_SXE_ZCURVE]([toscode] ASC, [rebatety] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

