﻿CREATE TABLE [import].[IMPORT_ECLIPSE_MATRIX] (
    [Matrix_ID]            VARCHAR (128) NULL,
    [Matrix_Level]         VARCHAR (128) NULL,
    [Matrix_Type]          VARCHAR (128) NULL,
    [Optimize_Matrix]      VARCHAR (128) NULL,
    [Customer_Level]       VARCHAR (128) NULL,
    [Customer_Level_Value] VARCHAR (128) NULL,
    [CNTRCT_ID]            VARCHAR (128) NULL,
    [CONTRACT_NAME]        VARCHAR (128) NULL,
    [REFERENCE]            VARCHAR (128) NULL,
    [Product_Level]        VARCHAR (128) NULL,
    [Product_Level_Value]  VARCHAR (128) NULL,
    [Whse_Level]           VARCHAR (128) NULL,
    [Whse_Level_Value]     VARCHAR (128) NULL,
    [BPC]                  VARCHAR (128) NULL,
    [P_BASIS]              VARCHAR (128) NULL,
    [PF_OPERATION1]        VARCHAR (128) NULL,
    [PF_OPERAND1]          VARCHAR (128) NULL,
    [PF_OPERATION2]        VARCHAR (128) NULL,
    [PF_OPERAND2]          VARCHAR (128) NULL,
    [PF_OPERATION3]        VARCHAR (128) NULL,
    [PF_OPERAND3]          VARCHAR (128) NULL,
    [Price_Date]           VARCHAR (128) NULL,
    [BCC]                  VARCHAR (128) NULL,
    [C_BASIS]              VARCHAR (128) NULL,
    [CF_OPERATION1]        VARCHAR (128) NULL,
    [CF_OPERAND1]          VARCHAR (128) NULL,
    [CF_OPERATION2]        VARCHAR (128) NULL,
    [CF_OPERAND2]          VARCHAR (128) NULL,
    [CF_OPERATION3]        VARCHAR (128) NULL,
    [CF_OPERAND3]          VARCHAR (128) NULL,
    [COST_DATE]            VARCHAR (128) NULL,
    [EFFECTIVE_DATE]       VARCHAR (128) NULL,
    [END_DATE]             VARCHAR (128) NULL,
    [P_RND_RULE]           VARCHAR (128) NULL,
    [P_RND_TO_VALUE]       VARCHAR (128) NULL,
    [P_RND_ADDER]          VARCHAR (128) NULL,
    [JOB_FK]               BIGINT        NULL,
    [IMPORT_FILENAME]      VARCHAR (128) NULL,
    [RANK]                 VARCHAR (50)  NULL
);


GO
CREATE NONCLUSTERED INDEX [IDX_IEM_20110128_RULES_CONTRACT_LOOKUP]
    ON [import].[IMPORT_ECLIPSE_MATRIX]([Matrix_ID] ASC, [JOB_FK] ASC, [CNTRCT_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_IEM_20110128_ECL_MTX_IMPORT]
    ON [import].[IMPORT_ECLIPSE_MATRIX]([CF_OPERATION1] ASC, [PF_OPERATION1] ASC, [JOB_FK] ASC, [Matrix_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

