﻿CREATE TABLE [import].[Import_SXE_PDSC] (
    [IMPORT_FILENAME] VARCHAR (100)  NULL,
    [JOB_FK]          INT            NULL,
    [Typecd]          NVARCHAR (50)  NULL,
    [commtype]        NVARCHAR (50)  NULL,
    [contractno]      NVARCHAR (50)  NULL,
    [custno]          NVARCHAR (50)  NULL,
    [custtype]        NVARCHAR (50)  NULL,
    [startdt]         NVARCHAR (50)  NULL,
    [enddt]           NVARCHAR (50)  NULL,
    [jobno]           NVARCHAR (50)  NULL,
    [levelcd]         NVARCHAR (50)  NULL,
    [maxqty]          NVARCHAR (50)  NULL,
    [minqty]          NVARCHAR (50)  NULL,
    [pdrecno]         NVARCHAR (50)  NULL,
    [pexactrnd]       NVARCHAR (50)  NULL,
    [prcdisc1]        NVARCHAR (50)  NULL,
    [prcdisc2]        NVARCHAR (50)  NULL,
    [prcdisc3]        NVARCHAR (50)  NULL,
    [prcdisc4]        NVARCHAR (50)  NULL,
    [prcdisc5]        NVARCHAR (50)  NULL,
    [prcdisc6]        NVARCHAR (50)  NULL,
    [prcdisc7]        NVARCHAR (50)  NULL,
    [prcdisc8]        NVARCHAR (50)  NULL,
    [prcdisc9]        NVARCHAR (50)  NULL,
    [prcmult1]        NVARCHAR (50)  NULL,
    [prcmult2]        NVARCHAR (50)  NULL,
    [prcmult3]        NVARCHAR (50)  NULL,
    [prcmult4]        NVARCHAR (50)  NULL,
    [prcmult5]        NVARCHAR (50)  NULL,
    [prcmult6]        NVARCHAR (50)  NULL,
    [prcmult7]        NVARCHAR (50)  NULL,
    [prcmult8]        NVARCHAR (50)  NULL,
    [prcmult9]        NVARCHAR (50)  NULL,
    [prctype]         NVARCHAR (50)  NULL,
    [pricing cost]    NVARCHAR (50)  NULL,
    [priceonty]       NVARCHAR (50)  NULL,
    [pricesheet]      NVARCHAR (50)  NULL,
    [pricetype]       NVARCHAR (50)  NULL,
    [prod]            NVARCHAR (50)  NULL,
    [prodcat]         NVARCHAR (50)  NULL,
    [prodline]        NVARCHAR (50)  NULL,
    [promofl]         NVARCHAR (50)  NULL,
    [pround]          NVARCHAR (50)  NULL,
    [ptarget]         NVARCHAR (50)  NULL,
    [qtybreak]        NVARCHAR (50)  NULL,
    [qtybrk1]         NVARCHAR (50)  NULL,
    [qtybrk2]         NVARCHAR (50)  NULL,
    [qtybrk3]         NVARCHAR (50)  NULL,
    [qtybrk4]         NVARCHAR (50)  NULL,
    [qtybrk5]         NVARCHAR (50)  NULL,
    [qtybrk6]         NVARCHAR (50)  NULL,
    [qtybrk7]         NVARCHAR (50)  NULL,
    [qtybrk8]         NVARCHAR (50)  NULL,
    [qtytype]         NVARCHAR (50)  NULL,
    [rebatetype]      NVARCHAR (50)  NULL,
    [refer]           NVARCHAR (256) NULL,
    [Shipto]          NVARCHAR (50)  NULL,
    [termsdisc]       NVARCHAR (50)  NULL,
    [termspct]        NVARCHAR (50)  NULL,
    [units]           NVARCHAR (50)  NULL,
    [user1]           VARCHAR (256)  NULL,
    [user2]           VARCHAR (256)  NULL,
    [user3]           VARCHAR (256)  NULL,
    [user4]           VARCHAR (256)  NULL,
    [user5]           VARCHAR (256)  NULL,
    [user6]           VARCHAR (256)  NULL,
    [user7]           VARCHAR (256)  NULL,
    [user8]           VARCHAR (256)  NULL,
    [user9]           VARCHAR (256)  NULL,
    [vendno ]         VARCHAR (50)   NULL,
    [whse]            VARCHAR (50)   NULL,
    [custom1]         VARCHAR (256)  NULL,
    [custom2]         VARCHAR (256)  NULL,
    [custom3]         VARCHAR (256)  NULL,
    [custom4]         VARCHAR (256)  NULL,
    [custom5]         VARCHAR (256)  NULL,
    [custom6]         VARCHAR (256)  NULL,
    [custom7]         VARCHAR (256)  NULL,
    [custom8]         VARCHAR (256)  NULL,
    [custom9]         VARCHAR (256)  NULL,
    [custom10 ]       VARCHAR (256)  NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_pdsc]
    ON [import].[Import_SXE_PDSC]([custno] ASC, [Shipto] ASC, [whse] ASC, [levelcd] ASC, [pdrecno] ASC, [custtype] ASC, [prctype] ASC, [pricetype] ASC, [prod] ASC, [vendno ] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_pdc]
    ON [import].[Import_SXE_PDSC]([whse] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_sxe_pdsc]
    ON [import].[Import_SXE_PDSC]([levelcd] ASC, [prctype] ASC, [custno] ASC, [Shipto] ASC, [pricetype] ASC, [rebatetype] ASC, [prodline] ASC, [prodcat] ASC, [whse] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_sxe_pdsc1]
    ON [import].[Import_SXE_PDSC]([levelcd] ASC, [promofl] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

