﻿CREATE TABLE [import].[Customer_Johnstone_MM_Audit] (
    [intCreateRecordType]            INT           NULL,
    [ProcessDate]                    DATETIME      NULL,
    [SQLcode]                        VARCHAR (MAX) NULL,
    [WhrClause]                      VARCHAR (MAX) NULL,
    [Customer_Johnstone_MM_Audit_ID] BIGINT        IDENTITY (1, 1) NOT NULL
);

