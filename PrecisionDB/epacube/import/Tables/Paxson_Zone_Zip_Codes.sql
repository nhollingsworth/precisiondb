﻿CREATE TABLE [import].[Paxson_Zone_Zip_Codes] (
    [PriceZone]       VARCHAR (50) NULL,
    [StartZipCode]    VARCHAR (50) NULL,
    [EndZipCode]      VARCHAR (50) NULL,
    [ZoneDatFileTime] VARCHAR (50) NULL,
    [Column 4]        VARCHAR (50) NULL,
    [Column 5]        VARCHAR (50) NULL
);

