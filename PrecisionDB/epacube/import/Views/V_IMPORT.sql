﻿
CREATE VIEW [import].[V_IMPORT]
AS
SELECT TOP (100) PERCENT
       (SELECT NAME
        FROM  import.IMPORT_PACKAGE
        WHERE IMPORT_PACKAGE_ID = imm.IMPORT_PACKAGE_FK ) AS PACKAGE
      ,imm.IMPORT_PACKAGE_FK      
      ,imm.SOURCE_FIELD_NAME
      ,imm.COLUMN_NAME AS IMPORT_COLUMN_NAME
      ,dn.NAME AS DATA_NAME
      ,DS.TABLE_NAME
      ,DS.COLUMN_NAME
      ,DS.EVENT_TYPE_CR_FK
----      ,IMM.DEFAULT_VALUE
      ,imm.DATA_NAME_FK
      ,imm.IDENT_DATA_NAME_FK
      ,imm.ASSOC_DATA_NAME_FK
      ,DS.DATA_TYPE_CR_FK
      ,DS.ENTITY_CLASS_CR_FK
      ,DS.ENTITY_STRUCTURE_EC_CR_FK AS COLUMN_ENTITY_CLASS_CR_FK
      ,DN.ENTITY_DATA_NAME_FK AS COLUMN_ENTITY_DATA_NAME_FK      
      ,DN.ORG_DATA_NAME_FK AS COLUMN_ORG_DATA_NAME_FK 
      ,DN.PARENT_DATA_NAME_FK AS PARENT_DATA_NAME_FK
      ,DS.CORP_IND     
      ,DS.ORG_IND    
      ,IMM.INSERT_MISSING_IND       
      ,DN.PARENT_DATA_NAME_FK AS COLUMN_PARENT_DATA_NAME_FK                  
      ,(SELECT CODE FROM epacube.CODE_REF 
        WHERE CODE_REF_ID = DS.ENTITY_CLASS_CR_FK ) AS ENTITY_CLASS
      ,(SELECT     ENTITY_CLASS_CR_FK
        FROM          import.IMPORT_PACKAGE AS IMPORT_PACKAGE_2
        WHERE      (IMPORT_PACKAGE_ID = imm.IMPORT_PACKAGE_FK)) AS IMPORT_ENTITY_CLASS_CR_FK
      ,(SELECT EVENT_SOURCE_CR_FK
        FROM  import.IMPORT_PACKAGE
        WHERE IMPORT_PACKAGE_ID = imm.IMPORT_PACKAGE_FK ) AS EVENT_SOURCE_CR_FK
      ,imm.nonunique_data_name_fk
      ,imm.RECORD_STATUS_CR_FK
      ,imm.IMPORT_MAP_METADATA_ID
FROM  epacube.DATA_NAME AS dn 
INNER JOIN epacube.data_set ds on ( dn.data_set_fk = ds.data_set_id )                 
LEFT OUTER JOIN import.IMPORT_MAP_METADATA AS imm ON imm.DATA_NAME_FK = dn.DATA_NAME_ID
WHERE  (dn.RECORD_STATUS_CR_FK = 1) 
AND    (imm.RECORD_STATUS_CR_FK = 1 )
ORDER BY ds.TABLE_NAME, imm.IMPORT_PACKAGE_FK, dn.DATA_NAME_ID

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[23] 4[39] 2[21] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -672
         Left = 0
      End
      Begin Tables = 
         Begin Table = "dn"
            Begin Extent = 
               Top = 29
               Left = 455
               Bottom = 165
               Right = 715
            End
            DisplayFlags = 280
            TopColumn = 16
         End
         Begin Table = "ds"
            Begin Extent = 
               Top = 294
               Left = 38
               Bottom = 409
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "imm"
            Begin Extent = 
               Top = 16
               Left = 56
               Bottom = 157
               Right = 310
            End
            DisplayFlags = 280
            TopColumn = 2
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 5145
         Alias = 2040
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'import', @level1type = N'VIEW', @level1name = N'V_IMPORT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'import', @level1type = N'VIEW', @level1name = N'V_IMPORT';

