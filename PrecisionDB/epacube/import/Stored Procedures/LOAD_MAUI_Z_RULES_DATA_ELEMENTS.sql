﻿--A_epaMAUI_LOAD_MAUI_Z_RULES_DATA_ELEMENTS

CREATE PROCEDURE [import].[LOAD_MAUI_Z_RULES_DATA_ELEMENTS]

AS
BEGIN

	SET NOCOUNT ON;

Declare @Job_FK bigint
Declare @Job_FK_i Numeric(10, 2)
Declare @ERP Varchar(32)
Declare @Prod_ID_DN_FK Bigint
Declare @Sheet_Results_DN_FK Bigint

Set @ERP = (Select Value from epacube.epacube_params where name = 'ERP HOST')
Set @Prod_ID_DN_FK = (Select data_name_fk from dbo.MAUI_Config_ERP where usage = 'Data_Name' and epaCUBE_Column = 'Product_ID' and ERP = @ERP)
Set @Sheet_Results_DN_FK = (select data_name_fk from dbo.MAUI_Config_ERP where usage = 'Data_Name' and epaCUBE_Column = 'Sheet_Results' and ERP = @ERP)

Set @Job_FK = (Select Max(Job_FK) from dbo.MAUI_Parameters_Jobs with (nolock))
Set @Job_FK_i = (Select Max(Job_FK_i) from dbo.MAUI_Parameters_Jobs with (nolock) where Job_FK = @Job_FK)

insert into dbo.maui_log Select @Job_FK, 'Start Loading MAUI Rules Data Elements', GetDate(), Null, Null
--Load Operations


	--IF EXISTS (SELECT name FROM sysindexes WHERE name = 'idx_rule_crit')
	--drop index idx_rule_crit on dbo.MAUI_Rules_Data_Elements

	--IF EXISTS (SELECT name FROM sysindexes WHERE name = 'idx_ids')
	--drop Index idx_ids on dbo.MAUI_Rules_Data_Elements

	IF object_id('tempdb..#Defaults') is not null
	drop table #Defaults

	Select * into #Defaults from dbo.MAUI_Rules_Data_Elements with (nolock) where Class = 'Defaults'

	IF object_id('tempdb..#config') is not null
	drop table #config
	
	IF object_id('tempdb..#PG') is not null
	drop table #PG

	Select * 
	into #config 
	from dbo.maui_configuration with (nolock) where class in ('whse', 'Operation-Price', 'Operation-Rebate')

	delete from dbo.MAUI_Rules_Data_Elements 
	
	Insert into dbo.MAUI_Rules_Data_Elements
	(Name, Class, Rule_Type, Data_Level, Structure_Price, Schedule_Price, Structure_Rebate, Schedule_Rebate, Result_Data_Name_FK_Price, Result_Data_Name_FK_Rebate, Seq, Update_Timestamp, Data_Elements_ID)
	select * 
	, Getdate(), Data_Elements_ID = Row_Number() over (Order by Rule_Type, Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
		Select top 1 'PRICE AND REBATE CONTRACT' Name, 'Rule Type' Class, 'BOTH' 'Rule_Type', 1 Data_Level, 200100 'Structure_Price', 2001002 'Schedule_Price', 19140 'Structure_Rebate', 1914001 'Schedule_Rebate', 111602 'Result_Data_Name_FK_Price', 111501 'Result_Data_Name_FK_Rebate', 1 Seq from epacube.data_name with (nolock) Union
		Select top 1 'PRICE RECORD' Name, 'Rule Type' Class, 'PRICING' 'Rule_Type', 1 Data_Level, 200100 'Structure_Price', 2001002 'Schedule_Price', Null 'Structure_Rebate', Null 'Schedule_Rebate', 111602 'Result_Data_Name_FK_Price', Null 'Result_Data_Name_FK_Rebate', 2 Seq from epacube.data_name with (nolock) Union
		Select top 1 'REBATE ON SALE' Name, 'Rule Type' Class, 'REBATE' 'Rule_Type', 1 Data_Level, Null 'Structure_Price', Null 'Schedule_Price', 19140 'Structure_Rebate', 1914001 'Schedule_Rebate', Null 'Result_Data_Name_FK_Price', 111501 'Result_Data_Name_FK_Rebate', 3 Seq from epacube.data_name with (nolock) Union
		Select top 1 'BUY SIDE REBATE' Name, 'Rule Type' Class, 'REBATE' 'Rule_Type', 1 Data_Level, Null 'Structure_Price', Null 'Schedule_Price', 19130 'Structure_Rebate', 1913001 'Schedule_Rebate', Null 'Result_Data_Name_FK_Price', 111502 'Result_Data_Name_FK_Rebate', 4 Seq from epacube.data_name with (nolock)
	) A Order by Rule_Type


	Insert into dbo.MAUI_Rules_Data_Elements
	(Name, ERP_Value_1, Class, Rule_Type, Data_Element_Source_Table, Reference_Name, Reference_FK, data_level, Update_Timestamp, Data_Elements_ID)
	select * 
	, GETDATE() , Data_Elements_ID = Row_Number() over (Order by [Rule_Type], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
	Select
	NAME 'Name'
	, Case
		When RULES_ACTION_OPERATION_ID = 1021 then '$'			--FIXED VALUE (N)
		When RULES_ACTION_OPERATION_ID in (1291, 1292) then 'n' --Rebate Down To%
		When RULES_ACTION_OPERATION_ID = 1293 then 'm'			--GROSS MARGIN GUARANTEE
		else '%' end ERP_Value_1
	, 'Operation' 'Class'
	, Case 
		When NAME like 'Rebate%' then 'REBATE'
		When NAME in ('Gross Margin (N)', 'Discount (N)', 'Percentage (N)') then 'PRICING'
		When NAME = 'Gross Margin Guarantee' Then 'REBATE'
		else 'BOTH' end 'Rule_Type'
	, 'synchronizer.RULES_ACTION' 'Data_Element_Source_Table'
	,'RULES_ACTION_OPERATION_FK' 'Reference_Name'
	, RULES_ACTION_OPERATION_ID 'Reference_FK'
	, 1 'data_level'
	from synchronizer.RULES_ACTION_OPERATION rao with (nolock)
	where ((Select CODE from epacube.code_ref  with (nolock)where code_ref_id = rao.basis_source_cr_fk) = 'Numeric'
		Or ((Select CODE from epacube.code_ref  with (nolock)where code_ref_id = rao.operator_cr_fk) = 'Named Operation'
			  And RULES_ACTION_OPERATION_ID > 1000)) 
		and (Select CODE from epacube.code_ref  with (nolock)where code_ref_id = rao.operator_cr_fk) Not in ('+', '-', '/')
		and NAME not like 'rebate amount%'
		and record_status_CR_FK = 1) A
	
		--Select * from dbo.MAUI_Rules_Data_Elements
		
--Load Product Criteria

	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, Class, Rule_Type, Data_Element_Source_Table, Reference_Name, Reference_FK, data_level, Matrix_Text, Update_Timestamp, Data_Elements_ID)
--	(Data_Name_FK, Name, ERP_Value_1, ERP_Value_2, Class, Rule_Type, Data_Element_Source_Table, Reference_Name, Reference_FK, data_level, Matrix_Text, Update_Timestamp, Data_Elements_ID)
	
	select * 
	, Getdate(), Data_Elements_ID = Row_Number() over (Order by [Rule_Type], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
	Select
	Data_Name_ID
	, Name
	, Org_Ind ERP_Value_1
	--, ERP_Value_2 = Row_Number() over (Order by isnull((Select Min(Precedence) Precedence from Import.IMPORT_PRECEDENCE_RULE_LOOKUP with (nolock) where Promo_Flag = 'N'
	--	and hierarchy =  (Select Value from epacube.EPACUBE_PARAMS with (nolock) where name = 'PRICING RULES TYPE') and prd_filter_DN = 
	--	(Select Name from epacube.data_name with (nolock) where data_name_id = Prod_Groupings.Data_name_ID)), 999))

	, 'Product Criteria' 'Class'
	, 'BOTH' 'Rule_Type'
	, Table_Name 'Data_Element_Source_Table'
	, Null 'Reference_Name'
	, Null 'Reference_FK'
	, 1 'Data_Level'
	, Case When Data_Name_ID = @Prod_ID_DN_FK then 'Product'
		When Data_Name_ID = 211901 then 'Product Price Type'
		When Data_Name_ID = 211903 then 'Rebate Type '
		When Data_Name_ID = 210901 then 'Product Category'
		When Data_Name_ID = 211902 then 'Product Line'
		When Data_Name_ID = 211904 then 'Rebate SubType'
		end Matrix_Text
	from
	(select DN.DATA_NAME_ID, DN.NAME, DS.Table_Name, DS.Org_Ind 
		from epacube.DATA_NAME DN with (nolock) inner join epacube.data_set DS with (nolock) on dn.data_set_fk = ds.data_set_id 
		where (DATA_SET_FK in (10900, 11900) or DN.Data_Name_ID = @Prod_ID_DN_FK) and dn.RECORD_STATUS_CR_FK = 1
		and (Data_Name_ID = @Prod_ID_DN_FK or (select count(*) from epacube.product_category with (nolock) where data_name_fk = dn.data_name_id) >= 1)
) Prod_Groupings
) A

--Load Customer Criteria

	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, Class, Rule_Type, Data_Element_Source_Table, Reference_Name, Reference_FK, data_level, Matrix_Text, Update_Timestamp, Data_Elements_ID)
	
	select * 
	, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [Rule_Type], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
	Select
	Data_Name_ID
	, Name
	, Null ERP_Value_1
	, 'Customer Criteria' 'Class'
	, Case
		When Name like '%Price%' then 'PRICING'
		When Name like '%Rebate%' then 'REBATE'
		else 'BOTH'
		end 'Rule_Type'
	, Table_Name 'Data_Element_Source_Table'
	, Null 'Reference_Name'
	, Null 'Reference_FK'
	, 1 'Data_Level'
	, Case When Data_Name_ID in (244900, 244901) then 'Customer Type' else 'Customer' End 'Matrix_Test'
	from
	(select DN.DATA_NAME_ID, DN.NAME, DS.Table_Name 
		from epacube.DATA_NAME DN with (nolock) inner join epacube.data_set DS with (nolock) on dn.data_set_fk = ds.data_set_id 
		where (DATA_SET_FK in (44300) or DN.Data_Name_ID in (144010, 144020)) and dn.RECORD_STATUS_CR_FK = 1
) Prod_Groupings
		) A

--Load Regions 

--Load Regions (If they exist)
	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, Class, Rule_Type, Reference_Name, Reference_FK, data_level, Update_Timestamp, Data_Elements_ID)
	select * 
	, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [Rule_Type], region_Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
	Select
	ein_p.entity_data_name_fk Data_Name_FK
	, ein_p.Value Region_Name
	, ei_p.Value ERP_Value_1
	, 'Region' 'Class'
	, 'Both' Rule_Type
	, 'entity_Structure_fk' 'Reference_Name'
	, ei_p.entity_Structure_fk 'Reference_FK'
	, 1 'Data_Level'
	from
		epacube.entity_identification ei_p with (nolock)
		left Join epacube.entity_ident_nonunique ein_p with (nolock)
			on ei_p.entity_structure_fk = ein_p.entity_structure_fk and ein_p.data_name_fk = 141112 and ein_p.entity_data_name_fk = 141030
	where ei_p.entity_data_name_fk = 141030
		) A

--Load Basis Options

	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, Class, Data_Criteria, Rule_Type, Data_Element_Source_Table, Reference_Name, Reference_FK, data_level, Update_Timestamp, Data_Elements_ID)
	
	select * 
	, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [Rule_Type], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
	Select
	Data_Name_ID
	, Replace(Name, 'Order COGS', 'REBATED COST') Name
	, Null ERP_Value_1
	, 'Value Basis' 'Class'
	, Case
		When Data_Name_ID in (111401, 231101, 231102, 231105, 111202) then 'VENDOR COST'
		When Data_Name_ID in (111602, 231103, 231104) then 'CUSTOMER PRICE'
		End Data_Criteria
	, Case
		When Data_Name_ID in (111602, 231105) then 'REBATE'
		When Data_Name_ID in (111401, 111202) then 'PRICING'
		else 'BOTH'
		end 'Rule_Type'
	, Table_Name 'Data_Element_Source_Table'
	, Null 'Reference_Name'
	, Null 'Reference_FK'
	, 1 'Data_Level'
	from
	(select DN.DATA_NAME_ID, DN.NAME, DS.Table_Name, DS.Data_Set_ID from epacube.DATA_NAME DN with (nolock) inner join epacube.data_set DS with (nolock) on dn.data_set_fk = ds.data_set_id 
	where ((((DATA_SET_FK in (31100, 31103, 31104, 31105) and DN.Data_Name_ID not in (231100, 231200)) or DN.DATA_NAME_ID in (111401, 111602)))
	or Data_Name_ID = (select 111202 DN from synchronizer.RULES_HIERARCHY where result_data_name_Fk = 111501 and RECORD_STATUS_CR_FK = 1)
	or Data_Name_ID = (select Value from epacube.epacube_Params with (nolock) where Name = 'PRICING COST BASIS'))
	and dn.RECORD_STATUS_CR_FK = 1
	and dn.data_name_id not in (select isnull(basis_calc_dn_FK, 0) from synchronizer.rules_action ra with (nolock) inner join synchronizer.rules r with (nolock) on r.rules_id = ra.rules_fk
	where r.rules_id < 2000000000 and result_data_name_fk = 111602 and r.record_status_cr_fk = 2)
) Prod_Groupings
) A

--Load UOM Codes

	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, Class, Rule_Type, data_level, Update_Timestamp, Data_Elements_ID)
	select *, 'UOM Code' Class, 'Both' Rule_Type, '1' Data_Level, GETDATE(), Data_Elements_ID = Row_Number() over (Order by Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (select dn.DATA_NAME_ID data_name_fk, dn.NAME from epacube.DATA_NAME DN with (nolock) inner join epacube.DATA_SET DS with (nolock) on DN.DATA_SET_FK = DS.DATA_SET_ID
		where (dn.NAME like '%uom%' or dn.NAME like '%pack%') and dn.RECORD_STATUS_CR_FK = 1) A
		
If @ERP = 'SXE'
Begin

--Load UOMs
	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, ERP_Value_2, Class, Rule_Type, Data_Level, Whse, Reference_Name, Reference_FK, Update_Timestamp, Data_Elements_ID)
	Select Data_Name_FK, UOM_Type Name, UOM ERP_Value_1, ProductID ERP_Value_2, 'UOMs' Class, 'BOTH' Rule_Type, '2' Data_Level, Whse, Reference_Name, Reference_FK, GETDATE(), Data_Elements_ID = Row_Number() over (Order by UOM_Type, UOM) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock)) from (
	select
	PUC.data_name_fk
	,(Select UOM_CODE from epacube.uom_code with (nolock) where uom_code_id = PUC.uom_code_fk) UOM
	,(Select Name from epacube.DATA_NAME with (nolock) where DATA_NAME_ID = PUC.data_name_fk) UOM_Type
	,(select value from epacube.product_identification with (nolock) where product_structure_fk = PUC.product_structure_fk and data_name_fk = @Prod_ID_DN_FK) ProductID
	,(Select value from epacube.entity_identification with (nolock) where ENTITY_STRUCTURE_FK = PA.ORG_ENTITY_STRUCTURE_FK and DATA_NAME_FK = 141111) Whse
	,'UOM_CODE_FK' 'Reference_Name'
	,UOM_CODE_FK 'Reference_FK'
	from epacube.PRODUCT_UOM_CLASS PUC with (nolock)
	Left join epacube.PRODUCT_ASSOCIATION PA with (nolock) on PUC.PRODUCT_STRUCTURE_FK = PA.PRODUCT_STRUCTURE_FK and PUC.ORG_ENTITY_STRUCTURE_FK = PA.ORG_ENTITY_STRUCTURE_FK and PA.ENTITY_CLASS_CR_FK = 10101
	where PUC.RECORD_STATUS_CR_FK = 1) A

--Load Costs
		--SX Replacement Cost
	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, ERP_Value_2, Class, Rule_Type, Data_Level, Whse, Update_Timestamp, Data_Elements_ID)
	Select Data_Name_FK, Name, ERP_Value_1, ProductID ERP_Value_2, 'Costs' Class, 'BOTH' Rule_Type, '2' Data_Level, Whse, GETDATE() Update_Timestamp, Data_Elements_ID = Row_Number() over (Order by ProductID, Whse) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock)) from (
	select
	231101 Data_Name_FK
	,(Select Name from epacube.data_name with (nolock) where data_name_id = 231101) Name
	,Net_Value1 ERP_Value_1
	,(select value from epacube.product_identification with (nolock) where product_structure_fk = SR.product_structure_fk and data_name_fk = @Prod_ID_DN_FK) ProductID
	,(Select value from epacube.entity_identification with (nolock) where ENTITY_STRUCTURE_FK = PA.ORG_ENTITY_STRUCTURE_FK and DATA_NAME_FK = 141111) Whse
	from marginmgr.SHEET_RESULTS_VALUES_FUTURE SR with (nolock)
	Left join epacube.PRODUCT_ASSOCIATION PA with (nolock) on SR.PRODUCT_STRUCTURE_FK = PA.PRODUCT_STRUCTURE_FK and SR.ORG_ENTITY_STRUCTURE_FK = PA.ORG_ENTITY_STRUCTURE_FK and PA.ENTITY_CLASS_CR_FK = 10101 and SR.DATA_NAME_FK = @Sheet_Results_DN_FK
	where SR.RECORD_STATUS_CR_FK = 1) A

	--SX Standard Cost
	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, ERP_Value_2, Class, Rule_Type, Data_Level, Whse, Update_Timestamp, Data_Elements_ID)
	Select Data_Name_FK, Name, ERP_Value_1, ProductID ERP_Value_2, 'Costs' Class, 'BOTH' Rule_Type, '2' Data_Level, Whse, GETDATE() Update_Timestamp, Data_Elements_ID = Row_Number() over (Order by ProductID, Whse) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock)) from (
	select
	231102 Data_Name_FK
	,(Select Name from epacube.data_name with (nolock) where data_name_id = 231102) Name
	,Net_Value2 ERP_Value_1
	,(select value from epacube.product_identification with (nolock) where product_structure_fk = SR.product_structure_fk and data_name_fk = @Prod_ID_DN_FK) ProductID
	,(Select value from epacube.entity_identification with (nolock) where ENTITY_STRUCTURE_FK = PA.ORG_ENTITY_STRUCTURE_FK and DATA_NAME_FK = 141111) Whse
	from marginmgr.SHEET_RESULTS_VALUES_FUTURE SR with (nolock)
	Left join epacube.PRODUCT_ASSOCIATION PA with (nolock) on SR.PRODUCT_STRUCTURE_FK = PA.PRODUCT_STRUCTURE_FK and SR.ORG_ENTITY_STRUCTURE_FK = PA.ORG_ENTITY_STRUCTURE_FK and PA.ENTITY_CLASS_CR_FK = 10101 and SR.DATA_NAME_FK = @Sheet_Results_DN_FK
	where SR.RECORD_STATUS_CR_FK = 1) A

	--Vendors by Product

	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, Class, Rule_Type, Reference_Name, Reference_FK, data_level, Update_Timestamp, Data_Elements_ID)
	select * 
	, Getdate(), Data_Elements_ID = Row_Number() over (Order by [Rule_Type], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
	Select
	Data_Name_FK
	, (Select Value from epacube.entity_ident_nonunique with (nolock) where data_name_fk = 143112 and entity_structure_fk = ei.entity_structure_fk) Name
	, Value ERP_Value_1
	, 'Vendor Name' 'Class'
	, 'Both' Rule_Type
	, 'entity_Structure_fk' 'Reference_Name'
	, entity_Structure_fk 'Reference_FK'
	, 2 'Data_Level'
	from
	epacube.entity_identification ei with (nolock) where data_name_fk = 143110
		) A Where isnumeric(erp_value_1) = 1 and ERP_Value_1 <> '999999999999'

End
--Load Product Criteria Level 2
	--Product ID
	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, ERP_Value_2, Class, Rule_Type, Data_Element_Source_Table, Reference_Name, Reference_FK, data_level, Update_Timestamp, Data_Elements_ID)
	select * 
	, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [Rule_Type], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
	Select
	Data_Name_FK
	, (Select Name from dbo.MAUI_Rules_Data_Elements with (nolock) where data_name_fk = pi.data_name_fk and data_level = 1) Name
	, Value ERP_Value_1
	
	, Value + ': ' + Replace(Replace(Replace((Select ltrim(rtrim(Description)) from epacube.PRODUCT_DESCRIPTION with (nolock) where data_name_fk = 110401 and product_structure_fk = pi.product_structure_fk) + 
		Case When (Select isnull(ltrim(rtrim(Description)), '') from epacube.PRODUCT_DESCRIPTION with (nolock) where data_name_fk = 110402 and product_structure_fk = pi.product_structure_fk) > '' then
		'-' + (Select isnull(ltrim(rtrim(Description)), '') from epacube.PRODUCT_DESCRIPTION with (nolock) where data_name_fk = 110402 and product_structure_fk = pi.product_structure_fk) else '' end
		, '  ', ' '), '  ', ' '), '  ', ' ') ERP_Value_2
	, 'Product Criteria' 'Class'
	, 'Both' Rule_Type
	, 'Product_Identification' Data_Element_Source_Table
	, Case When Data_Name_FK = @Prod_ID_DN_FK then 'Product_Structure_FK' else Null end 'Reference_Name'
	, Product_Structure_FK 'Reference_FK'
	, 2 'Data_Level'
	from
	epacube.product_identification pi with (nolock) where data_name_fk = @Prod_ID_DN_FK
	and left([Value], 1) <> '['
		) A order by ERP_Value_1

	--Product Groupings
	
	Select
	Groups.Data_Name_FK
	, Cast((Select Name from dbo.MAUI_Rules_Data_Elements with (nolock) where data_name_fk = Groups.data_name_fk and data_level = 1) as varchar(64)) Name
	, Cast((Select Value from epacube.data_value with (nolock) where data_value_id = Groups.data_value_fk) as varchar(64)) ERP_Value_1
	, Cast((Select Value from epacube.data_value with (nolock) where data_value_id = Groups.data_value_fk) + ': ' + (Select Description from epacube.data_value with (nolock) where data_value_id = Groups.data_value_fk) as varchar(64)) ERP_Value_2
	, 'Product Criteria' 'Class'
	, 'Product_Category' 'Data_Element_Source_Table'
	, 'Data_Value_FK' 'Reference_Name'
	, Groups.data_value_fk 'Reference_FK'
	, 2 'Data_Level'
	into #PG
	from
		(Select pc.data_name_fk, pc.data_value_fk
		from epacube.PRODUCT_CATEGORY pc  with (nolock)
		inner join dbo.MAUI_Rules_Data_Elements MRDE with (nolock) 
			on pc.DATA_NAME_FK = MRDE.Data_Name_FK 
			and MRDE.Class = 'Product Criteria' 
			and MRDE.Data_Element_Source_Table = 'Product_Category'
		group by pc.data_name_fk, pc.data_value_fk) Groups

	Create Index idx_pg on #PG(Data_Name_FK, Name, ERP_Value_1, Class, Data_Element_Source_Table, Reference_Name, Reference_FK, data_level)
	
	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, ERP_Value_2, Class, Data_Element_Source_Table, Reference_Name, Reference_FK, data_level, Update_Timestamp, Data_Elements_ID, Rule_Type)
	select * 
	, Getdate(), Data_Elements_ID = Row_Number() over (Order by Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	, Case When Name like '%Rebate SubType%' then 'REBATE' else 'Both' end Rule_Type
	from #PG A --Group by Data_Name_FK, Name, ERP_Value_1, Class, Data_Element_Source_Table, Reference_Name, Reference_FK, data_level
	
--Load Whse Criteria Level 2
	--Warehouses
	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, Parent_Value, Parent_Name, Class, Rule_Type, Reference_Name, Reference_FK, data_level, Update_Timestamp, Data_Elements_ID)
	select * 
	, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [Rule_Type], whse_Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
	Select
	ei.Data_Name_FK
	, ein.Value Whse_Name
	, ei.Value ERP_Value_1
	, ei_p.Value Region
	, ein_p.Value Region_Name
	, 'Whse' 'Class'
	, 'Both' Rule_Type
	, 'entity_Structure_fk' 'Reference_Name'
	, ei.entity_Structure_fk 'Reference_FK'
	, 2 'Data_Level'
	from
	epacube.entity_identification ei with (nolock) 
		left Join epacube.entity_ident_nonunique ein
			on ei.entity_structure_fk = ein.entity_structure_fk and ein.data_name_fk = 141112
		Left Join epacube.entity_structure es with (nolock)
			on ei.entity_structure_fk = es.entity_structure_id
		left Join epacube.entity_identification ei_p with (nolock)
			on ei_p.entity_structure_fk = es.parent_entity_structure_fk and ei_p.entity_data_name_fk = 141030
		left Join epacube.entity_ident_nonunique ein_p with (nolock)
			on ei_p.entity_structure_fk = ein_p.entity_structure_fk and ein_p.data_name_fk = 141112 and ein_p.entity_data_name_fk = 141030
	where ei.data_name_fk = 141111
		) A

--Customers
		
--Load Customer Price Types Level 2
	--Cust Type
	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, ERP_Value_2, Class, Rule_Type, Reference_Name, Reference_FK, data_level, Whse, Update_Timestamp, Data_Elements_ID)
	select * 
	, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [Rule_Type], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
	Select
	Data_Name_FK
	, 'SX CUST PRICE TYPE' Name
	, DV.value ERP_Value_1
	, DV.[Description] ERP_Value_2
	, 'Customer Criteria' 'Class'
	, 'PRICING' Rule_Type
	, Null 'Reference_Name'
	, Null 'Reference_FK'
	, 2 'Data_Level'
	, (Select top 1 ea.attribute_event_data Whse from epacube.entity_structure es with (nolock)
		inner join epacube.entity_mult_type emt0 with (nolock) on emt0.data_name_fk = 244900 and (es.entity_structure_id = emt0.entity_structure_fk or es.PARENT_ENTITY_STRUCTURE_FK = emt0.entity_structure_fk)
		inner join epacube.entity_attribute ea with (nolock) on ea.data_name_fk = 144900 and emt0.entity_structure_fk = ea.entity_structure_fk
		inner join epacube.entity_identification ei with (nolock) on ei.data_name_fk = 141111 and ea.attribute_event_data = ei.VALUE
		left join
			(Select cust_entity_structure_fk, Cast(sum(sales_qty) as int) sales_qty from dbo.maui_basis_calcs 
			group by cust_entity_structure_fk) MBC on (es.entity_structure_id = mbc.Cust_Entity_Structure_FK or es.PARENT_ENTITY_STRUCTURE_FK = mbc.Cust_Entity_Structure_FK)
			where emt0.data_value_fk = DV.DATA_VALUE_ID
			order by mbc.sales_qty desc) Whse
	from
	EPACUBE.DATA_VALUE DV WITH (NOLOCK) WHERE DATA_NAME_FK = 244900
	) A where erp_value_1 is not null 
	ORDER BY erp_value_1

	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, ERP_Value_2, Class, Rule_Type, Reference_Name, Reference_FK, data_level, Whse, Update_Timestamp, Data_Elements_ID)
	select * 
	, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [Rule_Type], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
	Select
	Data_Name_FK
	, 'SX CUST REBATE TYPE' Name
	, DV.value ERP_Value_1
	, DV.[Description] ERP_Value_2
	, 'Customer Criteria' 'Class'
	, 'REBATE' Rule_Type
	, Null 'Reference_Name'
	, Null 'Reference_FK'
	, 2 'Data_Level'
	, (Select top 1 ea.attribute_event_data Whse from epacube.entity_structure es with (nolock)
		inner join epacube.entity_mult_type emt0 with (nolock) on emt0.data_name_fk = DV.DATA_NAME_FK and (es.entity_structure_id = emt0.entity_structure_fk or es.PARENT_ENTITY_STRUCTURE_FK = emt0.entity_structure_fk)
		inner join epacube.entity_attribute ea with (nolock) on ea.data_name_fk = 144900 and emt0.entity_structure_fk = ea.entity_structure_fk
		inner join epacube.entity_identification ei with (nolock) on ei.data_name_fk = 141111 and ea.attribute_event_data = ei.VALUE
		left join
			(Select cust_entity_structure_fk, Cast(sum(sales_qty) as int) sales_qty from dbo.maui_basis_calcs with (nolock) 
			group by cust_entity_structure_fk) MBC on (es.entity_structure_id = mbc.Cust_Entity_Structure_FK or es.PARENT_ENTITY_STRUCTURE_FK = mbc.Cust_Entity_Structure_FK)
			where emt0.data_value_fk = DV.DATA_VALUE_ID
			order by mbc.sales_qty desc) Whse
	from
	EPACUBE.DATA_VALUE DV WITH (NOLOCK) WHERE DATA_NAME_FK = 244901
	) A where erp_value_1 is not null 
	ORDER BY erp_value_1

-- Cust ID's
	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, ERP_Value_2, Class, Rule_Type, Reference_Name, Reference_FK, data_level, Whse, Update_Timestamp, Data_Elements_ID)
	select * 
	, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [Rule_Type], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
	Select
	144111 Data_Name_FK
	, 'CUSTOMER BILL TO' Name
	, [value] ERP_Value_1
--	, (select value from epacube.ENTITY_IDENT_NONUNIQUE where data_name_fk = 144112 and entity_data_name_fk = 144010 and entity_structure_fk = ei.entity_structure_fk) ERP_Value_2
	, (select top 1 value from epacube.ENTITY_IDENT_NONUNIQUE with (nolock) where data_name_fk = 144112 and entity_structure_fk = ei.entity_structure_fk) ERP_Value_2
	, 'Customer Criteria' 'Class'
	, 'Both' Rule_Type
	, 'Entity_Structure_FK' 'Reference_Name'
	, Entity_Structure_FK 'Reference_FK'
	, 2 'Data_Level'
	, (Select ea.attribute_event_data Whse from epacube.entity_attribute ea with (nolock) where ea.data_name_fk = 144900 and ea.entity_structure_fk = ei.entity_structure_fk) Whse
	from
	epacube.entity_identification ei with (nolock) where data_name_fk = 144111 and entity_data_name_fk = 144010
) A

	Insert into dbo.MAUI_Rules_Data_Elements
	(Data_Name_FK, Name, ERP_Value_1, ERP_Value_2, Class, Rule_Type, Reference_Name, Reference_FK, data_level, Whse, Update_Timestamp, Data_Elements_ID)
	select * 
	, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [Rule_Type], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
	from (
	Select
	144111 Data_Name_FK
	, 'CUSTOMER SHIP TO' Name
	, [value] ERP_Value_1
--	, (select value from epacube.ENTITY_IDENT_NONUNIQUE where data_name_fk = 144112 and entity_data_name_fk = 144020 and entity_structure_fk = ei.entity_structure_fk) ERP_Value_2
	, (select top 1 value from epacube.ENTITY_IDENT_NONUNIQUE with (nolock) where data_name_fk = 144112 and entity_structure_fk = ei.entity_structure_fk) ERP_Value_2
	, 'Customer Criteria' 'Class'
	, 'Both' Rule_Type
	, 'Entity_Structure_FK' 'Reference_Name'
	, Entity_Structure_FK 'Reference_FK'
	, 2 'Data_Level'
	, (Select ea.attribute_event_data Whse from epacube.entity_attribute ea with (nolock) where ea.data_name_fk = 144900 and ea.entity_structure_fk = ei.entity_structure_fk) Whse
	from
	epacube.entity_identification ei with (nolock) where data_name_fk = 144111 and entity_data_name_fk = 144020
) A

--Rounding Methods

Insert into dbo.MAUI_Rules_Data_Elements
(Name, ERP_Value_1, Class, Rule_Type, Reference_Name, Reference_FK, Data_Level, Update_Timestamp, Data_Elements_ID)
select * 
, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [Rule_Type], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
from (
Select 
Code 'Name'
, Case Code_Ref_ID when 723 then 'n' when 724 then 'u' when 725 then 'd' end ERP_Value_1
, 'Rounding Method' 'Class'
, 'PRICING' 'Rule_Type'
, 'Code_Ref_ID' 'Reference_Name'
, Code_Ref_ID 'Reference_FK'
, 2 'Data_Level'
from epacube.code_ref with (nolock) where code_ref_id between 723 and 725) A

Insert into dbo.MAUI_Rules_Data_Elements
(Name, Class, ERP_Value_1, Data_Level, Update_Timestamp, Data_Elements_ID, Rule_Type)
select * 
, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [ERP_Value_1], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock)), 'PRICING' 'Rule_Type'
from (
Select top 1 '100' Name, 'Rounding Value' Class, 1 ERP_Value_1, 2 Data_Level from epacube.data_Name with (nolock) Union
Select top 1 '10' Name, 'Rounding Value' Class, 2 ERP_Value_1, 2 Data_Level from epacube.data_Name with (nolock) Union
Select top 1 '1' Name, 'Rounding Value' Class, 3 ERP_Value_1, 2 Data_Level from epacube.data_Name with (nolock) Union
Select top 1 '0.10' Name, 'Rounding Value' Class, 4 ERP_Value_1, 2 Data_Level from epacube.data_Name with (nolock) Union
Select top 1 '0.01' Name, 'Rounding Value' Class, 5 ERP_Value_1, 2 Data_Level from epacube.data_Name with (nolock) Union
Select top 1 '0.001' Name, 'Rounding Value' Class, 6 ERP_Value_1, 2 Data_Level from epacube.data_Name with (nolock) Union
Select top 1 '0.0001' Name, 'Rounding Value' Class, 7 ERP_Value_1, 2 Data_Level from epacube.data_Name with (nolock) Union
Select top 1 '0.00001' Name, 'Rounding Value' Class, 8 ERP_Value_1, 2 Data_Level from epacube.data_Name with (nolock) Union
Select top 1 'Specify Value' Name, 'Rounding Value' Class, 9 ERP_Value_1, 2 Data_Level from epacube.data_Name with (nolock)) A

----Hierarchy Rules
--Pricing
Insert into dbo.MAUI_Rules_Data_Elements
(Name, ERP_Value_1, ERP_Value_2, Matrix_Text, Class, Data_Name_FK, Data_Level, Update_Timestamp, Data_Elements_ID, Rule_Type)
select * 
, 3, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [ERP_Value_1], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock)), 'PRICING' 'Rule_Type'
from (
Select Name, 1 ERP_Value_1,  ERP_Value_2, Matrix_Text, 'Rule Hierarchy Product' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Product_Identification' and Class = 'Product Criteria' and Data_Level = 1 and Rule_Type in ('Pricing', 'Both') Union
Select Name, 2 ERP_Value_1,  ERP_Value_2, Matrix_Text, 'Rule Hierarchy Product' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Product_Category' and Class = 'Product Criteria' and Data_Level = 1 and Rule_Type in ('Pricing', 'Both') Union
Select Name, 3 ERP_Value_1,  ERP_Value_2, Matrix_Text, 'Rule Hierarchy Product' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Product_Identification' and Class = 'Product Criteria' and Data_Level = 1 and Rule_Type in ('Pricing', 'Both') Union
Select Name, 4 ERP_Value_1,  ERP_Value_2, Matrix_Text, 'Rule Hierarchy Product' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Product_Category' and Class = 'Product Criteria' and Data_Level = 1 and Rule_Type in ('Pricing', 'Both') and Data_Name_FK in (211901, 211903, 211904) Union
Select Name, 7 ERP_Value_1,  ERP_Value_2, Matrix_Text, 'Rule Hierarchy Product' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Product_Identification' and Class = 'Product Criteria' and Data_Level = 1 and Rule_Type in ('Pricing', 'Both') Union
Select Name, 8 ERP_Value_1,  ERP_Value_2, Matrix_Text, 'Rule Hierarchy Product' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Product_Category' and Class = 'Product Criteria' and Data_Level = 1 and Rule_Type in ('Pricing', 'Both') and Data_Name_FK in (211901) Union

Select Name, 1 ERP_Value_1,  ERP_Value_2, Matrix_Text, 'Rule Hierarchy Customer' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Entity_Structure' and Class = 'Customer Criteria' and Data_Level = 1 and Rule_Type in ('Pricing', 'Both') Union
Select Name, 2 ERP_Value_1,  ERP_Value_2, Matrix_Text, 'Rule Hierarchy Customer' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Entity_Structure' and Class = 'Customer Criteria' and Data_Level = 1 and Rule_Type in ('Pricing', 'Both') Union

Select Name, 3 ERP_Value_1,  ERP_Value_2, Matrix_Text, 'Rule Hierarchy Customer' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Entity_Mult_Type' and Class = 'Customer Criteria' and Data_Level = 1 and Rule_Type in ('Pricing', 'Both') Union
Select Name, 4 ERP_Value_1,  ERP_Value_2, Matrix_Text, 'Rule Hierarchy Customer' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Entity_Mult_Type' and Class = 'Customer Criteria' and Data_Level = 1 and Rule_Type in ('Pricing', 'Both') Union
Select Name, 5 ERP_Value_1,  ERP_Value_2, Matrix_Text, 'Rule Hierarchy Customer' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Entity_Structure' and Class = 'Customer Criteria' and Data_Level = 1 and Rule_Type in ('Pricing', 'Both') Union
Select Name, 6 ERP_Value_1,  ERP_Value_2, Matrix_Text, 'Rule Hierarchy Customer' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Entity_Mult_Type' and Class = 'Customer Criteria' and Data_Level = 1 and Rule_Type in ('Pricing', 'Both')
) A
--Rebates
Insert into dbo.MAUI_Rules_Data_Elements
(Name, ERP_Value_1, ERP_Value_2, Matrix_Text, Class, Data_Name_FK, Data_Level, Update_Timestamp, Data_Elements_ID, Rule_Type)
select * 
, 3, Getdate(), Data_Elements_ID = Row_Number() over (Order by [ERP_Value_1], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock)), 'REBATE' 'Rule_Type'
from (
Select Name, 1 ERP_Value_1, ERP_Value_2, Matrix_Text, 'Rule Hierarchy Product' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Product_Identification' and Class = 'Product Criteria' and Data_Level = 1 and Data_Name_FK in (@Prod_ID_DN_FK) Union
Select Name, 2 ERP_Value_1, ERP_Value_2, Matrix_Text, 'Rule Hierarchy Product' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Product_Category' and Class = 'Product Criteria' and Data_Level = 1 and Data_Name_FK in (211903, 211904) Union
Select Name, 3 ERP_Value_1, ERP_Value_2, Matrix_Text, 'Rule Hierarchy Product' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Product_Category' and Class = 'Product Criteria' and Data_Level = 1 and Data_Name_FK in (211901) Union
Select Name, 4 ERP_Value_1, ERP_Value_2, Matrix_Text, 'Rule Hierarchy Product' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Product_Category' and Class = 'Product Criteria' and Data_Level = 1 and Data_Name_FK in (211902) Union
Select Name, 5 ERP_Value_1, ERP_Value_2, Matrix_Text, 'Rule Hierarchy Product' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Data_Element_Source_Table = 'Product_Category' and Class = 'Product Criteria' and Data_Level = 1 and Data_Name_FK in (210901) Union

Select Name, 1 ERP_Value_1, ERP_Value_2, Matrix_Text, 'Rule Hierarchy Customer' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Class = 'Customer Criteria' and Data_Level = 1 and Rule_Type in ('Rebate', 'Both') Union
Select Name, 2 ERP_Value_1, ERP_Value_2, Matrix_Text, 'Rule Hierarchy Customer' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Class = 'Customer Criteria' and Data_Level = 1 and Rule_Type in ('Rebate', 'Both') Union
Select Name, 3 ERP_Value_1, ERP_Value_2, Matrix_Text, 'Rule Hierarchy Customer' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Class = 'Customer Criteria' and Data_Level = 1 and Rule_Type in ('Rebate', 'Both') Union
Select Name, 4 ERP_Value_1, ERP_Value_2, Matrix_Text, 'Rule Hierarchy Customer' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Class = 'Customer Criteria' and Data_Level = 1 and Rule_Type in ('Rebate', 'Both') Union
Select Name, 5 ERP_Value_1, ERP_Value_2, Matrix_Text, 'Rule Hierarchy Customer' Class, Data_Name_FK from dbo.MAUI_Rules_Data_Elements with (nolock) where Class = 'Customer Criteria' and Data_Level = 1 and Rule_Type in ('Rebate', 'Both')
) A

--Host_Matrix_Info
--Pricing
Insert into dbo.MAUI_Rules_Data_Elements
(Name, ERP_Value_1, Seq, Matrix_Text, Matrix_Text_Customer, Matrix_Text_Product, Class, Data_Level, Update_Timestamp, Data_Elements_ID, Rule_Type)
select * 
, 3, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [ERP_Value_1], epaMatrix, isnull(Seq, 0)) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock)), 'PRICING' 'Rule_Type'
from (
Select * from (
Select 
Isnull(Replace(Replace(Custs.Name, 'Customer Ship TO', 'CUSTOMER ID'), 'Customer BILL TO', 'CUSTOMER ID'), '') 
	+ Case When Custs.Matrix_Text is not null and Prods.Matrix_Text is not null then ': ' else '' end +
	Isnull(Prods.Name, '') epaMatrix
, Levels.ERP_Value_1
, isnull(Prods.ERP_Value_2, Custs.ERP_Value_2) Seq
, isnull(Custs.Matrix_Text, '')
	+ Case When Custs.Matrix_Text is not null and Prods.Matrix_Text is not null then '-' else '' end
	+ isnull(Prods.Matrix_Text, '') Matrix_Text
, Custs.Matrix_Text Matrix_Text_Customer
, Prods.Matrix_Text Matrix_Text_Product
, 'Rule Hierarchy Pricing' Class
from 
(Select ERP_Value_1 from dbo.maui_rules_data_elements with (nolock) where class like 'Rule Hierarchy%' and rule_type = 'Pricing' group by ERP_Value_1) Levels
	Left Join (Select * from dbo.maui_rules_data_elements with (nolock) where class = 'Rule Hierarchy Product' and rule_type = 'Pricing' and Matrix_Text is not null) Prods On Levels.ERP_Value_1 = Prods.ERP_Value_1
	Left Join (Select * from dbo.maui_rules_data_elements with (nolock) where class = 'Rule Hierarchy Customer' and rule_type = 'Pricing' and Matrix_Text is not null) Custs On Levels.ERP_Value_1 = Custs.ERP_Value_1
) Rules_Hierarchy_Basis
Group by epaMatrix, ERP_Value_1, Seq, Matrix_Text, Class, Matrix_Text_Customer, Matrix_Text_Product
) Rules_Hierarchy
Order by ERP_Value_1, Seq

--Rebates
Insert into dbo.MAUI_Rules_Data_Elements
(Name, ERP_Value_1, Seq, Matrix_Text, Class, Data_Level, Update_Timestamp, Data_Elements_ID, Rule_Type)
select * 
, 3, GETDATE(), Data_Elements_ID = Row_Number() over (Order by [ERP_Value_1], epaMatrix, isnull(Seq, 0)) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock)), 'REBATE' 'Rule_Type'
from (
Select * from (
Select 
Isnull(Replace(Replace(Custs.Name, 'Customer Ship TO', 'CUSTOMER ID'), 'Customer BILL TO', 'CUSTOMER ID'), '') 
	+ Case When Custs.Matrix_Text is not null and Prods.Matrix_Text is not null then ': ' else '' end +
	Isnull(Prods.Name, '') epaMatrix
, Levels.ERP_Value_1
, isnull(Prods.ERP_Value_2, Custs.ERP_Value_2) Seq
, isnull(Custs.Matrix_Text, '')
	+ Case When Custs.Matrix_Text is not null and Prods.Matrix_Text is not null then '-' else '' end
	+ isnull(Prods.Matrix_Text, '') Matrix_Text
, 'Rule Hierarchy Rebate' Class
from 
(Select ERP_Value_1 from dbo.maui_rules_data_elements with (nolock) where class like 'Rule Hierarchy%' and rule_type = 'Rebate' group by ERP_Value_1) Levels
	Left Join (Select * from dbo.maui_rules_data_elements with (nolock) where class = 'Rule Hierarchy Product' and rule_type = 'Rebate' and Matrix_Text is not null) Prods On Levels.ERP_Value_1 = Prods.ERP_Value_1
	Left Join (Select * from dbo.maui_rules_data_elements with (nolock) where class = 'Rule Hierarchy Customer' and rule_type = 'Rebate' and Matrix_Text is not null) Custs On Levels.ERP_Value_1 = Custs.ERP_Value_1
) Rules_Hierarchy_Basis
Group by epaMatrix, ERP_Value_1, Seq, Matrix_Text, Class
) Rules_Hierarchy
Order by ERP_Value_1, Seq

insert into dbo.maui_log Select @Job_FK_i, 'Rules Data Elements Loaded', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI Rules Data Elements'), 108)), Null--(Select COUNT(*) from dbo.MAUI_Rules_Data_Elements with (nolock))

-------
Delete from dbo.maui_configuration where class = 'Whse'
	Insert into dbo.maui_configuration
	(Value, Value_Group, Class, Record_Status_CR_FK, Boolean_Status, Value_Seq, Configuration_ID)
	Select *, Data_Elements_ID 'Value_Seq' from (
	select * 
	, Data_Elements_ID = Row_Number() over (Order by Case When Isnumeric(Value) = 0 then 99999999 else Value end)
	from (
	Select
	Value
	, (Select Value from epacube.entity_identification with (nolock) where data_name_fk = 141112 and entity_structure_fk = ei.entity_structure_fk) Value_Group
	, 'Whse' 'Class'
	, 1 'Record_Status_CR_FK'
	, -1 'Boolean_Staatus'
	from
	epacube.entity_identification ei with (nolock) where data_name_fk = 141111
		) A ) B

Delete from dbo.maui_configuration where class in ('Operation', 'Operation-Price', 'Operation-Rebate')
	Insert into dbo.maui_configuration
	(Value, Class, Record_Status_CR_FK, Boolean_Status, Configuration_ID)
	select * 
	, Data_Elements_ID = Row_Number() over (Order by [Value]) from (
		Select 
		Replace(Replace(Name, ' (N)',''), 'GROSS MARGIN', 'GROSS PROFIT') as Value
		, 'Operation-Price' Class
		, 1 Record_Status_CR_FK
		, -1 Boolean_Status
	from dbo.MAUI_Rules_Data_Elements with (nolock) where data_level = 1 and Class = 'Operation' and rule_type in ('Pricing', 'Both')
		AND NAME NOT IN ('50 Percent Rebate Sharing', 'Carrier Zcurve', 'SX Price Gross Margin(N)', 'SX Price Gross Margin(O)')
		AND NAME NOT IN (Select Case When (Select value from epacube.epacube_params where name = 'EPACUBE CUSTOMER') not like '%JASCO%' then 'Jasco Desired DI-1 Margin' else '' end)
	) A

	Insert into dbo.maui_configuration
	(Value, Class, Record_Status_CR_FK, Boolean_Status, Configuration_ID)
	select * 
	, Data_Elements_ID = Row_Number() over (Order by [Value]) from (
		Select 
		Replace(Name, ' (N)','') as Value
		, 'Operation-Rebate' Class
		, 1 Record_Status_CR_FK
		, -1 Boolean_Status
	from dbo.MAUI_Rules_Data_Elements with (nolock) where data_level = 1 and Class = 'Operation' and rule_type in ('Rebate', 'Both')
	AND NAME NOT IN ('SX Price Gross Margin(N)', 'SX Price Gross Margin(O)')
	AND NAME NOT IN (Select Case When (Select value from epacube.epacube_params where name = 'EPACUBE CUSTOMER') not like '%JASCO%' then 'Jasco Desired DI-1 Margin' else '' end)
	) A

Insert into dbo.MAUI_Rules_Data_Elements
(Name, ERP_Value_1, Class, Rule_Type, Data_Level, Data_Name_FK, Parent_Value, UPDATE_TIMESTAMP, UPDATE_USER, Data_Elements_ID)
Select Name, ERP_Value_1, Class, Rule_Type, Data_Level, Data_Name_FK, Parent_Value, Update_Timestamp, UPDATE_USER, Data_Elements_ID = Row_Number() over (Order by [ERP_Value_1], Name) + (Select Count(*) from dbo.MAUI_Rules_Data_Elements with (nolock))
from #Defaults 

Drop Table #defaults

Update cfg
Set Record_status_cr_fk = recs.record_status_cr_fk
, boolean_status = recs.boolean_status
from dbo.maui_configuration cfg
	inner join #config recs on cfg.class = recs.class and cfg.value = recs.value

Drop Table #config

insert into dbo.maui_log Select @Job_FK_i, 'Rules Data Elements Loaded and Updated', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI Rules Data Elements'), 108)), (Select COUNT(*) from dbo.MAUI_Rules_Data_Elements with (nolock))

--Create Unique Index idx_ids on dbo.MAUI_Rules_Data_Elements(data_elements_id)
--Create index idx_rule_crit on dbo.MAUI_Rules_Data_Elements (Name, Class, Data_Level, Rule_Type, data_name_fk, ERP_Value_1, Reference_Name, Reference_FK)

END
