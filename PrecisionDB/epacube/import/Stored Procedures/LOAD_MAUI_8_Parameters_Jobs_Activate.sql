﻿--A_epaMAUI_LOAD_MAUI_8_Parameters_Jobs_Activate

-- =============================================
-- Author:		Gary Stone
-- Create date: 9/7/2011
-- Description:	MAUI DATA LOAD
-- =============================================
CREATE PROCEDURE [import].[LOAD_MAUI_8_Parameters_Jobs_Activate] 
	@Job_FK BigInt, @Job_FK_i Numeric(10, 2), @Defaults_To_Use as Varchar(64)
AS
BEGIN

	SET NOCOUNT ON;
	
Declare @Job_FK_i_BL Numeric(10, 2)
Declare @ProjectionDays Numeric(5, 0)
Declare @HistoryDays Numeric(5, 0)
Declare @AssType Int
Declare @RebateMethod Int
Declare @PriceMethod Int
Declare @UseFileDates Int

Set @Job_FK_i_BL = (Select top 1 BL_Job_FK_i from dbo.MAUI_Basis_Baseline group by BL_Job_FK_i, BL_Sales_History_Start_Date, BL_Sales_History_End_Date) --(Select BL_Job_FK_i, CONVERT(VARCHAR(10), BL_Sales_History_Start_Date, 101) [Start_Date], CONVERT(VARCHAR(10), BL_Sales_History_End_Date, 101) [End_Date] from dbo.MAUI_Basis_Baseline group by BL_Job_FK_i, BL_Sales_History_Start_Date, BL_Sales_History_End_Date)
Set @AssType = (Select top 1 Assessment_type from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use)
Set @RebateMethod = (Select top 1 Future_Rebate_Lookup_Method from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use)		--@RebateMethod  1 = Lookup New Rebate Amounts; @RebateMethod  2 = Calculate New Rebate Amounts
Set @PriceMethod = (Select top 1 Future_Price_Lookup_Method from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use)
Set @HistoryDays = Cast((Select top 1 Sales_History_End_Date - Sales_History_Start_Date 
	from dbo.MAUI_BASIS where job_fk_i = @job_fk_i) as Numeric(6,0)) 
Set @UseFileDates = (Select top 1 UseFileDates from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use)
Set @ProjectionDays =
	Case when @UseFileDates = 0 then (Select top 1 ProjectionDays from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) else @HistoryDays	end

Update MPJ 
Set Job_Name = 'Validation'
, What_IF = 0
, OverRide_Type = 1
, Cost_Calc_Method = 'Multiply By'
, Cost_Calc_Value = 1
, Cost_Calc_Level = 'Global'
, Price_Calc_Method = 'Multiply By'
, Price_Calc_Value = 1
, Price_Calc_Level = 'Global'
, Qty_Calc_Value = 1
, Qty_Calc_Level = 'Global'
, FileDateRange = MBC.Sales_History_Range
, Archive_Status = 0
, Assessment_Type = @AssType
, ProjectionDays = @ProjectionDays
, RebateMethod = @RebateMethod
, PriceMethod = @PriceMethod
, Sales_History_Start_Date = MBC.Sales_History_Start_Date
, Sales_History_End_Date = MBC.Sales_History_End_Date
, Sales_History_End_Date_Projected = MBC.Sales_History_End_Date_Projected
, Assessment_Start_Date = MBC.Assessment_Start_Date
, Assessment_End_Date = MBC.Assessment_End_Date
, UseFileDates = @UseFileDates
, BL_Job_FK_i = @Job_FK_i_BL
, BL_Start_Date = MBC.BL_Start_Date
, BL_End_Date = MBC.BL_End_Date
, chkCachedOverview = MBC.chkCachedOverview
, chkShowThousands = MBC.chkShowThousands
, OverRide_Threshold = MBC.OverRide_Threshold
, Record_status_cr_fk = 1
, chkConvToEach = isnull((Select chkConvToEach from dbo.maui_defaults where Default_Name = @Defaults_To_Use), 0)
from dbo.MAUI_Parameters_Jobs MPJ
Inner Join (Select Top 1 
Job_FK_i
, CONVERT(VARCHAR(8), Sales_History_Start_Date, 1) + ' - ' +
	CONVERT(VARCHAR(8), Sales_History_End_Date, 1) 'Sales_History_Range'
, CONVERT(VARCHAR(10), Sales_History_Start_Date, 101) 'Sales_History_Start_Date'
, CONVERT(VARCHAR(10), Sales_History_End_Date, 101) 'Sales_History_End_Date'
, CONVERT(VARCHAR(10), Sales_History_Start_Date + @ProjectionDays, 101) 'Sales_History_End_Date_Projected'
, CONVERT(VARCHAR(10), Margin_Cost_Basis_Effective_Date, 101) 'Assessment_Start_Date'
, CONVERT(VARCHAR(10), Margin_Cost_Basis_Effective_Date + @ProjectionDays, 101) 'Assessment_End_Date'
, (Select top 1 bl_sales_history_start_date from dbo.MAUI_Basis_Baseline where bl_job_fk_i = @Job_FK_i_BL) BL_Start_Date
, (Select top 1 bl_sales_history_end_date from dbo.MAUI_Basis_Baseline where bl_job_fk_i = @Job_FK_i_BL) BL_End_Date
, (Select max(abs(chkCachedOverview)) from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) 'chkCachedOverview'
, (Select max(abs(chkShowThousands)) from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) 'chkShowThousands'
, (Select max(abs(OverRide_Threshold)) from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) 'OverRide_Threshold'
From dbo.MAUI_Basis_Calcs with (nolock) where Job_FK_i = @Job_FK_i) MBC on MPJ.Job_FK_i = MBC.Job_FK_i and MPJ.Job_FK_i = @Job_FK_i

insert into dbo.maui_log Select @Job_FK_i, 'MAUI Parameters Jobs Updated', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null

EXEC [import].[LOAD_MAUI_9_DIMENSIONS] @Job_FK, @Job_FK_i

END
