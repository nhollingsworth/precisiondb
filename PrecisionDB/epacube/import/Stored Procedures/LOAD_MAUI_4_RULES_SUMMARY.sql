﻿--A_epaMAUI_LOAD_MAUI_4_RULES_SUMMARY

-- =============================================
-- Author:		Gary Stone
-- Create date: 9/7/2011
-- Updated June 29, 2013 by GHS
-- Description:	MAUI DATA LOAD
-- =============================================
CREATE PROCEDURE [import].[LOAD_MAUI_4_RULES_SUMMARY]
	@Job_FK bigint, @Job_FK_i Numeric(10, 2), @Defaults_To_Use as Varchar(64)
AS
BEGIN

Set NOCOUNT On

--Declare @Job_FK bigint
--Declare @Job_FK_i Numeric(10, 2)
--Declare @Defaults_To_Use as Varchar(64)

--Set @Job_FK = 2818
--Set @Job_FK_i = 2817.01
--Set @Defaults_To_Use = 'Primary'

Declare @OpDiv Numeric(18, 6)

Set @OpDiv = (Select value from epacube.EPACUBE_PARAMS with (nolock) where NAME = 'operand divisor')

--insert into dbo.maui_log Select @Job_FK_i, 'Start Loading MAUI Rules Summary', GetDate(), Null, Null
--
Delete from dbo.MAUI_Rules_Summary where Job_FK_i = @Job_FK_i

IF object_id('tempdb..##rules') is not null
drop table ##rules
IF object_id('tempdb..##Uniques') is not null
drop table ##Uniques

select job_fk_i Job_fk_i2, identifier identifier2, mb1.calc_status, count(*) recs 
into ##Uniques 
	from dbo.MAUI_basis mb1  with (nolock) where job_fk_i = @Job_FK_i 
	group by job_fk_i, identifier, mb1.calc_status having count(*) = 1
	
Create index idx_unq on ##Uniques(Job_FK_i2, Identifier2, calc_status)

select 
MB.Job_FK
, MB.Calc_Status
, MB.Margin_Cost_Basis_Effective_Date
, MB.[Product_ID]
, MB.Prod_Description
, MB.Whse
, MB.Sales_Qty
, MB.Sell_Price_Cust_Host_Rule_Xref  Price_Rule_ID
, MB.Sell_Price_Cust_Rules_FK
, MB.Price_Rule_Type
, Case When sell_price_cust_rules_fk not in (2116021, 2116022) then MB.Sell_Price_Cust_Filter_Name end 'Customer Reference'
, Case When sell_price_cust_rules_fk not in (2116021, 2116022) then MB.Sell_Price_Cust_Filter_Value end 'Customer Focus'
, Case When sell_price_cust_rules_fk not in (2116021, 2116022) then MB.Sell_Price_Prod_Filter_Name end 'Product Reference'
, Case When sell_price_cust_rules_fk not in (2116021, 2116022) then MB.Sell_Price_Prod_Filter_Value end 'Product Focus'
, (Select value1 from synchronizer.rules_filter with (nolock) where Data_Name_FK = 141111 and entity_data_name_fk in (141000, 141030) and rules_Filter_ID =
	(Select top 1 Org_Filter_FK from synchronizer.rules_filter_set with (nolock) where rules_fk = MB.sell_Price_cust_Rules_FK)) 'Rule Org'
, MB.Sell_Price_Cust_Operation 'Operation'
, mb.Price_Operand_Pos
, mb.Discount_Operand_Pos
, Case When sell_price_cust_rules_fk not in (2116021, 2116022) then mb.Sell_Price_Cust_Number_1 end 'Operand'
, mb.Sell_Price_Cust_Basis_1_Col 'Rule Basis 1'
, mb.Sell_Price_Cust_Basis_1_Value * SPC_UOM 'Rule Value 1'
, mb.Sell_Price_Cust_Basis_2_Col 'Rule Basis 2'
, mb.Sell_Price_Cust_Basis_2_Value * SPC_UOM 'Rule Value 2'
, MB.Margin_Cost_Basis_Amt - isnull(MB.Rebate_CB_Amt, 0) 'Margin_Cost_Basis_Amt'
, MB.Rebate_CB_Amt
, MB.Rebate_Buy_Amt
, MB.Standard_Cost
, MB.Replacement_Cost
, r.effective_Date Sell_Price_Cust_Effective_Date
, r.end_Date Sell_Price_Cust_End_Date
, MB.Sell_Price_Cust_Amt
, MB.[Last OverRide Invoice Date]
, MB.[Last Override Price]
, MB.[Override Qty]
, MB.[Override Transactions]
, MB.[Total Transactions]
, MB.Product_Structure_fk
, MB.Org_Entity_Structure_FK
, MB.Cust_Entity_Structure_FK
, MB.Supl_Entity_Structure_FK
, MB.Qty_Break_Rule_Ind
, MB.Job_FK_i
, MB.Sell_Price_Cust_Host_Rule_Xref
, rao1.*
into ##rules
from dbo.MAUI_basis MB  with (nolock)
inner join synchronizer.RULES r with (nolock) on mb.Sell_Price_Cust_Rules_FK = r.rules_id
inner join synchronizer.RULES_ACTION ra with (nolock) on r.RULES_ID = ra.RULES_FK
left join synchronizer.RULES_ACTION_OPERANDS rao1 with (nolock) on ra.RULES_ACTION_ID = rao1.RULES_ACTION_FK and rao1.Basis_Position = 1
Inner join
##Uniques A 
on mb.job_fk_i = A.job_fk_i2 and mb.identifier = A.identifier2
where mb.job_fk_i = @job_fk_i --and mb.Calc_Status = 'current'

Create index idx_rules on ##Rules
(
Sell_Price_Cust_Host_Rule_Xref
, Sell_Price_Cust_Rules_FK
, Calc_Status
, Margin_Cost_Basis_Effective_Date
, [Customer Reference]
, [Customer Focus]
, [Product Reference]
, [Product Focus]
, [Rule Org]
, [Price_Rule_Type]
, Sell_Price_Cust_Effective_Date
, Sell_Price_Cust_End_Date
, Price_Operand_Pos
, Discount_Operand_Pos
, Operation
)

Insert Into dbo.MAUI_Rules_Summary
(
[Job_FK_i], [Sell_Price_Cust_Host_Rule_Xref], [Sell_Price_Cust_Rules_FK], [Calc_Status], [Margin_Cost_Basis_Effective_Date], [Customer Reference], [Customer Focus], [Product Reference], [Product Focus], [Rule Org], [Sell_Price_Cust_Effective_Date], [Sell_Price_Cust_End_Date], [Price_Operand_Pos], [Discount_Operand_Pos], [Operation], [Price_Rule_Type], [Op_Criteria], [Sales_Qty], [Rule Basis], [Rule Basis Value], [Rule Sell Price], [Rule Rebate CB Dollars], [Rule Rebate Buy Dollars], [Standard Cost], [Replacement Cost], [Margin Cost Basis Dollars], [Override Qty], [Override Transactions], [Total Transactions], [Override_Pct_Qty], [Override_Pct_Transactions], [Qty_Break_Rule_Ind], [QtyBrk1], [QtyBrk2], [QtyBrk3], [QtyBrk4], [QtyBrk5], [QtyBrk6], [QtyBrk7], [QtyBrk8], [QtyBrk9], [Operand1], [Operand2], [Operand3], [Operand4], [Operand5], [Operand6], [Operand7], [Operand8], [Operand9], [Record_Status_CR_FK]
)
Select
Job_FK_i
, Sell_Price_Cust_Host_Rule_Xref
, Sell_Price_Cust_Rules_FK
, Calc_Status
, Margin_Cost_Basis_Effective_Date
, [Customer Reference]
, [Customer Focus]
, [Product Reference]
, [Product Focus]
, [Rule Org]
, Sell_Price_Cust_Effective_Date
, Sell_Price_Cust_End_Date
, Price_Operand_Pos
, Discount_Operand_Pos
, Operation
, isnull([Price_Rule_Type], 'Default Pricing') 'Price_Rule_Type'
, Max(Isnull(Cast(Case Operation when 'Fixed Value' then Operand
	else Operand / @OpDiv end as Numeric(18, 6)), 0.00)) Op_Criteria
,Cast(Sum(Sales_Qty) as Numeric(18, 6)) Sales_Qty
, Replace(Max(Case When isnull([Rule value 2] , 0) = 0 then [Rule Basis 1]
	When isnull([Rule Value 1], 0) > [Rule Value 2] then [Rule Basis 2] else [Rule Basis 1] end), 'ORDER COGS', 'Rebated Cost') 'Rule Basis'

, Cast(Sum(Case When Operation = 'Fixed Value' then Margin_Cost_Basis_Amt
	When isnull([Rule value 2] , 0) = 0 then [Rule Value 1]
	When isnull([Rule Value 1], 0) > [Rule Value 2] then [Rule Value 2] else [Rule Value 1] end * Sales_Qty) as Numeric(18, 6)) 'Rule Basis Value'

, Cast(Sum(Sell_Price_Cust_Amt * [Sales_Qty]) as Numeric(18, 6)) 'Rule Sell Price'
, Cast(SUM(Rebate_CB_Amt * [Sales_Qty]) as Numeric(18, 6)) 'Rule Rebate CB Dollars'
, Cast(SUM(Rebate_BUY_Amt * [Sales_Qty]) as Numeric(18, 6)) 'Rule Rebate BUY Dollars'
,Cast(Sum(Standard_Cost * Sales_Qty) as numeric(18, 6)) 'Standard Cost'
,Cast(Sum(Replacement_Cost * Sales_Qty) as numeric(18, 6)) 'Replacement Cost'
,Cast(Sum(Margin_Cost_Basis_Amt * Sales_Qty) as numeric(18, 6)) 'Margin_Cost_Basis_Dollars'
,Cast(Sum([Override Qty]) as Numeric(18, 6)) 'Override Qty'
,Cast(Sum([Override Transactions]) as Numeric(18, 6)) 'Override Transactions'
,Cast(Sum([Total Transactions]) as Numeric(18, 6)) 'Total Transactions'
,Cast(Case Sum(Sales_Qty) when 0 then 0 else Sum([Override Qty]) / Sum([Sales_Qty]) end as Numeric(18, 6)) 'Override_Pct_Qty'
,Cast(Case Sum([Total Transactions]) When  0 then  0 else Sum([Override Transactions]) / Sum([Total Transactions]) end as Numeric(18, 6)) 'Override_Pct_Transactions'
,Max(Qty_Break_Rule_Ind) Qty_Break_Rule_Ind
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value1 end QtyBrk1
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value2 end  QtyBrk2
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value3 end  QtyBrk3
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value4 end  QtyBrk4
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value5 end  QtyBrk5
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value6 end  QtyBrk6
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value7 end  QtyBrk7
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value8 end  QtyBrk8
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value9 end  QtyBrk9
, Case when A.basis_position is null then Operand else Operand1 end Operand1
, A.Operand2
, A.Operand3
, A.Operand4
, A.Operand5
, A.Operand6
, A.Operand7
, A.Operand8
, A.Operand9
, 1
From ##Rules A
Group by Sell_Price_Cust_Host_Rule_Xref
, Sell_Price_Cust_Rules_FK
, Calc_Status
, Margin_Cost_Basis_Effective_Date
, [Customer Reference]
, [Customer Focus]
, [Product Reference]
, [Product Focus]
, [Rule Org]
, [Price_Rule_Type]
, Sell_Price_Cust_Effective_Date
, Sell_Price_Cust_End_Date
, Price_Operand_Pos
, Discount_Operand_Pos
, Operation
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value1 end 
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value2 end  
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value3 end  
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value4 end  
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value5 end  
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value6 end  
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value7 end  
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value8 end  
, Case A.OPERAND_FILTER_OPERATOR_CR_FK when 9020 then A.Filter_Value9 end  
, Case when A.basis_position is null then Operand else Operand1 end 
, A.Operand2
, A.Operand3
, A.Operand4
, A.Operand5
, A.Operand6
, A.Operand7
, A.Operand8
, A.Operand9
--, Operand
, Job_FK_i
having Sum(Sell_Price_Cust_Amt * [Sales_Qty]) <> 0
Order by [Product Reference] Desc, [Product Focus] Desc

IF object_id('tempdb..##rules') is not null
drop table ##rules
IF object_id('tempdb..##Uniques') is not null
drop table ##Uniques

insert into dbo.maui_log Select @Job_FK_i, 'Rules Summary Load Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI Rules Summary'), 108)), (Select COUNT(*) from dbo.MAUI_Rules_Summary where Job_FK_i = @Job_FK_i)

EXEC IMPORT.LOAD_MAUI_5_VARIANCES @Job_FK, @Job_FK_i, @Defaults_To_Use

END
