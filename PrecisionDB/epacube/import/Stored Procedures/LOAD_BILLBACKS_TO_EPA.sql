﻿





-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        06/08/2017   Initial Version 


CREATE PROCEDURE [import].[LOAD_BILLBACKS_TO_EPA] ( @IN_JOB_FK bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.LOAD_BILLBACKS_TO_EPA', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


-------------------------------------------------------------------------------------
--truncate table [import].[BILLBACK_TO_EPA_HEADER]
--truncate table [import].[BILLBACK_TO_EPA_DETAIL]


 select * from [import].[BILLBACK_TO_EPA_HEADER]
  select * from [import].[BILLBACK_TO_EPA_DETAIL]
  select * from import.billback_to_epa_dollars


  -----creating test data that I hope to get from Netsuite

  --select top 100 h.proposal_id, d.ad_group, h.start_date, h.end_date, d.program_type, h.payment_method
  --,d.proposal_amount_lump_sum, d.proposal_Amount_by_case, h.manufacturer_customer, h.proposal_grouping, h.billing_movement_source, h.termination_date_override
  --,h.comment, h.theme, h.performance_requirement, h.presenting_entity, h.Promo_dollars_entity, h.Bill_back_entity, h.create_timestamp, h.create_user, h.update_timestamp, h.update_user
  -- from epacube.promo_proposal_header h
  --inner join [epacube].[PROMO_PROPOSAL_DOLLARS] D on d.proposal_id = h.proposal_id


  --select top 100 h.proposal_id, d.ad_group,  d.program_type, d.proposal_amount_lump_sum, d.proposal_Amount_by_case, h.create_timestamp, h.create_user, h.update_timestamp, h.update_user
  -- from epacube.promo_proposal_header h
  --inner join [epacube].[PROMO_PROPOSAL_DOLLARS] D on d.proposal_id = h.proposal_id

  -- select top 100 a.proposal_id, pi.value,  NULL store_pack, a.allowance_type, a.start_date, a.end_date, a.percent_off, a.percent_off_basis, a.allowance_amount 
  --,a.program_type, a.create_date, a.create_user
  --from epacube.promo_proposal_item_allow A
  --inner join epacube.promo_proposal_header P on p.proposal_id = a.proposal_id
  --inner join epacube.product_identification pi on pi.product_structure_fk = a.product_structure_fk  and pi.data_name_fk = 110100
  

  ---
  ----------------------------------------------------------

  ---First get entity structures on header table

  --- When new proposal and we do not have ENTITY?  WHAT do we do?  

   select * from [import].[BILLBACK_TO_EPA_HEADER]  B
   inner join epacube.entity_identification ei on 
   b.presenting_entity = ei.value
   and ei.data_name_fk = 144111  

   update B
    set PRESENTING_ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
	from [import].[BILLBACK_TO_EPA_HEADER]  B
   inner join epacube.entity_identification ei on 
   b.presenting_entity = ei.value
   and ei.data_name_fk = 144111 
   and b.JOB_FK = @IN_JOB_FK

------

 UPDATE B
 SET PROMO_DOLLARS_ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK 
  from [import].[BILLBACK_TO_EPA_HEADER]  B
   inner join epacube.entity_identification ei on 
   b.promo_dollars_entity = ei.value
   and ei.data_name_fk = 144111
   and b.JOB_FK = @IN_JOB_FK


   --  select * from [import].[BILLBACK_TO_EPA_HEADER]  B
   --inner join epacube.entity_identification ei on 
   --b.promo_dollars_entity = ei.value
   --and ei.data_name_fk = 144111
---------

UPDATE B
SET BILL_BACK_ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
from [import].[BILLBACK_TO_EPA_HEADER]  B
   inner join epacube.entity_identification ei on 
   b.bill_back_entity = ei.value
   and ei.data_name_fk = 144111 
   and b.JOB_FK = @IN_JOB_FK


   -- select * from [import].[BILLBACK_TO_EPA_HEADER]  B
   --inner join epacube.entity_identification ei on 
   --b.bill_back_entity = ei.value
   --and ei.data_name_fk = 144111 
--------------------------------------------------------------------------------------------
----Get Item structure on Detail table

--select * from [import].[BILLBACK_TO_EPA_DETAIL] B
--inner join epacube.PRODUCT_IDENTIFICATION pi on
--b.ns_item_id = pi.value and pi.data_name_fk = 110100

UPDATE B
set product_structure_fk = pi.PRODUCT_STRUCTURE_FK
 from [import].[BILLBACK_TO_EPA_DETAIL] B
inner join epacube.PRODUCT_IDENTIFICATION pi on
b.ns_item_id = pi.value and pi.data_name_fk = 110100
and b.JOB_FK = @IN_JOB_FK



------------------------------------------------------------------------------------------------


----CREATE INSERTS FOR NEW PROPOSAL DATA


--select imp.* from [import].[BILLBACK_TO_EPA_HEADER]  IMP
--left join epacube.PROMO_PROPOSAL_HEADER PPH
--on PPH.proposal_id = imp.Proposal_id
--where PPH.proposal_id is NULL



INSERT INTO [epacube].[PROMO_PROPOSAL_HEADER]
           ([PROPOSAL_ID]
           ,[START_DATE]
           ,[END_DATE]
           ,[PAYMENT_METHOD]
           ,[MANUFACTURER_CUSTOMER]
           ,[PROPOSAL_GROUPING]
           ,[BILLING_MOVEMENT_SOURCE]
           ,[TERMINATION_DATE_OVERRIDE]
           ,[COMMENT]
           ,[THEME]
           ,[PERFORMANCE_REQUIREMENT]
           ,[PRESENTING_ENTITY]
           ,[PRESENTING_ENTITY_STRUCTURE_FK]
           ,[PROMO_DOLLARS_ENTITY]
           ,[PROMO_DOLLARS_ENTITY_STRUCTURE_FK]
           ,[BILL_BACK_ENTITY]
           ,[BILL_BACK_ENTITY_STRUCTURE_FK]
           ,[CREATE_TIMESTAMP]
           ,[CREATE_USER]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
  select imp.PROPOSAL_ID,
           [epacube].[URM_DATE_CONVERSION](imp.START_DATE),
           [epacube].[URM_DATE_CONVERSION](imp.END_DATE),
           imp.PAYMENT_METHOD,
		   imp.MANUFACTURER_CUSTOMER,
		   imp.PROPOSAL_GROUPING,
           imp.BILLING_MOVEMENT_SOURCE,
           imp.TERMINATION_DATE_OVERRIDE,
           imp.COMMENT,
           imp.THEME,
           imp.PERFORMANCE_REQUIREMENT,
           imp.PRESENTING_ENTITY,
           imp.PRESENTING_ENTITY_STRUCTURE_FK,
           imp.PROMO_DOLLARS_ENTITY,
           imp.PROMO_DOLLARS_ENTITY_STRUCTURE_FK,
           imp.BILL_BACK_ENTITY,
           imp.BILL_BACK_ENTITY_STRUCTURE_FK,
           imp.CREATE_TIMESTAMP,
           imp.CREATE_USER,
           imp.UPDATE_TIMESTAMP,
           imp.UPDATE_USER
		   
		    from [import].[BILLBACK_TO_EPA_HEADER]  IMP
left join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
where PPH.proposal_id is NULL  
and IMP.JOB_FK = @IN_JOB_FK


--select * from epacube.promo_proposal_header where proposal_id = 17712



INSERT INTO [epacube].[PROMO_PROPOSAL_DOLLARS]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[AD_GROUP]
           ,[PROGRAM_TYPE]
           ,[PROPOSAL_AMOUNT_LUMP_SUM]
           ,[PROPOSAL_AMOUNT_BY_CASE]
           --,[PROMO_DOLLARS]
           --,[PROPOSAL_USED]
		   )
		  SELECT 
		  PPH.PROMO_PROPOSAL_HEADER_ID
           ,IMP.PROPOSAL_ID
           ,IMP.AD_GROUP
           ,IMP.PROGRAM_TYPE
           ,IMP.PROPOSAL_AMT_LUMP_SUM
           ,IMP.PROPOSAL_AMT_BY_CASE
           --,IMP.PROMO_DOLLARS
		   		   from [import].[BILLBACK_TO_EPA_DOLLARS]  IMP
inner  join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
--inner join  [import].[BILLBACK_TO_EPA_DETAIL] IMD
--on IMD.proposal_id = IMP.Proposal_id 
and IMP.JOB_FK = @IN_JOB_FK






 INSERT INTO [epacube].[PROMO_PROPOSAL_ITEM_ALLOW]
           ([ALLOWANCE_AMOUNT]
           ,[PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ALLOWANCE_TYPE]
           ,[START_DATE]
           ,[END_DATE]
           ,[PERCENT_OFF]
           ,[PERCENT_OFF_BASIS]
           ,[PROGRAM_TYPE]
           --,[CREATE_DATE]
           ,[CREATE_USER]
		   )
		 SELECT  
		 IMD.Allowance_AMT,
		 PPH.PROMO_PROPOSAL_HEADER_ID,
         IMD.PROPOSAL_ID,
         IMD.PRODUCT_STRUCTURE_FK,
         IMD.ALLOWANCE_TYPE,
          [epacube].[URM_DATE_CONVERSION](imp.START_DATE),
          [epacube].[URM_DATE_CONVERSION](imp.END_DATE),
         IMD.PERCENT_OFF,
         IMD.PERCENT_BASIS,
         IMD.PROGRAM_TYPE,
         --IMD.CREATE_DATE,
         IMD.CREATE_USER 
			 from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner  join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
inner join  [import].[BILLBACK_TO_EPA_DETAIL] IMD
on IMD.proposal_id = IMP.Proposal_id 
and IMP.JOB_FK = @IN_JOB_FK


-------------------------------------------------------------------------------------------------------------------------
---CHECK FOR UPDATES
-------------------------------------------------------------------------------------------------------------------------
----LOG CHANGES Per GARY  
--- RECORD CHANGE IN LOGGING TABLE  
--USE TEMP TABLE 

----LOG CHANGE THEN MAKE CHANGE TO HEADER 

select * from epacube.promo_proposal_change_log
where proposal_id= 1417178

select *  from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id

--------Add in start data and end date...  format stuff

select  [epacube].[URM_DATE_CONVERSION](IMP.START_DATE   )
from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
where pph.START_DATE <>  [epacube].[URM_DATE_CONVERSION](Cast(IMP.START_DATE as numeric))

---------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])
SELECT  PPH.PROMO_PROPOSAL_HEADER_ID, pph.PROPOSAL_ID, 'PAYMENT_METHOD', PPH.PAYMENT_METHOD, IMP.PAYMENT_METHOD, IMP.JOB_FK, getdate()
from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PAYMENT_METHOD <>  imp.PAYMENT_METHOD
and IMP.JOB_FK = @IN_JOB_FK

UPDATE PPH
   SET PAYMENT_METHOD = IMP.PAYMENT_METHOD
      ,UPDATE_TIMESTAMP = IMP.UPDATE_TIMESTAMP
      ,UPDATE_USER = IMP.UPDATE_USER
 from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
AND PPH.PAYMENT_METHOD <>  imp.PAYMENT_METHOD
and IMP.JOB_FK = @IN_JOB_FK

-----------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])
SELECT  PPH.PROMO_PROPOSAL_HEADER_ID, pph.PROPOSAL_ID, 'MANUFACTURER_CUSTOMER', PPH.MANUFACTURER_CUSTOMER, IMP.MANUFACTURER_CUSTOMER, IMP.JOB_FK, getdate()
from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.MANUFACTURER_CUSTOMER <>  imp.MANUFACTURER_CUSTOMER
and IMP.JOB_FK = @IN_JOB_FK

UPDATE PPH
   SET MANUFACTURER_CUSTOMER = IMP.MANUFACTURER_CUSTOMER
      ,UPDATE_TIMESTAMP = IMP.UPDATE_TIMESTAMP
      ,UPDATE_USER = IMP.UPDATE_USER
 from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
AND PPH.MANUFACTURER_CUSTOMER <>  imp.MANUFACTURER_CUSTOMER
and IMP.JOB_FK = @IN_JOB_FK

--------------------------------------------------------------------------------------------------------------------


INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])

SELECT  PPH.PROMO_PROPOSAL_HEADER_ID, pph.PROPOSAL_ID, 'PROPOSAL_GROUPING', PPH.PROPOSAL_GROUPING, IMP.PROPOSAL_GROUPING, IMP.JOB_FK, getdate()
from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PROPOSAL_GROUPING <>  imp.PROPOSAL_GROUPING
and IMP.JOB_FK = @IN_JOB_FK

UPDATE PPH
   SET PROPOSAL_GROUPING = IMP.PROPOSAL_GROUPING
      ,UPDATE_TIMESTAMP = IMP.UPDATE_TIMESTAMP
      ,UPDATE_USER = IMP.UPDATE_USER
 from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
AND PPH.PROPOSAL_GROUPING <>  imp.PROPOSAL_GROUPING
and IMP.JOB_FK = @IN_JOB_FK

--------------------------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])

SELECT  PPH.PROMO_PROPOSAL_HEADER_ID, pph.PROPOSAL_ID, 'BILLING_MOVEMENT_SOURCE', PPH.BILLING_MOVEMENT_SOURCE, IMP.BILLING_MOVEMENT_SOURCE, IMP.JOB_FK, getdate()
from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.BILLING_MOVEMENT_SOURCE <>  imp.BILLING_MOVEMENT_SOURCE
and IMP.JOB_FK = @IN_JOB_FK


UPDATE PPH
   SET BILLING_MOVEMENT_SOURCE = IMP.BILLING_MOVEMENT_SOURCE 
      ,UPDATE_TIMESTAMP = IMP.UPDATE_TIMESTAMP
      ,UPDATE_USER = IMP.UPDATE_USER
 from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
AND PPH.BILLING_MOVEMENT_SOURCE <>  imp.BILLING_MOVEMENT_SOURCE
and IMP.JOB_FK = @IN_JOB_FK


-----------------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])

SELECT  PPH.PROMO_PROPOSAL_HEADER_ID, pph.PROPOSAL_ID, 'TERMINATION_DATE_OVERRIDE', PPH.TERMINATION_DATE_OVERRIDE, IMP.TERMINATION_DATE_OVERRIDE, IMP.JOB_FK, getdate()
from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.TERMINATION_DATE_OVERRIDE <>  imp.TERMINATION_DATE_OVERRIDE
and IMP.JOB_FK = @IN_JOB_FK


UPDATE PPH
   SET  TERMINATION_DATE_OVERRIDE = IMP.TERMINATION_DATE_OVERRIDE 
      ,UPDATE_TIMESTAMP = IMP.UPDATE_TIMESTAMP
      ,UPDATE_USER = IMP.UPDATE_USER
 from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
AND PPH.TERMINATION_DATE_OVERRIDE <>  imp.TERMINATION_DATE_OVERRIDE
and IMP.JOB_FK = @IN_JOB_FK

-----------------------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])

SELECT  PPH.PROMO_PROPOSAL_HEADER_ID, pph.PROPOSAL_ID, 'COMMENT', PPH.COMMENT, IMP.COMMENT, IMP.JOB_FK, getdate()
from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.COMMENT <>  imp.COMMENT
and IMP.JOB_FK = @IN_JOB_FK

UPDATE PPH
   SET COMMENT = IMP.COMMENT 
      ,UPDATE_TIMESTAMP = IMP.UPDATE_TIMESTAMP
      ,UPDATE_USER = IMP.UPDATE_USER
 from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
AND PPH.COMMENT <>  imp.COMMENT
and IMP.JOB_FK = @IN_JOB_FK

----------------------------------------------------------------------------------------------------------------


INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])

SELECT  PPH.PROMO_PROPOSAL_HEADER_ID, pph.PROPOSAL_ID, 'THEME', PPH.THEME, IMP.THEME, IMP.JOB_FK, getdate()
from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.THEME <>  imp.THEME
and IMP.JOB_FK = @IN_JOB_FK


UPDATE PPH
   SET THEME = IMP.THEME
      ,UPDATE_TIMESTAMP = IMP.UPDATE_TIMESTAMP
      ,UPDATE_USER = IMP.UPDATE_USER
 from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
AND PPH.THEME <>  imp.THEME
and IMP.JOB_FK = @IN_JOB_FK

--------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])
SELECT  PPH.PROMO_PROPOSAL_HEADER_ID, pph.PROPOSAL_ID, 'PERFORMANCE_REQUIREMENT', PPH.PERFORMANCE_REQUIREMENT, IMP.PERFORMANCE_REQUIREMENT, IMP.JOB_FK, getdate()
from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PERFORMANCE_REQUIREMENT <>  imp.PERFORMANCE_REQUIREMENT
and IMP.JOB_FK = @IN_JOB_FK

UPDATE PPH
   SET PERFORMANCE_REQUIREMENT = IMP.PERFORMANCE_REQUIREMENT 
      ,UPDATE_TIMESTAMP = IMP.UPDATE_TIMESTAMP
      ,UPDATE_USER = IMP.UPDATE_USER
 from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
AND PPH.PERFORMANCE_REQUIREMENT <>  imp.PERFORMANCE_REQUIREMENT
and IMP.JOB_FK = @IN_JOB_FK

--------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])
SELECT  PPH.PROMO_PROPOSAL_HEADER_ID, pph.PROPOSAL_ID, 'PRESENTING_ENTITY', PPH.PRESENTING_ENTITY, IMP.PRESENTING_ENTITY, IMP.JOB_FK, getdate()
from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PRESENTING_ENTITY <>  imp.PRESENTING_ENTITY
and IMP.JOB_FK = @IN_JOB_FK

UPDATE PPH
   SET PRESENTING_ENTITY = IMP.PRESENTING_ENTITY
      ,UPDATE_TIMESTAMP = IMP.UPDATE_TIMESTAMP
      ,UPDATE_USER = IMP.UPDATE_USER
 from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
AND PPH.PRESENTING_ENTITY <>  imp.PRESENTING_ENTITY
and IMP.JOB_FK = @IN_JOB_FK

--------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])
SELECT  PPH.PROMO_PROPOSAL_HEADER_ID, pph.PROPOSAL_ID, 'PROMO_DOLLARS_ENTITY', PPH.PROMO_DOLLARS_ENTITY, IMP.PROMO_DOLLARS_ENTITY, IMP.JOB_FK, getdate()
from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PROMO_DOLLARS_ENTITY <>  imp.PROMO_DOLLARS_ENTITY
and IMP.JOB_FK = @IN_JOB_FK

UPDATE PPH
   SET PROMO_DOLLARS_ENTITY = IMP.PROMO_DOLLARS_ENTITY
      ,UPDATE_TIMESTAMP = IMP.UPDATE_TIMESTAMP
      ,UPDATE_USER = IMP.UPDATE_USER
 from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
AND PPH.PROMO_DOLLARS_ENTITY <>  imp.PROMO_DOLLARS_ENTITY
and IMP.JOB_FK = @IN_JOB_FK

----------------------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])
SELECT  PPH.PROMO_PROPOSAL_HEADER_ID, pph.PROPOSAL_ID, 'BILL_BACK_ENTITY', PPH.BILL_BACK_ENTITY, IMP.BILL_BACK_ENTITY, IMP.JOB_FK, getdate()
from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.BILL_BACK_ENTITY <>  imp.BILL_BACK_ENTITY
and IMP.JOB_FK = @IN_JOB_FK

UPDATE PPH
   SET BILL_BACK_ENTITY = IMP.BILL_BACK_ENTITY
       ,UPDATE_TIMESTAMP = IMP.UPDATE_TIMESTAMP
      ,UPDATE_USER = IMP.UPDATE_USER
 from [import].[BILLBACK_TO_EPA_HEADER]  IMP
inner join epacube.PROMO_PROPOSAL_HEADER PPH
on PPH.proposal_id = imp.Proposal_id
AND PPH.BILL_BACK_ENTITY <>  imp.BILL_BACK_ENTITY
and IMP.JOB_FK = @IN_JOB_FK


-------------------------------------------------------------------------------------------------------------
----LOG CHANGE THEN MAKE CHANGE TO [PROMO_PROPOSAL_ITEM_ALLOW]
-------------------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])
SELECT  PPH.PROMO_PROPOSAL_HEADER_FK, pph.PROPOSAL_ID, 'ALLOWANCE_AMOUNT', PPH.ALLOWANCE_AMOUNT, IMP.ALLOWANCE_AMT, IMP.JOB_FK, getdate()
from import.BILLBACK_TO_EPA_DETAIL  IMP
inner join [epacube].[PROMO_PROPOSAL_ITEM_ALLOW] PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PRODUCT_STRUCTURE_FK = imp.PRODUCT_STRUCTURE_FK
AND PPH.PROGRAM_TYPE = imp.PROGRAM_TYPE
AND PPH.ALLOWANCE_AMOUNT <>  imp.ALLOWANCE_AMT
and IMP.JOB_FK = @IN_JOB_FK

UPDATE [epacube].[PROMO_PROPOSAL_ITEM_ALLOW]
   SET [ALLOWANCE_AMOUNT] = imp.ALLOWANCE_AMT
   from import.BILLBACK_TO_EPA_DETAIL  IMP
inner join [epacube].[PROMO_PROPOSAL_ITEM_ALLOW] PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PRODUCT_STRUCTURE_FK = imp.PRODUCT_STRUCTURE_FK
AND PPH.PROGRAM_TYPE = imp.PROGRAM_TYPE
AND PPH.ALLOWANCE_AMOUNT <>  imp.ALLOWANCE_AMT
and IMP.JOB_FK = @IN_JOB_FK
     
------------------------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])
SELECT  PPH.PROMO_PROPOSAL_HEADER_FK, pph.PROPOSAL_ID, 'ALLOWANCE_TYPE', PPH.ALLOWANCE_TYPE, IMP.ALLOWANCE_TYPE, IMP.JOB_FK, getdate()
from import.BILLBACK_TO_EPA_DETAIL  IMP
inner join [epacube].[PROMO_PROPOSAL_ITEM_ALLOW] PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PRODUCT_STRUCTURE_FK = imp.PRODUCT_STRUCTURE_FK
AND PPH.PROGRAM_TYPE = imp.PROGRAM_TYPE
AND PPH.ALLOWANCE_TYPE <>  imp.ALLOWANCE_TYPE
and IMP.JOB_FK = @IN_JOB_FK

UPDATE [epacube].[PROMO_PROPOSAL_ITEM_ALLOW]
   SET ALLOWANCE_TYPE= imp.ALLOWANCE_TYPE
   from import.BILLBACK_TO_EPA_DETAIL  IMP
inner join [epacube].[PROMO_PROPOSAL_ITEM_ALLOW] PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PRODUCT_STRUCTURE_FK = imp.PRODUCT_STRUCTURE_FK
AND PPH.PROGRAM_TYPE = imp.PROGRAM_TYPE
AND PPH.ALLOWANCE_TYPE <>  imp.ALLOWANCE_TYPE
and IMP.JOB_FK = @IN_JOB_FK
     

------------------------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])
SELECT  PPH.PROMO_PROPOSAL_HEADER_FK, pph.PROPOSAL_ID, 'PERCENT_OFF', PPH.PERCENT_OFF, IMP.PERCENT_OFF, IMP.JOB_FK, getdate()
from import.BILLBACK_TO_EPA_DETAIL  IMP
inner join [epacube].[PROMO_PROPOSAL_ITEM_ALLOW] PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PRODUCT_STRUCTURE_FK = imp.PRODUCT_STRUCTURE_FK
AND PPH.PROGRAM_TYPE = imp.PROGRAM_TYPE
AND PPH.PERCENT_OFF <>  imp.PERCENT_OFF
and IMP.JOB_FK = @IN_JOB_FK

UPDATE [epacube].[PROMO_PROPOSAL_ITEM_ALLOW]
   SET PERCENT_OFF = imp.PERCENT_OFF
   from import.BILLBACK_TO_EPA_DETAIL  IMP
inner join [epacube].[PROMO_PROPOSAL_ITEM_ALLOW] PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PRODUCT_STRUCTURE_FK = imp.PRODUCT_STRUCTURE_FK
AND PPH.PROGRAM_TYPE = imp.PROGRAM_TYPE
AND PPH.PERCENT_OFF <>  imp.PERCENT_OFF
and IMP.JOB_FK = @IN_JOB_FK


------------------------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])
SELECT  PPH.PROMO_PROPOSAL_HEADER_FK, pph.PROPOSAL_ID, 'PERCENT_OFF_BASIS', PPH.PERCENT_OFF_BASIS, IMP.PERCENT_BASIS, IMP.JOB_FK, getdate()
from import.BILLBACK_TO_EPA_DETAIL  IMP
inner join [epacube].[PROMO_PROPOSAL_ITEM_ALLOW] PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PRODUCT_STRUCTURE_FK = imp.PRODUCT_STRUCTURE_FK
AND PPH.PROGRAM_TYPE = imp.PROGRAM_TYPE
AND PPH.PERCENT_OFF_BASIS <>  imp.PERCENT_BASIS
and IMP.JOB_FK = @IN_JOB_FK

UPDATE [epacube].[PROMO_PROPOSAL_ITEM_ALLOW]
   SET PERCENT_OFF = imp.PERCENT_BASIS
   from import.BILLBACK_TO_EPA_DETAIL  IMP
inner join [epacube].[PROMO_PROPOSAL_ITEM_ALLOW] PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.PRODUCT_STRUCTURE_FK = imp.PRODUCT_STRUCTURE_FK
AND PPH.PROGRAM_TYPE = imp.PROGRAM_TYPE
AND PPH.PERCENT_OFF_BASIS <>  imp.PERCENT_BASIS
and IMP.JOB_FK = @IN_JOB_FK


select * from import.BILLBACK_TO_EPA_Dollars

select * from  [epacube].[PROMO_PROPOSAL_DOLLARS]


-------------------------------------------------------------------------------------------------------------
----LOG CHANGE THEN MAKE CHANGE TO [PROMO_PROPOSAL_DOLLARS]
-------------------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])
SELECT  PPH.PROMO_PROPOSAL_HEADER_FK, pph.PROPOSAL_ID, 'PROPOSAL_AMOUNT_LUMP_SUM', PPH.PROPOSAL_AMOUNT_LUMP_SUM, IMP.PROPOSAL_AMT_LUMP_SUM, IMP.JOB_FK, getdate()
from import.BILLBACK_TO_EPA_DOLLARS  IMP
inner join [epacube].[PROMO_PROPOSAL_DOLLARS] PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.AD_GROUP = imp.AD_GROUP
AND PPH.PROGRAM_TYPE = imp.PROGRAM_TYPE
AND PPH.PROPOSAL_AMOUNT_LUMP_SUM <>  imp.PROPOSAL_AMT_LUMP_SUM
and IMP.JOB_FK = @IN_JOB_FK

UPDATE [epacube].[PROMO_PROPOSAL_DOLLARS]
   SET PROPOSAL_AMOUNT_LUMP_SUM = imp.PROPOSAL_AMT_LUMP_SUM
  from import.BILLBACK_TO_EPA_DOLLARS  IMP
inner join [epacube].[PROMO_PROPOSAL_DOLLARS] PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.AD_GROUP = imp.AD_GROUP
AND PPH.PROGRAM_TYPE = imp.PROGRAM_TYPE
AND PPH.PROPOSAL_AMOUNT_LUMP_SUM <>  imp.PROPOSAL_AMT_LUMP_SUM
and IMP.JOB_FK = @IN_JOB_FK    
-------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PROMO_PROPOSAL_CHANGE_LOG]
           ([PROMO_PROPOSAL_HEADER_FK]
           ,[PROPOSAL_ID]
           ,[COLUMN_NAME]
           ,[CHANGE_FROM]
           ,[CHANGE_TO]
           ,[JOB_FK]
           ,[CREATE_DATE])
SELECT  PPH.PROMO_PROPOSAL_HEADER_FK, pph.PROPOSAL_ID, 'PROPOSAL_AMOUNT_BY_CASE', PPH.PROPOSAL_AMOUNT_BY_CASE, IMP.PROPOSAL_AMT_BY_CASE, IMP.JOB_FK, getdate()
from import.BILLBACK_TO_EPA_DOLLARS  IMP
inner join [epacube].[PROMO_PROPOSAL_DOLLARS] PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.AD_GROUP = imp.AD_GROUP
AND PPH.PROGRAM_TYPE = imp.PROGRAM_TYPE
AND PPH.PROPOSAL_AMOUNT_BY_CASE <>  imp.PROPOSAL_AMT_BY_CASE
and IMP.JOB_FK = @IN_JOB_FK

UPDATE [epacube].[PROMO_PROPOSAL_DOLLARS]
   SET PROPOSAL_AMOUNT_BY_CASE = imp.PROPOSAL_AMT_BY_CASE
  from import.BILLBACK_TO_EPA_DOLLARS  IMP
inner join [epacube].[PROMO_PROPOSAL_DOLLARS] PPH
on  PPH.PROPOSAL_ID = imp.PROPOSAL_ID
AND PPH.AD_GROUP = imp.AD_GROUP
AND PPH.PROGRAM_TYPE = imp.PROGRAM_TYPE
AND PPH.PROPOSAL_AMOUNT_LUMP_SUM <>  imp.PROPOSAL_AMT_BY_CASE
and IMP.JOB_FK = @IN_JOB_FK




--------------------------------------------------------------------------------------------------

      SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'IMPORT_COMPLETE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



 SET @status_desc =  'finished execution of IMPORT.LOAD_BILLBACKS_TO_EPA'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT.LOAD_BILLBACKS_TO_EPA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END





