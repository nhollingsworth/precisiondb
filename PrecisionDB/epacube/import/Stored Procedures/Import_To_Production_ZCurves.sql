﻿--A_Update_SP_Import_zCurves

CREATE PROCEDURE [import].[Import_To_Production_ZCurves]
AS
BEGIN
	SET NOCOUNT ON;
--------------------------------------------------
--Load Import Tables
--------------------------------------------------
Declare @In_JOB_FK bigint
Set @In_JOB_FK = (Select top 1 Job_FK from import.import_sxe_zcurve)

--IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'idx_sxe_pdsr')
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'idx_zcurve')
	drop index idx_zcurve on import.import_sxe_zcurve

IF object_id('tempdb..##ZC') is not null
drop table ##ZC;

IF object_id('tempdb..##tmp_ZC') is not null
drop table ##tmp_ZC;

Create Index idx_zcurve on import.import_sxe_zcurve (toscode, rebatety)

Truncate Table Import.import_rules
Truncate Table Import.import_rules_data

Select distinct PA.product_structure_fk, zc.*
into ##tmp_ZC
from import.import_sxe_zcurve zc with (nolock)
	inner join epacube.PRODUCT_ATTRIBUTE PA with (nolock) on zc.toscode = PA.ATTRIBUTE_CHAR and PA.DATA_NAME_FK = 290842
	where ISNULL(toscode, '') > '' and isnull(enddt, getdate() + 1) > getdate()
	and IMPORT_FILENAME = (Select MAX(Import_Filename) from import.import_sxe_zcurve)

Create Index idx_tmp_zcurve on ##tmp_ZC (toscode, rebatety, product_Structure_FK)

Select 
 ROW_NUMBER() over (order by zc.toscode, zc.rebatety, ZC.Product_Structure_FK) Record_No
, (Select [Name] from synchronizer.rules_structure with (nolock) where rules_structure_id = 206201) Rules_Structure_Name
, (Select [Name] from synchronizer.rules_schedule with (nolock) where rules_schedule_id = 20620100) Rules_Schedule_Name
, 'zCurve: ' + Cast(toscode as varchar(8)) + '/' + isnull(Cast(rebatety as varchar(16)), '') + ': ' 
	+ Cast(ZC.Product_Structure_FK as Varchar(32)) RULE_NAME
, 'Rebate CB Amt' Result_Data_Name
, CONVERT(Varchar, Cast(StartDt as datetime), 101) Effective_Date
, CONVERT(Varchar, Cast(enddt as datetime), 101) End_Date
, 'PRODUCT ID' Prod_Filter_DN
, (select value from epacube.product_identification with (nolock) where DATA_NAME_FK = 110100 and PRODUCT_STRUCTURE_FK = ZC.Product_Structure_FK)
	 Prod_Filter_Value1
, 'SX CUST REBATE TYPE' Cust_Filter_DN
, Null Cust_Filter_Entity_DN
, rebatety Cust_Filter_Value1
, Case When isnull(Cast(rebatety as varchar(16)), '') = '' then 101 else 100 end 'RULE_PRECEDENCE'
, 'zCurve' 'Host_Precedence_Level_Description'
, Cast(toscode as varchar(8)) + ':' + Cast(rebatety as varchar(16)) + Cast(ZC.Product_Structure_FK as Varchar(32)) Host_Rule_Xref
, Null Reference
, Null Contract_No, 999 Display_Seq
, (Select Code from epacube.code_ref with (nolock) where code_ref_id = Case When Cast(startdt as datetime) <= Getdate() then 710 else 711 end) Rule_Result_Type
, 'zCurve' + Cast(toscode as varchar(8)) + Cast(rebatety as varchar(16)) + Cast(ZC.Product_Structure_FK as Varchar(32))
  'Unique_Key1'
, Null Event_Action_CR_FK, 1 Record_Status_CR_FK, 'HOST REBATE ZCURVE RULES LOAD' Create_User
, 142111 'System_ID_DN'
, 'zCurve' 'System_ID_Value'
, zc.*
into ##ZC
from ##tmp_ZC zc
	order by toscode, rebatety, product_Structure_FK
	
Drop Table ##tmp_ZC

Insert into import.import_rules
(Job_FK, Import_FileName, Record_No, Rules_Structure_Name, Rules_Schedule_Name, RULE_NAME, Result_Data_Name--, Result_Data_Name_FK
, Effective_Date, End_Date, Prod_Filter_DN, Prod_Filter_Value1, Cust_Filter_DN, Cust_Filter_Entity_DN, Cust_Filter_Value1
, Rule_Precedence, HOST_PRECEDENCE_LEVEL_DESCRIPTION, Host_Rule_Xref, Reference
, Contract_No, Display_Seq, Rule_Result_Type, Unique_Key1, Event_Action_CR_FK, Record_Status_CR_FK, Update_User, Create_User, system_id_dn, system_id_value)

Select
Job_FK, Import_FileName, Record_No, Rules_Structure_Name, Rules_Schedule_Name, RULE_NAME, Result_Data_Name--, Result_Data_Name_FK
, Effective_Date, End_Date, Prod_Filter_DN, Prod_Filter_Value1, Cust_Filter_DN, Cust_Filter_Entity_DN, Cust_Filter_Value1
, Rule_Precedence, HOST_PRECEDENCE_LEVEL_DESCRIPTION, Host_Rule_Xref, Reference
, Contract_No, Display_Seq, Rule_Result_Type, Unique_Key1, Event_Action_CR_FK, Record_Status_CR_FK, Create_User, Create_User, system_id_dn, system_id_value
from ##ZC

Insert into import.import_rules_data
(
Job_FK, Record_No, Rule_Name
, Rules_Action_Operation1
, Compare_Operator
, Basis_Calc_DN, Basis1_DN
, OPERAND_FILTER1_DN
, OPERAND_FILTER1_OPERATOR
, Operand_1_1, Operand_1_2, Operand_1_3, Operand_1_4, Operand_1_5, Operand_1_6, Operand_1_7
, Record_Status_CR_FK, Update_User
)
Select
Job_FK
, Record_No
, RULE_NAME
, 'Carrier ZCurve' Rules_Action_Operation1	
, '='
, 'SELL PRICE CUST AMT' Basis_Calc_DN
, 'SX BASE' Basis1_DN
, 'Rebate_CB_Amt'
, 'NAMED OPERATION'
, BegSellMult2 Operand1
, AreaMult Operand2
, Cast((Cast(BEGSELLMULT2 as Numeric(18, 4)) * ((100 -Cast([BEGDGM%SEG2] as numeric(18, 4))) / 100)) as Numeric(18, 4)) Operand3
, EndSellMult3 Operand4
, SplitSeg2 Operand5
, EndSellMult2 Operand6
, SplitSeg3 Operand7
, 1 Record_Status_CR_FK
, 'HOST REBATE ZCURVE RULES LOAD' Update_User
from ##ZC

drop table ##ZC;

---LOAD INTO SYNCHRONIZER RULES TABLES

EXEC import.LOAD_IMPORT_RULES @IN_JOB_FK

Update RFS
Set Supl_Filter_fk = Null
from Synchronizer.rules_filter_set RFS 
where Supl_Filter_fk is not null and rules_fk in 
(select rules_id from synchronizer.rules with (nolock) where result_data_name_fk = 111501)

END
