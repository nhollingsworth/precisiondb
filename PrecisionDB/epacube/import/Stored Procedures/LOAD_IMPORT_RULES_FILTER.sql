﻿













-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- KS        06/14/2010   Initial Version
-- CV        06/14/2013   uncommented some code and added is null to it for update of org filter.


CREATE PROCEDURE [import].[LOAD_IMPORT_RULES_FILTER] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_IMPORT_RULES_FILTER', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps



-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( RULES_ERRORS TABLE )
--
--     SAVE EXAMPLES FOR LATER 
--
--	   ERROR NAME:  INVALID FILTER CRITERIA
--     MISSING OR INVALID DATA NAMES...  FOR ANY FILTER
--     MISSING ENTITY_DATA_NAME_FK ON ENTITY FILTERS THAT ARE IDENTIFIERS
--     WHEN ARE ASSOC DN REQUIRED ??
--
--     SET RECORD STATUS = PURGE FOR ALL RED CONDITIONS.... ???
--
-------------------------------------------------------------------------------------



-------------------------------------------------------------------------------------
--   INSERT ANY MISSING PRODUCT FILTERS
-------------------------------------------------------------------------------------

INSERT INTO [synchronizer].[RULES_FILTER]
           ([ENTITY_CLASS_CR_FK]
           ,[NAME]
           ,[DATA_NAME_FK]           
           ,[ENTITY_DATA_NAME_FK]
           ,[VALUE1]
           ,[VALUE2]
           ,[OPERATOR_CR_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_FILENAME]
           ,[UPDATE_USER]           
           )
SELECT DISTINCT
            DS.ENTITY_CLASS_CR_FK
           ,( DN.LABEL + ': ' + PROD_FILTER_VALUE1 ) 
           ,DN.DATA_NAME_ID
           ,NULL  -----DNE.DATA_NAME_ID 
           ,PROD_FILTER_VALUE1
           ,PROD_FILTER_VALUE2
           ,ISNULL ( CR.CODE_REF_ID, 9000 )
           ,IR.IMPORT_PACKAGE_FK
           ,IR.JOB_FK
           ,IR.IMPORT_FILENAME
           ,isnull(IR.[UPDATE_USER], 'EPACUBE')
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.NAME = IR.PROD_FILTER_DN )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )   
LEFT JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
  ON ( CODE_TYPE = 'OPERATOR' AND CODE = PROD_FILTER_OPERATOR )
LEFT JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK )
  ON ( RF.ENTITY_CLASS_CR_FK                = DS.ENTITY_CLASS_CR_FK 
  AND  RF.DATA_NAME_FK                      = DN.DATA_NAME_ID  
  AND  RF.VALUE1                            = IR.PROD_FILTER_VALUE1
  AND  ISNULL ( RF.VALUE2, 'NULL DATA' )    = ISNULL ( IR.PROD_FILTER_VALUE2, 'NULL DATA' ) 
  AND  RF.OPERATOR_CR_FK                    = ISNULL ( CR.CODE_REF_ID, 9000 )
  )
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.RULES_FK IS NOT NULL 
AND   IR.PROD_FILTER_VALUE1 IS NOT NULL 
AND   RF.RULES_FILTER_ID IS NULL 
           
           
 

-------------------------------------------------------------------------------------
--   INSERT ANY MISSING ORG FILTERS
-------------------------------------------------------------------------------------

INSERT INTO [synchronizer].[RULES_FILTER]
           ([ENTITY_CLASS_CR_FK]
           ,[NAME]
           ,[DATA_NAME_FK]           
           ,[ENTITY_DATA_NAME_FK]
           ,[VALUE1]
           ,[VALUE2]
           ,[OPERATOR_CR_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_FILENAME]
           ,[UPDATE_USER]           
           )
SELECT DISTINCT
            DS.ENTITY_CLASS_CR_FK
           ,( DN.LABEL + ': ' + ORG_FILTER_VALUE1 ) 
           ,DN.DATA_NAME_ID
           ,DNE.DATA_NAME_ID 
           ,ORG_FILTER_VALUE1
           ,ORG_FILTER_VALUE2
           ,ISNULL ( CR.CODE_REF_ID, 9000 )
           ,IR.IMPORT_PACKAGE_FK
           ,IR.JOB_FK
           ,IR.IMPORT_FILENAME
           ,isnull(IR.[UPDATE_USER], 'EPACUBE')
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.NAME = IR.ORG_FILTER_DN )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )   
LEFT JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
  ON ( CODE_TYPE = 'OPERATOR' AND CODE = ORG_FILTER_OPERATOR )
LEFT JOIN EPACUBE.DATA_NAME DNE WITH (NOLOCK)
  ON ( DNE.NAME = IR.ORG_FILTER_ENTITY_DN )    
LEFT JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK )
  ON ( RF.ENTITY_CLASS_CR_FK                = DS.ENTITY_CLASS_CR_FK 
  AND  ISNULL ( RF.ENTITY_DATA_NAME_FK, 0 ) = ISNULL ( DNE.DATA_NAME_ID, 0 )
  AND  RF.DATA_NAME_FK                      = DN.DATA_NAME_ID  
  AND  RF.VALUE1                            = IR.ORG_FILTER_VALUE1
  AND  ISNULL ( RF.VALUE2, 'NULL DATA' )    = ISNULL ( IR.ORG_FILTER_VALUE2, 'NULL DATA' ) 
  AND  RF.OPERATOR_CR_FK                    = ISNULL ( CR.CODE_REF_ID, 9000 )
  )
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.RULES_FK IS NOT NULL 
AND   IR.ORG_FILTER_VALUE1 IS NOT NULL 
AND   RF.RULES_FILTER_ID IS NULL 




-------------------------------------------------------------------------------------
--   INSERT ANY MISSING CUST FILTERS
-------------------------------------------------------------------------------------

INSERT INTO [synchronizer].[RULES_FILTER]
           ([ENTITY_CLASS_CR_FK]
           ,[NAME]
           ,[DATA_NAME_FK]           
           ,[ENTITY_DATA_NAME_FK]
           ,[VALUE1]
           ,[VALUE2]
           ,[OPERATOR_CR_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_FILENAME]
           ,[UPDATE_USER]
           )
SELECT DISTINCT
            DS.ENTITY_CLASS_CR_FK
           ,( DN.LABEL + ': ' + CUST_FILTER_VALUE1 ) 
           ,DN.DATA_NAME_ID
           ,DNE.DATA_NAME_ID 
           ,CUST_FILTER_VALUE1
           ,CUST_FILTER_VALUE2
           ,ISNULL ( CR.CODE_REF_ID, 9000 )
           ,IR.IMPORT_PACKAGE_FK
           ,IR.JOB_FK
           ,IR.IMPORT_FILENAME
           ,isnull(IR.[UPDATE_USER], 'EPACUBE')
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.NAME = IR.CUST_FILTER_DN )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )   
LEFT JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
  ON ( CODE_TYPE = 'OPERATOR' AND CODE = CUST_FILTER_OPERATOR )
LEFT JOIN EPACUBE.DATA_NAME DNE WITH (NOLOCK)
  ON ( DNE.NAME = IR.CUST_FILTER_ENTITY_DN )    
LEFT JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK )
  ON ( RF.ENTITY_CLASS_CR_FK                = DS.ENTITY_CLASS_CR_FK 
  AND  ISNULL ( RF.ENTITY_DATA_NAME_FK, 0 ) = ISNULL ( DNE.DATA_NAME_ID, 0 )
  AND  RF.DATA_NAME_FK                      = DN.DATA_NAME_ID  
  AND  RF.VALUE1                            = IR.CUST_FILTER_VALUE1
  AND  ISNULL ( RF.VALUE2, 'NULL DATA' )    = ISNULL ( IR.CUST_FILTER_VALUE2, 'NULL DATA' ) 
  AND  RF.OPERATOR_CR_FK                    = ISNULL ( CR.CODE_REF_ID, 9000 )
  )
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.RULES_FK IS NOT NULL 
AND   IR.CUST_FILTER_VALUE1 IS NOT NULL 
AND   RF.RULES_FILTER_ID IS NULL 
  

-------------------------------------------------------------------------------------
--   INSERT ANY MISSING SUPL FILTERS
-------------------------------------------------------------------------------------

INSERT INTO [synchronizer].[RULES_FILTER]
           ([ENTITY_CLASS_CR_FK]
           ,[NAME]
           ,[DATA_NAME_FK]           
           ,[ENTITY_DATA_NAME_FK]
           ,[VALUE1]
           ,[VALUE2]
           ,[OPERATOR_CR_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_FILENAME]
           ,[UPDATE_USER]           
           )
SELECT DISTINCT
            DS.ENTITY_CLASS_CR_FK
           ,( isnull(Supl_Filter_Assoc_DN, DN.LABEL) + ': ' + SUPL_FILTER_VALUE1 ) 
           ,DN.DATA_NAME_ID
           ,DNE.DATA_NAME_ID 
           ,SUPL_FILTER_VALUE1
           ,SUPL_FILTER_VALUE2
           ,ISNULL ( CR.CODE_REF_ID, 9000 )
           ,IR.IMPORT_PACKAGE_FK
           ,IR.JOB_FK
           ,IR.IMPORT_FILENAME
           ,isnull(IR.[UPDATE_USER], 'EPACUBE')
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.NAME = IR.SUPL_FILTER_DN )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )   
LEFT JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
  ON ( CODE_TYPE = 'OPERATOR' AND CODE = SUPL_FILTER_OPERATOR )
LEFT JOIN EPACUBE.DATA_NAME DNE WITH (NOLOCK)
  ON ( DNE.NAME = IR.SUPL_FILTER_ENTITY_DN )    
LEFT JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK )
  ON ( RF.ENTITY_CLASS_CR_FK                = DS.ENTITY_CLASS_CR_FK 
  AND  ISNULL ( RF.ENTITY_DATA_NAME_FK, 0 ) = ISNULL ( DNE.DATA_NAME_ID, 0 )
  AND  RF.DATA_NAME_FK                      = DN.DATA_NAME_ID  
  AND  RF.VALUE1                            = IR.SUPL_FILTER_VALUE1
  AND  ISNULL ( RF.VALUE2, 'NULL DATA' )    = ISNULL ( IR.SUPL_FILTER_VALUE2, 'NULL DATA' ) 
  AND  RF.OPERATOR_CR_FK                    = ISNULL ( CR.CODE_REF_ID, 9000 )
  )
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.RULES_FK IS NOT NULL 
AND   IR.SUPL_FILTER_VALUE1 IS NOT NULL 
AND   RF.RULES_FILTER_ID IS NULL 



-------------------------------------------------------------------------------------
--   INSERT FOR NEW RULES IN THIS JOB THEIR FILTER SETS 
-------------------------------------------------------------------------------------

INSERT INTO [synchronizer].[RULES_FILTER_SET]
           ([RULES_FK]
           ,[PROD_FILTER_FK]
           ,[ORG_FILTER_FK]
           ,[CUST_FILTER_FK]
           ,[SUPL_FILTER_FK]
           ,[PROD_FILTER_ASSOC_DN_FK]
           ,[ORG_FILTER_ASSOC_DN_FK]
           ,[CUST_FILTER_ASSOC_DN_FK]
           ,[SUPL_FILTER_ASSOC_DN_FK]
           ,[UPDATE_USER]           
           )
SELECT
            IR.RULES_FK
           ,CASE ISNULL ( IR.PROD_FILTER_DN, 'NULL DATA' )
            WHEN 'NULL DATA' THEN NULL
            ELSE ( SELECT RF.RULES_FILTER_ID
                   FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)
                   INNER JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK)
                    ON ( RF.DATA_NAME_FK   = DN.DATA_NAME_ID
                    AND  RF.VALUE1         = IR.PROD_FILTER_VALUE1
                    AND  ISNULL ( RF.VALUE2, 'NULL DATA' )    = ISNULL ( IR.PROD_FILTER_VALUE2, 'NULL DATA' ) 
------                    AND  RF.OPERATOR_CR_FK                    = ISNULL ( CR.CODE_REF_ID, 9000 )
                    )
                   WHERE DN.NAME = IR.PROD_FILTER_DN  )
            END
           ,CASE ISNULL ( IR.ORG_FILTER_DN, 'NULL DATA' )
            WHEN 'NULL DATA' THEN NULL
            ELSE ( SELECT RF.RULES_FILTER_ID
                   FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)                                     
                   INNER JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK)
                    ON ( RF.DATA_NAME_FK   = DN.DATA_NAME_ID
                    AND  RF.VALUE1         = IR.ORG_FILTER_VALUE1
                    AND  ISNULL ( RF.VALUE2, 'NULL DATA' )    = ISNULL ( IR.ORG_FILTER_VALUE2, 'NULL DATA' ) 
------                    AND  RF.OPERATOR_CR_FK                    = ISNULL ( CR.CODE_REF_ID, 9000 )
                    ------AND  RF.ENTITY_DATA_NAME_FK = ( SELECT DNE.DATA_NAME_ID
                    ------                                FROM EPACUBE.DATA_NAME DNE WITH (NOLOCK)
					               ------                 WHERE DNE.NAME = IR.ORG_FILTER_ENTITY_DN )    
                    )
                   WHERE DN.NAME = IR.ORG_FILTER_DN  )
            END 
           ,CASE ISNULL ( IR.CUST_FILTER_DN, 'NULL DATA' )
            WHEN 'NULL DATA' THEN NULL
            ELSE ( SELECT RF.RULES_FILTER_ID
                   FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)                                     
                   INNER JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK)
                    ON ( RF.DATA_NAME_FK   = DN.DATA_NAME_ID
                    AND  RF.VALUE1         = IR.CUST_FILTER_VALUE1
                    AND  ISNULL ( RF.VALUE2, 'NULL DATA' )    = ISNULL ( IR.CUST_FILTER_VALUE2, 'NULL DATA' ) 
------                    AND  RF.OPERATOR_CR_FK                    = ISNULL ( CR.CODE_REF_ID, 9000 )
                    AND  (   DN.ENTITY_DATA_NAME_FK IS NULL 
					      OR RF.ENTITY_DATA_NAME_FK = ( SELECT DNE.DATA_NAME_ID
                                                        FROM EPACUBE.DATA_NAME DNE WITH (NOLOCK)
					                                    WHERE DNE.NAME = IR.CUST_FILTER_ENTITY_DN )   
					      ) 
                    )
                   WHERE DN.NAME = IR.CUST_FILTER_DN  )
            END 
           ,CASE ISNULL ( IR.SUPL_FILTER_DN, 'NULL DATA' )
            WHEN 'NULL DATA' THEN NULL
            ELSE ( SELECT RF.RULES_FILTER_ID
                   FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)                                     
                   INNER JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK)
                    ON ( RF.DATA_NAME_FK   = DN.DATA_NAME_ID
                    AND  RF.VALUE1         = IR.SUPL_FILTER_VALUE1
                    AND  ISNULL ( RF.VALUE2, 'NULL DATA' )    = ISNULL ( IR.SUPL_FILTER_VALUE2, 'NULL DATA' ) 
------                    AND  RF.OPERATOR_CR_FK                    = ISNULL ( CR.CODE_REF_ID, 9000 )
                     AND  (   DN.ENTITY_DATA_NAME_FK IS NULL 
					       OR RF.ENTITY_DATA_NAME_FK = ( SELECT DNE.DATA_NAME_ID
                                                         FROM EPACUBE.DATA_NAME DNE WITH (NOLOCK)
					                                     WHERE DNE.NAME = IR.SUPL_FILTER_ENTITY_DN ) 
					       )   
                    )
                   WHERE DN.NAME = IR.SUPL_FILTER_DN  )
            END 
           ,CASE ISNULL ( IR.PROD_FILTER_ASSOC_DN, 'NULL DATA' )
            WHEN 'NULL DATA' THEN NULL
            ELSE ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.NAME = IR.PROD_FILTER_ASSOC_DN )
            END
           ,CASE ISNULL ( IR.ORG_FILTER_ASSOC_DN, 'NULL DATA' )
            WHEN 'NULL DATA' THEN NULL
            ELSE ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.NAME = IR.ORG_FILTER_ASSOC_DN )
            END
           ,CASE ISNULL ( IR.CUST_FILTER_ASSOC_DN, 'NULL DATA' )
            WHEN 'NULL DATA' THEN NULL
            ELSE ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.NAME = IR.CUST_FILTER_ASSOC_DN )
            END
           ,CASE ISNULL ( IR.SUPL_FILTER_ASSOC_DN, 'NULL DATA' )
            WHEN 'NULL DATA' THEN NULL
            ELSE ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.NAME = IR.SUPL_FILTER_ASSOC_DN )
            END
           ,isnull(IR.[UPDATE_USER], 'EPACUBE')
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 54  



-------------------------------------------------------------------------------------
--   UPDATE RULES_FILTER_SETS
-------------------------------------------------------------------------------------

UPDATE synchronizer.RULES_FILTER_SET
SET  PROD_FILTER_FK = ( SELECT RF.RULES_FILTER_ID
						   FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)
						   INNER JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK)
							ON ( RF.DATA_NAME_FK   = DN.DATA_NAME_ID
							AND  RF.VALUE1         = IR.PROD_FILTER_VALUE1
							AND  ISNULL ( RF.VALUE2, 'NULL DATA' )    = ISNULL ( IR.PROD_FILTER_VALUE2, 'NULL DATA' ) 
		------                    AND  RF.OPERATOR_CR_FK                    = ISNULL ( CR.CODE_REF_ID, 9000 )
							)
						   WHERE DN.NAME = IR.PROD_FILTER_DN  )
	,ORG_FILTER_FK =  ( SELECT RF.RULES_FILTER_ID
						   FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)                                     
						   INNER JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK)
							ON ( RF.DATA_NAME_FK   = DN.DATA_NAME_ID
							AND  RF.VALUE1         = IR.ORG_FILTER_VALUE1
							AND  ISNULL ( RF.VALUE2, 'NULL DATA' )    = ISNULL ( IR.ORG_FILTER_VALUE2, 'NULL DATA' ) 
		------                    AND  RF.OPERATOR_CR_FK                    = ISNULL ( CR.CODE_REF_ID, 9000 )
							AND  ISNULL(RF.ENTITY_DATA_NAME_FK,NULL) = ( SELECT DNE.DATA_NAME_ID
															FROM EPACUBE.DATA_NAME DNE WITH (NOLOCK)
															WHERE DNE.NAME = IR.ORG_FILTER_ENTITY_DN )    
							)
						   WHERE DN.NAME = IR.ORG_FILTER_DN  )
    ,CUST_FILTER_FK =  ( SELECT RF.RULES_FILTER_ID
						   FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)                                     
						   INNER JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK)
							ON ( RF.DATA_NAME_FK   = DN.DATA_NAME_ID
							AND  RF.VALUE1         = IR.CUST_FILTER_VALUE1
							AND  ISNULL ( RF.VALUE2, 'NULL DATA' )    = ISNULL ( IR.CUST_FILTER_VALUE2, 'NULL DATA' ) 
		------                    AND  RF.OPERATOR_CR_FK                    = ISNULL ( CR.CODE_REF_ID, 9000 )
							AND  (   DN.ENTITY_DATA_NAME_FK IS NULL 
							      OR RF.ENTITY_DATA_NAME_FK = ( SELECT DNE.DATA_NAME_ID
															    FROM EPACUBE.DATA_NAME DNE WITH (NOLOCK)
															    WHERE DNE.NAME = IR.CUST_FILTER_ENTITY_DN )   
							     ) 
							)
						   WHERE DN.NAME = IR.CUST_FILTER_DN  )    
    ,SUPL_FILTER_FK =    ( SELECT RF.RULES_FILTER_ID
						   FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)                                     
						   INNER JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK)
							ON ( RF.DATA_NAME_FK   = DN.DATA_NAME_ID
							AND  RF.VALUE1         = IR.SUPL_FILTER_VALUE1
							AND  ISNULL ( RF.VALUE2, 'NULL DATA' )    = ISNULL ( IR.SUPL_FILTER_VALUE2, 'NULL DATA' ) 
		------                    AND  RF.OPERATOR_CR_FK                    = ISNULL ( CR.CODE_REF_ID, 9000 )
							AND  (   DN.ENTITY_DATA_NAME_FK IS NULL 
							      OR RF.ENTITY_DATA_NAME_FK = ( SELECT DNE.DATA_NAME_ID
															    FROM EPACUBE.DATA_NAME DNE WITH (NOLOCK)
															    WHERE DNE.NAME = IR.SUPL_FILTER_ENTITY_DN )  
								 )  
							)
						   WHERE DN.NAME = IR.SUPL_FILTER_DN  )
   ,PROD_FILTER_ASSOC_DN_FK = ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.NAME = IR.PROD_FILTER_ASSOC_DN )
   ,ORG_FILTER_ASSOC_DN_FK  = ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.NAME = IR.ORG_FILTER_ASSOC_DN )
   ,CUST_FILTER_ASSOC_DN_FK = ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.NAME = IR.CUST_FILTER_ASSOC_DN )
   ,SUPL_FILTER_ASSOC_DN_FK = ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.NAME = IR.SUPL_FILTER_ASSOC_DN )                     
   ,[UPDATE_USER]           = isnull(IR.[UPDATE_USER], 'EPACUBE')
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 52 
AND   synchronizer.RULES_FILTER_SET.RULES_FK = IR.RULES_FK 




 SET @status_desc =  'finished execution of import.LOAD_IMPORT_RULES_FILTER'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_IMPORT_RULES_FILTER has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END























































