﻿


CREATE PROCEDURE [import].[NEW_ENTITY_TRANSFORMATION] ( @IN_JOB_FK bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure get primary entity
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure 
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--     
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int 
	DECLARE @v_import_package_fk   int  
	DECLARE @v_column_name   varchar(25) 
	DECLARE @v_primary   varchar(25)   
	DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
	 DECLARE @V_RULES_FILTER_LOOKUP  varchar(1)
	 DECLARE @V_SXE_MULTIPLE_CPT varchar(1)
	 DECLARE @V_SXE_MULTIPLE_CRT varchar(1)
     	
SET NOCOUNT ON;



BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.ENTITY_TRANSFORMATION', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps



--LOOKUP PRIMARY ENTITY BY PACKAGE
SET @v_import_package_fk = (select distinct import_package_fk 
							from import.import_record_data WITH (NOLOCK)
							where  job_fk =  @IN_JOB_FK)

SET @v_column_name = (select column_name 
						from import.import_map_metadata WITH (NOLOCK)
						where import_package_fk =  @v_import_package_fk
						and ident_data_name_fk  is not NULL
						and UNIQUE_SEQ_IND = 1)
						
						--= (select data_name_id
						--from epacube.data_name where name = 'CUSTOMER CODE'))  

SET @v_primary = (select source_field_name
						from import.import_map_metadata WITH (NOLOCK)
						where import_package_fk = @v_import_package_fk
						and ident_data_name_fk is not NULL
						and UNIQUE_SEQ_IND = 1)
						
--						=(select data_name_id
--from epacube.data_name where name = 'CUSTOMER CODE'))  

				UPDATE common.job
				SET name = (select name from import.import_package WITH (NOLOCK)
							WHERE import_package_id = @v_import_package_fk)
				WHERE job_id = @in_job_Fk

--LOOKUP WHAT MAKES THE ROW UNIQUE BY PACKAGE

SET @V_UNIQUE1 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 1),-999)

SET @V_UNIQUE2 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 2),-999)

SET @V_UNIQUE3 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 3), -999)

--------------------------------------------------------------------------------------
--  Call any special sql that needs to be performed using import sql table 
--  and procedure
--------------------------------------------------------------------------------------

			EXECUTE  [import].[EXECUTE_IMPORT_SQL] 
					@v_import_package_fk
					,@in_job_fk


/* ----------------------------------------------------------------------------------- */
--   --Create Temp table to insert distinct customer data 
/* ----------------------------------------------------------------------------------- */


--drop temp table if it exists
	IF object_id('tempdb..#TS_DISTINCT_CUSTOMERS') is not null
	   drop table #TS_DISTINCT_CUSTOMERS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_DISTINCT_CUSTOMERS(
	IMPORT_RECORD_DATA_FK	BIGINT,
	[IMPORT_FILENAME] [varchar](128),
	[RECORD_NO] [int],
	[JOB_FK] [bigint],
	[IMPORT_PACKAGE_FK] [int],
	[EFFECTIVE_DATE] [datetime],
	[END_DATE] [datetime],
	[ENTITY_CLASS] [varchar](64) ,
	[ENTITY_DATA_NAME] [varchar](64),
	[ENTITY_ID_DATA_NAME] [varchar](64),
	[ENTITY_ID_VALUE] [varchar](64),
	[ENTITY_STATUS] [varchar](32),
	[SYSTEM_ID_VALUE] [varchar](64),
	[PRIORITY] [varchar](32),
	[SEARCH_ACTIVITY_FK] [int],
	[UPDATE_TIMESTAMP] [datetime],
	[UPDATE_USER] [varchar](64)
		)

Create Index idx_tscust on #TS_DISTINCT_CUSTOMERS([JOB_FK], [ENTITY_ID_VALUE])

-------------------------------------------------------------------------------------
--   LOAD TEMP TABLE WITH DISTINCT ENTITY DATA
--   NEED to check if NULL Then do something else to get product identification 
-------------------------------------------------------------------------------------


SET @ls_exec =
						'INSERT INTO #TS_DISTINCT_CUSTOMERS
						(   [JOB_FK]
						   ,[IMPORT_PACKAGE_FK]
						   ,[END_DATE]
						   ,[ENTITY_CLASS]
						   ,[ENTITY_DATA_NAME]
						   ,[ENTITY_ID_DATA_NAME]
						   ,[ENTITY_ID_VALUE]
						   )
						SELECT distinct
							IRD.JOB_FK
							,IRD.IMPORT_PACKAGE_FK
						    ,IRD.END_DATE '
						   + ', ' 
						   +  ' ''PROFIT CENTER'' '
						   + ', '  
						   + ' ''PROFIT CENTER'' '
						    + ', ''' 
						   + @v_primary
						    + ''', '
						   + @v_column_name
						   + ' ' 
						+ ' FROM IMPORT.IMPORT_RECORD_DATA  IRD WITH (NOLOCK) '
						+ ' WHERE 1=1 '
						+ ' AND isnull(DO_NOT_IMPORT_IND,0) <> 1'
						+ ' AND '
						+ @v_column_name 
						+ ' IS NOT NULL'
						+ ' AND ISNULL(IRD.JOB_FK,-999) = '
						+ cast(isnull (@in_job_fk, -999) as varchar(20))



----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), @ls_exec )

         exec sp_executesql 	@ls_exec;

-----

UPDATE #TS_DISTINCT_CUSTOMERS
				SET RECORD_NO = x.ROW 
				,import_filename = (SELECT Top 1 import.import_record_data.import_filename
											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
											WHERE 1=1 
											AND Job_Fk = @in_job_fk)
				,effective_date = ISNULL((SELECT Top 1 import.import_record_data.effective_date
											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
											WHERE 1=1 
											AND Job_Fk = @in_job_fk), getdate())
				FROM (SELECT ROW_NUMBER() 
						OVER (ORDER BY ENTITY_ID_VALUE) AS Row, 
						ENTITY_ID_VALUE
				FROM #TS_DISTINCT_CUSTOMERS where job_fk = @in_job_fk)x
				where #TS_DISTINCT_CUSTOMERS.entity_id_value = x.entity_id_value
AND #TS_DISTINCT_CUSTOMERS.job_Fk = @in_job_fk



-----------------------------------------------------
	


-------------------------------------------------------------------------------------
--   LOAD IMPORT_RECORD TABLE(S) 
-------------------------------------------------------------------------------------

					INSERT INTO IMPORT.IMPORT_RECORD 
							   ([IMPORT_FILENAME]
							   ,[RECORD_NO]
							   ,[JOB_FK]
							   ,[IMPORT_PACKAGE_FK]
							   ,[EFFECTIVE_DATE]
							   ,[END_DATE]
							   ,[ENTITY_CLASS]
							   ,[ENTITY_DATA_NAME]
							   ,[ENTITY_ID_DATA_NAME]
							   ,[ENTITY_ID_VALUE]
							   ,[ENTITY_STATUS]
							   ,[UPDATE_TIMESTAMP]
							   ,[UPDATE_USER])

					SELECT 
							   TDP.IMPORT_FILENAME
							   ,TDP.RECORD_NO
							   ,TDP.JOB_FK
							   ,TDP.IMPORT_PACKAGE_FK
							   ,TDP.EFFECTIVE_DATE
							   ,TDP.END_DATE
							   ,TDP.ENTITY_CLASS
							   ,TDP.ENTITY_DATA_NAME
							   ,TDP.ENTITY_ID_DATA_NAME
							   ,TDP.ENTITY_ID_VALUE
							   ,'ACTIVE'
							   ,TDP.UPDATE_TIMESTAMP
							   ,TDP.UPDATE_USER
					FROM #TS_DISTINCT_CUSTOMERS TDP
					WHERE 1=1
					AND TDP.JOB_FK = @in_job_FK


-------------------------------------------------------------------------
 SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'ENTITY TRANSFORMATION',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


-------------------------------------------------------------------------------------
--   UPDATE IMPORT RECORD WITH RECORD_NO and SEQ_ID
-------------------------------------------------------------------------------------

			SET @ls_exec =
						'UPDATE IMPORT.IMPORT_RECORD_DATA
						set RECORD_NO = import.import_record.record_no
						from import.import_record WITH (NOLOCK)
						where'
						+ ' '
						+ @v_column_name 
						+ ' = entity_id_value
						and import.import_record_data.JOB_FK = '
						+ cast(isnull (@in_job_fk, -999) as varchar(20))
						+ ' and import.import_record.JOB_FK = '
						+ cast(isnull (@in_job_fk, -999) as varchar(20))


----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), @ls_exec )

         exec sp_executesql 	@ls_exec;


-----------

				UPDATE IMPORT.IMPORT_RECORD_DATA
				SET SEQ_ID = x.ROW 
				FROM (SELECT ROW_NUMBER() 
				OVER (ORDER BY IMPORT_RECORD_DATA_ID) AS Row, 
					 IMPORT_RECORD_DATA_ID
			FROM IMPORT.IMPORT_RECORD_DATA WITH (NOLOCK)
			where job_fk = @in_job_fk)x
			WHERE import.import_record_data.job_fk = @in_job_fk
			AND import.import_record_data.import_record_data_id = x.import_record_data_id


---------------------------------------------------------------------------
-- ADDED TO MARK ANY DUPLICATE ROWS AS DO NOT IMPORT
----------------------------------------------------------------------------


		SET @ls_exec =
						'UPDATE IMPORT.IMPORT_RECORD_DATA
						SET Do_Not_import_IND = 1 
						WHERE import_record_data_id IN (
								SELECT A.import_record_data_id
								FROM (         
								SELECT ird1.import_record_data_id 
					   ,DENSE_RANK() OVER ( PARTITION BY '
						   + ' ' 
                           + @V_UNIQUE1
						   + ' , ' 
                           + @V_UNIQUE2
					       + ' , '
                           + @V_UNIQUE3
						   + ''
						   + ' ORDER BY ird1.import_record_data_id DESC ) AS DRANK
							FROM IMPORT.IMPORT_RECORD_DATA IRD1
							WHERE ird1.import_record_data_id  in (
							select ird.import_record_data_id 
							from import.import_record_data ird
							where ird.job_fk = '
							+ cast(isnull (@in_job_fk, -999) as varchar(20))
							+ ')
										) A
										WHERE DRANK <> 1   )'		
					

----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), isnull ( @ls_exec, 'Trying to update do not import ind from stage.ENTITY_TRANSFORMATION' ) )

         exec sp_executesql 	@ls_exec;



--- EPA Constraint
UPDATE IMPORT.IMPORT_RECORD_DATA
				SET SEQ_ID = NULL
--,RECORD_NO = NULL
WHERE  job_fk = @in_job_fk
and DO_NOT_IMPORT_IND = 1

----------

------------------------------------------------------
---INSERT DATA INTO IMPORT MULTI VALUE TABLE
------------------------------------------------------
--HARD CODE FOR NOW

select * from epacube.data_name where name like '%Seller%'
select * from epacube.DATA_SET where DATA_SET_ID = 43120


If @v_import_package_fk = (select import_package_fk from import.IMPORT_MAP_METADATA imm with (nolock)
 INNER JOIN epacube.DATA_NAME dn With (nolock) 
 on (imm.DATA_NAME_FK = dn.DATA_NAME_ID
 and dn.RECORD_STATUS_CR_FK  = 1)
 INNER JOIN epacube.DATA_SET DS with (nolock)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK
 and TABLE_NAME = 'ENTITY_IDENT_MULT')
 and imm.RECORD_STATUS_CR_FK = 1
 and imm.IMPORT_PACKAGE_FK = @v_import_package_fk)


BEGIN 



--TRUNCATE TABLE EPACUBE.PRODUCT_MULT_TYPE


DECLARE 
               @vm$file_name varchar(100),
               @vm$record_no varchar(64),         
               @vm$job_fk int,
               @vm$import_package_fk bigint,                                           
               @vm$data_name varchar(64),
			   @vm$data_name_value varchar(max)

	DECLARE cur_v_import CURSOR local for
		SELECT  ird.IMPORT_FILENAME
			   ,ird.RECORD_NO
			   ,ird.job_fk
			   ,ird.IMPORT_PACKAGE_FK
			   ,vi.DATA_NAME
               ,vi.IMPORT_COLUMN_NAME
	  from import.import_record_data ird
	  inner join [import].[V_IMPORT] VI
	  on (VI.IMPORT_PACKAGE_FK = @v_import_package_fk
	  and VI.TABLE_NAME = 'ENTITY_IDENT_MULT')
		where ird.import_package_fk = @v_import_package_fk
		and ird.job_fk = @in_job_fk
		


         OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO @vm$file_name,
                                           @vm$record_no,         
                                           @vm$job_fk,
                                           @vm$import_package_fk,                                           
                                           @vm$data_name,
										   @vm$data_name_value
      WHILE @@FETCH_STATUS = 0
      BEGIN


		INSERT INTO [import].[IMPORT_RECORD_NAME_VALUE]
           ([IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[JOB_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[DATA_NAME]
           ,[DATA_VALUE]
           )
		select
			 @vm$file_name,
             @vm$record_no,         
             @vm$job_fk,
             @vm$import_package_fk,                                           
             @vm$data_name,
			 ss.ListItem
		from utilities.SplitString ( @vm$data_name_value, ',' ) ss	 




 FETCH NEXT FROM cur_v_import Into @vm$file_name,
                                   @vm$record_no,         
                                   @vm$job_fk,
                                   @vm$import_package_fk,                                           
                                   @vm$data_name,
								   @vm$data_name_value


END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_import
DEALLOCATE cur_v_import    

END
 




------------------------------------------------------
---CALL STAGE TO EVENTS
------------------------------------------------------
     SET @V_START_TIMESTAMP = getdate()			


			 EXEC[stage].[STG_TO_EVENTS] @IN_JOB_FK
	

      SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'STAGE TO EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



-------------------------------------------------------------------------------------
-- LAST Step is to Set Job Execution to Complete
-------------------------------------------------------------------------------------
----
----

      SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT ENTITIES TO STAGE' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT ENTITIES RESOLVED' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
		update common.job
		set job_complete_timestamp = getdate()
		where job_id = @in_job_fk

------------
--RUN THE SHIP TO TRANSFORMATION
------------


 SET @status_desc =  'finished execution of stage.ENTITY_TRANSFORMATION'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of STAGE.SXE_CUSTOMER_TRANSFORMATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;

EndOfProc:
END



