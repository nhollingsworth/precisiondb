﻿



-- Copyright 2007
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to activate/inactivate Import Map Metadata
-- 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        07/31/2007   Initial SQL Version

CREATE PROCEDURE [import].[IMPORT_MAPPING_MAINTENANCE] 
( @in_import_map_metadata_id int,
  @in_ui_action_cr_fk int)
												
AS

/***************  Parameter Defintions  **********************
.

***************************************************************/

BEGIN
--
--DECLARE @l_sysdate				datetime
--DECLARE @ls_stmt				varchar (1000)
--DECLARE @l_exec_no				bigint
--DECLARE @l_rows_processed		bigint
--DECLARE @v_count				bigint
--DECLARE @v_entity_structure_fk  bigint
--DECLARE @v_ENTITY_CLASS_CR_FK   bigint
--DECLARE @v_filter_ENTITY_CLASS_CR_FK  bigint
--DECLARE @v_filter_data_type_cr_fk bigint
--DECLARE @v_filter_data_name_fk   bigint
--DECLARE @v_filter_data_value_fk  bigint
--DECLARE @v_default_data_value_fk bigint
--DECLARE @v_table_name			varchar(64)
--DECLARE @v_filter_table_name	varchar(64)
--DECLARE @v_rule_data_name_fk    bigint
DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int

SET NOCOUNT ON;
--SET @l_sysdate = getdate ()
set @ErrorMessage = null


-------------------------------------------------------------------------------------
--   Peform UI Inactivate Action 
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = 8005  -- inactivate
BEGIN

UPDATE import.import_map_metadata
	set record_status_cr_fk = 2
		,update_timestamp = getdate()
    where import_map_metadata_id = @in_import_map_metadata_id	
		   return (0)
END -- IF @in_ui_action_cr_fk = 8005



-------------------------------------------------------------------------------------
--   Peform UI Activate Action 
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = 8007  -- activate

-------------------------------------------------------------------------------------
-- Validate that, for a given import package, there is only one active instance of a 
-- specific data position. For example, import package 4 cannot have two different active
-- import_map_metadata_ids for one data position, eg data_position25
-------------------------------------------------------------------------------------

BEGIN
	select @ErrorMessage 
    = 'Activated Value already exists for this Data Position in this Import Package. Import Package = ' + 
(select name from import.import_package where import_package_id = import_package_fk) + ' Column = ' + column_name
	from import.import_map_metadata
	where import_package_fk = 
	(select import_package_fk 
	from import.import_map_metadata
	where import_map_metadata_id = @in_import_map_metadata_id)
	and column_name = 
	(select column_name 
	from import.import_map_metadata
	where import_map_metadata_id = @in_import_map_metadata_id)
	and import_map_metadata_id <> @in_import_map_metadata_id
	and record_status_cr_fk = 1

	IF @ErrorMessage is not null
	BEGIN
		GOTO RAISE_ERROR
    END
END




BEGIN
UPDATE import.import_map_metadata
	set record_status_cr_fk = 1
		,update_timestamp = getdate()
    where import_map_metadata_id = @in_import_map_metadata_id
	return (0)
END -- IF @in_ui_action_cr_fk = 8007


RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END




































