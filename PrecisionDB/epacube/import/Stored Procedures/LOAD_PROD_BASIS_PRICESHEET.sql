﻿















-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        06/02/2011   Initial Version 
-- 

CREATE PROCEDURE [import].[LOAD_PROD_BASIS_PRICESHEET] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
      
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_IMPORT_PROD_BASIS_VALUES', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.IMPORT_ECLIPSE_PRICE_HISTORY WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM import.IMPORT_ECLIPSE_PRICE_HISTORY WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD IMPORT PROD BASIS VALUES',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;




-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( JOB_ERRORS TABLE )
--
--     THESE ERRORS PROHIBIT THE ROWS FROM BEING IMPORTED
--
--	   ERROR NAME:  INCOMPLETE OR INVALID DATA PROHIBITING IMPORT 
--     EXAMPLE IS MISSING OR INVALID STRUCTURE
--
--     IMPORT ROW MARKED  ERROR_ON_IMPORT_IND = 1 
--
-------------------------------------------------------------------------------------


----Make data_position100 = product_structure_fk

UPDATE import.IMPORT_RECORD_DATA
SET DATA_POSITION100 = (select pi.product_Structure_Fk
FROM epacube.product_identification pi
where import.IMPORT_RECORD_DATA.DATA_POSITION1 = pi.value
and pi.data_name_FK = 110100)
WHERE 1 = 1                   
AND   JOB_FK = @IN_JOB_FK 

----Make data_position101 = product_structure_fk
UPDATE import.IMPORT_RECORD_DATA
SET DATA_POSITION101 = isnull((select ei.entity_Structure_Fk
FROM epacube.entity_identification ei
where import.IMPORT_RECORD_DATA.DATA_POSITION2 = ei.value),1)
--and ei.entity_data_name_FK = (select (Case when D = 'Default' then 141099  --all whse
--				when BranchType = 'Branch' then 141000 --Whse
--				when BranchType = 'Territory' then 311030 --ecl territory
--				else null
--				end)
--))

WHERE 1 = 1                   
AND   JOB_FK  = @IN_JOB_FK 


update import.IMPORT_RECORD_DATA
set DO_NOT_IMPORT_IND = 1
where DATA_POSITION100 is  NULL
and job_fk = @in_job_Fk


update import.IMPORT_RECORD_DATA
set DO_NOT_IMPORT_IND= 1
where DATA_POSITION101 is  NULL
and job_fk = @in_job_Fk



INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[TABLE_PK]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[ERROR_DATA4]
--           ,[ERROR_DATA5]
--           ,[ERROR_DATA6]
           ,[CREATE_TIMESTAMP])
SELECT 
JOB_FK
,'IMPORT PRODUCT BASIS'
,NULL
,99
,'MISSING PRODUCT/ORG INFORMATION'
,DATA_POSITION1
,DATA_POSITION2
,effective_date
,NULL
,getdate()
FROM import.IMPORT_RECORD_DATA
WHERE ISNULL(DO_NOT_IMPORT_IND ,0) = 1
AND DATA_POSITION100 is NULL


-------------------------------------------------------------------------------------
--   MATCH PRODUCTS
--		1st 6 net values
-------------------------------------------------------------------------------------

TRUNCATE TABLE [marginmgr].[PRICESHEET]


DECLARE  @v_net_value1		  varchar(32)
DECLARE  @v_net_value2		  varchar(32)
DECLARE  @v_net_value3		  varchar(32)
DECLARE  @v_net_value4		  varchar(32)
DECLARE  @v_net_value5		  varchar(32)
DECLARE  @v_net_value6		  varchar(32)

SET @v_net_value1 =  ISNULL (
					'IRD.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = 450000
								AND   DN.PARENT_DATA_NAME_FK = 	381600
								AND   DS.COLUMN_NAME = 'NET_VALUE1' )
                     , 'NULL' )


SET @v_net_value2 =  ISNULL (
					'IRD.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = 450000
								AND   DN.PARENT_DATA_NAME_FK = 	381600
								AND   DS.COLUMN_NAME = 'NET_VALUE2' )
                     , 'NULL' )



SET @v_net_value3 =  ISNULL (
					'IRD.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = 450000
								AND   DN.PARENT_DATA_NAME_FK = 	381600
								AND   DS.COLUMN_NAME = 'NET_VALUE3' )
                     , 'NULL' )


SET @v_net_value4 =  ISNULL (
					'IRD.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = 450000
								AND   DN.PARENT_DATA_NAME_FK = 	381600
								AND   DS.COLUMN_NAME = 'NET_VALUE4' )
                     , 'NULL' )


SET @v_net_value5 =  ISNULL (
					'IRD.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = 450000
								AND   DN.PARENT_DATA_NAME_FK = 	381600
								AND   DS.COLUMN_NAME = 'NET_VALUE5' )
                     , 'NULL' )
                     
SET @v_net_value6 =  ISNULL (
					'IRD.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = 450000
								AND   DN.PARENT_DATA_NAME_FK = 	381600
								AND   DS.COLUMN_NAME = 'NET_VALUE6' )
                     , 'IRD.DATA_POSITION199' )


	SET @ls_exec =
				'INSERT INTO [marginmgr].[PRICESHEET]
           ([PRICESHEET_NAME]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[RESULT_TYPE_CR_FK]
           ,[NET_VALUE1]
           ,[NET_VALUE2]
           ,[NET_VALUE3]
           ,[NET_VALUE4]
           ,[NET_VALUE5]
           ,[NET_VALUE6]
           ,[PRICESHEET_QTY_PER]
           ,[VALUE_UOM_CODE_FK]
           ,[UOM_CODE]
           ,[PROD_ID_DN]
           ,[PROD_ID_VALUE]
           ,[ORG_ID_VALUE]
           ,[ENTITY_ID_ENTITY_DN]
           ,[ENTITY_ID_DN]
           ,[ENTITY_ID_VALUE]
           ,[CURRENCY_CR_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
(SELECT 
IRD.DATA_POSITION3 
,381600  
,IRD.DATA_POSITION100
,IRD.DATA_POSITION101
,NULL 
,IRD.DATA_POSITION4  
,IRD.DATA_POSITION5
,713'
          +  ' ,' + @v_net_value1 
         +  ' ,' + @v_net_value2 
         +  ' ,' + @v_net_value3 
         +  ' ,' + @v_net_value4 
         +  ' ,' + @v_net_value5 
         +  ' ,' + @v_net_value6 
         + ' ,IRD.DATA_POSITION7 
           ,(select UOM_CODE_ID FROM epacube.uom_code with (NOLOCK)
			Where uom_code = IRD.DATA_POSITION6)
           ,IRD.DATA_POSITION6  
           ,110100 
           ,IRD.DATA_POSITION1   
         
           ,IRD.DATA_POSITION3 
           ,NULL  
           ,NULL 
           ,NULL 
           ,1200  
           ,1
           ,getdate()
           ,NULL
           ,NULL
 FROM import.IMPORT_RECORD_DATA IRD
WHERE 1 = 1 
AND ISNULL(IRD.DO_NOT_IMPORT_IND,0) <> 1
AND IRD.JOB_FK = '
+ cast(isnull (@in_job_fk, -999) as varchar(20))
+ ')'

 INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), ISNULL ( @ls_exec, 'SYNCHRONIZER.IMPORT_PROD_BASIS_VALUES' )  )

         
         
   exec sp_executesql 	@ls_exec;



---------------------------------------------------------------------------------------
----   MATCH PRODUCTS
----		2nd set of 6 net values
---------------------------------------------------------------------------------------

SET @v_net_value1 =  ISNULL (
					'IRD.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = 450000
								AND   DN.PARENT_DATA_NAME_FK = 	381700
								AND   DS.COLUMN_NAME = 'NET_VALUE1' )
                     , 'NULL' )


SET @v_net_value2 =  ISNULL (
					'IRD.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = 450000
								AND   DN.PARENT_DATA_NAME_FK = 	381700
								AND   DS.COLUMN_NAME = 'NET_VALUE2' )
                     , 'NULL' )



SET @v_net_value3 =  ISNULL (
					'IRD.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = 450000
								AND   DN.PARENT_DATA_NAME_FK = 	381700
								AND   DS.COLUMN_NAME = 'NET_VALUE3' )
                     , 'NULL' )


SET @v_net_value4 =  ISNULL (
					'IRD.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = 450000
								AND   DN.PARENT_DATA_NAME_FK = 	381700
								AND   DS.COLUMN_NAME = 'NET_VALUE4' )
                     , 'NULL' )


SET @v_net_value5 =  ISNULL (
					'IRD.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = 450000
								AND   DN.PARENT_DATA_NAME_FK = 	381700
								AND   DS.COLUMN_NAME = 'NET_VALUE5' )
                     , 'NULL' )
                     
SET @v_net_value6 =  ISNULL (
					'IRD.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = 450000
								AND   DN.PARENT_DATA_NAME_FK = 	381700
								AND   DS.COLUMN_NAME = 'NET_VALUE6' )
                     , 'IRD.DATA_POSITION199' )


	SET @ls_exec =
				'INSERT INTO [marginmgr].[PRICESHEET]
           ([PRICESHEET_NAME]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[RESULT_TYPE_CR_FK]
           ,[NET_VALUE1]
           ,[NET_VALUE2]
           ,[NET_VALUE3]
           ,[NET_VALUE4]
           ,[NET_VALUE5]
           ,[NET_VALUE6]
           ,[PRICESHEET_QTY_PER]
           ,[VALUE_UOM_CODE_FK]
           ,[UOM_CODE]
           ,[PROD_ID_DN]
           ,[PROD_ID_VALUE]
           ,[ORG_ID_VALUE]
           ,[ENTITY_ID_ENTITY_DN]
           ,[ENTITY_ID_DN]
           ,[ENTITY_ID_VALUE]
           ,[CURRENCY_CR_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
(SELECT 
IRD.DATA_POSITION3 
,381700  
,IRD.DATA_POSITION100
,IRD.DATA_POSITION101
,NULL 
,IRD.DATA_POSITION4  
,IRD.DATA_POSITION5
,713'
          +  ' ,' + @v_net_value1 
         +  ' ,' + @v_net_value2 
         +  ' ,' + @v_net_value3 
         +  ' ,' + @v_net_value4 
         +  ' ,' + @v_net_value5 
         +  ' ,' + @v_net_value6 
         + ' ,IRD.DATA_POSITION7 
           ,(select UOM_CODE_ID FROM epacube.uom_code with (NOLOCK)
			Where uom_code = IRD.DATA_POSITION6)
           ,IRD.DATA_POSITION6  
           ,110100 
           ,IRD.DATA_POSITION1   
         
           ,IRD.DATA_POSITION3 
           ,NULL  
           ,NULL 
           ,NULL 
           ,1200  
           ,1
           ,getdate()
           ,NULL
           ,NULL
 FROM import.IMPORT_RECORD_DATA IRD
WHERE 1 = 1 
AND ISNULL(IRD.DO_NOT_IMPORT_IND,0) <> 1
AND IRD.JOB_FK = '
+ cast(isnull (@in_job_fk, -999) as varchar(20))
+ ')'

 INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), ISNULL ( @ls_exec, 'SYNCHRONIZER.IMPORT_PROD_BASIS_VALUES' )  )

         
         
   exec sp_executesql 	@ls_exec;


--------------------------------------------------------------------------

----add some logging

 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD PROD BASIS VALUES' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD PROD BASIS VALUES' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( RULES_ERRORS TABLE )
--
--     ADD RULES VALIDATION ROUTINE TO BE CALLED FROM UI LATER.. 
--
--	   THESE ERRORS ARE BUSINESS LOGIC ERRORS 
--        WHICH WILL BE BY RULE_TYPE_CR_FK AND RULES_ACTION_OPERATION_FK
--     
--     RULE WILL BE WRITTEN TO RULES TABLE BUT WITH AN ERROR CONDITION
--
--
-------------------------------------------------------------------------------------






 SET @status_desc =  'finished execution of import.LOAD_PROD_BASIS_VALUES'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_PROD_BASIS_VALUES has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END



















