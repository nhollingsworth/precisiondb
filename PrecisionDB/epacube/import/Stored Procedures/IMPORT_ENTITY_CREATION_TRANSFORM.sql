﻿













-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        06/16/2011   Initial Version 
CREATE PROCEDURE [import].[IMPORT_ENTITY_CREATION_TRANSFORM] ( @IN_JOB_FK bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to   
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure 
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--     
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int 
	 DECLARE @v_import_package_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.IMPORT_ENTITY_CREATE_TRANSFORMATION', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps

----------------------------------------------------------------------
--set entity Class
----------------------------------------------------------------------
--Update import.IMPORT_RECORD_DATA 
--set DATA_POSITION1 = 'VENDOR'
-- where 1=1 
-- AND data_position1 = 'MANUFACTURER'
-- AND JOB_FK = @IN_JOB_FK


--Update import.IMPORT_RECORD_DATA 
--set DATA_POSITION7 = 
--(case when data_position1 = 'MANUFACTURER' THEN 'VENDOR' 
-- when data_position1 like 'SALESREP%' THEN 'SALESREP' 
-- when data_position1 like 'CUSTOMER%' THEN 'CUSTOMER'
-- when data_position1 = 'WHSE_TERRITORY' THEN 'WAREHOUSE'
-- when data_position1 = 'ECL TERRITORY' THEN 'WAREHOUSE'
-- when data_position1 = 'SALES_TERRITORY' THEN 'TERRITORY'
-- when data_position1 = 'ORDER TAKER' THEN 'SALESREP'
-- ELSE DATA_POSITION1 end) 
-- ,data_position1 = 
-- (case when data_position1 = 'WHSE_TERRITORY' THEN 'ECL TERRITORY'
-- ELSE DATA_POSITION1 end)
-- where JOB_FK =    @IN_JOB_FK

--Update import.IMPORT_RECORD_DATA 
--set DATA_POSITION1 = 
--(case when data_position1 = 'MANUFACTURER' THEN 'VENDOR' 
-- when data_position1 like 'SALESREP%' THEN 'SALESREP' 
-- when data_position1 like 'CUSTOMER%' THEN 'CUSTOMER'
-- when data_position1 = 'WHSE_TERRITORY' THEN 'WAREHOUSE'
-- when data_position1 = 'ECL TERRITORY' THEN 'TERRITORY'
-- when data_position1 = 'SALES_TERRITORY' THEN 'TERRITORY'
-- when data_position1 = 'ORDER TAKER' THEN 'SALESREP'
-- ELSE DATA_POSITION1 end) 

-- where JOB_FK =    @IN_JOB_FK

--------------------------------------------------------------------
-- Get the entity class and loop through to create import record data
--------------------------------------------------------------------
DECLARE @entity_class   varchar(50),
		 @entity_Data_name   varchar(50),
		  @entity_Data_name_fk   varchar(50),
		  @class_Data_name_fk   varchar(50),
		  @ident_Data_name_fk   varchar(50),
		  @parent_Data_name_fk   varchar(50),
		  @Assoc_Data_name_fk   varchar(50),
		  @desc_Data_name_fk   varchar(50),
		   @job_fk   varchar(50)

DECLARE  cur_v_import cursor local for
				select distinct ird.data_position1 entity_class
				,ird.DATA_POSITION2 entity_data_name
				,isnull (dn.data_name_id, dn1.DATA_NAME_ID) entity_data_Name_fk
				,dn1.DATA_NAME_ID class_data_name_fk
				,isnull(ep.value, dn.DATA_NAME_ID) ident_data_name_fk
				,dn3.DATA_NAME_ID parent_data_name_Fk
				,(CASE when ird.data_position2 = 'CUSTOMER SHIP TO' 
				then  159401 Else NULL End) Assoc
				--,dn3.DATA_NAME_ID desc_data_name
				,(case when ird.data_position1 = 'PRODUCT' then 110401 
				else 
				(select dn4.data_name_id from epacube.DATA_NAME dn4 with (nolock)
				where ird.DATA_POSITION1 + ' NAME' = dn4.NAME)
				 END) desc_data_name
			     ,ird.job_fk
				from import.IMPORT_RECORD_DATA ird with (nolock) 
				inner  join epacube.DATA_NAME dn with (nolock)
				on (ird.DATA_POSITION2 = dn.NAME)
				inner join epacube.DATA_NAME dn1 with (nolock)
				on (ird.DATA_POSITION1 = dn1.NAME)
				left join epacube.EPACUBE_PARAMS ep
				on (ep.NAME = ird.DATA_POSITION1
				and ep.APPLICATION_SCOPE_FK = 100)
				left join epacube.DATA_NAME dn3 with (nolock)
				on (ird.DATA_POSITION5 = dn3.NAME)		
				order by dn3.DATA_NAME_id asc


				
				

OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO 
				@entity_class,  
			 @entity_Data_name,   
		  @entity_Data_name_fk,  
		  @class_Data_name_fk,
		  @ident_Data_name_fk,
		  @parent_Data_name_fk,
		  @Assoc_Data_name_fk,
		  @desc_Data_name_fk,
		   @job_fk  

				
         WHILE @@FETCH_STATUS = 0 
         BEGIN

---Update package for class and search activity
update import.IMPORT_PACKAGE
set ENTITY_CLASS_CR_FK = (select code_ref_id from epacube.CODE_REF
where CODE_TYPE = 'ENTITY_CLASS' and CODE = @entity_class)
,search_activity_fk = (case when @entity_class = 'PRODUCT' then 1
 else 2 end)
where IMPORT_PACKAGE_ID = 410000


--clear map metadata for next set.
update import.IMPORT_MAP_METADATA
set Data_name_fk = NULL 
,ident_data_name_fk =  NULL
,ASSOC_DATA_NAME_FK = NULL
where IMPORT_PACKAGE_FK = 410000
 
--- first update code
update import.IMPORT_MAP_METADATA
set Data_name_fk =  @entity_Data_name_fk 
,ident_data_name_fk =  @ident_Data_name_fk 
where import_map_metadata_id = 410003
and @job_fk = @IN_JOB_FK

--- then update name
update import.IMPORT_MAP_METADATA
set Data_name_fk = @desc_Data_name_fk 
,IDENT_DATA_NAME_FK = @ident_Data_name_fk
where import_map_metadata_id = 410004
and @job_fk = @IN_JOB_FK
and @entity_class <> 'PRODUCT'


 --- if parent entity
 
 update import.IMPORT_MAP_METADATA
set Data_name_fk = @parent_Data_name_fk
,assoc_data_name_fk = @assoc_Data_name_fk
,ident_data_name_fk =  @ident_Data_name_fk 
where import_map_metadata_id = 410006
And @parent_data_name_fk is not NULL
and @job_fk = @IN_JOB_FK

-- update import.IMPORT_MAP_METADATA
--set ident_data_name_fk =  @ident_Data_name_fk 
--where import_map_metadata_id = 410005
--and ASSOC_DATA_NAME_FK is Not NULL







--LOOKUP PRIMARY PRODUCT BY PACKAGE
SET @v_import_package_fk = (select distinct import_package_fk 
							from import.import_record_data WITH (NOLOCK)
							where  job_fk = @in_job_fk)

SET @v_column_name = (select column_name 
						from import.import_map_metadata WITH (NOLOCK)
						where import_package_fk = @v_import_package_fk
						and data_name_fk = (select data_name_id
						from epacube.data_name where name = @entity_data_name
						and UNIQUE_SEQ_IND = 1))  

SET @v_primary = (select source_field_name
						from import.import_map_metadata WITH (NOLOCK)
						where import_package_fk = @v_import_package_fk
						and data_name_fk =(select data_name_id
						from epacube.data_name where name = @entity_data_name
						and UNIQUE_SEQ_IND = 1))  

--LOOKUP WHAT MAKES THE ROW UNIQUE BY PACKAGE

SET @V_UNIQUE1 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 1),-999)

SET @V_UNIQUE2 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 2),-999)

SET @V_UNIQUE3 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 3), -999)


--ADDED FOR DMU IMPORT SET THE NAME OF PACKAGE

				UPDATE common.job
				SET name = (select name from import.import_package WITH (NOLOCK)
							WHERE import_package_id = @v_import_package_fk)
				WHERE job_id = @in_job_Fk

--------------------------------------------------------------------------------------
--  Call any special sql that needs to be performed using import sql table 
--  and procedure
--------------------------------------------------------------------------------------

			--EXECUTE  [import].[EXECUTE_IMPORT_SQL] 
			--		@v_import_package_fk
			--		,@in_job_fk


-------------------------------------------------------------------------------------------------
-- ADDED TO NOT ALLOW DUPLICATE ROWS TO BE IMPORTED
-------------------------------------------------------------------------------------------------



		SET @ls_exec =
						'UPDATE IMPORT.IMPORT_RECORD_DATA
						SET Do_Not_import_IND = 1 
						WHERE import_record_data_id IN (
								SELECT A.import_record_data_id
								FROM (         
								SELECT ird1.import_record_data_id 
					   ,DENSE_RANK() OVER ( PARTITION BY '
						   + ' ' 
						   + 'data_position2'
						   + ' , '
                           + @V_UNIQUE1
						   + ' , ' 
                           + @V_UNIQUE2
					       + ' , '
                           + @V_UNIQUE3
						   + ''
						   + ' ORDER BY ird1.import_record_data_id DESC ) AS DRANK
							FROM IMPORT.IMPORT_RECORD_DATA IRD1
							WHERE ird1.import_record_data_id  in (
							select ird.import_record_data_id 
							from import.import_record_data ird
							where ird.job_fk = '
							+ cast(isnull (@in_job_fk, -999) as varchar(20))
							+ ')
										) A
										WHERE DRANK <> 1   )
										AND IMPORT_PACKAGE_FK not in ( 213000)'							

----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), isnull ( @ls_exec, 'Trying to update do not import ind from stage.IMPORT_TRANSFORMATION' ) )

         exec sp_executesql 	@ls_exec;


/* ----------------------------------------------------------------------------------- */
--   --Create Temp table to insert distinct product data 
/* ----------------------------------------------------------------------------------- */


--drop temp table if it exists
	IF object_id('tempdb..#TS_DISTINCT_PRODUCTS') is not null
	   drop table #TS_DISTINCT_PRODUCTS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_DISTINCT_PRODUCTS(
    [RECORD_NO] [int] IDENTITY(1,1) NOT NULL,
	[IMPORT_RECORD_DATA_FK]	BIGINT,
	[IMPORT_FILENAME] [varchar](128),
	[JOB_FK] [bigint],
	[IMPORT_PACKAGE_FK] [int],
	[EFFECTIVE_DATE] [datetime],
	[END_DATE] [datetime],
	[ENTITY_CLASS] [varchar](64) ,
	[ENTITY_DATA_NAME] [varchar](64),
	[ENTITY_ID_DATA_NAME] [varchar](64),
	[ENTITY_ID_VALUE] [varchar](64),
	[ENTITY_STATUS] [varchar](32),
	[SYSTEM_ID_VALUE] [varchar](64),
	[PRIORITY] [varchar](32),
	[SEARCH_ACTIVITY_FK] [int],
	[UPDATE_TIMESTAMP] [datetime],
	[UPDATE_USER] [varchar](64)
		)



-------------------------------------------------------------------------------------
--   LOAD TEMP TABLE WITH DISTINCT PRODUCT DATA
--   NEED to check if NULL Then do something else to get product identification 
-------------------------------------------------------------------------------------

SET @v_effective_date = ISNULL((SELECT Top 1 import.import_record_data.effective_date
											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
											WHERE 1=1 
											AND Job_Fk = @in_job_fk), getdate())

SET @v_import_filename = (SELECT Top 1 import.import_record_data.import_filename
											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
											WHERE 1=1 
											AND Job_Fk = @in_job_fk)



SET @ls_exec =
						'INSERT INTO #TS_DISTINCT_PRODUCTS
						(   [JOB_FK]
						   ,[IMPORT_PACKAGE_FK]
						   
						   ,[IMPORT_FILENAME] 
                           ,[EFFECTIVE_DATE]
                           ,[ENTITY_CLASS]
						   ,[ENTITY_DATA_NAME]
						   ,[ENTITY_ID_DATA_NAME]
						   ,[ENTITY_ID_VALUE]
						   )
						SELECT distinct
							IRD.JOB_FK
							,IRD.IMPORT_PACKAGE_FK '
						   
						   + ', ''' 
                           + @v_import_filename
						   + ''' , ''' 
                           + cast ( @v_effective_date as varchar(30) )
						   + ''' , ''' 
						   +  @entity_class
						   + ''',  '''
						   + @entity_Data_name
						    + ''',''' 
						   + @v_primary
						    + ''','
						   + @v_column_name
						   + ' ' 
						+ ' FROM IMPORT.IMPORT_RECORD_DATA  IRD WITH (NOLOCK) '
						+ ' WHERE 1=1 '
						+ ' AND isnull(DO_NOT_IMPORT_IND,0) <> 1'
						+ ' AND '
						+ @v_column_name 
						+ ' IS NOT NULL AND DATA_POSITION2 = '''
						+ @entity_data_name
						+ ''' AND ISNULL(IRD.JOB_FK,-999) = '
						+ cast(isnull (@in_job_fk, -999) as varchar(20))



----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), isnull ( @ls_exec, 'Trying to load Null from inserting distinct record data' ) )

         exec sp_executesql 	@ls_exec;

-----------------------------------------------------
	
--- This was taking over one hour just for the update... so reworked SQL to do all during insert 

------				UPDATE #TS_DISTINCT_PRODUCTS
------				SET RECORD_NO = x.ROW 
------				,import_filename = (SELECT Top 1 import.import_record_data.import_filename
------											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
------											WHERE 1=1 
------											AND Job_Fk = @in_job_fk)
------				,effective_date = (SELECT Top 1 import.import_record_data.effective_date
------											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
------											WHERE 1=1 
------											AND Job_Fk = @in_job_fk)
------				FROM (SELECT ROW_NUMBER() 
------						OVER (ORDER BY ENTITY_ID_VALUE) AS Row, 
------						ENTITY_ID_VALUE
------				FROM #TS_DISTINCT_PRODUCTS where job_fk = @in_job_fk)x
------				where #TS_DISTINCT_PRODUCTS.entity_id_value = x.entity_id_value
------AND #TS_DISTINCT_PRODUCTS.job_Fk = @in_job_fk

---Couldn't just insert filename above as it may not be distinct 
--if multiple files importing
---  CINDY comment does not make sense... doing top 1 by job where job is parameter ?????
---			Each file should be its own job... need to do that in SSIS


-------------------------------------------------------------------------------------
--   LOAD IMPORT_RECORD TABLE(S) 
-------------------------------------------------------------------------------------

					INSERT INTO IMPORT.IMPORT_RECORD 
							   ([IMPORT_FILENAME]
							   ,[RECORD_NO]
							   ,[JOB_FK]
							   ,[IMPORT_PACKAGE_FK]
--							   ,[EFFECTIVE_DATE]
							   ,[END_DATE]
							   ,[ENTITY_CLASS]
							   ,[ENTITY_DATA_NAME]
							   ,[ENTITY_ID_DATA_NAME]
							   ,[ENTITY_ID_VALUE]
							   ,[UPDATE_TIMESTAMP]
							   ,[UPDATE_USER])

					SELECT 
							   TDP.IMPORT_FILENAME
							   ,TDP.RECORD_NO
							   ,TDP.JOB_FK
							   ,TDP.IMPORT_PACKAGE_FK
--							   ,TDP.EFFECTIVE_DATE
							   ,TDP.END_DATE
							   ,TDP.ENTITY_CLASS
							   ,TDP.ENTITY_DATA_NAME
							   ,TDP.ENTITY_ID_DATA_NAME
							   ,TDP.ENTITY_ID_VALUE
							   ,TDP.UPDATE_TIMESTAMP
							   ,TDP.UPDATE_USER
					FROM #TS_DISTINCT_PRODUCTS TDP
					WHERE 1=1
					AND TDP.JOB_FK = @in_job_FK


-------------------------------------------------------------------------
 SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'SSIS TRANSFORMATION',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


-------------------------------------------------------------------------------------
--   UPDATE IMPORT RECORD WITH RECORD_NO and SEQ_ID
-------------------------------------------------------------------------------------

			SET @ls_exec =
						'UPDATE IMPORT.IMPORT_RECORD_DATA
						set RECORD_NO = import.import_record.record_no
						from import.import_record WITH (NOLOCK)
						where'
						+ ' '
						+ @v_column_name 
						+ ' = import.import_record.entity_id_value
						and import.import_record_data.JOB_FK = '
						+ cast(isnull (@in_job_fk, -999) as varchar(20))
						+ ' and data_position2 = '
						+ ''''
						+ @entity_Data_name
						+ ''''
						


----           TRUNCATE TABLE C1ON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), isnull ( @ls_exec, 'Trying to Update Import Record data in stage.IMPORT_TRANSFORMATION' ) )

         exec sp_executesql 	@ls_exec;


-----------

				UPDATE IMPORT.IMPORT_RECORD_DATA
				SET SEQ_ID = x.ROW 
				FROM (SELECT ROW_NUMBER() 
				OVER (ORDER BY IMPORT_RECORD_DATA_ID) AS Row, 
					 IMPORT_RECORD_DATA_ID
			FROM IMPORT.IMPORT_RECORD_DATA WITH (NOLOCK)
			where job_fk = @in_job_fk)x
			WHERE import.import_record_data.job_fk = @in_job_fk
			AND import.import_record_data.RECORD_NO is not NULL
			AND import.import_record_data.import_record_data_id = x.import_record_data_id

--- EPA Constraint
UPDATE IMPORT.IMPORT_RECORD_DATA
				SET SEQ_ID = NULL
--,RECORD_NO = NULL
WHERE  job_fk = @in_job_fk
and DO_NOT_IMPORT_IND = 1

----------

UPDATE IMPORT.IMPORT_RECORD_DATA
SET EFFECTIVE_DATE = getdate() 
WHERE  JOB_Fk =  @in_job_FK
AND EFFECTIVE_DATE IS NULL


UPDATE IMPORT.IMPORT_RECORD
SET EFFECTIVE_DATE = (SELECT TOP 1 ISNULL(IRD.EFFECTIVE_DATE,getdate())
FROM IMPORT.IMPORT_RECORD_DATA IRD
WHERE IRD.RECORD_NO = import.import_record.record_no
AND IRD.JOB_fK = import.import_record.JOB_FK)
WHERE RECORD_NO = import.import_record.record_no
AND JOB_FK = import.import_record.JOB_FK
AND JOB_Fk =  @in_job_FK


update import.IMPORT_MAP_METADATA
set Data_name_fk = ( case when @entity_Data_name_fk = 141111 then 141000 else @entity_data_name_fk end)
where import_map_metadata_id = 410003
and @job_fk = @IN_JOB_FK

------------------------------------------------------
---CALL STAGE TO EVENTS
------------------------------------------------------
     SET @V_START_TIMESTAMP = getdate()			


			 EXEC[stage].[STG_TO_EVENTS] @IN_JOB_FK
			 
------------------------------------------------------------
--clean out to start over.
----------------------------------------------------------
update import.IMPORT_RECORD_DATA
set RECORD_NO = NULL, SEQ_ID = NULL 

truncate table  import.import_record --set job_fk = 0
truncate table cleanser.SEARCH_JOB_DATA_NAME
truncate table cleanser.SEARCH_JOB_RESULTS_BEST
truncate table cleanser.SEARCH_JOB_RESULTS_IDENT
truncate table cleanser.SEARCH_JOB_RESULTS_TASK
truncate table cleanser.SEARCH_JOB_RESULTS_TASK_WORK
truncate table cleanser.SEARCH_JOB_from_to_match
truncate table cleanser.SEARCH_JOB_STRING_WORK
delete from cleanser.search_JOB_string
DBCC CHECKIDENT ( 'cleanser.search_job_string',reseed,1000)
DELETE FROM cleanser.search_job

TRUNCATE TABLE stage.STG_RECORD_ERRORS
DELETE FROM STAGE.STG_RECORD_ENTITY_IDENT
TRUNCATE TABLE STAGE.STG_RECORD_IDENT
DELETE FROM STAGE.STG_RECORD_ENTITY
TRUNCATE TABLE STAGE.STG_RECORD_DATA
TRUNCATE TABLE STAGE.STG_RECORD_STRUCTURE
DELETE FROM stage.STG_RECORD
TRUNCATE TABLE stage.Import_Data_Exceptions

DBCC CHECKIDENT ( 'STAGE.STG_RECORD_ENTITY_IDENT', RESEED, 1000 )
DBCC CHECKIDENT ( 'STAGE.STG_RECORD_ENTITY', RESEED, 1000 )
DBCC CHECKIDENT ( 'stage.STG_RECORD', RESEED, 1000 )



	 
			 
			 
			 
			 FETCH NEXT FROM cur_v_import INTO 
										  @entity_class,  
			 @entity_Data_name,   
		  @entity_Data_name_fk,  
		  @class_Data_name_fk,
		  @ident_Data_name_fk,
		  @parent_Data_name_fk,
		  @Assoc_Data_name_fk,
		   @desc_Data_name_fk, 
		   @job_fk


     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import;
	 
	 
	

      SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'STAGE TO EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



-------------------------------------------------------------------------------------
-- LAST Step is to Set Job Execution to Complete
-------------------------------------------------------------------------------------
----
----

      SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT PRODUCTS TO STAGE' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT PRODUCTS RESOLVED' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
		update common.job
		set job_complete_timestamp = getdate()
		where job_id = @in_job_fk



--drop temp table if it exists
	IF object_id('tempdb..#TS_DISTINCT_PRODUCTS') is not null
	   drop table #TS_DISTINCT_PRODUCTS;
	   



 SET @status_desc =  'finished execution of stage.IMPORT_TRANSFORMATION'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of stage.IMPORT_TRANSFORMATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END


































































































