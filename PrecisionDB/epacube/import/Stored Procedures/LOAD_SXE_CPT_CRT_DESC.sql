﻿
















-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        09/03/2011   Initial Version - created for Hill and Marks
-- CV        09/09/2011   Making the VENDOR section update /insert
-- CV        09/12/2011   Adding more desc updates to for other data names
-- CV        10/17/2011   Added courser for update of vendor name
-- CV        10/21/2011   Added top 1 and not null to select for update of descptions


CREATE PROCEDURE [import].[LOAD_SXE_CPT_CRT_DESC] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128)
	 DECLARE @V_SXE_MULTIPLE_CPT varchar(1)
	 DECLARE @V_SXE_MULTIPLE_CRT varchar(1) 

--DECLARE @in_job_fk bigint

--set @in_job_fk = (select distinct job_fk from import.import_sxe_table_data)

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_SXE_CPT_CRT_DESC', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.IMPORT_SXE_TABLE_DATA WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM import.IMPORT_SXE_TABLE_DATA WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD IMPORT SXE CUSTOMER CPT-CRT DESC',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;




-------------------------------------------------------------------------------------
--   Update DATA VALUE TABLE FOR THE CUSTOMER PRICE TYPE AND THE CUSTOMER REBATE TYPE
--
-----------------------------------------------------------------------------------------

--customer price type
update epacube.DATA_VALUE
set DESCRIPTION =  (select top 1 import.IMPORT_SXE_TABLE_DATA.VALUE2 from import.IMPORT_SXE_TABLE_DATA 
where DATA_VALUE_ID = epacube.DATA_VALUE.DATA_VALUE_ID
and epacube.DATA_VALUE.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Customer Price type'
and import.Import_sxe_table_data.value2 is not null)
where epacube.data_value.DATA_NAME_FK = 244900
           
--customer rebate type     
update epacube.DATA_VALUE
set DESCRIPTION =  (select top 1 import.IMPORT_SXE_TABLE_DATA.VALUE2 from import.IMPORT_SXE_TABLE_DATA 
where DATA_VALUE_ID = epacube.DATA_VALUE.DATA_VALUE_ID
and epacube.DATA_VALUE.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Customer Rebate type'
and import.Import_sxe_table_data.value2 is not null)
where epacube.data_value.DATA_NAME_FK = 244901

--- product category 
update epacube.DATA_VALUE
set DESCRIPTION =  (select top 1 import.IMPORT_SXE_TABLE_DATA.VALUE2 from import.IMPORT_SXE_TABLE_DATA 
where DATA_VALUE_ID = epacube.DATA_VALUE.DATA_VALUE_ID
and epacube.DATA_VALUE.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Product Category'
and import.Import_sxe_table_data.value2 is not null)
where epacube.data_value.DATA_NAME_FK = 110906

--- product price type 
update epacube.DATA_VALUE
set DESCRIPTION =  (select top 1 import.IMPORT_SXE_TABLE_DATA.VALUE2 from import.IMPORT_SXE_TABLE_DATA 
where DATA_VALUE_ID = epacube.DATA_VALUE.DATA_VALUE_ID
and epacube.DATA_VALUE.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Product PriceType'
and import.Import_sxe_table_data.value2 is not null)
where epacube.data_value.DATA_NAME_FK = 211901

--- product line 
update epacube.DATA_VALUE
set DESCRIPTION =  (select top 1 import.IMPORT_SXE_TABLE_DATA.VALUE2 from import.IMPORT_SXE_TABLE_DATA 
where DATA_VALUE_ID = epacube.DATA_VALUE.DATA_VALUE_ID
and epacube.DATA_VALUE.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Product line'
and import.Import_sxe_table_data.value2 is not null)
where epacube.data_value.DATA_NAME_FK = 211902

--- Non Tax Reason
update epacube.DATA_VALUE
set DESCRIPTION =  (select top 1 import.IMPORT_SXE_TABLE_DATA.VALUE2 from import.IMPORT_SXE_TABLE_DATA 
where DATA_VALUE_ID = epacube.DATA_VALUE.DATA_VALUE_ID
and epacube.DATA_VALUE.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Non Tax Reason'
and import.Import_sxe_table_data.value2 is not null)
where epacube.data_value.DATA_NAME_FK = 211806
         
-------------------------------------------------------------------------------------
--   UPDATE IF EXIST THEN
--   INSERT THE VENDOR NAME IF NOT EXIST
-------------------------------------------------------------------------------------
 DECLARE 
               @Vendor_name varchar(100),
               @entity_structure_fk bigint                                          
               

	DECLARE cur_v_import CURSOR local for
		SELECT   import.IMPORT_SXE_TABLE_DATA.VALUE2, entity_structure_fk from import.IMPORT_SXE_TABLE_DATA  WITH (NOLOCK)
INNER JOIN epacube.ENTITY_IDENTIFICATION WITH (NOLOCK)
ON ( ENTITY_STRUCTURE_FK = epacube.ENTITY_IDENTIFICATION.ENTITY_STRUCTURE_FK
and epacube.ENTITY_IDENTIFICATION.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
and epacube.ENTITY_IDENTIFICATION.DATA_NAME_FK =143110 
and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'VENDOR')


         OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO @Vendor_name,
							@entity_structure_fk 


      WHILE @@FETCH_STATUS = 0
      BEGIN


		 
UPDATE [epacube].[ENTITY_IDENT_NONUNIQUE]
SET VALUE = @vendor_name
WHERE epacube.ENTITY_IDENT_NONUNIQUE.DATA_NAME_FK = 143112
and epacube.ENTITY_IDENT_NONUNIQUE.entity_structure_fk = @entity_structure_fk



 FETCH NEXT FROM cur_v_import Into @Vendor_name,
							 @entity_structure_fk


END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_import
DEALLOCATE cur_v_import    
 




INSERT INTO [epacube].[ENTITY_IDENT_NONUNIQUE]
           ([ENTITY_STRUCTURE_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[DATA_NAME_FK]
           ,[VALUE]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP])           
     (SELECT 
           ei.entity_structure_fk   --ENTITY_STRUCTURE_FK, bigint,>
           ,143000 --ENTITY_DATA_NAME_FK, int,>
           ,143112   --DATA_NAME_FK, int,>
           ,ISTD.VALUE2  --VALUE, varchar(128),>
           ,1 --RECORD_STATUS_CR_FK, int,>
           ,getdate()  --CREATE_TIMESTAMP
           FROM IMPORT.IMPORT_SXe_TABLE_DATA ISTD
           INNER JOIN Epacube.entity_identification ei
           on (ISTD.value1 = ei.value
           and ei.data_name_fk = 143110)
           AND ISTD.data_name = 'VENDOR'
             AND   NOT EXISTS
           ( SELECT 1
             FROM EPACUBE.ENTITY_IDENT_NONUNIQUE EIN WITH (NOLOCK)
             INNER JOIN IMPORT.IMPORT_SXe_TABLE_DATA ISTD
             on ( EIN.VALUE = ISTD.VALUE2
             AND ISTD.data_name = 'VENDOR'
             AND EIN.DATA_NAME_FK =  143112)))





 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD CUSTOMER CPT' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                            and  name = 'LOAD CUSTOMER CPT' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------


 SET @status_desc =  'finished execution of import.LOAD_SXE_CPT_CRT_DESC'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_SXE_CPT_CRT_DESC has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END




















