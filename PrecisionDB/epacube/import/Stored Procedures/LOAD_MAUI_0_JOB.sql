﻿--A_epaMAUI_LOAD_0_JOB

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [import].[LOAD_MAUI_0_JOB]
@AssessmentName varchar(128), @Job_FK_i Varchar(64), @Result_Type_CR_FK Varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @l_sysdate    datetime
DECLARE @ls_stmt      varchar (1000)
DECLARE @ls_queue     nvarchar (4000)
DECLARE @l_exec_no    bigint
DECLARE @l_rows_processed    BIGINT

DECLARE @v_batch_no   bigint
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(64)
DECLARE @v_data1      varchar(64)
DECLARE @v_data2      varchar(64)
DECLARE @v_data3      varchar(64)
DECLARE @v_job_user   varchar(64)
DECLARE @v_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

DECLARE @status_desc  varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

--BEGIN TRY
--SET NOCOUNT ON
--SET @l_sysdate = getdate ()


/**************************************************************************/
--   CREATE NEW JOB 
/**************************************************************************/
   
   EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'job_seq', @v_job_fk output
    
    SET @v_job_class_fk  = 481
    SET @v_job_user = 'MAUI'
    SET @v_job_name = 'MAUI Analysis'
    SET @v_job_date = GETDATE ()
    SET @v_data1   = @AssessmentName--- Gary put Name of the Job Here
    SET @v_data2   = @Job_FK_i 
    SET @v_data3   = @Result_Type_CR_FK 

EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, @V_DATA3, @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;


EXEC common.job_execution_create   @v_job_fk, 'START CALCULATION JOB', @v_input_rows, @v_output_rows, @v_exception_rows,200, @l_sysdate, @l_sysdate

END
