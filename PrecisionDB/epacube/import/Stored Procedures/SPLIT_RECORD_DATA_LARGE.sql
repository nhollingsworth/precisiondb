﻿



-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        07/20/2015   Initial Version 



CREATE PROCEDURE [import].[SPLIT_RECORD_DATA_LARGE]  ( @IN_JOB_FK bigint )
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to   
--
 
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of [IMPORT].[SPLIT_RECORD_DATA_LARGE]', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = NULL;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps




	 ------set value for counting -------
 update import.IMPORT_RECORD_DATA_LARGE
 set RESULT_TYPE =  '0'
 where JOB_FK = @in_job_fk

 ------------------------------------------------------------------------------------------------


Declare @File_size bigint
set @File_size = (select value from epacube.epacube_params where name = 'BREAK TO SIZE')


 
DECLARE @count_loop int
set @count_loop = (select count(1)/ @File_size from import.IMPORT_RECORD_DATA_LARGE where isNULL(Result_type,'0') = '0' and JOB_FK = @in_job_fk)




----
----drop temp table if it exists
	IF object_id('tempdb..#TS_LOOP_COUNT') is not null
	   drop table #TS_LOOP_COUNT;
	   
	   
	-- create temp table
	CREATE TABLE #TS_LOOP_COUNT(
	[BREAK_SIZE]	BIGINT,
	[COUNT_LOOP]  BIGINT)


insert into #TS_LOOP_COUNT (
[BREAK_SIZE],
[COUNT_LOOP])
select @File_size, @count_loop

--select * 
--from #TS_LOOP_COUNT

---------------------------------------------------------------------------------------------------
--create while loop
---------------------------------------------------------------------------------------------------

	  
	  DECLARE @v_Loop bigint
	 

		set @v_loop = (select COUNT_LOOP from #TS_LOOP_COUNT)
	
	 while EXISTS(select COUNT_LOOP from #TS_LOOP_COUNT)
	 	 begin

		 DECLARE @Value1 bigint
		DECLARE @Value2 bigint

set @Value1 = (
select top 1 IMPORT_RECORD_DATA_LARGE_ID from import.IMPORT_RECORD_DATA_LARGE where isNULL(Result_type,'0') = '0'  and JOB_FK = @in_job_fk
order by IMPORT_RECORD_DATA_LARGE_ID asc)


set @Value2 = (
select top 1 IMPORT_RECORD_DATA_LARGE_ID + @File_size  
from import.IMPORT_RECORD_DATA_LARGE where isNULL(Result_type,'0') = '0'  and JOB_FK = @in_job_fk
order by IMPORT_RECORD_DATA_LARGE_ID asc)


Update import.IMPORT_RECORD_DATA_LARGE
set RESULT_TYPE = ( (select max(RESULT_TYPE) from import.IMPORT_RECORD_DATA_LARGE where JOB_FK = @in_job_fk) + 1)
 where isNULL(Result_type,'0') ='0'
 and IMPORT_RECORD_DATA_LARGE_ID between @Value1 and @Value2

 -------------------------------------------------------------------------------------------
 
 update #TS_LOOP_COUNT
 set COUNT_LOOP =  (select count(1)/@File_size from import.IMPORT_RECORD_DATA_LARGE where isNULL(Result_type,'0') = '0' and JOB_FK = @in_job_fk)

 
 delete  from #TS_LOOP_COUNT 
				where  COUNT_LOOP = 0

				
--------------------------------------------------------------------------------------------
		
			
END

-------------------------------------------------------------------------
 
--drop temp table if it exists
	IF object_id('tempdb..#ts_loop_count') is not null
	   drop table #TS_LOOP_COUNT;
-----------------------------------------------------------------------	   
	   
	   



 SET @status_desc =  'finished execution of [IMPORT].[SPLIT_RECORD_DATA_LARGE]'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = NULL;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity bigINT;
	  DECLARE @ErrorState bigINT;

		SELECT 
			@ErrorMessage = 'Execution of [IMPORT].[SPLIT_RECORD_DATA_LARGE]' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = NULL;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END





































































































