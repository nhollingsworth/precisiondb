﻿













-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- KS        06/14/2010   Initial Version 


CREATE PROCEDURE [import].[LOAD_IMPORT_RULES_OPERANDS] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_IMPORT_RULES_OPERANDS', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps




-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( RULES_ERRORS TABLE )
--
--     SAVE EXAMPLES FOR LATER 
--
--     ERROR NAME:  INVALID OPERATION CODES
--	   ERROR NAME:  MISSING REQUIRED DATA FOR OPERATION REQUESTED
--	   EXAMPLES:  NULL VALUES; INVALID DATA NAMES; 
--
--     SET RECORD STATUS = PURGE FOR ALL RED CONDITIONS.... ???
--
--     YELLOW ERRORS  ( OPERATION SPECIFIC )
--
--     EXAMPLES:   ( OPERAND ) MULTIPLIER OUTSIDE OF RANGE
--
-------------------------------------------------------------------------------------





-------------------------------------------------------------------------------------
--   INSERT RULES_ACTION OPERATION 1
-------------------------------------------------------------------------------------

INSERT INTO [synchronizer].[RULES_ACTION_OPERANDS]
           ([RULES_ACTION_FK]
           ,[BASIS_POSITION]
           ,[RULES_FILTER_LOOKUP_OPERAND_FK]
           ,[OPERAND_FILTER_DN_FK]
           ,[OPERAND_FILTER_OPERATOR_CR_FK]
           ,[OPERAND1]
           ,[OPERAND2]
           ,[OPERAND3]
           ,[OPERAND4]
           ,[OPERAND5]
           ,[OPERAND6]
           ,[OPERAND7]
           ,[OPERAND8]
           ,[OPERAND9]
           ,[OPERAND10]
           ,[FILTER_VALUE1]
           ,[FILTER_VALUE2]
           ,[FILTER_VALUE3]
           ,[FILTER_VALUE4]
           ,[FILTER_VALUE5]
           ,[FILTER_VALUE6]
           ,[FILTER_VALUE7]
           ,[FILTER_VALUE8]
           ,[FILTER_VALUE9]
           ,[FILTER_VALUE10]
           ,[UPDATE_USER]
           ,[UPDATE_LOG_FK])
SELECT
            RA.RULES_ACTION_ID
           ,1
           ,(select rules_filter_lookup_operand_id from synchronizer.rules_filter_lookup_operand where name = IRD.rules_filter_lookup_operand_Name1)
           ,( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
              WHERE DNX.NAME = IRD.OPERAND_FILTER1_DN  )
           ,ISNULL (
            ( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
              WHERE CRX.CODE_TYPE = 'OPERATOR' AND CRX.CODE = IRD.OPERAND_FILTER1_OPERATOR )
            ,9000 )
           ,IRD.OPERAND_1_1 
           ,IRD.OPERAND_1_2 
           ,IRD.OPERAND_1_3 
           ,IRD.OPERAND_1_4 
           ,IRD.OPERAND_1_5 
           ,IRD.OPERAND_1_6 
           ,IRD.OPERAND_1_7 
           ,IRD.OPERAND_1_8 
           ,IRD.OPERAND_1_9 
           ,IRD.OPERAND_1_10 
           ,IRD.FILTER_VALUE_1_1 
           ,IRD.FILTER_VALUE_1_2 
           ,IRD.FILTER_VALUE_1_3 
           ,IRD.FILTER_VALUE_1_4 
           ,IRD.FILTER_VALUE_1_5 
           ,IRD.FILTER_VALUE_1_6 
           ,IRD.FILTER_VALUE_1_7 
           ,IRD.FILTER_VALUE_1_8 
           ,IRD.FILTER_VALUE_1_9 
           ,IRD.FILTER_VALUE_1_10           
           ,isnull(IR.[UPDATE_USER], 'EPACUBE')
           ,IR.IMPORT_RULES_ID
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_FK = IR.RULES_FK )  
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 54  
AND   IRD.OPERAND_FILTER1_DN IS NOT NULL



-------------------------------------------------------------------------------------
--   INSERT RULES_ACTION OPERATION 2
-------------------------------------------------------------------------------------

INSERT INTO [synchronizer].[RULES_ACTION_OPERANDS]
           ([RULES_ACTION_FK]
           ,[BASIS_POSITION]
           ,[RULES_FILTER_LOOKUP_OPERAND_FK]
           ,[OPERAND_FILTER_DN_FK]
           ,[OPERAND_FILTER_OPERATOR_CR_FK]
           ,[OPERAND1]
           ,[OPERAND2]
           ,[OPERAND3]
           ,[OPERAND4]
           ,[OPERAND5]
           ,[OPERAND6]
           ,[OPERAND7]
           ,[OPERAND8]
           ,[OPERAND9]
           ,[OPERAND10]
           ,[FILTER_VALUE1]
           ,[FILTER_VALUE2]
           ,[FILTER_VALUE3]
           ,[FILTER_VALUE4]
           ,[FILTER_VALUE5]
           ,[FILTER_VALUE6]
           ,[FILTER_VALUE7]
           ,[FILTER_VALUE8]
           ,[FILTER_VALUE9]
           ,[FILTER_VALUE10]
           ,[UPDATE_USER]
           ,[UPDATE_LOG_FK])
SELECT
            RA.RULES_ACTION_ID
           ,2
           ,(select rules_filter_lookup_operand_id from synchronizer.rules_filter_lookup_operand where name = IRD.rules_filter_lookup_operand_Name2)
           ,( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
              WHERE DNX.NAME = IRD.OPERAND_FILTER2_DN  )
           ,ISNULL (
            ( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
              WHERE CRX.CODE_TYPE = 'OPERATOR' AND CRX.CODE = IRD.OPERAND_FILTER1_OPERATOR )
            ,9000 )
           ,IRD.OPERAND_2_1 
           ,IRD.OPERAND_2_2 
           ,IRD.OPERAND_2_3 
           ,IRD.OPERAND_2_4 
           ,IRD.OPERAND_2_5 
           ,IRD.OPERAND_2_6 
           ,IRD.OPERAND_2_7 
           ,IRD.OPERAND_2_8 
           ,IRD.OPERAND_2_9 
           ,IRD.OPERAND_2_10 
           ,IRD.FILTER_VALUE_2_1 
           ,IRD.FILTER_VALUE_2_2 
           ,IRD.FILTER_VALUE_2_3 
           ,IRD.FILTER_VALUE_2_4 
           ,IRD.FILTER_VALUE_2_5 
           ,IRD.FILTER_VALUE_2_6 
           ,IRD.FILTER_VALUE_2_7 
           ,IRD.FILTER_VALUE_2_8 
           ,IRD.FILTER_VALUE_2_9 
           ,IRD.FILTER_VALUE_2_10 
           ,isnull(IR.[UPDATE_USER], 'EPACUBE')
           ,IR.IMPORT_RULES_ID
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_FK = IR.RULES_FK )  
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 54  
AND   IRD.OPERAND_FILTER2_DN IS NOT NULL


-------------------------------------------------------------------------------------
--   INSERT RULES_ACTION OPERATION 3
-------------------------------------------------------------------------------------

INSERT INTO [synchronizer].[RULES_ACTION_OPERANDS]
           ([RULES_ACTION_FK]
           ,[BASIS_POSITION]
           ,[RULES_FILTER_LOOKUP_OPERAND_FK]
           ,[OPERAND_FILTER_DN_FK]
           ,[OPERAND_FILTER_OPERATOR_CR_FK]
           ,[OPERAND1]
           ,[OPERAND2]
           ,[OPERAND3]
           ,[OPERAND4]
           ,[OPERAND5]
           ,[OPERAND6]
           ,[OPERAND7]
           ,[OPERAND8]
           ,[OPERAND9]
           ,[OPERAND10]
           ,[FILTER_VALUE1]
           ,[FILTER_VALUE2]
           ,[FILTER_VALUE3]
           ,[FILTER_VALUE4]
           ,[FILTER_VALUE5]
           ,[FILTER_VALUE6]
           ,[FILTER_VALUE7]
           ,[FILTER_VALUE8]
           ,[FILTER_VALUE9]
           ,[FILTER_VALUE10]
           ,[UPDATE_USER]
           ,[UPDATE_LOG_FK])
SELECT
            RA.RULES_ACTION_ID
           ,3
           ,(select rules_filter_lookup_operand_id from synchronizer.rules_filter_lookup_operand where name = IRD.rules_filter_lookup_operand_Name3)
           ,( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
              WHERE DNX.NAME = IRD.OPERAND_FILTER3_DN  )
           ,ISNULL (
            ( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
              WHERE CRX.CODE_TYPE = 'OPERATOR' AND CRX.CODE = IRD.OPERAND_FILTER3_OPERATOR )
            ,9000 )
           ,IRD.OPERAND_3_1 
           ,IRD.OPERAND_3_2 
           ,IRD.OPERAND_3_3 
           ,IRD.OPERAND_3_4 
           ,IRD.OPERAND_3_5 
           ,IRD.OPERAND_3_6 
           ,IRD.OPERAND_3_7 
           ,IRD.OPERAND_3_8 
           ,IRD.OPERAND_3_9 
           ,IRD.OPERAND_3_10 
           ,IRD.FILTER_VALUE_3_1 
           ,IRD.FILTER_VALUE_3_2 
           ,IRD.FILTER_VALUE_3_3 
           ,IRD.FILTER_VALUE_3_4 
           ,IRD.FILTER_VALUE_3_5 
           ,IRD.FILTER_VALUE_3_6 
           ,IRD.FILTER_VALUE_3_7 
           ,IRD.FILTER_VALUE_3_8 
           ,IRD.FILTER_VALUE_3_9 
           ,IRD.FILTER_VALUE_3_10 
           ,isnull(IR.[UPDATE_USER], 'EPACUBE')
           ,IR.IMPORT_RULES_ID
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_FK = IR.RULES_FK )  
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 54  
AND   IRD.OPERAND_FILTER3_DN IS NOT NULL




-------------------------------------------------------------------------------------
--   UPDATE EXISTING RULES ACTIONS OPERANDS 1 
-------------------------------------------------------------------------------------

UPDATE [synchronizer].[RULES_ACTION_OPERANDS]
   SET [OPERAND_FILTER_DN_FK]			= ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
												WHERE DNX.NAME = IRD.OPERAND_FILTER1_DN  )
      ,[OPERAND_FILTER_OPERATOR_CR_FK]	= ISNULL (
												( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
												  WHERE CRX.CODE_TYPE = 'OPERATOR' AND CRX.CODE = IRD.OPERAND_FILTER1_OPERATOR )
												,9000 )
	  ,[RULES_FILTER_LOOKUP_OPERAND_FK] = (select rules_filter_lookup_operand_id from synchronizer.rules_filter_lookup_operand where name = IRD.rules_filter_lookup_operand_Name1)
      ,[OPERAND1]						= IRD.OPERAND_1_1 
      ,[OPERAND2]						= IRD.OPERAND_1_2 
      ,[OPERAND3]						= IRD.OPERAND_1_3 
      ,[OPERAND4]						= IRD.OPERAND_1_4 
      ,[OPERAND5]						= IRD.OPERAND_1_5 
      ,[OPERAND6]						= IRD.OPERAND_1_6 
      ,[OPERAND7]						= IRD.OPERAND_1_7 
      ,[OPERAND8]						= IRD.OPERAND_1_8 
      ,[OPERAND9]						= IRD.OPERAND_1_9 
      ,[OPERAND10]						= IRD.OPERAND_1_10
      ,[FILTER_VALUE1]					= IRD.FILTER_VALUE_1_1 
      ,[FILTER_VALUE2]					= IRD.FILTER_VALUE_1_2
      ,[FILTER_VALUE3]					= IRD.FILTER_VALUE_1_3
      ,[FILTER_VALUE4]					= IRD.FILTER_VALUE_1_4
      ,[FILTER_VALUE5]					= IRD.FILTER_VALUE_1_5
      ,[FILTER_VALUE6]					= IRD.FILTER_VALUE_1_6
      ,[FILTER_VALUE7]					= IRD.FILTER_VALUE_1_7
      ,[FILTER_VALUE8]					= IRD.FILTER_VALUE_1_8
      ,[FILTER_VALUE9]					= IRD.FILTER_VALUE_1_9
      ,[FILTER_VALUE10]					= IRD.FILTER_VALUE_1_10
      ,[UPDATE_TIMESTAMP]				= GETDATE ()
      ,[UPDATE_USER]					= isnull(IR.[UPDATE_USER], 'EPACUBE')
      ,[UPDATE_LOG_FK]					= IR.IMPORT_RULES_ID
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_FK = IR.RULES_FK )  
INNER JOIN synchronizer.RULES_ACTION_OPERANDS RAO WITH (NOLOCK)
  ON ( RAO.RULES_ACTION_FK = RA.RULES_ACTION_ID )    
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 52 
AND   RAO.BASIS_POSITION = 1
AND   IRD.OPERAND_FILTER1_DN IS NOT NULL


-------------------------------------------------------------------------------------
--   UPDATE EXISTING RULES ACTIONS OPERANDS 2 
-------------------------------------------------------------------------------------

UPDATE [synchronizer].[RULES_ACTION_OPERANDS]
   SET [OPERAND_FILTER_DN_FK]			= ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
												WHERE DNX.NAME = IRD.OPERAND_FILTER2_DN  )
      ,[OPERAND_FILTER_OPERATOR_CR_FK]	= ISNULL (
												( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
												  WHERE CRX.CODE_TYPE = 'OPERATOR' AND CRX.CODE = IRD.OPERAND_FILTER2_OPERATOR )
												,9000 )
	  ,[RULES_FILTER_LOOKUP_OPERAND_FK] = (select rules_filter_lookup_operand_id from synchronizer.rules_filter_lookup_operand where name = IRD.rules_filter_lookup_operand_Name2)
      ,[OPERAND1]						= IRD.OPERAND_2_1 
      ,[OPERAND2]						= IRD.OPERAND_2_2 
      ,[OPERAND3]						= IRD.OPERAND_2_3 
      ,[OPERAND4]						= IRD.OPERAND_2_4 
      ,[OPERAND5]						= IRD.OPERAND_2_5 
      ,[OPERAND6]						= IRD.OPERAND_2_6 
      ,[OPERAND7]						= IRD.OPERAND_2_7 
      ,[OPERAND8]						= IRD.OPERAND_2_8 
      ,[OPERAND9]						= IRD.OPERAND_2_9 
      ,[OPERAND10]						= IRD.OPERAND_2_10
      ,[FILTER_VALUE1]					= IRD.FILTER_VALUE_2_1 
      ,[FILTER_VALUE2]					= IRD.FILTER_VALUE_2_2
      ,[FILTER_VALUE3]					= IRD.FILTER_VALUE_2_3
      ,[FILTER_VALUE4]					= IRD.FILTER_VALUE_2_4
      ,[FILTER_VALUE5]					= IRD.FILTER_VALUE_2_5
      ,[FILTER_VALUE6]					= IRD.FILTER_VALUE_2_6
      ,[FILTER_VALUE7]					= IRD.FILTER_VALUE_2_7
      ,[FILTER_VALUE8]					= IRD.FILTER_VALUE_2_8
      ,[FILTER_VALUE9]					= IRD.FILTER_VALUE_2_9
      ,[FILTER_VALUE10]					= IRD.FILTER_VALUE_2_10
      ,[UPDATE_TIMESTAMP]				= GETDATE ()
      ,[UPDATE_USER]					= isnull(IR.[UPDATE_USER], 'EPACUBE')
      ,[UPDATE_LOG_FK]					= IR.IMPORT_RULES_ID
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_FK = IR.RULES_FK )  
INNER JOIN synchronizer.RULES_ACTION_OPERANDS RAO WITH (NOLOCK)
  ON ( RAO.RULES_ACTION_FK = RA.RULES_ACTION_ID )    
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 52 
AND   RAO.BASIS_POSITION = 2
AND   IRD.OPERAND_FILTER2_DN IS NOT NULL


-------------------------------------------------------------------------------------
--   UPDATE EXISTING RULES ACTIONS OPERANDS 3 
-------------------------------------------------------------------------------------

UPDATE [synchronizer].[RULES_ACTION_OPERANDS]
   SET [OPERAND_FILTER_DN_FK]			= ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
												WHERE DNX.NAME = IRD.OPERAND_FILTER3_DN  )
      ,[OPERAND_FILTER_OPERATOR_CR_FK]	= ISNULL (
												( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
												  WHERE CRX.CODE_TYPE = 'OPERATOR' AND CRX.CODE = IRD.OPERAND_FILTER3_OPERATOR )
												,9000 )
	  ,[RULES_FILTER_LOOKUP_OPERAND_FK] = (select rules_filter_lookup_operand_id from synchronizer.rules_filter_lookup_operand where name = IRD.rules_filter_lookup_operand_Name3)
      ,[OPERAND1]						= IRD.OPERAND_3_1 
      ,[OPERAND2]						= IRD.OPERAND_3_2 
      ,[OPERAND3]						= IRD.OPERAND_3_3 
      ,[OPERAND4]						= IRD.OPERAND_3_4 
      ,[OPERAND5]						= IRD.OPERAND_3_5 
      ,[OPERAND6]						= IRD.OPERAND_3_6 
      ,[OPERAND7]						= IRD.OPERAND_3_7 
      ,[OPERAND8]						= IRD.OPERAND_3_8 
      ,[OPERAND9]						= IRD.OPERAND_3_9 
      ,[OPERAND10]						= IRD.OPERAND_3_10
      ,[FILTER_VALUE1]					= IRD.FILTER_VALUE_3_1 
      ,[FILTER_VALUE2]					= IRD.FILTER_VALUE_3_2
      ,[FILTER_VALUE3]					= IRD.FILTER_VALUE_3_3
      ,[FILTER_VALUE4]					= IRD.FILTER_VALUE_3_4
      ,[FILTER_VALUE5]					= IRD.FILTER_VALUE_3_5
      ,[FILTER_VALUE6]					= IRD.FILTER_VALUE_3_6
      ,[FILTER_VALUE7]					= IRD.FILTER_VALUE_3_7
      ,[FILTER_VALUE8]					= IRD.FILTER_VALUE_3_8
      ,[FILTER_VALUE9]					= IRD.FILTER_VALUE_3_9
      ,[FILTER_VALUE10]					= IRD.FILTER_VALUE_3_10
      ,[UPDATE_TIMESTAMP]				= GETDATE ()
      ,[UPDATE_USER]					= isnull(IR.[UPDATE_USER], 'EPACUBE')
      ,[UPDATE_LOG_FK]					= IR.IMPORT_RULES_ID
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_FK = IR.RULES_FK )  
INNER JOIN synchronizer.RULES_ACTION_OPERANDS RAO WITH (NOLOCK)
  ON ( RAO.RULES_ACTION_FK = RA.RULES_ACTION_ID )    
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 52 
AND   RAO.BASIS_POSITION = 3
AND   IRD.OPERAND_FILTER3_DN IS NOT NULL







 SET @status_desc =  'finished execution of import.LOAD_IMPORT_RULES_OPERANDS'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_IMPORT_RULES_OPERANDS has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END






















































