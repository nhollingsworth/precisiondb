﻿



-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Mark Schreiber
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- MS        09/04/2013   Initial Version 

CREATE PROCEDURE [import].[IMPORT_DESCRIPTION_CONVERSION_DATA] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint; 
	 DECLARE @v_max_row			bigint;
	 DECLARE @v_row_found		bigint;

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

	DECLARE @ErrorMessage		 nvarchar(4000);
	DECLARE @ErrorSeverity		 int;
	DECLARE @ErrorState			 int;

	DECLARE @buyline varchar(256);
	DECLARE @vendor varchar(256);
	DECLARE @data_name varchar(256);
	DECLARE @data_value1 varchar(256);
	DECLARE @data_value1_description varchar(256);
	DECLARE @part_format_dn varchar(256);
	DECLARE @data_value2 varchar(256);
	DECLARE @data_value2_description varchar(256);
	DECLARE @remove_special_char char (1);
	DECLARE @data_category varchar (256);
	DECLARE @display_seq bigint;

	DECLARE @lookup_data_value_fk bigint;
	DECLARE @vendor_entity_structure_fk bigint;
	DECLARE @data_name_fk bigint;
	DECLARE @entity_data_value1_fk bigint;
	DECLARE @part_format_dn_fk bigint;
	DECLARE @entity_data_value2_fk bigint;
	DECLARE @data_category_edv_fk bigint;

	DECLARE @data_category_dn_fk bigint;

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.IMPORT_DESCRIPTION_CONVERSION_DATA', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.DESCRIPTION_CONVERSION WITH (NOLOCK)
--       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM import.DESCRIPTION_CONVERSION WITH (NOLOCK)
--       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'UPDATE IMPORT DESCRIPTION CONVERSION DATA MAIN',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

	IF @v_input_rows = 0
		BEGIN
			SET @ErrorMessage = 'import.description_conversion table empty'
			GOTO RAISE_ERROR
		END

	
DECLARE desc_conv_data CURSOR FOR
 SELECT buyline, vendor, data_name, data_value1, data_value1_description, part_format_dn,
        data_value2, data_value2_description, remove_special_char, data_category, display_seq
 FROM   import.description_conversion
           
 OPEN desc_conv_data;
 FETCH NEXT FROM desc_conv_data 
	INTO @buyline, @vendor, @data_name, @data_value1, @data_value1_description, @part_format_dn,
		 @data_value2, @data_value2_description, @remove_special_char, @data_category, @display_seq
 WHILE @@FETCH_STATUS = 0
     BEGIN

--  lookup the buyline provided  (i.e. KOHLFIXT)
    
		SELECT top 1 @lookup_data_value_fk = data_value_id
		from epacube.data_value dv,
			epacube.product_category pc
		where pc.data_value_fk = dv.data_value_id
		and pc.data_name_fk = 
			(select distinct data_name_id from epacube.data_name 
			where name = 'ECL BUY LINE')
		and dv.value = @buyline

		SET @l_rows_processed = @@ROWCOUNT
		IF @l_rows_processed = 0
			BEGIN
				SET @ErrorMessage = 'Buyline ' + @buyline + ' on import not found on epacube.data_value table'
				GOTO RAISE_ERROR
			END

--  lookup the vendor provided  (i.e. KOHLER)

		SELECT top 1 @vendor_entity_structure_fk = entity_structure_fk
		from epacube.entity_identification ei
		where ei.entity_data_name_fk = 
			(select distinct data_name_id from epacube.data_name 
			where name = 'VENDOR')
		and ei.value = @vendor

		SET @l_rows_processed = @@ROWCOUNT
		IF @l_rows_processed = 0
			BEGIN
				SET @ErrorMessage = 'Vendor ' + @vendor + ' on import not found on epacube.entity_identification table'
				GOTO RAISE_ERROR
			END

--  lookup the main data name provided (i.e. ABBREVIATION, WORD CONVERSION, Model Number, etc)
 

		SELECT top 1 @data_name_fk = data_name_id
		from epacube.data_name dn
		where dn.name = @data_name

		SET @l_rows_processed = @@ROWCOUNT
		IF @l_rows_processed = 0
			BEGIN
				SET @ErrorMessage = 'Data Name ' + @data_name + ' on import not found on epacube.data_name table'
				GOTO RAISE_ERROR
			END

--  lookup the different entity data value 1 for the data names (i.e. HDL for ABBREVIATION)
DATA_VALUE1:
		SELECT top 1 @entity_data_value1_fk = entity_data_value_id
		from epacube.entity_data_value edv,
			epacube.data_name dn
		where edv.data_name_fk = dn.data_name_id
		and dn.name = @data_name
		and edv.value = @data_value1
		and edv.entity_structure_fk = @vendor_entity_structure_fk 

		SET @l_rows_processed = @@ROWCOUNT
		IF @l_rows_processed = 0
			BEGIN
--				SET @ErrorMessage = 'Data Value 1 ' + @data_value1 + ' on import not found on epacube.entity_data_value table'
				INSERT INTO [epacube].[ENTITY_DATA_VALUE]
				([VALUE]
				,[DATA_NAME_FK]
				,[ENTITY_STRUCTURE_FK]
				,[DESCRIPTION]
				,[JOB_FK]
				,[RECORD_STATUS_CR_FK]
				,[CREATE_TIMESTAMP]
				,[UPDATE_TIMESTAMP]
				,[UPDATE_USER])
			VALUES
				(@data_value1
				,@data_name_fk
				,@vendor_entity_structure_fk 
				,@data_value1_description
				,@in_job_fk
				,'1'
				,@V_START_TIMESTAMP
				,@V_START_TIMESTAMP
				,'DESC LOAD')

				GOTO DATA_VALUE1
			END

--  lookup part format data name if present

		IF @part_format_dn is not Null
			BEGIN
				SELECT top 1 @part_format_dn_fk = data_name_id
				from epacube.data_name dn
				where dn.name = @part_format_dn

				SET @l_rows_processed = @@ROWCOUNT
				IF @l_rows_processed = 0
					BEGIN
						SET @ErrorMessage = 'Part Format DN ' + @part_format_dn + ' on import not found on epacube.data_name table'
						GOTO RAISE_ERROR
					END
			END

--  lookup the different entity data value 2 for the data names

DATA_VALUE2:

		IF @data_value2 is not null
			BEGIN

			SELECT top 1 @entity_data_value2_fk = entity_data_value_id
			from epacube.entity_data_value edv,
				epacube.data_name dn
			where edv.data_name_fk = dn.data_name_id
			and dn.name = @data_name
			and edv.value = @data_value2
			and edv.entity_structure_fk = @vendor_entity_structure_fk 

			SET @l_rows_processed = @@ROWCOUNT
			IF @l_rows_processed = 0
				BEGIN
--					SET @ErrorMessage = 'Data Value 2 ' + @data_value2 + ' on import not found on epacube.entity_data_value table'
					INSERT INTO [epacube].[ENTITY_DATA_VALUE]
					([VALUE]
					,[DATA_NAME_FK]
					,[ENTITY_STRUCTURE_FK]
					,[DESCRIPTION]
					,[JOB_FK]
					,[RECORD_STATUS_CR_FK]
					,[CREATE_TIMESTAMP]
					,[UPDATE_TIMESTAMP]
					,[UPDATE_USER])
					VALUES
					(@data_value2
					,@data_name_fk
					,@vendor_entity_structure_fk 
					,@data_value2_description
					,@in_job_fk
					,'1'
					,@V_START_TIMESTAMP
					,@V_START_TIMESTAMP
					,'DESC LOAD')

					GOTO DATA_VALUE2
				END

			END

--  lookup the data category on the entity data value table

DATA_CATEGORY:

		IF @data_category is not null
			BEGIN
				SELECT top 1 @data_category_edv_fk = entity_data_value_id
				from epacube.entity_data_value edv,
					epacube.data_name dn
				where edv.data_name_fk = dn.data_name_id
				and dn.name = 'DATA CATEGORY'
				and edv.value = @data_category
				and edv.entity_structure_fk = @vendor_entity_structure_fk 

				SET @l_rows_processed = @@ROWCOUNT
				IF @l_rows_processed = 0
					BEGIN
						SELECT @data_category_dn_fk = data_name_id
						from epacube.data_name
						where name = 'DATA CATEGORY'

--						SET @ErrorMessage = 'Data Category ' + @data_category + ' on import not found on epacube.entity_data_value table'
						INSERT INTO [epacube].[ENTITY_DATA_VALUE]
						([VALUE]
						,[DATA_NAME_FK]
						,[ENTITY_STRUCTURE_FK]
						,[DESCRIPTION]
						,[JOB_FK]
						,[RECORD_STATUS_CR_FK]
						,[CREATE_TIMESTAMP]
						,[UPDATE_TIMESTAMP]
						,[UPDATE_USER])
						VALUES
						(@data_category
						,@data_category_dn_fk
						,@vendor_entity_structure_fk 
						,@data_category
						,@in_job_fk
						,'1'
						,@V_START_TIMESTAMP
						,@V_START_TIMESTAMP
						,'DESC LOAD')

						GOTO DATA_CATEGORY
					END

			END

--  IF no ERRORs INSERT into epacube.Description_conversion table

		SELECT @v_max_row = MAX(DESCRIPTION_CONVERSION_ID) + 1
        FROM epacube.DESCRIPTION_CONVERSION WITH (NOLOCK)

		IF @v_max_row is null 
           BEGIN
			SET @v_max_row = 1
		   END

		SELECT @v_row_found = COUNT(1)
        FROM [epacube].[DESCRIPTION_CONVERSION] WITH (NOLOCK)
		where LOOKUP_DATA_VALUE_FK = @LOOKUP_DATA_VALUE_FK
        and   VENDOR_ENTITY_STRUCTURE_FK = @VENDOR_ENTITY_STRUCTURE_FK
        and   DATA_NAME_FK = @DATA_NAME_FK
        and   ENTITY_DATA_VALUE1_FK = @ENTITY_DATA_VALUE1_FK
        and   (PART_FORMAT_DN_FK = @PART_FORMAT_DN_FK
		 or    PART_FORMAT_DN_FK IS NULL)
        and   (ENTITY_DATA_VALUE2_FK = @ENTITY_DATA_VALUE2_FK
		 or    ENTITY_DATA_VALUE2_FK IS NULL)
        and   REMOVE_SPECIAL_CHAR = @REMOVE_SPECIAL_CHAR
        and   (DATA_CATEGORY_EDV_FK = @DATA_CATEGORY_EDV_FK
		 or	   DATA_CATEGORY_EDV_FK IS NULL)

		IF @v_row_found = 0
			BEGIN

				INSERT INTO [epacube].[DESCRIPTION_CONVERSION]
--				([DESCRIPTION_CONVERSION_ID]
				 ([LOOKUP_DATA_VALUE_FK]
				,[VENDOR_ENTITY_STRUCTURE_FK]
				,[DATA_NAME_FK]
				,[ENTITY_DATA_VALUE1_FK]
				,[PART_FORMAT_DN_FK]
				,[ENTITY_DATA_VALUE2_FK]
				,[REMOVE_SPECIAL_CHAR]
				,[DATA_CATEGORY_EDV_FK]
				,[DISPLAY_SEQ]
				,[RECORD_STATUS_CR_FK]
				,[CREATE_TIMESTAMP]
				,[UPDATE_TIMESTAMP]
				,[UPDATE_USER])
				VALUES
--				 (@v_max_row
				(@LOOKUP_DATA_VALUE_FK
				,@VENDOR_ENTITY_STRUCTURE_FK
				,@DATA_NAME_FK
				,@ENTITY_DATA_VALUE1_FK
				,@PART_FORMAT_DN_FK
				,@ENTITY_DATA_VALUE2_FK
				,@REMOVE_SPECIAL_CHAR
				,@DATA_CATEGORY_EDV_FK
				,@DISPLAY_SEQ
--				,@v_row_found
				,'1'
				,@V_START_TIMESTAMP
				,@V_START_TIMESTAMP
				,'DESC LOAD')

			END

--  FETCH next record in the import table

		  FETCH NEXT FROM desc_conv_data 
			INTO @buyline, @vendor, @data_name, @data_value1, @data_value1_description, @part_format_dn,
				@data_value2, @data_value2_description, @remove_special_char, @data_category, @display_seq
             
     END

 CLOSE desc_conv_data;
 DEALLOCATE desc_conv_data;

-------------------------------------------------------------------------------------


 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT DESCRIPTION CONVERSION DATA MAIN' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT DESCRIPTION CONVERSION DATA MAIN' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
    END


 SET @status_desc =  'finished execution of import.IMPORT_DESCRIPTION_CONVERSION DATA MAIN'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of import.DESCRIPTION CONVERSION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END





















