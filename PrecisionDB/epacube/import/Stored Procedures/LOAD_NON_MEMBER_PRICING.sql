﻿





-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        06/20/2017   Initial Version 


CREATE PROCEDURE [import].[LOAD_NON_MEMBER_PRICING] ( @IN_JOB_FK bigint ) 
AS BEGIN 



     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.LOAD_NON_MEMBER_PRICING', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
  NS pulls non member pricing 
  -clover loads data and calls this procedure
  populate product structure and cust structure
  Populate synchronizer.event_calc
  Call Calc engine - possibly call it again to get more answers of results
  Send results back to NS  
  Move results to archive table  -- Create View  over this table 
   

-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

-------------------------------------------------------------------------------------
--truncate table [import].[NON_MEMBER_PRICING]



 --select * from [import].[NON_MEMBER_PRICING]


   -----creating test data that I hope to get from Netsuite

 --INSERT INTO [import].[NON_MEMBER_PRICING]
 --          ([NS_RECORD_ID]
 --          ,[NS_CUSTOMER_ID]
 --          ,[NS_ITEM_ID]
 --          ,[QTY_ORDERED]
 --          ,[PRICING_DATE]
 --          ,[PREPRICE_OVERRIDE_TYPE]
 --          ,[PRICE_OVERRIDE_AMOUNT]
 --          ,[ALLOWANCE_OVERRIDE_AMOUNT]
 --          ,[CREATE_TIMESTAMP]
 --          ,[CREATE_USER]
 --          )
 --select top 100 1, ei.value, pi.value, 5, getdate(), 'A', '3.00', '4.00', getdate(), 'NS' from epacube.product_association pa 
 --inner join epacube.product_identification pi on pa.product_structure_fk = pi.product_structure_fk  and pi.data_name_fk = 110100
 --inner join epacube.ENTITY_IDENTIFICATION ei on ei.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK and ei.entity_DATA_NAME_FK = 144020
 --and pa.data_name_fk = 159905


-- INSERT INTO [import].[NON_MEMBER_PRICING]
--           ([NS_RECORD_ID]
--           ,[NS_CUSTOMER_ID]
--           ,[NS_ITEM_ID]
--           ,[QTY_ORDERED]
--           ,[PRICING_DATE]
--           ,[PREPRICE_OVERRIDE_TYPE]
--           ,[PRICE_OVERRIDE_AMOUNT]
--           ,[ALLOWANCE_OVERRIDE_AMOUNT]
--           ,[CREATE_TIMESTAMP]
--           ,[CREATE_USER]
--           )
-- select top 2 1, ei.value, '20310W', 5, '6/23/2017', 'T','0','0', '6/23/2017','CV'
-- from epacube.segments_customer  sc 
-- --inner join epacube.product_identification pi on pa.product_structure_fk = pi.product_structure_fk  and pi.data_name_fk = 110100
-- inner join epacube.ENTITY_IDENTIFICATION ei on ei.ENTITY_STRUCTURE_FK = sc.cust_ENTITY_STRUCTURE_FK and ei.entity_DATA_NAME_FK = 144020
--where data_value_fk = 1000058618



    ---
  ----------------------------------------------------------

  ---First get cust entity structures and product structure

    update B
    set CUST_ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
	from [import].[NON_MEMBER_PRICING]  B
   inner join epacube.entity_identification ei on 
   b.NS_CUSTOMER_ID = ei.value
   and ei.data_name_fk = 144111  
   and b.JOB_FK = @IN_JOB_FK


   UPDATE B
set product_structure_fk = pi.PRODUCT_STRUCTURE_FK
from [import].[NON_MEMBER_PRICING]  B
inner join epacube.PRODUCT_IDENTIFICATION pi on
b.ns_item_id = pi.value and pi.data_name_fk = 110100
and b.JOB_FK = @IN_JOB_FK

-----------------------------------------------------------------------------------------------
--non member customers are any coustomer in cust segment table where the data value fk   = 
--1000058617
--1000058618

--select * from epacube.segments_customer where data_value_fk = 1000058618

--select * from epacube.data_name where data_name_id between 502041 and 502045
--select * from epacube.data_value where data_name_fk between 502041 and 502043

--502043	30	NON-MEMBERS
--502043	60	FOOD SERVICE

--price override type = o or T

--select * from epacube.code_ref where code_type = 'RESULT_type'




INSERT INTO [synchronizer].[EVENT_CALC]
           ([JOB_FK]
           ,[RESULT_DATA_NAME_FK]  
           ,[RESULT_TYPE_CR_FK]    
           ,[RESULT_EFFECTIVE_DATE]  
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[CUST_ENTITY_STRUCTURE_FK]
           ,[SOURCE_FK]        
		   )
    
 select		job_fk,
			503201 --[RESULT_DATA_NAME_FK]
           ,710  --CURRENT -- [RESULT_TYPE_CR_FK]
           ,PRICING_DATE ---[RESULT_EFFECTIVE_DATE] 
		   ,PRODUCT_STRUCTURE_FK
		   ,1
		   ,CUST_ENTITY_STRUCTURE_FK
		   ,NON_MEMBER_PRICING_ID
  from [import].[NON_MEMBER_PRICING]
  WHERE PRODUCT_STRUCTURE_FK is Not NULL
  AND CUST_ENTITY_STRUCTURE_FK is Not NULL
  AND JOB_FK = @in_job_fk

-----------------------------------------------------------------------------------------------
---CALL CALC
--------------------------------------------------------------------------------------------------

EXECUTE   [synchronizer].[CALC_RULES_URM_NON_MEMBER]
			 @in_job_fk

-------------------------------------------------------------------------------------------------
-----SECTION TO HAND BACK ANSWERS TO NS/CLOVER
--------------------------------------------------------------------------------------------------

--select IM.NS_RECORD_ID,   IM.NS_CUSTOMER_ID, IM.NS_ITEM_ID, IM.QTY_ORDERED, IM.PRICING_DATE, EC.RESULT, EC.PRICING_COST_BASIS_AMT, EC.MARGIN_COST_BASIS_AMT, EC.REBATE_CB_AMT FROM [synchronizer].[EVENT_CALC] EC
--inner join import.NON_MEMBER_PRICING IM 
-- on EC.source_FK = IM.NON_MEMBER_PRICING_ID
-- and ec.JOB_FK = im.JOB_FK
--And Ec.job_fk = @IN_JOB_FK

 --select * from import.NON_MEMBER_PRICING NMP
--select * from synchronizer.EVENT_CALC




----MOVE RESULTS TO TABLE?
INSERT INTO [synchronizer].[EVENT_CALC_ARCHIVE]
           ([EVENT_FK]
           ,[JOB_FK]
           ,[RESULT_DATA_NAME_FK]
           ,[RESULT_TYPE_CR_FK]
           ,[RESULT_EFFECTIVE_DATE]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[CUST_ENTITY_STRUCTURE_FK]
           ,[SUPL_ENTITY_STRUCTURE_FK]
           ,[RESULT]
           ,[RULES_FK]
           ,[RULE_TYPE_CR_FK]
           ,[CALC_GROUP_SEQ]
           ,[PARITY_DV_FK]
           ,[MODEL_SALES_QTY]
           ,[VALUE_UOM_CODE_FK]
           ,[PRICING_COST_BASIS_AMT]
           ,[MARGIN_COST_BASIS_AMT]
           ,[ORDER_COGS_AMT]
           ,[REBATED_COST_CB_AMT]
           ,[REBATE_CB_AMT]
           ,[REBATE_BUY_AMT]
           ,[SELL_PRICE_CUST_AMT]
           ,[MARGIN_AMT]
           ,[MARGIN_PCT]
           ,[RELATED_EVENT_FK]
           ,[RESULT_PARENT_DATA_NAME_FK]
           ,[COLUMN_NAME]
           ,[BATCH_NO]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_CONDITION_CR_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
           ,[UPDATE_LOG_FK]
           ,[EFF_END_DATE]
           ,[Product_Structure_FK_Subbed_From]
           ,[Product_Structure_FK_Subbed_To]
           ,[SPC_UOM]
		   ,SOURCE_FK)
SELECT [EVENT_ID]
      ,[JOB_FK]
      ,[RESULT_DATA_NAME_FK]
      ,[RESULT_TYPE_CR_FK]
      ,[RESULT_EFFECTIVE_DATE]
      ,[PRODUCT_STRUCTURE_FK]
      ,[ORG_ENTITY_STRUCTURE_FK]
      ,[CUST_ENTITY_STRUCTURE_FK]
      ,[SUPL_ENTITY_STRUCTURE_FK]
      ,[RESULT]
      ,[RULES_FK]
      ,[RULE_TYPE_CR_FK]
      ,[CALC_GROUP_SEQ]
      ,[PARITY_DV_FK]
      ,[MODEL_SALES_QTY]
      ,[VALUE_UOM_CODE_FK]
      ,[PRICING_COST_BASIS_AMT]
      ,[MARGIN_COST_BASIS_AMT]
      ,[ORDER_COGS_AMT]
      ,[REBATED_COST_CB_AMT]
      ,[REBATE_CB_AMT]
      ,[REBATE_BUY_AMT]
      ,[SELL_PRICE_CUST_AMT]
      ,[MARGIN_AMT]
      ,[MARGIN_PCT]
      ,[RELATED_EVENT_FK]
      ,[RESULT_PARENT_DATA_NAME_FK]
      ,[COLUMN_NAME]
      ,[BATCH_NO]
      ,[EVENT_SOURCE_CR_FK]
      ,[EVENT_CONDITION_CR_FK]
      ,[RECORD_STATUS_CR_FK]
      ,[UPDATE_TIMESTAMP]
      ,[UPDATE_USER]
      ,[UPDATE_LOG_FK]
      ,[EFF_END_DATE]
      ,[Product_Structure_FK_Subbed_From]
      ,[Product_Structure_FK_Subbed_To]
      ,[SPC_UOM]
	  ,SOURCE_FK
  FROM [synchronizer].[EVENT_CALC]
  where job_fk = @in_job_fk



--------------------------------------------------------------------------------------------------

      SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'IMPORT_COMPLETE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



 SET @status_desc =  'finished execution of IMPORT.LOAD_NON_MEMBER_PRICING'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT.LOAD_NON_MEMBER_PRICING has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END





