﻿















-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        06/06/2011   Initial Version 
-- CV        07/23/2011   Rework with new data names added
-- CV        09/22/2011   Clean up logging comments
-- CV        10/15/2011   added data name to org code lookup

CREATE PROCEDURE [import].[LOAD_SXE_HIST_PRICESHEETS] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_HIST_PRICESHEETS',
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.IMPORT_SXE_HISTORICAL_PRICESHEETS WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM import.IMPORT_SXE_HISTORICAL_PRICESHEETS WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD IMPORT SXE HISTORICAL PRICESHEETS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


----TO discuss ...  getting the product structure...

-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( JOB_ERRORS TABLE )
--
--     THESE ERRORS PROHIBIT THE ROWS FROM BEING IMPORTED
--
--	   ERROR NAME:  INCOMPLETE OR INVALID DATA PROHIBITING IMPORT 
--     EXAMPLE IS MISSING OR INVALID STRUCTURE
--
--     IMPORT ROW MARKED  ERROR_ON_IMPORT_IND = 1 
--
-----------------------------------------------------------------------------------------
--select * from import.IMPORT_SXE_HISTORICAL_PRICESHEETS with (nolock) where PRODUCT_STRUCTURE_FK is not null
--------select * from marginmgr.pricesheet
----select * from epacube.data_name where DATA_NAME_ID =110100
----select distinct data_name_fk from epacube.product_identification





---first check by sx id---
UPDATE import.IMPORT_SXE_HISTORICAL_PRICESHEETS
SET PRODUCT_STRUCTURE_FK = (select pi.product_Structure_Fk
FROM epacube.product_identification pi
where import.IMPORT_SXE_HISTORICAL_PRICESHEETS.Product_ID_Value = pi.value
and pi.data_name_FK = 110100)
,IMPORT_PACKAGE_FK = 110100
WHERE 1 = 1   
AND PRODUCT_STRUCTURE_FK IS NULL                
AND   JOB_FK = @IN_JOB_FK 



---Then check by UPC

UPDATE import.IMPORT_SXE_HISTORICAL_PRICESHEETS
SET PRODUCT_STRUCTURE_FK = (select pi.product_Structure_Fk
FROM epacube.product_identification pi
where import.IMPORT_SXE_HISTORICAL_PRICESHEETS.UPC = pi.value
and pi.data_name_FK = 110102)
,IMPORT_PACKAGE_FK = 110102
WHERE 1 = 1 
AND PRODUCT_STRUCTURE_FK IS NULL                    
AND   JOB_FK = @IN_JOB_FK 

---------------

--default Org to All Whse if Null

UPDATE import.IMPORT_SXE_HISTORICAL_PRICESHEETS
SET ORG_ID_VALUE = ltrim(rtrim(ORG_ID_VALUE))

UPDATE import.IMPORT_SXE_HISTORICAL_PRICESHEETS
SET ORG_ID_VALUE = 'All Whse'
where ORG_ID_VALUE = '' 

 
UPDATE import.IMPORT_SXE_HISTORICAL_PRICESHEETS
SET ORG_ENTITY_STRUCTURE_FK =  (select ei.entity_Structure_Fk
FROM epacube.entity_identification ei
where import.IMPORT_SXE_HISTORICAL_PRICESHEETS.ORG_ID_VALUE = ei.value
and ei.DATA_NAME_FK = 141111
)
WHERE 1 = 1                   
AND   JOB_FK  = @IN_JOB_FK 

UPDATE import.IMPORT_SXE_HISTORICAL_PRICESHEETS
SET MFR_ENTITY_STRUCTURE_FK = (select ei.Entity_Structure_Fk
FROM epacube.ENtity_identification ei
where import.IMPORT_SXE_HISTORICAL_PRICESHEETS.MFR_VALUE_ID = ei.value
and ei.data_name_FK = 143110)
WHERE 1 = 1                   
AND   JOB_FK = @IN_JOB_FK

------ third by mfr id
UPDATE import.IMPORT_SXE_HISTORICAL_PRICESHEETS
SET PRODUCT_STRUCTURE_FK = (select pi.product_Structure_Fk
FROM epacube.product_identification pi
where import.IMPORT_SXE_HISTORICAL_PRICESHEETS.MFR_CAT_NO = pi.value
AND import.IMPORT_SXE_HISTORICAL_PRICESHEETS.MFR_ENTITY_STRUCTURE_FK = pi.ENTITY_STRUCTURE_FK
and pi.data_name_FK = 110111)
,IMPORT_PACKAGE_FK = 110111
WHERE 1 = 1
AND PRODUCT_STRUCTURE_FK IS NULL                     
AND   JOB_FK = @IN_JOB_FK 


UPDATE import.IMPORT_SXE_HISTORICAL_PRICESHEETS
SET CUST_ENTITY_STRUCTURE_FK = (select ei.Entity_Structure_Fk
FROM epacube.ENtity_identification ei
where import.IMPORT_SXE_HISTORICAL_PRICESHEETS.CUST_ID_VALUE = ei.value
and ei.data_name_FK = 144111)
WHERE 1 = 1                   
AND   JOB_FK = @IN_JOB_FK



update import.IMPORT_SXE_HISTORICAL_PRICESHEETS
set error_on_import_ind = 1
where product_Structure_Fk is  NULL
and job_fk = @in_job_Fk


update import.IMPORT_SXE_HISTORICAL_PRICESHEETS
set error_on_import_ind = 1
where org_entity_Structure_Fk is  NULL
and job_fk = @in_job_Fk



INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[TABLE_PK]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[ERROR_DATA4]
--           ,[ERROR_DATA5]
--           ,[ERROR_DATA6]
           ,[CREATE_TIMESTAMP])
SELECT 
JOB_FK
,'IMPORT HISTORICAL PRICESHEETS'
,NULL
,99
,'MISSING PRODUCT/ORG INFORMATION'
,Product_ID_Value
,ORG_ID_Value
,effective_date
,NULL
,getdate()
FROM import.IMPORT_SXE_HISTORICAL_PRICESHEETS
WHERE ISNULL(ERROR_ON_IMPORT_IND ,0) = 1
AND PRODUCT_STRUCTURE_FK is NULL


-------------------------------------------------------------------------------------
--   MATCH PRODUCTS
--		1st 6 net values
-------------------------------------------------------------------------------------

TRUNCATE TABLE [marginmgr].[PRICESHEET]


INSERT INTO [marginmgr].[PRICESHEET]
           ([PRICESHEET_NAME]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[RESULT_TYPE_CR_FK]
           ,[NET_VALUE1]
           ,[NET_VALUE2]
           ,[NET_VALUE3]
           ,[NET_VALUE4]
           ,[NET_VALUE5]
           ,[NET_VALUE6]
           ,[PRICESHEET_QTY_PER]
           --,[VALUE_UOM_CODE_FK]
           --,[UOM_CODE]
           ,[PROD_ID_DN]
           ,[PROD_ID_VALUE]
           ,[ORG_ID_ENTITY_DN]
           ,[ORG_ID_DN]
           ,[ORG_ID_VALUE]
           ,[ENTITY_ID_ENTITY_DN]
           ,[ENTITY_ID_DN]
           ,[ENTITY_ID_VALUE]
           ,[CURRENCY_CR_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
(SELECT 
ISHP.SHEET_NAME -- price sheet name
,232100  --	 PRICESHEET HISTORY
,ISHP.PRODUCT_STRUCTURE_FK
,ISNULL (ISHP.ORG_ENTITY_STRUCTURE_FK , 1)--         ,[ORG_ENTITY_STRUCTURE_FK]
,ISHP.MFR_ENTITY_STRUCTURE_FK  --         ,[ENTITY_STRUCTURE_FK]
,ISHP.Effective_Date  --,[EFFECTIVE_DATE]
,NULL --,[END_DATE]
,713 --historical? --,[RESULT_TYPE_CR_FK]
           ,ISHP.NET_VALUE1  --,[NET_VALUE1]
           ,ISHP.NET_VALUE2    --[NET_VALUE2]
           ,ISHP.NET_VALUE3  --[NET_VALUE3]
           ,ISHP.NET_VALUE4  --[NET_VALUE4]
           ,ISHP.NET_VALUE5     --[NET_VALUE5]
           ,ISHP.NET_VALUE6   --  [NET_VALUE6]
           ,NULL  ---[PRICESHEET_QTY_PER]
   --        ,(select UOM_CODE_ID FROM epacube.uom_code with (NOLOCK)
			--Where uom_code = IEPH.PRICEUOM)    ---[VALUE_UOM_CODE_FK]
           --,IEPH.PRICEUOM   --[UOM_CODE]
           ,(select name from epacube.DATA_NAME where ISHP.IMPORT_PACKAGE_FK = DATA_NAME_ID) --[PROD_ID_DN]
           ,(case when ISHP.import_package_fk =110102 then ISHP.UPC
           when ISHP.IMPORT_package_fk = 110111 then ISHP.MFR_CAT_NO else ISHP.PRODUCT_ID_VALUE END )  ---[PROD_ID_VALUE]
           ,'WAREHOUSE'--[ORG_ID_ENTITY_DN]
           ,'WAREHOUSE CODE'  --[ORG_ID_DN]
           ,(case when ishp.org_id_value = ' ' then 'ALL WHSE' Else ISNULL(ISHP.ORG_ID_VALUE, 'ALL WHSE') end)
           ,'VENDOR'  --[ENTITY_ID_ENTITY_DN]
           ,'VENDOR NUMBER'  --[ENTITY_ID_DN]
           ,ISHP.MFR_VALUE_ID ---[ENTITY_ID_VALUE]
           ,1200   --USD  --[CURRENCY_CR_FK]
           ,1
           ,getdate()
           ,NULL
           ,NULL
 FROM import.IMPORT_SXE_HISTORICAL_PRICESHEETS ISHP
WHERE 1 = 1 
AND SHEET_CLASS = 'MFR HISTORICAL SHEET'
AND ISNULL(ERROR_ON_IMPORT_IND,0) <> 1
AND ISHP.JOB_FK = @IN_JOB_FK)


------------------------------------------------------------------------------------------
---cust historical sheets
--------------------------------------------------------------------------

INSERT INTO [marginmgr].[PRICESHEET]
           ([PRICESHEET_NAME]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[RESULT_TYPE_CR_FK]
           ,[NET_VALUE1]
           ,[NET_VALUE2]
           ,[NET_VALUE3]
           ,[NET_VALUE4]
           ,[NET_VALUE5]
           ,[NET_VALUE6]
           ,[PRICESHEET_QTY_PER]
           --,[VALUE_UOM_CODE_FK]
           --,[UOM_CODE]
           ,[PROD_ID_DN]
           ,[PROD_ID_VALUE]
           ,[ORG_ID_ENTITY_DN]
           ,[ORG_ID_DN]
           ,[ORG_ID_VALUE]
           ,[ENTITY_ID_ENTITY_DN]
           ,[ENTITY_ID_DN]
           ,[ENTITY_ID_VALUE]
           ,[CURRENCY_CR_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
(SELECT 
ISHP.SHEET_NAME -- price sheet name
,232200  --	 PRICESHEET HISTORY
,ISHP.PRODUCT_STRUCTURE_FK
,ISNULL (ISHP.ORG_ENTITY_STRUCTURE_FK , 1)--         ,[ORG_ENTITY_STRUCTURE_FK]
,ISHP.MFR_ENTITY_STRUCTURE_FK  --         ,[ENTITY_STRUCTURE_FK]
,ISHP.Effective_Date  --,[EFFECTIVE_DATE]
,NULL --,[END_DATE]
,713 --historical? --,[RESULT_TYPE_CR_FK]
           ,ISHP.NET_VALUE1  --,[NET_VALUE1]
           ,ISHP.NET_VALUE2    --[NET_VALUE2]
           ,ISHP.NET_VALUE3  --[NET_VALUE3]
           ,ISHP.NET_VALUE4  --[NET_VALUE4]
           ,ISHP.NET_VALUE5     --[NET_VALUE5]
           ,ISHP.NET_VALUE6   --  [NET_VALUE6]
           ,NULL  ---[PRICESHEET_QTY_PER]
   --        ,(select UOM_CODE_ID FROM epacube.uom_code with (NOLOCK)
			--Where uom_code = IEPH.PRICEUOM)    ---[VALUE_UOM_CODE_FK]
           --,IEPH.PRICEUOM   --[UOM_CODE]
           ,(select name from epacube.DATA_NAME where ISHP.IMPORT_PACKAGE_FK = DATA_NAME_ID) --[PROD_ID_DN]
           ,(case when ISHP.import_package_fk =110102 then ISHP.UPC
           when ISHP.IMPORT_package_fk = 110111 then ISHP.MFR_CAT_NO else ISHP.PRODUCT_ID_VALUE END )  ---[PROD_ID_VALUE]
           ,'WAREHOUSE'--[ORG_ID_ENTITY_DN]
           ,'WAREHOUSE CODE'  --[ORG_ID_DN]
           ,(case when ishp.org_id_value = ' ' then 'ALL WHSE' Else ISNULL(ISHP.ORG_ID_VALUE, 'ALL WHSE') end)
           ,'VENDOR'  --[ENTITY_ID_ENTITY_DN]
           ,'VENDOR NUMBER'  --[ENTITY_ID_DN]
           ,ISHP.MFR_VALUE_ID ---[ENTITY_ID_VALUE]
           ,1200   --USD  --[CURRENCY_CR_FK]
           ,1
           ,getdate()
           ,NULL
           ,NULL
 FROM import.IMPORT_SXE_HISTORICAL_PRICESHEETS ISHP
WHERE 1 = 1 
AND SHEET_CLASS = 'CUST HISTORICAL SHEET'
AND isnull(net_value1,0) + isnull(net_value2,0)
+ isnull(net_value3,0) + isnull(net_value4,0) 
+ isnull(net_value5,0) + isnull(net_value6,0)> 0    ----total values are greater then 0
AND ISNULL(ERROR_ON_IMPORT_IND,0) <> 1
AND ISHP.JOB_FK = @IN_JOB_FK)




INSERT INTO [marginmgr].[PRICESHEET]
           ([PRICESHEET_NAME]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[RESULT_TYPE_CR_FK]
           ,[NET_VALUE1]
           ,[NET_VALUE2]
           ,[NET_VALUE3]
           ,[NET_VALUE4]
           ,[NET_VALUE5]
           ,[NET_VALUE6]
           ,[PRICESHEET_QTY_PER]
           --,[VALUE_UOM_CODE_FK]
           --,[UOM_CODE]
           ,[PROD_ID_DN]
           ,[PROD_ID_VALUE]
           ,[ORG_ID_ENTITY_DN]
           ,[ORG_ID_DN]
           ,[ORG_ID_VALUE]
           ,[ENTITY_ID_ENTITY_DN]
           ,[ENTITY_ID_DN]
           ,[ENTITY_ID_VALUE]
           ,[CURRENCY_CR_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
(SELECT 
ISHP.SHEET_NAME -- price sheet name
,232200  --	 PRICESHEET HISTORY
,ISHP.PRODUCT_STRUCTURE_FK
,ISNULL (ISHP.ORG_ENTITY_STRUCTURE_FK , 1)--         ,[ORG_ENTITY_STRUCTURE_FK]
,ISHP.MFR_ENTITY_STRUCTURE_FK  --         ,[ENTITY_STRUCTURE_FK]
,ISHP.Effective_Date  --,[EFFECTIVE_DATE]
,NULL --,[END_DATE]
,713 --historical? --,[RESULT_TYPE_CR_FK]
           ,ISHP.NET_VALUE7  --,[NET_VALUE1]
           ,ISHP.NET_VALUE8    --[NET_VALUE2]
           ,ISHP.NET_VALUE9  --[NET_VALUE3]
           ,NULL  --[NET_VALUE4]
           ,NULL     --[NET_VALUE5]
           ,NULL   --  [NET_VALUE6]
           ,NULL  ---[PRICESHEET_QTY_PER]
   --        ,(select UOM_CODE_ID FROM epacube.uom_code with (NOLOCK)
			--Where uom_code = IEPH.PRICEUOM)    ---[VALUE_UOM_CODE_FK]
           --,IEPH.PRICEUOM   --[UOM_CODE]
           ,(select name from epacube.DATA_NAME where ISHP.IMPORT_PACKAGE_FK = DATA_NAME_ID) --[PROD_ID_DN]
           ,(case when ISHP.import_package_fk =110102 then ISHP.UPC
           when ISHP.IMPORT_package_fk = 110111 then ISHP.MFR_CAT_NO else ISHP.PRODUCT_ID_VALUE END )  ---[PROD_ID_VALUE]
           ,'WAREHOUSE'--[ORG_ID_ENTITY_DN]
           ,'WAREHOUSE CODE'  --[ORG_ID_DN]
           ,(case when ishp.org_id_value = ' ' then 'ALL WHSE' Else ISNULL(ISHP.ORG_ID_VALUE, 'ALL WHSE') end)
           ,'VENDOR'  --[ENTITY_ID_ENTITY_DN]
           ,'VENDOR NUMBER'  --[ENTITY_ID_DN]
           ,ISHP.MFR_VALUE_ID ---[ENTITY_ID_VALUE]
           ,1200   --USD  --[CURRENCY_CR_FK]
           ,1
           ,getdate()
           ,NULL
           ,NULL
 FROM import.IMPORT_SXE_HISTORICAL_PRICESHEETS ISHP
WHERE 1 = 1 
AND SHEET_CLASS = 'CUST HISTORICAL SHEET'
AND isnull(net_value7,0) + isnull(net_value8,0)
+ isnull(net_value9,0) > 0    ----total values are greater then 0
AND ISNULL(ERROR_ON_IMPORT_IND,0) <> 1
AND ISHP.JOB_FK = @IN_JOB_FK)
			
------------------------------------------------------------------------------------------------
---------vend historical sheets
--------------------------------------------------------------------------------
INSERT INTO [marginmgr].[PRICESHEET]
           ([PRICESHEET_NAME]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[RESULT_TYPE_CR_FK]
           ,[NET_VALUE1]
           ,[NET_VALUE2]
           ,[NET_VALUE3]
           ,[NET_VALUE4]
           ,[NET_VALUE5]
           ,[NET_VALUE6]
           ,[PRICESHEET_QTY_PER]
           --,[VALUE_UOM_CODE_FK]
           --,[UOM_CODE]
           ,[PROD_ID_DN]
           ,[PROD_ID_VALUE]
           ,[ORG_ID_ENTITY_DN]
           ,[ORG_ID_DN]
           ,[ORG_ID_VALUE]
           ,[ENTITY_ID_ENTITY_DN]
           ,[ENTITY_ID_DN]
           ,[ENTITY_ID_VALUE]
           ,[CURRENCY_CR_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
(SELECT 
ISHP.SHEET_NAME -- price sheet name
,232400  --	 PRICESHEET HISTORY
,ISHP.PRODUCT_STRUCTURE_FK
,ISNULL (ISHP.ORG_ENTITY_STRUCTURE_FK , 1)--         ,[ORG_ENTITY_STRUCTURE_FK]
,ISHP.MFR_ENTITY_STRUCTURE_FK  --         ,[ENTITY_STRUCTURE_FK]
,ISHP.Effective_Date  --,[EFFECTIVE_DATE]
,NULL --,[END_DATE]
,713 --historical? --,[RESULT_TYPE_CR_FK]
           ,ISHP.NET_VALUE1  --,[NET_VALUE1]
           ,ISHP.NET_VALUE2    --[NET_VALUE2]
           ,ISHP.NET_VALUE3  --[NET_VALUE3]
           ,ISHP.NET_VALUE4  --[NET_VALUE4]
           ,ISHP.NET_VALUE5     --[NET_VALUE5]
           ,ISHP.NET_VALUE6   --  [NET_VALUE6]
           ,NULL  ---[PRICESHEET_QTY_PER]
   --        ,(select UOM_CODE_ID FROM epacube.uom_code with (NOLOCK)
			--Where uom_code = IEPH.PRICEUOM)    ---[VALUE_UOM_CODE_FK]
           --,IEPH.PRICEUOM   --[UOM_CODE]
           ,(select name from epacube.DATA_NAME where ISHP.IMPORT_PACKAGE_FK = DATA_NAME_ID) --[PROD_ID_DN]
           ,(case when ISHP.import_package_fk =110102 then ISHP.UPC
           when ISHP.IMPORT_package_fk = 110111 then ISHP.MFR_CAT_NO else ISHP.PRODUCT_ID_VALUE END )  ---[PROD_ID_VALUE]
           ,'WAREHOUSE'--[ORG_ID_ENTITY_DN]
           ,'WAREHOUSE CODE'  --[ORG_ID_DN]
           ,(case when ishp.org_id_value = ' ' then 'ALL WHSE' Else ISNULL(ISHP.ORG_ID_VALUE, 'ALL WHSE') end)
           ,'VENDOR'  --[ENTITY_ID_ENTITY_DN]
           ,'VENDOR NUMBER'  --[ENTITY_ID_DN]
           ,ISHP.MFR_VALUE_ID ---[ENTITY_ID_VALUE]
           ,1200   --USD  --[CURRENCY_CR_FK]
           ,1
           ,getdate()
           ,NULL
           ,NULL
 FROM import.IMPORT_SXE_HISTORICAL_PRICESHEETS ISHP
WHERE 1 = 1 
AND SHEET_CLASS = 'VEND HISTORICAL SHEET'
AND isnull(net_value1,0) + isnull(net_value2,0)
+ isnull(net_value3,0) + isnull(net_value4,0) 
+ isnull(net_value5,0) + isnull(net_value6,0)> 0    ----total values are greater then 0
AND ISNULL(ERROR_ON_IMPORT_IND,0) <> 1
AND ISHP.JOB_FK = @IN_JOB_FK)




INSERT INTO [marginmgr].[PRICESHEET]
           ([PRICESHEET_NAME]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[RESULT_TYPE_CR_FK]
           ,[NET_VALUE1]
           ,[NET_VALUE2]
           ,[NET_VALUE3]
           ,[NET_VALUE4]
           ,[NET_VALUE5]
           ,[NET_VALUE6]
           ,[PRICESHEET_QTY_PER]
           --,[VALUE_UOM_CODE_FK]
           --,[UOM_CODE]
           ,[PROD_ID_DN]
           ,[PROD_ID_VALUE]
           ,[ORG_ID_ENTITY_DN]
           ,[ORG_ID_DN]
           ,[ORG_ID_VALUE]
           ,[ENTITY_ID_ENTITY_DN]
           ,[ENTITY_ID_DN]
           ,[ENTITY_ID_VALUE]
           ,[CURRENCY_CR_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
(SELECT 
ISHP.SHEET_NAME -- price sheet name
,232500  --	 PRICESHEET HISTORY
,ISHP.PRODUCT_STRUCTURE_FK
,ISNULL (ISHP.ORG_ENTITY_STRUCTURE_FK , 1)--         ,[ORG_ENTITY_STRUCTURE_FK]
,ISHP.MFR_ENTITY_STRUCTURE_FK  --         ,[ENTITY_STRUCTURE_FK]
,ISHP.Effective_Date  --,[EFFECTIVE_DATE]
,NULL --,[END_DATE]
,713 --historical? --,[RESULT_TYPE_CR_FK]
           ,ISHP.NET_VALUE7  --,[NET_VALUE1]
           ,ISHP.NET_VALUE8    --[NET_VALUE2]
           ,ISHP.NET_VALUE9  --[NET_VALUE3]
           ,NULL  --[NET_VALUE4]
           ,NULL     --[NET_VALUE5]
           ,NULL   --  [NET_VALUE6]
           ,NULL  ---[PRICESHEET_QTY_PER]
   --        ,(select UOM_CODE_ID FROM epacube.uom_code with (NOLOCK)
			--Where uom_code = IEPH.PRICEUOM)    ---[VALUE_UOM_CODE_FK]
           --,IEPH.PRICEUOM   --[UOM_CODE]
           ,(select name from epacube.DATA_NAME where ISHP.IMPORT_PACKAGE_FK = DATA_NAME_ID) --[PROD_ID_DN]
           ,(case when ISHP.import_package_fk =110102 then ISHP.UPC
           when ISHP.IMPORT_package_fk = 110111 then ISHP.MFR_CAT_NO else ISHP.PRODUCT_ID_VALUE END )  ---[PROD_ID_VALUE]
           ,'WAREHOUSE'--[ORG_ID_ENTITY_DN]
           ,'WAREHOUSE CODE'  --[ORG_ID_DN]
           ,(case when ishp.org_id_value = ' ' then 'ALL WHSE' Else ISNULL(ISHP.ORG_ID_VALUE, 'ALL WHSE') end)
           ,'VENDOR'  --[ENTITY_ID_ENTITY_DN]
           ,'VENDOR NUMBER'  --[ENTITY_ID_DN]
           ,ISHP.MFR_VALUE_ID ---[ENTITY_ID_VALUE]
           ,1200   --USD  --[CURRENCY_CR_FK]
           ,1
           ,getdate()
           ,NULL
           ,NULL
 FROM import.IMPORT_SXE_HISTORICAL_PRICESHEETS ISHP
WHERE 1 = 1 
AND SHEET_CLASS = 'VEND HISTORICAL SHEET'
AND isnull(net_value7,0) + isnull(net_value8,0)
+ isnull(net_value9,0) > 0    ----total values are greater then 0
AND ISNULL(ERROR_ON_IMPORT_IND,0) <> 1
AND ISHP.JOB_FK = @IN_JOB_FK)


----add some logging

 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD HISTORICAL PRICE SHEETS' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD HISTORICAL PRICE SHEETS' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( RULES_ERRORS TABLE )
--
--     ADD RULES VALIDATION ROUTINE TO BE CALLED FROM UI LATER.. 
--
--	   THESE ERRORS ARE BUSINESS LOGIC ERRORS 
--        WHICH WILL BE BY RULE_TYPE_CR_FK AND RULES_ACTION_OPERATION_FK
--     
--     RULE WILL BE WRITTEN TO RULES TABLE BUT WITH AN ERROR CONDITION
--
--
-------------------------------------------------------------------------------------






 SET @status_desc =  'finished execution of import.LOAD_HISTORICAL_PRICE_SHEETS'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_HISTORICAL PRICE SHEETS has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END




















