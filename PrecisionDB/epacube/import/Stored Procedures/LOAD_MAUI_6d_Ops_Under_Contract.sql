﻿--A_epaMAUI_LOAD_MAUI_6d_Ops_Under_Contract

-- =============================================
-- Author:		Gary Stone
-- Create date: 7/24/2012
-- Description:	MAUI DATA LOAD
-- =============================================
CREATE PROCEDURE [import].[LOAD_MAUI_6d_Ops_Under_Contract] 
@Job_FK_i Numeric(10, 2)
AS
BEGIN

	SET NOCOUNT ON;
	
	--Declare @Job_FK_i Numeric(10, 2)
	--Set @Job_FK_i = 3197.01

	Declare @ProjectionDays Int
	Declare @HistoryDays Numeric(6, 0)
	Declare @UseFileDates Int
	Declare @WhatIF_ID Int
	Declare @Rbts Int
	Declare @ERP Varchar(32)
	Set @HistoryDays = Cast((Select top 1 Sales_History_End_Date - Sales_History_Start_Date 
		from dbo.MAUI_BASIS With (NOLOCK) where job_fk_i = @Job_FK_i) as Numeric(6,0)) 
	Set @UseFileDates = -1
	Set @ProjectionDays =
		Case when @UseFileDates = 0 then 180 else @HistoryDays	end 
	Set @WhatIF_ID = 0
	Set @Rbts = 20
	Set @ERP = (Select Value from epacube.epacube_params with (nolock) where name = 'ERP HOST')
	
	Declare @Dim as Varchar(64)
	Declare @Command as Varchar(256)
	Declare @PID as varchar(32)
	Declare @SQL1 as VarChar(Max)
	Declare @SQL2 as VarChar(Max)
	Declare @SQLcontractsPrec as Varchar(Max)
	Declare @SQLcontractsNmbr as Varchar(Max)
	
	Delete from dbo.MAUI_Opportunities where Job_FK_i = @Job_FK_i AND Under_Contract = 1
	
	IF object_id('tempdb..##ContractRules') is not null
	drop table ##ContractRules

	Select top 1 Cast(Rules_ID as BigInt) Rules_ID into ##ContractRules from Synchronizer.RULES where 1 = 2
	
	Set @SQLcontractsPrec = 'Insert into ##ContractRules (Rules_ID) Select Cast(r.RULES_ID as bigint) Rules_ID from synchronizer.RULES r inner join synchronizer.RULES_PRECEDENCE rp on r.RULES_PRECEDENCE_FK = rp.RULES_PRECEDENCE_ID where r.RESULT_DATA_NAME_FK = 111602 and HOST_PRECEDENCE_LEVEL in (' + (select top 1 Replace(Replace(value, '(', ''), ')', '') from dbo.MAUI_Configuration where Class = 'contract definition' and Description = 'Host Precedence Level') + ')'
	Exec(@SQLcontractsPrec)
	
	Create Index idx_cnt on ##ContractRules(Rules_ID)
	
	Set @SQLcontractsNmbr = 'Insert into ##ContractRules (Rules_ID) select r.rules_id from synchronizer.rules r left join ##ContractRules tmp on r.rules_id = tmp.rules_id where tmp.rules_id is null
		and r.contract_no is not null and r.result_data_name_fk in (' + 
		
		(Select Case value  
			When 1 then '111602'
			When 2 then '111501'
			When 3 then '111501, 111602'
			else '0' end from dbo.MAUI_Configuration where Class = 'Contract Definition' and Description = 'ContractNo') + ')'
	EXEC(@SQLcontractsNmbr)
	
------------Transaction Groups
	Insert into dbo.MAUI_Opportunities
	select top 10
	Case When Max([Price_Mult]) <> 1 or Min([Price_Mult]) <> 1 then '*** ' else '' end +
	 Identifier Focus, 'Transaction Group' lblFocus
	, Cast(Sum(Case When isnull([Margin_Dollars_Delta_Cur_To_Hist], 0) < 0 then [Margin_Dollars_Delta_Cur_To_Hist] else 0 end ) as Numeric(18, 2))  'Impact Negative'
	, Cast(Sum(Case When isnull([Margin_Dollars_Delta_Cur_To_Hist], 0) > 0 then [Margin_Dollars_Delta_Cur_To_Hist] else 0 end ) as Numeric(18, 2))  'Impact Positive'
	, Cast(Sum([Sales_Dollars_Current_Ttl]) as money) 'Total Sales'
	, 0 'Pct Transactions Overridden'
	, 2 'Opportunity Type'
	, 1 'Opportunity_Parent_FK'
	, 0 'Sort Seq'
	, 1 'Under Contract'
	, Job_FK_i
	from (
	Select Cast([Sales_Dollars_Current] - [Sales_Dollars_Hist] - [Cost_Dollars_Current] + [Prod_Cost_Dollars_History] as Numeric(18, 2)) Margin_Dollars_Delta_Cur_To_Hist
	, *
	from (
	Select
	Prod_Cost_Dollars_Hist / [Sales_Mult] Prod_Cost_Amt_Hist
	, isnull(Prod_Cost_Dollars_Hist,0) - [Rebate_Dollars_Hist] 'Prod_Cost_Dollars_History'
	, (isnull(Margin_Cost_Basis_Amt,0) - [Rebate_Amount]) * [Sales_Mult] 'Cost_Dollars_Current'
	, [Sales_Dollars_Current_Ttl] 'Sales_Dollars_Current'
	, *
	from (
	Select Cast(Case 20
		When 10 then 0
		When 20 then isnull(Rebate_CB_Amt, 0)
		When 30 then isnull(Rebate_BUY_Amt, 0)
		When 35 then isnull(Rebate_Incentive_Amt_Cur, 0)
		When 40 then isnull(isnull(Rebate_CB_Amt, Rebate_CB_Amt), 0) + isnull(isnull(Rebate_BUY_Amt, Rebate_CB_Amt), 0) + isnull(isnull(Rebate_Incentive_Amt_Cur, Rebate_CB_Amt), 0)
		End as Numeric(18, 4))
	  'Rebate_Amount'
	, Cast(Case 20
		When 10 then 0
		When 20 then isnull(Rebate_CB_Amt, 0)
		When 30 then isnull(Rebate_BUY_Amt, 0)
		When 35 then isnull(Rebate_Incentive_Amt_cur, 0)
		When 40 then isnull(Rebate_CB_Amt, 0) + isnull(Rebate_BUY_Amt, 0) + isnull(Rebate_Incentive_Amt_Cur, 0)
		End * Case -1 When 0 then 180 * Sales_Qty_Daily else [Sales_Qty] end as Numeric(18, 4))
	  'Rebate_Dollars'
	, Cast(Case 20
		When 10 then 0
		When 20 then isnull(Rebate_CB_Dollars_Hist,0)
		When 30 then isnull(Rebate_BUY_Dollars_Hist,0)
		When 35 then isnull(Rebate_Incentive_Dollars_Hist,0)
		When 40 then isnull(Rebate_CB_Dollars_Hist,0) + isnull(Rebate_BUY_Dollars_Hist,0) + isnull(Rebate_Incentive_Dollars_Hist,0)
		End as Numeric(18, 4))
	  'Rebate_Dollars_Hist'
	  
	, 	Cast(Sell_Price_Cust_Amt_Cur * Case -1 When 0 then 180 * Sales_Qty_Daily else [Sales_Qty] 
			end as Numeric(18, 4)) 'Sales_Dollars_Current_Ttl'
		, Case -1 When 0 then 180 * Sales_Qty_Daily else [Sales_Qty] end 'Sales_Mult'
		, isnull((Select Rule_Sell_Price_Change from dbo.MAUI_Rules_Summary_WhatIF where What_If_Reference_ID 
			= Cast(Cast(MBC.What_If_Reference_ID as Varchar(32)) + Cast(0 as varchar(2)) as BigInt)), 1) Price_Mult
	, *
	from dbo.MAUI_Basis_Calcs MBC With (NOLOCK)
	Inner Join ##ContractRules tmp on MBC.Sell_Price_Cust_Rules_FK = tmp.rules_id or MBC.Rebate_CB_Rules_FK = tmp.rules_id
	Where Job_FK_i = @Job_FK_i and isnull(sales_qty, 0) <> 0 
	And Identifier is not null
	) A ) B ) C
	--FilterWhereA

	Group by Job_FK_i, Identifier
	Order by [Impact Negative] Asc
	
	Insert into dbo.MAUI_Opportunities
	select top 10
	Case When Max([Price_Mult]) <> 1 or Min([Price_Mult]) <> 1 then '*** ' else '' end +
	 Identifier Focus, 'Transaction Group' lblFocus
	, Cast(Sum(Case When isnull([Margin_Dollars_Delta_Cur_To_Hist], 0) < 0 then [Margin_Dollars_Delta_Cur_To_Hist] else 0 end ) as Numeric(18, 2))  'Impact Negative'
	, Cast(Sum(Case When isnull([Margin_Dollars_Delta_Cur_To_Hist], 0) > 0 then [Margin_Dollars_Delta_Cur_To_Hist] else 0 end ) as Numeric(18, 2))  'Impact Positive'
	, Cast(Sum([Sales_Dollars_Current_Ttl]) as money) 'Total Sales'
	, 0 'Pct Transactions Overridden'
	, 2 'Opportunity Type'
	, 1 'Opportunity_Parent_FK'
	, 1 'Sort Seq'
	, 1 'Under Contract'
	, Job_FK_i
	from (
	Select Cast([Sales_Dollars_Current] - [Sales_Dollars_Hist] - [Cost_Dollars_Current] + [Prod_Cost_Dollars_History] as Numeric(18, 2)) Margin_Dollars_Delta_Cur_To_Hist
	, *
	from (
	Select
	Prod_Cost_Dollars_Hist / [Sales_Mult] Prod_Cost_Amt_Hist
	, isnull(Prod_Cost_Dollars_Hist,0) - [Rebate_Dollars_Hist] 'Prod_Cost_Dollars_History'
	, (isnull(Margin_Cost_Basis_Amt,0) - [Rebate_Amount]) * [Sales_Mult] 'Cost_Dollars_Current'
	, [Sales_Dollars_Current_Ttl] 'Sales_Dollars_Current'
	, *
	from (
	Select Cast(Case 20
		When 10 then 0
		When 20 then isnull(Rebate_CB_Amt, 0)
		When 30 then isnull(Rebate_BUY_Amt, 0)
		When 35 then isnull(Rebate_Incentive_Amt_Cur, 0)
		When 40 then isnull(isnull(Rebate_CB_Amt, Rebate_CB_Amt), 0) + isnull(isnull(Rebate_BUY_Amt, Rebate_CB_Amt), 0) + isnull(isnull(Rebate_Incentive_Amt_Cur, Rebate_CB_Amt), 0)
		End as Numeric(18, 4))
	  'Rebate_Amount'
	, Cast(Case 20
		When 10 then 0
		When 20 then isnull(Rebate_CB_Amt, 0)
		When 30 then isnull(Rebate_BUY_Amt, 0)
		When 35 then isnull(Rebate_Incentive_Amt_cur, 0)
		When 40 then isnull(Rebate_CB_Amt, 0) + isnull(Rebate_BUY_Amt, 0) + isnull(Rebate_Incentive_Amt_Cur, 0)
		End * Case -1 When 0 then 180 * Sales_Qty_Daily else [Sales_Qty] end as Numeric(18, 4))
	  'Rebate_Dollars'
	, Cast(Case 20
		When 10 then 0
		When 20 then isnull(Rebate_CB_Dollars_Hist,0)
		When 30 then isnull(Rebate_BUY_Dollars_Hist,0)
		When 35 then isnull(Rebate_Incentive_Dollars_Hist,0)
		When 40 then isnull(Rebate_CB_Dollars_Hist,0) + isnull(Rebate_BUY_Dollars_Hist,0) + isnull(Rebate_Incentive_Dollars_Hist,0)
		End as Numeric(18, 4))
	  'Rebate_Dollars_Hist'
	, 	Cast(Sell_Price_Cust_Amt_Cur * Case -1 When 0 then 180 * Sales_Qty_Daily else [Sales_Qty] 
			end as Numeric(18, 4)) 'Sales_Dollars_Current_Ttl'
		, Case -1 When 0 then 180 * Sales_Qty_Daily else [Sales_Qty] end 'Sales_Mult'
		, isnull((Select Rule_Sell_Price_Change from dbo.MAUI_Rules_Summary_WhatIF where What_If_Reference_ID 
			= Cast(Cast(MBC.What_If_Reference_ID as Varchar(32)) + Cast(0 as varchar(2)) as BigInt)), 1) Price_Mult
	, *
	from dbo.MAUI_Basis_Calcs MBC With (NOLOCK) 
	Inner Join ##ContractRules tmp on MBC.Sell_Price_Cust_Rules_FK = tmp.rules_id or MBC.Rebate_CB_Rules_FK = tmp.rules_id 
	Where Job_FK_i = @Job_FK_i and isnull(sales_qty, 0) <> 0 
	And Identifier is not null
	) A ) B ) C
	--FilterWhereA

	Group by Job_FK_i, Identifier
	Order by [Impact Positive] Desc

------------

		DECLARE Ops Cursor local for
		Select 
		cols.ColCMBName
		, cols.ColCommand
		, 1
		from dbo.maui_cols cols
		where cols.ERP = @ERP and cols.ColCMBName not in ('TRANSACTION GROUP', 'Prods Expired from Contract') and cols.ColumnNameMaui is not null
	
		 OPEN Ops;
		 FETCH NEXT FROM Ops INTO @Dim, @Command, @PID
            
		 WHILE @@FETCH_STATUS = 0
	Begin
	
	Set @SQL1 = '
	Set NOCOUNT On

	Insert into dbo.MAUI_Opportunities
	select top 10
	Case When Max([Price_Mult]) <> 1 or Min([Price_Mult]) <> 1 then ''*** '' else '''' end +
	 Cast(' + @Command + ' as varchar(32)) Focus, ''' + @Dim + ''' lblFocus

	, Cast(Sum(Case When isnull([OverRide_Impact_Amt], 0) < 0 then [OverRide_Impact_Amt] else 0 end * [OverRide Qty Proj] ) as Numeric(18, 2))  ''Impact Negative''
	, Cast(Sum(Case When isnull([OverRide_Impact_Amt], 0) > 0 then [OverRide_Impact_Amt] else 0 end  * [OverRide Qty Proj] ) as Numeric(18, 2))  ''Impact Positive''
	, Cast(Sum([Sales_Dollars_Current_Ttl]) as money) ''Total Sales''
	, Sum([Override Transactions]) / SUM([Total Transactions]) ''Pct Transactions OR''
	, 1 ''Opportunity Type''
	, ' + @PID + ''' Opportunity_Parent_FK''
	, 0 ''Sort Seq''
	, 1 ''Under Contract''
	, Job_FK_i
	from (
	Select 
	Cast(Sell_Price_Cust_Amt_Cur * Case ' + Cast(@UseFileDates as varchar(8)) + ' When 0 then ' + Cast(@ProjectionDays as varchar(8)) + ' * Sales_Qty_Daily else [Sales_Qty] 
		end as Numeric(18, 4)) ''Sales_Dollars_Current_Ttl''
	, Case ' + Cast(@UseFileDates as varchar(8)) + ' When 0 then ' + Cast(@ProjectionDays as varchar(8)) + ' * Sales_Qty_Daily else [Sales_Qty] end ''Sales_Mult''
	, isnull([Override Qty] / ' + Cast(@HistoryDays as varchar(8)) + ' * ' + Cast(@ProjectionDays as varchar(8)) + ',0) ''OverRide Qty Proj''
	, isnull((Select Rule_Sell_Price_Change from dbo.MAUI_Rules_Summary_WhatIF where What_If_Reference_ID 
		= Cast(Cast(MBC.What_If_Reference_ID as Varchar(32)) + Cast(' + Cast(@WhatIF_ID as varchar(8)) + ' as varchar(2)) as BigInt)), 1) Price_Mult
	, *
	from dbo.MAUI_Basis_Calcs MBC With (NOLOCK) 
	Inner Join ##ContractRules tmp on MBC.Sell_Price_Cust_Rules_FK = tmp.rules_id or MBC.Rebate_CB_Rules_FK = tmp.rules_id 
	Where Job_FK_i = ' + Cast(@Job_FK_i as varchar(8)) + ' and isnull(sales_qty, 0) <> 0 
	And ' + @Command + ' is not null
	) A
	--FilterWhereA

	Group by Job_FK_i, ' + @Command + '

	Order by [Impact Negative] asc
	
	Insert into dbo.MAUI_Opportunities
	select top 10
	Case When Max([Price_Mult]) <> 1 or Min([Price_Mult]) <> 1 then ''*** '' else '''' end +
	 Cast(' + @Command + ' as varchar(32)) Focus, ''' + @Dim + ''' lblFocus

	, Cast(Sum(Case When isnull([OverRide_Impact_Amt], 0) < 0 then [OverRide_Impact_Amt] else 0 end * [OverRide Qty Proj] ) as Numeric(18, 2))  ''Impact Negative''
	, Cast(Sum(Case When isnull([OverRide_Impact_Amt], 0) > 0 then [OverRide_Impact_Amt] else 0 end  * [OverRide Qty Proj] ) as Numeric(18, 2))  ''Impact Positive''
	, Cast(Sum([Sales_Dollars_Current_Ttl]) as money) ''Total Sales''
	, Sum([Override Transactions]) / SUM([Total Transactions]) ''Pct Transactions OR''
	, 1 ''Opportunity Type''
	, ' + @PID + ''' Opportunity_Parent_FK''
	, 1 ''Sort Seq''
	, 1 ''Under Contract''
	, Job_FK_i
	from (
	Select 
	Cast(Sell_Price_Cust_Amt_Cur * Case ' + Cast(@UseFileDates as varchar(8)) + ' When 0 then ' + Cast(@ProjectionDays as varchar(8)) + ' * Sales_Qty_Daily else [Sales_Qty] 
		end as Numeric(18, 4)) ''Sales_Dollars_Current_Ttl''
	, Case ' + Cast(@UseFileDates as varchar(8)) + ' When 0 then ' + Cast(@ProjectionDays as varchar(8)) + ' * Sales_Qty_Daily else [Sales_Qty] end ''Sales_Mult''
	, isnull([Override Qty] / ' + Cast(@HistoryDays as varchar(8)) + ' * ' + Cast(@ProjectionDays as varchar(8)) + ',0) ''OverRide Qty Proj''
	, isnull((Select Rule_Sell_Price_Change from dbo.MAUI_Rules_Summary_WhatIF where What_If_Reference_ID 
		= Cast(Cast(MBC.What_If_Reference_ID as Varchar(32)) + Cast(' + Cast(@WhatIF_ID as varchar(8)) + ' as varchar(2)) as BigInt)), 1) Price_Mult
	, *
	from dbo.MAUI_Basis_Calcs MBC With (NOLOCK) 
	Inner Join ##ContractRules tmp on MBC.Sell_Price_Cust_Rules_FK = tmp.rules_id or MBC.Rebate_CB_Rules_FK = tmp.rules_id 
	Where Job_FK_i = ' + Cast(@Job_FK_i as varchar(8)) + ' and isnull(sales_qty, 0) <> 0 
	And ' + @Command + ' is not null
	) A
	--FilterWhereA

	Group by Job_FK_i, ' + @Command + '

	Order by [Impact Positive] Desc

	Insert into dbo.MAUI_Opportunities
	select top 10
	Case When Max([Price_Mult]) <> 1 or Min([Price_Mult]) <> 1 then ''*** '' else '''' end +
	 Cast(' + @Command + ' as varchar(32)) Focus, ''' + @Dim + ''' lblFocus
	, Cast(Sum(Case When isnull([Margin_Dollars_Delta_Cur_To_Hist], 0) < 0 then [Margin_Dollars_Delta_Cur_To_Hist] else 0 end ) as Numeric(18, 2))  ''Impact Negative''
	, Cast(Sum(Case When isnull([Margin_Dollars_Delta_Cur_To_Hist], 0) > 0 then [Margin_Dollars_Delta_Cur_To_Hist] else 0 end ) as Numeric(18, 2))  ''Impact Positive''
	, Cast(Sum([Sales_Dollars_Current_Ttl]) as money) ''Total Sales''
	, 0 ''Pct Transactions Overridden''
	, 2 ''Opportunity Type''
	, ' + @PID + ''' Opportunity_Parent_FK''
	, 0 ''Sort Seq''
	, 1 ''Under Contract''
	, Job_FK_i
	from (
	Select Cast([Sales_Dollars_Current] - [Sales_Dollars_Hist] - [Cost_Dollars_Current] + [Prod_Cost_Dollars_History] as Numeric(18, 2)) Margin_Dollars_Delta_Cur_To_Hist
	, *
	from (
	Select
	Prod_Cost_Dollars_Hist / [Sales_Mult] Prod_Cost_Amt_Hist
	, isnull(Prod_Cost_Dollars_Hist,0) - [Rebate_Dollars_Hist] ''Prod_Cost_Dollars_History''
	, (isnull(Margin_Cost_Basis_Amt,0) - [Rebate_Amount]) * [Sales_Mult] ''Cost_Dollars_Current''
	, [Sales_Dollars_Current_Ttl] ''Sales_Dollars_Current''
	, *
	from (
	Select Cast(Case ' + Cast(@Rbts as Varchar(8)) + '
		When 10 then 0
		When 20 then isnull(Rebate_CB_Amt, 0)
		When 30 then isnull(Rebate_BUY_Amt, 0)
		When 35 then isnull(Rebate_Incentive_Amt_Cur, 0)
		When 40 then isnull(isnull(Rebate_CB_Amt, Rebate_CB_Amt), 0) + isnull(isnull(Rebate_BUY_Amt, Rebate_CB_Amt), 0) + isnull(isnull(Rebate_Incentive_Amt_Cur, Rebate_CB_Amt), 0)
		End as Numeric(18, 4))
	  ''Rebate_Amount''
	, Cast(Case ' + Cast(@Rbts as Varchar(8)) + '
		When 10 then 0
		When 20 then isnull(Rebate_CB_Amt, 0)
		When 30 then isnull(Rebate_BUY_Amt, 0)
		When 35 then isnull(Rebate_Incentive_Amt_cur, 0)
		When 40 then isnull(Rebate_CB_Amt, 0) + isnull(Rebate_BUY_Amt, 0) + isnull(Rebate_Incentive_Amt_Cur, 0)
		End * Case ' + Cast(@UseFileDates as varchar(8)) + ' When 0 then ' + Cast(@ProjectionDays as varchar(8)) + ' * Sales_Qty_Daily else [Sales_Qty] end as Numeric(18, 4))
	  ''Rebate_Dollars''
	, Cast(Case ' + Cast(@Rbts as Varchar(8)) + '
		When 10 then 0
		When 20 then isnull(Rebate_CB_Dollars_Hist,0)
		When 30 then isnull(Rebate_BUY_Dollars_Hist,0)
		When 35 then isnull(Rebate_Incentive_Dollars_Hist,0)
		When 40 then isnull(Rebate_CB_Dollars_Hist,0) + isnull(Rebate_BUY_Dollars_Hist,0) + isnull(Rebate_Incentive_Dollars_Hist,0)
		End as Numeric(18, 4))
	  ''Rebate_Dollars_Hist''
	  
	, 	Cast(Sell_Price_Cust_Amt_Cur * Case ' + Cast(@UseFileDates as varchar(8)) + ' When 0 then ' + Cast(@ProjectionDays as varchar(8)) + ' * Sales_Qty_Daily else [Sales_Qty] 
			end as Numeric(18, 4)) ''Sales_Dollars_Current_Ttl''
		, Case ' + Cast(@UseFileDates as varchar(8)) + ' When 0 then ' + Cast(@ProjectionDays as varchar(8)) + ' * Sales_Qty_Daily else [Sales_Qty] end ''Sales_Mult''
		, isnull((Select Rule_Sell_Price_Change from dbo.MAUI_Rules_Summary_WhatIF where What_If_Reference_ID 
			= Cast(Cast(MBC.What_If_Reference_ID as Varchar(32)) + Cast(' + Cast(@WhatIF_ID as varchar(8)) + ' as varchar(2)) as BigInt)), 1) Price_Mult
	 
	 
	, *
	from dbo.MAUI_Basis_Calcs MBC With (NOLOCK)
	Inner Join ##ContractRules tmp on MBC.Sell_Price_Cust_Rules_FK = tmp.rules_id or MBC.Rebate_CB_Rules_FK = tmp.rules_id 
	Where Job_FK_i = ' + Cast(@Job_FK_i as varchar(8)) + ' and isnull(sales_qty, 0) <> 0 
	And ' + @Command + ' is not null
	) A ) B ) C
	--FilterWhereA

	Group by Job_FK_i, ' + @Command + '
	Order by [Impact Negative] asc

	'
	
	Set @SQL2 = 
	
	'
	Insert into dbo.MAUI_Opportunities
	select top 10
	Case When Max([Price_Mult]) <> 1 or Min([Price_Mult]) <> 1 then ''*** '' else '''' end +
	 Cast(' + @Command + ' as varchar(32)) Focus, ''' + @Dim + ''' lblFocus
	, Cast(Sum(Case When isnull([Margin_Dollars_Delta_Cur_To_Hist], 0) < 0 then [Margin_Dollars_Delta_Cur_To_Hist] else 0 end ) as Numeric(18, 2))  ''Impact Negative''
	, Cast(Sum(Case When isnull([Margin_Dollars_Delta_Cur_To_Hist], 0) > 0 then [Margin_Dollars_Delta_Cur_To_Hist] else 0 end ) as Numeric(18, 2))  ''Impact Positive''
	, Cast(Sum([Sales_Dollars_Current_Ttl]) as money) ''Total Sales''
	, 0 ''Pct Transactions Overridden''
	, 2 ''Opportunity Type''
	, ' + @PID + ''' Opportunity_Parent_FK''
	, 1 ''Sort Seq''
	, 1 ''Under Contract''
	, Job_FK_i
	from (
	Select Cast([Sales_Dollars_Current] - [Sales_Dollars_Hist] - [Cost_Dollars_Current] + [Prod_Cost_Dollars_History] as Numeric(18, 2)) Margin_Dollars_Delta_Cur_To_Hist
	, *
	from (
	Select
	Prod_Cost_Dollars_Hist / [Sales_Mult] Prod_Cost_Amt_Hist
	, isnull(Prod_Cost_Dollars_Hist,0) - [Rebate_Dollars_Hist] ''Prod_Cost_Dollars_History''
	, (isnull(Margin_Cost_Basis_Amt,0) - [Rebate_Amount]) * [Sales_Mult] ''Cost_Dollars_Current''
	, [Sales_Dollars_Current_Ttl] ''Sales_Dollars_Current''
	, *
	from (
	Select Cast(Case ' + Cast(@Rbts as Varchar(8)) + '
		When 10 then 0
		When 20 then isnull(Rebate_CB_Amt, 0)
		When 30 then isnull(Rebate_BUY_Amt, 0)
		When 35 then isnull(Rebate_Incentive_Amt_Cur, 0)
		When 40 then isnull(isnull(Rebate_CB_Amt, Rebate_CB_Amt), 0) + isnull(isnull(Rebate_BUY_Amt, Rebate_CB_Amt), 0) + isnull(isnull(Rebate_Incentive_Amt_Cur, Rebate_CB_Amt), 0)
		End as Numeric(18, 4))
	  ''Rebate_Amount''
	, Cast(Case ' + Cast(@Rbts as Varchar(8)) + '
		When 10 then 0
		When 20 then isnull(Rebate_CB_Amt, 0)
		When 30 then isnull(Rebate_BUY_Amt, 0)
		When 35 then isnull(Rebate_Incentive_Amt_cur, 0)
		When 40 then isnull(Rebate_CB_Amt, 0) + isnull(Rebate_BUY_Amt, 0) + isnull(Rebate_Incentive_Amt_Cur, 0)
		End * Case ' + Cast(@UseFileDates as varchar(8)) + ' When 0 then ' + Cast(@ProjectionDays as varchar(8)) + ' * Sales_Qty_Daily else [Sales_Qty] end as Numeric(18, 4))
	  ''Rebate_Dollars''
	, Cast(Case ' + Cast(@Rbts as Varchar(8)) + '
		When 10 then 0
		When 20 then isnull(Rebate_CB_Dollars_Hist,0)
		When 30 then isnull(Rebate_BUY_Dollars_Hist,0)
		When 35 then isnull(Rebate_Incentive_Dollars_Hist,0)
		When 40 then isnull(Rebate_CB_Dollars_Hist,0) + isnull(Rebate_BUY_Dollars_Hist,0) + isnull(Rebate_Incentive_Dollars_Hist,0)
		End as Numeric(18, 4))
	  ''Rebate_Dollars_Hist''
	  
	, 	Cast(Sell_Price_Cust_Amt_Cur * Case ' + Cast(@UseFileDates as varchar(8)) + ' When 0 then ' + Cast(@ProjectionDays as varchar(8)) + ' * Sales_Qty_Daily else [Sales_Qty] 
			end as Numeric(18, 4)) ''Sales_Dollars_Current_Ttl''
		, Case ' + Cast(@UseFileDates as varchar(8)) + ' When 0 then ' + Cast(@ProjectionDays as varchar(8)) + ' * Sales_Qty_Daily else [Sales_Qty] end ''Sales_Mult''
		, isnull((Select Rule_Sell_Price_Change from dbo.MAUI_Rules_Summary_WhatIF where What_If_Reference_ID 
			= Cast(Cast(MBC.What_If_Reference_ID as Varchar(32)) + Cast(' + Cast(@WhatIF_ID as varchar(8)) + ' as varchar(2)) as BigInt)), 1) Price_Mult
	 
	 
	, *
	from dbo.MAUI_Basis_Calcs MBC With (NOLOCK)
	Inner Join ##ContractRules tmp on MBC.Sell_Price_Cust_Rules_FK = tmp.rules_id or MBC.Rebate_CB_Rules_FK = tmp.rules_id  
	Where Job_FK_i = ' + Cast(@Job_FK_i as varchar(8)) + ' and isnull(sales_qty, 0) <> 0 
	And ' + @Command + ' is not null
	) A ) B ) C
	--FilterWhereA

		Group by Job_FK_i, ' + @Command + '
	Order by [Impact Positive] Desc
	
	'
--	Print(@SQL1)
	exec(@SQL1 + ' ' + @SQL2)
	
		FETCH NEXT FROM Ops INTO @Dim, @Command, @PID
	End
	Close Ops;
	Deallocate Ops;

	IF object_id('tempdb..##ContractRules') is not null
	drop table ##ContractRules

END
