﻿--A_epaMAUI_LOAD_MAUI_9_DIMENSIONS

--May 1, 2013			GHS		Enabled Dimensions for Effective Date
--June 2013				GHS		Added code to prevent back-to-back comma error

-- =============================================
-- Author:		Gary Stone
-- Create date: 9/7/2011
-- Description:	MAUI DATA LOAD
-- =============================================
CREATE PROCEDURE [import].[LOAD_MAUI_9_DIMENSIONS]
	@Job_FK bigint, @Job_FK_i Numeric(10, 2)
AS
BEGIN

	SET NOCOUNT ON;

--Declare @Job_FK bigint
--Declare @Job_FK_i Numeric(10, 2)
--Set @Job_FK = 108
--Set @Job_FK_i = 108.01

Declare @ERP Varchar(32)
Set @ERP = (Select Value from epacube.epacube_params with (nolock) where name = 'ERP HOST')

insert into dbo.maui_log Select @Job_FK_i, 'Start Loading MAUI_Dimensions', getdate(),Null, Null

IF object_id('tempdb..##tmp_Dims') is not null
drop table ##tmp_Dims;

IF object_id('tempdb..##Dim_Upd') is not null
Drop Table ##Dim_Upd

delete from dbo.MAUI_dimensions where job_fk = @Job_FK

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[MAUI_Dimensions]') AND name = N'idx_dims')
DROP INDEX [idx_dims] ON [dbo].[MAUI_Dimensions] WITH ( ONLINE = OFF )

Insert into dbo.MAUI_Dimensions
Select 
Job_FK
, 1 DimensionLevel
, Upper(isnull(Price_Rule_Type, 'Default Pricing')) DimensionName
, Null DimensionValue
, Null DimensionsDescription
, 'Pricing Rule' DimensionCriteria
, Upper(Sell_Price_Cust_Filter_Name) ComponentCust
, Upper(Sell_Price_Prod_Filter_Name) ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis with (nolock) where job_FK = @Job_FK
--and price_rule_type is not null
and isnull(Price_Rule_Type, 'Default Pricing') not in (Select DimensionName from dbo.MAUI_Dimensions with (nolock) where job_FK = @Job_FK 
and DimensionLevel = 1)
Group by Job_FK, Price_Rule_Type, Sell_Price_Cust_Filter_Name, Sell_Price_Prod_Filter_Name
Order by Price_Rule_Type

Insert into dbo.MAUI_Dimensions
Select 
Job_FK
, 1 DimensionLevel
, Upper(Rebate_CB_Rule_Type) DimensionName
, Null DimensionValue
, Null DimensionsDescription
, 'CUSTOMER Rebate Rule' DimensionCriteria
, Upper(Rebate_CB_Cust_Filter_Name) ComponentCust
, Upper(Rebate_CB_Prod_Filter_Name) ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis with (nolock) where job_FK = @Job_FK
and Rebate_CB_Rule_Type is not null
and Rebate_CB_Rule_Type not in (Select DimensionName from dbo.MAUI_Dimensions with (nolock) where job_FK = @Job_FK and DimensionLevel = 1)
Group by Job_FK, Rebate_CB_Rule_Type, Rebate_CB_Cust_Filter_Name, Rebate_CB_Prod_Filter_Name
Order by Rebate_CB_Rule_Type

Insert into dbo.MAUI_Dimensions
Select 
Job_FK
, 1 DimensionLevel
, Upper(Rebate_BUY_Rule_Type) DimensionName
, Null DimensionValue
, Null DimensionsDescription
, 'BUY Rebate Rule' DimensionCriteria
, Upper(Rebate_BUY_Cust_Filter_Name) ComponentCust
, Upper(Rebate_BUY_Prod_Filter_Name) ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis with (nolock) where job_FK = @Job_FK
and Rebate_BUY_Rule_Type is not null
and Rebate_BUY_Rule_Type not in (Select DimensionName from dbo.MAUI_Dimensions with (nolock) where job_FK = @Job_FK and DimensionLevel = 1)
Group by Job_FK, Rebate_BUY_Rule_Type, Rebate_BUY_Cust_Filter_Name, Rebate_BUY_Prod_Filter_Name
Order by Rebate_BUY_Rule_Type

Insert into dbo.MAUI_Dimensions
Select 
Job_FK
, 1 DimensionLevel
, Upper(Sell_Price_Cust_Operation) DimensionName
, Null DimensionValue
, Null DimensionsDescription
, 'Price Rule Operation' DimensionCriteria
, Upper(Sell_Price_Cust_Filter_Name) ComponentCust
, Upper(Sell_Price_Prod_Filter_Name) ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis with (nolock) where job_FK = @Job_FK
and Sell_Price_Cust_Operation is not null
and Sell_Price_Cust_Operation not in (Select DimensionName from dbo.MAUI_Dimensions with (nolock) where job_FK = @Job_FK and DimensionLevel = 1
	and DimensionCriteria = 'Price Rule Operation')
Group by Job_FK, Sell_Price_Cust_Operation, Sell_Price_Cust_Filter_Name, Sell_Price_Prod_Filter_Name
Order by Sell_Price_Cust_Operation

Insert into dbo.MAUI_Dimensions
Select 
Job_FK
, 1 DimensionLevel
, Upper(Rebate_CB_Operation) DimensionName
, Null DimensionValue
, Null DimensionsDescription
, 'CUSTOMER Rule Operation' DimensionCriteria
, Upper(Rebate_CB_Cust_Filter_Name) ComponentCust
, Upper(Rebate_CB_Prod_Filter_Name) ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis with (nolock) where job_FK = @Job_FK
and Rebate_CB_Operation is not null
and Rebate_CB_Operation not in (Select DimensionName from dbo.MAUI_Dimensions with (nolock) where job_FK = @Job_FK and DimensionLevel = 1
	and DimensionCriteria = 'CUSTOMER Rule Operation')
Group by Job_FK, Rebate_CB_Operation, Rebate_CB_Cust_Filter_Name, Rebate_CB_Prod_Filter_Name
Order by Rebate_CB_Operation

Insert into dbo.MAUI_Dimensions
Select 
Job_FK
, 1 DimensionLevel
, Upper(Rebate_BUY_Operation) DimensionName
, Null DimensionValue
, Null DimensionsDescription
, 'BUY Rule Operation' DimensionCriteria
, Upper(Rebate_BUY_Cust_Filter_Name) ComponentCust
, Upper(Rebate_BUY_Prod_Filter_Name) ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis with (nolock) where job_FK = @Job_FK
and Rebate_BUY_Operation is not null
and Rebate_BUY_Operation not in (Select DimensionName from dbo.MAUI_Dimensions with (nolock) where job_FK = @Job_FK and DimensionLevel = 1
	and DimensionCriteria = 'BUY Rule Operation')
Group by Job_FK, Rebate_BUY_Operation, Rebate_BUY_Cust_Filter_Name, Rebate_BUY_Prod_Filter_Name
Order by Rebate_BUY_Operation

CREATE UNIQUE CLUSTERED INDEX [idx_dims] ON [dbo].[MAUI_Dimensions] 
(
	[Job_FK] ASC,
	[DimensionLevel] ASC,
	[DimensionName] ASC,
	[DimensionValue] ASC,
	[DimensionCriteria] ASC,
	[ComponentCust] ASC,
	[ComponentProd] ASC,
	[DimensionDescription] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

Select DimensionName, ComponentCust, ComponentProd, DimensionCriteria, Job_FK into ##Dim_Upd from dbo.MAUI_dimensions with (nolock)
	Where Job_FK = @Job_FK and DimensionLevel = 1 and DimensionCriteria like '%Rule'
Create Index idx_dim_upd on ##Dim_Upd(DimensionName, ComponentCust, ComponentProd, DimensionCriteria, Job_FK)

Update D
Set DimensionAffiliation = P.DimensionName
from dbo.MAUI_dimensions D
Inner Join 
##Dim_Upd P
on P.ComponentCust = D.ComponentCust and P.ComponentProd = D.ComponentProd 
Where D.DimensionLevel = 1 and D.DimensionCriteria like '%Operation' and D.Job_FK = @Job_FK and P.Job_FK = D.Job_FK and
((P.DimensionCriteria = 'Pricing Rule' and D.DimensionCriteria = 'Price Rule Operation') or
(P.DimensionCriteria = 'Customer Rebate Rule' and D.DimensionCriteria = 'Customer Rule Operation') or
(P.DimensionCriteria = 'Buy Rebate Rule' and D.DimensionCriteria = 'Buy Rule Operation'))

Drop Table ##Dim_Upd

Insert into dbo.MAUI_Dimensions
Select 
Job_FK
, 2 DimensionLevel
, Upper(Sell_Price_Cust_Filter_Name) DimensionName
, Null DimensionValue
, Null DimensionsDescription
, 'Price Rule: Customer' DimensionCriteria
, Null ComponentCust
, Null ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis with (nolock) where job_FK = @Job_FK
and Sell_Price_Cust_Filter_Name not in (Select DimensionName from dbo.MAUI_Dimensions with (nolock) where DimensionLevel = 2 
	and Sell_Price_Cust_Filter_Name is not null and job_FK = @Job_FK)
and Sell_Price_Cust_Filter_Name is not null 
Group by Job_FK, Sell_Price_Cust_Filter_Name
Order by Sell_Price_Cust_Filter_Name

Insert into dbo.MAUI_Dimensions
Select 
Job_FK
, 2 DimensionLevel
, Upper(Sell_Price_Prod_Filter_Name) DimensionName
, Null DimensionValue
, Null DimensionsDescription
, 'Price Rule: Product' DimensionCriteria
, Null ComponentCust
, Null ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis with (nolock) where job_FK = @Job_FK
and Sell_Price_Prod_Filter_Name is not null
and Sell_Price_Prod_Filter_Name not in (Select DimensionName from dbo.MAUI_Dimensions with (nolock) where job_FK = @Job_FK and DimensionLevel = 2)
Group by Job_FK, Sell_Price_Prod_Filter_Name
Order by Sell_Price_Prod_Filter_Name

Insert into dbo.MAUI_Dimensions
Select 
Job_FK
, 3 DimensionLevel
, Upper(Sell_Price_Cust_Filter_Name) DimensionName
, Upper(Sell_Price_Cust_Filter_Value) DimensionValue
, Null DimensionsDescription
, 'Price Rule: Customer' DimensionCriteria
, Null ComponentCust
, Null ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis with (nolock) where job_FK = @Job_FK
and Sell_Price_Cust_Filter_Name not in (Select DimensionName from dbo.MAUI_Dimensions with (nolock) where job_FK = @Job_FK and DimensionLevel = 3)
and Sell_Price_Cust_Filter_Name is not null
Group by Job_FK, Sell_Price_Cust_Filter_Name, Sell_Price_Cust_Filter_Value
Order by Sell_Price_Cust_Filter_Name, Sell_Price_Cust_Filter_Value

--/*
Insert into dbo.MAUI_Dimensions
Select Distinct @Job_FK 
, 3 DimensionLevel
, Upper('PD PRICE RECORD TYPE') DimensionName
, Upper(isnull([PRICE_RULE_TYPE], 'Default Pricing')) DimensionValue
, '' DimensionDescription
, 'Price Rule: Customer' DimensionCriteria
, Null ComponentCust
, Null ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis MB with (nolock) where job_FK = @Job_FK
and [PRICE_RULE_TYPE] is null
and [PRICE_RULE_TYPE] not in
	(Select DimensionValue from dbo.maui_dimensions with (nolock) where job_fk = @Job_FK and dimensionlevel = 3 and dimensionName = 'PD PRICE RECORD TYPE' 
	and dimensioncriteria = 'Price Rule: Customer')
Group by Job_FK, [PRICE_RULE_TYPE]
--*/

insert into dbo.maui_log Select @Job_FK_i, 'Start MAUI_Dimensions Dynamic SQL', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (nolock) where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log with (nolock) where Job_fk_i = @Job_FK_i)), 108)), Null

Declare @SqlCd as varchar(Max)
Declare @Cols$CMB as Varchar(256)
Declare @Cols$CDC as varchar(256)
Declare @Cols$CMD as varchar(256)
Declare @Cols$GroupAdd as Varchar(256)
Declare @Cols$Job as varchar(256)

DECLARE DimNames Cursor local for
Select ColCMBname CMB
	, Case When ColDisplayCode is null then '''''' when ColDisplayCode = '' then '''''' else ColDisplayCode end CDC
, Replace(Case When ColGroupSec is not null then ', ' + ColGroupSec  + ', ' + ColCommand 
	   When ColDisplayCode is not null then ', ' + ColCommand else '' end, '_cur', '') GroupAdd, Replace(ColCommand, '_cur', '') CMD, @Job_FK from dbo.MAUI_cols cols with (nolock) where ERP = @ERP and ColumnNameMaui is not null
and ColCMBname <> 'Enter Custom Name Here' and Record_Status_CR_FK_Basis = 1 and Record_Status_CR_FK_Basis_Calcs = 1

	     OPEN DimNames;
         FETCH NEXT FROM DimNames INTO @Cols$CMB, @Cols$CDC, @Cols$GroupAdd, @Cols$CMD, @Cols$Job
            
         WHILE @@FETCH_STATUS = 0
Begin
--insert into dbo.maui_log Select @Job_FK_i, 'MAUI_Dimensions D_SQL Cust_Filter 1', getdate(), Null, Null

Set @SqlCD = 
'
Insert into dbo.MAUI_Dimensions
Select ' + @Cols$Job + '
, 3 DimensionLevel
, Upper(Sell_Price_Cust_Filter_Name) DimensionName
, Upper(Sell_Price_Cust_Filter_Value) DimensionValue
, ' + @Cols$CDC + ' DimensionDescription
, ''Price Rule: Customer'' DimensionCriteria
, Null ComponentCust
, Null ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis MB with (nolock) where job_FK =  ' + @Cols$Job + '
and Sell_Price_Cust_Filter_Name = ''' + @Cols$CMB + '''
and Sell_Price_Cust_Filter_Name not in (Select DimensionName from dbo.MAUI_Dimensions with (nolock) where job_FK = ' + @Cols$Job + ' and DimensionLevel = 3)
and Sell_Price_Cust_Filter_Value not in (Select DimensionValue from dbo.MAUI_Dimensions with (nolock) where job_FK = ' + @Cols$Job + ' and DimensionLevel = 3)
Group by Job_FK, Sell_Price_Cust_Filter_Name, Sell_Price_Cust_Filter_Value' + @Cols$GroupAdd + '
Order by Sell_Price_Cust_Filter_Name, Sell_Price_Cust_Filter_Value'

Set @SqlCD = Replace(@SqlCD, ', , ', ', ')

Exec (@SqlCD)
--insert into dbo.maui_log Select @Job_FK_i, 'MAUI_Dimensions D_SQL Prod_Filter 2', getdate(), Null, Null
Set @SqlCD = 

'
Insert into dbo.MAUI_Dimensions
Select ' + @Cols$Job + ' 
, 3 DimensionLevel
, Upper(Sell_Price_Prod_Filter_Name) DimensionName
, Upper(Sell_Price_Prod_Filter_Value) DimensionValue
, ' + @Cols$CDC + ' DimensionDescription
, ''Price Rule: Customer'' DimensionCriteria
, Null ComponentCust
, Null ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis MB with (nolock) where job_FK = ' + @Cols$Job + '
and Sell_Price_Prod_Filter_Name = ''' + @Cols$CMB + '''
and Sell_Price_Prod_Filter_Name not in (Select DimensionName from dbo.MAUI_Dimensions with (nolock) where job_FK = ' + @Cols$Job + ' and DimensionLevel = 3)
and Sell_Price_Prod_Filter_Value not in (Select DimensionValue from dbo.MAUI_Dimensions with (nolock) where job_FK = ' + @Cols$Job + ' and DimensionLevel = 3)
Group by Job_FK, Sell_Price_Prod_Filter_Name, Sell_Price_Prod_Filter_Value' + @Cols$GroupAdd + '
Order by Sell_Price_Prod_Filter_Name, Sell_Price_Prod_Filter_Value'

Set @SqlCD = Replace(@SqlCD, ', , ', ', ')

Exec (@SqlCD)
--insert into dbo.maui_log Select @Job_FK_i, 'MAUI_Dimensions D_SQL @Cols$CMB3', getdate(), Null, Null
Set @SqlCD =
'
Insert into dbo.MAUI_Dimensions
Select Distinct ' + @Cols$Job + ' 
, 3 DimensionLevel
, Upper(' + '''' + @Cols$CMB + '''' + ') DimensionName
, Upper(' + @Cols$CMD + ') DimensionValue
, ' + @Cols$CDC + ' DimensionDescription
, ''Price Rule: Customer'' DimensionCriteria
, Null ComponentCust
, Null ComponentProd
, Null DimensionAffiliation
from dbo.MAUI_Basis MB with (nolock) where job_FK = ' + @Cols$Job + '
and ' + @Cols$CMD + ' is not null
and ' + @Cols$CMD + ' not in
	(Select DimensionValue from dbo.maui_dimensions with (nolock) where job_fk = ' + @Cols$Job + ' and dimensionlevel = 3 and dimensionName = ' + '''' + @Cols$CMB + '''' + ' and dimensioncriteria = ''Price Rule: Customer'')
Group by Job_FK' + @Cols$GroupAdd + Case When @Cols$GroupAdd not like '%' + @Cols$CMD + '%' then ',' + @Cols$CMD else '' end

Set @SqlCD = Replace(@SqlCD, ', , ', ', ')

Exec (@SqlCD)
--insert into dbo.maui_log Select @Job_FK_i, 'MAUI_Dimensions D_SQL 4', getdate(), Null, Null
Set @SqlCd = 
'
Insert into dbo.MAUI_Dimensions(Job_FK, DimensionLevel, DimensionName, DimensionCriteria)
Select Top 1 ' + @Cols$Job + ' Job_FK, 2 DimensionLevel, ' + '''' + @Cols$CMB + '''' + ' DimensionName, ''Non-Rule Product'' DimensionCriteria 
from epacube.data_name with (nolock)
Where ' + '''' + @Cols$CMB + '''' + ' not in (Select DimensionName from dbo.MAUI_Dimensions with (nolock) where Job_FK = ' + @Cols$Job + ' and DimensionLevel = 2)'

Set @SqlCD = Replace(@SqlCD, ', , ', ', ')

Exec (@SqlCD)

Set @Cols$CMD = Replace(@Cols$CMD, 'Sell_Price_Cust_Effective_Date', 'Sell_Price_Cust_Effective_Date_Cur')
Set @Cols$GroupAdd = Replace(@Cols$GroupAdd, 'Sell_Price_Cust_Effective_Date', 'Sell_Price_Cust_Effective_Date_Cur')

Set @SqlCd = 
'
Select distinct DimensionValue into ##tmp_dims from dbo.MAUI_Dimensions with (nolock) where job_fk = ' + @Cols$Job + ' and DimensionLevel = 3 and DimensionName =  ' + '''' + @Cols$CMB + '''' + '
Create index idx_td on ##tmp_dims(DimensionValue)

Insert into dbo.MAUI_Dimensions(Job_FK, DimensionLevel, DimensionName, DimensionValue, DimensionDescription, DimensionCriteria)
Select Distinct ' + @Cols$Job + ' Job_FK, 3 DimensionLevel, ' + '''' + @Cols$CMB + '''' + ' DimensionName, ' + @Cols$CMD + ' DimensionValue, ' + @Cols$CDC + ' DimensionDescription, ''Non-Rule Product'' DimensionCriteria 
	from dbo.MAUI_Basis_Calcs MBC with (nolock)
	left join ##tmp_dims TD with (nolock) on ' + Replace(@Cols$CMD, '[', 'MBC.[') + ' = TD.DimensionValue
	where ' + Replace(@Cols$CMD, '[', 'MBC.[') + ' is not null and TD.DimensionValue is null
	Group by ' + Replace(@Cols$CMD, '[', 'MBC.[') + Replace(@Cols$GroupAdd, '[', 'MBC.[') + ' Order by ' + Replace(@Cols$CMD, '[', 'MBC.[') + '
	drop table ##tmp_dims'

Set @SqlCD = Replace(@SqlCD, ', , ', ', ')

Exec (@SqlCD)


         FETCH NEXT FROM DimNames INTO @Cols$CMB, @Cols$CDC, @Cols$GroupAdd, @Cols$CMD, @Cols$Job

End
Close DimNames;
Deallocate DimNames;

insert into dbo.maui_log Select @Job_FK_i, 'MAUI Dimensions Loaded', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (nolock) where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI_Dimensions'), 108)), (Select COUNT(*) from dbo.MAUI_Dimensions with (nolock) where Job_FK = @Job_FK)

END
