﻿







-- Copyright 2006 epaCUBE, Inc.
--
-- Procedure created by Walt Tucholski
--
-- Purpose: Assign taxonomy nodes to products.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- WT        10/09/2006   Initial SQL Version
-- WT        11/15/2006   Modified to use sx product id from import table
--						  instead of product_structure_fk (i.e., key off of sx_product_id).
--						  Also add logic to insert/update print sequence.
--
CREATE PROCEDURE [import].[PRODUCT_TAXONOMY_IMPORT] ( @in_job_fk bigint )

AS
BEGIN

DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int
DECLARE @V_COUNT			 bigint
DECLARE @node_name			 varchar (100)
DECLARE @product_structure_fk bigint
DECLARE @sx_product_id		 varchar(64)
DECLARE @cnt				 int


BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0
SET @ErrorMessage = null

SET @ls_stmt = 'started execution of IMPORT.PRODUCT_TAXONOMY_IMPORT.' + ' Job FK = '
				 + cast(isnull(@in_job_fk,'NULL') as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
								   		  @exec_id   = @l_exec_no OUTPUT;

SET @l_sysdate = getdate()

--verify master node names exist
SELECT top 1 @node_name = MASTER_NODE_NAME
FROM IMPORT.IMPORT_PRODUCT_TAXONOMY
WHERE JOB_FK = @in_job_fk
	AND MASTER_NODE_NAME IS NOT NULL
	AND MASTER_NODE_NAME <> SPACE(1)
	AND MASTER_NODE_NAME NOT IN
		(SELECT TAXONOMY_NODE_NAME FROM epacube.TAXONOMY_NODE)

SET @l_rows_processed = @@ROWCOUNT
IF @l_rows_processed > 0
BEGIN
	SET @ErrorMessage = 'Master node name ' + @node_name + ' on import not found on epacube.TAXONOMY_NODE table'
    GOTO RAISE_ERROR
END
--verify sub node names exist
SELECT top 1 @node_name = SUB_NODE_NAME	
FROM IMPORT.IMPORT_PRODUCT_TAXONOMY
WHERE JOB_FK = @in_job_fk 
	AND SUB_NODE_NAME IS NOT NULL
    AND SUB_NODE_NAME <> SPACE(1)
	AND SUB_NODE_NAME NOT IN
		(SELECT TAXONOMY_NODE_NAME FROM epacube.TAXONOMY_NODE)

SET @l_rows_processed = @@ROWCOUNT
IF @l_rows_processed > 0
BEGIN
	SET @ErrorMessage = 'Sub node name ' + @node_name + ' on import not found on epacube.TAXONOMY_NODE table'
    GOTO RAISE_ERROR
END
--verify second sub node names exist
SELECT top 1 @node_name = SECOND_SUB_NODE_NAME	
FROM IMPORT.IMPORT_PRODUCT_TAXONOMY
WHERE JOB_FK = @in_job_fk 
  AND SECOND_SUB_NODE_NAME IS NOT NULL
  AND SECOND_SUB_NODE_NAME <> SPACE(1)
  AND SECOND_SUB_NODE_NAME NOT IN
		(SELECT TAXONOMY_NODE_NAME FROM epacube.TAXONOMY_NODE)

SET @l_rows_processed = @@ROWCOUNT
IF @l_rows_processed > 0
BEGIN
	SET @ErrorMessage = 'Second Sub node name ' + @node_name + ' on import not found on epacube.TAXONOMY_NODE table'
    GOTO RAISE_ERROR
END
--verify third sub node names exist
SELECT top 1 @node_name = THIRD_SUB_NODE_NAME	
FROM IMPORT.IMPORT_PRODUCT_TAXONOMY
WHERE JOB_FK = @in_job_fk 
  AND THIRD_SUB_NODE_NAME IS NOT NULL
  AND THIRD_SUB_NODE_NAME <> SPACE(1)
  AND THIRD_SUB_NODE_NAME NOT IN
		(SELECT TAXONOMY_NODE_NAME FROM epacube.TAXONOMY_NODE)

SET @l_rows_processed = @@ROWCOUNT
IF @l_rows_processed > 0
BEGIN
	SET @ErrorMessage = 'Third Sub node name ' + @node_name + ' on import not found on epacube.TAXONOMY_NODE table'
    GOTO RAISE_ERROR
END
--
-- verify there are no duplicate sx product ids on import file. Procedure will abend
-- during insert to product_taxonomy table if there are duplicates because more than
-- one product structure fk will be inserted in the temp table and hence the product_taxonomy table.
SELECT TOP 1 @SX_PRODUCT_ID = a.SX_PRODUCT_ID,
			 @cnt = a.cnt
FROM
(
SELECT DISTINCT SX_PRODUCT_ID, count(*) cnt
FROM IMPORT.IMPORT_PRODUCT_TAXONOMY
WHERE JOB_FK = @IN_JOB_FK
  AND MASTER_NODE_NAME IS NOT NULL
  AND MASTER_NODE_NAME <> SPACE(1)
  AND SUB_NODE_NAME IS NOT NULL
  AND SUB_NODE_NAME <> SPACE(1)
  AND SX_PRODUCT_ID IS NOT NULL
  AND SX_PRODUCT_ID <> SPACE(1)
group by sx_product_id
having count(*) > 1
) a

SET @l_rows_processed = @@ROWCOUNT
IF @l_rows_processed > 0
BEGIN
	SET @ErrorMessage = 'Duplicate SX_PRODUCT_ID ' + @SX_PRODUCT_ID + ' found on import table.'
    GOTO RAISE_ERROR
END
--create temp table
IF object_id('tempdb..#TS_PRODUCT_TAXONOMY') is not null
   drop table #TS_PRODUCT_TAXONOMY

CREATE TABLE #TS_PRODUCT_TAXONOMY
(
PRODUCT_STRUCTURE_FK bigint not null,
TAXONOMY_TREE_FK int null,
TAXONOMY_NODE_FK int null,
PRINT_SEQ varchar(64) null
)

INSERT INTO #TS_PRODUCT_TAXONOMY
   (PRODUCT_STRUCTURE_FK
   ,TAXONOMY_TREE_FK
   ,TAXONOMY_NODE_FK
   ,PRINT_SEQ)
SELECT 
     B.PRODUCT_STRUCTURE_FK
    ,B.TAXONOMY_TREE_FK
--    ,CASE ISNULL (B.TAX_NODE_LEVEL2_FK, 0 )
--		 WHEN 0 THEN B.TAX_NODE_LEVEL1_FK
--		 ELSE B.TAX_NODE_LEVEL2_FK
--     END AS TAXONOMY_NODE_FK
--    ,CASE ISNULL (B.TAX_NODE_LEVEL3_FK, 0 )
--		 WHEN 0 THEN ISNULL (B.TAX_NODE_LEVEL2_FK,1)
--		 WHEN 1 THEN B.TAX_NODE_LEVEL1_FK
--		 ELSE B.TAX_NODE_LEVEL1_FK
--     END AS TAXONOMY_NODE_FK
    ,CASE WHEN B.TAX_NODE_LEVEL3_FK IS NOT NULL
		    THEN B.TAX_NODE_LEVEL3_FK
          WHEN B.TAX_NODE_LEVEL2_FK IS NOT NULL
		    THEN B.TAX_NODE_LEVEL2_FK
		  ELSE B.TAX_NODE_LEVEL1_FK
     END AS TAXONOMY_NODE_FK
	,B.PRINT_SEQ
FROM (
--
		SELECT A.PRODUCT_STRUCTURE_FK
			  ,A.TAX_NODE_LEVEL1_FK
			  ,A.TAXONOMY_TREE_FK
			  ,A.SUB_NODE_NAME
			  ,A.SECOND_SUB_NODE_NAME
			  ,A.THIRD_SUB_NODE_NAME
			  ,( SELECT TN.TAXONOMY_NODE_ID
				FROM EPACUBE.TAXONOMY_NODE TN
				INNER JOIN epacube.Get_Taxonomy_Children ( A.TAX_NODE_LEVEL1_FK ) AS GTC
				  ON ( GTC.TAXONOMY_NODE_ID = TN.TAXONOMY_NODE_ID )
				WHERE TN.TAXONOMY_NODE_NAME = A.SECOND_SUB_NODE_NAME 
				AND   TN.LEVEL_SEQ = 2 ) AS TAX_NODE_LEVEL2_FK 
			  ,( SELECT TN.TAXONOMY_NODE_ID
				FROM EPACUBE.TAXONOMY_NODE TN
				INNER JOIN epacube.Get_Taxonomy_Children ( A.TAX_NODE_LEVEL2_FK ) AS GTX
				  ON ( GTX.TAXONOMY_NODE_ID = TN.TAXONOMY_NODE_ID )
				WHERE TN.TAXONOMY_NODE_NAME = A.THIRD_SUB_NODE_NAME 
				AND   TN.LEVEL_SEQ = 3 ) AS TAX_NODE_LEVEL3_FK  
 			  ,A.PRINT_SEQ
		FROM (
		SELECT
			 PI.PRODUCT_STRUCTURE_FK
			,( SELECT TAXONOMY_TREE_ID FROM epacube.TAXONOMY_TREE
			   WHERE NAME = IPT.MASTER_NODE_NAME ) AS TAXONOMY_TREE_FK
			,( SELECT TN.TAXONOMY_NODE_ID
				FROM EPACUBE.TAXONOMY_NODE TN
				WHERE TN.TAXONOMY_NODE_NAME = IPT.SUB_NODE_NAME 
				AND   TN.LEVEL_SEQ = 1 ) AS TAX_NODE_LEVEL1_FK
-- PUT TOP 1 IN THE NEXT QUERY??
--			,( SELECT TN.TAXONOMY_NODE_ID
--				FROM EPACUBE.TAXONOMY_NODE TN
--				WHERE TN.TAXONOMY_NODE_NAME = IPT.SECOND_SUB_NODE_NAME 
--				AND   TN.LEVEL_SEQ = 2 ) AS TAX_NODE_LEVEL2_FK
			,( SELECT TN.TAXONOMY_NODE_ID
				FROM EPACUBE.TAXONOMY_NODE TN
				INNER JOIN EPACUBE.TAXONOMY_NODE TX ON (TX.TAXONOMY_NODE_ID = TN.PARENT_TAXONOMY_NODE_FK)
				WHERE TN.TAXONOMY_NODE_NAME = IPT.SECOND_SUB_NODE_NAME 
				AND   TN.LEVEL_SEQ = 2
				AND   TX.TAXONOMY_NODE_NAME = IPT.SUB_NODE_NAME
				AND   TX.LEVEL_SEQ = 1 ) AS TAX_NODE_LEVEL2_FK
			,IPT.SUB_NODE_NAME
			,IPT.SECOND_SUB_NODE_NAME
			,IPT.THIRD_SUB_NODE_NAME
			,IPT.PRINT_SEQ
		FROM IMPORT.IMPORT_PRODUCT_TAXONOMY IPT
		INNER JOIN EPACUBE.PRODUCT_IDENTIFICATION PI
			 ON (PI.VALUE = LTRIM(RTRIM(IPT.SX_PRODUCT_ID)))
        WHERE IPT.JOB_FK = @in_job_fk
		  AND IPT.MASTER_NODE_NAME IS NOT NULL
          AND IPT.MASTER_NODE_NAME <> SPACE(1)
		  AND IPT.SUB_NODE_NAME IS NOT NULL
          AND IPT.SUB_NODE_NAME <> SPACE(1)
		  AND IPT.SX_PRODUCT_ID IS NOT NULL
          AND IPT.SX_PRODUCT_ID <> SPACE(1)
		  AND PI.DATA_NAME_FK = 100	
		) A
        WHERE a.TAXONOMY_TREE_FK IS NOT NULL
          AND a.PRODUCT_STRUCTURE_FK IS NOT NULL
) B

SET @l_rows_processed = @@ROWCOUNT
SET @status_desc = 'Total rows INSERTED into the #TS_PRODUCT_TAXONOMY table = '
	+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc


-- UPDATE EXISTING PRODUCTS WITH TAXONOMY NODE CHANGES
UPDATE epacube.PRODUCT_TAXONOMY
   SET TAXONOMY_TREE_FK = TPT.TAXONOMY_TREE_FK
	  ,TAXONOMY_NODE_FK = TPT.TAXONOMY_NODE_FK
	  ,UPDATE_TIMESTAMP = @l_sysdate
FROM #TS_PRODUCT_TAXONOMY TPT
WHERE epacube.PRODUCT_TAXONOMY.PRODUCT_STRUCTURE_FK = TPT.PRODUCT_STRUCTURE_FK 
  AND TPT.PRODUCT_STRUCTURE_FK IS NOT NULL
  AND TPT.TAXONOMY_TREE_FK IS NOT NULL
  AND TPT.TAXONOMY_NODE_FK IS NOT NULL

SET @l_rows_processed = @@ROWCOUNT
SET @status_desc = 'Total rows UPDATED in the epacube.PRODUCT_TAXONOMY table = '
	+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc

-- ASSIGN NEW TAXONOMY NODES TO PRODUCTS
INSERT INTO epacube.PRODUCT_TAXONOMY
	(PRODUCT_STRUCTURE_FK
	,TAXONOMY_TREE_FK
	,TAXONOMY_NODE_FK)
SELECT  TPT.PRODUCT_STRUCTURE_FK
	   ,TPT.TAXONOMY_TREE_FK
	   ,TPT.TAXONOMY_NODE_FK
FROM  #TS_PRODUCT_TAXONOMY TPT
WHERE TPT.PRODUCT_STRUCTURE_FK IS NOT NULL
  AND TPT.TAXONOMY_TREE_FK IS NOT NULL
  AND TPT.TAXONOMY_NODE_FK IS NOT NULL
  AND NOT EXISTS
  (SELECT 1 FROM epacube.PRODUCT_TAXONOMY PT
	WHERE PT.PRODUCT_STRUCTURE_FK = TPT.PRODUCT_STRUCTURE_FK)

SET @l_rows_processed = @@ROWCOUNT
SET @status_desc = 'Total rows INSERTED into the epacube.PRODUCT_TAXONOMY table = '
	+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc

-- UPDATE EXISTING PRINT SEQUENCES
UPDATE epacube.PRODUCT_ATTRIBUTE
   SET ATTRIBUTE_CHAR = TPT.PRINT_SEQ
	  ,UPDATE_TIMESTAMP = @l_sysdate
FROM #TS_PRODUCT_TAXONOMY TPT
WHERE epacube.PRODUCT_ATTRIBUTE.PRODUCT_STRUCTURE_FK = TPT.PRODUCT_STRUCTURE_FK 
  AND DATA_NAME_FK = 851
  AND TPT.PRODUCT_STRUCTURE_FK IS NOT NULL
  AND TPT.TAXONOMY_TREE_FK IS NOT NULL
  AND TPT.TAXONOMY_NODE_FK IS NOT NULL
  AND TPT.PRINT_SEQ IS NOT NULL
  AND TPT.PRINT_SEQ <> SPACE(1)

SET @l_rows_processed = @@ROWCOUNT
SET @status_desc = 'Total rows UPDATED in the epacube.PRODUCT_ATTRIBUTE table = '
	+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc

-- ASSIGN NEW PRINT SEQUENCES
INSERT INTO epacube.PRODUCT_ATTRIBUTE
	(PRODUCT_STRUCTURE_FK
	,DATA_NAME_FK
	,ORG_ENTITY_STRUCTURE_FK
    ,ATTRIBUTE_CHAR)
SELECT  TPT.PRODUCT_STRUCTURE_FK
	   ,851
	   ,1
	   ,TPT.PRINT_SEQ
FROM  #TS_PRODUCT_TAXONOMY TPT
WHERE TPT.PRODUCT_STRUCTURE_FK IS NOT NULL
  AND TPT.TAXONOMY_TREE_FK IS NOT NULL
  AND TPT.TAXONOMY_NODE_FK IS NOT NULL
  AND TPT.PRINT_SEQ IS NOT NULL
  AND TPT.PRINT_SEQ <> SPACE(1)
  AND NOT EXISTS
  (SELECT 1 FROM epacube.PRODUCT_ATTRIBUTE PA
	WHERE PA.PRODUCT_STRUCTURE_FK = TPT.PRODUCT_STRUCTURE_FK
	  AND DATA_NAME_FK = 851)

SET @l_rows_processed = @@ROWCOUNT
SET @status_desc = 'Total rows INSERTED into the epacube.PRODUCT_ATTRIBUTE table = '
	+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc

--drop temp table
IF object_id('tempdb..#TS_PRODUCT_TAXONOMY') is not null
   drop table #TS_PRODUCT_TAXONOMY
--
RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
--		return(-1)
    END
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of IMPORT.PRODUCT_TAXONOMY_IMPORT'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of IMPORT.PRODUCT_TAXONOMY_IMPORT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END








