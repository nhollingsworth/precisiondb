﻿















-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        01/12/2011   Initial Version 
-- CV        02/19/2011   Added second historical price sheet insert
-- CV        02/22/2011   Remapped pricesheet netvalues to match current price sheet


CREATE PROCEDURE [import].[LOAD_ECLIPSE_HIST_PRICESHEETS] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_ECLIPSE_HIST_PRICESHEETS', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.IMPORT_ECLIPSE_PRICE_HISTORY WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM import.IMPORT_ECLIPSE_PRICE_HISTORY WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD HISTORICAL PRICE SHEETS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;




-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( JOB_ERRORS TABLE )
--
--     THESE ERRORS PROHIBIT THE ROWS FROM BEING IMPORTED
--
--	   ERROR NAME:  INCOMPLETE OR INVALID DATA PROHIBITING IMPORT 
--     EXAMPLE IS MISSING OR INVALID STRUCTURE
--
--     IMPORT ROW MARKED  ERROR_ON_IMPORT_IND = 1 
--
-------------------------------------------------------------------------------------

UPDATE import.IMPORT_ECLIPSE_PRICE_HISTORY
SET PRODUCT_STRUCTURE_FK = (select pi.product_Structure_Fk
FROM epacube.product_identification pi
where import.IMPORT_ECLIPSE_PRICE_HISTORY.ProductID = pi.value
and pi.data_name_FK = 310101)
WHERE 1 = 1                   
AND   JOB_FK = @IN_JOB_FK 

UPDATE import.IMPORT_ECLIPSE_PRICE_HISTORY
SET ORG_ENTITY_STRUCTURE_FK = (select ei.entity_Structure_Fk
FROM epacube.entity_identification ei
where import.IMPORT_ECLIPSE_PRICE_HISTORY.PriceSheetID = ei.value
and ei.entity_data_name_FK = (select (Case when BranchType = 'Default' then 141099  --all whse
				when BranchType = 'Branch' then 141000 --Whse
				when BranchType = 'Territory' then 311030 --ecl territory
				else null
				end)
))

WHERE 1 = 1                   
AND   JOB_FK  = @IN_JOB_FK 


update import.IMPORT_ECLIPSE_PRICE_HISTORY
set error_on_import_ind = 1
where product_Structure_Fk is  NULL
and job_fk = @in_job_Fk


update import.IMPORT_ECLIPSE_PRICE_HISTORY
set error_on_import_ind = 1
where org_entity_Structure_Fk is  NULL
and job_fk = @in_job_Fk



INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[TABLE_PK]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[ERROR_DATA4]
--           ,[ERROR_DATA5]
--           ,[ERROR_DATA6]
           ,[CREATE_TIMESTAMP])
SELECT 
JOB_FK
,'IMPORT HISTORICAL PRICE SHEETS'
,NULL
,99
,'MISSING PRODUCT/ORG INFORMATION'
,ProductID
,PriceSheetID
,effectivedate
,NULL
,getdate()
FROM import.IMPORT_ECLIPSE_PRICE_HISTORY
WHERE ISNULL(ERROR_ON_IMPORT_IND ,0) = 1
AND PRODUCT_STRUCTURE_FK is NULL


-------------------------------------------------------------------------------------
--   MATCH PRODUCTS
--		1st 6 net values
-------------------------------------------------------------------------------------

TRUNCATE TABLE [marginmgr].[PRICESHEET]


INSERT INTO [marginmgr].[PRICESHEET]
           ([PRICESHEET_NAME]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[RESULT_TYPE_CR_FK]
           ,[NET_VALUE1]
           ,[NET_VALUE2]
           ,[NET_VALUE3]
           ,[NET_VALUE4]
           ,[NET_VALUE5]
           ,[NET_VALUE6]
           ,[PRICESHEET_QTY_PER]
           ,[VALUE_UOM_CODE_FK]
           ,[UOM_CODE]
           ,[PROD_ID_DN]
           ,[PROD_ID_VALUE]
           ,[ORG_ID_ENTITY_DN]
           ,[ORG_ID_DN]
           ,[ORG_ID_VALUE]
           ,[ENTITY_ID_ENTITY_DN]
           ,[ENTITY_ID_DN]
           ,[ENTITY_ID_VALUE]
           ,[CURRENCY_CR_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
(SELECT 
IEPH.PriceSheetID -- price sheet name
,381600  --	ECL PRICESHEET HISTORY
,IEPH.PRODUCT_STRUCTURE_FK
,IEPH.ORG_ENTITY_STRUCTURE_FK --         ,[ORG_ENTITY_STRUCTURE_FK]
,NULL  --         ,[ENTITY_STRUCTURE_FK]
,IEPH.EffectiveDate  --,[EFFECTIVE_DATE]
,NULL --,[END_DATE]
,713 --historical? --,[RESULT_TYPE_CR_FK]
           ,IEPH.IMPA  --,[NET_VALUE1]
           ,IEPH.REPLCost     --[NET_VALUE2]
           ,IEPH.STDCost   --[NET_VALUE3]
           ,IEPH.BURDENCOST   --[NET_VALUE4]
           ,NULL     --[NET_VALUE5]
           ,NULL   -- base [NET_VALUE6]
           ,IEPH.PricePer  ---[PRICESHEET_QTY_PER]
           ,(select UOM_CODE_ID FROM epacube.uom_code with (NOLOCK)
			Where uom_code = IEPH.PRICEUOM)    ---[VALUE_UOM_CODE_FK]
           ,IEPH.PRICEUOM   --[UOM_CODE]
           ,310101  --ECL PROD FILE ID [PROD_ID_DN]
           ,IEPH.PRODUCTID   ---[PROD_ID_VALUE]
           ,(select (Case when BranchType = 'Default' then 141099  --all whse
				when BranchType = 'Branch' then 141000 --Whse
				when BranchType = 'Territory' then 311030 --ecl territory
				else null
				end) ) --[ORG_ID_ENTITY_DN]
				,141111
    ----       ,(select (Case when BranchType = 'Default' then 141111  --all whse
				----when BranchType = 'Branch' then 141111 --Whse
				----when BranchType = 'Territory' then 141111 --ecl territory
				----else null
				----end) )  --[ORG_ID_DN]
           ,IEPH.PriceSheetID
           ,NULL  --[ENTITY_ID_ENTITY_DN]
           ,NULL  --[ENTITY_ID_DN]
           ,NULL ---[ENTITY_ID_VALUE]
           ,1200   --USD  --[CURRENCY_CR_FK]
           ,1
           ,getdate()
           ,NULL
           ,NULL
 FROM import.IMPORT_ECLIPSE_PRICE_HISTORY IEPH
WHERE 1 = 1 
AND ISNULL(ERROR_ON_IMPORT_IND,0) <> 1
AND IEPH.JOB_FK = @IN_JOB_FK)


-------------------------------------------------------------------------------------
--   MATCH PRODUCTS
--		2st 6 net values
-------------------------------------------------------------------------------------

INSERT INTO [marginmgr].[PRICESHEET]
           ([PRICESHEET_NAME]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[RESULT_TYPE_CR_FK]
           ,[NET_VALUE1]
           ,[NET_VALUE2]
           ,[NET_VALUE3]
           ,[NET_VALUE4]
           ,[NET_VALUE5]
           ,[NET_VALUE6]
           ,[PRICESHEET_QTY_PER]
           ,[VALUE_UOM_CODE_FK]
           ,[UOM_CODE]
           ,[PROD_ID_DN]
           ,[PROD_ID_VALUE]
           ,[ORG_ID_ENTITY_DN]
           ,[ORG_ID_DN]
           ,[ORG_ID_VALUE]
           ,[ENTITY_ID_ENTITY_DN]
           ,[ENTITY_ID_DN]
           ,[ENTITY_ID_VALUE]
           ,[CURRENCY_CR_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
(SELECT 
IEPH.PriceSheetID -- price sheet name
,381700  --	ECL PRICESHEET HISTORY sub sheet
,IEPH.PRODUCT_STRUCTURE_FK
,IEPH.ORG_ENTITY_STRUCTURE_FK --         ,[ORG_ENTITY_STRUCTURE_FK]
,NULL  --         ,[ENTITY_STRUCTURE_FK]
,IEPH.EffectiveDate  --,[EFFECTIVE_DATE]
,NULL --,[END_DATE]
,713 --historical? --,[RESULT_TYPE_CR_FK]
           ,IEPH.ListPrice  --,[NET_VALUE1]
           ,IEPH.TSColumn1     --[NET_VALUE2]
           ,IEPH.TSColumn2   --[NET_VALUE3]
           ,IEPH.TSColumn3   --[NET_VALUE4]
           ,IEPH.TSCost     --[NET_VALUE5]
           ,NULL   -- base [NET_VALUE6]
           ,IEPH.PricePer  ---[PRICESHEET_QTY_PER]
           ,(select UOM_CODE_ID FROM epacube.uom_code with (NOLOCK)
			Where uom_code = IEPH.PRICEUOM)    ---[VALUE_UOM_CODE_FK]
           ,IEPH.PRICEUOM   --[UOM_CODE]
           ,310101  --ECL PROD FILE ID [PROD_ID_DN]
           ,IEPH.PRODUCTID   ---[PROD_ID_VALUE]
           ,(select (Case when BranchType = 'Default' then 141099  --all whse
				when BranchType = 'Branch' then 141000 --Whse
				when BranchType = 'Territory' then 311030 --ecl territory
				else null
				end) ) --[ORG_ID_ENTITY_DN]
				,141111
    ----       ,(select (Case when BranchType = 'Default' then 141111  --all whse
				----when BranchType = 'Branch' then 141111 --Whse
				----when BranchType = 'Territory' then 141111 --ecl territory
				----else null
				----end) )  --[ORG_ID_DN]
           ,IEPH.PriceSheetID
           ,NULL  --[ENTITY_ID_ENTITY_DN]
           ,NULL  --[ENTITY_ID_DN]
           ,NULL ---[ENTITY_ID_VALUE]
           ,1200   --USD  --[CURRENCY_CR_FK]
           ,1
           ,getdate()
           ,NULL
           ,NULL
 FROM import.IMPORT_ECLIPSE_PRICE_HISTORY IEPH
WHERE 1 = 1 
AND ISNULL(ERROR_ON_IMPORT_IND,0) <> 1
AND IEPH.JOB_FK = @IN_JOB_FK)




--------------------------------------------------------------------------

----add some logging

 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD HISTORICAL PRICE SHEETS' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD HISTORICAL PRICE SHEETS' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( RULES_ERRORS TABLE )
--
--     ADD RULES VALIDATION ROUTINE TO BE CALLED FROM UI LATER.. 
--
--	   THESE ERRORS ARE BUSINESS LOGIC ERRORS 
--        WHICH WILL BE BY RULE_TYPE_CR_FK AND RULES_ACTION_OPERATION_FK
--     
--     RULE WILL BE WRITTEN TO RULES TABLE BUT WITH AN ERROR CONDITION
--
--
-------------------------------------------------------------------------------------






 SET @status_desc =  'finished execution of import.LOAD_ECLIPSE_HIST_PRICESHEETS'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_ECLIPSE_HIST_PRICESHEETS has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END



















