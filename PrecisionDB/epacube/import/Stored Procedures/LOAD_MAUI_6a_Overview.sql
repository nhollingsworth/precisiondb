﻿--A_epaMAUI_LOAD_MAUI_6a_Overview

---- GHS  6/14/2016	Converted Global Temp Tables to Local Temp Tables

CREATE PROCEDURE [import].[LOAD_MAUI_6a_Overview] 
	@RbtsA Varchar(8), @ColumnsA varchar(64), @GroupA varchar(64), @ClassA varchar(64), @WIF Varchar(8), @HD Varchar(8), @UFD Varchar(8), @PD Varchar(8), @Job Varchar(8), @Job_i Varchar(8)
AS
BEGIN

	SET NOCOUNT ON;
Declare @SQLcd as Varchar(Max)
Declare @SQLcdB as Varchar(Max)
Declare @SQLcdC as Varchar(Max)
Declare @SQLcdD as Varchar(Max)

Set @SQLcd = '

Insert into dbo.MAUI_Basis_Overview

Select * from (
select ' + @ColumnsA + '
, Cast(Sum(isnull([OverRide_Impact_Amt], 0) * [OverRide Qty Proj]) as Numeric(18, 2))  ''Projected Override Impact''
, Cast(Sum(Sales_Qty) as Numeric(10, 1)) ''Sales_Qty''
, Min(CONVERT(VARCHAR(8), [Sales_History_Start_Date], 1)) + ''-''+ Max(CONVERT(VARCHAR(8), [Sales_History_End_Date], 1)) ''Sales_History_Date_Range''
, Cast(Sum(Sales_Dollars_Hist - ([Sales_Mult] * Rebate_Sell_Amount)) as Numeric(18, 4))''Sales_Dollars_Hist''
, Cast(Sum([Prod_Cost_Dollars_History]) as Numeric(18, 2))''Prod_Cost_Dollars_Hist''
, Cast(Sum([Sales_Dollars_Hist] - ([Sales_Mult] * Rebate_Sell_Amount) - [Prod_Cost_Dollars_History]) as Numeric(18, 4))''Margin_Dollars_Hist''
, Cast((Select Case When Sum(Sales_Dollars_Hist- ([Sales_Mult] * Rebate_Sell_Amount)) = 0 then 0 When MRG > 9.9 then 9.9 When MRG < -9.9 then -9.9 else MRG end from 
 (Select Sum(([Sales_Dollars_Hist] - ([Sales_Mult] * Rebate_Sell_Amount) - [Prod_Cost_Dollars_History])) / Sum(Sales_Dollars_Hist - ([Sales_Mult] * Rebate_Sell_Amount)) MRG) T) as Numeric(18, 4)) ''Margin_Pct_Hist''
, Cast(Sum([Sales_Dollars_Current]) as Numeric(18, 2))''Sales_Dollars_Cur''
, Cast(Sum([Cost_Dollars_Current]) as Numeric(18, 2))''Cost_Dollars_Cur''
, Cast(Sum([Sales_Dollars_Current] - [Cost_Dollars_Current]) as Numeric(18, 2))''Margin_Dollars_Cur''
, Cast((Select Case When Sum([Sales_Dollars_Current]) = 0 then 0 When MRG > 9.9 then 9.9 When MRG < -9.9 then -9.9 else MRG end from 
 (Select Sum([Sales_Dollars_Current] - [Cost_Dollars_Current]) / Sum([Sales_Dollars_Current])  MRG) T) as numeric(18, 4)) ''Margin_Pct_Cur''
, Cast(Sum([Sales_Dollars_NewA]) as Numeric(18, 2))''Sales_Dollars_New''
, Cast(Sum([Cost_Dollars_NewA]) as Numeric(18, 2))''Cost_Dollars_New''
, Cast(Sum([Sales_Dollars_NewA] - [Cost_Dollars_NewA]) as Numeric(18, 2))''Margin_Dollars_New''
, Cast((Select Case When Sum([Sales_Dollars_NewA]) = 0 then 0 When MRG > 9.9 then 9.9 When MRG < -9.9 then -9.9 else MRG end from 
 (Select Sum([Sales_Dollars_NewA] - [Cost_Dollars_NewA]) / Sum([Sales_Dollars_NewA]) MRG) T) as Numeric(18, 4)) ''Margin_Pct_New''
, Cast(Sum([Sales_Dollars_NewA] - [Sales_Dollars_Current] - [Cost_Dollars_NewA] + [Cost_Dollars_Current]) as Numeric(18, 2))''Margin_Dollars_Change''
, Cast((Select Case When Sum([Sales_Dollars_Current] - [Cost_Dollars_Current]) = 0 then 0 When MRG > 9.9 then 9.9 When MRG < -9.9 then -9.9 else MRG end from 
	(Select Sum([Sales_Dollars_NewA] - [Sales_Dollars_Current] - [Cost_Dollars_NewA] + [Cost_Dollars_Current]) 
	/ Sum([Sales_Dollars_Current] - [Cost_Dollars_Current]) MRG) T) as Numeric(18, 4)) ''Margin_Pct_Change''
, Cast((Select Case When Sum(Sales_Dollars_Current) = 0 then 0 When MRG > 9.9 then 9.9 When MRG < -9.9 then -9.9 else MRG end from 
 (Select Sum(Sales_Dollars_NewA - Sales_Dollars_Current) / Sum(Sales_Dollars_Current) MRG) T) as Numeric(18, 4)) ''Price_Pct_Change_1''

, Cast(Sum(Sales_Dollars_Current - (Sales_Dollars_Hist - Rebate_Sell_Amount_Total)) as Numeric(18, 2))''Sales_Delta_Cur_to_Hist''
, Cast(Sum([Cost_Dollars_Current] - [Prod_Cost_Dollars_History]) as Numeric(18, 2))''Prod_Cost_Delta_Cur_To_Hist''
, Cast(Sum([Sales_Dollars_Current] - ([Sales_Dollars_Hist] - Rebate_Sell_Amount_Total) - [Cost_Dollars_Current] + [Prod_Cost_Dollars_History]) as Numeric(18, 2))''Margin_Dollars_Delta_Cur_To_Hist''
, Cast((Select Case When Sum([Sales_Dollars_Hist]-[prod_cost_dollars_history]) = 0 then 0 When MRG > 9.9 then 9.9 When MRG < -9.9 then -9.9 else MRG end from 
 (Select Sum([Sales_Dollars_Current] - ([Sales_Dollars_Hist] - Rebate_Sell_Amount_Total) - [Cost_Dollars_Current] + [prod_cost_dollars_history])
	/ Sum([Sales_Dollars_Hist]-[prod_cost_dollars_history]) MRG) T) as Numeric(18, 4)) ''Margin_Pct_Delta_Cur_To_Hist''
, Cast((Select Case When Sum(Sales_Dollars_Hist - Rebate_Sell_Amount_Total) = 0 then 0 When MRG > 9.9 then 9.9 When MRG < -9.9 then -9.9 else MRG end from 
 (Select Sum(Sales_Dollars_Current - (Sales_Dollars_Hist - Rebate_Sell_Amount_Total)) / Sum(Sales_Dollars_Hist - Rebate_Sell_Amount_Total) MRG) T) as Numeric(18, 4)) ''Price_Pct_Change_Cur_to_Hist''

, Cast((Select Case When sum([Cost_Dollars_Cur]) = 0 then 0 When MRG > 9.9 then 9.9 When MRG < -9.9 then -9.9 else MRG end from 
 (Select Sum(isnull([Cost_Dollars_New], [Cost_Dollars_Cur])-[Cost_Dollars_Cur])/sum([Cost_Dollars_Cur]) MRG) T) as Numeric(18, 4)) ''Cost_Pct_Change''
,Cast(Sum(((isnull([Margin_Cost_Basis_Amt_2], [Margin_Cost_Basis_Amt])-[Margin_Cost_Basis_Amt]) + [Rebate_Amount_New] - [Rebate_Amount]) 
	* [Sales_Mult]) as Numeric(18, 2)) ''Cost_Dollars_Change_New_To_Cur''
, Cast((Select Case When sum([Margin_Cost_Basis_Amt] - [Rebate_Amount]) = 0 then 0 When MRG > 9.9 then 9.9 When MRG < -9.9 then -9.9 else MRG end from 
	(Select Sum(((isnull([Margin_Cost_Basis_Amt_2], [Margin_Cost_Basis_Amt])-[Margin_Cost_Basis_Amt]) + ((Rebate_Amount_New - Rebate_Amount)))) / sum([Margin_Cost_Basis_Amt] - (Rebate_Amount)) MRG) T) as Numeric(18, 4)) ''Cost_Pct_Change_New_To_Cur''
, Cast((Select Case When sum(isnull([Prod_Cost_Dollars_Hist], 0) - isnull([Rebate_Dollars_Hist], 0)) = 0 then 0 When MRG > 9.9 then 9.9 When MRG < -9.9 then -9.9 else MRG end from 
	(Select Sum(isnull([Margin_Cost_Basis_Amt], [Prod_Cost_Amt_Hist]) - [Prod_Cost_Amt_Hist] 
	+ isnull(Rebate_Amount, 0) - isnull([Rebate_Amount_Hist],0)) / Sum([Prod_Cost_Amt_Hist]) MRG) T) as Numeric(18, 4)) ''Cost_Pct_Change_Cur_To_Hist''
,Case When sum(isnull([Prod_Cost_Dollars_Hist], 0) - isnull([Rebate_Dollars_Hist], 0)) = 0 then 0.99 else
	Cast(Sum((isnull([Margin_Cost_Basis_Amt], [Prod_Cost_Amt_Hist]) - [Prod_Cost_Amt_Hist] + isnull(Rebate_Amount, 0) - isnull([Rebate_Amount_Hist],0))
	* [Sales_Mult]) as Numeric(18, 2))end ''Cost_Dollars_Change_Cur_To_Hist''
, Null ''Cost_Impact_Dollars''
, Null ''Price_Recovery_Dollars''
, Null ''Total_Impact_Dollars''
, Case When isnull(max(Rebate_BUY_Operation), '''') > '''' then ''B'' else ''  '' end + Case When isnull(Max(Rebate_CB_Operation), '''') > '''' then ''C'' else ''  '' end  + Case When isnull(Max(Rebate_Incentive_Amt_cur), 0) > 0 then ''I'' else ''  '' end ''Rebate_Indicator''
, Cast(Sum([OverRide Qty Proj]) as Numeric(18, 2))''Override Qty''
, Cast(Sum([Override Transactions]) as Numeric(18, 2))''Override Transactions''
, Cast(Sum([Total Transactions]) as Numeric(18, 2))''Total Transactions''
, Cast(Sum([Override Qty] / ' + @HD + ' * ' + @PD + ') / Sum([Sales_Qty]) as Numeric(18, 2)) Override_Pct_Qty
, Cast(Sum([Override Transactions]) / Sum([Total Transactions]) as Numeric(18, 2))Override_Pct_Trans
, ' + @Job_i + ' ''Job_FK_i''
, ' + @RbtsA + ' ''Rbts''
, ' + @WIF + ' ''WhatIF_ID''
, ''' + @ClassA  + ''' ''Class''
from '

Set @SQLcdB = '(
Select
Prod_Cost_Dollars_Hist / [Sales_Mult] Prod_Cost_Amt_Hist
, Case Rebate_CB_Dollars_Hist When 0 then Null else Rebate_CB_Dollars_Hist / [Sales_Mult] end Rebate_CB_Amt_Hist
, Case Rebate_Buy_Dollars_Hist When 0 then Null else Rebate_Buy_Dollars_Hist / [Sales_Mult] end Rebate_BUY_Amt_Hist
, Cast([Rebate_Dollars_Hist] / [Sales_Mult] as Numeric(18, 4)) Rebate_Amount_Hist
, isnull(Prod_Cost_Dollars_Hist,0) - [Rebate_Dollars_Hist] Prod_Cost_Dollars_History
, (isnull(Margin_Cost_Basis_Amt,0) - [Rebate_Amount]) * [Sales_Mult] Cost_Dollars_Current
, (isnull(Margin_Cost_Basis_Amt_2, Margin_Cost_Basis_Amt) - [Rebate_Amount]) * [Sales_Mult] Cost_Dollars_NewA
, Case When Sell_Price_Override_Hist_Avg - Rebate_Sell_Amount = 0 then 0 else
	((Sell_Price_Override_Hist_Avg - Rebate_Sell_Amount - Unit_Cost_Of_Goods_Override_Hist_Avg + (isnull([Rebate_Dollars_Hist],0) / [Sales_Mult]))) / (Sell_Price_Override_Hist_Avg - ([Sales_Mult] * Rebate_Sell_Amount))
	end Pct_Margin_Of_Override_Transactions
, Case When ([Sell_Price_Cust_Amt_Cur] - Rebate_Sell_Amount) = 0 then 0
	else ([Sell_Price_Cust_Amt_Cur] - [margin_cost_basis_amt] - Rebate_Sell_Amount) / ([Sell_Price_Cust_Amt_Cur] - Rebate_Sell_Amount) end Pct_Margin_Of_Current_Values
, [Sales_Dollars_Current_Ttl] * Price_Mult Sales_Dollars_Current
, [Sales_Dollars_NewA_Ttl] * Price_Mult Sales_Dollars_NewA
, Sell_Price_Cust_Amt_New_At1 * Price_Mult Sell_Price_Cust_Amt_New_At01
, (Sell_Price_Cust_Amt_Cur - Rebate_Sell_Amount) * Price_Mult Sell_Price_Cust_Amt_Current
, Rebate_Sell_Amount * Sales_Qty Rebate_Sell_Amount_Total
, *
from '

Set @SQLcdC = '(
Select 
Cast(isnull(Case ' + @RbtsA + ' 
	When 10 then 0
	When 20 then isnull(Rebate_CB_Amt, 0)
	When 30 then isnull(Rebate_BUY_Amt, 0)
	When 35 then isnull(Rebate_Incentive_Amt_Cur, 0)
	When 40 then isnull(Rebate_CB_Amt, 0) + isnull(Rebate_BUY_Amt, 0) + isnull(Rebate_Incentive_Amt_Cur, 0)
	When 60 then isnull(Rebate_CB_Amt, 0)
	End, 0) as Numeric(18, 4))
  Rebate_Amount
, Cast(isnull(Case ' + @RbtsA + ' 
	When 10 then 0
	When 20 then isnull(Rebate_CB_Amt, 0)
	When 30 then isnull(Rebate_BUY_Amt, 0)
	When 35 then isnull(Rebate_Incentive_Amt_cur, 0)
	When 40 then isnull(Rebate_CB_Amt, 0) + isnull(Rebate_BUY_Amt, 0) + isnull(Rebate_Incentive_Amt_Cur, 0)
	When 60 then isnull(Rebate_CB_Amt, 0)
	End, 0) * isnull(Case ' + @UFD + ' When 0 then ' + @PD + ' * Sales_Qty_Daily else [Sales_Qty] end, 0) as Numeric(18, 4))
  Rebate_Dollars
, Cast(isnull(Case ' + @RbtsA + ' 
	When 10 then 0
	When 20 then isnull(Rebate_CB_Dollars_Hist,0)
	When 30 then isnull(Rebate_BUY_Dollars_Hist,0)
	When 35 then isnull(Rebate_Incentive_Dollars_Hist,0)
	When 40 then isnull(Rebate_CB_Dollars_Hist,0) + isnull(Rebate_BUY_Dollars_Hist,0) + isnull(Rebate_Incentive_Dollars_Hist,0)
	When 60 then isnull(Rebate_CB_Dollars_Hist, 0)
	End, 0) as Numeric(18, 4))
  Rebate_Dollars_Hist
,Cast(isnull(Case ' + @RbtsA + ' 
	When 10 then 0
	when 20 then isnull(Rebate_CB_Amt_New, 0)
	When 30 then isnull(Rebate_BUY_Amt_New, 0)
	When 35 then isnull(Rebate_Incentive_Amt_New, 0)
	When 40 then isnull(Rebate_CB_Amt_New,0) + isnull(Rebate_BUY_Amt_New, 0) + isnull(Rebate_Incentive_Amt_New,0) 
	When 60 then isnull(Rebate_CB_Amt_New, 0)
	end, 0) as Numeric(18, 4)) Rebate_Amount_New
,Cast(isnull(Case ' + @RbtsA + ' 
	When 10 then 0
	when 20 then isnull(Rebate_CB_Amt_New, 0)
	When 30 then isnull(Rebate_BUY_Amt_New, 0)
	When 35 then isnull(Rebate_Incentive_Amt_New, 0)
	When 40 then isnull(Rebate_CB_Amt_New,0) + isnull(Rebate_BUY_Amt_New, 0) + isnull(Rebate_Incentive_Amt_New,0) 
	When 60 then isnull(Rebate_CB_Amt_New, 0)
	end, 0) 
	* isnull(Case ' + @UFD + ' When 0 then ' + @PD + ' * Sales_Qty_Daily else [Sales_Qty] end, 0) as Numeric(18, 4)) Rebate_Dollars_New

,Cast(isnull(Case ' + @RbtsA + ' 
	When 38 then isnull(Rebate_Sell_Amt, 0)
	End, 0) as Numeric(18, 4))
  ''Rebate_Sell_Amount''

, Cast((Sell_Price_Cust_Amt_Cur - isnull(Case ' + @RbtsA + ' When 38 then isnull(Rebate_Sell_Amt, 0) End, 0)) * Case ' + @UFD + ' When 0 then ' + @PD + ' * Sales_Qty_Daily else [Sales_Qty] 
	end as Numeric(18, 4)) Sales_Dollars_Current_Ttl

, Cast(Case isnull(Sell_Price_Cust_Amt_New_At1, 0)
	When 0 then Sell_Price_Cust_Amt_Cur - isnull(Case ' + @RbtsA + ' When 38 then isnull(Rebate_Sell_Amt, 0) End, 0) else Sell_Price_Cust_Amt_New_At1 - isnull(Case ' + @RbtsA + ' When 38 then isnull(Rebate_Sell_Amt, 0) End, 0) End 
	* Case ' + @UFD + ' When 0 then ' + @PD + ' * Sales_Qty_Daily else [Sales_Qty] 
	end as Numeric(18, 4)) Sales_Dollars_NewA_Ttl

, Case When isnull(Contracted_Next_Cust_Change_Date_2, Margin_Cost_Basis_Effective_Date_2) <> Margin_Cost_Basis_Effective_Date_2 
	then ''Contracted Customer Sell Price Change Date Different than Vendor Cost Change Date'' else '''' 
	end [Sell Price Effective Change Date Contracted]
, Case ' + @UFD + ' When 0 then ' + @PD + ' * Sales_Qty_Daily else [Sales_Qty] end Sales_Mult
, isnull([Override Qty] / ' + @HD + ' * ' + @PD + ',0) [OverRide Qty Proj]
, isnull((Select Rule_Sell_Price_Change from dbo.MAUI_Rules_Summary_WhatIF with (nolock) where What_If_Reference_ID 
	= Cast(Cast(MBC.What_If_Reference_ID as Varchar(32)) + Cast(' + @WIF + ' as varchar(2)) as BigInt)), 1) Price_Mult
, *
'

Set @SQLcdD = '
from (
Select [Sales_History_Date_Range], [Sales_Qty], [Sales_Qty_Daily], [Sales_Dollars_Hist], [Prod_Cost_Dollars_Hist], [Rebate_CB_Dollars_Hist], [Rebate_Buy_Dollars_Hist], [Rebate_Incentive_Dollars_Hist], [Prod_Cost_Net_CB_Dollars_Hist], [Prod_Cost_Net_BUY_Dollars_Hist], [Prod_Cost_Net_Incentive_Dollars_Hist], [Prod_Cost_Net_CB_BUY_Incentive_Dollars_Hist], [Margin_Dollars_Hist], [Margin_Net_CB_Dollars_Hist], [Margin_Net_BUY_Dollars_Hist], [Margin_Net_Incentive_Dollars_Hist], [Margin_Net_CB_BUY_Incentive_Dollars_Hist], [Sales_Dollars_Cur], [Cost_Dollars_Cur], [Cost_Net_CB_Dollars_Cur], [Cost_Net_BUY_Dollars_Cur], [Cost_Net_Incentive_Dollars_Cur], [Cost_Net_CB_BUY_Incentive_Dollars_Cur], [Margin_Dollars_Cur], [Margin_Net_CB_Dollars_Cur], [Margin_Net_BUY_Dollars_Cur], [Margin_Net_Incentive_Dollars_Cur], [Margin_Net_CB_BUY_Incentive_Dollars_Cur], [Sales_Dollars_New], [Cost_Dollars_New], [Cost_Net_CB_Dollars_New], [Cost_Net_BUY_Dollars_New], [Cost_Net_Incentive_Dollars_New], [Cost_Net_CB_Buy_Incentive_Dollars_New], [Margin_Dollars_New], [Margin_Net_CB_Dollars_New], [Margin_Net_BUY_Dollars_New], [Margin_Net_Incentive_Dollars_New], [Margin_Net_CB_BUY_Incentive_Dollars_New], [Cost_Impact_Dollars_Until_Price_Change], [Cost_Impact_Dollars_At_New_Price], [Price_Recovery_Dollars], [Total_Impact_Dollars], [Cost_Net_CB_Impact_Dollars_Until_Price_Change], [Cost_Net_BUY_Impact_Dollars_Until_Price_Change], [Cost_Net_Incentive_Impact_Dollars_Until_Price_Change], [Cost_Net_CB_BUY_Incentive_Impact_Dollars_Until_Price_Change], [Cost_Net_CB_Impact_Dollars_At_New_Price], [Cost_Net_BUY_Impact_Dollars_At_New_Price], [Cost_Net_Incentive_Impact_Dollars_At_New_Price], [Cost_Net_CB_BUY_Incentive_Impact_Dollars_At_New_Price], [Total_Net_CB_Impact_Dollars], [Total_Net_BUY_Impact_Dollars], [Total_Net_Incentive_Impact_Dollars], [Total_Net_CB_BUY_Incentive_Impact_Dollars], [Rebate_CB_Dollars_Cur], [Rebate_BUY_Dollars_Cur], [Rebate_CB_Dollars_New], [Rebate_BUY_Dollars_New], [Rebate_Incentive_Dollars_Cur], [Rebate_Incentive_Dollars_New], [Margin_Cost_Basis_Amt], [Margin_Cost_Basis_Amt_2]
, Cast(isnull(Case ' + @RbtsA + '
	When 60 then [Sell_Price_Cust_Amt_Cur] + isnull(OverRide_Impact_Amt, 0)
	else [Sell_Price_Cust_Amt_Cur] end, 0) as Numeric(18, 4)) ''Sell_Price_Cust_Amt_Cur''
, [Sell_Price_Cust_Amt_New], [Margin_Amt], [Margin_Pct], [Sales_History_End_Date], [Sales_History_Start_Date], [Sales_Ext_Hist], [Prod_Cost_Ext_Hist], [Rebate_Amt_CB_Hist], [Rebate_Amt_BUY_Hist], [Margin_Cost_Basis_Effective_Date], [Margin_Cost_Basis_Effective_Date_2], [Contracted_Next_Cust_Change_Date_2], [Days_until_cost_change], [Days_until_Price_Change], [Days_at_New_Price], [Assessment_End_Date], [Rebate_CB_Amt], [Rebate_BUY_Amt], [Sell_Price_Rebate_Cap_Cur], [Rebate_CB_Amt_New], [Rebate_Buy_Amt_New], [Sell_Price_Rebate_Cap_New], [Cost_Dollars_Cost_Cur], [Cost_Dollars_Cost_Change], [Cost_Dollars_Price_Change], [Sales_Dollars_Price_Cur], [Sales_Dollars_Price_Hold], [Sales_Dollars_Price_New], [GroupingCustom], [GroupingCustomDescription], [Product_ID], [Prod_Description], [CustomerID_BT], [CustomerID_BT_Name], [CustomerID_ST], [CustomerID_ST_Name], [SalesRep_Record_In], [SalesRep_Record_Out], [Whse_Record], [Whse], [Whse Name], [Cust_Group1], [Cust_Group2], [Cust_Group3], [Prod_Group1], [Prod_Group2], [Prod_Group3], [Prod_Group4], [Prod_Group5], [Sell_Price_Cust_Filter_Name], [Sell_Price_Cust_Filter_Value], [Sell_Price_Prod_Filter_Name], [Sell_Price_Prod_Filter_Value], [Org_Vendor_ID], [Org Vendor Name], [Price_Rule_Type], [Rebate_CB_Rule_Type], [Rebate_BUY_Rule_Type], [Product_Structure_FK], [Org_Entity_Structure_FK], [Cust_Entity_Structure_FK], [Supl_Entity_Structure_FK], [Sell_Price_Cust_Amt_New_AT1], [Margin_Cost_Basis_Effective_Date_AT1], [Sell_Price_Cust_Operation], [Rebate_CB_Operation], [Rebate_BUY_Operation], [Last Override Invoice Date], [Last Override Price], [Last Override Cost], [Override Qty], [Override Transactions], [Total Transactions], [Sell_Price_Cust_Rules_FK], [Rebate_CB_Rules_FK], [Rebate_BUY_Rules_FK], [Contract_No_Price], [Contract_No_Rebate_CB], [Contract_No_Rebate_BUY], [Sales_Rep_Inside_ID], [Sales_Rep_Inside_Name], [Sales_Rep_Outside_ID], [Sales_Rep_Outside_Name], [Order_Taken_By], [Qty_Break_Rule_Ind], [Rebate_Vendor_Incentive_Percent], [Rebate_Incentive_Amt_Hist], [Rebate_Incentive_Amt_Cur], [Rebate_Incentive_Amt_New], [Sell_Price_Cust_Host_Rule_Xref], [Rebate_CB_Host_Rule_Xref], [Rebate_BUY_Host_Rule_Xref], [Sell_Price_Cust_Number_1], [Sell_Price_Cust_Basis_1_Col], [Price_Operand_Pos], [Discount_Operand_Pos], [Sell_Price_Override_Hist_Avg], [Unit_Cost_Of_Goods_Override_Hist_Avg], [OverRide_Impact_Amt], [OverRide_Impact_Amt_Future], [What_If_Reference_ID], [QTY_MODE], [QTY_MEDIAN], [QTY_MEAN], [Qty_Q1], [Qty_Q2], [Qty_Q3], [Qty_Q4], [Sales_Q1], [Sales_Q2], [Sales_Q3], [Sales_Q4], [Cost_Net_Q1], [Cost_Net_Q2], [Cost_Net_Q3], [Cost_Net_Q4], [Date_Range_Q1], [Date_Range_Q2], [Date_Range_Q3], [Date_Range_Q4], [Sell_Price_Cust_Host_Rule_Xref_Hist], [Rebate_CB_Host_Rule_Xref_Hist], [Rebate_Buy_Host_Rule_Xref_Hist], [CONTRACT_NO_HIST_LAST_PRICE], [CONTRACT_NO_HIST_LAST_REBATE_CB], [Replacement_Cost], [Standard_Cost], [BASE_PRICE], [LIST_PRICE], [SPC_UOM], [Sell_Price_Cust_Rules_FK_New], [Sell_Price_Cust_Effective_Date_Cur], [Sell_Price_Cust_Effective_Date_New], [Sell_Price_Cust_End_Date_Cur], [Sell_Price_Cust_End_Date_New], [Identifier], [Rebate_Sell_Amt], [Rebate_Sell_Amt_Hist], [Rebate_Sell_Amt_New], [STK_UOM], [SPC_UOM_Code], [Margin_Cost_Basis_Amt_Hist], [Sell_Price_Cust_Amt_Hist_Avg], [Average_Cost_Cur], [Average_Cost_New], [Vendor_Name], [Velocity_Product_Rank], [Velocity_Customer_Rank]
, [Rebate Transactions]
from #MBC MBC 
) MBC ) A ) B
having sum(isnull(sales_dollars_hist, 0)) <> 0 and Sum(Sales_Dollars_Cur) <> 0
) C
Where Focus is not null
'

--dbo.MAUI_Basis_Calcs MBC With (NOLOCK) Where Job_FK_i = ' + @Job_i + ' and isnull(sales_qty, 0) <> 0

--PRINT (@SQLcd)
--PRINT (@SQLcdB)
--PRINT (@SQLcdC)
--Print (@SQLcdD)

exec (@SQLcd + @SQLcdB + @SQLcdC + @SQLcdD)

Set NOCOUNT off

END
