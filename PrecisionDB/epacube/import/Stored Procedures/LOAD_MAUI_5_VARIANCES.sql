﻿--A_epaMAUI_LOAD_MAUI_5_VARIANCES

CREATE PROCEDURE [import].[LOAD_MAUI_5_VARIANCES]
	@Job_FK bigint, @Job_FK_i Numeric(10, 2), @Defaults_To_Use as Varchar(64)
AS
BEGIN

Set NOCOUNT ON

--Declare @Job_FK bigint
--Declare @Job_FK_i as Numeric(10, 2)
--Declare @Defaults_To_Use as Varchar(64)
--set @Job_FK = 10101
--Set @Job_FK_i = 10100.01
--Set @Defaults_To_Use = 'Primary'

Declare @Calc_Status Varchar(16)
Set @Calc_Status = 'Current'

Declare @ERP Varchar(32)
Declare @Prod_ID_DN_FK Bigint
Declare @PS_Table Varchar(Max)
Declare @SPC Varchar(Max)

Set @ERP = (Select Value from epacube.epacube_params with (nolock) where name = 'ERP HOST')
Set @Prod_ID_DN_FK = (Select data_name_fk from dbo.MAUI_Config_ERP with (nolock) where usage = 'Data_Name' and epaCUBE_Column = 'Product_ID' and ERP = @ERP)
Set @PS_Table = (Select SQL_CD from dbo.MAUI_Config_ERP with (nolock) where Temp_Table = '##Product_Status' and ERP = @ERP)
Set @SPC = (Select SQL_CD from dbo.MAUI_Config_ERP with (nolock) where Temp_Table = '##spc' and ERP = @ERP and Usage = 'DynamicSQL')

IF object_id('tempdb..##ST') is not null
	   drop table ##ST;
IF object_id('tempdb..##ST_basis') is not null
	   drop table ##ST_basis;
IF object_id('tempdb..##MM_SXE') is not null
	   drop table ##MM_SXE;
IF object_id('tempdb..##MM_ECLIPSE') is not null
	   drop table ##MM_ECLIPSE;
IF object_id('tempdb..##Variances') is not null
	   drop table ##Variances;
IF object_id('tempdb..##Product_Status') is not null
	   drop table ##Product_Status;
IF object_id('tempdb..##ST_basis_A') is not null
	   drop table ##ST_basis_A;
IF object_id('tempdb..##ST_basis_A_Eclipse') is not null
	   drop table ##ST_basis_A_Eclipse;
IF object_id('tempdb..##Variances_Eclipse') is not null
	   drop table ##Variances_Eclipse;
IF object_id('tempdb..##ST_Eclipse') is not null
	   drop table ##ST_Eclipse;
IF object_id('tempdb..##SPC') is not null
	   drop table ##SPC;
IF object_id('tempdb..##MB') is not null	   
	   drop table ##MB;
	   
insert into dbo.maui_log Select @Job_FK_i, 'Create Variance Temp Tables', GetDate(), Null, Null

Exec (@PS_Table)

Create Index idx_status on ##Product_Status(Product_Structure_FK, Product_Status)

Exec (@SPC)

Create Index idx_spcuom on ##SPC(product_structure_fk, org_entity_structure_fk)

If @ERP = 'SXE'
Begin
	Select @Job_FK_i Job_FK_i, Cast(Prod as varchar(64)) Product_ID_Host, Case when shipto is null then Cast(custno as Varchar(64)) else Cast(custno + '-' + isnull(shipto, '') as Varchar(64)) end Customer_ID_Host, Cast(odh.Whse as Varchar(64)) Whse, odh.Price_Rule_Xref, Cast(odh.sales_qty as numeric(18, 6)) Qty, Cast(NetAmt as numeric(18, 6)) Sales, PS.Product_Status, Left(ltrim(rtrim(PS.Kit_Type)), 8) Kit_Type, Cast(ST.OrderNo as Varchar(32)) OrderNo, CAST(ST.ordersuf as Varchar(32)) + ' - ' + Cast(ST.[Lineno] as Varchar(32)) OrderLine, ST.SPECIAL_STOCK_IND
	Into ##ST_basis_A 
	from import.import_sxe_sales_transactions_active ST with (nolock) 
	inner join dbo.maui_order_data_history odh with (nolock) on ODH.Order_No = ST.OrderNo and odh.order_line = CAST(ST.ordersuf as Varchar(32)) + ' - ' + Cast(ST.[Lineno] as Varchar(32)) and odh.Job_FK_i = @Job_FK_i
	left join ##Product_Status PS with (nolock) on odh.product_structure_fk = ps.product_structure_fk
	--where transtype in ('Stock Order')
	--and Isnull(SPECIAL_STOCK_IND, '0') not between 'a' and 'z'
	--and whse in (select whse from dbo.maui_basis with (nolock) where job_fk_i = @job_fk_i group by whse)
	--and Cast(invoicedt as datetime) between (select top 1 sales_history_start_date from dbo.maui_basis with (nolock) where job_fk_i = job_fk_i) and (select top 1 sales_history_end_date from dbo.maui_basis with (nolock) where job_fk_i = job_fk_i)

	Create Index idx_ba on ##ST_basis_A(job_fk_i, OrderNo, OrderLine)
	
	Select 
	Product_ID_Host, Customer_ID_Host, Whse, Price_Rule_Xref, Qty, Sales, Product_Status, Kit_Type
	, Case When Isnull(SPECIAL_STOCK_IND, '0') between 'a' and 'z' then SPECIAL_STOCK_IND else Null end 'Special_Stock_Ind'
	into ##ST_Basis
	from ##ST_basis_A A with (nolock)
	--Inner Join dbo.MAUI_Order_Data_History H with (nolock) on A.job_fk_i = H.Job_FK_i and A.Orderno = H.Order_No and A.OrderLine = H.Order_Line

	Create index idx_trans on ##ST_basis(Product_ID_Host, Customer_ID_Host, Product_Status, Kit_Type, Whse, Special_Stock_Ind)
	
	insert into dbo.maui_log Select @Job_FK_i, 'Rules Variances ST_Basis', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (nolock) where Job_fk_i = @Job_FK_i and Activity = 'Create Variance Temp Tables'), 108)), Null

	Select Product_ID_Host, Customer_ID_Host, Whse, Max(isnull(Product_Status, '')) Product_Status, Max(ltrim(rtrim(isnull(Special_Stock_Ind, '')))) Special_Stock_Ind, Max(isnull(Kit_Type, '')) Kit_Type
	, (Select product_structure_fk from epacube.product_identification pi with (nolock) where data_name_fk = @Prod_ID_DN_FK and value = ST.Product_ID_Host) Product_Structure_FK
	, (Select entity_structure_fk from epacube.entity_identification with (nolock) where entity_data_name_fk = 141000 and data_name_fk = 141111 and value = ST.Whse) Org_Entity_Structure_FK
	, (Select top 1 entity_structure_fk from epacube.entity_identification with (nolock) where entity_data_name_fk like '144%' and value = ST.Customer_ID_Host order by entity_data_name_fk) Cust_Entity_Structure_FK
	, max(Price_Rule_Xref) Max_Price_Rule_Xref, Sum(CAST(Qty AS NUMERIC(18, 2))) Qty, Sum(CAST(Sales as Numeric(18, 2))) Sales 
	into ##ST 
	from ##ST_Basis ST with (nolock)
	group by Product_ID_Host, Customer_ID_Host, Whse

	Create index idx_odh on ##ST(Product_ID_Host, Product_Structure_FK, Customer_ID_Host, Product_Status, Kit_Type, Whse, Special_Stock_Ind)
	
	insert into dbo.maui_log Select @Job_FK_i, 'Rules Variances ST', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (nolock) where Job_fk_i = @Job_FK_i and Activity = 'Rules Variances ST_Basis'), 108)), Null

	Select 
	Max(sell_price_cust_amt) sell_price_cust_amt
	, Max(Sell_Price_Cust_Host_Rule_Xref) Price_Rule
	, Max(Sell_Price_Cust_Operation) Operation
	, Max(Qty_Break_Rule_Ind) Qty_Break_Rule_Ind
	, Product_ID
	, Isnull(CustomerID_ST, CustomerID_BT) CustomerID_ST
	, Whse
	, Product_Structure_FK
	, Org_Entity_Structure_FK
	, Cust_Entity_Structure_FK
	, Supl_Entity_Structure_FK, identifier
	, Calc_Status
	into ##MB
	from dbo.MAUI_Basis MB with (nolock) where Job_FK = @Job_FK
	group by Product_ID, Isnull(CustomerID_ST, CustomerID_BT), Whse, Product_Structure_FK, Org_Entity_Structure_FK, Cust_Entity_Structure_FK, Calc_Status, Supl_Entity_Structure_FK, identifier
	Create Index idx_mb on ##MB(Product_ID, CustomerID_ST, Whse, Product_Structure_FK, Org_Entity_Structure_FK, Cust_Entity_Structure_FK, Calc_Status, Supl_Entity_Structure_FK, identifier)

	Select Sum(ST.Qty) Sales_Qty, Sum(MB.sell_price_cust_amt * ST.Qty) MM_Dollars
	, Max(Price_Rule) Price_Rule
	, Max(Operation) Operation
	, Max(Qty_Break_Rule_Ind) Qty_Break_Rule_Ind
	, [Product_ID]
	, CustomerID_ST
	, MB.Whse
	, MB.product_structure_fk, MB.org_entity_structure_fk, MB.cust_entity_structure_fk
	, MB.Supl_Entity_Structure_FK, MB.identifier
	into ##MM_SXE
	from ##MB MB
	inner join ##ST ST with (nolock) 
		on MB.Product_Structure_FK = ST.Product_Structure_FK 
		and MB.Org_Entity_Structure_FK = ST.Org_Entity_Structure_FK 
		and MB.Cust_Entity_Structure_FK = ST.Cust_Entity_Structure_FK
	where MB.Calc_Status = @Calc_Status
	Group by MB.[Product_ID], MB.CustomerID_ST, MB.Whse, MB.product_structure_fk, MB.org_entity_structure_fk, MB.cust_entity_structure_fk, MB.Supl_Entity_Structure_FK, MB.identifier
	Create index idx_MM on ##MM_SXE([Product_ID], CustomerID_ST, whse)
	
	insert into dbo.maui_log Select @Job_FK_i, 'Rules Variances MM SXE', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (nolock) where Job_fk_i = @Job_FK_i and Activity = 'Rules Variances ST'), 108)), Null
	
End

If @ERP = 'ECLIPSE'
Begin
		
	Select @Job_FK_i Job_FK_i, Cast(ST.Product_ID as varchar(64)) Product_ID_Host, Isnull(ST.Ship_To_Customer_ID, ST.Bill_To_Customer_ID) Customer_ID_Host, Cast(ST.Whse_ID as Varchar(64)) Whse, ST.Price_Matrix_ID_Historical Price_Rule_Xref, Cast(ST.sales_qty as numeric(18, 6)) Qty, Cast(ST.Sell_Price_Amt as Numeric(18, 4)) * Cast(ST.Sales_Qty as numeric(18, 4)) Sales, PS.Product_Status, PS.Kit_Type, Cast(ST.Sales_Order_Number as Varchar(32)) OrderNo, CAST(ST.Sales_Order_Suffix as Varchar(32)) + ' - ' + Cast(ST.Sales_Order_Line as Varchar(32)) OrderLine
	, ST.Product_structure_fk, ST.org_entity_structure_fk, ST.Cust_entity_structure_fk
	Into ##ST_basis_A_Eclipse
	from import.import_sales_history_active ST with (nolock) 
	inner join dbo.maui_order_data_history odh with (nolock) on ODH.Order_No = Cast(ST.Sales_Order_Number as Varchar(32)) + Cast(Sales_Order_Suffix as varchar(32)) and Cast(odh.order_line as varchar(32)) = Cast(ST.[Sales_Order_Line] as Varchar(32)) and odh.Job_FK_i = @Job_FK_i
	left join ##Product_Status PS with (nolock) on ODH.Product_Structure_FK = PS.Product_Structure_FK

	Create Index idx_ba on ##ST_basis_A_Eclipse(job_fk_i, OrderNo, OrderLine)

	Select Product_ID_Host, Customer_ID_Host, Whse, Product_Status, Kit_Type, Product_structure_fk, org_entity_structure_fk, Cust_entity_structure_fk
	, max(Price_Rule_Xref) Max_Price_Rule_Xref, Sum(Qty) Qty, Sum(Sales) Sales 
	into ##ST_Eclipse 
	from ##ST_basis_A_Eclipse ST with (nolock)
	group by Product_structure_fk, org_entity_structure_fk, Cust_entity_structure_fk, Product_ID_Host, Customer_ID_Host, Product_Status, Kit_Type, Whse

	Create index idx_odh on ##ST_Eclipse(Product_ID_Host, Product_structure_fk, org_entity_structure_fk, Cust_entity_structure_fk, Customer_ID_Host, Product_Status, Kit_Type, Whse)

	Select Sum(ST.Qty) Sales_Qty, Sum(MB.sell_price_cust_amt * ST.Qty) MM_Dollars
	, Max(MB.Sell_Price_Cust_Host_Rule_Xref) Price_Rule
	, Max(MB.Sell_Price_Cust_Operation) Operation
	, Max(MB.Qty_Break_Rule_Ind) Qty_Break_Rule_Ind
	, MB.[Product_ID], Isnull(MB.CustomerID_ST, MB.CustomerID_BT) CustomerID_ST, MB.Whse, MB.product_structure_fk, MB.org_entity_structure_fk, MB.cust_entity_structure_fk, MB.identifier
	into ##MM_Eclipse
	from dbo.MAUI_Basis MB with (nolock) 
	inner join ##ST_Eclipse ST with (nolock) on MB.Product_Structure_FK = ST.Product_Structure_FK and MB.Org_Entity_Structure_FK = ST.Org_Entity_Structure_FK and MB.Cust_Entity_Structure_FK = ST.Cust_Entity_Structure_FK
	where MB.job_fk_i = @Job_FK_i and MB.Calc_Status = @Calc_Status
	Group by MB.[Product_ID], Isnull(MB.CustomerID_ST, MB.CustomerID_BT), MB.Whse, MB.product_structure_fk, MB.org_entity_structure_fk, MB.cust_entity_structure_fk, MB.identifier
	Create index idx_MM on ##MM_ECLIPSE([Product_ID], CustomerID_ST, whse)

End

If @ERP = 'SXE'
Begin

	select @Job_FK_i Job_FK_i, ST.Product_ID_Host, ST.Customer_ID_Host, ST.Whse, ST.qty ST_Qty, MM.sales_qty MM_Qty
	, ST.Sales ST_Dollars, MM_Dollars, MM.Price_Rule Max_Price_Rule_MM, ST.MAX_Price_Rule_Xref Max_Price_Rule_ST
	, MM.Operation Operation, MM.Qty_Break_Rule_Ind Qty_Break_Rule_Ind
	, MM.product_structure_fk, MM.org_entity_structure_fk, MM.cust_entity_structure_fk, MM.supl_entity_structure_fk

	, (Select ei.Entity_Structure_FK from epacube.entity_identification ei with (nolock) 
		inner join epacube.product_association pa with (nolock) 
			on pa.data_name_fk = 159100 and pa.product_structure_fk = ST.product_structure_fk 
				and ei.entity_data_name_fk = 141000 and ei.data_name_fk = 141111
				and (ei.entity_structure_fk = pa.org_entity_structure_fk or pa.org_entity_structure_fk = 1)
		where ei.value = ST.Whse) epa_Org_Entity_Structure_FK

	, (Select top 1 Entity_Structure_FK from epacube.entity_identification ei with (nolock) where value = ST.Customer_ID_Host and ei.data_name_fk = 144111 order by entity_data_name_fk) epa_Cust_Entity_Structure_FK
	, (Select Product_Structure_FK from epacube.Product_identification pi with (nolock) where value = ST.Product_ID_Host and pi.data_name_fk = @Prod_ID_DN_FK) epa_Product_Structure_FK
	, Cast((Select Product_Structure_FK from epacube.Product_identification pi with (nolock) where value = ST.Product_ID_Host and pi.data_name_fk = @Prod_ID_DN_FK) as varchar(64)) + '-' +
		Cast((Select ei.Entity_Structure_FK from epacube.entity_identification ei with (nolock) 
		inner join epacube.product_association pa with (nolock) 
			on pa.data_name_fk = 159100 and pa.product_structure_fk = ST.product_structure_fk 
				and ei.entity_data_name_fk = 141000 and ei.data_name_fk = 141111
				and (ei.entity_structure_fk = pa.org_entity_structure_fk or pa.org_entity_structure_fk = 1)
		where ei.value = ST.Whse) as Varchar(64)) + '-' +
		Cast((Select Entity_Structure_FK from epacube.entity_identification ei with (nolock) where value = ST.Customer_ID_Host and ei.entity_data_name_fk = 144020 and ei.data_name_fk = 144111) as varchar(64)) Identifier_POC
	, MM.identifier, ST.Product_Status, ST.Kit_Type, ST.Special_Stock_Ind
	into ##Variances
	from ##ST ST with (nolock)
		  left join ##MM_SXE MM with (nolock)
		  on ST.product_id_host = MM.[Product_ID]
		  and ST.customer_id_host = MM.customerID_ST
		  and ST.Whse = MM.Whse

	Create Index idx_vars on ##Variances(Job_FK_i, Identifier_POC, Product_ID_Host, Customer_ID_Host, Whse)
	
	insert into dbo.maui_log Select @Job_FK_i, 'Rules Variances ##Variances', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (nolock) where Job_fk_i = @Job_FK_i and Activity = 'Rules Variances MM SXE'), 108)), Null	

	Update ##Variances
	Set Identifier = Identifier_POC
	Where Identifier is null and Identifier_POC is not null and job_fk_i = @job_fk_i
	
	insert into dbo.maui_log Select @Job_FK_i, 'Rules Variances Update ##Variances', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (nolock) where Job_fk_i = @Job_FK_i and Activity = 'Rules Variances ##Variances'), 108)), Null	

	Delete from dbo.MAUI_Variances where job_fk_i = @Job_FK_i

	insert into dbo.maui_log Select @Job_FK_i, 'Start Loading MAUI Variances', Getdate(), Null, Null

	Insert into dbo.MAUI_Variances
	select @Job_FK_i Job_FK_i
	, Product_ID_Host
	, Customer_ID_Host
	, Whse
	, Null Transaction_Type
	, isnull(SPC.SPC_UOM, 1) SPC_UOM
	, ST_Qty
	, MM_Qty
	, Round(ST_Dollars, 4) ST_Dollars
	, Round(MM_Dollars, 4) MM_Dollars
	, Round(Case When ST_Qty <> 0 then Cast(ST_Dollars / ST_Qty as Numeric(18, 2)) else ST_Dollars end, 4) ST_Sell_Price_Unit
	, Round(Case When MM_Qty <> 0 then Cast(MM_Dollars / MM_Qty as Numeric(18, 2)) else MM_Dollars end, 4) MM_Sell_Price_Unit
	, Max_Price_Rule_ST
	, Max_Price_Rule_MM 
	, Operation
	, Qty_Break_Rule_Ind
	, Case ST_Dollars When 0 then 0 else MM_Dollars / ST_Dollars end Diff 
	, Case when Product_ID_Host = 'Uncalculated Aggregates' then 0
		when (epa_Product_Structure_FK is null or epa_Org_Entity_Structure_FK is null or epa_Cust_Entity_Structure_FK is null) and isnull(MM_Dollars, 0) = 0 then 1 
		When epa_Product_Structure_FK is not null and epa_Org_Entity_Structure_FK is not null and epa_Cust_Entity_Structure_FK is not null and isnull(MM_Dollars, 0) = 0 then 2 else 3 end Grp 
	, Case When epa_Product_Structure_FK is null then 0 else -1 end ProductValid
	, Case When epa_Org_Entity_Structure_FK is null then 0 else -1 end WhseValid
	, Case When epa_Cust_Entity_Structure_FK is null then 0 else -1 end CustomerValid
	--, Case When Identifier is null and Identifier_POC is null then 0 else -1 end DrillDownValid
	, Case When Identifier is null then 0 else -1 end DrillDownValid
	, Case When Product_Status is not null and SPECIAL_STOCK_IND is not null then
			ltrim(rtrim(Product_Status)) + '/' + Upper(left(ltrim(rtrim(Special_Stock_Ind)), 1))
			When SPECIAL_STOCK_IND is null then ltrim(rtrim(Product_Status))
			else
			isnull(Product_Status, '-') + isnull(Case When Isnull(SPECIAL_STOCK_IND, '0') between 'a' and 'z' then '/' + Upper(left(ltrim(rtrim(Special_Stock_Ind)), 1)) end , '')
		end
	, Kit_Type
	, V.product_structure_fk, V.org_entity_structure_fk, V.cust_entity_structure_fk, V.supl_entity_structure_fk, V.identifier
	from ##Variances V with (nolock)
		Left join ##SPC SPC with (nolock) on V.product_structure_FK = SPC.Product_Structure_FK and (V.Org_Entity_Structure_FK = SPC.Org_Entity_Structure_FK or SPC.Org_Entity_Structure_FK = 1)
	Where V.identifier is not null
	order by isnull(Qty_Break_Rule_Ind, 1000000000) Desc, isnull(MM_Dollars / Case When ST_Dollars <> 0 then ST_Dollars else 1 end, 1000000000) desc

	insert into dbo.maui_log Select @Job_FK_i, 'Rules Variances Insert Into Variances', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (nolock) where Job_fk_i = @Job_FK_i and Activity = 'Rules Variances Update ##Variances'), 108)), Null	

End


--Set NOCOUNT ON

--Declare @Job_FK bigint
--Declare @Job_FK_i as Numeric(10, 2)
--Declare @Defaults_To_Use as Varchar(64)
--set @Job_FK = 101
--Set @Job_FK_i = 101.01
--Set @Defaults_To_Use = 'Primary'
--Declare @Calc_Status Varchar(16)
--Set @Calc_Status = 'Current'

--Declare @ERP Varchar(32)
--Declare @Prod_ID_DN_FK Bigint
--Declare @PS_Table Varchar(Max)

--Set @ERP = (Select Value from epacube.epacube_params where name = 'ERP HOST')
--Set @Prod_ID_DN_FK = (Select data_name_fk from dbo.MAUI_Config_ERP where usage = 'Data_Name' and epaCUBE_Column = 'Product_ID' and ERP = @ERP)
--Set @PS_Table = (Select SQL_CD from dbo.MAUI_Config_ERP with (nolock) where Temp_Table = '##Product_Status' and ERP = @ERP)

--IF object_id('tempdb..##Variances_Eclipse') is not null
--	   drop table ##Variances_Eclipse;

If @ERP = 'Eclipse'
Begin

	select @Job_FK_i Job_FK_i, ST.Product_ID_Host, ST.Customer_ID_Host, ST.Whse, ST.qty ST_Qty, MM.sales_qty MM_Qty
	, ST.Sales ST_Dollars, MM_Dollars, MM.Price_Rule Max_Price_Rule_MM, ST.MAX_Price_Rule_Xref Max_Price_Rule_ST
	, MM.Operation Operation, MM.Qty_Break_Rule_Ind Qty_Break_Rule_Ind
	, ST.product_structure_fk, ST.org_entity_structure_fk, ST.cust_entity_structure_fk

	, (Select ei.Entity_Structure_FK from epacube.entity_identification ei with (nolock) 
		inner join epacube.product_association pa with (nolock) 
			on pa.data_name_fk = 159100 and pa.product_structure_fk = ST.product_structure_fk 
				and ei.entity_data_name_fk = 141000 and ei.data_name_fk = 141111
				and (ei.entity_structure_fk = pa.org_entity_structure_fk or pa.org_entity_structure_fk = 1)
		where ei.value = ST.Whse) epa_Org_Entity_Structure_FK

	, (Select top 1 Entity_Structure_FK from epacube.entity_identification ei with (nolock) where value = ST.Customer_ID_Host and ei.data_name_fk = 144111) epa_Cust_Entity_Structure_FK
	, (Select Product_Structure_FK from epacube.Product_identification pi with (nolock) where value = ST.Product_ID_Host and pi.data_name_fk = @Prod_ID_DN_FK) epa_Product_Structure_FK
	, Cast((Select Product_Structure_FK from epacube.Product_identification pi with (nolock) where value = ST.Product_ID_Host and pi.data_name_fk = @Prod_ID_DN_FK) as varchar(64)) + '-' +
		Cast((Select top 1 ei.Entity_Structure_FK from epacube.entity_identification ei with (nolock) 
		inner join epacube.product_association pa with (nolock) 
			on pa.data_name_fk = 159100 and pa.product_structure_fk = ST.product_structure_fk 
				and ei.entity_data_name_fk = 141000 and ei.data_name_fk = 141111
				and (ei.entity_structure_fk = pa.org_entity_structure_fk or pa.org_entity_structure_fk = 1)
		where ei.value = ST.Whse) as Varchar(64)) + '-' +
		Cast((Select top 1 Entity_Structure_FK from epacube.entity_identification ei with (nolock) where value = ST.Customer_ID_Host and ei.data_name_fk = 144111) as varchar(64)) Identifier_POC
	, MM.identifier, ST.Product_Status, ST.Kit_Type
	into ##Variances_Eclipse
	from ##ST_Eclipse ST with (nolock)
		  left join ##MM_Eclipse MM with (nolock)
		  on ST.product_id_host = MM.[Product_ID]
		  and ST.customer_id_host = MM.customerID_ST
		  and ST.Whse = MM.Whse

	Create Index idx_vars on ##Variances_Eclipse(Job_FK_i, Identifier_POC, Product_ID_Host, Customer_ID_Host, Whse)

	Delete from dbo.MAUI_Variances where job_fk_i = @Job_FK_i

	insert into dbo.maui_log Select @Job_FK_i, 'Start Loading MAUI Variances', Getdate(), Null, Null

	Insert into dbo.MAUI_Variances
	select @Job_FK_i Job_FK_i
	, Product_ID_Host
	, Customer_ID_Host
	, Whse
	, Null Transaction_Type
	, isnull(isnull(spc_Org.spc_uom, SPC_1.spc_uom), 1) SPC_UOM 
	, ST_Qty
	, MM_Qty
	, Round(ST_Dollars, 4) ST_Dollars
	, Round(MM_Dollars, 4) MM_Dollars
	, Round(Case When ST_Qty <> 0 then Cast(ST_Dollars / ST_Qty as Numeric(18, 2)) else ST_Dollars end, 4) ST_Sell_Price_Unit
	, Round(Case When MM_Qty <> 0 then Cast(MM_Dollars / MM_Qty as Numeric(18, 2)) else MM_Dollars end, 4) MM_Sell_Price_Unit
	, Max_Price_Rule_ST
	, Max_Price_Rule_MM 
	, Operation
	, Qty_Break_Rule_Ind
	, Case ST_Dollars When 0 then 0 else MM_Dollars / ST_Dollars end Diff 
	, Case when Product_ID_Host = 'Uncalculated Aggregates' then 0
		when (epa_Product_Structure_FK is null or epa_Org_Entity_Structure_FK is null or epa_Cust_Entity_Structure_FK is null) and isnull(MM_Dollars, 0) = 0 then 1 
		When epa_Product_Structure_FK is not null and epa_Org_Entity_Structure_FK is not null and epa_Cust_Entity_Structure_FK is not null and isnull(MM_Dollars, 0) = 0 then 2 else 3 end Grp 
	, Case When epa_Product_Structure_FK is null then 0 else -1 end ProductValid
	, Case When epa_Org_Entity_Structure_FK is null then 0 else -1 end WhseValid
	, Case When epa_Cust_Entity_Structure_FK is null then 0 else -1 end CustomerValid
	, Case When Identifier is null then 0 else -1 end DrillDownValid
	, Null SPECIAL_STOCK_IND
	, Kit_Type
	, VE.product_structure_fk, VE.org_entity_structure_fk, VE.cust_entity_structure_fk, Null Supl_Entity_Structure_FK, VE.identifier
	from ##Variances_Eclipse VE with (nolock)
	Left join ##spc spc_org with (nolock) on VE.product_structure_fk = spc_org.product_structure_fk and VE.org_entity_structure_fk = spc_org.org_entity_structure_fk and spc_org.org_entity_structure_fk <> 1
	Left join ##spc spc_1 with (nolock) on VE.product_structure_fk = spc_1.product_structure_fk and spc_1.org_entity_structure_fk = 1

	order by isnull(Qty_Break_Rule_Ind, 1000000000) Desc, isnull(MM_Dollars / Case When ST_Dollars <> 0 then ST_Dollars else 1 end, 1000000000) desc
	
End

insert into dbo.maui_log Select @Job_FK_i, 'Rules Variances Load Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (nolock) where Job_fk_i = @Job_FK_i and Activity = 'Create Variance Temp Tables'), 108)), (Select COUNT(*) from dbo.MAUI_Variances with (nolock) where Job_FK_i = @Job_FK_i)

IF object_id('tempdb..##ST') is not null
	   drop table ##ST;
IF object_id('tempdb..##ST_basis') is not null
	   drop table ##ST_basis;
IF object_id('tempdb..##MM_SXE') is not null
	   drop table ##MM_SXE;
IF object_id('tempdb..##MM_ECLIPSE') is not null
	   drop table ##MM_ECLIPSE;
IF object_id('tempdb..##Variances') is not null
	   drop table ##Variances;
IF object_id('tempdb..##Product_Status') is not null
	   drop table ##Product_Status;
IF object_id('tempdb..##ST_basis_A') is not null
	   drop table ##ST_basis_A;
IF object_id('tempdb..##ST_basis_A_Eclipse') is not null
	   drop table ##ST_basis_A_Eclipse;
IF object_id('tempdb..##Variances_Eclipse') is not null
	   drop table ##Variances_Eclipse;
IF object_id('tempdb..##ST_Eclipse') is not null
	   drop table ##ST_Eclipse;
IF object_id('tempdb..##SPC') is not null
	   drop table ##SPC;
IF object_id('tempdb..##MB') is not null	   
	   drop table ##MB;
	   	   
EXEC [import].[LOAD_MAUI_6_OVERVIEW]  @Job_FK, @Job_FK_i, @Defaults_To_Use

END
