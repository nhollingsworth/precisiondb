﻿--A_epaMAUI_Import_To_Production_PDSC

CREATE PROCEDURE [import].[Import_To_Production_PDSC] 
AS
BEGIN

--Modified 4/14/2013 to accommodate Fairmont customization - Type 4 rules of Customer Price Type by Product Category
--Modified 8/14/2013 to remove corrupt price rules so this procedure will replace where appropriate.
--Modified 8/21/2013 to move Tenaquip customization into flexible format
--Modified 11/6/2013 to improve performance of rounding rule updates at end of procedure

	SET NOCOUNT ON;

If (Select count(*) from import.import_sxe_pdsc) = 0
goto EndOfProc

Declare @In_JOB_FK bigint
Declare @Op_Lookup Int
Declare @HistNamePrefix varchar(64)
Declare @CustomCode_Last_Step Varchar(Max)

Set @In_JOB_FK = (Select top 1 Job_FK from import.import_sxe_pdsc with (nolock) order by JOB_FK desc)
Set @Op_Lookup = (Select value from epacube.EPACUBE_PARAMS with (nolock) where NAME = 'RULES_FILTER_LOOKUP_OPERAND')
Set @HistNamePrefix = Case When (Select COUNT(*) from marginmgr.PRICESHEET with (nolock) where PRICESHEET_NAME like 'HISTORICAL - %') > 0 then 'HISTORICAL - ' else '' end

IF object_id('tempdb..##PDSC') is not null
drop table ##PDSC;

IF object_id('tempdb..#rounding') is not null
drop table #rounding

IF object_id('tempdb..##tmp0_PDSC') is not null
drop table ##tmp0_PDSC;

IF object_id('tempdb..##tmp_PDSC') is not null
drop table ##tmp_PDSC;

IF object_id('tempdb..##tmp2_PDSC') is not null
drop table ##tmp2_PDSC;

IF object_id('tempdb..##Corrupt') is not null
drop table ##Corrupt;

IF object_id('tempdb..#dels') is not null
drop table #dels;

---Code to get rid of existing corrupt pricing rules
Select r.RULES_ID into ##Corrupt from synchronizer.RULES r with (nolock)
inner join synchronizer.RULES_ACTION ra with (nolock) on r.RULES_ID = ra.RULES_FK
left join synchronizer.RULES_ACTION_OPERANDS rao with (nolock) on ra.RULES_ACTION_ID = rao.RULES_ACTION_FK
where 1 = 1
and (right(ra.RULES_ACTION_OPERATION1_FK, 1) = 3
and r.RESULT_DATA_NAME_FK = 111602
and rao.BASIS_POSITION is null)
or
(
	r.RESULT_DATA_NAME_FK = 111602
and rao.rules_action_OPERANDS_ID is not null
and coalesce(Operand1, Operand2, Operand3, Operand4, Operand5, Operand6, Operand7, Operand8, Operand9) is null
)

Create index idx_crpt on ##corrupt(Rules_ID)

delete from dbo.MAUI_Rules_Maintenance where rules_fk in (select rules_id from ##Corrupt)

Delete from Synchronizer.rules_filter_set where rules_fk in (select rules_id from ##Corrupt)

Select rules_filter_fk into #dels from
	(
	select prod_filter_fk rules_filter_fk from synchronizer.RULES_FILTER_SET with (nolock) where prod_filter_fk is not null group by prod_filter_fk Union
	select Org_filter_fk from synchronizer.RULES_FILTER_SET with (nolock) where Org_filter_fk is not null group by Org_filter_fk Union
	select cust_filter_fk from synchronizer.RULES_FILTER_SET with (nolock) where cust_filter_fk is not null group by cust_filter_fk Union
	select supl_filter_fk from synchronizer.RULES_FILTER_SET with (nolock) where supl_filter_fk is not null group by supl_filter_fk
	) A
	
Create index idx_dels on #dels(rules_filter_fk)

Delete rf from synchronizer.RULES_FILTER rf
left join #dels dels on rf.RULES_FILTER_ID = dels.rules_filter_fk
where dels.rules_filter_fk is null

Delete from Synchronizer.rules_action_operands where RULES_ACTION_FK in (select rules_action_id from synchronizer.RULES_ACTION where RULES_FK in (select rules_id from ##Corrupt))

Delete from synchronizer.RULES_ACTION_STRFUNCTION where RULES_ACTION_FK in (select rules_action_id from synchronizer.RULES_ACTION where RULES_FK in (select rules_id from ##Corrupt))
Delete from Synchronizer.rules_action where rules_fk in (select rules_id from ##Corrupt)
Delete from synchronizer.rules where RULES_ID in (select rules_id from ##Corrupt)
----------

Truncate Table Import.Import_Rules
Truncate Table Import.Import_Rules_Data

Select 
[IMPORT_FILENAME], [JOB_FK], [Typecd], [commtype], [contractno], [custno], [custtype], [startdt], [enddt], [jobno], [levelcd], [maxqty], [minqty], [pdrecno], [pexactrnd], [prcdisc1], [prcdisc2], [prcdisc3], [prcdisc4], [prcdisc5], [prcdisc6], [prcdisc7], [prcdisc8], [prcdisc9], [prcmult1], [prcmult2], [prcmult3], [prcmult4], [prcmult5], [prcmult6], [prcmult7], [prcmult8], [prcmult9], [prctype], [pricing cost], [priceonty], [pricesheet], [pricetype], [prod], [prodcat], [prodline], [promofl], [pround], [ptarget], [qtybreak], [qtybrk1], [qtybrk2], [qtybrk3], [qtybrk4], [qtybrk5], [qtybrk6], [qtybrk7], [qtybrk8], [qtytype], [rebatetype], [refer], [Shipto], [termsdisc], [termspct], [units], [user1], [user2], [user3], [user4], [user5], [user6], [user7], [user8], [user9], [vendno ], [whse], [custom1], [custom2], [custom3], [custom4], [custom5], [custom6], [custom7], [custom8], [custom9], [custom10]
into ##tmp0_PDSC
from import.Import_SXE_PDSC with (nolock) where (enddt is null or (isdate(enddt) = 1 and isnull(Cast(enddt as datetime), getdate() + 1) > getdate()))
	and (
	(Levelcd = '1' and custno is not null and prod is not null) Or 
	(Levelcd = '2' and custno is not null and (pricetype is not null or prodcat is not null or prodline is not null or rebatetype is not null)) Or
	(Levelcd = '3' and custtype is not null and Prod is not null) Or
	(Levelcd = '4' and custtype is not null and (PriceType is not null or RebateType is not null or prodcat is not null)) Or
	(Levelcd = '5' and custno is not null) Or
	(Levelcd = '6' and custtype is not null) Or
	(Levelcd = '7' and prod is not null) Or
	(Levelcd = '8' and PriceType is not null)
	)
	and IMPORT_FILENAME = (Select MAX(IMPORT_FILENAME) from import.import_sxe_pdsc)
group by [IMPORT_FILENAME], [JOB_FK], [Typecd], [commtype], [contractno], [custno], [custtype], [startdt], [enddt], [jobno], [levelcd], [maxqty], [minqty], [pdrecno], [pexactrnd], [prcdisc1], [prcdisc2], [prcdisc3], [prcdisc4], [prcdisc5], [prcdisc6], [prcdisc7], [prcdisc8], [prcdisc9], [prcmult1], [prcmult2], [prcmult3], [prcmult4], [prcmult5], [prcmult6], [prcmult7], [prcmult8], [prcmult9], [prctype], [pricing cost], [priceonty], [pricesheet], [pricetype], [prod], [prodcat], [prodline], [promofl], [pround], [ptarget], [qtybreak], [qtybrk1], [qtybrk2], [qtybrk3], [qtybrk4], [qtybrk5], [qtybrk6], [qtybrk7], [qtybrk8], [qtytype], [rebatetype], [refer], [Shipto], [termsdisc], [termspct], [units], [user1], [user2], [user3], [user4], [user5], [user6], [user7], [user8], [user9], [vendno ], [whse], [custom1], [custom2], [custom3], [custom4], [custom5], [custom6], [custom7], [custom8], [custom9], [custom10]
Order by [IMPORT_FILENAME]

alter table ##tmp0_PDSC add row_id int identity(1, 1)

Select pd.* into ##tmp_pdsc from ##tmp0_pdsc pd
inner join (select pdrecno, Max(row_id) M_id from ##tmp0_PDSC group by pdrecno) tpdsc
	on pd.pdrecno = tpdsc.pdrecno and pd.row_id = tpdsc.M_id

Update ##tmp_PDSC
Set
prcmult1 = Case When prcmult1 = '0' then Null else prcmult1 end
,prcmult2 = Case When prcmult2 = '0' then Null else prcmult2 end
,prcmult3 = Case When prcmult3 = '0' then Null else prcmult3 end
,prcmult4 = Case When prcmult4 = '0' then Null else prcmult4 end
,prcmult5 = Case When prcmult5 = '0' then Null else prcmult5 end
,prcmult6 = Case When prcmult6 = '0' then Null else prcmult6 end
,prcmult7 = Case When prcmult7 = '0' then Null else prcmult7 end
,prcmult8 = Case When prcmult8 = '0' then Null else prcmult8 end
,prcmult9 = Case When prcmult9 = '0' then Null else prcmult9 end
--,prcdisc1 = Case When prcdisc1 = '0' then Null else prcdisc1 end
,prcdisc2 = Case When prcdisc2 = '0' then Null else prcdisc2 end
,prcdisc3 = Case When prcdisc3 = '0' then Null else prcdisc3 end
,prcdisc4 = Case When prcdisc4 = '0' then Null else prcdisc4 end
,prcdisc5 = Case When prcdisc5 = '0' then Null else prcdisc5 end
,prcdisc6 = Case When prcdisc6 = '0' then Null else prcdisc6 end
,prcdisc7 = Case When prcdisc7 = '0' then Null else prcdisc7 end
,prcdisc8 = Case When prcdisc8 = '0' then Null else prcdisc8 end
,prcdisc9 = Case When prcdisc9 = '0' then Null else prcdisc9 end
,qtybrk1 = Case When qtybreak is not null then  isnull(qtybrk1, 0) else Null end
,qtybrk2 = Case When qtybreak is not null then  isnull(qtybrk2, 0) else Null end
,qtybrk3 = Case When qtybreak is not null then  isnull(qtybrk3, 0) else Null end
,qtybrk4 = Case When qtybreak is not null then  isnull(qtybrk4, 0) else Null end
,qtybrk5 = Case When qtybreak is not null then  isnull(qtybrk5, 0) else Null end
,qtybrk6 = Case When qtybreak is not null then  isnull(qtybrk6, 0) else Null end
,qtybrk7 = Case When qtybreak is not null then  isnull(qtybrk7, 0) else Null end
,qtybrk8 = Case When qtybreak is not null then  isnull(qtybrk8, 0) else Null end

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'idx_sxe_tmp_pdsc1')
	drop index idx_sxe_pdsc1 on ##tmp_PDSC
	
Create Index idx_sxe_pdsc1 on ##tmp_PDSC(levelcd, promofl)
	
select
Case When Cast(isnull(prcmult1, 0) as Numeric(18, 6)) + Cast(isnull(prcmult2, 0) as Numeric(18, 6)) + Cast(isnull(prcmult3, 0) as Numeric(18, 6)) + Cast(isnull(prcmult4, 0) as Numeric(18, 6)) + Cast(isnull(prcmult5, 0) as Numeric(18, 6)) +
				Cast(isnull(prcmult6, 0) as Numeric(18, 6)) + Cast(isnull(prcmult7, 0) as Numeric(18, 6)) + Cast(isnull(prcmult8, 0) as Numeric(18, 6)) + Cast(isnull(prcmult9, 0) as Numeric(18, 6)) > 0 then 
		1 else 0 end 'Multiplier_Ind'

, Case When Cast(isnull(prcdisc1, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc2, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc3, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc4, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc5, 0) as Numeric(18, 6)) +
				Cast(isnull(prcdisc6, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc7, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc8, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc9, 0) as Numeric(18, 6)) > 0 
			OR Cast(isnull(prcmult1, 0) as Numeric(18, 6)) + Cast(isnull(prcmult2, 0) as Numeric(18, 6)) + Cast(isnull(prcmult3, 0) as Numeric(18, 6)) + Cast(isnull(prcmult4, 0) as Numeric(18, 6)) + Cast(isnull(prcmult5, 0) as Numeric(18, 6)) +
				Cast(isnull(prcmult6, 0) as Numeric(18, 6)) + Cast(isnull(prcmult7, 0) as Numeric(18, 6)) + Cast(isnull(prcmult8, 0) as Numeric(18, 6)) + Cast(isnull(prcmult9, 0) as Numeric(18, 6)) = 0
		then 
		1 else 0 end 'Discount_Ind'
		
, Case	When isnull(qtybreak, '') like '%P%' then 1
		When isnull(Cast(Coalesce(prcmult1, prcmult2, prcmult3, prcmult4, prcmult5, prcmult6, prcmult7, prcmult8, prcmult9) as Numeric(18, 6)), 0) = 0 then 0
		When isnull(Cast(Coalesce(prcmult1, prcmult2, prcmult3, prcmult4, prcmult5, prcmult6, prcmult7, prcmult8, prcmult9) as Numeric(18, 6)), 0) > 0 and qtybreak is not null then 1
		When Cast(isnull(prcmult1, 0) as Numeric(18, 6)) + Cast(isnull(prcmult2, 0) as Numeric(18, 6)) + Cast(isnull(prcmult3, 0) as Numeric(18, 6)) + Cast(isnull(prcmult4, 0) as Numeric(18, 6)) + 
			Cast(isnull(prcmult5, 0) as Numeric(18, 6)) + Cast(isnull(prcmult6, 0) as Numeric(18, 6)) + Cast(isnull(prcmult7, 0) as Numeric(18, 6)) + Cast(isnull(prcmult8, 0) as Numeric(18, 6)) + 
			Cast(isnull(prcmult9, 0) as Numeric(18, 6))
			= isnull(Cast(Coalesce(prcmult1, prcmult2, prcmult3, prcmult4, prcmult5, prcmult6, prcmult7, prcmult8, prcmult9) as Numeric(18, 6)), 0) and Cast(isnull(prcmult1, 0) as Numeric(18, 6)) <> 0 and qtybreak is null then
		0 else 1 end 'Multiple_Multiplier_Operands_Ind'

, Case	When isnull(qtybreak, '') like '%D%' then 1
		When isnull(Cast(Coalesce(prcdisc2, prcdisc3, prcdisc4, prcdisc5, prcdisc6, prcdisc7, prcdisc8, prcdisc9) as Numeric(18, 6)), 0) = 0 then 0
		When isnull(Cast(Coalesce(prcdisc2, prcdisc3, prcdisc4, prcdisc5, prcdisc6, prcdisc7, prcdisc8, prcdisc9) as Numeric(18, 6)), 0) <> 0 and qtybreak is not null then 1
		When Cast(isnull(prcdisc1, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc2, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc3, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc4, 0) as Numeric(18, 6)) + 
			Cast(isnull(prcdisc5, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc6, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc7, 0) as Numeric(18, 6)) + Cast(isnull(prcdisc8, 0) as Numeric(18, 6)) + 
			Cast(isnull(prcdisc9, 0) as Numeric(18, 6))
		= isnull(Cast(Coalesce(prcdisc1, prcdisc2, prcdisc3, prcdisc4, prcdisc5, prcdisc6, prcdisc7, prcdisc8, prcdisc9) as Numeric(18, 6)), 0) and Cast(isnull(prcdisc1, 0) as Numeric(18, 6)) <> 0 and qtybreak is null then
		0 else 1 end 'Multiple_Discount_Operands_Ind'

, Case When Promofl = 'Y' and levelcd in ('7', '8') then 1 else 0 end 'Promo'
, *
Into ##tmp2_PDSC
from ##tmp_PDSC
--where isnull(Cast(Coalesce(prcmult1, prcmult2, prcmult3, prcmult4, prcmult5, prcmult6, prcmult7, prcmult8, prcmult9, prcdisc1, prcdisc2, prcdisc3, prcdisc4, prcdisc5, prcdisc6, prcdisc7, prcdisc8, prcdisc9) as Numeric(18, 6)), 0) > 0

Create Index idx_sxe_pdsc2 on ##tmp2_PDSC(levelcd, prctype, priceonty, pricesheet, pricetype, rebatetype, prodline, prodcat, custno, shipto, custtype, Vendno, promo)

Select
--Removed Basis_Calc_DN_FK on 10/18/2011 Replace(Replace(Replace(Replace(Replace(IsNull(Cast(Result_Data_Name_FK as Varchar), '0') + IsNull(Cast(LevelCode as Varchar), '0') + IsNull(Cast(Multiplier_Operation_FK as Varchar), '0') + IsNull(Cast(Discount_Operation_FK as Varchar), '0') + isnull(Cast(Promo as Varchar), '0') + IsNull(Cast(Basis_Calc_DN_FK as Varchar), '0') + IsNull(Cast(Isnull(Cust_Filter_Entity_DN_FK, Cust_Filter_DN_FK) as Varchar), '0') + IsNull(Cast(Cust_Filter_Value as Varchar), '0') + IsNull(Cast(Product_Filter_DN_FK as Varchar), '0') + IsNull(Cast(Product_Filter_Value as Varchar), '0') + IsNull(Cast(SUPL_FILTER_ENTITY_DN_FK as Varchar), '0') + IsNull(Cast(Supl_Filter_Value as Varchar), '0') + IsNull(Cast(ORG_FILTER_ENTITY_DN_FK as Varchar), '0') + IsNull(Cast(Org_Filter_Value as Varchar), '0') + IsNull(Cast(UOM_Code_FK as Varchar), '0') + IsNull(Cast(StartDt as Varchar), ''), '''', ''), ' ', ''), 'Null', ''), ',', ''), '"', '') Unique_Key1
Replace(Replace(Replace(Replace(Replace(
		IsNull(Cast(Result_Data_Name_FK as Varchar), '0') 
		+ IsNull(Cast(LevelCode as Varchar), '0') 
		+ IsNull(Cast(Multiplier_Operation_FK as Varchar), '0') 
		+ IsNull(Cast(Discount_Operation_FK as Varchar), '0') 
		+ isnull(Cast(Promo as Varchar), '0') 
		+ IsNull(Cast(Isnull(Cust_Filter_Entity_DN_FK, Cust_Filter_DN_FK) as Varchar), '0') 
		+ IsNull(Cast(Cust_Filter_Value as Varchar), '0') 
		+ IsNull(Cast(Product_Filter_DN_FK as Varchar), '0') 
		+ IsNull(Cast(Product_Filter_Value as Varchar), '0') 
		+ IsNull(Cast(Case SUPL_FILTER_ENTITY_DN_FK When 143099 then 0 else SUPL_FILTER_ENTITY_DN_FK end as Varchar), '0') 
		+ IsNull(Cast(Supl_Filter_Value as Varchar), '0') + IsNull(Cast(Case ORG_FILTER_ENTITY_DN_FK When 141099 then 0 else ORG_FILTER_ENTITY_DN_FK end as Varchar), '0') 
		+ IsNull(Cast(Org_Filter_Value as Varchar), '0') 
		+ IsNull(Cast(UOM_Code_FK as Varchar), '0') 
		+ IsNull(Convert(varchar(8), cast(StartDt as datetime), 1), '')
	, '''', ''), ' ', ''), 'Null', ''), ',', ''), '"', '') Unique_Key1
, *
Into ##PDSC
from
(Select 
pdrecno Host_Rule_Xref
, 111602 Result_Data_Name_FK
, (Select rules_structure_fk from [synchronizer].[RULES_STRUCTURE_ASSIGNMENT] with (nolock) where record_status_cr_fk = 1 and Result_Data_Name_FK = 111602 
	and host_precedence_level = pdsc.levelcd and Promo_Ind = Case When PDSC.promofl = 'Y' and pdsc.levelcd in ('7', '8') then 1 else 0 end
	And Rules_Data_Source = 'PDSC' And rules_hierarchy_fk = 
	(Select Rules_hierarchy_id from synchronizer.rules_hierarchy with (nolock) where RULES_PRECEDENCE_SCHEME_FK in
		(Select rules_precedence_scheme_id from synchronizer.rules_precedence_scheme with (nolock) where name =
		(Select value from epacube.epacube_params with (nolock) where name = 'PRICING PRECEDENCE')))
	) 	rules_structure_fk
, (Select rules_precedence_scheme_id from synchronizer.rules_precedence_scheme with (nolock) where name =
		(Select value from epacube.epacube_params with (nolock) where name = 'PRICING PRECEDENCE')) Rules_Precedence_Scheme_FK
, (Select rules_schedule_fk from [synchronizer].[RULES_STRUCTURE_ASSIGNMENT] with (nolock) where record_status_cr_fk = 1 and Result_Data_Name_FK = 111602
	and host_precedence_level = pdsc.levelcd and Promo_Ind = Case When PDSC.promofl = 'Y' and pdsc.levelcd in ('7', '8') then 1 else 0 end
	And Rules_Data_Source = 'PDSC' And rules_hierarchy_fk = 
	(Select Rules_hierarchy_id from synchronizer.rules_hierarchy with (nolock) where RULES_PRECEDENCE_SCHEME_FK in
		(Select rules_precedence_scheme_id from synchronizer.rules_precedence_scheme with (nolock) where name =
		(Select value from epacube.epacube_params with (nolock) where name = 'PRICING PRECEDENCE')))
	) rules_schedule_fk
	
, levelcd LevelCode
, StartDT StartDate
, Enddt EndDate
, Case prctype
	When '$' then 
		Case	When Multiplier_Ind = 1 and Multiple_Multiplier_Operands_Ind = 0 then 1021 --Fixed Value (N)
				When Multiplier_Ind = 1 and Multiple_Multiplier_Operands_Ind = 1 then 1023 --Fixed Value(O)
		end 
	When '%' then
		Case When Multiplier_Ind = 1 and Multiple_Multiplier_Operands_Ind = 0 then
						--Single Operand (N)   
					Case When priceonty in ('m', 'rm', 'mr')	then 1051 --'Gross Margin(N)'
																else 1031 --'MULTIPLIER(N)'
																-- ks 02/10/2012 change GM to Named Operation
																--               due to hardcoded nature specific to SX
					End
			 When Multiplier_Ind = 1 and Multiple_Multiplier_Operands_Ind = 1 then
						--Multiple Operands (O)
					Case When priceonty in ('m', 'rm', 'mr') 
						then 1053 --'Gross Margin(O)'
						else 1033 --'MULTIPLIER(O)'
					End
	End End 'Multiplier_Operation_FK'
, Case 
	When Discount_Ind = 1 and Multiple_Discount_Operands_Ind = 0 then 1041 --Discount(N)
	When Discount_Ind = 1 and Multiple_Discount_Operands_Ind = 1 then 1043 --Discount(O)
	End as 'Discount_Operation_FK'
,	Case when isnull(pricesheet, '') > '' then
		(select data_name_id from epacube.data_name with (nolock) where name = 'SX HIST ' + 
			Case When PriceOnty in ('CB', 'I') then 'REBATE COST'
				When PriceOnty in ('RC', 'MR', 'RM') then 'REPL COST'
				When PriceOnty = 'B' then 'BASE'
				When PriceOnty = 'L' then 'LIST'
				When PriceOnty in ('S', 'CS', 'MS') then 'STD COST'
				else PriceOnty end)
		When prctype = '$' then NULL --'FIXED VALUE'
		When prctype = '%' then
		Case When PriceOnty in ('RM', 'RC') then 111202 --(select value from epacube.epacube_params where name = 'pricing cost basis')
			When PriceOnty = 'I' then 231105
			When PriceOnty = 'B' then 231103
			When PriceOnty = 'L' then 231104
			When PriceOnty = 'S' then 231102
			When PriceOnty = 'P' then 111602
			else (Select Value from epacube.EPACUBE_PARAMS with (nolock) where NAME = 'PRICING COST BASIS')	
		End
	End as Basis_Calc_DN_FK
--, Case When PriceOnty in ('RM', 'RC') then (select value from epacube.epacube_params where name = 'pricing cost basis') end Basis1_DN_FK
, Case when isnull(pricesheet, '') > '' then ltrim(@HistNamePrefix + ltrim(rtrim(pricesheet))) end 'Basis_Calc_Pricesheet_Name'
, Case levelcd
		When 1 then 144111
		When 2 then 144111
		When 3 then 244900
		When 4 then 244900
		When 5 then 144111
		When 6 then 244900
		When 7 then 144111
		When 8 then 144111
	end as 'Cust_Filter_DN_FK'
, 	Case
		when isnull(ShipTo,'') <> '' then 144020 --'CUSTOMER SHIP TO' 
		when isnull(Custno,'') <> '' then 144010 --'CUSTOMER BILL TO'
	end as 'Cust_Filter_Entity_DN_FK'
, Case levelcd
		When 1 then
			Case when isnull(ShipTo,'') <> '' then [custno] + '-' + [ShipTo] else custno end
		When 2 then
			Case when isnull(ShipTo,'') <> '' then [custno] + '-' + [ShipTo] else custno end
		When 3 then custtype
		When 4 then custtype
		When 5 then 
			Case when isnull(ShipTo,'') <> '' then [custno] + '-' + [ShipTo] else custno end
		When 6 then custtype
		When 7 then Null
		When 8 then Null
	end as 'Cust_Filter_Value'
, Case levelcd
		When 1 then 110100 --PRODUCT ID
		When 2 then
			Case 
				when isnull(pricetype,'') <> '' then 211901 --'SX PRICE TYPE'
				when len(rtrim(ltrim(rebatetype))) > 8 or (len(rtrim(ltrim(rebatetype))) > 4 and ISNULL(rtrim(ltrim(rebatetype)), '') like '% %') then 211910 --'SX CONC REBATE TYPE & SUBTYPE'
				when isnull(rebatetype,'') <> '' then 211903--'SX REBATE TYPE'
				when isnull(prodline,'') <> '' then 211902 --'SX PRODUCT LINE'
				when isnull(prodcat,'') <> '' then 210901 --'SX PRODUCT CATEGORY'
			end
		When 3 then 110100 --PRODUCT ID
		When 4 then
			Case
				when isnull(pricetype,'') <> '' then 211901 --'SX PRICE TYPE'
				when len(rtrim(ltrim(rebatetype))) > 8 or (len(rtrim(ltrim(rebatetype))) > 4 and ISNULL(rtrim(ltrim(rebatetype)), '') like '% %') then 211910 --'SX CONC REBATE TYPE & SUBTYPE'
				when isnull(rebatetype,'') <> '' then 211903 --'SX REBATE TYPE'
				when isnull(prodcat,'') <> '' then 210901 --'SX PRODUCT CATEGORY'
			end
		When 7 then 110100 --PRODUCT ID
		When 8 then 211901 --'SX PRICE TYPE'
	end Product_Filter_DN_FK
, Case levelcd
		When 1 then prod
		When 2 then
			Case 
				when isnull(pricetype,'') <> '' then right(Rtrim(Ltrim(pricetype)), 4)
				--when len(rebatetype) > 8 then ltrim(right(Rebatetype,8))
				when isnull(rebatetype,'') <> '' then Replace(Replace(Replace(rtrim(ltrim(rebatetype)), '  ', ' '), '  ', ' '), '  ', ' ')
				when isnull(prodline,'') <> '' then prodline
				when isnull(prodcat,'') <> '' then right(Rtrim(Ltrim(prodcat)), 4)
			end
		When 3 then prod
		When 4 then
			Case
				when isnull(pricetype,'') <> '' then right(Rtrim(Ltrim(pricetype)), 4)
				--when len(rebatetype) > 8 then ltrim(right(Rebatetype,8))
				when isnull(rebatetype,'') <> '' then Replace(Replace(Replace(rtrim(ltrim(rebatetype)), '  ', ' '), '  ', ' '), '  ', ' ')
				when isnull(prodcat,'') <> '' then right(Rtrim(Ltrim(prodcat)), 4)
			end
		When 5 then null
		When 6 then null
		When 7 then prod
		When 8 then right(Rtrim(Ltrim(pricetype)), 4)
	end Product_Filter_Value
, CASE
		When isnull(Vendno,'') <> '' then 143110 --253501
		When isnumeric(pricetype) = 0 and Len(pricetype) > 4 then 143110 --253501
		When isnumeric(prodcat) = 0 and len(prodcat) > 4 then 143110 --253501
		else Null 
	End as SUPL_FILTER_DN_FK
, CASE
		When isnull(Vendno,'') <> '' then 143000 --253501
		When isnumeric(pricetype) = 0 and Len(pricetype) > 4 then 143000 --253501
		When isnumeric(prodcat) = 0 and len(prodcat) > 4 then 143000 --253501
		else Null 
	End as SUPL_FILTER_ENTITY_DN_FK
, CASE
		When isnull(Vendno,'') <> '' then 253501 ELSE NULL END SUPL_FILTER_ASSOC_DATA_NAME_FK
, Cast(CASE
		When isnull(Vendno,'') <> '' and Vendno <> '0' then Vendno
		When isnumeric(pricetype) = 0 and Len(pricetype) > 8 and left(pricetype, 1) = 'p' then left(substring(pricetype, 3, len(pricetype) - 2), len(pricetype) - 6)
		When isnumeric(prodcat) = 0 and len(prodcat) > 8 and left(prodcat, 1) = 'p' then left(Substring(prodcat, 3, Len(prodcat) - 2), len(prodcat) - 6)
		else Null 
	End as bigint) as SUPL_FILTER_VALUE
, Case When isnull(Whse, '') > '' then 141111 end Org_Filter_DN_FK
, Case When isnull(Whse, '') > '' then (Select Entity_data_name_fk from epacube.ENTITY_IDENTIFICATION with (nolock) where DATA_NAME_FK = 141111 and VALUE = [whse]) end Org_Filter_ENTITY_DN_FK
, Whse Org_Filter_Value 
, (Select UOM_CODE_ID from epacube.uom_code with (nolock) where UOM_Code = PDSC.units) UOM_Code_FK
, *
from 
##tmp2_PDSC PDSC) PDSC_Recs 

Create Index idx_tmp_pdsc on ##PDSC(Host_Rule_Xref, Result_Data_Name_FK, Org_Filter_DN_FK, Org_Filter_Entity_DN_FK
, Cust_Filter_DN_FK, Cust_Filter_Entity_DN_FK, Supl_Filter_Entity_DN_FK, SUPL_FILTER_ASSOC_DATA_NAME_FK)

Create Index idx_tmp_pdscB on ##PDSC(Host_Rule_Xref, Product_Filter_DN_FK, typecd, promo, StartDate, EndDate)


---------- Customer Customization Code - PDSC Supplemental Added August 21, 2013

Set @CustomCode_Last_Step = 
	(select Top 1 SQL_cd from dbo.MAUI_Config_ERP 
	where 1 = 1
	and ERP = (select value from epacube.epacube_params where APPLICATION_SCOPE_FK = 100 and [NAME] = 'ERP HOST')
	and usage = 'Customer_Customization'
	and [Table] = 'SYNCHRONIZER.RULES'
	AND [Temp_Table] = 'PDSC SUPPLEMENTAL'
	and epacube_customer = (select value from epacube.epacube_params where APPLICATION_SCOPE_FK = 100 and [NAME] = 'EPACUBE CUSTOMER'))

--Set @CustomCode_Last_Step = Replace(Replace(@CustomCode_Last_Step, '@Job_FK_i', @Job_FK_i), '@Job_FK', @Job_FK)

Exec(@CustomCode_Last_Step)

----------
--Added delete on 11/18/11 to check for and get rid of duplicate prod_filter_fk values
----------
--IF object_id('dbo.MAUI_A_Errors_PDSC') is not null
--drop table dbo.MAUI_A_Errors_PDSC

--Select * into dbo.MAUI_A_Errors_PDSC 
--from import.import_rules
--where 1 = 1 
--and host_rule_xref in 
--(
--Select pdrecno from import.import_sxe_pdsc with (nolock)
--where 1 = 1 
--and pdrecno in (select host_rule_xref from import.import_rules with (nolock) where unique_key1 in
--(select unique_key1 from import.import_rules with (nolock) group by unique_key1 having count(*) > 1))
--and isnull(Cast(enddt as datetime), getdate() + 1) > getdate()
--)

--delete from import.import_rules
--where 1 = 1 
--and host_rule_xref in 
--(
--Select pdrecno from import.import_sxe_pdsc with (nolock)
--where 1 = 1 
--and pdrecno in (select host_rule_xref from import.import_rules with (nolock) where unique_key1 in
--(select unique_key1 from import.import_rules with (nolock) group by unique_key1 having count(*) > 1))
--and isnull(Cast(enddt as datetime), getdate() + 1) > getdate()
--)

----------

Insert into import.import_rules
(Job_FK, Import_FileName, Record_No, Rules_Structure_Name, Rules_Schedule_Name, RULE_NAME, Result_Data_Name--, Result_Data_Name_FK
, Effective_Date, End_Date, Prod_Filter_DN, Prod_Filter_Value1, Org_Filter_DN, Org_Filter_Entity_DN, Org_Filter_Value1, Org_Filter_Assoc_DN
, Cust_Filter_DN, Cust_Filter_Entity_DN, Cust_Filter_Value1, Supl_Filter_DN, Supl_Filter_Entity_DN, Supl_Filter_Value1, Supl_Filter_Assoc_DN, Rule_Precedence, Host_Precedence_Level_Description, Host_Rule_Xref, Reference
, Contract_No, Display_Seq, Rule_Result_Type, Unique_Key1, Event_Action_CR_FK, Record_Status_CR_FK, Update_User, Create_User, system_id_dn, system_id_value)

Select
Job_FK
, PDSC.Import_FileName
, Row_Number() over (Order by pdrecno) Record_No
, (Select [Name] from synchronizer.rules_structure with (nolock) where rules_structure_id = PDSC.Rules_Structure_FK) Rules_Structure_Name
, (Select [Name] from synchronizer.rules_schedule with (nolock) where rules_schedule_id = PDSC.Rules_Schedule_FK) Rules_Schedule_Name
, 'PRC: ' + Replace(typecd, 'Customer Type', 'Cust Price Type') + ': ' + Host_Rule_Xref RULE_NAME
, (Select [Name] from epacube.data_name with (nolock) where data_name_id = PDSC.Result_Data_Name_FK) Result_Data_Name
, Cast(StartDate as datetime) Effective_Date
, Case When EndDate = '12/31/2199' then null else cast(EndDate as datetime) end End_Date
, (Select Name from epacube.data_name with (nolock) where data_name_id = PDSC.Product_Filter_DN_FK) Prod_Filter_DN
, Product_Filter_Value
, (Select Name from epacube.data_name with (nolock) where data_name_id = PDSC.Org_Filter_DN_FK) Org_Filter_DN
, (Select Name from epacube.data_name with (nolock) where data_name_id = PDSC.Org_Filter_Entity_DN_FK) Org_Filter_Entity_DN
, Org_Filter_Value
, Case When isnull(Org_Filter_Value, '') > '' then (Select Name from epacube.data_name with (nolock) where data_name_id = 159100) end Org_Filter_Assoc_DN
, (Select Name from epacube.data_name with (nolock) where data_name_id = PDSC.Cust_Filter_DN_FK) Cust_Filter_DN
, (Select Name from epacube.data_name with (nolock) where data_name_id = PDSC.Cust_Filter_ENTITY_DN_FK) Cust_Filter_Entity_DN
, Cust_Filter_Value
, Case When PDSC.Supl_Filter_DN_FK is not null then (Select Name from epacube.data_name with (nolock) where data_name_id = PDSC.Supl_Filter_DN_FK) end Supl_Filter_DN
, Case When PDSC.Supl_Filter_Entity_DN_FK is not null then (Select Name from epacube.data_name with (nolock) where data_name_id = PDSC.Supl_Filter_Entity_DN_FK) end Supl_Filter_Entity_DN
, Supl_Filter_Value
, Case When IsNull(Supl_Filter_Value, '') > '' then 
	(Select Name from epacube.data_name with (nolock) where data_name_id = PDSC.SUPL_FILTER_ASSOC_DATA_NAME_FK) end Supl_Filter_Assoc_DN
, (Select rule_precedence from synchronizer.RULES_Precedence Rprec with (nolock)
	where Rprec.RULES_Precedence_Scheme_FK = PDSC.Rules_Precedence_Scheme_FK
	and Rprec.PROD_Filter_DN_FK = ISNULL(PDSC.Product_Filter_DN_FK, 149099)
	and (isnull(Rprec.Cust_Filter_DN_FK, 144099) = isnull(PDSC.Cust_Filter_DN_FK, 144099) and isnull(Rprec.Cust_Filter_Entity_DN_FK, 144099) = isnull(PDSC.Cust_Filter_Entity_DN_FK, 144099))  
	and (isnull(Rprec.Org_Filter_DN_FK, 141111) = isnull(PDSC.Org_Filter_DN_FK, 141111) and isnull(Rprec.Org_Filter_Entity_DN_FK, 141099) = ISNULL(PDSC.Org_Filter_Entity_DN_FK, 141099))
	and (isnull(Rprec.SUPL_FILTER_DN_FK, 143110) = isnull(PDSC.SUPL_Filter_DN_FK, 143110) and isnull(Rprec.SUPL_FILTER_Entity_DN_FK, 143099) = ISNULL(PDSC.Supl_Filter_Entity_DN_FK, 143099))
	and Rprec.Record_Status_CR_FK = 1
	) 'RULE_PRECEDENCE'
, LevelCd + ': ' + typecd Host_Precedence_Level_Description
, Host_Rule_Xref
, refer Reference
, ContractNo Contract_No
, 999 DISPLAY_SEQ
, (Select Code from epacube.code_ref with (nolock) where code_ref_id = Case When Cast(StartDate as datetime) <= Getdate() then 710 else 711 end) Rule_Result_Type
, PDSC.Unique_Key1
, Null Event_Action_CR_FK
, Case When Isnull(EndDate, '12/31/2199') < Getdate() then 2 else 1 end Record_Status_CR_FK
, 'HOST PRICE RULES LOAD' 'UPDATE_USER'
, 'HOST PRICE RULES LOAD' 'CREATE_USER'
, 142111 'System_ID_DN'
, 'PDSC' 'System_ID_Value'
from ##PDSC PDSC
Order by Host_rule_xref

Insert into import.import_rules_data
(
Job_FK, Record_No, Rule_Name
, Rules_Action_Operation1, Rules_Action_Operation2
, Compare_Operator
, Basis_Calc_DN, Basis1_Number--, Basis1_DN
, Basis2_Number
, Basis_Calc_Pricesheet_Name
, rounding_method
, rounding_to_value
, Operand_Filter1_DN, Operand_Filter1_Operator, Rules_Filter_Lookup_Operand_Name1, Operand_1_1, Operand_1_2, Operand_1_3, Operand_1_4, Operand_1_5, Operand_1_6, Operand_1_7, Operand_1_8, Operand_1_9
, Filter_Value_1_1, Filter_Value_1_2, Filter_Value_1_3, Filter_Value_1_4, Filter_Value_1_5, Filter_Value_1_6, Filter_Value_1_7, Filter_Value_1_8, Filter_Value_1_9
, Operand_Filter2_DN, Operand_Filter2_Operator, Rules_Filter_Lookup_Operand_Name2, Operand_2_1, Operand_2_2, Operand_2_3, Operand_2_4, Operand_2_5, Operand_2_6, Operand_2_7, Operand_2_8, Operand_2_9
, Filter_Value_2_1, Filter_Value_2_2, Filter_Value_2_3, Filter_Value_2_4, Filter_Value_2_5, Filter_Value_2_6, Filter_Value_2_7, Filter_Value_2_8, Filter_Value_2_9
, Record_Status_CR_FK, Update_User
)
Select * from (
Select 
r.Job_FK
, r.Record_No
, r.Rule_Name
, Case	When Multiplier_ind = 1 then (Select Name from synchronizer.rules_action_Operation with (nolock) where rules_action_operation_ID = PDSC.Multiplier_Operation_FK) 
		When Discount_Ind = 1 then (Select Name from synchronizer.rules_action_Operation with (nolock) where rules_action_operation_ID = PDSC.Discount_Operation_FK) end Rules_Action_Operation1
, Case When Multiplier_Ind = 1 and Discount_Ind = 1 then (Select Name from synchronizer.rules_action_Operation with (nolock) where rules_action_operation_ID = PDSC.Discount_Operation_FK) end Rules_Action_Operation2
, '=' Compare_Operator
, (Select Name from epacube.data_name with (nolock) where data_name_id = pdsc.basis_calc_dn_fk) Basis_Calc_DN
, Case	when Multiplier_ind = 1 and Multiple_Multiplier_Operands_Ind = 0 and Multiple_Discount_Operands_Ind = 0 then isnull(Cast(Coalesce(prcmult1, prcmult2, prcmult3, prcmult4, prcmult5, prcmult6, prcmult7, prcmult8, prcmult9) as Numeric(18, 6)), 0) 
		When Discount_Ind = 1 and Multiple_Multiplier_Operands_Ind = 0 and Multiple_Discount_Operands_Ind = 0 then isnull(Cast(Coalesce(prcdisc1, prcdisc2, prcdisc3, prcdisc4, prcdisc5, prcdisc6, prcdisc7, prcdisc8, prcdisc9) as Numeric(18, 6)), 0)
		end 'Basis1_Number'
, Case When Multiplier_Ind = 1 and Discount_Ind = 1 and Multiple_Multiplier_Operands_Ind = 0 and Multiple_Discount_Operands_Ind = 0 then isnull(Cast(Coalesce(prcdisc1, prcdisc2, prcdisc3, prcdisc4, prcdisc5, prcdisc6, prcdisc7, prcdisc8, prcdisc9) as Numeric(18, 6)), 0)
		end 'Basis2_Number'
, Basis_Calc_Pricesheet_Name
, Case When PDSC.prctype = '$' then Null else
		Case	When PDSC.pround like '%n%' then 720
				When PDSC.pround like '%u%' then 721
				When PDSC.pround like '%d%' then 722
		end
  end
rounding_method_cr_fk
, Case When prctype = '$' then Null else
	case ptarget
		when '1' then 100
		when '2' then 10
		when '3' then 1
		when '4' then .10
		when '5' then .01
		when '6' then .001
		when '7' then .0001
		when '8' then .00001
		when '9' then [pexactrnd] 
	End End
rounding_to_value
, Case When qtybreak is not null then 'MODEL_SALES_QTY' When Multiple_Multiplier_Operands_Ind = 1 and Multiplier_ind = 1 then 'SX CUST PRICE CODE' When Multiple_Discount_Operands_Ind = 1 and Discount_ind = 1 then 'SX CUST DISC CODE' end Operand_Filter1_DN
, Case When qtybreak is not null then 'COALESCE QTY BRK' When Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1 then 'COALESCE <=' end Operand_Filter1_Operator

, Case When qtybreak is null and ((Multiple_Multiplier_Operands_Ind = 1 and Multiplier_ind = 1) Or (Multiple_Discount_Operands_Ind = 1 and Discount_ind = 1)) 
	and levelcode in ('3', '4', '6') and @Op_Lookup = 1 then 'SXE CPT specific Price and Discount Codes' end 'Rules_Filter_Lookup_Operand_Name1'

, Case When Multiple_Multiplier_Operands_Ind = 1 or (Multiple_Discount_Operands_Ind = 1 and Multiplier_ind = 1) then PDSC.prcmult1 When Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc1 end Operand_1_1
, Case When Multiple_Multiplier_Operands_Ind = 1 or (Multiple_Discount_Operands_Ind = 1 and Multiplier_ind = 1) then PDSC.prcmult2 When Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc2 end Operand_1_2
, Case When Multiple_Multiplier_Operands_Ind = 1 or (Multiple_Discount_Operands_Ind = 1 and Multiplier_ind = 1) then PDSC.prcmult3 When Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc3 end Operand_1_3
, Case When Multiple_Multiplier_Operands_Ind = 1 or (Multiple_Discount_Operands_Ind = 1 and Multiplier_ind = 1) then PDSC.prcmult4 When Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc4 end Operand_1_4
, Case When Multiple_Multiplier_Operands_Ind = 1 or (Multiple_Discount_Operands_Ind = 1 and Multiplier_ind = 1) then PDSC.prcmult5 When Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc5 end Operand_1_5
, Case When Multiple_Multiplier_Operands_Ind = 1 or (Multiple_Discount_Operands_Ind = 1 and Multiplier_ind = 1) then PDSC.prcmult6 When Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc6 end Operand_1_6
, Case When Multiple_Multiplier_Operands_Ind = 1 or (Multiple_Discount_Operands_Ind = 1 and Multiplier_ind = 1) then PDSC.prcmult7 When Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc7 end Operand_1_7
, Case When Multiple_Multiplier_Operands_Ind = 1 or (Multiple_Discount_Operands_Ind = 1 and Multiplier_ind = 1) then PDSC.prcmult8 When Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc8 end Operand_1_8
, Case When Multiple_Multiplier_Operands_Ind = 1 or (Multiple_Discount_Operands_Ind = 1 and Multiplier_ind = 1) then PDSC.prcmult9 When Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc9 end Operand_1_9

, Case When qtybreak is not null then qtybrk1 When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) then 1 End Filter_Value_1_1
, Case When qtybreak is not null then qtybrk2 When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) then 2 End Filter_Value_1_2
, Case When qtybreak is not null then qtybrk3 When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) then 3 End Filter_Value_1_3
, Case When qtybreak is not null then qtybrk4 When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) then 4 End Filter_Value_1_4
, Case When qtybreak is not null then qtybrk5 When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) then 5 End Filter_Value_1_5
, Case When qtybreak is not null then qtybrk6 When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) then 6 End Filter_Value_1_6
, Case When qtybreak is not null then qtybrk7 When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) then 7 End Filter_Value_1_7
, Case When qtybreak is not null then qtybrk8 When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) then 8 End Filter_Value_1_8
, Case When qtybreak is not null then Null When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) then 9 End Filter_Value_1_9
, Case When qtybreak is not null and (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then 'MODEL_SALES_QTY' When Multiple_Multiplier_Operands_Ind = 1 and Multiple_Discount_Operands_Ind = 1 And Multiplier_ind = 1 and Discount_ind = 1 then 'SX CUST DISC CODE' end Operand_Filter2_DN
, Case When qtybreak is not null and (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then 'COALESCE QTY BRK' When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then 'COALESCE <=' end Operand_Filter2_Operator
, Case When qtybreak is null and (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 
	and levelcode in ('3', '4', '6') and @Op_Lookup = 1 then 'SXE CPT specific Price and Discount Codes' end 'Rules_Filter_Lookup_Operand_Name2'
, Case When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc1 end Operand_2_1
, Case When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc2 end Operand_2_2
, Case When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc3 end Operand_2_3
, Case When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc4 end Operand_2_4
, Case When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc5 end Operand_2_5
, Case When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc6 end Operand_2_6
, Case When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc7 end Operand_2_7
, Case When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc8 end Operand_2_8
, Case When (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then PDSC.prcdisc9 end Operand_2_9

, Case When qtybreak is not null and (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then qtybrk1 When (Multiple_Multiplier_Operands_Ind = 1 and Multiple_Discount_Operands_Ind = 1) then 1 End Filter_Value_2_1
, Case When qtybreak is not null and (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then qtybrk2 When (Multiple_Multiplier_Operands_Ind = 1 and Multiple_Discount_Operands_Ind = 1) then 2 End Filter_Value_2_2
, Case When qtybreak is not null and (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then qtybrk3 When (Multiple_Multiplier_Operands_Ind = 1 and Multiple_Discount_Operands_Ind = 1) then 3 End Filter_Value_2_3
, Case When qtybreak is not null and (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then qtybrk4 When (Multiple_Multiplier_Operands_Ind = 1 and Multiple_Discount_Operands_Ind = 1) then 4 End Filter_Value_2_4
, Case When qtybreak is not null and (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then qtybrk5 When (Multiple_Multiplier_Operands_Ind = 1 and Multiple_Discount_Operands_Ind = 1) then 5 End Filter_Value_2_5
, Case When qtybreak is not null and (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then qtybrk6 When (Multiple_Multiplier_Operands_Ind = 1 and Multiple_Discount_Operands_Ind = 1) then 6 End Filter_Value_2_6
, Case When qtybreak is not null and (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then qtybrk7 When (Multiple_Multiplier_Operands_Ind = 1 and Multiple_Discount_Operands_Ind = 1) then 7 End Filter_Value_2_7
, Case When qtybreak is not null and (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then qtybrk8 When (Multiple_Multiplier_Operands_Ind = 1 and Multiple_Discount_Operands_Ind = 1) then 8 End Filter_Value_2_8
, Case When qtybreak is not null and (Multiple_Multiplier_Operands_Ind = 1 or Multiple_Discount_Operands_Ind = 1) and Multiplier_ind = 1 and Multiple_Discount_Operands_Ind = 1 then Null When (Multiple_Multiplier_Operands_Ind = 1 and Multiple_Discount_Operands_Ind = 1) then 9 End Filter_Value_2_9
, R.Record_Status_CR_FK
, 'HOST PRICE RULES LOAD' Update_User
from ##PDSC PDSC
	Inner Join Import.import_Rules R with (nolock) on PDSC.Host_Rule_Xref = R.Host_Rule_Xref
		And PDSC.Result_Data_Name_FK = 111602
) A  
Order by Record_No

delete from import.IMPORT_RULES_DATA where RECORD_NO in
(select max(record_no) from import.IMPORT_RULES with (nolock) where UNIQUE_KEY1 in
(select UNIQUE_KEY1 from import.import_rules with (nolock) where prod_filter_dn = 'SX CONC REBATE TYPE & SUBTYPE' group by unique_key1 having COUNT(*) > 1)
group by UNIQUE_KEY1)

delete from import.IMPORT_RULES where RECORD_NO in
(select max(record_no) from import.IMPORT_RULES with (nolock) where UNIQUE_KEY1 in
(select UNIQUE_KEY1 from import.import_rules with (nolock) where prod_filter_dn = 'SX CONC REBATE TYPE & SUBTYPE' group by unique_key1 having COUNT(*) > 1)
group by UNIQUE_KEY1)

Update MB
Set Sell_Price_Cust_Rules_FK = r.rules_id
from dbo.maui_basis mb
inner join synchronizer.RULES r with (nolock) on mb.sell_price_cust_host_rule_xref = r.HOST_RULE_XREF and r.RESULT_DATA_NAME_FK = 111602
where isnull(Sell_Price_Cust_Rules_FK, 0) <> r.rules_id

Update MBC
Set Sell_Price_Cust_Rules_FK = r.rules_id
from dbo.maui_basis_calcs mbc
inner join synchronizer.RULES r with (nolock) on mbc.sell_price_cust_host_rule_xref = r.HOST_RULE_XREF and r.RESULT_DATA_NAME_FK = 111602
where isnull(Sell_Price_Cust_Rules_FK, 0) <> r.rules_id

Update RS
Set sell_price_cust_rules_fk = r.rules_id
from dbo.MAUI_Rules_Summary RS
	Inner join synchronizer.rules r with (nolock) on RS.sell_price_cust_host_rule_xref = r.host_rule_xref
where isnull(Sell_Price_Cust_Rules_FK, 0) <> r.rules_id

Update MRM
Set Host_Rule_Xref = ir.Host_Rule_Xref
from dbo.MAUI_Rules_Maintenance MRM
	Inner Join Import.import_rules ir with (nolock)
	on MRM.Unique_Key1 = ir.Unique_Key1
Where MRM.Transferred_To_ERP = 1 and MRM.Host_Rule_Xref like 'New:%' and MRM.result_data_name_fk = 111602

Update R
Set Name = ir.rule_name
, host_rule_xref = ir.host_rule_xref
from Synchronizer.rules R
	Inner Join import.IMPORT_RULES ir with (nolock) 
	on ir.UNIQUE_KEY1 = r.unique_key1 and r.result_data_name_fk = 111602

If (select count(*) from import.import_rules with (nolock) where isnull(end_date, getdate() + 1) > getdate()) > (Select Cast(isnull((Select value from epacube.epacube_params with (nolock) where name = 'MIN RECORDS EXPECTED PRICES'), 10000000) as bigint))
Begin
	Update R
	Set Record_status_cr_fk = 2
	, End_Date = Getdate() -1
	from Synchronizer.rules R
	Left Join
	(Select 'PRC: ' + Replace(typecd, 'Customer Type', 'Cust Price Type') + ': ' + Host_Rule_XRef Rule_Name from ##PDSC union select rule_name from dbo.MAUI_Rules_Maintenance) Rls
	on rls.Rule_Name = r.Host_rule_xref
	where result_data_name_fk = 111602
	and Rules_ID >= 2000000000
	and record_status_cr_fk = 1
	and Rls.Rule_Name is null
End

--Set up currently inactive rules to be re-activated where host erp rules have been re-activated

	Update Rls
	Set Record_status_cr_fk = 1
	, end_date = Null
	from Synchronizer.rules rls
	inner join import.import_rules ir with (nolock) on rls.host_rule_xref = ir.host_rule_xref and rls.RESULT_DATA_NAME_FK = (select data_name_id from epacube.DATA_NAME with (nolock) where name = ir.Result_data_Name)
	where 1 = 1
	and rls.record_status_cr_fk = 2
	and isnull(ir.end_date, getdate() + 1) > getdate()
----

Update r
Set end_date = cast(pdsc.enddt as datetime)
, r.record_status_cr_fk = 1
from synchronizer.rules r
inner join ##PDSC pdsc on r.host_rule_xref = pdsc.Host_Rule_Xref
where end_date < getdate() 
and RECORD_STATUS_CR_FK = 2
and r.RESULT_DATA_NAME_FK = 111602
and (enddt is null or (isdate(enddt) = 1 and isnull(Cast(enddt as datetime), getdate() + 1) > getdate()))

Update DN
set record_status_cr_fk = 1
from epacube.data_name Dn
where record_status_cr_fk = 2
and name in (select basis_calc_dn from import.import_rules_data with (nolock) group by basis_calc_dn)

drop table ##tmp0_PDSC
Drop Table ##tmp_PDSC
Drop Table ##tmp2_PDSC
IF object_id('tempdb..##Corrupt') is not null
drop table ##Corrupt;

IF object_id('dbo.MAUI_A_Errors_PDSC') is not null
drop table dbo.MAUI_A_Errors_PDSC

IF object_id('tempdb..#dels') is not null
drop table #dels;

---LOAD INTO SYNCHRONIZER RULES TABLES

EXEC import.LOAD_IMPORT_RULES @IN_JOB_FK

Select host_rule_xref, prctype, pround, ptarget, pexactrnd into #rounding from ##PDSC where isnull(pround, '') <> '' and prctype <> '$'
create index idx_rnd on #rounding(host_rule_xref, prctype, pround)

--Temp fix to Rounding Instructions
Update ra
Set rounding_method_cr_fk = Case When PDSC.prctype = '$' then Null else
		Case	When PDSC.pround like '%n%' then 720
				When PDSC.pround like '%u%' then 721
				When PDSC.pround like '%d%' then 722
				else Null
	end end
, rounding_to_value = Case When prctype = '$' then Null else
	case pdsc.ptarget
		when '1' then 100
		when '2' then 10
		when '3' then 1
		when '4' then .10
		when '5' then .01
		when '6' then .001
		when '7' then .0001
		when '8' then .00001
		when '9' then [pexactrnd]
		else Null
	End End
from synchronizer.rules_action ra
inner join synchronizer.rules r with (nolock) on ra.rules_fk = r.rules_id and r.result_data_name_fk = 111602
inner join #rounding pdsc on r.host_rule_xref = pdsc.host_rule_xref
where r.result_data_name_fk = 111602

Drop Table ##PDSC
drop table #rounding

--Update Maui_Rules_Maintenance
If (Select Record_status_CR_FK from dbo.MAUI_Configuration where [Class] = 'Customer Price List Calculation' and Value = 'Customer Criteria') = 1
BEGIN
	Exec [import].[Import_To_Production_Rules_Maintenance_Update_From_SX]	
	Exec import.LOAD_MAUI_Z_RULES_DATA_ELEMENTS	
END

	Exec [CPL].[RULE_SETUP_DETAILS_GENERATION]

EndOfProc:
END

--N = Single Operand
--O = Multiple Operands
--D = Data Name 
--L = Lookup Table
