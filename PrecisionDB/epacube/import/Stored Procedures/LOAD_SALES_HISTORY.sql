﻿


-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        01/25/2011   Initial Version 
-- CV        06/07/2011   Added insert to Marginmgr.sales_history table
-- CV        08/04/2011   Making more generic renamed table. removed eclipse 
--							use epacube paramaters to look up data names
-- GHS		 06/15/2012   Updated to support 5.1.6 code and made a few corrections.


----------------------------------------------------------------------------------------
--------This procedure keeps the Import.Import_Sales_History_Active table current to set defaults.
-------- if you have special exclude logic please use the package 46000 in the 
-------- import package sql table to add special customer exclude logic.
-----loads order data history and sales history table   -- remove order data history eventually
----------------------------------------------------------------------------------------


CREATE PROCEDURE [import].[LOAD_SALES_HISTORY] ( @IN_JOB_FK bigint ) 
AS BEGIN 

Declare @Maintain_Rolling_History int; --- = 1
Declare @Months_To_Keep int;   ---= Null
Declare @Total_Refresh int;   ----= 0




     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY

set @Maintain_Rolling_history = 1
set @Total_refresh = 0 

      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_IMPORT_SALES_HISTORY', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.IMPORT_SALES_HISTORY WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM import.IMPORT_SALES_HISTORY WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD SALES HISTORY',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( JOB_ERRORS TABLE )
--
--     THESE ERRORS PROHIBIT THE ROWS FROM BEING IMPORTED
--
--	   ERROR NAME:  INCOMPLETE OR INVALID DATA PROHIBITING IMPORT 
--     EXAMPLE IS MISSING OR INVALID STRUCTURE
--
--     IMPORT ROW MARKED  ERROR_ON_IMPORT_IND = 1 
--
-------------------------------------------------------------------------------------
---Exclude Non stock items from this import --
--Products that do not have Burden cost (net value 4 on current price sheets 
-- marginmgr.sheet resulsts table) do not import
-- in this procure as per Todd.
 
---MOVE TO IMPORT.IMPORT PACKAGE SQL TABLE for package 460000--sales history

--EXECUTE  [import].[EXECUTE_IMPORT_SQL] 
--					460000
--					,@in_job_fk

--Declare @IN_JOB_FK bigint
--Set @IN_JOB_FK = (Select top 1 JOB_FK from import.IMPORT_SALES_HISTORY)

Delete from common.JOB_ERRORS 
where TABLE_NAME = 'IMPORT SALES HISTORY'

UPDATE import.IMPORT_SALES_HISTORY
SET ERROR_ON_IMPORT_IND = 1
where import.IMPORT_SALES_HISTORY.Product_ID in (
select iesh.product_ID from  import.IMPORT_SALES_HISTORY iesh
inner join epacube.product_identification pi
on (iesh.Product_ID = pi.value
and pi.data_name_FK = 310101)
--------select data_name_fk 
--------from epacube.EPACUBE_PARAMS where NAME = 'PRODUCT' and APPLICATION_SCOPE_FK = 8000))
inner join marginmgr.SHEET_RESULTS_VALUES_FUTURE sr
on (pi.PRODUCT_STRUCTURE_FK = sr.PRODUCT_STRUCTURE_FK
and sr.DATA_NAME_FK = 381100
and ISNULL(sr.NET_VALUE4,0)  = 0 ))      
AND   JOB_FK = @IN_JOB_FK 

----Do not load the negative order qty data

UPDATE import.IMPORT_SALES_HISTORY
SET ERROR_ON_IMPORT_IND = 1
where Cast(import.IMPORT_SALES_HISTORY.sales_Qty as Numeric(18, 6)) < 0
AND   JOB_FK = @IN_JOB_FK 

UPDATE import.IMPORT_SALES_HISTORY
SET PRODUCT_STRUCTURE_FK = (select pi.product_Structure_Fk
FROM epacube.product_identification pi
where import.IMPORT_SALES_HISTORY.Product_ID = pi.value
and pi.data_name_FK = (select value 
from epacube.EPACUBE_PARAMS where NAME = 'PRODUCT' and APPLICATION_SCOPE_FK = 8000))  --310101)
WHERE 1 = 1                   
AND   JOB_FK = @IN_JOB_FK 

update import.IMPORT_SALES_HISTORY
set error_on_import_ind = 1
where product_Structure_Fk is  NULL
and job_fk = @in_job_Fk

UPDATE import.IMPORT_SALES_HISTORY
SET ORG_ENTITY_STRUCTURE_FK = (select ei.entity_structure_fk 
from epacube.entity_identification ei where 
import.IMPORT_SALES_HISTORY.WHSE_ID = ei.value
and ei.entity_data_name_fk = 141000)
WHERE 1 = 1                   
AND   JOB_FK = @IN_JOB_FK 

update import.IMPORT_SALES_HISTORY
set error_on_import_ind = 1
where org_entity_Structure_Fk is  NULL
and job_fk = @in_job_Fk

---- CUST  -- Use the SHIPTO
UPDATE import.IMPORT_SALES_HISTORY
SET CUST_ENTITY_STRUCTURE_FK = (select ei.entity_structure_fk 
from epacube.entity_identification ei where 
import.IMPORT_SALES_HISTORY.Ship_To_Customer_Id= ei.value
and ei.entity_data_name_fk = 144020)
WHERE 1 = 1                   
AND   JOB_FK = @IN_JOB_FK 

update import.IMPORT_SALES_HISTORY
set error_on_import_ind = 1
where Cust_entity_Structure_Fk is  NULL
and job_fk = @IN_JOB_FK 

INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[TABLE_PK]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[ERROR_DATA4]
--           ,[ERROR_DATA5]
--           ,[ERROR_DATA6]
           ,[CREATE_TIMESTAMP])
SELECT 
JOB_FK
,'IMPORT SALES HISTORY'
,NULL
,99
,'MISSING SOME INFORMATION'
,Product_ID
,Ship_To_Customer_ID
,PRICE_MATRIX_ID_HISTORICAL
,SALES_ORDER_NUMBER
,getdate()
FROM import.IMPORT_SALES_HISTORY
WHERE ISNULL(ERROR_ON_IMPORT_IND ,0) = 1
AND   JOB_FK = @IN_JOB_FK


IF EXISTS (SELECT name FROM sysindexes WHERE name = 'idx_slshist')
drop index idx_slshist on import.IMPORT_SALES_HISTORY

create index idx_slshist on import.IMPORT_SALES_HISTORY(Sales_Order_Ship_Date, Whse_ID)

Declare @Months_Sales_History_To_Keep as Int
If @Months_To_Keep is null
	Set @Months_Sales_History_To_Keep = Isnull((Select top 1 Months_Sales_History_To_Keep from dbo.MAUI_Defaults), 12)
else
	Set @Months_Sales_History_To_Keep = @Months_To_Keep
	
If @Maintain_Rolling_History = 1
	Begin
		DELETE from import.IMPORT_SALES_HISTORY_ACTIVE
		where cast(Sales_Order_Ship_Date as datetime) < Cast(DATEADD(s,0,DATEADD(mm, DATEDIFF(m,0,getdate())-@Months_Sales_History_To_Keep,0)) as DATETIME)

		DELETE from import.IMPORT_SALES_HISTORY_ACTIVE
		where cast(Sales_Order_Ship_Date as datetime)  >= (Select MIN(Sales_Order_Ship_Date) from import.IMPORT_SALES_HISTORY)-- where transtype = 'stock order')
	End
Else
	Begin
		If @Total_Refresh = 1
		Truncate Table import.IMPORT_SALES_HISTORY_ACTIVE
	End
	
Insert into import.IMPORT_SALES_HISTORY_ACTIVE
([ANALYSIS_EFFECTIVE_DATE], [ANALYSIS_PERIOD_START_DATE], [ANALYSIS_PERIOD_END_DATE], [SALES_ORDER_ENTRY_DATE], [SALES_ORDER_SHIP_DATE], [SALES_ORDER_NUMBER], [SALES_ORDER_SUFFIX], [SALES_ORDER_LINE], [ANALYSIS_DATA_SET_ABBREV], [MATRIX_UOM], [PRODUCT_ID], [SHIP_TO_CUSTOMER_ID], [BILL_TO_CUSTOMER_ID], [WHSE_ID], [WHSE_GROUP_ID], [MFR_ID], [VENDOR_ID], [SALES_TERRITORY_ID], [OUTSIDE_SALESREP_ID], [INSIDE_SALESREP_ID], [ORDER_TAKER_ID], [BUYER_ID], [SALES_QTY], [SALES_UOM], [SALES_PRIMARY_UOM_CONVERSION], [HISTORICAL_UOM], [HISTORICAL_PRIMARY_UOM_CONVERSION], [SPECIAL_UOM_CONVERSION], [CURRENCY], [SELL_PRICE_AMT], [SELL_PRICE_MATRIX_AMT], [SELL_PRICE_OVERRIDE_AMT], [ORDER_COGS_AMT], [ORDER_COGS_OVERRIDE_AMT], [REPL_COST_AMT], [MARGIN_COST_AMT], [REBATED_COST_CB_AMT], [REBATE_CB_AMT], [REBATE_BUY_AMT], [PRICE_MATRIX_ID_HISTORICAL], [PRICE_CONTRACT_NO_HISTORICAL], [PRICE_CONTRACT_REFERENCE_HISTORICAL], [PRICE_FORMULA_HISTORICAL], [PRICE_BASIS_COLMUN_HISTORICAL], [REBATE_CB_MATRIX_ID_HISTORICAL], [REBATE_CB_CONTRACT_NO_HISTORICAL], [REBATE_CB_CONTRACT_REFERENCE_HISTORICAL], [REBATE_CB_FORMULA_HISTORICAL], [REBATE_CB_BASIS_COLMUN_HISTORICAL], [REBATE_BUY_MATRIX_ID_HISTORICAL], [REBATE_BUY_CONTRACT_NO_HISTORICAL], [REBATE_BUY_CONTRACT_REFERENCE_HISTORICAL], [REBATE_BUY_FORMULA_HISTORICAL], [REBATE_BUY_BASIS_COLMUN_HISTORICAL], [JOB_FK], [IMPORT_FILENAME], [PRODUCT_STRUCTURE_FK], [ORG_ENTITY_STRUCTURE_FK], [CUST_ENTITY_STRUCTURE_FK], [ERROR_ON_IMPORT_IND])
Select 
[ANALYSIS_EFFECTIVE_DATE], [ANALYSIS_PERIOD_START_DATE], [ANALYSIS_PERIOD_END_DATE], [SALES_ORDER_ENTRY_DATE], [SALES_ORDER_SHIP_DATE], [SALES_ORDER_NUMBER], [SALES_ORDER_SUFFIX], [SALES_ORDER_LINE], [ANALYSIS_DATA_SET_ABBREV], [MATRIX_UOM], [PRODUCT_ID], [SHIP_TO_CUSTOMER_ID], [BILL_TO_CUSTOMER_ID], [WHSE_ID], [WHSE_GROUP_ID], [MFR_ID], [VENDOR_ID], [SALES_TERRITORY_ID], [OUTSIDE_SALESREP_ID], [INSIDE_SALESREP_ID], [ORDER_TAKER_ID], [BUYER_ID], [SALES_QTY], [SALES_UOM], [SALES_PRIMARY_UOM_CONVERSION], [HISTORICAL_UOM], [HISTORICAL_PRIMARY_UOM_CONVERSION], [SPECIAL_UOM_CONVERSION], [CURRENCY], [SELL_PRICE_AMT], [SELL_PRICE_MATRIX_AMT], [SELL_PRICE_OVERRIDE_AMT], [ORDER_COGS_AMT], [ORDER_COGS_OVERRIDE_AMT], [REPL_COST_AMT], [MARGIN_COST_AMT], [REBATED_COST_CB_AMT], [REBATE_CB_AMT], [REBATE_BUY_AMT], [PRICE_MATRIX_ID_HISTORICAL], [PRICE_CONTRACT_NO_HISTORICAL], [PRICE_CONTRACT_REFERENCE_HISTORICAL], [PRICE_FORMULA_HISTORICAL], [PRICE_BASIS_COLMUN_HISTORICAL], [REBATE_CB_MATRIX_ID_HISTORICAL], [REBATE_CB_CONTRACT_NO_HISTORICAL], [REBATE_CB_CONTRACT_REFERENCE_HISTORICAL], [REBATE_CB_FORMULA_HISTORICAL], [REBATE_CB_BASIS_COLMUN_HISTORICAL], [REBATE_BUY_MATRIX_ID_HISTORICAL], [REBATE_BUY_CONTRACT_NO_HISTORICAL], [REBATE_BUY_CONTRACT_REFERENCE_HISTORICAL], [REBATE_BUY_FORMULA_HISTORICAL], [REBATE_BUY_BASIS_COLMUN_HISTORICAL], [JOB_FK], [IMPORT_FILENAME], [PRODUCT_STRUCTURE_FK], [ORG_ENTITY_STRUCTURE_FK], [CUST_ENTITY_STRUCTURE_FK], [ERROR_ON_IMPORT_IND] 
from import.IMPORT_SALES_HISTORY where cast(Sales_Order_Ship_Date as datetime)  > isnull((Select Max(Sales_Order_Ship_Date) from import.IMPORT_SALES_HISTORY_ACTIVE), '2001-01-01')
and cast(Sales_Order_Ship_Date as datetime) >= Cast(DATEADD(s,0,DATEADD(mm, DATEDIFF(m,0,getdate())-@Months_Sales_History_To_Keep,0)) as DATETIME)
AND ISNULL(ERROR_ON_IMPORT_IND ,0) = 0
AND JOB_FK = @IN_JOB_FK

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'idx_slshistact')
drop index idx_slshistact on import.IMPORT_SALES_HISTORY_ACTIVE

create index idx_slshistact on import.IMPORT_SALES_HISTORY_active(Product_ID, Bill_To_Customer_ID, Ship_To_Customer_ID, whse_ID, Sales_Order_Ship_Date, Vendor_ID, SALES_PRIMARY_UOM_CONVERSION)


----add some logging

 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD SALES HISTORY' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD SALES HISTORY' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

 SET @status_desc =  'finished execution of import.IMPORT_SALES_HISTORY'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_IMPORT_SALES_HISTORY has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END



















