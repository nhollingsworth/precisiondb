﻿













-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Import Record data table
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        01/06/2011   Initial Version 
-- CV        


CREATE PROCEDURE [import].[ECL_CUSTSHIP_TRANSFORMATION] ( @IN_JOB_FK bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to first create the Customer Bill to
Then need to run thru and create the customer ship to.
two differnt packages and two different job numbers...
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure 
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--     
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int 
	DECLARE @v_import_package_fk   int  
	DECLARE @v_column_name   varchar(25) 
	DECLARE @v_primary   varchar(25)   
	DECLARE @epa_job_id BIGINT
     	
          

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.SXE_CUSTOMER_TRANSFORMATION', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


--INSERT INTO RECORD DATA FOR SX CUSTOMER SHIP TO RECORDS
--NEED TO GET NEW JOB NUMBER??

 EXEC [common].[JOB_CREATE] @IN_JOB_FK = NULL,
                       @IN_job_class_fk = 100,
                       @IN_JOB_NAME = 'ECL CUSTOMERS (SHIP TO)',
                       @IN_DATA1 = NULL,
                       @IN_DATA2 = NULL,
                       @IN_DATA3 = NULL,
                       @IN_JOB_USER = NULL,
                       @OUT_JOB_FK = @epa_job_id OUTPUT,
                       @CLIENT_APP = 'IMPORT' ;

--THEN UPDATE IMPORT_RECORD DATA WITH IT
--update import.import_sxe_customers
--set job_fk = @epa_job_id
--where ship_to is not null
--and job_fk = @in_job_fk

--------------------------------------------------------------------------------------
--  CREATE BILL TO RECORD FROM RAW DATA
--------------------------------------------------------------------------------------

IF object_id('tempdb..##ST_Deletes') is not null
drop table ##ST_Deletes

INSERT INTO [import].[IMPORT_RECORD_DATA]
           ([IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[SEQ_ID]
           ,[JOB_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[DATA_POSITION1]
           ,[DATA_POSITION2]
           ,[DATA_POSITION3]
           ,[DATA_POSITION4]
           ,[DATA_POSITION5]
			,[DATA_POSITION6]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
)
(select distinct
'ECL CUSTOMERS (SHIP TO)'
,NULL
,NULL
,@epa_job_id
,342000 -- ECL SHIP TO CUSTOMERS
,CUST_ID
,CUST_NAME
,Class
,Rank
,type
,(select case when Bill_to_cust_id is NULL then CUST_ID else Bill_to_cust_id end)
,getdate()
,'IMPORT'
FROM epacube.dbo.ecl_custs
where 1 =1 
and ship_to_ind = 'yes'  --1
)



update import.IMPORT_RECORD_DATA
set DATA_POSITION3 =  Class
from epacube.dbo.ecl_custs
where epacube.dbo.ecl_custs.CUST_ID = import.IMPORT_RECORD_DATA.DATA_POSITION6
And DATA_POSITION3 is NULL
and import.import_record_data.JOB_FK = @epa_job_id


update import.IMPORT_RECORD_DATA
set DATA_POSITION4 =  Rank
from epacube.dbo.ecl_custs
where epacube.dbo.ecl_custs.CUST_ID = import.IMPORT_RECORD_DATA.DATA_POSITION6
And DATA_POSITION4 is NULL
and import.import_record_data.JOB_FK = @epa_job_id

update import.IMPORT_RECORD_DATA
set DATA_POSITION5 =  Type
from epacube.dbo.ecl_custs
where epacube.dbo.ecl_custs.CUST_ID = import.IMPORT_RECORD_DATA.DATA_POSITION6
And DATA_POSITION5 is NULL
and import.import_record_data.JOB_FK = @epa_job_id


IF object_id('epacube.dbo.ecl_custs') is not null
drop table epacube.dbo.ecl_custs

		
--LOOKUP PRIMARY CUSTOMER BY PACKAGE
SET @v_import_package_fk = (select distinct import_package_fk 
							from import.import_record_data WITH (NOLOCK)
							where  job_fk = @epa_job_id)

SET @v_column_name = (select column_name 
						from import.import_map_metadata WITH (NOLOCK)
						where import_package_fk = @v_import_package_fk
						and data_name_fk = (select data_name_id
from epacube.data_name where name = 'CUSTOMER SHIP TO'))  

SET @v_primary = (select source_field_name
						from import.import_map_metadata WITH (NOLOCK)
						where import_package_fk = @v_import_package_fk
						and data_name_fk =(select data_name_id
from epacube.data_name where name = 'CUSTOMER SHIP TO'))  



/* ----------------------------------------------------------------------------------- */
--   --Create Temp table to insert distinct customer data 
/* ----------------------------------------------------------------------------------- */


--drop temp table if it exists
	IF object_id('tempdb..#TS_DISTINCT_CUSTOMERS') is not null
	   drop table #TS_DISTINCT_CUSTOMERS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_DISTINCT_CUSTOMERS(
	IMPORT_RECORD_DATA_FK	BIGINT,
	[IMPORT_FILENAME] [varchar](128),
	[RECORD_NO] [int],
	[JOB_FK] [bigint],
	[IMPORT_PACKAGE_FK] [int],
	[EFFECTIVE_DATE] [datetime],
	[END_DATE] [datetime],
	[ENTITY_CLASS] [varchar](64) ,
	[ENTITY_DATA_NAME] [varchar](64),
	[ENTITY_ID_DATA_NAME] [varchar](64),
	[ENTITY_ID_VALUE] [varchar](64),
	[ENTITY_STATUS] [varchar](32),
	[SYSTEM_ID_VALUE] [varchar](64),
	[PRIORITY] [varchar](32),
	[SEARCH_ACTIVITY_FK] [int],
	[UPDATE_TIMESTAMP] [datetime],
	[UPDATE_USER] [varchar](64)
		)



-------------------------------------------------------------------------------------
--   LOAD TEMP TABLE WITH DISTINCT PRODUCT DATA
--   NEED to check if NULL Then do something else to get product identification 
-------------------------------------------------------------------------------------


SET @ls_exec =
						'INSERT INTO #TS_DISTINCT_CUSTOMERS
						(   [JOB_FK]
						   ,[IMPORT_PACKAGE_FK]
						   ,[END_DATE]
						   ,[ENTITY_CLASS]
						   ,[ENTITY_DATA_NAME]
						   ,[ENTITY_ID_DATA_NAME]
						   ,[ENTITY_ID_VALUE]
						   )
						SELECT distinct
							IRD.JOB_FK
							,IRD.IMPORT_PACKAGE_FK
						    ,IRD.END_DATE '
						   + ', ' 
						   +  ' ''CUSTOMER'' '
						   + ', '  
						   + ' ''CUSTOMER SHIP TO'' '
						    + ', ''' 
						   + @v_primary
						    + ''', '
						   + @v_column_name
						   + ' ' 
						+ ' FROM IMPORT.IMPORT_RECORD_DATA  IRD WITH (NOLOCK) '
						+ ' WHERE 1=1 '
						+ ' AND isnull(DO_NOT_IMPORT_IND,0) <> 1'
						+ ' AND '
						+ @v_column_name 
						+ ' IS NOT NULL'
						+ ' AND ISNULL(IRD.JOB_FK,-999) = '
						+ cast(isnull (@epa_job_id, -999) as varchar(20))



----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), @ls_exec )

         exec sp_executesql 	@ls_exec;

-----

UPDATE #TS_DISTINCT_CUSTOMERS
				SET RECORD_NO = x.ROW 
				,import_filename = (SELECT Top 1 import.import_record_data.import_filename
											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
											WHERE 1=1 
											AND Job_Fk = @epa_job_id)
				,effective_date = ISNULL((SELECT Top 1 import.import_record_data.effective_date
											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
											WHERE 1=1 
											AND Job_Fk = @epa_job_id), getdate())
				FROM (SELECT ROW_NUMBER() 
						OVER (ORDER BY ENTITY_ID_VALUE) AS Row, 
						ENTITY_ID_VALUE
				FROM #TS_DISTINCT_CUSTOMERS where job_fk = @epa_job_id)x
				where #TS_DISTINCT_CUSTOMERS.entity_id_value = x.entity_id_value
				AND #TS_DISTINCT_CUSTOMERS.job_Fk = @epa_job_id



-----------------------------------------------------
	


-------------------------------------------------------------------------------------
--   LOAD IMPORT_RECORD TABLE(S) 
-------------------------------------------------------------------------------------

					INSERT INTO IMPORT.IMPORT_RECORD 
							   ([IMPORT_FILENAME]
							   ,[RECORD_NO]
							   ,[JOB_FK]
							   ,[IMPORT_PACKAGE_FK]
							   ,[EFFECTIVE_DATE]
							   ,[END_DATE]
							   ,[ENTITY_CLASS]
							   ,[ENTITY_DATA_NAME]
							   ,[ENTITY_ID_DATA_NAME]
							   ,[ENTITY_ID_VALUE]
							   ,[ENTITY_STATUS]
							   ,[UPDATE_TIMESTAMP]
							   ,[UPDATE_USER])

					SELECT 
							   TDP.IMPORT_FILENAME
							   ,TDP.RECORD_NO
							   ,TDP.JOB_FK
							   ,TDP.IMPORT_PACKAGE_FK
							   ,TDP.EFFECTIVE_DATE
							   ,TDP.END_DATE
							   ,TDP.ENTITY_CLASS
							   ,TDP.ENTITY_DATA_NAME
							   ,TDP.ENTITY_ID_DATA_NAME
							   ,TDP.ENTITY_ID_VALUE
							   ,'ACTIVE'
							   ,TDP.UPDATE_TIMESTAMP
							   ,TDP.UPDATE_USER
					FROM #TS_DISTINCT_CUSTOMERS TDP
					WHERE 1=1
					AND TDP.JOB_FK = @epa_job_id


-------------------------------------------------------------------------
 SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @epa_job_id;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @epa_job_id;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'ECL CUSTOMER TRANSFORMATION',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


-------------------------------------------------------------------------------------
--   UPDATE IMPORT RECORD WITH RECORD_NO and SEQ_ID
-------------------------------------------------------------------------------------

			SET @ls_exec =
						'UPDATE IMPORT.IMPORT_RECORD_DATA
						set RECORD_NO = import.import_record.record_no
						from import.import_record WITH (NOLOCK)
						where'
						+ ' '
						+ @v_column_name 
						+ ' = entity_id_value
						and import.import_record_data.JOB_FK = '
						+ cast(isnull (@epa_job_id, -999) as varchar(20))
						+ ' and import.import_record.JOB_FK = '
						+ cast(isnull (@epa_job_id, -999) as varchar(20))


----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), @ls_exec )

         exec sp_executesql 	@ls_exec;


-----------

				UPDATE IMPORT.IMPORT_RECORD_DATA
				SET SEQ_ID = x.ROW 
				FROM (SELECT ROW_NUMBER() 
				OVER (ORDER BY IMPORT_RECORD_DATA_ID) AS Row, 
					 IMPORT_RECORD_DATA_ID
			FROM IMPORT.IMPORT_RECORD_DATA WITH (NOLOCK)
			where job_fk = @epa_job_id)x
			WHERE import.import_record_data.job_fk = @epa_job_id
			AND import.import_record_data.import_record_data_id = x.import_record_data_id

------------------------------------------------------
---INSERT DATA INTO IMPORT MULTI VALUE TABLE
------------------------------------------------------
--CUST PRICE TYPE

DECLARE 
               @vm$file_name varchar(100),
               @vm$record_no varchar(64),         
               @vm$job_fk int,
               @vm$import_package_fk bigint,                                           
               @vm$data_name varchar(64),
			   @vm$data_name_value varchar(64)

	DECLARE cur_v_import CURSOR local for
		SELECT  ird.IMPORT_FILENAME
			   ,ird.RECORD_NO
			   ,ird.job_fk
			   ,ird.IMPORT_PACKAGE_FK
			   ,'SX CUST PRICE TYPE'
               ,data_position6
	  from import.import_record_data ird
		where ird.import_package_fk = 242000
		and ird.job_fk = @epa_job_id
        and ird.data_position6 is not null

         OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO @vm$file_name,
                                           @vm$record_no,         
                                           @vm$job_fk,
                                           @vm$import_package_fk,                                           
                                           @vm$data_name,
										   @vm$data_name_value
      WHILE @@FETCH_STATUS = 0
      BEGIN


		INSERT INTO [import].[IMPORT_RECORD_NAME_VALUE]
           ([IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[JOB_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[DATA_NAME]
           ,[DATA_VALUE]
           )
		select
			 @vm$file_name,
             @vm$record_no,         
             @vm$job_fk,
             @vm$import_package_fk,                                           
             @vm$data_name,
			 ss.ListItem
		from utilities.SplitString ( @vm$data_name_value, ',' ) ss	 




 FETCH NEXT FROM cur_v_import Into @vm$file_name,
                                   @vm$record_no,         
                                   @vm$job_fk,
                                   @vm$import_package_fk,                                           
                                   @vm$data_name,
								   @vm$data_name_value


END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_import
DEALLOCATE cur_v_import    


--CUST REBATE TYPE


	DECLARE cur_v_import CURSOR local for
		SELECT  ird.IMPORT_FILENAME
			   ,ird.RECORD_NO
			   ,ird.job_fk
			   ,ird.IMPORT_PACKAGE_FK
			   ,'SX CUST REBATE TYPE'
               ,data_position12
	  from import.import_record_data ird
		where ird.import_package_fk = 242000
		and ird.job_fk = @epa_job_id
        and ird.data_position12 is not null


         OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO @vm$file_name,
                                           @vm$record_no,         
                                           @vm$job_fk,
                                           @vm$import_package_fk,                                           
                                           @vm$data_name,
										   @vm$data_name_value
      WHILE @@FETCH_STATUS = 0
      BEGIN


		INSERT INTO [import].[IMPORT_RECORD_NAME_VALUE]
           ([IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[JOB_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[DATA_NAME]
           ,[DATA_VALUE]
           )
		select
			 @vm$file_name,
             @vm$record_no,         
             @vm$job_fk,
             @vm$import_package_fk,                                           
             @vm$data_name,
			 ss.ListItem
		from utilities.SplitString ( @vm$data_name_value, ',' ) ss	 




 FETCH NEXT FROM cur_v_import Into @vm$file_name,
                                   @vm$record_no,         
                                   @vm$job_fk,
                                   @vm$import_package_fk,                                           
                                   @vm$data_name,
								   @vm$data_name_value


END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_import
DEALLOCATE cur_v_import    



------------------------------------------------------
---CALL STAGE TO EVENTS
------------------------------------------------------
     SET @V_START_TIMESTAMP = getdate()			


			 EXEC[stage].[STG_TO_EVENTS] @epa_JOB_ID
	

      SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @epa_job_id;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @epa_job_id;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @epa_job_id, 'STAGE TO EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



-------------------------------------------------------------------------------------
-- LAST Step is to Set Job Execution to Complete
-------------------------------------------------------------------------------------
----
----

      SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @epa_job_id
                              and   name = 'IMPORT ENTITIES TO STAGE' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @epa_job_id
                              and   name = 'IMPORT ENTITIES RESOLVED' )

     exec common.job_execution_create  @epa_job_id, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
		update common.job
		set job_complete_timestamp = getdate()
		where job_id = @epa_job_id



 SET @status_desc =  'finished execution of stage.ECL_CUSTOMER_TRANSFORMATION'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of STAGE.ECL_CUSTOMER_TRANSFORMATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END


































































































