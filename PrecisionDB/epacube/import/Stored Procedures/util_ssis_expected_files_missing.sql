﻿CREATE PROCEDURE [import].[util_ssis_expected_files_missing]

AS
BEGIN
	SET NOCOUNT ON;

--	From Revised Carrier Code 12/15/2015
---------------------------------------------------------------------------------------------
-- Variable decleration
---------------------------------------------------------------------------------------------
	Declare @Dir    VARCHAR(1000)

    declare @curdir nvarchar(400)
    declare @line varchar(400)
    declare @command varchar(400)
    declare @counter int

    DECLARE @1MB    DECIMAL
    SET     @1MB = 1024 * 1024

    DECLARE @1KB    DECIMAL
    SET     @1KB = 1024 

---------------------------------------------------------------------------------------------
-- Temp tables creation
---------------------------------------------------------------------------------------------

If OBJECT_ID('tempdb..#Tempoutput') is not null
DROP TABLE #Tempoutput  

If OBJECT_ID('tempdb..#dirs') is not null
DROP TABLE #dirs

If OBJECT_ID('tempdb..#tempFilePaths') is not null
DROP TABLE #tempFilePaths

If OBJECT_ID('tempdb..#tempFileInformation') is not null
DROP TABLE #tempFileInformation  

If OBJECT_ID('tempdb..#EFiles') is not null
Drop table #EFiles

If OBJECT_ID('Output') is not null
DROP TABLE output
	
CREATE TABLE #dirs (DIRID int identity(1,1), directory varchar(400))

If OBJECT_ID('epacube.import.ss_config_files_in_place') is null
Begin
	Create Table epacube.import.ss_config_files_in_place(ss_config_files_in_place_id int identity(1,1), ImportPath varchar(128), FileNameStart varchar(64), [FileName] varchar(128), FileCount int, Insert_Timestamp datetime)
	ALTER TABLE [import].[ss_config_files_in_place] ADD  CONSTRAINT [DF_ss_config_files_in_place_Insert_Timestamp]  DEFAULT (getdate()) FOR [Insert_Timestamp]
End

CREATE TABLE #tempoutput (line varchar(400))
CREATE TABLE output (Directory varchar(400), FilePath VARCHAR(400), SizeInMB DECIMAL(13,2), SizeInKB DECIMAL(13,2))

CREATE TABLE #tempFilePaths (Files VARCHAR(500))
CREATE TABLE #tempFileInformation (FilePath VARCHAR(500), FileSize VARCHAR(100))

	Select 
	IMPORT_FILE_PATH + '\Import' 'ImportPath'
	, REPLACE(REPLACE(REPLACE( REPLACE( REPLACE( REPLACE(
		Package_Name
		, 'Import_SX_', ''), 'Import_', ''), 'Customers', 'Cust'), '.dtsx', ''), 'PDSC', 'Price'), 'PDSR', 'Rebate') 'FileNameStart'
	, CAST(Null as int) 'FileCount'
	into #EFiles
	from epacube.[import].[SSIS_CONFIG_PARAMS] with (nolock) where record_status_cr_fk = 1
	and 
	package_Name in ('IMPORT_SX_ICSP.dtsx', 'IMPORT_SX_ICSW.dtsx', 'Import_SX_CUSTOMERS.dtsx', 'IMPORT_ZCURVE.dtsx', 'IMPORT_PDSR.dtsx', 'IMPORT_PDSC.dtsx', 'IMPORT_SX_TABLE.dtsx')
	
	
DECLARE FileNames Cursor local for
	
	Select ImportPath from #EFiles group by ImportPath

OPEN FileNames
	
Fetch Next From FileNames into @DIR

	WHILE @@FETCH_STATUS = 0
	Begin

---------------------------------------------------------------------------------------------
-- Call xp_cmdshell
---------------------------------------------------------------------------------------------    

     SET @command = 'dir "'+ @Dir +'" /S/O/B/A:D'
     INSERT INTO #dirs exec xp_cmdshell @command
     INSERT INTO #dirs SELECT @Dir
     SET @counter = (select count(*) from #dirs)

---------------------------------------------------------------------------------------------
-- Process the return data
---------------------------------------------------------------------------------------------      

        WHILE @Counter <> 0
          BEGIN
            DECLARE @filesize INT
            SET @curdir = (SELECT directory FROM #dirs WHERE DIRID = @counter)
            SET @command = 'dir "' + @curdir +'"'
            ------------------------------------------------------------------------------------------
                -- Clear the table
                DELETE FROM #tempFilePaths


                INSERT INTO #tempFilePaths
                EXEC MASTER..XP_CMDSHELL @command 

                --delete all directories
                DELETE #tempFilePaths WHERE Files LIKE '%<dir>%'

                --delete all informational messages
                DELETE #tempFilePaths WHERE Files LIKE ' %'

                --delete the null values
                DELETE #tempFilePaths WHERE Files IS NULL

                --get rid of dateinfo
                UPDATE #tempFilePaths SET files =RIGHT(files,(LEN(files)-20))

                --get rid of leading spaces
                UPDATE #tempFilePaths SET files =LTRIM(files)

                --split data into size and filename
                ----------------------------------------------------------
                -- Clear the table
                DELETE FROM #tempFileInformation;

                -- Store the FileName & Size
                INSERT INTO #tempFileInformation
                SELECT  
                        RIGHT(files,LEN(files) -PATINDEX('% %',files)) AS FilePath,
                        LEFT(files,PATINDEX('% %',files)) AS FileSize
                FROM    #tempFilePaths

                --------------------------------
                --  Remove the commas
                UPDATE  #tempFileInformation
                SET     FileSize = REPLACE(FileSize, ',','')



                --------------------------------------------------------------
                -- Store the results in the output table
                --------------------------------------------------------------

                INSERT INTO output--(FilePath, SizeInMB, SizeInKB)
                SELECT  
                        @curdir,
                        FilePath,
                        CAST(CAST(FileSize AS DECIMAL(13,2))/ @1MB AS DECIMAL(13,2)),
                        CAST(CAST(FileSize AS DECIMAL(13,2))/ @1KB AS DECIMAL(13,2))
                FROM    #tempFileInformation

            --------------------------------------------------------------------------------------------


            Set @counter = @counter -1
           END

	Fetch Next From FileNames into @DIR
			
	End
		
Close FileNames;
Deallocate FileNames;

    DELETE FROM OUTPUT WHERE Directory is null       
----------------------------------------------
-- DROP temp tables
----------------------------------------------           
If OBJECT_ID('tempdb..#Tempoutput') is not null
DROP TABLE #Tempoutput  

If OBJECT_ID('tempdb..#dirs') is not null
DROP TABLE #dirs

If OBJECT_ID('tempdb..#tempFilePaths') is not null
DROP TABLE #tempFilePaths

If OBJECT_ID('tempdb..#tempFileInformation') is not null
DROP TABLE #tempFileInformation  

Update EF
Set FileCount = (Select COUNT(*) from output where FilePath like EF.FileNameStart + '%')
from #EFiles EF

Update EF
Set FileCount = 0
from #EFiles EF
inner join output O on o.FilePath like EF.FileNameStart + '%'
where 1 = 1
and ef.FileCount = 1 
and o.FilePath not like '%' + (Select right('0' + Cast(MONTH(getdate()) as varchar(3)), 2) + right('0' + Cast(Day(getdate()) as varchar(3)), 2) + Cast(Year(getdate()) as varchar(4))) + '%'

Insert into epacube.import.ss_config_files_in_place
(ImportPath, FileNameStart, [FileName], FileCount)
Select EF.ImportPath, Upper(EF.FileNameStart), O.[FilePath], EF.FileCount
from #EFiles EF
left Join output O on O.FilePath like EF.FileNameStart + '%'
Where (Select COUNT(1) from epacube.import.ss_config_files_in_place where CAST(Insert_Timestamp as DATE) = CAST(GETDATE() as DATE)) = 0

Declare @ServiceBrokerCall Varchar(Max)
Declare @SubSelect varchar(Max)
Declare @subject varchar(128)
Declare @recipients varchar(128)
Declare @Hdr Varchar(128)

If (Select COUNT(*) from #EFiles where filecount = 0) > 0
Begin

	Set @SubSelect = '(Select importpath ''Path'', FileNameStart ''Missing_Files'' from epacube.import.ss_config_files_in_place where FileCount = 0 and CAST(Insert_Timestamp as DATE) = Cast(GETDATE() as date))'

	Set @subject = 'Not all of today''''s import files made it to epaCUBE in time to be imported.'
	
	Set @recipients = 'David.Bernardez@carrierenterprise.com; gstone@epacube.com; support@epacube.com; DLCEepacube@carrierenterprise.com'
	
	Set @Hdr = 'Expected Files Missing - You will hear from I.T. when resolved.'

	Exec [epacube].[EMAIL_DYNAMIC_FORMATTED_CALL]
	@Recipients, @subject, @Hdr, @SubSelect

End
Else
Begin

	Set @SubSelect = '(Select top 1 '''' ''ALL_Expected_Files_In_Place'' from epacube.data_name where 1 = 1)'

	Set @subject = 'Todays epaCUBE Files from SXE are all in place to be imported.'
	
--	Set @recipients = 'gstone@epacube.com; David.Bernardez@carrierenterprise.com; carmen.aguilar@carrierenterprise.com; Carolyn.K.Taylor@carrierenterprise.com; James.E.Jeffries@carrierenterprise.com; jennifer.mize@carrierenterprise.com; kari.largent@carrierenterprise.com'

	Set @recipients = 'David.Bernardez@carrierenterprise.com; gstone@epacube.com; DLCEepacube@carrierenterprise.com'	

	Set @Hdr = 'Todays imports are running on time. The system should be ready for price books by 9:00 AM CST.'

	Exec [epacube].[EMAIL_DYNAMIC_FORMATTED_CALL]
	@Recipients, @subject, @Hdr, @SubSelect

End

Declare @InsertTimestamp as date
Set @InsertTimestamp = Cast((Select MAX(Insert_Timestamp) from epacube.import.ss_config_files_in_place) as DATE)

--Select ImportPath, [FileName] from (
--select *, DENSE_RANK() over(partition by FileNameStart order by filename desc) Rnk from epacube.import.ss_config_files_in_place where cast(Insert_Timestamp as DATE) = cast(Getdate() as DATE) and FileCount > 1 and FileNameStart <> 'ICSW'
--) A where Rnk <> 1
--order by FileName desc


Declare @Pth Varchar(128)
Declare @FN Varchar(128)
Declare @FN2D Varchar(Max)
Declare @FN2D_alt Varchar(Max)
Declare @Move Varchar(5000)
Declare @Delete Varchar(128)
Declare @RowID bigint

DECLARE FileNames2 Cursor local for
	
	Select ImportPath, [FileName], ss_config_files_in_place_id from (
		select *, DENSE_RANK() over(partition by FileNameStart order by filename desc) Rnk from epacube.import.ss_config_files_in_place where cast(Insert_Timestamp as DATE) = cast(getdate() as DATE) and FileCount > 1 and FileNameStart <> 'ICSW'
		) A where Rnk <> 1
		order by FileName desc

OPEN FileNames2
	
	Fetch Next From FileNames2 into @Pth, @FN, @RowID
	WHILE @@FETCH_STATUS = 0
	Begin	

	Set @FN2D = @Pth + Case Right(RTRIM(@Pth), 1) when '\' then '' else '\' end + @FN
	
	Set @FN2D_alt = Left(@Pth, Len(@Pth) - 7) + Case Right(RTRIM(@Pth), 1) when '\' then '' else '\' end + 'Archive\' + @FN

	Set @Move = 'Move ' + @FN2D + ' ' + @FN2D_alt
	
	Exec xp_cmdshell @Move
	
	Update F
	Set File_Deleted = 1
	from epacube.import.ss_config_files_in_place F
	Where ss_config_files_in_place_id = @RowID

	Fetch Next From FileNames2 into @Pth, @FN, @RowID
			
	End
		
Close FileNames2;
Deallocate FileNames2;
	
If OBJECT_ID('Output') is not null
DROP TABLE output
	
END
