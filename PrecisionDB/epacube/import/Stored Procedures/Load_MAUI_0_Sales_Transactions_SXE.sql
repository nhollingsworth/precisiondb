﻿--A_epaMAUI_Load_MAUI_0_Sales_Transactions_SXE

--GHS  August, 2013  Refined Transaction_Type Filters, Transaction Types limited to 'Stock Order' and 'Drop Ship'

-- =============================================
-- Author:		<Gary Stone>
-- Create date: <October 22, 2009>
-- Updated: <June 27, 2013>
-- Description:	<Transform Sales Transaction History and load into import.import_order_data Table>
-- =============================================

CREATE PROCEDURE [import].[Load_MAUI_0_Sales_Transactions_SXE] 
@Job_FK_i Numeric(10, 2)
, @Process_BY_Region as int = Null
, @Process_Future_AND_Current_Assessment as int = Null
 ,@Process_Cost_Changes_Only as int = Null
, @Parent_Entity_Structure_FK Varchar(32) = Null
, @Whse_To_Process as Varchar(Max) = Null
, @Whse_To_Exclude as Varchar(Max) = Null
, @Vendor_IDs as Varchar(max) = Null
, @Date_Range_Start as DateTime = Null
, @Date_Range_End as DateTime = Null
, @StockOrderFilter as Varchar(Max)
, @DropShipFilter as Varchar(Max)
, @SpecialOrdersFilter as Varchar(Max)
, @NonStocksFilter as Varchar(Max)
, @BlanketOrderFilter as Varchar(Max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
------------------------------------
--Transfer Raw Sales Transaction History to staging Import Table
------------------------------------
--Remove any SX Duplicate Records thru Grouping into a temp table
IF object_id('tempdb..##Sales_Transactions') is not null
drop table ##Sales_Transactions;

IF object_id('tempdb..##Prod_Vend_Assoc') is not null
drop table ##Prod_Vend_Assoc;

IF object_id('tempdb..##Cust_Exclusions') is not null
drop table ##Cust_Exclusions;

IF object_id('tempdb..##Drank') is not null
drop table ##Drank

IF object_id('tempdb..##CntrctEx') is not null
drop table ##CntrctEx

IF object_id('tempdb..##whse_reg') is not null
drop table ##whse_reg

IF object_id('tempdb..##whses') is not null
drop table ##whses

IF object_id('tempdb..##ST_LP') is not null
drop table ##ST_LP

--Declare @Job_FK_i Numeric(10, 2)
--Declare @Process_BY_Region as int
--Declare @Process_Future_AND_Current_Assessment as int
--Declare @Process_Cost_Changes_Only as int
--Declare @Parent_Entity_Structure_FK Varchar(32)
--Declare @Whse_To_Process as Varchar(Max)
--Declare @Whse_To_Exclude as Varchar(Max)
--Declare @Vendor_IDs as Varchar(max)
--Declare @Date_Range_Start as DateTime
--Declare @Date_Range_End as DateTime
--Declare @SO as Varchar(32)
--Declare @RT as Varchar(32)
--Declare @PC as Varchar(32)
--Declare @DS as Varchar(32)
--Declare @NonStocksFilter as Varchar(Max)
--Declare @SpecialOrdersFilter as Varchar(Max)
--Declare @StockOrders as int
--Declare @Defaults_To_Use as Varchar(64)
--Declare @NonStocks int
--Declare @DropShips Int
--Declare @SpecialOrders int
--Declare @StockOrderFilter Varchar(Max)
--Declare @DropShipFilter Varchar(Max)

--Set @Job_FK_i = 10708.01
--Set @Defaults_To_Use = 'Primary'
--Set @Process_BY_Region = Null
--Set @Process_Future_AND_Current_Assessment = Null
--Set @Process_Cost_Changes_Only = Null
--Set @Parent_Entity_Structure_FK = Null
--Set @Whse_To_Process = Null
--Set @Whse_To_Exclude = Null
--Set @Vendor_IDs = Null
--Set @Date_Range_Start = '2013-03-01'
--Set @Date_Range_End = '2013-03-31'
--Set @SO = 'Stock Order'
--Set @RT = Null
--Set @PC = Null
--Set @DS = 'Drop Ship'
--If abs(@StockOrders) = 1 or abs(isnull((Select top 1 chkStockOrders from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), -1)) = 1
--	Set @StockOrderFilter = ''
--else
--	Set @StockOrderFilter = ' and ST.transtype <> ''stock order'''
--If Abs(@DropShips) = 1 or abs(isnull((Select Top 1 chkDropShips from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), 0)) = 1
--	Set @DropShipFilter = ''
--else
--	Set @DropShipFilter = ' and ST.transtype <> ''drop ship'''
--If abs(isnull(@NonStocks, 0)) = 1 or abs(isnull((Select Top 1 chkNonStocks from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), 1)) = 1
--	Set @NonStocksFilter = ''
--	else
--	Set @NonStocksFilter = ' And isnull(ST.Special_Stock_Ind, '''') not like ''%n%'''
--If abs(isnull(@SpecialOrders, 0)) = 1 or abs(isnull((Select Top 1 chkSpecialOrders from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), 1)) = 1
--	Set @SpecialOrdersFilter = ''
--	else
--	Set @SpecialOrdersFilter = ' And isnull(ST.Special_Stock_Ind, '''') not like ''%s%'''

Declare @SQLcd as varchar(Max)
Declare @SQLex as varchar(Max)
Declare @Join_Exclusion as varchar(Max)
Declare @Custom_Filter as varchar(Max)
Declare @Fltr_Process_By_Region as varchar(Max)
Declare @Fltr_Region_Name_To_Process as varchar(Max)
Declare @Fltr_Whse_To_Process as varchar(Max)
Declare @Fltr_Whse_To_Exclude as varchar(Max)
Declare @Fltr_Dates as varchar(Max)
Declare @Fltr_Vendor_IDs as varchar(Max)
Declare @Join_Vendors as varchar(Max)
Declare @Join_CstChg as varchar(Max)

Declare @Filter as varchar(Max)
Declare @Join_Region Varchar(Max)
Declare @Join_Whses Varchar(Max)

Exec [import].[Load_MAUI_0_Sales_Transactions_Exclusions] 'GLOBAL ASSESSMENT'

insert into dbo.maui_log Select @Job_FK_i, 'Sales Transaction Exclusions Identified', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null --Cast((Select count(*) from ##Prod_Excl_Global) as varchar(32)) + ' Prods/' + Cast((Select count(*) from ##Cust_Excl_Global) as varchar(32)) + ' Custs Excluded'

insert into dbo.maui_log Select @Job_FK_i, 'Start Sales Transaction Temp Table Load', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null

Create Table ##Sales_Transactions(
	[IMPORT_FILENAME] [varchar](100) NULL,
	[Job_FK] [bigint] NULL,
	[transtype] [nvarchar](64) NULL,
	[orderno] [nvarchar](max) NULL,
	[ordersuf] [nvarchar](max) NULL,
	[lineno] [nvarchar](max) NULL,
	[custno] [nvarchar](64) NULL,
	[custrebamt] [nvarchar](max) NULL,
	[pder rebrecno] [nvarchar](max) NULL,
	[pdsr refer] [nvarchar](max) NULL,
	[pdsr contract] [nvarchar](max) NULL,
	[invoicedt] [datetime] NULL,
	[line discount] [nvarchar](max) NULL,
	[netamt] [nvarchar](max) NULL,
	[otherdisc] [nvarchar](max) NULL,
	[pdrecno] [nvarchar](max) NULL,
	[pdsc refer] [nvarchar](max) NULL,
	[pdsc contract] [nvarchar](max) NULL,
	[prccostmult] [numeric](18, 6) NULL,
	[price] [nvarchar](max) NULL,
	[priceoverfl] [nvarchar](max) NULL,
	[pricetype] [nvarchar](max) NULL,
	[priceunit] [nvarchar](max) NULL,
	[prod] [nvarchar](64) NULL,
	[prodcat] [nvarchar](max) NULL,
	[prodcost] [nvarchar](max) NULL,
	[prodline] [nvarchar](max) NULL,
	[qtyship] [numeric](18, 6) NULL,
	[slsrepin] [nvarchar](64) NULL,
	[slsrepout] [nvarchar](64) NULL,
	[Order_Taker] [nvarchar](64) NULL,
	[shipto] [nvarchar](64) NULL,
	[unit] [nvarchar](max) NULL,
	[unitconv] [nvarchar](max) NULL,
	[user1] [nvarchar](max) NULL,
	[user10] [nvarchar](max) NULL,
	[user11] [nvarchar](max) NULL,
	[user12] [nvarchar](max) NULL,
	[user13] [nvarchar](max) NULL,
	[user14] [nvarchar](max) NULL,
	[user15] [nvarchar](max) NULL,
	[user16] [nvarchar](max) NULL,
	[user17] [nvarchar](max) NULL,
	[user18] [nvarchar](max) NULL,
	[user19] [nvarchar](max) NULL,
	[user2] [nvarchar](max) NULL,
	[user20] [nvarchar](max) NULL,
	[user21] [nvarchar](max) NULL,
	[user22] [nvarchar](max) NULL,
	[user23] [nvarchar](max) NULL,
	[user24] [nvarchar](max) NULL,
	[user3] [nvarchar](max) NULL,
	[user4] [nvarchar](max) NULL,
	[user5] [nvarchar](max) NULL,
	[user6] [nvarchar](max) NULL,
	[user7] [nvarchar](max) NULL,
	[user8] [nvarchar](max) NULL,
	[user9] [nvarchar](max) NULL,
	[vendno] [nvarchar](128) NULL,
	[vendrebamt] [nvarchar](max) NULL,
	[pder rebrecno 2] [nvarchar](max) NULL,
	[pdsr refer 2] [nvarchar](max) NULL,
	[pdsr contract 2] [nvarchar](256) NULL,
	[whse] [nvarchar](64) NULL,
	[custom1] [nvarchar](max) NULL,
	[custom2] [nvarchar](max) NULL,
	[custom3] [nvarchar](max) NULL,
	[custom4] [nvarchar](max) NULL,
	[custom5] [nvarchar](max) NULL,
	[custom6] [nvarchar](max) NULL,
	[custom7] [nvarchar](max) NULL,
	[custom8] [nvarchar](max) NULL,
	[custom9] [nvarchar](max) NULL,
	[custom10] [nvarchar](max) NULL
) ON [PRIMARY]

Create Index idx_st on ##Sales_Transactions(qtyship, transtype, slsrepout, prccostmult, [pdsr contract 2])

--If isnull(@Process_Future_AND_Current_Assessment, 0) = 1 or ISNULL(@Process_Cost_Changes_Only,0) = 1
If ISNULL(@Process_Cost_Changes_Only,0) = 1
	Begin
		Set @Join_CstChg = ' inner join ##CstChg CstChg on ST.Whse = CstChg.Whse and ST.Prod = CstChg.Product_ID'
	End
ELSE
	Begin
		Set @Join_CstChg = ''
	End

If (Select value from epacube.EPACUBE_PARAMS where NAME = 'EPACUBE CUSTOMER')= 'CARRIER'
	BEGIN

	Set @SQLex = 'Select * into ##Cust_Exclusions from (select Ltrim(Rtrim(LEFT(ei.VALUE, CHARINDEX(''-'', ei.VALUE) - 1))) Cust_NO, Ltrim(Rtrim(Right(ei.VALUE, Len(ei.VALUE) - CHARINDEX(''-'', ei.VALUE)))) Ship_To
				, Cast((Select value from epacube.DATA_VALUE dv with (NOLOCK) where dv.DATA_VALUE_ID = ec.data_value_fk and dv.DATA_NAME_FK = ec.DATA_NAME_FK) as Varchar(96)) Cust_Type from epacube.ENTITY_CATEGORY ec 
				inner join epacube.ENTITY_IDENTIFICATION ei with (NOLOCK) on ec.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK and ei.ENTITY_DATA_NAME_FK = 144020 where ec.DATA_NAME_FK = 244907) a 
				where cust_type in (''DIS'', ''INT'', ''RLC'', ''RNA'') 
				
				Create Index idx_CT on ##Cust_Exclusions(cust_no, ship_to, Cust_Type)'
	
	Set @Join_Exclusion = ' Left Join ##Cust_Exclusions EX on Ltrim(Rtrim(Cast(ST.custno as VarChar(64)))) = Cast(EX.CUST_NO as VarChar(64)) and ISNULL(Ltrim(Rtrim(Cast(ST.shipto as VarChar(64)))), '''') = isnull(Cast(EX.ship_to as VarChar(64)), '''')'

		select distinct LEFT([contract_no], charindex('~', [contract_no])-1) Contract_NO into ##CntrctEx from synchronizer.rules with (NOLOCK) where [contract_no] like '%~%'
		Create Index ctex on ##CntrctEx(Contract_NO)

	Set @Custom_Filter = ' and isnull(Cast(ST.[pdsr contract 2] as varchar(32)), '''') not in (select Contract_NO from ##CntrctEx)
					and Cast(ST.slsrepout as varchar(32)) not in (''9h99'', ''8H99'', ''8H98'', ''7H98'', ''7H99'', ''2H99'', ''4H99'', ''5H99'', ''3H99'')'
	
	End
ELSE
	BEGIN

	Set @SQLex = 'Select Null cust_no, Null ship_to, Null CUST_TYPE into ##Cust_Exclusions'
	
	Set @Join_Exclusion = ' Left Join ##Cust_Exclusions EX on Ltrim(Rtrim(Cast(ST.custno as VarChar(64)))) = Cast(EX.CUST_NO as VarChar(64)) and ISNULL(Ltrim(Rtrim(Cast(ST.shipto as VarChar(64)))), '''') = isnull(Cast(EX.ship_to as VarChar(64)), '''')'
	Set @Custom_Filter = ''
	END

	Exec (@SQLex)
		
If isnull(@Process_BY_Region, 0) = 1 OR @Parent_Entity_Structure_FK IS NOT NULL
	Begin
	
	Select Cast(value as varchar(32)) whse into ##whse_reg from epacube.entity_identification with (NOLOCK) where entity_structure_fk in (select entity_structure_id from epacube.entity_structure with (NOLOCK) where parent_entity_structure_fk = @Parent_Entity_Structure_FK)
	Create index idx_whs on ##whse_reg(whse)
	
	Set @Join_Region = 'inner join ##whse_reg whs on ST.whse = whs.whse'
--	Set @Fltr_Process_By_Region = ' and ST.whse in (Select value from epacube.entity_identification where entity_structure_fk in (select entity_structure_id from epacube.entity_structure where parent_entity_structure_fk = ' + @Parent_Entity_Structure_FK + '))'
	insert into dbo.maui_log Select @Job_FK_i, 'Creating Region Filter', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i)), 108)), Null
	End
	else 
	Set @Join_Region = ''
--	Set @Fltr_Process_By_Region = ''

If @Date_Range_Start is not null and @Date_Range_End is not null
	Set @Fltr_Dates = ' and invoicedt between ''' + Convert(Varchar(10), @Date_Range_Start, 121) + '''' + ' and ' + '''' + Convert(Varchar(10), @Date_Range_End, 121) + ''''
Else if @Date_Range_Start is not null
	Set @Fltr_Dates = ' and invoicedt >= ''' + Convert(Varchar(10), @Date_Range_Start, 121) + ''''
Else if @Date_Range_End is not null
	Set @Fltr_Dates = ' and invoicedt <= ''' + Convert(Varchar(10), @Date_Range_End, 121) + ''''
Else
	Set @Fltr_Dates = ''

If isnull(@Whse_To_Process, '') > ''
	Begin
	Declare @SQL_whse varchar(max)
	Set @SQL_whse = 'Select Cast(value as varchar(32)) whse into ##whses from epacube.entity_identification with (NOLOCK) where value in (' + @Whse_To_Process + ')'
	Exec @SQL_whse
	Create index idx_whs on ##whses(whse)

	Set @Join_Whses = 'inner join ##whse whses on ST.whse = whses.whse'
--	Set @Fltr_Whse_To_Process = ' and ST.whse in (' + @Whse_To_Process + ')'
	insert into dbo.maui_log Select @Job_FK_i, 'Creating Warehouse(s) Filter ' + @Whse_To_Process, GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i)), 108)), Null
	end

	else
	Set @Join_Whses = ''
--	Set @Fltr_Whse_To_Process = ''

If isnull(@Whse_To_Exclude, '') > ''
	Begin
	Set @Fltr_Whse_To_Exclude = ' and ST.whse not in (' + @Whse_To_Exclude + ')'
	insert into dbo.maui_log Select @Job_FK_i, 'Creating Warehouse(s) Exclusion Filter ' + @Whse_To_Exclude, GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i)), 108)), Null
	end

	else
	Set @Fltr_Whse_To_Exclude = ''
		
If isnull(@Vendor_IDs, '') > ''
	Begin
	insert into dbo.maui_log Select @Job_FK_i, 'Creating Vendor(s) Filter ' + @Vendor_IDs, GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i)), 108)), Null

	Set @Fltr_Vendor_IDs = ' and PVA.Vendor_ID in (' + @Vendor_IDs + ')'
			
		Select PI.Value Product_ID, ei.value Vendor_ID	
		into ##Prod_Vend_Assoc
		from epacube.PRODUCT_IDENTIFICATION PI with (nolock) 
		inner join epacube.PRODUCT_ASSOCIATION PA with (nolock) on PI.PRODUCT_STRUCTURE_FK = PA.PRODUCT_STRUCTURE_FK and PA.DATA_NAME_FK = 253501
		inner join epacube.ENTITY_IDENTIFICATION EI with (nolock) on PA.ENTITY_STRUCTURE_FK = EI.ENTITY_STRUCTURE_FK and entity_data_name_fk = 143000
		group by PI.Value, ei.value

		Create Index idx_pva on ##Prod_Vend_Assoc(Product_ID, Vendor_ID)
		Set @Join_Vendors = 'inner join ##Prod_Vend_Assoc PVA on ST.prod = PVA.Product_ID and ST.Vendno = PVA.Vendor_ID'	
	end
	else
	Begin
		Set @Fltr_Vendor_IDs = ''
		Set @Join_Vendors = ''
	End
		
	Set @Filter = @Fltr_Whse_To_Exclude + @Fltr_Vendor_IDs + @Custom_Filter + @Fltr_Dates + @NonStocksFilter + @SpecialOrdersFilter + @DropShipFilter + @StockOrderFilter + @BlanketOrderFilter

	Select Cast(Orderno as varchar(64)) Orderno into ##ST_LP from import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE ST with (nolock) where prod like '%lot%price%' group by Orderno
	create index idx_ordno on ##ST_LP(orderno)
	
	Set @SQLcd = '
		Insert into ##Sales_Transactions([IMPORT_FILENAME], [Job_FK], [transtype], [orderno], [ordersuf], [lineno], [custno], [custrebamt], [pder rebrecno], [pdsr refer], [pdsr contract], [invoicedt], [line discount], [netamt], [otherdisc], [pdrecno], [pdsc refer], [pdsc contract], [prccostmult], [price], [priceoverfl], [pricetype], [priceunit], [prod], [prodcat], [prodcost], [prodline], [qtyship], [slsrepin], [slsrepout], [shipto], [unit], [unitconv], [user1], [user10], [user11], [user12], [user13], [user14], [user15], [user16], [user17], [user18], [user19], [user2], [user20], [user21], [user22], [user23], [user24], [user3], [user4], [user5], [user6], [user7], [user8], [user9], [vendno], [vendrebamt], [pder rebrecno 2], [pdsr refer 2], [pdsr contract 2], [whse], [custom1], [custom2], [custom3], [custom4], [custom5], [custom6], [custom7], [custom8], [custom9], [custom10], Order_Taker)
		Select Distinct
		ST.[IMPORT_FILENAME], ST.[Job_FK], ST.[transtype], ST.[orderno], ST.[ordersuf], ST.[lineno], ST.[custno], ST.[custrebamt], ST.[pder rebrecno], ST.[pdsr refer], ST.[pdsr contract], ST.[invoicedt], ST.[line discount], ST.[netamt], ST.[otherdisc], ST.[pdrecno], ST.[pdsc refer], ST.[pdsc contract], ST.[prccostmult], ST.[price], ST.[priceoverfl], ST.[pricetype], ST.[priceunit], ST.[prod], ST.[prodcat], ST.[prodcost], ST.[prodline], ST.[qtyship], ST.[salesrep_in], ST.[slsrepout], ST.[shipto], ST.[unit], ST.[unitconv], ST.[user1], ST.[user10], ST.[user11], ST.[user12], ST.[user13], ST.[user14], ST.[user15], ST.[user16], ST.[user17], ST.[user18], ST.[user19], ST.[user2], ST.[user20], ST.[user21], ST.[user22], ST.[user23], ST.[user24], ST.[user3], ST.[user4], ST.[user5], ST.[user6], ST.[user7], ST.[user8], ST.[user9], ST.[vendno], ST.[vendrebamt], ST.[pder rebrecno 2], ST.[pdsr refer 2], ST.[pdsr contract 2], ST.[whse], ST.[custom1], ST.[custom2], ST.[custom3], ST.[custom4], ST.[custom5], ST.[custom6], ST.[custom7], ST.[custom8], ST.[custom9], ST.[custom10], ST.Order_Taker
		from import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE ST with (nolock)
		left join ##ST_LP ST_LP on ST.orderno = ST_LP.orderno
			' + @Join_CstChg + '
			' + @Join_Vendors + ' 
			' + @Join_Exclusion + '
			' + @Join_Region + '
			' + @Join_Whses + '
		where 1=1
		and EX.Cust_Type is null
		and isnumeric(ST.[vendno]) = 1
		and ST_LP.orderno is null
		and isnumeric(isnull(prodcost, 0)) = 1 and isnumeric(isnull(vendrebamt, 0)) = 1
		and Cast(isnull(Prodcost, 0) as money) >= Cast(isnull(vendrebamt, 0) as money)
		and ST.transtype not in (''Price Correction'', ''Return'')
		and ST.transtype not like ''%kit%''
		' + @Filter

	Exec (@SQLcd)

	IF object_id('tempdb..##Prod_Vend_Assoc') is not null
	drop table ##Prod_Vend_Assoc
	IF object_id('tempdb..##Cust_Exclusions') is not null
	drop table ##Cust_Exclusions;

Truncate Table import.import_order_data

insert into dbo.maui_log Select @Job_FK_i, 'Transactions being Anayzed', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i)), 108)), (Select COUNT(*) from ##Sales_Transactions)

Insert into import.import_order_data
(
IMPORT_FILENAME, TRANSACTION_TYPE, Seq_ID, JOB_FK, ORDER_DATA_SET, ORDER_TYPE_CR_FK, PRODUCT_DATA_NAME, PRODUCT_ID_VALUE
, ORG_DATA_NAME, ORG_ID_VALUE, CUST_DATA_NAME, CUST_ID_VALUE, SUPL_DATA_NAME, SUPL_ID_VALUE
, SALES_DATA_NAME_IN, SALES_ID_VALUE_IN, SALES_DATA_NAME_OUT, SALES_ID_VALUE_OUT
, ORDER_NO, ORDER_LINE, INVOICE_DATE, PRICE_RULE_XREF, PRICE_REFERENCE, PRICE_CONTRACT_NO
, SELL_UOM_CODE_FK, UOM_SELL_CODE, PRICE_OVERRIDE_IND
, Product_Category, Product_Line, Product_Price_Type, UOM_Sell_Price_Conv, UOM_Sell_Pack_Conv
, SALES_DOLLARS_Ext, REBATE_AMT_SPA_Ext, SALES_QTY, PRICE_Unit, REPL_COST_Unit, REBATE_AMT_SPA_Unit, COGS_Unit
, MARGIN_DOLLARS_Ext, MARGIN_DOLLARS_Unit, MARGIN_PERCENT, REBATE_SPA_RULE_XREF, REBATE_SPA_REFERENCE
, REBATE_SPA_CONTRACT_NO, Order_Taker
)

Select
import_filename
, TransType
, 1 Seq_ID
, Job_FK Import_Job_FK
, Null Order_Data_Set
, Null Order_Type_CR_FK
, 'Product ID' Product_Data_Name
, Prod Product_ID_Value
, 'Warehouse Code' Org_Data_Name
, Whse Org_ID_Value
, Case When Shipto is null then 'Customer Bill To'
	else 'Customer Ship To' end Cust_Data_Name
, Case When Shipto is null then custno
	else custno + '-' + shipto end CUST_ID_VALUE
, 'Arp Vendor' Supl_Data_Name
, vendno
, Case When isnull([slsrepin], '') > '' then 'SALESREP IN' end Sales_Data_Name_IN
, Case When isnull([slsrepin], '') > '' then [slsrepin] end Sales_ID_Value_IN
, Case When isnull(slsrepout, '') > '' then 'SALESREP Out' end Sales_Data_Name_OUT
, Case When isnull(slsrepout, '') > '' then slsrepout end Sales_ID_Value_OUT
, orderno Order_No
, Cast(ordersuf as varchar) + ' - ' + Cast([lineno] as varchar) Order_Line
, invoicedt Invoice_Date
, pdrecno Price_Rule_Xref
, left([pdsc refer], 24) Price_Reference 
, left([pdsc contract], 24) Price_Contract_No
, (Select UOM_Code_ID from epacube.UOM_Code with (NOLOCK) where UOM_Code = Sales.Unit) Sell_UOM_Code_FK
, Unit UOM_SELL_CODE
, Case When Cast(QtyShip as Numeric(18, 6)) < 0 then 'False' else
	Case When (Select top 1 Value from epacube.epacube_params with (NOLOCK) where Name = 'EPACUBE CUSTOMER') like '%Bradco%' then
	Case When (isnull(pdrecno,0) = 0 or isnull(pdrecno, '') = '') and isnull(rtrim(substring(user5, 2, 8)), '') > '' then 'True' else 'False' end
	Else
	Case priceoverfl 
		When 'Y' then 'True' else 'False' end end end Price_Override
, prodcat product_category
, prodline product_line
, pricetype product_price_type
, UOM_PRICE_Conv UOM_Sell_Price_Conv
, UOM_Pack_Conv UOM_Sell_Pack_Conv
, Sales_Dollars_Ext
, cast(Rebate_Unit * qtyship as numeric(18, 4)) REBATE_AMT_SPA_Ext
, Cast(Sales_Qty_TTL * Cast([unitconv] as Numeric(18, 6)) as Numeric(18, 6)) Sales_Qty
, Price_Unit
, Cost_Unit Repl_Cost_Unit
, isnull(Rebate_Unit, 0) REBATE_AMT_SPA_Unit
, isnull(Cost_Unit, 0) - isnull(Rebate_Unit, 0) COGS_Unit
, (isnull(Price_Unit, 0) - isnull(Cost_Unit, 0) + isnull(Rebate_Unit,0)) * isnull(Sales_Qty_TTL, 0) Margin_Dollars_Ext
, isnull(Price_Unit, 0) - isnull(Cost_Unit, 0) + isnull(Rebate_Unit,0) Margin_Dollars_Unit
, Case When isnull(Price_Unit, 0) > 0 then 
	(isnull(Price_Unit, 0) - isnull(Cost_Unit, 0) + isnull(Rebate_Unit,0)) 
	/ Price_Unit else 0 end Margin_Percent
, [pder rebrecno 2] Rebate_Spa_Rule_Xref
, Left([pdsr refer 2], 64) Rebate_SPA_Reference
, [pdsr contract 2] Rebate_SPA_Contract_No
, Order_Taker
from
(Select
Cast(netamt as Numeric(18, 4)) Sales_Dollars_Ext
, Cast(qtyship as numeric(18, 4)) Sales_Qty_Ttl 
--, Cast(Cast(Cast(netamt as numeric(18, 4)) / Cast(qtyship as numeric(18, 4)) as numeric(18, 4)) * Case when PriceUnit is not null then Cast([prccostmult] as Numeric(18, 6)) else 1 end  as Numeric(18, 4)) Price_Unit
, Cast(Cast(Cast(netamt as numeric(18, 4)) / Cast(qtyship as numeric(18, 4)) as numeric(18, 4)) as Numeric(18, 4)) Price_Unit
, Cast(Cast(ProdCost as numeric(18, 4)) * Case when PriceUnit is not null then Cast([prccostmult] as Numeric(18, 6)) else 1 end as Numeric(18, 4)) Cost_Unit 
, Cast(prccostmult as numeric(18, 4)) UOM_PRICE_Conv
, Cast(unitconv as numeric(18, 4)) UOM_Pack_Conv
, Cast(Case When isnumeric(vendrebamt) = 1 then Cast(isnull(vendrebamt, 0) as Numeric(18, 4)) * Cast(prccostmult as numeric(18, 4)) else 0 end as Numeric(18, 4)) Rebate_Unit
, * from

##Sales_Transactions
where ISNULL(qtyship, '0') <> '0') Sales
where Cast(qtyship as numeric(18, 4)) <> 0 and Cast([prccostmult] as Numeric(18, 6)) <> 0

---
---Transfer Sales History Data from Import Stage Table into Production
---

--------------------------------------

Truncate Table marginmgr.ORDER_DATA_HISTORY

insert into dbo.maui_log Select @Job_FK_i, 'Load MM Order Data History', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i)), 108)), Null

Insert into marginmgr.ORDER_DATA_HISTORY
(
DRANK
, TRANSACTION_TYPE
, ORDER_DATE
, ORDER_DATA_SET
, ORDER_TYPE_CR_FK
, PRODUCT_STRUCTURE_FK
, ORG_ENTITY_STRUCTURE_FK
, SUPL_ENTITY_STRUCTURE_FK
, CUST_ENTITY_STRUCTURE_FK
, SALES_ENTITY_STRUCTURE_FK
, TERR_ENTITY_STRUCTURE_FK
, BUYER_ENTITY_STRUCTURE_FK
, MARGIN_DOLLARS_EXT
, MARGIN_DOLLARS_UNIT
, MARGIN_PERCENT
, SALES_DOLLARS_EXT
, SALES_QTY
, PRICE_UNIT
, COGS_UNIT
, REPL_COST_UNIT
, MARKET_COST
, REBATE_AMT_SPA_Ext
, REBATE_AMT_SPA_Unit
, REBATE_AMT_BUY
, REBATE_AMT_SELL_SIDE
, CURRENCY_CR_FK
, ORDER_NO
, ORDER_LINE
, ORDER_QTY
, SELL_UOM_CODE_FK
, BUY_UOM_CODE_FK
, PRICE_OVERRIDE_IND
, LINE_DISCOUNT
, OTHER_DISCOUNT
, SHIP_COST_ESTIMATE
, SHIP_COST_INVOICED
, SHIP_WEIGHT
, SHIP_CUBE
, SHIP_DATE
, INVOICE_DATE
, PRICE_CALC_RULE_TYPE_CR_FK
, PRICE_RULE_XREF
, PRICE_REFERENCE
, PRICE_CONTRACT_NO
, REBATE_SPA_RULE_XREF
, REBATE_SPA_REFERENCE
, REBATE_SPA_CONTRACT_NO
, REBATE_BUY_RULE_XREF
, REBATE_BUY_REFERENCE
, REBATE_BUY_CONTRACT_NO
, REBATE_SELL_SIDE_RULE_XREF
, REBATE_SELL_SIDE_REFERENCE
, REBATE_SELL_SIDE_CONTRACT_NO
, PRODUCT_ID_HOST
, ORG_ID_HOST
, CUSTOMER_ID_HOST
, VENDOR_ID_HOST
, SALESPERSON_ID_OUTSIDE_HOST
, SALESPERSON_ID_INSIDE_HOST
, BUYER_HOST
, Product_Category
, Product_Line
, Product_Price_Type
, UOM_Sell_Price_Conv
, UOM_Sell_Pack_Conv
, UOM_BUY_CODE
, UOM_SELL_CODE
, IMPORT_JOB_FK
, IMPORT_FILENAME
, RECORD_STATUS_CR_FK
, UPDATE_USER
, QTY_MODE
, QTY_MEDIAN
, QTY_MEAN
, ORDER_TAKEN_BY
)

Select A.* from (
Select
DENSE_RANK() over (Partition By iod.product_id_value
, iod.org_id_value
, iod.Cust_id_value
Order by Case Transaction_Type When 'Stock Order' then 1 When 'Drop Ship' then 2 When 'Return' then 3 else 4 end
,INVOICE_DATE Desc, Sales_Dollars_Ext Desc, Sales_Qty Desc, iod.order_no Desc, iod.Order_Line
) 
DENSE_RANK
,Transaction_Type
,Invoice_Date ORDER_DATE, Null ORDER_DATA_SET, Null ORDER_TYPE_CR_FK
,(Select product_structure_fk from epacube.product_identification with (NOLOCK) where data_name_fk = 110100 and value = iod.product_id_value) product_structure_fk
,(select entity_structure_fk from epacube.entity_identification ei with (NOLOCK) where ei.data_name_fk = 141111 and entity_data_name_fk = 141000 and ei.value = iod.org_id_value) Org_Entity_Structure_fk
,(select entity_structure_fk from epacube.entity_identification with (NOLOCK) where data_name_fk = 143110 and value = iod.Supl_id_value) Supl_Entity_Structure_fk
, Case Cust_Data_Name
	When 'Customer Bill To' then (select entity_structure_fk from epacube.entity_identification with (NOLOCK) where data_name_fk = 144111 and entity_data_name_fk = 144010 and value = iod.Cust_id_value)
	else (select entity_structure_fk from epacube.entity_identification with (NOLOCK) where data_name_fk = 144111 and entity_data_name_fk = 144020 and value = iod.Cust_id_value) end Cust_Entity_Structure_fk
,(select entity_structure_fk from epacube.entity_identification with (NOLOCK) where data_name_fk = 145020 and value = iod.Sales_id_value_out) Sales_Entity_Structure_fk
,Null Terr_Entity_Structure_fk
,(select entity_structure_fk from epacube.entity_identification with (NOLOCK) where data_name_fk = 147111 and value = iod.Buy_id_value) Buyer_Entity_Structure_fk

, MARGIN_DOLLARS_EXT, MARGIN_DOLLARS_UNIT, MARGIN_PERCENT, SALES_DOLLARS_EXT, SALES_QTY, PRICE_UNIT, COGS_UNIT, REPL_COST_UNIT, MARKET_COST, REBATE_AMT_SPA_Ext, REBATE_AMT_SPA_Unit, REBATE_AMT_BUY, REBATE_AMT_SELL_SIDE, CURRENCY_CR_FK, ORDER_NO, ORDER_LINE, ORDER_QTY, SELL_UOM_CODE_FK, BUY_UOM_CODE_FK
, PRICE_OVERRIDE_IND, LINE_DISCOUNT, OTHER_DISCOUNT, SHIP_COST_ESTIMATE, SHIP_COST_INVOICED, SHIP_WEIGHT, SHIP_CUBE, SHIP_DATE, INVOICE_DATE, PRICE_CALC_RULE_TYPE_CR_FK, PRICE_RULE_XREF, PRICE_REFERENCE, PRICE_CONTRACT_NO, REBATE_SPA_RULE_XREF, REBATE_SPA_REFERENCE, REBATE_SPA_CONTRACT_NO, REBATE_BUY_RULE_XREF, REBATE_BUY_REFERENCE, REBATE_BUY_CONTRACT_NO, REBATE_SELL_SIDE_RULE_XREF, REBATE_SELL_SIDE_REFERENCE, REBATE_SELL_SIDE_CONTRACT_NO, PRODUCT_ID_Value, ORG_ID_Value, CUST_ID_VALUE, SUPL_ID_VALUE, SALES_ID_VALUE_OUT, SALES_ID_VALUE_IN, BUY_ID_VALUE, Product_Category, Product_Line, Product_Price_Type, UOM_Sell_Price_Conv, UOM_Sell_Pack_Conv, UOM_BUY_CODE, UOM_SELL_CODE, JOB_FK, IMPORT_FILENAME, 1 RECORD_STATUS_CR_FK, UPDATE_USER
,(Select top 1 Sales_qty from (Select sales_qty, count(*) recs from import.IMPORT_ORDER_DATA with (NOLOCK) where product_id_value = iod.product_id_value and Cust_id_value = iod.Cust_id_value 
	and Org_ID_Value = iod.Org_ID_Value group by sales_qty) A Order by Recs Desc, sales_qty Desc ) Qty_Mode
,(Select top 1 sales_qty from (Select top 50 Percent sales_qty from import.IMPORT_ORDER_DATA with (NOLOCK) where product_id_value = iod.product_id_value and Cust_id_value = iod.Cust_id_value and Org_ID_Value = iod.Org_ID_Value order by sales_qty) A Order by Sales_Qty Desc) Qty_Median
,(Select Avg(sales_qty) from import.IMPORT_ORDER_DATA with (NOLOCK) where product_id_value = iod.product_id_value and Cust_id_value = iod.Cust_id_value and Org_ID_Value = iod.Org_ID_Value) Qty_Mean
, Order_Taker
from import.IMPORT_ORDER_DATA iod with (NOLOCK)
) A 
left join epacube.product_association pa 
	on A.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK  
	and A.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK
	and pa.data_name_fk = 159100
where 1 = 1
and A.product_structure_fk not in (select product_structure_fk from ##Prod_Excl_Global)
and A.cust_entity_structure_fk not in (select entity_structure_fk from ##Cust_Excl_Global)
and pa.PRODUCT_ASSOCIATION_ID is not null

delete from marginmgr.order_data_history where product_structure_fk is null or cust_entity_structure_fk is null or org_entity_structure_fk is null

Select
DENSE_RANK() over (Partition By 
Product_Structure_FK, Org_Entity_Structure_FK, Cust_Entity_Structure_FK
Order by Case Transaction_Type When 'Stock Order' then 1 When 'Drop Ship' then 2 When 'Return' then 3 else 4 end
, Invoice_Date Desc, order_no, order_line) Drank
, Product_Structure_FK, Org_Entity_Structure_FK, Cust_Entity_Structure_FK, Invoice_Date, order_no, order_line
into ##drank
from marginmgr.order_data_history with (NOLOCK)

Create Index idx_dr on ##drank(Product_Structure_FK, Org_Entity_Structure_FK, Cust_Entity_Structure_FK, Invoice_Date, order_no, order_line)

Update odh
Set Drank = dr.Drank
from marginmgr.ORDER_DATA_HISTORY odh
inner join ##drank dr on dr.Product_Structure_FK = odh.Product_Structure_FK and dr.Org_Entity_Structure_FK = odh.Org_Entity_Structure_FK and dr.Cust_Entity_Structure_FK
= odh.Cust_Entity_Structure_FK and dr.Invoice_Date = odh.Invoice_Date and dr.order_no = odh.order_no
and dr.order_line = odh.order_line

insert into dbo.maui_log Select @Job_FK_i, 'ODH Records with Drank of 1', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i)), 108)), (Select COUNT(*) from marginmgr.ORDER_DATA_HISTORY with (NOLOCK) where DRANK = 1)
insert into dbo.maui_log Select @Job_FK_i, 'ODH Line Items with Duplicates', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log with (NOLOCK) where Job_fk_i = @Job_FK_i)), 108)), (
	Select COUNT(*) from (
	Select product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk, COUNT(*) records from marginmgr.ORDER_DATA_HISTORY with (NOLOCK)
	where DRANK = 1
	group by product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk having COUNT(*) > 1) A )

drop table ##Sales_Transactions
drop table ##drank

delete from marginmgr.ORDER_DATA_HISTORY where ORDER_DATA_ID in
(
Select Order_data_id from (
Select
Product_structure_fk
, org_entity_structure_fk
, cust_entity_structure_fk
, supl_entity_structure_fk
, Drank
, MIN(ORDER_DATA_ID) Order_Data_ID
, COUNT(*) recs
from marginmgr.ORDER_DATA_HISTORY with (nolock)
--where DRANK = 1
group by 
Product_structure_fk
, org_entity_structure_fk
, cust_entity_structure_fk
, supl_entity_structure_fk
, Drank
having COUNT(*) > 1) A
)

IF object_id('tempdb..##CntrctEx') is not null
drop table ##CntrctEx

IF object_id('tempdb..##whse_reg') is not null
drop table ##whse_reg

IF object_id('tempdb..##whses') is not null
drop table ##whses

IF object_id('tempdb..##excl_ProdsCrit') is not null
drop table ##excl_ProdsCrit

IF object_id('tempdb..##Prod_Excl_Global') is not null
drop table ##Prod_Excl_Global

IF object_id('tempdb..##excl_CustsCrit') is not null
drop table ##excl_CustsCrit

IF object_id('tempdb..##Cust_Excl_Global') is not null
drop table ##Cust_Excl_Global

IF object_id('tempdb..##ST_LP') is not null
drop table ##ST_LP

END
