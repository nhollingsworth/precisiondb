﻿





-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        03/08/2018   Initial Version 
-- CV        07/22/2019   load upc_sys


CREATE PROCEDURE [import].[LOAD_GTIN_TO_EPA] ( @JOB_FK bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.LOAD_GTIN_TO_EPA', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @JOB_FK;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


-------------------------------------------------------------------------------------
truncate table epacube.product_gtin

--select * from [import].import_gtin

----truncate table [import].import_gtin

--select  top 1 * from epacube.product_gtin

--select  len(upc_code), UPC_CODE , reverse(substring(reverse(upc_code),1, 5))  from import.IMPORT_GTIN  

--select top 10 pi.PRODUCT_STRUCTURE_FK, g.* from [import].[IMPORT_GTIN] G 
--inner join epacube.product_identification pi with (nolock) on pi.value = g.item_surr
--and pi.data_name_fk = 110103

-- select [epacube].[URM_DATE_CONVERSION](DSCDATE)
--,  [epacube].[URM_DATE_CONVERSION](DTE_CRT)
-- ,  [epacube].[URM_DATE_TIME_CONVERSION](DTE_CRT,TME_CRT)
--,DTE_UPD , [epacube].[URM_DATE_CONVERSION](DTE_UPD)
--,TME_UPD , [epacube].[URM_DATE_TIME_CONVERSION](DTE_UPD, TME_UPD)
-- from [import].[IMPORT_GTIN] g
--inner join epacube.product_identification pi on pi.value = g.item_surr
--and pi.data_name_fk = 110103 


UPDATE g
set g.product_structure_fk = pi.PRODUCT_STRUCTURE_FK
 from [import].[IMPORT_GTIN] g
inner join epacube.product_identification pi on pi.value = g.item_surr
and pi.data_name_fk = 110103
and g.job_fk = @JOB_FK

--remove duplicate
update import.import_gtin
set do_not_import = 1 
where IMPORT_GTIN_ID in (
  select a.import_gtin_id
  from (
      select IMPORT_GTIN_ID,
      DENSE_RANK() over (Partition By  [ITEM_SURR]
      ,[GPCPCKCDE]
      ,[GTITMNBR]
      ,[PRIOR_KEY]
      ,[DSCDATE]
      ,[DISC_RSN]
      ,[UPC_CODE]
      ,[UPCCHKDIG]
      ,[GPC_MFG]
      ,[DTE_CRT]
      --,[TME_CRT]
      --,[USR_CRT]
      --  ,[USR_UPD]
      ,[PRODUCT_STRUCTURE_FK] 
      Order by IMPORT_GTIN_ID DESC ) as drank
      from import.import_gtin i with (nolock) where job_fk = @JOB_FK
  ) a
  where a.drank > 1
)



------------------------------------------------------------------------------------------------

INSERT INTO [epacube].[PRODUCT_GTIN]
           ([PRODUCT_STRUCTURE_FK]
           --,[PRODUCT_UOM_FK]
           ,[GLOBAL_PACK_CODE]
           ,[DATA_NAME_FK]
           ,[UPC_CODE]
           ,[UPC_SYS]
           ,[UPC_MANUF]
           ,[UPC_ITEM]
           ,[UPC_CHK_DIGIT]
           ,[GLOBAL_ITEM_NUMBER]
           ,[PRECEDENCE]
           ,[DISCONTINUE_DATE]
           ,[DISCONTINUE_REASON_DV_FK]
           ,[EFFECTIVE_DATE]
--           ,[END_DATE]
--           ,[ORG_ENTITY_STRUCTURE_FK]
--           ,[ENTITY_STRUCTURE_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
           ,[CREATE_USER]
		   )
		   select  --distinct
		   G.PRODUCT_STRUCTURE_FK
		   ,g.GPCPCKCDE 
		   ,500030
		   ,g.upc_code
		    ,Case when len(UPC_CODE) <= 10 then null else left(UPC_CODE, len(UPC_CODE) - 10) end upc_sys
		   ,g.gpc_mfg
		  ,reverse(substring(reverse(upc_code),1, 5))  --UPC_ITEM
		   ,g.UPCCHKDIG
		    ,g.GTITMNBR
			--,g.PRIOR_KEY
			, dense_rank()over(partition by item_surr, gpcpckcde order by prior_key, DTE_CRT desc, DTE_UPD desc, TME_UPD desc) + case when isnull(dscdate, '0') <> '0' then 98 else 0 end 'Precedence'
			,[epacube].[URM_DATE_CONVERSION](DSCDATE)
		,g.DISC_RSN
		,[epacube].[URM_DATE_CONVERSION](DTE_CRT)  ---Effective Date
		,case when g.dscdate = '0' then 1 else 2 end 'Record_Status_CR_FK'
		,[epacube].[URM_DATE_TIME_CONVERSION](DTE_CRT,TME_CRT)
		,[epacube].[URM_DATE_TIME_CONVERSION](DTE_UPD, TME_UPD)
		,g.USR_UPD
		,g.USR_CRT 
		from [import].[IMPORT_GTIN] G
		--inner join epacube.product_identification pi on pi.value = g.item_surr
		--and pi.data_name_fk = 110103
		--and pi.PRODUCT_STRUCTURE_FK is not NULL
		where 1 = 1
		and g.PRODUCT_STRUCTURE_FK is not null
		and g.do_not_import is NULL
		and g.job_fk = @JOB_FK

--------------------------------------------------------------------------------------------------

      SELECT @v_input_rows = COUNT(1)
        FROM [import].import_gtin WITH (NOLOCK)
       WHERE job_fk = @JOB_FK;

      SELECT @v_output_rows = COUNT(1)
        FROM [import].import_gtin WITH (NOLOCK)
       WHERE job_fk = @JOB_FK
		AND PRODUCT_STRUCTURE_FK is NOT NULL ;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @JOB_FK, 'IMPORT_COMPLETE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



 SET @status_desc =  'finished execution of IMPORT.LOAD_GTIN_TO_EPA'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @JOB_FK;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT.LOAD_GTIN_TO_EPA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @JOB_FK;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END





