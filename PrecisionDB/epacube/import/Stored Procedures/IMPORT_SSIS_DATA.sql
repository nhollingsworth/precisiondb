﻿
-- Copyright 2006
--
-- Procedure created by Walt Tucholski
--
--
-- Purpose: Execute SSIS Import packages
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- WT        08/25/2006   Initial SQL Version
-- WT		 02/21/2007	  Added common Execlog status reporting
-- YN        10/23/2008   Changed the stored procedure to be executed in a Service Broker queue

CREATE PROCEDURE [import].[IMPORT_SSIS_DATA] 
(
	@in_ssis_config_params_id INT
)
AS

BEGIN

    DECLARE @rt INT
    DECLARE @cmd VARCHAR(MAX)
    DECLARE @package_path VARCHAR(256)
    DECLARE @package_name VARCHAR(256)
	DECLARE @env_reference VARCHAR(50)
    DECLARE @import_file_path VARCHAR(256)
    DECLARE @server_name VARCHAR(256)
	DECLARE @package_type VARCHAR(256)
	DECLARE @cmd_textFromBroker VARCHAR(20)
    DECLARE @ls_stmt VARCHAR(1000)
    DECLARE @l_exec_no BIGINT
    DECLARE @status_desc VARCHAR(max)
    DECLARE @ErrorMessage NVARCHAR(4000)
    DECLARE @ErrorSeverity INT
    DECLARE @ErrorState INT
    DECLARE @job_name VARCHAR(250)
    DECLARE @epa_job_id BIGINT
	DECLARE @job_class_id  INT

    BEGIN TRY

		SET NOCOUNT ON

		SET @l_exec_no = 0 ;
		SET @ls_stmt = 'started execution of import.IMPORT_SSIS_DATA. ' + ' @in_ssis_config_params_id = ' + cast(isnull(@in_ssis_config_params_id, 0) AS VARCHAR(16))

		EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, @exec_id = @l_exec_no OUTPUT;

		SELECT @package_path = PACKAGE_PATH
			, @package_name = PACKAGE_NAME
			, @env_reference = ENV_REFERENCE
			, @import_file_path = IMPORT_FILE_PATH
			, @server_name = SERVER_NAME
			, @package_type = PACKAGE_TYPE
			, @job_class_id = job_class_cr_Fk
		FROM import.SSIS_CONFIG_PARAMS
		WHERE SSIS_CONFIG_PARAMS_ID = @in_ssis_config_params_id

		SELECT @job_name = 'Executing SSIS package: ' + @package_name
      
		EXEC common.job_create @in_job_fk = NULL,
			@in_job_class_fk = @job_class_id,
			@in_job_name = @job_name,
			@in_data1 = NULL,
			@in_data2 = NULL,
			@in_data3 = NULL,
			@in_job_user = NULL,
			@out_job_fk = @epa_job_id OUTPUT,
			@client_app = 'SSIS' ;

		IF @package_type = 'CONSOLIDATOR' 
			BEGIN
				SET @cmd_textFromBroker = 'CMD'
				SET @cmd = 'DTEXEC /ISSERVER' +
					' "' +
					@package_path +
					'\' +
					@package_name +
					'" ' +
					'/SERVER "' + @server_name + '" ' + 
					'/Par "$ServerOption::LOGGING_LEVEL(Int16)";1 /Par "$ServerOption::SYNCHRONIZED(Boolean)";True ' +
					' /CALLERINFO SQLAGENT /REPORTING E'                      
			END
		ELSE 
			BEGIN
				SET @cmd = 'dtexec /server ' +
					@server_name  +
					' /isserver ' +
					@package_path + '\' +
					@package_name +
					' ' +
					'/envreference ' +
					@env_reference +
					' ' + '/set' + ' ' +
					'\Package.Variables[User::vJobID].Properties[Value];' + CAST(@epa_job_id AS VARCHAR(30)) +
					' /Par "$ServerOption::LOGGING_LEVEL(Int16)";1 /Par "$ServerOption::SYNCHRONIZED(Boolean)";True ' +
					'/CALLERINFO SQLAGENT /REPORTING E' 

				SET @cmd_textFromBroker = 'CMD'
			END

				
		INSERT INTO common.t_sql (sql_text) SELECT (SELECT @cmd)
					

		--put the SSIS execution into a Service Broker Queue         
		DECLARE @RC INT
		DECLARE @service_name NVARCHAR(50)
		DECLARE @cmd_text NVARCHAR(4000)
		DECLARE @cmd_type NVARCHAR(10)

		SELECT @service_name = 'TargetEpaService'
			, @cmd_text = @cmd
			, @cmd_type = @cmd_textFromBroker
		EXECUTE @RC = [queue_manager].[enqueue] @service_name, @cmd_text, @epa_job_id, @cmd_type

		SET @status_desc = 'finished execution of import.IMPORT_SSIS_DATA'
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
          
	END TRY
    BEGIN CATCH 

		SELECT @ErrorMessage = 'Execution of import.IMPORT_SSIS_DATA has failed ' + ERROR_MESSAGE()
			, @ErrorSeverity = ERROR_SEVERITY()
			, @ErrorState = ERROR_STATE();

		EXEC exec_monitor.Report_Error_sp @l_exec_no;
		DECLARE @exceptionRecipients VARCHAR(1024);
		SET @exceptionRecipients = isnull((SELECT email_address FROM msdb.dbo.sysoperators WHERE name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
		RAISERROR (@ErrorMessage, -- Message text.
			@ErrorSeverity, -- Severity.
			@ErrorState -- State.
		) ;
    END CATCH
END


















