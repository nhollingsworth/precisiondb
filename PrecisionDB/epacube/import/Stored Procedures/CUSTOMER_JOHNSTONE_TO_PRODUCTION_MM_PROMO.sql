﻿
CREATE PROCEDURE [import].[CUSTOMER_JOHNSTONE_TO_PRODUCTION_MM_PROMO]
	@in_job_fk           bigint
AS
BEGIN

--Declare @in_job_fk bigint
--Set @in_job_fk = (Select top 1 Job_FK from [import].[Customer_Johnstone_MM_Promo] order by job_fk desc)

DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);
	  DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
	 DECLARE @TotalAfterRecords int, @TotalBeforeRecords int

	SET NOCOUNT ON;
	SET @V_START_TIMESTAMP = getdate()  -- used in steps

--Load Promo Archive Table
Insert into [import].[Customer_Johnstone_MM_Promo_Archive]
(
[Company #], [Price Sheet #], [Price Sheet Descrip], [Promo Page #], [Flyer Month], [Product], [WHSE Code], [WHSE Costs Effective Date], [WHSE Costs End Date], [Replacement Cost], [Standard Cost], [Store DC Costs Effective Date], [Store DC Costs End Date], [Base Price], [CORP Costs Effective Date], [CORP Costs End Date], [CORP Price], [End User Price Effective Date], [End User Price End Date], [End User Each Price], [End User 1 Price], [End User 2 Price], [CreateRecordType], [Import_Filename], [Job_FK], [StartDateCorpICSW], [EndDateCorpICSW], [CorpReplacementCost], [CorpStandardCost], [Archive_Timestamp]
)
Select
[Company #], [Price Sheet #], [Price Sheet Descrip], [Promo Page #], [Flyer Month], [Product], [WHSE Code], [WHSE Costs Effective Date], [WHSE Costs End Date], [Replacement Cost], [Standard Cost], [Store DC Costs Effective Date], [Store DC Costs End Date], [Base Price], [CORP Costs Effective Date], [CORP Costs End Date], [CORP Price], [End User Price Effective Date], [End User Price End Date], [End User Each Price], [End User 1 Price], [End User 2 Price], [CreateRecordType], [Import_Filename], [Job_FK], [StartDateCorpICSW], [EndDateCorpICSW], [CorpReplacementCost], [CorpStandardCost]
, getdate() 'Archive_Timestamp'
from [import].[Customer_Johnstone_MM_Promo]

Delete from [import].[Customer_Johnstone_MM_Promo_Archive] where cast(Archive_Timestamp as date) < Cast(getdate() - 365 as date)

----Delete Records that were previously loaded from the same Price_Sheet_#
Exec [custom].[Johnstone_MM_PROMO_Record_Deletes_On_Import]
Exec [custom].[Johnstone_MM_PD_Record_Deletes_On_Import] 7

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of [import].[CUSTOMER_JOHNSTONE_TO_PRODUCTION_MM_PROMO]', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE()
	 select @TotalBeforeRecords = count(*) from [IMPORT].[IMPORT_MASS_MAINTENANCE_PROMO] with (nolock)

--Load 3 Promo DC From Vendor'
Insert into [import].[IMPORT_MASS_MAINTENANCE_PROMO]
(
[Company], [Price_Sheet_id], [Price_Sheet_Description], [Price_Sheet_Page], [Flyer_Month], [Warehouses], [ProductValue], [CreateRecordType], [intCreateRecordType], [StartDate], [EndDate], [Replacement_Cost], [Standard_Cost]
--, [Base_Price], [Corp_Price], [End_User_Each_Price], [End_User_1_Price], [End_User_2_Price]
, [Create_Timestamp], [Update_Timestamp]
--, [Create_User], [Update_User]
, [Action_Status], [Customer_Johnstone_MM_Promo_FK], [Import_FileName], [Job_FK]
, StartDateCorpICSW, EndDateCorpICSW, CorpReplacementCost, CorpStandardCost
)
Select Distinct
[Company #]
, [Price Sheet #]
, [Price Sheet Descrip]
, [Promo Page #]
, [Flyer Month]
, [Whse Code]
, [Product]
, 'Promo DC From Vendor' 'CreateRecordType'
, 3 'intCreateRecordType'

, [Whse Costs Effective Date]
, [Whse Costs End Date]
, [Replacement Cost]
, [Standard Cost]

, Getdate() 'create_timestamp'
, getdate() 'update_timestamp'
, 0 'Action_Status'
, Customer_Johnstone_MM_Promo_ID 'Customer_Johnstone_MM_Promo_FK'
, isnull(import_filename, 'Tmp_PlaceHolder') import_filename
, job_fk
, StartDateCorpICSW, EndDateCorpICSW, CorpReplacementCost, CorpStandardCost
from [import].[Customer_Johnstone_MM_Promo] CJMP with (Nolock)
where isnull([Whse Costs Effective Date], '') <> '' 
--and (isnull([Replacement Cost], '') <> '' and isnull([Replacement Cost], '') <> '0')
--and (isnull([Standard Cost], '') <> '' and isnull([Standard Cost], '') <> '0')
and Job_FK = @in_job_fk

--load 4 Promo To Store

Insert into [import].[IMPORT_MASS_MAINTENANCE_PROMO]
(
[Company], [Price_Sheet_id], [Price_Sheet_Description], [Price_Sheet_Page], [Flyer_Month], [Warehouses], [ProductValue], [CreateRecordType], [intCreateRecordType], [StartDateCorp], [EndDateCorp]
, [StartDate], [EndDate]
--, [Replacement_Cost], [Standard_Cost]
, [Base_Price]
, [Corp_Price]
--, [End_User_Each_Price], [End_User_1_Price], [End_User_2_Price]
, [Create_Timestamp], [Update_Timestamp]
--, [Create_User], [Update_User]
, [Action_Status], [Customer_Johnstone_MM_Promo_FK], [Import_FileName], [Job_FK]
)
Select Distinct
[Company #]
, [Price Sheet #]
, [Price Sheet Descrip]
, [Promo Page #]
, [Flyer Month]
, [Whse Code]
, [Product]
, 'Promo To Store - Create Typ 7 PD Records' 'CreateRecordType'
, 4 'intCreateRecordType'
, Case isnull([Corp Price], '') when '' then Null else [Corp Costs Effective Date] end
, Case isnull([Corp Price], '') when '' then Null else [Corp Costs End Date] end
, [Store DC Costs Effective Date]
, [Store DC Costs End Date]
, [Base Price]
, Case isnull([Corp Price], '') when '' then Null else [Corp Price] end
, Getdate() 'create_timestamp'
, getdate() 'update_timestamp'
, 0 'Action_Status'
, Customer_Johnstone_MM_Promo_ID 'Customer_Johnstone_MM_Promo_FK'
, isnull(import_filename, 'Tmp_PlaceHolder') import_filename
, job_fk
from [import].[Customer_Johnstone_MM_Promo] CJMP with (Nolock)
where (isnull(Cast([Corp Price] as money), 0) <> 0 or isnull(Cast([Base Price] as money), 0) <> 0)
and Job_FK = @in_job_fk

----Load 5 Promo To Store
--Insert into [import].[IMPORT_MASS_MAINTENANCE_PROMO]
--(
--[Company], [Price_Sheet_id], [Price_Sheet_Description], [Price_Sheet_Page], [Flyer_Month], [Warehouses], [ProductValue], [CreateRecordType], [intCreateRecordType], [StartDate], [EndDate]
----, [Replacement_Cost], [Standard_Cost]
--, [Base_Price]
----, [Corp_Price], [End_User_Each_Price], [End_User_1_Price], [End_User_2_Price]
--, [Create_Timestamp], [Update_Timestamp]
----, [Create_User], [Update_User]
--, [Action_Status], [Customer_Johnstone_MM_Promo_FK], [Import_FileName], [Job_FK]
--)
--Select 
--[Company #]
--, [Price Sheet #]
--, [Price Sheet Descrip]
--, [Promo Page #]
--, [Flyer Month]
--, [Whse Code]
--, [Product]
--, 'Promo To Store' 'CreateRecordType'
--, 5 'intCreateRecordType'
--, [Store DC Costs Effective Date]
--, [Store DC Costs End Date]
--, [Base Price]
--, Getdate() 'create_timestamp'
--, getdate() 'update_timestamp'
--, 0 'Action_Status'
--, Customer_Johnstone_MM_Promo_ID 'Customer_Johnstone_MM_Promo_FK'
--, isnull(import_filename, 'Tmp_PlaceHolder') import_filename
--, job_fk
--from [import].[Customer_Johnstone_MM_Promo] CJMP with (Nolock)
--where isnull(Cast([Base Price] as money), 0) <> 0
--and Job_FK = @in_job_fk

--Load 6 Promo To End User
Insert into [import].[IMPORT_MASS_MAINTENANCE_PROMO]
(
[Company], [Price_Sheet_id], [Price_Sheet_Description], [Price_Sheet_Page], [Flyer_Month], [Warehouses], [ProductValue], [CreateRecordType], [intCreateRecordType], [StartDate], [EndDate]
--, [Replacement_Cost], [Standard_Cost], [Base_Price]
--, [Corp_Price]
, [End_User_Each_Price], [End_User_1_Price], [End_User_2_Price]
, [Create_Timestamp], [Update_Timestamp]
--, [Create_User], [Update_User]
, [Action_Status]
, [Customer_Johnstone_MM_Promo_FK], [Import_FileName], [Job_FK]
)
Select Distinct
[Company #]
, [Price Sheet #]
, [Price Sheet Descrip]
, [Promo Page #]
, [Flyer Month]
, [Whse Code]
, [Product]
, 'Promo To End User' 'CreateRecordType'
, 6 'intCreateRecordType'

, [End User Price Effective Date]
, [End User Price End Date]
, [End User Each Price]
, [End User 1 Price]
, [End User 2 Price]
, Getdate() 'create_timestamp'
, getdate() 'update_timestamp'
, 0 'Action_Status'
, Customer_Johnstone_MM_Promo_ID 'Customer_Johnstone_MM_Promo_FK'
, isnull(import_filename, 'Tmp_PlaceHolder') import_filename
, job_fk
from [import].[Customer_Johnstone_MM_Promo] CJMP with (Nolock)
where 1 = 1
and Job_FK = @in_job_fk
--and (isnull([End User 1 Price], '') <> ''
--or isnull([End User 2 Price], '') <> ''
--or isnull([End User Each Price], '') <> '') 


Update IMMP
Set ProductDescription = PD.[Description]
from [import].[IMPORT_MASS_MAINTENANCE_PROMO] IMMP
inner join epacube.product_identification pi with (nolock) on IMMP.productvalue = pi.value and pi.DATA_NAME_FK = 110100
inner join epacube.PRODUCT_DESCRIPTION pd with (nolock) on pi.PRODUCT_STRUCTURE_FK = pd.PRODUCT_STRUCTURE_FK and pd.data_name_fk = 110401
Where IMMP.ProductDescription is null and IMMP.Job_FK = @in_job_fk

Update Promo
Set StartDateCorpICSW = Null
, EndDateCorpICSW = Null
, CorpReplacementCost = Null
, CorpStandardCost = Null
from [import].[IMPORT_MASS_MAINTENANCE_PROMO] Promo
where (isnull(corpreplacementcost, 0) = 0 and isnull(CorpStandardCost, 0) = 0)
and Job_FK = @in_job_fk

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------
	
	select @TotalAfterRecords = count(*) from [IMPORT].[IMPORT_MASS_MAINTENANCE_PROMO] with (nolock)

	SET @V_END_TIMESTAMP = getdate()

      EXEC common.job_execution_create  @in_job_fk, 'Inserted to table IMPORT_MASS_MAINTENANCE_PROMO',
		                                 @TotalAfterRecords, @TotalAfterRecords, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

     SET @status_desc =  'finished execution of [import].[CUSTOMER_JOHNSTONE_TO_PRODUCTION_MM_PROMO]';
     EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
     
      
      EXEC exec_monitor.print_status_sp @exec_id = @l_exec_no;
   END TRY
   BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;

EndOfProcedure:
--Print 'Records Already Loaded'
END

