﻿















-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        08/10/2011   Initial Version 



CREATE PROCEDURE [import].[LOAD_TS_PRICESHEETS] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_TS_PRICESHEETS', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.IMPORT_ECLIPSE_PRICE_HISTORY WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM import.IMPORT_ECLIPSE_PRICE_HISTORY WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD TS PRICESHEETS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


--------------------------------------------------------------------
---match product - check by UPC
--------------------------------------------------------------------

UPDATE import.Import_record_data
SET DATA_POSITION100 = (select pi.product_Structure_Fk
FROM epacube.product_identification pi
where import.IMPORT_RECORD_DATA.DATA_POSITION44 = pi.value
and pi.data_name_FK = 110102)
WHERE 1 = 1 
AND   JOB_FK = @IN_JOB_FK 


update import.IMPORT_RECORD_DATA
set DO_NOT_IMPORT_IND = 1
where DATA_POSITION100 is  NULL
and job_fk = @in_job_Fk



INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[TABLE_PK]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[ERROR_DATA4]
--           ,[ERROR_DATA5]
--           ,[ERROR_DATA6]
           ,[CREATE_TIMESTAMP])
SELECT 
JOB_FK
,'IMPORT TS PRICESHEETS'
,NULL
,99
,'MISSING PRODUCT INFORMATION'
,DATA_POSITION44
,NULL
,effective_date
,NULL
,getdate()
FROM import.IMPORT_RECORD_DATA
WHERE ISNULL(DO_NOT_IMPORT_IND ,0) = 1
AND DATA_POSITION100 is NULL



-------------------------------------------------------------------------------------
--   Load Pricesheet table
-------------------------------------------------------------------------------------

--TRUNCATE TABLE [marginmgr].[PRICESHEET]


INSERT INTO [marginmgr].[PRICESHEET]
           ([PRICESHEET_NAME]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[RESULT_TYPE_CR_FK]
           ,[NET_VALUE1]
           ,[NET_VALUE2]
           ,[NET_VALUE3]
           ,[NET_VALUE4]
           ,[NET_VALUE5]
           ,[NET_VALUE6]
           ,[PRICESHEET_QTY_PER]
           ,[VALUE_UOM_CODE_FK]
           ,[UOM_CODE]
           ,[PROD_ID_DN]
           ,[PROD_ID_VALUE]
           ,[ORG_ID_ENTITY_DN]
           ,[ORG_ID_DN]
           ,[ORG_ID_VALUE]
           ,[ENTITY_ID_ENTITY_DN]
           ,[ENTITY_ID_DN]
           ,[ENTITY_ID_VALUE]
           ,[CURRENCY_CR_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
(SELECT 
'TRADE PRICESHEET' -- price sheet name
,822100  --	Trade cost columns
,IRD.DATA_POSITION100 ---PRODUCT_STRUCTURE_FK
,1        --[ORG_ENTITY_STRUCTURE_FK]
,NULL  --         ,[ENTITY_STRUCTURE_FK]
,ISNULL(IRD.EFFECTIVE_DATE, GETDATE())  --,[EFFECTIVE_DATE]
,NULL --,[END_DATE]
,710 --Current --,[RESULT_TYPE_CR_FK]
           ,IRD.DATA_POSITION28  --[NET_VALUE1] 822101
           ,IRD.DATA_POSITION26     --[NET_VALUE2]
           ,IRD.DATA_POSITION25   --[NET_VALUE3]
           ,IRD.DATA_POSITION24   --[NET_VALUE4]
           ,IRD.DATA_POSITION23     --[NET_VALUE5]
           ,NULL   -- base [NET_VALUE6]
           ,IRD.DATA_POSITION21  ---[PRICESHEET_QTY_PER]
           ,(select UOM_CODE_ID FROM epacube.uom_code with (NOLOCK)
			Where uom_code = IRD.DATA_POSITION20)    ---[VALUE_UOM_CODE_FK]
           ,IRD.DATA_POSITION20   --[UOM_CODE]
           ,110102  --UPC [PROD_ID_DN]
           ,IRD.DATA_POSITION44  ---[PROD_ID_VALUE]
           ,'ALL WHSE' --[ORG_ID_ENTITY_DN]
				,141111
           ,NULL --IEPH.PriceSheetID
           ,NULL  --[ENTITY_ID_ENTITY_DN]
           ,NULL  --[ENTITY_ID_DN]
           ,NULL ---[ENTITY_ID_VALUE]
           ,1200   --USD  --[CURRENCY_CR_FK]
           ,1
           ,getdate()
           ,NULL
           ,NULL
 FROM import.IMPORT_RECORD_DATA IRD
WHERE 1 = 1 
AND ISNULL(DO_NOT_IMPORT_IND ,0) <> 1
AND IRD.JOB_FK = @IN_JOB_FK)

--------------------------------------------------------------------------

----add some logging

 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD TS PRICESHEETS' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD TS PRICESHEETS' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( RULES_ERRORS TABLE )
--
--     ADD RULES VALIDATION ROUTINE TO BE CALLED FROM UI LATER.. 
--
--	   THESE ERRORS ARE BUSINESS LOGIC ERRORS 
--        WHICH WILL BE BY RULE_TYPE_CR_FK AND RULES_ACTION_OPERATION_FK
--     
--     RULE WILL BE WRITTEN TO RULES TABLE BUT WITH AN ERROR CONDITION
--
--
-------------------------------------------------------------------------------------






 SET @status_desc =  'finished execution of import.LOAD_TS_PRICESHEETS'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_TS_PRICESHEETS has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END



















