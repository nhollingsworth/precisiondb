﻿







-- Copyright 2013 epaCUBE, Inc.
--
-- Procedure created by Leslie Andrews
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- LA        01/16/2013   Initial Version - copied from LOAD_SXE_GENERAL DATA
--                         to allow Eclipse customers to load Data Values via Import



CREATE PROCEDURE [import].[LOAD_ECLIPSE_GENERAL_DATA] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128)
	 DECLARE @V_SXE_MULTIPLE_CPT varchar(1)
	 DECLARE @V_SXE_MULTIPLE_CRT varchar(1) 

--DECLARE @in_job_fk bigint

--set @in_job_fk = (select distinct job_fk from import.IMPORT_ECLIPSE_TABLE_DATA)

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_ECLIPSE_GENERAL_DATA', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.IMPORT_ECLIPSE_TABLE_DATA WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM import.IMPORT_ECLIPSE_TABLE_DATA WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD IMPORT ECLIPSE CUSTOMER GENERAL DATA',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


-------------------------------------------------------------------------------------
--INSERT into DATA VALUE TABLE 
--    Product Category, Product Price Type, Family Group, Non Tax Reason
--    Rebate Type, and Rebate Sub Type, Supplier Group 
--Only for Records that DO NOT already exist in database
-------------------------------------------------------------------------------------

--Buy Line
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '310904', i.VALUE1, max(i.VALUE2) as Value2, 1  from import.IMPORT_ECLIPSE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 310904
Where DATA_NAME LIKE ('%Buy Line%') and DATA_VALUE_ID is NULL GROUP BY i.VALUE1)

--Budget Group
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '310907', i.VALUE1, max(i.VALUE2) as Value2, 1  from import.IMPORT_ECLIPSE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 310907
Where DATA_NAME LIKE ('%Budget Group%') and DATA_VALUE_ID is NULL GROUP BY i.VALUE1)

--Price Line
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '310903', i.VALUE1, max(i.VALUE2) as Value2, 1  from import.IMPORT_ECLIPSE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 310903
Where DATA_NAME LIKE ('%Price Line%') and DATA_VALUE_ID is NULL GROUP BY i.VALUE1)

--Product Status
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '310201', i.VALUE1, max(i.VALUE2) as Value2, 1  from import.IMPORT_ECLIPSE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 310201
Where DATA_NAME LIKE ('%Product Status%') and DATA_VALUE_ID is NULL GROUP BY i.VALUE1)

--Commission Group
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '311920', i.VALUE1, max(i.VALUE2) as Value2, 1  from import.IMPORT_ECLIPSE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 311920
Where DATA_NAME LIKE ('%Commission Group%') and DATA_VALUE_ID is NULL GROUP BY i.VALUE1)

--Discount Class
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '311903', i.VALUE1, max(i.VALUE2) as Value2, 1  from import.IMPORT_ECLIPSE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 311903
Where DATA_NAME LIKE ('%Discount Class%') and DATA_VALUE_ID is NULL GROUP BY i.VALUE1)

--Pass Along Discount
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '311921', i.VALUE1, max(i.VALUE2) as Value2, 1  from import.IMPORT_ECLIPSE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 311921
Where DATA_NAME LIKE ('%Pass Along Discount%') and DATA_VALUE_ID is NULL GROUP BY i.VALUE1)

--GL Product Type
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '311901', i.VALUE1, max(i.VALUE2) as Value2, 1  from import.IMPORT_ECLIPSE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 311901
Where DATA_NAME LIKE ('%GL Product Type%') and DATA_VALUE_ID is NULL GROUP BY i.VALUE1)

--Index Type
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '311902', i.VALUE1, max(i.VALUE2) as Value2, 1  from import.IMPORT_ECLIPSE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 311902
Where DATA_NAME LIKE ('%Index Type%') and DATA_VALUE_ID is NULL GROUP BY i.VALUE1)

--Select Group
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '311906', i.VALUE1, max(i.VALUE2) as Value2, 1  from import.IMPORT_ECLIPSE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 311906
Where DATA_NAME LIKE ('%Select Group%') and DATA_VALUE_ID is NULL GROUP BY i.VALUE1)

-------------------------------------------------------------------------------------
--Update the Description of Data_Value records 
--Where they already DO exist in the database
-----------------------------------------------------------------------------------------

--Buy Line 
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_ECLIPSE_TABLE_DATA.VALUE2
FROM import.IMPORT_ECLIPSE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_ECLIPSE_TABLE_DATA.VALUE1
   and import.IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Buy Line%'
   and epacube.data_value.DATA_NAME_FK = 310904

--Budget Group
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_ECLIPSE_TABLE_DATA.VALUE2
FROM import.IMPORT_ECLIPSE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_ECLIPSE_TABLE_DATA.VALUE1
   and import.IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Budget Group%'
   and epacube.data_value.DATA_NAME_FK = 310907


--Price Line
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_ECLIPSE_TABLE_DATA.VALUE2
FROM import.IMPORT_ECLIPSE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_ECLIPSE_TABLE_DATA.VALUE1
   and import.IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Price Line%'
   and epacube.data_value.DATA_NAME_FK = 310903


--Product Status
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_ECLIPSE_TABLE_DATA.VALUE2
FROM import.IMPORT_ECLIPSE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_ECLIPSE_TABLE_DATA.VALUE1
   and import.IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Product Status%'
   and epacube.data_value.DATA_NAME_FK = 310201


--Commission Group
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_ECLIPSE_TABLE_DATA.VALUE2
FROM import.IMPORT_ECLIPSE_TABLE_DATA
WHERE
   DATA_VALUE.VALUE = IMPORT_ECLIPSE_TABLE_DATA.VALUE1
   and IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Commission Group%'
   and data_value.DATA_NAME_FK = 311902


--Discount Class 
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_ECLIPSE_TABLE_DATA.VALUE2
FROM import.IMPORT_ECLIPSE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_ECLIPSE_TABLE_DATA.VALUE1
   and import.IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Discount Class%'
   and epacube.data_value.DATA_NAME_FK = 311903

--Pass Along Discount
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_ECLIPSE_TABLE_DATA.VALUE2
FROM import.IMPORT_ECLIPSE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_ECLIPSE_TABLE_DATA.VALUE1
   and import.IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Pass Along Discount%'
   and epacube.data_value.DATA_NAME_FK = 311921
   
--GL Product Type
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_ECLIPSE_TABLE_DATA.VALUE2
FROM import.IMPORT_ECLIPSE_TABLE_DATA
WHERE
   DATA_VALUE.VALUE = IMPORT_ECLIPSE_TABLE_DATA.VALUE1
   and IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%GL Product Type%'
   and data_value.DATA_NAME_FK = 310901


--Index Type 
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_ECLIPSE_TABLE_DATA.VALUE2
FROM import.IMPORT_ECLIPSE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_ECLIPSE_TABLE_DATA.VALUE1
   and import.IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Index Type%'
   and epacube.data_value.DATA_NAME_FK = 310902

--Select Group
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_ECLIPSE_TABLE_DATA.VALUE2
FROM import.IMPORT_ECLIPSE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_ECLIPSE_TABLE_DATA.VALUE1
   and import.IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Select Group%'
   and epacube.data_value.DATA_NAME_FK = 310906
   

-------------------------------------------------------------------------------------
--INSERT into DATA VALUE TABLE 
--    Necessary Vendor Records:  [ENTITY_STRUCTURE]
--                               [ENTITY_IDENTIFICATION] Requires ENTITY_STRUCTURE_FK 
--                               [ENTITY_IDENT_NONUNIQUE]Requires ENTITY_STRUCTURE_FK
--Only for Records that DO NOT already exist in database
-------------------------------------------------------------------------------------
--For all vendorIDs that are new in the file, insert required data into all tables

  INSERT INTO epacube.ENTITY_STRUCTURE (ENTITY_STATUS_CR_FK, DATA_NAME_FK, LEVEL_SEQ, ENTITY_CLASS_CR_FK, RECORD_STATUS_CR_FK, IMPORT_FILENAME)
  (SELECT 10200, 143000, 1, 10103, 1, VALUE1 FROM import.IMPORT_ECLIPSE_TABLE_DATA i
  left outer join epacube.ENTITY_IDENTIFICATION ei on i.VALUE1 = ei.value and ENTITY_DATA_NAME_FK = 143000
  where DATA_NAME LIKE '%Vendor%' and ei.VALUE is null)
  
  INSERT INTO epacube.ENTITY_IDENTIFICATION (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, VALUE, RECORD_STATUS_CR_FK)
  (select ENTITY_STRUCTURE_ID, 143000, CASE WHEN ISNUMERIC(VALUE1) > 0 THEN 143110 else 143111 end, VALUE1, 1
  FROM import.IMPORT_ECLIPSE_TABLE_DATA d
  inner JOIN epacube.ENTITY_STRUCTURE E on E.Import_FileName = d.value1 and DATA_NAME_FK = 143000
  left outer join epacube.ENTITY_IDENTIFICATION ei on e.IMPORT_FILENAME = ei.value and ENTITY_DATA_NAME_FK = 143000
  where DATA_NAME LIKE '%Vendor%' and ei.VALUE is null)
  
  INSERT INTO [epacube].[ENTITY_IDENT_NONUNIQUE] (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, VALUE)
  (select e.ENTITY_STRUCTURE_FK, e.ENTITY_DATA_NAME_FK, 143112, VALUE2
  FROM import.IMPORT_ECLIPSE_TABLE_DATA d
  inner join epacube.ENTITY_IDENTIFICATION e on d.VALUE1 = e.VALUE
  left outer join epacube.Entity_Ident_Nonunique s on e.ENTITY_STRUCTURE_FK = s.ENTITY_STRUCTURE_FK
  where DATA_NAME LIKE '%Vendor%' and s.ENTITY_STRUCTURE_FK is null)
  
  --the IMPORT_FILE field in the ENTITY_STRUCTURE table was populated with VALUE1 in order
  --to give an easy identifier field to join on.  This query updates the field to remove that ID
  UPDATE epacube.ENTITY_STRUCTURE SET IMPORT_FILENAME = NULL 
  WHERE IMPORT_FILENAME IN (SELECT VALUE1 FROM import.IMPORT_ECLIPSE_TABLE_DATA where DATA_NAME LIKE '%Vendor%')
  AND ENTITY_STATUS_CR_FK = 10200 AND DATA_NAME_FK = 143000
  
-------------------------------------------------------------------------------------
--   UPDATE Vendor name with changes from Source file
-------------------------------------------------------------------------------------
 DECLARE 
               @Vendor_name varchar(100),
               @entity_structure_fk bigint                                          
               

	DECLARE cur_v_import CURSOR local for
		SELECT   import.IMPORT_ECLIPSE_TABLE_DATA.VALUE2, entity_structure_fk from import.IMPORT_ECLIPSE_TABLE_DATA  WITH (NOLOCK)
INNER JOIN epacube.ENTITY_IDENTIFICATION WITH (NOLOCK)
ON ( ENTITY_STRUCTURE_FK = epacube.ENTITY_IDENTIFICATION.ENTITY_STRUCTURE_FK
and epacube.ENTITY_IDENTIFICATION.VALUE = import.IMPORT_ECLIPSE_TABLE_DATA.value1
and epacube.ENTITY_IDENTIFICATION.DATA_NAME_FK IN (143110 , 143111)
and import.IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Vendor%')


         OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO @Vendor_name,
							@entity_structure_fk 


      WHILE @@FETCH_STATUS = 0
      BEGIN


		 
UPDATE [epacube].[ENTITY_IDENT_NONUNIQUE]
SET VALUE = @vendor_name
WHERE epacube.ENTITY_IDENT_NONUNIQUE.DATA_NAME_FK = 143112
and epacube.ENTITY_IDENT_NONUNIQUE.entity_structure_fk = @entity_structure_fk



 FETCH NEXT FROM cur_v_import Into @Vendor_name,
							 @entity_structure_fk


END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_import
DEALLOCATE cur_v_import    
--end Vendor Name Update

  
-------------------------------------------------------------------------------------
--INSERT into DATA VALUE TABLE 
--    Necessary WHSE Records:  [ENTITY_STRUCTURE]
--                             [ENTITY_IDENTIFICATION] Requires ENTITY_STRUCTURE_FK 
--                             [ENTITY_IDENT_NONUNIQUE]Requires ENTITY_STRUCTURE_FK
--Only for Records that DO NOT already exist in database
-------------------------------------------------------------------------------------
--For all WHSEIDs that are new in the file, insert required data into all tables

  INSERT INTO epacube.ENTITY_STRUCTURE (ENTITY_STATUS_CR_FK, DATA_NAME_FK, LEVEL_SEQ, ENTITY_CLASS_CR_FK, RECORD_STATUS_CR_FK, IMPORT_FILENAME)
  (SELECT 10200, 141000, 1, 10101, 1, VALUE1 FROM import.IMPORT_ECLIPSE_TABLE_DATA i
  left outer join epacube.ENTITY_IDENTIFICATION ei on i.VALUE1 = ei.value and ENTITY_DATA_NAME_FK = 141000
  where DATA_NAME LIKE '%Branch%' and ei.VALUE is null)
  
  INSERT INTO epacube.ENTITY_IDENTIFICATION (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, VALUE, RECORD_STATUS_CR_FK)
  (select ENTITY_STRUCTURE_ID, 141000, 141111, VALUE1, 1
  FROM import.IMPORT_ECLIPSE_TABLE_DATA d
  inner JOIN epacube.ENTITY_STRUCTURE E on E.Import_FileName = d.value1 and DATA_NAME_FK = 141000
  left outer join epacube.ENTITY_IDENTIFICATION ei on e.IMPORT_FILENAME = ei.value and ENTITY_DATA_NAME_FK = 143000
  where DATA_NAME LIKE '%Branch%' and ei.VALUE is null)
  
  INSERT INTO [epacube].[ENTITY_IDENT_NONUNIQUE] (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, VALUE)
  (select e.ENTITY_STRUCTURE_FK, e.ENTITY_DATA_NAME_FK, 141112, VALUE2
  FROM import.IMPORT_ECLIPSE_TABLE_DATA d
  inner join epacube.ENTITY_IDENTIFICATION e on d.VALUE1 = e.VALUE
  left outer join epacube.Entity_Ident_Nonunique s on e.ENTITY_STRUCTURE_FK = s.ENTITY_STRUCTURE_FK
  where DATA_NAME LIKE '%Branch%' and s.ENTITY_STRUCTURE_FK is null)
  
  ----the IMPORT_FILE field in the ENTITY_STRUCTURE table was populated with VALUE1 in order
  ----to give an easy identifier field to join on.  This query updates the field to remove that ID
  UPDATE epacube.ENTITY_STRUCTURE SET IMPORT_FILENAME = NULL 
  WHERE IMPORT_FILENAME IN (SELECT VALUE1 FROM import.IMPORT_ECLIPSE_TABLE_DATA where DATA_NAME LIKE '%Branch%')
  AND ENTITY_STATUS_CR_FK = 10200 AND DATA_NAME_FK = 141000

-------------------------------------------------------------------------------------
--   UPDATE Warehouse name with changes from Source file
--reuse variables 
-------------------------------------------------------------------------------------
DECLARE 
               @Warehouse_name varchar(100),
               @Whse_entity_structure_fk bigint                                          
               
DECLARE cur_v_import CURSOR local for
		SELECT   import.IMPORT_ECLIPSE_TABLE_DATA.VALUE2, entity_structure_fk from import.IMPORT_ECLIPSE_TABLE_DATA  WITH (NOLOCK)
INNER JOIN epacube.ENTITY_IDENTIFICATION WITH (NOLOCK)
ON ( ENTITY_STRUCTURE_FK = epacube.ENTITY_IDENTIFICATION.ENTITY_STRUCTURE_FK
and epacube.ENTITY_IDENTIFICATION.VALUE = import.IMPORT_ECLIPSE_TABLE_DATA.value1
and epacube.ENTITY_IDENTIFICATION.DATA_NAME_FK =141111 
and import.IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Branch%')


         OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO @Warehouse_name,
							@Whse_entity_structure_fk 


      WHILE @@FETCH_STATUS = 0
      BEGIN


		 
UPDATE [epacube].[ENTITY_IDENT_NONUNIQUE]
SET VALUE = @Warehouse_name
WHERE epacube.ENTITY_IDENT_NONUNIQUE.DATA_NAME_FK = 141112
and epacube.ENTITY_IDENT_NONUNIQUE.entity_structure_fk = @Whse_entity_structure_fk



 FETCH NEXT FROM cur_v_import Into @Warehouse_name,
							 @Whse_entity_structure_fk


END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_import
DEALLOCATE cur_v_import    
--end Warehouse Name Update

--create a temporary table variable with all the DATA_NAME_FK values to join
--into imported data table to get one record for EACH DATA_NAME_FK for each SELL GROUP record
DECLARE @SellCodes TABLE(
	[NAME] [varchar](10) NULL)
	INSERT INTO @SellCodes (NAME) VALUES ('311901')
	INSERT INTO @SellCodes (NAME) VALUES ('311904')
	INSERT INTO @SellCodes (NAME) VALUES ('311905')
	INSERT INTO @SellCodes (NAME) VALUES ('311906')
	INSERT INTO @SellCodes (NAME) VALUES ('311907')
	INSERT INTO @SellCodes (NAME) VALUES ('311908')
	INSERT INTO @SellCodes (NAME) VALUES ('311909')
	INSERT INTO @SellCodes (NAME) VALUES ('311910')
	INSERT INTO @SellCodes (NAME) VALUES ('311911')
	
--Insert Sell Group
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select NAME, i.VALUE1, i.VALUE2, 1  from import.IMPORT_ECLIPSE_TABLE_DATA i
cross join @SellCodes
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK IN 
(311901, 311904, 311905, 311906, 311907, 311908 ,311909, 311910, 311911)
Where DATA_NAME LIKE '%Sell Group%' and DATA_VALUE_ID is NULL)

--Update Sell Group
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_ECLIPSE_TABLE_DATA.VALUE2
FROM import.IMPORT_ECLIPSE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_ECLIPSE_TABLE_DATA.VALUE1
   and import.IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Sell Group%'
   and epacube.data_value.DATA_NAME_FK IN (311901, 311904, 311905, 311906, 311907, 311908 ,311909, 311910, 311911)
	
--create a temporary table variable with all the DATA_NAME_FK values to join
--into imported data table to get one record for EACH DATA_NAME_FK for each BUY GROUP record
DECLARE @BuyCodes TABLE(
	[NAME] [varchar](10) NULL)
	INSERT INTO @BuyCodes (NAME) VALUES ('311902')
	INSERT INTO @BuyCodes (NAME) VALUES ('311912')
	INSERT INTO @BuyCodes (NAME) VALUES ('311913')
	INSERT INTO @BuyCodes (NAME) VALUES ('311914')
	INSERT INTO @BuyCodes (NAME) VALUES ('311915')

	
--Insert Sell Group
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select NAME, i.VALUE1, i.VALUE2, 1  from import.IMPORT_ECLIPSE_TABLE_DATA i
cross join @BuyCodes
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK IN 
(311902, 311912, 311913, 311914, 311915)
Where DATA_NAME LIKE '%Buy Group%' and DATA_VALUE_ID is NULL)

--Update Sell Group
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_ECLIPSE_TABLE_DATA.VALUE2
FROM import.IMPORT_ECLIPSE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_ECLIPSE_TABLE_DATA.VALUE1
   and import.IMPORT_ECLIPSE_TABLE_DATA.DATA_NAME LIKE '%Buy Group%'
   and epacube.data_value.DATA_NAME_FK IN (311902, 311912, 311913, 311914, 311915)

 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD GENERAL_DATA' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                            and  name = 'LOAD GENERAL_DATA' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------


 SET @status_desc =  'finished execution of import.LOAD_ECL_GENERAL_DATA'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_ECL_GENERAL_DATA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END


























