﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [import].[Import_To_Production_CPTs_From_CustFile]
AS
BEGIN
	SET NOCOUNT ON;

If (Select COUNT(*) from import.IMPORT_SXE_TABLE_DATA) > (select value from epacube.epacube_params where NAME = 'MIN RECORDS EXPECTED TABLE')
Begin
	Declare @cpt_job_fk int

	set @cpt_job_fk = (select distinct job_fk from import.IMPORT_SXE_TABLE_DATA )

	EXEC import.LOAD_SXE_CPT  
	 @cpt_job_fk
end

If (Select Value from epacube.EPACUBE_PARAMS where NAME = 'RULES_FILTER_LOOKUP_OPERAND') = 0
Begin

	IF object_id('tempdb..##CPT') is not null
	drop table ##CPT
	Select * into ##CPT from
	(Select Cast(cust_price_type as Varchar(8)) CPT, cust_no, CAst('' as varchar(32)) ship_to, (select entity_structure_fk from epacube.entity_identification ei where ei.[value] = C.cust_no and entity_data_name_fk = 144010) entity_structure_fk
	from import.import_sxe_customers C where ship_to is null and cust_price_type is not null) A where entity_structure_fk is not null

	Insert into ##CPT
	Select * from (
	Select Cast(cust_price_type as Varchar(8)) CPT, cust_no, ship_to, (select entity_structure_fk from epacube.entity_identification ei where ei.[value] = Cast(C.cust_no as Varchar(32))+ '-' + CAst(C.Ship_To as Varchar(32)) and entity_data_name_fk = 144020) entity_structure_fk
	from import.import_sxe_customers C where ship_to is not null  and cust_price_type is not null) A where entity_structure_fk is not null

	Create index idx_tmp on ##CPT(CPT, entity_structure_fk)

	Insert into epacube.data_value
	(data_name_fk, [value], record_status_cr_fk)
	Select 244900, CPT, 1 from ##CPT 
	where CPT not in (Select value from epacube.data_value where data_name_fk = 244900) group by CPT
-----ADDED BECAUSE IF EVENTS ARE OUT THERE THEN THIS ISNSERTs and CAUSES CONTSTRAINT ERROR--CINDY
DELETE from synchronizer.event_data where event_status_cr_fk in (88,87,86,85,80) and data_name_fk in (
select data_name_id from epacube.data_name where data_Set_fk in (
select data_set_id from epacube.data_set where table_name = 'ENTITY_MULT_TYPE'))

	Insert into epacube.entity_mult_type
	(entity_structure_fk, data_name_fk, data_value_fk, record_status_cr_fk)
	Select Distinct cpt.entity_structure_fk, 244900, dv.data_value_id, 1
	from ##CPT CPT inner join epacube.data_value dv on CPT.CPT = dv.value and dv.data_name_FK = 244900
	left Join epacube.entity_mult_type emt on dv.data_name_fk = emt.data_name_fk and dv.data_value_id = emt.data_value_fk and CPT.entity_structure_fk = emt.entity_structure_fk
	where emt.entity_mult_type_id is null and CPT.entity_Structure_FK is not null

	IF object_id('tempdb..##CPT') is not null
	drop table ##CPT
End

Exec import.LOAD_MAUI_Z_RULES_DATA_ELEMENTS	

If (select value from epacube.EPACUBE_PARAMS where application_scope_fk = 100 and NAME = 'ERP HOST') = 'SXE'
Begin
	update dv
	set description = istd.value2
	from epacube.data_value dv
	inner join import.import_sxe_table_data istd on dv.value = istd.value1
	where istd.data_name = 'Product Category' and dv.data_name_fk = 210901

	Update MRDE
	Set ERP_Value_2 = dv.Value + ': ' + dv.description
	from dbo.MAUI_Rules_Data_Elements MRDE
	inner join epacube.data_value dv on MRDE.data_name_fk = dv.data_name_fk and MRDE.ERP_value_1 = dv.value

	Update CPLS
	set DescriptionGroup = dv.description
	from dbo.MAUI_Customer_Price_List_Sort CPLS
	inner join epacube.data_value dv on cpls.sortgroup = dv.value 
	where cpls.sortlevel = 2 and dv.data_name_fk = 210901
End

END

