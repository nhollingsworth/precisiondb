﻿
-- =============================================
-- Author:		<Gary Stone>
-- Create date: <October 22, 2009>
-- Description:	<Transform Sales Transaction History and load into import.import_order_data Table>
-- =============================================
CREATE PROCEDURE [import].[Load_MAUI_0_Sales_Transactions_ECLIPSE] 
@Job_FK_i Numeric(10, 2)
, @Process_BY_Region as int = Null
, @Process_Future_AND_Current_Assessment as int = Null
 ,@Process_Cost_Changes_Only as int = Null
, @Parent_Entity_Structure_FK Varchar(32) = Null
, @Whse_To_Process as Varchar(Max) = Null
, @Whse_To_Exclude as Varchar(Max) = Null
, @Sell_Lines as Varchar(max) = Null
, @Date_Range_Start as DateTime = Null
, @Date_Range_End as DateTime = Null
, @SO as Varchar(32)
, @RT as Varchar(32)
, @PC as Varchar(32)
, @DS as Varchar(32)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
------------------------------------
--Transfer Raw Sales Transaction History to Active Table
------------------------------------
IF object_id('tempdb..##Sales_Transactions') is not null
drop table ##Sales_Transactions;

IF object_id('tempdb..##Prod_Vend_Assoc') is not null
drop table ##Prod_Vend_Assoc;

IF object_id('tempdb..##Cust_Exclusions') is not null
drop table ##Cust_Exclusions;

IF object_id('tempdb..##DRank') is not null
drop table ##DRank

IF object_id('tempdb..##CntrctEx') is not null
drop table ##CntrctEx

IF object_id('tempdb..##whse_reg') is not null
drop table ##whse_reg

IF object_id('tempdb..##whses') is not null
drop table ##whses

--Declare @Job_FK_i Numeric(10, 2) = 110.01
--Declare @Process_BY_Region as int = 1
--Declare @Process_Future_AND_Current_Assessment as int = Null
--Declare @Process_Cost_Changes_Only as int = Null
--Declare @Parent_Entity_Structure_FK Varchar(32) = Null
--Declare @Whse_To_Process as Varchar(Max) = Null
--Declare @Whse_To_Exclude as Varchar(Max) = Null
--Declare @Sell_Lines as Varchar(max) = Null
--Declare @Date_Range_Start as DateTime = '2012-05-01'
--Declare @Date_Range_End as DateTime = '2012-05-31'
--Declare @SO as Varchar(32)
--Declare @RT as Varchar(32)
--Declare @PC as Varchar(32)
--Declare @DS as Varchar(32)

Declare @SQLcd as varchar(Max)
Declare @SQLex as varchar(Max)
Declare @Join_Exclusion as varchar(Max)
Declare @Custom_Filter as varchar(Max)
Declare @Fltr_Process_By_Region as varchar(Max)
Declare @Fltr_Region_Name_To_Process as varchar(Max)
Declare @Fltr_Whse_To_Process as varchar(Max)
Declare @Fltr_Whse_To_Exclude as varchar(Max)
Declare @Fltr_Dates as varchar(Max)
Declare @Fltr_Sell_Lines as varchar(Max)
Declare @Join_Sell_Lines as varchar(Max)
Declare @Join_CstChg as varchar(Max)
Declare @ERP Varchar(32)

Declare @Filter as varchar(Max)
Declare @Join_Region Varchar(Max)
Declare @Join_Whses Varchar(Max)

Set @ERP = (Select Value from epacube.epacube_params with (nolock) where name = 'ERP HOST')

insert into dbo.maui_log Select @Job_FK_i, 'Start Sales Transaction Temp Table Load', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null

Create Table ##Sales_Transactions(
	[ANALYSIS_EFFECTIVE_DATE] [varchar](128) NULL,
	[ANALYSIS_PERIOD_START_DATE] [varchar](128) NULL,
	[ANALYSIS_PERIOD_END_DATE] [varchar](128) NULL,
	[SALES_ORDER_ENTRY_DATE] [varchar](128) NULL,
	[SALES_ORDER_SHIP_DATE] [varchar](128) NULL,
	[SALES_ORDER_NUMBER] [varchar](128) NULL,
	[SALES_ORDER_SUFFIX] [varchar](128) NULL,
	[SALES_ORDER_LINE] [varchar](128) NULL,
	[ANALYSIS_DATA_SET_ABBREV] [varchar](128) NULL,
	[MATRIX_UOM] [varchar](128) NULL,
	[PRODUCT_ID] [varchar](128) NULL,
	[SHIP_TO_CUSTOMER_ID] [varchar](128) NULL,
	[BILL_TO_CUSTOMER_ID] [varchar](128) NULL,
	[WHSE_ID] [varchar](128) NULL,
	[WHSE_GROUP_ID] [varchar](128) NULL,
	[MFR_ID] [varchar](128) NULL,
	[VENDOR_ID] [varchar](128) NULL,
	[SALES_TERRITORY_ID] [varchar](128) NULL,
	[OUTSIDE_SALESREP_ID] [varchar](128) NULL,
	[INSIDE_SALESREP_ID] [varchar](128) NULL,
	[ORDER_TAKER_ID] [varchar](128) NULL,
	[BUYER_ID] [varchar](128) NULL,
	[SALES_QTY] [varchar](128) NULL,
	[SALES_UOM] [varchar](128) NULL,
	[SALES_PRIMARY_UOM_CONVERSION] [varchar](128) NULL,
	[HISTORICAL_UOM] [varchar](128) NULL,
	[HISTORICAL_PRIMARY_UOM_CONVERSION] [varchar](128) NULL,
	[SPECIAL_UOM_CONVERSION] [varchar](128) NULL,
	[CURRENCY] [varchar](128) NULL,
	[SELL_PRICE_AMT] [varchar](128) NULL,
	[SELL_PRICE_MATRIX_AMT] [varchar](128) NULL,
	[SELL_PRICE_OVERRIDE_AMT] [varchar](128) NULL,
	[ORDER_COGS_AMT] [varchar](128) NULL,
	[ORDER_COGS_OVERRIDE_AMT] [varchar](128) NULL,
	[REPL_COST_AMT] [varchar](128) NULL,
	[MARGIN_COST_AMT] [varchar](128) NULL,
	[REBATED_COST_CB_AMT] [varchar](128) NULL,
	[REBATE_CB_AMT] [varchar](128) NULL,
	[REBATE_BUY_AMT] [varchar](128) NULL,
	[PRICE_MATRIX_ID_HISTORICAL] [varchar](128) NULL,
	[PRICE_CONTRACT_NO_HISTORICAL] [varchar](128) NULL,
	[PRICE_CONTRACT_REFERENCE_HISTORICAL] [varchar](128) NULL,
	[PRICE_FORMULA_HISTORICAL] [varchar](128) NULL,
	[PRICE_BASIS_COLMUN_HISTORICAL] [varchar](128) NULL,
	[REBATE_CB_MATRIX_ID_HISTORICAL] [varchar](128) NULL,
	[REBATE_CB_CONTRACT_NO_HISTORICAL] [varchar](128) NULL,
	[REBATE_CB_CONTRACT_REFERENCE_HISTORICAL] [varchar](128) NULL,
	[REBATE_CB_FORMULA_HISTORICAL] [varchar](128) NULL,
	[REBATE_CB_BASIS_COLMUN_HISTORICAL] [varchar](128) NULL,
	[REBATE_BUY_MATRIX_ID_HISTORICAL] [varchar](128) NULL,
	[REBATE_BUY_CONTRACT_NO_HISTORICAL] [varchar](128) NULL,
	[REBATE_BUY_CONTRACT_REFERENCE_HISTORICAL] [varchar](128) NULL,
	[REBATE_BUY_FORMULA_HISTORICAL] [varchar](128) NULL,
	[REBATE_BUY_BASIS_COLMUN_HISTORICAL] [varchar](128) NULL,
	[JOB_FK] [bigint] NULL,
	[IMPORT_FILENAME] [varchar](256) NULL,
	[PRODUCT_STRUCTURE_FK] [bigint] NULL,
	[ORG_ENTITY_STRUCTURE_FK] [bigint] NULL,
	[CUST_ENTITY_STRUCTURE_FK] [bigint] NULL,
	[ERROR_ON_IMPORT_IND] [int] NULL,
	Record_No [bigint] Null
) ON [PRIMARY]

Create Index idx_st on ##Sales_Transactions(Sales_Qty, SALES_PRIMARY_UOM_CONVERSION, REBATE_CB_CONTRACT_NO_HISTORICAL)

--If isnull(@Process_Future_AND_Current_Assessment, 0) = 1 or ISNULL(@Process_Cost_Changes_Only,0) = 1
If ISNULL(@Process_Cost_Changes_Only,0) = 1
	Begin
		Set @Join_CstChg = ' inner join ##CstChg CstChg on ST.Whse_ID = CstChg.Whse and ST.Product_ID = CstChg.Product_ID'
	End
ELSE
	Begin
		Set @Join_CstChg = ''
	End

If (Select value from epacube.EPACUBE_PARAMS where NAME = 'EPACUBE CUSTOMER')= 'CARRIER'
	BEGIN

	Set @SQLex = 'Select * into ##Cust_Exclusions from (select entity_structure_fk cust_entity_structure_fk, dv.value Class
					from epacube.ENTITY_CATEGORY ec inner join epacube.data_value dv on ec.data_value_fk = dv.data_value_id and ec.data_name_fk = dv.data_name_fk
					where dv.data_name_fk = (select isnull(data_name_fk, 0) from dbo.MAUI_Config_ERP with (nolock) where usage = ''Customer_Grouping'' and epaCUBE_Column = ''Cust_Group1'' and ERP = @ERP)
					and dv.value in (''DIS'', ''INT'', ''RLC'', ''RNA'') 
				
				Create Index idx_CT on ##Cust_Exclusions(entity_structure_fk)'
	
	
	
	Set @Join_Exclusion = ' Left Join ##Cust_Exclusions EX on EX.Cust_Entity_Structure_FK = 
						(Select entity_structure_fk from epacube.entity_identification where entity_data_name_fk = 144020 and value = ST.Ship_To_Customer_ID)'
	

		select distinct LEFT([contract_no], charindex('~', [contract_no])-1) Contract_NO into ##CntrctEx from synchronizer.rules where [contract_no] like '%~%'
		Create Index ctex on ##CntrctEx(Contract_NO)

	Set @Custom_Filter = ' and isnull(Cast(ST.[REBATE_CB_CONTRACT_NO_HISTORICAL] as varchar(32)), '''') not in (select Contract_NO from ##CntrctEx)
					and Cast(ST.Outside_SalesRep_ID as varchar(32)) not in (''9h99'', ''8H99'', ''8H98'', ''7H98'', ''7H99'', ''2H99'', ''4H99'', ''5H99'', ''3H99'')'
	
	End
ELSE
	BEGIN

	Set @SQLex = 'Select Null Bill_To_Customer_ID, Null Ship_To_Customer_ID, Null Class into ##Cust_Exclusions'
	
	Set @Join_Exclusion = ' Left Join ##Cust_Exclusions EX on ST.Bill_To_Customer_ID = EX. Bill_To_Customer_ID'
	Set @Custom_Filter = ''
	END

	Exec (@SQLex)
		
If isnull(@Process_BY_Region, 0) = 1 OR @Parent_Entity_Structure_FK IS NOT NULL
	Begin
	
	Select Cast(value as varchar(32)) whse into ##whse_reg from epacube.entity_identification where entity_structure_fk in (select entity_structure_id from epacube.entity_structure where parent_entity_structure_fk = @Parent_Entity_Structure_FK)
	Create index idx_whs on ##whse_reg(whse)
	
	Set @Join_Region = 'inner join ##whse_reg whs on ST.whse_ID = whs.whse'
	insert into dbo.maui_log Select @Job_FK_i, 'Creating Region Filter', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null
	End
	else 
	Set @Join_Region = ''

If @Date_Range_Start is not null and @Date_Range_End is not null
	Set @Fltr_Dates = ' and SALES_ORDER_SHIP_DATE between ''' + Convert(Varchar(10), @Date_Range_Start, 121) + '''' + ' and ' + '''' + Convert(Varchar(10), @Date_Range_End, 121) + ''''
Else if @Date_Range_Start is not null
	Set @Fltr_Dates = ' and SALES_ORDER_SHIP_DATE >= ''' + Convert(Varchar(10), @Date_Range_Start, 121) + ''''
Else if @Date_Range_End is not null
	Set @Fltr_Dates = ' and SALES_ORDER_SHIP_DATE <= ''' + Convert(Varchar(10), @Date_Range_End, 121) + ''''
Else
	Set @Fltr_Dates = ''

If isnull(@Whse_To_Process, '') > ''
	Begin
	Declare @SQL_whse varchar(max)
	Set @SQL_whse = 'Select Cast(value as varchar(32)) whse into ##whses from epacube.entity_identification where value in (' + @Whse_To_Process + ')'
	Exec @SQL_whse
	Create index idx_whs on ##whses(whse)

	Set @Join_Whses = 'inner join ##whse whses on ST.whse_ID = whses.whse'
	insert into dbo.maui_log Select @Job_FK_i, 'Creating Warehouse(s) Filter ' + @Whse_To_Process, GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null
	end

	else
	Set @Join_Whses = ''

If isnull(@Whse_To_Exclude, '') > ''
	Begin
	Set @Fltr_Whse_To_Exclude = ' and ST.whse_ID not in (' + @Whse_To_Exclude + ')'
	insert into dbo.maui_log Select @Job_FK_i, 'Creating Warehouse(s) Exclusion Filter ' + @Whse_To_Exclude, GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null
	end

	else
	Set @Fltr_Whse_To_Exclude = ''
		
If isnull(@Sell_Lines, '') > ''
	Begin
	insert into dbo.maui_log Select @Job_FK_i, 'Creating Vendor(s) Filter ' + @Sell_Lines, GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null

	Set @Fltr_Sell_Lines = ' and PVA.Sell_Line in (' + @Sell_Lines + ')'
			
		Select PI.Value Product_ID, dv_PG4.value Sell_Line	
		into ##Prod_Vend_Assoc
		from epacube.PRODUCT_IDENTIFICATION PI with (nolock) 
		inner Join epacube.PRODUCT_CATEGORY PG4 with (nolock) on PG4.Product_Structure_FK = PI.Product_Structure_FK and PG4.Org_Entity_Structure_FK = 1
			and PG4.Data_Name_FK = (select data_name_fk from dbo.MAUI_Config_ERP with (nolock) where usage = 'Product_Grouping' and epaCUBE_Column = 'Prod_Group4' and ERP = @ERP)
		inner Join epacube.data_value dv_PG4 with (nolock) on PG4.data_value_fk = dv_PG4.data_value_id
		where pi.DATA_NAME_FK = (Select data_name_fk from dbo.MAUI_Config_ERP with (nolock) where usage = 'Data_Name' and epaCUBE_Column = 'Product_ID' and ERP = @ERP)
		group by PI.Value, dv_PG4.value
		
		Create Index idx_pva on ##Prod_Vend_Assoc(Product_ID, Sell_Line)

		Set @Join_Sell_Lines = 'inner join ##Prod_Vend_Assoc PVA on ST.Product_ID = PVA.Product_ID'	
	end
	else
	Begin
		Set @Fltr_Sell_Lines = ''
		Set @Join_Sell_Lines = ''
	End
		
	Set @Filter = @Fltr_Whse_To_Exclude + @Fltr_Sell_Lines + @Custom_Filter + @Fltr_Dates
		
	Set @SQLcd = '
		Insert into ##Sales_Transactions([ANALYSIS_EFFECTIVE_DATE], [ANALYSIS_PERIOD_START_DATE], [ANALYSIS_PERIOD_END_DATE], [SALES_ORDER_ENTRY_DATE], [SALES_ORDER_SHIP_DATE], [SALES_ORDER_NUMBER], [SALES_ORDER_SUFFIX], [SALES_ORDER_LINE], [ANALYSIS_DATA_SET_ABBREV], [MATRIX_UOM], [PRODUCT_ID], [SHIP_TO_CUSTOMER_ID], [BILL_TO_CUSTOMER_ID], [WHSE_ID], [WHSE_GROUP_ID], [MFR_ID], [VENDOR_ID], [SALES_TERRITORY_ID], [OUTSIDE_SALESREP_ID], [INSIDE_SALESREP_ID], [ORDER_TAKER_ID], [BUYER_ID], [SALES_QTY], [SALES_UOM], [SALES_PRIMARY_UOM_CONVERSION], [HISTORICAL_UOM], [HISTORICAL_PRIMARY_UOM_CONVERSION], [SPECIAL_UOM_CONVERSION], [CURRENCY], [SELL_PRICE_AMT], [SELL_PRICE_MATRIX_AMT], [SELL_PRICE_OVERRIDE_AMT], [ORDER_COGS_AMT], [ORDER_COGS_OVERRIDE_AMT], [REPL_COST_AMT], [MARGIN_COST_AMT], [REBATED_COST_CB_AMT], [REBATE_CB_AMT], [REBATE_BUY_AMT], [PRICE_MATRIX_ID_HISTORICAL], [PRICE_CONTRACT_NO_HISTORICAL], [PRICE_CONTRACT_REFERENCE_HISTORICAL], [PRICE_FORMULA_HISTORICAL], [PRICE_BASIS_COLMUN_HISTORICAL], [REBATE_CB_MATRIX_ID_HISTORICAL], [REBATE_CB_CONTRACT_NO_HISTORICAL], [REBATE_CB_CONTRACT_REFERENCE_HISTORICAL], [REBATE_CB_FORMULA_HISTORICAL], [REBATE_CB_BASIS_COLMUN_HISTORICAL], [REBATE_BUY_MATRIX_ID_HISTORICAL], [REBATE_BUY_CONTRACT_NO_HISTORICAL], [REBATE_BUY_CONTRACT_REFERENCE_HISTORICAL], [REBATE_BUY_FORMULA_HISTORICAL], [REBATE_BUY_BASIS_COLMUN_HISTORICAL], [JOB_FK], [IMPORT_FILENAME], [PRODUCT_STRUCTURE_FK], [ORG_ENTITY_STRUCTURE_FK], [CUST_ENTITY_STRUCTURE_FK], [ERROR_ON_IMPORT_IND])
		Select Distinct
		ST.[ANALYSIS_EFFECTIVE_DATE], ST.[ANALYSIS_PERIOD_START_DATE], ST.[ANALYSIS_PERIOD_END_DATE], ST.[SALES_ORDER_ENTRY_DATE], ST.[SALES_ORDER_SHIP_DATE], ST.[SALES_ORDER_NUMBER], ST.[SALES_ORDER_SUFFIX], ST.[SALES_ORDER_LINE], ST.[ANALYSIS_DATA_SET_ABBREV], ST.[MATRIX_UOM], ST.[PRODUCT_ID], ST.[SHIP_TO_CUSTOMER_ID], ST.[BILL_TO_CUSTOMER_ID], ST.[WHSE_ID], ST.[WHSE_GROUP_ID], ST.[MFR_ID], ST.[VENDOR_ID], ST.[SALES_TERRITORY_ID], ST.[OUTSIDE_SALESREP_ID], ST.[INSIDE_SALESREP_ID], ST.[ORDER_TAKER_ID], ST.[BUYER_ID], ST.[SALES_QTY], ST.[SALES_UOM], ST.[SALES_PRIMARY_UOM_CONVERSION], ST.[HISTORICAL_UOM], ST.[HISTORICAL_PRIMARY_UOM_CONVERSION], ST.[SPECIAL_UOM_CONVERSION], ST.[CURRENCY], ST.[SELL_PRICE_AMT], ST.[SELL_PRICE_MATRIX_AMT], ST.[SELL_PRICE_OVERRIDE_AMT], ST.[ORDER_COGS_AMT], ST.[ORDER_COGS_OVERRIDE_AMT], ST.[REPL_COST_AMT], ST.[MARGIN_COST_AMT], ST.[REBATED_COST_CB_AMT], ST.[REBATE_CB_AMT], ST.[REBATE_BUY_AMT], ST.[PRICE_MATRIX_ID_HISTORICAL], ST.[PRICE_CONTRACT_NO_HISTORICAL], ST.[PRICE_CONTRACT_REFERENCE_HISTORICAL], ST.[PRICE_FORMULA_HISTORICAL], ST.[PRICE_BASIS_COLMUN_HISTORICAL], ST.[REBATE_CB_MATRIX_ID_HISTORICAL], ST.[REBATE_CB_CONTRACT_NO_HISTORICAL], ST.[REBATE_CB_CONTRACT_REFERENCE_HISTORICAL], ST.[REBATE_CB_FORMULA_HISTORICAL], ST.[REBATE_CB_BASIS_COLMUN_HISTORICAL], ST.[REBATE_BUY_MATRIX_ID_HISTORICAL], ST.[REBATE_BUY_CONTRACT_NO_HISTORICAL], ST.[REBATE_BUY_CONTRACT_REFERENCE_HISTORICAL], ST.[REBATE_BUY_FORMULA_HISTORICAL], ST.[REBATE_BUY_BASIS_COLMUN_HISTORICAL], ST.[JOB_FK], ST.[IMPORT_FILENAME], ST.[PRODUCT_STRUCTURE_FK], ST.[ORG_ENTITY_STRUCTURE_FK], ST.[CUST_ENTITY_STRUCTURE_FK], ST.[ERROR_ON_IMPORT_IND]
		from import.IMPORT_SALES_HISTORY_ACTIVE ST with (nolock)
		inner join epacube.product_structure PS on ST.product_structure_fk = PS.product_structure_id
			' + @Join_CstChg + '
			' + @Join_Sell_Lines + ' 
			' + @Join_Exclusion + '
			' + @Join_Region + '
			' + @Join_Whses + '
		where 1=1
		--and EX.Cust_Type is null
		' + @Filter

	Exec (@SQLcd)

	IF object_id('tempdb..##Prod_Vend_Assoc') is not null
	drop table ##Prod_Vend_Assoc
	IF object_id('tempdb..##Cust_Exclusions') is not null
	drop table ##Cust_Exclusions;
	
insert into dbo.maui_log Select @Job_FK_i, 'Transactions being Anayzed', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), (Select COUNT(*) from ##Sales_Transactions)

-------------------------------------------------------------------------------------
--   INSERT ORDER DATA HISTORY PRODUCTS
------------------------------------------------------------------------------------

TRUNCATE TABLE  [marginmgr].[ORDER_DATA_HISTORY] 

INSERT INTO [marginmgr].[ORDER_DATA_HISTORY] (
            [DRANK]
         , [TRANSACTION_TYPE]
           ,[ORDER_DATE]
--           ,[ORDER_DATA_SET]
           ,[ORDER_TYPE_CR_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[CUST_ENTITY_STRUCTURE_FK]
--           ,[SUPL_ENTITY_STRUCTURE_FK]
--           ,[SALES_ENTITY_STRUCTURE_FK]
--           ,[TERR_ENTITY_STRUCTURE_FK]
--           ,[BUYER_ENTITY_STRUCTURE_FK]
           ,[MARGIN_DOLLARS_EXT]
           ,[MARGIN_DOLLARS_UNIT]
           ,[MARGIN_PERCENT]
           ,[SALES_DOLLARS_EXT]
           ,[SALES_QTY]
           ,[PRICE_UNIT]
           ,[COGS_UNIT]
           ,[REPL_COST_UNIT]
--           ,[MARKET_COST]
           ,[REBATE_AMT_SPA_Ext]
           ,[REBATE_AMT_SPA_Unit]
--           ,[REBATE_AMT_BUY]
           ,[REBATE_AMT_SELL_SIDE]
           ,[CURRENCY_CR_FK]
           ,[ORDER_NO]
           ,[ORDER_LINE]
           ,[ORDER_QTY]
           ,[SELL_UOM_CODE_FK]
--           ,[BUY_UOM_CODE_FK]
           ,[PRICE_OVERRIDE_IND]
--           ,[LINE_DISCOUNT]
--           ,[OTHER_DISCOUNT]
--           ,[SHIP_COST_ESTIMATE]
--           ,[SHIP_COST_INVOICED]
--           ,[SHIP_WEIGHT]
--           ,[SHIP_CUBE]
           ,[SHIP_DATE]
           ,[INVOICE_DATE]
--           ,[PRICE_CALC_RULE_TYPE_CR_FK]
           ,[PRICE_RULE_XREF]
--           ,[PRICE_REFERENCE]
--           ,[PRICE_CONTRACT_NO]
           ,[REBATE_SPA_RULE_XREF]
--           ,[REBATE_SPA_REFERENCE]
           ,[REBATE_SPA_CONTRACT_NO]
--           ,[REBATE_BUY_RULE_XREF]
--           ,[REBATE_BUY_REFERENCE]
--           ,[REBATE_BUY_CONTRACT_NO]
--           ,[REBATE_SELL_SIDE_RULE_XREF]
--           ,[REBATE_SELL_SIDE_REFERENCE]
--           ,[REBATE_SELL_SIDE_CONTRACT_NO]
           ,[PRODUCT_ID_HOST]
--           ,[ORG_ID_HOST]
--           ,[CUSTOMER_ID_HOST]
           ,[VENDOR_ID_HOST]
           ,[SALESPERSON_ID_OUTSIDE_HOST]
           ,[SALESPERSON_ID_INSIDE_HOST]
           ,[BUYER_HOST]
           , ORDER_TAKEN_BY
--           ,[PRODUCT_CATEGORY]
--           ,[PRODUCT_LINE]
--           ,[PRODUCT_PRICE_TYPE]
           ,[UOM_SELL_PRICE_CONV]
--           ,[UOM_SELL_PACK_CONV]
--           ,[UOM_BUY_CODE]
           ,[UOM_SELL_CODE]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_FILENAME]
--           ,[IMPORT_PACKAGE_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
--           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
           ,[UPDATE_LOG_FK]
)
   ( SELECT 
			DENSE_RANK() over (Partition By
			  SH.PRODUCT_STRUCTURE_FK
			, SH.ORG_ENTITY_STRUCTURE_FK
			, SH.CUST_ENTITY_STRUCTURE_FK
			  ORDER BY sh.SALES_ORDER_SHIP_DATE DESC, SH.SALES_ORDER_NUMBER DESC, SH.SALES_ORDER_SUFFIX Desc, SH.SALES_ORDER_LINE Desc, SH.RECORD_NO DESC 
-------- Order by Case Transaction_Type When 'Stock Order' then 1 When 'Drop Ship' then 2 When 'Return' then 3 else 4 end
--------,INVOICE_DATE Desc, Sales_Dollars_Ext Desc, Sales_Qty Desc, iod.order_no Desc, iod.Order_Line
) 
-----------
           ,sh.ANALYSIS_DATA_SET_ABBREV  --TRANSACTION_TYPE
           ,sh.SALES_ORDER_SHIP_DATE ---<ORDER_DATE, datetime,>
--           ,<ORDER_DATA_SET
           ,3101  -- sales history ORDER_TYPE_CR_FK
           ,SH.PRODUCT_STRUCTURE_FK
           ,SH.ORG_ENTITY_STRUCTURE_FK
           ,SH.CUST_ENTITY_STRUCTURE_FK
--           ,<SUPL_ENTITY_STRUCTURE_FK, bigint,>
--           ,<SALES_ENTITY_STRUCTURE_FK, bigint,>
--           ,<TERR_ENTITY_STRUCTURE_FK, bigint,>
--           ,<BUYER_ENTITY_STRUCTURE_FK, bigint,>
, (isnull(cast (sh.sell_price_amt as numeric (18,4)), 0) - isnull(cast(sh.order_cogs_amt as numeric (18,4)), 0)) * (isnull(sh.sales_qty, 0))-- Margin_Dollars_Ext
, isnull(cast(sh.sell_price_amt as numeric (18,4)), 0) - isnull(cast(sh.order_cogs_amt as numeric (18,4)), 0) -- Margin_Dollars_Unit
, Case When isnull(cast (sh.sell_price_amt as numeric (18,4)), 0) > 0 then 
	(isnull(cast (sh.sell_price_amt as numeric (18,4)), 0) - isnull(cast(sh.order_cogs_amt as numeric (18,4)), 0) + 0) 
	/cast (sh.sell_price_amt as numeric (18,4)) else 0 end   -- MARGIN_PERCENT
           ,Cast(sh.sell_price_amt as Numeric(18, 4)) * Cast(sh.sales_qty as Numeric(18, 4)) --SALES_DOLLARS_EXT
           ,sh.sales_qty --SALES_QTY
           ,Cast(sh.sell_price_amt as numeric(18, 4)) --PRICE_UNIT
           ,Cast(sh.order_cogs_amt as numeric(18, 4))  --COGS_UNIT
           ,Cast(sh.REPL_COST_AMT as numeric(18, 4)) ---REPL_COST_UNIT
--           ,<MARKET_COST
			, isnull((Cast(sh.REPL_COST_AMT as numeric(18, 4)) - Cast(sh.order_cogs_amt as numeric(18, 4)) ) * sh.SALES_QTY, 0) REBATE_AMT_SPA_Ext
			, isnull((Cast(sh.REPL_COST_AMT as numeric(18, 4)) - Cast(sh.order_cogs_amt as numeric(18, 4)) ) , 0) REBATE_AMT_SPA_Unit
--            replacement cost - the rebated cost  <REBATE_AMT_SPA_Unit, numeric(18,6),>
--           ,<REBATE_AMT_BUY, numeric(18,6),>
           ,Cast(sh.sell_price_override_amt as numeric(18, 4))  --<REBATE_AMT_SELL_SIDE, numeric(18,6),>
           ,1200  --usd CURRENCY_CR_FK
           ,sh.SALES_ORDER_NUMBER + sh.SALES_ORDER_SUFFIX  --ORDER_NO
           ,sh.SALES_ORDER_LINE  --order generation?  ORDER_LINE
           ,sh.sales_qty  --ORDER_QTY
           ,(select uom_code_id from epacube.UOM_CODE where uom_code = sh.SALES_UOM) --SELL_UOM_CODE_FK
--           ,<BUY_UOM_CODE_FK, int,>
           ,(select CASE when ISNULL(Cast (sh.sell_price_override_amt as numeric(18,4)) ,0) <> 0 then 1 else 0 end ) --PRICE_OVERRIDE_IND
--           ,<LINE_DISCOUNT, numeric(18,6),>
--           ,<OTHER_DISCOUNT, numeric(18,6),>
--           ,<SHIP_COST_ESTIMATE, numeric(18,6),>
--           ,<SHIP_COST_INVOICED, numeric(18,6),>
--           ,<SHIP_WEIGHT, numeric(18,6),>
--           ,<SHIP_CUBE, numeric(18,6),>
           ,sh.SALES_ORDER_SHIP_DATE --SHIP_DATE
           ,sh.SALES_ORDER_SHIP_DATE --INVOICE_DATE, datetime,>
--           ,sh.SALES_ORDER_ENTRY_DATE --INVOICE_DATE, datetime,>
--           ,<PRICE_CALC_RULE_TYPE_CR_FK, int,>
           ,sh.PRICE_MATRIX_ID_HISTORICAL  --PRICE_RULE_XREF
--           ,<PRICE_REFERENCE, varchar(32),>
--           ,<PRICE_CONTRACT_NO, varchar(50),>
           ,sh.Rebate_CB_Matrix_ID_Historical --rebate_CB_Rule_XRef
--           ,<REBATE_SPA_REFERENCE, varchar(32),>
           ,sh.Rebate_CB_Contract_No_Historical --REBATE_SPA_CONTRACT_NO, varchar(32),>
--           ,<REBATE_BUY_RULE_XREF, varchar(32),>
--           ,<REBATE_BUY_REFERENCE, varchar(32),>
--           ,<REBATE_BUY_CONTRACT_NO, varchar(32),>
--           ,<REBATE_SELL_SIDE_RULE_XREF, varchar(32),>
--           ,<REBATE_SELL_SIDE_REFERENCE, varchar(32),>
--           ,<REBATE_SELL_SIDE_CONTRACT_NO, varchar(32),>
           ,sh.PRODUCT_ID --<PRODUCT_ID_HOST
--           ,<ORG_ID_HOST, varchar(64),>
--           ,<CUSTOMER_ID_HOST, varchar(64),>
           ,sh.VENDOR_ID  --VENDOR_ID_HOST, varchar(64),>
           ,sh.OUTSIDE_SALESREP_ID --SALESPERSON_ID_OUTSIDE_HOST, varchar(64),>
           ,sh.INSIDE_SALESREP_ID --SALESPERSON_ID_INSIDE_HOST, varchar(64),>
           ,sh.BUYER_ID  --<BUYER_HOST, varchar(64),>
           ,sh.ORDER_TAKER_ID --<Order_Taken_By>
--           ,<PRODUCT_CATEGORY, varchar(50),>
--           ,<PRODUCT_LINE, varchar(50),>
--           ,<PRODUCT_PRICE_TYPE, varchar(50),>
           ,sh.SALES_PRIMARY_UOM_CONVERSION --<UOM_SELL_PRICE_CONV, numeric(18,6),>
--           ,<UOM_SELL_PACK_CONV, numeric(18,6),>
--           ,<UOM_BUY_CODE, varchar(50),>
           ,(select uom_code_id from epacube.UOM_CODE where uom_code = sh.SALES_UOM)  --UOM_SELL_CODE, varchar(50),>
           ,SH.JOB_FK
           ,SH.IMPORT_FILENAME
--           ,<IMPORT_PACKAGE_FK, int,>
           ,1
           ,getdate()
--           ,<UPDATE_TIMESTAMP, datetime,>
           ,'IMPORT'  --UPDATE_USER
           ,sh.record_no --UPDATE_LOG_FK
           
           FROM ##Sales_Transactions SH
           WHERE 1 = 1
           and CUST_ENTITY_STRUCTURE_FK in (select entity_structure_id from epacube.ENTITY_STRUCTURE where DATA_NAME_FK in (144010, 144020))
           )

insert into dbo.maui_log Select @Job_FK_i, 'ODH Records with DRank of 1', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), (Select COUNT(*) from marginmgr.ORDER_DATA_HISTORY where DRANK = 1)
insert into dbo.maui_log Select @Job_FK_i, 'ODH Line Items with Duplicates', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), (
	Select COUNT(*) from (
	Select product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk, COUNT(*) records from marginmgr.ORDER_DATA_HISTORY
	where DRANK = 1
	group by product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk having COUNT(*) > 1) A )

IF object_id('tempdb..##Sales_Transactions') is not null
drop table ##Sales_Transactions

IF object_id('tempdb..##CntrctEx') is not null
drop table ##CntrctEx

IF object_id('tempdb..##whse_reg') is not null
drop table ##whse_reg

IF object_id('tempdb..##whses') is not null
drop table ##whses

END
