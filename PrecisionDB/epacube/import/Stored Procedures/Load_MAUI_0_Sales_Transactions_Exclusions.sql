﻿--A_epaMAUI_Load_0_Sales_Transactions_Exclusions

-- =============================================
-- Author:		<Gary Stone>
-- Create date: <October 22, 2009>
-- Description:	<Identify Global Product and Customer Excusions for Sales Transactions>
-- =============================================
CREATE PROCEDURE [import].[Load_MAUI_0_Sales_Transactions_Exclusions] @ActivityType Varchar(32)


AS
BEGIN

	SET NOCOUNT ON;
	
--Declare @ActivityType Varchar(32) = 'GLOBAL ASSESSMENT'

IF object_id('tempdb..##excl_ProdsCrit') is not null
drop table ##excl_ProdsCrit

IF object_id('tempdb..##Prod_Excl_Global') is not null
drop table ##Prod_Excl_Global

IF object_id('tempdb..##TransGroup_Excl_Global') is not null
Drop table ##TransGroup_Excl_Global

IF object_id('tempdb..##excl_CustsCrit') is not null
drop table ##excl_CustsCrit

IF object_id('tempdb..##Cust_Excl_Global') is not null
drop table ##Cust_Excl_Global

Declare @ERP Varchar(32)
Declare @Prod_ID_DN_FK Bigint

Declare @ActivityTypeP Varchar(64)
Declare @ActivityTypeC Varchar(64)

Set @ERP = (Select Value from epacube.epacube_params with (nolock) where name = 'ERP HOST')
Set @Prod_ID_DN_FK = (Select data_name_fk from epacube.dbo.MAUI_Config_ERP with (nolock) where usage = 'Data_Name' and epaCUBE_Column in ('Product_ID', 'Product ID') and ERP = @ERP)


Set @ActivityTypeP = @ActivityType + + ' PRODUCT EXCLUSION'
Set @ActivityTypeC = @ActivityType + + ' CUSTOMER EXCLUSION'

Update CPLC
Set Identifier_FK = dv.data_value_id
from CPL.aCPL_Criteria cplc
inner join epacube.epacube.DATA_VALUE dv with (nolock) on dv.DATA_NAME_FK = ISNULL(cplc.entity_data_name_fk, cplc.data_name_fk) and dv.VALUE = cplc.criteria_value
where cplc.Entity_Class_CR_FK = 10109 and cplc.DATA_NAME not in ('Product ID', 'Product_ID', 'Transaction Group', 'Contains Product ID')

Update CPLC
Set Identifier_FK = pi.product_structure_fk
from CPL.aCPL_Criteria cplc
inner join epacube.epacube.product_identification pi with (nolock) on PI.DATA_NAME_FK = ISNULL(cplc.entity_data_name_fk, cplc.data_name_fk) and pi.VALUE = cplc.criteria_value
where cplc.Entity_Class_CR_FK = 10109 and cplc.DATA_NAME in ('Product ID', 'Product_ID') 

---Identify Global Product Exclusions
--Get Column Definitions
select dn.[name], dn.data_name_id, ds.table_name, ds.column_name, cplc.identifier_fk, cplc.criteria_value, ds.corp_ind, ds.org_ind, cplc.Activity
into ##excl_ProdsCrit
from CPL.aCPL_Criteria cplc with (nolock)
	inner join epacube.epacube.data_name dn with (nolock) on isnull(cplc.Entity_Data_Name_FK, cplc.data_name_fk) = dn.DATA_NAME_ID
	inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id
	where cplc.Entity_Class_CR_FK = 10109
group by dn.[name], dn.data_name_id, ds.table_name, ds.column_name, cplc.identifier_fk, cplc.criteria_value, ds.corp_ind, ds.org_ind, cplc.Activity

----Identify Product Exclusions
Select * 
into ##Prod_Excl_Global 
from (
----Get Product Structures from Product Categories
Select pc.product_structure_fk from epacube.epacube.product_category pc with (nolock)
inner join ##excl_ProdsCrit pCrit on pc.data_name_fk = pCrit.data_name_id and pc.DATA_VALUE_FK = pCrit.Identifier_FK
Where 1 = 1
		and pcrit.Activity = @ActivityTypeP 
		and pCrit.table_name = 'PRODUCT_CATEGORY'
group by product_structure_fk
union
--Get Product ID's
Select identifier_fk 
from ##excl_ProdsCrit pCrit
Where 1 = 1
		and TABLE_NAME = 'Product_Identification'
		and DATA_NAME_ID = @Prod_ID_DN_FK
		and Activity = @ActivityTypeP
group by identifier_fk
Union
----Get Descriptions Containing...
Select product_structure_fk from epacube.epacube.product_description pd with (nolock) 
inner join CPL.aCPL_Criteria cplc with (nolock) on pd.[description] like '%' + cplc.Criteria_Value + '%'
where 1 = 1
		and cplc.Entity_Class_CR_FK = 10109 
		and cplc.Data_Name = 'DESCRIPTION CONTAINS'
		and cplc.Activity = @ActivityTypeP
group by product_structure_fk
Union
----Get Product ID's Containing...
Select product_structure_fk from epacube.epacube.product_identification pi with (nolock) 
inner join CPL.aCPL_Criteria cplc with (nolock) on pi.[value] like '%' + cplc.Criteria_Value + '%'
where 1 = 1
		and cplc.Entity_Class_CR_FK = 10109 
		and cplc.Data_Name = 'PRODUCT ID CONTAINS'
		and cplc.Activity = @ActivityTypeP
		and pi.DATA_NAME_FK = @Prod_ID_DN_FK
group by product_structure_fk
) A

Create Index idx_prod_excl on ##Prod_Excl_Global(Product_Structure_FK)

--Identify Transaction Group Exclusions
Select Criteria_Value Transaction_Group 
into ##TransGroup_Excl_Global 
from CPL.aCPL_Criteria cplc with (nolock) where Entity_Class_CR_FK = 10109 and Data_Name = 'Transaction Group'
Group by Criteria_Value

Create index idx_tg on ##TransGroup_Excl_Global(Transaction_Group)

-------------------------------
--***///END OF PRODUCTS
-------------------------------

---Identify Global Customer Exclusions
Update CPLC
Set Identifier_FK = dv.DATA_VALUE_ID
from CPL.aCPL_Criteria cplc
inner join epacube.epacube.DATA_VALUE dv with (nolock) on dv.DATA_NAME_FK = ISNULL(cplc.entity_data_name_fk, cplc.data_name_fk) and dv.VALUE = cplc.Criteria_Value
Where 1 = 1
	and entity_class_cr_fk = 10104
	and cplc.Activity = @ActivityTypeC

Update CPLC
Set Identifier_FK = ei.entity_structure_fk
from CPL.aCPL_Criteria cplc
inner join epacube.epacube.entity_identification ei with (nolock) on ei.DATA_NAME_FK = cplc.entity_data_name_fk and ei.VALUE = cplc.Criteria_Value
Where 1 = 1
	and entity_class_cr_fk = 10104
	and cplc.Activity = @ActivityTypeC


--Get Column Definitions
select dn.[name], dn.data_name_id, ds.table_name, ds.column_name, cplc.identifier_fk, cplc.criteria_value, ds.corp_ind, ds.org_ind, cplc.Activity
into ##excl_CustsCrit
from CPL.aCPL_Criteria cplc with (nolock)
	inner join epacube.epacube.data_name dn with (nolock) on isnull(cplc.Entity_Data_Name_FK, cplc.data_name_fk) = dn.DATA_NAME_ID
	inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id
	where cplc.Entity_Class_CR_FK = 10104
	and cplc.Activity = @ActivityTypeC
group by dn.[name], dn.data_name_id, ds.table_name, ds.column_name, cplc.identifier_fk, cplc.criteria_value, ds.corp_ind, ds.org_ind, cplc.Activity


--Get Customer Mult Types
Select * 
into ##Cust_Excl_Global 
from (
----Get Entity_structure_fk values by matching Customer Price Type / Customer Rebate Type
Select emt.entity_structure_fk from epacube.entity_mult_type emt with (nolock)
inner join ##excl_CustsCrit cCrit on emt.DATA_NAME_FK = cCrit.data_name_id and emt.DATA_VALUE_FK = cCrit.identifier_fk
where 1 = 1
	and cCrit.table_name = 'ENTITY_MULT_TYPE'
group by entity_structure_fk
Union
----Get entity_Structure_Values Specified
Select identifier_fk from ##excl_CustsCrit cCrit
where 1 = 1
	and cCrit.table_name = 'ENTITY_STRUCTURE'
	and cCrit.DATA_NAME_ID in (144010, 144020)
group by identifier_fk
--Union
----Get Child Cust_Entity_Structure values from parents specified
--Select entity_structure_id from epacube.ENTITY_Structure es with (nolock)
--inner join ##excl_CustsCrit cCrit on es.DATA_NAME_FK = 144020 and cCrit.DATA_NAME_ID = 144010 and es.PARENT_ENTITY_STRUCTURE_FK = cCrit.Identifier_FK
--where 1 = 1
--	--and cCrit.Activity = @ActivityTypeC
--	and cCrit.table_name = 'ENTITY_STRUCTURE'
--group by entity_structure_id
Union
--Get Descriptions Containing...
Select entity_structure_fk from epacube.entity_identification ei
	inner join CPL.aCPL_Criteria cplc  on ei.entity_data_name_fk = 144010
	where cplc.data_name = 'BILL-TO ID CONTAINS'
		and cplc.Activity = @ActivityTypeC
		and ei.value like '%' + cplc.criteria_value + '%'
group by entity_structure_fk
Union
Select entity_structure_fk from epacube.entity_identification ei
	inner join CPL.aCPL_Criteria cplc  on ei.entity_data_name_fk = 144020
	where cplc.data_name = 'SHIP-TO ID CONTAINS'
		and cplc.Activity = @ActivityTypeC
		and ei.value like '%' + cplc.criteria_value + '%'
group by entity_structure_fk
Union
Select entity_structure_fk from epacube.entity_ident_nonunique ein
	inner join CPL.aCPL_Criteria cplc  on ein.entity_data_name_fk = 144010
		and cplc.data_name = 'BILL-TO NAME CONTAINS'
		and cplc.Activity = @ActivityTypeC
		and ein.value like '%' + cplc.criteria_value + '%'
group by entity_structure_fk
Union
Select entity_structure_fk from epacube.entity_ident_nonunique ein
	inner join CPL.aCPL_Criteria cplc  on ein.entity_data_name_fk = 144020
		and cplc.data_name = 'SHIP-TO NAME CONTAINS'
		and cplc.Activity = @ActivityTypeC
		and ein.value like '%' + cplc.criteria_value + '%'
group by entity_structure_fk
) A

Create Index idx_cust_excl on ##Cust_Excl_Global(Entity_Structure_FK)

Insert into ##Cust_Excl_Global
Select es.ENTITY_STRUCTURE_ID 
from ##Cust_Excl_Global cCrit
inner join epacube.epacube.ENTITY_STRUCTURE es on cCrit.ENTITY_STRUCTURE_FK = es.PARENT_ENTITY_STRUCTURE_FK
left join ##Cust_Excl_Global cCritChild on es.ENTITY_STRUCTURE_ID = cCritChild.ENTITY_STRUCTURE_FK
where es.DATA_NAME_FK = 144020
and cCritChild.ENTITY_STRUCTURE_FK is null

End
