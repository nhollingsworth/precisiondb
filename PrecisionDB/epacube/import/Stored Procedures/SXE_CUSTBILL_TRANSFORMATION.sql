﻿--A_epaMAUI_SXE_CUSTBILL_TRANSFORMATION

CREATE PROCEDURE [import].[SXE_CUSTBILL_TRANSFORMATION] ( @IN_JOB_FK bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to first create the Customer Bill to
Then need to run thru and create the customer ship to.
two differnt packages and two different job numbers...
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure 
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--     
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV                     Initial Version 
-- CV        01/17/2013   Added this modification history.  Tightened up deletes from event data
-- CV        05/02/2013   Tightened up delete from nonunique table.
-- GHS		 04/30/2014	  Added Johnstone customization as a callable query - no longer hard-coded in this procedure.
-- GHS		 05/29/2014	  Added code to insure any missing customer price/rebate types get added; count shows up in Table Import
-- GHS		 10/16/2014	  Added initial Custom Code to deal with Johnstone ship-to Variations
-- GHS		 9/19/2015	  Streamlined exceptions in beginning to truly capture differences only
-- GHS		 6/10/2015	  Accounted for new Precedence column in epacube.entity_mult_type, -historical tables.

	--Declare @IN_JOB_FK bigint
	--Set @IN_JOB_FK = 35494

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int 
	DECLARE @v_import_package_fk   int  
	DECLARE @v_column_name   varchar(25) 
	DECLARE @v_primary   varchar(25)   
	DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
	 DECLARE @V_RULES_FILTER_LOOKUP  varchar(1)
	 DECLARE @V_SXE_MULTIPLE_CPT varchar(1)
	 DECLARE @V_SXE_MULTIPLE_CRT varchar(1)
     	
SET NOCOUNT ON;

If (select count(*) from import.import_sxe_customers) = 0
goto EndOfProc

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.SXE_CUSTOMER_TRANSFORMATION', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps

IF object_id('tempdb..##CustBT') is not null
Drop Table ##CustBT

IF object_id('tempdb..##CustBT1') is not null
Drop Table ##CustBT1

IF object_id('tempdb..##CustST') is not null
Drop Table ##CustST

IF object_id('import.import_sxe_customers_Ship_To') is not null
Drop Table import.import_sxe_customers_Ship_To

Declare @InactiveShipTo int
Set @InactiveShipTo = isnull((Select Value from epacube.epacube.epacube_params where EPACUBE_PARAMS_ID = 113200), 0)

Declare @CustomCode_First_Step Varchar(Max)
Declare @CustomCode_Last_Step Varchar(Max)

 ----****Custom Code Start**** -- Ship_to_Adjustments Step 1

Set @CustomCode_First_Step = 
	(select Top 1 SQL_cd from dbo.MAUI_Config_ERP 
	where 1 = 1
	and ERP = (select value from epacube.epacube_params where APPLICATION_SCOPE_FK = 100 and [NAME] = 'ERP HOST')
	and usage = 'Customer_Customization'
	and [Table] = 'import.import_sxe_customers'
	AND [Temp_Table] = 'Ship_to_Adjustments_Step1'
	and epacube_customer = (select value from epacube.epacube_params where APPLICATION_SCOPE_FK = 100 and [NAME] = 'EPACUBE CUSTOMER'))

Exec(@CustomCode_First_Step)

----****Custom Code End****
	
Select C1.*
into ##CustBT1
from IMPORT.import_sxe_customers C1
	left join IMPORT.import_sxe_customers_last_import C2 on c1.CUST_NO = c2.CUST_NO and isnull(c1.SHIP_TO, '') = isnull(c2.SHIP_TO, '')
	left join epacube.ENTITY_IDENTIFICATION ei on ei.entity_data_name_fk = 144010 and ei.value = C1.Cust_NO
	--left join epacube.entity_mult_type emt_p on ei.entity_structure_fk = emt_p.entity_structure_fk and emt_p.data_name_fk = 244900
	--left join epacube.data_value dv_p on dv_p.data_value_id = emt_p.data_value_fk and dv_p.data_name_fk = emt_p.data_name_fk --and dv_p.VALUE = C1.CUST_PRICE_TYPE
	--left join epacube.entity_mult_type emt_r on ei.entity_structure_fk = emt_r.entity_structure_fk and emt_r.data_name_fk = 244901
	--left join epacube.data_value dv_r on dv_r.data_value_id = emt_r.data_value_fk and dv_r.data_name_fk = emt_r.data_name_fk --and dv_r.VALUE = c1.REBATE_TYPE
	left join epacube.entity_category ec with (nolock) on ec.data_name_fk = 244907 and ei.ENTITY_STRUCTURE_FK = ec.ENTITY_STRUCTURE_FK
	left join epacube.data_value dv_ct with (nolock) on dv_ct.data_name_fk  = 244907 and ec.data_value_fk = dv_ct.data_value_id
	where 1 = 1
	and isnull(c1.[Ship_to], '') = ''
	and (c1.[status] like 'A%' or isnull(c1.[status], 'A') <> isnull(c2.[status], 'Y') or c1.[status] like '%1%')
	and 
	(
	(c2.cust_name is null and c1.cust_name is not null)
	or
	ei.value is null
	--or
	--((dv_p.value is null and c1.CUST_PRICE_TYPE is not null and isnull(c1.CUST_PRICE_TYPE, '') <> isnull(c2.CUST_PRICE_TYPE, '')) or (dv_p.value is not null and c1.CUST_PRICE_TYPE is null))
	--or isnull(dv_p.VALUE, '') not like '%' + isnull(c1.cust_price_type, '') + '%'
	--or
	--((dv_r.value is null and c1.REBATE_TYPE IS not null and isnull(c1.REBATE_TYPE, '') <> isnull(c2.REBATE_TYPE, '')) or (dv_r.value is not null and c1.REBATE_TYPE is null))
	--or isnull(dv_r.VALUE, '') not like '%' + isnull(c1.rebate_type, '') + '%'

	or
	((((dv_ct.value is null and c1.[CUST_TYPE] is not null and isnull(c1.[CUST_TYPE], '') <> isnull(c2.[CUST_TYPE], '')) or (dv_ct.value is not null and c1.[CUST_TYPE] is null))
	or isnull(dv_ct.VALUE, '') not like '%' + isnull(c1.[CUST_TYPE], '') + '%') and (select RECORD_STATUS_CR_FK from epacube.DATA_NAME where DATA_NAME_ID = 244907) = 1)

	or
	c1.[CUST_NO] + c1.[CITY] + c1.[CLASS] + c1.[COUNTRY] + c1.[CUST_TYPE] + c1.[DISC_CODE] + c1.[JOB_DESC] + c1.[CUST_NAME] + c1.[PRICE_CUST_NO] + c1.[PRICE_CODE] + c1.[ROUTE] + c1.[SALESREP_OUT] + c1.[SALESREP_IN] + c1.[TERRITORY] + c1.[SHIP_TO] + c1.[SIC_CODE1] + c1.[SIC_CODE2] + c1.[SIC_CODE3] + c1.[STATE] + c1.[USER1] + c1.[USER10] + c1.[USER11] + c1.[USER12] + c1.[USER13] + c1.[USER14] + c1.[USER15] + c1.[USER16] + c1.[USER17] + c1.[USER18] + c1.[USER19] + c1.[USER2] + c1.[USER20] + c1.[USER21] + c1.[USER22] + c1.[USER23] + c1.[USER24] + c1.[USER3] + c1.[USER4] + c1.[USER5] + c1.[USER6] + c1.[USER7] + c1.[USER8] + c1.[USER9] + c1.[WAREHOUSE_ID] + c1.[ZIP_CODE] + c1.[CUSTOM1] + c1.[CUSTOM2] + c1.[CUSTOM3] + c1.[CUSTOM4] + c1.[CUSTOM5] + c1.[CUSTOM6] + c1.[CUSTOM7] + c1.[CUSTOM8] + c1.[CUSTOM9] + c1.[CUSTOM10] + c1.[DSO] + c1.[STATUS]
<>
	C2.[CUST_NO] + C2.[CITY] + C2.[CLASS] + C2.[COUNTRY] + C2.[CUST_TYPE] + C2.[DISC_CODE] + C2.[JOB_DESC] + C2.[CUST_NAME] + C2.[PRICE_CUST_NO] + C2.[PRICE_CODE] + C2.[ROUTE] + C2.[SALESREP_OUT] + C2.[SALESREP_IN] + C2.[TERRITORY] + C2.[SHIP_TO] + C2.[SIC_CODE1] + C2.[SIC_CODE2] + C2.[SIC_CODE3] + C2.[STATE] + C2.[USER1] + C2.[USER10] + C2.[USER11] + C2.[USER12] + C2.[USER13] + C2.[USER14] + C2.[USER15] + C2.[USER16] + C2.[USER17] + C2.[USER18] + C2.[USER19] + C2.[USER2] + C2.[USER20] + C2.[USER21] + C2.[USER22] + C2.[USER23] + C2.[USER24] + C2.[USER3] + C2.[USER4] + C2.[USER5] + C2.[USER6] + C2.[USER7] + C2.[USER8] + C2.[USER9] + C2.[WAREHOUSE_ID] + C2.[ZIP_CODE] + C2.[CUSTOM1] + C2.[CUSTOM2] + C2.[CUSTOM3] + C2.[CUSTOM4] + C2.[CUSTOM5] + C2.[CUSTOM6] + C2.[CUSTOM7] + C2.[CUSTOM8] + C2.[CUSTOM9] + C2.[CUSTOM10] + C2.[DSO] + C2.[STATUS]
	)

select distinct * into ##CustBT from ##CustBT1

Create Index idx_custBT on ##CustBT([Cust_NO], [Ship_To], [Status], [Job_FK])

Drop table ##CustBT1

Select C1.*
into ##CustST
from IMPORT.import_sxe_customers C1
	left join IMPORT.import_sxe_customers_last_import C2 on c1.CUST_NO = c2.CUST_NO and isnull(c1.SHIP_TO, '') = isnull(c2.SHIP_TO, '')
	left join epacube.ENTITY_IDENTIFICATION ei on ei.entity_data_name_fk = 144020 and ei.value = C1.Cust_NO + '-' + C1.Ship_To
	--left join epacube.entity_mult_type emt_p on ei.entity_structure_fk = emt_p.entity_structure_fk and emt_p.data_name_fk = 244900
	--left join epacube.data_value dv_p on dv_p.data_value_id = emt_p.data_value_fk and dv_p.data_name_fk = emt_p.data_name_fk --and dv_p.VALUE = C1.CUST_PRICE_TYPE
	--left join epacube.entity_mult_type emt_r on ei.entity_structure_fk = emt_r.entity_structure_fk and emt_r.data_name_fk = 244901
	--left join epacube.data_value dv_r on dv_r.data_value_id = emt_r.data_value_fk and dv_r.data_name_fk = emt_r.data_name_fk --and dv_r.VALUE = c1.REBATE_TYPE
	left join epacube.entity_category ec with (nolock) on ec.data_name_fk = 244907 and ei.ENTITY_STRUCTURE_FK = ec.ENTITY_STRUCTURE_FK
	left join epacube.data_value dv_ct with (nolock) on dv_ct.data_name_fk  = 244907 and ec.data_value_fk = dv_ct.data_value_id
	where (1 = 1
	and isnull(c1.[Ship_to], '') <> ''
	and 
		(c1.[status] like 'A%' or isnull(c1.[status], 'A') <> isnull(c2.[status], 'Y') or c1.[status] like '%1%'
		--or c1.cust_no in (Select cust_no from ##CustBT)
		)
	and 
	(
	(c2.cust_name is null and c1.cust_name is not null)
	or
	ei.value is null
	or
	--((dv_p.value is null and c1.CUST_PRICE_TYPE is not null and isnull(c1.CUST_PRICE_TYPE, '') <> isnull(c2.CUST_PRICE_TYPE, '')) or (dv_p.value is not null and c1.CUST_PRICE_TYPE is null))
	--or isnull(dv_p.VALUE, '') not like '%' + isnull(c1.cust_price_type, '') + '%'
	--or
	--((dv_r.value is null and c1.REBATE_TYPE IS not null and isnull(c1.REBATE_TYPE, '') <> isnull(c2.REBATE_TYPE, '')) or (dv_r.value is not null and c1.REBATE_TYPE is null))
	--or isnull(dv_r.VALUE, '') not like '%' + isnull(c1.rebate_type, '') + '%'

	((((dv_ct.value is null and c1.[CUST_TYPE] is not null and isnull(c1.[CUST_TYPE], '') <> isnull(c2.[CUST_TYPE], '')) or (dv_ct.value is not null and c1.[CUST_TYPE] is null))
	or isnull(dv_ct.VALUE, '') not like '%' + isnull(c1.[CUST_TYPE], '') + '%') and (select RECORD_STATUS_CR_FK from epacube.DATA_NAME where DATA_NAME_ID = 244907) = 1)

	or
	c1.[CUST_NO] + c1.[CITY] + c1.[CLASS] + c1.[COUNTRY] + c1.[CUST_TYPE] + c1.[DISC_CODE] + c1.[JOB_DESC] + c1.[CUST_NAME] + c1.[PRICE_CUST_NO] + c1.[PRICE_CODE] + c1.[ROUTE] + c1.[SALESREP_OUT] + c1.[SALESREP_IN] + c1.[TERRITORY] + c1.[SHIP_TO] + c1.[SIC_CODE1] + c1.[SIC_CODE2] + c1.[SIC_CODE3] + c1.[STATE] + c1.[USER1] + c1.[USER10] + c1.[USER11] + c1.[USER12] + c1.[USER13] + c1.[USER14] + c1.[USER15] + c1.[USER16] + c1.[USER17] + c1.[USER18] + c1.[USER19] + c1.[USER2] + c1.[USER20] + c1.[USER21] + c1.[USER22] + c1.[USER23] + c1.[USER24] + c1.[USER3] + c1.[USER4] + c1.[USER5] + c1.[USER6] + c1.[USER7] + c1.[USER8] + c1.[USER9] + c1.[WAREHOUSE_ID] + c1.[ZIP_CODE] + c1.[CUSTOM1] + c1.[CUSTOM2] + c1.[CUSTOM3] + c1.[CUSTOM4] + c1.[CUSTOM5] + c1.[CUSTOM6] + c1.[CUSTOM7] + c1.[CUSTOM8] + c1.[CUSTOM9] + c1.[CUSTOM10] + c1.[DSO] + c1.[STATUS]
<>
	C2.[CUST_NO] + C2.[CITY] + C2.[CLASS] + C2.[COUNTRY] + C2.[CUST_TYPE] + C2.[DISC_CODE] + C2.[JOB_DESC] + C2.[CUST_NAME] + C2.[PRICE_CUST_NO] + C2.[PRICE_CODE] + C2.[ROUTE] + C2.[SALESREP_OUT] + C2.[SALESREP_IN] + C2.[TERRITORY] + C2.[SHIP_TO] + C2.[SIC_CODE1] + C2.[SIC_CODE2] + C2.[SIC_CODE3] + C2.[STATE] + C2.[USER1] + C2.[USER10] + C2.[USER11] + C2.[USER12] + C2.[USER13] + C2.[USER14] + C2.[USER15] + C2.[USER16] + C2.[USER17] + C2.[USER18] + C2.[USER19] + C2.[USER2] + C2.[USER20] + C2.[USER21] + C2.[USER22] + C2.[USER23] + C2.[USER24] + C2.[USER3] + C2.[USER4] + C2.[USER5] + C2.[USER6] + C2.[USER7] + C2.[USER8] + C2.[USER9] + C2.[WAREHOUSE_ID] + C2.[ZIP_CODE] + C2.[CUSTOM1] + C2.[CUSTOM2] + C2.[CUSTOM3] + C2.[CUSTOM4] + C2.[CUSTOM5] + C2.[CUSTOM6] + C2.[CUSTOM7] + C2.[CUSTOM8] + C2.[CUSTOM9] + C2.[CUSTOM10] + C2.[DSO] + C2.[STATUS]
	)
	) or (@InactiveShipTo = 1 and c1.cust_no in (Select cust_no from ##CustBT Union Select value from epacube.entity_identification where entity_data_name_fk = 144010 and record_status_cr_fk = 1) and isnull(c1.[Ship_to], '') <> '')

IF (SELECT VALUE FROM EPACUBE.EPACUBE_PARAMS WHERE NAME = 'EPACUBE CUSTOMER') = 'Johnstone'
	Delete C1 from ##CustST C1 Where isnull(c1.SHIP_TO, '') <> '' and C1.cust_no in (select value from epacube.entity_identification where entity_data_name_fk in (144020))

Select distinct * into import.import_sxe_customers_Ship_To from ##CustST

Create Index idx_custST on import.import_sxe_customers_Ship_To([Cust_NO], [Ship_To], [Status], [Job_FK])

IF object_id('tempdb..##CustST') is not null
Drop Table ##CustST

IF (Select COUNT(*) from import.import_sxe_customers where STATUS like 'A%') > (select value from epacube.epacube_params where NAME = 'MIN RECORDS EXPECTED CUSTOMERS')
Begin
-------------------------------------------------------------------------------
--Clear out old Customer Data no longer in use - added by GHS 8/18/2011
-------------------------------------------------------------------------------
 
	IF object_id('tempdb..##shipto') is not null
	Drop Table ##shipto
	IF object_id('tempdb..##cust_no') is not null
	Drop Table ##cust_no
	IF object_id('tempdb..##tmpCusts') is not null
	Drop Table  ##tmpCusts
	
	Select [Cust_no] 'Custno' into ##cust_no from import.IMPORT_SXE_CUSTOMERS where isnull(cust_no, '') > '' and [STATUS] like '%A%' group by [Cust_no]
	Select isnull([Cust_no], '') + '-' + isnull(ship_to, '') 'Shipto' into ##shipto from import.IMPORT_SXE_CUSTOMERS where isnull(ship_to, '') > '' 
		and 
			([STATUS] like '%A%'
				OR
				isnull([Cust_no], '') + '-' + isnull(ship_to, '') in
				(
					select st.cust_no + '-' + st.SHIP_TO Cust_ID_ST from import.import_sxe_customers ST with (nolock)
					inner join import.import_sxe_customers BT with (nolock) on ST.CUST_NO = BT.CUST_NO and st.SHIP_TO is not null and bt.SHIP_TO is null
					where bt.[status] like '%A%' and st.[status] not like '%A%'
				)
			)
	 
	Select Entity_Structure_FK, entity_data_Name_FK, data_name_fk, value into ##tmpCusts from epacube.entity_identification where entity_data_name_fk = 144020 and data_name_fk = 144111 and value not in
	(select Shipto from ##shipto)
	 
	Insert into ##tmpCusts (Entity_Structure_FK, entity_data_Name_FK, data_name_fk, value)
	Select Entity_Structure_FK, entity_data_Name_FK, data_name_fk, value from epacube.entity_identification where entity_data_name_fk = 144010 and data_name_fk = 144111 and value not in
	(select Custno from ##cust_no)

	Create index idx_cst on ##tmpCusts(entity_structure_fk)
	 
	Delete from epacube.ENTITY_IDENT_NONUNIQUE where entity_structure_fk in (Select entity_structure_fk from ##tmpCusts)
--	Truncate Table epacube.ENTITY_MULT_TYPE
	Delete from epacube.ENTITY_MULT_TYPE where entity_structure_fk in (Select entity_structure_fk from ##tmpCusts)
	-----Added to remove old events that have not been approved as new events are created
	Delete from synchronizer.event_data_errors where event_fk in (
		select event_id from synchronizer.event_data where epacube_id 
			in (Select entity_structure_fk from ##tmpCusts))
---added to remove constraint errors Jira - Maui 26
	DELETE from synchronizer.event_data_rules where event_fk in (
		select event_id from synchronizer.event_data where epacube_id 
			in (Select entity_structure_fk from ##tmpCusts))


	DELETE from synchronizer.event_data where epacube_id 
			in (Select entity_structure_fk from ##tmpCusts)
	
	Delete from epacube.ENTITY_Category where entity_structure_fk in (Select entity_structure_fk from ##tmpCusts)
	Delete from epacube.ENTITY_Attribute where entity_structure_fk in (Select entity_structure_fk from ##tmpCusts)
	Delete from epacube.ENTITY_IDENTIFICATION where entity_data_name_fk in (144010, 144020) and entity_structure_fk in (Select entity_structure_fk from ##tmpCusts)
	 
	Delete from synchronizer.EVENT_CALC_RULES_BASIS where event_rules_fk in (Select Event_Rules_ID from synchronizer.event_calc_Rules where event_fk in (select event_id from synchronizer.event_calc where cust_entity_structure_fk  in (Select entity_structure_fk from ##tmpCusts)))
	 
	Delete from synchronizer.event_calc_Rules where event_fk in (select event_id from synchronizer.event_calc where cust_entity_structure_fk  in (Select entity_structure_fk from ##tmpCusts))
	Delete from synchronizer.event_calc where cust_entity_structure_fk  in (Select entity_structure_fk from ##tmpCusts)
	Delete from marginmgr.order_data_history where cust_entity_structure_fk in (Select entity_structure_fk from ##tmpCusts)
	 
	Delete from epacube.ENTITY_STRUCTURE where entity_structure_ID in (Select entity_structure_fk from ##tmpCusts)
	 
	--Drop Table ##shipto
	--Drop Table ##cust_no
	--Drop Table  ##tmpCusts

End

----            SET @ls_exec = 'STOP CODE HERE'            
----            INSERT INTO COMMON.T_SQL
----                ( TS,  SQL_TEXT )
----            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
----            exec sp_executesql 	@ls_exec;



--INSERT INTO RECORD DATA FOR SX CUSTOMER BILL TO RECORDS

--------------------------------------------------------------------------------------
--  CREATE BILL TO RECORD FROM RAW DATA
---Check paramater to see if we load cust price code and cust disc code here or from SXE TABLE DATA
--------------------------------------------------------------------------------------
--set @V_RULES_FILTER_LOOKUP = (
--select isnull(value,'0') 
--from epacube.EPACUBE_PARAMS 
--where name = 'RULES_FILTER_LOOKUP_OPERAND')

SET @status_desc = '100 Load Import_Record_Data'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

		INSERT INTO [import].[IMPORT_RECORD_DATA]
           ([IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[SEQ_ID]
           ,[JOB_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[DATA_POSITION1]
           ,[DATA_POSITION2]
           ,[DATA_POSITION3]
           ,[DATA_POSITION4]
           ,[DATA_POSITION5]
           ,[DATA_POSITION6]
           ,[DATA_POSITION7]
           ,[DATA_POSITION8]
           ,[DATA_POSITION9]
           ,[DATA_POSITION10]
           ,[DATA_POSITION11]
           ,[DATA_POSITION12]
           ,[DATA_POSITION13]
           ,[DATA_POSITION14]
           ,[DATA_POSITION15]
           ,[DATA_POSITION16]
           ,[DATA_POSITION17]
           ,[DATA_POSITION18]
           ,[DATA_POSITION19]
           ,[DATA_POSITION20]
           ,[DATA_POSITION21]
           ,[DATA_POSITION22]
           ,[DATA_POSITION23]
           ,[DATA_POSITION24]
           ,[DATA_POSITION25]
           ,[DATA_POSITION26]
           ,[DATA_POSITION27]
           ,[DATA_POSITION28]
           ,[DATA_POSITION29]
           ,[DATA_POSITION30]
           ,[DATA_POSITION31]
           ,[DATA_POSITION32]
           ,[DATA_POSITION33]
           ,[DATA_POSITION34]
           ,[DATA_POSITION35]
           ,[DATA_POSITION36]
           ,[DATA_POSITION37]
           ,[DATA_POSITION38]
           ,[DATA_POSITION39]
           ,[DATA_POSITION40]
           ,[DATA_POSITION41]
           ,[DATA_POSITION42]
           ,[DATA_POSITION43]
           ,[DATA_POSITION44]
           ,[DATA_POSITION45]
           ,[DATA_POSITION46]
           ,[DATA_POSITION47]
           ,[DATA_POSITION48]
           ,[DATA_POSITION49]
           ,[DATA_POSITION50]
           ,[DATA_POSITION51]
           ,[DATA_POSITION52]
           ,[DATA_POSITION53]
           ,[DATA_POSITION54]
           ,[DATA_POSITION55]
           ,[DATA_POSITION56]
           ,[DATA_POSITION57]
           ,[DATA_POSITION58]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
)
(select distinct
'SX CUSTOMERS (BILL TO)'
,NULL
,NULL
,@IN_JOB_FK
,241000 -- SX BILL TO CUSTOMERS
,CUST_NO
           ,[CITY]
           ,[CLASS]
           ,[COUNTRY]
           ,[CUST_TYPE]
           ,[CUST_PRICE_TYPE]
           ,DISC_CODE
           ,[JOB_DESC]
           ,[CUST_NAME]
           ,[PRICE_CUST_NO]
           ,PRICE_CODE
           ,[REBATE_TYPE]
           ,[ROUTE]
           ,[SALESREP_OUT]
           ,[SALESREP_IN]
           ,[TERRITORY]
           ,[SHIP_TO]
           ,[SIC_CODE1]
           ,[SIC_CODE2]
           ,[SIC_CODE3]
           ,[STATE]
           ,[USER1]
           ,[USER10]
           ,[USER11]
           ,[USER12]
           ,[USER13]
           ,[USER14]
           ,[USER15]
           ,[USER16]
           ,[USER17]
           ,[USER18]
           ,[USER19]
           ,[USER2]
           ,[USER20]
           ,[USER21]
           ,[USER22]
           ,[USER23]
           ,[USER24]
           ,[USER3]
           ,[USER4]
           ,[USER5]
           ,[USER6]
           ,[USER7]
           ,[USER8]
           ,[USER9]
           ,[WAREHOUSE_ID]
           ,[ZIP_CODE]
           ,[CUSTOM1]
           ,[CUSTOM2]
           ,[CUSTOM3]
           ,[CUSTOM4]
           ,[CUSTOM5]
           ,[CUSTOM6]
           ,[CUSTOM7]
           ,[CUSTOM8]
           ,[CUSTOM9]
           ,[CUSTOM10]
           ,[DSO]
           ,getdate()
           ,'IMPORT'
           FROM
			##CustBT
--WHERE SHIP_TO IS NULL
--AND [STATUS] = 'A'
--and job_fk = @in_job_fk
)

IF object_id('tempdb..##CustBT') is not null
Drop Table ##CustBT

--LOOKUP PRIMARY CUSTOMER BY PACKAGE
SET @v_import_package_fk = (select distinct import_package_fk 
							from import.import_record_data WITH (NOLOCK)
							where  job_fk = @in_job_fk)

SET @v_column_name = (select column_name 
						from import.import_map_metadata WITH (NOLOCK)
						where import_package_fk = @v_import_package_fk
						and ident_data_name_fk = (select data_name_id
from epacube.data_name where name = 'CUSTOMER CODE'))  

SET @v_primary = (select source_field_name
						from import.import_map_metadata WITH (NOLOCK)
						where import_package_fk = @v_import_package_fk
						and ident_data_name_fk =(select data_name_id
from epacube.data_name where name = 'CUSTOMER CODE'))  

				UPDATE common.job
				SET name = (select name from import.import_package WITH (NOLOCK)
							WHERE import_package_id = @v_import_package_fk)
				WHERE job_id = @in_job_Fk

--LOOKUP WHAT MAKES THE ROW UNIQUE BY PACKAGE

SET @V_UNIQUE1 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 1),-999)

SET @V_UNIQUE2 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 2),-999)

SET @V_UNIQUE3 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 3), -999)



/* ----------------------------------------------------------------------------------- */
--   --Create Temp table to insert distinct customer data 
/* ----------------------------------------------------------------------------------- */


--drop temp table if it exists
	IF object_id('tempdb..#TS_DISTINCT_CUSTOMERS') is not null
	   drop table #TS_DISTINCT_CUSTOMERS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_DISTINCT_CUSTOMERS(
	IMPORT_RECORD_DATA_FK	BIGINT,
	[IMPORT_FILENAME] [varchar](128),
	[RECORD_NO] [int],
	[JOB_FK] [bigint],
	[IMPORT_PACKAGE_FK] [int],
	[EFFECTIVE_DATE] [datetime],
	[END_DATE] [datetime],
	[ENTITY_CLASS] [varchar](64) ,
	[ENTITY_DATA_NAME] [varchar](64),
	[ENTITY_ID_DATA_NAME] [varchar](64),
	[ENTITY_ID_VALUE] [varchar](64),
	[ENTITY_STATUS] [varchar](32),
	[SYSTEM_ID_VALUE] [varchar](64),
	[PRIORITY] [varchar](32),
	[SEARCH_ACTIVITY_FK] [int],
	[UPDATE_TIMESTAMP] [datetime],
	[UPDATE_USER] [varchar](64)
		)

Create Index idx_tscust on #TS_DISTINCT_CUSTOMERS([JOB_FK], [ENTITY_ID_VALUE])

-------------------------------------------------------------------------------------
--   LOAD TEMP TABLE WITH DISTINCT PRODUCT DATA
--   NEED to check if NULL Then do something else to get product identification 
-------------------------------------------------------------------------------------


SET @ls_exec =
						'INSERT INTO #TS_DISTINCT_CUSTOMERS
						(   [JOB_FK]
						   ,[IMPORT_PACKAGE_FK]
						   ,[END_DATE]
						   ,[ENTITY_CLASS]
						   ,[ENTITY_DATA_NAME]
						   ,[ENTITY_ID_DATA_NAME]
						   ,[ENTITY_ID_VALUE]
						   )
						SELECT distinct
							IRD.JOB_FK
							,IRD.IMPORT_PACKAGE_FK
						    ,IRD.END_DATE '
						   + ', ' 
						   +  ' ''CUSTOMER'' '
						   + ', '  
						   + ' ''CUSTOMER BILL TO'' '
						    + ', ''' 
						   + @v_primary
						    + ''', '
						   + @v_column_name
						   + ' ' 
						+ ' FROM IMPORT.IMPORT_RECORD_DATA  IRD WITH (NOLOCK) '
						+ ' WHERE 1=1 '
						+ ' AND isnull(DO_NOT_IMPORT_IND,0) <> 1'
						+ ' AND '
						+ @v_column_name 
						+ ' IS NOT NULL'
						+ ' AND ISNULL(IRD.JOB_FK,-999) = '
						+ cast(isnull (@in_job_fk, -999) as varchar(20))

SET @status_desc = '110 Load Distinct Customers'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), @ls_exec )

         exec sp_executesql 	@ls_exec;

-----
SET @status_desc = '120 Update Distinct Customers'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

UPDATE #TS_DISTINCT_CUSTOMERS
				SET RECORD_NO = x.ROW 
				,import_filename = (SELECT Top 1 import.import_record_data.import_filename
											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
											WHERE 1=1 
											AND Job_Fk = @in_job_fk)
				,effective_date = ISNULL((SELECT Top 1 import.import_record_data.effective_date
											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
											WHERE 1=1 
											AND Job_Fk = @in_job_fk), getdate())
				FROM (SELECT ROW_NUMBER() 
						OVER (ORDER BY ENTITY_ID_VALUE) AS Row, 
						ENTITY_ID_VALUE
				FROM #TS_DISTINCT_CUSTOMERS where job_fk = @in_job_fk)x
				where #TS_DISTINCT_CUSTOMERS.entity_id_value = x.entity_id_value
AND #TS_DISTINCT_CUSTOMERS.job_Fk = @in_job_fk



-----------------------------------------------------
	


-------------------------------------------------------------------------------------
--   LOAD IMPORT_RECORD TABLE(S) 
-------------------------------------------------------------------------------------
SET @status_desc = '130 Load Import_Record'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

					INSERT INTO IMPORT.IMPORT_RECORD 
							   ([IMPORT_FILENAME]
							   ,[RECORD_NO]
							   ,[JOB_FK]
							   ,[IMPORT_PACKAGE_FK]
							   ,[EFFECTIVE_DATE]
							   ,[END_DATE]
							   ,[ENTITY_CLASS]
							   ,[ENTITY_DATA_NAME]
							   ,[ENTITY_ID_DATA_NAME]
							   ,[ENTITY_ID_VALUE]
							   ,[ENTITY_STATUS]
							   ,[UPDATE_TIMESTAMP]
							   ,[UPDATE_USER])

					SELECT 
							   TDP.IMPORT_FILENAME
							   ,TDP.RECORD_NO
							   ,TDP.JOB_FK
							   ,TDP.IMPORT_PACKAGE_FK
							   ,TDP.EFFECTIVE_DATE
							   ,TDP.END_DATE
							   ,TDP.ENTITY_CLASS
							   ,TDP.ENTITY_DATA_NAME
							   ,TDP.ENTITY_ID_DATA_NAME
							   ,TDP.ENTITY_ID_VALUE
							   ,'ACTIVE'
							   ,TDP.UPDATE_TIMESTAMP
							   ,TDP.UPDATE_USER
					FROM #TS_DISTINCT_CUSTOMERS TDP
					WHERE 1=1
					AND TDP.JOB_FK = @in_job_FK


-------------------------------------------------------------------------
 SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'SXE CUSTOMER TRANSFORMATION',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


-------------------------------------------------------------------------------------
--   UPDATE IMPORT RECORD WITH RECORD_NO and SEQ_ID
-------------------------------------------------------------------------------------

SET @status_desc = '140 Update Import_Record_Data'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

			SET @ls_exec =
						'UPDATE IMPORT.IMPORT_RECORD_DATA
						set RECORD_NO = import.import_record.record_no
						from import.import_record WITH (NOLOCK)
						where'
						+ ' '
						+ @v_column_name 
						+ ' = entity_id_value
						and import.import_record_data.JOB_FK = '
						+ cast(isnull (@in_job_fk, -999) as varchar(20))
						+ ' and import.import_record.JOB_FK = '
						+ cast(isnull (@in_job_fk, -999) as varchar(20))


----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), @ls_exec )

         exec sp_executesql 	@ls_exec;


-----------

SET @status_desc = '150 Update 2 Import_Record_Data'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
				UPDATE IMPORT.IMPORT_RECORD_DATA
				SET SEQ_ID = x.ROW 
				FROM (SELECT ROW_NUMBER() 
				OVER (ORDER BY IMPORT_RECORD_DATA_ID) AS Row, 
					 IMPORT_RECORD_DATA_ID
			FROM IMPORT.IMPORT_RECORD_DATA WITH (NOLOCK)
			where job_fk = @in_job_fk)x
			WHERE import.import_record_data.job_fk = @in_job_fk
			AND import.import_record_data.import_record_data_id = x.import_record_data_id


---------------------------------------------------------------------------
-- ADDED TO MARK ANY DUPLICATE ROWS AS DO NOT IMPORT
----------------------------------------------------------------------------


		SET @ls_exec =
						'UPDATE IMPORT.IMPORT_RECORD_DATA
						SET Do_Not_import_IND = 1 
						WHERE import_record_data_id IN (
								SELECT A.import_record_data_id
								FROM (         
								SELECT ird1.import_record_data_id 
					   ,DENSE_RANK() OVER ( PARTITION BY '
						   + ' ' 
                           + @V_UNIQUE1
						   + ' , ' 
                           + @V_UNIQUE2
					       + ' , '
                           + @V_UNIQUE3
						   + ''
						   + ' ORDER BY ird1.import_record_data_id DESC ) AS DRANK
							FROM IMPORT.IMPORT_RECORD_DATA IRD1
							WHERE ird1.import_record_data_id  in (
							select ird.import_record_data_id 
							from import.import_record_data ird
							where ird.job_fk = '
							+ cast(isnull (@in_job_fk, -999) as varchar(20))
							+ ')
										) A
										WHERE DRANK <> 1   )'		
					
SET @status_desc = '150 Mark Duplicates Do Not Import'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), isnull ( @ls_exec, 'Trying to update do not import ind from stage.SXE_CUSTBILL_TRANSFORMATION' ) )

         exec sp_executesql 	@ls_exec;



--- EPA Constraint
UPDATE IMPORT.IMPORT_RECORD_DATA
				SET SEQ_ID = NULL
--,RECORD_NO = NULL
WHERE  job_fk = @in_job_fk
and DO_NOT_IMPORT_IND = 1

------------
----------add Paramater check to see if this is needed

------------------------------------------------------
---CALL STAGE TO EVENTS
------------------------------------------------------
SET @status_desc = '160 Call Stage_To_Events'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc; 

     SET @V_START_TIMESTAMP = getdate()			


			 EXEC[stage].[STG_TO_EVENTS] @IN_JOB_FK
	

      SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'STAGE TO EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



-------------------------------------------------------------------------------------
-- LAST Step is to Set Job Execution to Complete
-------------------------------------------------------------------------------------
----
----

      SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT ENTITIES TO STAGE' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT ENTITIES RESOLVED' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
		update common.job
		set job_complete_timestamp = getdate()
		where job_id = @in_job_fk

------------
--RUN THE SHIP TO TRANSFORMATION
------------

SET @status_desc = '170 Call Ship-To Transformation'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc; 

 EXEC  [import].[SXE_CUSTSHIP_TRANSFORMATION] @IN_JOB_FK

--Insert Warehouses and SalesReps of Record

SET @status_desc = '180 Load Warehouses, Salesreps'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc; 

Delete from epacube.entity_attribute where data_name_fk in (145010, 145020, 144900)

Insert into epacube.entity_attribute (Entity_Structure_FK, Data_Name_fk, Attribute_Event_Data, Record_Status_CR_FK)
Select * from 
(
Select isnull((Select entity_structure_fk from epacube.ENTITY_IDENTIFICATION where ENTITY_DATA_NAME_FK in (144010, 144020) and VALUE =
	Case when isnull(cust.ship_to, '') = '' then cust.cust_no else cust.cust_no + '-' + cust.ship_to end) 
	,
	(Select entity_structure_fk from epacube.ENTITY_IDENTIFICATION where ENTITY_DATA_NAME_FK in (144010, 144020) and VALUE =
	cust.cust_no)) 'Entity_Structure_FK'
, 144900 Data_Name_FK
, Cust.Warehouse_ID
, 1 Record_Status_CR_FK
from import.IMPORT_SXE_CUSTOMERS cust 
where 1 = 1
and 
	(status like '%A%' or status = '1'
		or (@InactiveShipTo = 1 and cust.cust_no in (Select cust_no from import.IMPORT_SXE_CUSTOMERS Union Select value from epacube.entity_identification where entity_data_name_fk = 144010 and record_status_cr_fk = 1) and isnull(cust.[Ship_to], '') <> '')
	)
and isnull(warehouse_id, '') > ''
and IMPORT_FILE_NAME in (select MAX(IMPORT_FILE_NAME) from import.import_sxe_customers)
) A
where Entity_Structure_FK Not In (Select entity_structure_fk from epacube.entity_attribute where data_name_fk = 144900)

Insert into epacube.entity_attribute (Entity_Structure_FK, Data_Name_fk, Attribute_Event_Data, Record_Status_CR_FK)
Select * from 
(Select isnull((Select entity_structure_fk from epacube.ENTITY_IDENTIFICATION where ENTITY_DATA_NAME_FK in (144010, 144020) and VALUE =
	Case when isnull(cust.ship_to, '') = '' then cust.cust_no else cust.cust_no + '-' + cust.ship_to end) 
	,
	(Select entity_structure_fk from epacube.ENTITY_IDENTIFICATION where ENTITY_DATA_NAME_FK in (144010, 144020) and VALUE =
	cust.cust_no)) 'Entity_Structure_FK'
, 145010 Data_Name_FK
, Cust.Salesrep_In
, 1 Record_Status_CR_FK
from import.IMPORT_SXE_CUSTOMERS cust 
where 1 = 1
and 
	(status like '%A%' or status = '1'
		or (@InactiveShipTo = 1 and cust.cust_no in (Select cust_no from import.IMPORT_SXE_CUSTOMERS Union Select value from epacube.entity_identification where entity_data_name_fk = 144010 and record_status_cr_fk = 1) and isnull(cust.[Ship_to], '') <> '')
	)
and isnull(Cust.Salesrep_In, '') > ''
and IMPORT_FILE_NAME in (select MAX(IMPORT_FILE_NAME) from import.import_sxe_customers)) A
where Entity_Structure_FK Not In (Select entity_structure_fk from epacube.entity_attribute where data_name_fk = 145010)

Insert into epacube.entity_attribute (Entity_Structure_FK, Data_Name_fk, Attribute_Event_Data, Record_Status_CR_FK)
Select * from 
(Select isnull((Select entity_structure_fk from epacube.ENTITY_IDENTIFICATION where ENTITY_DATA_NAME_FK in (144010, 144020) and VALUE =
	Case when isnull(cust.ship_to, '') = '' then cust.cust_no else cust.cust_no + '-' + cust.ship_to end) 
	,
	(Select entity_structure_fk from epacube.ENTITY_IDENTIFICATION where ENTITY_DATA_NAME_FK in (144010, 144020) and VALUE =
	cust.cust_no)) 'Entity_Structure_FK'
, 145020 Data_Name_FK
, Cust.Salesrep_Out
, 1 Record_Status_CR_FK
from import.IMPORT_SXE_CUSTOMERS cust 
where 1 = 1
and 
	(status like '%A%' or status = '1'
		or (@InactiveShipTo = 1 and cust.cust_no in (Select cust_no from import.IMPORT_SXE_CUSTOMERS Union Select value from epacube.entity_identification where entity_data_name_fk = 144010 and record_status_cr_fk = 1) and isnull(cust.[Ship_to], '') <> '')
	)
 and isnull(Cust.Salesrep_Out, '') > ''
and IMPORT_FILE_NAME in (select MAX(IMPORT_FILE_NAME) from import.import_sxe_customers)) A
where Entity_Structure_FK Not In (Select entity_structure_fk from epacube.entity_attribute where data_name_fk = 145020)

----****Custom Code Start**** -- Ship_to_Adjustments
SET @status_desc = '190 Call Custom Code 2'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc; 

Set @CustomCode_Last_Step = 
	(select Top 1 SQL_cd from dbo.MAUI_Config_ERP 
	where 1 = 1
	and ERP = (select value from epacube.epacube_params where APPLICATION_SCOPE_FK = 100 and [NAME] = 'ERP HOST')
	and usage = 'Customer_Customization'
	and [Table] = 'import.import_sxe_customers'
	AND [Temp_Table] = 'Ship_to_Adjustments'
	and epacube_customer = (select value from epacube.epacube_params where APPLICATION_SCOPE_FK = 100 and [NAME] = 'EPACUBE CUSTOMER'))

Exec(@CustomCode_Last_Step)

----****Custom Code End****


--Load CPT and CRT Records from import.IMPORT_SXE_TABLE_DATA When Appropriate
If	(
		isnull((Select COUNT(*) from import.IMPORT_SXE_TABLE_DATA where data_name = 'Customer Ptype'), 0) + isnull((Select COUNT(*) from import.IMPORT_SXE_Customers where isnull(CUST_PRICE_TYPE, '') <> ''), 0) > (select value from epacube.epacube_params where NAME = 'MIN RECORDS EXPECTED TABLE')
		And (Select COUNT(*) from import.IMPORT_SXE_TABLE_DATA where data_name = 'Customer Ptype') = 0
	)
	or
		(	Select COUNT(*) from import.IMPORT_SXE_TABLE_DATA istd
			inner join epacube.ENTITY_IDENTIFICATION ei with (nolock) on istd.VALUE2 = ei.VALUE and ei.ENTITY_DATA_NAME_FK in (144020, 144010)
			where data_name = 'Customer Ptype'
		) > (select value from epacube.epacube_params where NAME = 'MIN RECORDS EXPECTED TABLE')
		
SET @status_desc = '200 Load CPT CRT'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc; 

Begin
	Delete from [epacube].[ENTITY_MULT_TYPE_HISTORICAL]  where Load_From_EMT_Date not in (
	select top 5 Load_From_EMT_Date from [epacube].[ENTITY_MULT_TYPE_HISTORICAL]  with (nolock) group by Load_From_EMT_Date order by Load_From_EMT_Date Desc) 

	If Cast(getdate() as date) not in (Select distinct Cast(load_from_emt_date as date) from [epacube].[ENTITY_MULT_TYPE_HISTORICAL])
	INSERT INTO [epacube].[ENTITY_MULT_TYPE_HISTORICAL]
	(entity_structure_fk, data_name_fk, data_value_fk, Value, RECORD_STATUS_CR_FK, precedence)
	select emt.entity_structure_fk, emt.data_name_fk, emt.data_value_fk, dv.value, emt.RECORD_STATUS_CR_FK , precedence
	from [epacube].[ENTITY_MULT_TYPE] EMT with (nolock) 
	inner join epacube.data_value dv with (nolock) on EMT.DATA_VALUE_FK = dv.DATA_VALUE_ID and emt.DATA_NAME_FK = dv.DATA_NAME_FK

	Truncate Table epacube.ENTITY_MULT_TYPE

	Declare @cpt_job_fk int

	set @cpt_job_fk = (select distinct job_fk from import.IMPORT_SXE_TABLE_DATA )

SET @status_desc = '201 EXEC import.LOAD_SXE_CPT'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc; 

	EXEC import.LOAD_SXE_CPT  
	 @cpt_job_fk

--Add any missing CPTs and CRTs that exist in the import.import_sxe_customers table

SET @status_desc = '202 CPT CRT Load from Cursor'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc; 

	IF object_id('tempdb..#emt_tmp') is not null
	drop table #emt_tmp

	Create Table #emt_tmp(CES_FK bigint Null, Data_Name_FK bigint Null, Data_Name Varchar(128) Null)

	IF object_id('tempdb..##EMT') is not null
	drop table ##EMT

	Select Cast(Null as bigint) Cust_Entity_Structure_FK, Cast(Null as bigint) Data_Name_FK, Cast(Null as Varchar(64)) Cust_Value into ##EMT where 1 = 2
	Create index idx_emt on ##EMT(Cust_Entity_Structure_FK, Data_Name_FK, Cust_Value)
	
	
	Declare
		@DN_FK Bigint

		Declare cur_dn Cursor local for
			Select 244900
			Union Select 244901
			OPEN cur_dn;
		
		FETCH NEXT FROM cur_dn INTO @dn_fk
			WHILE @@FETCH_STATUS = 0
			BEGIN		

			Insert into #emt_tmp
			SELECT
					(Select entity_structure_fk from epacube.entity_identification where entity_data_name_fk in (144010, 144020) 
						and value = Case when isnull(c.ship_to, '') > '' then c.cust_no + '-' + c.ship_to else c.cust_no end) 'CES_FK'
					, @dn_fk 'Data_Name_FK'
					, Case @dn_fk When 244900 then [Cust_Price_Type] when 244901 then [Rebate_type] end 'Data_Name'
					from import.import_sxe_customers c where (status like '%a%' or status like '%1%')
					and ((@dn_fk = 244900 and CUST_PRICE_TYPE is not null) or (@dn_fk = 244901 and Rebate_TYPE is not null))
			
			If (Select COUNT(*) from import.IMPORT_SXE_CUSTOMERS) = 0
			Insert into #emt_tmp
			SELECT
					(Select entity_structure_fk from epacube.entity_identification where entity_data_name_fk in (144010, 144020) 
						and value = Case when isnull(c.ship_to, '') > '' then c.cust_no + '-' + c.ship_to else c.cust_no end) CES_FK
					, @dn_fk
					, Case @dn_fk When 244900 then [Cust_Price_Type] when 244901 then [Rebate_type] end
					from import.IMPORT_SXE_CUSTOMERS_LAST_IMPORT c where (status like '%a%' or status like '%1%')
					and ((@dn_fk = 244900 and CUST_PRICE_TYPE is not null) or (@dn_fk = 244901 and Rebate_TYPE is not null))
							
		FETCH NEXT FROM cur_dn INTO @dn_fk
		End
		Close cur_dn
		Deallocate cur_dn							
		
		Create index idx_emttmp on #emt_tmp(CES_FK, Data_Name_FK)	

		DECLARE 
			@CES_FK bigint
			, @cust_value varchar(max)

			DECLARE cur_v_import CURSOR local for
				SELECT * from #emt_tmp

				 OPEN cur_v_import;
				 FETCH NEXT FROM cur_v_import INTO @CES_FK,
												   @dn_fk,         
												   @cust_value
			  WHILE @@FETCH_STATUS = 0
			  BEGIN

				INSERT INTO ##EMT
				   (Cust_Entity_Structure_FK
				   ,Data_Name_FK
				   ,Cust_Value
				   )
				select
					 @CES_FK,
					 @dn_fk,
					 *
				from utilities.SplitString ( @cust_value, ',' ) ss	 

		 FETCH NEXT FROM cur_v_import INTO @CES_FK,
												   @dn_fk,         
												   @cust_value
		END 
		CLOSE cur_v_import
		DEALLOCATE cur_v_import

	SET @status_desc = '203 Cursor Data Created'
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc; 

	Insert into epacube.data_value
	(DATA_NAME_FK, VALUE, RECORD_STATUS_CR_FK, update_user)
	Select emt.data_name_fk, emt.cust_value, 1, 'Missing Value Insert'
	from ##emt emt
	left join epacube.DATA_VALUE dv on emt.Data_Name_FK = dv.DATA_NAME_FK and emt.Cust_Value = dv.VALUE
	where dv.VALUE is null group by emt.data_name_fk, emt.cust_value

	Insert into epacube.entity_mult_type
	(entity_structure_fk, DATA_NAME_FK, DATA_VALUE_FK, RECORD_STATUS_CR_FK)
	Select A.cust_entity_structure_fk, A.data_name_fk, A.data_value_id, 1 from
	(Select emt.cust_entity_structure_fk, emt.data_name_fk, emt.cust_value, dv.data_value_id
		from ##EMT emt
		inner join epacube.DATA_VALUE dv on emt.Data_Name_FK = dv.DATA_NAME_FK and emt.Cust_Value = dv.VALUE) A
	left join epacube.ENTITY_MULT_TYPE B on A.cust_entity_structure_fk = B.ENTITY_STRUCTURE_FK and A.Data_Name_FK = B.DATA_NAME_FK and A.DATA_VALUE_ID = B.DATA_VALUE_FK
	where B.ENTITY_MULT_TYPE_ID is null and A.Cust_Entity_Structure_FK is not null
	Group by A.cust_entity_structure_fk, A.data_name_fk, A.data_value_id

	Delete from epacube.data_value where data_name_fk in (244900, 244901) and data_value_id not in (select data_value_fk from epacube.entity_mult_type)

	SET @status_desc = '204 Cursor Load Complete'
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc; 
	
	IF object_id('tempdb..##EMT') is not null
	drop table ##EMT
	
	IF object_id('tempdb..#emt_tmp') is not null
	drop table #emt_tmp
End
--------------------------------------------------------

SET @status_desc = '210 CPT CRT Loaded'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc; 

IF (Select COUNT(*) from import.import_sxe_customers) > 0 --(select value from epacube.epacube_params where NAME = 'MIN RECORDS EXPECTED CUSTOMERS')
Begin
	Truncate Table import.IMPORT_SXE_CUSTOMERS_LAST_IMPORT
	Insert into import.IMPORT_SXE_CUSTOMERS_LAST_IMPORT
	Select * from import.IMPORT_SXE_CUSTOMERS
End

SET @status_desc = '220 import.import_sxe_customers_last_import populated'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc; 

--------------------------------------------------------

--If (select count(*) from import.import_sxe_table_data where data_name = 'customer ptype') > 0
--Begin
Update JE_e
	Set input_rows = (select count(*) from epacube.entity_mult_type where data_name_fk in (244900, 244901))
	from common.job_execution JE_e
	where Job_execution_id = 
	(select top 1 job_execution_id from common.job_execution JE
	inner join common.job J with (nolock) on je.job_fk = j.job_id and JE.name = 'IMPORT COMPLETE'
	where J.Name = 'IMPORT SX TABLE DATA'
	order by JE.job_execution_id desc)
	and JE_e.Job_FK = @cpt_job_fk
--End

insert into dbo.maui_log
(job_fk_i, Activity, start_time, record_inserts)
Select @in_Job_FK, Activity, GETDATE(), [Record Inserts] from 
(Select 'Import Customers: ' + Cast((Select COUNT(*) from epacube.DATA_VALUE where DATA_NAME_FK = 244900) as varchar(32)) + ' Unique CPTs; ' + Replace(Convert(varchar(32), Convert(money,(Select COUNT(*) from epacube.ENTITY_MULT_TYPE where DATA_NAME_FK = 244900), 126), 1), '.00', '')  + ' CPTs by Customer' Activity, (Select COUNT(*) from epacube.ENTITY_MULT_TYPE where DATA_NAME_FK = 244900) 'Record Inserts'
Union Select 'Import Customers: ' + Cast((Select COUNT(*) from epacube.DATA_VALUE where DATA_NAME_FK = 244901) as varchar(32)) + ' Unique CRTs; ' + Replace(Convert(varchar(32), Convert(money,(Select COUNT(*) from epacube.ENTITY_MULT_TYPE where DATA_NAME_FK = 244901), 126), 1), '.00', '')  + ' CRTs by Customer' Activity, (Select COUNT(*) from epacube.ENTITY_MULT_TYPE where DATA_NAME_FK = 244901) 'Record Inserts'
Union 
Select 'Import Customer Bill-to: ' + Cast((Select COUNT(*) from epacube.entity_structure where DATA_NAME_FK = 144010) as varchar(32)) + ' Bill-To' Activity, (Select COUNT(*) from epacube.entity_structure where DATA_NAME_FK = 144010) 'Record Inserts'
Union Select 'Import Customer Ship-to: ' + Cast((Select COUNT(*) from epacube.entity_structure where DATA_NAME_FK = 144020) as varchar(32)) + ' Ship-To' Activity, (Select COUNT(*) from epacube.entity_structure where DATA_NAME_FK = 144020) 'Record Inserts'
) A

 SET @status_desc =  'finished execution of stage.SXE_CUSTOMER_TRANSFORMATION'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of STAGE.SXE_CUSTOMER_TRANSFORMATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;

EndOfProc:
END
