﻿



-- Copyright 2006
--
-- Procedure created by Walt Tucholski
--
--
-- Purpose: Execute SSIS Import packages for DMU vendor imports
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- WT        03/23/2007   Initial SQL Version
-- YN        10/23/2008   Changed the stored procedure to be executed in a Service Broker queue
-- CV        11/02/2008   Passing paramaters to allow for multi DMU users. 

CREATE PROCEDURE [import].[IMPORT_DMU_SSIS_PACKAGE]
      (
    --    @in_dmu_ssis_config_params_id int,
			 @PACKAGE_PATH varchar(256),
             @IMPORT_FILE_PATH varchar(256),
             @SERVER_NAME varchar(256),
             @DMU_FILE_PATH varchar(256),
			 @FILE_NAME varchar(256),
			 @EMAIL varchar(256), 
			 @PACKAGE_NAME varchar(256) )

AS 
      BEGIN

            DECLARE @rt int
            DECLARE @cmd varchar(4000)
           -- DECLARE @PACKAGE_PATH varchar(256)
           -- DECLARE @PACKAGE_NAME varchar(256)
           -- DECLARE @IMPORT_FILE_PATH varchar(256)
           -- DECLARE @SERVER_NAME varchar(256)
           -- DECLARE @DMU_FILE_PATH varchar(256)

            DECLARE @ls_stmt varchar(1000)
                  , @l_exec_no bigint
                  , @status_desc varchar(max)
                  , @ErrorMessage nvarchar(4000)
                  , @ErrorSeverity int
                  , @ErrorState INT
            DECLARE @job_name VARCHAR(250)
            DECLARE @epa_job_id BIGINT

            BEGIN TRY

                  SET NOCOUNT ON

                  SET @l_exec_no = 0 ;
                  SET @ls_stmt = 'started execution of import.IMPORT_DMU_SSIS_PACKAGE. ' +
                      ' @in_package path = ' +
                      cast(isnull(@PACKAGE_PATH, 0) as varchar(256))

                  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                       @exec_id = @l_exec_no OUTPUT ;

                 -- SELECT    @PACKAGE_PATH = PACKAGE_PATH
                     --     , @PACKAGE_NAME = PACKAGE_NAME
                  --        , @IMPORT_FILE_PATH = IMPORT_FILE_PATH
                  --        , @SERVER_NAME = SERVER_NAME
                  --        , @DMU_FILE_PATH = DMU_FILE_PATH
                 -- FROM      epacube.import.DMU_SSIS_CONFIG_PARAMS
                --  WHERE     DMU_SSIS_CONFIG_PARAMS_ID = @in_dmu_ssis_config_params_id

                  SELECT    @job_name = 'Executing SSIS package: ' + @PACKAGE_NAME
      

                  EXEC [common].[JOB_CREATE] @IN_JOB_FK = NULL,
                       @IN_job_class_fk = 270,
                       @IN_JOB_NAME = @job_name,
                       @IN_DATA1 = NULL,
                       @IN_DATA2 = NULL,
                       @IN_DATA3 = NULL,
                       @IN_JOB_USER = NULL,
                       @OUT_JOB_FK = @epa_job_id OUTPUT,
                       @CLIENT_APP = 'SSIS' ;
 

                  SET @cmd = 'DTEXEC /FILE' +
                      ' ' +  '"' +
                      @PACKAGE_PATH + 
                      '\' + 
                      @PACKAGE_NAME + '"' +
                      ' ' +
                      '/MAXCONCURRENT " -1 "' +
                      ' ' +
                      '/CHECKPOINTING OFF' +
                      ' ' +
                      '/Decrypt epacube' +
                      ' ' + '/SET' + ' ' +
                      '\Package.Variables[User::V_IMPORT_FILE_DIRECTORY].Properties[Value];' + '"' +
                      @IMPORT_FILE_PATH + '"' +
                      ' ' + '/SET' + ' ' +
                      '\Package.Connections[ntsdb.epacube].ServerName;' +
                      @SERVER_NAME + ' ' +
                      '/SET' + ' ' +
                      '\Package.Connections[SourceConnectionOLEDB].ServerName;' + '"' +
                      @DMU_FILE_PATH + '"' + ' ' +
                     '/SET' + ' ' +
                     '\Package.Variables[User::V_IMPORT_FILE_NAME].Properties[Value];' + '"' +
                     @FILE_NAME + '"' +
					   ' ' + '/SET' + ' ' +
                      '\Package.Variables[User::V_JOB_ID].Properties[Value];' +
                      CAST(@epa_job_id AS VARCHAR(30))


insert into common.t_sql
(sql_text)
select 
 (select @cmd )

--select * from common.T_sql
--EXEC master..xp_cmdshell @cmd,NO_OUTPUT

 --put the SSIS execution into a Service Broker Queue         
                  DECLARE @RC INT
                  DECLARE @service_name NVARCHAR(50)
                  DECLARE @cmd_text NVARCHAR(4000)
                  DECLARE @cmd_type NVARCHAR(10)

                  SELECT    @service_name = 'TargetEpaService'
                          , @cmd_text = @cmd
                          , @cmd_type = 'CMD'
                  EXECUTE @RC = [queue_manager].[enqueue] @service_name, @cmd_text, @epa_job_id, @cmd_type

--
                  SET @status_desc = 'finished execution of import.IMPORT_DMU_SSIS_PACKAGE'
                  EXEC exec_monitor.Report_Status_sp @l_exec_no,
                       @status_desc ;
            END TRY
            BEGIN CATCH 

                  SELECT    @ErrorMessage = 'Execution of import.IMPORT_DMU_SSIS_PACKAGE has failed ' + ERROR_MESSAGE()
                          , @ErrorSeverity = ERROR_SEVERITY()
                          , @ErrorState = ERROR_STATE() ;

                  EXEC exec_monitor.Report_Error_sp @l_exec_no ;
                  declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
                  RAISERROR ( @ErrorMessage, -- Message text.
                            @ErrorSeverity, -- Severity.
                            @ErrorState -- State.
				    ) ;
            END CATCH
      END
























