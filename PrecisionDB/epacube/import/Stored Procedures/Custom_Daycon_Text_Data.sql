﻿



-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        05/26/2015   Initial Version 



CREATE PROCEDURE [import].[Custom_Daycon_Text_Data] 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to   
--
 
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     
	 DECLARE @v_again  bigint

	 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import].[Custom_Daycon_Text_Data', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = NULL;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


--
--drop temp table if it exists
	IF object_id('tempdb..#TS_PRODUCTS') is not null
	   drop table #TS_PRODUCTS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_PRODUCTS(
	[product_structure_FK]	BIGINT,
	--[Text] [varchar](max),
	text_length numeric,
	[DIVIDED]  numeric)


insert into #TS_PRODUCTS (
[product_structure_FK],
	--[Text],
	text_length ,
	[DIVIDED])
select  PRODUCT_STRUCTURE_FK, --text, 
len(text), (len(text)/61 + 1)  from epacube.PRODUCT_TEXT    where len(text) >61 
and TEXT not like '%^%'

--select * 
--from #TS_PRODUCTS

---------------------------------------------------------------------------------------------------
--create while loop
---------------------------------------------------------------------------------------------------

	  DECLARE @v_product_structure_fk   bigint  
	  DECLARE @v_divided numeric  
	 

		set @v_divided = 1
	
	 while @v_divided > 0 
	 begin

		 set @v_divided =(select top 1 divided from #TS_PRODUCTS 
			where DIVIDED <> 0 and DIVIDED not like '-%')
	 
	  set @v_product_structure_fk =(select top 1 product_structure_FK from #TS_PRODUCTS 
			where DIVIDED <> 0 and DIVIDED not like '-%' )
	
 ----------------------------------------------------------------------------------------------------------  	
				
				update pt
				set pt.TEXT = REPLACE(pt.TEXT, A.old, a.new)
				from epacube.PRODUCT_TEXT pt
				inner join 
				(SELECT @v_product_structure_fk prod
				, ListItem old
				,(select  STUFF(  listitem ,	(select len((SELECT epacube.ADD_BASIS_TOKEN_BY_SUBSTRING( NULL, NULL, 61, 1, NULL, 1 ,listitem ))
						)) + 1  ,1,'^')) new
				FROM utilities.SplitString ((select text from epacube.PRODUCT_TEXT where PRODUCT_STRUCTURE_FK = @v_product_structure_fk and @v_divided >0
				),'^')
				where len(ListItem) > 61) 
				a  on pt.PRODUCT_STRUCTURE_FK = a.prod
				
				--------------------------
				
				update #TS_PRODUCTS
				set DIVIDED = (DIVIDED - 1)
				where product_structure_FK = @v_product_structure_fk
				
				delete  from #TS_PRODUCTS 
				where product_structure_FK = @v_product_structure_fk
				and DIVIDED = 0
				
				
				-------------------------
			
			
END

-------------------------------------------------------------------------
 
--drop temp table if it exists
	IF object_id('tempdb..#TS_PRODUCTS') is not null
	   drop table #TS_PRODUCTS;
-----------------------------------------------------------------------	   
	   
	   



 SET @status_desc =  'finished execution of import].[Custom_Daycon_Text_Data]'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = NULL;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity bigINT;
	  DECLARE @ErrorState bigINT;

		SELECT 
			@ErrorMessage = 'Execution of [import].[Custom_Daycon_Text_Data has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = NULL;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END





































































































