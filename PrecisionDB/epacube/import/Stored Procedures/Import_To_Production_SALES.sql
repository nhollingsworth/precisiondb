﻿--A_epaMAUI_Import_To_Production_SALES

-- =============================================
-- Author:		<Gary Stone>
-- Create date: <October 22, 2009>
-- Description:	<Transform Sales Transaction History and load into import.import_order_data Table>
-- =============================================
CREATE PROCEDURE [import].[Import_To_Production_SALES]
@Maintain_Rolling_History int = 1, @Months_To_Keep int = Null, @Total_Refresh int = 1

AS
BEGIN

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint; 
	  DECLARE @status_desc        varchar(max); 
	   DECLARE @in_JOB_fk             bigint;  

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;



BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.IMPORT_TO_PRODUCTION_SALES', 
												@exec_id = @l_exec_no OUTPUT;
												

     
     set @in_JOB_fk = (select top 1 job_fk from import.IMPORT_SXE_SALES_TRANSACTIONS order by JOB_fk desc)
     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.IMPORT_SXE_SALES_TRANSACTIONS WITH (NOLOCK);

      SELECT @v_output_rows = COUNT(1)
        FROM import.IMPORT_SXE_SALES_TRANSACTIONS WITH (NOLOCK);

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_Job_fk, 'LOAD SALES HISTORY',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;






IF EXISTS (SELECT name FROM sysindexes WHERE name = 'idx_slstran')
drop index idx_slstran on import.IMPORT_SXE_SALES_TRANSACTIONS

create index idx_slstran on import.IMPORT_SXE_SALES_TRANSACTIONS(transtype, invoicedt)

IF object_id('tempdb..##Dups') is not null
drop table ##Dups;

Declare @Months_Sales_History_To_Keep as Int
If @Months_To_Keep is null
	Set @Months_Sales_History_To_Keep = Isnull((Select top 1 Months_Sales_History_To_Keep from dbo.MAUI_Defaults), 12)
else
	Set @Months_Sales_History_To_Keep = @Months_To_Keep
	
If @Maintain_Rolling_History = 1
	Begin
		DELETE from import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE
		where cast(invoicedt as datetime) < Cast(DATEADD(s,0,DATEADD(mm, DATEDIFF(m,0,getdate())-@Months_Sales_History_To_Keep,0)) as DATETIME)

		DELETE from import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE
		where cast(invoicedt as datetime)  >= (Select MIN(Invoicedt) from import.IMPORT_SXE_SALES_TRANSACTIONS)-- where transtype = 'stock order')
	End
Else
	Begin
		If @Total_Refresh = 1
		Truncate Table import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE
	End
	
Insert into import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE
Select *, Null, Null, Null from import.IMPORT_SXE_SALES_TRANSACTIONS where cast(invoicedt as datetime)  > isnull((Select Max(invoicedt) from import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE), '2001-01-01')
and cast(invoicedt as datetime) >= Cast(DATEADD(s,0,DATEADD(mm, DATEDIFF(m,0,getdate())-@Months_Sales_History_To_Keep,0)) as DATETIME)
and isnumeric(vendno) = 1

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'idx_slstranact')
drop index idx_slstranact on import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE

create index idx_slstranact on import.import_sxe_sales_transactions_active(prod, custno, shipto, whse, invoicedt, vendno, transtype, [prccostmult], slsrepout)

select 
Max(Import_FileName) MaxFN
, Cast(orderno as varchar(64)) orderno
, Cast(ordersuf as varchar(64)) ordersuf
, Cast([lineno] as varchar(64)) line_no
, COUNT(*) recs
into ##Dups
from import.IMPORT_SXE_SALES_TRANSACTIONS_active
group by 
orderno
, ordersuf
, [lineno]
having COUNT(*) > 1

Create Index idx_dups on ##Dups(MaxFN, OrderNo, Ordersuf, Line_no)

Delete Trans from import.IMPORT_SXE_SALES_TRANSACTIONS_active trans
inner join ##Dups Dups
	on cast(trans.orderno as varchar(64)) = dups.orderno and cast(trans.ordersuf as varchar(64)) = dups.ordersuf and cast(trans.[lineno] as varchar(64)) = dups.line_no
where trans.IMPORT_FILENAME <> dups.MaxFN

IF object_id('tempdb..##Dups') is not null
drop table ##Dups;

Update ST
Set cust_entity_structure_fk = Case When isnull(EI_c.ENTITY_STRUCTURE_FK, 0) = 0 then EI_p.ENTITY_STRUCTURE_FK else EI_c.ENTITY_STRUCTURE_FK end
, org_entity_structure_fk = EI_o.ENTITY_STRUCTURE_FK
, product_structure_fk = pi.product_structure_fk
from import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE ST with (nolock) 
	left join epacube.entity_identification EI_p on st.custno = EI_p.value and EI_p.entity_data_name_fk = 144010 and st.shipto is null
	left join epacube.entity_identification EI_c on st.custno + '-' + st.shipto = EI_c.value and EI_c.entity_data_name_fk = 144020 and st.shipto is not null
	left join epacube.entity_identification EI_o on st.whse = EI_o.value and EI_o.entity_data_name_fk = 141000
	left join epacube.PRODUCT_IDENTIFICATION pi on st.prod = pi.VALUE and pi.DATA_NAME_FK = 
		(Select data_name_fk from dbo.MAUI_Config_ERP with (nolock) where usage = 'Data_Name' and epaCUBE_Column = 'Product_ID' and ERP = (Select Value from epacube.epacube_params with (nolock) where name = 'ERP HOST'))

--Alter index idx_slstranact on import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE REBUILD

--Truncate Table import.IMPORT_SXE_SALES_TRANSACTIONS

 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                             where   name = 'LOAD SALES HISTORY' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where   name = 'LOAD SALES HISTORY' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

 SET @status_desc =  'finished execution of import.IMPORT_SALES_HISTORY'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_IMPORT_SALES_HISTORY has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END
