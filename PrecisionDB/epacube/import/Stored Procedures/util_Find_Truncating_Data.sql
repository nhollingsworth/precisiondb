﻿--A_Update_SP_util_B

/****** Object:  StoredProcedure [import].[util_Find_Truncating_Data]    Script Date: 04/09/2012 12:45:30 ******/
CREATE PROCEDURE [import].[util_Find_Truncating_Data] (
@Table_Name_Physical Varchar(64),
@Table_Name_Temp Varchar(64),
@Equal_Column_Number_Phys as int = Null ,
@Equal_Column_Number_Tmp as int = Null ,
@Job_FK_i as Numeric(10, 2) = Null
   )
   AS
BEGIN

Declare @Column_Name Varchar(64)
Declare @Data_Type Varchar(64)
Declare @Position Varchar(64)
Declare @Qry Varchar(max)
Declare @Qry1a Varchar(max)
Declare @Qry1b Varchar(max)
Declare @Qry1c Varchar(max)
Declare @Qry1d Varchar(max)

Declare @Qry2 Varchar(Max)
Declare @Qry2a Varchar(Max)
Declare @Qry2b Varchar(Max)
Declare @Qry2c Varchar(Max)
Declare @Qry2d Varchar(Max)
Declare @Qry3 Varchar(Max)
Declare @Column_Differential Varchar(8)
Declare @ColCount Numeric(10, 2)
Declare @Qry5 Varchar(Max)
Declare @Qry6 Varchar(Max)

Set @Column_Differential = Cast(isnull(@Equal_Column_Number_Tmp, 1) - isnull(@Equal_Column_Number_Phys, 1) as varchar(8))

IF object_id('tempdb..##DataLengths') is not null
drop table ##DataLengths

IF object_id('tempdb..##tmpProblems') is not null
drop table ##tmpProblems

Set @ColCount = (Select Max(Ordinal_Position) from tempdb.information_schema.columns where table_name = @Table_Name_Temp)

Set @Qry = 'Select'
Set @Qry2 = 'Select'
Set @Qry1a = ''
Set @Qry1b = ''
Set @Qry1c = ''
Set @Qry1d = ''
Set @Qry2a = ''
Set @Qry2b = ''
Set @Qry2c = ''
Set @Qry2d = ''

Declare DataLengthTest1 Cursor local for
Select Column_Name, Data_Type, Ordinal_Position from tempdb.information_schema.columns where table_name = @Table_Name_Temp and Ordinal_Position <= @ColCount / 4
	
OPEN DataLengthTest1;
FETCH NEXT FROM DataLengthTest1 INTO @Column_Name, @Data_Type, @Position
    
WHILE @@FETCH_STATUS = 0
Begin

Set @Qry1a = @Qry1a + ' Max(Len([' + @Column_Name + ']))' + '''' + @Column_Name + ''',' 
Set @Qry2a = @Qry2a + ' Cast(' + @Position + ' as int) Ordinal_Position, ' + '''' + @Column_Name + ''' Column_Name, (Select top 1 [' + @Column_Name + '] from ##DataLengths) Max_Length Union Select '

FETCH NEXT FROM DataLengthTest1 INTO @Column_Name, @Data_Type, @Position
End
Close DataLengthTest1;
Deallocate DataLengthTest1;

---------

Declare DataLengthTest2 Cursor local for
Select Column_Name, Data_Type, Ordinal_Position from tempdb.information_schema.columns where table_name = @Table_Name_Temp and Ordinal_Position > @ColCount / 4 and Ordinal_Position <= @ColCount / 4 * 2
	
OPEN DataLengthTest2;
FETCH NEXT FROM DataLengthTest2 INTO @Column_Name, @Data_Type, @Position
    
WHILE @@FETCH_STATUS = 0
Begin

Set @Qry1b = @Qry1b + ' Max(Len([' + @Column_Name + ']))' + '''' + @Column_Name + ''',' 
Set @Qry2b = @Qry2b + ' Cast(' + @Position + ' as int) Ordinal_Position, ' + '''' + @Column_Name + ''' Column_Name, (Select top 1 [' + @Column_Name + '] from ##DataLengths) Max_Length Union Select '

FETCH NEXT FROM DataLengthTest2 INTO @Column_Name, @Data_Type, @Position
End
Close DataLengthTest2;
Deallocate DataLengthTest2;

---------

Declare DataLengthTest3 Cursor local for
Select Column_Name, Data_Type, Ordinal_Position from tempdb.information_schema.columns where table_name = @Table_Name_Temp and Ordinal_Position > @ColCount / 4 * 2 and Ordinal_Position <= @ColCount / 4 * 3
	
OPEN DataLengthTest3;
FETCH NEXT FROM DataLengthTest3 INTO @Column_Name, @Data_Type, @Position
    
WHILE @@FETCH_STATUS = 0
Begin

Set @Qry1c = @Qry1c + ' Max(Len([' + @Column_Name + ']))' + '''' + @Column_Name + ''',' 
Set @Qry2c = @Qry2c + ' Cast(' + @Position + ' as int) Ordinal_Position, ' + '''' + @Column_Name + ''' Column_Name, (Select top 1 [' + @Column_Name + '] from ##DataLengths) Max_Length Union Select '

FETCH NEXT FROM DataLengthTest3 INTO @Column_Name, @Data_Type, @Position
End
Close DataLengthTest3;
Deallocate DataLengthTest3;

---------

Declare DataLengthTest4 Cursor local for
Select Column_Name, Data_Type, Ordinal_Position from tempdb.information_schema.columns where table_name = @Table_Name_Temp and Ordinal_Position > @ColCount / 4 * 3 
	
OPEN DataLengthTest4;
FETCH NEXT FROM DataLengthTest4 INTO @Column_Name, @Data_Type, @Position
    
WHILE @@FETCH_STATUS = 0
Begin

Set @Qry1d = @Qry1d + ' Max(Len([' + @Column_Name + ']))' + '''' + @Column_Name + ''',' 
Set @Qry2d = @Qry2d + ' Cast(' + @Position + ' as int) Ordinal_Position, ' + '''' + @Column_Name + ''' Column_Name, (Select top 1 [' + @Column_Name + '] from ##DataLengths) Max_Length Union Select '

FETCH NEXT FROM DataLengthTest4 INTO @Column_Name, @Data_Type, @Position
End
Close DataLengthTest4;
Deallocate DataLengthTest4;
Set @Qry = @Qry + Left(isnull(@Qry1a, '') + isnull(@Qry1b, '') + isnull(@Qry1c, '') + isnull(@Qry1d, ''), Len(isnull(@Qry1a, '') + isnull(@Qry1b, '') + isnull(@Qry1c, '') + isnull(@Qry1d, '')) - 1)
Set @Qry2 = @Qry2 + Left(isnull(@Qry2a, '') + isnull(@Qry2b, '') + isnull(@Qry2c, '') + isnull(@Qry2d, ''), Len(isnull(@Qry2a, '') + isnull(@Qry2b, '') + isnull(@Qry2c, '') + isnull(@Qry2d, '')) - 12)
--
Set @Qry = @Qry + ' into ##DataLengths from ' + @Table_Name_Temp

Exec (@Qry)

Set @Qry3 = '
Select tmp.Ordinal_Position, tmp.Column_Name, tmp.Max_Length, phys.Character_Maximum_Length
into ##tmpProblems
from (' + @qry2 + ') tmp inner join
(Select Ordinal_position, Column_name, isnull(Character_Maximum_Length, Numeric_Precision) Character_Maximum_Length from information_schema.columns where table_name =  ' + '''' + @Table_Name_Physical + ''') Phys
on tmp.Ordinal_position = phys.Ordinal_position + ' + @Column_Differential + ' or tmp.column_name = phys.column_name'

Exec (@Qry3)

Declare @Qry4 Varchar(Max)
Declare @Max_Length_Allowed varchar(8)
Declare @Max_Length_Actual varchar(8)

Set @Qry4 = 'Select '

Declare FindBadData Cursor local for
Select * from ##tmpProblems where Cast(Max_Length as int) > Cast(isnull(Character_Maximum_Length, Max_Length) as int)
	
OPEN FindBadData;
FETCH NEXT FROM FindBadData INTO @Position, @Column_Name, @Max_Length_Actual, @Max_Length_Allowed
    
WHILE @@FETCH_STATUS = 0
Begin

Set @Qry4 = 
@Qry4 + '''' + @Column_Name + '''' + ' ''Column Name-Bad Data'', ' + @Position + '' + ' ''Column Number'', ' + @Max_Length_Allowed + '' + ' ''Max Length Allowed'', ' + @Max_Length_Actual + '' + ' ''Max Actual Length'', Cast([' + @Column_Name + '] as Varchar(128)) Bad_Value, * from '  + @Table_Name_Temp + ' where Len([' + @Column_Name + ']) > ' + @Max_Length_Allowed + ' Union Select '

FETCH NEXT FROM FindBadData INTO @Position, @Column_Name, @Max_Length_Actual, @Max_Length_Allowed
End
Close FindBadData;
Deallocate FindBadData;

Set @Qry4 = Case When Isnull(@Qry4, '') > '' then Left(@Qry4,Len(@Qry4) - 13) end

Set @Qry5 = 'Select * into dbo.' + @Table_Name_Physical + '_Load_Errors_' + Replace(Cast(@Job_FK_i as varchar(32)), '.', '_') + ' from (' + @Qry4 + ') A'
Exec (@Qry5)

Set @Qry6 = 'Create Index idx_errs on dbo.' + @Table_Name_Physical + '_Load_Errors_' + Replace(Cast(@Job_FK_i as varchar(32)), '.', '_') + '([Column Name-Bad Data], [Column Number], [Max Length Allowed], [Max Actual Length], Bad_Value)'
Exec (@Qry6)

END
