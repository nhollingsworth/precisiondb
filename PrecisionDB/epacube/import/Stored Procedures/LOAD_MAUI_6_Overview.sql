﻿--A_epaMAUI_LOAD_MAUI_6_Overview

---- GHS  6/14/2016	Converted Global Temp Tables to Local Temp Tables

-- =============================================
-- Author:		Gary Stone
-- Create date: 9/7/2011
-- Description:	MAUI DATA LOAD
-- =============================================
CREATE PROCEDURE [import].[LOAD_MAUI_6_Overview] 
	@Job_FK bigint, @Job_FK_i Numeric(10, 2), @Defaults_To_Use as Varchar(64)
AS
BEGIN

	SET NOCOUNT ON;

--Declare @Job_FK bigint
--Declare @Job_FK_i as Numeric(10, 2)
--Declare @Defaults_To_Use as Varchar(64)
--set @Job_FK = (Select top 1 Job_fk from dbo.maui_basis order by Job_FK desc)
--Set @Job_FK_i = (Select top 1 Job_fk_i from dbo.maui_basis order by Job_FK_i desc)
--Set @Defaults_To_Use = 'Primary'

Declare @Job as Varchar(8)
Declare @Job_i as Varchar(8)
Declare @RbtsA as Varchar(8)
Declare @ColumnsA as varchar(Max)
Declare @GroupA as varchar(Max)
Declare @ClassA as varchar(Max)

Declare @ProjectionDays Int
Declare @HistoryDays Numeric(6, 0)
Declare @UseFileDates Int
Declare @WhatIF_ID Int
Declare @ERP varchar(32)

Set @ERP = (Select Value from epacube.epacube_params with (nolock) where name = 'ERP HOST')

insert into dbo.maui_log Select @Job_FK_i, 'Start Loading MAUI Cached Overview-No Groups', GetDate(), Null, Null

IF object_id('tempdb..#MBC') is not null
Drop table #MBC

Select 
	Case Isnull(Rebate_CB_Dollars_Cur, 0) + 
	Case @ERP when 'Eclipse' then 0 else Isnull(Rebate_BUY_Dollars_Cur, 0) end
	+ Isnull(Rebate_INCENTIVE_Dollars_Cur, 0) When 0 then 'WITHOUT Rebates' Else  'WITH Rebates' end 'Rebate Transactions'
	, * 
into #MBC 
from dbo.MAUI_Basis_Calcs with (nolock) 
where Job_FK_i = @Job_FK_i and isnull(sales_qty, 0) <> 0

Create Index idx_mbc on #MBC([Price_Rule_Type], [Sell_Price_Cust_Operation], [Whse], [Rebate Transactions])

Delete from dbo.MAUI_Basis_Overview where Job_FK_i = @Job_FK_i

Set @WhatIF_ID = 0
Set @HistoryDays = Cast((Select top 1 Sales_History_End_Date - Sales_History_Start_Date 
	from dbo.MAUI_BASIS where job_fk_i = @job_fk_i) as Numeric(6,0)) 
Set @UseFileDates = (Select top 1 UseFileDates from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use)
Set @ProjectionDays =
	Case when @UseFileDates = 0 then (Select top 1 ProjectionDays from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) else @HistoryDays	end
		
Declare @SQLcd as varchar(max)
Declare @HD as varchar(64)
Declare @PD as varchar(64)
Declare @WIF as varchar(64)
Declare @UFD as varchar(64)

DECLARE Overview Cursor local for
select CONFA.Configuration_ID
, CONFB.[Value]
, CONFB.[Value_Group]
, CONFB.Class 
, @WhatIF_ID
, @HistoryDays
, @UseFileDates
, @ProjectionDays
, @Job_FK
, @Job_FK_i
from dbo.MAUI_Configuration CONFA with (nolock), dbo.MAUI_Configuration CONFB  with (nolock)
Where CONFA.Class = 'GP_Calculation_Basis' and CONFB.Class in ('Overview_Cols_Assess', 'Overview_Cols_Recovery', 'Overview_Cols_Recovery_Baseline') and CONFB.[Value_Group] is null order by Configuration_ID

	     OPEN Overview;
         FETCH NEXT FROM Overview INTO @RbtsA, @ColumnsA, @GroupA, @ClassA, @WIF, @HD, @UFD, @PD, @Job, @Job_i
            
         WHILE @@FETCH_STATUS = 0
Begin
    
    --If @ClassA = 'Overview_Cols_Assess'
		Exec [import].[LOAD_MAUI_6a_Overview] @RbtsA, @ColumnsA, @GroupA, @ClassA, @WhatIF_ID, @HistoryDays, @UseFileDates, @ProjectionDays, @Job, @Job_i

       FETCH NEXT FROM Overview INTO @RbtsA, @ColumnsA, @GroupA, @ClassA, @WIF, @HD, @UFD, @PD, @Job, @Job_i

End
Close Overview;
Deallocate Overview;

insert into dbo.maui_log Select @Job_FK_i, 'MAUI Cached Overview Loaded-No Groups', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI Cached Overview-No Groups'), 108)), Null

DECLARE Overview_Groups Cursor local for
select CONFA.Configuration_ID
, CONFB.[Value]
, CONFB.[Value_Group]
, CONFB.Class 
, @WhatIF_ID
, @HistoryDays
, @UseFileDates
, @ProjectionDays
, @Job_FK
, @Job_FK_i
from dbo.MAUI_Configuration CONFA with (nolock), dbo.MAUI_Configuration CONFB  with (nolock)
Where CONFA.Class = 'GP_Calculation_Basis' and CONFB.Class in ('Overview_Cols_Assess', 'Overview_Cols_Recovery', 'Overview_Cols_Recovery_Baseline') and CONFB.[Value_Group] is not null order by Configuration_ID

	     OPEN Overview_groups;
         FETCH NEXT FROM Overview_Groups INTO @RbtsA, @ColumnsA, @GroupA, @ClassA, @WIF, @HD, @UFD, @PD, @Job, @Job_i
            
         WHILE @@FETCH_STATUS = 0
Begin
    
    --If @ClassA = 'Overview_Cols_Assess'
		Exec [import].[LOAD_MAUI_6b_Overview] @RbtsA, @ColumnsA, @GroupA, @ClassA, @WhatIF_ID, @HistoryDays, @UseFileDates, @ProjectionDays, @Job, @Job_i

       FETCH NEXT FROM Overview_Groups INTO @RbtsA, @ColumnsA, @GroupA, @ClassA, @WIF, @HD, @UFD, @PD, @Job, @Job_i

End
Close Overview_Groups;
Deallocate Overview_Groups;

insert into dbo.maui_log Select @Job_FK_i, 'MAUI Cached Overview Loaded-Groups', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'MAUI Cached Overview Loaded-No Groups'), 108)), Null

IF object_id('tempdb..#MBC') is not null
Drop table #MBC

insert into dbo.maui_log Select @Job_FK_i, 'MAUI Cached Overview Load Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI Cached Overview-No Groups'), 108)), (Select COUNT(*) from dbo.MAUI_Basis_Overview where Job_FK_i = @Job_FK_i)

EXEC import.LOAD_MAUI_6c_Ops_No_Contract @Job_FK_i
EXEC import.LOAD_MAUI_6d_Ops_Under_Contract @Job_FK_i
EXEC [import].[LOAD_MAUI_7_GP_STATS] @Job_FK, @Job_FK_i, @Defaults_To_Use

END
