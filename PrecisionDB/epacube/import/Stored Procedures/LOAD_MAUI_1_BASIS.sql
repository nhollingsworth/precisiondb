﻿--A_epaMAUI_LOAD_MAUI_1_BASIS

CREATE PROCEDURE [import].[LOAD_MAUI_1_BASIS] @Job_FK bigint, @Job_FK_i Numeric(10, 2), @Defaults_To_Use as Varchar(64)
AS
BEGIN

Set NOCOUNT On
--Declare @Job_FK bigint
--Declare @Job_FK_i as Numeric(10, 2)
--Declare @Defaults_To_Use as Varchar(64)
--set @Job_FK = 2171
--Set @Job_FK_i = 2170.01
--Set @Defaults_To_Use = 'Primary'
Declare @MinDate as datetime
Declare @MaxDate as datetime
Declare @O_R_Threshold_Max as Numeric(10, 2)
Declare @O_R_Threshold_Min as Numeric(10, 2)
Declare @Supercedes Varchar(1)
Declare @ERP Varchar(32)
Declare @PS_Table Varchar(Max)
Declare @SPC Varchar(Max)
Declare @Prod_ID_DN_FK Bigint
Declare @Job_FK_Fut bigint
Declare @CustomCode_Last_Step varchar(Max)

Set @O_R_Threshold_Max = 1 + isnull((Select max(abs(OverRide_Threshold)) from dbo.MAUI_Defaults with (nolock) where [Default_Name] = @Defaults_To_Use), 0)
Set @O_R_Threshold_Min = 1 - isnull((Select max(abs(OverRide_Threshold)) from dbo.MAUI_Defaults with (nolock) where [Default_Name] = @Defaults_To_Use), 0)
Set @Supercedes = Case When (Select top 1 chkSupercedes from dbo.MAUI_Defaults with (nolock) where Default_Name = @Defaults_To_Use) = -1 then 'S' else '' end
Set @ERP = (Select Value from epacube.epacube_params with (nolock) where name = 'ERP HOST')
Set @SPC = (Select SQL_CD from dbo.MAUI_Config_ERP with (nolock) where Temp_Table = '##spc' and ERP = @ERP and Usage = 'DynamicSQL')
Set @PS_Table = (Select SQL_CD from dbo.MAUI_Config_ERP with (nolock) where Temp_Table = '##Product_Status' and ERP = @ERP)
Set @Prod_ID_DN_FK = (Select data_name_fk from dbo.MAUI_Config_ERP with (nolock) where usage = 'Data_Name' and epaCUBE_Column = 'Product_ID' and ERP = @ERP)
Set @Job_FK_Fut = isnull((Select top 1 Job_FK_Fut from dbo.maui_parameters_jobs with (nolock) where what_if = 0 and job_fk_i = @Job_FK_i), 0)

IF object_id('tempdb..##CCD') is not null
drop table ##CCD;
IF object_id('tempdb..##MAUI_Hist') is not null
drop table ##MAUI_Hist;
IF object_id('tempdb..##Qtrs') is not null
drop table ##Qtrs;
IF object_id('tempdb..##Updates') is not null
drop table ##Updates;
IF object_id('tempdb..##MarginResults') is not null
drop table ##MarginResults;
IF object_id('tempdb..##MarginResultsAggregated') is not null
drop table ##MarginResultsAggregated;
IF object_id('tempdb..##Product_Status') is not null
drop table ##Product_Status;
IF object_id('tempdb..##MissingLookups') is not null
drop table ##MissingLookups
IF object_id('tempdb..##VMRA') is not null
drop table ##VMRA;
IF object_id('tempdb..##spc') is not null
drop table ##spc
IF object_id('tempdb..##Ors') is not null
drop table ##Ors
IF object_id('tempdb..##Trans') is not null
drop table ##Trans
IF object_id('tempdb..##VMRA') is not null
drop table ##VMRA

IF object_id('dbo.MAUI_Basis_Load_Errors_' + Replace(Cast(@Job_FK_i as varchar(32)), '.', '_')) is not null
Begin
	Declare @DrpTbl Varchar(Max)
	Set @DrpTbl = 'Drop Table dbo.MAUI_Basis_Load_Errors_' + Replace(Cast(@Job_FK_i as varchar(32)), '.', '_')
	Exec (@DrpTbl)
End

insert into dbo.maui_log Select @Job_FK_i, 'Start Loading MAUI Basis', GetDate(), Null, Null

Exec (@PS_Table)

Create Index idx_status on ##Product_Status(Product_Structure_FK, Product_Status)

Exec (@SPC)

Create Index idx_spcuom on ##SPC(product_structure_fk, org_entity_structure_fk)

Select Invoice_Date, Price_Unit, REPL_Cost_Unit, product_structure_fk, Org_Entity_Structure_FK, Cust_Entity_Structure_FK, isnull(Supl_Entity_Structure_FK, 0) supl_entity_structure_fk, Sales_Qty
into ##Ors
from marginmgr.ORDER_DATA_HISTORY odh with (nolock) where isnull(Price_Override_Ind, 0) <> 0 and isnull(Price_Rule_Xref, '0') = '0' and REPL_Cost_Unit > 0.1 --and Drank = 1

Create Index idxs on ##Ors(Invoice_Date, product_structure_fk, Org_Entity_Structure_FK, Cust_Entity_Structure_FK, Supl_Entity_Structure_FK)

Select product_structure_fk, Org_Entity_Structure_FK, Cust_Entity_Structure_FK, isnull(Supl_Entity_Structure_FK, 0) supl_entity_structure_fk, Count(*) Total_Transactions
into ##Trans
from marginmgr.ORDER_DATA_HISTORY odh with (nolock) where REPL_Cost_Unit > 0.1 
Group by product_structure_fk, Org_Entity_Structure_FK, Cust_Entity_Structure_FK, Supl_Entity_Structure_FK

Create Index idxs on ##Trans(product_structure_fk, Org_Entity_Structure_FK, Cust_Entity_Structure_FK, Supl_Entity_Structure_FK)

Select 
ec_p.job_fk
, ec_p.result_type_cr_fk
, ec_p.result_effective_date effective_date
, ec_p.product_structure_fk
, ec_p.org_entity_structure_fk
, ec_p.cust_entity_structure_fk
, isnull(ec_p.supl_entity_structure_fk, 0) supl_entity_structure_fk
, ec_p.margin_cost_basis_amt * isnull(isnull(spc_Org.spc_uom, SPC_1.spc_uom), 1) margin_cost_basis_amt
, ec_p.pricing_cost_basis_amt * isnull(isnull(spc_Org.spc_uom, SPC_1.spc_uom), 1) pricing_cost_basis_amt
, ec_p.sell_price_cust_amt * isnull(isnull(spc_Org.spc_uom, SPC_1.spc_uom), 1) sell_price_cust_amt
, Abs(isnull(ec_p.rebate_cb_amt, ec_rbtd.rebate_cb_amt)) * isnull(isnull(spc_Org.spc_uom, SPC_1.spc_uom), 1) rebate_cb_amt
, ec_p.rebate_buy_amt * isnull(isnull(spc_Org.spc_uom, SPC_1.spc_uom), 1) rebate_buy_amt
, (select value from epacube.epacube_params where name = 'PRICING COST BASIS') pricing_cost_basis_dn_fk
, (select value from epacube.epacube_params where name = 'MARGIN COST BASIS') margin_cost_basis_dn_fk
, ec_p.rules_fk sell_price_cust_rules_fk
, isnull(ec_cb.Rules_fk, ec_rbtd.Rules_fk) rebate_cb_rules_fk
, ec_buy.Rules_fk rebate_buy_rules_fk
, [pi].value product_id
, epacube.getEntityIdent ( ec_p.org_entity_structure_fk ) as whse_id
, epacube.getEntityIdent ( ec_p.cust_entity_structure_fk ) as cust_id
, epacube.getEntityIdent ( ec_p.supl_entity_structure_fk ) as vendor_id
, isnull(isnull(spc_Org.spc_uom, SPC_1.spc_uom), 1) SPC_UOM 
, pstatus.product_status
, pstatus.kit_type
, Cast(isnull(ec_p.product_structure_fk, 0) as varchar(32)) + '-' + Cast(isnull(ec_p.org_entity_structure_fk, 0) as varchar(32)) + '-' + 
	Cast(isnull(ec_p.cust_entity_structure_fk, 0) as varchar(32)) + '-' + Cast(isnull(ec_p.supl_entity_structure_fk, 0) as varchar(32)) Identifier
into ##VMRA
from synchronizer.EVENT_CALC ec_p with (nolock)
left join synchronizer.EVENT_CALC ec_cb with (nolock) on ec_p.product_structure_fk = ec_cb.product_structure_fk and ec_p.org_entity_structure_fk = ec_cb.org_entity_structure_fk
	and ec_p.cust_entity_structure_fk = ec_cb.cust_entity_structure_fk and isnull(ec_p.supl_entity_structure_fk, 0) = isnull(ec_cb.supl_entity_structure_fk, 0)
	and ec_p.result_type_cr_fk = ec_cb.result_type_cr_fk and ec_p.job_fk = ec_cb.job_fk and ec_cb.result_data_name_fk = 111501
	and ec_cb.JOB_FK in (@Job_FK, @Job_FK_Fut)
left join synchronizer.EVENT_CALC ec_buy with (nolock) on ec_p.product_structure_fk = ec_buy.product_structure_fk and ec_p.org_entity_structure_fk = ec_buy.org_entity_structure_fk
	and ec_p.cust_entity_structure_fk = ec_buy.cust_entity_structure_fk and isnull(ec_p.supl_entity_structure_fk, 0) = isnull(ec_buy.supl_entity_structure_fk, 0)
	and ec_p.result_type_cr_fk = ec_buy.result_type_cr_fk and ec_p.job_fk = ec_buy.job_fk and ec_buy.result_data_name_fk = 111502
	and ec_buy.JOB_FK in (@Job_FK, @Job_FK_Fut)
left join synchronizer.EVENT_CALC ec_rbtd with (nolock) on ec_p.product_structure_fk = ec_rbtd.product_structure_fk and ec_p.org_entity_structure_fk = ec_rbtd.org_entity_structure_fk
	and ec_p.cust_entity_structure_fk = ec_rbtd.cust_entity_structure_fk and isnull(ec_p.supl_entity_structure_fk, 0) = isnull(ec_rbtd.supl_entity_structure_fk, 0)
	and ec_p.result_type_cr_fk = ec_rbtd.result_type_cr_fk and ec_p.job_fk = ec_rbtd.job_fk and ec_rbtd.result_data_name_fk = 111401
	and ec_rbtd.JOB_FK in (@Job_FK, @Job_FK_Fut)
left join epacube.product_identification pi with (nolock) on ec_p.product_structure_fk = pi.product_structure_fk 
	and PI.DATA_NAME_FK = @Prod_ID_DN_FK
Left join ##spc spc_org on ec_p.product_structure_fk = spc_org.product_structure_fk and ec_p.org_entity_structure_fk = spc_org.org_entity_structure_fk and spc_org.org_entity_structure_fk <> 1
Left join ##spc spc_1 on ec_p.product_structure_fk = spc_1.product_structure_fk and spc_1.org_entity_structure_fk = 1
Left join ##product_status pstatus on ec_p.product_structure_fk = pstatus.product_structure_fk
where 1 = 1
and ec_p.result_data_name_fk = 111602 
and ec_p.job_fk in (@Job_FK, @Job_FK_Fut)

create index idx_vmra on ##VMRA(PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, CUST_ENTITY_STRUCTURE_FK, SUPL_ENTITY_STRUCTURE_FK)

Select
Sum(Sales_Qty) Sales_Qty
, Sum(Sales_Dollars_Ext) Sales_Ext_Hist
, Sum(abs(Sales_Qty) * REPL_Cost_Unit / uom_sell_pack_conv) Prod_Cost_Ext_Hist
, Sum(abs(Sales_Qty) * Rebate_Amt_Spa_Unit / uom_sell_pack_conv) Rebate_Amt_CB_Hist
, Sum(abs(Sales_Qty) * Rebate_Amt_BUY_Hist / uom_sell_pack_conv) Rebate_Amt_BUY_Hist
, Sum(abs(Sales_Qty) * (REPL_Cost_Unit - Rebate_Amt_Spa_Unit - Rebate_Amt_BUY_Hist) / uom_sell_pack_conv) COGS_Net_Ext
, Min(Invoice_Date) MinDate
, Max(Invoice_Date) MaxDate
, Max([Last Override Invoice Date]) 'Last Override Invoice Date'
, Max([last Override Price] / uom_sell_pack_conv) 'last Override Price'
, Max([Last Override Cost] / uom_sell_pack_conv) 'Last Override Cost' 
, Max([Override Qty]) 'Override Qty'
, Max([Override Transactions]) 'Override Transactions'
, Max([Total Transactions]) 'Total Transactions'
, Case Sum(isnull([Override Qty],0)) When 0 then 0
	else Sum(Price_Unit * Sales_Qty / uom_sell_pack_conv) / Sum(Sales_Qty) end 'Sell_Price_Override_Hist_Avg'
, Case Sum(isnull([Override Qty], 0)) When 0 then 0
	else Sum(REPL_Cost_Unit * Sales_Qty / uom_sell_pack_conv) / sum(sales_qty) end 'Unit_Cost_Of_Goods_Override_Hist_Avg'
, Case When Sum(isnull([Override Qty],0)) > 0 then 0
	else Sum(Price_Unit * Sales_Qty) / Sum(Sales_Qty) end 'Sell_Price_Calc_Hist_Avg'
, Case When Sum(isnull([Override Qty],0)) > 0 then 0
	else Sum(REPL_Cost_Unit * Sales_Qty / uom_sell_pack_conv) / sum(sales_qty) end 'Unit_Cost_Of_Goods_Calc_Hist_Avg'
, Max(Qty_Mode) Qty_Mode
, Max(Qty_Median) Qty_Median
, Max(Qty_Mean) Qty_Mean
, Max(Sell_Price_Cust_Host_Rule_Xref_Hist) Sell_Price_Cust_Host_Rule_Xref_Hist
, Max(Rebate_CB_Host_Rule_Xref_Hist) Rebate_CB_Host_Rule_Xref_Hist
, Max(Rebate_BUY_Host_Rule_Xref_Hist) Rebate_BUY_Host_Rule_Xref_Hist
, MAX(PRICE_CONTRACT_NO) CONTRACT_NO_HIST_LAST_PRICE
, MAX(REBATE_SPA_CONTRACT_NO) CONTRACT_NO_HIST_LAST_REBATE_CB
,Identifier, A.Product_Structure_fk, Org_entity_Structure_fk, Cust_Entity_Structure_fk--, Supl_Entity_Structure_fk
into ##MAUI_Hist
from (
select
isnull(Cast(ODH.Sales_Qty as Numeric(18, 4))  , 0.00) Sales_Qty
,isnull(Cast(Sales_Dollars_Ext as Numeric(18, 4)), 0.00) Sales_Dollars_Ext
,isnull(Cast(Price_Unit as Numeric(18, 4)), 0.00) Price_Unit
,isnull(Cast(REPL_Cost_Unit as Numeric(18, 4)),0.00) REPL_Cost_Unit
,isnull(Cast(Rebate_Amt_Spa_Unit as Numeric(18, 4)), 0.00) Rebate_Amt_Spa_Unit
,Cast(isnull((Select Basis1_Number from synchronizer.rules_action with (nolock) where rules_fk =
	(Select Rules_fk from synchronizer.rules_filter_set with (nolock) where update_user = 'HOST REBATE RULES LOAD' and org_filter_fk is null and cust_filter_fk is null 
	and supl_filter_fk is null and prod_filter_fk = 
	(Select rules_filter_id from synchronizer.rules_filter with (nolock) where entity_class_Cr_Fk = 10109 
	and data_name_fk = @Prod_ID_DN_FK and value1 = odh.product_ID_Host and Operator_CR_FK = 9000))), 0) / 100 * Cast(isnull([Repl_Cost_Unit], 0) as Numeric(18, 4)) as Numeric(18, 4)
	) 'Rebate_Amt_BUY_Hist'
,(Select top 1 Invoice_Date from ##Ors odh_i where odh_i.Product_Structure_fk = odh.Product_Structure_fk and odh_i.Org_Entity_Structure_fk = odh.Org_Entity_Structure_fk
	and odh_i.Cust_Entity_Structure_fk = odh.Cust_Entity_Structure_fk and isnull(odh_i.Supl_Entity_Structure_fk, 0) = isnull(odh.Supl_Entity_Structure_fk, 0) 
	and REPL_Cost_Unit > 0.1 and odh_i.Price_Unit not between vmra.sell_price_cust_amt * @O_R_Threshold_Min and vmra.sell_price_cust_amt * @O_R_Threshold_Max
	and odh.PRICE_RULE_XREF = '0'
	order by Invoice_Date Desc) 'Last Override Invoice Date'
,(Select top 1 price_unit from ##Ors odh_i where odh_i.Product_Structure_fk = odh.Product_Structure_fk and odh_i.Org_Entity_Structure_fk = odh.Org_Entity_Structure_fk
	and odh_i.Cust_Entity_Structure_fk = odh.Cust_Entity_Structure_fk and isnull(odh_i.Supl_Entity_Structure_fk, 0) = isnull(odh.Supl_Entity_Structure_fk, 0) 
	and REPL_Cost_Unit > 0.1 and odh_i.Price_Unit not between vmra.sell_price_cust_amt * @O_R_Threshold_Min and vmra.sell_price_cust_amt * @O_R_Threshold_Max
	and odh.PRICE_RULE_XREF = '0'
	Order by Invoice_Date Desc) 'Last Override Price'
,(Select top 1 REPL_Cost_Unit from ##Ors odh_i where odh_i.Product_Structure_fk = odh.Product_Structure_fk and odh_i.Org_Entity_Structure_fk = odh.Org_Entity_Structure_fk
	and odh_i.Cust_Entity_Structure_fk = odh.Cust_Entity_Structure_fk and isnull(odh_i.Supl_Entity_Structure_fk, 0) = isnull(odh.Supl_Entity_Structure_fk, 0) 
	and REPL_Cost_Unit > 0.1 and odh_i.Price_Unit not between vmra.sell_price_cust_amt * @O_R_Threshold_Min and vmra.sell_price_cust_amt * @O_R_Threshold_Max
	and odh.PRICE_RULE_XREF = '0'
	Order by Invoice_Date Desc) 'Last Override Cost'
,(Select Sum(Sales_Qty) from ##Ors odh_i where odh_i.Product_Structure_fk = odh.Product_Structure_fk and odh_i.Org_Entity_Structure_fk = odh.Org_Entity_Structure_fk
	and odh_i.Cust_Entity_Structure_fk = odh.Cust_Entity_Structure_fk and isnull(odh_i.Supl_Entity_Structure_fk, 0) = isnull(odh.Supl_Entity_Structure_fk, 0) 
	and REPL_Cost_Unit > 0.1 and odh_i.Price_Unit not between vmra.sell_price_cust_amt * @O_R_Threshold_Min and vmra.sell_price_cust_amt * @O_R_Threshold_Max
	and odh.PRICE_RULE_XREF = '0'
	) 'Override Qty'
,(Select Count(Product_Structure_FK) from ##Ors odh_i where odh_i.Product_Structure_fk = odh.Product_Structure_fk and odh_i.Org_Entity_Structure_fk = odh.Org_Entity_Structure_fk
	and odh_i.Cust_Entity_Structure_fk = odh.Cust_Entity_Structure_fk and isnull(odh_i.Supl_Entity_Structure_fk, 0) = isnull(odh.Supl_Entity_Structure_fk, 0) 
	and REPL_Cost_Unit > 0.1 and odh_i.Price_Unit not between vmra.sell_price_cust_amt * @O_R_Threshold_Min and vmra.sell_price_cust_amt * @O_R_Threshold_Max
	and odh.PRICE_RULE_XREF = '0'
	) 'Override Transactions'
,(Select Total_Transactions from ##Trans odh_i where 
	odh_i.Product_Structure_fk = odh.Product_Structure_fk and odh_i.Org_Entity_Structure_fk = odh.Org_Entity_Structure_fk
	and odh_i.Cust_Entity_Structure_fk = odh.Cust_Entity_Structure_fk and ISNULL(isnull(odh_i.Supl_Entity_Structure_fk, 0), 0) = ISNULL(isnull(odh.Supl_Entity_Structure_fk, 0), 0)
	) 'Total Transactions'
,Invoice_Date
, isnull(uom_sell_pack_conv, 1) uom_sell_pack_conv
,VMRA.Identifier Identifier
,ODH.Product_Structure_fk
,ODH.Org_entity_Structure_fk
,ODH.Cust_Entity_Structure_fk
--,isnull(odh.Supl_Entity_Structure_fk, 0)
,ODH.QTY_MODE
,ODH.QTY_MEDIAN
,ODH.QTY_MEAN
,(select top 1 price_rule_xref from marginmgr.ORDER_DATA_HISTORY with (nolock) where product_structure_fk = odh.product_structure_fk and org_entity_structure_fk = odh.org_entity_structure_fk 
	and cust_entity_structure_fk = odh.cust_entity_structure_fk and isnull(supl_entity_structure_fk, 0) = isnull(odh.Supl_Entity_Structure_fk, 0) 
		and price_rule_xref is not null order by invoice_date desc) sell_price_cust_host_rule_xref_hist

,(select top 1 REBATE_SPA_RULE_XREF from marginmgr.ORDER_DATA_HISTORY with (nolock) where product_structure_fk = odh.product_structure_fk and org_entity_structure_fk = odh.org_entity_structure_fk 
	and cust_entity_structure_fk = odh.cust_entity_structure_fk and isnull(supl_entity_structure_fk, 0) = isnull(odh.Supl_Entity_Structure_fk, 0) 
		and REBATE_SPA_RULE_XREF is not null order by invoice_date desc) REBATE_CB_HOST_RULE_XREF_HIST

,(select top 1 REBATE_BUY_RULE_XREF from marginmgr.ORDER_DATA_HISTORY with (nolock) where product_structure_fk = odh.product_structure_fk and org_entity_structure_fk = odh.org_entity_structure_fk 
	and cust_entity_structure_fk = odh.cust_entity_structure_fk and isnull(supl_entity_structure_fk, 0) = isnull(odh.Supl_Entity_Structure_fk, 0) 
		and REBATE_BUY_RULE_XREF is not null order by invoice_date desc) Rebate_BUY_Host_Rule_Xref_Hist

,(select top 1 REBATE_SPA_CONTRACT_NO from marginmgr.ORDER_DATA_HISTORY with (nolock) where product_structure_fk = odh.product_structure_fk and org_entity_structure_fk = odh.org_entity_structure_fk 
	and cust_entity_structure_fk = odh.cust_entity_structure_fk and isnull(supl_entity_structure_fk, 0) = isnull(odh.Supl_Entity_Structure_fk, 0) 
		and REBATE_SPA_CONTRACT_NO is not null order by invoice_date desc) REBATE_SPA_CONTRACT_NO

,(select top 1 PRICE_CONTRACT_NO from marginmgr.ORDER_DATA_HISTORY with (nolock) where product_structure_fk = odh.product_structure_fk and org_entity_structure_fk = odh.org_entity_structure_fk 
	and cust_entity_structure_fk = odh.cust_entity_structure_fk and isnull(supl_entity_structure_fk, 0) = isnull(odh.Supl_Entity_Structure_fk, 0) 
		and PRICE_CONTRACT_NO is not null order by invoice_date desc) PRICE_CONTRACT_NO

from marginmgr.ORDER_DATA_HISTORY odh with (nolock)
	left join ##Product_Status PS on odh.product_structure_fk = PS.product_structure_fk
	Left Join ##VMRA vmra on 
		odh.product_structure_fk = vmra.product_structure_fk
		and odh.Org_Entity_Structure_fk = vmra.Org_Entity_Structure_fk
		and odh.Cust_Entity_Structure_fk = vmra.Cust_Entity_Structure_fk
--		and isnull(odh.Supl_Entity_Structure_fk, 0) = vmra.Supl_Entity_Structure_fk
		and vmra.result_type_cr_fk = 710
	Where 1 = 1 
		and ((isnull(ps.Product_status, 'A') in ('A', 'I', @Supercedes) and @ERP = 'SXE') or (isnull(ps.Product_status, '1') between '1' and '8' and @ERP = 'ECLIPSE'))
		and PS.Kit_Type is null 
		and isnull(uom_sell_pack_conv, 1) <> 0
) A 
Group by identifier, A.Product_Structure_fk, A.Org_entity_Structure_fk, A.Cust_Entity_Structure_fk--, A.Supl_Entity_Structure_fk
Having Sum(Sales_Qty) <> 0

Create Index idx_hist on ##MAUI_Hist(Identifier, product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk, MinDate, MaxDate)

insert into dbo.maui_log Select @Job_FK_i, 'History File Created', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity =  'Start Loading MAUI Basis'), 108)), (Select COUNT(*) from ##MAUI_Hist)

Drop Table ##Product_Status

Set @MinDate = (Select Min(MinDate) from ##MAUI_Hist with (nolock) where ISDATE(MinDate) = 1)
Set @MaxDate = (Select Max(MaxDate) from ##MAUI_Hist with (nolock) where ISDATE(MaxDate) = 1)

Select
Qtrs.Product_Structure_fk
, Qtrs.Org_entity_Structure_fk
, Qtrs.Cust_Entity_Structure_fk
, Sum(Qty_Q1) Qty_Q1
, Sum(Qty_Q2) Qty_Q2
, Sum(Qty_Q3) Qty_Q3
, Sum(Qty_Q4) Qty_Q4
, Sum(Sales_Q1) Sales_Q1
, Sum(Sales_Q2) Sales_Q2
, Sum(Sales_Q3) Sales_Q3
, Sum(Sales_Q4) Sales_Q4
, Sum(Cost_Net_Q1) Cost_Net_Q1
, Sum(Cost_Net_Q2) Cost_Net_Q2
, Sum(Cost_Net_Q3) Cost_Net_Q3
, Sum(Cost_Net_Q4) Cost_Net_Q4
, Max(Date_Range_Q1) Date_Range_Q1
, Max(Date_Range_Q2) Date_Range_Q2
, Max(Date_Range_Q3) Date_Range_Q3
, Max(Date_Range_Q4) Date_Range_Q4
into ##Qtrs
From (
Select
Case When ODH.Invoice_Date >= @MaxDate - 91 and ODH.Invoice_Date <= @MaxDAte then isnull(ODH.Sales_Qty, 0) else 0 end Qty_Q1
, Case When ODH.Invoice_Date >= @MaxDate - 183 and ODH.Invoice_Date <= @MaxDAte - 92 then isnull(ODH.Sales_Qty, 0) else 0 end Qty_Q2
, Case When ODH.Invoice_Date >= @MaxDate - 275 and ODH.Invoice_Date <= @MaxDAte - 184 then isnull(ODH.Sales_Qty, 0) else 0 end Qty_Q3
, Case When ODH.Invoice_Date >= @MinDate and ODH.Invoice_Date <= @MaxDAte - 276 then isnull(ODH.Sales_Qty, 0) else 0 end Qty_Q4
, Case When ODH.Invoice_Date >= @MaxDate - 91 and ODH.Invoice_Date <= @MaxDAte then isnull(ODH.Sales_Dollars_Ext, 0) else 0 end Sales_Q1
, Case When ODH.Invoice_Date >= @MaxDate - 183 and ODH.Invoice_Date <= @MaxDAte - 92 then isnull(ODH.Sales_Dollars_Ext, 0) else 0 end Sales_Q2
, Case When ODH.Invoice_Date >= @MaxDate - 275 and ODH.Invoice_Date <= @MaxDAte - 184 then isnull(ODH.Sales_Dollars_Ext, 0) else 0 end Sales_Q3
, Case When ODH.Invoice_Date >= @MinDate and ODH.Invoice_Date <= @MaxDAte - 276 then isnull(ODH.Sales_Dollars_Ext, 0) else 0 end Sales_Q4
, Case When ODH.Invoice_Date >= @MaxDate - 91 and ODH.Invoice_Date <= @MaxDAte then isnull((odh.Repl_Cost_Unit - Rebate_Amt_SPA_Unit) * odh.Sales_Qty, 0) else 0 end Cost_Net_Q1
, Case When ODH.Invoice_Date >= @MaxDate - 183 and ODH.Invoice_Date <= @MaxDAte - 92 then isnull((odh.Repl_Cost_Unit - odh.Rebate_Amt_SPA_Unit) * odh.Sales_Qty, 0) else 0 end Cost_Net_Q2
, Case When ODH.Invoice_Date >= @MaxDate - 275 and ODH.Invoice_Date <= @MaxDAte - 184 then isnull((odh.Repl_Cost_Unit - odh.Rebate_Amt_SPA_Unit) * odh.Sales_Qty, 0) else 0 end Cost_Net_Q3
, Case When ODH.Invoice_Date >= @MinDate and ODH.Invoice_Date <= @MaxDAte - 276 then isnull((odh.Repl_Cost_Unit - odh.Rebate_Amt_SPA_Unit) * odh.Sales_Qty, 0) else 0 end Cost_Net_Q4
, CONVERT(VARCHAR(8), @MaxDate - 91, 1) + ' - ' +	CONVERT(VARCHAR(8), @MaxDAte, 1) 'Date_Range_Q1'
, CONVERT(VARCHAR(8), @MaxDate - 183, 1) + ' - ' +	CONVERT(VARCHAR(8), @MaxDAte - 92, 1) 'Date_Range_Q2'
, CONVERT(VARCHAR(8), @MaxDate - 275, 1) + ' - ' +	CONVERT(VARCHAR(8), @MaxDAte - 184, 1) 'Date_Range_Q3'
, CONVERT(VARCHAR(8), @MinDate, 1) + ' - ' +	CONVERT(VARCHAR(8), @MaxDAte - 276, 1) 'Date_Range_Q4'
,ODH.Product_Structure_fk
,ODH.Org_entity_Structure_fk
,ODH.Cust_Entity_Structure_fk
from marginmgr.ORDER_DATA_HISTORY odh with (nolock)
) Qtrs
Group by Product_Structure_fk, Org_entity_Structure_fk, Cust_Entity_Structure_fk

Create Index idx_fks on ##Qtrs(Product_Structure_fk, Org_entity_Structure_fk, Cust_Entity_Structure_fk)

Select VMRA.*
into ##MarginResults
from  ##VMRA VMRA
inner join ##MAUI_Hist Hist
on VMRA.identifier = Hist.identifier
Where 1 = 1
And Kit_Type is null
And (
	(isnull(Product_status, 'A') in ('A', 'I', @Supercedes) and @ERP = 'SXE') or (isnull(Product_status, '1') between '1' and '8' and @ERP = 'ECLIPSE')
	)
and isnull(margin_cost_basis_amt, 0) > 0 and vmra.JOB_FK in (@Job_FK, @Job_FK_Fut)

create index idx_ident on ##MarginResults(identifier, Sell_Price_Cust_Rules_FK, Rebate_CB_Rules_FK, Rebate_Buy_Rules_FK, PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, CUST_ENTITY_STRUCTURE_FK, SUPL_ENTITY_STRUCTURE_FK, Result_Type_CR_FK, Margin_Cost_Basis_DN_FK)

IF object_id('tempdb..##VMRA') is not null
drop table ##VMRA

IF object_id('tempdb..##Ors') is not null
drop table ##Ors

IF object_id('tempdb..##Trans') is not null
drop table ##Trans

Select 
VMRA.PRODUCT_STRUCTURE_FK
, VMRA.ORG_ENTITY_STRUCTURE_FK
, VMRA.CUST_ENTITY_STRUCTURE_FK
, VMRA.SUPL_ENTITY_STRUCTURE_FK
, Null 'GC'
, r.CONTRACT_NO 'CONTRACT_NO_PRICE'
, r_cb.CONTRACT_NO 'CONTRACT_NO_REBATE_CB'
, r_buy.CONTRACT_NO 'CONTRACT_NO_REBATE_BUY'
, dv_t.VALUE 'Customer Type'
, Host_Precedence_level 'Sell_Price_Cust_Level_Code'
, isnull(ecrb_price.BASIS_OPERAND_POSITION, 1)  'Price_Operand' --isnull(lvls.Price_Operand, dvP.Value) 'Price_Operand'
, isnull(ecrb_discount.BASIS_OPERAND_POSITION, 1)  'Discount_Operand' --isnull(lvls.Discount_Operand, dvD.Value) 'Discount_Operand'
, Case ecrb_price.basis_operand_filter_operator_cr_fk When 9020 then 1 else 0 end 'Qty_break_rule_ind'
, rao1.operand1
, rao1.operand2
, rao1.operand3
, rao1.operand4
, rao1.operand5
, rao1.operand6
, rao1.operand7
, rao1.operand8
, rao1.operand9
,Case When rao1.operand1 is not null then filter_value1 end OpPos1
,Case When rao1.operand2 is not null then filter_value2 end OpPos2
,Case When rao1.operand3 is not null then filter_value3 end OpPos3
,Case When rao1.operand4 is not null then filter_value4 end OpPos4
,Case When rao1.operand5 is not null then filter_value5 end OpPos5
,Case When rao1.operand6 is not null then filter_value6 end OpPos6
,Case When rao1.operand7 is not null then filter_value7 end OpPos7
,Case When rao1.operand8 is not null then filter_value8 end OpPos8
,Case When rao1.operand9 is not null then filter_value9 end OpPos9
,Cast(C.Sales_Qty as Numeric(18, 2)) Sales_Qty
,VMRA.Sell_Price_Cust_Amt
,VMRA.Margin_Cost_Basis_Amt
,Cast(VMRA.Effective_Date as datetime) Margin_Cost_Basis_Effective_Date
,VMRA.Sell_Price_Cust_Amt - VMRA.Margin_Cost_Basis_Amt Margin_Amt
, Case VMRA.Sell_Price_Cust_Amt when 0 then 0 else (VMRA.Sell_Price_Cust_Amt - VMRA.Margin_Cost_Basis_Amt) / VMRA.Sell_Price_Cust_Amt end Margin_Pct
--,(VMRA.Sell_Price_Cust_Amt - VMRA.Margin_Cost_Basis_Amt) / VMRA.Sell_Price_Cust_Amt Margin_Pct
,Margin_Cost_Basis_DN_FK
,VMRA.Rebate_CB_Amt
,VMRA.Margin_Cost_Basis_Amt - isnull(VMRA.Rebate_CB_Amt, 0) Rebated_Cost_CB_Amt
,VMRA.Sell_price_Cust_Amt - isnull(VMRA.Margin_Cost_Basis_Amt, 0) + isnull(VMRA.Rebate_CB_Amt, 0) Margin_Net_Amt 
, Case VMRA.Sell_Price_Cust_Amt when 0 then 0 else (VMRA.Sell_price_Cust_Amt - isnull(VMRA.Margin_Cost_Basis_Amt, 0) + isnull(VMRA.Rebate_CB_Amt, 0)) / VMRA.Sell_price_Cust_Amt end Margin_Net_Pct
--,(VMRA.Sell_price_Cust_Amt - isnull(VMRA.Margin_Cost_Basis_Amt, 0) + isnull(VMRA.Rebate_CB_Amt, 0)) / VMRA.Sell_price_Cust_Amt Margin_Net_Pct
,VMRA.Rebate_Buy_Amt 
,VMRA.Margin_Cost_Basis_Amt - isnull(VMRA.Rebate_Buy_Amt, 0) Rebated_Cost_Net_Amt 
,VMRA.Sell_price_Cust_Amt - isnull(VMRA.Margin_Cost_Basis_Amt, 0) + isnull(VMRA.Rebate_CB_Amt, 0) + isnull(VMRA.Rebate_BUY_Amt, 0) Margin_Net_Net_Amt 
, Case VMRA.Sell_Price_Cust_Amt when 0 then 0 else (VMRA.Sell_price_Cust_Amt - isnull(VMRA.Margin_Cost_Basis_Amt, 0) + isnull(VMRA.Rebate_CB_Amt, 0) + isnull(VMRA.Rebate_BUY_Amt, 0)) / VMRA.Sell_price_Cust_Amt end Margin_Net_Net_Pct
--,(VMRA.Sell_price_Cust_Amt - isnull(VMRA.Margin_Cost_Basis_Amt, 0) + isnull(VMRA.Rebate_CB_Amt, 0) + isnull(VMRA.Rebate_BUY_Amt, 0)) / VMRA.Sell_price_Cust_Amt Margin_Net_Net_Pct
,Sell_Price_Cust_Rules_FK
,Rebate_CB_Rules_FK
,Rebate_Buy_Rules_FK
,VMRA.result_type_cr_fk
,VMRA.Job_FK
,@MinDate 'Sales_History_Start_Date'
,@MaxDate 'Sales_History_End_Date'
,Case when @ERP = 'Eclipse' then Null When Cast(RP.HOST_PRECEDENCE_LEVEL as varchar(8)) in ('3', '4', '6') then (select value1 from synchronizer.RULES_FILTER with (nolock) where RULES_FILTER_ID = ecr_price.CUST_FILTER_FK) end 'Cust_Price_Type'
,Case when @ERP = 'Eclipse' then Null When Cast(RP.HOST_PRECEDENCE_LEVEL as varchar(8)) in ('3', '4', '6') then (select value1 from synchronizer.RULES_FILTER with (nolock) where RULES_FILTER_ID = ecr_rebate.CUST_FILTER_FK) end 'Cust_Rebate_Type'
,VMRA.Identifier
,C.Sales_Ext_Hist
,C.Prod_Cost_Ext_Hist
,C.Rebate_Amt_CB_Hist
,C.Rebate_Amt_BUY_Hist
,C.[Last Override Invoice Date]
,C.[last Override Price]
,C.[Last Override Cost]
,C.[Override Qty]
,C.[Override Transactions]
,C.[Total Transactions]
,C.[Sell_Price_Override_Hist_Avg]
,C.[Unit_Cost_Of_Goods_Override_Hist_Avg]
,C.[Sell_Price_Calc_Hist_Avg]
,C.[Unit_Cost_Of_Goods_Calc_Hist_Avg]
,C.Qty_Mode
,C.Qty_Median
,C.Qty_Mean
,C.Sell_Price_Cust_Host_Rule_Xref_Hist
,C.Rebate_CB_Host_Rule_Xref_Hist
,C.Rebate_BUY_Host_Rule_Xref_Hist
,Qtrs.Qty_Q1
,Qtrs.Qty_Q2
,Qtrs.Qty_Q3
,Qtrs.Qty_Q4
,Qtrs.Sales_Q1
,Qtrs.Sales_Q2
,Qtrs.Sales_Q3
,Qtrs.Sales_Q4
,Qtrs.Cost_Net_Q1
,Qtrs.Cost_Net_Q2
,Qtrs.Cost_Net_Q3
,Qtrs.Cost_Net_Q4
,Qtrs.Date_Range_Q1
,Qtrs.Date_Range_Q2
,Qtrs.Date_Range_Q3
,Qtrs.Date_Range_Q4
,vmra.spc_uom
,C.CONTRACT_NO_HIST_LAST_PRICE
,C.CONTRACT_NO_HIST_LAST_REBATE_CB
, Cast(VMRA.PRODUCT_STRUCTURE_FK as varchar(64)) + '-' + Cast(VMRA.ORG_ENTITY_STRUCTURE_FK as varchar(64)) + '-' + Cast(VMRA.CUST_ENTITY_STRUCTURE_FK as varchar(64)) Identifier_ODH
into ##MarginResultsAggregated
from ##MarginResults VMRA
Left Join ##MAUI_Hist C on VMRA.Identifier = C.Identifier
Left Join ##Qtrs Qtrs on Qtrs.Product_Structure_FK = C.Product_Structure_FK and Qtrs.Org_Entity_Structure_FK = C.Org_Entity_Structure_FK and Qtrs.Cust_Entity_Structure_FK = C.Cust_Entity_Structure_FK
Left Join Synchronizer.rules R with (nolock) on VMRA.Sell_Price_Cust_Rules_FK = R.Rules_ID
Left Join Synchronizer.rules_Action RA with (nolock) on R.Rules_id = RA.Rules_fk
Left Join Synchronizer.rules_Action_Operands RAO1 with (nolock) on RA.Rules_Action_ID = RAO1.Rules_Action_FK and basis_position = 1
Left Join Synchronizer.rules_precedence rp with (nolock) on r.rules_precedence_fk = rp.rules_precedence_id
left join synchronizer.event_calc ec_price with (nolock) on ec_price.product_structure_fk = VMRA.product_structure_fk and ec_price.org_entity_structure_fk = VMRA.org_entity_structure_fk 
	and ec_price.cust_entity_structure_fk = VMRA.cust_entity_structure_fk and isnull(ec_price.supl_entity_structure_fk, 0) = isnull(VMRA.supl_entity_structure_fk, 0) and ec_price.result_data_name_fk = 111602
	and VMRA.result_type_CR_FK = ec_price.result_type_CR_FK
	and ec_price.job_fk in (@Job_FK, @Job_FK_Fut)
left join synchronizer.event_calc_rules ecr_price with (nolock) on ec_price.event_id = ecr_price.event_fk and ecr_price.result_rank = 1
left join synchronizer.EVENT_CALC_RULES_BASIS ecrb_price with (nolock) on ecrb_price.event_rules_fk = ecr_price.event_rules_id and ecrb_price.BASIS_OPERAND is not null and ecr_price.result_rank = 1 and (ecrb_price.BASIS_OPERAND_FILTER_DN_FK = 244905 or ecrb_price.basis_operand_filter_operator_cr_fk = 9020)
		and (ecrb_price.basis_position is null or ecrb_price.basis_position = rao1.basis_position)
left join synchronizer.EVENT_CALC_RULES_BASIS ecrb_discount with (nolock) on ecrb_discount.event_rules_fk = ecr_price.event_rules_id and ecrb_discount.BASIS_OPERAND is not null and ecr_price.result_rank = 1 and (ecrb_price.BASIS_OPERAND_FILTER_DN_FK = 244908  or ecrb_price.basis_operand_filter_operator_cr_fk = 9020)
			and (ecrb_discount.basis_position is null or ecrb_discount.basis_position = rao1.basis_position)
left join synchronizer.event_calc ec_rebate with (nolock) on ec_rebate.product_structure_fk = VMRA.product_structure_fk and ec_rebate.org_entity_structure_fk = VMRA.org_entity_structure_fk 
	and ec_rebate.cust_entity_structure_fk = VMRA.cust_entity_structure_fk and isnull(ec_rebate.supl_entity_structure_fk, 0) = isnull(VMRA.supl_entity_structure_fk, 0) and ec_rebate.result_data_name_fk = 111501
	and VMRA.result_type_CR_FK = ec_rebate.result_type_CR_FK
	and ec_rebate.job_fk in (@Job_FK, @Job_FK_Fut)
left join synchronizer.event_calc_rules ecr_rebate with (nolock) on ec_rebate.event_id = ecr_rebate.event_fk and ecr_rebate.result_rank = 1
Left Join epacube.entity_category ec with (nolock) on C.cust_entity_structure_fk = ec.entity_structure_fk and ec.DATA_NAME_FK = 244905 --Cust Price Operand
left join epacube.data_value dvP with (nolock) on ec.data_value_fk = dvP.data_value_id and ec.DATA_NAME_FK = dvP.data_name_fk 
Left Join epacube.entity_category ecD with (nolock) on C.cust_entity_structure_fk = ecD.entity_structure_fk and ecD.DATA_NAME_FK = 244908 --Cust Discount Operand
left join epacube.data_value dvD with (nolock) on ecD.data_value_fk = dvD.data_value_id and ecD.data_name_fk = dvD.data_name_fk
Left Join epacube.entity_category ec_t with (nolock) on vmra.cust_entity_structure_fk = ec_t.entity_structure_fk and ec_t.data_name_fk = 244907 --Cust Type
left join epacube.data_value dv_t with (nolock) on ec_t.data_name_fk = dv_t.data_name_fk and ec_t.data_value_fk = dv_t.data_value_id
left join synchronizer.RULES r_cb with (nolock) on VMRA.REBATE_CB_RULES_FK = r_cb.RULES_ID
left join synchronizer.RULES r_buy with (nolock) on VMRA.REBATE_BUY_RULES_FK = r_buy.RULES_ID

create index idx_ident on ##MarginResultsAggregated(identifier, Identifier_ODH, Sell_Price_Cust_Rules_FK, Rebate_CB_Rules_FK, Rebate_Buy_Rules_FK, PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, CUST_ENTITY_STRUCTURE_FK, SUPL_ENTITY_STRUCTURE_FK, Result_Type_CR_FK, Margin_Cost_Basis_DN_FK)

drop table ##MarginResults

drop table ##Qtrs

Delete from dbo.MAUI_Basis where Job_FK_i = @job_fk_i

Declare @RegNM as varchar(64)

Exec [import].[LOAD_MAUI_1A_BASIS] @Job_FK, @Job_FK_i

insert into dbo.maui_log Select @Job_FK_i, 'Start Updating MAUI Basis', GetDate(), Null, Null

--Update Future Cost Changes - Amount and Effective Date 

--drop table ##Updates
Select ed1.Net_Value1_New, ed1.Value_Effective_Date, ed1.Product_Structure_fk, ed1.Org_Entity_Structure_FK, ed1.Event_Status_CR_FK 
into ##Updates 
from synchronizer.event_data ed1 with (nolock)
	inner join (Select Min(ed2.Value_Effective_Date) VED, ed2.Product_Structure_fk, ed2.Org_Entity_Structure_FK, ed2.Event_Status_CR_FK from synchronizer.event_data ed2 with (nolock) 
	Where ed2.data_name_fk = 231100 AND ed2.value_effective_date > getdate() and isnull(net_value1_new,0) > 0 group by ed2.product_structure_fk, ed2.Org_Entity_Structure_FK, ed2.Event_Status_CR_FK) Updates
on ed1.product_structure_fk = Updates.product_structure_fk and ed1.org_entity_structure_fk = Updates.org_entity_structure_fk and ed1.Event_Status_CR_FK = updates.Event_Status_CR_FK
Where ed1.data_name_fk = 231100 AND ed1.value_effective_date > getdate()- 10 and ed1.event_status_cr_fk <> 82

--
Create Index idx_upd on ##Updates(product_structure_fk, org_entity_structure_fk)
--
Update MB
Set Margin_Cost_Basis_Effective_Date_2 = Updates.Value_Effective_date
, Margin_Cost_Basis_Amt_2 = Updates.Net_Value1_New
from dbo.MAUI_Basis MB with (nolock) Inner Join ##Updates Updates
on MB.product_structure_fk = Updates.product_structure_fk and MB.org_entity_structure_fk = Updates.org_entity_structure_fk
where Updates.Net_Value1_New <> MB.Margin_Cost_Basis_Amt
and job_fk_i = @Job_FK_i

If (Select count(*) from dbo.MAUI_Rebate_Volume_Dollars) > 0
Begin
	Update MB
	Set Rebate_Vendor_Incentive_Percent = isnull(MRVD.Rebate_Percent_Annual, 0)
	, Rebate_Incentive_Amt_Hist = isnull(MRVD.Rebate_Percent_Annual * Hist.Prod_Cost_Ext_Hist, 0)
	, Rebate_Incentive_Amt_Cur = isnull(MRVD.Rebate_Percent_Annual * MB.Replacement_Cost, 0)
	, Rebate_Incentive_Amt_New = isnull(MRVD.Rebate_Percent_Annual * isnull(MB.Margin_Cost_Basis_Amt_2, MB.Replacement_Cost), 0)
	from
	dbo.MAUI_Basis MB with (nolock)
		Left Join dbo.MAUI_Rebate_Volume_Dollars MRVD with (nolock)
			on MB.Org_Vendor_ID = MRVD.Org_Vendor_ID
		Left Join ##MAUI_Hist Hist
			on MB.identifier = Hist.identifier
	where MB.Job_FK_i = @Job_FK_i
End

--Custom Code Start

Set @CustomCode_Last_Step = 
	(select Top 1 SQL_cd from dbo.MAUI_Config_ERP 
	where 1 = 1
	and ERP = (select value from epacube.epacube_params where APPLICATION_SCOPE_FK = 100 and [NAME] = 'ERP HOST')
	and usage = 'Customer_Customization'
	and [Table] = 'DBO.MAUI_BASIS'
	AND [Temp_Table] = 'LAST STEP'
	and epacube_customer = (select value from epacube.epacube_params where APPLICATION_SCOPE_FK = 100 and [NAME] = 'EPACUBE CUSTOMER'))

Set @CustomCode_Last_Step = Replace(Replace(@CustomCode_Last_Step, '@Job_FK_i', @Job_FK_i), '@Job_FK', @Job_FK)

If ISNULL(@CustomCode_Last_Step, '') > ''
insert into dbo.maui_log Select @Job_FK_i, 'Loading MAUI Customization-Basis Last Step', getdate(), Null, (Select COUNT(*) from dbo.MAUI_Basis where Job_FK_i = @Job_FK_i)

Exec(@CustomCode_Last_Step)

--Custom Code End

insert into dbo.maui_log Select @Job_FK_i, 'MAUI Basis Load Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI Basis'), 108)), (Select COUNT(*) from dbo.MAUI_Basis where Job_FK_i = @Job_FK_i)

IF object_id('tempdb..##CCD') is not null
drop table ##CCD;
IF object_id('tempdb..##MAUI_Hist') is not null
drop table ##MAUI_Hist;
IF object_id('tempdb..##Qtrs') is not null
drop table ##Qtrs;
IF object_id('tempdb..##Updates') is not null
drop table ##Updates;
IF object_id('tempdb..##MarginResults') is not null
drop table ##MarginResults;
IF object_id('tempdb..##MarginResultsAggregated') is not null
drop table ##MarginResultsAggregated;
IF object_id('tempdb..##Product_Status') is not null
drop table ##Product_Status;
IF object_id('tempdb..##MissingLookups') is not null
drop table ##MissingLookups
IF object_id('tempdb..##VMRA') is not null
drop table ##VMRA
IF object_id('tempdb..##spc') is not null
drop table ##spc

IF object_id('dbo.MAUI_Basis_Load_Errors_' + Replace(Cast(@Job_FK_i as varchar(32)), '.', '_')) is null
	Begin
		Exec import.LOAD_MAUI_1B_BASIS_QUALIFIERS @Job_FK, @Job_FK_i
		Exec import.LOAD_MAUI_2_Order_Data_History @Job_FK, @Job_FK_i, @Defaults_To_Use
	End
END
