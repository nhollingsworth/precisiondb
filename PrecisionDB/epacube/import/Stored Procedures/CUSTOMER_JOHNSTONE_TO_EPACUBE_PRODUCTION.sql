﻿--A_epaMAUI_JOHNSTONE_TO_EPACUBE_PRODUCTION
-- =============================================
-- Author:		<Gary Stone>
-- Create date: <March 21, 2014>
--	3-17-2015	Adjusted Unique_Key1 code to match import.import_to_production_pdsc value for round-trip Match-Up.
-- =============================================
CREATE PROCEDURE [import].[CUSTOMER_JOHNSTONE_TO_EPACUBE_PRODUCTION]
	@WhrClause Varchar(Max), @CreateRecordType Int, @WinUser Varchar(32) = '', @Resubmit int = 0
AS
BEGIN

	SET NOCOUNT ON;

--Declare @WhrClause Varchar(Max)
--Declare @CreateRecordType Int
--Declare @WinUser Varchar(32)
--Declare @Resubmit int

--Set @WhrClause = ' And CreateRecordType = ''Price'' and VendorNo = ''871'' and DataValidationStatus = 1 and StartDate = ''04/22/16'''
--Set @CreateRecordType = 1
--Set @WinUser = 'bgantt'
--Set @Resubmit = 0

IF object_id('tempdb..#Flyers') is not null
drop table #Flyers;

Declare @Out_Job_FK Bigint
Declare @Job_FK Bigint
Declare @SQLcd Varchar(Max)
Declare @Job_FK_vc Varchar(16)
Declare @JobName Varchar(128)
Declare @ServiceBrokerCall nvarchar(4000)

If @Resubmit = 1 and @CreateRecordType = 4
Begin
Set @SQLcd = '
Update Promo
Set Action_Status = -1
from [import].[IMPORT_MASS_MAINTENANCE_PROMO] Promo
	where 1 = 1
	and Action_Status = 3
	and StartDate is not null
	' + @WhrClause

	Exec(@SQLcd)
End

If @CreateRecordType > 2 --Identify all Promo records that are about to be processed
Begin
	Select 
	P3.Import_FileName
	, P3.Price_Sheet_ID
	, P3.Price_Sheet_Description
	, p3.ProductValue
	, (select Product_Structure_FK from epacube.product_identification where value = p3.ProductValue and data_name_fk = 110100) Product_Structure_FK
	, P3.Price_Sheet_Page P3_Price_Sheet_Page
	, P4.Price_Sheet_Page P4_Price_Sheet_Page
	, P6.Price_Sheet_Page P6_Price_Sheet_Page
	, P3.Flyer_Month P3_Flyer_Month
	, P3.StartDate Flyer_On_Start_Date
	, P6.StartDate End_User_Start_Date
	, P6.EndDate End_User_End_Date
	, P3.Action_Status P3_AS
	, P4.Action_Status P4_AS
	, P6.Action_status P6_AS
	, P3.StartDateCorpICSW
	into #Flyers
	from [import].[IMPORT_MASS_MAINTENANCE_PROMO] P3 with (nolock)
	left join [import].[IMPORT_MASS_MAINTENANCE_PROMO] P4 with (nolock) on P3.price_Sheet_ID = P4.Price_Sheet_ID and P3.ProductValue = P4.ProductValue and P4.intCreateRecordType  = 4
	left join [import].[IMPORT_MASS_MAINTENANCE_PROMO] P6 with (nolock) on P3.price_Sheet_ID = P6.Price_Sheet_ID and P3.ProductValue = P6.ProductValue and P6.intCreateRecordType  = 6
	where 1 = 1
	and (P3.StartDate is not null or P6.StartDate is not null)
	and P3.intCreateRecordType  = 3
	--and P4.intCreateRecordType  = 4
	--and P6.intCreateRecordType  = 6
	and (P3.Action_Status = -1 or P4.Action_Status = -1 or P6.Action_Status = -1)
	Group by
	P3.Import_FileName
	, P3.Price_Sheet_ID
	, P3.Price_Sheet_Description
	, p3.ProductValue
	, P3.Price_Sheet_Page
	, P4.Price_Sheet_Page
	, P6.Price_Sheet_Page
	, P3.Flyer_Month
	, P3.StartDate
	, P6.StartDate
	, P6.EndDate
	, P3.Action_Status
	, P4.Action_Status
	, P6.Action_status
	, P3.StartDateCorpICSW

	Create index idx_vals3 on #Flyers(Price_Sheet_ID, Product_Structure_FK,P3_Price_Sheet_Page, Flyer_On_Start_Date, End_User_Start_Date, End_User_End_Date, StartDateCorpICSW, P3_AS, P4_AS, P6_AS)
End

If @CreateRecordType = 1 --1 Store-specific Type 1 PD record created for all whses where a sell price exists; 1 record specific to Corp reflecting the Corp promo sell price (Fixed Price) where it exists, or 100% Base Price where no corp promo sell price exists
Begin
	--Create 1 record for each warehouse
	Set @SQLcd = '
	Insert into dbo.maui_rules_maintenance
	(
	 [RuleType], [LevelCode], [Result_Data_Name_FK], [Rule_Result_Type_CR_FK], [CustCriteria_DN_FK], [CustValue], [RULES_ACTION_OPERATION1_FK]
	 --, [Rule_Name], [Host_Rule_Xref]
	 , [Effective_Date], [Rules_Hierarchy_FK], [Rules_Structure_FK], [Rules_Schedule_FK], [Rules_Precedence_FK], [Rule_Precedence]
	 --, [Unique_Key1]
	 , [ProdCriteria_DN_FK], [ProdValue]
	 , [Operand_1_1], [Filter_Value_1_1], [Filter_Value_1_2], [Filter_Value_1_3], [Filter_Value_1_4], [Filter_Value_1_5], [Filter_Value_1_6], [Filter_Value_1_7], [Filter_Value_1_8], [Filter_Value_1_9]
	 --, WhseValue
	 --, Org_Entity_Structure_FK
	 , [Record_Status_CR_FK], [Update_User], [Create_User], [Update_Timestamp], [Create_Timestamp], [Transferred_To_ERP], [Transferred_To_Rules], [IMPORT_MASS_MAINTENANCE_PRICE_REBATE_FK]
	)
	Select 
	''PRICE RECORD'' RuleType
	, 1 ''LevelCode''
	, 111602 Result_data_name_fk
	--, 313 Rule_result_type_cr_fk
	, Case when Cast(StartDate as date) > getdate() then 711 else 710 end Rule_result_type_cr_fk
--	, 144010 CustCriteria_DN_FK
	, isnull((Select top 1 entity_data_name_fk from epacube.entity_identification with (nolock) where data_name_fk = 144111 and value = PR.[CustValue]), 144111) CustCriteria_DN_FK
	, CustValue
	, 1021 Rules_action_operation1_FK
	--Rule_Name
	--Host_Rue_XRef
	, StartDate Effective_Date
	, 20110 Rules_Hierarchy_FK
	, 201102 Rules_Structure_FK
	, 2011021 Rules_Schedule_FK
	, 1014 Rules_Precedence_FK
	, 114 Rule_Precedence
	--Unique_Key1
	, 110100 PRodCriteria_DN_FK
	, ProductValue
	, StoreSellPrice operand_1_1
	, 1 Filter_Value_1_1
	, 2
	, 3
	, 4
	, 5
	, 6
	, 7
	, 8
	, 9
	--, ei.value WhseValue
	--, ei.entity_structure_fk Org_Entity_Structure_FK
	, 1 record_status_cr_fk
	, ''' + @WinUser + ''' Update_User
	, ''' + @WinUser + ''' Create_User
	, getdate() Update_Timestamp
	, getdate() Create_Timestamp
	, 0 Transferred_To_ERP
	, 0 Transferred_To_Rules
	, IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID
	from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] PR with (nolock)
	--cross join epacube.epacube.entity_identification ei with (nolock) 
	where 1 = 1
	--and ei.data_name_fk = 141111 
	--and ei.value not in (''all whse'', ''corp'', ''ZNat'', ''MDC'')
	and Action_Status = -1
	and StartDate is not null
	and isnull(Transferred_To_ERP, 0) = 0
	and isnull(StoreSellPrice, 0) <> 0
	' + @WhrClause

	Exec(@SQLcd)

	Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
	Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause

	--Create Corp Record
	Set @SQLcd = '
	Insert into dbo.maui_rules_maintenance
	(
	 [RuleType], [LevelCode], [Result_Data_Name_FK], [Rule_Result_Type_CR_FK], [CustCriteria_DN_FK], [CustValue], [RULES_ACTION_OPERATION1_FK]
	 --, [Rule_Name], [Host_Rule_Xref]
	 , [Effective_Date], [Rules_Hierarchy_FK], [Rules_Structure_FK], [Rules_Schedule_FK], [Rules_Precedence_FK], [Rule_Precedence]
	 --, [Unique_Key1]
	 , [ProdCriteria_DN_FK], [ProdValue]
	 , Basis_Calc_DN_FK
	 , [Operand_1_1], [Filter_Value_1_1], [Filter_Value_1_2], [Filter_Value_1_3], [Filter_Value_1_4], [Filter_Value_1_5], [Filter_Value_1_6], [Filter_Value_1_7], [Filter_Value_1_8], [Filter_Value_1_9]
	 , WhseValue
	 , Org_Entity_Structure_FK
	 , [Record_Status_CR_FK], [Update_User], [Create_User], [Update_Timestamp], [Create_Timestamp], [Transferred_To_ERP], [Transferred_To_Rules], [IMPORT_MASS_MAINTENANCE_PRICE_REBATE_FK]
	)
	Select 
	''PRICE RECORD'' RuleType
	, 1 ''LevelCode''
	, 111602 Result_data_name_fk
	, Case when Cast(StartDate as date) > getdate() then 711 else 710 end Rule_result_type_cr_fk
	--, 144010 CustCriteria_DN_FK
	, isnull((Select top 1 entity_data_name_fk from epacube.entity_identification with (nolock) where data_name_fk = 144111 and value = PR.[CustValue]), 144111) CustCriteria_DN_FK
	, CustValue
	, Case when isnull(CorpSellPrice, 0) <> 0 then 1021 else 1031 end Rules_action_operation1_FK
	--Rule_Name
	--Host_Rue_XRef
	, StartDate Effective_Date
	, 20110 Rules_Hierarchy_FK
	, 201102 Rules_Structure_FK
	, 2011021 Rules_Schedule_FK
	, 1010 Rules_Precedence_FK
	, 110 Rule_Precedence
	--Unique_Key1
	, 110100 PRodCriteria_DN_FK
	, ProductValue
	, Case when isnull(CorpSellPrice, 0) <> 0 then Null else 100100417 end Basis_Calc_DN_FK
	, Case when isnull(CorpSellPrice, 0) <> 0 then CorpSellPrice else 1 end operand_1_1
	, 1 Filter_Value_1_1
	, 2
	, 3
	, 4
	, 5
	, 6
	, 7
	, 8
	, 9
	, ei.value WhseValue
	, ei.entity_structure_fk Org_Entity_Structure_FK
	, 1 record_status_cr_fk
	, ''' + @WinUser + ''' Update_User
	, ''' + @WinUser + ''' Create_User
	, getdate() Update_Timestamp
	, getdate() Create_Timestamp
	, 0 Transferred_To_ERP
	, 0 Transferred_To_Rules
	, IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID
	from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] PR with (nolock)
	cross join epacube.epacube.entity_identification ei with (nolock) 
	where 1 = 1
	and ei.data_name_fk = 141111 
	and ei.value in (''corp'')
	and Action_Status = -1
	and StartDate is not null
	and isnull(Transferred_To_ERP, 0) = 0
	--and isnull(CorpSellPrice, 0) <> 0
	' + @WhrClause

	Exec(@SQLcd)

	Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
	Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause

	Set @SQLcd = '
	Update PR
	Set Action_Status = 1
	from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] PR
	Where Action_Status = -1
	and isnull(Transferred_To_ERP, 0) = 0
	and (isnull(StoreSellPrice, 0) <> 0 or isnull(CorpSellPrice, 0) <> 0)
	' + @WhrClause

	Exec(@SQLcd)

End
Else If @CreateRecordType = 2  --1 rebate record for all whses; 1 for corp set to Fixed Amount = 0
Begin
	Set @SQLcd = '
	Insert into dbo.maui_rules_maintenance
	(
	 [RuleType], [LevelCode], [Result_Data_Name_FK], [Rule_Result_Type_CR_FK], [CustCriteria_DN_FK], [CustValue], [RULES_ACTION_OPERATION1_FK]
	 --, [Rule_Name], [Host_Rule_Xref]
	 , [Effective_Date], [Rules_Hierarchy_FK], [Rules_Structure_FK], [Rules_Schedule_FK], [Rules_Precedence_FK], [Rule_Precedence]
	 --, [Unique_Key1]
	 , [ProdCriteria_DN_FK], [ProdValue]
	 , Basis_Calc_DN_FK
	 --, Basis1_DN_FK
	 , [Operand_1_1], [Filter_Value_1_1]

	 , WhseValue
	 , Org_Entity_Structure_FK
	 , Vendor_ID
	 , Supl_Entity_Structure_FK
	 , [Record_Status_CR_FK], [Update_User], [Create_User], [Update_Timestamp], [Create_Timestamp], [Transferred_To_ERP], [Transferred_To_Rules], [IMPORT_MASS_MAINTENANCE_PRICE_REBATE_FK]
	)
	Select 
	''Rebate RECORD'' RuleType
	, 1 ''LevelCode''
	, 111501 Result_data_name_fk
	, Case when Cast(StartDate as date) > getdate() then 711 else 710 end Rule_result_type_cr_fk
	, isnull((Select top 1 entity_data_name_fk from epacube.entity_identification with (nolock) where data_name_fk = 144111 and value = PR.[CustValue]), 144111) CustCriteria_DN_FK
	, CustValue
	--, Case ei.value when ''Corp'' then 1021 else 1291 end Rules_action_operation1_FK
	, Case when ei.value = ''Corp'' Or Isnull([DC Multiplier], 0) = 0 or ClaimBackType = 3 then 1021 else 1291 end Rules_action_operation1_FK
	, StartDate Effective_Date
	, 20400 Rules_Hierarchy_FK
	, 204001 Rules_Structure_FK
	, 2040011 Rules_Schedule_FK
	, Case ei.value when ''Corp'' then 1321 else 1325 end Rules_Precedence_FK
	, Case ei.value when ''Corp'' then 1109 else 1113 end Rule_Precedence
	, 110100 PRodCriteria_DN_FK
	, ProductValue
	--, Case when ei.value = ''Corp'' then Null 
	, Case when ei.value = ''Corp'' Or Isnull([DC Multiplier], 0) = 0 then Null 
		When claimbacktype = 1 then 100100531
		When claimbacktype = 2 then 100100417
		else Null end Basis_Calc_DN_FK
	--, Case when ei.value = ''Corp'' then 0 else ClaimBackEndValue end operand_1_1
	, Case when ei.value = ''Corp'' Or Isnull([DC Multiplier], 0) = 0 then 0 else ClaimBackEndValue end operand_1_1
	, 1 Filter_Value_1_1
	, Case ei.value when ''Corp'' then ei.value end WhseValue
	, Case ei.value when ''Corp'' then (Select entity_structure_fk from epacube.entity_identification with (nolock) where data_name_fk = 141111 and value = ''corp'') end Org_Entity_Structure_FK
	, VendorNo
	, (select entity_structure_fk from epacube.entity_identification with (nolock) where entity_data_name_fk = 143000 and data_name_fk = 143110 and value = PR.VendorNo) supl_entity_structure_fk
	, 1 record_status_cr_fk
	, ''' + @WinUser + ''' Update_User
	, ''' + @WinUser + ''' Create_User
	, getdate() Update_Timestamp
	, getdate() Create_Timestamp
	, 0 Transferred_To_ERP
	, 0 Transferred_To_Rules
	, IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID
	from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] PR with (nolock)
	cross join epacube.epacube.entity_identification ei with (nolock) 
	where 1 = 1
	and ei.data_name_fk = 141111 
	and ei.value in (''ADC'', ''Corp'')
	and PR.Action_Status = -1
	and StartDate is not null
	and isnull(PR.Transferred_To_ERP, 0) = 0
	and isnull(claimbacktype, 0) <> 0
	' + @WhrClause

	Exec(@SQLcd)

	Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
	Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause

	Set @SQLcd = '
	Update PR
	Set Action_Status = 1
	from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] PR
	Where Action_Status = -1
	and isnull(Transferred_To_ERP, 0) = 0
	and isnull(ClaimbackAction, '''') <> ''''
	' + @WhrClause

	Exec(@SQLcd)

	--Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
	--Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause

End
Else If @CreateRecordType = 4 --1 Promo Type 7 PD record created for all whses where a sell price exists; 1 Corp-Specific Promo record created reflecting either the Corp Sell Price or 100% Base Price
Begin

	Set @SQLcd = '
	Insert into dbo.maui_rules_maintenance
	(
	 [RuleType], [LevelCode], [Result_Data_Name_FK], [Rule_Result_Type_CR_FK], [CustCriteria_DN_FK], [CustValue], [RULES_ACTION_OPERATION1_FK]
	 --, [Rule_Name], [Host_Rule_Xref]
	 , [Effective_Date], [End_Date], [Rules_Hierarchy_FK], [Rules_Structure_FK], [Rules_Schedule_FK], [Rules_Precedence_FK], [Rule_Precedence]
	 --, [Unique_Key1]
	 , [ProdCriteria_DN_FK], [ProdValue]
	 , [Operand_1_1], [Filter_Value_1_1], [Filter_Value_1_2], [Filter_Value_1_3], [Filter_Value_1_4], [Filter_Value_1_5], [Filter_Value_1_6], [Filter_Value_1_7], [Filter_Value_1_8], [Filter_Value_1_9]
	 --, WhseValue
	 --, Org_Entity_Structure_FK
	 , [Record_Status_CR_FK], [Update_User], [Create_User], [Update_Timestamp], [Create_Timestamp], [Transferred_To_ERP], [Transferred_To_Rules], [IMPORT_MASS_MAINTENANCE_PROMO_FK], Promo
	 , Reference, Contract_NO
	)
	Select 
	''PRICE RECORD'' RuleType
	, 7 ''LevelCode''
	, 111602 Result_data_name_fk
	, Case when Cast(StartDate as date) > getdate() then 711 else 710 end Rule_result_type_cr_fk
	, Null CustCriteria_DN_FK
	, Null CustValue
	, 1021 Rules_action_operation1_FK
	--Rule_Name
	--Host_Rue_XRef
	, StartDate Effective_Date
	, EndDate EndDate
	, 20110 Rules_Hierarchy_FK
	, 201101 Rules_Structure_FK
	, 2011017 Rules_Schedule_FK
	, 1134 Rules_Precedence_FK
	, 706 Rule_Precedence
	--Unique_Key1
	, 110100 PRodCriteria_DN_FK
	, ProductValue
	, Base_Price Operand_1_1
	, 1 Filter_Value_1_1
	, 2
	, 3
	, 4
	, 5
	, 6
	, 7
	, 8
	, 9
	--, ei.value WhseValue
	--, ei.entity_structure_fk Org_Entity_Structure_FK
	, 1 record_status_cr_fk
	, ''' + @WinUser + ''' Update_User
	, ''' + @WinUser + ''' Create_User
	, getdate() Update_Timestamp
	, getdate() Create_Timestamp
	, 0 Transferred_To_ERP
	, 0 Transferred_To_Rules
	, IMPORT_MASS_MAINTENANCE_PROMO_ID
	, 1 Promo_Ind
	, Cast(Price_Sheet_ID as varchar(32)) + ''-'' + [Price_Sheet_Description]
	, [Flyer_Month]
	from [import].[IMPORT_MASS_MAINTENANCE_PROMO] with (nolock)
	--cross join epacube.epacube.entity_identification ei with (nolock) 
	where 1 = 1
	--ei.data_name_fk = 141111 
	--and ei.value not in (''all whse'', ''corp'', ''ZNat'', ''MDC'')
	and Action_Status = -1
	and StartDate is not null
	and isnull(Transferred_To_ERP, 0) = 0
	and isnull(Base_Price, 0) <> 0
	' + @WhrClause

	Exec(@SQLcd)

	Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
	Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause

	Set @SQLcd = '
	Insert into dbo.maui_rules_maintenance
	(
	 [RuleType], [LevelCode], [Result_Data_Name_FK], [Rule_Result_Type_CR_FK], [CustCriteria_DN_FK], [CustValue], [RULES_ACTION_OPERATION1_FK]
	 --, [Rule_Name], [Host_Rule_Xref]
	 , [Effective_Date], [End_Date], [Rules_Hierarchy_FK], [Rules_Structure_FK], [Rules_Schedule_FK], [Rules_Precedence_FK], [Rule_Precedence]
	 --, [Unique_Key1]
	 , [ProdCriteria_DN_FK], [ProdValue], [Basis_Calc_DN_FK]
	 , [Operand_1_1], [Filter_Value_1_1], [Filter_Value_1_2], [Filter_Value_1_3], [Filter_Value_1_4], [Filter_Value_1_5], [Filter_Value_1_6], [Filter_Value_1_7], [Filter_Value_1_8], [Filter_Value_1_9]
	 , WhseValue
	 , Org_Entity_Structure_FK
	 , [Record_Status_CR_FK], [Update_User], [Create_User], [Update_Timestamp], [Create_Timestamp], [Transferred_To_ERP], [Transferred_To_Rules], [IMPORT_MASS_MAINTENANCE_PROMO_FK], Promo
	 , Reference, Contract_NO
	)
	Select 
	''PRICE RECORD'' RuleType
	, 7 ''LevelCode''
	, 111602 Result_data_name_fk
	, Case when Cast(StartDate as date) > getdate() then 711 else 710 end Rule_result_type_cr_fk
	, Null CustCriteria_DN_FK
	, Null CustValue
	, 1031 Rules_action_operation1_FK
	--Rule_Name
	--Host_Rue_XRef
	, StartDate Effective_Date
	, EndDate EndDate
	, 20110 Rules_Hierarchy_FK
	, 201101 Rules_Structure_FK
	, 2011017 Rules_Schedule_FK
	, 1134 Rules_Precedence_FK
	, 706 Rule_Precedence
	--Unique_Key1
	, 110100 PRodCriteria_DN_FK
	, ProductValue
	, 100100417
	, 1 Operand_1_1
	, 1 Filter_Value_1_1
	, 2
	, 3
	, 4
	, 5
	, 6
	, 7
	, 8
	, 9
	, ei.value WhseValue
	, ei.entity_structure_fk Org_Entity_Structure_FK
	, 1 record_status_cr_fk
	, ''' + @WinUser + ''' Update_User
	, ''' + @WinUser + ''' Create_User
	, getdate() Update_Timestamp
	, getdate() Create_Timestamp
	, 0 Transferred_To_ERP
	, 0 Transferred_To_Rules
	, IMPORT_MASS_MAINTENANCE_PROMO_ID
	, 1 Promo_Ind
	, Cast(Price_Sheet_ID as varchar(32)) + ''-'' + [Price_Sheet_Description]
	, [Flyer_Month]
	from [import].[IMPORT_MASS_MAINTENANCE_PROMO] with (nolock)
	cross join epacube.epacube.entity_identification ei with (nolock) 
	where 1 = 1
	and ei.data_name_fk = 141111 
	and ei.value in (''CORP'')
	and Action_Status = -1
	and StartDate is not null
	and isnull(Transferred_To_ERP, 0) = 0
	and isnull(Base_Price, 0) <> 0
	and isnull(Corp_Price, 0) = 0
	' + @WhrClause

	Exec(@SQLcd)

	Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
	Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause
	
	Set @SQLcd = '
	Insert into dbo.maui_rules_maintenance
	(
	 [RuleType], [LevelCode], [Result_Data_Name_FK], [Rule_Result_Type_CR_FK], [CustCriteria_DN_FK], [CustValue], [RULES_ACTION_OPERATION1_FK]
	 --, [Rule_Name], [Host_Rule_Xref]
	 , [Effective_Date], [End_Date], [Rules_Hierarchy_FK], [Rules_Structure_FK], [Rules_Schedule_FK], [Rules_Precedence_FK], [Rule_Precedence]
	 --, [Unique_Key1]
	 , [ProdCriteria_DN_FK], [ProdValue]
	 , Basis_Calc_DN_FK
	 , [Operand_1_1], [Filter_Value_1_1], [Filter_Value_1_2], [Filter_Value_1_3], [Filter_Value_1_4], [Filter_Value_1_5], [Filter_Value_1_6], [Filter_Value_1_7], [Filter_Value_1_8], [Filter_Value_1_9]
	 , WhseValue
	 , Org_Entity_Structure_FK
	 , [Record_Status_CR_FK], [Update_User], [Create_User], [Update_Timestamp], [Create_Timestamp], [Transferred_To_ERP], [Transferred_To_Rules], [IMPORT_MASS_MAINTENANCE_PROMO_FK], Promo
	 , Reference, Contract_NO
	)
	Select 
	''PRICE RECORD'' RuleType
	, 7 ''LevelCode''
	, 111602 Result_data_name_fk
	, Case when Cast(StartDate as date) > getdate() then 711 else 710 end Rule_result_type_cr_fk
	, Null CustCriteria_DN_FK
	, Null CustValue
	, Case when isnull(Corp_Price, 0) <> 0 then 1021 else 1031 end Rules_action_operation1_FK
	--Rule_Name
	--Host_Rue_XRef
	, StartDateCorp Effective_Date
	, EndDateCorp End_Date
	, 20110 Rules_Hierarchy_FK
	, 201101 Rules_Structure_FK
	, 2011017 Rules_Schedule_FK
	, 1130 Rules_Precedence_FK
	, 702 Rule_Precedence
	--Unique_Key1
	, 110100 PRodCriteria_DN_FK
	, ProductValue
	, Case when isnull(Corp_Price, 0) <> 0 then Null else 100100417 end Basis_Calc_DN_FK
	, Case when isnull(Corp_Price, 0) <> 0 then Corp_Price else 1 end Operand_1_1
	, 1 Filter_Value_1_1
	, 2
	, 3
	, 4
	, 5
	, 6
	, 7
	, 8
	, 9
	, ei.value WhseValue
	, ei.entity_structure_fk Org_Entity_Structure_FK
	, 1 record_status_cr_fk
	, ''' + @WinUser + ''' Update_User
	, ''' + @WinUser + ''' Create_User
	, getdate() Update_Timestamp
	, getdate() Create_Timestamp
	, 0 Transferred_To_ERP
	, 0 Transferred_To_Rules
	, IMPORT_MASS_MAINTENANCE_PROMO_ID
	, 1 Promo_Ind
	, Cast(Price_Sheet_ID as varchar(32)) + ''-'' + [Price_Sheet_Description]
	, [Flyer_Month]
	from [import].[IMPORT_MASS_MAINTENANCE_PROMO] with (nolock)
	cross join epacube.epacube.entity_identification ei with (nolock) 
	where 1 = 1
	and ei.data_name_fk = 141111 
	and ei.value in (''CORP'')
	and Action_Status = -1
	and StartDateCorp is not null
	and isnull(Transferred_To_ERP, 0) = 0
	and isnull(Corp_Price, 0) <> 0
	' + @WhrClause

	Exec(@SQLcd)

	Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
	Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause

	Set @SQLcd = '
	Update PR
	Set Action_Status = 1
	from [import].[IMPORT_MASS_MAINTENANCE_PROMO] PR
	Where Action_Status = -1
	and isnull(Transferred_To_ERP, 0) = 0
	and (isnull(Base_Price, 0) <> 0 or isnull(Corp_Price, 0) <> 0)
	' + @WhrClause

	Exec(@SQLcd)

	Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
	Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause

End
Else If @CreateRecordType = 3 --Create Fixed Value Standard and Replacement Cost Reduction events for each DC re vendor temporary reductions except Corp
Begin

	Set @JobName = 'Mass Maintenance Whse Promo Cost Upload'
	Exec [common].[JOB_CREATE] Null, 100, @JobName, Null, Null, Null, Null, @Out_Job_FK Output

	Set @Job_FK = @Out_Job_FK
	Set @Job_FK_vc = Cast(@Job_FK as varchar(16))
	
	Set @SQLcd = '
	Insert into epacube.import.import_record_data
	(
	Import_Filename, Job_FK, Import_Package_FK
	, EFFECTIVE_DATE, END_DATE
	, DATA_POSITION1, DATA_POSITION2, DATA_POSITION190, DATA_POSITION191, DATA_POSITION6
	)
	select 
	Import_FileName, ' + @Job_FK_vc + ', 211000 ''Import_Package_FK''
	, StartDate
	, EndDate
	, ProductValue ''Import_Product_ID'', ProductValue ''Product_ID''
	, Replacement_Cost
	, Standard_Cost
	, ei.value ''Whse''
	from epacube.[import].[IMPORT_MASS_MAINTENANCE_PROMO] DCs with (nolock)
	cross join epacube.epacube.entity_identification ei with (nolock) 
	where 1 = 1
	and ei.data_name_fk = 141111 
	and ei.value not in (''all whse'', ''corp'', ''ZNat'', ''MDC'')
	and isnull(Replacement_Cost, 0) <> 0 and isnull(Standard_Cost, 0) <> 0
	and Action_Status = -1
	and isnull(Transferred_To_ERP, 0) = 0
	' + @WhrClause
	+ ' Union
		select 
	Import_FileName, ' + @Job_FK_vc + ', 211000 ''Import_Package_FK''
	, StartDateCorpICSW
	, EndDateCorpICSW
	, ProductValue ''Import_Product_ID'', ProductValue ''Product_ID''
	, CorpReplacementCost
	, CorpStandardCost
	, ei.value ''Whse''
	from epacube.[import].[IMPORT_MASS_MAINTENANCE_PROMO] DCs with (nolock)
	cross join epacube.epacube.entity_identification ei with (nolock) 
	where 1 = 1
	and ei.data_name_fk = 141111 
	and ei.value in (''corp'')
	and isnull(CorpReplacementCost, 0) <> 0 and isnull(CorpStandardCost, 0) <> 0
	and Action_Status = -1
	and isnull(Transferred_To_ERP, 0) = 0
	' + @WhrClause

	Exec(@SQLcd)

	Set @SQLcd = '
	Update DCs
	Set action_status = 1
	from epacube.[import].[IMPORT_MASS_MAINTENANCE_PROMO] DCs
	Where 1 = 1
	and Action_Status = -1
	and isnull(Transferred_To_ERP, 0) = 0
	' + @WhrClause

	Exec(@SQLcd)

	--Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
	--Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause

End
Else If @CreateRecordType = 6 --Create Fixed Value End-User Price Reduction events for each DC re vendor temporary reductions except Corp
Begin

	Set @JobName = 'Mass Maintenance End_User Promo Upload'
	Exec [common].[JOB_CREATE] Null, 100, @JobName, Null, Null, Null, Null, @Out_Job_FK Output

	Set @Job_FK = @Out_Job_FK
	Set @Job_FK_vc = Cast(@Job_FK as varchar(16))

	Set @SQLcd = '
	Insert into epacube.import.import_record_data
	(
	Import_Filename, Job_FK, Import_Package_FK
	, EFFECTIVE_DATE, END_DATE
	, DATA_POSITION1, DATA_POSITION2, DATA_POSITION193, DATA_POSITION6
	)
	select 
	Import_FileName, ' + @Job_FK_vc + ', 211000 ''Import_Package_FK''
	, StartDate, EndDate, ProductValue ''Import_Product_ID'', ProductValue ''Product_ID'', end_user_each_price, ei.value ''Whse''
	from epacube.[import].[IMPORT_MASS_MAINTENANCE_PROMO] DCs with (nolock)
	cross join epacube.epacube.entity_identification ei with (nolock) 
	where 1 = 1
	and ei.data_name_fk = 141111 
	and ei.value not in (''all whse'', ''corp'', ''ZNat'', ''MDC'')
	and intCreateRecordType = 6
	and isnull(end_user_each_price, 0) <> 0
	and Action_Status = -1
	' + @WhrClause

	Exec(@SQLcd)

	Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
	Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause

	Set @SQLcd = '
	Update DCs
	Set action_status = 1
	from epacube.[import].[IMPORT_MASS_MAINTENANCE_PROMO] DCs
	Where 1 =1 
	and intCreateRecordType = 6
	--and isnull(end_user_each_price, 0) <> 0
	and Action_Status = -1
	' + @WhrClause

	Exec(@SQLcd)
	--Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
	--Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause
End

If @CreateRecordType in (3, 6)
	Begin
		Set @ServiceBrokerCall = 'Exec epacube.stage.import_transformation ' + @Job_FK_vc + ', ''' + @JobName + ''''

			EXEC queue_manager.enqueue
			@service_name = N'TargetEpaService', --  nvarchar(50)
			@cmd_text = @ServiceBrokerCall, --  nvarchar(4000)
			@epa_job_id = @Job_FK, --  int
			@cmd_type = N'SQL' --  nvarchar(10)
	End

If @CreateRecordType > 2
	Begin

		Set @JobName = 'Mass Maintenance Promo OnFlyer'
		Exec [common].[JOB_CREATE] Null, 100, @JobName, Null, Null, Null, Null, @Out_Job_FK Output

		Set @Job_FK = @Out_Job_FK
		Set @Job_FK_vc = Cast(@Job_FK as varchar(16))

		--FlyerOn DN 100100276

			Set @SQLcd = '
			Insert into epacube.import.import_record_data
			(
			Import_Filename, Job_FK, Import_Package_FK
			, EFFECTIVE_DATE, END_DATE
			, DATA_POSITION1, DATA_POSITION2, DATA_POSITION146
			)
			select 
			Import_FileName, ' + @Job_FK_vc + ', 205000 ''Import_Package_FK''
			, Flyer_On_Start_Date
			, End_User_End_Date
			, ProductValue
			, ProductValue
			, P3_Flyer_Month
			from #Flyers
			Where isnull(P3_Flyer_Month, '''') <> ''''
			Group by
			Import_Filename
			, Flyer_On_Start_Date
			, End_User_End_Date
			, ProductValue
			, P3_Flyer_Month'

			Exec(@SQLcd)

			Set @ServiceBrokerCall = 'Exec epacube.stage.import_transformation ' + @Job_FK_vc + ', ''' + @JobName + ''''

				EXEC queue_manager.enqueue
				@service_name = N'TargetEpaService', --  nvarchar(50)
				@cmd_text = @ServiceBrokerCall, --  nvarchar(4000)
				@epa_job_id = @Job_FK, --  int
				@cmd_type = N'SQL' --  nvarchar(10)

		--FlyerMonth DN 100100229; PromoPage DN 100100287

		Set @JobName = 'Mass Maintenance PromoMonth Page'
		Exec [common].[JOB_CREATE] Null, 100, @JobName, Null, Null, Null, Null, @Out_Job_FK Output

		Set @Job_FK = @Out_Job_FK
		Set @Job_FK_vc = Cast(@Job_FK as varchar(16))

			Set @SQLcd = '
			Insert into epacube.import.import_record_data
			(
			Import_Filename, Job_FK, Import_Package_FK
			, EFFECTIVE_DATE, END_DATE
			, DATA_POSITION1, DATA_POSITION2, DATA_POSITION145, DATA_POSITION144
			)
			select 
			Import_FileName, ' + @Job_FK_vc + ', 205000 ''Import_Package_FK''
			, End_User_Start_Date
			, End_User_End_Date
			, ProductValue
			, ProductValue
			, P3_Flyer_Month
			, P6_Price_Sheet_Page
			from #Flyers
			Where isnull(P3_Flyer_Month, '''') <> ''''
			Group by
			Import_Filename
			, End_User_Start_Date
			, End_User_End_Date
			, ProductValue
			, P3_Flyer_Month
			, P6_Price_Sheet_Page'

			Exec(@SQLcd)

			Exec(@SQLcd)

			Set @ServiceBrokerCall = 'Exec epacube.stage.import_transformation ' + @Job_FK_vc + ', ''' + @JobName + ''''

				EXEC queue_manager.enqueue
				@service_name = N'TargetEpaService', --  nvarchar(50)
				@cmd_text = @ServiceBrokerCall, --  nvarchar(4000)
				@epa_job_id = @Job_FK, --  int
				@cmd_type = N'SQL' --  nvarchar(10)


-----

		Set @JobName = 'Mass Maintenance Promo ID/Description'
		Exec [common].[JOB_CREATE] Null, 100, @JobName, Null, Null, Null, Null, @Out_Job_FK Output

		Set @Job_FK = @Out_Job_FK
		Set @Job_FK_vc = Cast(@Job_FK as varchar(16))

		--PriceSheetID DN 100100280 Data_Position185
		--PriceSheetDescription DN 100100281  Data_Position186

			Set @SQLcd = '
			Insert into epacube.import.import_record_data
			(
			Import_Filename, Job_FK, Import_Package_FK
			, EFFECTIVE_DATE
			, DATA_POSITION1, DATA_POSITION2, DATA_POSITION6, DATA_POSITION185, DATA_POSITION186
			)
			select 
			Import_FileName, ' + @Job_FK_vc + ', 211000 ''Import_Package_FK''
			, StartDateCorpICSW
			, ProductValue
			, ProductValue
			, ''Corp''
			, Price_Sheet_ID
			, Price_Sheet_Description
			from #Flyers
			Where StartDateCorpICSW is not null
			Group by
			Import_Filename
			, StartDateCorpICSW
			, ProductValue
			, Price_Sheet_ID
			, Price_Sheet_Description

			Union

			select 
			Import_FileName, ' + @Job_FK_vc + ', 211000 ''Import_Package_FK''
			, Flyer_On_Start_Date
			, ProductValue
			, ProductValue
			, ei.value
			, Price_Sheet_ID
			, Price_Sheet_Description
			from #Flyers
			cross join epacube.epacube.entity_identification ei with (nolock) 
			where 1 = 1
			and ei.data_name_fk = 141111 
			and ei.value not in (''all whse'', ''corp'', ''ZNat'', ''MDC'')
			and Flyer_On_Start_Date is not null
			Group by
			Import_Filename
			, Flyer_On_Start_Date
			, ProductValue
			, ei.value
			, Price_Sheet_ID
			, Price_Sheet_Description
			'
			Exec(@SQLcd)

			Set @ServiceBrokerCall = 'Exec epacube.stage.import_transformation ' + @Job_FK_vc + ', ''' + @JobName + ''''

				EXEC queue_manager.enqueue
				@service_name = N'TargetEpaService', --  nvarchar(50)
				@cmd_text = @ServiceBrokerCall, --  nvarchar(4000)
				@epa_job_id = @Job_FK, --  int
				@cmd_type = N'SQL' --  nvarchar(10)


	End

If @CreateRecordType in (1, 2, 4)
	Begin
		Update MRM
			Set Record_status_CR_FK = 2
				, End_Date = Getdate() - 1
				, Inactivated_By = 'epaMassMaint'
		from dbo.MAUI_RUles_Maintenance MRM
		where Unique_key1 in
		(Select
			Replace(Replace(Replace(Replace(Replace(
				IsNull(Cast(Result_Data_Name_FK as Varchar), '0') 
				+ IsNull(Cast(LevelCode as Varchar), '0') 
				+ IsNull(Cast(Rules_action_operation1_FK as Varchar), '0') 
				+ IsNull(Cast(0 as Varchar), '0') 
				+ isnull(Cast(0 as Varchar), '0') 
				+ IsNull(Cast([CustCriteria_DN_FK] as Varchar), '0') 
				+ IsNull(Cast([CustValue] as Varchar), '0') 
				+ IsNull(Cast([ProdCriteria_DN_FK] as Varchar), '0') 
				+ IsNull(Cast([ProdValue] as Varchar), '0') 
				+ Case When Isnull(Vendor_ID, '') > '' then '143000' else '0' end --SuplCriteria_DN_FK
				+ isnull(Cast(Vendor_ID as Varchar(64)), '0')
				+ '0' --OrgCriteria_DN_FK
				+ IsNull(Cast(WhseValue as Varchar), '0') 
				+ IsNull(Cast(UOM_Code_FK as Varchar), '0') 
				+ IsNull(Convert(varchar(8), cast([Effective_Date] as datetime), 1), '')
			, '''', ''), ' ', ''), 'Null', ''), ',', ''), '"', '') 
		from dbo.MAUI_Rules_Maintenance MRM where Rule_Name is null and Record_Status_CR_FK = 1 and Transferred_To_Rules = 0
		)
		And Rule_Name is not null

		Update R
			Set Record_status_CR_FK = 2
				, End_Date = Getdate() - 1
		from Synchronizer.rules R
		where Unique_key1 in
		(Select
			Replace(Replace(Replace(Replace(Replace(
				IsNull(Cast(Result_Data_Name_FK as Varchar), '0') 
				+ IsNull(Cast(LevelCode as Varchar), '0') 
				+ IsNull(Cast(Rules_action_operation1_FK as Varchar), '0') 
				+ IsNull(Cast(0 as Varchar), '0') 
				+ isnull(Cast(0 as Varchar), '0') 
				+ IsNull(Cast([CustCriteria_DN_FK] as Varchar), '0') 
				+ IsNull(Cast([CustValue] as Varchar), '0') 
				+ IsNull(Cast([ProdCriteria_DN_FK] as Varchar), '0') 
				+ IsNull(Cast([ProdValue] as Varchar), '0') 
				+ Case When Isnull(Vendor_ID, '') > '' then '143000' else '0' end --SuplCriteria_DN_FK
				+ isnull(Cast(Vendor_ID as Varchar(64)), '0')
				+ '0' --OrgCriteria_DN_FK
				+ IsNull(Cast(WhseValue as Varchar), '0') 
				+ IsNull(Cast(UOM_Code_FK as Varchar), '0') 
				+ IsNull(Convert(varchar(8), cast([Effective_Date] as datetime), 1), '')
			, '''', ''), ' ', ''), 'Null', ''), ',', ''), '"', '') 
		from dbo.MAUI_Rules_Maintenance MRM where Rule_Name is null and Record_Status_CR_FK = 1 and Transferred_To_Rules = 0
		)
		And RECORD_STATUS_CR_FK = 1 and RESULT_DATA_NAME_FK = 111602

		Update MRM
		Set Rule_Name = 'New: ' + Cast(Maui_Rules_Maintenance_ID as varchar(36))
		, Host_Rule_Xref = 'New: ' + Cast(Maui_Rules_Maintenance_ID as varchar(36))
		, Unique_key1 = 
				Replace(Replace(Replace(Replace(Replace(
				IsNull(Cast(Result_Data_Name_FK as Varchar), '0') 
				+ IsNull(Cast(LevelCode as Varchar), '0') 
				+ IsNull(Cast(Rules_action_operation1_FK as Varchar), '0') 
				+ IsNull(Cast(0 as Varchar), '0') 
				+ isnull(Cast(mrm.Promo as Varchar), '0') 
				+ IsNull(Cast(Case LevelCode
						When 1 then isnull((Select top 1 entity_data_name_fk from epacube.entity_identification with (nolock) where data_name_fk = 144111 and value = mrm.[CustValue]), 144111)
						When 2 then isnull((Select top 1 entity_data_name_fk from epacube.entity_identification with (nolock) where data_name_fk = 144111 and value = mrm.[CustValue]), 144111)
						When 3 then 244900
						When 4 then 244900
						When 5 then isnull((Select top 1 entity_data_name_fk from epacube.entity_identification with (nolock) where data_name_fk = 144111 and value = mrm.[CustValue]), 144111)
						When 6 then 244900
						When 7 then 144111
						When 8 then 144111
					end  as Varchar), '0')
				
				+ IsNull(Cast([CustValue] as Varchar), '0') 
				+ IsNull(Cast([ProdCriteria_DN_FK] as Varchar), '0') 
				+ IsNull(Cast([ProdValue] as Varchar), '0') 
				+ Case When Isnull(Vendor_ID, '') > '' then '143000' else '0' end --SuplCriteria_DN_FK
				+ isnull(Cast(Vendor_ID as Varchar(64)), '0')
				+ '0' --OrgCriteria_DN_FK
				+ IsNull(Cast(WhseValue as Varchar), '0') 
				+ IsNull(Cast(UOM_Code_FK as Varchar), '0') 
				+ IsNull(Convert(varchar(8), cast([Effective_Date] as datetime), 1), '')
			, '''', ''), ' ', ''), 'Null', ''), ',', ''), '"', '') 
		from dbo.MAUI_Rules_Maintenance MRM where Rule_Name is null and Record_Status_CR_FK = 1 and Transferred_To_Rules = 0
--------
		/*
		Insert into Synchronizer.rules
		(Rules_Structure_FK, Rules_Schedule_FK, Precedence, [Name], [Result_Data_Name_FK], Result_Data_Name2_FK, UOM_DN_FK, UOM_Code_FK, Effective_Date, End_Date, Rule_Precedence
		, Host_Rule_Xref, Reference, Contract_No, Unique_Key1, Rule_Result_Type_CR_FK, Record_Status_CR_FK, Update_User, Create_Timestamp, Update_Timestamp, Promo_Ind, Host_Precedence_Level)
		Select
		MRM.Rules_Structure_FK
		, MRM.Rules_Schedule_FK
		, MRM.Rules_precedence_fk
		, MRM.Rule_Name 'Name'
		, MRM.Result_Data_Name_FK
		, Null Result_Data_Name2_FK
		, MRM.UOM_DN_FK
		, MRM.UOM_Code_FK
		, MRM.Effective_Date
		, MRM.End_Date
		, MRM.Rule_Precedence
		, MRM.Host_Rule_Xref
		, MRM.Reference
		, MRM.Contract_No
		, MRM.Unique_Key1
		, MRM.Rule_Result_Type_CR_FK
		, MRM.record_status_cr_fk 
		, MRM.Update_User
		, MRM.Create_Timestamp
		, MRM.Update_Timestamp
		, MRM.Promo
		, MRM.LevelCode
		from dbo.MAUI_Rules_Maintenance MRM with (Nolock)
		left join synchronizer.rules r on mrm.rules_fk = r.rules_id
		where 1 = 1
			and MRM.Transferred_To_Rules = 0
			and MRM.Inactivated_By is null
			and MRM.record_status_cr_fk = 1
			and r.rules_id is null
			and (MRM.Update_User <> 'Rule Created in SX')
			and isnull(levelcode, 0) <> 0
		*/
		Update MRM
		Set MRM.rules_fk = r.rules_id
		from dbo.MAUI_Rules_Maintenance MRM
			Inner Join Synchronizer.rules r
			on MRM.host_rule_xref = r.host_rule_xref
		where 1 = 1
			and Transferred_To_Rules = 0
		--	and isnull(MRM.rules_fk, 0) = 0 
			and (MRM.Update_User <> 'Rule Created in SX')

		Insert into synchronizer.rules_action
		( Rules_FK, Rules_Action_Operation1_FK, Rules_Action_Operation2_FK, Compare_operator_CR_FK, Basis_Calc_DN_FK, Basis1_DN_FK, Basis2_DN_FK, Basis3_DN_FK, Basis1_Number, Basis2_Number
		, Rounding_Method_CR_FK, Rounding_To_Value, Record_Status_CR_FK, Update_User, Create_Timestamp, Update_Timestamp )
		Select
		MRM.Rules_fk
		, Case MRM.Rules_Action_Operation1_FK when 0 then Null else MRM.Rules_Action_Operation1_FK end Rules_Action_Operation1_FK
		, Case MRM.Rules_Action_Operation2_FK when 0 then Null else MRM.Rules_Action_Operation2_FK end Rules_Action_Operation2_FK
		, Case When MRM.Operand_Filter1_Operator_cr_fk = 9000 then 9000 end Compare_operator_CR_FK
		, Case MRM.Basis_Calc_DN_FK When 0 then Null else MRM.Basis_Calc_DN_FK end 'Basis_Calc_DN_FK'
		, Case MRM.Basis1_DN_FK When 0 then Null else MRM.Basis1_DN_FK end 'Basis1_DN_FK'
		, Case MRM.Basis2_DN_FK When 0 then Null else MRM.Basis2_DN_FK end 'Basis2_DN_FK'
		, Case MRM.Basis3_DN_FK When 0 then Null else MRM.Basis3_DN_FK end 'Basis3_DN_FK'
		, Case When MRM.Rules_Action_Operation1_FK in (1021, 1031, 1041, 1051, 1291) then Coalesce(Operand_1_1, Operand_1_2, Operand_1_3, Operand_1_4, Operand_1_5, Operand_1_6, Operand_1_7, Operand_1_8, Operand_1_9)
			End * Case When MRM.Rules_Action_Operation1_FK in (1021, 1291) then 1 else isnull((select Value from epacube.epacube_params where name = 'OPERAND DIVISOR'), 1) end 'Basis1_Number'
		, Case When MRM.Rules_Action_Operation2_FK in (1021, 1031, 1041, 1051) then Coalesce(Operand_2_1, Operand_2_2, Operand_2_3, Operand_2_4, Operand_2_5, Operand_2_6, Operand_2_7, Operand_2_8, Operand_2_9)
			End * Case When MRM.Rules_Action_Operation2_FK = 1021 then 1 else isnull((select Value from epacube.epacube_params where name = 'OPERAND DIVISOR'), 1) end 'Basis2_Number'
		, MRM.Rounding_Method_CR_FK
		, MRM.Rounding_To_Value
		, MRM.Record_Status_CR_FK
		, MRM.Update_User
		, MRM.Create_Timestamp
		, MRM.Update_Timestamp
		from dbo.MAUI_Rules_Maintenance MRM with (Nolock)
		left join synchronizer.rules_action ra on ra.rules_fk = mrm.rules_fk
		where 1 = 1
			and MRM.Transferred_To_Rules = 0 
			and ra.rules_fk is null 
			and (MRM.Update_User <> 'Rule Created in SX')

		Update MRM
		Set MRM.rules_action_fk = ra.rules_action_id
		from dbo.MAUI_Rules_Maintenance MRM
			Inner Join Synchronizer.rules r	on MRM.host_rule_xref = r.host_rule_xref
			Inner Join Synchronizer.rules_action ra on r.rules_id = ra.rules_fk
		where 1 = 1
			and Transferred_To_Rules = 0 
		--	and isnull(MRM.rules_action_fk, 0) = 0 
			and (MRM.Update_User <> 'Rule Created in SX')

		delete from Synchronizer.rules_action_operands 
			where rules_action_fk in 
				(select rules_action_id from synchronizer.rules_action with (nolock) where rules_fk in 
				(select rules_fk 
				from dbo.MAUI_Rules_Maintenance MRM with (Nolock) 
				where 1 = 1
					and MRM.Transferred_To_Rules = 0 
				and (MRM.Update_User <> 'Rule Created in SX')
				))

		Insert into Synchronizer.rules_action_operands
		(
		Rules_action_FK, Basis_Position, Operand_Filter_DN_FK, Operand_Filter_Operator_CR_FK, Operand1, Operand2, Operand3, Operand4, Operand5, Operand6 ,Operand7, Operand8, Operand9
		, Filter_Value1, Filter_Value2, Filter_Value3, Filter_Value4, Filter_Value5, Filter_Value6, Filter_Value7, Filter_Value8, Filter_Value9, Record_Status_CR_FK, Update_User, Create_Timestamp, Update_Timestamp
		)
		Select 
		Rules_Action_FK, Basis_Position, Operand_Filter1_DN_FK, Operand_Filter1_Operator_CR_FK
		, Operand_1_1 * Mult, Operand_1_2 * Mult, Operand_1_3 * Mult, Operand_1_4 * Mult, Operand_1_5 * Mult, Operand_1_6 * Mult, Operand_1_7 * Mult, Operand_1_8 * Mult, Operand_1_9 * Mult
		, Filter_Value_1_1, Filter_Value_1_2, Filter_Value_1_3, Filter_Value_1_4, Filter_Value_1_5, Filter_Value_1_6 ,Filter_Value_1_7, Filter_Value_1_8, Filter_Value_1_9
		, Record_Status_CR_FK, Update_User, Create_Timestamp, Update_Timestamp
		from (
		Select
		MRM.Rules_Action_FK
		, 1 Basis_Position
		, MRM.Operand_Filter1_DN_FK
		, MRM.Operand_Filter1_Operator_CR_FK
		, Operand_1_1, Operand_1_2, Operand_1_3, Operand_1_4, Operand_1_5, Operand_1_6 ,Operand_1_7, Operand_1_8, Operand_1_9
		, Filter_Value_1_1, Filter_Value_1_2, Filter_Value_1_3, Filter_Value_1_4, Filter_Value_1_5, Filter_Value_1_6 ,Filter_Value_1_7, Filter_Value_1_8, Filter_Value_1_9
		, MRM.Record_Status_CR_FK
		, MRM.Update_User
		, MRM.Create_Timestamp
		, MRM.Update_Timestamp
		, MRM.Transferred_To_ERP
		, Case When Rules_Action_Operation1_FK between 1021 and 1024 then 1 else isnull((select Value from epacube.epacube_params where name = 'OPERAND DIVISOR'), 1) end Mult
		from dbo.MAUI_Rules_Maintenance MRM with (Nolock)
		inner join synchronizer.rules r with (nolock) on MRM.rules_fk = r.rules_id
		Where 1 = 1
		and right(isnull(MRM.Rules_Action_Operation1_FK, 1), 1) = 3 
		and MRM.Transferred_To_Rules = 0
		and (MRM.Update_User <> 'Rule Created in SX')

		Union

		Select
		MRM.Rules_Action_FK
		, 2 Basis_Position
		, MRM.Operand_Filter2_DN_FK
		, MRM.Operand_Filter2_Operator_CR_FK
		, Operand_2_1, Operand_2_2, Operand_2_3, Operand_2_4, Operand_2_5, Operand_2_6 ,Operand_2_7, Operand_2_8, Operand_2_9
		, Filter_Value_2_1, Filter_Value_2_2, Filter_Value_2_3, Filter_Value_2_4, Filter_Value_2_5, Filter_Value_2_6 ,Filter_Value_2_7, Filter_Value_2_8, Filter_Value_2_9
		, MRM.Record_Status_CR_FK
		, MRM.Update_User
		, MRM.Create_Timestamp
		, MRM.Update_Timestamp
		, MRM.Transferred_To_ERP
		, Case When Rules_Action_Operation2_FK between 1021 and 1024 then 1 else isnull((select Value from epacube.epacube_params with (nolock) where name = 'OPERAND DIVISOR'), 1) end Mult
		from dbo.MAUI_Rules_Maintenance MRM with (Nolock)
		inner join synchronizer.rules r with (nolock) on MRM.rules_fk = r.rules_id
		Where 1 = 1
			and right(isnull(MRM.Rules_Action_Operation2_FK, 1), 1) = 3 
			and MRM.Transferred_To_Rules = 0
			and (MRM.Update_User <> 'Rule Created in SX')

		Union

		Select
		MRM.Rules_Action_FK
		, 3 Basis_Position
		, MRM.Operand_Filter3_DN_FK
		, MRM.Operand_Filter3_Operator_CR_FK
		, Operand_3_1, Operand_3_2, Operand_3_3, Operand_3_4, Operand_3_5, Operand_3_6 ,Operand_3_7, Operand_3_8, Operand_3_9
		, Filter_Value_3_1, Filter_Value_3_2, Filter_Value_3_3, Filter_Value_3_4, Filter_Value_3_5, Filter_Value_3_6 ,Filter_Value_3_7, Filter_Value_3_8, Filter_Value_3_9
		, MRM.Record_Status_CR_FK
		, MRM.Update_User
		, MRM.Create_Timestamp
		, MRM.Update_Timestamp
		, MRM.Transferred_To_ERP
		, Case When Rules_Action_Operation3_FK between 1021 and 1024 then 1 else isnull((select Value from epacube.epacube_params with (nolock) where name = 'OPERAND DIVISOR'), 1) end Mult
		from dbo.MAUI_Rules_Maintenance MRM with (Nolock)
		inner join synchronizer.rules r with (nolock) on MRM.rules_fk = r.rules_id
		Where 1 = 1 
			and right(isnull(MRM.Rules_Action_Operation3_FK, 1), 1) = 3 
			and MRM.Transferred_To_Rules = 0
			and (MRM.Update_User <> 'Rule Created in SX')
		) Ops

		----Add Missing Filters
		--Product Filters
		INSERT INTO [synchronizer].[RULES_FILTER]
				   ([ENTITY_CLASS_CR_FK]
				   ,[NAME]
				   ,[DATA_NAME_FK]           
				   ,[ENTITY_DATA_NAME_FK]
				   ,[VALUE1]
		--           ,[VALUE2]
				   ,[OPERATOR_CR_FK]
				   ,[UPDATE_USER]           
				   ,[RECORD_STATUS_CR_FK]
					)
		SELECT DISTINCT
					10109
				   ,( DN.LABEL + ': ' + MRM.ProdValue ) 
				   ,DN.DATA_NAME_ID
				   ,NULL  -----DNE.DATA_NAME_ID 
				   ,MRM.ProdValue
		--           ,PROD_FILTER_VALUE2
				   ,9000 --ISNULL ( MRM.Operand_Filter_Operator_CR_FK, 9000 )
				   ,isnull(MRM.[UPDATE_USER], 'UNKNOWN')
				   ,1
		from dbo.MAUI_Rules_Maintenance MRM with (NoLock)
		Inner Join epacube.data_name dn with (NoLock)
			On MRM.ProdCriteria_DN_FK = DN.Data_Name_ID
		LEFT JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK )
		  ON ( RF.ENTITY_CLASS_CR_FK                = 10109
		  AND  RF.DATA_NAME_FK                      = DN.DATA_NAME_ID  
		  AND  RF.VALUE1                            = MRM.ProdValue
		--  AND  RF.OPERATOR_CR_FK                    = ISNULL ( MRM.Operand_Filter_Operator_CR_FK, 9000 )
		  )
		 Where 1 = 1
			and RF.RULES_FILTER_ID IS NULL 
			and MRM.ProdValue is not null 
			and MRM.Transferred_To_Rules = 0 
			and MRM.Rules_FK in (select rules_id from synchronizer.rules)
			and (MRM.Update_User <> 'Rule Created in SX')

		--Org Filters
		INSERT INTO [synchronizer].[RULES_FILTER]
				   ([ENTITY_CLASS_CR_FK]
				   ,[NAME]
				   ,[DATA_NAME_FK]           
				   ,[ENTITY_DATA_NAME_FK]
				   ,[VALUE1]
		--           ,[VALUE2]
				   ,[OPERATOR_CR_FK]
				   ,[UPDATE_USER]           
				   ,[RECORD_STATUS_CR_FK]
					)
		SELECT DISTINCT
					10101
				   ,( DN.LABEL + ': ' + MRM.WhseValue ) 
				   ,DN.DATA_NAME_ID
				   ,141000
				   ,MRM.WhseValue
		--           ,ORG_FILTER_VALUE2
				   ,9000 --ISNULL ( MRM.Operand_Filter_Operator_CR_FK, 9000 )
				   ,isnull(MRM.[UPDATE_USER], 'UNKNOWN')
				   ,1
		from dbo.MAUI_Rules_Maintenance MRM with (NoLock)
		Inner Join epacube.data_name dn with (NoLock)
			On DN.Data_Name_ID = 141111
		LEFT JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK )
		  ON ( RF.ENTITY_CLASS_CR_FK                = 10101 
		  AND  ISNULL ( RF.ENTITY_DATA_NAME_FK, 0 ) = 141000
		  AND  RF.DATA_NAME_FK                      = DN.DATA_NAME_ID
		  AND  RF.VALUE1                            = MRM.WhseValue
		--  AND  RF.OPERATOR_CR_FK                    = ISNULL ( MRM.Operand_Filter_Operator_CR_FK, 9000 )
		  )
		WHERE 1 = 1
			and RF.RULES_FILTER_ID IS NULL and 
			MRM.WhseValue is not null 
			and MRM.Transferred_To_Rules = 0
			and MRM.Rules_FK in (select rules_id from synchronizer.rules)
			and (MRM.Update_User <> 'Rule Created in SX')

		--Cust Filters
		INSERT INTO [synchronizer].[RULES_FILTER]
				   ([ENTITY_CLASS_CR_FK]
				   ,[NAME]
				   ,[DATA_NAME_FK]           
				   ,[ENTITY_DATA_NAME_FK]
				   ,[VALUE1]
		--           ,[VALUE2]
				   ,[OPERATOR_CR_FK]
				   ,[UPDATE_USER]           
				   ,[RECORD_STATUS_CR_FK]
					)
		SELECT DISTINCT
					10104
				   ,( DN.LABEL + ': ' + MRM.CustValue ) 
				   ,Case When MRM.CustCriteria_DN_FK <> 244900 then 144111 else 244900 end
				   ,Case When MRM.CustCriteria_DN_FK <> 244900 then MRM.CustCriteria_DN_FK end
				   ,MRM.CustValue
		--           ,Cust_FILTER_VALUE2
				   ,9000 --ISNULL ( MRM.Operand_Filter_Operator_CR_FK, 9000 )
				   ,isnull(MRM.[UPDATE_USER], 'UNKNOWN')
				   ,1
		from dbo.MAUI_Rules_Maintenance MRM with (NoLock)
		Inner Join epacube.data_name dn with (NoLock)
			On DN.Data_Name_ID = Case When MRM.CustCriteria_DN_FK in (144010, 144020) then 144111 else MRM.CustCriteria_DN_FK end
		LEFT JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK )
		  ON ( RF.ENTITY_CLASS_CR_FK                = 10104 
		  --AND  ISNULL ( RF.ENTITY_DATA_NAME_FK, 0 ) = Case When MRM.CustCriteria_DN_FK in (144010, 144020) then MRM.CustCriteria_DN_FK else 0 end
		  AND  RF.DATA_NAME_FK                      = DN.DATA_NAME_ID
		  AND  RF.VALUE1                            = MRM.CustValue
		--  AND  RF.OPERATOR_CR_FK                    = ISNULL ( MRM.Operand_Filter_Operator_CR_FK, 9000 )
		  )
		WHERE 1 = 1
			and RF.RULES_FILTER_ID IS NULL 
			and MRM.CustValue is not null 
			and MRM.Transferred_To_Rules = 0
			and MRM.Rules_FK in (select rules_id from synchronizer.rules)
			and (MRM.Update_User <> 'Rule Created in SX')

		--Vendor Filters
		INSERT INTO [synchronizer].[RULES_FILTER]
				   ([ENTITY_CLASS_CR_FK]
				   ,[NAME]
				   ,[DATA_NAME_FK]           
				   ,[ENTITY_DATA_NAME_FK]
				   ,[VALUE1]
		--           ,[VALUE2]
				   ,[OPERATOR_CR_FK]
				   ,[UPDATE_USER]           
				   ,[RECORD_STATUS_CR_FK]
					)
		SELECT DISTINCT
					10103
				   ,( DN.LABEL + ': ' + MRM.Vendor_ID ) 
				   ,DN.DATA_NAME_ID
				   ,143000
				   ,MRM.Vendor_ID
		--           ,ORG_FILTER_VALUE2
				   ,9000 --ISNULL ( MRM.Operand_Filter_Operator_CR_FK, 9000 )
				   ,isnull(MRM.[UPDATE_USER], 'UNKNOWN')
				   ,1
		from dbo.MAUI_Rules_Maintenance MRM with (NoLock)
		Inner Join epacube.data_name dn with (NoLock)
			On DN.Data_Name_ID = 143110
		LEFT JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK )
		  ON ( RF.ENTITY_CLASS_CR_FK                = 10103 
		  AND  ISNULL ( RF.ENTITY_DATA_NAME_FK, 0 ) = 143000
		  AND  RF.DATA_NAME_FK                      = DN.DATA_NAME_ID
		  AND  RF.VALUE1                            = MRM.Vendor_ID
		--  AND  RF.OPERATOR_CR_FK                    = ISNULL ( MRM.Operand_Filter_Operator_CR_FK, 9000 )
		  )
		WHERE 1 = 1
			and RF.RULES_FILTER_ID IS NULL
			and MRM.Vendor_ID is not null 
			and MRM.Rules_FK in (select rules_id from synchronizer.rules)
			and MRM.Transferred_To_Rules = 0
			and (MRM.Update_User <> 'Rule Created in SX')

		INSERT INTO [synchronizer].[RULES_FILTER_SET]
				   ([RULES_FK]
				   ,[PROD_FILTER_FK]
				   ,[ORG_FILTER_FK]
				   ,[CUST_FILTER_FK]
				   ,[SUPL_FILTER_FK]
				   ,[PROD_FILTER_ASSOC_DN_FK]
				   ,[ORG_FILTER_ASSOC_DN_FK]
				   ,[CUST_FILTER_ASSOC_DN_FK]
				   ,[SUPL_FILTER_ASSOC_DN_FK]
				   ,[UPDATE_USER]           
				   )
		SELECT
		--Prod
					MRM.RULES_FK
				   ,( SELECT RF.RULES_FILTER_ID
						   FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)
						   INNER JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK)
							ON ( RF.DATA_NAME_FK   = DN.DATA_NAME_ID
							AND  RF.VALUE1         = MRM.ProdValue 
							And RF.Data_Name_FK		= MRM.PRodCriteria_DN_FK) )
						'Prod_Filter_FK'
            
		--Org
				   ,  ( SELECT RF.RULES_FILTER_ID
						   from synchronizer.RULES_FILTER RF WITH (NOLOCK)
							Where RF.DATA_NAME_FK   = 141111
							AND  RF.VALUE1         = MRM.WhseValue
							AND  RF.ENTITY_DATA_NAME_FK = 141000 ) 
						'Org_Filter_FK'
		--Custs
				   , Case When isnull(MRM.CustValue, '') > '' then
						( SELECT RF.RULES_FILTER_ID
						   FROM synchronizer.RULES_FILTER RF WITH (NOLOCK)
							Where 1 = 1
							AND  RF.VALUE1 = MRM.CustValue
							and rf.ENTITY_CLASS_CR_FK = 10104
							--and RF.DATA_NAME_FK = Case When MRM.CustCriteria_DN_FK in (144010, 144020) then 144111 else MRM.CustCriteria_DN_FK end
							--AND  isnull(RF.ENTITY_DATA_NAME_FK, 0) = Case When MRM.CustCriteria_DN_FK in (144010, 144020) then MRM.CustCriteria_DN_FK else 0 end 
						)
					End 'Cust_Filter_FK'
		--Vendors
				   , Case when isnull(MRM.Vendor_ID, 0) <> 0 then
						( SELECT RF.RULES_FILTER_ID
							From synchronizer.RULES_FILTER RF WITH (NOLOCK)
							Where RF.DATA_NAME_FK   = 143110
							AND  RF.VALUE1         = MRM.Vendor_ID
							AND  RF.ENTITY_DATA_NAME_FK = 143000 )
					End 'Supl_Filter_FK'
				   ,Null Prod_Filter_Assoc_DN_FK
				   ,Case when isnull(MRM.WhseValue, '') > '' then 159100 end Org_Filter_Assoc_DN_FK
				   ,Null Cust_Filter_Assoc_DN_FK
				   ,Case when isnull(MRM.Vendor_ID,0) > 0 then 253501 end Supl_Filter_Assoc_DN_FK
				   ,isnull(MRM.[UPDATE_USER], 'EPACUBE')

		from dbo.MAUI_Rules_Maintenance MRM with (Nolock)
			Left Join Synchronizer.rules_filter_set rfs on MRM.rules_fk = rfs.rules_fk
		WHERE 1 = 1
			and MRM.Transferred_To_Rules = 0 
			and rfs.rules_fk is null 
			and (MRM.Update_User <> 'Rule Created in SX')
			AND MRM.RULES_FK IS NOT NULL

		Update MRM
		Set MRM.rules_action_fk = ra.rules_action_id
		from dbo.MAUI_Rules_Maintenance MRM
			Inner Join Synchronizer.rules_action ra
			on MRM.rules_fk = ra.rules_fk
		where 1 = 1
			and Transferred_To_Rules = 0 
			and isnull(MRM.rules_action_fk, 0) = 0 
			and (MRM.Update_User <> 'Rule Created in SX')

		Update MRM
		Set Transferred_To_Rules = 1
		from dbo.MAUI_Rules_Maintenance MRM
			Inner Join Synchronizer.rules_filter_set rfs
			on MRM.rules_fk = rfs.rules_fk
		where 1 = 1
			and Transferred_To_Rules = 0
			and rule_name like 'New%' 
			and (MRM.Update_User <> 'Rule Created in SX')

	If @CreateRecordType < 3 and @Resubmit = 0
	Begin
		Set @SQLcd = '
			Update IMMPR
			Set Action_Status = 3
			from Import.[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] IMMPR
			left join dbo.MAUI_Rules_Maintenance MRM on IMMPR.[IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID] = MRM.[IMPORT_MASS_MAINTENANCE_PRICE_REBATE_FK]
			Left join synchronizer.rules r on MRM.rules_fk = r.rules_ID and r.RESULT_DATA_NAME_FK = mrm.Result_Data_Name_FK
			where 1 = 1 and IMMPR.Action_Status = 1 and r.RULES_PRECEDENCE_FK is null ' + @WhrClause

		Exec(@SQLcd)
	End
	Else 
	if @Resubmit = 0
	Begin
		Set @SQLcd = '
		Update IMMP
		Set Action_Status = 3
		from [import].[IMPORT_MASS_MAINTENANCE_PROMO] IMMP
		Left join dbo.MAUI_Rules_Maintenance MRM on IMMP.IMPORT_MASS_MAINTENANCE_PROMO_ID = mrm.IMPORT_MASS_MAINTENANCE_PROMO_FK
		Left join synchronizer.rules r on MRM.rules_fk = r.rules_ID and r.RESULT_DATA_NAME_FK = mrm.Result_Data_Name_FK
		where IMMP.Action_Status = 1 and r.RULES_PRECEDENCE_FK is null ' + @WhrClause
		Exec(@SQLcd)
	End

	Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
	Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause

	If @CreateRecordType = 4
	Begin
		
		Set @SQLcd = '
		Update IMMP
		Set Rules_FK_Corp = r.rules_id
		from synchronizer.rules r
		inner join synchronizer.rules_filter_set rfs on r.rules_id = rfs.rules_fk
		inner join synchronizer.rules_filter rf_p on rfs.PROD_FILTER_FK = rf_p.RULES_FILTER_ID
		left join synchronizer.rules_filter rf_o on rfs.ORG_FILTER_FK = rf_o.rules_filter_id
		inner join [import].[IMPORT_MASS_MAINTENANCE_PROMO] IMMP on rf_p.value1 = IMMP.ProductValue and immp.intCreateRecordType = 4 and (enddate > getdate() or enddatecorp > getdate())
		where r.HOST_RULE_XREF like ''New%''
		and rf_o.value1 = ''CORP''
		and IMMP.action_status = 1 ' + @WhrClause 
		+ '
		Update IMMP
		Set Rules_FK_All = r.rules_id
		from synchronizer.rules r
		inner join synchronizer.rules_filter_set rfs on r.rules_id = rfs.rules_fk
		inner join synchronizer.rules_filter rf_p on rfs.PROD_FILTER_FK = rf_p.RULES_FILTER_ID
		left join synchronizer.rules_filter rf_o on rfs.ORG_FILTER_FK = rf_o.rules_filter_id
		inner join [import].[IMPORT_MASS_MAINTENANCE_PROMO] IMMP on rf_p.value1 = IMMP.ProductValue and immp.intCreateRecordType = 4 and (enddate > getdate() or enddatecorp > getdate())
		where r.HOST_RULE_XREF like ''New%''
		and rf_o.value1 is null
		and IMMP.action_status = 1 ' + @WhrClause

		Exec(@SQLcd)

		Insert into [import].[Customer_Johnstone_MM_Audit](intCreateRecordType, ProcessDate, SQLCode, WhrClause)
		Select @CreateRecordType, Getdate(), @SQLcd, @WhrClause
	End

--------
End

IF object_id('dbo.aaa_flyers') is not null
drop table dbo.aaa_flyers;

IF object_id('tempdb..#Flyers') is not null
drop table #Flyers;

End
