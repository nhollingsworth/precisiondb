﻿




-- Copyright 2006
--
-- Procedure created by Walt Tucholski
--
--
-- Purpose: Execute SSIS Import procedures for flat file imports. Called from SSIS FF
-- packages (flat file import).
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- WT        05/20/2007   Initial SQL Version

CREATE PROCEDURE [import].[IMPORT_SSIS_DATA_WRAPPER] (@IN_PACKAGE_FK int, @IN_JOB_FK bigint)
												
AS

BEGIN

DECLARE @ls_stmt varchar(1000),
		@l_exec_no bigint,
		@status_desc varchar (max),
		@ErrorMessage nvarchar(4000),
		@ErrorSeverity int,
		@ErrorState int,
	    @search_activity_fk int,
		@import_procedure_call varchar (128),
		@sql nvarchar(4000)


BEGIN TRY
	  SET NOCOUNT ON
	  SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of import.IMPORT_SSIS_DATA_WRAPPER. '
				 + ' Job_FK: ' + cast(isnull(@IN_JOB_FK,0) as varchar(16))
				 + ', Package_FK: ' + cast(isnull(@IN_PACKAGE_FK,0) as varchar(16))

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
					   						  @exec_id   = @l_exec_no OUTPUT;

	  SELECT @import_procedure_call = IMPORT_PROCEDURE_CALL
		    ,@search_activity_fk = isnull(SEARCH_ACTIVITY_FK,0)
	  FROM import.IMPORT_PACKAGE
	  WHERE IMPORT_PACKAGE_ID = @IN_PACKAGE_FK

	  IF @import_procedure_call = 'stage.IMPORT_TO_STG' and @search_activity_fk is null
	  BEGIN
		 SET @status_desc =  'Exiting Import.IMPORT_SSIS_DATA_WRAPPER. Search_activity_fk not found on import_package table '
				+ 'for job: ' + cast(isnull(@IN_JOB_FK,0) as varchar(16))
				+ ', package: ' + cast(isnull(@IN_PACKAGE_FK,0) as varchar(16))
		 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
	  END

	  IF @import_procedure_call = 'stage.IMPORT_TO_STG' and @search_activity_fk is not null
	  BEGIN
		  SET @sql = (select @import_procedure_call
	  		+ ' '
	  		+ cast(@search_activity_fk as varchar(20))
			+ ','
			+ cast(@IN_JOB_FK as varchar(20)))
		  EXEC sp_executesql @sql
	  END

	  IF @import_procedure_call = 'stage.STG_TO_EVENTS' and @IN_PACKAGE_FK is not null
	  BEGIN
		  SET @sql = (select @import_procedure_call
	  		+ ' '
	  		+ cast(@IN_PACKAGE_FK as varchar(20))
			+ ','
			+ cast(@IN_JOB_FK as varchar(20)))
		  EXEC sp_executesql @sql
	  END

--		  SET @sql = (select @import_procedure_call
--	  		+ ' '
--	  		+ cast(isnull(@search_activity_fk,@IN_PACKAGE_FK) as varchar(20))
--			+ ','
--			+ cast(@IN_JOB_FK as varchar(20)))
--		  EXEC sp_executesql @sql
     SET @status_desc =  'finished execution of import.IMPORT_SSIS_DATA_WRAPPER';
     EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of import.IMPORT_SSIS_DATA_WRAPPER has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
END

































