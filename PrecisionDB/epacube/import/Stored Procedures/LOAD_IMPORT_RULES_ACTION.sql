﻿



-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- KS        06/14/2010   Initial Version 
-- KS        11/07/2012   GARY.... DO NOT Commented out the BASIS_CALC_PRICESHEET_DATE
--                          This caused the wrong basis value to be selected at NCE
-- 

CREATE PROCEDURE [import].[LOAD_IMPORT_RULES_ACTION] ( @IN_JOB_FK bigint ) 
AS BEGIN 

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_IMPORT_RULES_ACTION', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps

-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( RULES_ERRORS TABLE )
--
--     SAVE EXAMPLES FOR LATER 
--
--     ERROR NAME:  INVALID OPERATION CODES
--	   ERROR NAME:  MISSING REQUIRED DATA FOR OPERATION REQUESTED
--	   EXAMPLES:  NULL VALUES; INVALID DATA NAMES; 
--
--     SET RECORD STATUS = PURGE FOR ALL RED CONDITIONS.... ???
--
--     YELLOW ERRORS  ( OPERATION SPECIFIC )
--
--     EXAMPLES:   ( OPERAND ) MULTIPLIER OUTSIDE OF RANGE
--
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--   INSERT RULES_ACTION FOR NEW RULES
-------------------------------------------------------------------------------------
INSERT INTO [synchronizer].[RULES_ACTION]
           ([RULES_FK]
           ,[RULES_ACTION_OPERATION1_FK]
           ,[RULES_ACTION_OPERATION2_FK]
           ,[RULES_ACTION_OPERATION3_FK]
           ,[COMPARE_OPERATOR_CR_FK]
           ,[BASIS_CALC_DN_FK]           
           ,[BASIS1_DN_FK]
           ,[BASIS2_DN_FK]
           ,[BASIS3_DN_FK]
           ,[BASIS1_NUMBER]
           ,[BASIS2_NUMBER]
           ,[BASIS3_NUMBER]
           ,[BASIS1_VALUE]
           ,[BASIS2_VALUE]
           ,[BASIS3_VALUE]
           ,[PREFIX1]
           ,[PREFIX2]
           ,[PREFIX3]           
           ,[BASIS_CALC_PRICESHEET_NAME]
           ,[BASIS1_PRICESHEET_NAME]
           ,[BASIS2_PRICESHEET_NAME]
           ,[BASIS3_PRICESHEET_NAME]
	       ,[BASIS_CALC_PRICESHEET_DATE]
           ,[BASIS_USE_CORP_ONLY_IND]
           ,[ROUNDING_METHOD_CR_FK]
           ,[ROUNDING_TO_VALUE]
           ,[ROUNDING_ADDER_VALUE]
           ,[FORMULA]             
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]  
           ,[UPDATE_USER] 
           ,[RULES_OPTIMIZATION_FK]                            
           )
SELECT
            IR.RULES_FK
           ,( SELECT RAOP.RULES_ACTION_OPERATION_ID FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
              WHERE RAOP.NAME = IRD.RULES_ACTION_OPERATION1  )
           ,( SELECT RAOP.RULES_ACTION_OPERATION_ID FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
              WHERE RAOP.NAME = IRD.RULES_ACTION_OPERATION2  )
           ,( SELECT RAOP.RULES_ACTION_OPERATION_ID FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
              WHERE RAOP.NAME = IRD.RULES_ACTION_OPERATION3  )
           ,( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
              WHERE CRX.CODE_TYPE = 'OPERATOR' AND CRX.CODE = IRD.COMPARE_OPERATOR )
           ,( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
              WHERE DNX.NAME = IRD.BASIS_CALC_DN  )           
           ,( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
              WHERE DNX.NAME = IRD.BASIS1_DN  )
           ,( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
              WHERE DNX.NAME = IRD.BASIS2_DN  )
           ,( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
              WHERE DNX.NAME = IRD.BASIS3_DN  )
           ,CAST ( IRD.BASIS1_NUMBER AS NUMERIC (18,6) )
           ,CAST ( IRD.BASIS2_NUMBER AS NUMERIC (18,6) )
           ,CAST ( IRD.BASIS3_NUMBER AS NUMERIC (18,6) )
           ,IRD.BASIS1_VALUE 
           ,IRD.BASIS2_VALUE 
           ,IRD.BASIS3_VALUE 
           ,IRD.PREFIX1 
           ,IRD.PREFIX2 
           ,IRD.PREFIX3            
           ,IRD.BASIS_CALC_PRICESHEET_NAME
           ,IRD.BASIS1_PRICESHEET_NAME
           ,IRD.BASIS2_PRICESHEET_NAME
           ,IRD.BASIS3_PRICESHEET_NAME
           ,IRD.BASIS_CALC_PRICESHEET_DATE
           ,IRD.BASIS_USE_CORP_ONLY_IND 
           ,( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
              WHERE CRX.CODE_TYPE = 'ROUNDING_METHOD' AND CRX.CODE = IRD.ROUNDING_METHOD )
           ,CAST ( IRD.ROUNDING_TO_VALUE AS NUMERIC (18,6) )           
           ,CAST ( IRD.ROUNDING_ADDER_VALUE AS NUMERIC (18,6) )
           ------- FORMULA
           ,( SELECT CASE CRO.CODE
                     WHEN 'NO ACTION'       THEN 'NO ACTION' 
                     WHEN '='				THEN CASE CRB.CODE
                                                 WHEN 'DATA NAME'	THEN IRD.BASIS_CALC_DN 
                                                 WHEN 'OPERANDS'	THEN 'LEVEL/BRACKET'
                                                 ELSE  IRD.BASIS1_NUMBER
                                                 END   
                     WHEN 'DISC'	        THEN ( IRD.BASIS_CALC_DN + ' * ( 1 - ( ' + CAST ( IRD.BASIS1_NUMBER AS VARCHAR(16) )   + ' ) )'  )                                                                            
                     WHEN 'GM'				THEN ( IRD.BASIS_CALC_DN + ' / ( 1 - ( ' + CAST ( IRD.BASIS1_NUMBER AS VARCHAR(16) )   + ' ) '  )                           
                     WHEN 'PCT'				THEN ( IRD.BASIS_CALC_DN + ' * ( 1 + ( ' + CAST ( IRD.BASIS1_NUMBER AS VARCHAR(16) )   + ' ) '  )  
                     WHEN 'NAMED OPERATION'	THEN CASE RAO1.NAME 
                                                 WHEN 'REBATE DOWN TO FIXED'	  THEN ( IRD.BASIS_CALC_DN + ' - ' + IRD.BASIS1_DN   )    
                                                 WHEN 'REBATE DOWN TO CALCULATED' THEN ( IRD.BASIS_CALC_DN + ' - ( ' +  IRD.BASIS1_DN + ' * ' +  CAST ( IRD.BASIS1_NUMBER AS VARCHAR(16) )   + ' ) '  )
                                                 END
                     ---- ALL OTHER OPERATORS ( *, /, +, - )                                                 
                     ELSE                ( IRD.BASIS_CALC_DN + ' ' + CRO.CODE + ' ' + 
                                             CASE CRB.CODE
											 WHEN 'DATA NAME'	THEN IRD.BASIS_CALC_DN 
											 WHEN 'OPERANDS'	THEN 'LEVEL/BRACKET'
											 ELSE  CAST ( IRD.BASIS1_NUMBER AS VARCHAR(16) )
											 END   )
                     END )   AS FORMULA                          
           ,( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
              WHERE CRX.CODE_TYPE = 'EVENT_PRIORITY' AND CRX.CODE = IRD.EVENT_PRIORITY )
           ,( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
              WHERE CRX.CODE_TYPE = 'EVENT_STATUS' AND CRX.CODE = IRD.EVENT_STATUS )
           ,( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
              WHERE CRX.CODE_TYPE = 'EVENT_CONDITION' AND CRX.CODE = IRD.EVENT_CONDITION )
           ,IRD.ERROR_NAME   
           ,isnull(IR.[UPDATE_USER], 'EPACUBE') 
           ,IRD.RULES_OPTIMIZATION_FK
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
LEFT JOIN synchronizer.RULES_ACTION_OPERATION RAO1 WITH (NOLOCK)
  ON ( RAO1.NAME = IRD.RULES_ACTION_OPERATION1 )
LEFT JOIN epacube.CODE_REF CRO WITH (NOLOCK)
  ON  ( CRO.CODE_REF_ID = RAO1.OPERATOR_CR_FK )
LEFT JOIN epacube.CODE_REF CRB WITH (NOLOCK)
  ON  ( CRB.CODE_REF_ID = RAO1.BASIS_SOURCE_CR_FK )
------------  
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 54  


-------------------------------------------------------------------------------------
--   UPDATE EXISTING RULES ACTIONS
-------------------------------------------------------------------------------------

UPDATE synchronizer.RULES_ACTION
   SET 
       [RULES_ACTION_OPERATION1_FK] = ( SELECT RAOP.RULES_ACTION_OPERATION_ID FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
										WHERE RAOP.NAME = IRD.RULES_ACTION_OPERATION1  )
      ,[RULES_ACTION_OPERATION2_FK] = ( SELECT RAOP.RULES_ACTION_OPERATION_ID FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
										WHERE RAOP.NAME = IRD.RULES_ACTION_OPERATION2  )
      ,[RULES_ACTION_OPERATION3_FK] = ( SELECT RAOP.RULES_ACTION_OPERATION_ID FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
										WHERE RAOP.NAME = IRD.RULES_ACTION_OPERATION3  )
      ,[COMPARE_OPERATOR_CR_FK]		= ( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
											WHERE CRX.CODE_TYPE = 'OPERATOR' AND CRX.CODE = IRD.COMPARE_OPERATOR )										
      ,[BASIS_CALC_DN_FK]			= ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
										WHERE DNX.NAME = IRD.BASIS_CALC_DN  )      
      ,[BASIS1_DN_FK]				= ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
										WHERE DNX.NAME = IRD.BASIS1_DN  )
      ,[BASIS2_DN_FK]				= ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
										WHERE DNX.NAME = IRD.BASIS2_DN  )
      ,[BASIS3_DN_FK]				= ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
										WHERE DNX.NAME = IRD.BASIS3_DN  ) 
      ,[BASIS1_NUMBER]				= CAST ( IRD.BASIS1_NUMBER AS NUMERIC (18,6) )
      ,[BASIS2_NUMBER]				= CAST ( IRD.BASIS2_NUMBER AS NUMERIC (18,6) )
      ,[BASIS3_NUMBER]				= CAST ( IRD.BASIS3_NUMBER AS NUMERIC (18,6) )
      ,[BASIS1_VALUE]				= IRD.BASIS1_VALUE 
      ,[BASIS2_VALUE]				= IRD.BASIS2_VALUE 
      ,[BASIS3_VALUE]				= IRD.BASIS3_VALUE 
      ,[RULES_OPTIMIZATION_FK]      = IRD.RULES_OPTIMIZATION_FK
      ,[PREFIX1]					= IRD.PREFIX1
      ,[PREFIX2]					= IRD.PREFIX2
      ,[PREFIX3]					= IRD.PREFIX3      
      ,[BASIS_CALC_PRICESHEET_NAME]	= IRD.BASIS_CALC_PRICESHEET_NAME
      ,[BASIS1_PRICESHEET_NAME]	    = IRD.BASIS1_PRICESHEET_NAME
      ,[BASIS2_PRICESHEET_NAME]	    = IRD.BASIS2_PRICESHEET_NAME
      ,[BASIS3_PRICESHEET_NAME]	    = IRD.BASIS3_PRICESHEET_NAME
      ,[BASIS_CALC_PRICESHEET_DATE] = IRD.BASIS_CALC_PRICESHEET_DATE
      ,[BASIS_USE_CORP_ONLY_IND]	= IRD.BASIS_USE_CORP_ONLY_IND
--------      ,[ROUNDING_METHOD_CR_FK]		= ( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
--------										WHERE CRX.CODE_TYPE = 'ROUNDING_METHOD' AND CRX.CODE = IRD.ROUNDING_METHOD )
      ,[ROUNDING_TO_VALUE]			= CAST ( IRD.ROUNDING_TO_VALUE AS NUMERIC (18,6) )
      ,[ROUNDING_ADDER_VALUE]		= CAST ( IRD.ROUNDING_ADDER_VALUE AS NUMERIC (18,6) )
      ,[EVENT_PRIORITY_CR_FK]		= ( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
										WHERE CRX.CODE_TYPE = 'EVENT_PRIORITY' AND CRX.CODE = IRD.EVENT_PRIORITY )
      ,[EVENT_STATUS_CR_FK]			= ( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
										WHERE CRX.CODE_TYPE = 'EVENT_STATUS' AND CRX.CODE = IRD.EVENT_STATUS )
      ,[EVENT_CONDITION_CR_FK]		= ( SELECT CRX.CODE_REF_ID FROM EPACUBE.CODE_REF CRX WITH (NOLOCK)
										WHERE CRX.CODE_TYPE = 'EVENT_CONDITION' AND CRX.CODE = IRD.EVENT_CONDITION )
      ,[ERROR_NAME]					= IRD.ERROR_NAME       
      ,[RECORD_STATUS_CR_FK]		= CASE ISNULL ( IR.RULE_STATUS, 'NULL DATA' )
									   WHEN 'NULL DATA' THEN 1
									   WHEN 'ACTIVE'    THEN 1    
									   WHEN 'INACTIVE'  THEN 2
									   END  ---- RECORD_STATUS_CR_FK    MAKE THE SAME AS THE RULE 
      ,[UPDATE_TIMESTAMP]			= GETDATE ()
      ,[UPDATE_USER]				= isnull(IR.[UPDATE_USER], 'EPACUBE')
      ,[UPDATE_LOG_FK]				= IR.IMPORT_RULES_ID 
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 52 
AND   synchronizer.RULES_ACTION.RULES_FK = IR.RULES_FK

 SET @status_desc =  'finished execution of import.LOAD_IMPORT_RULES_ACTION'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_IMPORT_RULES_ACTION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END




























































