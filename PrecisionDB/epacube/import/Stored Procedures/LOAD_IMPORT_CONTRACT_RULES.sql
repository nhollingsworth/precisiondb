﻿















-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        01/10/2011   Initial Version 


CREATE PROCEDURE [import].[LOAD_IMPORT_CONTRACT_RULES] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_IMPORT_CONTRACT_RULES', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.IMPORT_ECLIPSE_CONTRACTS WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM import.IMPORT_ECLIPSE_CONTRACTS WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD IMPORT CONTRACT RULES',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;




-------------------------------------------------------------------------------------
--Call To Trim all Data
-------------------------------------------------------------------------------------

--EXECUTE [cleanser].[IMPORT_RULES_CLEANSER] 
--   @IN_JOB_FK

-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( JOB_ERRORS TABLE )
--
--     THESE ERRORS PROHIBIT THE RULE ROWS FROM BEING IMPORTED
--
--	   ERROR NAME:  INCOMPLETE OR INVALID DATA PROHIBITING IMPORT 
--     EXAMPLE IS MISSING OR INVALID STRUCTURE, SCHEDULE OR NO RULE NAME
--
--     IMPORT ROW MARKED  ERROR_ON_IMPORT_IND = 1 
--
-------------------------------------------------------------------------------------

--UPDATE IMPORT.IMPORT_RULES
--SET   EVENT_ACTION_CR_FK = NULL 
--     ,RULES_FK = NULL 
--WHERE 1 = 1                   
--AND   import.IMPORT_RULES.JOB_FK = @IN_JOB_FK 
--AND   ISNULL ( import.IMPORT_RULES.ERROR_ON_IMPORT_IND, 0 ) = 0  
     


-------------------------------------------------------------------------------------
--   MATCH IMPORT CONTRACT RULES TO   SYNCHRONIZER.RULES_CONTRACT 
--		
-------------------------------------------------------------------------------------

Update IEC
Set Rules_CONTRACT_FK = ( SELECT RC.RULES_CONTRACT_ID
						   FROM synchronizer.RULES_CONTRACT RC
						   WHERE RC.CONTRACT_NO = IEC.CONTRACT_ID)
						  
FROM import.IMPORT_ECLIPSE_CONTRACTS IEC 
WHERE 1 = 1                   
AND   IEC.JOB_FK = @IN_JOB_FK 
AND   ISNULL ( IEC.ERROR_ON_IMPORT_IND, 0 ) = 0  





-------------------------------------------------------------------------------------
--   SYNCHRONIZER.RULES_CONTRACT  UPDATES
--
--    NEED TO DISCUSS WHAT CAN BE CHANGED
--
-------------------------------------------------------------------------------------
--
--
--
UPDATE [synchronizer].[RULES_CONTRACT]
   SET 
--      ,[CONTRACT_TYPE_CR_FK] = <CONTRACT_TYPE_CR_FK, int,>
--      [CONTRACT_NO] = IEC.CONTRACT_ID
      [NAME] = IEC.CONTRACT_NAME
      ,[REFERENCE] = IEC.REFERENCE
      ,[REFERENCE2] = IEC.REFERENCE2
      ,[EFFECTIVE_DATE] = IEC.EFFECTIVE_DATE
      ,[END_DATE] = IEC.END_DATE
--      ,[NOTIFICATION_DAYS] = 
--      ,[OUTSIDE_SALES_REFERENCE] = 
--      ,[INSIDE_SALES_REFERENCE] = 
--      ,[BUYER_REFERENCE] = 
--      ,[OUTSIDE_SALES_ENTITY_STRUCTURE_FK] = 
--      ,[INSIDE_SALES_ENTITY_STRUCTURE_FK] = 
--      ,[BUYER_ENTITY_STRUCTURE_FK] = 
--      ,[TARGET_CLOSE_DATE] = 
       ,[CONTRACT_STATUS_CR_FK] = ISNULL((select code_ref_id from epacube.code_ref
										where Code = IEC.CONTRACT_STATUS
										and code_type = 'CONTRACT_STATUS'),10470)  ---active
--      ,[RECORD_STATUS_CR_FK] = 
      ,[UPDATE_TIMESTAMP] = getdate()
      ,[UPDATE_USER] = 'IMPORT_UPDATE'
      ,[UPDATE_LOG_FK] = NULL
 FROM import.IMPORT_ECLIPSE_CONTRACTS IEC 
WHERE 1 = 1    
AND IEC.RULES_CONTRACT_FK = RULES_CONTRACT_ID
AND   IEC.RULES_CONTRACT_FK IS NOT NULL               
AND   IEC.JOB_FK = @IN_JOB_FK 
AND   ISNULL ( IEC.ERROR_ON_IMPORT_IND, 0 ) = 0  
--

-------------------------------------------------------------------------------------
--   SYNCHRONIZER.RULES_CONTRACT INSERTS
-------------------------------------------------------------------------------------

INSERT INTO [synchronizer].[RULES_CONTRACT]
           (
           [CONTRACT_TYPE_CR_FK]
           ,[CONTRACT_NO]
           ,[NAME]
           ,[REFERENCE]
           ,[REFERENCE2]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
--           ,[NOTIFICATION_DAYS]
--           ,[OUTSIDE_SALES_REFERENCE]
--           ,[INSIDE_SALES_REFERENCE]
--           ,[BUYER_REFERENCE]
--           ,[OUTSIDE_SALES_ENTITY_STRUCTURE_FK]
--           ,[INSIDE_SALES_ENTITY_STRUCTURE_FK]
--           ,[BUYER_ENTITY_STRUCTURE_FK]
--           ,[TARGET_CLOSE_DATE]
           ,[CONTRACT_STATUS_CR_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
--           ,[UPDATE_LOG_FK])
)
(SELECT
(select code_ref_id from epacube.code_ref 
where code = iec.contract_type
and code_type = 'CONTRACT_TYPE')
,IEC.CONTRACT_ID
,IEC.CONTRACT_NAME
,IEC.REFERENCE
,IEC.REFERENCE2
,IEC.EFFECTIVE_DATE
,IEC.END_DATE
,ISNULL((select code_ref_id from epacube.code_ref
		where Code = IEC.CONTRACT_STATUS
		and code_type = 'CONTRACT_STATUS'),10470)  -- ACTIVE Contract Status
,1   --Record status
,getdate()
,'IMPORT'
FROM import.IMPORT_ECLIPSE_CONTRACTS IEC
WHERE 1=1
AND IEC.RULES_CONTRACT_FK is NULL
AND   ISNULL ( IEC.ERROR_ON_IMPORT_IND, 0 ) = 0  
AND JOB_FK = @in_job_fk)
--
--
--


----add some logging

 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD IMPORT RULES CONTRACTS' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD IMPORT RULES CONTRACTS' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( RULES_ERRORS TABLE )
--
--     ADD RULES VALIDATION ROUTINE TO BE CALLED FROM UI LATER.. 
--
--	   THESE ERRORS ARE BUSINESS LOGIC ERRORS 
--        WHICH WILL BE BY RULE_TYPE_CR_FK AND RULES_ACTION_OPERATION_FK
--     
--     RULE WILL BE WRITTEN TO RULES TABLE BUT WITH AN ERROR CONDITION
--
--
-------------------------------------------------------------------------------------






 SET @status_desc =  'finished execution of import.LOAD_IMPORT_RULES_CONTRACT'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_IMPORT_RULES_CONTRACT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END



















