﻿
--A_epaMAUI_JOHNSTONE_TO_PRODUCTION_MM_PRICE

CREATE PROCEDURE [import].[CUSTOMER_JOHNSTONE_TO_PRODUCTION_MM_PD]
	@in_job_fk           bigint
AS
BEGIN

--Declare @in_job_fk bigint
--Set @in_job_fk = (Select top 1 Job_FK from [import].[Customer_Johnstone_MM_PD] order by job_fk desc)

DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);
	  DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
	 DECLARE @TotalAfterRecords int, @TotalBeforeRecords int

	SET NOCOUNT ON;
	SET @V_START_TIMESTAMP = getdate()  -- used in steps

----DataValidationStatus
------1 = Everything Matches
------2 = Product ID Matches but Vendor Part Number does not
------3 = Vendor Part Number Matches but Product ID Totally Unrelated
------4 = Vendor Part Number is not correct for that Vendor
------5 = Data Duplication
------7 = Customer # Does not exist
------8 = Product ID Does not exist
------9 = Other

----Delete PD Records that were previously loaded from the same vendor with any start date in current file
Exec Custom.Johnstone_MM_PD_Record_Deletes_On_Import 1

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of [import].[CUSTOMER_JOHNSTONE_TO_PRODUCTION_MM_PRICE]', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE()
	 select @TotalBeforeRecords = count(*) from [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] with (nolock)

--Update Vendor Name to the name that is loaded into epaCUBE
Update JPR
Set [Vendor Name] = isnull(ein.value, 'Name Not Available')
from [import].[Customer_Johnstone_MM_PD] JPR
inner join epacube.entity_identification ei with (nolock) on JPR.[Vendor #] = ei.value and ei.data_name_fk = 143110 and ei.entity_data_name_fk = 143000
inner join epacube.ENTITY_IDENT_NONUNIQUE ein with (nolock) on ei.entity_structure_fk = ein.entity_structure_fk and ei.entity_data_name_fk = ein.ENTITY_DATA_NAME_FK and ein.DATA_NAME_FK = 143112
where ei.data_name_fk = 143110 and ei.entity_data_name_fk = 143000 --and ei.value = JPR.[Vendor #]
and JPR.[mfg part #] is not null

--Archive loaded data
Insert into [import].[Customer_Johnstone_MM_PD_Archive]
([Mfg Part #], [Vendor #], [Vendor Name], [Contract #], [Start Date], [3 Digit Store #], [6 Digit Store #], [Johnstone Stock #], [Store Price], [Standard Claimback Type], [Fixed Claimback Amount], [DS Multiplier], [DC Multiplier], [Import_Filename], [Job_FK]
, Archive_Timestamp, StartDate_Timestamp)
Select 
[Mfg Part #], [Vendor #], [Vendor Name], [Contract #], [Start Date], [3 Digit Store #], [6 Digit Store #], [Johnstone Stock #], [Store Price], [Standard Claimback Type], [Fixed Claimback Amount], [DS Multiplier], [DC Multiplier], [Import_Filename], [Job_FK]
, getdate() 'Archive_Timestamp', Case when isdate([Start Date]) = 1 then cast([Start Date] as date) end 'StartDate_Timestamp'
from [import].[Customer_Johnstone_MM_PD]

Delete from [import].[Customer_Johnstone_MM_PD_Archive] where cast(Archive_Timestamp as date) < Cast(getdate() - 365 as date)

--------------------------------
--Price Records Load
--------------------------------

		--5 DataValidationStatus = Duplicate Data Sent
		Insert into [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE]
		([VendorPartNumber], [VendorNo], [VendorName], [VendorPartNumber_OnFile], [VendorNo_OnFile], [VendorName_OnFile], [ContractNo], [StartDate], [Cust_Data_Name_FK], [Product_Data_Name_FK], [CustValueShort], [CustValue], [ProductValue], [Price_For_Calculation], [StoreSellPrice], [CorpSellPrice], [CreateRecordType], [DS Multiplier], [DC Multiplier], [DataValidationStatus], [MassCreateStatus], [Create_Timestamp], [Update_Timestamp], [Create_User], [Update_User], [Import_Filename], [Customer_Johnstone_MM_PD_FK], Job_FK)
		select
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], Null VPN_On_File, Null VendorNo_OnFile, Null VendorName_OnFile, JPR.[Contract #]
		, Case isdate(JPR.[Start Date]) when 0 then Cast(Cast(JPR.[Start Date] as bigint) as smalldatetime) else Cast(JPR.[Start Date] as smalldatetime) end 'StartDate'
		, 144010, 110100, JPR.[3 Digit Store #], JPR.[6 Digit Store #], JPR.[Johnstone Stock #], JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), 'Price' 'Record type', JPR.[DS Multiplier], JPR.[DC Multiplier], 5, 0, Getdate(), Getdate(), SUSER_SNAME(), SUSER_SNAME(), JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK
		from [import].[Customer_Johnstone_MM_PD] JPR
		Inner Join
		(
			Select [Vendor #], [Start Date], [6 Digit Store #], [Johnstone Stock #], count(*) Records 
			from [import].[Customer_Johnstone_MM_PD] 
			Where [Start Date] is not null and [6 Digit Store #] is not null and [Johnstone Stock #] is not null and job_fk = @In_Job_FK 
			group by [Vendor #], [Start Date], [6 Digit Store #], [Johnstone Stock #] 
			Having count(*) > 1
		) Dups 
			On JPR.[Vendor #]  = Dups.[Vendor #]
			and JPR.[Start Date]  = Dups.[Start Date]
			and JPR.[6 Digit Store #]  = Dups.[6 Digit Store #]
			and JPR.[Johnstone Stock #]  = Dups.[Johnstone Stock #]
		where jpr.Job_FK = @in_job_fk
		And isnull(JPR.[Store Price], 0) * isnull(JPR.[DC Multiplier], 0) <> 0
		and JPR.[mfg part #] is not null
		Order by JPR.[Vendor #], JPR.[Start Date], JPR.[6 Digit Store #], JPR.[Johnstone Stock #]


		--1 DataValidationStatus = Customer #, ProductID, and VendorPartNumber All Match
		Insert into [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE]
		([VendorPartNumber], [VendorNo], [VendorName], [VendorPartNumber_OnFile], [VendorNo_OnFile], [VendorName_OnFile], [ContractNo], [StartDate], [Cust_Data_Name_FK], [Product_Data_Name_FK], [CustValueShort], [CustValue], [ProductValue], [Price_For_Calculation], [StoreSellPrice], [CorpSellPrice], [CreateRecordType], [DS Multiplier], [DC Multiplier], [DataValidationStatus], [MassCreateStatus], [Create_Timestamp], [Update_Timestamp], [Create_User], [Update_User], [Import_Filename], [Customer_Johnstone_MM_PD_FK], Job_FK)
		Select
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], pi_vpn.value VPN_On_File, ei_vndr.value VendorNo_OnFile, ein.value VendorName_OnFile, JPR.[Contract #]
		, Case isdate(JPR.[Start Date]) when 0 then Cast(Cast(JPR.[Start Date] as bigint) as smalldatetime) else Cast(JPR.[Start Date] as smalldatetime) end 'StartDate'
		, 144010, 110100, JPR.[3 Digit Store #], JPR.[6 Digit Store #], pi_prod.value, JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), 'Price' 'Record type', JPR.[DS Multiplier], JPR.[DC Multiplier], 1, 0, Getdate(), Getdate(), SUSER_SNAME(), SUSER_SNAME(), JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK
		from epacube.product_identification pi_prod with (nolock)
		inner join epacube.product_identification pi_vpn with (nolock) 
			on pi_prod.DATA_NAME_FK = 110100 
			and pi_vpn.data_name_fk = 110111
			and pi_prod.product_structure_fk = pi_vpn.PRODUCT_STRUCTURE_FK
		inner join epacube.product_association pa with (nolock)
			on pa.data_name_fk = 253501
			and pi_prod.product_structure_fk = pa.PRODUCT_STRUCTURE_FK
			and pi_vpn.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK
		inner join epacube.entity_identification ei_vndr with (nolock)
			on ei_vndr.entity_data_name_fk = 143000 and ei_vndr.DATA_NAME_FK = 143110
			and pa.entity_structure_fk = ei_vndr.ENTITY_STRUCTURE_FK
		Left Join epacube.ENTITY_IDENT_NONUNIQUE EIN with (nolock)
			on ein.entity_data_name_fk = 143000
			and ein.data_name_fk = 143112
			and ei_vndr.ENTITY_STRUCTURE_FK = ein.ENTITY_STRUCTURE_FK
		inner join [import].[Customer_Johnstone_MM_PD] JPR with (nolock)
			on pi_prod.value = JPR.[Johnstone Stock #]
			and pi_vpn.value = JPR.[Mfg Part #]
		inner join epacube.ENTITY_IDENTIFICATION ei_cust with (nolock)
			on ei_cust.entity_data_name_fk in (144010, 144020)
			and ei_cust.value = JPR.[6 Digit Store #]
		Left Join [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] New with (Nolock)
			On 1 = 1	
			--and JPR.Customer_Johnstone_MM_PD_ID  = New.Customer_Johnstone_MM_PD_FK and new.createrecordtype = 'Price'
			and jpr.[Vendor #] = new.VendorNo
			and jpr.job_fk = new.job_fk
			and jpr.[6 Digit Store #]  = new.CustValue
			and jpr.[Johnstone Stock #]  = new.ProductValue
			and new.createrecordtype = 'Price'
		Where JPR.[Start Date] is not null and JPR.[6 Digit Store #] is not null and JPR.[Johnstone Stock #] is not null
		and JPR.[Mfg Part #] = pi_vpn.value
		and New.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID is null
		--and jpr.Job_FK = @in_job_fk
		And isnull(JPR.[Store Price], 0) * isnull(JPR.[DC Multiplier], 0) <> 0
		and JPR.[mfg part #] is not null
		Group by
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], pi_vpn.value, ei_vndr.value, ein.value, JPR.[Contract #], JPR.[Start Date], JPR.[3 Digit Store #], JPR.[6 Digit Store #], pi_prod.value, JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), JPR.[DS Multiplier], JPR.[DC Multiplier], JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK


		--7 DataValidationStatus = Customer # Does not exist in epaCUBE
		Insert into [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE]
		([VendorPartNumber], [VendorNo], [VendorName], [VendorPartNumber_OnFile], [VendorNo_OnFile], [VendorName_OnFile], [ContractNo], [StartDate], [Cust_Data_Name_FK], [Product_Data_Name_FK], [CustValueShort], [CustValue], [ProductValue], [Price_For_Calculation], [StoreSellPrice], [CorpSellPrice], [CreateRecordType], [DS Multiplier], [DC Multiplier], [DataValidationStatus], [MassCreateStatus], [Create_Timestamp], [Update_Timestamp], [Create_User], [Update_User], [Import_Filename], [Customer_Johnstone_MM_PD_FK], Job_FK)
		Select
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], pi_vpn.value VPN_On_File, ei_vndr.value VendorNo_OnFile, ein.value VendorName_OnFile, JPR.[Contract #]
		, Case isdate(JPR.[Start Date]) when 0 then Cast(Cast(JPR.[Start Date] as bigint) as smalldatetime) else Cast(JPR.[Start Date] as smalldatetime) end 'StartDate'
		, 144010, 110100, JPR.[3 Digit Store #], JPR.[6 Digit Store #], pi_prod.value, JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), 'Price' 'Record type', JPR.[DS Multiplier], JPR.[DC Multiplier], 7, 0, Getdate(), Getdate(), SUSER_SNAME(), SUSER_SNAME(), JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK
		from [import].[Customer_Johnstone_MM_PD] JPR with (nolock)
		Left Join epacube.product_identification pi_prod with (nolock)
			on pi_prod.value = JPR.[Johnstone Stock #] and pi_prod.data_name_fk = 110100
		left join epacube.product_identification pi_vpn with (nolock) 
			on pi_vpn.data_name_fk = 110111
			and pi_prod.product_structure_fk = pi_vpn.PRODUCT_STRUCTURE_FK
		left join epacube.product_association pa with (nolock)
			on pa.data_name_fk = 253501
			and pi_prod.product_structure_fk = pa.PRODUCT_STRUCTURE_FK
			and pi_vpn.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK
		left join epacube.entity_identification ei_vndr with (nolock)
			on ei_vndr.entity_data_name_fk = 143000 and ei_vndr.DATA_NAME_FK = 143110
			and pa.entity_structure_fk = ei_vndr.ENTITY_STRUCTURE_FK
		Left Join epacube.ENTITY_IDENT_NONUNIQUE EIN with (nolock)
			on ein.entity_data_name_fk = 143000
			and ein.data_name_fk = 143112
			and ei_vndr.ENTITY_STRUCTURE_FK = ein.ENTITY_STRUCTURE_FK
		left join epacube.ENTITY_IDENTIFICATION ei_cust with (nolock)
			on ei_cust.entity_data_name_fk in (144010, 144020)
			and ei_cust.value = JPR.[6 Digit Store #]
		Left Join [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] New with (Nolock)
			On 1 = 1	
			--and JPR.Customer_Johnstone_MM_PD_ID  = New.Customer_Johnstone_MM_PD_FK and new.createrecordtype = 'Price'
			and jpr.[Vendor #] = new.VendorNo
			and jpr.job_fk = new.job_fk
			and jpr.[6 Digit Store #]  = new.CustValue
			and jpr.[Johnstone Stock #]  = new.ProductValue
			and new.createrecordtype = 'Price'
		Where 1 = 1
		and New.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID is null
		and ei_cust.entity_structure_fk is null
		and JPR.[mfg part #] is not null
		and jpr.Job_FK = @in_job_fk
		Group by
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], pi_vpn.value, ei_vndr.value, ein.value, JPR.[Contract #], JPR.[Start Date], JPR.[3 Digit Store #], JPR.[6 Digit Store #], pi_prod.value, JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), JPR.[DS Multiplier], JPR.[DC Multiplier], JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK
		Order by JPR.[6 Digit Store #], pi_prod.value

		--8 DataValidationStatus = Product ID Does not exist in epaCUBE
		Insert into [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE]
		([VendorPartNumber], [VendorNo], [VendorName], [VendorPartNumber_OnFile], [VendorNo_OnFile], [VendorName_OnFile], [ContractNo], [StartDate], [Cust_Data_Name_FK], [Product_Data_Name_FK], [CustValueShort], [CustValue], [ProductValue], [Price_For_Calculation], [StoreSellPrice], [CorpSellPrice], [CreateRecordType], [DS Multiplier], [DC Multiplier], [DataValidationStatus], [MassCreateStatus], [Create_Timestamp], [Update_Timestamp], [Create_User], [Update_User], [Import_Filename], [Customer_Johnstone_MM_PD_FK], Job_FK)
		Select
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], Null VPN_On_File, Null VendorNo_OnFile, Null VendorName_OnFile, JPR.[Contract #]
		, Case isdate(JPR.[Start Date]) when 0 then Cast(Cast(JPR.[Start Date] as bigint) as smalldatetime) else Cast(JPR.[Start Date] as smalldatetime) end 'StartDate'
		, 144010, 110100, JPR.[3 Digit Store #], JPR.[6 Digit Store #], pi_prod.value, JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), 'Price' 'Record type', JPR.[DS Multiplier], JPR.[DC Multiplier], 8, 0, Getdate(), Getdate(), SUSER_SNAME(), SUSER_SNAME(), JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK
		from 
		[import].[Customer_Johnstone_MM_PD] JPR with (nolock)
		Left Join epacube.product_identification pi_prod with (nolock)
			on pi_prod.value = JPR.[Johnstone Stock #] and pi_prod.data_name_fk = 110100
		Left Join [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] New with (Nolock)
			On 1 = 1	
			--and JPR.Customer_Johnstone_MM_PD_ID  = New.Customer_Johnstone_MM_PD_FK and new.createrecordtype = 'Price'
			and jpr.[Vendor #] = new.VendorNo
			and jpr.job_fk = new.job_fk
			and jpr.[6 Digit Store #]  = new.CustValue
			and jpr.[Johnstone Stock #]  = new.ProductValue
			and new.createrecordtype = 'Price'
		Where 1 = 1
		and New.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID is null
		and pi_prod.product_structure_fk is null
		and JPR.[mfg part #] is not null
		and jpr.Job_FK = @in_job_fk
		Group by
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], JPR.[Contract #], JPR.[Start Date], JPR.[3 Digit Store #], JPR.[6 Digit Store #], pi_prod.value, JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), JPR.[DS Multiplier], JPR.[DC Multiplier], JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK
		Order by JPR.[6 Digit Store #], pi_prod.value

		--2 DataValidationStatus = VPN's don't match, but Product ID's do
		Insert into [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE]
		([VendorPartNumber], [VendorNo], [VendorName], [VendorPartNumber_OnFile], [VendorNo_OnFile], [VendorName_OnFile], [ContractNo], [StartDate], [Cust_Data_Name_FK], [Product_Data_Name_FK], [CustValueShort], [CustValue], [ProductValue], [Price_For_Calculation], [StoreSellPrice], [CorpSellPrice], [CreateRecordType], [DS Multiplier], [DC Multiplier], [DataValidationStatus], [MassCreateStatus], [Create_Timestamp], [Update_Timestamp], [Create_User], [Update_User], [Import_Filename], [Customer_Johnstone_MM_PD_FK], Job_FK)
		Select
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], pi_vpn.value VPN_On_File, ei_vndr.value VendorNo_OnFile, ein.value VendorName_OnFile, JPR.[Contract #]
		, Case isdate(JPR.[Start Date]) when 0 then Cast(Cast(JPR.[Start Date] as bigint) as smalldatetime) else Cast(JPR.[Start Date] as smalldatetime) end 'StartDate'
		, 144010, 110100, JPR.[3 Digit Store #], JPR.[6 Digit Store #], pi_prod.value, JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), 'Price' 'Record type', JPR.[DS Multiplier], JPR.[DC Multiplier], 2, 0, Getdate(), Getdate(), SUSER_SNAME(), SUSER_SNAME(), JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK
		from epacube.product_identification pi_prod with (nolock)
		left join epacube.product_identification pi_vpn with (nolock) 
			on pi_prod.DATA_NAME_FK = 110100 
			and pi_vpn.data_name_fk = 110111
			and pi_prod.product_structure_fk = pi_vpn.PRODUCT_STRUCTURE_FK
		left join epacube.product_association pa with (nolock)
			on pa.data_name_fk = 253501
			and pi_prod.product_structure_fk = pa.PRODUCT_STRUCTURE_FK
			and pi_vpn.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK
		left join epacube.entity_identification ei_vndr with (nolock)
			on ei_vndr.entity_data_name_fk = 143000 and ei_vndr.DATA_NAME_FK = 143110
			and pa.entity_structure_fk = ei_vndr.ENTITY_STRUCTURE_FK
		Left Join epacube.ENTITY_IDENT_NONUNIQUE EIN with (nolock)
			on ein.entity_data_name_fk = 143000
			and ein.data_name_fk = 143112
			and ei_vndr.ENTITY_STRUCTURE_FK = ein.ENTITY_STRUCTURE_FK
		inner join [import].[Customer_Johnstone_MM_PD] JPR with (nolock)
			on pi_prod.value = JPR.[Johnstone Stock #]
			and pi_vpn.value <> JPR.[Mfg Part #]
		inner join epacube.ENTITY_IDENTIFICATION ei_cust with (nolock)
			on ei_cust.entity_data_name_fk in (144010, 144020)
			and ei_cust.value = JPR.[6 Digit Store #]
		Left Join [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] New with (Nolock)
			On 1 = 1	
			--and JPR.Customer_Johnstone_MM_PD_ID  = New.Customer_Johnstone_MM_PD_FK and new.createrecordtype = 'Price'
			and jpr.[Vendor #] = new.VendorNo
			and jpr.job_fk = new.job_fk
			and jpr.[6 Digit Store #]  = new.CustValue
			and jpr.[Johnstone Stock #]  = new.ProductValue
			and new.createrecordtype = 'Price'
		Where JPR.[Start Date] is not null and JPR.[6 Digit Store #] is not null and JPR.[Johnstone Stock #] is not null
		and New.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID is null
		And isnull(JPR.[Store Price], 0) * isnull(JPR.[DC Multiplier], 0) <> 0
		and JPR.[mfg part #] is not null
		and jpr.Job_FK = @in_job_fk
		Group by
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], pi_vpn.value, ei_vndr.value, ein.value, JPR.[Contract #], JPR.[Start Date], JPR.[3 Digit Store #], JPR.[6 Digit Store #], pi_prod.value, JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), JPR.[DS Multiplier], JPR.[DC Multiplier], JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK


		--3 DataValidationStatus = Vendor Part Number Matches to something epaCUBE, but not expected product
		Insert into [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE]
		([VendorPartNumber], [VendorNo], [VendorName], [VendorPartNumber_OnFile], [VendorNo_OnFile], [VendorName_OnFile], [ContractNo], [StartDate], [Cust_Data_Name_FK], [Product_Data_Name_FK], [CustValueShort], [CustValue], [ProductValue], [Price_For_Calculation], [StoreSellPrice], [CorpSellPrice], [CreateRecordType], [DS Multiplier], [DC Multiplier], [DataValidationStatus], [MassCreateStatus], [Create_Timestamp], [Update_Timestamp], [Create_User], [Update_User], [Import_Filename], [Customer_Johnstone_MM_PD_FK], Job_FK)
		Select
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], pi_vpn.value VPN_On_File, ei_vndr.value VendorNo_OnFile, ein.value VendorName_OnFile, JPR.[Contract #]
		, Case isdate(JPR.[Start Date]) when 0 then Cast(Cast(JPR.[Start Date] as bigint) as smalldatetime) else Cast(JPR.[Start Date] as smalldatetime) end 'StartDate'
		, 144010, 110100, JPR.[3 Digit Store #], JPR.[6 Digit Store #], pi_prod.value, JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), 'Price' 'Record type', JPR.[DS Multiplier], JPR.[DC Multiplier], 3, 0, Getdate(), Getdate(), SUSER_SNAME(), SUSER_SNAME(), JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK
		from epacube.product_identification pi_vpn with (nolock) 
		Left Join epacube.product_identification pi_prod with (nolock)
			on pi_prod.DATA_NAME_FK = 110100 
			and pi_vpn.data_name_fk = 110111
			and pi_prod.product_structure_fk = pi_vpn.PRODUCT_STRUCTURE_FK
		Left join epacube.product_association pa with (nolock)
			on pa.data_name_fk = 253501
			and pi_prod.product_structure_fk = pa.PRODUCT_STRUCTURE_FK
			and pi_vpn.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK
		Left join epacube.entity_identification ei_vndr with (nolock)
			on ei_vndr.entity_data_name_fk = 143000 and ei_vndr.DATA_NAME_FK = 143110
			and pa.entity_structure_fk = ei_vndr.ENTITY_STRUCTURE_FK
		Left Join epacube.ENTITY_IDENT_NONUNIQUE EIN with (nolock)
			on ein.entity_data_name_fk = 143000
			and ein.data_name_fk = 143112
			and ei_vndr.ENTITY_STRUCTURE_FK = ein.ENTITY_STRUCTURE_FK
		inner join [import].[Customer_Johnstone_MM_PD] JPR with (nolock)
			on pi_vpn.value = JPR.[Mfg Part #]
		left join epacube.ENTITY_IDENTIFICATION ei_cust with (nolock)
			on ei_cust.entity_data_name_fk in (144010, 144020)
			and ei_cust.value = JPR.[6 Digit Store #]
		Left Join [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] New with (Nolock)
			On 1 = 1	
			--and JPR.Customer_Johnstone_MM_PD_ID  = New.Customer_Johnstone_MM_PD_FK and new.createrecordtype = 'Price'
			and jpr.[Vendor #] = new.VendorNo
			and jpr.job_fk = new.job_fk
			and jpr.[6 Digit Store #]  = new.CustValue
			and jpr.[Johnstone Stock #]  = new.ProductValue
			 and new.createrecordtype = 'Price'
		Where JPR.[Start Date] is not null and JPR.[6 Digit Store #] is not null and JPR.[Johnstone Stock #] is not null
		and New.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID is null
		And isnull(JPR.[Store Price], 0) * isnull(JPR.[DC Multiplier], 0) <> 0
		and JPR.[mfg part #] is not null
		and jpr.Job_FK = @in_job_fk
		Group by
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], pi_vpn.value, ei_vndr.value, ein.value, JPR.[Contract #], JPR.[Start Date], JPR.[3 Digit Store #], JPR.[6 Digit Store #], pi_prod.value, JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), JPR.[DS Multiplier], JPR.[DC Multiplier], JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK



		--9 DataValidationStatus = Other Information is Not Correct
		Insert into [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE]
		([VendorPartNumber], [VendorNo], [VendorName], [ContractNo], [StartDate], [Cust_Data_Name_FK], [Product_Data_Name_FK], [CustValueShort], [CustValue], [ProductValue], [Price_For_Calculation], [StoreSellPrice], [CorpSellPrice], [CreateRecordType], [DS Multiplier], [DC Multiplier], [DataValidationStatus], [MassCreateStatus], [Create_Timestamp], [Update_Timestamp], [Create_User], [Update_User], [Import_Filename], [Customer_Johnstone_MM_PD_FK], Job_FK)
		Select
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], JPR.[Contract #]
		, Case isdate(JPR.[Start Date]) when 0 then Cast(Cast(JPR.[Start Date] as bigint) as smalldatetime) else Cast(JPR.[Start Date] as smalldatetime) end 'StartDate'
		, 144010, 110100, JPR.[3 Digit Store #], JPR.[6 Digit Store #], JPR.[Johnstone Stock #], JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), 'Price' 'Record type', JPR.[DS Multiplier], JPR.[DC Multiplier], 9, 0, Getdate(), Getdate(), SUSER_SNAME(), SUSER_SNAME(), JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK
		from [import].[Customer_Johnstone_MM_PD] JPR with (nolock)
		Left Join [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] New with (Nolock)
			On 1 = 1	
			--and JPR.Customer_Johnstone_MM_PD_ID  = New.Customer_Johnstone_MM_PD_FK and new.createrecordtype = 'Price'
			and jpr.[Vendor #] = new.VendorNo
			and jpr.job_fk = new.job_fk
			and jpr.[6 Digit Store #]  = new.CustValue
			and jpr.[Johnstone Stock #]  = new.ProductValue
			 and new.createrecordtype = 'Price'
		Where 1 = 1
		and New.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID is null
		and JPR.[mfg part #] is not null
		and jpr.Job_FK = @in_job_fk
		Group by
		JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], JPR.[Contract #], JPR.[Start Date], JPR.[3 Digit Store #], JPR.[6 Digit Store #], JPR.[Johnstone Stock #], JPR.[Store Price], JPR.[Store Price] * isnull(JPR.[DC Multiplier], 0), JPR.[Store Price] * isnull(JPR.[DS Multiplier], 0), JPR.[DS Multiplier], JPR.[DC Multiplier], JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK

--------------------------------
--End of Price Records Load
--------------------------------
--------------------------------
--------------------------------
--Rebate Records Load
--------------------------------

--Load PDSR Rebates
	Insert into [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE]
	([VendorPartNumber], [VendorNo], [VendorName], [VendorPartNumber_OnFile], [VendorNo_OnFile], [VendorName_OnFile], [ContractNo], [StartDate], [Cust_Data_Name_FK], [Product_Data_Name_FK], [CustValueShort], [CustValue], [ProductValue]
	--, [ClaimbackFrom], [ClaimbackDownTo]
	, ClaimBackType, ClaimBackFromDN_FK, ClaimBackEndValue, ClaimBackAction
	, [CreateRecordType], [DS Multiplier], [DC Multiplier], [DataValidationStatus], [MassCreateStatus], [Create_Timestamp], [Update_Timestamp], [Create_User], [Update_User], [Import_Filename], [Customer_Johnstone_MM_PD_FK], Job_FK)
	Select
	JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], pi_vpn.value VPN_On_File, ei_vndr.value VendorNo_OnFile, ein.value VendorName_OnFile, JPR.[Contract #]
	, Case isdate(JPR.[Start Date]) when 0 then Cast(Cast(JPR.[Start Date] as bigint) as smalldatetime) else Cast(JPR.[Start Date] as smalldatetime) end 'StartDate'
	, 144010, 110100, JPR.[3 Digit Store #], JPR.[6 Digit Store #], pi_prod.value
	--, jpr.[Claimback From], jpr.[Claimback Down To] 
	, jpr.[Standard Claimback Type]
	, Case jpr.[Standard Claimback Type] When 1 then 100100531 When 2 then 100100417 else Null end 'ClaimBackFromDN_FK'
	, Case jpr.[Standard Claimback Type] 
		When 1 then JPR.[Store Price] * isnull(jpr.[DS Multiplier], 0)
		When 2 then JPR.[Store Price] * isnull(jpr.[DC Multiplier], 0)
		When 3 then JPR.[Fixed Claimback Amount] else Null end 'ClaimBackEndValue'

	, Replace(Replace(Case jpr.[Standard Claimback Type]	when 1 then (Select Label from epacube.data_name where data_name_id = 100100531) + ' To '
											when 2 then (Select Label from epacube.data_name where data_name_id = 100100417) + ' To '
											when 3 then 'Fixed Amt Of ' end, 'Current Repl Cost To', 'Cur RC To'), 'Regular Base Price To', 'Reg Base To') 'ClaimBackAction'
	, 'Rebate' 'Record type', JPR.[DS Multiplier], JPR.[DC Multiplier], 1, 0, Getdate(), Getdate(), SUSER_SNAME(), SUSER_SNAME(), JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK
	from epacube.product_identification pi_prod with (nolock)
	inner join epacube.product_identification pi_vpn with (nolock) 
		on pi_prod.DATA_NAME_FK = 110100 
		and pi_vpn.data_name_fk = 110111
		and pi_prod.product_structure_fk = pi_vpn.PRODUCT_STRUCTURE_FK
	inner join epacube.product_association pa with (nolock)
		on pa.data_name_fk = 253501
		and pi_prod.product_structure_fk = pa.PRODUCT_STRUCTURE_FK
		and pi_vpn.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK
	inner join epacube.entity_identification ei_vndr with (nolock)
		on ei_vndr.entity_data_name_fk = 143000 and ei_vndr.DATA_NAME_FK = 143110
		and pa.entity_structure_fk = ei_vndr.ENTITY_STRUCTURE_FK
	Left Join epacube.ENTITY_IDENT_NONUNIQUE EIN with (nolock)
		on ein.entity_data_name_fk = 143000
		and ein.data_name_fk = 143112
		and ei_vndr.ENTITY_STRUCTURE_FK = ein.ENTITY_STRUCTURE_FK
	inner join [import].[Customer_Johnstone_MM_PD] JPR with (nolock)
		on pi_prod.value = JPR.[Johnstone Stock #]
		and pi_vpn.value = JPR.[Mfg Part #]
	inner join epacube.ENTITY_IDENTIFICATION ei_cust with (nolock)
		on ei_cust.entity_data_name_fk in (144010, 144020)
		and ei_cust.value = JPR.[6 Digit Store #]
	Left Join [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] New with (Nolock)
		On 1 = 1	
		--and JPR.Customer_Johnstone_MM_PD_ID  = New.Customer_Johnstone_MM_PD_FK and new.createrecordtype = 'Price'
		and jpr.[Vendor #] = new.VendorNo
		and jpr.job_fk = new.job_fk
		and jpr.[6 Digit Store #]  = new.CustValue
		and jpr.[Johnstone Stock #]  = new.ProductValue
		and JPR.[mfg part #] is not null
		 and new.createrecordtype = 'Rebate'
	Where JPR.[Start Date] is not null and JPR.[6 Digit Store #] is not null and JPR.[Johnstone Stock #] is not null
	and JPR.[Mfg Part #] = pi_vpn.value
	and New.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID is null
	and jpr.Job_FK = @in_job_fk
	And isnull(JPR.[Standard Claimback Type], 0) <> 0
	Group by
	JPR.[Mfg Part #], JPR.[Vendor #], JPR.[Vendor Name], pi_vpn.value, ei_vndr.value, ein.value, JPR.[Contract #], JPR.[Start Date], JPR.[3 Digit Store #], JPR.[6 Digit Store #], pi_prod.value
	, jpr.[Standard Claimback Type]
	, Case jpr.[Standard Claimback Type] When 1 then 1001100531 When 2 then 100100417 else Null end
	, Case jpr.[Standard Claimback Type] 
		When 1 then JPR.[Store Price] * isnull(jpr.[DS Multiplier], 0)
		When 2 then JPR.[Store Price] * isnull(jpr.[DC Multiplier], 0)
		When 3 then JPR.[Fixed Claimback Amount] else Null end
	, JPR.[DS Multiplier], JPR.[DC Multiplier], JPR.[Import_Filename], JPR.[Customer_Johnstone_MM_PD_ID], jpr.Job_FK

--------------------------------
--END OF Rebate Records Load
--------------------------------

Update M
Set Action_Status = 0
from [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] M where Action_Status is null and job_fk = @in_job_fk

IF object_id('tempdb..##MCR') is not null
drop table ##MCR

IF object_id('tempdb..##PA') is not null
drop table ##PA

IF object_id('tempdb..##RF') is not null
drop table ##RF

Select Max(r1.effective_date) Effective_Date, rfs1.PROD_FILTER_FK, isnull(rfs1.ORG_FILTER_FK, 0) Org_Filter_FK, rfs1.CUST_FILTER_FK, isnull(rfs1.SUPL_FILTER_FK, 0) Supl_Filter_FK 
into ##MCR
from synchronizer.rules r1 with (nolock)
inner join synchronizer.rules_filter_set rfs1 with (nolock) on r1.rules_id = rfs1.rules_fk
where r1.RESULT_DATA_NAME_FK in (111602, 111501, 111502) and r1.EFFECTIVE_DATE <= getdate()
group by rfs1.PROD_FILTER_FK, isnull(rfs1.ORG_FILTER_FK, 0), rfs1.CUST_FILTER_FK, isnull(rfs1.SUPL_FILTER_FK, 0)

Create index idx_fks on ##MCR(Effective_Date, PROD_FILTER_FK, Org_Filter_FK, CUST_FILTER_FK, Supl_Filter_FK)

Select product_structure_fk, entity_structure_fk into ##PA from epacube.product_association pa with (nolock) where pa.DATA_NAME_FK = 253501 group by product_structure_fk, entity_structure_fk
create index idx_pa on ##PA(product_structure_fk, entity_structure_fk)

Select * into ##RF from synchronizer.rules_filter with (nolock) where data_name_fk = 110100 or entity_data_name_fk in (144010, 144020)
Create index idx_rf on ##RF(rules_filter_ID, value1, data_name_fk, entity_data_name_fk)

--Declare @Vendor_ID varchar(36)
--Set @Vendor_ID = (select top 1 [vendor #] from [import].[Customer_Johnstone_MM_PD] with (nolock) where job_fk = @In_Job_FK)

Delete from import.Customer_Johnstone_MM_Triage where Vendor_ID in (select VendorNo from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] with (nolock) where job_fk = @In_Job_FK group by vendorno)

--Price Triage
Insert into import.Customer_Johnstone_MM_Triage
(
[epaRules_ID], [Rule_ID], [EFFECTIVE_DATE], [Product_ID], [Product_Description], [Store_ID], [Store_Name], [Vendor_ID], [Whse_ID], [Store_Status], [Parent_Entity_Structure_FK], [Matching New Record], [New Records to Create], [Parent Stores New], [Child Stores New], [Unique Products New], [PD Record Sell Price], [New Sell Price], [PD Record Rebate Amount], [New Rebate Amount], [CreateRecordType], [Create_User], [Create_Timestamp], [Job_FK]
)
select r.rules_id, r.host_rule_xref 'Rule_ID', r.EFFECTIVE_DATE, rfp.VALUE1 'Product_ID', pd.[description] 'Product_Description', rfc.value1 'Store_ID', ein.value 'Store_Name', ei.value 'Vendor_ID', rfo.value1 'Whse ID'
, Case ei_c.entity_data_name_fk when 144010 then 'Parent' else 'Child' end Store_Status 
, isnull(es_p.parent_Entity_Structure_fk, es_p.entity_structure_id) Parent_Entity_Structure_FK
, Case When isnull(iprmm.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID, 0) <> 0 then 1 else 0 end 'Matching New Record'
, (Select count(*) from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] with (nolock) where createrecordtype = 'Price' and VendorNo = ei.value and Job_FK = @in_job_fk) 'New Records to Create'
, (Select Count(*) from (Select CustValue, Cust_Data_Name_FK from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] where job_fk = @in_job_FK and VendorNo = ei.value group by CustValue, Cust_Data_Name_FK) A where Cust_Data_Name_FK = 144010) 'Parent Stores New'
, (Select Count(*) from (Select CustValue, Cust_Data_Name_FK from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] where job_fk = @in_job_FK and VendorNo = ei.value group by CustValue, Cust_Data_Name_FK) A where Cust_Data_Name_FK = 144020) 'Child Stores New'
, (Select Count(*) from (Select ProductValue from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] where job_fk = @in_job_FK and VendorNo = ei.value group by ProductValue) A) 'Unique Products New'
, Case when ra.RULES_ACTION_OPERATION1_FK = 1021 then Basis1_Number end 'PD Record Sell Price'
, iprmm.StoreSellPrice 'New Sell Price'
, Null 'PD Record Rebate Amount'
, Null 'New Rebate Amount'
, 'Price' RecordType
, Null Create_User
, getdate() Create_Timestamp
, @in_job_fk Job_FK

from synchronizer.rules r with (nolock) --where RESULT_DATA_NAME_FK = 111602
inner join synchronizer.rules_action ra with (nolock) on r.rules_id = ra.rules_fk
inner join synchronizer.rules_filter_set rfs with (nolock) on r.RULES_ID = rfs.RULES_FK
inner join ##RF rfp with (nolock) on rfs.PROD_FILTER_FK = rfp.RULES_FILTER_ID and rfp.data_name_fk = 110100
inner join ##RF rfc with (nolock) on rfs.cust_filter_fk = rfc.RULES_FILTER_ID and rfc.entity_data_name_fk in (144010, 144020)
left join ##RF rfo with (nolock) on rfs.org_filter_fk = rfo.rules_filter_id
inner join epacube.product_identification pi with (nolock) on rfp.VALUE1 = pi.value and pi.data_name_fk = 110100
inner join ##pa pa on pi.product_structure_fk = pa.PRODUCT_STRUCTURE_FK
inner join epacube.entity_identification ei with (nolock) on pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK and ei.ENTITY_DATA_NAME_FK like '143%'
inner join epacube.entity_identification ei_c with (nolock) on rfc.value1 = ei_c.value and ei_c.entity_data_name_fk in (144010, 144020)
inner join epacube.entity_structure es_p with (nolock) on ei_c.entity_structure_fk = es_p.entity_structure_id
left join epacube.entity_ident_nonunique ein with (nolock) on ei_c.entity_structure_fk = ein.ENTITY_STRUCTURE_FK and ein.data_name_fk = 144112
left join epacube.product_description pd with (nolock) on pi.PRODUCT_STRUCTURE_FK = pd.PRODUCT_STRUCTURE_FK and pd.DATA_NAME_FK = 110401
left join [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] iprmm with (nolock) on pi.value = iprmm.[ProductValue] and rfc.value1 = iprmm.[CustValue] and iprmm.createrecordtype = 'Price'
	and iprmm.Job_FK = @in_job_fk

inner join ##MCR MCR on rfs.PROD_FILTER_FK = MCR.PROD_FILTER_FK 
		and isnull(rfs.org_filter_fk, 0) = MCR.Org_Filter_FK 
		and rfs.CUST_FILTER_FK = MCR.CUST_FILTER_FK 
		and isnull(rfs.SUPL_FILTER_FK, 0) = MCR.Supl_Filter_FK
		and r.effective_date = MCR.Effective_Date

where r.RESULT_DATA_NAME_FK = 111602
and r.HOST_PRECEDENCE_LEVEL = '1'
and r.RECORD_STATUS_CR_FK = 1
and ei.value in (select VendorNo from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] with (nolock) where job_fk = @In_Job_FK group by vendorno)
group by r.rules_id, r.host_rule_xref, r.EFFECTIVE_DATE, rfp.VALUE1, pd.[description], rfc.value1, rfo.value1, ei_c.entity_data_name_fk, isnull(es_p.parent_Entity_Structure_fk, es_p.entity_structure_id), ei.value, ein.value, ra.RULES_ACTION_OPERATION1_FK , Basis1_Number, iprmm.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID, iprmm.StoreSellPrice
Order by rfp.VALUE1, rfc.value1

/*
--Claimback Triage-Rebates
Insert into import.Customer_Johnstone_MM_Triage
(
[Rule_ID], [EFFECTIVE_DATE], [Product_ID], [Product_Description], [Store_ID], [Store_Name], [Vendor_ID], [Store_Status], [Parent_Entity_Structure_FK], [Matching New Record], [PD Record Sell Price], [New Sell Price], [PD Record Rebate Amount], [New Rebate Amount], [CreateRecordType], [Create_User], [Create_Timestamp], [Job_FK]
)
select r.host_rule_xref 'Rule_ID', r.EFFECTIVE_DATE, rfp.VALUE1 'Product_ID', pd.[description] 'Product_Description', rfc.value1 'Store_ID', ein.value 'Store_Name', ei.value 'Vendor_ID'
, Case ei_c.entity_data_name_fk when 144010 then 'Parent' else 'Child' end Store_Status 
, isnull(es_p.parent_Entity_Structure_fk, es_p.entity_structure_id) Parent_Entity_Structure_FK
, Case When isnull(iprmm.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID, 0) <> 0 then 1 else 0 end 'Matching New Record'
, Null 'PD Record Sell Price'
, Null 'New Sell Price'
, Case when ra.RULES_ACTION_OPERATION1_FK = 1021 then Basis1_Number end 'PD Record Rebate Amount'
, Null 'New Rebate Amount'
, 'Rebate' RecordType
, Null Create_User
, getdate() Create_Timestamp
, @in_job_fk Job_FK

from synchronizer.rules r with (nolock) --where RESULT_DATA_NAME_FK = 111602
inner join synchronizer.rules_action ra with (nolock) on r.rules_id = ra.rules_fk
inner join synchronizer.rules_precedence rp with (nolock) on r.RULES_PRECEDENCE_FK = rp.RULES_PRECEDENCE_ID
inner join synchronizer.rules_filter_set rfs with (nolock) on r.RULES_ID = rfs.RULES_FK
inner join ##RF rfp with (nolock) on rfs.PROD_FILTER_FK = rfp.RULES_FILTER_ID and rfp.data_name_fk = 110100
inner join ##RF rfc with (nolock) on rfs.cust_filter_fk = rfc.RULES_FILTER_ID and rfc.entity_data_name_fk in (144010, 144020)
inner join epacube.product_identification pi with (nolock) on rfp.VALUE1 = pi.value and pi.data_name_fk = 110100
inner join ##pa pa on pi.product_structure_fk = pa.PRODUCT_STRUCTURE_FK
inner join epacube.entity_identification ei with (nolock) on pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK and ei.ENTITY_DATA_NAME_FK like '143%'
inner join epacube.entity_identification ei_c with (nolock) on rfc.value1 = ei_c.value and ei_c.entity_data_name_fk in (144010, 144020)
inner join epacube.entity_structure es_p with (nolock) on ei_c.entity_structure_fk = es_p.entity_structure_id
left join epacube.entity_ident_nonunique ein with (nolock) on ei_c.entity_structure_fk = ein.ENTITY_STRUCTURE_FK and ein.data_name_fk = 144112
left join epacube.product_description pd with (nolock) on pi.PRODUCT_STRUCTURE_FK = pd.PRODUCT_STRUCTURE_FK and pd.DATA_NAME_FK = 110401
left join [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] iprmm with (nolock) on pi.value = iprmm.[ProductValue] and rfc.value1 = iprmm.[CustValue] and iprmm.createrecordtype = 'Rebate'
	and iprmm.Job_FK = @in_job_fk

inner join ##MCR MCR on rfs.PROD_FILTER_FK = MCR.PROD_FILTER_FK 
		and isnull(rfs.org_filter_fk, 0) = MCR.Org_Filter_FK 
		and rfs.CUST_FILTER_FK = MCR.CUST_FILTER_FK 
		and isnull(rfs.SUPL_FILTER_FK, 0) = MCR.Supl_Filter_FK
		and r.effective_date = MCR.Effective_Date

where r.RESULT_DATA_NAME_FK = 111501
--and rp.HOST_PRECEDENCE_LEVEL = '1'
and r.RECORD_STATUS_CR_FK = 1
and ei.value in (select VendorNo from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] with (nolock) where job_fk = @In_Job_FK group by vendorno)
group by r.host_rule_xref, r.EFFECTIVE_DATE, rfp.VALUE1, pd.[description], rfc.value1, ei_c.entity_data_name_fk, isnull(es_p.parent_Entity_Structure_fk, es_p.entity_structure_id), ei.value, ein.value, ra.RULES_ACTION_OPERATION1_FK , Basis1_Number, iprmm.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID
Order by rfp.VALUE1, rfc.value1
*/

Update immpr
set customername = (select ein.value from epacube.ENTITY_IDENT_NONUNIQUE ein with (nolock)
	inner join epacube.entity_identification ei with (nolock) 
		on ein.entity_data_name_fk = ei.entity_data_name_fk 
		and ein.data_name_fk = 144112 
		and ei.data_name_fk = 144111
		and ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK 
where ei.value = immpr.custvalue) 
from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] immpr
where customername is null

IF object_id('tempdb..##MCR') is not null
drop table ##MCR

IF object_id('tempdb..##PA') is not null
drop table ##PA

IF object_id('tempdb..##RF') is not null
drop table ##RF

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------
	
	select @TotalAfterRecords = count(*) from [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] with (nolock)

	SET @V_END_TIMESTAMP = getdate()

      EXEC common.job_execution_create  @in_job_fk, 'Inserted to table CUSTOMER_JOHNSTONE_TO_PRODUCTION_MM_PRICE_REBATE',
		                                 @TotalAfterRecords, @TotalAfterRecords, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

     SET @status_desc =  'finished execution of [import].[CUSTOMER_JOHNSTONE_TO_PRODUCTION_MM_PRICE]';
     EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
     
      
      EXEC exec_monitor.print_status_sp @exec_id = @l_exec_no;
   END TRY
   BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;

EndOfProcedure:

END

