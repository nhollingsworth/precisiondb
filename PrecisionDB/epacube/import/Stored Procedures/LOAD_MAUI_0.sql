﻿--A_Update_MAUI_0

CREATE PROCEDURE [import].[LOAD_MAUI_0]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

insert into dbo.maui_log Select 0, 'LOAD_MAUI_0 CALLED THRU SERVICE BROKER', GetDate(), Null, Null
	
Declare @ServiceBrokerCall Varchar(Max)

SET @ServiceBrokerCall = 'EXEC EPACUBE.[IMPORT].[LOAD_MAUI_0_MASTER] ' 

EXEC queue_manager.enqueue
	@service_name = N'TargetEpaService', --  nvarchar(50)
	@cmd_text = @ServiceBrokerCall, --  nvarchar(4000)
	@epa_job_id = 0, --  int
	@cmd_type = N'SQL' --  nvarchar(10)
	
Print(@ServiceBrokerCall)

END
