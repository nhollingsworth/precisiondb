﻿


-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- KS        11/14/2010   Initial Version
-- KS        05/30/2012   Do not import Contract Rule if CNTRCT_ID is null 
--
/*
        BEST PRICE and COST

        BPC = 0 ....      That means Best Price is  ON or OFF? – 0 = ON and 1 = OFF
		BCC = 'No' ....   That means Best Cost is  ON or OFF? – Yes = ON and No = OFF

*/


CREATE PROCEDURE [import].[LOAD_ECLIPSE_MATRIX] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @V_COUNT			BIGINT

	DECLARE @v_job_fk         bigint
	DECLARE @v_job_class_fk   int
	DECLARE @v_job_name   varchar(32)
	DECLARE @v_data1      varchar(32)
	DECLARE @v_data2      varchar(32)
	DECLARE @v_job_user   varchar(32)
	DECLARE @v_job_date   datetime
	DECLARE @v_input_rows      int
	DECLARE @v_output_rows     int
	DECLARE @v_exception_rows  int
	DECLARE @v_out_job_fk BIGINT	

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  

	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_ECLIPSE_MATRIX', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


   IF @in_job_fk NOT IN ( SELECT JOB_ID
						 FROM COMMON.JOB 
						 WHERE JOB_ID = @in_job_fk )
   BEGIN 

    SET @v_job_fk = @in_job_fk
    SET @v_job_class_fk  = 999        --- NOT SURE ??
    SET @v_job_user = 'EPACUBE'
    SET @v_job_name = 'ECLIPSE MATRIX IMPORT'
    SET @v_job_date = @l_sysdate
    SET @v_data1   = ( SELECT TOP 1 IMPORT_FILENAME FROM IMPORT.IMPORT_ECLIPSE_MATRIX
                       WHERE JOB_FK = @IN_JOB_FK )

    EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
										   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;


    EXEC common.job_execution_create   @v_job_fk, 'START RULE IMPORT:: ',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate    

   END 

      SELECT @v_input_rows = COUNT(1)
        FROM IMPORT.IMPORT_ECLIPSE_MATRIX WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM IMPORT.IMPORT_ECLIPSE_MATRIX WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD ECLIPSE MATRIX',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;





TRUNCATE TABLE IMPORT.IMPORT_RULES_DATA
TRUNCATE TABLE IMPORT.IMPORT_RULES



------------------------- MAKE SURE MATRIX RULE IS REBATED COST OF REPL COST *1 >> TO BE REMOVED

INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[CREATE_TIMESTAMP])   
SELECT       
			 IEM.JOB_FK     
		   ,'MATRIX RULE IS GENERIC REBATED COST = REPL COST *1 '
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
			,'MATRIX ID IS BLOCKED FROM IMPORT '
           ,IEM.MATRIX_ID
           ,GETDATE ()  
---           
FROM import.IMPORT_ECLIPSE_MATRIX IEM WITH (NOLOCK) 
WHERE 1 = 1
AND   JOB_FK = @IN_JOB_FK 
AND   ( RTRIM ( LTRIM ( ISNULL ( IEM.CF_OPERATION1, ' ' ) ) ) <> '' )    ---- HAS A REBATE 
AND   IEM.C_BASIS IN ( 'REPL_COS', 'REPL-COS', 'REPLCOST' )
AND   IEM.CF_OPERATION1 = '*' 
AND   IEM.CF_OPERAND1 IN ( '1', '1.0' )
AND   IEM.Matrix_ID LIKE '~CALL~GALL~%'



------------------------- MAKE SURE MATRIX_ID IS NOT DUPLICATED

INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[CREATE_TIMESTAMP])   
SELECT       
			 IEM.JOB_FK     
		   ,'MATRIX RULES'
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
			,'MATRIX ID IS DUPLICATED'
           ,IEM.MATRIX_ID
           ,GETDATE ()                   
FROM import.IMPORT_ECLIPSE_MATRIX IEM WITH (NOLOCK) 
WHERE 1 = 1
AND   JOB_FK = @IN_JOB_FK 
GROUP BY IEM.JOB_FK, IEM.Matrix_ID
HAVING COUNT(*) > 1



------------------------- MAKE SURE MATRIX_ID IS NOT NULL OR BLANK

           
INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[CREATE_TIMESTAMP])   
SELECT       
			 IEM.JOB_FK     
		   ,'MATRIX RULES'
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
			,'MATRIX ID IS BLANK OR NULL'
           ,IEM.MATRIX_ID
           ,GETDATE ()                              
FROM import.IMPORT_ECLIPSE_MATRIX IEM WITH (NOLOCK) 
WHERE 1 = 1
AND   JOB_FK = @IN_JOB_FK 
AND   ( RTRIM ( LTRIM ( ISNULL (MATRIX_ID, ' ' ) ) ) = '' )



------------------------- MAKE SURE OPERANDS ARE NUMBERIC
INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[CREATE_TIMESTAMP])
SELECT
            IEM.JOB_FK
            ,'MATRIX RULES'
            ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'CF_OPERAND1 IS NOT NUMERIC'
           ,IEM.MATRIX_ID
           ,GETDATE ()
FROM import.IMPORT_ECLIPSE_MATRIX IEM WITH (NOLOCK) 
WHERE 1 = 1
AND   JOB_FK = @IN_JOB_FK 
AND   ( RTRIM ( LTRIM ( ISNULL (CF_OPERATION1, ' ' ) ) ) <> '' )
AND   ISNUMERIC ( CF_OPERAND1 ) = 0



INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[CREATE_TIMESTAMP])
SELECT
            IEM.JOB_FK
            ,'MATRIX RULES'
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'CF_OPERAND2 IS NOT NUMERIC'
           ,IEM.MATRIX_ID
           ,GETDATE ()
FROM import.IMPORT_ECLIPSE_MATRIX IEM WITH (NOLOCK) 
WHERE 1 = 1
AND   JOB_FK = @IN_JOB_FK 
AND   ( RTRIM ( LTRIM ( ISNULL (CF_OPERATION2, ' ' ) ) ) <> '' )
AND   ISNUMERIC ( CF_OPERAND2 ) = 0



INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[CREATE_TIMESTAMP])
SELECT
            IEM.JOB_FK
           ,'MATRIX RULES'
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'CF_OPERAND3 IS NOT NUMERIC'
           ,IEM.MATRIX_ID
           ,GETDATE ()
FROM import.IMPORT_ECLIPSE_MATRIX IEM WITH (NOLOCK) 
WHERE 1 = 1
AND   JOB_FK = @IN_JOB_FK 
AND   ( RTRIM ( LTRIM ( ISNULL (CF_OPERATION3, ' ' ) ) ) <> '' )
AND   ISNUMERIC ( CF_OPERAND3 ) = 0


------ MAKE SURE CONTRACTS EXIST

           INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[CREATE_TIMESTAMP])
SELECT
            IEM.JOB_FK
          ,'MATRIX RULES'
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'CONTRACT IS MISSING'
            ,IEM.MATRIX_ID
           ,CNTRCT_ID            
           ,GETDATE ()
FROM import.IMPORT_ECLIPSE_MATRIX IEM WITH (NOLOCK)
WHERE 1 = 1
AND   JOB_FK = @IN_JOB_FK
AND   customer_level = 'CONTRACT' 
AND   CNTRCT_ID NOT IN ( 
         SELECT CONTRACT_NO
         FROM synchronizer.RULES_CONTRACT RC WITH (NOLOCK) )


------ MAKE SURE CONTRACTS HAVE CNTRCT_ID 

           INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[CREATE_TIMESTAMP])
SELECT
            IEM.JOB_FK
          ,'MATRIX RULES'
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'CONTRACT IS NULL'
            ,IEM.MATRIX_ID
           ,CNTRCT_ID            
           ,GETDATE ()
FROM import.IMPORT_ECLIPSE_MATRIX IEM WITH (NOLOCK)
WHERE 1 = 1
AND   JOB_FK = @IN_JOB_FK 
AND   customer_level = 'CONTRACT'
and   CNTRCT_ID is null



--------------------------------------------------------------------------------

SET @V_COUNT = ( SELECT COUNT(*) FROM common.Job_errors
                                 WHERE JOB_FK = @IN_JOB_FK )

SET @status_desc =  'import.LOAD_ECLIPSE_MATRIX: '
					+ ' ERRORS IN IMPORT_ECLIPSE_MATRIX TABLE:: ' 
					+ CAST ( @V_COUNT AS VARCHAR(16) )                        
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


-------------------------------------------------------------------------------------------------------------




-------------------------------------------------------------------------------------
--   PRICES AND REBATES ARE ALL COMBINED TOGETHER IN THE CURRENT FORMAT
--   ONE ROW COULD HAVE BOTH A REBATE AND PRICE RULE ON IT
--   DO REBATES FIRST  ( CF NOT NULL )  THEN PRICING ( PF NOT NULL )
-------------------------------------------------------------------------------------



/************************************************  REBATE RULES  **************************************************/

-------------------------------------------------------------------------------------
--   INSERT REBATE RULES
-------------------------------------------------------------------------------------


INSERT INTO [import].[IMPORT_RULES]
           ([JOB_FK]
           ,[RECORD_NO]
           ,[IMPORT_FILENAME]
           ,[IMPORT_PACKAGE_FK]
           ,[RULES_STRUCTURE_NAME]
           ,[RULES_SCHEDULE_NAME]
           ,[RULE_NAME]
           ,[RESULT_DATA_NAME]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[PROD_FILTER_DN]
           ,[PROD_FILTER_VALUE1]
           ,[PROD_FILTER_OPERATOR]
           ,[ORG_FILTER_DN]
           ,[ORG_FILTER_ENTITY_DN]
           ,[ORG_FILTER_VALUE1]
           ,[ORG_FILTER_OPERATOR]
           ,[ORG_FILTER_ASSOC_DN]
           ,[CUST_FILTER_DN]
           ,[CUST_FILTER_ENTITY_DN]
           ,[CUST_FILTER_VALUE1]
           ,[CUST_FILTER_OPERATOR]
           ,[RULE_PRECEDENCE]
           ,[HOST_PRECEDENCE_LEVEL_DESCRIPTION]
           ,[HOST_RULE_XREF]
           ,[REFERENCE]
           ,[CONTRACT_NO]
           ,[RULES_CONTRACT_IND]
           ,[RULE_STATUS]
           ,[RULE_TYPE]
           ,UPDATE_USER
         )
SELECT
            IEM.JOB_FK
           ,( Row_Number()  over (Order by IEM.Matrix_ID ) + 1000000000 ) Record_No
           ,IMPORT_FILENAME 
           ,NULL AS IMPORT_PACKAGE
           ,CASE ISNULL ( BCC, 'No' )
            WHEN 'No' THEN 'ECL REBATES'
            ELSE 'ECL REBATES BEST'
            END AS RULES_STRUCTURE
           ,CASE Customer_Level
            WHEN 'CUSTOMER CLASS' THEN 'CLASS'
            WHEN 'CUSTOMER TYPE'  THEN 'TYPE'
            WHEN 'CONTRACT' THEN 'CONTRACT'
            WHEN 'CUSTOMER BILL TO'  THEN 'CUSTOMER'
            WHEN 'CUSTOMER SHIP TO'  THEN 'CUSTOMER'            
            ELSE 'DEFAULT'
            END AS RULES_SCHEDULE
           ,'REBATE::' + IEM.Matrix_ID  as Matrix_ID
           ,'REBATED COST CB AMT' as RESULT_DATA_NAME         ----- 111401	REBATED COST CB AMT
           ,EFFECTIVE_DATE
           ,END_DATE
           ,CASE Product_Level
            WHEN 'ALL' THEN NULL
            WHEN 'Product ID' THEN 'ECL PRODUCT FILE ID'
            ELSE Product_Level
            END  AS PROD_FILTER_DN
           ,Product_Level_Value AS PROD_FILTER_VALUE
           ,'=' AS PROD_FILTER_OPERATOR
-----------------           
           ,CASE Whse_Level
            WHEN 'ALL' THEN NULL
            ELSE 'WAREHOUSE CODE'
            END  AS ORG_FILTER_DN
           ,CASE Whse_Level
            WHEN 'DEFAULT' THEN NULL
            WHEN 'ALL' THEN NULL
            WHEN 'WHSE' THEN 'WAREHOUSE'
            WHEN 'TERR' THEN 'ECL TERRITORY'   ---- NEED TO CHECK DATA
            ELSE 'UNKNOWN'
            END  AS ORG_FILTER_ENTITY_DN
           ,Whse_Level_Value
           ,'=' AS ORG_FILTER_OPERATOR
           ,CASE WHEN ( RTRIM ( LTRIM ( ISNULL ( Whse_Level, ' ' ) ) ) <> '' )
                 THEN 'PRODUCT WHSE'
                 END AS ORG_FILTER_ASSOC_DN           
           ,CASE Customer_Level
            WHEN 'CUSTOMER BILL TO' THEN 'CUSTOMER CODE'
            WHEN 'CUSTOMER SHIP TO' THEN 'CUSTOMER CODE'  
            WHEN 'CONTRACT'         THEN 'CONTRACT'
            WHEN 'CLASS'            THEN 'CUSTOMER CLASS'
            WHEN 'TYPE'             THEN 'CUSTOMER TYPE'
            WHEN 'DEFAULT'          THEN NULL                       
            ELSE Customer_Level                               --- NOT SURE IF THERE ARE OTHERS
            END  AS CUST_FILTER_DN
           ,CASE Customer_Level
            WHEN 'CUSTOMER BILL TO' THEN 'CUSTOMER BILL TO'   --- NOT SURE SINCE NCE DOES NOT USE ??
            WHEN 'CUSTOMER SHIP TO' THEN 'CUSTOMER SHIP TO'   --- NOT SURE SINCE NCE DOES NOT USE ??
            ELSE NULL
            END  AS CUST_FILTER_ENTITY_DN
           ,CASE Customer_Level
            WHEN 'CONTRACT' THEN CNTRCT_ID
            ELSE Customer_Level_Value
            END  AS CUST_FILTER_VALUE1
           ,'=' AS CUST_FILTER_OPERATOR
            ,NULL AS PRECEDENCE  --- WILL LOOKUP IN IMPORT_RULES PROC
		   ,Matrix_Level AS HOST_PRECEDENCE_LEVEL_DESCRIPTION
           ,IEM.Matrix_ID  AS HOST_RULE_XREF
           ,CONTRACT_NAME AS REFERENCE
           ,CNTRCT_ID AS CONTRACT_NO
           ,CASE Customer_Level
            WHEN 'CONTRACT' THEN 1
            ELSE 0
            END  AS RULES_CONTRACT_IND
           ,'ACTIVE'  AS RULE_STATUS
           ,NULL AS RULE_TYPE
           ,'POC'
FROM IMPORT.IMPORT_ECLIPSE_MATRIX IEM WITH (NOLOCK) 
LEFT JOIN common.JOB_ERRORS JE WITH (NOLOCK) 
  ON ( JE.JOB_FK = IEM.JOB_FK
  AND  JE.ERROR_DATA1 = IEM.Matrix_ID )
    
WHERE 1 = 1
AND   IEM.JOB_FK = @IN_JOB_FK
AND   ( RTRIM ( LTRIM ( ISNULL ( IEM.CF_OPERATION1, ' ' ) ) ) <> '' )    ---- HAS A REBATE 
-------
AND   JE.JOB_ERRORS_ID IS NULL   --- NO IMPORT ERROR EXISTS


--------------------------------------------------------------------------------

SET @V_COUNT = ( SELECT COUNT(*) FROM IMPORT.IMPORT_RULES WHERE JOB_FK = @IN_JOB_FK AND RESULT_DATA_NAME =  'REBATED COST CB AMT' )

SET @status_desc =  'import.LOAD_ECLIPSE_MATRIX: '
					+ ' REBATE IMPORT_RULES: ' 
					+ CAST ( @V_COUNT AS VARCHAR(16) )                        
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

------------------------------------------------------------------------------------------------

INSERT INTO [import].[IMPORT_RULES_DATA]
           ([JOB_FK]
           ,[RECORD_NO]
           ,[RULE_NAME]
           ,[RULES_ACTION_OPERATION1]
           ,[RULES_ACTION_OPERATION2]
           ,[RULES_ACTION_OPERATION3]
           ,[BASIS_CALC_DN]
           ,[BASIS1_NUMBER]
           ,[BASIS2_NUMBER]
           ,[BASIS3_NUMBER]
           ,[BASIS_CALC_PRICESHEET_DATE]
           ,[ROUNDING_METHOD]
           ,[ROUNDING_TO_VALUE]
           ,[ROUNDING_ADDER_VALUE]
           ,[RULES_OPTIMIZATION_FK]           
           )
SELECT
            IR.JOB_FK
           ,IR.RECORD_NO
           ,IR.RULE_NAME
           ,CASE IEM.CF_OPERATION1
            WHEN '-' THEN 'DISCOUNT (N)'
            WHEN '+' THEN 'PERCENTAGE (N)'
            WHEN 'GP' THEN 'GROSS MARGIN (N)'
            WHEN 'D' THEN 'DIVIDE (N)'
            WHEN '*' THEN 'MULTIPLIER (N)'
            WHEN '$' THEN 'FIXED VALUE (N)'
            WHEN '-$' THEN 'SUBTRACT (N)'
            WHEN '+$' THEN 'ADD (N)'            
            END AS RULES_ACTION_OPERATION1
           ,CASE IEM.CF_OPERATION2
            WHEN '-' THEN 'DISCOUNT (N)'
            WHEN '+' THEN 'PERCENTAGE (N)'
            WHEN 'GP' THEN 'GROSS MARGIN (N)'
            WHEN 'D' THEN 'DIVIDE (N)'
            WHEN '*' THEN 'MULTIPLIER (N)'
            WHEN '$' THEN 'FIXED VALUE (N)'
            WHEN '-$' THEN 'SUBTRACT (N)'
            WHEN '+$' THEN 'ADD (N)'            
            END AS RULES_ACTION_OPERATION2
           ,CASE IEM.CF_OPERATION3
            WHEN '-' THEN 'DISCOUNT (N)'
            WHEN '+' THEN 'PERCENTAGE (N)'
            WHEN 'GP' THEN 'GROSS MARGIN (N)'
            WHEN 'D' THEN 'DIVIDE (N)'
            WHEN '*' THEN 'MULTIPLIER (N)'
            WHEN '$' THEN 'FIXED VALUE (N)'
            WHEN '-$' THEN 'SUBTRACT (N)'
            WHEN '+$' THEN 'ADD (N)'            
            END AS RULES_ACTION_OPERATION3
           ,CASE WHEN IEM.CF_OPERATION1 IN ('$', '-$', '+$' )
                 THEN NULL
                 ELSE ( CASE ( ISNULL ( IEM.C_BASIS, 'NULL DATA' ) )
						WHEN 'NULL DATA' THEN NULL
						WHEN 'ORD COGS'  THEN 'ORDER COGS'   --- 111202	ORDER COGS
						ELSE ( SELECT DN.NAME 
								FROM epacube.DATA_NAME DN WITH (NOLOCK) 
								WHERE DN.PARENT_DATA_NAME_FK IN ( 381600, 381700 ) --- HISTORICAL PRICESHEET
								AND   DN.LABEL = IEM.C_BASIS )
						END  )
             END AS BASIS_CALC_DN                    
           ,CASE WHEN ( RTRIM( LTRIM( ISNULL ( IEM.CF_OPERAND1, '' ) ) ) = '' )
            THEN NULL
            ELSE CASE WHEN IEM.CF_OPERATION1 IN ( '-','+', 'GP' ) THEN ( CAST ( RTRIM( LTRIM( IEM.CF_OPERAND1) ) AS NUMERIC (18,6 ) ) / 100 )
					  ELSE  IEM.CF_OPERAND1
					  END
            END AS BASIS1_NUMBER
           ,CASE WHEN ( RTRIM( LTRIM( ISNULL ( IEM.CF_OPERAND2, '' ) ) ) = '' )
            THEN NULL
            ELSE CASE WHEN IEM.CF_OPERATION2 IN ( '-','+', 'GP' ) THEN ( CAST ( RTRIM( LTRIM( IEM.CF_OPERAND2) ) AS NUMERIC (18,6 ) ) / 100 )
					  ELSE  IEM.CF_OPERAND2
					  END
            END AS BASIS2_NUMBER
           ,CASE WHEN ( RTRIM( LTRIM( ISNULL ( IEM.CF_OPERAND3, '' ) ) ) = '' )
            THEN NULL
            ELSE CASE WHEN IEM.CF_OPERATION3 IN ( '-','+', 'GP' ) THEN ( CAST ( RTRIM( LTRIM( IEM.CF_OPERAND3) ) AS NUMERIC (18,6 ) ) / 100 )
					  ELSE  IEM.CF_OPERAND3
					  END
            END AS BASIS3_NUMBER
           ,IEM.COST_DATE AS BASIS_PRICESHEET_DATE
           ,NULL AS ROUNDING_METHOD
           ,NULL AS ROUNDING_TO_VALUE
           ,NULL AS ROUNDING_ADDER_VALUE
           ,(SELECT RO.RULES_OPTIMIZATION_ID FROM synchronizer.RULES_OPTIMIZATION RO WHERE RO.NAME = IEM.Optimize_Matrix )
FROM IMPORT.IMPORT_ECLIPSE_MATRIX IEM WITH (NOLOCK)   
INNER JOIN IMPORT.IMPORT_RULES IR WITH (NOLOCK)
  ON ( IR.JOB_FK = IEM.JOB_FK
  AND  IR.RULE_NAME = ( 'REBATE::' + IEM.Matrix_ID )
  AND  IR.RESULT_DATA_NAME = 'REBATED COST CB AMT' )
WHERE 1 = 1
AND   IEM.JOB_FK = @IN_JOB_FK
AND   ( RTRIM ( LTRIM ( ISNULL (CF_OPERATION1, ' ' ) ) ) <> '' )



--------------------------------------------------------------------------------

SET @V_COUNT = ( SELECT COUNT(*) 
                 FROM IMPORT.IMPORT_RULES IR
                 INNER JOIN IMPORT_RULES_DATA IRD ON ( IRD.JOB_FK = IR.JOB_FK
                                                  AND  IRD.RULE_NAME = IR.RULE_NAME )
                 WHERE IR.JOB_FK = @IN_JOB_FK AND IR.RESULT_DATA_NAME =  'REBATE CB AMT' )

SET @status_desc =  'import.LOAD_ECLIPSE_MATRIX: '
					+ ' COMPLETED REBATE IMPORT_RULES_DATA: ' 
					+ CAST ( @V_COUNT AS VARCHAR(16) )                        
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;




/************************************************  PRICING RULES  **************************************************/


-------------------------------------------------------------------------------------
--   INSERT PRICING RULES
-------------------------------------------------------------------------------------



INSERT INTO [import].[IMPORT_RULES]
           ([JOB_FK]
           ,[RECORD_NO]
           ,[IMPORT_FILENAME]
           ,[IMPORT_PACKAGE_FK]
           ,[RULES_STRUCTURE_NAME]
           ,[RULES_SCHEDULE_NAME]
           ,[RULE_NAME]
           ,[RESULT_DATA_NAME]
           ,[EFFECTIVE_DATE]
           ,[END_DATE]
           ,[PROD_FILTER_DN]
           ,[PROD_FILTER_VALUE1]
           ,[PROD_FILTER_OPERATOR]
           ,[ORG_FILTER_DN]
           ,[ORG_FILTER_ENTITY_DN]
           ,[ORG_FILTER_VALUE1]
           ,[ORG_FILTER_OPERATOR]
           ,[ORG_FILTER_ASSOC_DN]
           ,[CUST_FILTER_DN]
           ,[CUST_FILTER_ENTITY_DN]
           ,[CUST_FILTER_VALUE1]
           ,[CUST_FILTER_OPERATOR]
           ,[RULE_PRECEDENCE]
           ,[HOST_PRECEDENCE_LEVEL_DESCRIPTION]
           ,[HOST_RULE_XREF]
           ,[REFERENCE]
           ,[CONTRACT_NO]
           ,[RULES_CONTRACT_IND]
           ,[RULE_STATUS]
           ,[RULE_TYPE]
           ,UPDATE_USER
         )
SELECT
            IEM.JOB_FK
           ,( Row_Number()  over (Order by IEM.Matrix_ID ) + 2000000000 ) Record_No
           ,IMPORT_FILENAME 
           ,NULL AS IMPORT_PACKAGE
           ,CASE ISNULL ( BPC, '0' )
            WHEN '0' THEN 'ECL PRICING BEST'
            ELSE 'ECL PRICING'
            END AS RULES_STRUCTURE
           ,CASE Customer_Level
            WHEN 'CUSTOMER CLASS' THEN 'CLASS'
            WHEN 'CUSTOMER TYPE'  THEN 'TYPE'
            WHEN 'CONTRACT' THEN 'CONTRACT'
            WHEN 'CUSTOMER BILL TO'  THEN 'CUSTOMER'
            WHEN 'CUSTOMER SHIP TO'  THEN 'CUSTOMER'            
            ELSE 'DEFAULT'
            END AS RULES_SCHEDULE
           ,'PRICE::' + IEM.Matrix_ID 
           ,'SELL PRICE CUST AMT'
           ,EFFECTIVE_DATE
           ,END_DATE
           ,CASE Product_Level
            WHEN 'ALL' THEN NULL
            WHEN 'Product ID' THEN 'ECL PRODUCT FILE ID'            
            ELSE Product_Level
            END  AS PROD_FILTER_DN
           ,Product_Level_Value AS PROD_FILTER_VALUE
           ,'=' AS PROD_FILTER_OPERATOR
-----------------           
           ,CASE Whse_Level
            WHEN 'ALL' THEN NULL
            ELSE 'WAREHOUSE CODE'
            END  AS ORG_FILTER_DN
           ,CASE Whse_Level
            WHEN 'DEFAULT' THEN NULL
            WHEN 'ALL' THEN NULL
            WHEN 'WHSE' THEN 'WAREHOUSE'
            WHEN 'TERR' THEN 'ECL TERRITORY'   ---- NEED TO CHECK DATA
            ELSE 'UNKNOWN'
            END  AS ORG_FILTER_ENTITY_DN
           ,Whse_Level_Value
           ,'=' AS ORG_FILTER_OPERATOR
           ,CASE WHEN ( RTRIM ( LTRIM ( ISNULL ( Whse_Level, ' ' ) ) ) <> '' )
                 THEN 'PRODUCT WHSE'
                 END AS ORG_FILTER_ASSOC_DN           
           ,CASE Customer_Level
            WHEN 'CUSTOMER BILL TO' THEN 'CUSTOMER CODE'
            WHEN 'CUSTOMER SHIP TO' THEN 'CUSTOMER CODE'  
            WHEN 'CONTRACT'         THEN 'CONTRACT'
            WHEN 'CLASS'            THEN 'CUSTOMER CLASS'
            WHEN 'TYPE'             THEN 'CUSTOMER TYPE'
            WHEN 'DEFAULT'          THEN NULL                       
            ELSE Customer_Level                               --- NOT SURE IF THERE ARE OTHERS
            END  AS CUST_FILTER_DN
           ,CASE Customer_Level
            WHEN 'CUSTOMER BILL TO' THEN 'CUSTOMER BILL TO'   --- NOT SURE SINCE NCE DOES NOT USE ??
            WHEN 'CUSTOMER SHIP TO' THEN 'CUSTOMER SHIP TO'   --- NOT SURE SINCE NCE DOES NOT USE ??
            ELSE NULL
            END  AS CUST_FILTER_ENTITY_DN
           ,CASE Customer_Level
            WHEN 'CONTRACT' THEN CNTRCT_ID
            ELSE Customer_Level_Value
            END  AS CUST_FILTER_VALUE1
           ,'=' AS CUST_FILTER_OPERATOR
           ,NULL AS PRECEDENCE   --- WILL LOOKUP IN IMPORT_RULES PROC
		   ,Matrix_Level AS HOST_PRECEDENCE_LEVEL_DESCRIPTION
           ,IEM.Matrix_ID  AS HOST_RULE_XREF
           ,CONTRACT_NAME AS REFERENCE
           ,CNTRCT_ID AS CONTRACT_NO
           ,CASE Customer_Level
            WHEN 'CONTRACT' THEN 1
            ELSE 0
            END  AS RULES_CONTRACT_IND
           ,'ACTIVE'  AS RULE_STATUS
           ,NULL AS RULE_TYPE
           ,'POC'
FROM IMPORT.IMPORT_ECLIPSE_MATRIX IEM WITH (NOLOCK)   
LEFT JOIN common.JOB_ERRORS JE WITH (NOLOCK) 
  ON ( JE.JOB_FK = IEM.JOB_FK
  AND  JE.ERROR_DATA1 = IEM.Matrix_ID )   
WHERE 1 = 1
AND   IEM.JOB_FK = @IN_JOB_FK
AND   ( RTRIM ( LTRIM ( ISNULL ( IEM.PF_OPERATION1, ' ' ) ) ) <> '' )    ---- PRICING RULE 
-------
AND   JE.JOB_ERRORS_ID IS NULL   --- NO IMPORT ERROR EXISTS


--------------------------------------------------------------------------------

SET @V_COUNT = ( SELECT COUNT(*) 
                 FROM IMPORT.IMPORT_RULES IR                 
                 WHERE IR.JOB_FK = @IN_JOB_FK AND IR.RESULT_DATA_NAME =  'SELL PRICE CUST AMT' )

SET @status_desc =  'import.LOAD_ECLIPSE_MATRIX: '
					+ ' PRICE IMPORT_RULES: ' 
					+ CAST ( @V_COUNT AS VARCHAR(16) )                        
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


-------------------------------------------------------------------------------------------------------------


INSERT INTO [import].[IMPORT_RULES_DATA]
           ([JOB_FK]
           ,[RECORD_NO]
           ,[RULE_NAME]
           ,[RULES_ACTION_OPERATION1]
           ,[RULES_ACTION_OPERATION2]
           ,[RULES_ACTION_OPERATION3]
           ,[BASIS_CALC_DN]
           ,[BASIS1_NUMBER]
           ,[BASIS2_NUMBER]
           ,[BASIS3_NUMBER]
           ,[BASIS_CALC_PRICESHEET_DATE]
           ,[ROUNDING_METHOD]
           ,[ROUNDING_TO_VALUE]
           ,[ROUNDING_ADDER_VALUE]
           ,[RULES_OPTIMIZATION_FK]                      
           )
SELECT
            IR.JOB_FK
           ,IR.RECORD_NO
           ,IR.RULE_NAME
           ,CASE IEM.PF_OPERATION1
            WHEN '-' THEN 'DISCOUNT (N)'
            WHEN '+' THEN 'PERCENTAGE (N)'
            WHEN 'GP' THEN 'GROSS MARGIN (N)'
            WHEN 'D' THEN 'DIVIDE (N)'
            WHEN '*' THEN 'MULTIPLIER (N)'
            WHEN '$' THEN 'FIXED VALUE (N)'
            WHEN '-$' THEN 'SUBTRACT (N)'
            WHEN '+$' THEN 'ADD (N)'            
            END AS RULES_ACTION_OPERATION1
           ,CASE IEM.PF_OPERATION2
            WHEN '-' THEN 'DISCOUNT (N)'
            WHEN '+' THEN 'PERCENTAGE (N)'
            WHEN 'GP' THEN 'GROSS MARGIN (N)'
            WHEN 'D' THEN 'DIVIDE (N)'
            WHEN '*' THEN 'MULTIPLIER (N)'
            WHEN '$' THEN 'FIXED VALUE (N)'
            WHEN '-$' THEN 'SUBTRACT (N)'
            WHEN '+$' THEN 'ADD (N)'            
            END AS RULES_ACTION_OPERATION2
           ,CASE IEM.PF_OPERATION3
            WHEN '-' THEN 'DISCOUNT (N)'
            WHEN '+' THEN 'PERCENTAGE (N)'
            WHEN 'GP' THEN 'GROSS MARGIN (N)'
            WHEN 'D' THEN 'DIVIDE (N)'
            WHEN '*' THEN 'MULTIPLIER (N)'
            WHEN '$' THEN 'FIXED VALUE (N)'
            WHEN '-$' THEN 'SUBTRACT (N)'
            WHEN '+$' THEN 'ADD (N)'            
            END AS RULES_ACTION_OPERATION3
           ,CASE WHEN IEM.PF_OPERATION1 IN ('$', '-$', '+$' )
                 THEN NULL
                 ELSE ( CASE ( ISNULL ( IEM.P_BASIS, 'NULL DATA' ) )
						WHEN 'NULL DATA' THEN NULL
						WHEN 'ORD COGS'  THEN 'ORDER COGS'   --- 111202   ORDER COGS
						ELSE ( SELECT DN.NAME 
								FROM epacube.DATA_NAME DN WITH (NOLOCK) 
								WHERE DN.PARENT_DATA_NAME_FK IN ( 381600, 381700 ) --- HISTORICAL PRICESHEET
								AND   DN.LABEL = IEM.P_BASIS )
						END  )
             END AS BASIS_CALC_DN                    
           ,CASE WHEN ( RTRIM( LTRIM( ISNULL ( IEM.PF_OPERAND1, '' ) ) ) = '' )
            THEN NULL
            ELSE CASE WHEN IEM.PF_OPERATION1 IN ( '-','+', 'GP' ) THEN ( CAST ( RTRIM( LTRIM( IEM.PF_OPERAND1) ) AS NUMERIC (18,6 ) ) / 100 )
					  ELSE  IEM.PF_OPERAND1
					  END
            END AS BASIS1_NUMBER
           ,CASE WHEN ( RTRIM( LTRIM( ISNULL ( IEM.PF_OPERAND2, '' ) ) ) = '' )
            THEN NULL
            ELSE CASE WHEN IEM.PF_OPERATION2 IN ( '-','+', 'GP' ) THEN ( CAST ( RTRIM( LTRIM( IEM.PF_OPERAND2) ) AS NUMERIC (18,6 ) ) / 100 )
					  ELSE  IEM.PF_OPERAND2
					  END
            END AS BASIS2_NUMBER
           ,CASE WHEN ( RTRIM( LTRIM( ISNULL ( IEM.PF_OPERAND3, '' ) ) ) = '' )
            THEN NULL
            ELSE CASE WHEN IEM.PF_OPERATION3 IN ( '-','+', 'GP' ) THEN ( CAST ( RTRIM( LTRIM( IEM.PF_OPERAND3) ) AS NUMERIC (18,6 ) ) / 100 )
					  ELSE  IEM.PF_OPERAND3
					  END
            END AS BASIS3_NUMBER
           ,IEM.PRICE_DATE AS BASIS_PRICESHEET_DATE
           ,CASE ISNULL ( IEM.P_RND_RULE, '0' )        
            WHEN '1' THEN 724   -- 'UPTOVALUE'
            ELSE NULL 
            END  AS ROUNDING_METHOD
           ,CASE ISNULL ( IEM.P_RND_RULE, '0' )
            WHEN '1' THEN IEM.P_RND_TO_VALUE
            ELSE NULL 
            END  AS ROUNDING_TO_VALUE
           ,CASE ISNULL ( IEM.P_RND_RULE, '0' )
            WHEN '1' THEN  IEM.P_RND_ADDER  
            ELSE NULL 
            END  AS ROUNDING_ADDER_VALUE
           ,(SELECT RO.RULES_OPTIMIZATION_ID FROM synchronizer.RULES_OPTIMIZATION RO WHERE RO.NAME = IEM.Optimize_Matrix )            
FROM IMPORT.IMPORT_ECLIPSE_MATRIX IEM WITH (NOLOCK)     
INNER JOIN IMPORT.IMPORT_RULES IR WITH (NOLOCK)
  ON ( IR.JOB_FK = IEM.JOB_FK
  AND  IR.RULE_NAME = ( 'PRICE::' + IEM.Matrix_ID )
  AND  IR.RESULT_DATA_NAME = 'SELL PRICE CUST AMT' )
WHERE 1 = 1
AND   IEM.JOB_FK = @IN_JOB_FK
AND   ( RTRIM ( LTRIM ( ISNULL (PF_OPERATION1, ' ' ) ) ) <> '' )
AND   ISNULL ( IR.ERROR_ON_IMPORT_IND, 0 ) <> 9   


--------------------------------------------------------------------------------

SET @V_COUNT = ( SELECT COUNT(*) 
                 FROM IMPORT.IMPORT_RULES IR
                 INNER JOIN IMPORT_RULES_DATA IRD ON ( IRD.JOB_FK = IR.JOB_FK
                                                  AND  IRD.RULE_NAME = IR.RULE_NAME )
                 WHERE IR.JOB_FK = @IN_JOB_FK AND IR.RESULT_DATA_NAME =  'SELL PRICE CUST AMT' )

SET @status_desc =  'import.LOAD_ECLIPSE_MATRIX: '
					+ ' COMPLETED PRICE IMPORT_RULES_DATA: ' 
					+ CAST ( @V_COUNT AS VARCHAR(16) )                        
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;





-------------------------------------------------------------------------------------
--   EXECUTE IMPORT RULES
-------------------------------------------------------------------------------------

--Set up currently inactive rules to be re-activated where host erp rules have been re-activated

Update Rls
Set Record_status_cr_fk = 1
, end_date = Null
from Synchronizer.rules rls
inner join import.import_rules ir on rls.host_rule_xref = ir.host_rule_xref and rls.RESULT_DATA_NAME_FK = (select data_name_id from epacube.DATA_NAME where name = ir.Result_data_Name)
where 1 = 1
and rls.record_status_cr_fk = 2
and isnull(ir.end_date, getdate() + 1) > getdate()
and rls.RESULT_DATA_NAME_FK in (111401, 111602)

--Inactivate Rules not in current import file

If (select count(*) from import.import_rules where isnull(end_date, getdate() + 1) > getdate()) > (Select Cast(isnull((Select value from epacube.epacube_params where name = 'MIN RECORDS EXPECTED MATRIX'), 10000000) as bigint))

Update Rls
Set Record_status_Cr_FK = 2
from Synchronizer.RULES Rls
where Rls.RULES_ID in (
select Rules_ID from synchronizer.RULES R
left join import.import_rules IR on R.HOST_RULE_XREF = ir.host_rule_xref and r.RESULT_DATA_NAME_FK = (select data_name_id from epacube.DATA_NAME where name = ir.Result_data_Name)
where r.RECORD_STATUS_CR_FK = 1 and (ir.IMPORT_RULES_ID is null or r.END_DATE < GETDATE() or Ir.END_DATE < GETDATE())
and r.RESULT_DATA_NAME_FK in (111401, 111602)
) and rls.RESULT_DATA_NAME_FK in (111401, 111602)

EXEC import.LOAD_IMPORT_RULES @in_job_fk



-------------------------------------------------------------------------------------
--   LOGGING 
-------------------------------------------------------------------------------------


 SET @status_desc =  'finished execution of import.LOAD_ECLIPSE_MATRIX'

 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;

END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_ECLIPSE_MATRIX has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END






















