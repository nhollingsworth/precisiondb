﻿

--A_Update_Import_SXE_CustShip

CREATE PROCEDURE [import].[SXE_CUSTSHIP_TRANSFORMATION] ( @IN_JOB_FK bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to first create the Customer Bill to
Then need to run thru and create the customer ship to.
two differnt packages and two different job numbers...
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure 
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--     
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int 
	DECLARE @v_import_package_fk   int  
	DECLARE @v_column_name   varchar(25) 
	DECLARE @v_primary   varchar(25)   
	DECLARE @epa_job_id BIGINT
	DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25)
	 DECLARE @V_RULES_FILTER_LOOKUP  varchar(1)
	 DECLARE @V_SXE_MULTIPLE_CPT varchar(1)
	 DECLARE @V_SXE_MULTIPLE_CRT varchar(1)
     	
          

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.SXE_CUSTOMER_TRANSFORMATION', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


--INSERT INTO RECORD DATA FOR SX CUSTOMER SHIP TO RECORDS
--NEED TO GET NEW JOB NUMBER??

 EXEC [common].[JOB_CREATE] @IN_JOB_FK = NULL,
                       @IN_job_class_fk = 100,
                       @IN_JOB_NAME = 'SX CUSTOMERS (SHIP TO)',
                       @IN_DATA1 = NULL,
                       @IN_DATA2 = NULL,
                       @IN_DATA3 = NULL,
                       @IN_JOB_USER = NULL,
                       @OUT_JOB_FK = @epa_job_id OUTPUT,
                       @CLIENT_APP = 'IMPORT' ;

--THEN UPDATE IMPORT_RECORD DATA WITH IT
update import.import_sxe_customers_Ship_To
set job_fk = @epa_job_id
--where ship_to is not null
--and job_fk = @in_job_fk

--------------------------------------------------------------------------------------
--  CREATE SHIP TO RECORD FROM RAW DATA
--------------------------------------------------------------------------------------

--set @V_RULES_FILTER_LOOKUP = (
--select isnull(value,'0') 
--from epacube.EPACUBE_PARAMS 
--where name = 'RULES_FILTER_LOOKUP_OPERAND')

		INSERT INTO [import].[IMPORT_RECORD_DATA]
           ([IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[SEQ_ID]
           ,[JOB_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[DATA_POSITION1]
           ,[DATA_POSITION2]
           ,[DATA_POSITION3]
           ,[DATA_POSITION4]
           ,[DATA_POSITION5]
           ,[DATA_POSITION6]
           ,[DATA_POSITION7]
           ,[DATA_POSITION8]
           ,[DATA_POSITION9]
           ,[DATA_POSITION10]
           ,[DATA_POSITION11]
           ,[DATA_POSITION12]
           ,[DATA_POSITION13]
           ,[DATA_POSITION14]
           ,[DATA_POSITION15]
           ,[DATA_POSITION16]
           ,[DATA_POSITION17]
           ,[DATA_POSITION18]
           ,[DATA_POSITION19]
           ,[DATA_POSITION20]
           ,[DATA_POSITION21]
           ,[DATA_POSITION22]
           ,[DATA_POSITION23]
           ,[DATA_POSITION24]
           ,[DATA_POSITION25]
           ,[DATA_POSITION26]
           ,[DATA_POSITION27]
           ,[DATA_POSITION28]
           ,[DATA_POSITION29]
           ,[DATA_POSITION30]
           ,[DATA_POSITION31]
           ,[DATA_POSITION32]
           ,[DATA_POSITION33]
           ,[DATA_POSITION34]
           ,[DATA_POSITION35]
           ,[DATA_POSITION36]
           ,[DATA_POSITION37]
           ,[DATA_POSITION38]
           ,[DATA_POSITION39]
           ,[DATA_POSITION40]
           ,[DATA_POSITION41]
           ,[DATA_POSITION42]
           ,[DATA_POSITION43]
           ,[DATA_POSITION44]
           ,[DATA_POSITION45]
           ,[DATA_POSITION46]
           ,[DATA_POSITION47]
           ,[DATA_POSITION48]
           ,[DATA_POSITION49]
           ,[DATA_POSITION50]
           ,[DATA_POSITION51]
           ,[DATA_POSITION52]
           ,[DATA_POSITION53]
           ,[DATA_POSITION54]
           ,[DATA_POSITION55]
           ,[DATA_POSITION56]
           ,[DATA_POSITION57]
		   ,[DATA_POSITION58]
		   ,[DATA_POSITION59]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
)
(select distinct
'SX CUSTOMERS (SHIP TO)'
,NULL
,NULL
,@epa_job_id
,242000 -- SX BILL TO CUSTOMERS
,CUST_NO + '-' + SHIP_TO
           ,[CITY]
           ,[CLASS]
           ,[COUNTRY]
           ,[CUST_TYPE]
           ,[CUST_PRICE_TYPE]
           ,DISC_CODE
           ,[JOB_DESC]
           ,[CUST_NAME]
           ,[PRICE_CUST_NO]
           ,PRICE_CODE
           ,[REBATE_TYPE]
           ,[ROUTE]
           ,[SALESREP_OUT]
           ,[SALESREP_IN]
           ,[TERRITORY]
           ,[SHIP_TO]
           ,[SIC_CODE1]
           ,[SIC_CODE2]
           ,[SIC_CODE3]
           ,[STATE]
           ,[USER1]
           ,[USER10]
           ,[USER11]
           ,[USER12]
           ,[USER13]
           ,[USER14]
           ,[USER15]
           ,[USER16]
           ,[USER17]
           ,[USER18]
           ,[USER19]
           ,[USER2]
           ,[USER20]
           ,[USER21]
           ,[USER22]
           ,[USER23]
           ,[USER24]
           ,[USER3]
           ,[USER4]
           ,[USER5]
           ,[USER6]
           ,[USER7]
           ,[USER8]
           ,[USER9]
           ,[WAREHOUSE_ID]
           ,[ZIP_CODE]
           ,[CUSTOM1]
           ,[CUSTOM2]
           ,[CUSTOM3]
           ,[CUSTOM4]
           ,[CUSTOM5]
           ,[CUSTOM6]
           ,[CUSTOM7]
           ,[CUSTOM8]
           ,[CUSTOM9]
           ,[CUSTOM10]
			,CUST_NO
			,DSO
           ,getdate()
           ,'IMPORT'
           FROM
import.import_sxe_customers_Ship_To
--WHERE SHIP_TO IS NOT NULL
--AND [Status] = 'A'
--and job_fk = @epa_job_id
)

IF object_id('import.import_sxe_customers_Ship_To') is not null
Drop Table import.import_sxe_customers_Ship_To

--LOOKUP PRIMARY CUSTOMER BY PACKAGE
SET @v_import_package_fk = (select distinct import_package_fk 
							from import.import_record_data WITH (NOLOCK)
							where  job_fk = @epa_job_id)

SET @v_column_name = (select column_name 
						from import.import_map_metadata WITH (NOLOCK)
						where import_package_fk = @v_import_package_fk
						and data_name_fk = (select data_name_id
from epacube.data_name where name = 'CUSTOMER SHIP TO'))  

SET @v_primary = (select source_field_name
						from import.import_map_metadata WITH (NOLOCK)
						where import_package_fk = @v_import_package_fk
						and data_name_fk =(select data_name_id
from epacube.data_name where name = 'CUSTOMER SHIP TO'))  



/* ----------------------------------------------------------------------------------- */
--   --Create Temp table to insert distinct customer data 
/* ----------------------------------------------------------------------------------- */


--drop temp table if it exists
	IF object_id('tempdb..#TS_DISTINCT_CUSTOMERS') is not null
	   drop table #TS_DISTINCT_CUSTOMERS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_DISTINCT_CUSTOMERS(
	IMPORT_RECORD_DATA_FK	BIGINT,
	[IMPORT_FILENAME] [varchar](128),
	[RECORD_NO] [int],
	[JOB_FK] [bigint],
	[IMPORT_PACKAGE_FK] [int],
	[EFFECTIVE_DATE] [datetime],
	[END_DATE] [datetime],
	[ENTITY_CLASS] [varchar](64) ,
	[ENTITY_DATA_NAME] [varchar](64),
	[ENTITY_ID_DATA_NAME] [varchar](64),
	[ENTITY_ID_VALUE] [varchar](64),
	[ENTITY_STATUS] [varchar](32),
	[SYSTEM_ID_VALUE] [varchar](64),
	[PRIORITY] [varchar](32),
	[SEARCH_ACTIVITY_FK] [int],
	[UPDATE_TIMESTAMP] [datetime],
	[UPDATE_USER] [varchar](64)
		)

Create Index idx_tscust on #TS_DISTINCT_CUSTOMERS([JOB_FK], [ENTITY_ID_VALUE])

-------------------------------------------------------------------------------------
--   LOAD TEMP TABLE WITH DISTINCT PRODUCT DATA
--   NEED to check if NULL Then do something else to get product identification 
-------------------------------------------------------------------------------------


SET @ls_exec =
						'INSERT INTO #TS_DISTINCT_CUSTOMERS
						(   [JOB_FK]
						   ,[IMPORT_PACKAGE_FK]
						   ,[END_DATE]
						   ,[ENTITY_CLASS]
						   ,[ENTITY_DATA_NAME]
						   ,[ENTITY_ID_DATA_NAME]
						   ,[ENTITY_ID_VALUE]
						   )
						SELECT distinct
							IRD.JOB_FK
							,IRD.IMPORT_PACKAGE_FK
						    ,IRD.END_DATE '
						   + ', ' 
						   +  ' ''CUSTOMER'' '
						   + ', '  
						   + ' ''CUSTOMER SHIP TO'' '
						    + ', ''' 
						   + @v_primary
						    + ''', '
						   + @v_column_name
						   + ' ' 
						+ ' FROM IMPORT.IMPORT_RECORD_DATA  IRD WITH (NOLOCK) '
						+ ' WHERE 1=1 '
						+ ' AND isnull(DO_NOT_IMPORT_IND,0) <> 1'
						+ ' AND '
						+ @v_column_name 
						+ ' IS NOT NULL'
						+ ' AND ISNULL(IRD.JOB_FK,-999) = '
						+ cast(isnull (@epa_job_id, -999) as varchar(20))



----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), @ls_exec )

         exec sp_executesql 	@ls_exec;

-----

UPDATE #TS_DISTINCT_CUSTOMERS
				SET RECORD_NO = x.ROW 
				,import_filename = (SELECT Top 1 import.import_record_data.import_filename
											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
											WHERE 1=1 
											AND Job_Fk = @epa_job_id)
				,effective_date = ISNULL((SELECT Top 1 import.import_record_data.effective_date
											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
											WHERE 1=1 
											AND Job_Fk = @epa_job_id), getdate())
				FROM (SELECT ROW_NUMBER() 
						OVER (ORDER BY ENTITY_ID_VALUE) AS Row, 
						ENTITY_ID_VALUE
				FROM #TS_DISTINCT_CUSTOMERS where job_fk = @epa_job_id)x
				where #TS_DISTINCT_CUSTOMERS.entity_id_value = x.entity_id_value
AND #TS_DISTINCT_CUSTOMERS.job_Fk = @epa_job_id



-----------------------------------------------------
	


-------------------------------------------------------------------------------------
--   LOAD IMPORT_RECORD TABLE(S) 
-------------------------------------------------------------------------------------

					INSERT INTO IMPORT.IMPORT_RECORD 
							   ([IMPORT_FILENAME]
							   ,[RECORD_NO]
							   ,[JOB_FK]
							   ,[IMPORT_PACKAGE_FK]
							   ,[EFFECTIVE_DATE]
							   ,[END_DATE]
							   ,[ENTITY_CLASS]
							   ,[ENTITY_DATA_NAME]
							   ,[ENTITY_ID_DATA_NAME]
							   ,[ENTITY_ID_VALUE]
							   ,[ENTITY_STATUS]
							   ,[UPDATE_TIMESTAMP]
							   ,[UPDATE_USER])

					SELECT 
							   TDP.IMPORT_FILENAME
							   ,TDP.RECORD_NO
							   ,TDP.JOB_FK
							   ,TDP.IMPORT_PACKAGE_FK
							   ,TDP.EFFECTIVE_DATE
							   ,TDP.END_DATE
							   ,TDP.ENTITY_CLASS
							   ,TDP.ENTITY_DATA_NAME
							   ,TDP.ENTITY_ID_DATA_NAME
							   ,TDP.ENTITY_ID_VALUE
							   ,'ACTIVE'
							   ,TDP.UPDATE_TIMESTAMP
							   ,TDP.UPDATE_USER
					FROM #TS_DISTINCT_CUSTOMERS TDP
					WHERE 1=1
					AND TDP.JOB_FK = @epa_job_id


-------------------------------------------------------------------------
 SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @epa_job_id;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @epa_job_id;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'SXE CUSTOMER TRANSFORMATION',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


-------------------------------------------------------------------------------------
--   UPDATE IMPORT RECORD WITH RECORD_NO and SEQ_ID
-------------------------------------------------------------------------------------

			SET @ls_exec =
						'UPDATE IMPORT.IMPORT_RECORD_DATA
						set RECORD_NO = import.import_record.record_no
						from import.import_record WITH (NOLOCK)
						where'
						+ ' '
						+ @v_column_name 
						+ ' = entity_id_value
						and import.import_record_data.JOB_FK = '
						+ cast(isnull (@epa_job_id, -999) as varchar(20))
						+ ' and import.import_record.JOB_FK = '
						+ cast(isnull (@epa_job_id, -999) as varchar(20))


----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), @ls_exec )

         exec sp_executesql 	@ls_exec;


-----------

				UPDATE IMPORT.IMPORT_RECORD_DATA
				SET SEQ_ID = x.ROW 
				FROM (SELECT ROW_NUMBER() 
				OVER (ORDER BY IMPORT_RECORD_DATA_ID) AS Row, 
					 IMPORT_RECORD_DATA_ID
			FROM IMPORT.IMPORT_RECORD_DATA WITH (NOLOCK)
			where job_fk = @epa_job_id)x
			WHERE import.import_record_data.job_fk = @epa_job_id
			AND import.import_record_data.import_record_data_id = x.import_record_data_id
			
			
-----------------------------------------------------------------------------
---- ADDED TO MARK ANY DUPLICATE ROWS AS DO NOT IMPORT
------------------------------------------------------------------------------


		SET @ls_exec =
						'UPDATE IMPORT.IMPORT_RECORD_DATA
						SET Do_Not_import_IND = 1 
						WHERE import_record_data_id IN (
								SELECT A.import_record_data_id
								FROM (         
								SELECT ird1.import_record_data_id 
					   ,DENSE_RANK() OVER ( PARTITION BY '
						   + ' ' 
                           + @V_UNIQUE1
						   + ' , ' 
                           + @V_UNIQUE2
					       + ' , '
                           + @V_UNIQUE3
						   + ''
						   + ' ORDER BY ird1.import_record_data_id DESC ) AS DRANK
							FROM IMPORT.IMPORT_RECORD_DATA IRD1
							WHERE ird1.import_record_data_id  in (
							select ird.import_record_data_id 
							from import.import_record_data ird
							where ird.job_fk = '
							+ cast(isnull (@in_job_fk, -999) as varchar(20))
							+ ')
										) A
										WHERE DRANK <> 1   )'		
					

----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), isnull ( @ls_exec, 'Trying to update do not import ind from stage.SXE_CUSTBILL_TRANSFORMATION' ) )

         exec sp_executesql 	@ls_exec;



--- EPA Constraint
UPDATE IMPORT.IMPORT_RECORD_DATA
				SET SEQ_ID = NULL
--,RECORD_NO = NULL
WHERE  job_fk = @in_job_fk
and DO_NOT_IMPORT_IND = 1

----------

------------------------------------------------------
---CALL STAGE TO EVENTS
------------------------------------------------------
     SET @V_START_TIMESTAMP = getdate()			


			 EXEC[stage].[STG_TO_EVENTS] @epa_JOB_ID
	

      SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @epa_job_id;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @epa_job_id;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @epa_job_id, 'STAGE TO EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

-------------------------------------------------------------------------------------
-- LAST Step is to Set Job Execution to Complete
-------------------------------------------------------------------------------------
----
----

      SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @epa_job_id
                              and   name = 'IMPORT ENTITIES TO STAGE' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @epa_job_id
                              and   name = 'IMPORT ENTITIES RESOLVED' )

     exec common.job_execution_create  @epa_job_id, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
		update common.job
		set job_complete_timestamp = getdate()
		where job_id = @epa_job_id
		
		update common.JOB_EXECUTION
		set NAME = 'IMPORT COMPLETE'
		where NAME = 'SXE CUSTOMER TRANSFORMATION'
		and JOB_FK = (select top 1 job_id from common.job 
		where NAME = 'SX CUSTOMERS (BILL TO)' order by JOB_ID desc)
		
 SET @status_desc =  'finished execution of stage.SXE_CUSTOMER_TRANSFORMATION'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of STAGE.SXE_CUSTOMER_TRANSFORMATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;

END

