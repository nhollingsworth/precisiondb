﻿












-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- KS        06/14/2010   Initial Version 


CREATE PROCEDURE [import].[LOAD_IMPORT_RULES_STRFUNCTION] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_IMPORT_RULES_STRFUNCTION', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps



-------------------------------------------------------------------------------------
--   INSERT RULES_ACTION_SUBSTRING  BASIS POSITION 1
-------------------------------------------------------------------------------------


INSERT INTO [synchronizer].[RULES_ACTION_STRFUNCTION]
           ([RULES_ACTION_FK]
           ,[BASIS_POSITION]
           ,[SUBSTR_START_POS]
           ,[SUBSTR_LENGTH]
           ,[SUBSTR_WHOLE_TOKEN_IND]
           ,[TRUNC_FIRST_TOKEN_IND]
           ,[REMOVE_SPEC_CHARS_IND]
           ,[SPECIAL_CHARS_SET]
           ,[REMOVE_SPACES_IND]
           ,[REPLACING_SPACE_CHAR]
           ,[REPLACE_CHAR]
           ,[REPLACING_CHAR]
           ,[REPLACE_STRING]
           ,[REPLACING_STRING]
           ,[UPDATE_USER]
           ,[UPDATE_LOG_FK])
SELECT
           RA.RULES_ACTION_ID
          ,1
		  ,IRD.SUBSTR_START_POS_1 
		  ,IRD.SUBSTR_LENGTH_1 
		  ,IRD.SUBSTR_WHOLE_TOKEN_IND_1 
		  ,IRD.TRUNC_FIRST_TOKEN_IND_1 
		  ,IRD.REMOVE_SPEC_CHARS_IND_1 
		  ,IRD.SPECIAL_CHARS_SET_1 
		  ,IRD.REMOVE_SPACES_IND_1 
		  ,IRD.REPLACING_SPACE_CHAR_1 
		  ,IRD.REPLACE_CHAR_1 
		  ,IRD.REPLACING_CHAR_1 
		  ,IRD.REPLACE_STRING_1 
		  ,IRD.REPLACING_STRING_1 
          ,isnull(IR.[UPDATE_USER], 'EPACUBE')
          ,IR.IMPORT_RULES_ID
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_FK = IR.RULES_FK )  
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 54  
AND   (
         IRD.SUBSTR_START_POS_1  IS NOT NULL
      OR IRD.SUBSTR_WHOLE_TOKEN_IND_1 IS NOT NULL 
      OR IRD.REMOVE_SPEC_CHARS_IND_1 IS NOT NULL 
      OR IRD.REMOVE_SPACES_IND_1  IS NOT NULL 
      OR IRD.REPLACE_CHAR_1 IS NOT NULL
      OR IRD.REPLACE_STRING_1 IS NOT NULL 
      )
      


-------------------------------------------------------------------------------------
--   INSERT RULES_ACTION_SUBSTRING  BASIS POSITION 2
-------------------------------------------------------------------------------------


INSERT INTO [synchronizer].[RULES_ACTION_STRFUNCTION]
           ([RULES_ACTION_FK]
           ,[BASIS_POSITION]
           ,[SUBSTR_START_POS]
           ,[SUBSTR_LENGTH]
           ,[SUBSTR_WHOLE_TOKEN_IND]
           ,[TRUNC_FIRST_TOKEN_IND]
           ,[REMOVE_SPEC_CHARS_IND]
           ,[SPECIAL_CHARS_SET]
           ,[REMOVE_SPACES_IND]
           ,[REPLACING_SPACE_CHAR]
           ,[REPLACE_CHAR]
           ,[REPLACING_CHAR]
           ,[REPLACE_STRING]
           ,[REPLACING_STRING]
           ,[UPDATE_USER]
           ,[UPDATE_LOG_FK])
SELECT
           RA.RULES_ACTION_ID
          ,2
		  ,IRD.SUBSTR_START_POS_2 
		  ,IRD.SUBSTR_LENGTH_2 
		  ,IRD.SUBSTR_WHOLE_TOKEN_IND_2 
		  ,IRD.TRUNC_FIRST_TOKEN_IND_2 
		  ,IRD.REMOVE_SPEC_CHARS_IND_2 
		  ,IRD.SPECIAL_CHARS_SET_2 
		  ,IRD.REMOVE_SPACES_IND_2 
		  ,IRD.REPLACING_SPACE_CHAR_2 
		  ,IRD.REPLACE_CHAR_2 
		  ,IRD.REPLACING_CHAR_2 
		  ,IRD.REPLACE_STRING_2 
		  ,IRD.REPLACING_STRING_2 
          ,isnull(IR.[UPDATE_USER], 'EPACUBE')
          ,IR.IMPORT_RULES_ID
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_FK = IR.RULES_FK )  
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 54  
AND   (
         IRD.SUBSTR_START_POS_2  IS NOT NULL
      OR IRD.SUBSTR_WHOLE_TOKEN_IND_2 IS NOT NULL 
      OR IRD.REMOVE_SPEC_CHARS_IND_2 IS NOT NULL 
      OR IRD.REMOVE_SPACES_IND_2  IS NOT NULL 
      OR IRD.REPLACE_CHAR_2 IS NOT NULL
      OR IRD.REPLACE_STRING_2 IS NOT NULL 
      )




-------------------------------------------------------------------------------------
--   INSERT RULES_ACTION_SUBSTRING  BASIS POSITION 3
-------------------------------------------------------------------------------------


INSERT INTO [synchronizer].[RULES_ACTION_STRFUNCTION]
           ([RULES_ACTION_FK]
           ,[BASIS_POSITION]
           ,[SUBSTR_START_POS]
           ,[SUBSTR_LENGTH]
           ,[SUBSTR_WHOLE_TOKEN_IND]
           ,[TRUNC_FIRST_TOKEN_IND]
           ,[REMOVE_SPEC_CHARS_IND]
           ,[SPECIAL_CHARS_SET]
           ,[REMOVE_SPACES_IND]
           ,[REPLACING_SPACE_CHAR]
           ,[REPLACE_CHAR]
           ,[REPLACING_CHAR]
           ,[REPLACE_STRING]
           ,[REPLACING_STRING]
           ,[UPDATE_USER]
           ,[UPDATE_LOG_FK])
SELECT
           RA.RULES_ACTION_ID
          ,3
		  ,IRD.SUBSTR_START_POS_3 
		  ,IRD.SUBSTR_LENGTH_3 
		  ,IRD.SUBSTR_WHOLE_TOKEN_IND_3 
		  ,IRD.TRUNC_FIRST_TOKEN_IND_3 
		  ,IRD.REMOVE_SPEC_CHARS_IND_3 
		  ,IRD.SPECIAL_CHARS_SET_3 
		  ,IRD.REMOVE_SPACES_IND_3 
		  ,IRD.REPLACING_SPACE_CHAR_3 
		  ,IRD.REPLACE_CHAR_3 
		  ,IRD.REPLACING_CHAR_3 
		  ,IRD.REPLACE_STRING_3 
		  ,IRD.REPLACING_STRING_3 
          ,isnull(IR.[UPDATE_USER], 'EPACUBE')
          ,IR.IMPORT_RULES_ID
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_FK = IR.RULES_FK )  
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 54  
AND   (
         IRD.SUBSTR_START_POS_3  IS NOT NULL
      OR IRD.SUBSTR_WHOLE_TOKEN_IND_3 IS NOT NULL 
      OR IRD.REMOVE_SPEC_CHARS_IND_3 IS NOT NULL 
      OR IRD.REMOVE_SPACES_IND_3  IS NOT NULL 
      OR IRD.REPLACE_CHAR_3 IS NOT NULL
      OR IRD.REPLACE_STRING_3 IS NOT NULL 
      )




-------------------------------------------------------------------------------------
--   UPDATE EXISTING RULES ACTIONS_STRFUNCTION POSITION 1
-------------------------------------------------------------------------------------      
      
UPDATE [synchronizer].[RULES_ACTION_STRFUNCTION]
   SET 
       [SUBSTR_START_POS]				= IRD.SUBSTR_START_POS_1
      ,[SUBSTR_LENGTH]					= IRD.SUBSTR_LENGTH_1
      ,[SUBSTR_WHOLE_TOKEN_IND]			= IRD.SUBSTR_WHOLE_TOKEN_IND_1
      ,[TRUNC_FIRST_TOKEN_IND]			= IRD.TRUNC_FIRST_TOKEN_IND_1
      ,[REMOVE_SPEC_CHARS_IND]			= IRD.REMOVE_SPEC_CHARS_IND_1
      ,[SPECIAL_CHARS_SET]				= IRD.SPECIAL_CHARS_SET_1
      ,[REMOVE_SPACES_IND]				= IRD.REMOVE_SPACES_IND_1
      ,[REPLACING_SPACE_CHAR]			= IRD.REPLACING_SPACE_CHAR_1
      ,[REPLACE_CHAR]					= IRD.REPLACE_CHAR_1
      ,[REPLACING_CHAR]					= IRD.REPLACING_CHAR_1
      ,[REPLACE_STRING]					= IRD.REPLACE_STRING_1
      ,[REPLACING_STRING]				= IRD.REPLACING_STRING_1
      ,[UPDATE_TIMESTAMP]				= GETDATE ()
      ,[UPDATE_USER]					= isnull(IR.[UPDATE_USER], 'EPACUBE') 
      ,[UPDATE_LOG_FK]					= IR.IMPORT_RULES_ID
----------------------
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_FK = IR.RULES_FK )  
INNER JOIN synchronizer.RULES_ACTION_STRFUNCTION RASF WITH (NOLOCK)
  ON ( RASF.RULES_ACTION_FK = RA.RULES_ACTION_ID )    
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 52 
AND   RASF.BASIS_POSITION = 1



-------------------------------------------------------------------------------------
--   UPDATE EXISTING RULES ACTIONS_STRFUNCTION POSITION 2 
-------------------------------------------------------------------------------------      
      
UPDATE [synchronizer].[RULES_ACTION_STRFUNCTION]
   SET 
       [SUBSTR_START_POS]				= IRD.SUBSTR_START_POS_2
      ,[SUBSTR_LENGTH]					= IRD.SUBSTR_LENGTH_2
      ,[SUBSTR_WHOLE_TOKEN_IND]			= IRD.SUBSTR_WHOLE_TOKEN_IND_2
      ,[TRUNC_FIRST_TOKEN_IND]			= IRD.TRUNC_FIRST_TOKEN_IND_2
      ,[REMOVE_SPEC_CHARS_IND]			= IRD.REMOVE_SPEC_CHARS_IND_2
      ,[SPECIAL_CHARS_SET]				= IRD.SPECIAL_CHARS_SET_2
      ,[REMOVE_SPACES_IND]				= IRD.REMOVE_SPACES_IND_2
      ,[REPLACING_SPACE_CHAR]			= IRD.REPLACING_SPACE_CHAR_2
      ,[REPLACE_CHAR]					= IRD.REPLACE_CHAR_2
      ,[REPLACING_CHAR]					= IRD.REPLACING_CHAR_2
      ,[REPLACE_STRING]					= IRD.REPLACE_STRING_2
      ,[REPLACING_STRING]				= IRD.REPLACING_STRING_2
      ,[UPDATE_TIMESTAMP]				= GETDATE ()
      ,[UPDATE_USER]					= isnull(IR.[UPDATE_USER], 'EPACUBE') 
      ,[UPDATE_LOG_FK]					= IR.IMPORT_RULES_ID
----------------------
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_FK = IR.RULES_FK )  
INNER JOIN synchronizer.RULES_ACTION_STRFUNCTION RASF WITH (NOLOCK)
  ON ( RASF.RULES_ACTION_FK = RA.RULES_ACTION_ID )    
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 52 
AND   RASF.BASIS_POSITION = 2




-------------------------------------------------------------------------------------
--   UPDATE EXISTING RULES ACTIONS_STRFUNCTION POSITION 3 
-------------------------------------------------------------------------------------      
      
UPDATE [synchronizer].[RULES_ACTION_STRFUNCTION]
   SET 
       [SUBSTR_START_POS]				= IRD.SUBSTR_START_POS_3
      ,[SUBSTR_LENGTH]					= IRD.SUBSTR_LENGTH_3
      ,[SUBSTR_WHOLE_TOKEN_IND]			= IRD.SUBSTR_WHOLE_TOKEN_IND_3
      ,[TRUNC_FIRST_TOKEN_IND]			= IRD.TRUNC_FIRST_TOKEN_IND_3
      ,[REMOVE_SPEC_CHARS_IND]			= IRD.REMOVE_SPEC_CHARS_IND_3
      ,[SPECIAL_CHARS_SET]				= IRD.SPECIAL_CHARS_SET_3
      ,[REMOVE_SPACES_IND]				= IRD.REMOVE_SPACES_IND_3
      ,[REPLACING_SPACE_CHAR]			= IRD.REPLACING_SPACE_CHAR_3
      ,[REPLACE_CHAR]					= IRD.REPLACE_CHAR_3
      ,[REPLACING_CHAR]					= IRD.REPLACING_CHAR_3
      ,[REPLACE_STRING]					= IRD.REPLACE_STRING_3
      ,[REPLACING_STRING]				= IRD.REPLACING_STRING_3
      ,[UPDATE_TIMESTAMP]				= GETDATE ()
      ,[UPDATE_USER]					= isnull(IR.[UPDATE_USER], 'EPACUBE') 
      ,[UPDATE_LOG_FK]					= IR.IMPORT_RULES_ID
----------------------
FROM IMPORT.IMPORT_RULES IR WITH (NOLOCK)
INNER JOIN IMPORT.IMPORT_RULES_DATA IRD WITH (NOLOCK) 
  ON ( IR.JOB_FK = IRD.JOB_FK
  AND  IR.RECORD_NO = IRD.RECORD_NO )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_FK = IR.RULES_FK )  
INNER JOIN synchronizer.RULES_ACTION_STRFUNCTION RASF WITH (NOLOCK)
  ON ( RASF.RULES_ACTION_FK = RA.RULES_ACTION_ID )    
WHERE IR.JOB_FK = @IN_JOB_FK
AND   IR.EVENT_ACTION_CR_FK = 52 
AND   RASF.BASIS_POSITION = 3







 SET @status_desc =  'finished execution of import.LOAD_IMPORT_RULES_STRFUNCTION'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_IMPORT_RULES_STRFUNCTION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END





















































