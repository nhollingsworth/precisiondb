﻿--A_epaMAUI_Import_To_Production_PDSR

--Modified 05/12/2014 by GHS to delete expired rules where the rebate record number is being re-used by SX.e
--Modified 10/24/2014 Per Jennifer Mize no longer exclude W Contracts from PDSR File
--Modified 08/17/2015 to deal better with Inactive Rules
--Modified 08/21/2015 to deal with Carrier Rebate Sharing Bad Data by limiting case statement to Ascii codes 49 thru 57
--Modified 6/8/2016 Updated Unique_Key1 to be able to match round-trip requirements
--Modified 10/25/2016 to better manage incoming PDSR rules that have no start date and to inactivate rules that are no longer active. Code did not account for changes made to end date at host site.

CREATE PROCEDURE [import].[Import_To_Production_PDSR]
AS
BEGIN
      SET NOCOUNT ON;
--------------------------------------------------
--Load Import Tables
--------------------------------------------------
If (select count(*) from import.import_sxe_pdsr) = 0
goto EndOfProc

Truncate Table Import.Import_Rules
Truncate Table Import.Import_Rules_Data

Update R
Set Unique_Key1 = Case When UNIQUE_KEY1 like '%_Expired%' then UNIQUE_KEY1 else UNIQUE_KEY1 + '_Expired' end
from Synchronizer.rules R
where R.RESULT_DATA_NAME_FK in (111501, 111502) and R.RECORD_STATUS_CR_FK = 2

Declare @In_JOB_FK bigint
Declare @zCurves Int
Declare @Rbt_Share Int
Declare @CustName varchar(64)
Declare @HistNamePrefix varchar(64)
Set @In_JOB_FK = (Select top 1 Job_FK from import.import_sxe_pdsr with (nolock))
Set @zCurves = (Select Value from epacube.EPACUBE_PARAMS with (nolock) where NAME = 'zCURVE REBATE CHECK')
Set @Rbt_Share = (Select Value from epacube.EPACUBE_PARAMS with (nolock) where NAME = 'REBATE SHARING CHECK')
Set @CustName = (Select value from epacube.EPACUBE_PARAMS with (nolock) where NAME = 'epaCUBE Customer')
Set @HistNamePrefix = Case When (Select COUNT(*) from marginmgr.PRICESHEET with (nolock) where PRICESHEET_NAME like 'HISTORICAL - %') > 0 then 'HISTORICAL - ' else '' end

IF object_id('tempdb..##tmp_PDSR') is not null
drop table ##tmp_PDSR;

IF object_id('tempdb..##tmp_PDSR_1') is not null
drop table ##tmp_PDSR_1;

IF object_id('tempdb..##PDSR') is not null
drop table ##PDSR;

IF object_id('tempdb..##excl') is not null
drop table ##excl

IF object_id('tempdb..##Corrupt') is not null
drop table ##Corrupt

IF object_id('tempdb..#dels') is not null
drop table #dels

Select Rules_ID into ##Corrupt from synchronizer.rules r with (nolock)
inner join import.import_sxe_pdsr pdsr on r.host_rule_xref = pdsr.rebrecno and r.RESULT_DATA_NAME_FK in (111501, 111502)
where r.result_data_name_fk in (111501, 111502) and r.RECORD_STATUS_CR_FK = 2 and isnull(pdsr.enddt, getdate() + 1) > getdate()

Create index idx_crpt on ##corrupt(Rules_ID)

Insert into ##corrupt(Rules_ID)
	Select Rules_ID from synchronizer.rules r with (nolock)
	inner join import.import_sxe_pdsr pdsr on r.host_rule_xref = pdsr.rebrecno and r.RESULT_DATA_NAME_FK in (111501, 111502)
	where r.result_data_name_fk  in (111501, 111502) 
	and r.RECORD_STATUS_CR_FK = 2 
	and isnull(pdsr.enddt, getdate() + 1) > getdate()
	Union
	Select rules_id from synchronizer.rules where unique_key1 in (
	Select unique_key1 from synchronizer.rules 
	where result_data_name_fk in (111501, 111502)
	group by unique_key1 having count(*) > 1)

delete from dbo.MAUI_Rules_Maintenance where rules_fk in (select rules_id from ##Corrupt)

Delete from Synchronizer.rules_filter_set where rules_fk in (select rules_id from ##Corrupt)

Select rules_filter_fk into #dels from
	(
	select prod_filter_fk rules_filter_fk from synchronizer.RULES_FILTER_SET with (nolock) where prod_filter_fk is not null group by prod_filter_fk Union
	select Org_filter_fk from synchronizer.RULES_FILTER_SET with (nolock) where Org_filter_fk is not null group by Org_filter_fk Union
	select cust_filter_fk from synchronizer.RULES_FILTER_SET with (nolock) where cust_filter_fk is not null group by cust_filter_fk Union
	select supl_filter_fk from synchronizer.RULES_FILTER_SET with (nolock) where supl_filter_fk is not null group by supl_filter_fk
	) A
	
Create index idx_dels on #dels(rules_filter_fk)

Delete rf from synchronizer.RULES_FILTER rf
left join #dels dels on rf.RULES_FILTER_ID = dels.rules_filter_fk
where dels.rules_filter_fk is null

Delete from Synchronizer.rules_action_operands where RULES_ACTION_FK in (select rules_action_id from synchronizer.RULES_ACTION where RULES_FK in (select rules_id from ##Corrupt))

Delete from synchronizer.RULES_ACTION_STRFUNCTION where RULES_ACTION_FK in (select rules_action_id from synchronizer.RULES_ACTION where RULES_FK in (select rules_id from ##Corrupt))
Delete from Synchronizer.rules_action where rules_fk in (select rules_id from ##Corrupt)
Delete from synchronizer.rules where RULES_ID in (select rules_id from ##Corrupt)


--IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'idx_sxe_pdsr')
IF EXISTS (SELECT name FROM sysindexes WHERE name = 'idx_sxe_pdsr1')
      drop index idx_sxe_pdsr1 on import.import_sxe_pdsr

Create Index idx_sxe_pdsr1 on import.import_sxe_pdsr(contractno, dropshipty, enddt)

select RebRecNo 
into ##excl 
from import.IMPORT_SXE_PDSR with (nolock)
where ((pricetype is null and product is null and prodcat is null and prodline is null and rebatety is null and rebsubty is null)
      or (rebcalcty = 'N' and rebdowntoty = 'f' and ISNULL(Cast(rebateamt as float), 0) = 0) 
      or (ISDATE(enddt) = 0 and enddt is not null) 
      or (ISDATE(enddt) = 1 and enddt is not null and Cast(enddt as datetime) < Cast(startdt as datetime)))
      AND IMPORT_FILENAME = (Select MAX(IMPORT_FILENAME) from import.import_sxe_pdsr)

Create index rebrecno on ##excl(RebRecNo)
      
Select 
Case When @Rbt_Share = 1 and (Contractno like 'X%' or Contractno like 'J%') and @CustName = 'Carrier' then 1 
--Case When @Rbt_Share = 1 and (Contractno like 'X%' or Contractno like 'J%' or Contractno like 'W%') and @CustName = 'Carrier' then 1 
      --When @Rbt_Share = 1 and (Contractno like 'X%' or Contractno like 'J%') and (@CustName like '%Carrier%' or @CustName like '%Refrigerated%')then 1 
      else 0 end 'Restrict'
, pdsr.* into ##tmp_PDSR_1 
from import.import_sxe_pdsr pdsr with (nolock)
left join ##excl excl on pdsr.rebrecno = excl.rebrecno
where (isdate(pdsr.enddt) = 1 or pdsr.enddt is null) and excl.rebrecno is null and IMPORT_FILENAME = (Select MAX(IMPORT_FILENAME) from import.import_sxe_pdsr with (nolock))

Create index idx_pdsra on ##tmp_PDSR_1(enddt, dropshipty)

IF object_id('tempdb..##excl') is not null
drop table ##excl

select * into ##tmp_PDSR from ##tmp_PDSR_1  
where isnull(enddt, getdate() + 1) > getdate() 
and dropshipty = 'w' 
and isnumeric(levelcd) = 1

IF object_id('tempdb..##tmp_PDSR_1') is not null
Drop Table ##tmp_PDSR_1 

If @CustName = 'Carrier'
Insert into ##tmp_PDSR
([RESTRICT], IMPORT_FILENAME, JOB_FK, Typecd, Hierarchy, Contractno, custno, custrebty, startdt, enddt, dropshipty, levelcd, margincostty, pricesheet, pricetype, product, prodcat, prodline, rebateamt, rebatecd, rebatecostty, rebatepct, rebatety, rebcalcty, rebdowntoty, rebrecno, rebsubty, refer, shipto, user1, user2, user3, user4, user5, user6, user7, user8, user9, vendno , whse, custom1, custom2, custom3, custom4, custom5, custom6, custom7, custom8, custom9, custom10, pricesheetto, priceeffectivedate)--, priceeffectivedateto )
Select 0, IMPORT_FILENAME, JOB_FK, Typecd, Hierarchy, contractno + '~' + custno 'Contractno', custrebty, startdt, enddt, dropshipty, levelcd, margincostty, pricesheet, pricetype, product, prodcat, prodline, rebateamt, rebatecd, rebatecostty, rebatepct, rebatety, rebcalcty, rebdowntoty, rebrecno, rebsubty, refer, shipto, user1, user2, user3, user4, user5, user6, user7, user8, user9, vendno , whse, custom1, custom2, custom3, custom4, custom5, custom6, custom7, custom8, custom9, custom10, pricesheetto, priceeffectivedate, priceeffectivedateto 
from import.IMPORT_SXE_PDSR with (nolock) where dropshipty not in ('w', 'd') and levelcd = 'w'
      
Create Index idx_sxe_pdsr1 on ##tmp_PDSR([restrict], levelcd, typecd, custno, shipto, custrebty, Whse, dropshipty, rebcalcty, rebdowntoty, rebatecostty, margincostty)

Delete from ##tmp_PDSR where [Restrict] = 1

Select
levelcd
, rebrecno HOST_RULE_XREF
, TypeCD
, (Select rules_structure_fk from [synchronizer].[RULES_STRUCTURE_ASSIGNMENT] with (nolock) where record_status_cr_fk = 1 and rule_type = 'Rebate' and host_precedence_level = pdsr.levelcd 
      and Promo_Ind = 0 and RULES_DATA_SOURCE = 'PDSR' and result_data_name_fk = Case when TypeCD like '%-Sale' then 111501 else 111502 end
      and Cust_Data_Name_FK = Case When isnull(pdsr.custno, '') <> '' or isnull(pdsr.shipto, '') <> '' then 144111 When isnull(pdsr.custrebty, '') <> '' then 244901 else 144099 end
      And rules_hierarchy_fk = 
      (Select Rules_hierarchy_id from synchronizer.rules_hierarchy with (nolock) where 
            result_data_name_fk = Case when TypeCD like '%-Sale' then 111501 else 111502 end
            and RULES_PRECEDENCE_SCHEME_FK in
            (Select rules_precedence_scheme_id from synchronizer.rules_precedence_scheme with (nolock) where name =
            (Select value from epacube.epacube_params with (nolock) where name = Case when TypeCD like '%-Sale' then 'REBATE CB PRECEDENCE' else 'REBATE BUY PRECEDENCE' end)))
      ) rules_structure_fk
, (Select rules_schedule_fk from [synchronizer].[RULES_STRUCTURE_ASSIGNMENT] with (nolock) where record_status_cr_fk = 1 and rule_type = 'Rebate' and host_precedence_level = pdsr.levelcd 
      and Promo_Ind = 0 and RULES_DATA_SOURCE = 'PDSR' and result_data_name_fk = Case when TypeCD like '%-Sale' then 111501 else 111502 end
      and Cust_Data_Name_FK = Case When isnull(pdsr.custno, '') <> '' or isnull(pdsr.shipto, '') <> '' then 144111 When isnull(pdsr.custrebty, '') <> '' then 244901 else 144099 end
      And rules_hierarchy_fk = 
      (Select Rules_hierarchy_id from synchronizer.rules_hierarchy with (nolock) where 
            result_data_name_fk = Case when TypeCD like '%-Sale' then 111501 else 111502 end
            and RULES_PRECEDENCE_SCHEME_FK in
            (Select rules_precedence_scheme_id from synchronizer.rules_precedence_scheme with (nolock) where name =
            (Select value from epacube.epacube_params with (nolock) where name = Case when TypeCD like '%-Sale' then 'REBATE CB PRECEDENCE' else 'REBATE BUY PRECEDENCE' end)))
      ) rules_schedule_fk
, (Select rules_precedence_scheme_id from synchronizer.rules_precedence_scheme with (nolock) where name =
            (Select value from epacube.epacube_params with (nolock) where name = Case when TypeCD like '%-Sale' then 'REBATE CB PRECEDENCE' else 'REBATE BUY PRECEDENCE' end)
      ) Rules_Precedence_Scheme_FK
, Case rebcalcty
      When '$' then 'FIXED VALUE (N)'
      When '%' then 'MULTIPLIER (N)'
      When 'n' then
            Case rebdowntoty
                  When 'f' then 'REBATE DOWN TO FIXED'
                  else 'REBATE DOWN TO CALCULATED'
                  end
      When 'm' then 'GROSS MARGIN GUARANTEE'
      End as 'Rules_Action_Operation1'

, Case when isnull(pricesheet, '') > '' then ltrim(@HistNamePrefix + ltrim(rtrim(pricesheet))) end Basis_Calc_Pricesheet_Name
, Case when isnull(priceeffectivedate, '') > '' and isnull(pricesheet, '') > '' then priceeffectivedate end Basis_Calc_Pricesheet_Date
, Case When rebcalcty = 'n' and rebdowntoty <> 'f' and pricesheetto IS not null then ltrim(@HistNamePrefix + ltrim(rtrim(pricesheetto))) end Basis1_Pricesheet_Name 
, Case When rebcalcty = 'n' and rebdowntoty <> 'f' and pricesheetto IS not null and isnull(priceeffectivedateto, '') <> '' then priceeffectivedateto end Basis1_Pricesheet_Date 

--, Case When @Rbt_Share = 0 then Null When @Rbt_Share = 1 and ISNULL(Left(user3, 10), '') > '' And isnumeric(ltrim(rtrim(Replace(Left(User3, 10), ',', '')))) = 1 and Cast(ltrim(rtrim(Replace(Left(User3, 10), ',', ''))) as Numeric(18, 2)) <> 0 then '50 PERCENT REBATE SHARING' END 'Rules_Action_Operation2'
, Case When @Rbt_Share = 0 then Null When @Rbt_Share = 1 and Ascii(Left(user3, 10)) between 49 and 57 and isnumeric(Left(user3, 10)) = 1 then '50 PERCENT REBATE SHARING' END 'Rules_Action_Operation2'

, Case when isnull(pricesheet, '') > '' then
            'SX HIST ' + 
                  Case When rebatecostty in ('CB', 'I', 'E') then 'REBATE COST'
                        When rebatecostty in ('R', 'RC', 'MR', 'RM') then 'REPL COST'
                        When rebatecostty = 'B' then 'BASE'
                        When rebatecostty = 'L' then 'LIST'
                        When rebatecostty in ('S', 'CS', 'MS') then 'STD COST'
                        else rebatecostty end
      When rebcalcty = '%' or (rebcalcty = 'n' and rebdowntoty = 'f') then
            Case rebatecostty
                  When 'b' then 'SX BASE'
                  When 'l' then 'SX LIST'
                  When 'p' then 'SELL PRICE CUST AMT'
            --    When 'a' then 'SX AVERAGE COST' {DATA NAME NOT YET DEFINED) WE NEED TO GET FROM SX?
                  When 't' then 'SX LAST COST'
                  When 'r' then 'SX REPL COST'
                  When 's' then 'SX STD'
                  When 'e' then 'SX REBATE'
                  When 'o' then 'SX REPLACEMENT COST FOREIGN'
            --    When 'c' then 'SX ACTUAL COST' (DATA NAME NOT YET DEFINED) WE NEED TO GET FROM SX?
            End
      When rebcalcty = 'm' then
            Case margincostty
                  When 'b' then 'SX BASE'
                  When 'l' then 'SX LIST'
                  When 'p' then 'SELL PRICE CUST AMT'
            --    When 'a' then 'SX AVERAGE COST'
                  When 't' then 'SX LAST COST'
                  When 'r' then 'SX REPL COST'
                  When 's' then 'SX STD'
                  When 'e' then 'SX REBATE'
                  When 'o' then 'SX REPLACEMENT COST FOREIGN' 
            --    When 'c' then 'SX ACTUAL COST'
            End
      When rebcalcty = 'n' and rebdowntoty <> 'f' then
            Case rebatecostty
                  When 'b' then 'SX BASE'
                  When 'l' then 'SX LIST'
                  When 'p' then 'SELL PRICE CUST AMT' 
            --    When 'a' then 'SX AVERAGE COST'
                  When 't' then 'SX LAST COST'
                  When 'r' then 'SX REPL COST'
                  When 's' then 'SX STD'
                  When 'e' then 'SX REBATE'
                  When 'o' then 'SX REPLACEMENT COST FOREIGN' 
            --    When 'c' then 'SX ACTUAL COST'
            End
      end as Basis_Calc_DN
, Case When rebcalcty = '$' or (rebcalcty = 'n' and rebdowntoty = 'f') then rebateamt
      else rebatepct
      end Basis1_Number
, Case when isnull(pricesheetto, '') > ''  and rebdowntoty <> 'F' then
            'SX HIST ' + 
                  Case When rebdowntoty in ('CB', 'I', 'E') then 'REBATE COST'
                        When rebdowntoty in ('R', 'RC', 'MR', 'RM') then 'REPL COST'
                        When rebdowntoty = 'B' then 'BASE'
                        When rebdowntoty = 'L' then 'LIST'
                        When rebdowntoty in ('S', 'CS', 'MS') then 'STD COST'
                        else rebdowntoty end
      When rebcalcty = 'n' and rebdowntoty <> 'f' then
            Case rebdowntoty
                  When 'b' then 'SX BASE'
                  When 'l' then 'SX LIST'
                  When 'p' then 'SELL PRICE CUST AMT'
            --    When 'a' then 'SX AVERAGE COST' {DATA NAME NOT YET DEFINED) WE NEED TO GET FROM SX?
                  When 't' then 'SX LAST COST'
                  When 'r' then 'SX REPL COST'
                  When 's' then 'SX STD'
                  When 'e' then 'SX REBATE'
                  When 'o' then 'SX REPLACEMENT COST FOREIGN'
            --    When 'c' then 'SX ACTUAL COST' (DATA NAME NOT YET DEFINED) WE NEED TO GET FROM SX?
            End
      When rebcalcty = 'm' then 'SELL PRICE CUST AMT'
      else null
      end as Basis1_DN
--, Case When @Rbt_Share = 0 then Null When @Rbt_Share = 1 and ISNULL(Left(user3, 10), '') > '' And isnumeric(ltrim(rtrim(Replace(Left(User3, 10), ',', '')))) = 1 and Cast(ltrim(rtrim(Replace(Left(User3, 10), ',', ''))) as Numeric(18, 2)) <> 0 AND substring(user3, 11, 1) <> 'Y' then 'SX LIST' end Basis2_DN
--, Cast(Case When @Rbt_Share = 0 then Null When @Rbt_Share = 1 And ISNULL(Left(user3, 10), '') <> '' And isnumeric(ltrim(rtrim(Replace(Left(User3, 10), ',', '')))) = 1 and Cast(ltrim(rtrim(Replace(Left(User3, 10), ',', ''))) as Numeric(18, 2)) <> 0 then Ltrim(Rtrim(Replace(Left(User3, 10), ',', ''))) end as money) Basis2_Number
--, Case When @Rbt_Share = 0 then Null When @Rbt_Share = 1 and ISNULL(Left(user3, 10), '') > '' And isnumeric(ltrim(rtrim(Replace(Left(User3, 10), ',', '')))) = 1 and Cast(ltrim(rtrim(Replace(Left(User3, 10), ',', ''))) as Numeric(18, 2)) <> 0 then 'SELL PRICE CUST AMT' end Basis3_DN

, Case When @Rbt_Share = 0 then Null When @Rbt_Share = 1 and Ascii(Left(user3, 10)) between 49 and 57 and isnumeric(Left(user3, 10)) = 1 AND substring(user3, 11, 1) <> 'Y' then 'SX LIST' end Basis2_DN
, Cast(Case When @Rbt_Share = 0 then Null When @Rbt_Share = 1 and Ascii(Left(user3, 10)) between 49 and 57 and isnumeric(Left(user3, 10)) = 1 then Ltrim(Rtrim(Replace(Left(User3, 10), ',', ''))) end as money) Basis2_Number
, Case When @Rbt_Share = 0 then Null When @Rbt_Share = 1 and Ascii(Left(user3, 10)) between 49 and 57 and isnumeric(Left(user3, 10)) = 1 then 'SELL PRICE CUST AMT' end Basis3_DN

, 'Rebate-' + [Hierarchy] 'Hierarchy'
, JOB_FK
, Import_Filename
, Row_Number() over (Order by rebrecno) Record_No
, Upper(TYPECD + ': ' + REBRECNO) RULE_NAME
, Case When Typecd like '%-Buy' then 'REBATE BUY AMT'
      ELSE 'REBATE CB AMT' END  RESULT_DATA_NAME
, startdt EFFECTIVE_DATE
, enddt END_DATE
,     Case 
            when isnull(product, '') <> '' then 'PRODUCT ID'
            when isnull(pricetype, '') <> '' then 'SX PRICE TYPE'

            When isnull(rebatety,'') <> '' and isnull(rebsubty,'') <> '' and len(rebsubty) < 9 Then 'SX CONC REBATE TYPE & SUBTYPE'
            
            when isnull(rebsubty,'') <> '' and len(rebsubty) < 9 then 'SX REBATE SUBTYPE'
            when isnull(rebatety,'') <> '' then 'SX REBATE TYPE'
            when isnull(prodline,'') <> '' then 'SX PRODUCT LINE'
            when isnull(prodcat,'') <> '' then 'SX PRODUCT CATEGORY'
      end Prod_Filter_DN
,     Case 
            when isnull(product, '') <> '' then product
            when isnull(pricetype, '') <> '' then pricetype
            When isnull(rebatety,'') <> '' and isnull(rebsubty,'') <> '' and len(rebsubty) < 9 then Cast(ltrim(rtrim(rebatety)) as varchar(8)) + ' ' + Cast(ltrim(rtrim(rebsubty)) as varchar(8))
            when isnull(rebsubty,'') <> '' and len(rebsubty) < 9 then rebsubty
            when isnull(rebatety,'') <> '' then rebatety
            when isnull(prodline,'') <> '' then prodline
            when isnull(prodcat,'') <> '' then prodcat
      end Prod_Filter_Value1

, Case When isnull(Whse, '') <> '' then 'WAREHOUSE CODE' end Org_Filter_DN
, Case When isnull(Whse, '') <> '' then (Select Name from epacube.data_name with (nolock) where data_name_id = (Select Entity_data_name_fk from epacube.ENTITY_IDENTIFICATION with (nolock) where DATA_NAME_FK = 141111 and VALUE = [whse])) end  Org_Filter_Entity_DN
, Case When isnull(whse,'') <> '' then whse end Org_Filter_Value1
, Case when isnull(whse, '') <> '' then (Select Name from epacube.data_name with (nolock) where data_name_id = 159100) end Org_Filter_Assoc_DN
, Case      when isnull(ShipTo,'') <> '' or isnull(Custno,'') <> '' then 'CUSTOMER CODE'
            when isnull(custrebty, '') <> '' then 'SX CUST REBATE TYPE'
      end 'Cust_Filter_DN'
,     Case
            when isnull(ShipTo,'') <> '' then 'CUSTOMER SHIP TO'  
            when isnull(Custno,'') <> '' then 'CUSTOMER BILL TO' 
      end as 'Cust_Filter_Entity_DN'
,     Case
            when isnull(ShipTo,'') <> '' then [custno] + '-' + [ShipTo]
            when isnull(Custno,'') <> '' then custno
            when isnull(custrebty,'') <> '' then custrebty
      end as 'Cust_Filter_Value1'
, Case When isnull(Vendno,'') <> '' then 'VENDOR NUMBER' end SUPL_FILTER_DN
, CASE
            When isnull(Vendno,'') <> '' then 'VENDOR' --253501
            End as SUPL_FILTER_ENTITY_DN
, CASE When isnull(VendNo,'') <> '' and VendNo <> '0' then VendNo End as SUPL_FILTER_VALUE1
, CASE When isnull(VendNo,'') <> '' and VendNo <> '0' then 'SX ARP VENDOR' ELSE NULL END Supl_Filter_Assoc_DN
, refer REFERENCE
, contractno CONTRACT_NO
, 999 DISPLAY_SEQ
, (Select Code from epacube.code_ref with (nolock) where code_ref_id = Case When Cast(startdt as datetime) <= Getdate() then 710 else 711 end) Rule_Result_Type
, Null Event_Action_CR_FK
, 1 RECORD_STATUS_CR_FK       
, 'HOST REBATE RULES LOAD' UPDATE_USER
into ##PDSR
from ##tmp_PDSR pdsr

Create Index idx_tmp_pdsr on ##PDSR(HOST_RULE_XREF, Record_No, Result_Data_Name, Prod_Filter_DN, Org_Filter_DN, Org_Filter_Entity_DN
, Cust_Filter_DN, Cust_Filter_Entity_DN, Supl_Filter_Entity_DN, SUPL_FILTER_ASSOC_DN)

Drop Table ##tmp_PDSR

Insert into import.import_rules
(Job_FK, Import_FileName, Record_No, Rules_Structure_Name, Rules_Schedule_Name, RULE_NAME, Result_Data_Name--, Result_Data_Name_FK
, Effective_Date, End_Date, Prod_Filter_DN, Prod_Filter_Value1, Org_Filter_DN, Org_Filter_Entity_DN
, Org_Filter_Value1, Org_Filter_Assoc_DN, Cust_Filter_DN, Cust_Filter_Entity_DN, Cust_Filter_Value1
, Supl_Filter_DN, Supl_Filter_Entity_DN, Supl_Filter_Value1, Supl_Filter_Assoc_DN, Rule_Precedence, HOST_PRECEDENCE_LEVEL_DESCRIPTION, Host_Rule_Xref, Reference
, Contract_No, Display_Seq, Rule_Result_Type, Unique_Key1, Event_Action_CR_FK, Record_Status_CR_FK, Update_User, Create_User, system_id_dn, system_id_value)

Select 
Job_FK, Import_FileName, Record_No
, (Select [Name] from synchronizer.rules_structure with (nolock) where rules_structure_id = PDSR.Rules_Structure_FK) Rules_Structure_Name
, (Select [Name] from synchronizer.rules_schedule with (nolock) where rules_schedule_id = PDSR.Rules_Schedule_FK) Rules_Schedule_Name
, RULE_NAME, Result_Data_Name
, Effective_Date, End_Date, Prod_Filter_DN, Prod_Filter_Value1, Org_Filter_DN, Org_Filter_Entity_DN
, Org_Filter_Value1, Org_Filter_Assoc_DN, Cust_Filter_DN, Cust_Filter_Entity_DN
, Cust_Filter_Value1, Supl_Filter_DN, Supl_Filter_Entity_DN, Supl_Filter_Value1, Supl_Filter_Assoc_DN

, (Select rule_precedence from synchronizer.RULES_Precedence Rprec with (nolock)
      where Rprec.RULES_Precedence_Scheme_FK = PDSR.Rules_Precedence_Scheme_FK
      and isnull(Rprec.PROD_Filter_DN_FK, 149099) = Isnull((Select Data_Name_ID from epacube.data_name with (nolock) where Name = PDSR.Prod_Filter_DN), 149099)
      
      and (Isnull(Rprec.Cust_Filter_DN_FK, 144111) = Isnull((Select Data_Name_ID from epacube.data_name with (nolock) where Name = PDSR.Cust_Filter_DN), 144111)
            and Isnull(Rprec.Cust_Filter_Entity_DN_FK, 144099) = Isnull((Select Data_Name_ID from epacube.data_name with (nolock) where Name = PDSR.Cust_Filter_ENTITY_DN), 144099))
      and (Isnull(Rprec.Org_Filter_DN_FK, 141111) = ISNULL((Select Data_Name_ID from epacube.data_name with (nolock) where Name = PDSR.Org_Filter_DN), 141111)
            and Isnull(Rprec.Org_Filter_Entity_DN_FK, 141099) = Isnull((Select Data_Name_ID from epacube.data_name with (nolock) where Name = PDSR.Org_Filter_ENTITY_DN), 141099))
      and (Isnull(Rprec.SUPL_FILTER_DN_FK, 143110) = ISNULL((Select Data_Name_ID from epacube.data_name with (nolock) where Name = PDSR.Supl_Filter_DN), 143110)
            and Isnull(Rprec.SUPL_FILTER_ENTITY_DN_FK, 143099) = ISNULL((Select Data_Name_ID from epacube.data_name with (nolock) where Name = PDSR.Supl_Filter_Entity_DN), 143099))
      and Rprec.Record_Status_CR_FK = 1
      ) 'RULE_PRECEDENCE'

, levelcd + ': ' + TypeCD 'Host_Precedence_Level_Description'
, Host_Rule_Xref, Reference
, Contract_No, Display_Seq, Rule_Result_Type
, Replace(Replace(Replace(Replace(Replace(
      Isnull(Cast((Select Data_name_id from epacube.DATA_NAME with (nolock) where [NAME] = PDSR.Result_Data_Name) as varchar(64)), '0')
      + isnull(Cast(levelcd as Varchar(5)), '0')
      + Isnull(Cast((Select Rules_Action_Operation_ID from synchronizer.RULES_ACTION_OPERATION with (nolock) where [NAME] = PDSR.Rules_Action_Operation1) as varchar(64)), '0')
      + '00'
      + isnull(Cast((Select Data_name_id from epacube.DATA_NAME with (nolock) where [NAME] = Isnull(PDSR.Cust_Filter_Entity_DN, PDSR.Cust_Filter_DN)) as varchar(64)), '0')
      + isnull(Cast(Cust_Filter_Value1 as Varchar(64)), '0')
      + Isnull(Cast((Select Data_name_id from epacube.DATA_NAME with (nolock) where [NAME] = PDSR.Prod_Filter_DN) as varchar(64)), '0')
      + isnull(Cast(Prod_FILTER_VALUE1 as Varchar(64)), '0')
	+ Case When Isnull(SUPL_FILTER_VALUE1, '') > '' then '143000' else '0' end --SuplCriteria_DN_FK
	+ isnull(Cast(SUPL_FILTER_VALUE1 as Varchar(64)), '0')
	+ '0' --OrgCriteria_DN_FK
	+ IsNull(Cast(Org_Filter_Value1 as Varchar), '0') 
	+ '0' --IsNull(Cast(UOM_Code_FK as Varchar), '0') 
	  + IsNull(Convert(Varchar(8),Cast(Effective_Date as datetime), 1), '0')
      , '''', ''), ' ', ''), 'Null', ''), ',', ''), '"', '') 
  'Unique_Key1'
 , Event_Action_CR_FK, Record_Status_CR_FK, Update_User, Update_User
, 142111 'System_ID_DN'
, 'PDSR' 'System_ID_Value'
from ##PDSR PDSR
Where EFFECTIVE_DATE is not null
order by HOST_RULE_XREF

----------
--Added delete on 11/18/11 to check for and get rid of duplicate prod_filter_fk values
----------
IF object_id('dbo.MAUI_A_Errors_PDSR') is not null
Drop Table dbo.MAUI_A_Errors_PDSR

Select * into dbo.MAUI_A_Errors_PDSR 
from import.import_rules with (nolock)
where 1 = 1 
and host_rule_xref in 
(
Select rebrecno from import.import_sxe_pdsr with (nolock)
where 1 = 1 
and rebrecno in (select host_rule_xref from import.import_rules with (nolock) where unique_key1 in
(select unique_key1 from import.import_rules with (nolock) group by unique_key1 having count(*) > 1))
and isnull(Cast(enddt as datetime), getdate() + 1) > getdate()
)

delete from import.import_rules
where 1 = 1 
and host_rule_xref in 
(
Select rebrecno from import.import_sxe_pdsr with (nolock)
where 1 = 1 
and rebrecno in (select host_rule_xref from import.import_rules with (nolock) where unique_key1 in
(select unique_key1 from import.import_rules with (nolock) group by unique_key1 having count(*) > 1))
and rebsubty is not null
)

--------------------
--Load Rule Actions
--------------------
Insert into import.import_rules_data
(
Job_FK, Record_No, Rule_Name
, Rules_Action_Operation1
, Rules_Action_Operation2
, Compare_Operator
, Basis_Calc_DN, Basis1_Number, Basis1_DN, Basis2_DN, Basis2_Number, Basis3_DN
, Record_Status_CR_FK, Update_User, BASIS1_PRICESHEET_NAME, BASIS1_PRICESHEET_DATE
, Basis_Calc_Pricesheet_Name, Basis_Calc_Pricesheet_Date
)
Select
Job_FK
, Record_No
, RULE_NAME
, Rules_Action_Operation1     
, Rules_Action_Operation2
, '='
, Basis_Calc_DN
, Basis1_Number
, Basis1_DN
, Basis2_DN
, Basis2_Number
, Basis3_DN
, 1 Record_Status_CR_FK
, 'HOST REBATE RULES LOAD' Update_User
, BASIS1_PRICESHEET_NAME
, BASIS1_PRICESHEET_DATE
, Basis_Calc_Pricesheet_Name
, Basis_Calc_Pricesheet_Date
from ##PDSR PDSR

Update MB
Set Rebate_CB_Rules_FK = r.rules_id
from dbo.maui_basis mb
inner join synchronizer.RULES r with (nolock) on mb.rebate_cb_host_rule_xref = r.HOST_RULE_XREF and r.RESULT_DATA_NAME_FK = 111501
where isnull(Rebate_CB_Rules_FK, 0) <> r.rules_id

Update MB
Set Rebate_CB_Rules_FK = r.rules_id
from dbo.maui_basis mb
inner join synchronizer.RULES r with (nolock) on mb.rebate_cb_host_rule_xref = r.HOST_RULE_XREF and r.RESULT_DATA_NAME_FK = 111502
where isnull(Rebate_CB_Rules_FK, 0) <> r.rules_id

Update MBC
Set Rebate_CB_Rules_FK = r.rules_id
from dbo.maui_basis_calcs mbc
inner join synchronizer.RULES r with (nolock) on mbc.rebate_cb_host_rule_xref = r.HOST_RULE_XREF and r.RESULT_DATA_NAME_FK = 111501
where isnull(Rebate_CB_Rules_FK, 0) <> r.rules_id

Update MBC
Set Rebate_CB_Rules_FK = r.rules_id
from dbo.maui_basis_calcs mbc
inner join synchronizer.RULES r with (nolock) on mbc.rebate_cb_host_rule_xref = r.HOST_RULE_XREF and r.RESULT_DATA_NAME_FK = 111502
where isnull(Rebate_CB_Rules_FK, 0) <> r.rules_id

Update MRM
Set Host_Rule_Xref = IR.Host_Rule_Xref
from dbo.MAUI_Rules_Maintenance MRM
      Inner Join Import.IMPORT_RULES IR with (nolock) on MRM.Unique_Key1 = IR.Unique_Key1
Where MRM.Transferred_To_ERP = 1 and MRM.Host_Rule_Xref like 'New:%' and ir.ERROR_ON_IMPORT_IND is null

Update R
Set Name = ir.rule_name
, host_rule_xref = ir.host_rule_xref
from Synchronizer.rules R
      Inner Join import.IMPORT_RULES ir with (nolock) 
      on ir.UNIQUE_KEY1 = r.unique_key1 and r.result_data_name_fk in (111501, 111502)

If (select count(*) from import.import_rules with (nolock) where isnull(end_date, getdate() + 1) > getdate()) > (Select Cast(isnull((Select value from epacube.epacube_params with (nolock) where name = 'MIN RECORDS EXPECTED REBATES'), 10000000) as bigint))
Begin
		Update R
		Set Record_status_cr_fk = 2
		, End_Date = Getdate() -1
		, Unique_Key1 = Case When UNIQUE_KEY1 like '%_Expired%' then UNIQUE_KEY1 else UNIQUE_KEY1 + '_Expired' end
		from Synchronizer.rules R
		Left Join ##PDSR Rls
		on rls.HOST_RULE_XREF = r.Host_rule_xref
		where r.result_data_name_fk in (111501, 111502)
		and r.Rules_ID >= 2000000000
		and r.record_status_cr_fk = 1
		and r.UPDATE_USER not like '%zcurve%'
		and Rls.EFFECTIVE_DATE is null

		Update R
		Set Record_status_cr_fk = 2
			, End_Date = Cast(pdsr.enddt as date)
			, Unique_Key1 = Case When UNIQUE_KEY1 like '%_Expired%' then UNIQUE_KEY1 else UNIQUE_KEY1 + '_Expired' end
		from synchronizer.RULES r 
		inner join import.IMPORT_SXE_PDSR pdsr on r.HOST_RULE_XREF = pdsr.rebrecno and r.RESULT_DATA_NAME_FK in (111501, 111502)
		where r.RESULT_DATA_NAME_FK in (111501, 111502)
		and isnull(Cast(pdsr.enddt as datetime), GETDATE() + 1) < GETDATE()
		and RECORD_STATUS_CR_FK = 1

End

--Set up currently inactive rules to be re-activated where host erp rules have been re-activated

      Update Rls
      Set Record_status_cr_fk = 1
      , end_date = Null
	  , Unique_key1 = Replace(rls.Unique_Key1, '_Expired', '')
      from Synchronizer.rules rls
      inner join import.import_rules ir with (nolock) on rls.host_rule_xref = ir.host_rule_xref and rls.RESULT_DATA_NAME_FK = (select data_name_id from epacube.DATA_NAME with (nolock) where name = ir.Result_data_Name)
      where 1 = 1
      and rls.record_status_cr_fk = 2
      and isnull(ir.end_date, getdate() + 1) > getdate()
----

Update DN
set record_status_cr_fk = 1
from epacube.data_name Dn
where record_status_cr_fk = 2
and name in (select basis_calc_dn from import.import_rules_data with (nolock) group by basis_calc_dn)

Drop Table ##PDSR
IF object_id('dbo.MAUI_A_Errors_PDSR') is not null
Drop Table dbo.MAUI_A_Errors_PDSR

IF object_id('tempdb..##Corrupt') is not null
drop table ##Corrupt

IF object_id('tempdb..#dels') is not null
drop table #dels

---LOAD INTO SYNCHRONIZER RULES TABLES

            EXEC import.LOAD_IMPORT_RULES @IN_JOB_FK

Update RA
Set BASIS1_PRICESHEET_DATE = RA_u.PriceEffectiveDateTo
from synchronizer.RULES_ACTION RA
inner join
(Select Cast(pst_date as datetime) PriceEffectiveDateTo, RULES_ACTION_ID from (
Select Convert(Varchar(10), left(PDSR.[priceeffectivedateto ], 2) + '/' + SUBSTRING(PDSR.[priceeffectivedateto ], 4, 2) + '/' + Substring(PDSR.[priceeffectivedateto ], 7, 2), 1) PST_Date, ra.* from synchronizer.RULES r with (nolock)
inner join synchronizer.RULES_ACTION ra with (nolock) on r.RULES_ID = ra.RULES_FK
inner join import.IMPORT_SXE_PDSR PDSR with (nolock) on r.HOST_RULE_XREF = pdsr.rebrecno and r.RESULT_DATA_NAME_FK = 111501
where ra.BASIS1_PRICESHEET_NAME is not null and ra.BASIS1_PRICESHEET_DATE is null and PDSR.[priceeffectivedateto ] is not null
and ISNUMERIC(left(PDSR.[priceeffectivedateto ], 2)) = 1
and r.RECORD_STATUS_CR_FK = 1 
) A where PST_Date not like '%//%') RA_u on RA.rules_action_ID = RA_u.Rules_action_ID

--Update Maui_Rules_Maintenance
If (Select Record_status_CR_FK from dbo.MAUI_Configuration with (nolock) where [Class] = 'Customer Price List Calculation' and Value = 'Customer Criteria') = 1
BEGIN
	Exec [import].[Import_To_Production_Rules_Maintenance_Update_From_SX] 111501

		Update MRM
		Set rules_fk = r.rules_id
		from dbo.MAUI_Rules_Maintenance MRM
			Inner Join synchronizer.rules r with (nolock)
			on MRM.host_rule_xref = r.host_rule_xref and MRM.Result_Data_Name_FK = R.RESULT_DATA_NAME_FK
		Where MRM.result_data_name_fk = 111501
		and MRM.Rules_FK <> r.RULES_ID

		Update MRM
		Set rules_action_fk = ra.rules_action_id
		from dbo.MAUI_Rules_Maintenance MRM
			Inner Join synchronizer.rules r with (nolock)
				on MRM.host_rule_xref = r.host_rule_xref and MRM.Result_Data_Name_FK = R.RESULT_DATA_NAME_FK
			Inner Join synchronizer.rules_action ra with (nolock) on r.rules_id = ra.RULES_FK
		Where MRM.result_data_name_fk = 111501
		and MRM.Rules_action_FK <> ra.RULES_ACTION_ID

	--Exec import.LOAD_MAUI_Z_RULES_DATA_ELEMENTS	
END

EndOfProc:
END
