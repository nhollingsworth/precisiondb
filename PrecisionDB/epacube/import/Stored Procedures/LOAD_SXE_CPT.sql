﻿--A_epaMAUI_IMPORT_LOAD_SXE_CPT

-- Copyright 2011 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        07/30/2011   Initial Version 
-- CV        08/31/2011   Tightening down code and add logging 
-- KS        09/27/2011   Added to Where Clause to not allow the insert of NULL value to DATA_VALUE Table
-- KS        09/27/2011   Added NO LOCK to all Selects and Commented out Check for Customer Exists
-- CV        10/18/2011   allow 0 value for discount code to be loaded.
-- CV        01/17/2012   added call to cpt crt desc procedure
-- CV        01/16/2013   JIRA - EPA-3470 creating bill to custprice type when should not. added code to stop.
-- CV        02/07/2013   found drop of wrong temp table
-- CV        10/28/2013   Value1 IS NULL added to query on insert entity mult type
-- GS		 3/17/2015	  Added check to end of procedure so it runs again if the counts of imported parent Customer Price Types don't match the counts in the import table.
-- GS		 9/23/2015	  Added an ASCII(istd.VALUE4) between 49 and 57 to limit selection to only valid data (otherwise getting data errors where a carriage return is mistakenly in the field).


CREATE PROCEDURE [import].[LOAD_SXE_CPT] ( @IN_JOB_FK bigint = Null) 
AS BEGIN 

If @IN_JOB_FK = Null
Set @IN_JOB_FK = (select top 1 job_fk from import.IMPORT_SXE_TABLE_DATA)

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128)
	 DECLARE @V_SXE_MULTIPLE_CPT varchar(1)
	 DECLARE @V_SXE_MULTIPLE_CRT varchar(1)
	 DECLARE @V_RULES_FILTER_LOOKUP_OPERAND varchar(1) 
	 Declare @Pass int
	 Set @Pass = 0

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_IMPORT_CPT_DATA', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.IMPORT_SXE_TABLE_DATA WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM import.IMPORT_SXE_TABLE_DATA WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD IMPORT SXE CUSTOMER CPT',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


TryAgain:
Set @Pass = @Pass + 1
-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( JOB_ERRORS TABLE )
--
--     THESE ERRORS PROHIBIT THE ROWS FROM BEING IMPORTED
--
--	   ERROR NAME:  INCOMPLETE OR INVALID DATA PROHIBITING IMPORT 
--     EXAMPLE IS MISSING OR INVALID price code or discount code = 0
--
--     IMPORT ROW MARKED  ERROR_ON_IMPORT_IND = 1 
--
-----------------------------------------------------------------------------------------
set @V_RULES_FILTER_LOOKUP_OPERAND  = (
select isnull(value,'0') 
from epacube.EPACUBE_PARAMS 
where name = 'RULES_FILTER_LOOKUP_OPERAND')

IF @V_RULES_FILTER_LOOKUP_OPERAND = 1

BEGIN


update import.IMPORT_SXE_TABLE_DATA
set error_on_import_ind = 1
where DATA_NAME <> 'Customer PType'
and job_fk = @in_job_Fk



--update import.IMPORT_SXE_TABLE_DATA
--set error_on_import_ind = 1
--where DATA_NAME = 'Customer PType'
--and substring(value4,1,1) = '0'
--and job_fk = @in_job_Fk


--INSERT INTO [common].[JOB_ERRORS]
--           ([JOB_FK]
--           ,[TABLE_NAME]
--           ,[TABLE_PK]
--           ,[EVENT_CONDITION_CR_FK]
--           ,[ERROR_NAME]
--           ,[ERROR_DATA1]
--           ,[ERROR_DATA2]
--           ,[ERROR_DATA3]
--           ,[ERROR_DATA4]
----           ,[ERROR_DATA5]
----           ,[ERROR_DATA6]
--           ,[CREATE_TIMESTAMP])
--SELECT 
--JOB_FK
--,'RULES_FILTER_LOOKUP_OPERAND_POSITION'
--,NULL
--,99
--,'INVALID PRICE/DISCOUNT CODE POSITION'
--,Value4
--,'Customer PTYPE'
--,VALUE1
--,NULL
--,getdate()
--FROM import.IMPORT_SXE_TABLE_DATA WITH (NOLOCK) 
--WHERE ISNULL(ERROR_ON_IMPORT_IND ,0) = 1
--AND DATA_NAME = 'Customer PType'
--AND JOB_FK = @in_job_fk

------------------------------------------------------------------------------------------
--- Truncate and Insert
------------------------------------------------------------------------------------------

TRUNCATE TABLE synchronizer.RULES_FILTER_LOOKUP_OPERAND_POSITION

------------------------------------------------------------------------------------------

-- create temporary tables and indexes

SET @status_desc = ('Loading Bill To Customer Price Types')
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk
			
			 exec common.job_execution_create  @in_job_fk, 'Loading Bill to Customer Price types',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;


CREATE TABLE #TS_GET_BILLTO  (
	[RECORD_NO] [bigint],
    [BILL_TO] Varchar(128))
    
    --- Get all bill to where ship to is NULL
    INSERT INTO #TS_GET_BILLTO
    (RECORD_NO
    ,BILL_TO)
    (SELECT Record_no
    ,VALUE2
    FROM import.IMPORT_SXE_TABLE_DATA  WITH (NOLOCK) 
	where VALUE3 is NULL
	AND  ISNULL(ERROR_ON_IMPORT_IND ,0) = 0
	AND DATA_NAME = 'Customer PType'
	AND JOB_FK = @in_job_fk)
	
	--- Get all Bill To where ship to is not null and bill to is not already there
	INSERT INTO #TS_GET_BILLTO
    (RECORD_NO
    ,BILL_TO)
    (SELECT Record_no
    ,VALUE2
    FROM import.IMPORT_SXE_TABLE_DATA  WITH (NOLOCK) 
	where VALUE3 is NOT NULL
	AND  ISNULL(ERROR_ON_IMPORT_IND ,0) = 0
	AND VALUE2 NOT IN (select GB.BILL_TO FROM #TS_GET_BILLTO GB)
	AND DATA_NAME = 'Customer PType'
	AND JOB_FK = @in_job_fk)

----BILL To and PRICE CODE where ship to is null

INSERT INTO [synchronizer].[RULES_FILTER_LOOKUP_OPERAND_POSITION]
           ([RULES_FILTER_LOOKUP_OPERAND_FK]
           ,[RULES_FILTER_ENTITY_DN_FK]
           ,[RULES_FILTER_ENTITY_ID_VALUE]
           ,[RULES_FILTER_DN_FK]
           ,[RULES_FILTER_VALUE]
           ,[OPERAND_FILTER_DN_FK]
           ,[OPERAND_FILTER_VALUE]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
     (SELECT Distinct 
           1000   --RULES_FILTER_LOOKUP_OPERAND_FK
           ,144010  -- BILL TO DATANAME   RULES_FILTER_ENTITY_DN_FK
           ,ISTD.VALUE2 ---RULES_FILTER_ENTITY_ID_VALUE
           ,244900 --RULES_FILTER_DN_FK
           ,ISTD.VALUE1  --RULES_FILTER_VALUE
           ,244905 --OPERAND_FILTER_DN_FK
           ,substring(ISTD.value4,1,1)  --pricecode OPERAND_FILTER_VALUE
           ,1  --RECORD_STATUS_CR_FK
           ,getdate()  --CREATE_TIMESTAMP
           ,NULL --UPDATE_TIMESTAMP
           ,NULL  --UPDATE_LOG_FK
            FROM import.IMPORT_SXE_TABLE_DATA  ISTD  WITH (NOLOCK)
            INNER JOIN #TS_GET_BILLTO GB WITH (NOLOCK)
            ON (ISTD.RECORD_NO = GB.RECORD_NO)
			where ISNULL(ERROR_ON_IMPORT_IND ,0) = 0
			and ASCII(istd.VALUE4) between 49 and 57
			AND ISTD.Job_fk = @in_job_fk)
           

----BILL To and Discount CODE

INSERT INTO [synchronizer].[RULES_FILTER_LOOKUP_OPERAND_POSITION]
           ([RULES_FILTER_LOOKUP_OPERAND_FK]
           ,[RULES_FILTER_ENTITY_DN_FK]
           ,[RULES_FILTER_ENTITY_ID_VALUE]
           ,[RULES_FILTER_DN_FK]
           ,[RULES_FILTER_VALUE]
           ,[OPERAND_FILTER_DN_FK]
           ,[OPERAND_FILTER_VALUE]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
     (SELECT distinct 
           1000   --RULES_FILTER_LOOKUP_OPERAND_FK
           ,144010  -- BILL TO DATANAME   RULES_FILTER_ENTITY_DN_FK
           ,ISTD.VALUE2 ---RULES_FILTER_ENTITY_ID_VALUE
           ,244900 --RULES_FILTER_DN_FK
           ,ISTD.VALUE1  --RULES_FILTER_VALUE
           ,244908 --OPERAND_FILTER_DN_FK
           ,substring(ISTD.value4,3,1)---discountcode  OPERAND_FILTER_VALUE
           ,1  --RECORD_STATUS_CR_FK
           ,getdate()  --CREATE_TIMESTAMP
           ,NULL --UPDATE_TIMESTAMP
           ,NULL  --UPDATE_LOG_FK          
            FROM import.IMPORT_SXE_TABLE_DATA  ISTD  WITH (NOLOCK)
            INNER JOIN #TS_GET_BILLTO GB WITH (NOLOCK)
            ON (ISTD.RECORD_NO = GB.RECORD_NO)
			where ISNULL(ERROR_ON_IMPORT_IND ,0) = 0
			and ASCII(istd.VALUE4) between 49 and 57
			AND ISTD.Job_fk = @in_job_fk)
           
           
           
 DROP TABLE #TS_GET_BILLTO
 
 

----------------------------------------------------------------------------------------------------
--  SHIP TO INSERTS
----------------------------------------------------------------------------------------------------


SET @status_desc = ('Loading Ship To Customer Price Types')
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk
			
			 exec common.job_execution_create  @in_job_fk, 'Loading Ship to Customer Price types',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;


INSERT INTO [synchronizer].[RULES_FILTER_LOOKUP_OPERAND_POSITION]
           ([RULES_FILTER_LOOKUP_OPERAND_FK]
           ,[RULES_FILTER_ENTITY_DN_FK]
           ,[RULES_FILTER_ENTITY_ID_VALUE]
           ,[RULES_FILTER_DN_FK]
           ,[RULES_FILTER_VALUE]
           ,[OPERAND_FILTER_DN_FK]
           ,[OPERAND_FILTER_VALUE]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
     (SELECT distinct
           1000   --RULES_FILTER_LOOKUP_OPERAND_FK
           ,144020  -- SHIP TO DATANAME   RULES_FILTER_ENTITY_DN_FK
           ,ISTD.VALUE2 + '-'+ ISTD.VALUE3 ---RULES_FILTER_ENTITY_ID_VALUE
           ,244900 --RULES_FILTER_DN_FK
           ,ISTD.VALUE1  --RULES_FILTER_VALUE
           ,244905 --OPERAND_FILTER_DN_FK
           ,substring(ISTD.value4,1,1)  --pricecode OPERAND_FILTER_VALUE
           ,1  --RECORD_STATUS_CR_FK
           ,getdate()  --CREATE_TIMESTAMP
           ,NULL --UPDATE_TIMESTAMP
           ,NULL  --UPDATE_LOG_FK
           FROM import.IMPORT_SXE_TABLE_DATA  ISTD  WITH (NOLOCK) 
			where ISNULL(ERROR_ON_IMPORT_IND ,0) = 0
			and ISTD.VALUE3 is not NULL
			and ASCII(istd.VALUE4) between 49 and 57
			AND ISTD.Job_fk = @in_job_fk)

----Ship To and Discount CODE

INSERT INTO [synchronizer].[RULES_FILTER_LOOKUP_OPERAND_POSITION]
           ([RULES_FILTER_LOOKUP_OPERAND_FK]
           ,[RULES_FILTER_ENTITY_DN_FK]
           ,[RULES_FILTER_ENTITY_ID_VALUE]
           ,[RULES_FILTER_DN_FK]
           ,[RULES_FILTER_VALUE]
           ,[OPERAND_FILTER_DN_FK]
           ,[OPERAND_FILTER_VALUE]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK])
     (SELECT distinct 
           1000   --RULES_FILTER_LOOKUP_OPERAND_FK
           ,144020  -- BILL TO DATANAME   RULES_FILTER_ENTITY_DN_FK
           ,ISTD.VALUE2 + '-'+ ISTD.VALUE3 ---RULES_FILTER_ENTITY_ID_VALUE
           ,244900 --RULES_FILTER_DN_FK
           ,ISTD.VALUE1  --RULES_FILTER_VALUE
           ,244908 --OPERAND_FILTER_DN_FK
           ,substring(ISTD.value4,3,1)---discountcode  OPERAND_FILTER_VALUE
           ,1  --RECORD_STATUS_CR_FK
           ,getdate()  --CREATE_TIMESTAMP
           ,NULL --UPDATE_TIMESTAMP
           ,NULL  --UPDATE_LOG_FK
           FROM import.IMPORT_SXE_TABLE_DATA  ISTD  WITH (NOLOCK) 
			where ISTD.ERROR_ON_IMPORT_IND is null
			AND ISNULL(ERROR_ON_IMPORT_IND ,0) = 0
			and ASCII(istd.VALUE4) between 49 and 57
			AND ISTD.VALUE3 is Not NULL
			AND ISTD.Job_fk = @in_job_FK)


END --@V_RULES_FILTER_LOOKUP_OPERAND

-------------------------------------------------------------------------------
---insert cust price type to entity mult type table
---delete from data value and delete from entity mult type
--insert to data value then mutl type
-----------------------------------------------------------------------------------

SET @status_desc = ('Loading Bill To Entity Multi Type')
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk
			
			 exec common.job_execution_create  @in_job_fk, 'Loading Bill To Entity Multi Type',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

set @V_SXE_MULTIPLE_CPT  = (
select isnull(value,'0') 
from epacube.EPACUBE_PARAMS 
where name = 'SXE MULTIPLE CPT CHECK')

If @V_SXE_MULTIPLE_CPT = 1

BEGIN 

---Reset the data for CPT import

update import.IMPORT_SXE_TABLE_DATA
set error_on_import_ind = 0
where DATA_NAME = 'Customer PType'
and job_fk = @in_job_Fk

delete from epacube.ENTITY_MULT_TYPE where DATA_NAME_FK = 244900

delete from epacube.DATA_VALUE where DATA_NAME_FK = 244900


INSERT INTO [epacube].[DATA_VALUE]
           ([DATA_NAME_FK]
           ,[VALUE]
           ,[JOB_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_USER])
     (SELECT distinct
            244900
           ,ISTD.VALUE1
           ,ISTD.JOB_FK
           ,1
           ,GETDATE()
           ,'CPT IMPORT' --UPDATE_USER
           from import.IMPORT_SXE_TABLE_DATA  ISTD  WITH (NOLOCK) 
			where ISTD.DATA_NAME = 'Customer PType'
			AND ISNULL(istd.ERROR_ON_IMPORT_IND ,0) <> 1
			and istd.job_fk = @IN_JOB_FK
			AND ISTD.VALUE1 IS NOT NULL   --- KS ADDED TO NOT ALLOW NULL VALUE INSERT INTO DATA VALUE TABLE
			)


INSERT INTO [epacube].[ENTITY_MULT_TYPE]
           ([ENTITY_STRUCTURE_FK]
           ,[DATA_NAME_FK]
           ,[DATA_VALUE_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP])
   ( SELECT DISTINCT 
           EI.entity_structure_fk  --ENTITY_STRUCTURE_FK
           ,244900  --DATA_NAME_FK
           ,DV.DATA_VALUE_id
           ,1
           ,GETDATE()         
from import.IMPORT_SXE_TABLE_DATA ISTD with (nolock)
INNER JOIN epacube.ENTITY_IDENTIFICATION EI with (nolock)
on (EI. value = ISTD.value2
and EI.data_name_fk = 144111 
and EI.ENTITY_DATA_NAME_FK = 144010)
INNER JOIN epacube.DATA_VALUE DV With (nolock)
on (dv.value = istd.value1 and dv.data_name_fk = 244900)
where ISTD.DATA_NAME = 'Customer PType'
AND istd.VALUE3 is NULL   ---added JIRA epa - 3470
AND ISNULL(ERROR_ON_IMPORT_IND ,0) <> 1
and istd.job_fk = @IN_JOB_FK )

 

update import.IMPORT_SXE_TABLE_DATA
set error_on_import_ind = 0
where DATA_NAME = 'Customer PType'
and job_fk = @in_job_Fk



INSERT INTO [epacube].[ENTITY_MULT_TYPE]
           ([ENTITY_STRUCTURE_FK]
           ,[DATA_NAME_FK]
           ,[DATA_VALUE_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP])
   ( SELECT DISTINCT
           ei.entity_structure_fk  --ENTITY_STRUCTURE_FK
           ,244900  --DATA_NAME_FK
           ,(select DATA_VALUE_id from epacube.data_value  WITH (NOLOCK)  
             where value = value1 and data_name_fk = 244900)
           ,1
           ,GETDATE()         
from import.IMPORT_SXE_TABLE_DATA ISTD  with (NOLOCK)
INNER JOIN EPACUBE.ENTITY_IDENTIFICATION EI with (NOLOCK)
ON (EI.value = ISTD.VALUE2 + '-' + ISTD.VALUE3
AND data_name_fk = 144111 
AND ENTITY_DATA_NAME_FK = 144020)
AND ISTD.DATA_NAME = 'Customer PType'
AND ISNULL(ERROR_ON_IMPORT_IND ,0) <> 1
AND ISTD.VALUE3 IS NOT NULL
and ISTD.value1 is Not NULL
and job_fk = @in_job_fk )



update epacube.DATA_VALUE
set DESCRIPTION =  (select import.IMPORT_SXE_TABLE_DATA.VALUE2 from import.IMPORT_SXE_TABLE_DATA  WITH (NOLOCK) 
where DATA_VALUE_ID = epacube.DATA_VALUE.DATA_VALUE_ID
and epacube.DATA_VALUE.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Customer Price type')
where epacube.data_value.DATA_NAME_FK = 244900

  
 END  --- end customer bill to ship to customer price type load
 
 
 
-------------------------------------------------------------------------------
---insert Rebate type to entity mult type table
---delete from data value and delete from entity mult type
--insert to data value then mutl type
-----------------------------------------------------------------------------------

SET @status_desc = ('Loading Ship To Entity Multi Type')
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk
			
			 exec common.job_execution_create  @in_job_fk, 'Loading Ship To Entity Multi Type',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;


set @V_SXE_MULTIPLE_CRT  = (
select isnull(value,'0') 
from epacube.EPACUBE_PARAMS  WITH (NOLOCK) 
where name = 'SXE MULTIPLE CRT CHECK')


If @V_SXE_MULTIPLE_CRT = 1

BEGIN 

---Reset the data for CRT import

update import.IMPORT_SXE_TABLE_DATA
set error_on_import_ind = 0
where DATA_NAME = 'Customer RType'
and job_fk = @in_job_Fk



 INSERT INTO epacube.DATA_VALUE
    (DATA_NAME_FK, 
    VALUE, 
    JOB_FK,  
    UPDATE_USER )
         SELECT  244901, 
         a.VALUE, 
         a.JOB_FK,  
         'CPT IMPORT'
      FROM 
          ( SELECT DISTINCT T_1.data_name_fk
          , T_1.VALUE
          , T_1.JOB_FK
              FROM  (SELECT '244901' as data_name_fk
              , istd.VALUE1 AS VALUE, 
				istd.JOB_FK AS JOB_FK 
                      FROM import.IMPORT_SXE_TABLE_DATA istd  WITH (NOLOCK)
			where ISTD.DATA_NAME = 'Customer RType'
			AND ISNULL(istd.ERROR_ON_IMPORT_IND ,0) <> 1
			and istd.job_fk = @in_job_fk
			) T_1
			WHERE T_1.VALUE IS NOT NULL  ---- KS ADDED TO AVOID CREATING A NULL ENTRY IN DATA_VALUE TABLE 
		 ) a
	  WHERE NOT( EXISTS
                  ( 
                    SELECT 1
                      FROM epacube.DATA_VALUE dv1  WITH (NOLOCK)
                      WHERE ((dv1.DATA_NAME_FK = 244901) AND 
                              (dv1.VALUE = a.VALUE))
                  ))
                  
                  



INSERT INTO [epacube].[ENTITY_MULT_TYPE]
           ([ENTITY_STRUCTURE_FK]
           ,[DATA_NAME_FK]
           ,[DATA_VALUE_FK]
           )
           SELECT a.entity_structure_fk, 
           a.data_name_fk, 
           a.data_value_id 
           FROM 
   ( select T_1.ENTITY_STRUCTURE_FK
   ,T_1.data_name_fk 
   ,T_1.DATA_VALUE_ID
   FROM ( SELECT DISTINCT
           EI.entity_structure_fk AS ENTITY_STRUCTURE_FK
           ,244901  AS DATA_NAME_FK
           ,DV.DATA_VALUE_id AS DATA_VALUE_ID       
from import.IMPORT_SXE_TABLE_DATA ISTD with (nolock)
INNER JOIN epacube.ENTITY_IDENTIFICATION EI with (nolock)
on (EI. value = ISTD.value2
AND data_name_fk = 144111
AND ENTITY_DATA_NAME_FK = 144010)
INNER JOIN epacube.DATA_VALUE DV With (nolock)
on (dv.value = istd.value1 and dv.data_name_fk = 244901)
where ISTD.DATA_NAME = 'Customer RType'
AND istd.VALUE3 is NULL   ---epa 3470
AND ISNULL(ERROR_ON_IMPORT_IND ,0) <> 1
and istd.job_fk = @in_job_fk) T_1 ) a 
    WHERE  NOT( EXISTS
                      ( 
                        SELECT 1
                          FROM epacube.ENTITY_MULT_TYPE emt  WITH (NOLOCK)
                          WHERE ((emt.DATA_NAME_FK = a.data_name_fk) AND 
                                  (emt.DATA_VALUE_FK = a.DATA_VALUE_ID) AND 
                                  (emt.ENTITY_STRUCTURE_FK = a.entity_structure_fk))
                      ))

 

update import.IMPORT_SXE_TABLE_DATA
set error_on_import_ind = 0
where DATA_NAME = 'Customer RType'
and job_fk = @in_job_fk




INSERT INTO [epacube].[ENTITY_MULT_TYPE]
           ([ENTITY_STRUCTURE_FK]
           ,[DATA_NAME_FK]
           ,[DATA_VALUE_FK]
           )
           SELECT a.entity_structure_fk, 
           a.data_name_fk, 
           a.data_value_id 
           FROM 
   ( select T_1.ENTITY_STRUCTURE_FK
   ,T_1.data_name_fk 
   ,T_1.DATA_VALUE_ID
   FROM ( SELECT DISTINCT
           EI.entity_structure_fk AS ENTITY_STRUCTURE_FK
           ,244901  AS DATA_NAME_FK
           ,DV.DATA_VALUE_id AS DATA_VALUE_ID       
from import.IMPORT_SXE_TABLE_DATA ISTD with (nolock)
INNER JOIN epacube.ENTITY_IDENTIFICATION EI with (nolock)
on (EI.value = ISTD.VALUE2 + '-' + ISTD.VALUE3
AND data_name_fk = 144111
AND ENTITY_DATA_NAME_FK = 144020)
INNER JOIN epacube.DATA_VALUE DV With (nolock)
on (dv.value = istd.value1 and dv.data_name_fk = 244901)
where ISTD.DATA_NAME = 'Customer RType'
AND ISTD.VALUE3 is NOT NULL
AND ISNULL(ERROR_ON_IMPORT_IND ,0) <> 1
and istd.job_fk = @in_job_fk) T_1 ) a 
    WHERE  NOT( EXISTS
                      ( 
                        SELECT 1
                          FROM epacube.ENTITY_MULT_TYPE emt  WITH (NOLOCK)
                          WHERE ((emt.DATA_NAME_FK = a.data_name_fk) AND 
                                  (emt.DATA_VALUE_FK = a.DATA_VALUE_ID) AND 
                                  (emt.ENTITY_STRUCTURE_FK = a.entity_structure_fk))
                      ))
           
          
update epacube.DATA_VALUE
set DESCRIPTION =  (select import.IMPORT_SXE_TABLE_DATA.VALUE2 
                    from import.IMPORT_SXE_TABLE_DATA  WITH (NOLOCK) 
					where DATA_VALUE_ID = epacube.DATA_VALUE.DATA_VALUE_ID
					and epacube.DATA_VALUE.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
					and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Customer Rebate type')
					where epacube.data_value.DATA_NAME_FK = 244901

          
 
 END  --- end customer bill to ship to customer Rebate type load


 --------------------------------------------------------------------------------
 EXECUTE  [import].[LOAD_SXE_CPT_CRT_DESC] 
   @IN_JOB_FK





 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD CUSTOMER CPT' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD CUSTOMER CPT' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------

If @Pass = 1 and (select COUNT(*) from import.IMPORT_SXE_TABLE_DATA where DATA_NAME = 'Customer PType') > isnull((Select value from epacube.epacube_params where NAME = 'MIN RECORDS EXPECTED TABLE'), 1000) and exists (
	Select * from (
	select emt.entity_structure_fk cust_entity_structure_fk, count(*) 'Local Records' from epacube.DATA_VALUE dv with (nolock)
	inner join epacube.ENTITY_MULT_TYPE emt with (nolock) on dv.DATA_VALUE_ID = emt.DATA_VALUE_FK 
	where dv.DATA_NAME_FK = 244900
	group by emt.entity_structure_fk
	) A
	Left join
	(
	Select * from (
	select VALUE2 CustID, (Select entity_structure_fk from epacube.ENTITY_IDENTIFICATION where DATA_NAME_FK = 144111 and ENTITY_DATA_NAME_FK = 144010
	and VALUE = ista.VALUE2) Cust_Entity_Structure_fk, COUNT(*) 'Import Records'
	from import.IMPORT_SXE_TABLE_DATA ista with (nolock)
	where DATA_NAME = 'customer ptype' and VALUE3 is null
	group by VALUE2
	) B
	Where Cust_Entity_Structure_fk is not null
	) C on A.cust_entity_structure_fk = C.Cust_Entity_Structure_fk
	where a.[Local Records] <> c.[Import Records]
)
Begin
	SET @status_desc = ('CPT Load: Record Counts Not Matching First Attempt')
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk

		insert into dbo.maui_log
		(job_fk_i, Activity, start_time)
		Select @IN_JOB_FK, 'CPT Import Pass 1: Counts Not Matching', GETDATE()

	Goto TryAgain
End

If @Pass = 2 and (select COUNT(*) from import.IMPORT_SXE_TABLE_DATA where DATA_NAME = 'Customer PType') > isnull((Select value from epacube.epacube_params where NAME = 'MIN RECORDS EXPECTED TABLE'), 1000) and exists (
	Select * from (
	select emt.entity_structure_fk cust_entity_structure_fk, count(*) 'Local Records' from epacube.DATA_VALUE dv with (nolock)
	inner join epacube.ENTITY_MULT_TYPE emt with (nolock) on dv.DATA_VALUE_ID = emt.DATA_VALUE_FK 
	where dv.DATA_NAME_FK = 244900
	group by emt.entity_structure_fk
	) A
	Left join
	(
	Select * from (
	select VALUE2 CustID, (Select entity_structure_fk from epacube.ENTITY_IDENTIFICATION where DATA_NAME_FK = 144111 and ENTITY_DATA_NAME_FK = 144010
	and VALUE = ista.VALUE2) Cust_Entity_Structure_fk, COUNT(*) 'Import Records'
	from import.IMPORT_SXE_TABLE_DATA ista with (nolock)
	where DATA_NAME = 'customer ptype' and VALUE3 is null
	group by VALUE2
	) B
	Where Cust_Entity_Structure_fk is not null
	) C on A.cust_entity_structure_fk = C.Cust_Entity_Structure_fk
	where a.[Local Records] > c.[Import Records] + 1
)
Begin
	SET @status_desc = ('CPT Load: Record Counts Not Matching 2nd Attempt')
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk

		insert into dbo.maui_log
		(job_fk_i, Activity, start_time)
		Select @IN_JOB_FK, 'CPT Import Pass 2: Counts Still Not Matching', GETDATE()
End

 SET @status_desc =  'finished execution of import.LOAD_CUSTOMER_CPT'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_CUSTOMER_CPT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END
