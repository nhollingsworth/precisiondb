﻿--A_epaMAUI_LOAD_0_MASTER

--GHS  August, 2013  Refined Transaction_Type Filters, Add OrderNo to import_Sales_Transactions_Active index
--GHS  October, 2013 Added Filter for Blanket Order Sales Transactions

-- =============================================
-- Author:		Gary Stone
-- Create date: 9/7/2011
-- Description:	MAUI DATA LOAD
-- =============================================
CREATE PROCEDURE [import].[LOAD_MAUI_0_MASTER] 
@Process_Cost_Changes_Only as int = Null
, @Process_BY_Region as int = Null
, @Assessment_Name as Varchar(128) = Null
, @Process_Future_AND_Current_Assessment as int = NULL
, @Date_Range_Start as DateTime = Null
, @Date_Range_End as DateTime = Null
, @Region_Name_To_Process as Varchar(64) = Null
, @Whse_To_Process as Varchar(Max) = Null
, @Whse_To_Exclude as varchar(Max) = Null
, @Vendor_IDs as Varchar(Max) = Null
, @Months_For_Assessment as Int = Null
, @StockOrders as int = Null
, @BlanketOrders as int = Null
, @PriceCorrections as int = Null
, @DropShips as int = Null
, @Job_FK_Fut as bigint = Null
, @MAUI_Job_FK as bigint = Null
, @MAUI_Job_FK_i as Numeric(10, 2) = Null
, @Load_MAUI_Only as int = 0
, @Use_All_Defaults as Int = 1
, @Defaults_To_Use as Varchar(64) = Null
, @NonStocks Int = 0
, @SpecialOrders Int = 0

AS
BEGIN

Set NOCOUNT On

If @Load_MAUI_Only = 1
	goto MAUI_Load

------
--Declare @Process_Cost_Changes_Only as int
--Declare @Process_BY_Region as int
--Declare @Assessment_Name as Varchar(128)
--Declare @Process_Future_AND_Current_Assessment as int
--Declare @Date_Range_Start as DateTime
--Declare @Date_Range_End as DateTime
--Declare @Region_Name_To_Process as Varchar(64)
--Declare @Whse_To_Process as Varchar(Max)
--Declare @Whse_To_Exclude as varchar(Max)
--Declare @Vendor_IDs as Varchar(Max)
--Declare @Months_For_Assessment as Int
--Declare @StockOrders as int
--Declare @BlanketOrders as int
--Declare @PriceCorrections as int
--Declare @DropShips as int
--Declare @MAUI_Job_FK as bigint
--Declare @MAUI_Job_FK_i as Numeric(10, 2)
--Declare @Load_MAUI_Only as int = 0
--Declare @Use_All_Defaults as Int = 1
--Declare @Defaults_To_Use as Varchar(64)
--Declare @NonStocks int
--Declare @SpecialOrders int

--Set @Process_Cost_Changes_Only = Null
--Set @Process_BY_Region = Null
--Set @Assessment_Name = Null
--Set @Process_Future_AND_Current_Assessment = NULL
--Set @Date_Range_Start = Null
--Set @Date_Range_End = Null
--Set @Region_Name_To_Process = Null
--Set @Whse_To_Process = Null
--Set @Whse_To_Exclude = Null
--Set @Vendor_IDs = Null
--Set @Months_For_Assessment = Null
--Set @StockOrders = Null
--Set @BlanketOrders = Null
--Set @PriceCorrections = Null
--Set @DropShips = Null
--Set @MAUI_Job_FK = Null
--Set @MAUI_Job_FK_i = Null
--Set @Load_MAUI_Only = 0
--Set @Use_All_Defaults = 1
--Set @Defaults_To_Use = Null
--Set @NonStocks = 0
--Set @SpecialOrders = 0

------
Declare @Job_FK as Bigint
Declare @Job_FK_i as Numeric(10, 2)
Declare @ERP as Varchar(32)
Declare @Prod_ID_DN_FK as bigint
Declare @Descript as varchar(8)
Declare @SQL_ec as Varchar(Max)
Declare @Calc_Exec_ID bigint
Declare @NonStocksFilter as Varchar(Max)
Declare @SpecialOrdersFilter as Varchar(Max)
Declare @StockOrderFilter as Varchar(Max)
Declare @DropShipFilter as Varchar(Max)
Declare @BlanketOrderFilter as Varchar(Max)

Set @ERP = (Select Value from epacube.epacube_params where name = 'ERP HOST')
Set @Prod_ID_DN_FK = (Select data_name_fk from dbo.MAUI_Config_ERP where usage = 'Data_Name' and epaCUBE_Column = 'Product_ID' and ERP = @ERP)

IF EXISTS (SELECT name FROM sysindexes WHERE name = 'idx_slstranact')
drop index idx_slstranact on import.import_sxe_sales_transactions_active

create index idx_slstranact on import.import_sxe_sales_transactions_active(prod, custno, shipto, whse, invoicedt, vendno, transtype, [prccostmult], slsrepout, Special_Stock_Ind, OrderNo)

IF object_id('tempdb..##CstChg') is not null
drop table ##CstChg

If @Defaults_To_Use is null
	Set @Defaults_To_Use = 'Primary'

exec [import].[LOAD_MAUI_0_JOB] 'Company Assessment Launch', 0, '710'
Set @Job_FK = (select top 1 Job_ID from common.job where JOB_CLASS_FK = 481 order by JOB_ID desc)

If @Use_All_Defaults = 1
	Begin
		If @Months_For_Assessment is Null and (Select top 1 Months_For_Assessment from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) is not null
			Set @Months_For_Assessment = isnull((Select top 1 Months_For_Assessment from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), 12)

		If @Process_Cost_Changes_Only is Null and (Select top 1 Process_Cost_Changes_Only from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) is not null
			Set @Process_Cost_Changes_Only = isnull((Select top 1 Process_Cost_Changes_Only from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), 0)

		If @Process_BY_Region is Null and (Select top 1 Process_BY_Region from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) is not null
			Set @Process_BY_Region = isnull((Select top 1 Process_BY_Region from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), 0)

		If @Assessment_Name is Null and (Select top 1 Assessment_Name from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) is not null
			Set @Assessment_Name = (Select top 1 Assessment_Name from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use)

		If @Process_Future_AND_Current_Assessment is Null and (Select top 1 Process_Future_AND_Current_Assessment from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) is not null
			Set @Process_Future_AND_Current_Assessment = isnull((Select top 1 Process_Future_AND_Current_Assessment from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), 0)

		If @Date_Range_Start is Null and (Select top 1 Date_Range_Start from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) is not null
			Set @Date_Range_Start = (Select top 1 Date_Range_Start from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use)
		Else If @Date_Range_Start is Null and @Months_For_Assessment is not null
			Set @Date_Range_Start = Cast(DATEADD(s,0,DATEADD(mm, DATEDIFF(m,0,getdate())-@Months_For_Assessment,0)) as DATETIME)

		If @Date_Range_End is Null and (Select top 1 Date_Range_End from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) is not null
			Set @Date_Range_End = (Select top 1 Date_Range_End from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use)
		Else If @Date_Range_End is Null and @Months_For_Assessment is not null
			Set @Date_Range_End = Cast(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)) as DATETIME)

		If @Region_Name_To_Process is Null and (Select top 1 Region_Name_To_Process from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) is not null
			Set @Region_Name_To_Process = (Select top 1 Region_Name_To_Process from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use)

		If @Whse_To_Process is Null and (Select top 1 Whse_To_Process from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) is not null
			Set @Whse_To_Process = (Select top 1 Whse_To_Process from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use)

		If @Whse_To_Exclude is Null and (Select top 1 Whse_To_Exclude from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) is not null
			Set @Whse_To_Exclude = (Select top 1 Whse_To_Exclude from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use)

		If @Vendor_IDs is Null and (Select top 1 Vendor_IDs from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use) is not null
			Set @Vendor_IDs = (Select top 1 Vendor_IDs from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use)

		If abs(@StockOrders) = 1 or abs(isnull((Select top 1 chkStockOrders from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), -1)) = 1
			Begin
				Set @StockOrders = 1
				Set @StockOrderFilter = ''
			End
		else
			Begin
			Set @StockOrderFilter = ' and ST.transtype <> ''stock order'''
		End
		
		If abs(@BlanketOrders) = 1 or abs(isnull((Select top 1 chkBlanketOrders from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), 0)) = 1
			Begin
				Set @BlanketOrders = 1
				Set @BlanketOrderFilter = ''
			End
		Else
			Set @BlanketOrderFilter = ' and ST.transtype <> ''Blanket Order'''

		--If @PriceCorrections = 1 or isnull((Select Top 1 chkPriceCorrections from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), 0) <> 0
		--	Set @PC = 'Price Correction'

		If Abs(@DropShips) = 1 or abs(isnull((Select Top 1 chkDropShips from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), 0)) = 1
			Begin
				Set @DropShipFilter = ''
				Set @DropShips = 1
			End
		else
				Set @DropShipFilter = ' and ST.transtype <> ''drop ship'''
						
		If abs(isnull(@NonStocks, 0)) = 1 or abs(isnull((Select Top 1 chkNonStocks from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), 1)) = 1
			Begin
				Set @NonStocks= 1
				Set @NonStocksFilter = ''
			End
		else
				Set @NonStocksFilter = ' And isnull(ST.Special_Stock_Ind, '''') not like ''%n%'''

			
		If Abs(isnull(@SpecialOrders, 0)) = 1 or abs(isnull((Select Top 1 chkSpecialOrders from dbo.MAUI_Defaults where [Default_Name] = @Defaults_To_Use), 0)) = 1
			Begin
				Set @SpecialOrdersFilter = ''
				Set @SpecialOrders = 1
			End
		else
				Set @SpecialOrdersFilter = ' And isnull(ST.Special_Stock_Ind, '''') not like ''%s%'''
			
	End
Else
	Begin
		If abs(isnull(@StockOrders, 0)) = 1
			Begin
				Set @StockOrderFilter = ''
				Set @StockOrders = 1
			End
		else
				Set @StockOrderFilter = ' and ST.transtype <> ''stock order'''
			
		If abs(isnull(@BlanketOrders, 0)) = 1
			Begin
				Set @BlanketOrderFilter = ''
				Set @BlanketOrders = 1
			End
		else
				Set @BlanketOrderFilter = ' and ST.transtype <> ''Blanket Order'''
			
		--If @PriceCorrections = 1
		--	Set @PC = 'Price Correction'

		If abs(isnull(@DropShips, 0)) = 1
			Begin
				Set @DropShipFilter = ''
				Set @DropShips = 1
			End
		else
				Set @DropShipFilter = ' and ST.transtype <> ''drop ship'''
								
		If abs(isnull(@NonStocks, 0)) = 1
			Begin
				Set @NonStocksFilter = ''
				Set @NonStocks = 1
			End
		else
				Set @NonStocksFilter = ' And isnull(ST.Special_Stock_Ind, '''') not like ''%n%'''
					
		If abs(isnull(@SpecialOrders, 0)) = 1
			Begin
				Set @SpecialOrdersFilter = ''
				Set @SpecialOrders = 1
			End
			else
				Set @SpecialOrdersFilter = ' And isnull(ST.Special_Stock_Ind, '''') not like ''%s%'''
	End

Set @Descript =		Case	When isnull(@Process_Future_AND_Current_Assessment, 0) = 1 then 'Cmp '
					When isnull(@Process_Cost_Changes_Only, 0) = 1 then 'Cst '
					Else 'Cur ' end

insert into dbo.maui_log Select @Job_FK, 'Start Assessment Load', getdate(), Null, Null

insert into dbo.maui_log Select @Job_FK, '@Job_FK = ' + Cast(@Job_FK as varchar(16)), getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Process_Cost_Changes_Only = ' + isnull(Cast(@Process_Cost_Changes_Only as varchar(16)), 'Null') , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Process_BY_Region = ' + isnull(Cast(@Process_BY_Region as varchar(16)), 'Null') , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Assessment_Name = ' + isnull(@Assessment_Name, 'Null') , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Process_Future_AND_Current_Assessment = ' + isnull(Cast(@Process_Future_AND_Current_Assessment as Varchar(16)), 'Null') , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Date_Range_Start = ' + isnull(Convert(Varchar(10),@Date_Range_Start, 1), 'Null') , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Date_Range_End = ' + isnull(Convert(Varchar(10),@Date_Range_End, 1), 'Null') , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Region_Name_To_Process = ' + isnull(@Region_Name_To_Process, 'Null') , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Whse_To_Process = ' + isnull(@Whse_To_Process, 'Null') , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Whse_To_Exclude = ' + isnull(@Whse_To_Exclude, 'Null') , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Vendor_IDs = ' + isnull(@Vendor_IDs, 'Null') , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Months_For_Assessment = ' + isnull(Cast(@Months_For_Assessment as varchar(16)), 'Null') , getdate(), Null, Null

insert into dbo.maui_log Select @Job_FK, '@StockOrders Include = ' + Cast(isnull(@StockOrders, '0') as varchar(16)) , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@DropShips Include = ' + Cast(isnull(@DropShips, '0') as varchar(16)) , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@BlanketOrders Include = ' + Cast(isnull(@BlanketOrders, '0') as varchar(16)) , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@NonStocks Include = ' + Cast(Abs(isnull(@NonStocks, '0')) as varchar(16)) , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@SpecialOrders Include = ' + Cast(Abs(isnull(@SpecialOrders, '0')) as varchar(16)) , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Defaults_To_Use = ' + isnull(Cast(@Defaults_To_Use as varchar(64)), 'Null') , getdate(), Null, Null
insert into dbo.maui_log Select @Job_FK, '@Use_All_Defaults = ' + isnull(Cast(@Use_All_Defaults as varchar(16)), 'Null') , getdate(), Null, Null

If ISNULL(@Process_Cost_Changes_Only,0) = 1
	Begin
		insert into dbo.maui_log Select @Job_FK_i, 'Creating Cost Change Filter', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null
		select 
			ed.PRODUCT_STRUCTURE_FK
			, ed.ORG_ENTITY_STRUCTURE_FK
			, ei.value Whse
			, pi.value Product_ID
			into ##CstChg
			from synchronizer.EVENT_DATA ED
			inner join epacube.PRODUCT_IDENTIFICATION PI on ED.Product_structure_fk = PI.product_Structure_fk and PI.data_name_fk = @Prod_ID_DN_FK
			inner join epacube.entity_identification EI on ED.Org_Entity_Structure_FK = EI.entity_Structure_FK and EI.entity_data_name_fk = 141000
			inner join
				(Select ed2.PRODUCT_STRUCTURE_FK, ed2.ORG_ENTITY_STRUCTURE_FK, Min(ed2.value_effective_date) Value_Effective_Date
				from synchronizer.EVENT_DATA ed2 
				Where ed2.value_effective_date > getdate()
				group by ed2.PRODUCT_STRUCTURE_FK, ed2.ORG_ENTITY_STRUCTURE_FK) EFFs
				on ED.Product_Structure_FK = EFFs.Product_Structure_FK and ED.ORG_ENTITY_STRUCTURE_FK = EFFs.ORG_ENTITY_STRUCTURE_FK and ED.Value_Effective_Date = EFFs.Value_Effective_Date
			inner join
				(Select ed3.PRODUCT_STRUCTURE_FK, ed3.ORG_ENTITY_STRUCTURE_FK, ed3.value_effective_date, MAX(ed3.Batch_No) Batch_No
				from synchronizer.EVENT_DATA ed3
				Where ed3.value_effective_date > getdate()
				group by ed3.PRODUCT_STRUCTURE_FK, ed3.ORG_ENTITY_STRUCTURE_FK, ed3.value_effective_date) BTCHs
				On ED.Product_Structure_FK = BTCHs.Product_Structure_FK and ED.ORG_ENTITY_STRUCTURE_FK = BTCHs.ORG_ENTITY_STRUCTURE_FK and ED.Value_Effective_Date = BTCHs.Value_Effective_Date
				and ED.Batch_No = BTCHs.Batch_No
			where 1 = 1
				and ed.EVENT_ACTION_CR_FK in (51, 52) 
				and ed.event_type_cr_fk = 152 
				and ed.RESULT_TYPE_CR_FK = 711 
				and ed.DATA_NAME_FK = 231100 
				and ed.VALUE_EFFECTIVE_DATE > GETDATE() 
			
			Create Index idx_chges on ##CstChg(PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, Whse, Product_ID)
	End

If @Process_BY_Region = 1 or @Region_Name_To_Process is not null
Begin
	Set @Job_FK_i = CAST(@Job_FK as Numeric(10, 2)) + 0.00
	
	Declare @Parent_Entity_Structure_FK as Bigint
	Declare @RN as Varchar(64)
	
		DECLARE Regions Cursor local for
		Select distinct es_id.parent_entity_structure_fk, ein.value Region_Name--, ei_ID2.Value Region_Code
		from import.import_sxe_sales_transactions_active ##st with (nolock) 
		left join epacube.entity_identification ei_id with (nolock) on ##st.whse = ei_id.value and ei_id.data_name_fk = 141111 --and Ei_ID.entity_data_name_fk = 141030
		left join epacube.entity_structure ES_ID with (nolock) on es_ID.Entity_Structure_ID = EI_ID.Entity_Structure_FK
		left Join epaCUBE.entity_identification ei_ID2 with (nolock) on ES_ID.parent_Entity_structure_fk = ei_ID2.entity_Structure_fk and Ei_ID2.entity_data_name_fk = 141030
		left join epacube.entity_identification ei_n with (nolock) on ##st.whse = ei_n.value and ei_n.data_name_fk = 141111 --and Ei_ID.entity_data_name_fk = 141030
		left join epacube.entity_structure ES_n with (nolock) on es_n.Entity_Structure_ID = EI_n.Entity_Structure_FK
		left Join epaCUBE.ENTITY_IDENT_NONUNIQUE ein with (nolock) on ES_n.parent_Entity_structure_fk = ein.entity_Structure_fk and Ein.entity_data_name_fk = 141030
		where ei_id2.value is not null and ein.value = isnull(@Region_Name_To_Process, ein.value)
		order by ein.value
	
			 OPEN Regions;
			 FETCH NEXT FROM Regions INTO @Parent_Entity_Structure_FK, @RN
	            
			 WHILE @@FETCH_STATUS = 0
	Begin
		If isnull(@Process_Future_AND_Current_Assessment, 0) = 0
			Begin
				Set @Job_FK_i = @Job_FK_i + 0.01
				exec [import].[LOAD_MAUI_0_JOB] @RN, @Job_FK_i, '710'
				Set @Job_FK = (Select job_id from common.job where job_class_fk = 481 and isnumeric(data2) = 1 and data2 = @Job_fk_i and data3 = '710')
				insert into dbo.MAUI_Parameters_Jobs(Job_FK, Job_FK_i, Job_Alias, What_if, Assessment_FK, Record_status_CR_FK) Select @Job_FK, @Job_FK_i, @Descript + ' ' + @RN, 0, @Job_FK, 2
			
			End
			Else
			Begin
				Set @Job_FK_i = @Job_FK_i + 0.01
				exec [import].[LOAD_MAUI_0_JOB] @RN, @Job_FK_i, '710'
				exec [import].[LOAD_MAUI_0_JOB] @RN, @Job_FK_i, '711'
				Set @Job_FK = (Select job_id from common.job where job_class_fk = 481 and isnumeric(data2) = 1 and data2 = @Job_fk_i and data3 = '710')
				Set @Job_FK_Fut = (Select job_id from common.job where job_class_fk = 481 and isnumeric(data2) = 1 and data2 = @Job_FK_i and data3 = '711')
				
				Update MPJ Set Job_FK_Fut = @Job_FK_Fut from dbo.MAUI_Parameters_Jobs MPJ where Job_FK_i = @Job_FK_i and isnull(What_if, 0) = 0
				
			End

		insert into dbo.maui_log Select @Job_FK_i, 'Start ' + @RN, getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null
		
		Exec [import].[Load_MAUI_0_Sales_Transactions_SXE] 
			@Job_FK_i
			, @Process_BY_Region
			, @Process_Future_AND_Current_Assessment
			, @Process_Cost_Changes_Only
			, @Parent_Entity_Structure_FK
			, @Whse_To_Process
			, @Whse_To_Exclude
			, @Vendor_IDs
			, @Date_Range_Start
			, @Date_Range_End
			, @StockOrderFilter
			, @DropShipFilter
			, @SpecialOrdersFilter
			, @NonStocksFilter
			, @BlanketOrderFilter

		If (Select COUNT(*) from marginmgr.ORDER_DATA_HISTORY) = 0 
		Begin
		goto NxtRegion
		end
		
		EXEC marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, 0

		insert into dbo.maui_log Select @Job_FK_i, 'Purge Calc Tables', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null
		
		insert into dbo.maui_log Select @Job_FK_i, 'Start Calc Job-Current', getdate(), Null, Null
		
		Insert into marginmgr.ANALYSIS_JOB
		(Analysis_Job_ID, Analysis_Name, result_type_cr_fk, rule_type_filter, sales_history_ind)
		Select @Job_FK, 'MAUI Assessment ' + (select Data1 + ' - ' + Data2 from common.job where job_id = @Job_FK), 710, '(312, 313)', 1
		
		EXEC  [marginmgr].[CALCULATION_PROCESS] @Job_FK

			Set @Calc_Exec_ID = (select exec_id from exec_monitor.ExecLog where exec_desc = 'started execution of MARGINMGR.CALCULATION_PROCESS_SELECT JOB:: ' + CAST(@Job_FK as varchar(8)))

			If (select COUNT(*) from exec_monitor.errlog where 1 = 1 and exec_id between @Calc_Exec_ID + 1 and @Calc_Exec_ID + 10 and (error_messsage like '%Execution of MARGINMGR.CALCULATION_ENGINE has failed%' or error_messsage like '%has failed Violation of UNIQUE KEY constraint%')) > 0 or (select COUNT(*) from synchronizer.EVENT_CALC_RULES ecr inner join synchronizer.EVENT_CALC ec on ecr.EVENT_FK = ec.EVENT_ID where ec.JOB_FK = @Job_FK) = 0
			Begin
				insert into dbo.maui_log Select @Job_FK_i, 'Current Calc Job Error - Process Stopped: Ln354', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Calc Job-Current'), 108)), (select count(*) from synchronizer.event_calc where result_data_name_fk = 111602 and job_fk = @Job_FK)
				goto eop
			End
		
		insert into dbo.maui_log Select @Job_FK_i, 'Current Calc Job Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Calc Job-Current'), 108)), (select count(*) from synchronizer.event_calc where result_data_name_fk = 111602 and job_fk = @Job_FK)

		If isnull(@Process_Future_AND_Current_Assessment, 0) = 1 or isnull(@Process_Cost_Changes_Only, 0) = 1
		Begin
			
			EXEC marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK_Fut, 0
			insert into dbo.maui_log Select @Job_FK_i, 'Start Calc Job-Future', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null

			Insert into marginmgr.ANALYSIS_JOB
			(Analysis_Job_ID, Analysis_Name, result_type_cr_fk, rule_type_filter, sales_history_ind, cost_change_first_ind)
			Select @Job_FK_Fut, 'MAUI Assessment ' + (select Data1 + ' - ' + Data2 from common.job where job_id = @Job_FK_Fut), 711, '(312, 313)', 1, 1

			EXEC  [marginmgr].[CALCULATION_PROCESS] @Job_FK_Fut
			
			insert into dbo.maui_log Select @Job_FK_i, 'Future Calc Job Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Calc Job-Future'), 108)), (select count(*) from synchronizer.event_calc where result_data_name_fk = 111602 and job_fk = @Job_FK_Fut)
			insert into dbo.maui_log Select @Job_FK_i, 'Start MAUI_Basis_Load - Current and Future', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null

			Exec [import].[LOAD_MAUI_1_BASIS] @Job_FK, @Job_FK_i, @Defaults_To_Use
			
			insert into dbo.maui_log Select @Job_FK_i, @RN + ' Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start ' + @RN), 108)), Null
			
			EXEC marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK_Fut, 0
			
			goto ContinueOn
		End

		Else
			Begin
				insert into dbo.maui_log Select @Job_FK_i, 'Start MAUI_Basis_Load - Current Only', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null
			
				Exec [import].[LOAD_MAUI_1_BASIS] @Job_FK, @Job_FK_i, @Defaults_To_Use
			
				insert into dbo.maui_log Select @Job_FK_i, @RN + ' Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start ' + @RN), 108)), Null
				
				EXEC marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, 0
				
				goto ContinueOn
			End
				    
	NxtRegion:
		If isnull(@Process_Future_AND_Current_Assessment, 0) = 0
			insert into dbo.maui_log Select @Job_FK_i, @RN + ' Not Specified', getdate(), Null, Null

	ContinueOn:
	
			FETCH NEXT FROM Regions INTO @Parent_Entity_Structure_FK, @RN    

	End
	Close Regions;
	Deallocate Regions;
End

Else
BEGIN

		If isnull(@Process_Future_AND_Current_Assessment, 0) = 0 --and isnull(@Process_BY_Region, 0) = 0 and isnull(@Region_Name_To_Process, 0) = 0
			Begin
				Set @Job_FK_i = CAST(@Job_FK as Numeric(10, 2)) + 0.01
				exec [import].[LOAD_MAUI_0_JOB] @Job_FK_i, @Job_FK_i, '710'
				Set @Job_FK = (Select job_id from common.job where job_class_fk = 481 and isnumeric(data2) = 1 and data2 = @Job_fk_i and DATA3 = '710')
				insert into dbo.MAUI_Parameters_Jobs(Job_FK, Job_FK_i, Job_Alias, What_if, Assessment_FK, Record_status_CR_FK) Select @Job_FK, @Job_FK_i, @Descript + ' ' + @Assessment_Name, 0, @Job_FK, 2
			End
			Else If isnull(@Process_Future_AND_Current_Assessment, 0) = 1 and isnull(@Process_BY_Region, 0) = 0 and isnull(@Region_Name_To_Process, 0) = 0
			Begin
				Set @Job_FK_i = CAST(@Job_FK as Numeric(10, 2)) + 0.01
				exec [import].[LOAD_MAUI_0_JOB] 'Current of C and F Assessment', @Job_FK_i, '710'
				exec [import].[LOAD_MAUI_0_JOB] 'Future of C and F Assessment', @Job_FK_i, '711'
				
				Set @Job_FK = (Select job_id from common.job where job_class_fk = 481 and isnumeric(data2) = 1 and data2 = @Job_fk_i and DATA3 = '710')
				Set @Job_FK_Fut = (Select job_id from common.job where job_class_fk = 481 and isnumeric(data2) = 1 and data2 = @Job_FK_i and DATA3 = '711')
				insert into dbo.MAUI_Parameters_Jobs(Job_FK, Job_FK_i, Job_Alias, What_if, Assessment_FK, Job_FK_Fut, Record_status_CR_FK) Select @Job_FK, @Job_FK_i, @Descript + ' ' + @Assessment_Name, 0, @Job_FK, @Job_FK_Fut, 2
			End
			
		insert into dbo.maui_log Select @Job_FK_i, 'Start Load - Single Assessment for entire run', getdate(), Null, Null
		
		If @ERP = 'SXE'
		Begin
			Exec [import].[Load_MAUI_0_Sales_Transactions_SXE] 
				@Job_FK_i
				, 0
				, @Process_Future_AND_Current_Assessment
				, @Process_Cost_Changes_Only
				, @Parent_Entity_Structure_FK
				, @Whse_To_Process
				, @Whse_To_Exclude
				, @Vendor_IDs
				, @Date_Range_Start
				, @Date_Range_End
				, @StockOrderFilter
				, @DropShipFilter
				, @SpecialOrdersFilter
				, @NonStocksFilter
				, @BlanketOrderFilter
		End		
		Else If @ERP = 'Eclipse'
		Begin
			Exec import.Load_MAUI_0_Sales_Transactions_ECLIPSE
				@Job_FK_i
				, 0
				, @Process_Future_AND_Current_Assessment
				, @Process_Cost_Changes_Only
				, @Parent_Entity_Structure_FK
				, @Whse_To_Process
				, @Whse_To_Exclude
				, @Vendor_IDs
				, @Date_Range_Start
				, @Date_Range_End
				, @StockOrderFilter
				, @DropShipFilter
				, @SpecialOrdersFilter
				, @NonStocksFilter

		End
		
		If (Select COUNT(*) from marginmgr.ORDER_DATA_HISTORY) = 0 goto eop
		
		EXEC marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, 0

		insert into dbo.maui_log Select @Job_FK_i, 'Purge Calc Tables', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null
		
		insert into dbo.maui_log Select @Job_FK_i, 'Start Calc Job-Current', getdate(), Null, Null
		
		Insert into marginmgr.ANALYSIS_JOB
		(Analysis_Job_ID, Analysis_Name, result_type_cr_fk, rule_type_filter, sales_history_ind)
		Select @Job_FK, 'MAUI Assessment ' + (select Data1 + ' - ' + Data2 from common.job where job_id = @Job_FK), 710, '(312, 313)', 1
		
		EXEC  [marginmgr].[CALCULATION_PROCESS] @Job_FK

			Set @Calc_Exec_ID = (select exec_id from exec_monitor.ExecLog where exec_desc = 'started execution of MARGINMGR.CALCULATION_PROCESS_SELECT JOB:: ' + CAST(@Job_FK as varchar(8)))

			If (select COUNT(*) from exec_monitor.errlog where 1 = 1 and exec_id between @Calc_Exec_ID + 1 and @Calc_Exec_ID + 10 and (error_messsage like '%Execution of MARGINMGR.CALCULATION_ENGINE has failed%' or error_messsage like '%has failed Violation of UNIQUE KEY constraint%')) > 0 or (select COUNT(*) from synchronizer.EVENT_CALC_RULES ecr inner join synchronizer.EVENT_CALC ec on ecr.EVENT_FK = ec.EVENT_ID where ec.JOB_FK = @Job_FK) = 0
			Begin
				insert into dbo.maui_log Select @Job_FK_i, 'Current Calc Job Error - Process Stopped: Ln479', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Calc Job-Current'), 108)), (select count(*) from synchronizer.event_calc where result_data_name_fk = 111602 and job_fk = @Job_FK)
				goto eop
			End
		
		insert into dbo.maui_log Select @Job_FK_i, 'Current Calc Job Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select Min(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), (select COUNT(*) from synchronizer.event_calc where JOB_FK = @Job_FK)

			If isnull(@Process_Future_AND_Current_Assessment, 0) = 1 or isnull(@Process_Cost_Changes_Only, 0) = 1
					Begin
					
						EXEC marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK_Fut, 0
						insert into dbo.maui_log Select @Job_FK_i, 'Purge Calc Tables-Current', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null
						insert into dbo.maui_log Select @Job_FK_i, 'Start Calc Job-Future', getdate(), Null, Null
			
						Insert into marginmgr.ANALYSIS_JOB
						(Analysis_Job_ID, Analysis_Name, result_type_cr_fk, rule_type_filter, sales_history_ind, cost_change_first_ind)
						Select @Job_FK_Fut, 'MAUI Assessment ' + (select Data1 + ' - ' + Data2 from common.job where job_id = @Job_FK_Fut), 711, '(312, 313)', 1, 1
				
						EXEC  [marginmgr].[CALCULATION_PROCESS] @Job_FK_Fut
						
						insert into dbo.maui_log Select @Job_FK_i, 'Future Calc Job Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select Min(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), (select COUNT(*) from synchronizer.event_calc where JOB_FK = @Job_FK_Fut)
						
						insert into dbo.maui_log Select @Job_FK, 'Start MAUI_Basis_Load - Current and Future', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null
						Exec [import].[LOAD_MAUI_1_BASIS] @Job_FK, @Job_FK_i, @Defaults_To_Use
						
						insert into dbo.maui_log Select @Job_FK_i, 'Assessment Load Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select Min(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select MAX(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null
						
						EXEC marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK_Fut, 0
						
					End

					Else
						Begin
							insert into dbo.maui_log Select @Job_FK_i, 'Start MAUI_Basis_Load - Current Only', getdate(), Null, Null
						
							Exec [import].[LOAD_MAUI_1_BASIS] @Job_FK, @Job_FK_i, @Defaults_To_Use
						
							insert into dbo.maui_log Select @Job_FK_i, 'Assessment Load Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and MAUI_Log_ID = (Select Min(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i)), 108)), Null
						
					EXEC marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, 0
						
						End

END
Goto EndOfCalcProcess

MAUI_Load:

	Exec [import].[LOAD_MAUI_1_BASIS] @MAUI_Job_FK, @MAUI_Job_FK_i, @Defaults_To_Use

	insert into dbo.maui_log Select @MAUI_Job_FK_i, 'Assessment Load Complete', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @MAUI_Job_FK_i and MAUI_Log_ID = (Select Min(MAUI_Log_ID) from dbo.MAUI_Log where Job_fk_i = @MAUI_Job_FK_i)), 108)), Null

	EXEC marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, 0
	
EndOfCalcProcess:

	If (isnull(@Process_Future_AND_Current_Assessment, 0) = 0 and isnull(@Process_Cost_Changes_Only, 0) = 0) or @Load_MAUI_Only = 1
		Begin
			If (Select COUNT(*) from dbo.MAUI_Basis_Calcs where Job_FK = @Job_FK) > 0 
				If ((Select top 1 datepart(DAYOFYEAR,Cast(CONVERT(Varchar(10), Update_timestamp, 1) as datetime)) from dbo.MAUI_Rules_Data_Elements with (nolock)) <> datepart(dayofyear, cast(GETDATE() as datetime)) Or (Select COUNT(*) from dbo.MAUI_Rules_Data_Elements) = 0) And (select Value from epacube.epacube_params where NAME = 'PRICING PRECEDENCE') = 'SX PRICE HIERARCHY STANDARD'
				Begin
					Exec [import].[LOAD_MAUI_Z_RULES_DATA_ELEMENTS]
				End
			insert into dbo.maui_log Select @Job_FK, 'Assessment Load and System Update Complete', GETDATE(), Convert(Varchar(10),GETDATE() - (Select Min(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i), 108), Null
		End
		Else
		Begin
			If (Select COUNT(*) from dbo.MAUI_Basis_Calcs where Job_FK = @Job_FK) > 0
				If ((Select top 1 datepart(DAYOFYEAR,Cast(CONVERT(Varchar(10), Update_timestamp, 1) as datetime)) from dbo.MAUI_Rules_Data_Elements with (nolock)) <> datepart(dayofyear, cast(GETDATE() as datetime)) Or (Select COUNT(*) from dbo.MAUI_Rules_Data_Elements) = 0) And (select Value from epacube.epacube_params where NAME = 'PRICING PRECEDENCE') = 'SX PRICE HIERARCHY STANDARD'
				Exec [import].[LOAD_MAUI_Z_RULES_DATA_ELEMENTS]
			insert into dbo.maui_log Select @Job_FK, 'Assessment Load and System Update Complete', GETDATE(), Convert(Varchar(10),GETDATE() - (Select Min(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i - 0.01), 108), Null
		End

eop:

IF object_id('tempdb..##CstChg') is not null
drop table ##CstChg

END
