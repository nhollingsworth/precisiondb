﻿















-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        01/10/2011   Initial Version
-- CV        02/22/2011   Only update the ship to cust if bill to is not found. 

CREATE PROCEDURE [import].[LOAD_IMPORT_CUST_CONTRACT_RULES] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_IMPORT_CUSTOMER_CONTRACT_RULES', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.IMPORT_ECLIPSE_CUSTOMER_CONTRACTS WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM import.IMPORT_ECLIPSE_CUSTOMER_CONTRACTS WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD IMPORT CUSTOMER CONTRACT RULES',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;




-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( JOB_ERRORS TABLE )
--
--     Remove Data that does not have the contract name.  given to us becuase 
--		these imports are used for other things.
-------------------------------------------------------------------------------------

delete from import.IMPORT_ECLIPSE_CUSTOMER_CONTRACTS
where Contract like 'ZZZ%'

delete from import.IMPORT_ECLIPSE_CUSTOMER_CONTRACTS
where Contract is NULL

-------------------------------------------------------------------------------------
--   MATCH IMPORT CUSTOMER CONTRACT RULES TO   SYNCHRONIZER.RULES_CONTRACT 
--		FOR CONTRACT AND CUSTOMERS
-------------------------------------------------------------------------------------

Update IECC
Set Rules_CONTRACT_FK = ( SELECT RC.RULES_CONTRACT_ID
						   FROM synchronizer.RULES_CONTRACT RC
						   WHERE RC.CONTRACT_NO = IECC.ID)
						  
FROM import.IMPORT_ECLIPSE_CUSTOMER_CONTRACTS IECC 
WHERE 1 = 1                   
AND   IECC.JOB_FK = @IN_JOB_FK 
AND   ISNULL ( IECC.ERROR_ON_IMPORT_IND, 0 ) = 0  


update import.IMPORT_ECLIPSE_CUSTOMER_CONTRACTS
set error_on_import_ind = 1
where Rules_contract_fk is  NULL
--
--INSERT INTO [common].[JOB_ERRORS]
--           ([JOB_FK]
--           ,[TABLE_NAME]
--           ,[TABLE_PK]
--           ,[EVENT_CONDITION_CR_FK]
--           ,[ERROR_NAME]
--           ,[ERROR_DATA1]
--           ,[ERROR_DATA2]
--           ,[ERROR_DATA3]
--           ,[ERROR_DATA4]
----           ,[ERROR_DATA5]
----           ,[ERROR_DATA6]
--           ,[CREATE_TIMESTAMP])
--SELECT 
--JOB_FK
--,'IMPORT CUSTOMER CONTRACTS'
--,NULL
--,99
--,'MISSING CONTRACT INFORMATION'
--,CUST_ID
--,CUST_NAME
--,ID
--,CONTRACT
--,getdate()
--FROM import.IMPORT_ECLIPSE_CUSTOMER_CONTRACTS
--WHERE ISNULL(ERROR_ON_IMPORT_IND ,0) = 1
--AND RULES_CONTRACT_FK IS NULL


-------------------------------------------------------------------------------------
--   MATCH IMPORT CUSTOMER  TO BILL To and SHIP TO EPACUBE.ENTITY_IDETIFICATION
--		FOR CUSTOMERS
-------------------------------------------------------------------------------------

Update IECC
Set ENTITY_STRUCTURE_FK = ( SELECT ei.entity_structure_fk
FROM EPACUBE.ENTITY_IDENTIFICATION EI
WHERE ei.value = IECC.CUST_ID
AND EI.ENTITY_DATA_NAME_FK =144010)  ---BILL TO CUST
						  
FROM import.IMPORT_ECLIPSE_CUSTOMER_CONTRACTS IECC 
WHERE 1 = 1                   
AND   IECC.JOB_FK = @IN_JOB_FK 


Update IECC
Set ENTITY_STRUCTURE_FK = ( SELECT ei.entity_structure_fk
FROM EPACUBE.ENTITY_IDENTIFICATION EI
WHERE ei.value = IECC.CUST_ID
AND EI.ENTITY_DATA_NAME_FK =144020)  ---SHIP TO CUST
						  
FROM import.IMPORT_ECLIPSE_CUSTOMER_CONTRACTS IECC 
WHERE 1 = 1       
AND IECC.ENTITY_STRUCTURE_FK IS NULL            
AND   IECC.JOB_FK = @IN_JOB_FK 
 



update import.IMPORT_ECLIPSE_CUSTOMER_CONTRACTS
set error_on_import_ind = 1
where entity_structure_fk is  NULL


INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[TABLE_PK]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[ERROR_DATA4]
--           ,[ERROR_DATA5]
--           ,[ERROR_DATA6]
           ,[CREATE_TIMESTAMP])
SELECT 
JOB_FK
,'IMPORT CUSTOMER CONTRACTS'
,NULL
,99
,'MISSING INFORMATION'
,CUST_ID
,CUST_NAME
,ID
,CONTRACT
,getdate()
FROM import.IMPORT_ECLIPSE_CUSTOMER_CONTRACTS
WHERE ISNULL(ERROR_ON_IMPORT_IND ,0) = 1
--AND ENTITY_STRUCTURE_FK IS NULL




-------------------------------------------------------------------------------------
--   SYNCHRONIZER.RULES_CUSTOMER CONTRACT INSERTS
-------------------------------------------------------------------------------------


INSERT INTO [synchronizer].[RULES_CONTRACT_CUSTOMER]
           ([RULES_CONTRACT_FK]
           ,[CUST_REFERENCE]
           ,[CUST_ENTITY_STRUCTURE_FK]
           ,[PRECEDENCE_ADJUSTMENT]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
						)
(SELECT
IECC.RULES_CONTRACT_FK
,IECC.CUST_NAME
,IECC.ENTITY_STRUCTURE_FK
,IECC.SEQ_ORDER
,1
,getdate()
,'IMPORT'
FROM IMPORT.IMPORT_ECLIPSE_CUSTOMER_CONTRACTS IECC
WHERE ISNULL(IECC.ERROR_ON_IMPORT_IND,0) = 0)


--
--UPDATE import.IMPORT_RULES
--SET RULES_FK = R.RULES_ID
--FROM synchronizer.RULES R WITH (NOLOCK)				
--WHERE 1 = 1
--AND   R.IMPORT_JOB_FK = @IN_JOB_FK
--AND   R.IMPORT_JOB_FK = import.IMPORT_RULES.JOB_FK
--AND   R.IMPORT_RULES_FK = import.IMPORT_RULES.IMPORT_RULES_ID
--AND   R.EVENT_ACTION_CR_FK = 54  
--

-------------------------------------------------------------------------------------
--   MATCH IMPORT RULES TO   SYNCHRONIZER.RULES JUST INSERTED 
--	 (  ALL IMPORT RULES SHOULD HAVE RULES_FK FROM THIS POINT FORWARD IN PROCESS )
-------------------------------------------------------------------------------------

--
--UPDATE import.IMPORT_RULES
--SET EVENT_ACTION_CR_FK = R.EVENT_ACTION_CR_FK
--FROM synchronizer.RULES R WITH (NOLOCK)				
--WHERE 1 = 1
--AND   import.IMPORT_RULES.JOB_FK = @IN_JOB_FK
--AND   R.RULES_ID = import.IMPORT_RULES.RULES_FK



-------------------------------------------------------------------------------------
--   SYNCHRONIZER.RULES_FILTER AND SYNCHRONIZER.RULES_FILTER_SET
-------------------------------------------------------------------------------------

--EXEC [import].[LOAD_IMPORT_RULES_FILTER] @IN_JOB_FK



-------------------------------------------------------------------------------------
--   SYNCHRONIZER.RULES_ACTION
-------------------------------------------------------------------------------------

--EXEC [import].[LOAD_IMPORT_RULES_ACTION] @IN_JOB_FK



-------------------------------------------------------------------------------------
--   SYNCHRONIZER.RULES_ACTION_OPERANDS
-------------------------------------------------------------------------------------


--EXEC [import].[LOAD_IMPORT_RULES_OPERANDS] @IN_JOB_FK


-------------------------------------------------------------------------------------
--   SYNCHRONIZER.RULES_ACTION_STRFUNCTION
-------------------------------------------------------------------------------------


--EXEC [import].[LOAD_IMPORT_RULES_STRFUNCTION] @IN_JOB_FK


----add some logging

 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD IMPORT RULES CUSTOMER CONTRACTS' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD IMPORT RULES CUSTOMER CONTRACTS' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
-------------------------------------------------------------------------------------
--   INSERT ERRORS....    ( RULES_ERRORS TABLE )
--
--     ADD RULES VALIDATION ROUTINE TO BE CALLED FROM UI LATER.. 
--
--	   THESE ERRORS ARE BUSINESS LOGIC ERRORS 
--        WHICH WILL BE BY RULE_TYPE_CR_FK AND RULES_ACTION_OPERATION_FK
--     
--     RULE WILL BE WRITTEN TO RULES TABLE BUT WITH AN ERROR CONDITION
--
--
-------------------------------------------------------------------------------------






 SET @status_desc =  'finished execution of import.LOAD_IMPORT_RULES_CUSTOMER_CONTRACT'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_IMPORT_RULES_CUSTOMER_CONTRACT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END
