﻿--A_epaMAUI_LOAD_MAUI_3_BASIS_CALCS

CREATE PROCEDURE [import].[LOAD_MAUI_3_BASIS_CALCS] 
		@Job_FK BigInt, @Job_FK_i Numeric(10, 2), @Defaults_To_Use as Varchar(64)
	AS
	BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

	--Declare @Job_FK bigint
	--Declare @Job_FK_i as Numeric(10, 2)
	--Declare @Defaults_To_Use as Varchar(64)
	--set @Job_FK = 3348
	--Set @Job_FK_i = 3347.01
	--Set @Defaults_To_Use = 'Primary'

		-- Insert statements for procedure here
	Declare @Job_FK_i_BL Numeric(10, 2)
	Declare @ProjectionDays Numeric(5, 0)
	Declare @HistoryDays Numeric(5, 0)
	Declare @AssType Int
	Declare @RebateMethod Int
	Declare @PriceMethod Int
	Declare @UseFileDates Int
	Declare @ERP Varchar(32)

	Set @Job_FK_i_BL = isnull((Select top 1 BL_Job_FK_i from dbo.MAUI_Basis_Baseline with (nolock) group by BL_Job_FK_i, BL_Sales_History_Start_Date, BL_Sales_History_End_Date), 0) --(Select BL_Job_FK_i, CONVERT(VARCHAR(10), BL_Sales_History_Start_Date, 101) [Start_Date], CONVERT(VARCHAR(10), BL_Sales_History_End_Date, 101) [End_Date] from dbo.MAUI_Basis_Baseline group by BL_Job_FK_i, BL_Sales_History_Start_Date, BL_Sales_History_End_Date)
	Set @AssType = (Select top 1 Assessment_type from dbo.MAUI_Defaults with (nolock) where [Default_Name] = @Defaults_To_Use)
	Set @RebateMethod = (Select top 1 Future_Rebate_Lookup_Method from dbo.MAUI_Defaults with (nolock) where [Default_Name] = @Defaults_To_Use)		--@RebateMethod  1 = Lookup New Rebate Amounts; @RebateMethod  2 = Calculate New Rebate Amounts
	Set @PriceMethod = (Select top 1 Future_Price_Lookup_Method from dbo.MAUI_Defaults with (nolock) where [Default_Name] = @Defaults_To_Use)
	Set @HistoryDays = Cast((Select top 1 Sales_History_End_Date - Sales_History_Start_Date 
		from dbo.MAUI_BASIS with (nolock) where job_fk_i = @job_fk_i) as Numeric(6,0)) 
	Set @UseFileDates = (Select top 1 UseFileDates from dbo.MAUI_Defaults with (nolock) where [Default_Name] = @Defaults_To_Use)
	Set @ProjectionDays =
		Case when @UseFileDates = 0 then (Select top 1 ProjectionDays from dbo.MAUI_Defaults with (nolock) where [Default_Name] = @Defaults_To_Use) else @HistoryDays	end
	Set @ERP = (Select Value from epacube.epacube_params with (nolock) where name = 'ERP HOST')

	Delete from dbo.MAUI_Basis_Calcs where job_fk_i = @Job_FK_i

	insert into dbo.maui_log Select @Job_FK_i, 'Start Loading MAUI Basis Calcs', GetDate(), Null, Null

	IF object_id('tempdb..##Calcs1') is not null
	Drop table ##Calcs1

	IF object_id('tempdb..##Calcs2') is not null
	Drop table ##Calcs2

	IF object_id('tempdb..##Calcs3') is not null
	Drop table ##Calcs3

	IF object_id('tempdb..##Basis_New') is not null
	Drop Table ##Basis_New

	insert into dbo.maui_log Select @Job_FK_i, 'Start Loading MAUI Basis Calcs-##Calcs1', GetDate(), Null, Null

	Select
	Job_FK_i
	, calc_status
	, identifier
	, isnull([Sell_Price_Cust_Amt], 0) 'Sell_Price_Cust_Amt_New_AT1'
	, isnull([Sell_Price_Cust_Amt], 0) 'Sell_Price_Cust_Amt_New'
	, [Margin_Cost_Basis_Effective_Date] 'Margin_Cost_Basis_Effective_Date_AT1'
	, [Margin_Cost_Basis_Effective_Date] 'Margin_Cost_Basis_Effective_Date_2'
	, [Margin_Cost_Basis_Amt] 'Margin_Cost_Basis_Amt_2'
	, Case @ERP When 'SXE' then Rebate_CB_Amt When 'Eclipse' then [Margin_Cost_Basis_Amt] - ISNULL([Rebated_Cost_CB_Amt], 0) end	'Rebate_CB_Amt_New'
	, isnull(Rebate_BUY_Amt, 0) 'Rebate_BUY_Amt_New'
	, Sell_Price_Rebate_Cap
	, sell_price_cust_rules_fk
	, sell_price_cust_effective_date
	, sell_price_cust_end_date
	Into ##Basis_New
	from dbo.MAUI_BASIS with (nolock) where Job_FK_i = @Job_FK_i and calc_status = 'New'
	and sell_price_cust_amt <> 0 and identifier is not null

	Create index idx_bas1_new on ##Basis_New(Job_FK_i, calc_status, identifier)

	Select Sales_Qty / @HistoryDays 'Sales_Qty_Daily'
	, Round(Sales_Qty / @HistoryDays * @ProjectionDays, 2) 'Sales_Qty'
	, [Sales_History_End_Date]
	, [Sales_History_Start_Date]
	, isnull(Sales_Ext_Hist, 0) Sales_Ext_Hist
	, isnull(Prod_Cost_Ext_Hist, 0) Prod_Cost_Ext_Hist
	, Isnull(Rebate_Amt_CB_Hist, 0) Rebate_Amt_CB_Hist
	, Isnull(Rebate_Amt_BUY_Hist, 0) Rebate_Amt_BUY_Hist
	, Margin_Amt
	, Margin_Cost_Basis_Amt
	, Cast(Case when isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) is not null then
			isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) - [Margin_Cost_Basis_Effective_Date]
		else 0 end as Numeric(10, 0)) 'Days_until_cost_change'

	, Cast(Case when isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) is not null and ([Contracted_Next_Cust_Change_Date_2] is null 
			or [Contracted_Next_Cust_Change_Date_2] = isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2])) then
			isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) - [Margin_Cost_Basis_Effective_Date]

		when isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) is not null and [Contracted_Next_Cust_Change_Date_2] is not null 
			and Cast([Margin_Cost_Basis_Effective_Date] + @projectiondays as datetime) > [Contracted_Next_Cust_Change_Date_2] then
			[Contracted_Next_Cust_Change_Date_2] - isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2])
		when isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) is not null and [Contracted_Next_Cust_Change_Date_2] is not null 
			and Cast([Margin_Cost_Basis_Effective_Date] + @projectiondays as datetime) <= [Contracted_Next_Cust_Change_Date_2] then
			@projectiondays - (isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) - [Margin_Cost_Basis_Effective_Date])
		else 0 end as numeric(18, 0)) 'Days_until_Price_Change'

	, Case 
		When isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) = [Contracted_Next_Cust_Change_Date_2] 
			or isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) is not null and [Contracted_Next_Cust_Change_Date_2] is null then
			@ProjectionDays - Cast(isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) - [Margin_Cost_Basis_Effective_Date] as numeric(18, 0))
		when Cast([Margin_Cost_Basis_Effective_Date] + @projectiondays as datetime) <= [Contracted_Next_Cust_Change_Date_2] 
				Or isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) is null then 0
		else Cast((Cast([Margin_Cost_Basis_Effective_Date] + @projectiondays as datetime) 
		- isnull([Contracted_Next_Cust_Change_Date_2], isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]))) as Numeric(10, 0)) end 'Days_at_New_Price'

	, Margin_Cost_Basis_Effective_Date
	, new.[Margin_Cost_Basis_Effective_Date_2] 'Margin_Cost_Basis_Effective_Date_2'
	, new.[Margin_Cost_Basis_Amt_2]
	--, isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) 'Margin_Cost_Basis_Effective_Date_2'
	--, Case When @AssType = 1 then isnull(isnull(cur.[Margin_Cost_Basis_Amt_2], new.[Margin_Cost_Basis_Amt_2]), cur.[Margin_Cost_Basis_Amt]) end 'Margin_Cost_Basis_Amt_2'
	, Cast([Margin_Cost_Basis_Effective_Date] + @projectiondays as datetime) 'Assessment_End_Date'
	, Margin_Pct
	, Contracted_Next_Cust_Change_Date_2
	, cur.Sell_Price_Cust_Amt 'Sell_Price_Cust_Amt_Cur'
	--, Cast(
	--	Case When @AssType = 1 then 
	--		Case When @PriceMethod = 1 then isnull(New.[sell_price_cust_amt_new], cur.[sell_price_cust_amt])
	--		When @PriceMethod = 2 and isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) is not null then
	--		isnull(cur.[Margin_Cost_Basis_Amt_2], new.[Margin_Cost_Basis_Amt_2]) / (1 - Margin_Pct) 
	--		else cur.[Sell_Price_Cust_Amt] end else 0 end as Numeric(18, 6)) 'Sell_Price_Cust_Amt_New'


	, Cast(
		Case When @PriceMethod = 1 then New.[sell_price_cust_amt_new]
			When @AssType = 1 and @PriceMethod = 2 and isnull(cur.[Margin_Cost_Basis_Effective_Date_2], new.[Margin_Cost_Basis_Effective_Date_2]) is not null then
			isnull(cur.[Margin_Cost_Basis_Amt_2], new.[Margin_Cost_Basis_Amt_2]) / (1 - Margin_Pct) 
			else cur.[Sell_Price_Cust_Amt] end as Numeric(18, 6)) 'Sell_Price_Cust_Amt_New'

	, isnull(Rebate_CB_Amt, 0) 'Rebate_CB_Amt'
	, isnull(Rebate_Buy_Amt, 0) 'Rebate_Buy_Amt'
	, ISNULL(New.Sell_Price_Cust_Amt_New_AT1, 0) 'Sell_Price_Cust_Amt_New_AT1'
	, New.Margin_Cost_Basis_Effective_Date_AT1
	, GroupingCustom
	, GroupingCustomDescription
	, [Product_ID]
	, Prod_Description
	, CustomerID_BT
	, CustomerID_BT_Name
	, CustomerID_ST
	, CustomerID_ST_Name
	, Whse
	, Whse_Record
	, SalesRep_Record_In
	, SalesRep_Record_Out
	, [Cust_Group1]
	, PROD_GROUP1
	, PROD_GROUP2
	, PROD_GROUP3
	, PROD_GROUP4
	, PROD_GROUP5
	, Sell_Price_Cust_Operation
	, Rebate_CB_Operation
	, Rebate_BUY_Operation
	, Sell_Price_Cust_Filter_Name
	, Sell_Price_Cust_Filter_Value
	, Sell_Price_Prod_Filter_Name
	, Sell_Price_Prod_Filter_Value
	, [Org_Vendor_ID]
	, [Org Vendor Name]
	, Price_Rule_Type
	, Rebate_CB_Rule_Type
	, Rebate_BUY_Rule_Type
	, Job_FK
	, cur.Job_FK_i
	, [Last Override Invoice Date]
	, [last Override Price]
	, [Last Override Cost]
	, [Override Qty]
	, [Override Transactions]
	, [Total Transactions]
	, cur.Sell_Price_Cust_Rules_FK
	, Rebate_CB_Rules_FK
	, Rebate_BUY_Rules_FK
	, Contract_No_Price
	, Contract_No_Rebate_CB
	, Contract_No_Rebate_Buy
	, Product_Structure_FK
	, Org_Entity_Structure_FK
	, Cust_Entity_Structure_FK
	, Supl_Entity_Structure_FK
	, Sales_Rep_Inside_ID
	, Sales_Rep_Inside_Name
	, Sales_Rep_Outside_ID
	, Sales_Rep_Outside_Name
	, Order_Taken_By
	, cur.Identifier
	, Qty_Break_Rule_Ind
	, Replacement_Cost
	, Standard_Cost
	, Rebate_Vendor_Incentive_Percent
	, Rebate_Incentive_Amt_Hist
	, Rebate_Incentive_Amt_Cur
	, Rebate_Incentive_Amt_New
	, Case When @AssType = 1 and @PriceMethod = 1 then isnull(ISNULL(New.Rebate_CB_Amt_New, cur.Rebate_CB_Amt), 0) else ISNULL(New.Rebate_CB_Amt_New, 0) end 'Rebate_CB_Amt_New'
	, Case When @AssType = 1 and @PriceMethod = 1 then isnull(ISNULL(New.Rebate_BUY_Amt_New, cur.Rebate_BUY_Amt), 0) else ISNULL(New.Rebate_BUY_Amt_New, 0) end 'Rebate_BUY_Amt_New'
	, Sell_Price_Cust_Host_Rule_Xref
	, Rebate_CB_Host_Rule_Xref
	, Rebate_BUY_Host_Rule_Xref
	, Sell_Price_Override_Hist_Avg
	, Unit_Cost_Of_Goods_Override_Hist_Avg
	, Cust_Group2 --Cust Price type
	, [What_If_Reference_ID]
	, Qty_Mode
	, Qty_Median
	, Qty_Mean
	, Sell_Price_Cust_Number_1
	, Sell_Price_Cust_Basis_1_Col
	, price_operand_pos
	, discount_operand_pos
	, Base_Price
	, List_Price
	, Qty_Q1
	, Qty_Q2
	, Qty_Q3
	, Qty_Q4
	, Sales_Q1
	, Sales_Q2
	, Sales_Q3
	, Sales_Q4
	, Cost_Net_Q1
	, Cost_Net_Q2
	, Cost_Net_Q3
	, Cost_Net_Q4
	, Date_Range_Q1
	, Date_Range_Q2
	, Date_Range_Q3
	, Date_Range_Q4
	, Sell_Price_Cust_Host_Rule_Xref_Hist
	, Rebate_CB_Host_Rule_Xref_Hist
	, Rebate_BUY_Host_Rule_Xref_Hist
	, Cust_Group3 --[SX Cust Rebate Type]
	, cur.CONTRACT_NO_HIST_LAST_PRICE
	, cur.CONTRACT_NO_HIST_LAST_REBATE_CB
	, cur.Sell_Price_Rebate_Cap Sell_Price_Rebate_Cap_Cur
	, new.Sell_Price_Rebate_Cap Sell_Price_Rebate_Cap_New
	, cur.spc_uom
	, new.sell_price_cust_rules_fk sell_price_cust_rules_fk_new
	, cur.sell_price_cust_effective_date sell_price_cust_effective_date_cur
	, new.sell_price_cust_effective_date sell_price_cust_effective_date_new
	, cur.sell_price_cust_end_date sell_price_cust_end_date_cur
	, new.sell_price_cust_end_date sell_price_cust_end_date_new
	into ##Calcs1
	from
	dbo.MAUI_BASIS Cur with (nolock) 
		Left Join ##Basis_New New
		on cur.Identifier = new.Identifier and cur.Job_FK_i = new.Job_FK_i
	where cur.Job_FK_i = @Job_FK_i and cur.calc_status = 'Current'
	and cur.sell_price_cust_amt <> 0 and cur.identifier is not null

	IF object_id('tempdb..##Basis_New') is not null
	Drop Table ##Basis_New

	insert into dbo.maui_log Select @Job_FK_i, 'MAUI Basis Calcs-##Calcs1 Loaded', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI Basis Calcs-##Calcs1'), 108)), Null
	insert into dbo.maui_log Select @Job_FK_i, 'Start Loading MAUI Basis Calcs-##Calcs2', GetDate(), Null, Null

	Select
	Sales_Qty
	, Sales_Qty_Daily
	,CONVERT(VARCHAR(8), [Sales_History_Start_Date], 1) 'Sales_History_Start'
	,CONVERT(VARCHAR(8), [Sales_History_End_Date], 1) 'Sales_History_End'
	, [Sales_History_Start_Date]
	, [Sales_History_End_Date]
	, Margin_Cost_Basis_Amt
	, Margin_Cost_Basis_Amt_2
	, Sell_Price_Cust_Amt_Cur
	, Sell_Price_Cust_Amt_New
	, Margin_Amt
	, Margin_Pct
	, Sales_Ext_Hist
	, Prod_Cost_Ext_Hist
	, Rebate_Amt_CB_Hist
	, Rebate_Amt_BUY_Hist
	, Margin_Cost_Basis_Effective_Date
	, Margin_Cost_Basis_Effective_Date_2
	, Contracted_Next_Cust_Change_Date_2
	, [Days_until_cost_change]
	, [Days_until_Price_Change]
	, [Days_at_New_Price]
	, Assessment_End_Date
	, Rebate_CB_Amt
	, Rebate_BUY_Amt
	, Rebate_CB_Amt_New
	, Rebate_BUY_Amt_New
	, Cast(Case [Days_until_cost_change]
		When 0 then Round(@projectiondays * Sales_Qty_Daily, 0) * Margin_Cost_Basis_Amt
		else Round([Days_until_cost_change] * Sales_Qty_Daily, 0) * Margin_Cost_Basis_Amt end as Numeric(18, 6)) 'Cost_Dollars_Cost_Cur'

	, Cast(Case When [Days_until_Price_Change] > 0 then
			Round([Days_until_Price_Change] * Sales_Qty_Daily, 0) * Margin_Cost_Basis_Amt_2 
			When [Days_until_cost_change] = 0 then 0
			else Round((@projectiondays - [Days_until_cost_change]) * Sales_Qty_Daily, 0) * Margin_Cost_Basis_Amt_2 
		end as Numeric(18, 6)) 'Cost_Dollars_Cost_Change'
	, Cast(Case When [Days_at_New_Price] = 0 then 0
		When Contracted_Next_Cust_Change_Date_2 is null AND
	  Cast(Case when [Margin_Cost_Basis_Effective_Date_2] is not null then
			[Margin_Cost_Basis_Effective_Date_2] - [Margin_Cost_Basis_Effective_Date]
		else 0 end as Numeric(10, 0)) 

		<>

	  Cast(Case when [Margin_Cost_Basis_Effective_Date_2] is not null and ([Contracted_Next_Cust_Change_Date_2] is null 
			or [Contracted_Next_Cust_Change_Date_2] = [Margin_Cost_Basis_Effective_Date_2]) then
			[Margin_Cost_Basis_Effective_Date_2] - [Margin_Cost_Basis_Effective_Date]

		when [Margin_Cost_Basis_Effective_Date_2] is not null and [Contracted_Next_Cust_Change_Date_2] is not null 
			and Cast([Margin_Cost_Basis_Effective_Date] + @projectiondays as datetime) > [Contracted_Next_Cust_Change_Date_2] then
			[Contracted_Next_Cust_Change_Date_2] - [Margin_Cost_Basis_Effective_Date_2]
		when [Margin_Cost_Basis_Effective_Date_2] is not null and [Contracted_Next_Cust_Change_Date_2] is not null 
			and Cast([Margin_Cost_Basis_Effective_Date] + @projectiondays as datetime) <= [Contracted_Next_Cust_Change_Date_2] then
			@projectiondays - ([Margin_Cost_Basis_Effective_Date_2] - [Margin_Cost_Basis_Effective_Date])
		else 0 end as numeric(18, 0))
	  then 0
		else Round([Days_at_New_Price] * Sales_Qty_Daily, 0) * Margin_Cost_Basis_Amt_2 end as Numeric(18, 6)) 'Cost_Dollars_Price_Change'

	, Cast(Case [Days_until_cost_change]
		When 0 then Round(@ProjectionDays * Sales_Qty_Daily, 0) * [Sell_Price_Cust_Amt_Cur]
		else Round([Days_until_cost_change] * Sales_Qty_Daily, 0) * [Sell_Price_Cust_Amt_Cur] end as Numeric(18, 6)) 'Sales_Dollars_Price_Cur'
	, Cast(Case [Days_until_Price_Change]
		When 0 then 0 --Round((@projectiondays - [Days_until_cost_change]) * Sales_Qty_Daily, 0) * [Sell_Price_Cust_Amt_cur]
		else Round([Days_until_Price_Change] * Sales_Qty_Daily, 0) * Sell_Price_Cust_Amt_cur end as Numeric(18, 6)) 'Sales_Dollars_Price_Hold'
	, Cast(Case [Days_at_New_Price]
		When 0 then 0
		else Round([Days_at_New_Price] * Sales_Qty_Daily, 0) * Sell_Price_Cust_Amt_New end as Numeric(18, 6)) 'Sales_Dollars_Price_New'
	, GroupingCustom
	, GroupingCustomDescription
	, [Product_ID]
	, Prod_Description
	, CustomerID_BT
	, CustomerID_BT_Name
	, CustomerID_ST
	, CustomerID_ST_Name
	, Whse
	, Whse_Record
	, SalesRep_Record_In
	, SalesRep_Record_Out
	, Cust_Group1 --[SX Cust Type]
	, Cust_Group2 --[SX CUST PRICE TYPE]
	, Cust_Group3 --[SX Cust Rebate Type]
	, Prod_Group1 --[SX Product Category]
	, Prod_Group2 --[SX Product Line]
	, Prod_Group3 --[SX Price Type]
	, Prod_Group4 --[SX Rebate Type]
	, Prod_Group5 --[SX Rebate SubType]
	, Sell_Price_Cust_Filter_Name
	, Sell_Price_Cust_Filter_Value
	, Sell_Price_Prod_Filter_Name
	, Sell_Price_Prod_Filter_Value
	, [Org_Vendor_ID]
	, [Org Vendor Name]
	, Job_FK
	, Job_FK_i
	, Price_Rule_Type
	, Rebate_CB_Rule_Type
	, Rebate_BUY_Rule_Type
	, Product_Structure_FK
	, Org_Entity_Structure_FK
	, Cust_Entity_Structure_FK
	, Supl_Entity_Structure_FK
	, [Sell_Price_Cust_Amt_New_AT1]
	, [Margin_Cost_Basis_Effective_Date_AT1]
	, Sell_Price_Cust_Operation
	, Rebate_CB_Operation
	, Rebate_BUY_Operation
	, [Last Override Invoice Date]
	, [Last Override Cost]
	, [last Override Price]
	, [Override Qty]
	, [Override Transactions]
	, [Total Transactions]
	, Sell_Price_Cust_Rules_FK
	, Rebate_CB_Rules_FK
	, Rebate_BUY_Rules_FK
	, Contract_No_Price
	, Contract_No_Rebate_CB
	, Contract_No_Rebate_Buy
	, Sales_Rep_Inside_ID
	, Sales_Rep_Inside_Name
	, Sales_Rep_Outside_ID
	, Sales_Rep_Outside_Name
	, Order_Taken_By
	, Identifier
	, Qty_Break_Rule_Ind
	, Replacement_Cost
	, Standard_Cost
	, Rebate_Vendor_Incentive_Percent
	, Rebate_Incentive_Amt_Hist
	, Rebate_Incentive_Amt_Cur
	, Rebate_Incentive_Amt_New
	, Sell_Price_Cust_Host_Rule_Xref
	, Rebate_CB_Host_Rule_Xref
	, Rebate_BUY_Host_Rule_Xref
	, Sell_Price_Override_Hist_Avg
	, Unit_Cost_Of_Goods_Override_Hist_Avg
	, [What_If_Reference_ID]
	, Qty_Mode
	, Qty_Median
	, Qty_Mean
	, Sell_Price_Cust_Number_1
	, Sell_Price_Cust_Basis_1_Col
	, price_operand_pos
	, discount_operand_pos
	, Base_Price
	, List_Price
	, Qty_Q1
	, Qty_Q2
	, Qty_Q3
	, Qty_Q4
	, Sales_Q1
	, Sales_Q2
	, Sales_Q3
	, Sales_Q4
	, Cost_Net_Q1
	, Cost_Net_Q2
	, Cost_Net_Q3
	, Cost_Net_Q4
	, Date_Range_Q1
	, Date_Range_Q2
	, Date_Range_Q3
	, Date_Range_Q4
	, Sell_Price_Cust_Host_Rule_Xref_Hist
	, Rebate_CB_Host_Rule_Xref_Hist
	, Rebate_BUY_Host_Rule_Xref_Hist
	, CONTRACT_NO_HIST_LAST_PRICE
	, CONTRACT_NO_HIST_LAST_REBATE_CB
	, Sell_Price_Rebate_Cap_Cur
	, Sell_Price_Rebate_Cap_New
	, spc_uom
	, sell_price_cust_rules_fk_new
	, sell_price_cust_effective_date_cur
	, sell_price_cust_effective_date_new
	, sell_price_cust_end_date_cur
	, sell_price_cust_end_date_new
	into ##Calcs2 from ##Calcs1

	Create index idx_orgs on ##Calcs2(Org_Entity_Structure_FK)

	IF object_id('tempdb..##Calcs1') is not null
	Drop table ##Calcs1

	insert into dbo.maui_log Select @Job_FK_i, 'MAUI Basis Calcs-##Calcs2 Loaded', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI Basis Calcs-##Calcs2'), 108)), Null
	insert into dbo.maui_log Select @Job_FK_i, 'Start Loading MAUI Basis Calcs-Final Step', GetDate(), Null, Null

	select job_fk_i Job_fk_i2, identifier identifier2, count(*) recs 
	into ##Calcs3 
	from ##calcs2 
	group by job_fk_i, identifier
	having COUNT(*) = 1

	Create Index idx_mbc on ##calcs3(job_fk_i2, identifier2)

	Insert into dbo.MAUI_Basis_Calcs
	Select Job_FK, Job_FK_i
	, Cast(Sales_History_Start_Date as Varchar) + ' - ' + Cast(Sales_History_End_Date as Varchar) 'Sales_History_Date_Range'
	, Sales_Qty
	, Sales_Qty_Daily
	, Sales_Ext_Hist / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Sales_Dollars_Hist'
	, Prod_Cost_Ext_Hist / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Prod_Cost_Dollars_Hist'
	, Rebate_Amt_CB_Hist / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Rebate_CB_Dollars_Hist'
	, Rebate_Amt_BUY_Hist / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Rebate_Buy_Dollars_Hist'
	, Rebate_Incentive_Amt_Hist / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Rebate_Incentive_Dollars_Hist'

	, (Prod_Cost_Ext_Hist - Rebate_Amt_CB_Hist) / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Prod_Cost_Net_CB_Dollars_Hist'
	, (Prod_Cost_Ext_Hist - Rebate_Amt_BUY_Hist)  / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Prod_Cost_Net_BUY_Dollars_Hist'
	, (Prod_Cost_Ext_Hist - Rebate_Incentive_Amt_Hist)  / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Prod_Cost_Net_Incentive_Dollars_Hist'
	, (Prod_Cost_Ext_Hist - Rebate_Amt_CB_Hist - Rebate_Amt_BUY_Hist - Rebate_Incentive_Amt_Hist)  / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Prod_Cost_Net_CB_BUY_Incentive_Dollars_Hist'

	, (Sales_Ext_Hist - Prod_Cost_Ext_Hist) / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Margin_Dollars_Hist'
	, (Sales_Ext_Hist - Prod_Cost_Ext_Hist + Rebate_Amt_CB_Hist) / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Margin_Net_CB_Dollars_Hist'
	, (Sales_Ext_Hist - Prod_Cost_Ext_Hist + Rebate_Amt_BUY_Hist)  / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Margin_Net_BUY_Dollars_Hist'
	, (Sales_Ext_Hist - Prod_Cost_Ext_Hist + Rebate_Incentive_Amt_Hist) / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Margin_Net_Incentive_Dollars_Hist'
	, (Sales_Ext_Hist - Prod_Cost_Ext_Hist + Rebate_Amt_CB_Hist + Rebate_Amt_BUY_Hist + Rebate_Incentive_Amt_Hist)  / Case @UseFileDates When 0 then @HistoryDays / @ProjectionDays else 1 end 'Margin_Net_CB_BUY_Incentive_Dollars_Hist'

	, Sell_Price_Cust_Amt_Cur * Case @UseFileDates When 0 then Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end else [Sales_Qty] end 'Sales_Dollars_Cur'
	, Margin_Cost_Basis_Amt * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Cost_Dollars_Cur'
	, (Margin_Cost_Basis_Amt - Rebate_CB_Amt) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Cost_Net_CB_Dollars_Cur'
	, (Margin_Cost_Basis_Amt - Rebate_BUY_Amt) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Cost_Net_BUY_Dollars_Cur' --****
	, (Margin_Cost_Basis_Amt - Rebate_Incentive_Amt_Cur) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Cost_Net_Incentive_Dollars_Cur' --****
	, (Margin_Cost_Basis_Amt - Rebate_CB_Amt - Rebate_BUY_Amt - Rebate_Incentive_Amt_Cur) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Cost_Net_CB_BUY_Incentive_Dollars_Cur'

	, Margin_Amt * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Margin_Dollars_Cur'
	, (Margin_Amt + Rebate_CB_Amt) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Margin_Net_CB_Dollars_Cur'
	, (Margin_Amt + Rebate_BUY_Amt) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Margin_Net_BUY_Dollars_Cur' --****
	, (Margin_Amt + Rebate_Incentive_Amt_Cur) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Margin_Net_Incentive_Dollars_Cur'
	, (Margin_Amt + Rebate_CB_Amt + Rebate_BUY_Amt + Rebate_Incentive_Amt_Cur) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Margin_Net_CB_Buy_Incentive_Dollars_Cur'

	, Case When isnull([Margin_Cost_Basis_Amt_2], 0) = 0 then
		Sell_Price_Cust_Amt_Cur * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end
		else (Sales_Dollars_Price_Cur + Sales_Dollars_Price_Hold + Sales_Dollars_Price_New) end 'Sales_Dollars_New'
	, Case When isnull([Margin_Cost_Basis_Amt_2], 0) = 0 then
		Margin_Cost_Basis_Amt * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end
		else (Cost_Dollars_Cost_Cur + Cost_Dollars_Cost_Change + Cost_Dollars_Price_Change) end 'Cost_Dollars_New'
	, Case When isnull([Margin_Cost_Basis_Amt_2], 0) = 0 then
		(Margin_Cost_Basis_Amt - Rebate_CB_Amt) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end
		else((Cost_Dollars_Cost_Cur + Cost_Dollars_Cost_Change + Cost_Dollars_Price_Change) 
		- (Rebate_CB_Amt * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end)) end 'Cost_Net_CB_Dollars_New'
	, Case When isnull([Margin_Cost_Basis_Amt_2], 0) = 0 then
		(Margin_Cost_Basis_Amt - Rebate_BUY_Amt) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end
		else((Cost_Dollars_Cost_Cur + Cost_Dollars_Cost_Change + Cost_Dollars_Price_Change) 
		- (Rebate_BUY_Amt * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end)) end 'Cost_Net_BUY_Dollars_New' --****
	, Case When isnull([Margin_Cost_Basis_Amt_2], 0) = 0 then
		(Margin_Cost_Basis_Amt -  Rebate_Incentive_Amt_Cur) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end
		else((Cost_Dollars_Cost_Cur + Cost_Dollars_Cost_Change + Cost_Dollars_Price_Change) 
		- (Rebate_Incentive_Amt_New * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end)) end 'Cost_Net_Incentive_Dollars_New' --****
	, Case When isnull([Margin_Cost_Basis_Amt_2], 0) = 0 then
		(Margin_Cost_Basis_Amt - Rebate_CB_Amt - Rebate_BUY_Amt - Rebate_Incentive_Amt_Cur) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end
		else((Cost_Dollars_Cost_Cur + Cost_Dollars_Cost_Change + Cost_Dollars_Price_Change) 
		- ((Rebate_CB_Amt + Rebate_BUY_Amt + Rebate_Incentive_Amt_New) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end)) end 'Cost_Net_CB_BUY_Incentive_Dollars_New'
	, Case When isnull([Margin_Cost_Basis_Amt_2], 0) = 0 then
		Margin_Amt * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end
		else ((Sales_Dollars_Price_Cur + Sales_Dollars_Price_Hold + Sales_Dollars_Price_New)
		 - (Cost_Dollars_Cost_Cur + Cost_Dollars_Cost_Change + Cost_Dollars_Price_Change)) end 'Margin_Dollars_New'
	, Case When isnull([Margin_Cost_Basis_Amt_2], 0) = 0 then
		(Margin_Amt + Rebate_CB_Amt) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 
		else ((Sales_Dollars_Price_Cur + Sales_Dollars_Price_Hold + Sales_Dollars_Price_New)
		 - ((Cost_Dollars_Cost_Cur + Cost_Dollars_Cost_Change + Cost_Dollars_Price_Change)
		 - (Rebate_CB_Amt * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end))) end 'Margin_Net_CB_Dollars_New'

	, Case When isnull([Margin_Cost_Basis_Amt_2], 0) = 0 then
		(Margin_Amt + Rebate_BUY_Amt) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 
		else ((Sales_Dollars_Price_Cur + Sales_Dollars_Price_Hold + Sales_Dollars_Price_New)
		 - ((Cost_Dollars_Cost_Cur + Cost_Dollars_Cost_Change + Cost_Dollars_Price_Change)
		 - (Rebate_BUY_Amt * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end))) end 'Margin_Net_BUY_Dollars_New' --****

	, Case When isnull([Margin_Cost_Basis_Amt_2], 0) = 0 then
		(Margin_Amt + Rebate_Incentive_Amt_Cur) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 
		else ((Sales_Dollars_Price_Cur + Sales_Dollars_Price_Hold + Sales_Dollars_Price_New)
		 - ((Cost_Dollars_Cost_Cur + Cost_Dollars_Cost_Change + Cost_Dollars_Price_Change)
		 - (Rebate_Incentive_Amt_New * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end))) end 'Margin_Net_Incentive_Dollars_New' --****

	, Case When isnull([Margin_Cost_Basis_Amt_2], 0) = 0 then
		(Margin_Amt + Rebate_CB_Amt + Rebate_BUY_Amt + Rebate_Incentive_Amt_Cur) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end
		else ((Sales_Dollars_Price_Cur + Sales_Dollars_Price_Hold + Sales_Dollars_Price_New)
		 - ((Cost_Dollars_Cost_Cur + Cost_Dollars_Cost_Change + Cost_Dollars_Price_Change)
		 - ((Rebate_CB_Amt + Rebate_BUY_Amt + Rebate_Incentive_Amt_New) * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end))) end 'Margin_Net_CB_BUY_Incentive_Dollars_New'
	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round([Days_until_Price_Change] * Sales_Qty_Daily, 0) * (Margin_Cost_Basis_Amt - Margin_Cost_Basis_Amt_2) end 'Cost_Impact_Dollars_Until_Price_Change'
	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round([Days_at_New_Price] * Sales_Qty_Daily, 0) * (Margin_Cost_Basis_Amt - Margin_Cost_Basis_Amt_2) end 'Cost_Impact_Dollars_At_New_Price'
	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round([Days_at_New_Price] * Sales_Qty_Daily, 0) * (Sell_Price_Cust_Amt_New - Sell_Price_Cust_Amt_cur) end 'Price_Recovery_Dollars'
	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round(([Days_until_Price_Change] + [Days_at_New_Price]) * Sales_Qty_Daily, 0) * (Margin_Cost_Basis_Amt - Margin_Cost_Basis_Amt_2) +
		Round([Days_at_New_Price] * Sales_Qty_Daily, 0) * (Sell_Price_Cust_Amt_New - Sell_Price_Cust_Amt_cur) end 'Total_Impact_Dollars'
	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round([Days_until_Price_Change] * Sales_Qty_Daily, 0) 
		* ((Margin_Cost_Basis_Amt - Rebate_CB_Amt) - (Margin_Cost_Basis_Amt_2 - Rebate_CB_Amt_New)) end 'Cost_Net_CB_Impact_Dollars_Until_Price_Change'

	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round([Days_until_Price_Change] * Sales_Qty_Daily, 0) 
		* ((Margin_Cost_Basis_Amt - Rebate_BUY_Amt) - (Margin_Cost_Basis_Amt_2 - Rebate_BUY_Amt_New)) end 'Cost_Net_BUY_Impact_Dollars_Until_Price_Change'
	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round([Days_until_Price_Change] * Sales_Qty_Daily, 0) 
		* ((Margin_Cost_Basis_Amt - Rebate_Incentive_Amt_Cur) - (Margin_Cost_Basis_Amt_2 - Rebate_Incentive_Amt_New)) end 'Cost_Net_Incentive_Impact_Dollars_Until_Price_Change'
	-- ****
	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round([Days_until_Price_Change] * Sales_Qty_Daily, 0) 
		* ((Margin_Cost_Basis_Amt - Rebate_CB_Amt - Rebate_BUY_Amt - Rebate_Incentive_Amt_Cur) 
		- (Margin_Cost_Basis_Amt_2 - Rebate_CB_Amt_New - Rebate_Buy_Amt_New - Rebate_Incentive_Amt_New)) end 'Cost_Net_CB_BUY_Incentive_Impact_Dollars_Until_Price_Change'
	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round(([Days_until_Price_Change] + [Days_at_New_Price]) * Sales_Qty_Daily, 0) 
		* ((Margin_Cost_Basis_Amt - Rebate_CB_Amt) - (Margin_Cost_Basis_Amt_2 - Rebate_CB_Amt_New)) end 'Cost_Net_CB_Impact_Dollars_At_New_Price'

	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round(([Days_until_Price_Change] + [Days_at_New_Price]) * Sales_Qty_Daily, 0) 
		* ((Margin_Cost_Basis_Amt - Rebate_BUY_Amt) - (Margin_Cost_Basis_Amt_2 - Rebate_BUY_Amt_New)) end 'Cost_Net_BUY_Impact_Dollars_At_New_Price'
	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round(([Days_until_Price_Change] + [Days_at_New_Price]) * Sales_Qty_Daily, 0) 
		* ((Margin_Cost_Basis_Amt - Rebate_Incentive_Amt_Cur) - (Margin_Cost_Basis_Amt_2 - Rebate_Incentive_Amt_New)) end 'Cost_Net_Incentive_Impact_Dollars_At_New_Price'
	--****
	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round(([Days_until_Price_Change] + [Days_at_New_Price]) * Sales_Qty_Daily, 0) 
		* ((Margin_Cost_Basis_Amt - Rebate_CB_Amt - Rebate_BUY_Amt - Rebate_Incentive_Amt_Cur) 
		- (Margin_Cost_Basis_Amt_2 - Rebate_CB_Amt_New - Rebate_Buy_Amt_New - Rebate_Incentive_Amt_New)) end 'Cost_Net_CB_BUY_Incentive_Impact_Dollars_At_New_Price'

	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round([Days_until_Price_Change] + [Days_at_new_price] * Sales_Qty_Daily, 0) 
		* ((Margin_Cost_Basis_Amt - Rebate_CB_Amt) - (Margin_Cost_Basis_Amt_2 - Rebate_CB_Amt_New)) +
		Round([Days_at_New_Price] * Sales_Qty_Daily, 0) * (Sell_Price_Cust_Amt_New - Sell_Price_Cust_Amt_cur) end 'Total_Net_CB_Impact_Dollars'

	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round([Days_until_Price_Change] + [Days_at_new_price] * Sales_Qty_Daily, 0) 
		* ((Margin_Cost_Basis_Amt - Rebate_BUY_Amt) - (Margin_Cost_Basis_Amt_2 - Rebate_BUY_Amt_New)) +
		Round([Days_at_New_Price] * Sales_Qty_Daily, 0) * (Sell_Price_Cust_Amt_New - Sell_Price_Cust_Amt_cur) end 'Total_Net_BUY_Impact_Dollars'
	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round([Days_until_Price_Change] + [Days_at_new_price] * Sales_Qty_Daily, 0) 
		* ((Margin_Cost_Basis_Amt - Rebate_Incentive_Amt_Cur) - (Margin_Cost_Basis_Amt_2 - Rebate_Incentive_Amt_New)) +
		Round([Days_at_New_Price] * Sales_Qty_Daily, 0) * (Sell_Price_Cust_Amt_New - Sell_Price_Cust_Amt_New) end 'Total_Net_Incentive_Impact_Dollars'
	--****
	, Case When [Contracted_Next_Cust_Change_Date_2] is null then 0 else
		Round(([Days_until_Price_Change] + [Days_at_New_Price]) * Sales_Qty_Daily, 0) 
		* ((Margin_Cost_Basis_Amt - Rebate_CB_Amt - Rebate_BUY_Amt - Rebate_Incentive_Amt_Cur) 
		- (Margin_Cost_Basis_Amt_2 - Rebate_CB_Amt_New - Rebate_Buy_Amt_New - Rebate_Incentive_Amt_New)) +
		Round([Days_at_New_Price] * Sales_Qty_Daily, 0) * (Sell_Price_Cust_Amt_New - Sell_Price_Cust_Amt_cur) end 'Total_Net_CB_BUY_Incentive_Impact_Dollars'
	, Rebate_CB_Amt * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Rebate_CB_Dollars_Cur'
	, Rebate_BUY_Amt * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Rebate_BUY_Dollars_Cur'
	, Rebate_CB_Amt_New * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Rebate_CB_Dollars_New'
	, Rebate_Buy_Amt_New * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Rebate_BUY_Dollars_New'
	, Rebate_Incentive_Amt_Cur * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Rebate_Incentive_Dollars_Cur'
	, Rebate_Incentive_Amt_New * Case @UseFileDates When 0 then Sales_Qty_Daily * @ProjectionDays else [Sales_Qty] end 'Rebate_Incentive_Dollars_New'
	, Margin_Cost_Basis_Amt
	, Margin_Cost_Basis_Amt_2
	, Sell_Price_Cust_Amt_Cur
	, Sell_Price_Cust_Amt_New
	, Margin_Amt
	, Margin_Pct
	, [Sales_History_End_Date]
	, [Sales_History_Start_Date]
	, Sales_Ext_Hist
	, Prod_Cost_Ext_Hist
	, Rebate_Amt_CB_Hist
	, Rebate_Amt_BUY_Hist
	, Margin_Cost_Basis_Effective_Date
	, Margin_Cost_Basis_Effective_Date_2
	, Contracted_Next_Cust_Change_Date_2
	, [Days_until_cost_change]
	, [Days_until_Price_Change]
	, [Days_at_New_Price]
	, Assessment_End_Date
	, Rebate_CB_Amt
	, Rebate_BUY_Amt
	, Sell_Price_Rebate_Cap_Cur
	, Rebate_CB_Amt_New
	, Rebate_Buy_Amt_New
	, Sell_Price_Rebate_Cap_New
	, Cost_Dollars_Cost_Cur
	, Cost_Dollars_Cost_Change
	, Cost_Dollars_Price_Change
	, Sales_Dollars_Price_Cur
	, Sales_Dollars_Price_Hold
	, Sales_Dollars_Price_New
	, GroupingCustom
	, GroupingCustomDescription
	, [Product_ID]
	, Prod_Description
	, CustomerID_BT
	, CustomerID_BT_Name
	, CustomerID_ST
	, CustomerID_ST_Name
	, SalesRep_Record_In
	, SalesRep_Record_Out
	, Whse_Record
	, Whse
	, (Select Value from epacube.ENTITY_IDENT_NONUNIQUE EIN where EIN.entity_Data_Name_FK = 141000 and EIN.Data_Name_FK = 141112 and EIN.entity_Structure_FK = Org_Entity_Structure_FK) 'Whse Name'
	, Cust_Group1 --[SX Cust Type]
	, Cust_Group2 --[SX CUST PRICE TYPE]
	, Cust_Group3 --[SX CUST REBATE TYPE]
	, Prod_Group1 --[SX Product Category]
	, Prod_Group2 --[SX Product Line]
	, Prod_Group3 --[SX Price Type]
	, Prod_Group4 --[SX Rebate Type]
	, Prod_Group5 --[SX Rebate SubType]
	, Sell_Price_Cust_Filter_Name
	, Sell_Price_Cust_Filter_Value
	, Sell_Price_Prod_Filter_Name
	, Sell_Price_Prod_Filter_Value
	, [Org_Vendor_ID]
	, [Org Vendor Name]
	, Price_Rule_Type
	, Rebate_CB_Rule_Type
	, Rebate_BUY_Rule_Type
	, Product_Structure_FK
	, Org_Entity_Structure_FK
	, Cust_Entity_Structure_FK
	, Supl_Entity_Structure_FK
	, [Sell_Price_Cust_Amt_New_AT1]
	, [Margin_Cost_Basis_Effective_Date_AT1]
	, Sell_Price_Cust_Operation
	, Rebate_CB_Operation
	, Rebate_BUY_Operation
	, [Last Override Invoice Date]
	, [last Override Price]
	, [Last Override Cost]
	, [Override Qty]
	, [Override Transactions]
	, [Total Transactions]
	, Sell_Price_Cust_Rules_FK
	, Rebate_CB_Rules_FK
	, Rebate_BUY_Rules_FK
	, Contract_No_Price
	, Contract_No_Rebate_CB
	, Contract_No_Rebate_Buy
	, Sales_Rep_Inside_ID
	, Sales_Rep_Inside_Name
	, Sales_Rep_Outside_ID
	, Sales_Rep_Outside_Name
	, Order_Taken_By
	, Qty_Break_Rule_Ind
	, Rebate_Vendor_Incentive_Percent
	, Rebate_Incentive_Amt_Hist
	, Rebate_Incentive_Amt_Cur
	, Rebate_Incentive_Amt_New
	, Sell_Price_Cust_Host_Rule_Xref
	, Rebate_CB_Host_Rule_Xref
	, Rebate_BUY_Host_Rule_Xref
	, Sell_Price_Cust_Number_1
	, Sell_Price_Cust_Basis_1_Col
	, price_operand_pos
	, discount_operand_pos
	, Sell_Price_Override_Hist_Avg
	, Unit_Cost_Of_Goods_Override_Hist_Avg

	, (Select Case	When [Override Qty] = 0 then 0
					When isnull(Unit_Cost_Of_Goods_Override_Hist_Avg, 0) = 0 then 0
					When isnull(Sell_Price_Override_Hist_Avg, 0) = 0 then 0
					Else Imp end from 
	(Select ((((Margin_Cost_Basis_Amt / 
									(1 - ((Sell_Price_Override_Hist_Avg - Unit_Cost_Of_Goods_Override_Hist_Avg) / 
									isnull(Sell_Price_Override_Hist_Avg, 1)))) - Margin_Cost_Basis_Amt) - [Margin_Amt])
	) Imp) Impact_Amt) 'OverRide Impact Amt'

	, (Select Case	When [Override Qty] = 0 then 0
					When isnull(Unit_Cost_Of_Goods_Override_Hist_Avg, 0) = 0 then 0
					When isnull(Sell_Price_Override_Hist_Avg, 0) = 0 then 0
					Else Imp_Future end from 
	(Select ((((Margin_Cost_Basis_Amt_2 / 
									(1 - ((Sell_Price_Override_Hist_Avg - Unit_Cost_Of_Goods_Override_Hist_Avg) / 
									isnull(Sell_Price_Override_Hist_Avg, 1)))) - Margin_Cost_Basis_Amt_2) - (Sell_Price_Cust_Amt_New - Margin_Cost_Basis_Amt_2))
	) Imp_Future) Impact_Amt_Future) 'OverRide Impact Amt Future'

	, [What_If_Reference_ID]
	, Qty_Mode
	, Qty_Median
	, Qty_Mean
	, Qty_Q1
	, Qty_Q2
	, Qty_Q3
	, Qty_Q4
	, Sales_Q1
	, Sales_Q2
	, Sales_Q3
	, Sales_Q4
	, Cost_Net_Q1
	, Cost_Net_Q2
	, Cost_Net_Q3
	, Cost_Net_Q4
	, Date_Range_Q1
	, Date_Range_Q2
	, Date_Range_Q3
	, Date_Range_Q4
	, Sell_Price_Cust_Host_Rule_Xref_Hist
	, Rebate_CB_Host_Rule_Xref_Hist
	, Rebate_BUY_Host_Rule_Xref_Hist
	, CONTRACT_NO_HIST_LAST_PRICE
	, CONTRACT_NO_HIST_LAST_REBATE_CB
	, Replacement_Cost
	, Standard_Cost
	, Base_Price
	, List_Price
	, spc_uom
	, sell_price_cust_rules_fk_new
	, sell_price_cust_effective_date_cur
	, sell_price_cust_effective_date_new
	, sell_price_cust_end_date_cur
	, sell_price_cust_end_date_new
	, Identifier
	from ##Calcs2 C2
	Inner join ##Calcs3 C3 on c2.job_fk_i = c3.job_fk_i2 and c2.identifier = c3.identifier2

	IF object_id('tempdb..##Calcs1') is not null
	Drop table ##Calcs1

	IF object_id('tempdb..##Calcs2') is not null
	Drop table ##Calcs2

	IF object_id('tempdb..##Calcs3') is not null
	Drop table ##Calcs3

	insert into dbo.maui_log Select @Job_FK_i, 'MAUI Basis Calcs Final Table Loaded', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI Basis Calcs-Final Step'), 108)), Null

	insert into dbo.maui_log Select @Job_FK_i, 'MAUI Basis Calcs Load Complete', GetDate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI Basis Calcs'), 108)), (select COUNT(*) from dbo.MAUI_Basis_Calcs where Job_FK_i = @Job_FK_i)

	EXEC import.LOAD_MAUI_4_RULES_SUMMARY @Job_FK, @Job_FK_i, @Defaults_To_Use

End
