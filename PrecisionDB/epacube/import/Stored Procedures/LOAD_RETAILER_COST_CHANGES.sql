﻿

-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        03/08/2018   Initial Version 
-- GHS		 08/28/2018	  Updated Unit Cost calculations on CWL (Constant Random Weight Items)
-- GHS		 01/18/2019	  Updated Unit Cost calculations on RWL (Random Weight) items to equal Case Cost, not Case Cost / pack.
-- GHS		 07/18/2019	  Added code to remove discontinued attribute for items with discontinued dates <= 4 years ago today.
-- GHS		 09/18/2019	  Rebuilt to utilize sur_tme from URM to uniquely identify rescind records
-- GHS		 09/24/2019	  Refined load to process in sequence from starting job of complete dump and each daily forward; More complete work with rescinded records
-- GHS-NH	 10/17/2019	  Incorporated new pack table - epacube.product_uom_pack - and added case pack to current unit pack when determining new records to keep
-- GHS		 01/30/2020	  refined immediate cost change input a couple weeks ago, making sure only the right new cost is put into place.
-- GHS		 05/15/2020	  IN:887 URM-1884 Enable Saturday cost declines to be included with immediate cost chagnes. FInished and demo'd on 5/21/20

CREATE PROCEDURE [import].[LOAD_RETAILER_COST_CHANGES] ( @JOB_FK bigint ) 
AS  

	DECLARE @l_exec_no          bigint;
	DECLARE @status_desc        varchar(max);
	DECLARE @v_input_rows       bigint;
	DECLARE @v_output_rows      bigint; 
	DECLARE @v_exception_rows		 bigint;
	DECLARE @l_sysdate			 DATETIME
	DECLARE @V_START_TIMESTAMP     DATETIME
	DECLARE @V_END_TIMESTAMP       DATETIME

--/*
	DECLARE @ls_exec            nvarchar (max);
	DECLARE @l_rows_processed   bigint;
	DECLARE @ls_table_name      varchar(max);
	DECLARE @time_before datetime;
	DECLARE @time_after datetime;
	DECLARE @elapsed_time numeric(10,4);
	DECLARE @rows_affected varchar(max);
	DECLARE @rows_dml bigint;
	DECLARE @ParmDefinition nvarchar(500);
	DECLARE @v_batch_no             bigint
	DECLARE @v_search_activity_fk   int     
	DECLARE @v_entity_class_cr_fk   int  
	DECLARE @v_column_name   varchar(25)
	DECLARE @v_primary   varchar(25)   
	DECLARE @V_UNIQUE1   varchar(25) 
	DECLARE @V_UNIQUE2   varchar(25) 
	DECLARE @V_UNIQUE3   varchar(25) 
	DECLARE @v_effective_date  datetime 
	DECLARE @v_import_filename varchar(128) 
	DECLARE @data_name_id_for_Product	bigint
--*/

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.LOAD_RETAILER COST CHANGES', 
												@exec_id = @l_exec_no OUTPUT;
--												@epa_job_id = @JOB_FK;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


---------------------------------------------------------------------------------------------------------------------------
--When dealing with a Full Load, truncate table. Full load is recognized as a single import with more than 100,000 records.
---------------------------------------------------------------------------------------------------------------------------

----Purge discontinued dates from attributes where dates are <= 4 years ago today.
	delete
	from epacube.product_attribute
	where DATA_NAME_FK = 500012
	and attribute_date <= dateadd(year, -4, getdate())

	--Purge discontinued dates where a new established date is more current
	delete d
	from epacube.PRODUCT_ATTRIBUTE e
	inner join epacube.PRODUCT_ATTRIBUTE d 
		on d.PRODUCT_STRUCTURE_FK = e.PRODUCT_STRUCTURE_FK and d.DATA_NAME_FK = 500012
	where e.DATA_NAME_FK = 500011
	and e.ATTRIBUTE_EVENT_DATA > d.ATTRIBUTE_EVENT_DATA

	If object_id('tempdb..#PBVpacks') is not null
	drop table #PBVpacks

	If object_id('tempdb..#pbv1') is not null
	drop table #pbv1

	truncate table marginmgr.pricesheet_basis_values_icc

/*
	Declare @Job_FK bigint, @dt date, @recs int
	set @Job_fk = (Select top 1 job_fk from [import].[URM_COST_CHANGES_RETAILER] with (nolock) group by job_fk having count(*) > 100000 order by job_fk desc)

	--truncate table marginmgr.pricesheet_basis_values

		DECLARE MbrCosts Cursor local for
			Select job_fk, cast(import_timestamp as date) dt, count(*) recs
			from [import].[URM_COST_CHANGES_RETAILER]
			where 1 = 1
			and job_fk >= @Job_fk
			group by job_fk, cast(import_timestamp as date)
			order by cast(import_timestamp as date) 

			OPEN MbrCosts;
			FETCH NEXT FROM MbrCosts INTO @Job_FK, @dt, @recs
			WHILE @@FETCH_STATUS = 0

			Begin--Fetch

			Print @Job_FK
*/
	--Declare @Job_FK bigint = 7628

	update uccr
	Set unique_cost_record_id = cast(item_surrogate as varchar(16)) + '-' + cast(effective_date as varchar(16)) + '-' + cast(SUR_TME as varchar(16))
	from [import].[URM_COST_CHANGES_RETAILER]  uccr
	where job_fk = @job_fk

	Declare @ZES_FK bigint
	Set @ZES_FK = (Select top 1 entity_structure_fk from epacube.epacube.entity_identification eiz with (nolock) where eiz.entity_data_name_fk = 151000 and eiz.data_name_fk = 151110 and eiz.[value] = '90001')

	If (select count(*) from [import].[URM_COST_CHANGES_RETAILER] where job_fk = @Job_FK) > 100000
	Begin
		truncate table marginmgr.pricesheet_basis_values
	End

	truncate table marginmgr.pricesheet_basis_values_unique

--	Load active records into temp perm table
	Insert into marginmgr.pricesheet_basis_values_unique
	(Data_Name_FK, Zone_Entity_Structure_FK, Item_Surrogate, Product_Structure_FK, sur_tme, unique_cost_record_id, Effective_Date, Effective_Date_Raw, Member_Price, rescind, rescind_timestamp, Update_Timestamp, Create_Timestamp, DTE_CRT, TME_CRT, USR_CRT, DTE_UPD, TME_UPD, USR_UPD, Job_FK, Import_Timestamp)
	select * from (
		Select 503201 'Data_Name_FK', Zone_Entity_Structure_FK, Item_Surrogate, Product_Structure_FK, SUR_TME
		, unique_cost_record_id
		, cast(epacube.[epacube].[URM_DATE_CONVERSION](effective_date) as date) 'Effective_Date'
		, effective_date 'Effective_Date_Raw'
		,  Cast(Member_Price / 100 as money) 'Member_Price'
		, Case isnull(rescind, 'N') when 'Y' then 1 else 0 end 'rescind'
		, Case isnull(rescind, 'N') when 'Y' then epacube.[epacube].[URM_DATE_TIME_CONVERSION](dte_upd, tme_upd) else Null end 'rescind_timestamp'
		, epacube.[epacube].[URM_DATE_TIME_CONVERSION](dte_upd, tme_upd) 'Update_Timestamp'
		, epacube.[epacube].[URM_DATE_TIME_CONVERSION](dte_crt, TME_CRT) 'Create_Timestamp'
		, DTE_CRT, TME_CRT, USR_CRT, DTE_UPD, TME_UPD, USR_UPD, JOB_FK
		, IMPORT_TIMESTAMP 
		from (
		Select @ZES_FK 'Zone_Entity_Structure_FK'
		, uccr.*, pi.PRODUCT_STRUCTURE_FK
		from [import].[URM_COST_CHANGES_RETAILER]  uccr
		inner join epacube.product_identification pi with (nolock) on uccr.ITEM_SURROGATE = pi.value and pi.DATA_NAME_FK = 110103
		where 1 = 1
		and uccr.job_fk = @Job_FK 
		and isnull(uccr.rescind, 'N') = 'N'
		) A 
	) B
	order by Item_Surrogate, effective_date desc, Create_Timestamp desc, Update_Timestamp desc, SUR_TME desc

	If object_id('tempdb..#PBVpacks') is not null
	drop table #PBVpacks

	Select * 
	into #PBVpacks 
	from (
			select pup.product_structure_fk, pup.pack, pup.EFFECTIVE_DATE
			, dense_rank()over(partition by pup.product_structure_fk order by pup.effective_date desc) DRank
			from epacube.PRODUCT_UOM_PACK pup
			inner join marginmgr.pricesheet_basis_values_unique pbvu on pup.PRODUCT_STRUCTURE_FK = pbvu.Product_Structure_FK
			and pup.effective_date <= pbvu.Effective_Date
		) A where DRank = 1

	Insert into #PBVpacks
	(Product_structure_fk, pack, effective_date, drank)
	select pup.product_structure_fk, pup.pack, pup.EFFECTIVE_DATE
	, dense_rank()over(partition by pup.product_structure_fk order by pup.effective_date) DRank
	from epacube.PRODUCT_UOM_PACK pup
	inner join marginmgr.pricesheet_basis_values_unique pbvu on pup.PRODUCT_STRUCTURE_FK = pbvu.Product_Structure_FK and pup.effective_date > pbvu.Effective_Date
	left join #PBVpacks pp on pup.product_structure_fk = pp.PRODUCT_STRUCTURE_FK
	where pp.PRODUCT_STRUCTURE_FK is null

	create clustered index idx_01 on #PBVpacks(product_structure_fk)

	Insert into marginmgr.pricesheet_basis_values
	(effective_date, Product_Structure_FK, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, data_name_fk, value, pack, CO_ENTITY_STRUCTURE_FK, RECORD_STATUS_CR_FK, job_fk, Unit_Value, Create_Timestamp, UPDATE_TIMESTAMP, CREATE_USER, UPDATE_USER
	, IMPORT_TIMESTAMP, unique_cost_record_id)
	Select distinct
	pbvu.effective_date, pbvu.Product_Structure_FK, 1 'Org_Entity_Structure_FK', pbvu.Zone_Entity_Structure_FK, pbvu.data_name_fk, pbvu.Member_Price, packs.pack 'PACK', 101 'CO_ENTITY_STRUCTURE_FK', 1 'RECORD_STATUS_CR_FK'
	, pbvu.job_fk
	, Case	when uc.uom_code = 'RWC'	then cast((Cast(pbvu.Member_Price as money) / ((isnull(puc.QUANTITY_MULTIPLE, 1) * cast(packs.pack as numeric(18, 6))) / 16)) as Numeric(18, 6))
				when uc.uom_code = 'RWL'	then Cast(pbvu.Member_Price as Numeric(18, 6))
										else Cast(pbvu.Member_Price / packs.PACK as Numeric(18, 6))
				end 'Unit_Value'
	, pbvu.Create_Timestamp, pbvu.UPDATE_TIMESTAMP, pbvu.usr_crt 'CREATE_USER', pbvu.usr_upd 'UPDATE_USER', pbvu.IMPORT_TIMESTAMP, pbvu.unique_cost_record_id
	from marginmgr.pricesheet_basis_values_unique pbvu
	left join marginmgr.pricesheet_basis_values pbv with (nolock) on pbvu.unique_cost_record_id = pbv.unique_cost_record_id
	inner join #PBVpacks packs on pbvu.Product_Structure_FK = packs.PRODUCT_STRUCTURE_FK
	left join epacube.product_uom_class puc with (nolock) on pbvu.product_structure_fk = puc.product_structure_fk and puc.data_name_fk = 500025
	left join epacube.uom_code uc with (nolock) on puc.uom_code_fk = uc.uom_code_id
	where pbv.Pricesheet_Basis_Values_ID is null

--	Add rescinded records to permanent temp table
	Insert into marginmgr.pricesheet_basis_values_unique
	(Data_Name_FK, Zone_Entity_Structure_FK, Item_Surrogate, Product_Structure_FK, sur_tme, unique_cost_record_id, Effective_Date, Effective_Date_Raw, Member_Price, rescind, rescind_timestamp, Update_Timestamp, Create_Timestamp, DTE_CRT, TME_CRT, USR_CRT, DTE_UPD, TME_UPD, USR_UPD, Job_FK, Import_Timestamp, Apply_Rescind_Flag)
	select * from (
		Select 503201 'Data_Name_FK', Zone_Entity_Structure_FK, Item_Surrogate, Product_Structure_FK, SUR_TME
		, unique_cost_record_id
		, cast(epacube.[epacube].[URM_DATE_CONVERSION](effective_date) as date) 'Effective_Date'
		, effective_date 'Effective_Date_Raw'
		,  Cast(Member_Price / 100 as money) 'Member_Price'
		, Case isnull(rescind, 'N') when 'Y' then 1 else 0 end 'rescind'
		, Case isnull(rescind, 'N') when 'Y' then epacube.[epacube].[URM_DATE_TIME_CONVERSION](dte_upd, tme_upd) else Null end 'rescind_timestamp'
		, epacube.[epacube].[URM_DATE_TIME_CONVERSION](dte_upd, tme_upd) 'Update_Timestamp'
		, epacube.[epacube].[URM_DATE_TIME_CONVERSION](dte_crt, TME_CRT) 'Create_Timestamp'
		, DTE_CRT, TME_CRT, USR_CRT, DTE_UPD, TME_UPD, USR_UPD, JOB_FK
		, IMPORT_TIMESTAMP 
		, 1 'Apply_Rescind_Flag'
		from (
		Select 
		@ZES_FK 'Zone_Entity_Structure_FK'
		, uccr.*, pi.PRODUCT_STRUCTURE_FK
		from [import].[URM_COST_CHANGES_RETAILER]  uccr
		inner join epacube.product_identification pi with (nolock) on uccr.ITEM_SURROGATE = pi.value and pi.DATA_NAME_FK = 110103
		where 1 = 1
		and uccr.job_fk = @Job_FK 
		and isnull(uccr.rescind, 'N') = 'Y'
		) A 
	) B
	order by Item_Surrogate, effective_date desc, Create_Timestamp desc, Update_Timestamp desc, SUR_TME desc

	Update pbv
	set Rescind = 1
	, RESCIND_TIMESTAMP = pbvu.rescind_timestamp
	, RECORD_STATUS_CR_FK = 2
	, UPDATE_TIMESTAMP = pbvu.UPDATE_TIMESTAMP
	from marginmgr.PRICESHEET_BASIS_VALUES pbv
	inner join marginmgr.pricesheet_basis_values_unique pbvu on pbv.unique_cost_record_id = pbvu.unique_cost_record_id
	where pbvu.Apply_Rescind_Flag = 1 and pbvu.Apply_Rescind_timestamp is null

--get rid of any records that with an effective date older than 90 days prior to the items effective date (new item).
	delete pbv
	from epacube.product_attribute pa
	inner join marginmgr.PRICESHEET_BASIS_VALUES pbv with (nolock) on pa.PRODUCT_STRUCTURE_FK = pbv.Product_Structure_FK
																and pbv.Cust_Entity_Structure_FK is null
																and pbv.Zone_Entity_Structure_FK is not null
																and pbv.data_name_fk = 503201
																and pa.data_name_fk = 500011
	where 1 = 1
	and pbv.effective_date < dateadd(d, -90, pa.ATTRIBUTE_DATE)

--Set all records to active that are inactive but have not been rescinded - they are most likely duplicates to today and future effective_dated records
	Update pbv
	Set RECORD_STATUS_CR_FK = 1
	from marginmgr.pricesheet_basis_values pbv
	where RECORD_STATUS_CR_FK = 2
	and Rescind is null
	and RESCIND_TIMESTAMP is null

	If Object_ID('tempdb..#pbv') is not null
	drop table #pbv

	Select 
	data_name_fk
	, product_structure_fk
	, cust_entity_structure_fk
	, zone_entity_structure_fk
	, effective_date
	, value
	, unit_value
	, rescind
	, pricesheet_basis_values_id
	, dense_rank()over(partition by data_name_fk, product_structure_fk, rescind order by effective_date desc) DRank_Eff
	into #pbv
	from marginmgr.pricesheet_basis_values
	where cust_entity_structure_fk is null
	and Zone_Entity_Structure_FK = @ZES_FK --(Select top 1 entity_structure_fk from epacube.epacube.entity_identification eiz with (nolock) where eiz.entity_data_name_fk = 151000 and eiz.data_name_fk = 151110 and eiz.[value] = '90001')
	and data_name_fk = 503201
	and effective_date < cast((select top 1 IMPORT_TIMESTAMP from marginmgr.pricesheet_basis_values_unique) as date)
	
	create index idx_pbv on #pbv
	(data_name_fk, rescind, DRank_Eff, Zone_Entity_Structure_FK, Cust_Entity_Structure_FK, product_structure_fk, value)		

	If Object_ID('tempdb..#dels') is not null
	drop table #dels

	create table #dels(product_structure_fk bigint not null, pricesheet_basis_values_fk bigint not null, DRank_Eff int not null)

	declare @i integer
	declare @Imax integer
	set @i = 1
	Set @Imax = (Select max(DRank_Eff) from #pbv)

	While @i <= @imax - 1
	begin

		insert into #dels(product_structure_fk, pricesheet_basis_values_fk, DRank_Eff)
		Select product_structure_fk, pricesheet_basis_values_id 'pricesheet_basis_values_fk', @i 'DRank_Eff'
		from marginmgr.pricesheet_basis_values 
		where 1 = 1
		and Pricesheet_Basis_Values_ID in
			(	Select pbv1.pricesheet_basis_values_id
				from #pbv pbv1
				inner join #pbv pbv2 on pbv1.data_name_fk = pbv2.data_name_fk 
				and pbv1.product_structure_fk = pbv2.product_structure_fk 
				and isnull(pbv1.Cust_Entity_Structure_FK, 0) = isnull(pbv2.Cust_Entity_Structure_FK , 0)
				and pbv1.Zone_Entity_Structure_FK = pbv2.Zone_Entity_Structure_FK
				and isnull(pbv1.rescind, 0) = isnull(pbv2.rescind, 0)
				and pbv1.unit_value = pbv2.unit_value 
				and pbv1.value = pbv2.value
				and pbv1.DRank_Eff = @i and pbv2.DRank_Eff = @i + 1
				and isnull(pbv1.rescind, 0) = 0 and isnull(pbv2.rescind, 0) = 0
			)

		Set @i = @i + 1

	end

	Delete pbv
	from marginmgr.pricesheet_basis_values pbv
	inner join #dels dels on pbv.pricesheet_basis_values_id = dels.pricesheet_basis_values_fk 

--Set Cost Decline Indicator - use table for declines and inactivating future records
	If Object_ID('tempdb..#Declines') is not null
	drop table #Declines

	create table #Declines(product_structure_fk bigint not null, pricesheet_basis_values_fk bigint not null, DRank_Eff int not null, Change_Status int null, effective_date date null, value money null)

	If Object_ID('tempdb..#pbv_declines') is not null
	drop table #pbv_declines

----added 2019-12-13 to test correction of declines not being reset to increases where appropriate.
	Update pbv
	Set Change_Status = Null
	from marginmgr.pricesheet_basis_values pbv

	Select 
	data_name_fk
	, product_structure_fk
	, cust_entity_structure_fk
	, zone_entity_structure_fk
	, effective_date
	, value
	, unit_value
	, rescind
	, pricesheet_basis_values_id
	, dense_rank()over(partition by data_name_fk, product_structure_fk, rescind order by effective_date desc) DRank_Eff
	into #pbv_declines
	from marginmgr.pricesheet_basis_values
	where cust_entity_structure_fk is null
	and Zone_Entity_Structure_FK = @ZES_FK --(Select top 1 entity_structure_fk from epacube.epacube.entity_identification eiz with (nolock) where eiz.entity_data_name_fk = 151000 and eiz.data_name_fk = 151110 and eiz.[value] = '90001')
	and data_name_fk = 503201
	
	create index idx_pbv on #pbv_declines
	(data_name_fk, rescind, DRank_Eff, Zone_Entity_Structure_FK, Cust_Entity_Structure_FK, product_structure_fk, value)		

	set @i = 1
	Set @Imax = (Select max(DRank_Eff) from #pbv)

	While @i <= @imax - 1
	begin

		insert into #Declines(product_structure_fk, pricesheet_basis_values_fk, DRank_Eff, Change_Status, effective_date, value)
		Select product_structure_fk, pricesheet_basis_values_id 'pricesheet_basis_values_fk', @i 'DRank_Eff'
		, -1 'Change_Status', effective_date, value
		from marginmgr.pricesheet_basis_values where Pricesheet_Basis_Values_ID in
			(	Select pbv1.pricesheet_basis_values_id
				from #pbv_declines pbv1
				inner join #pbv_declines pbv2 on pbv1.data_name_fk = pbv2.data_name_fk 
				and pbv1.product_structure_fk = pbv2.product_structure_fk 
				and isnull(pbv1.Cust_Entity_Structure_FK, 0) = isnull(pbv2.Cust_Entity_Structure_FK , 0)
				and pbv1.Zone_Entity_Structure_FK = pbv2.Zone_Entity_Structure_FK
				and isnull(pbv1.rescind, 0) = isnull(pbv2.rescind, 0)
				and pbv1.unit_value < pbv2.unit_value 
				and pbv1.DRank_Eff = @i and pbv2.DRank_Eff = @i + 1
				and isnull(pbv1.rescind, 0) = 0 and isnull(pbv2.rescind, 0) = 0
			)

		Set @i = @i + 1

	end

	Update pbv
	Set Change_Status = Decl.Change_Status
	from marginmgr.pricesheet_basis_values pbv
	inner join #Declines Decl on pbv.Pricesheet_Basis_Values_ID = decl.pricesheet_basis_values_fk

----------------------------
--Inactives
--Set Cost Decline Indicator
	If Object_ID('tempdb..#Inactives') is not null
	drop table #Inactives

	create table #Inactives(product_structure_fk bigint not null, pricesheet_basis_values_fk bigint not null, DRank_Eff int not null, Change_Status int null, effective_date date null, value money null)

	set @i = 1
	Set @Imax = (Select max(DRank_Eff) from #pbv)

	While @i <= @imax - 1
	begin

		insert into #Inactives(product_structure_fk, pricesheet_basis_values_fk, DRank_Eff, effective_date, value)
		Select product_structure_fk, pricesheet_basis_values_id 'pricesheet_basis_values_fk', @i 'DRank_Eff'
		, effective_date, value
		from marginmgr.pricesheet_basis_values where Pricesheet_Basis_Values_ID in
			(	Select pbv1.pricesheet_basis_values_id
				from #pbv_declines pbv1
				inner join #pbv_declines pbv2 on pbv1.data_name_fk = pbv2.data_name_fk 
				and pbv1.product_structure_fk = pbv2.product_structure_fk 
				and isnull(pbv1.Cust_Entity_Structure_FK, 0) = isnull(pbv2.Cust_Entity_Structure_FK , 0)
				and pbv1.Zone_Entity_Structure_FK = pbv2.Zone_Entity_Structure_FK
				and isnull(pbv1.rescind, 0) = isnull(pbv2.rescind, 0)
				and pbv1.unit_value = pbv2.unit_value 
				and pbv1.value = pbv2.value
				and pbv1.DRank_Eff = @i and pbv2.DRank_Eff = @i + 1
				and isnull(pbv1.rescind, 0) = 0 
				and isnull(pbv2.rescind, 0) = 0
			)

		Set @i = @i + 1

	end

	Update pbv
	Set RECORD_STATUS_CR_FK = 2
	from marginmgr.pricesheet_basis_values pbv
	inner join #Inactives ina on pbv.Pricesheet_Basis_Values_ID = ina.pricesheet_basis_values_fk

/*
	FETCH NEXT FROM MbrCosts INTO @Job_FK, @dt, @recs
		End--Fetch
	Close MbrCosts;
	Deallocate MbrCosts;
*/

	If Object_ID('tempdb..#pbv') is not null
	drop table #pbv

--Set rescind status of PRC and find immediate cost change records
Declare @Price_Change_Effective_Date date = cast(getdate() + 6 - datepart(dw, getdate()) as date)

If (select count(*) 
	from marginmgr.pricesheet_retail_changes prc
	where PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
	and 
		(
			(unique_cost_record_id in (select unique_cost_record_id from marginmgr.pricesheet_basis_values_unique where isnull(Apply_Rescind_Flag, 0) = 1))
			Or
			(isnull(ind_rescind, 0) = 1 and product_structure_fk in (select product_structure_fk from marginmgr.pricesheet_basis_values_unique where isnull(Apply_Rescind_Flag, 0) = 0))
			Or
			(PRODUCT_STRUCTURE_FK in (select PRODUCT_STRUCTURE_FK from marginmgr.pricesheet_basis_values_unique pbvu where cast(IMPORT_TIMESTAMP as date) > cast(getdate() + 4 - datepart(dw, getdate()) as date) and effective_date <= dateadd(d, 7, @Price_Change_Effective_Date)
			and isnull(pbvu.Apply_Rescind_Flag, 0) <> 1) and isnull(prc.ind_rescind, 0) <> 1)
		)	
	) > 0

	Begin

		Update PRC
		Set ind_Rescind = 1
		, HOLD_CUR_PRICE = 1
		, Reviewed = 1
		, Update_timestamp = getdate()
		, Price_Multiple_New = Price_Multiple_Cur
		, SRP_Adjusted = PRICE_CUR
		, GM_Adjusted = CUR_GM_PCT
		, TM_Adjusted = Null
		, IND_COST_CHANGE_ITEM = Null
		, Comments_Rescind = isnull(prc.Comments_Rescind, '') + 'Step1, '
		from marginmgr.PRICESHEET_RETAIL_CHANGES prc
		inner join marginmgr.pricesheet_basis_values_unique pbvu with (nolock) on prc.unique_cost_record_id = pbvu.unique_cost_record_id
		where 1 = 1
		and prc.PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
		and (prc.ITEM_CHANGE_TYPE_CR_FK in (655, 661) or prc.related_item_alias_id is not null)
		and isnull(pbvu.Apply_Rescind_Flag, 0) = 1

		Update PRC
		Set ind_Rescind = 1
		, Update_timestamp = getdate()
		, Price_Multiple_New = Price_Multiple_Cur
		, IND_COST_CHANGE_ITEM = Null
		, Comments_Rescind = isnull(prc.Comments_Rescind, '') + 'Step2, '
		from marginmgr.PRICESHEET_RETAIL_CHANGES prc
		inner join marginmgr.pricesheet_basis_values_unique pbvu with (nolock) on prc.unique_cost_record_id = pbvu.unique_cost_record_id
		where prc.PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
		and prc.ITEM_CHANGE_TYPE_CR_FK = 657
		and isnull(pbvu.Apply_Rescind_Flag, 0) = 1

		update prc
		set case_cost_new = pbvu.member_price
		, unit_cost_new =	Case	when uc.uom_code = 'RWC'	then cast(Cast(pbvu.member_price as money) / cast(isnull(puc.QUANTITY_MULTIPLE, 1) * prc.STORE_PACK / 16 as Numeric(18, 3)) as Numeric(18, 6))
							when uc.uom_code = 'RWL'	then Cast(pbvu.member_price as Numeric(18, 6))
														else Cast(pbvu.member_price / prc.STORE_PACK as Numeric(18, 6))
							end
		, unique_cost_record_id = pbvu.unique_cost_record_id
		, cost_change = cast(pbvu.member_price / prc.STORE_PACK as money) - unit_cost_cur
		, HOLD_CUR_PRICE = Null
		, Reviewed = Null
		, Price_Multiple_New = prc.Price_Multiple_Cur
		, SRP_Adjusted = Null
		, GM_Adjusted = Null
		, IND_COST_CHANGE_ITEM = 1 --(added 5/15/20 Jura urm-1884)
		, IND_RESCIND = 2 --(means the rescinded cost is being replaced)
		, Comments_Rescind = isnull(prc.Comments_Rescind, '') + 'Step3, '
		from marginmgr.PRICESHEET_RETAIL_CHANGES prc
		inner join (
					select * 
					from (
						select p1.data_name_fk, p1.item_surrogate, p1.Product_Structure_FK, p1.unique_cost_record_id, p1.Effective_Date, p1.member_price, p1.rescind, p1.import_timestamp, p1.Create_Timestamp, p1.Apply_Rescind_Flag, pbv.Change_Status
						, prcpbv.zone_entity_structure_fk, prcpbv.ITEM_CHANGE_TYPE_CR_FK
						, Dense_rank()over(partition by p1.product_structure_fk, prcpbv.zone_entity_structure_fk, prcpbv.ITEM_CHANGE_TYPE_CR_FK 
							order by case when pbv.change_status is null then p1.effective_date else null end desc
							, case when pbv.change_status is not null then p1.effective_date else null end
							) DRank
						from marginmgr.pricesheet_basis_values_unique p1 with (nolock)
						inner join marginmgr.pricesheet_basis_values pbv with (nolock) on p1.unique_cost_record_id = pbv.unique_cost_record_id
						inner join marginmgr.PRICESHEET_RETAIL_CHANGES prcpbv with (nolock) on p1.Product_Structure_FK = prcpbv.PRODUCT_STRUCTURE_FK
						where 1 = 1
						and p1.rescind = 0
						and p1.effective_date <= 
							case when pbv.change_status is null 
								then dateadd(d, cast(isnull(prcpbv.DAYSOUT, 0) as int), @Price_Change_Effective_date) 
								else dateadd(d, 1, @Price_Change_Effective_date) end
						) pbvu1 where DRank = 1
					) pbvu on prc.product_structure_fk = pbvu.product_structure_fk and prc.ZONE_ENTITY_STRUCTURE_FK = pbvu.zone_entity_structure_fk and prc.ITEM_CHANGE_TYPE_CR_FK = pbvu.ITEM_CHANGE_TYPE_CR_FK
		inner join epacube.product_uom_class puc with (nolock) on pbvu.product_structure_fk = puc.product_structure_fk and puc.data_name_fk = 500025
		inner join epacube.uom_code uc with (nolock) on puc.uom_code_fk = uc.uom_code_id
		where 1 = 1
		and prc.PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
		and (
				(prc.ind_rescind = 1 and isnull(pbvu.rescind, 0) = 0)
				or
				(cast(pbvu.IMPORT_TIMESTAMP as date) > cast(getdate() + 4 - datepart(dw, getdate()) as date)
				and isnull(pbvu.Apply_Rescind_Flag, 0) <> 1 and isnull(prc.ind_rescind, 0) <> 1
				and (
						(prc.COST_CHANGE_EFFECTIVE_DATE > @Price_Change_Effective_Date or prc.COST_CHANGE_EFFECTIVE_DATE is null) 
						and pbvu.Effective_Date <= case when isnull(pbvu.Change_Status, 0) = -1 then dateadd(d, 1, @Price_Change_Effective_Date) else dateadd(d, cast(isnull(prc.daysout, 0) as int), @Price_Change_Effective_Date) end
						and (pbvu.Member_Price >= prc.case_cost_new or isnull(pbvu.Change_Status, 0) = -1))
				)
			) --addressed all instances of saturday date when change_status = -1 by use of dateadd functionality.

		insert into marginmgr.pricesheet_basis_values_icc
		(value, unit_value, effective_date, pack, product_structure_fk, data_name_fk, create_timestamp, unique_cost_record_id, ind_cost_change_item, change_status)
		select distinct
		pbvu.member_price
		, Case	when uc.uom_code = 'RWC'	then cast(Cast(pbvu.member_price as money) / cast(isnull(puc.QUANTITY_MULTIPLE, 1) * prc.STORE_PACK / 16 as Numeric(18, 3)) as Numeric(18, 6))
							when uc.uom_code = 'RWL'	then Cast(pbvu.member_price as Numeric(18, 6))
														else Cast(pbvu.member_price / prc.STORE_PACK as Numeric(18, 6))
							end
		, pbvu.Effective_Date
		, prc.STORE_PACK
		, pbvu.Product_Structure_FK
		, pbvu.Data_Name_FK
		, pbvu.Create_Timestamp
		, pbvu.unique_cost_record_id
		, 1 'ind_cost_change_item'
		, pbvu.Change_Status
		from marginmgr.PRICESHEET_RETAIL_CHANGES prc
		inner join (
				select * 
					from (
						select p1.data_name_fk, p1.item_surrogate, p1.Product_Structure_FK, p1.unique_cost_record_id, p1.Effective_Date, p1.member_price, p1.rescind, p1.import_timestamp, p1.Create_Timestamp, p1.Apply_Rescind_Flag, pbv.Change_Status
						, prcpbv.zone_entity_structure_fk, prcpbv.ITEM_CHANGE_TYPE_CR_FK
						, Dense_rank()over(partition by p1.product_structure_fk, prcpbv.zone_entity_structure_fk, prcpbv.ITEM_CHANGE_TYPE_CR_FK 
							order by case when pbv.change_status is null then p1.effective_date else null end desc
							, case when pbv.change_status is not null then p1.effective_date else null end
							) DRank
						from marginmgr.pricesheet_basis_values_unique p1 with (nolock)
						inner join marginmgr.pricesheet_basis_values pbv with (nolock) on p1.unique_cost_record_id = pbv.unique_cost_record_id
						inner join marginmgr.PRICESHEET_RETAIL_CHANGES prcpbv with (nolock) on p1.Product_Structure_FK = prcpbv.PRODUCT_STRUCTURE_FK
						where 1 = 1
						and p1.rescind = 0
						and p1.effective_date <= 
							case when pbv.change_status is null 
								then dateadd(d, cast(isnull(prcpbv.DAYSOUT, 0) as int), @Price_Change_Effective_date) 
								else dateadd(d, 1, @Price_Change_Effective_Date) end
						) pbvu1 where DRank = 1
					) pbvu on prc.product_structure_fk = pbvu.product_structure_fk and prc.ZONE_ENTITY_STRUCTURE_FK = pbvu.zone_entity_structure_fk and prc.ITEM_CHANGE_TYPE_CR_FK = pbvu.ITEM_CHANGE_TYPE_CR_FK
		inner join epacube.product_uom_class puc with (nolock) on pbvu.product_structure_fk = puc.product_structure_fk and puc.data_name_fk = 500025
		inner join epacube.uom_code uc with (nolock) on puc.uom_code_fk = uc.uom_code_id
		where 1 = 1
		and prc.PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
		and (
				(prc.ind_rescind = 1 and isnull(pbvu.rescind, 0) = 0)
				or
				(cast(pbvu.IMPORT_TIMESTAMP as date) > cast(getdate() + 4 - datepart(dw, getdate()) as date)
				and isnull(pbvu.Apply_Rescind_Flag, 0) <> 1 and isnull(prc.ind_rescind, 0) <> 1
				and (
						(1 = 1 --prc.COST_CHANGE_EFFECTIVE_DATE > @Price_Change_Effective_Date or prc.COST_CHANGE_EFFECTIVE_DATE is null) 
						and pbvu.Effective_Date <= case when isnull(pbvu.Change_Status, 0) = -1 then dateadd(d, 1, @Price_Change_Effective_Date) else dateadd(d, cast(prc.daysout as int), @Price_Change_Effective_Date) end
						and (pbvu.Member_Price >= prc.case_cost_new or isnull(pbvu.Change_Status, 0) = -1)
						)
					)	
				)
			)

		Update pbvicc
		set pricesheet_basis_values_id = pbv.pricesheet_basis_values_id
		, change_status = pbv.change_status
		from marginmgr.pricesheet_basis_values_icc pbvicc
		inner join marginmgr.pricesheet_basis_values pbv on pbvicc.unique_cost_record_id = pbv.unique_cost_record_id

		Update PRC
		Set ind_rescind = 1
		, Comments_Rescind = isnull(prc.Comments_Rescind, '') + 'Step4, '
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC
		inner join marginmgr.PRICESHEET_RETAIL_CHANGES prc1 on prc.ZONE_ENTITY_STRUCTURE_FK = prc1.ZONE_ENTITY_STRUCTURE_FK
															and prc.PRICE_CHANGE_EFFECTIVE_DATE = prc1.PRICE_CHANGE_EFFECTIVE_DATE
															and prc.product_structure_fk = prc1.PARITY_ALIAS_PL_PARENT_STRUCTURE_FK
		where 1 = 1
		and PRC.PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
		and isnull(prc1.ind_rescind, 0) = 1
		and isnull(prc1.IND_COST_CHANGE_ITEM, 0) = 1

		Update PRC
		Set ind_rescind = 1
		, Comments_Rescind = isnull(prc.Comments_Rescind, '') + 'Step5, '
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC
		inner join marginmgr.PRICESHEET_RETAIL_CHANGES prc1 on prc.ZONE_ENTITY_STRUCTURE_FK = prc1.ZONE_ENTITY_STRUCTURE_FK
															and prc.PRICE_CHANGE_EFFECTIVE_DATE = prc1.PRICE_CHANGE_EFFECTIVE_DATE
															and prc.product_structure_fk = prc1.PARITY_PL_PARENT_STRUCTURE_FK
		where 1 = 1
		and PRC.PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
		and isnull(prc1.ind_rescind, 0) = 1
		and isnull(prc1.IND_COST_CHANGE_ITEM, 0) = 1

		Update PRC
		Set ind_rescind = 1
		, Hold_Cur_Price = 1
		, Reviewed = 1
		, SRP_ADJUSTED = prc.price_cur
		, GM_Adjusted = prc.cur_gm_pct
		, TM_Adjusted = Null
		, Comments_Rescind = isnull(prc.Comments_Rescind, '') + 'Step6, '
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC
		inner join marginmgr.PRICESHEET_RETAIL_CHANGES prc1 on prc.ZONE_ENTITY_STRUCTURE_FK = prc1.ZONE_ENTITY_STRUCTURE_FK
															and prc.alias_id = prc1.alias_id
															and prc.PRICE_CHANGE_EFFECTIVE_DATE = prc1.PRICE_CHANGE_EFFECTIVE_DATE
															and prc.ITEM_CHANGE_TYPE_CR_FK = 656
															and prc1.ITEM_CHANGE_TYPE_CR_FK = 657
		where PRC.PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
		and isnull(prc1.ind_rescind, 0) = 1
		and prc.ITEM_CHANGE_TYPE_CR_FK = 656
		and prc.unit_cost_new = prc1.unit_cost_new
		and 
			(
				(select count(*) from marginmgr.pricesheet_retail_changes PRCs where PRCs.PRICE_CHANGE_EFFECTIVE_DATE = prc.PRICE_CHANGE_EFFECTIVE_DATE
					and PRCs.alias_id = prc.alias_id
					and PRCs.zone_entity_structure_fk = prc.zone_entity_structure_fk
					and isnull(PRCs.ind_cost_change_item, 0) = 1
					and PRCs.ITEM_CHANGE_TYPE_CR_FK = 657
					and isnull(PRCs.ind_rescind, 0) = 0
					and prc.case_cost_new = 
						(select max(case_cost_new) from marginmgr.pricesheet_retail_changes PRCs2 where PRCs2.PRICE_CHANGE_EFFECTIVE_DATE = prc.PRICE_CHANGE_EFFECTIVE_DATE 
							and PRCs2.alias_id = prc.ALIAS_ID
							and PRCs2.ZONE_ENTITY_STRUCTURE_FK = prc.ZONE_ENTITY_STRUCTURE_FK
							and PRCs2.ITEM_CHANGE_TYPE_CR_FK not in (656, 662)
							and isnull(prcs2.ind_rescind, 0) = 0
						)
					) = 0
				OR
					(prc1.unit_cost_cur = prc1.unit_cost_new)
			)

--		Set Rescind status to alias in parity with item where the cost change is not contained within the alias.
		Update PRC2
		Set ind_Rescind = 1
		, HOLD_CUR_PRICE = 1
		, Update_timestamp = getdate()
		, Price_Multiple_New = prc2.Price_Multiple_Cur
		, SRP_Adjusted = prc2.PRICE_CUR
		, GM_Adjusted = prc2.CUR_GM_PCT
		, TM_Adjusted = Null
		, IND_COST_CHANGE_ITEM = Null
		, Comments_Rescind = isnull(prc.Comments_Rescind, '') + 'Step7, '
		from marginmgr.PRICESHEET_RETAIL_CHANGES prc
		inner join marginmgr.PRICESHEET_RETAIL_CHANGES prc2 on prc.PRICE_CHANGE_EFFECTIVE_DATE = prc2.PRICE_CHANGE_EFFECTIVE_DATE
														 and prc.ZONE_ENTITY_STRUCTURE_FK = prc2.ZONE_ENTITY_STRUCTURE_FK 
														 and 
															(
																prc.related_item_alias_id = prc2.alias_id
																or
																prc.parity_child_product_structure_fk = prc2.PRODUCT_STRUCTURE_FK
																or
																prc.parity_parent_product_structure_fk = prc2.PRODUCT_STRUCTURE_FK
															)
		where 1 = 1
		and prc.PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
		and isnull(prc2.IND_COST_CHANGE_ITEM, 0) = 0
		and (prc2.ITEM_CHANGE_TYPE_CR_FK not in (655, 661, 657))
		and isnull(prc.ind_rescind, 0) = 1
		and isnull(prc2.ind_rescind, 0) = 0
		and prc2.parity_alias_id is null

--	Update Level gross for any rescinded records that now have appropriate costs in place for this week's level gross. Set ind_rescind = 2 in PRC
	exec [precision].[price_maintenance_processing_zone] 0, 0, 1

---
END
-----
--Account for any of today's pack changes where corresponding costs have been loaded prior to today

	if object_id('tempdb..#pk_pbv') is not null
	drop table #pk_pbv

	if object_id('tempdb..#pk') is not null
	drop table #pk

	select * into #pk_pbv from (
	select pricesheet_basis_values_id, Product_Structure_FK, effective_date, value, unit_value, pack, CREATE_TIMESTAMP, IMPORT_TIMESTAMP
	, dense_rank()over(partition by product_structure_fk order by effective_date desc) DRank
	from marginmgr.PRICESHEET_BASIS_VALUES
	where cast(effective_date as date) <= cast(getdate() as date)
	and RESCIND_TIMESTAMP is null
	and unique_cost_record_id not in
		(select unique_cost_record_id from  marginmgr.PRICESHEET_BASIS_VALUES where RESCIND_TIMESTAMP is not null)
	) a where DRank = 1

	create index idx_01 on #pk_pbv(product_structure_fk)
	create index idx_02 on #pk_pbv(pricesheet_basis_values_id)

	Select *
	into #pk from (
	Select PRODUCT_STRUCTURE_FK, effective_date, pack, CREATE_TIMESTAMP, IMPORT_TIMESTAMP
	, dense_rank()over(partition by product_structure_fk order by effective_date desc) drank
	from epacube.PRODUCT_UOM_PACK
	where cast(effective_date as date) <= cast(getdate() as date)
	) a where drank = 1

	create index idx_01 on #pk(product_structure_fk)

	--Update existing cost record(s) that have not taken effect yet that have a pack change not yet accounted for in cost
	Update PBV
	Set pack = pk.pack
	, unit_value = Case	when uc.uom_code = 'RWC'	then cast(Cast(pbv.value as money) / cast(isnull(puc.QUANTITY_MULTIPLE, 1) * pk.pack / 16 as Numeric(18, 3)) as Numeric(18, 6))
					when uc.uom_code = 'RWL'	then Cast(pbv.value as Numeric(18, 6))
											else Cast(pbv.value / pk.pack as Numeric(18, 6))
					end
	, comment = 'Pack, Unit_value updated based on pack change dated ''' + convert(varchar(10), pk.effective_date, 1) + ''''
	from #pk_pbv pbv1
	inner join marginmgr.PRICESHEET_BASIS_VALUES pbv on pbv1.Pricesheet_Basis_Values_ID = pbv.Pricesheet_Basis_Values_ID
	inner join epacube.product_identification pi with (nolock) on pbv.Product_Structure_FK = pi.PRODUCT_STRUCTURE_FK and pi.data_name_fk = 110103
	inner join epacube.product_uom_class puc with (nolock) on pbv.product_structure_fk = puc.product_structure_fk and puc.data_name_fk = 500025
	inner join epacube.uom_code uc with (nolock) on puc.uom_code_fk = uc.uom_code_id
	left join #pk pk on pbv.Product_Structure_FK = pk.PRODUCT_STRUCTURE_FK
	where pbv1.PACK <> pk.PACK
	and pk.EFFECTIVE_DATE <= pbv.effective_date

	--Create new cost record(s) that have a pack change going into effect after a current cost record's effective date has passed and no new cost associated with the pack change has been received.
	Insert into marginmgr.PRICESHEET_BASIS_VALUES
	([PriceSheetID], [Effective_Date], [End_Date], [Product_Structure_FK], [Org_Entity_Structure_FK], [Cust_Entity_Structure_FK], [Vendor_Entity_Structure_FK], [Zone_Type_CR_FK], [Zone_Entity_Structure_FK], [Data_Name_FK], [Basis_Position], [Value], [UOM_CODE_FK], [PACK], [SPC_UOM_FK], [Calculated_Value], [Promo], [CO_ENTITY_STRUCTURE_FK], [RECORD_STATUS_CR_FK], [JOB_FK], [UPDATE_USER], [CREATE_TIMESTAMP], [UPDATE_TIMESTAMP], [CREATE_USER], [Comp_Entity_Structure_FK], [Comp_Segment_FK], [UNIT_VALUE], [Rescind], [RESCIND_TIMESTAMP], [Change_Status], [IMPORT_TIMESTAMP], [Cost_Change_Processed_Date], [comment], [PRICESHEET_BASIS_VALUES_UNIQUE_FK], [unique_cost_record_id])
	Select distinct
	pbv.[PriceSheetID], pk.[Effective_Date], pbv.[End_Date], pbv.[Product_Structure_FK], pbv.[Org_Entity_Structure_FK], pbv.[Cust_Entity_Structure_FK], pbv.[Vendor_Entity_Structure_FK], pbv.[Zone_Type_CR_FK], pbv.[Zone_Entity_Structure_FK], pbv.[Data_Name_FK], pbv.[Basis_Position], pbv.[Value], pbv.[UOM_CODE_FK], pk.[PACK], pbv.[SPC_UOM_FK], pbv.[Calculated_Value], pbv.[Promo], pbv.[CO_ENTITY_STRUCTURE_FK], pbv.[RECORD_STATUS_CR_FK], pbv.[JOB_FK], pbv.[UPDATE_USER], pbv.[CREATE_TIMESTAMP], pbv.[UPDATE_TIMESTAMP], pbv.[CREATE_USER], pbv.[Comp_Entity_Structure_FK], pbv.[Comp_Segment_FK], cast(pbv.value / pk.pack as Numeric(18, 6)) 'UNIT_VALUE', pbv.[Rescind], pbv.[RESCIND_TIMESTAMP], pbv.[Change_Status], pbv.[IMPORT_TIMESTAMP], pbv.[Cost_Change_Processed_Date], 'New record created based on pack change dated ''' + convert(varchar(10), pk.effective_date, 1) + '''' 'comment', pbv.[PRICESHEET_BASIS_VALUES_UNIQUE_FK], pbv.[unique_cost_record_id]
	from #pk_pbv pbv1
	inner join marginmgr.PRICESHEET_BASIS_VALUES pbv on pbv1.Pricesheet_Basis_Values_ID = pbv.Pricesheet_Basis_Values_ID
	inner join epacube.product_identification pi with (nolock) on pbv.Product_Structure_FK = pi.PRODUCT_STRUCTURE_FK and pi.data_name_fk = 110103
	left join #pk pk on pbv.Product_Structure_FK = pk.PRODUCT_STRUCTURE_FK
	where pbv1.PACK <> pk.PACK
	and pk.EFFECTIVE_DATE > pbv.effective_date

--Update Incorrect Packs
	select * 
	into #pbv1
	from (
			Select pbv.Pricesheet_Basis_Values_ID, pbv.product_structure_fk, pbv.Effective_Date 'pbv_effective_date', pbv.value, pbv.unit_value, pbv.rescind, pbv.pack 'pbv_pack'
			, pup.EFFECTIVE_DATE 'pup_effective_date', pup.pack 'pup_pack'
			, Dense_Rank()over(partition by pbv.product_structure_fk, pbv.effective_date order by pup.effective_date desc) 'DRank'
			from marginmgr.PRICESHEET_BASIS_VALUES pbv
			inner join epacube.product_uom_pack pup on pbv.product_structure_fk = pup.product_structure_fk
			where pup.EFFECTIVE_DATE <= pbv.effective_date
		) A where DRank = 1

	Update pbv
	set pack = pbv1.pup_pack
	, unit_value = Case	when uc.uom_code = 'RWC'	then cast(Cast(pbv.value as money) / cast(isnull(puc.QUANTITY_MULTIPLE, 1) * pbv1.pup_pack / 16 as Numeric(18, 3)) as Numeric(18, 6))
					when uc.uom_code = 'RWL'	then Cast(pbv.value as Numeric(18, 6))
											else Cast(pbv.value / pbv1.pup_pack as Numeric(18, 6))
					end
	from marginmgr.pricesheet_basis_values pbv
	inner join #pbv1 pbv1 on pbv.Pricesheet_Basis_Values_ID = pbv1.Pricesheet_Basis_Values_ID
	inner join epacube.product_uom_class puc with (nolock) on pbv.product_structure_fk = puc.product_structure_fk and puc.data_name_fk = 500025
	inner join epacube.uom_code uc with (nolock) on puc.uom_code_fk = uc.uom_code_id
	where pbv1.pbv_pack <> pbv1.pup_pack

--End of Pack Change updates
--Update pbv.unit_value on constant random weight items where out of sync.

	update pbv_c
	set unit_value = fix.calc_unit_value
	from marginmgr.pricesheet_basis_values pbv_c
	inner join (
	select * from (
	select pbv.product_structure_fk, pi.value 'item', pbv1.pup_pack, pbv.value 'case_cist'
		, Case	when uc.uom_code = 'RWC'	then cast(Cast(pbv.value as money) / cast(isnull(puc.QUANTITY_MULTIPLE, 1) * pbv1.pup_pack / 16 as Numeric(18, 3)) as Numeric(18, 6))
						when uc.uom_code = 'RWL'	then Cast(pbv.value as Numeric(18, 6))
												else Cast(pbv.value / pbv1.pup_pack as Numeric(18, 6))
						end 'calc_unit_value'
						, pbv.unit_value 'pbv_unit_value'

						, cast(isnull(puc.QUANTITY_MULTIPLE, 1) * cast(pbv1.pup_pack as numeric(18, 6)) / 16 as Numeric(18, 3)) 'units'
						, pbv.Effective_Date
		, pbv.pricesheet_basis_values_id
		from marginmgr.pricesheet_basis_values pbv
		inner join #pbv1 pbv1 on pbv.Pricesheet_Basis_Values_ID = pbv1.Pricesheet_Basis_Values_ID
		inner join epacube.product_uom_class puc with (nolock) on pbv.product_structure_fk = puc.product_structure_fk and puc.data_name_fk = 500025
		inner join epacube.uom_code uc with (nolock) on puc.uom_code_fk = uc.uom_code_id
		inner join epacube.PRODUCT_IDENTIFICATION pi with (nolock) on pbv.Product_Structure_FK = pi.product_structure_fk and pi.DATA_NAME_FK = 110100
		where uc.uom_code = 'RWC'
	) a where 1 = 1
	and round(pbv_unit_value, 2) <> round(calc_unit_value, 2)
	and round(pbv_unit_value, 2) between round(calc_unit_value, 2) * 0.9 and round(calc_unit_value, 2) * 1.1
	) fix on pbv_c.Pricesheet_Basis_Values_ID = fix.Pricesheet_Basis_Values_ID


	SELECT @v_input_rows = COUNT(1)
	FROM [import].import_gtin WITH (NOLOCK)
	WHERE job_fk = @JOB_FK;

	SELECT @v_output_rows = COUNT(1)
	FROM [import].import_gtin WITH (NOLOCK)
	WHERE job_fk = @JOB_FK
				AND PRODUCT_STRUCTURE_FK is NOT NULL ;

	SET @v_exception_rows = @v_input_rows - @v_output_rows;
	SET @V_END_TIMESTAMP = getdate()
	EXEC common.job_execution_create  @JOB_FK, 'IMPORT_COMPLETE',
										@v_input_rows, @v_output_rows, @v_exception_rows,
										201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

	SET @status_desc =  'finished execution of IMPORT.LOAD_RETAILER COST CHANGES'
 
	Exec common.exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @JOB_FK;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT.LOAD_RETAILER COST has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @JOB_FK;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
