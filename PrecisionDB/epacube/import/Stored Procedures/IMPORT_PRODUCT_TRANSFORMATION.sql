﻿





-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        05/06/2009   Initial Version 
-- CV        08/28/2009   EPA-2536 Update the common.job with name of package 
-- CV        08/31/2009   Enhance logging getting start and end dates straight
-- CV        10/09/2009   Adding in the Do not import code along with execute import sql proc 
-- CV        01/13/2010   FOR ICSW IMPORT. Mark duplicate do not insert.
-- KS        03/18/2010   Very SLOW.... for small ICSP takes over 50 minutes ( UPDATE #TS_DISTINCT_PRODUCTS )
-- CV        05/27/2010   SUP 373 moved where update to effective date happens.
-- CV        01/17/2012   Moved the Drop Table 
-- CV        05/01/2012   Add insert to record name value table. (jasco specific) 
-- CV        10/05/2013   removed some hard coding and truncate from mult type 
-- MN		 01/21/2014	  Stripped out dynamic sql and use direct SQL queries
-- CV        03/31/2014   looking at name value for taxonomy data
-- CV        04/16/2014   Adding more detail for taxonomy import packages
-- CV        05/06/2014   Added new taxonomy validation procedure
-- GHS		 05/16/2014	  Added @JobName as optional parameter
-- CV        08/04/2015   fixed error for product mult type loops


CREATE PROCEDURE [import].[IMPORT_PRODUCT_TRANSFORMATION] ( @IN_JOB_FK bigint, @JobName Varchar(64) = Null ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to   
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure 
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--     
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int 
	 DECLARE @v_import_package_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
	 DECLARE @V_UNIQUE4   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.IMPORT_PRODUCT_TRANSFORMATION', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


--LOOKUP PRIMARY PRODUCT BY PACKAGE
SET	@data_name_id_for_Product = (select data_name_id from epacube.data_name where name = 'PRODUCT')

SET @v_import_package_fk = (select distinct import_package_fk 
							from import.import_record_data WITH (NOLOCK)
							where  job_fk = @in_job_fk)

SET @v_column_name = (select column_name 
						from import.import_map_metadata WITH (NOLOCK)
						where import_package_fk = @v_import_package_fk
						and data_name_fk = @data_name_id_for_Product)  

SET @v_primary = (select source_field_name
						from import.import_map_metadata WITH (NOLOCK)
						where import_package_fk = @v_import_package_fk
						and data_name_fk = @data_name_id_for_Product)  

--LOOKUP WHAT MAKES THE ROW UNIQUE BY PACKAGE

SET @V_UNIQUE1 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 1),-999)

SET @V_UNIQUE2 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 2),-999)

SET @V_UNIQUE3 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 3), -999)

SET @V_UNIQUE4 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 4), -999)



--ADDED FOR DMU IMPORT SET THE NAME OF PACKAGE

				UPDATE J
				SET name = Isnull(@JobName,
							(select name from import.import_package WITH (NOLOCK)
								WHERE import_package_id = @v_import_package_fk))
							From common.job J WHERE job_id = @in_job_Fk

--------------------------------------------------------------------------------------
--  Call any special sql that needs to be performed using import sql table 
--  and procedure
--------------------------------------------------------------------------------------

 SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      --SET @l_sysdate = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'START [import].[EXECUTE_IMPORT_SQL] LOGIC',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
					
			
If @v_import_package_fk not in  (select import_package_id from import.IMPORT_PACKAGE where REPLACE_TAX_ATTRIBUTES_IND = 1)

BEGIN


	EXECUTE  [import].[EXECUTE_IMPORT_SQL] 
					@v_import_package_fk
					,@in_job_fk


END
     
-------------------------------------------------------------------------------------------------
-- ADDED TO NOT ALLOW DUPLICATE ROWS TO BE IMPORTED
-------------------------------------------------------------------------------------------------
		SET @v_exception_rows = @v_input_rows - @v_output_rows;
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'UPDATE IMPORT.IMPORT_RECORD_DATA',
		                                 null, null, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;										

		--UPDATE IMPORT.IMPORT_RECORD_DATA
		--SET Do_Not_import_IND = 1 
		--WHERE import_record_data_id IN (
		--						SELECT A.import_record_data_id
		--						FROM (         
		--						SELECT ird1.import_record_data_id 
		--			   ,DENSE_RANK() OVER ( PARTITION BY 
		--				   ISNULL((select column_name from import.import_map_metadata
		--						where import_package_fk = @v_import_package_fk and unique_seq_ind = 1),-999), 
  --                        ISNULL((select column_name from import.import_map_metadata
		--						where import_package_fk = @v_import_package_fk and unique_seq_ind = 2),-999),
		--					ISNULL((select column_name from import.import_map_metadata
		--						where import_package_fk = @v_import_package_fk and unique_seq_ind = 3), -999)
		--				   ORDER BY ird1.import_record_data_id DESC ) AS DRANK
		--				   FROM IMPORT.IMPORT_RECORD_DATA IRD1
		--				   WHERE ird1.job_fk = @in_job_fk    ) A     
		--					WHERE DRANK <> 1 )
		--								AND IMPORT_PACKAGE_FK <> 213000
		
		SET @ls_exec =
						'UPDATE IMPORT.IMPORT_RECORD_DATA
						SET Do_Not_import_IND = 1 
						WHERE import_record_data_id IN (
								SELECT A.import_record_data_id
								FROM (         
								SELECT ird1.import_record_data_id 
					   ,DENSE_RANK() OVER ( PARTITION BY '
						   + ' ' 
                           + @V_UNIQUE1
						   + ' , ' 
                           + @V_UNIQUE2
					       + ' , '
                           + @V_UNIQUE3
						    + ' , '
                           + @V_UNIQUE4
						   + ''
						   + ' ORDER BY ird1.import_record_data_id DESC ) AS DRANK
							FROM IMPORT.IMPORT_RECORD_DATA IRD1
							WHERE ird1.import_record_data_id  in (
							select ird.import_record_data_id 
							from import.import_record_data ird
							where ird.job_fk = '
							+ cast(isnull (@in_job_fk, -999) as varchar(20))
							+ ')
										) A
										WHERE DRANK <> 1   )
										AND IMPORT_PACKAGE_FK not in ( 213000)'							

----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), isnull ( @ls_exec, 'Trying to update Do_Not_import_IND from IMPORT.IMPORT_RECORD_DATA' ) )

         exec sp_executesql 	@ls_exec;														


/* ----------------------------------------------------------------------------------- */
--   --Create Temp table to insert distinct product data 
/* ----------------------------------------------------------------------------------- */


--drop temp table if it exists
	IF object_id('tempdb..#TS_DISTINCT_PRODUCTS') is not null
	   drop table #TS_DISTINCT_PRODUCTS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_DISTINCT_PRODUCTS(
    [RECORD_NO] [int] IDENTITY(1,1) NOT NULL,
	[IMPORT_RECORD_DATA_FK]	BIGINT,
	[IMPORT_FILENAME] [varchar](128),
	[JOB_FK] [bigint],
	[IMPORT_PACKAGE_FK] [int],
	[EFFECTIVE_DATE] [datetime],
	[END_DATE] [datetime],
	[ENTITY_CLASS] [varchar](64) ,
	[ENTITY_DATA_NAME] [varchar](64),
	[ENTITY_ID_DATA_NAME] [varchar](64),
	[ENTITY_ID_VALUE] [varchar](64),
	[ENTITY_STATUS] [varchar](32),
	[SYSTEM_ID_VALUE] [varchar](64),
	[PRIORITY] [varchar](32),
	[SEARCH_ACTIVITY_FK] [int],
	[UPDATE_TIMESTAMP] [datetime],
	[UPDATE_USER] [varchar](64)
		)



-------------------------------------------------------------------------------------
--   LOAD TEMP TABLE WITH DISTINCT PRODUCT DATA
--   NEED to check if NULL Then do something else to get product identification 
-------------------------------------------------------------------------------------

SET @v_effective_date = ISNULL((SELECT Top 1 import.import_record_data.effective_date
											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
											WHERE 1=1 
											AND Job_Fk = @in_job_fk), getdate())

SET @v_import_filename = (SELECT Top 1 import.import_record_data.import_filename
											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
											WHERE 1=1 
											AND Job_Fk = @in_job_fk)

 SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record
       WHERE job_fk = @in_job_fk;
       
SET @v_exception_rows = @v_input_rows - @v_output_rows;
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD TEMP TABLE #TS_DISTINCT_PRODUCTS WITH DISTINCT PRODUCT DATA',
		                                 null, null, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;					

						INSERT INTO #TS_DISTINCT_PRODUCTS
						(   [JOB_FK]
						   ,[IMPORT_PACKAGE_FK]
						   ,[IMPORT_FILENAME] 
                           ,[EFFECTIVE_DATE]
						    ,END_DATE
                           ,[ENTITY_CLASS]
						   ,[ENTITY_DATA_NAME]
						   ,[ENTITY_ID_DATA_NAME]
						   ,[ENTITY_ID_VALUE]
						   )
						SELECT distinct
							IRD.JOB_FK
							,IRD.IMPORT_PACKAGE_FK 						   
						    ,@v_import_filename
						    ,cast ( @v_effective_date as varchar(30) )
							,cast(ird.END_DATE as datetime)
						    , 'PRODUCT'
						    , 'PRODUCT'
						    , @v_primary 
						   ,[epacube].[GetImportDataColumn](@v_column_name, ird.import_record_data_id) 						    
						 FROM IMPORT.IMPORT_RECORD_DATA  IRD WITH (NOLOCK) 
						 WHERE 1=1 
						  AND isnull(DO_NOT_IMPORT_IND,0) <> 1
						 AND [epacube].[GetImportDataColumn](@v_column_name, ird.import_record_data_id) IS NOT NULL
						 AND IRD.JOB_FK = @in_job_fk 

-----------------------------------------------------
	
--- This was taking over one hour just for the update... so reworked SQL to do all during insert 

------				UPDATE #TS_DISTINCT_PRODUCTS
------				SET RECORD_NO = x.ROW 
------				,import_filename = (SELECT Top 1 import.import_record_data.import_filename
------											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
------											WHERE 1=1 
------											AND Job_Fk = @in_job_fk)
------				,effective_date = (SELECT Top 1 import.import_record_data.effective_date
------											FROM IMPORT.IMPORT_RECORD_DATA  WITH (NOLOCK)
------											WHERE 1=1 
------											AND Job_Fk = @in_job_fk)
------				FROM (SELECT ROW_NUMBER() 
------						OVER (ORDER BY ENTITY_ID_VALUE) AS Row, 
------						ENTITY_ID_VALUE
------				FROM #TS_DISTINCT_PRODUCTS where job_fk = @in_job_fk)x
------				where #TS_DISTINCT_PRODUCTS.entity_id_value = x.entity_id_value
------AND #TS_DISTINCT_PRODUCTS.job_Fk = @in_job_fk

---Couldn't just insert filename above as it may not be distinct 
--if multiple files importing
---  CINDY comment does not make sense... doing top 1 by job where job is parameter ?????
---			Each file should be its own job... need to do that in SSIS


-------------------------------------------------------------------------------------
--   LOAD IMPORT_RECORD TABLE(S) 
-------------------------------------------------------------------------------------

					INSERT INTO IMPORT.IMPORT_RECORD 
							   ([IMPORT_FILENAME]
							   ,[RECORD_NO]
							   ,[JOB_FK]
							   ,[IMPORT_PACKAGE_FK]
--							   ,[EFFECTIVE_DATE]
							   ,[END_DATE]
							   ,[ENTITY_CLASS]
							   ,[ENTITY_DATA_NAME]
							   ,[ENTITY_ID_DATA_NAME]
							   ,[ENTITY_ID_VALUE]
							   ,[UPDATE_TIMESTAMP]
							   ,[UPDATE_USER])

					SELECT 
							   TDP.IMPORT_FILENAME
							   ,TDP.RECORD_NO
							   ,TDP.JOB_FK
							   ,TDP.IMPORT_PACKAGE_FK
--							   ,TDP.EFFECTIVE_DATE
							   ,TDP.END_DATE
							   ,TDP.ENTITY_CLASS
							   ,TDP.ENTITY_DATA_NAME
							   ,TDP.ENTITY_ID_DATA_NAME
							   ,TDP.ENTITY_ID_VALUE
							   ,TDP.UPDATE_TIMESTAMP
							   ,TDP.UPDATE_USER
					FROM #TS_DISTINCT_PRODUCTS TDP
					WHERE 1=1
					AND TDP.JOB_FK = @in_job_FK


-----drop table here


DROP TABLE #TS_DISTINCT_PRODUCTS


-------------------------------------------------------------------------
 SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'SSIS TRANSFORMATION',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


-------------------------------------------------------------------------------------
--   UPDATE IMPORT RECORD WITH RECORD_NO and SEQ_ID
-------------------------------------------------------------------------------------
 SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record
       WHERE job_fk = @in_job_fk;
       
SET @v_exception_rows = @v_input_rows - @v_output_rows;
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'UPDATE RECORD_NO FOR IMPORT.IMPORT_RECORD_DATA',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
					/* --Version 1
					UPDATE IMPORT.IMPORT_RECORD_DATA
						set RECORD_NO = import.import_record.record_no
						from import.import_record WITH (NOLOCK)
						where entity_id_value = [epacube].[GetImportDataColumn](@v_column_name, IMPORT.IMPORT_RECORD_DATA.import_record_data_id)
						and import.import_record_data.JOB_FK = @in_job_fk						
						and import.import_record.JOB_FK = @in_job_fk
					
					 --Version 2
					;with EntityIDValueFromIRD 
						as (select ir.record_no, entity_id_value, ird.import_record_data_id					
						from IMPORT.IMPORT_RECORD ir WITH (NOLOCK)
						left join IMPORT.IMPORT_RECORD_DATA ird WITH (NOLOCK) 
							on ir.job_fk = ird.job_fk 
							and ir.entity_id_value = [epacube].[GetImportDataColumn](@v_column_name, ird.import_record_data_id) 
						where ir.JOB_FK = @in_job_fk							
						) 
						update ird
						set  ird.record_no = EntityIDValueFromIRD.record_no
						from IMPORT.IMPORT_RECORD_DATA ird WITH (NOLOCK) 
						inner join 	EntityIDValueFromIRD ON 					
						 ird.import_record_data_id = EntityIDValueFromIRD.import_record_data_id
						*/

-----------
			
				SET @V_END_TIMESTAMP = getdate()
				EXEC common.job_execution_create  @in_job_fk, 'UPDATE SEQ_ID FOR IMPORT.IMPORT_RECORD_DATA',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

				UPDATE IMPORT.IMPORT_RECORD_DATA
				SET SEQ_ID = x.ROW 
				FROM (SELECT ROW_NUMBER() 
				OVER (ORDER BY IMPORT_RECORD_DATA_ID) AS Row, 
					 IMPORT_RECORD_DATA_ID
			FROM IMPORT.IMPORT_RECORD_DATA WITH (NOLOCK)
			where job_fk = @in_job_fk)x
			WHERE import.import_record_data.job_fk = @in_job_fk
			AND import.import_record_data.import_record_data_id = x.import_record_data_id


SET @ls_exec =
						'UPDATE IMPORT.IMPORT_RECORD_DATA
						set RECORD_NO = import.import_record.record_no
						from import.import_record WITH (NOLOCK)
						where'
						+ ' '
						+ @v_column_name 
						+ ' = entity_id_value
						and import.import_record_data.JOB_FK = '
						+ cast(isnull (@in_job_fk, -999) as varchar(20))
						+ ' and import.import_record.JOB_FK = '
						+ cast(isnull (@in_job_fk, -999) as varchar(20))


----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), isnull ( @ls_exec, 'Trying to Update Import Record data in import.IMPORT_Product_TRANSFORMATION' ) )

         exec sp_executesql 	@ls_exec;


-----------

				UPDATE IMPORT.IMPORT_RECORD_DATA
				SET SEQ_ID = x.ROW 
				FROM (SELECT ROW_NUMBER() 
				OVER (ORDER BY IMPORT_RECORD_DATA_ID) AS Row, 
					 IMPORT_RECORD_DATA_ID
			FROM IMPORT.IMPORT_RECORD_DATA WITH (NOLOCK)
			where job_fk = @in_job_fk)x
			WHERE import.import_record_data.job_fk = @in_job_fk
			AND import.import_record_data.import_record_data_id = x.import_record_data_id


--- EPA Constraint
UPDATE IMPORT.IMPORT_RECORD_DATA
				SET SEQ_ID = NULL
--,RECORD_NO = NULL
WHERE  job_fk = @in_job_fk
and DO_NOT_IMPORT_IND = 1

----------

UPDATE IMPORT.IMPORT_RECORD_DATA
SET EFFECTIVE_DATE = getdate() 
WHERE  JOB_Fk =  @in_job_FK
AND EFFECTIVE_DATE IS NULL


UPDATE IMPORT.IMPORT_RECORD
SET EFFECTIVE_DATE = (SELECT TOP 1 ISNULL(IRD.EFFECTIVE_DATE,getdate())
FROM IMPORT.IMPORT_RECORD_DATA IRD
WHERE IRD.RECORD_NO = import.import_record.record_no
AND IRD.JOB_fK = import.import_record.JOB_FK)
WHERE RECORD_NO = import.import_record.record_no
AND JOB_FK = import.import_record.JOB_FK
AND JOB_Fk =  @in_job_FK


--------------------------------------------------------------------------------------
--- TAXONOMY SECTION
---CALL NEW TAXONOMY_TRANSFORMATION
---------------------------------------------------------------------------------------

If @v_import_package_fk in  (select import_package_id from import.IMPORT_PACKAGE where REPLACE_TAX_ATTRIBUTES_IND = 1)

BEGIN

EXECUTE  [import].[IMPORT_TAXONOMY_TRANSFORMATION] 
   @IN_JOB_FK, @v_import_package_fk

 END

 ------------------------------------------------------
---INSERT DATA INTO IMPORT MULTI VALUE TABLE
------------------------------------------------------
---drop table if exist

IF object_id('tempdb..#TS_DATA_NAMES') is not null
	   drop table #TS_DATA_NAMES;
	   
 
-- create temp table
	CREATE TABLE #TS_DATA_NAMES(
	[NAME]	varchar(50),
	[POSITION] varchar(50),
	[FILE_NAME] varchar(256),
	[IMPORT_RECORD_DATA_ID] bigint,
	[RECORD_NO] bigint,
	[DATA_VALUE] varchar(256)
	)


insert into #TS_DATA_NAMES(
[NAME],
	[POSITION],[FILE_NAME],[IMPORT_RECORD_DATA_ID], [Record_no]
	)
select dn.name, imm.COLUMN_NAME , ird.IMPORT_FILENAME, ird.IMPORT_RECORD_DATA_ID, ird.RECORD_NO
  from import.import_record_data ird with (nolock)
	  inner join import.IMPORT_MAP_METADATA imm with (nolock)
	  on ( imm.IMPORT_PACKAGE_FK = ird.IMPORT_PACKAGE_FK
	  and imm.RECORD_STATUS_CR_FK = 1)
 INNER JOIN epacube.DATA_NAME dn With (nolock) 
 on (imm.DATA_NAME_FK = dn.DATA_NAME_ID
 and dn.RECORD_STATUS_CR_FK  = 1)
 INNER JOIN epacube.DATA_SET DS with (nolock)
 on (ds.DATA_SET_ID = dn.DATA_SET_FK
 and TABLE_NAME = 'PRODUCT_MULT_TYPE')
		where ird.import_package_fk = @v_import_package_fk
		and ird.job_fk = @in_job_fk 


	  update temp
	  set data_value =  (SELECT top 1 [epacube].[getValueFromImportData] (ird.IMPORT_RECORD_DATA_ID, temp.POSITION) AS dataposition  )      
	  from import.import_record_data ird with (nolock)
	  inner join #TS_DATA_NAMES temp on (temp.import_record_data_id = ird.import_record_data_id)

	  delete from #TS_DATA_NAMES where DATA_VALUE is NULL
	 

 while EXISTS( select * from #TS_DATA_NAMES)

 BEGIN



DECLARE 
               @vm$file_name varchar(100),
               @vm$record_no varchar(64),         
               @vm$job_fk int,
               @vm$import_package_fk bigint,                                           
               @vm$data_name varchar(64),
			   @vm$data_name_value varchar(max)

 	DECLARE cur_v_import CURSOR local for
	SELECT  ird.IMPORT_FILENAME
			   ,ird.RECORD_NO
			   ,ird.job_fk
			   ,ird.IMPORT_PACKAGE_FK
			   ,temp.NAME
			   ,[epacube].[getValueFromImportData] (IRD.import_record_data_id, temp.POSITION) AS dataposition        
	  from import.import_record_data ird with (nolock)
	  inner join #TS_DATA_NAMES temp on (temp.import_record_data_id = ird.import_record_data_id)
		   

		   

		   
		   OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO @vm$file_name,
                                           @vm$record_no,         
                                           @vm$job_fk,
                                           @vm$import_package_fk,                                           
                                           @vm$data_name,
										   @vm$data_name_value
		
		


		INSERT INTO [import].[IMPORT_RECORD_NAME_VALUE]
           ([IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[JOB_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[DATA_NAME]
           ,[DATA_VALUE]
           )
		select
			 @vm$file_name,
             @vm$record_no,         
             @vm$job_fk,
             @vm$import_package_fk,                                           
             @vm$data_name,
			 ss.ListItem
		from utilities.SplitString ( @vm$data_name_value, ',' ) ss	 

	delete from #TS_DATA_NAMES  
	 where cast(IMPORT_RECORD_DATA_ID as varchar(50)) +  name  = (select top 1 cast(IMPORT_RECORD_DATA_ID as varchar(50)) +  name from #TS_DATA_NAMES)   
	
	 
	 

 FETCH NEXT FROM cur_v_import Into @vm$file_name,
                                   @vm$record_no,         
                                   @vm$job_fk,
                                   @vm$import_package_fk,                                           
                                   @vm$data_name,
								   @vm$data_name_value


						
CLOSE cur_v_import
DEALLOCATE cur_v_import   


END -- 

------------------------------------------------------
---CALL STAGE TO EVENTS
------------------------------------------------------
     SET @V_START_TIMESTAMP = getdate()			


			 EXEC[stage].[STG_TO_EVENTS] @IN_JOB_FK
	

      SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'STAGE TO EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



-------------------------------------------------------------------------------------
-- LAST Step is to Set Job Execution to Complete
-------------------------------------------------------------------------------------
----
----

      SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT PRODUCTS TO STAGE' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT PRODUCTS RESOLVED' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------
		update common.job
		set job_complete_timestamp = getdate()
		where job_id = @in_job_fk



--drop temp table if it exists
	IF object_id('tempdb..#TS_DISTINCT_PRODUCTS') is not null
	   drop table #TS_DISTINCT_PRODUCTS;
	   



 SET @status_desc =  'finished execution of stage.IMPORT_PRODUCT_TRANSFORMATION'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of stage.IMPORT_PRODUCT_TRANSFORMATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END





