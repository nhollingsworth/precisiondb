﻿--A_Update_SP_Combine_Common_tbls

/****** Object:  StoredProcedure [import].[util_Find_Truncating_Data]    Script Date: 04/09/2012 12:45:30 ******/
CREATE PROCEDURE [import].[util_Com_Cols_Temp_Phys_tbls] (
@Table_Name_Physical Varchar(64),
@Table_Name_Temp Varchar(64)
   )
   AS
BEGIN

--Declare @Table_Name_Physical Varchar(64)
--Declare @Table_Name_Temp Varchar(64)
--
--Set @Table_Name_Physical = 'MAUI_Basis_Archive'
--Set @Table_Name_Temp = '##MA'

Declare @Column_Name Varchar(64)
Declare @Data_Type Varchar(64)
Declare @Position Varchar(64)
Declare @Qry Varchar(max)
Declare @Qry1a Varchar(max)
Declare @Qry1b Varchar(max)
Declare @Qry1c Varchar(max)
Declare @Qry1d Varchar(max)

Declare @Qry2 Varchar(Max)
Declare @Qry2a Varchar(Max)
Declare @Qry2b Varchar(Max)
Declare @Qry2c Varchar(Max)
Declare @Qry2d Varchar(Max)
Declare @Qry3 Varchar(Max)
Declare @ColCount Numeric(10, 2)

IF object_id('tempdb..##DataLengths') is not null
drop table ##DataLengths

IF object_id('tempdb..##tmpProblems') is not null
drop table ##tmpProblems

Set @ColCount = (Select Max(Ordinal_Position) from tempdb.information_schema.columns where table_name = @Table_Name_Temp)

Set @Qry = 'Select'
Set @Qry2 = 'Select'
Set @Qry1a = ''
Set @Qry1b = ''
Set @Qry1c = ''
Set @Qry1d = ''
Set @Qry2a = ''
Set @Qry2b = ''
Set @Qry2c = ''
Set @Qry2d = ''

Declare Sel1 Cursor local for
Select tsc.Column_Name, tsc.Data_Type, tsc.Ordinal_Position 
from tempdb.information_schema.columns tsc 
inner join information_schema.columns sc on tsc.column_name = sc.column_Name
where tsc.table_name = @Table_Name_Temp and sc.table_name = @Table_Name_Physical
and tsc.Ordinal_Position <= @ColCount / 4
	
OPEN Sel1;
FETCH NEXT FROM Sel1 INTO @Column_Name, @Data_Type, @Position
    
WHILE @@FETCH_STATUS = 0
Begin

Set @Qry1a = @Qry1a + ' [' + @Column_Name + ']' + ',' 

FETCH NEXT FROM Sel1 INTO @Column_Name, @Data_Type, @Position
End
Close Sel1;
Deallocate Sel1;

---------

Declare Sel2 Cursor local for
Select tsc.Column_Name, tsc.Data_Type, tsc.Ordinal_Position 
from tempdb.information_schema.columns tsc 
inner join information_schema.columns sc on tsc.column_name = sc.column_Name
where tsc.table_name = @Table_Name_Temp and sc.table_name = @Table_Name_Physical 
and tsc.Ordinal_Position > @ColCount / 4 and tsc.Ordinal_Position <= @ColCount / 4 * 2
	
OPEN Sel2;
FETCH NEXT FROM Sel2 INTO @Column_Name, @Data_Type, @Position
    
WHILE @@FETCH_STATUS = 0
Begin

Set @Qry1b = @Qry1b +  + ' [' + @Column_Name + ']' + ','

FETCH NEXT FROM Sel2 INTO @Column_Name, @Data_Type, @Position
End
Close Sel2;
Deallocate Sel2;

---------

Declare Sel3 Cursor local for
Select tsc.Column_Name, tsc.Data_Type, tsc.Ordinal_Position 
from tempdb.information_schema.columns tsc 
inner join information_schema.columns sc on tsc.column_name = sc.column_Name
where tsc.table_name = @Table_Name_Temp and sc.table_name = @Table_Name_Physical 
and tsc.Ordinal_Position > @ColCount / 4 * 2 and tsc.Ordinal_Position <= @ColCount / 4 * 3
	
OPEN Sel3;
FETCH NEXT FROM Sel3 INTO @Column_Name, @Data_Type, @Position
    
WHILE @@FETCH_STATUS = 0
Begin

Set @Qry1c = @Qry1c +  + ' [' + @Column_Name + ']' + ','

FETCH NEXT FROM Sel3 INTO @Column_Name, @Data_Type, @Position
End
Close Sel3;
Deallocate Sel3;

---------

Declare Sel4 Cursor local for
Select tsc.Column_Name, tsc.Data_Type, tsc.Ordinal_Position 
from tempdb.information_schema.columns tsc 
inner join information_schema.columns sc on tsc.column_name = sc.column_Name
where tsc.table_name = @Table_Name_Temp and sc.table_name = @Table_Name_Physical 
and tsc.Ordinal_Position > @ColCount / 4 * 3 
	
OPEN Sel4;
FETCH NEXT FROM Sel4 INTO @Column_Name, @Data_Type, @Position
    
WHILE @@FETCH_STATUS = 0
Begin

Set @Qry1d = @Qry1d +  + ' [' + @Column_Name + ']' + ','

FETCH NEXT FROM Sel4 INTO @Column_Name, @Data_Type, @Position
End
Close Sel4;
Deallocate Sel4;

Set @Qry = Left(isnull(@Qry1a, '') + isnull(@Qry1b, '') + isnull(@Qry1c, '') + isnull(@Qry1d, ''), Len(isnull(@Qry1a, '') + isnull(@Qry1b, '') + isnull(@Qry1c, '') + isnull(@Qry1d, '')) - 1)
--
Set @Qry = 'Insert Into ' + @Table_Name_Physical + ' (' + @Qry + ') Select ' + @Qry + ' From ' + @Table_Name_Temp

Exec (@Qry)

END
