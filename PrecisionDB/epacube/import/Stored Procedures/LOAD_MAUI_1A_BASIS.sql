﻿--A_epaMAUI_LOAD_MAUI_1A_BASIS

---- =============================================
---- Author:		Gary Stone
---- Create date: 9/7/2011
---- Description:	MAUI DATA LOAD
---- =============================================
CREATE PROCEDURE [import].[LOAD_MAUI_1A_BASIS] 
@Job_FK Bigint, @Job_FK_i Numeric(10, 2)
AS
BEGIN

	SET NOCOUNT ON;

--Declare @Job_FK bigint
--Declare @Job_FK_i as Numeric(10, 2)
--set @Job_FK = 2169
--Set @Job_FK_i = 2168.01

Declare @ERP Varchar(32)
Declare @Sheet_Results_DN_FK Bigint
Declare @Job_FK_Fut Bigint

Set @ERP = (Select Value from epacube.epacube_params with (nolock) where name = 'ERP HOST')
Set @Job_FK_Fut = isnull((Select top 1 Job_fk_Fut from dbo.maui_parameters_jobs with (nolock) where what_if = 0 and job_fk_i = @Job_FK_i), 0)
Set @Sheet_Results_DN_FK = (select data_name_fk from dbo.MAUI_Config_ERP where usage = 'Data_Name' and epaCUBE_Column = 'Sheet_Results' and ERP = @ERP)

IF object_id('tempdb..##TMRA') is not null
drop table ##TMRA
IF object_id('tempdb..##MB') is not null
drop table ##MB
IF object_id('tempdb..##Prices') is not null
drop table ##Prices
IF object_id('tempdb..##rbt_cb') is not null
drop table ##rbt_cb
IF object_id('tempdb..##rbt_buy') is not null
drop table ##rbt_buy
IF object_id('tempdb..##Prods') is not null
Drop Table ##Prods
IF object_id('tempdb..##Custs_Vndr') is not null
drop table ##Custs_Vndr
IF object_id('tempdb..##Names') is not null
drop table ##Names
IF object_id('tempdb..##Inside') is not null
drop table ##Inside
IF object_id('tempdb..##Outside') is not null
drop table ##Outside
IF object_id('tempdb..##Order_Taken_By') is not null
drop table ##Order_Taken_By

Select Identifier, Identifier_ODH, result_type_cr_fk, product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk, isnull(supl_entity_structure_fk, 0) supl_entity_structure_fk, sell_price_cust_rules_fk, Rebate_CB_Rules_FK, Rebate_Buy_Rules_FK, Price_Operand, spc_uom into ##TMRA from ##MarginResultsAggregated
group by Identifier, Identifier_ODH, result_type_cr_fk, product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk, isnull(supl_entity_structure_fk, 0), sell_price_cust_rules_fk, Rebate_CB_Rules_FK, Rebate_Buy_Rules_FK, Price_Operand, spc_uom

Create index idx_tmra1 on ##TMRA(Identifier, Identifier_ODH, result_type_cr_fk, product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk, supl_entity_structure_fk, sell_price_cust_rules_fk, Rebate_CB_Rules_FK, Rebate_Buy_Rules_FK, Price_Operand)

Select salesperson_id_inside_host, Cast(ODH.product_structure_fk as Varchar(32)) + '-' + Cast(ODH.Org_Entity_structure_fk as Varchar(32)) + '-' + Cast(ODH.Cust_Entity_structure_fk as Varchar(32)) Identifier_ODH
into ##Inside
from marginmgr.order_data_history ODH with (nolock)
Inner Join
(
Select Min(Order_Data_ID) Min_Order_Data_ID, Min(Drank) MinDRank, product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk
from marginmgr.order_data_history with (nolock)
where salesperson_id_inside_host is not null
Group by product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk
) A on ODH.Product_Structure_fk = A.Product_Structure_FK and ODH.Org_Entity_Structure_FK = A.Org_entity_structure_fk and ODH.Cust_Entity_Structure_FK = A.Cust_entity_structure_fk
and ODH.Order_Data_ID = A.Min_Order_Data_ID

Create Index idx_ins on ##Inside(Identifier_ODH)

Select salesperson_id_Outside_host, Cast(ODH.product_structure_fk as Varchar(32)) + '-' + Cast(ODH.Org_Entity_structure_fk as Varchar(32)) + '-' + Cast(ODH.Cust_Entity_structure_fk as Varchar(32)) Identifier_ODH
into ##Outside
from marginmgr.order_data_history ODH with (nolock)
Inner Join
(
Select Min(Order_Data_ID) Min_Order_Data_ID, Min(Drank) MinDRank, product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk
from marginmgr.order_data_history with (nolock)
where salesperson_id_Outside_host is not null
Group by product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk
) A on ODH.Product_Structure_fk = A.Product_Structure_FK and ODH.Org_Entity_Structure_FK = A.Org_entity_structure_fk and ODH.Cust_Entity_Structure_FK = A.Cust_entity_structure_fk
and ODH.Order_Data_ID = A.Min_Order_Data_ID

Create Index idx_out on ##Outside(Identifier_ODH)

Select Order_Taken_By, Cast(ODH.product_structure_fk as Varchar(32)) + '-' + Cast(ODH.Org_Entity_structure_fk as Varchar(32)) + '-' + Cast(ODH.Cust_Entity_structure_fk as Varchar(32)) Identifier_ODH
into ##Order_Taken_By
from marginmgr.order_data_history ODH with (nolock)
Inner Join
(
Select Min(Order_Data_ID) Min_Order_Data_ID, Min(Drank) MinDRank, product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk
from marginmgr.order_data_history with (nolock)
where Order_Taken_By is not null
Group by product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk
) A on ODH.Product_Structure_fk = A.Product_Structure_FK and ODH.Org_Entity_Structure_FK = A.Org_entity_structure_fk and ODH.Cust_Entity_Structure_FK = A.Cust_entity_structure_fk 
and ODH.Order_Data_ID = A.Min_Order_Data_ID

Create Index idx_ord on ##Order_Taken_By(Identifier_ODH)

Select
TMRA.identifier
, TMRA.result_type_cr_fk
,Isnull(Case @ERP When 'SXE' then rp_price.Host_Precedence_level + ': ' + Replace(rp_price.Host_Precedence_Level_Description, 'Customer Type', 'Cust Price Type') else rp_price.Host_Precedence_Level_Description end, 'Default Pricing') 'Price_Rule_Type'
,(Select name from synchronizer.rules_filter with (nolock) where rules_filter_id = ecr_price.cust_filter_fk) 'Sell_Price_Cust_Filter_Name'
,(Select value1 from synchronizer.rules_filter with (nolock) where rules_filter_id = ecr_price.cust_filter_fk) 'Sell_Price_Cust_Filter_Value'
,(Select name from synchronizer.rules_filter with (nolock) where rules_filter_id = ecr_price.prod_filter_fk) 'Sell_Price_Prod_Filter_Name'
,(Select value1 from synchronizer.rules_filter with (nolock) where rules_filter_id = ecr_price.prod_filter_fk) 'Sell_Price_Prod_Filter_Value'
, isnull(r_price.host_rule_xref, 'Default Pricing') 'Sell_Price_Cust_Host_Rule_Xref'
,Replace((Select Name from epacube.data_name with (nolock) where data_name_id = ra_price.Basis_Calc_DN_FK), 'ORDER COGS', 'REBATED COST') 'Sell_Price_Cust_Basis_1_Col'
,(Select Name from epacube.data_name with (nolock) where data_name_id = ra_price.Basis1_DN_FK) 'Sell_Price_Cust_Basis_2_Col'
,ecr_price.basis_calc 'Sell_Price_Cust_Basis_1_Value'
,ecr_price.basis1 'Sell_Price_Cust_Basis_2_Value'
,ecr_price.Number1 'Sell_Price_Cust_Number_1'
,r_price.Effective_date 'Sell_Price_Cust_Effective_Date'
,r_price.end_date 'Sell_Price_Cust_End_Date'
,Case When r_price.rules_id in (2116021, 2116022) then Ltrim(Rtrim(Replace(r_price.name, 'Default Pricing - ', '')))
	else Ltrim(Rtrim(Replace(Replace(Replace(Replace(Replace(Replace(Replace(rao1_price.name, '(N)', ''), '(O)', ''), '(D)', ''), '(L)', ''), 'SX Price ', ''), 'Margin', 'Profit'), '  ', ' '))) end 'Sell_Price_Cust_Operation'
, ins.salesperson_id_inside_host
, outs.salesperson_id_outside_host
, ord.Order_Taken_By
into ##Prices 
from ##TMRA TMRA
left join Synchronizer.event_calc ec_price with (nolock) on ec_price.product_structure_fk = TMRA.product_structure_fk and ec_price.org_entity_structure_fk = TMRA.org_entity_structure_fk 
	and ec_price.cust_entity_structure_fk = TMRA.cust_entity_structure_fk and isnull(ec_price.supl_entity_structure_fk, 0) = isnull(TMRA.supl_entity_structure_fk, 0)
	and TMRA.result_type_cr_fk = ec_price.result_type_cr_fk and ec_price.result_data_name_fk = 111602 and ec_price.JOB_FK in (@Job_FK, @Job_FK_Fut)
left join Synchronizer.event_calc_rules ecr_price with (nolock) on ec_price.event_id = ecr_price.event_fk and ec_price.result_type_cr_fk = ecr_price.result_type_cr_fk 
	and ec_price.result_data_name_fk = ecr_price.result_data_name_fk and ecr_price.result_rank = 1
left join synchronizer.rules r_price with (nolock) on r_price.rules_id = TMRA.sell_price_cust_rules_fk
left join synchronizer.rules_action ra_price with (nolock) on ra_price.rules_fk = r_price.rules_id
left join synchronizer.rules_action_operation rao1_price with (nolock) on ra_price.rules_action_operation1_fk = rao1_price.rules_action_operation_id
Left Join epacube.entity_structure ES_C with (nolock) on ES_C.Entity_Structure_ID = TMRA.Cust_Entity_Structure_FK and ES_C.data_name_fk = 144020
Left Join epacube.entity_identification EI_C with (nolock) on ES_C.Entity_Structure_ID = EI_C.Entity_Structure_FK and EI_C.data_name_fk = 144111
Left Join epacube.entity_identification EI_CP with (nolock) on es_c.parent_entity_structure_fk = EI_CP.entity_structure_fk
Left Join synchronizer.rules_precedence rp_price with (nolock) on r_price.rules_precedence_fk = rp_price.rules_precedence_id
left join ##Inside ins on TMRA.Identifier_ODH = ins.identifier_ODH
left join ##Outside outs on TMRA.Identifier_ODH = outs.identifier_ODH
left join ##Order_Taken_By ord on TMRA.Identifier_ODH = ord.Identifier_ODH

create index idx_prices on ##Prices(Identifier, result_type_cr_fk)

--Rebate_CB_Info
Select
Identifier
,TMRA.result_type_cr_fk
,rp_rbt_cb.Host_Precedence_level + ': ' + rp_rbt_cb.Host_Precedence_Level_Description 'Rebate_CB_Rule_Type'
,(Select name from synchronizer.rules_filter with (nolock) where rules_filter_id = ecr_rbt_cb.cust_filter_fk) 'Rebate_CB_Cust_Filter_Name'
,(Select value1 from synchronizer.rules_filter with (nolock) where rules_filter_id = ecr_rbt_cb.cust_filter_fk) 'Rebate_CB_Cust_Filter_Value'
,(Select name from synchronizer.rules_filter with (nolock) where rules_filter_id = ecr_rbt_cb.prod_filter_fk) 'Rebate_CB_Prod_Filter_Name'
,(Select value1 from synchronizer.rules_filter with (nolock) where rules_filter_id = ecr_rbt_cb.prod_filter_fk) 'Rebate_CB_Prod_Filter_Value'
,(Select Name from epacube.data_name with (nolock) where data_name_id = ra_rbt_cb.Basis_Calc_DN_FK) 'Rebate_CB_Basis_1_Col'
,ecr_rbt_cb.basis_calc 'Rebate_CB_Basis_1_Value'
,ecr_rbt_cb.Number1 Rebate_CB_Number_1
,(Select Name from epacube.data_name with (nolock) where data_name_id = ra_rbt_cb.Basis1_DN_FK) 'Rebate_CB_Basis_2_Col'
,ecr_rbt_cb.basis1 'Rebate_CB_Basis_2_Value'
,ecr_rbt_cb.Number2 'Rebate_CB_Number_2'
,r_rbt_cb.Effective_date 'Rebate_CB_Effective_Date'
,r_rbt_cb.end_date 'Rebate_CB_End_Date'
,Ltrim(Rtrim(Replace(Replace(Replace(Replace(Replace(rao1_rbt_cb.name, '(N)', ''), '(O)', ''), '(D)', ''), '(L)', ''), '  ', ' '))) 'Rebate_CB_Operation'
, r_rbt_cb.host_rule_xref 'Rebate_CB_Host_Rule_Xref'

, Case ecr_rbt_cb.Rules_Action_Operation2_FK When 1294 then
		Case When ecr_rbt_cb.Basis_calc is null then ecr_rbt_cb.NUMBER2
			else isnull(ecr_rbt_cb.NUMBER2, 0) * isnull(ecr_rbt_cb.Basis1, 0) end end Sell_Price_Rebate_Cap
into ##Rbt_CB
from ##TMRA TMRA
left join Synchronizer.Event_Calc ec_rbt_cb with (nolock) on ec_rbt_cb.product_structure_fk = TMRA.product_structure_fk and ec_rbt_cb.org_entity_structure_fk = TMRA.org_entity_structure_fk 
	and ec_rbt_cb.cust_entity_structure_fk = TMRA.cust_entity_structure_fk and isnull(ec_rbt_cb.supl_entity_structure_fk, 0) = isnull(TMRA.supl_entity_structure_fk, 0)
	and TMRA.result_type_cr_fk = ec_rbt_cb.result_type_cr_fk and ec_rbt_cb.result_data_name_fk in (111501, 111401) and ec_rbt_cb.JOB_FK  in (@Job_FK, @Job_FK_Fut)
left join Synchronizer.Event_Calc_rules ecr_rbt_cb with (nolock) on ec_rbt_cb.event_id = ecr_rbt_cb.event_fk and ec_rbt_cb.result_type_cr_fk = ecr_rbt_cb.result_type_cr_fk 
	and ec_rbt_cb.result_data_name_fk = ecr_rbt_cb.result_data_name_fk and ecr_rbt_cb.result_rank = 1
left join synchronizer.rules r_rbt_cb with (nolock) on r_rbt_cb.rules_id = TMRA.rebate_cb_rules_fk
left join synchronizer.rules_action ra_rbt_cb with (nolock) on ra_rbt_cb.rules_fk = r_rbt_cb.rules_id
left join synchronizer.rules_action_operation rao1_rbt_cb with (nolock) on ra_rbt_cb.rules_action_operation1_fk = rao1_rbt_cb.rules_action_operation_id
Left Join synchronizer.rules_precedence rp_rbt_cb with (nolock) on r_rbt_cb.rules_precedence_fk = rp_rbt_cb.rules_precedence_id

create index idx_rbt_cb on ##rbt_cb(identifier, result_type_cr_fk)

--Rebate_BUY_Data
Select
Identifier
, ecr_rbt_BUY.result Rebate_BUY_Amt
, ecr_rbt_BUY.result_type_cr_fk
, rp_rbt_buy.Host_Precedence_level + ': ' + rp_rbt_buy.Host_Precedence_Level_Description 'Rebate_BUY_Rule_Type'
,(Select name from synchronizer.rules_filter with (nolock) where rules_filter_id = ecr_rbt_BUY.cust_filter_fk) 'Rebate_BUY_Cust_Filter_Name'
,(Select value1 from synchronizer.rules_filter with (nolock) where rules_filter_id = ecr_rbt_BUY.cust_filter_fk) 'Rebate_BUY_Cust_Filter_Value'
,(Select name from synchronizer.rules_filter with (nolock) where rules_filter_id = ecr_rbt_BUY.prod_filter_fk) 'Rebate_BUY_Prod_Filter_Name'
,(Select value1 from synchronizer.rules_filter with (nolock) where rules_filter_id = ecr_rbt_BUY.prod_filter_fk) 'Rebate_BUY_Prod_Filter_Value'
,(Select Name from epacube.data_name with (nolock) where data_name_id = ra_rbt_buy.Basis_Calc_DN_FK) 'Rebate_BUY_Basis_1_Col'
,ecr_rbt_BUY.basis_calc 'Rebate_BUY_Basis_1_Value'
,ecr_rbt_BUY.Number1 'Rebate_BUY_Number_1'
,(Select Name from epacube.data_name with (nolock) where data_name_id = ra_rbt_buy.Basis1_DN_FK) 'Rebate_BUY_Basis_2_Col'
,ecr_rbt_BUY.basis1 'Rebate_BUY_Basis_2_Value'
,ecr_rbt_BUY.Number2 'Rebate_BUY_Number_2'
,r_rbt_BUY.Effective_date 'Rebate_BUY_Effective_Date'
,r_rbt_BUY.end_date 'Rebate_BUY_End_Date'
,Ltrim(Rtrim(Replace(Replace(Replace(Replace(Replace(rao1_rbt_BUY.name, '(N)', ''), '(O)', ''), '(D)', ''), '(L)', ''), '  ', ' '))) 'Rebate_BUY_Operation'
, r_rbt_BUY.host_rule_xref 'Rebate_BUY_Host_Rule_Xref'
into ##rbt_buy
from ##TMRA TMRA
left join Synchronizer.Event_Calc ec_rbt_buy with (nolock) on ec_rbt_buy.product_structure_fk = TMRA.product_structure_fk and ec_rbt_buy.org_entity_structure_fk = TMRA.org_entity_structure_fk 
	and ec_rbt_buy.cust_entity_structure_fk = TMRA.cust_entity_structure_fk and isnull(ec_rbt_buy.supl_entity_structure_fk, 0) = isnull(TMRA.supl_entity_structure_fk, 0)
	and TMRA.result_type_cr_fk = ec_rbt_buy.result_type_cr_fk and ec_rbt_buy.result_data_name_fk = 111502 and ec_rbt_buy.JOB_FK  in (@Job_FK, @Job_FK_Fut)
left join Synchronizer.Event_Calc_rules ecr_rbt_buy with (nolock) on ec_rbt_buy.event_id = ecr_rbt_buy.event_fk and ec_rbt_buy.result_type_cr_fk = ecr_rbt_buy.result_type_cr_fk 
	and ec_rbt_buy.result_data_name_fk = ecr_rbt_buy.result_data_name_fk and ecr_rbt_buy.result_rank = 1
left join synchronizer.rules r_rbt_buy with (nolock) on r_rbt_buy.rules_id = TMRA.rebate_buy_rules_fk
left join synchronizer.rules_action ra_rbt_buy with (nolock) on ra_rbt_buy.rules_fk = r_rbt_buy.rules_id
left join synchronizer.rules_action_operation rao1_rbt_buy with (nolock) on ra_rbt_buy.rules_action_operation1_fk = rao1_rbt_buy.rules_action_operation_id
Left Join synchronizer.rules_precedence rp_rbt_buy with (nolock) on r_rbt_buy.rules_precedence_fk = rp_rbt_buy.rules_precedence_id

create index idx_rbt_buy on ##rbt_buy(Identifier, result_type_cr_fk)

Select
Identifier
,TMRA.result_type_cr_fk
,pi_i.value Product_ID
,Case When isnull(pi_d2.[description],'') = '' then pi_d1.[description] else pi_d1.[description] + '-' + pi_d2.[description] end Prod_Description

, (select value from epacube.data_value dv where data_value_id = (select top 1 data_value_fk from epacube.PRODUCT_CATEGORY PG1
	where PG1.Product_Structure_FK = TMRA.Product_Structure_FK and (PG1.Org_Entity_Structure_FK = TMRA.ORG_ENTITY_STRUCTURE_FK or PG1.Org_Entity_Structure_FK = 1)
	and PG1.Data_Name_FK = (select data_name_fk from dbo.MAUI_Config_ERP with (nolock) where usage = 'Product_Grouping' and epaCUBE_Column = 'Prod_Group1' and ERP = @ERP) order by PG1.Org_Entity_Structure_FK Desc)) 'Prod_Group1'
, (select value from epacube.data_value dv where data_value_id = (select top 1 data_value_fk from epacube.PRODUCT_CATEGORY PG2
	where PG2.Product_Structure_FK = TMRA.Product_Structure_FK and (PG2.Org_Entity_Structure_FK = TMRA.ORG_ENTITY_STRUCTURE_FK or PG2.Org_Entity_Structure_FK = 1)
	and PG2.Data_Name_FK = (select data_name_fk from dbo.MAUI_Config_ERP with (nolock) where usage = 'Product_Grouping' and epaCUBE_Column = 'Prod_Group2' and ERP = @ERP) order by PG2.Org_Entity_Structure_FK Desc)) 'Prod_Group2'
, (select value from epacube.data_value dv where data_value_id = (select top 1 data_value_fk from epacube.PRODUCT_CATEGORY PG3
	where PG3.Product_Structure_FK = TMRA.Product_Structure_FK and (PG3.Org_Entity_Structure_FK = TMRA.ORG_ENTITY_STRUCTURE_FK or PG3.Org_Entity_Structure_FK = 1)
	and PG3.Data_Name_FK = (select data_name_fk from dbo.MAUI_Config_ERP with (nolock) where usage = 'Product_Grouping' and epaCUBE_Column = 'Prod_Group3' and ERP = @ERP) order by PG3.Org_Entity_Structure_FK Desc)) 'Prod_Group3'
, (select value from epacube.data_value dv where data_value_id = (select top 1 data_value_fk from epacube.PRODUCT_CATEGORY PG4
	where PG4.Product_Structure_FK = TMRA.Product_Structure_FK and (PG4.Org_Entity_Structure_FK = TMRA.ORG_ENTITY_STRUCTURE_FK or PG4.Org_Entity_Structure_FK = 1)
	and PG4.Data_Name_FK = (select data_name_fk from dbo.MAUI_Config_ERP with (nolock) where usage = 'Product_Grouping' and epaCUBE_Column = 'Prod_Group4' and ERP = @ERP) order by PG4.Org_Entity_Structure_FK Desc)) 'Prod_Group4'
, (select value from epacube.data_value dv where data_value_id = (select top 1 data_value_fk from epacube.PRODUCT_CATEGORY PG5
	where PG5.Product_Structure_FK = TMRA.Product_Structure_FK and (PG5.Org_Entity_Structure_FK = TMRA.ORG_ENTITY_STRUCTURE_FK or PG5.Org_Entity_Structure_FK = 1)
	and PG5.Data_Name_FK = (select data_name_fk from dbo.MAUI_Config_ERP with (nolock) where usage = 'Product_Grouping' and epaCUBE_Column = 'Prod_Group5' and ERP = @ERP) order by PG5.Org_Entity_Structure_FK Desc)) 'Prod_Group5'

,piu.value 'UPC'
,dv_k.value 'Kit Type'
,uc.uom_code 'Stk_UOM'
into ##Prods
from ##TMRA TMRA
Left Join epacube.product_identification pi_i with (nolock) on pi_i.Product_Structure_FK = TMRA.Product_Structure_FK and data_name_fk = 
		( select data_name_fk from dbo.maui_config_erp with (nolock) where usage = 'data_name' and epaCUBE_Column = 'Product_ID' and ERP = @ERP ) --ProductID
left join epacube.product_description pi_d1 with (nolock) on pi_d1.Product_Structure_FK = TMRA.Product_Structure_FK and pi_d1.data_name_fk = 110401 --Description1
left join epacube.product_description pi_d2 with (nolock) on pi_d2.Product_Structure_FK = TMRA.Product_Structure_FK and pi_d2.data_name_fk = 110402 --Description2
left join epacube.product_identification Piu with (nolock) on Piu.Product_Structure_fk = TMRA.product_structure_fk and Piu.data_name_fk = 110102 --UPC
left join epacube.PRODUCT_ATTRIBUTE Pak with (nolock) on Pak.Product_Structure_FK = TMRA.Product_Structure_FK and Pak.Data_Name_FK = 210820  --Kit Type
left join epacube.data_value dv_k with (nolock) on dv_k.data_value_id = Pak.data_value_fk
left join epacube.PRODUCT_UOM_CLASS UOM with (nolock) on UOM.Product_Structure_FK = TMRA.Product_Structure_FK and UOM.Data_Name_FK = 110603 
left join epacube.uom_code UC with (nolock) on  UC.UOM_Code_ID = UOM.UOM_Code_FK 

Create index idx_prods on ##Prods(Identifier, result_type_cr_fk)

Select
Identifier
, result_type_cr_fk
, CustomerID_BT
, CustomerID_ST
, ein_p.value 'CustomerID_BT_Name'
, ein_c.value 'CustomerID_ST_Name'
, Whse
, Whse_Record
, SalesRep_Record_In
, SalesRep_Record_Out
, Org_Vendor_ID
, [Org Vendor Name]
, Replacement_Cost
, Standard_Cost
, Base_Price
, List_Price
, Vendor_Rebate_Cost
, Cust_Group1
, Cust_Group2
, Cust_Group3
into ##Custs_Vndr
from 
(Select
Identifier
,result_type_cr_fk
,EI_C.Value 'CustomerID_ST'
,isnull(EI_CP.value, ei.value) 'CustomerID_BT'
, EI_C.entity_structure_fk ST_Entity_Structure_FK
,isnull(EI_CP.entity_structure_fk, ei.entity_structure_fk) BT_Entity_Structure_FK
,eiw.value 'Whse'
,ei_vo.value 'Org_Vendor_ID'
,EIN_vno.value 'Org Vendor Name'
,SR.NET_VALUE1 'Replacement_Cost'
,SR.NET_VALUE2 'Standard_Cost'
,SR.NET_VALUE3 'Base_Price'
,SR.NET_VALUE4 'List_Price'
,SR.NET_VALUE5 'Vendor_Rebate_Cost'
,dv_cat1.value 'Cust_Group1'
,dv_cat2.value 'Cust_Group2'
,dv_cat3.value 'Cust_Group3'
,(Select Attribute_Event_Data from epacube.ENTITY_ATTRIBUTE where ENTITY_STRUCTURE_FK = TMRA.cust_entity_structure_fk and DATA_NAME_FK = 144900) 'Whse_Record'
,(Select Attribute_Event_Data from epacube.ENTITY_ATTRIBUTE where ENTITY_STRUCTURE_FK = TMRA.cust_entity_structure_fk and DATA_NAME_FK = 145010) 'SalesRep_Record_In'
,(Select Attribute_Event_Data from epacube.ENTITY_ATTRIBUTE where ENTITY_STRUCTURE_FK = TMRA.cust_entity_structure_fk and DATA_NAME_FK = 145020) 'SalesRep_Record_Out'
from ##TMRA TMRA
left join epacube.ENTITY_CATEGORY ec1 with (nolock) on ec1.entity_structure_fk = TMRA.cust_entity_structure_fk and ec1.data_name_fk = (select isnull(data_name_fk, 0) from dbo.MAUI_Config_ERP with (nolock) 
			where usage = 'Customer_Grouping' and epaCUBE_Column = 'Cust_Group1' and ERP = @ERP)
left join epacube.data_value dv_cat1 with (nolock) 
	on ec1.data_name_fk = 
		dv_cat1.data_name_fk
	and ec1.DATA_VALUE_FK = 
		dv_cat1.DATA_VALUE_ID
left join epacube.ENTITY_CATEGORY ec2 with (nolock) on ec2.entity_structure_fk = TMRA.cust_entity_structure_fk and ec2.data_name_fk = (select isnull(data_name_fk, 0) from dbo.MAUI_Config_ERP with (nolock) 
			where usage = 'Customer_Grouping' and epaCUBE_Column = 'Cust_Group2' and ERP = @ERP)
left join epacube.data_value dv_cat2 with (nolock) 
	on ec2.data_name_fk = 
		dv_cat2.data_name_fk
	and ec2.DATA_VALUE_FK = 
		dv_cat2.DATA_VALUE_ID
left join epacube.ENTITY_CATEGORY ec3 with (nolock) on ec3.entity_structure_fk = TMRA.cust_entity_structure_fk and ec3.data_name_fk = (select isnull(data_name_fk, 0) from dbo.MAUI_Config_ERP with (nolock) 
			where usage = 'Customer_Grouping' and epaCUBE_Column = 'Cust_Group3' and ERP = @ERP)
left join epacube.data_value dv_cat3 with (nolock) 
	on ec3.data_name_fk = 
		dv_cat3.data_name_fk
	and ec3.DATA_VALUE_FK = 
		dv_cat3.DATA_VALUE_ID
left join epacube.entity_structure es with (nolock) on es.entity_structure_id = TMRA.cust_entity_structure_fk and es.data_name_fk = 144010
left join epacube.entity_identification ei with (nolock) on es.entity_structure_id = ei.entity_structure_fk and ei.entity_data_name_fk = 144010
left join epacube.entity_identification eiw with (nolock) on eiw.entity_data_name_fk = 141000 and eiw.entity_structure_fk = TMRA.ORG_ENTITY_STRUCTURE_FK and eiw.data_name_fk = 141111 --whse
left join epacube.Entity_Identification ei_vo with (nolock) on isnull(ei_vo.entity_structure_fk, 0) = isnull(TMRA.SUPL_ENTITY_STRUCTURE_FK, 0) and ei_vo.Data_Name_FK = 143110
	left join epacube.product_association pa_vo with (nolock) on ei_vo.Entity_Structure_FK = pa_vo.entity_Structure_fk --Org_Vendor_ID
	AND pa_vo.Org_entity_structure_fk = TMRA.Org_Entity_Structure_FK AND pa_vo.DATA_NAME_FK = 253501
	And pa_vo.Product_Structure_FK = TMRA.Product_Structure_FK 
Left Join epacube.entity_structure ES_C with (nolock) on ES_C.Entity_Structure_ID = TMRA.Cust_Entity_Structure_FK and ES_C.data_name_fk = 144020
Left Join epacube.entity_identification EI_C with (nolock) on ES_C.Entity_Structure_ID = EI_C.Entity_Structure_FK and EI_C.data_name_fk = 144111
Left Join epacube.entity_identification EI_CP with (nolock) on es_c.parent_entity_structure_fk = EI_CP.entity_structure_fk
Left Join marginmgr.SHEET_RESULTS_VALUES_FUTURE SR with (nolock) on SR.Product_Structure_FK = TMRA.Product_Structure_FK And SR.Org_Entity_Structure_FK = TMRA.Org_Entity_Structure_FK
	and SR.DATA_NAME_FK = @Sheet_Results_DN_FK
left join epacube.ENTITY_IDENT_NONUNIQUE EIN_vno with (nolock) on isnull(EIN_vno.entity_structure_fk, 0) = isnull(TMRA.SUPL_ENTITY_STRUCTURE_FK, 0) and EIN_vno.Data_Name_FK = 143112 --Org_Vendor_Name
	left join epacube.product_association PA_vno with (nolock) on EIN_vno.Entity_Structure_FK = PA_vno.entity_Structure_fk AND PA_vno.Org_entity_structure_fk = TMRA.Org_Entity_Structure_FK
	And PA_vno.Product_Structure_FK = TMRA.Product_Structure_FK
	and SR.Data_Name_FK = 231100) Csts
left join epacube.entity_ident_nonunique ein_c with (nolock) on Csts.ST_Entity_Structure_FK = ein_c.entity_structure_fk and ein_c.entity_data_name_fk = 144020 and ein_c.data_name_fk = 144112
left join epacube.entity_ident_nonunique ein_p with (nolock) on Csts.BT_Entity_Structure_FK = ein_p.entity_structure_fk and ein_p.entity_data_name_fk = 144010 and ein_p.data_name_fk = 144112	

Create Index idx_custs on ##Custs_Vndr(Identifier, result_type_cr_fk)

	Select
	@Job_FK Job_FK
	,@Job_FK_i Job_FK_i
	,Case TMRA.result_type_cr_fk When 711 then 'New' else 'Current' end Calc_Status
	,Null 'GroupingCustom'
	,Null 'GroupingCustomDescription'
	,Sales_History_Start_Date
	,Sales_History_End_Date
	,Cast(Sales_History_End_Date - Sales_History_Start_Date as Numeric(6,0)) Sales_History_Days
	,Sales_Qty / Cast(Sales_History_End_Date - Sales_History_Start_Date as Numeric(6,0)) Sales_Qty_Per_Day
	,Product_ID
	,Prod_Description
	,CustomerID_ST
	,CustomerID_ST_Name
	,CustomerID_BT
	,CustomerID_BT_Name
	,Whse
	,Whse_Record
	,SalesRep_Record_In
	,SalesRep_Record_Out
	,prc.salesperson_id_inside_host 'Sales_Rep_Inside_ID'
	,Null 'Sales_Rep_Inside_Name'
	,prc.salesperson_id_outside_host 'Sales_Rep_Outside_ID'
	,Null 'Sales_Rep_Outside_Name'
	,prc.Order_Taken_By
	,prods.[Prod_Group1]
	,prods.[Prod_Group2]
	,prods.[Prod_Group3]
	,prods.[Prod_Group4]
	,prods.[Prod_Group5]
	,Case @ERP When 'SXE' then TMRA.[Customer Type] When 'Eclipse' then custs.Cust_Group1 end 'Cust_Group1'
	,Case @ERP When 'SXE' then TMRA.[Cust_Price_Type] When 'Eclipse' then custs.Cust_Group2 end 'Cust_Group2'
	,Case @ERP When 'SXE' then TMRA.[Cust_Rebate_Type] When 'Eclipse' then custs.Cust_Group3 end 'Cust_Group3'
	,[UPC]
	,[Kit Type]
	,[Stk_UOM]
	,TMRA.SPC_UOM
	,Sales_Qty
	,TMRA.Price_Operand 'Price_Operand_Pos'
	,TMRA.Discount_Operand 'Discount_Operand_Pos'
	,Price_Rule_Type
	,Rebate_CB_Rule_Type
	,Rebate_BUY_Rule_Type
	,Sell_Price_Cust_Filter_Name
	,Sell_Price_Cust_Filter_Value
	,Sell_Price_Prod_Filter_Name
	,Sell_Price_Prod_Filter_Value
	,Sell_Price_Cust_Rules_FK
	,Sell_Price_Cust_Host_Rule_Xref
	,TMRA.sell_Price_cust_host_rule_xref_Hist
	,Sell_Price_Cust_Basis_1_Col
	,Sell_Price_Cust_Basis_2_Col
	,Sell_Price_Cust_Basis_1_Value
	,Sell_Price_Cust_Basis_2_Value
	,Sell_Price_Cust_Number_1
	,Sell_Price_Cust_Effective_Date
	,Sell_Price_Cust_End_Date
	,Sell_Price_Cust_Operation
	,Sell_Price_Cust_Amt
	,Margin_Cost_Basis_Amt
	,Margin_Cost_Basis_Effective_Date
	,Margin_Amt
	,Margin_Pct
	,Cast(Null as Numeric(18, 6)) Margin_Cost_Basis_Amt_2
	,Cast(Null as datetime) Margin_Cost_Basis_Effective_Date_2
	,Cast(Null as Numeric(18, 6)) Margin_Cost_Basis_Amt_3
	,Cast(Null as datetime) Margin_Cost_Basis_Effective_Date_3
	,Cast(Null as Numeric(18, 6)) Margin_Cost_Basis_Amt_4
	,Cast(Null as datetime) Margin_Cost_Basis_Effective_Date_4
	,Cast(Null as Numeric(18, 6)) Margin_Cost_Basis_Amt_5
	,Cast(Null as datetime) Margin_Cost_Basis_Effective_Date_5
	,Cast(Null as Numeric(18, 6)) Margin_Cost_Basis_Amt_6
	,Cast(Null as datetime) Margin_Cost_Basis_Effective_Date_6
	,Cast(Null as datetime) Contracted_Next_Cust_Change_Date_1
	,Cast(Null as datetime) Contracted_Next_Cust_Change_Date_2
	,Cast(Null as datetime) Contracted_Next_Cust_Change_Date_3
	,Cast(Null as datetime) Contracted_Next_Cust_Change_Date_4
	,Cast(Null as datetime) Contracted_Next_Cust_Change_Date_5
	,Cast(Null as datetime) Contracted_Next_Cust_Change_Date_6
	,(Select Name from epacube.data_name with (nolock) where data_name_id = TMRA.Margin_Cost_Basis_DN_FK) 'Cost_Basis_Col'
	,Rebate_CB_Cust_Filter_Name
	,Rebate_CB_Cust_Filter_Value
	,Rebate_CB_Prod_Filter_Name
	,Rebate_CB_Prod_Filter_Value
	,Rebate_CB_Basis_1_Col
	,Rebate_CB_Basis_1_Value
	,Rebate_CB_Number_1
	,Rebate_CB_Basis_2_Col
	,Rebate_CB_Basis_2_Value
	,Rebate_CB_Number_2
	,Rebate_CB_Effective_Date
	,Rebate_CB_End_Date
	,Rebate_CB_Operation
	,Rebate_CB_Rules_FK
	,Rebate_CB_Host_Rule_Xref
	,TMRA.rebate_cb_host_rule_xref_Hist
	,Rebate_CB_Amt
	,Rebated_Cost_CB_Amt
	,Sell_Price_Rebate_Cap
	,Rebate_BUY_Cust_Filter_Name
	,Rebate_BUY_Cust_Filter_Value
	,Rebate_BUY_Prod_Filter_Name
	,Rebate_BUY_Prod_Filter_Value
	,Rebate_BUY_Basis_1_Col
	,Rebate_BUY_Basis_1_Value
	,Rebate_BUY_Number_1
	,Rebate_BUY_Basis_2_Col
	,Rebate_BUY_Basis_2_Value
	,Rebate_BUY_Number_2
	,Rebate_BUY_Effective_Date
	,Rebate_BUY_End_Date
	,Rebate_BUY_Operation
	,Rebate_BUY_Rules_FK
	,Rebate_BUY_Host_Rule_Xref
	,TMRA.rebate_buy_host_rule_xref_Hist
	,BUY.Rebate_Buy_Amt 
	,Case When @ERP = 'SXE' then Org_Vendor_ID When @ERP = 'Eclipse' then Prods.Prod_Group4 end Org_Vendor_ID
	,[Org Vendor Name]
	,TMRA.PRODUCT_STRUCTURE_FK
	,TMRA.ORG_ENTITY_STRUCTURE_FK
	,TMRA.CUST_ENTITY_STRUCTURE_FK
	,isnull(TMRA.SUPL_ENTITY_STRUCTURE_FK, 0) supl_entity_structure_fk
	,Sales_Qty Sales_Qty_Hist
	,Sales_Ext_Hist
	,Prod_Cost_Ext_Hist
	,Rebate_Amt_CB_Hist
	,Rebate_Amt_BUY_Hist
	,[Last Override Invoice Date]
	,Cast([last Override Price] as Numeric(18, 6)) 'last Override Price'
	,Cast([Last Override Cost] as Numeric(18, 6)) 'Last Override Cost'
	,[Override Qty]
	,[Override Transactions]
	,[Total Transactions]
	,Left(Contract_No_Price, 64) Contract_No_Price
	,Contract_No_Rebate_CB
	,Contract_No_Rebate_Buy
	, Qty_Break_Rule_Ind
	, Null Rebate_Vendor_Incentive_Percent
	, Null Rebate_Incentive_Amt_Hist
	, Null Rebate_Incentive_Amt_Cur
	, Null Rebate_Incentive_Amt_New
	,[Sell_Price_Override_Hist_Avg]
	,[Unit_Cost_Of_Goods_Override_Hist_Avg]
	,Cast(Cast(sell_price_cust_rules_fk as varchar(10)) + isnull(Cast(isnull([TMRA].[Price_Operand], [TMRA].[Discount_Operand]) as varchar(1)), 0) 
	+ Cast(Replace(@Job_FK_i, '.', '') as varchar(6)) as bigint) What_If_Reference_ID
	, Qty_Mode
	, Qty_Median
	, Qty_Mean
	, Replacement_Cost
	, Standard_Cost
	, Base_Price
	, List_Price
	, Vendor_Rebate_Cost
	, Qty_Q1
	, Qty_Q2
	, Qty_Q3
	, Qty_Q4
	, Sales_Q1
	, Sales_Q2
	, Sales_Q3
	, Sales_Q4
	, Cost_Net_Q1
	, Cost_Net_Q2
	, Cost_Net_Q3
	, Cost_Net_Q4
	, Date_Range_Q1
	, Date_Range_Q2
	, Date_Range_Q3
	, Date_Range_Q4
	, TMRA.CONTRACT_NO_HIST_LAST_PRICE
	, TMRA.CONTRACT_NO_HIST_LAST_REBATE_CB
	, TMRA.Identifier
	Into ##MB
	from ##MarginResultsAggregated TMRA with (nolock)
	left join ##Prices	Prc		on TMRA.identifier = Prc.Identifier and TMRA.result_type_cr_fk = Prc.result_type_cr_fk
	left Join ##rbt_cb	cb		on TMRA.identifier = cb.Identifier and TMRA.result_type_cr_fk = cb.result_type_cr_fk
	left join ##rbt_buy	buy		on TMRA.identifier = buy.Identifier and TMRA.result_type_cr_fk = buy.result_type_cr_fk
	left join ##prods	prods	on TMRA.identifier = prods.Identifier and TMRA.result_type_cr_fk = prods.result_type_cr_fk
	left join ##Custs_Vndr	custs	on TMRA.identifier = custs.Identifier and TMRA.result_type_cr_fk = custs.result_type_cr_fk

	insert into dbo.maui_log Select @Job_FK_i, 'All Results Loaded into MAUI Basis', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI Basis'), 108)), (Select COUNT(*) from dbo.MAUI_Basis where Job_FK_i = @Job_FK_i)

Begin Try
	Insert into dbo.MAUI_Basis
	Select * from ##MB with (nolock)
	where identifier not in (select identifier from ##MB with (nolock) group by job_fk, calc_status, identifier having COUNT(*) > 1)
End Try

Begin Catch
	Exec import.util_Find_Truncating_Data 'MAUI_Basis', '##MB', 1, 1, Job_FK_i
	
	insert into dbo.maui_log Select @Job_FK_i, 'DATA LOAD ERROR - MAUI Basis', getdate(), (Select Convert(Varchar(10), GETDATE() - (Select MAX(Start_Time) from dbo.MAUI_Log where Job_fk_i = @Job_FK_i and Activity = 'Start Loading MAUI Basis'), 108)), Null

	Declare @OpenQry as varchar(256)
	Set @Openqry = 'Select * from dbo.MAUI_Basis_Load_Errors_' + Replace(Cast(@Job_FK_i as varchar(32)), '.', '_')
	
	Exec (@Openqry)

End Catch
	

IF object_id('tempdb..##TMRA') is not null
drop table ##TMRA
IF object_id('tempdb..##Prices') is not null
drop table ##Prices
IF object_id('tempdb..##rbt_cb') is not null
drop table ##rbt_cb
IF object_id('tempdb..##rbt_buy') is not null
drop table ##rbt_buy
IF object_id('tempdb..##Prods') is not null
Drop Table ##Prods
IF object_id('tempdb..##Custs_Vndr') is not null
drop table ##Custs_Vndr
IF object_id('tempdb..##Names') is not null
drop table ##Names
IF object_id('tempdb..##Inside') is not null
drop table ##Inside
IF object_id('tempdb..##Outside') is not null
drop table ##Outside
IF object_id('tempdb..##Order_Taken_By') is not null
drop table ##Order_Taken_By
IF object_id('tempdb..##MB') is not null
drop table ##MB

END
