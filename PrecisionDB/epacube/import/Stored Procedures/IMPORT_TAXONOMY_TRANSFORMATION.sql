﻿





-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        05/08/2014   Initial Version 


CREATE PROCEDURE [import].[IMPORT_TAXONOMY_TRANSFORMATION] ( @IN_JOB_FK bigint, @v_import_package_fk bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.IMPORT_TAXONOMY_TRANSFORMATION', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


-------------------------------------------------------------------------------------
----- Flag rows with MISSING PRODUCT ID's as do not import.
update import.IMPORT_RECORD_DATA
set DO_NOT_IMPORT_IND = 1
where DATA_POSITION2 is null 


--------------------------------------------------------------------------------------
--- TAXONOMY SECTION
---
---------------------------------------------------------------------------------------

-------LOOKUP the data positions for the data name and data value for import

DECLARE @v_data_name varchar(50),
        @v_data_value varchar(50)



SET @V_DATA_NAME = ISNULL((select column_name from import.import_map_metadata
where 1=1
and import_package_fk = @v_import_package_fk
and DATA_NAME_FK = 999003),-999)

SET @V_DATA_VALUE =ISNULL((select column_name from import.import_map_metadata
where 1=1
and  import_package_fk = @v_import_package_fk
and DATA_NAME_FK = 999004),-999)



DECLARE 
               @vm$file_name varchar(100),
               @vm$record_no varchar(64),         
               @vm$job_fk int,
               @vm$import_package_fk bigint,                                           
               @vm$data_name varchar(256),
			   @vm$data_name_value varchar(max)

	DECLARE cur_v_import CURSOR local for
		SELECT  distinct ird.IMPORT_FILENAME
			   ,ird.RECORD_NO
			   ,ird.job_fk
			   ,ird.IMPORT_PACKAGE_FK
			   ,[epacube].[GetImportDataColumn](@v_data_name, ird.import_record_data_id) 
			   ,[epacube].[GetImportDataColumn](@v_data_value, ird.import_record_data_id) 
	
	  from import.import_record_data ird
		where 1=1
		and isNULL(ird.DO_NOT_IMPORT_IND,0) = 0
		and ird.import_package_fk = @v_import_package_fk
		and ird.job_fk = @in_job_fk
			and	 isNULL([epacube].[GetImportDataColumn](@v_data_name, ird.import_record_data_id),'0') <> '0'
		


         OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO @vm$file_name,
                                           @vm$record_no,         
                                           @vm$job_fk,
                                           @vm$import_package_fk,                                           
                                           @vm$data_name,
										   @vm$data_name_value
      WHILE @@FETCH_STATUS = 0
      BEGIN


		INSERT INTO [import].[IMPORT_RECORD_NAME_VALUE]
           ([IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[JOB_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[DATA_NAME]
           ,[DATA_VALUE]
           )
		select distinct
			 @vm$file_name,
             @vm$record_no,         
             @vm$job_fk,
             @vm$import_package_fk,                                           
             @vm$data_name,
			 @vm$data_name_value	 




 FETCH NEXT FROM cur_v_import Into @vm$file_name,
                                   @vm$record_no,         
                                   @vm$job_fk,
                                   @vm$import_package_fk,                                           
                                   @vm$data_name,
								   @vm$data_name_value
								   
								   
								  

END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_import
DEALLOCATE cur_v_import    

----------------------------------------------------------------------------------------------
---  CAll import validation to check tree/nodes etc. before removing data from import
---------------------------------------------------------------------------------------------

--EXECUTE [cleanser].[IMPORT_TAXONOMY_VALIDATION] 
--   @IN_JOB_FK
 
 ----remove the name value data after insert of name value from record data so we do not have duplicate errors.


 EXECUTE  [import].[EXECUTE_IMPORT_SQL] 
					@v_import_package_fk
					,@in_job_fk



EXECUTE [cleanser].[IMPORT_TAXONOMY_VALIDATION] 
   @IN_JOB_FK
 ,@v_import_package_fk


UPDATE import.IMPORT_RECORD_DATA
SET DO_NOT_IMPORT_IND = 1
WHERE JOB_FK = @IN_JOB_FK 
AND import_record_data_id in 
(select TABLE_PK from common.JOB_ERRORS where job_fk = @IN_JOB_FK)

--------------------------------------------------------------------------------------------------

      SELECT @v_input_rows = COUNT(1)
        FROM import.import_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM stage.STG_Record WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'TAXONOMY_TRANSFORMATION',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



 SET @status_desc =  'finished execution of stage.IMPORT_TAXONOMY_TRANSFORMATION'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of stage.IMPORT_TAXONOMY_TRANSFORMATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END





