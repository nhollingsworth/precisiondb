﻿


-- Copyright 2010
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: To create import data from estorefront tables.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        02/03/2012   Initial SQL Version

--
CREATE PROCEDURE [import].[IMPORT_ESTOREFRONT_DATA] 

AS
BEGIN

-------------------------------------------------------------------------------
----variables to pass Name of Tree to export.  
----Langage code to export - AT the tree level along with the Attribute level
----- changes from date.


DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @ls_exec2            varchar(100)
DECLARE  @ls_exec3             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint
DECLARE @test varchar(50)

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime
DECLARE  @v_import_name       varchar (50)
DECLARE  @v_schema_name       varchar (50)
DECLARE  @v_Future_eff_days int

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of EXPORT.Export_to_Estorefront' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;


----------------------------------------------------------------------
---FIRST LOAD IMPORT TABLES TABLES --
----------------------------------------------------------------------

INSERT INTO [import].[IMPORT_TAXONOMY_TREE]
           ([IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[JOB_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[Delete_Flag]
           ,[Tree_Name]
           ,[Node_Code]
           ,[Description]
           ,[Level]
           ,[Parent_Node]
           )
SELECT 
'Cindy Import',
x.sf_ctg_id,
--(select row_number() over (order by ctg_name desc) as row from import.SF_category
--where cat_id = 32) + 1,
3,
190000,
'N',
'Catalog',
x.cat_name code,
x.cat_name,
x.Level,
x.parent

from (

select  cat1.cat_id cat_id,
cat1.ctg_id sf_ctg_id,
NULL Epa_Ctg_id,
cat1.ctg_name cat_name,
'0' level,
NULL parent,
Null Epa_parent_ctg_id
 from import.SF_category cat1
where cat1.ctg_level = 0
and cat_id = 32

Union All

select distinct 
cat1.cat_id cat_id,
cat1.ctg_id sf_catid,
NULL Epa_Cat_id,
cat1.ctg_name cat,
'1' level,
p.ctg_name parent,
--cat1.ctg_parent SF_parent_cat_id,
Null Epa_parent_cat_id
 from import.SF_category cat1
inner join import.sf_category p
on (cat1.ctg_parent = p.ctg_id)
where cat1.ctg_level = 1
and cat1.cat_id = 32
--
Union All

select distinct 
cat1.cat_id cat_id,
cat1.ctg_id sf_catid,
NULL Epa_Cat_id1,
cat1.ctg_name cat1,
'2' level,
p.ctg_name parent,
Null Epa_parent_cat_id
 from import.SF_category cat1
inner join import.sf_category p
on (cat1.ctg_parent = p.ctg_id)
where cat1.ctg_level = 2
and cat1.cat_id = 32
--
Union All

select distinct 
cat1.cat_id cat_id,
cat1.ctg_id sf_catid,
NULL Epa_Cat_id1,
cat1.ctg_name cat1,
'3' level,
p.ctg_name parent,
Null Epa_parent_cat_id
 from import.SF_category cat1
inner join import.sf_category p
on (cat1.ctg_parent = p.ctg_id)
where cat1.ctg_level = 3
and cat1.cat_id = 32

Union All

select distinct 
cat1.cat_id cat_id,
cat1.ctg_id sf_catid,
NULL Epa_Cat_id1,
cat1.ctg_name cat1,
'4' level,
p.ctg_name parent,
Null Epa_parent_cat_id
 from import.SF_category cat1
inner join import.sf_category p
on (cat1.ctg_parent = p.ctg_id)
where cat1.ctg_level = 4
and cat1.cat_id = 32

Union All

select distinct 
cat1.cat_id cat_id,
cat1.ctg_id sf_catid,
NULL Epa_Cat_id1,
cat1.ctg_name cat1,
'5' level,
p.ctg_name parent,
Null Epa_parent_cat_id
 from import.SF_category cat1
inner join import.sf_category p
on (cat1.ctg_parent = p.ctg_id)
where cat1.ctg_level = 5
and cat1.cat_id = 32) X
     




--need to remove that first record
--delete from import.import_taxonomy_tree where record_no = 43820



---------------------------------------------------------------------------
--Load import tax tree attribute and then call procedure
--------------------------------------------------------------------------
select * from import.import_taxonomy_tree_attribute


select * from epacube.data_name where name in (
'Manufacturer',
'Great Deals!!',
'Size')

select * from epacube.taxonomy_tree

select * from epacube.data_value where data_value_id in (
select tax_node_dv_fk from epacube.taxonomy_node)

INSERT INTO [import].[IMPORT_TAXONOMY_TREE_ATTRIBUTE]
           ([IMPORT_FILENAME]
           ,[RECORD_NO]
           ,[JOB_FK]
           ,[IMPORT_PACKAGE_FK]
           ,[Delete_Flag]
           ,[Tree_Name]
           ,[Node_Code]
           ,[Attribute_Name]
           ,[Attribute_Value]
           ,[Data_Type]
           ,[Default_Value]
           ,[Restricted_Lookup]
           )
(SELECT distinct
'Cindy Import',
cat.ctg_id,
3,
191000,
'N',
'Catalog',
cat.ctg_name,
a.attr_name,
av.attr_value,
'CHARACTER',
NULL, --default value
NULL --Restricted Value

from import.SF_attribute A
inner join import.SF_attributevalue av
on (av.attr_id = a.attr_id)
inner join import.sf_attributevalueassign aav
on (av.attr_value_id = aav.attr_value_id)
inner join import.SF_item i
on (i.itm_id = aav.itm_id)
inner join import.SF_catalogitem ci
on (ci.itm_id = aav.itm_id)
inner join import.sf_category  cat
on (cat.ctg_id = ci.ctg_id)
)




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of EXPORT.EXPORT_TO_ESTOREFRONT'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of EXPORT.EXPORT_TO_ESTOREFRONT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END

















