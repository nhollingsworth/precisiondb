﻿







-- Copyright 2013 epaCUBE, Inc.
--
-- Procedure created by Leslie Andrews
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- LA        01/16/2013   Initial Version - created for Kaman (modified from LOAD_SXE_CPT_CRT_DESC)
--                        Loads the Product Category, Product Price Type, Family Group, Non Tax Reason
--                        Rebate Type, and Rebate Sub Type and Supplier Group from the SX export file
--                        Adds new UOM, Vendor and Warehouse records found in SX Out 
--                             and updates existing records



CREATE PROCEDURE [import].[LOAD_SXE_GENERAL_DATA] ( @IN_JOB_FK bigint ) 
AS BEGIN 


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_import_package_fk   int  
	    	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128)
	 DECLARE @V_SXE_MULTIPLE_CPT varchar(1)
	 DECLARE @V_SXE_MULTIPLE_CRT varchar(1) 

--DECLARE @in_job_fk bigint

--set @in_job_fk = (select distinct job_fk from import.import_sxe_table_data)

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of import.LOAD_SXE_GENERAL_DATA', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps
     
      SELECT @v_input_rows = COUNT(1)
        FROM import.IMPORT_SXE_TABLE_DATA WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM import.IMPORT_SXE_TABLE_DATA WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'LOAD IMPORT SXE CUSTOMER GENERAL DATA',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


-------------------------------------------------------------------------------------
--INSERT into DATA VALUE TABLE 
--    Product Category, Product Price Type, Family Group, Non Tax Reason
--    Rebate Type, and Rebate Sub Type, Supplier Group 
--Only for Records that DO NOT already exist in database
-------------------------------------------------------------------------------------

--Product Category
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '210901', i.VALUE1, i.VALUE2, 1  from import.IMPORT_SXE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 210901
Where DATA_NAME = ('Product Category') and i.value1 is not null and DATA_VALUE_ID is NULL)

--Product PriceType
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '211901', i.VALUE1, i.VALUE2, 1  from import.IMPORT_SXE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 211901
Where DATA_NAME = ('Product PriceType') and i.value1 is not null and DATA_VALUE_ID is NULL)

--Family Group
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '211905', i.VALUE1, i.VALUE2, 1  from import.IMPORT_SXE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 211905
Where DATA_NAME = ('Family Group') and i.value1 is not null and DATA_VALUE_ID is NULL)

--Non Tax Reason
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '211806', i.VALUE1, i.VALUE2, 1  from import.IMPORT_SXE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 211806
Where DATA_NAME = ('Non Tax Reason') and i.value1 is not null and DATA_VALUE_ID is NULL)

--Rebate Type
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '211903', i.VALUE1, i.VALUE2, 1  from import.IMPORT_SXE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 211903
Where DATA_NAME = ('Rebate Type') and i.value1 is not null and DATA_VALUE_ID is NULL)

--Rebate Sub Type
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '211904', i.VALUE1, i.VALUE2, 1  from import.IMPORT_SXE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 211904
Where DATA_NAME = ('Rebate Sub Type') and i.value1 is not null and DATA_VALUE_ID is NULL)

--Supplier Group
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '210902', i.VALUE1, i.VALUE2, 1  from import.IMPORT_SXE_TABLE_DATA i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 210902
Where DATA_NAME = ('Supplier Group') and i.value1 is not null and DATA_VALUE_ID is NULL)

-------------------------------------------------------------------------------------
--Update the Description of Data_Value records 
--    Product Category, Product Price Type, 
--    Family Group, Non Tax Reason, Rebate Type, and Rebate Sub Type
--Where they already DO exist in the database
-----------------------------------------------------------------------------------------

-- product category 
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_SXE_TABLE_DATA.VALUE2
FROM import.IMPORT_SXE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_SXE_TABLE_DATA.VALUE1
   and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Product Category'
   and epacube.data_value.DATA_NAME_FK = 210901

-- Product PriceType
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_SXE_TABLE_DATA.VALUE2
FROM import.IMPORT_SXE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_SXE_TABLE_DATA.VALUE1
   and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Product PriceType'
   and epacube.data_value.DATA_NAME_FK = 211901


-- Family Group 
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_SXE_TABLE_DATA.VALUE2
FROM import.IMPORT_SXE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_SXE_TABLE_DATA.VALUE1
   and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Family Group '
   and epacube.data_value.DATA_NAME_FK = 211905


-- Non Tax Reason
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_SXE_TABLE_DATA.VALUE2
FROM import.IMPORT_SXE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_SXE_TABLE_DATA.VALUE1
   and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Non Tax Reason'
   and epacube.data_value.DATA_NAME_FK = 211806


-- Rebate Type 
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_SXE_TABLE_DATA.VALUE2
FROM import.IMPORT_SXE_TABLE_DATA
WHERE
   DATA_VALUE.VALUE = IMPORT_SXE_TABLE_DATA.VALUE1
   and IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Rebate Type'
   and data_value.DATA_NAME_FK = 211903


-- Rebate Sub Type 
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_SXE_TABLE_DATA.VALUE2
FROM import.IMPORT_SXE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_SXE_TABLE_DATA.VALUE1
   and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Rebate Sub Type'
   and epacube.data_value.DATA_NAME_FK = 211904

-- Supplier Group 
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = IMPORT_SXE_TABLE_DATA.VALUE2
FROM import.IMPORT_SXE_TABLE_DATA
WHERE
   epacube.DATA_VALUE.VALUE = IMPORT_SXE_TABLE_DATA.VALUE1
   and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Supplier Group'
   and epacube.data_value.DATA_NAME_FK = 210902
   
   
-------------------------------------------------------------------------------------
--INSERT into DATA VALUE TABLE 
--    DISTINCT Product Lines
--Only for Records that DO NOT already exist in database
-------------------------------------------------------------------------------------

--Product Line
INSERT INTO epacube.DATA_VALUE (DATA_NAME_FK, VALUE, DESCRIPTION, RECORD_STATUS_CR_FK)
(select '211902', i.VALUE1, i.VALUE2, 1  from 
(SELECT value1, MIN(value2) as value2 FROM import.IMPORT_SXE_TABLE_DATA where DATA_NAME = 'Product Line'
group by value1) as i
left outer join epacube.DATA_VALUE dv on dv.VALUE = i.VALUE1 and DATA_NAME_FK = 211902
Where DATA_VALUE_ID is NULL)

-------------------------------------------------------------------------------------
--Update the Description of Data_Value records 
--    DISTINCT Product Lines
--Where they already DO exist in the database
-----------------------------------------------------------------------------------------

--Product Line 
UPDATE epacube.DATA_VALUE
SET DESCRIPTION = i.VALUE2
FROM (SELECT value1, MIN(value2) as value2 FROM import.IMPORT_SXE_TABLE_DATA where DATA_NAME = 'Product Line'
group by value1) i
WHERE
   epacube.DATA_VALUE.VALUE = i.VALUE1
   and epacube.data_value.DATA_NAME_FK = 211902
  
-------------------------------------------------------------------------------------
--INSERT into Unit of Measure Data
--Only for Records that do not already exist in database
-------------------------------------------------------------------------------------

--INSERT Unit of Measure
INSERT INTO epacube.UOM_CODE (UOM_CODE, UOM_DESCRIPTION, LOWER_QTY, RECORD_STATUS_CR_FK)
(select i.VALUE1, i.VALUE2, 1, 1  from import.IMPORT_SXE_TABLE_DATA i
left outer join epacube.UOM_CODE u on u.UOM_CODE = i.VALUE1 
Where DATA_NAME = ('General Units of Measure') and UOM_CODE is NULL)

-------------------------------------------------------------------------------------
--Update the Description of Unit of Measure Data
--Only for Records that DO already exist in database
-------------------------------------------------------------------------------------

--UPDATE Unit of Measure 
UPDATE epacube.UOM_CODE
SET UOM_DESCRIPTION = IMPORT_SXE_TABLE_DATA.VALUE2
FROM import.IMPORT_SXE_TABLE_DATA
WHERE
   epacube.UOM_CODE.UOM_CODE = IMPORT_SXE_TABLE_DATA.VALUE1
   and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'General Units of Measure'
 

-------------------------------------------------------------------------------------
--INSERT into DATA VALUE TABLE 
--    Necessary Vendor Records:  [ENTITY_STRUCTURE]
--                               [ENTITY_IDENTIFICATION] Requires ENTITY_STRUCTURE_FK 
--                               [ENTITY_IDENT_NONUNIQUE]Requires ENTITY_STRUCTURE_FK
--Only for Records that DO NOT already exist in database
-------------------------------------------------------------------------------------
--For all vendorIDs that are new in the file, insert required data into all tables

  INSERT INTO epacube.ENTITY_STRUCTURE (ENTITY_STATUS_CR_FK, DATA_NAME_FK, LEVEL_SEQ, ENTITY_CLASS_CR_FK, RECORD_STATUS_CR_FK, IMPORT_FILENAME)
  (SELECT 10200, 143000, 1, 10103, 1, VALUE1 FROM import.IMPORT_SXE_TABLE_DATA i
  left outer join epacube.ENTITY_IDENTIFICATION ei on i.VALUE1 = ei.value and ENTITY_DATA_NAME_FK = 143000
  where DATA_NAME = 'Vendor' and ei.VALUE is null)
  
  INSERT INTO epacube.ENTITY_IDENTIFICATION (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, VALUE, RECORD_STATUS_CR_FK)
  (select ENTITY_STRUCTURE_ID, 143000, CASE WHEN ISNUMERIC(VALUE1) > 0 THEN 143110 else 143111 end, VALUE1, 1
  FROM import.IMPORT_SXE_TABLE_DATA d
  inner JOIN epacube.ENTITY_STRUCTURE E on E.Import_FileName = d.value1 and DATA_NAME_FK = 143000
  left outer join epacube.ENTITY_IDENTIFICATION ei on e.IMPORT_FILENAME = ei.value and ENTITY_DATA_NAME_FK = 143000
  where DATA_NAME = 'Vendor' and ei.VALUE is null)
  
  INSERT INTO [epacube].[ENTITY_IDENT_NONUNIQUE] (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, VALUE)
  (select e.ENTITY_STRUCTURE_FK, e.ENTITY_DATA_NAME_FK, 143112, VALUE2
  FROM import.IMPORT_SXE_TABLE_DATA d
  inner join epacube.ENTITY_IDENTIFICATION e on d.VALUE1 = e.VALUE
  left outer join epacube.Entity_Ident_Nonunique s on e.ENTITY_STRUCTURE_FK = s.ENTITY_STRUCTURE_FK
  where DATA_NAME = 'Vendor' and s.ENTITY_STRUCTURE_FK is null)
  
  --the IMPORT_FILE field in the ENTITY_STRUCTURE table was populated with VALUE1 in order
  --to give an easy identifier field to join on.  This query updates the field to remove that ID
  UPDATE epacube.ENTITY_STRUCTURE SET IMPORT_FILENAME = NULL 
  WHERE IMPORT_FILENAME IN (SELECT VALUE1 FROM import.IMPORT_SXE_TABLE_DATA where DATA_NAME = 'Vendor')
  AND ENTITY_STATUS_CR_FK = 10200 AND DATA_NAME_FK = 143000
  
-------------------------------------------------------------------------------------
--   UPDATE Vendor name with changes from Source file
-------------------------------------------------------------------------------------
 DECLARE 
               @Vendor_name varchar(100),
               @entity_structure_fk bigint                                          
               

	DECLARE cur_v_import CURSOR local for
		SELECT   import.IMPORT_SXE_TABLE_DATA.VALUE2, entity_structure_fk from import.IMPORT_SXE_TABLE_DATA  WITH (NOLOCK)
INNER JOIN epacube.ENTITY_IDENTIFICATION WITH (NOLOCK)
ON ( ENTITY_STRUCTURE_FK = epacube.ENTITY_IDENTIFICATION.ENTITY_STRUCTURE_FK
and epacube.ENTITY_IDENTIFICATION.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
and epacube.ENTITY_IDENTIFICATION.DATA_NAME_FK IN (143110 , 143111)
and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'VENDOR')


         OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO @Vendor_name,
							@entity_structure_fk 


      WHILE @@FETCH_STATUS = 0
      BEGIN


		 
UPDATE [epacube].[ENTITY_IDENT_NONUNIQUE]
SET VALUE = @vendor_name
WHERE epacube.ENTITY_IDENT_NONUNIQUE.DATA_NAME_FK = 143112
and epacube.ENTITY_IDENT_NONUNIQUE.entity_structure_fk = @entity_structure_fk



 FETCH NEXT FROM cur_v_import Into @Vendor_name,
							 @entity_structure_fk


END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_import
DEALLOCATE cur_v_import    
--end Vendor Name Update

-------------------------------------------------------------------------------------
--INSERT into DATA VALUE TABLE 
--    Necessary Region Records:  [ENTITY_STRUCTURE]
--                             [ENTITY_IDENTIFICATION] Requires ENTITY_STRUCTURE_FK 
--                             [ENTITY_IDENT_NONUNIQUE]Requires ENTITY_STRUCTURE_FK
--Only for Records that DO NOT already exist in database
-------------------------------------------------------------------------------------
--For all RegionIDs that are new in the file, insert required data into all tables

--  INSERT INTO epacube.ENTITY_STRUCTURE (ENTITY_STATUS_CR_FK, DATA_NAME_FK, LEVEL_SEQ, ENTITY_CLASS_CR_FK, PARENT_ENTITY_STRUCTURE_FK, RECORD_STATUS_CR_FK, IMPORT_FILENAME)
--  (SELECT 10200, 141030, 1, 10101, 1, 1, VALUE1 FROM import.IMPORT_SXE_TABLE_DATA i
--  left outer join epacube.ENTITY_IDENTIFICATION ei on i.VALUE1 = ei.value and ENTITY_DATA_NAME_FK = 141030
--  where DATA_NAME = 'Region' and ei.VALUE is null)
  
--  INSERT INTO epacube.ENTITY_IDENTIFICATION (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, VALUE, RECORD_STATUS_CR_FK)
--  (select ENTITY_STRUCTURE_ID, 141030, 141111, VALUE1, 1
--  FROM import.IMPORT_SXE_TABLE_DATA d
--  inner JOIN epacube.ENTITY_STRUCTURE E on E.Import_FileName = d.value1 and DATA_NAME_FK = 141030
--  left outer join epacube.ENTITY_IDENTIFICATION ei on e.IMPORT_FILENAME = ei.value and ENTITY_DATA_NAME_FK = 141030
--  where DATA_NAME = 'Region' and ei.VALUE is null)
  
  
--  --the IMPORT_FILE field in the ENTITY_STRUCTURE table was populated with VALUE1 in order
--  --to give an easy identifier field to join on.  This query updates the field to remove that ID
--  UPDATE epacube.ENTITY_STRUCTURE SET IMPORT_FILENAME = NULL 
--  WHERE IMPORT_FILENAME IN (SELECT VALUE1 FROM import.IMPORT_SXE_TABLE_DATA where DATA_NAME = 'Region')
--  AND ENTITY_STATUS_CR_FK = 10200 AND DATA_NAME_FK = 141030

-------------------------------------------------------------------------------------
--   UPDATE Region name with changes from Source file
--reuse variables 
-------------------------------------------------------------------------------------
--DECLARE 
--               @Region_name varchar(100),
--               @Region_entity_structure_fk bigint                                          
               
--DECLARE cur_v_import CURSOR local for
--		SELECT   import.IMPORT_SXE_TABLE_DATA.VALUE2, entity_structure_fk from import.IMPORT_SXE_TABLE_DATA  WITH (NOLOCK)
--INNER JOIN epacube.ENTITY_IDENTIFICATION WITH (NOLOCK)
--ON ( ENTITY_STRUCTURE_FK = epacube.ENTITY_IDENTIFICATION.ENTITY_STRUCTURE_FK
--and epacube.ENTITY_IDENTIFICATION.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
--and epacube.ENTITY_IDENTIFICATION.DATA_NAME_FK =141111 
--and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Region')


--         OPEN cur_v_import;
--         FETCH NEXT FROM cur_v_import INTO @Region_name,
--							@Region_entity_structure_fk 


--      WHILE @@FETCH_STATUS = 0
--      BEGIN


		 
--UPDATE [epacube].[ENTITY_IDENT_NONUNIQUE]
--SET VALUE = @Region_name
--WHERE epacube.ENTITY_IDENT_NONUNIQUE.DATA_NAME_FK = 141112
--and epacube.ENTITY_IDENT_NONUNIQUE.entity_structure_fk = @Region_entity_structure_fk



-- FETCH NEXT FROM cur_v_import Into @Region_name,
--							 @Region_entity_structure_fk


--END -- WHILE @@FETCH_STATUS = 0

							
--CLOSE cur_v_import
--DEALLOCATE cur_v_import    
--end Region Name Update

  
-------------------------------------------------------------------------------------
--INSERT into DATA VALUE TABLE 
--    Necessary WHSE Records:  [ENTITY_STRUCTURE]
--                             [ENTITY_IDENTIFICATION] Requires ENTITY_STRUCTURE_FK 
--                             [ENTITY_IDENT_NONUNIQUE]Requires ENTITY_STRUCTURE_FK
--Only for Records that DO NOT already exist in database
-------------------------------------------------------------------------------------
--For all WHSEIDs that are new in the file, insert required data into all tables

  INSERT INTO epacube.ENTITY_STRUCTURE (ENTITY_STATUS_CR_FK, DATA_NAME_FK, LEVEL_SEQ, ENTITY_CLASS_CR_FK, RECORD_STATUS_CR_FK, IMPORT_FILENAME)
  (SELECT 10200, 141000, 1, 10101, 1, VALUE1 FROM import.IMPORT_SXE_TABLE_DATA i
  left outer join epacube.ENTITY_IDENTIFICATION ei on i.VALUE1 = ei.value and ENTITY_DATA_NAME_FK = 141000
  where DATA_NAME = 'Warehouse' and ei.VALUE is null)
  
  INSERT INTO epacube.ENTITY_IDENTIFICATION (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, VALUE, RECORD_STATUS_CR_FK)
  (select ENTITY_STRUCTURE_ID, 141000, 141111, VALUE1, 1
  FROM import.IMPORT_SXE_TABLE_DATA d
  inner JOIN epacube.ENTITY_STRUCTURE E on E.Import_FileName = d.value1 and DATA_NAME_FK = 141000
  left outer join epacube.ENTITY_IDENTIFICATION ei on e.IMPORT_FILENAME = ei.value and ENTITY_DATA_NAME_FK = 141000
  where DATA_NAME = 'Warehouse' and ei.VALUE is null)
  
  INSERT INTO [epacube].[ENTITY_IDENT_NONUNIQUE] (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, VALUE)
  (select e.ENTITY_STRUCTURE_FK, e.ENTITY_DATA_NAME_FK, 141112, VALUE2
  FROM import.IMPORT_SXE_TABLE_DATA d
  inner join epacube.ENTITY_IDENTIFICATION e on d.VALUE1 = e.VALUE
  left outer join epacube.Entity_Ident_Nonunique s on e.ENTITY_STRUCTURE_FK = s.ENTITY_STRUCTURE_FK
  where DATA_NAME = 'Warehouse' and s.ENTITY_STRUCTURE_FK is null)
  
  ----the IMPORT_FILE field in the ENTITY_STRUCTURE table was populated with VALUE1 in order
  ----to give an easy identifier field to join on.  This query updates the field to remove that ID
  UPDATE epacube.ENTITY_STRUCTURE SET IMPORT_FILENAME = NULL 
  WHERE IMPORT_FILENAME IN (SELECT VALUE1 FROM import.IMPORT_SXE_TABLE_DATA where DATA_NAME = 'Warehouse')
  AND ENTITY_STATUS_CR_FK = 10200 AND DATA_NAME_FK = 141000

-------------------------------------------------------------------------------------
--   UPDATE Warehouse name with changes from Source file
--reuse variables 
-------------------------------------------------------------------------------------
DECLARE 
               @Warehouse_name varchar(100),
               @Whse_entity_structure_fk bigint                                          
               
DECLARE cur_v_import CURSOR local for
		SELECT   import.IMPORT_SXE_TABLE_DATA.VALUE2, entity_structure_fk from import.IMPORT_SXE_TABLE_DATA  WITH (NOLOCK)
INNER JOIN epacube.ENTITY_IDENTIFICATION WITH (NOLOCK)
ON ( ENTITY_STRUCTURE_FK = epacube.ENTITY_IDENTIFICATION.ENTITY_STRUCTURE_FK
and epacube.ENTITY_IDENTIFICATION.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
and epacube.ENTITY_IDENTIFICATION.DATA_NAME_FK =141111 
and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'Warehouse')


         OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO @Warehouse_name,
							@Whse_entity_structure_fk 


      WHILE @@FETCH_STATUS = 0
      BEGIN


		 
UPDATE [epacube].[ENTITY_IDENT_NONUNIQUE]
SET VALUE = @Warehouse_name
WHERE epacube.ENTITY_IDENT_NONUNIQUE.DATA_NAME_FK = 141112
and epacube.ENTITY_IDENT_NONUNIQUE.entity_structure_fk = @Whse_entity_structure_fk



 FETCH NEXT FROM cur_v_import Into @Warehouse_name,
							 @Whse_entity_structure_fk


END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_import
DEALLOCATE cur_v_import    
--end Warehouse Name Update

-------------------------------------------------------------------------------------
--INSERT into DATA VALUE TABLE 
--    Necessary Brand Code Records:  [ENTITY_STRUCTURE]
--                               [ENTITY_IDENTIFICATION] Requires ENTITY_STRUCTURE_FK 
--                               [ENTITY_IDENT_NONUNIQUE]Requires ENTITY_STRUCTURE_FK
--Only for Records that DO NOT already exist in database
-------------------------------------------------------------------------------------
--For all vendorIDs that are new in the file, insert required data into all tables

  INSERT INTO epacube.ENTITY_STRUCTURE (ENTITY_STATUS_CR_FK, DATA_NAME_FK, LEVEL_SEQ, ENTITY_CLASS_CR_FK, RECORD_STATUS_CR_FK, IMPORT_FILENAME)
  (SELECT 10200, 210903, 1, 10103, 1, VALUE1 FROM import.IMPORT_SXE_TABLE_DATA i
  left outer join epacube.ENTITY_IDENTIFICATION ei on i.VALUE1 = ei.value and ENTITY_DATA_NAME_FK = 210903
  where DATA_NAME = 'Brand Code' and ei.VALUE is null)
  
  INSERT INTO epacube.ENTITY_IDENTIFICATION (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, VALUE, RECORD_STATUS_CR_FK)
  (select ENTITY_STRUCTURE_ID, 210903, CASE WHEN ISNUMERIC(VALUE1) > 0 THEN 143110 else 143111 end, VALUE1, 1
  FROM import.IMPORT_SXE_TABLE_DATA d
  inner JOIN epacube.ENTITY_STRUCTURE E on E.Import_FileName = d.value1 and DATA_NAME_FK = 210903
  left outer join epacube.ENTITY_IDENTIFICATION ei on e.IMPORT_FILENAME = ei.value and ENTITY_DATA_NAME_FK = 210903
  where DATA_NAME = 'Brand Code' and ei.VALUE is null)
  
  INSERT INTO [epacube].[ENTITY_IDENT_NONUNIQUE] (ENTITY_STRUCTURE_FK, ENTITY_DATA_NAME_FK, DATA_NAME_FK, VALUE)
  (select e.ENTITY_STRUCTURE_FK, e.ENTITY_DATA_NAME_FK, 143112, VALUE2
  FROM import.IMPORT_SXE_TABLE_DATA d
  inner join epacube.ENTITY_IDENTIFICATION e on d.VALUE1 = e.VALUE
  left outer join epacube.Entity_Ident_Nonunique s on e.ENTITY_STRUCTURE_FK = s.ENTITY_STRUCTURE_FK
  where DATA_NAME = 'Brand Code' and s.ENTITY_STRUCTURE_FK is null)
  
  --the IMPORT_FILE field in the ENTITY_STRUCTURE table was populated with VALUE1 in order
  --to give an easy identifier field to join on.  This query updates the field to remove that ID
  UPDATE epacube.ENTITY_STRUCTURE SET IMPORT_FILENAME = NULL 
  WHERE IMPORT_FILENAME IN (SELECT VALUE1 FROM import.IMPORT_SXE_TABLE_DATA where DATA_NAME = 'Brand Code')
  AND ENTITY_STATUS_CR_FK = 10200 AND DATA_NAME_FK = 210903
  
-------------------------------------------------------------------------------------
--   UPDATE Vendor name with changes from Source file
-------------------------------------------------------------------------------------
 DECLARE 
               @Brand_name varchar(100),
               @Brand_entity_structure_fk bigint                                          
               

	DECLARE cur_v_import CURSOR local for
		SELECT   import.IMPORT_SXE_TABLE_DATA.VALUE2, entity_structure_fk from import.IMPORT_SXE_TABLE_DATA  WITH (NOLOCK)
INNER JOIN epacube.ENTITY_IDENTIFICATION WITH (NOLOCK)
ON ( ENTITY_STRUCTURE_FK = epacube.ENTITY_IDENTIFICATION.ENTITY_STRUCTURE_FK
and epacube.ENTITY_IDENTIFICATION.VALUE = import.IMPORT_SXE_TABLE_DATA.value1
and epacube.ENTITY_IDENTIFICATION.DATA_NAME_FK IN (143110 , 143111)
and import.IMPORT_SXE_TABLE_DATA.DATA_NAME = 'BRAND CODE')


         OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO @Brand_name,
							@Brand_entity_structure_fk 


      WHILE @@FETCH_STATUS = 0
      BEGIN


		 
UPDATE [epacube].[ENTITY_IDENT_NONUNIQUE]
SET VALUE = @Brand_name
WHERE epacube.ENTITY_IDENT_NONUNIQUE.DATA_NAME_FK = 143112
and epacube.ENTITY_IDENT_NONUNIQUE.entity_structure_fk = @Brand_entity_structure_fk



 FETCH NEXT FROM cur_v_import Into @Brand_name,
							 @Brand_entity_structure_fk


END -- WHILE @@FETCH_STATUS = 0

							
CLOSE cur_v_import
DEALLOCATE cur_v_import    
--end Brand Name Update


 SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'LOAD GENERAL_DATA' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                            and  name = 'LOAD GENERAL_DATA' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------


 SET @status_desc =  'finished execution of import.LOAD_SXE_GENERAL_DATA'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of import.LOAD_SXE_GENERAL_DATA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END


























