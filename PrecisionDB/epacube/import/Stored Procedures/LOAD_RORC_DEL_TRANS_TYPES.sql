﻿



















-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        07/01/2017   Initial Version 



CREATE PROCEDURE [import].[LOAD_RORC_DEL_TRANS_TYPES]  
	@IN_JOB_FK bigint 
	

AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to   determine if the import data needs to follow the 
--   product transformation or entity transformation
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure 
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--     
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int 
	 DECLARE @v_import_package_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.LOAD_RORC_DEL_TRANS_TYPES', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


	

--------------------------------------------------------------------------------------
---  Run import package sql to get product and vendor data
---------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------

SET @v_import_package_fk = (select distinct import_package_fk 
							from import.import_record_data WITH (NOLOCK)
							where  job_fk = @in_job_fk)

SET @v_entity_class_cr_fk = (select entity_class_cr_fk
						from import.IMPORT_PACKAGE WITH (NOLOCK)
						where import_package_id = @v_import_package_fk)  




EXECUTE  [import].[EXECUTE_IMPORT_SQL] 
					@v_import_package_fk
					,@in_job_fk



--------------------------------------------------------------------------------------
---  Check Trans Type to create the Correct events per Type
---------------------------------------------------------------------------------------
  ---- DEL TRANS TYPES
    ---PROD DEL  store vendor item plu
	---ITEM DEL  store vendor item
	---PLU DEL   store - item
	---DEAL DEL  store item vendor deal
	---DEPT DEL  store department
	---VEND DEL store - alias vendor


----------------------------------------------------------------------------------------------
---CREATE EVENTS FOR SETTING END DATE
------------------------------------------------------------------------------------------------

----select pi.PRODUCT_STRUCTURE_FK, ei.value, pa.PRODUCT_ASSOCIATION_ID  --, ein.value
----,  ird.* from import.IMPORT_RECORD_DATA IRD 
----inner join epacube.PRODUCT_IDENTIFICATION pi on pi.value = ird.DATA_POSITION4 and pi.DATA_NAME_FK = 110100
----left join epacube.ENTITY_IDENTIFICATION ei on ei.value = ird.DATA_POSITION3 and ei.DATA_NAME_FK = 144111
----left join epacube.PRODUCT_ASSOCIATION pa on pa.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
----and ei.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK and pa.DATA_NAME_FK = 159905
----left join epacube.ENTITY_IDENT_NONUNIQUE ein on ein.value = ird.DATA_POSITION196 and ein.DATA_NAME_FK  =  143112
----where ird.JOB_FK = 1076

---VEND DEL

INSERT INTO synchronizer.EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[TABLE_ID_FK]			
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			   ,[END_DATE_NEW]
			   ,CUST_ENTITY_STRUCTURE_FK
			   ,VENDOR_ENTITY_STRUCTURE_FK
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])
SELECT DISTINCT
	 154,  -- ASSOC EVENT
	 10104,
	 getdate(),
	NULL,   --no product
	500209,
	'VEND-DEL - ' + cast(ird.EFFECTIVE_DATE as Varchar(50)),
	NULL,  -- current_end_date
	ei.entity_structure_fk,
	1,
		10104,
		144020,
		ei.value,
		97,
		80,
		66,
		90,
		53,
		sv.SEGMENTS_VENDOR_ID,
		ird.JOB_FK,         
		ird.EFFECTIVE_DATE,                                                                                     
		ird.IMPORT_FILENAME,
		ird.import_package_fk,
		ird.DATA_POSITION26,
		ei.ENTITY_STRUCTURE_FK cust,
		ein.ENTITY_STRUCTURE_FK,    --vend
		getdate(),  
	ird.update_user      
 from import.IMPORT_RECORD_DATA IRD 
left join epacube.ENTITY_IDENTIFICATION ei on ei.value = ird.DATA_POSITION2   and ei.DATA_NAME_FK = 144111
left join epacube.SEGMENTS_VENDOR sv on sv.CUST_ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK and sv.DATA_NAME_FK = 500209
left join epacube.CODE_REF cr on cr.CODE_REF_ID = sv.VENDOR_TYPE_CR_FK
left join epacube.ENTITY_DATA_VALUE  edv on edv.value = ird.DATA_POSITION1  and edv.DATA_NAME_FK  =500209
and edv.ENTITY_DATA_VALUE_ID = sv.ENTITY_DATA_VALUE_FK
left join epacube.ENTITY_IDENT_NONUNIQUE ein on ein.ENTITY_STRUCTURE_FK = edv.ENTITY_STRUCTURE_FK and ein.ENTITY_STRUCTURE_FK = sv.VENDOR_ENTITY_STRUCTURE_FK
where ird.DATA_POSITION198 = 'VEND-DEL'
and sv.SEGMENTS_VENDOR_ID is not NULL
and ird.JOB_FK =   @IN_JOB_FK


---no vendor segment?
INSERT INTO synchronizer.EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[TABLE_ID_FK]			
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			   ,[END_DATE_NEW]
			   ,CUST_ENTITY_STRUCTURE_FK
			   ,VENDOR_ENTITY_STRUCTURE_FK
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])
SELECT DISTINCT
	 154,  -- ASSOC EVENT
	 10104,
	 getdate(),
	NULL,   --no product
	500209,
	'Vendor not Associated to Store' ,
	NULL,  -- current_end_date
	ei.entity_structure_fk,
	1,
		10104,
		144020,
		ei.value,
		97,
		80,
		66,
		90,
		53,
		edv.entity_data_value_ID,
		ird.JOB_FK,         
		ird.EFFECTIVE_DATE,                                                                                     
		ird.IMPORT_FILENAME,
		ird.import_package_fk,
	
		ird.DATA_POSITION26,
		ei.ENTITY_STRUCTURE_FK cust,
		ein.ENTITY_STRUCTURE_FK,    --vend
		getdate(),  
	ird.update_user      
 from import.IMPORT_RECORD_DATA IRD 
left join epacube.ENTITY_IDENTIFICATION ei on ei.value = ird.DATA_POSITION2   and ei.DATA_NAME_FK = 144111
left join epacube.SEGMENTS_VENDOR sv on sv.CUST_ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK and sv.DATA_NAME_FK = 500209
left join epacube.CODE_REF cr on cr.CODE_REF_ID = sv.VENDOR_TYPE_CR_FK
left join epacube.ENTITY_DATA_VALUE  edv on edv.value = ird.DATA_POSITION1  and edv.DATA_NAME_FK  =500209
and edv.ENTITY_DATA_VALUE_ID = sv.ENTITY_DATA_VALUE_FK
left join epacube.ENTITY_IDENT_NONUNIQUE ein on ein.ENTITY_STRUCTURE_FK = edv.ENTITY_STRUCTURE_FK and ein.ENTITY_STRUCTURE_FK = sv.VENDOR_ENTITY_STRUCTURE_FK
where ird.DATA_POSITION198 = 'VEND-DEL'
and sv.SEGMENTS_VENDOR_ID is  NULL
and ird.JOB_FK =   @IN_JOB_FK

--select    ei.value vendor_name, ei.ENTITY_STRUCTURE_FK,
-- cust.value customer, 
-- cr.code vendor_type , edv.value  store , sv.SEGMENTS_VENDOR_ID, sv.*
--from epacube.SEGMENTS_VENDOR sv 
--inner join epacube.CODE_REF cr on cr.CODE_REF_ID = sv.VENDOR_TYPE_CR_FK
-- left join epacube.ENTITY_IDENT_NONUNIQUE ei on ei.ENTITY_STRUCTURE_FK = sv.VENDOR_ENTITY_STRUCTURE_FK
--and ei.DATA_NAME_FK = 143112
--left join epacube.ENTITY_IDENTIFICATION cust on cust.ENTITY_STRUCTURE_FK = sv.cust_ENTITY_STRUCTURE_FK
--and cust.entity_DATA_NAME_FK = 144020
--left join epacube.ENTITY_DATA_VALUE edv on edv.ENTITY_DATA_VALUE_ID = sv.ENTITY_DATA_VALUE_FK
--where  1=1  
--and sv.DATA_NAME_FK = 500209
--and cust.value = '312'

--because the vendor store is being deleted then we need to remove the item vendor store relationship 
--from here if it's not already been indicated

--select * from epacube.ENTITY_DATA_VALUE where DATA_NAME_FK = 500209 and ENTITY_STRUCTURE_FK = 2139
--select * from epacube.ENTITY_IDENT_NONUNIQUE where ENTITY_STRUCTURE_FK = 1913

---dept DEL
INSERT INTO synchronizer.EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[TABLE_ID_FK]
			
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			 
			   ,[END_DATE_NEW]
			   ,CUST_ENTITY_STRUCTURE_FK
			   ,VENDOR_ENTITY_STRUCTURE_FK
			
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])
SELECT DISTINCT
	 154,  -- ASSOC EVENT
	 10104,
	 getdate(),
	NULL,   --no product
	141124,
	'DEPT-DEL - ' + ird.DATA_POSITION12,
	NULL,  -- current_end_date
	ei.entity_structure_fk,
	1,
		10104,
		144020,
		ei.value,
		97,
		80,
		66,
		90,
		53,
		edv.entity_data_value_ID,
		ird.JOB_FK,         
		ird.EFFECTIVE_DATE,                                                                                     
		ird.IMPORT_FILENAME,
		ird.import_package_fk,
	
		ird.DATA_POSITION26,
		ei.ENTITY_STRUCTURE_FK cust,
		NULL vend,   --no Vendor
		getdate(),  
	ird.update_user       
--select  ei.value,    edv.*
  from import.IMPORT_RECORD_DATA IRD 
 left join epacube.ENTITY_IDENTIFICATION ei on ei.value = ird.DATA_POSITION3   and ei.DATA_NAME_FK = 144111
  left join epacube.ENTITY_DATA_VALUE  edv on edv.value = ird.DATA_POSITION12 and edv.DATA_NAME_FK  =141124
and edv.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
where ird.DATA_POSITION198 = 'DEPT-DEL'
and ird.JOB_FK =  @IN_JOB_FK




----------------------------------------------------------

  INSERT INTO synchronizer.EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[TABLE_ID_FK]
			
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			 
			   ,[END_DATE_NEW]
			   ,CUST_ENTITY_STRUCTURE_FK
			   ,VENDOR_ENTITY_STRUCTURE_FK
			
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])
SELECT DISTINCT
	 155,  -- ASSOC EVENT
	 10109,
	 getdate(),
	pi.PRODUCT_STRUCTURE_FK, 
	159905,
	'PROD-DEL - ' + ird.DATA_POSITION26,
	pa.END_DATE,  -- current_end_date
	pi.PRODUCT_STRUCTURE_FK,
	1,
		10104,
		144020,
		ei.value,
		97,
		80,
		66,
		90,
		53,
		pa.PRODUCT_ASSOCIATION_ID,
		ird.JOB_FK,         
		ird.EFFECTIVE_DATE,                                                                                     
		ird.IMPORT_FILENAME,
		ird.import_package_fk,
	
		ird.DATA_POSITION26,
		ei.ENTITY_STRUCTURE_FK cust,
		ein.ENTITY_STRUCTURE_FK vend, 
		getdate(),  
	ird.update_user            
		
 from import.IMPORT_RECORD_DATA IRD 
inner join epacube.PRODUCT_IDENTIFICATION pi on pi.value = ird.DATA_POSITION4 and pi.DATA_NAME_FK = 110100
inner join epacube.ENTITY_IDENTIFICATION ei on ei.value = ird.DATA_POSITION3 and ei.DATA_NAME_FK = 144111
inner join epacube.PRODUCT_ASSOCIATION pa on pa.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
and ei.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK and pa.DATA_NAME_FK = 159905
inner join epacube.ENTITY_IDENT_NONUNIQUE ein on ein.value = ird.DATA_POSITION196 and ein.DATA_NAME_FK = 143112
where ird.DATA_POSITION198 = 'PROD-DEL'
and  ird.JOB_FK =   @in_job_fk

-----MISSING PRODUCT

INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,[CREATE_TIMESTAMP])

	select ird.job_fk, 'IMPORT', ird.IMPORT_RECORD_DATA_ID, 99, 'INVALID ITEM' , ird.DATA_POSITION4	, getdate()   		
 from import.IMPORT_RECORD_DATA IRD 
left join epacube.PRODUCT_IDENTIFICATION pi on pi.value = ird.DATA_POSITION4 and pi.DATA_NAME_FK = 110100
left join epacube.ENTITY_IDENTIFICATION ei on ei.value = ird.DATA_POSITION3 and ei.DATA_NAME_FK = 144111
left join epacube.PRODUCT_ASSOCIATION pa on pa.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
and ei.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK and pa.DATA_NAME_FK = 159905
left join epacube.ENTITY_IDENT_NONUNIQUE ein on ein.value = ird.DATA_POSITION196 and ein.DATA_NAME_FK = 143112
where pi.PRODUCT_STRUCTURE_FK is NULL
AND ird.DATA_POSITION198 = 'PROD-DEL'
AND ird.JOB_FK =   @in_job_fk


INSERT INTO synchronizer.EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[TABLE_ID_FK]
			
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			 
			   ,[END_DATE_NEW]
			   ,CUST_ENTITY_STRUCTURE_FK
			   ,VENDOR_ENTITY_STRUCTURE_FK
			
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])
SELECT DISTINCT
	 155,  -- ASSOC EVENT
	 10109,
	 getdate(),
	0,  --pi.PRODUCT_STRUCTURE_FK, 
	159905,
	'INVALID ITEM - ' + ird.DATA_POSITION4,
	NULL,  -- current_end_date
	0,  --no product
	1,
		10104,
		144020,
		ei.value,
		99,
		80,
		66,
		90,
		53,
		NULL,
		ird.JOB_FK,         
		ird.EFFECTIVE_DATE,                                                                                     
		ird.IMPORT_FILENAME,
		ird.import_package_fk,
	
		ird.DATA_POSITION26,
		ei.ENTITY_STRUCTURE_FK cust,
		ein.ENTITY_STRUCTURE_FK vend, 
		getdate(),  
	ird.update_user            
from import.IMPORT_RECORD_DATA IRD 
left join epacube.PRODUCT_IDENTIFICATION pi on pi.value = ird.DATA_POSITION4 and pi.DATA_NAME_FK = 110100
left join epacube.ENTITY_IDENTIFICATION ei on ei.value = ird.DATA_POSITION3 and ei.DATA_NAME_FK = 144111
left join epacube.PRODUCT_ASSOCIATION pa on pa.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
and ei.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK and pa.DATA_NAME_FK = 159905
left join epacube.ENTITY_IDENT_NONUNIQUE ein on ein.value = ird.DATA_POSITION196 and ein.DATA_NAME_FK = 143112
where pi.PRODUCT_STRUCTURE_FK is NULL
AND ird.DATA_POSITION198 = 'PROD-DEL'
AND ird.JOB_FK = @IN_JOB_FK

-----MISSING STORE AUTHORIZATION

INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
		   ,ERROR_DATA2
           ,[CREATE_TIMESTAMP])

	select ird.job_fk, 'IMPORT', ird.IMPORT_RECORD_DATA_ID, 99, 'Item Not Authorized to Store ' , ird.DATA_POSITION4, ei.value	, getdate()   		
 from import.IMPORT_RECORD_DATA IRD 
inner join epacube.PRODUCT_IDENTIFICATION pi on pi.value = ird.DATA_POSITION4 and pi.DATA_NAME_FK = 110100
inner join epacube.ENTITY_IDENTIFICATION ei on ei.value = ird.DATA_POSITION3 and ei.DATA_NAME_FK = 144111
left join epacube.PRODUCT_ASSOCIATION pa on pa.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
and ei.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK and pa.DATA_NAME_FK = 159905
left join epacube.ENTITY_IDENT_NONUNIQUE ein on ein.value = ird.DATA_POSITION196 and ein.DATA_NAME_FK = 143112
where pa.PRODUCT_ASSOCIATION_ID is NULL
AND pi.PRODUCT_STRUCTURE_FK is Not NULL
AND ird.DATA_POSITION198 = 'PROD-DEL'
and ird.JOB_FK =   @in_job_fk


INSERT INTO synchronizer.EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[TABLE_ID_FK]
			
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			 
			   ,[END_DATE_NEW]
			   ,CUST_ENTITY_STRUCTURE_FK
			   ,VENDOR_ENTITY_STRUCTURE_FK
			
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])
SELECT DISTINCT
	 155,  -- ASSOC EVENT
	 10109,
	 getdate(),
	pi.PRODUCT_STRUCTURE_FK, 
	159905,
	'Item Not Authorized to Store - ',
	NULL,  -- current_end_date
	pi.PRODUCT_STRUCTURE_FK,  
	1,
		10104,
		144020,
		ei.value,
		99,
		80,
		66,
		90,
		53,
		NULL,
		ird.JOB_FK,         
		ird.EFFECTIVE_DATE,                                                                                     
		ird.IMPORT_FILENAME,
		ird.import_package_fk,
	
		ird.DATA_POSITION26,
		ei.ENTITY_STRUCTURE_FK cust,
		ein.ENTITY_STRUCTURE_FK vend, 
		getdate(),  
	ird.update_user            
from import.IMPORT_RECORD_DATA IRD 
inner join epacube.PRODUCT_IDENTIFICATION pi on pi.value = ird.DATA_POSITION4 and pi.DATA_NAME_FK = 110100
inner join epacube.ENTITY_IDENTIFICATION ei on ei.value = ird.DATA_POSITION3 and ei.DATA_NAME_FK = 144111
left join epacube.PRODUCT_ASSOCIATION pa on pa.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
and ei.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK and pa.DATA_NAME_FK = 159905
left join epacube.ENTITY_IDENT_NONUNIQUE ein on ein.value = ird.DATA_POSITION196 and ein.DATA_NAME_FK = 143112
where pa.PRODUCT_ASSOCIATION_ID is NULL
AND pi.PRODUCT_STRUCTURE_FK is Not NULL
AND ird.DATA_POSITION198 = 'PROD-DEL'
and ird.JOB_FK =   @in_job_fk




-----MIssing STORE

--select pi.PRODUCT_STRUCTURE_FK, ei.value, pa.PRODUCT_ASSOCIATION_ID,  ird.* 
--from import.IMPORT_RECORD_DATA IRD 
--inner join epacube.PRODUCT_IDENTIFICATION pi on pi.value = ird.DATA_POSITION4 and pi.DATA_NAME_FK = 110100
--left join epacube.ENTITY_IDENTIFICATION ei on ei.value = ird.DATA_POSITION3 and ei.DATA_NAME_FK = 144111
--left join epacube.PRODUCT_ASSOCIATION pa on pa.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
--and ei.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK and pa.DATA_NAME_FK = 159905
--left join epacube.ENTITY_IDENT_NONUNIQUE ein on ein.value = ird.DATA_POSITION196 and ein.DATA_NAME_FK = 143112
--where ei.ENTITY_STRUCTURE_FK is NULL
--AND ird.DATA_POSITION198 = 'PROD-DEL'
--and ird.JOB_FK =   @in_job_fk





---------------------------------------------------------------------------------------
----   add Delay Delete days to event
---------------------------------------------------------------------------------------
update Ed
set current_data = 'Delay Delete Days =' + attribute_event_data
 from epacube.SEGMENTS_SETTINGS  SS 
inner join synchronizer.event_data ed on ss.CUST_ENTITY_STRUCTURE_FK = ed.CUST_ENTITY_STRUCTURE_FK
and ss.DATA_NAME_FK = 144861
and ed.IMPORT_JOB_FK = @IN_JOB_FK


-------------------------------------------------------------------
-- LAST Step is to Set Job Execution to Complete
-------------------------------------------------------------------------------------
----
----

      SET @V_END_TIMESTAMP = getdate()


      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT PRODUCTS TO STAGE' )

      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
                              where job_fk =  @in_job_fk
                              and   name = 'IMPORT PRODUCTS RESOLVED' )

     exec common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

-------------------------

 SET @status_desc =  'finished execution of import.LOAD_RORC_DEL_TRANS_TYPES'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;

 -- If isnull(@JobName, '') <> ''
	--Begin
	--	Update J
	--	Set Name = @JobName
	--	from common.job J
	--	where Job_ID = @in_job_fk	
	--End
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT.LOAD_RORC_DEL_TRANS_TYPES has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
 

END








































































































