﻿-- =============================================
-- Author:		Yuri Nogin
-- Create date: 11/14/2006
-- Description:	Creates a new CMD Exec job and assigns the statement_id to process
-- Modified:    7/27/2008 By Yuri Nogin. Added epa_job_id parameter
-- =============================================
CREATE PROCEDURE [job_manager].[Start_CMD_Job_sp] 
	-- Add the parameters for the stored procedure here
    @exec_id bigint,
	@cmd_statement nvarchar(4000),
    @JobQueue_job_id uniqueidentifier OUTPUT,
    @epa_job_id BIGINT = NULL 
AS
BEGIN
    
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
    -- Insert statements for procedure here
    BEGIN TRY
	DECLARE @sExec nvarchar(max);
    DECLARE @status_desc varchar(max);
	DECLARE @time_before datetime;
	DECLARE @time_after datetime;
	DECLARE @elapsed_time numeric(10,4);
    DECLARE @job_name nvarchar(100);
    DECLARE @job_id uniqueidentifier;
		-- create the job name as the getdate
		SET @job_name = convert(nvarchar(50),getdate(),109);		
    
		EXEC msdb.dbo.sp_add_job
			@job_name = @job_name, 
			@enabled = 1,
			@delete_level = 3,
            @notify_level_eventlog = 0,
            @job_id = @job_id output;

        SET @sExec = @cmd_statement;
        EXEC msdb.dbo.sp_add_jobstep
			@job_id = @job_id,
			@step_name = @job_name,
			@subsystem = N'CMDEXEC',
			@command = @sExec,
            @on_fail_action = 1, -- Quit with success
			@retry_attempts = 0 ; -- no retries


        EXEC msdb.dbo.sp_add_jobserver @job_id = @job_id;

        EXEC  msdb.dbo.sp_start_job @job_id = @job_id;
        
        Set @JobQueue_job_id = @job_id;
		

    END TRY
    BEGIN CATCH
		 EXEC exec_monitor.Report_Error_sp @exec_id = @exec_id, @epa_job_id = @epa_job_id
    END CATCH;

END





















