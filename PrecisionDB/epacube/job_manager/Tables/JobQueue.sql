﻿CREATE TABLE [job_manager].[JobQueue] (
    [job_id]         UNIQUEIDENTIFIER NOT NULL,
    [create_dt]      DATETIME         CONSTRAINT [DF_JobQueue_create_dt] DEFAULT (getdate()) NOT NULL,
    [create_user_id] VARCHAR (50)     CONSTRAINT [DF_JobQueue_create_user_id] DEFAULT (suser_name()) NOT NULL,
    [host_id]        VARCHAR (50)     CONSTRAINT [DF_JobQueue_host_id] DEFAULT (host_id()) NOT NULL,
    [host_name]      VARCHAR (50)     CONSTRAINT [DF_JobQueue_host_name] DEFAULT (host_name()) NOT NULL,
    [exec_id]        BIGINT           NOT NULL,
    [statement_id]   BIGINT           NOT NULL,
    [epa_job_id]     BIGINT           NULL,
    CONSTRAINT [PK_JobQueue] PRIMARY KEY CLUSTERED ([job_id] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_JobQueue_ExecLog] FOREIGN KEY ([exec_id]) REFERENCES [exec_monitor].[ExecLog] ([exec_id]),
    CONSTRAINT [FK_JobQueue_StatementQueue] FOREIGN KEY ([statement_id]) REFERENCES [exec_manager].[StatementQueue] ([statement_id])
);



