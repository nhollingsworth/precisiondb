﻿CREATE TABLE [synchronizer].[RULES_SCHEME_CUSTOMER_SEGMENT_DEFAULT] (
    [cust_segment_fk]       BIGINT        NOT NULL,
    [customer_segment_type] VARCHAR (64)  NOT NULL,
    [cust_segment]          VARCHAR (256) NULL,
    [RULES_SCHEME_FK]       BIGINT        NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_rscbs1]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_SEGMENT_DEFAULT]([RULES_SCHEME_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

