﻿CREATE TABLE [synchronizer].[RULES_SCHEME_BASIS_MAPS] (
    [Parent_Result_Data_Name_FK] INT          NOT NULL,
    [Node1_Cust_DV_FK]           BIGINT       NOT NULL,
    [Node2_Cust_DV_FK]           BIGINT       NOT NULL,
    [result_data_name_fk]        INT          NULL,
    [BASIS_CALC_DN_FK]           INT          NULL,
    [Node1_Cust]                 VARCHAR (50) NULL,
    [Node2_cust]                 VARCHAR (50) NULL,
    [Result_To_Calc]             VARCHAR (64) NOT NULL,
    [Calc_Basis]                 VARCHAR (64) NOT NULL,
    [Burden_Cost_to_OL]          INT          NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_01]
    ON [synchronizer].[RULES_SCHEME_BASIS_MAPS]([Node1_Cust_DV_FK] ASC, [Node2_Cust_DV_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

