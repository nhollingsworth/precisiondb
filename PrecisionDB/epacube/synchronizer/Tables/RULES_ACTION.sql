﻿CREATE TABLE [synchronizer].[RULES_ACTION] (
    [RULES_ACTION_ID]            BIGINT          IDENTITY (2000000000, 1) NOT NULL,
    [RULES_FK]                   BIGINT          NULL,
    [RULES_ACTION_OPERATION1_FK] INT             NULL,
    [RULES_ACTION_OPERATION2_FK] INT             NULL,
    [RULES_ACTION_OPERATION3_FK] INT             NULL,
    [COMPARE_OPERATOR_CR_FK]     INT             NULL,
    [BASIS_CALC_DN_FK]           INT             NULL,
    [BASIS1_DN_FK]               INT             NULL,
    [BASIS2_DN_FK]               INT             NULL,
    [BASIS3_DN_FK]               INT             NULL,
    [BASIS1_NUMBER]              NUMERIC (18, 6) NULL,
    [BASIS2_NUMBER]              NUMERIC (18, 6) NULL,
    [BASIS3_NUMBER]              NUMERIC (18, 6) NULL,
    [BASIS1_VALUE]               VARCHAR (128)   NULL,
    [BASIS2_VALUE]               VARCHAR (128)   NULL,
    [BASIS3_VALUE]               VARCHAR (128)   NULL,
    [PREFIX1]                    VARCHAR (16)    NULL,
    [PREFIX2]                    VARCHAR (16)    NULL,
    [PREFIX3]                    VARCHAR (16)    NULL,
    [BASIS_CALC_PRICESHEET_NAME] VARCHAR (128)   NULL,
    [BASIS_CALC_PRICESHEET_DATE] DATETIME        NULL,
    [BASIS1_PRICESHEET_NAME]     VARCHAR (128)   NULL,
    [BASIS1_PRICESHEET_DATE]     DATETIME        NULL,
    [BASIS2_PRICESHEET_NAME]     VARCHAR (128)   NULL,
    [BASIS2_PRICESHEET_DATE]     DATETIME        NULL,
    [BASIS3_PRICESHEET_NAME]     VARCHAR (128)   NULL,
    [BASIS3_PRICESHEET_DATE]     DATETIME        NULL,
    [BASIS_USE_CORP_ONLY_IND]    SMALLINT        NULL,
    [ROUNDING_METHOD_CR_FK]      INT             NULL,
    [ROUNDING_TO_VALUE]          NUMERIC (18, 6) NULL,
    [ROUNDING_ADDER_VALUE]       NUMERIC (18, 6) NULL,
    [RULES_OPTIMIZATION_FK]      BIGINT          NULL,
    [FORMULA]                    VARCHAR (256)   NULL,
    [EVENT_PRIORITY_CR_FK]       INT             NULL,
    [EVENT_STATUS_CR_FK]         INT             NULL,
    [EVENT_CONDITION_CR_FK]      INT             NULL,
    [ERROR_NAME]                 VARCHAR (128)   NULL,
    [RECORD_STATUS_CR_FK]        INT             CONSTRAINT [DF__RULE_ACT__RECOR__361203C5] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]           DATETIME        CONSTRAINT [DF__RULE_ACT__CREAT__370627FE] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]           DATETIME        CONSTRAINT [DF__RULE_ACT__UPDAT__37FA4C37] DEFAULT (getdate()) NULL,
    [UPDATE_USER]                VARCHAR (64)    NULL,
    [UPDATE_LOG_FK]              BIGINT          NULL,
    CONSTRAINT [PK_RULE_ACTION] PRIMARY KEY CLUSTERED ([RULES_ACTION_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_RA_B3DNF_DN] FOREIGN KEY ([BASIS3_DN_FK]) REFERENCES [epacube].[DATA_NAME] ([DATA_NAME_ID]),
    CONSTRAINT [FK_RA_BODNF_DN] FOREIGN KEY ([BASIS1_DN_FK]) REFERENCES [epacube].[DATA_NAME] ([DATA_NAME_ID]),
    CONSTRAINT [FK_RA_BTDNF_DN] FOREIGN KEY ([BASIS2_DN_FK]) REFERENCES [epacube].[DATA_NAME] ([DATA_NAME_ID]),
    CONSTRAINT [FK_RA_ECCRF_CR] FOREIGN KEY ([EVENT_CONDITION_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID]),
    CONSTRAINT [FK_RA_EPCRF_CR] FOREIGN KEY ([EVENT_PRIORITY_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID]),
    CONSTRAINT [FK_RA_ESCRF_CR] FOREIGN KEY ([EVENT_STATUS_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID]),
    CONSTRAINT [FK_RA_RAOF_RAO] FOREIGN KEY ([RULES_ACTION_OPERATION1_FK]) REFERENCES [synchronizer].[RULES_ACTION_OPERATION] ([RULES_ACTION_OPERATION_ID]),
    CONSTRAINT [FK_RA_RF_R] FOREIGN KEY ([RULES_FK]) REFERENCES [synchronizer].[RULES] ([RULES_ID]),
    CONSTRAINT [FK_RA_RMCRF_CR] FOREIGN KEY ([ROUNDING_METHOD_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID]),
    CONSTRAINT [FK_RA_RSCRF_CR] FOREIGN KEY ([RECORD_STATUS_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID])
);


GO
CREATE NONCLUSTERED INDEX [_dta_index_RULES_ACTION_5_328440294__K3_K8_K1_K2_17]
    ON [synchronizer].[RULES_ACTION]([PREFIX1] ASC, [RULES_ACTION_OPERATION1_FK] ASC, [BASIS1_DN_FK] ASC, [RULES_ACTION_ID] ASC, [RULES_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [RA_RULES_OPTIMIZATION_20110225]
    ON [synchronizer].[RULES_ACTION]([RULES_ACTION_ID] ASC, [RULES_OPTIMIZATION_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_RULES_ACTION_10_1545525035__K14_2]
    ON [synchronizer].[RULES_ACTION]([RULES_FK] ASC, [BASIS1_DN_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_RULES_ACTION_10_1545525035__K15_K16_K14_2]
    ON [synchronizer].[RULES_ACTION]([RULES_FK] ASC, [BASIS2_DN_FK] ASC, [BASIS3_DN_FK] ASC, [BASIS1_DN_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_RULES_ACTION_10_1545525035__K16_2]
    ON [synchronizer].[RULES_ACTION]([RULES_FK] ASC, [BASIS3_DN_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_RULES_ACTION_5_328440294__K2_K7_K8_K9_K10]
    ON [synchronizer].[RULES_ACTION]([RULES_FK] ASC, [BASIS_CALC_DN_FK] ASC, [BASIS1_DN_FK] ASC, [BASIS2_DN_FK] ASC, [BASIS3_DN_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

