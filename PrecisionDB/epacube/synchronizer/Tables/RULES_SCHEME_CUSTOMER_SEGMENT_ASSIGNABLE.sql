﻿CREATE TABLE [synchronizer].[RULES_SCHEME_CUSTOMER_SEGMENT_ASSIGNABLE] (
    [schedule]                 INT           NULL,
    [Cust_Segment_FK]          BIGINT        NOT NULL,
    [Cust_Segment]             VARCHAR (256) NULL,
    [Cust_Segment_Description] VARCHAR (256) NULL,
    [Assignable_Rules]         INT           NULL,
    [rules_scheme_FK]          BIGINT        NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_sa1]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_SEGMENT_ASSIGNABLE]([rules_scheme_FK] ASC, [Cust_Segment_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

