﻿CREATE TABLE [synchronizer].[RULES_ACTION_OPERANDS] (
    [RULES_ACTION_OPERANDS_ID]       BIGINT          IDENTITY (2000000000, 1) NOT NULL,
    [RULES_ACTION_FK]                BIGINT          NOT NULL,
    [BASIS_POSITION]                 SMALLINT        NULL,
    [RULES_FILTER_LOOKUP_OPERAND_FK] INT             NULL,
    [OPERAND_FILTER_DN_FK]           INT             NULL,
    [OPERAND_FILTER_OPERATOR_CR_FK]  INT             NULL,
    [OPERAND1]                       NUMERIC (18, 6) NULL,
    [OPERAND2]                       NUMERIC (18, 6) NULL,
    [OPERAND3]                       NUMERIC (18, 6) NULL,
    [OPERAND4]                       NUMERIC (18, 6) NULL,
    [OPERAND5]                       NUMERIC (18, 6) NULL,
    [OPERAND6]                       NUMERIC (18, 6) NULL,
    [OPERAND7]                       NUMERIC (18, 6) NULL,
    [OPERAND8]                       NUMERIC (18, 6) NULL,
    [OPERAND9]                       NUMERIC (18, 6) NULL,
    [OPERAND10]                      NUMERIC (18, 6) NULL,
    [FILTER_VALUE1]                  VARCHAR (32)    NULL,
    [FILTER_VALUE2]                  VARCHAR (32)    NULL,
    [FILTER_VALUE3]                  VARCHAR (32)    NULL,
    [FILTER_VALUE4]                  VARCHAR (32)    NULL,
    [FILTER_VALUE5]                  VARCHAR (32)    NULL,
    [FILTER_VALUE6]                  VARCHAR (32)    NULL,
    [FILTER_VALUE7]                  VARCHAR (32)    NULL,
    [FILTER_VALUE8]                  VARCHAR (32)    NULL,
    [FILTER_VALUE9]                  VARCHAR (32)    NULL,
    [FILTER_VALUE10]                 VARCHAR (32)    NULL,
    [RECORD_STATUS_CR_FK]            INT             CONSTRAINT [DF__RAOP__RECOR__361203C5] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]               DATETIME        CONSTRAINT [DF__RAOP__CREAT__370627FE] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]               DATETIME        CONSTRAINT [DF__RAOP__UPDAT__37FA4C37] DEFAULT (getdate()) NULL,
    [UPDATE_USER]                    VARCHAR (64)    NULL,
    [UPDATE_LOG_FK]                  BIGINT          NULL,
    CONSTRAINT [PK_RULE_ACTION_OPERANDS] PRIMARY KEY CLUSTERED ([RULES_ACTION_OPERANDS_ID] ASC),
    CONSTRAINT [FK_RAOPD_RAF_RA] FOREIGN KEY ([RULES_ACTION_FK]) REFERENCES [synchronizer].[RULES_ACTION] ([RULES_ACTION_ID]),
    CONSTRAINT [FK_RAOPD_RSCRF_CR] FOREIGN KEY ([RECORD_STATUS_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID])
);


GO
CREATE NONCLUSTERED INDEX [rao_RULES_ACTION_FK]
    ON [synchronizer].[RULES_ACTION_OPERANDS]([RULES_ACTION_FK] ASC, [BASIS_POSITION] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

