﻿CREATE TABLE [synchronizer].[RULES_FILTER_LOOKUP_OPERAND] (
    [RULES_FILTER_LOOKUP_OPERAND_ID] BIGINT       IDENTITY (1000, 1) NOT NULL,
    [NAME]                           VARCHAR (64) NULL,
    [RULES_FILTER_DN_FK]             INT          NULL,
    [RECORD_STATUS_CR_FK]            INT          CONSTRAINT [DF__RULES_FLO__RECOR__4F47C5E3] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]               DATETIME     CONSTRAINT [DF__RULES_FLO__CREAT__503BEA1C] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]               DATETIME     CONSTRAINT [DF__RULES_FLO_UPDAT__51300E55] DEFAULT (getdate()) NULL,
    [UPDATE_LOG_FK]                  BIGINT       NULL,
    CONSTRAINT [PK_RULES_FILTER_LOOKUP_OPERAND] PRIMARY KEY CLUSTERED ([RULES_FILTER_LOOKUP_OPERAND_ID] ASC),
    CONSTRAINT [FK_RFLO_LDNF_DN] FOREIGN KEY ([RULES_FILTER_DN_FK]) REFERENCES [epacube].[DATA_NAME] ([DATA_NAME_ID]),
    CONSTRAINT [FK_RFLO_RSCRF_CR] FOREIGN KEY ([RECORD_STATUS_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID])
);

