﻿CREATE TABLE [synchronizer].[EVENT_CALC] (
    [EVENT_ID]                         BIGINT          IDENTITY (1000, 1) NOT NULL,
    [JOB_FK]                           BIGINT          NULL,
    [RESULT_DATA_NAME_FK]              INT             NULL,
    [RESULT_TYPE_CR_FK]                INT             NULL,
    [RESULT_EFFECTIVE_DATE]            DATETIME        CONSTRAINT [DF_EC_EFFECTIVE_DATE] DEFAULT (getdate()) NULL,
    [PRODUCT_STRUCTURE_FK]             BIGINT          NULL,
    [ORG_ENTITY_STRUCTURE_FK]          BIGINT          NULL,
    [CUST_ENTITY_STRUCTURE_FK]         BIGINT          NULL,
    [SUPL_ENTITY_STRUCTURE_FK]         BIGINT          NULL,
    [RESULT]                           NUMERIC (18, 6) NULL,
    [RULES_FK]                         BIGINT          NULL,
    [RULE_TYPE_CR_FK]                  INT             NULL,
    [CALC_GROUP_SEQ]                   INT             NULL,
    [PARITY_DV_FK]                     INT             NULL,
    [MODEL_SALES_QTY]                  NUMERIC (18, 6) NULL,
    [VALUE_UOM_CODE_FK]                INT             NULL,
    [PRICING_COST_BASIS_AMT]           NUMERIC (18, 6) NULL,
    [MARGIN_COST_BASIS_AMT]            NUMERIC (18, 6) NULL,
    [ORDER_COGS_AMT]                   NUMERIC (18, 6) NULL,
    [REBATED_COST_CB_AMT]              NUMERIC (18, 6) NULL,
    [REBATE_CB_AMT]                    NUMERIC (18, 6) NULL,
    [REBATE_BUY_AMT]                   NUMERIC (18, 6) NULL,
    [SELL_PRICE_CUST_AMT]              NUMERIC (18, 6) NULL,
    [MARGIN_AMT]                       NUMERIC (18, 6) NULL,
    [MARGIN_PCT]                       NUMERIC (18, 6) NULL,
    [RELATED_EVENT_FK]                 BIGINT          NULL,
    [RESULT_PARENT_DATA_NAME_FK]       INT             NULL,
    [COLUMN_NAME]                      VARCHAR (32)    NULL,
    [BATCH_NO]                         BIGINT          NULL,
    [EVENT_SOURCE_CR_FK]               INT             CONSTRAINT [DF_EVENT_CALC_EVENT_SOURCE_CR_FK] DEFAULT ((77)) NULL,
    [EVENT_CONDITION_CR_FK]            INT             CONSTRAINT [DF_EC_EVENT_CONDITION_CR_FK] DEFAULT ((97)) NULL,
    [RECORD_STATUS_CR_FK]              INT             CONSTRAINT [DF_EVENT_CALC_RECORD_STATUS_CR_FK] DEFAULT ((1)) NULL,
    [UPDATE_TIMESTAMP]                 DATETIME        CONSTRAINT [DF_EC_UPDAT_1B0907CE] DEFAULT (getdate()) NULL,
    [UPDATE_USER]                      VARCHAR (64)    NULL,
    [UPDATE_LOG_FK]                    BIGINT          NULL,
    [EFF_END_DATE]                     DATE            NULL,
    [Product_Structure_FK_Subbed_From] BIGINT          NULL,
    [Product_Structure_FK_Subbed_To]   BIGINT          NULL,
    [SPC_UOM]                          NUMERIC (18, 6) NULL,
    [SOURCE_FK]                        BIGINT          NULL,
    CONSTRAINT [EC_PK] PRIMARY KEY CLUSTERED ([EVENT_ID] ASC),
    CONSTRAINT [FK_EC_CESF_ES] FOREIGN KEY ([CUST_ENTITY_STRUCTURE_FK]) REFERENCES [epacube].[ENTITY_STRUCTURE] ([ENTITY_STRUCTURE_ID]),
    CONSTRAINT [FK_EC_DNF_DN] FOREIGN KEY ([RESULT_DATA_NAME_FK]) REFERENCES [epacube].[DATA_NAME] ([DATA_NAME_ID]),
    CONSTRAINT [FK_EC_ECCF_CR] FOREIGN KEY ([EVENT_CONDITION_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID]),
    CONSTRAINT [FK_EC_OESF_ES] FOREIGN KEY ([ORG_ENTITY_STRUCTURE_FK]) REFERENCES [epacube].[ENTITY_STRUCTURE] ([ENTITY_STRUCTURE_ID]),
    CONSTRAINT [FK_EC_RSCF_CR] FOREIGN KEY ([RECORD_STATUS_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID]),
    CONSTRAINT [FK_EC_RTCF_CR] FOREIGN KEY ([RESULT_TYPE_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID]),
    CONSTRAINT [FK_EC_SESF_ES] FOREIGN KEY ([SUPL_ENTITY_STRUCTURE_FK]) REFERENCES [epacube].[ENTITY_STRUCTURE] ([ENTITY_STRUCTURE_ID])
);


GO
CREATE NONCLUSTERED INDEX [IDX_EC_BN_RDNF_IEID_IJ_IS_IRTCF]
    ON [synchronizer].[EVENT_CALC]([EVENT_ID] ASC, [JOB_FK] ASC, [RESULT_TYPE_CR_FK] ASC, [BATCH_NO] ASC, [RESULT_DATA_NAME_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_EC_CEF_IEID]
    ON [synchronizer].[EVENT_CALC]([EVENT_ID] ASC, [CUST_ENTITY_STRUCTURE_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [EC_20100913_REF_JF_RDNF_PF_OEF_IEID_IREF]
    ON [synchronizer].[EVENT_CALC]([EVENT_ID] ASC, [RELATED_EVENT_FK] ASC, [JOB_FK] ASC, [RESULT_DATA_NAME_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC, [ORG_ENTITY_STRUCTURE_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [EC_20100913_EC_TO_ED]
    ON [synchronizer].[EVENT_CALC]([EVENT_ID] ASC, [RESULT] ASC, [JOB_FK] ASC, [RESULT_PARENT_DATA_NAME_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC, [ORG_ENTITY_STRUCTURE_FK] ASC, [COLUMN_NAME] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_EC_OEF_IEID]
    ON [synchronizer].[EVENT_CALC]([EVENT_ID] ASC, [ORG_ENTITY_STRUCTURE_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_EC_PF_IEID]
    ON [synchronizer].[EVENT_CALC]([EVENT_ID] ASC, [PRODUCT_STRUCTURE_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_EVENT_CALC_5_278344106__K18_K1_K5_12]
    ON [synchronizer].[EVENT_CALC]([RESULT] ASC, [RELATED_EVENT_FK] ASC, [EVENT_ID] ASC, [RESULT_DATA_NAME_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [EC_20100913_REF_JF_BN_IEID]
    ON [synchronizer].[EVENT_CALC]([EVENT_ID] ASC, [RELATED_EVENT_FK] ASC, [JOB_FK] ASC, [BATCH_NO] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [_dta_index_EVENT_CALC_5_774762313__K5_K19_K10_K1]
    ON [synchronizer].[EVENT_CALC]([RESULT_DATA_NAME_FK] ASC, [BATCH_NO] ASC, [CUST_ENTITY_STRUCTURE_FK] ASC, [EVENT_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_EC_SEF_IEID]
    ON [synchronizer].[EVENT_CALC]([EVENT_ID] ASC, [SUPL_ENTITY_STRUCTURE_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

