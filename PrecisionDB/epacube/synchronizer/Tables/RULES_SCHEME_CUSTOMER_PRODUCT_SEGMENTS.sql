﻿CREATE TABLE [synchronizer].[RULES_SCHEME_CUSTOMER_PRODUCT_SEGMENTS] (
    [Product_segment_Type]        VARCHAR (64)  NULL,
    [Product_Segment]             VARCHAR (256) NULL,
    [CUST_ENTITY_STRUCTURE_FK]    BIGINT        NOT NULL,
    [Rules_Scheme_FK]             BIGINT        NOT NULL,
    [Qualifying_Rules_Identifier] BIGINT        NULL,
    [Rule_Count]                  INT           NULL
);

