﻿CREATE TABLE [synchronizer].[Rules_Scheme] (
    [Parent_Result_Data_Name_FK] INT           NOT NULL,
    [Node1_Cust_DV_FK]           BIGINT        NOT NULL,
    [Node1_Cust]                 VARCHAR (50)  NULL,
    [Node1_Cust_Desc]            VARCHAR (128) NULL,
    [Node1_Cust_Data_Name_FK]    INT           NOT NULL,
    [Node2_Cust_DV_FK]           BIGINT        NOT NULL,
    [Node2_Cust]                 VARCHAR (50)  NULL,
    [Node2_Cust_Data_Name_FK]    INT           NOT NULL,
    [Node2_Prod_DV_FK]           BIGINT        NULL,
    [Node2_Prod]                 VARCHAR (2)   NOT NULL,
    [Node2_Prod_Description]     VARCHAR (514) NOT NULL,
    [Node2_Precedence]           INT           NOT NULL,
    [precedence_cust]            INT           NOT NULL,
    [schedule]                   INT           NOT NULL,
    [RULES_SCHEME_ID]            BIGINT        IDENTITY (1, 1) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_rs]
    ON [synchronizer].[Rules_Scheme]([Node1_Cust_DV_FK] ASC, [Node2_Cust_DV_FK] ASC, [Node2_Prod_DV_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_rs_id]
    ON [synchronizer].[Rules_Scheme]([RULES_SCHEME_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

