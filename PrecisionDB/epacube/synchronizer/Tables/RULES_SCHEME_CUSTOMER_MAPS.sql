﻿CREATE TABLE [synchronizer].[RULES_SCHEME_CUSTOMER_MAPS] (
    [cust_entity_structure_fk]      BIGINT        NOT NULL,
    [Customer_Number]               VARCHAR (128) NULL,
    [Customer_Name]                 VARCHAR (128) NULL,
    [Parent_Result_Data_Name_FK]    INT           NOT NULL,
    [Node1_Cust_DV_FK]              BIGINT        NOT NULL,
    [Node1_Cust]                    VARCHAR (50)  NULL,
    [Node1_Cust_Desc]               VARCHAR (128) NULL,
    [Node1_Cust_Data_Name_FK]       INT           NOT NULL,
    [Node2_Cust_DV_FK]              BIGINT        NOT NULL,
    [Node2_Cust]                    VARCHAR (50)  NULL,
    [Node2_Cust_Data_Name_FK]       INT           NOT NULL,
    [Node2_Prod_DV_FK]              BIGINT        NULL,
    [Node2_Prod]                    VARCHAR (2)   NOT NULL,
    [Node2_Prod_Description]        VARCHAR (514) NOT NULL,
    [Node2_Precedence]              INT           NOT NULL,
    [precedence_cust]               INT           NOT NULL,
    [schedule]                      INT           NOT NULL,
    [Rules_Scheme_FK]               BIGINT        NOT NULL,
    [rules_fk]                      BIGINT        NOT NULL,
    [prod_segment_fk]               BIGINT        NULL,
    [CUST_SEGMENT_FK]               BIGINT        NOT NULL,
    [result_data_name_fk]           INT           NULL,
    [BASIS_CALC_DN_FK]              INT           NULL,
    [Customer_Segment_Type]         VARCHAR (64)  NOT NULL,
    [Cust_Segment]                  VARCHAR (256) NULL,
    [Cust_Segment_Description]      VARCHAR (256) NULL,
    [Product_Segment_Type]          VARCHAR (64)  NULL,
    [Product_Segment]               VARCHAR (256) NULL,
    [Qualifying_Rules_Identifier]   BIGINT        NULL,
    [RULES_SCHEME_CUSTOMER_MAPS_ID] BIGINT        IDENTITY (1, 1) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx1]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_MAPS]([cust_entity_structure_fk] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx2]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_MAPS]([Customer_Number] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx3]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_MAPS]([Node2_Prod_DV_FK] ASC, [prod_segment_fk] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx4]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_MAPS]([Rules_Scheme_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx5]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_MAPS]([Qualifying_Rules_Identifier] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx6]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_MAPS]([rules_fk] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [isx7]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_MAPS]([Node1_Cust_DV_FK] ASC, [Node2_Cust_DV_FK] ASC, [Node2_Prod_DV_FK] ASC, [CUST_SEGMENT_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx8]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_MAPS]([Cust_Segment] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

