﻿CREATE TABLE [synchronizer].[EVENT_DATA_HISTORY_DETAILS] (
    [EVENT_DATA_HISTORY_DETAILS_ID] BIGINT          IDENTITY (1000, 1) NOT NULL,
    [EVENT_DATA_HISTORY_HEADER_FK]  BIGINT          NOT NULL,
    [EVENT_ID]                      BIGINT          NOT NULL,
    [EVENT_TYPE_CR_FK]              INT             NULL,
    [EVENT_ENTITY_CLASS_CR_FK]      INT             NULL,
    [EVENT_EFFECTIVE_DATE]          DATETIME        NULL,
    [EPACUBE_ID]                    BIGINT          NULL,
    [DATA_NAME_FK]                  INT             NULL,
    [ENTITY_CLASS_CR_FK]            INT             NULL,
    [ENTITY_DATA_NAME_FK]           INT             NULL,
    [LEVEL_SEQ]                     INT             NULL,
    [ASSOC_PARENT_CHILD]            VARCHAR (8)     NULL,
    [NEW_DATA]                      VARCHAR (MAX)   NULL,
    [CURRENT_DATA]                  VARCHAR (MAX)   NULL,
    [NET_VALUE1_NEW]                NUMERIC (18, 6) NULL,
    [NET_VALUE1_CUR]                NUMERIC (18, 6) NULL,
    [PERCENT_CHANGE1]               NUMERIC (18, 6) NULL,
    [MARGIN_PERCENT]                NUMERIC (18, 6) NULL,
    [MARGIN_DOLLARS]                NUMERIC (18, 6) NULL,
    [VALUES_SHEET_NAME]             VARCHAR (64)    NULL,
    [VALUES_SHEET_DATE]             DATETIME        NULL,
    [VALUE_UOM_CODE_FK]             INT             NULL,
    [ASSOC_UOM_CODE_FK]             INT             NULL,
    [UOM_CONVERSION_FACTOR]         NUMERIC (18, 6) NULL,
    [DATA_VALUE_FK]                 BIGINT          NULL,
    [ENTITY_DATA_VALUE_FK]          BIGINT          NULL,
    [VALUE_EFFECTIVE_DATE]          DATETIME        NULL,
    [EFFECTIVE_DATE_CUR]            DATETIME        NULL,
    [END_DATE_NEW]                  DATETIME        NULL,
    [END_DATE_CUR]                  DATETIME        NULL,
    [DATA_LEVEL]                    VARCHAR (128)   NULL,
    [EVENT_CONDITION_CR_FK]         INT             NOT NULL,
    [EVENT_STATUS_CR_FK]            INT             NOT NULL,
    [EVENT_PRIORITY_CR_FK]          INT             NOT NULL,
    [EVENT_SOURCE_CR_FK]            INT             NOT NULL,
    [EVENT_ACTION_CR_FK]            INT             NOT NULL,
    [EVENT_REASON_CR_FK]            INT             NULL,
    [RESULT_TYPE_CR_FK]             INT             NULL,
    [CUR_RESULT_TYPE_CR_FK]         INT             NULL,
    [SINGLE_ASSOC_IND]              SMALLINT        NULL,
    [AUDIT_FILENAME]                VARCHAR (128)   NULL,
    [JOB_CLASS_FK]                  INT             NULL,
    [CALC_JOB_FK]                   BIGINT          NULL,
    [IMPORT_JOB_FK]                 BIGINT          NULL,
    [IMPORT_DATE]                   DATETIME        NULL,
    [IMPORT_PACKAGE_FK]             INT             NULL,
    [IMPORT_FILENAME]               VARCHAR (128)   NULL,
    [IMPORT_BATCH_NO]               BIGINT          NULL,
    [BATCH_NO]                      BIGINT          NULL,
    [STG_RECORD_FK]                 BIGINT          NULL,
    [TABLE_ID_FK]                   BIGINT          NULL,
    [RULES_FK]                      BIGINT          NULL,
    [PREV_RULES_FK]                 BIGINT          NULL,
    [RELATED_EVENT_FK]              BIGINT          NULL,
    [RECORD_STATUS_CR_FK]           INT             NULL,
    [UPDATE_TIMESTAMP]              DATETIME        NULL,
    [UPDATE_USER]                   VARCHAR (64)    NULL,
    [CUST_DATA_VALUE_FK]            BIGINT          NULL,
    [DEPT_ENTITY_DATA_VALUE_FK]     INT             NULL,
    [RESULT_DATA_VALUE_FK]          BIGINT          NULL,
    [PRECEDENCE]                    INT             NULL,
    [ZONE_TYPE_CR_FK]               INT             NULL,
    [SEARCH1]                       VARCHAR (128)   NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_jfk]
    ON [synchronizer].[EVENT_DATA_HISTORY_DETAILS]([IMPORT_JOB_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_to_hdr1]
    ON [synchronizer].[EVENT_DATA_HISTORY_DETAILS]([EVENT_DATA_HISTORY_HEADER_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_to_hdr2]
    ON [synchronizer].[EVENT_DATA_HISTORY_DETAILS]([EVENT_DATA_HISTORY_HEADER_FK] ASC, [DATA_NAME_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

