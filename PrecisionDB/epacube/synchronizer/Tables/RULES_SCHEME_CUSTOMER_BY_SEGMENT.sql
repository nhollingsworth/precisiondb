﻿CREATE TABLE [synchronizer].[RULES_SCHEME_CUSTOMER_BY_SEGMENT] (
    [cust_segment_fk]          BIGINT        NOT NULL,
    [customer_segment_type]    VARCHAR (64)  NOT NULL,
    [cust_segment]             VARCHAR (256) NULL,
    [cust_entity_structure_fk] BIGINT        NOT NULL,
    [RULES_SCHEME_FK]          BIGINT        NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_rscbs1]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_BY_SEGMENT]([RULES_SCHEME_FK] ASC, [cust_entity_structure_fk] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_rscbs2]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_BY_SEGMENT]([RULES_SCHEME_FK] ASC, [cust_entity_structure_fk] ASC, [cust_segment_fk] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

