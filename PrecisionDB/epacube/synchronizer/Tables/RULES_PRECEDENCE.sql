﻿CREATE TABLE [synchronizer].[RULES_PRECEDENCE] (
    [RULES_PRECEDENCE_ID]               INT           NOT NULL,
    [RULES_PRECEDENCE_SCHEME_FK]        INT           NULL,
    [HOST_PRECEDENCE_LEVEL]             VARCHAR (64)  NULL,
    [HOST_PRECEDENCE_LEVEL_DESCRIPTION] VARCHAR (64)  NULL,
    [PROD_FILTER_DN_FK]                 INT           NULL,
    [ORG_FILTER_ENTITY_DN_FK]           INT           NULL,
    [ORG_FILTER_DN_FK]                  INT           NULL,
    [CUST_FILTER_ENTITY_DN_FK]          INT           NULL,
    [CUST_FILTER_DN_FK]                 INT           NULL,
    [SUPL_FILTER_ENTITY_DN_FK]          INT           NULL,
    [SUPL_FILTER_DN_FK]                 INT           NULL,
    [RULE_PRECEDENCE]                   SMALLINT      NOT NULL,
    [COMMENT]                           VARCHAR (256) NULL,
    [RECORD_STATUS_CR_FK]               INT           CONSTRAINT [DF__RULE_RP__RECOR__361203C5] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]                  DATETIME      CONSTRAINT [DF__RULE_RP__CREAT__370627FE] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]                  DATETIME      CONSTRAINT [DF__RULE_RP__UPDAT__37FA4C37] DEFAULT (getdate()) NULL,
    [UPDATE_USER]                       VARCHAR (64)  NULL,
    [UPDATE_LOG_FK]                     BIGINT        NULL,
    CONSTRAINT [PK_RULES_PRECEDENCE] PRIMARY KEY CLUSTERED ([RULES_PRECEDENCE_ID] ASC),
    CONSTRAINT [FK_PR_RPS] FOREIGN KEY ([RULES_PRECEDENCE_SCHEME_FK]) REFERENCES [synchronizer].[RULES_PRECEDENCE_SCHEME] ([RULES_PRECEDENCE_SCHEME_ID])
);

