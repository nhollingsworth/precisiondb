﻿CREATE TABLE [synchronizer].[RULES] (
    [RULES_ID]                             BIGINT        IDENTITY (2000000000, 1) NOT NULL,
    [RULES_STRUCTURE_FK]                   BIGINT        NULL,
    [RULES_SCHEDULE_FK]                    BIGINT        NULL,
    [NAME]                                 VARCHAR (128) NOT NULL,
    [SHORT_NAME]                           VARCHAR (32)  NULL,
    [SCHEDULE1]                            INT           NULL,
    [PRECEDENCE_ACTION1_CR_FK]             INT           NULL,
    [SCHEDULE2]                            INT           NULL,
    [PRECEDENCE_ACTION2_CR_FK]             INT           NULL,
    [RESULT_DATA_NAME_FK]                  INT           NULL,
    [RESULT_DATA_NAME2_FK]                 BIGINT        NULL,
    [CALC_RESULT_DATA_NAME_FK]             INT           NULL,
    [PRECEDENCE]                           INT           NULL,
    [UOM_DN_FK]                            BIGINT        NULL,
    [UOM_Code_FK]                          BIGINT        NULL,
    [EFFECTIVE_DATE]                       DATETIME      NULL,
    [END_DATE]                             DATETIME      NULL,
    [HOST_RULE_XREF]                       VARCHAR (128) NULL,
    [OPERAND_FILTER_CR_FK]                 INT           NULL,
    [OPERATIONS_CR_FK]                     INT           NULL,
    [REFERENCE]                            VARCHAR (128) NULL,
    [CONTRACT_NO]                          VARCHAR (128) NULL,
    [UNIQUE_KEY1]                          VARCHAR (128) NULL,
    [DISPLAY_SEQ]                          INT           NULL,
    [EVENT_ACTION_CR_FK]                   INT           NULL,
    [EVENT_CONDITION_CR_FK]                INT           NULL,
    [PROMO_IND]                            INT           NULL,
    [Rules_Type_Cust_Segment_Data_Name_FK] BIGINT        NULL,
    [Rules_Type_Cust_Segment_FK]           BIGINT        NULL,
    [Prod_Segment_Data_Name_FK]            BIGINT        NULL,
    [Prod_Segment_FK]                      BIGINT        NULL,
    [Cust_Segment_Data_Name_FK]            BIGINT        NULL,
    [Cust_Segment_FK]                      BIGINT        NULL,
    [Cust_Parent_Segment_FK]               BIGINT        NULL,
    [Org_Segment_Data_Name_FK]             BIGINT        NULL,
    [Org_Segment_FK]                       BIGINT        NULL,
    [Vendor_Segment_Data_Name_FK]          BIGINT        NULL,
    [Vendor_Segment_FK]                    BIGINT        NULL,
    [Rules_FK_Sub]                         BIGINT        NULL,
    [Host_Rule_XRef_Sub]                   VARCHAR (128) NULL,
    [TRIGGER_EVENT_TYPE_CR_FK]             INT           NULL,
    [TRIGGER_EVENT_DN_FK]                  INT           NULL,
    [TRIGGER_OPERATOR_CR_FK]               INT           NULL,
    [TRIGGER_EVENT_VALUE1]                 VARCHAR (64)  NULL,
    [TRIGGER_EVENT_VALUE2]                 VARCHAR (64)  NULL,
    [TRIGGER_IMPORT_PACKAGE_FK]            INT           NULL,
    [TRIGGER_EVENT_SOURCE_CR_FK]           INT           NULL,
    [TRIGGER_EVENT_ACTION_CR_FK]           INT           NULL,
    [APPLY_TO_ORG_CHILDREN_IND]            SMALLINT      NULL,
    [CO_ENTITY_STRUCTURE_FK]               BIGINT        NULL,
    [RECORD_STATUS_CR_FK]                  INT           CONSTRAINT [DF_RULE_REC__361203C5] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]                     DATETIME      CONSTRAINT [DF_RULE_CREAT__370627FE] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]                     DATETIME      CONSTRAINT [DF_RULE_UPDAT__37FA4C37] DEFAULT (getdate()) NULL,
    [UPDATE_USER]                          VARCHAR (64)  NULL,
    [UPDATE_LOG_FK]                        BIGINT        NULL,
    [CREATE_USER]                          VARCHAR (64)  NULL,
    [RULE_PRECEDENCE]                      BIGINT        NULL,
    [RULE_TYPE_CR_FK]                      BIGINT        NULL,
    [CONTROLS_FK]                          BIGINT        NULL,
    [COMMENT]                              VARCHAR (128) NULL,
    [Zone_Segment_Data_Name_FK]            BIGINT        NULL,
    [Zone_Segment_FK]                      BIGINT        NULL,
    [AUTO_APPLY_RESULTS_DV_FK]             BIGINT        NULL,
    [Follow_Zone_Segment_FK]               BIGINT        NULL,
    CONSTRAINT [PK_RULES] PRIMARY KEY CLUSTERED ([RULES_ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_R_RDNF_DN] FOREIGN KEY ([RESULT_DATA_NAME_FK]) REFERENCES [epacube].[DATA_NAME] ([DATA_NAME_ID]),
    CONSTRAINT [FK_R_TEDNF_DN] FOREIGN KEY ([TRIGGER_EVENT_DN_FK]) REFERENCES [epacube].[DATA_NAME] ([DATA_NAME_ID])
);


GO
CREATE NONCLUSTERED INDEX [IDX_R_NAME_UC]
    ON [synchronizer].[RULES]([NAME] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_rls_promo]
    ON [synchronizer].[RULES]([Rules_Type_Cust_Segment_Data_Name_FK] ASC, [Rules_Type_Cust_Segment_FK] ASC, [Cust_Segment_Data_Name_FK] ASC, [Cust_Segment_FK] ASC, [Prod_Segment_Data_Name_FK] ASC, [Prod_Segment_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_rls_rtls_zn]
    ON [synchronizer].[RULES]([RESULT_DATA_NAME_FK] ASC, [Cust_Segment_Data_Name_FK] ASC, [Cust_Segment_FK] ASC, [Prod_Segment_Data_Name_FK] ASC, [Prod_Segment_FK] ASC, [Zone_Segment_FK] ASC, [RECORD_STATUS_CR_FK] ASC, [AUTO_APPLY_RESULTS_DV_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_rls_rtls]
    ON [synchronizer].[RULES]([RESULT_DATA_NAME_FK] ASC, [Cust_Segment_Data_Name_FK] ASC, [Cust_Segment_FK] ASC, [Prod_Segment_Data_Name_FK] ASC, [Prod_Segment_FK] ASC, [RECORD_STATUS_CR_FK] ASC, [AUTO_APPLY_RESULTS_DV_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_t1]
    ON [synchronizer].[RULES]([RESULT_DATA_NAME_FK] ASC, [RECORD_STATUS_CR_FK] ASC, [AUTO_APPLY_RESULTS_DV_FK] ASC, [Cust_Segment_Data_Name_FK] ASC, [Prod_Segment_Data_Name_FK] ASC, [Cust_Segment_FK] ASC, [Prod_Segment_FK] ASC, [Follow_Zone_Segment_FK] ASC, [RULES_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_rdn_pdn_ps_cs_ed_fzs_rs_rid]
    ON [synchronizer].[RULES]([RESULT_DATA_NAME_FK] ASC, [Prod_Segment_Data_Name_FK] ASC, [Prod_Segment_FK] ASC, [Cust_Segment_FK] ASC, [EFFECTIVE_DATE] ASC, [Follow_Zone_Segment_FK] ASC, [RECORD_STATUS_CR_FK] ASC, [RULES_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_rls_zn]
    ON [synchronizer].[RULES]([RESULT_DATA_NAME_FK] ASC, [Prod_Segment_Data_Name_FK] ASC, [Prod_Segment_FK] ASC, [Zone_Segment_FK] ASC, [EFFECTIVE_DATE] ASC, [RECORD_STATUS_CR_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_rls_rtfk_rsfk]
    ON [synchronizer].[RULES]([RECORD_STATUS_CR_FK] ASC, [RULE_TYPE_CR_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

