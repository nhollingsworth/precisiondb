﻿CREATE TABLE [synchronizer].[RULES_SCHEMA_EXCLUDE] (
    [Rules_Scheme_FK]   BIGINT NOT NULL,
    [Data_Name_Exclude] INT    NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_rs_id]
    ON [synchronizer].[RULES_SCHEMA_EXCLUDE]([Rules_Scheme_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

