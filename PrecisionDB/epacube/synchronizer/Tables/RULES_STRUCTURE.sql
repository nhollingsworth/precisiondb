﻿CREATE TABLE [synchronizer].[RULES_STRUCTURE] (
    [RULES_STRUCTURE_ID]  INT           NOT NULL,
    [RULES_HIERARCHY_FK]  INT           NULL,
    [NAME]                VARCHAR (64)  NOT NULL,
    [LABEL]               VARCHAR (64)  NULL,
    [RULE_TYPE_CR_FK]     INT           NULL,
    [MATRIX_ACTION_CR_FK] INT           NULL,
    [PRECEDENCE]          SMALLINT      NULL,
    [COMMENT]             VARCHAR (256) NULL,
    [RECORD_STATUS_CR_FK] INT           CONSTRAINT [DF_RULES_STR_REC__361203C5] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]    DATETIME      CONSTRAINT [DF_RULES_STR_CREAT__370627FE] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]    DATETIME      CONSTRAINT [DF_RULES_STR_UPDAT__37FA4C37] DEFAULT (getdate()) NULL,
    [UPDATE_USER]         VARCHAR (64)  NULL,
    [UPDATE_LOG_FK]       BIGINT        NULL,
    [Promo_Ind]           INT           NULL,
    CONSTRAINT [PK_RULES_STRUCTURE] PRIMARY KEY CLUSTERED ([RULES_STRUCTURE_ID] ASC),
    CONSTRAINT [FK_RST_MACRF_CR] FOREIGN KEY ([MATRIX_ACTION_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID]),
    CONSTRAINT [FK_RST_RH] FOREIGN KEY ([RULES_HIERARCHY_FK]) REFERENCES [synchronizer].[RULES_HIERARCHY] ([RULES_HIERARCHY_ID]),
    CONSTRAINT [FK_RST_RSCRF_CR] FOREIGN KEY ([RECORD_STATUS_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID]),
    CONSTRAINT [FK_RST_RTCRF_CR] FOREIGN KEY ([RULE_TYPE_CR_FK]) REFERENCES [epacube].[CODE_REF] ([CODE_REF_ID])
);


GO
CREATE NONCLUSTERED INDEX [IDX_RS_RTCF_IRSID]
    ON [synchronizer].[RULES_STRUCTURE]([RULES_STRUCTURE_ID] ASC, [RULE_TYPE_CR_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

