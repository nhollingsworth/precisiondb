﻿CREATE TABLE [synchronizer].[RULES_OPTIMIZATION] (
    [RULES_OPTIMIZATION_ID] BIGINT          IDENTITY (2000000000, 1) NOT NULL,
    [NAME]                  VARCHAR (64)    NULL,
    [CUST_DN_FK]            INT             NULL,
    [PROD_DN_FK]            INT             NULL,
    [INCREMENT_DN_FK]       INT             NULL,
    [NUMBER_OF_INCREMENTS]  SMALLINT        NULL,
    [PERCENT_INCREMENT]     NUMERIC (18, 6) NULL,
    [PERCENT_FLOOR]         NUMERIC (18, 6) NULL,
    [PERCENT_CEILING]       NUMERIC (18, 6) NULL,
    [RECORD_STATUS_CR_FK]   INT             CONSTRAINT [DF_RO__RECOR__361203C5] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]      DATETIME        CONSTRAINT [DF_RO_CREAT__370627FE] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]      DATETIME        CONSTRAINT [DF_RO_UPDAT__37FA4C37] DEFAULT (getdate()) NULL,
    [UPDATE_USER]           VARCHAR (64)    NULL,
    [UPDATE_LOG_FK]         BIGINT          NULL,
    CONSTRAINT [PK_RULES_OPTIMIZATION] PRIMARY KEY CLUSTERED ([RULES_OPTIMIZATION_ID] ASC)
);

