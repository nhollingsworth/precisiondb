﻿CREATE TABLE [synchronizer].[RULES_SCHEME_CUSTOMER_GROUP] (
    [rules_scheme]                          VARCHAR (256) NULL,
    [Rules_Scheme_Data_Value_FK]            BIGINT        NOT NULL,
    [rules_type]                            VARCHAR (256) NULL,
    [Rules_Type_Data_Value_FK]              BIGINT        NULL,
    [rules_scheme_prod_segment_description] VARCHAR (256) NOT NULL,
    [Rules_Scheme_Prod_Segment_FK]          BIGINT        NOT NULL,
    [Customer_Segment_Type]                 VARCHAR (64)  NOT NULL,
    [Cust_Segment_FK]                       BIGINT        NOT NULL,
    [Customer_Segment]                      VARCHAR (256) NULL,
    [Product_Segment_Type]                  VARCHAR (64)  NULL,
    [schedule]                              INT           NULL,
    [Precedence_Rules_Scheme]               INT           NOT NULL,
    [Rules]                                 INT           NULL,
    [RULES_SCHEME_CUSTOMER_GROUP_ID]        BIGINT        IDENTITY (1, 1) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [isx_rps1]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_GROUP]([rules_scheme] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [isx_rps2]
    ON [synchronizer].[RULES_SCHEME_CUSTOMER_GROUP]([Rules_Scheme_Data_Value_FK] ASC, [Rules_Type_Data_Value_FK] ASC, [Rules_Scheme_Prod_Segment_FK] ASC, [Cust_Segment_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

