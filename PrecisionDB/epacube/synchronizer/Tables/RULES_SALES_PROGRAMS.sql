﻿CREATE TABLE [synchronizer].[RULES_SALES_PROGRAMS] (
    [Result_data_name_fk]          BIGINT          NULL,
    [Effective_Date]               DATE            NULL,
    [End_Date]                     DATE            NULL,
    [Cust_Segment_Data_Name_FK]    BIGINT          NULL,
    [Cust_Segment_FK]              BIGINT          NULL,
    [Prod_Segment_Data_Name_FK]    BIGINT          NULL,
    [Prod_Segment_FK]              BIGINT          NULL,
    [rules_action_operation1_FK]   BIGINT          NULL,
    [basis_calc_dn_fk]             BIGINT          NULL,
    [Value]                        NUMERIC (18, 4) NULL,
    [Qty_Committed]                NUMERIC (18, 2) NULL,
    [Precedence_Sales_Program]     INT             NOT NULL,
    [Sales_Program_Type_CR_FK]     INT             NULL,
    [Sales_Program_Class_CR_FK]    INT             NULL,
    [Sales_Program_DV_FK]          BIGINT          NULL,
    [SALES_PROGRAM_BUY_HEADER_FK]  BIGINT          NOT NULL,
    [SALES_PROGRAM_BUY_ASSIGN_FK]  BIGINT          NOT NULL,
    [SALES_PROGRAM_BUY_DETAILS_FK] BIGINT          NOT NULL,
    [Rebate_ID]                    BIGINT          NULL,
    [RULES_SALES_PROGRAMS_ID]      BIGINT          IDENTITY (1000, 1) NOT NULL,
    [Pack]                         NUMERIC (18, 2) NULL,
    [data_name_fk]                 BIGINT          NULL,
    [Qty_Remaining]                NUMERIC (18, 2) NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_1]
    ON [synchronizer].[RULES_SALES_PROGRAMS]([Result_data_name_fk] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_2]
    ON [synchronizer].[RULES_SALES_PROGRAMS]([Result_data_name_fk] ASC, [Effective_Date] ASC, [End_Date] ASC, [Cust_Segment_FK] ASC, [Prod_Segment_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_3]
    ON [synchronizer].[RULES_SALES_PROGRAMS]([Result_data_name_fk] ASC, [Effective_Date] ASC, [End_Date] ASC, [Prod_Segment_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_4]
    ON [synchronizer].[RULES_SALES_PROGRAMS]([SALES_PROGRAM_BUY_DETAILS_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_5]
    ON [synchronizer].[RULES_SALES_PROGRAMS]([RULES_SALES_PROGRAMS_ID] ASC, [Prod_Segment_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [IDX_6]
    ON [synchronizer].[RULES_SALES_PROGRAMS]([Result_data_name_fk] ASC, [Prod_Segment_FK] ASC, [Cust_Segment_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

