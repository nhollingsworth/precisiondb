﻿


CREATE VIEW [synchronizer].[V_RULE_RELATED_DATA_NAMES] AS
SELECT DISTINCT A.RULE_TYPE_CR_FK, A.RESULT_DATA_NAME_FK, A.RELATED_DATA_NAME_FK, A.RELATED_PARENT_DATA_NAME_FK
FROM (
---- TRIGGER DATA NAME
SELECT DISTINCT R.RULE_TYPE_CR_FK, R.RESULT_DATA_NAME_FK, R.TRIGGER_EVENT_DN_FK AS RELATED_DATA_NAME_FK
                ,DNX.PARENT_DATA_NAME_FK AS RELATED_PARENT_DATA_NAME_FK 
FROM synchronizer.RULES R WITH (NOLOCK)
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK ) 
INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK) ON ( DNX.DATA_NAME_ID = R.TRIGGER_EVENT_DN_FK )  
WHERE R.RECORD_STATUS_CR_FK = 1
AND   DN.RECORD_STATUS_CR_FK = 1 
AND   DNX.RECORD_STATUS_CR_FK = 1 
AND   R.TRIGGER_EVENT_DN_FK IS NOT NULL  
AND   R.RULE_TYPE_CR_FK IN ( 301, 302, 303, 311 )
UNION ALL
----  BASIS DATA NAMES
SELECT DISTINCT R.RULE_TYPE_CR_FK, R.RESULT_DATA_NAME_FK, RA.BASIS_CALC_DN_FK AS RELATED_DATA_NAME_FK 
                ,DNX.PARENT_DATA_NAME_FK AS RELATED_PARENT_DATA_NAME_FK 
FROM synchronizer.RULES R WITH (NOLOCK)
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
 ON ( RA.RULES_FK = R.RULES_ID )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK )
INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK) ON ( DNX.DATA_NAME_ID = RA.BASIS_CALC_DN_FK )     
WHERE R.RECORD_STATUS_CR_FK = 1
AND   DN.RECORD_STATUS_CR_FK = 1 
AND   DNX.RECORD_STATUS_CR_FK = 1 
AND   RA.BASIS_CALC_DN_FK IS NOT NULL  
AND  R.RULE_TYPE_CR_FK IN ( 301, 302, 303, 311 )
UNION ALL
SELECT DISTINCT R.RULE_TYPE_CR_FK, R.RESULT_DATA_NAME_FK, RA.BASIS1_DN_FK AS RELATED_DATA_NAME_FK 
                ,DNX.PARENT_DATA_NAME_FK AS RELATED_PARENT_DATA_NAME_FK 
FROM synchronizer.RULES R WITH (NOLOCK)
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
 ON ( RA.RULES_FK = R.RULES_ID )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK )  
INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK) ON ( DNX.DATA_NAME_ID = RA.BASIS1_DN_FK  )  
WHERE R.RECORD_STATUS_CR_FK = 1
AND   DN.RECORD_STATUS_CR_FK = 1 
AND   DNX.RECORD_STATUS_CR_FK = 1 
AND   RA.BASIS1_DN_FK IS NOT NULL  
AND  R.RULE_TYPE_CR_FK IN ( 301, 302, 303, 311 )
UNION ALL
SELECT DISTINCT R.RULE_TYPE_CR_FK, R.RESULT_DATA_NAME_FK, RA.BASIS2_DN_FK AS RELATED_DATA_NAME_FK 
                ,DNX.PARENT_DATA_NAME_FK AS RELATED_PARENT_DATA_NAME_FK 
FROM synchronizer.RULES R WITH (NOLOCK)
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
 ON ( RA.RULES_FK = R.RULES_ID )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK )  
INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK) ON ( DNX.DATA_NAME_ID = RA.BASIS2_DN_FK )  
WHERE R.RECORD_STATUS_CR_FK = 1
AND   DN.RECORD_STATUS_CR_FK = 1 
AND   DNX.RECORD_STATUS_CR_FK = 1 
AND   RA.BASIS2_DN_FK IS NOT NULL  
AND  R.RULE_TYPE_CR_FK IN ( 301, 302, 303, 311 ) 
UNION ALL
SELECT DISTINCT R.RULE_TYPE_CR_FK, R.RESULT_DATA_NAME_FK, RA.BASIS3_DN_FK AS RELATED_DATA_NAME_FK 
                ,DNX.PARENT_DATA_NAME_FK AS RELATED_PARENT_DATA_NAME_FK 
FROM synchronizer.RULES R WITH (NOLOCK)
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
 ON ( RA.RULES_FK = R.RULES_ID )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK )  
INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK) ON ( DNX.DATA_NAME_ID = RA.BASIS3_DN_FK )  
WHERE R.RECORD_STATUS_CR_FK = 1
AND   DN.RECORD_STATUS_CR_FK = 1 
AND   DNX.RECORD_STATUS_CR_FK = 1 
AND   RA.BASIS3_DN_FK IS NOT NULL
AND  R.RULE_TYPE_CR_FK IN ( 301, 302, 303, 311 )

UNION ALL

----SEGMENT PRODUCT DATA NAMES
SELECT DISTINCT R.RULE_TYPE_CR_FK, R.RESULT_DATA_NAME_FK, SP.DATA_NAME_FK AS RELATED_DATA_NAME_FK 
              ,DNX.PARENT_DATA_NAME_FK AS RELATED_PARENT_DATA_NAME_FK 
FROM synchronizer.RULES R WITH (NOLOCK)
INNER JOIN Epacube.SEGMENTS_PRODUCT SP on SP.PROD_SEGMENT_FK = R.Prod_Segment_FK
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK ) 
INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK) ON ( DNX.DATA_NAME_ID = SP.DATA_NAME_FK )  
WHERE R.RECORD_STATUS_CR_FK = 1
AND   DN.RECORD_STATUS_CR_FK = 1 
AND   DNX.RECORD_STATUS_CR_FK = 1 
AND   SP.DATA_NAME_FK IS NOT NULL
AND  R.RULE_TYPE_CR_FK IN ( 301, 302, 303, 311 )

----   PRODUCT FILTER DATA NAME
--SELECT DISTINCT R.RULE_TYPE_CR_FK, R.RESULT_DATA_NAME_FK, RF.DATA_NAME_FK AS RELATED_DATA_NAME_FK 
--                ,DNX.PARENT_DATA_NAME_FK AS RELATED_PARENT_DATA_NAME_FK 
--FROM synchronizer.RULES R WITH (NOLOCK)
--INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
-- ON ( RFS.RULES_FK = R.RULES_ID )
--INNER JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK)
-- ON ( RF.RULES_FILTER_ID = RFS.PROD_FILTER_FK )
--INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK ) 
--INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK) ON ( DNX.DATA_NAME_ID = RF.DATA_NAME_FK )  
--WHERE R.RECORD_STATUS_CR_FK = 1
--AND   DN.RECORD_STATUS_CR_FK = 1 
--AND   DNX.RECORD_STATUS_CR_FK = 1 
--AND   RF.DATA_NAME_FK IS NOT NULL
--AND  R.RULE_TYPE_CR_FK IN ( 301, 302, 303, 311 )

UNION ALL

----SEGMENT VENDOR
SELECT DISTINCT R.RULE_TYPE_CR_FK, R.RESULT_DATA_NAME_FK, sv.DATA_NAME_FK AS RELATED_DATA_NAME_FK 
                ,DNX.PARENT_DATA_NAME_FK AS RELATED_PARENT_DATA_NAME_FK 
FROM synchronizer.RULES R WITH (NOLOCK)
INNER JOIN epacube.SEGMENTS_VENDOR SV WITH (NOLOCK)
 ON ( SV.VENDOR_SEGMENT_FK = R.Vendor_Segment_FK)
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK ) 
INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK) ON ( DNX.DATA_NAME_ID = sv.DATA_NAME_FK )   
WHERE R.RECORD_STATUS_CR_FK = 1
AND   DN.RECORD_STATUS_CR_FK = 1 
AND   DNX.RECORD_STATUS_CR_FK = 1 
AND   sv.DATA_NAME_FK IS NOT NULL
AND  R.RULE_TYPE_CR_FK IN ( 301, 302, 303, 311 )

----   SUPPLIER FILTER DATA NAME 
----SELECT DISTINCT R.RULE_TYPE_CR_FK, R.RESULT_DATA_NAME_FK, RFS.SUPL_FILTER_ASSOC_DN_FK AS RELATED_DATA_NAME_FK 
----                ,DNX.PARENT_DATA_NAME_FK AS RELATED_PARENT_DATA_NAME_FK 
----FROM synchronizer.RULES R WITH (NOLOCK)
----INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
---- ON ( RFS.RULES_FK = R.RULES_ID )
----INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK ) 
----INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK) ON ( DNX.DATA_NAME_ID = RFS.SUPL_FILTER_ASSOC_DN_FK )   
----WHERE R.RECORD_STATUS_CR_FK = 1
----AND   DN.RECORD_STATUS_CR_FK = 1 
----AND   DNX.RECORD_STATUS_CR_FK = 1 
----AND   RFS.SUPL_FILTER_ASSOC_DN_FK IS NOT NULL
----AND  R.RULE_TYPE_CR_FK IN ( 301, 302, 303, 311 )


UNION ALL
----   PROD / WHSE ASSOC ( IF RESULT DATA NAME HAS ORG_IND = 1 ) 
SELECT DISTINCT R.RULE_TYPE_CR_FK, R.RESULT_DATA_NAME_FK
                ,159100 AS RELATED_DATA_NAME_FK               ----- PRODUCT WHSE
                ,NULL   AS RELATED_PARENT_DATA_NAME_FK    
FROM synchronizer.RULES R WITH (NOLOCK)
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK ) 
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
WHERE R.RECORD_STATUS_CR_FK = 1
AND   DN.RECORD_STATUS_CR_FK = 1 
AND   DS.ORG_IND = 1
AND  R.RULE_TYPE_CR_FK IN ( 301, 302, 303, 311 )
) A



