﻿




CREATE view [synchronizer].[V_RULE_FILTERS_CONTRACTS]
( 
	ENTITY_STRUCTURE_FK,
	DATA_NAME_FK,
	ENTITY_CLASS_CR_FK,
	ENTITY_DATA_NAME_FK,
	PARENT_ENTITY_STRUCTURE_FK,
	CHILD_ENTITY_STRUCTURE_FK,
	RULES_FILTER_FK,
	RULES_CONTRACT_CUSTOMER_FK,
	ENTITY_FILTER_HIERARCHY )
  AS
------ CONTRACTS - ship to
    SELECT rcc.cust_ENTITY_structure_fk as entity_structure_fk,
           f.data_name_fk,    
		   ES.ENTITY_CLASS_CR_FK,
		   ES.DATA_NAME_FK,
		   ES.PARENT_ENTITY_STRUCTURE_FK,
		   ES.ENTITY_STRUCTURE_ID AS CHILD_ENTITY_STRUCTURE_FK,
           f.rules_filter_id filter_fk,
           RCC.RULES_CONTRACT_CUSTOMER_ID AS RULES_CONTRACT_CUSTOMER_FK,
           ECT.PRECEDENCE AS ENTITY_FILTER_HEIRARCHY              
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = dn.data_name_id )
    INNER JOIN synchronizer.rules_contract rc  WITH (NOLOCK) 
                  ON ( rc.contract_no = f.value1 )	
	INNER JOIN synchronizer.rules_contract_customer rcc WITH (NOLOCK)  
                  ON ( rcc.RULES_CONTRACT_FK = rc.rules_contract_id )
	INNER JOIN epacube.entity_structure es WITH (NOLOCK)
	              ON ( es.entity_structure_id = rcc.cust_entity_structure_fk ) 
    INNER JOIN epacube.ENTITY_CLASS_TREE ECT  WITH (NOLOCK )
			                    ON ( ECT.DATA_NAME_FK = ES.DATA_NAME_FK
			                    AND  ECT.RECORD_STATUS_CR_FK = 1 )	                                      	                                     
	WHERE 1 = 1
	AND   ds.table_name = 'RULES_CONTRACT'    -- Customer Contract
	AND   es.DATA_NAME_FK = 144020  --- ship to only

------ CONTRACTS - bill to

UNION ALL

    SELECT es.ENTITY_STRUCTURE_ID as entity_structure_fk,
           f.data_name_fk,    
		   ES.ENTITY_CLASS_CR_FK,
		   ESP.DATA_NAME_FK,             ---- USE Parent's Entity Data Name --- ie BILL TO CUSTOMER
		   ES.PARENT_ENTITY_STRUCTURE_FK,
		   ES.ENTITY_STRUCTURE_ID AS CHILD_ENTITY_STRUCTURE_FK,
           f.rules_filter_id filter_fk,
           RCC.RULES_CONTRACT_CUSTOMER_ID AS RULES_CONTRACT_CUSTOMER_FK,
           ECT.PRECEDENCE AS ENTITY_FILTER_HEIRARCHY              
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = dn.data_name_id )
    INNER JOIN synchronizer.rules_contract rc  WITH (NOLOCK) 
                  ON ( rc.contract_no = f.value1 )	
	INNER JOIN synchronizer.rules_contract_customer rcc WITH (NOLOCK)  
                  ON ( rcc.RULES_CONTRACT_FK = rc.rules_contract_id )
	INNER JOIN epacube.entity_structure esp WITH (NOLOCK)     --- Match Parent Entity ID for Entity Data Name
	              ON ( esp.entity_structure_id = rcc.cust_entity_structure_fk
	              AND  esp.DATA_NAME_FK = 144010  )   --- bill to only
	INNER JOIN epacube.entity_structure es WITH (NOLOCK)     --- Match Parent Entity to all Children Entities
	              ON ( es.parent_entity_structure_fk = esp.entity_structure_id )
    INNER JOIN epacube.ENTITY_CLASS_TREE ECT  WITH (NOLOCK )
		                    ON ( ECT.DATA_NAME_FK = ESP.DATA_NAME_FK
		                    AND  ECT.RECORD_STATUS_CR_FK = 1 )	                                      	              	              
	WHERE 1 = 1
	AND   ds.table_name = 'RULES_CONTRACT'    -- Customer Contract
















