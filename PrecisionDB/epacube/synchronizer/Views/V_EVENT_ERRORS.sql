﻿CREATE VIEW synchronizer.V_EVENT_ERRORS AS
  SELECT
    ed.BATCH_NO,
    eddn.NAME    DATA_NAME,
    eddn.LABEL   LABEL,
    etcr.CODE    EVENT_TYPE,
    eccr.CODE AS EVENT_CONDITION,
    eacr.CODE    EVENT_ACTION,
    ederr.ERROR_NAME,
    ederr.ERROR_DATA1,
    ederr.ERROR_DATA2,
    ederr.ERROR_DATA3,
    ederr.ERROR_DATA4,
    ed.DATA_NAME_FK,
    ed.NEW_DATA,
    ed.CURRENT_DATA,
    ed.DATA_LEVEL,
    ed.EFFECTIVE_DATE_CUR,
    ed.END_DATE_CUR,
    ed.END_DATE_NEW,
    ed.EVENT_ID,
    ederr.EVENT_ERRORS_ID
  FROM
    synchronizer.EVENT_DATA_ERRORS ederr
    INNER JOIN synchronizer.event_data ed ON (ed.EVENT_ID = ederr.EVENT_FK)
    INNER JOIN epacube.DATA_NAME eddn ON (eddn.DATA_NAME_ID = ed.DATA_NAME_FK)
    INNER JOIN epacube.CODE_REF eccr ON (eccr.CODE_REF_ID = ederr.EVENT_CONDITION_CR_FK)
    INNER JOIN epacube.CODE_REF etcr ON (etcr.CODE_REF_ID = ed.EVENT_TYPE_CR_FK)
    INNER JOIN epacube.CODE_REF eacr ON (eacr.CODE_REF_ID = ed.EVENT_ACTION_CR_FK)