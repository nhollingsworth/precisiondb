﻿






CREATE view [synchronizer].[V_EVENT_DATA_DETAIL] as
select v.event_id EVENT, v.EVENT_ENTITY_CLASS, v.DATA_NAME, v.DN_LABEL,  pi.value PRODUCT, cust.value CUST, org.value ORG, isNULL(vend.value,vend2.value) VENDOR,    zone.value ZONE, 
v.EVENT_TYPE, v.EVENT_CONDITION, v.EVENT_STATUS, v.EVENT_ACTION, ed.NEW_DATA NEW_VALUE, ed.CURRENT_DATA CURRENT_VALUE, v.EVENT_EFFECTIVE_DATE EVENT_DATE, v.IMPORT_JOB_FK JOB
,er.ERROR_NAME, er.ERROR_DATA1, er.ERROR_DATA2, er.ERROR_DATA3, pd.DESCRIPTION, ein.value CUST_NAME, pa.ATTRIBUTE_EVENT_DATA STORE_PACK, pa.ATTRIBUTE_EVENT_DATA SIZE_DESC, ed.*

from [synchronizer].[V_EVENT_DATA]  V
left join epacube.epacube_params ep on ep.name  = v.Event_entity_class and ep.application_scope_fk = 100
left join epacube.PRODUCT_IDENTIFICATION pi on pi.PRODUCT_STRUCTURE_FK = v.product_structure_fk and pi.data_name_fk = ep.value
left join epacube.ENTITY_IDENTIFICATION cust on cust.ENTITY_STRUCTURE_FK = v.CUST_ENTITY_STRUCTURE_FK and cust.DATA_NAME_FK = (select value from epacube.EPACUBE_PARAMS where APPLICATION_SCOPE_FK = (
select CODE_REF_ID from epacube.CODE_REF where CODE_TYPE = 'ENTITY_CLASS' and Code = 'CUSTOMER') 
and Name = 'SEARCH2')
left join epacube.ENTITY_IDENT_NONUNIQUE ein on cust.ENTITY_STRUCTURE_FK = ein.ENTITY_STRUCTURE_FK and ein.DATA_NAME_FK = 144112
left join epacube.ENTITY_IDENTIFICATION org on org.ENTITY_STRUCTURE_FK = v.CUST_ENTITY_STRUCTURE_FK and org.DATA_NAME_FK =(
select value from epacube.EPACUBE_PARAMS where APPLICATION_SCOPE_FK = (
select CODE_REF_ID from epacube.CODE_REF where CODE_TYPE = 'ENTITY_CLASS' and Code = 'WAREHOUSE') 
and Name = 'SEARCH2')
left join epacube.ENTITY_IDENTIFICATION vend on vend.ENTITY_STRUCTURE_FK = v.VENDOR_ENTITY_STRUCTURE_FK and vend.DATA_NAME_FK = (
select value from epacube.EPACUBE_PARAMS where APPLICATION_SCOPE_FK = (
select CODE_REF_ID from epacube.CODE_REF where CODE_TYPE = 'ENTITY_CLASS' and Code = 'VENDOR') 
and Name = 'SEARCH2')
left join epacube.ENTITY_IDENT_NONUNIQUE vend2 on vend2.ENTITY_STRUCTURE_FK = v.VENDOR_ENTITY_STRUCTURE_FK 
left join epacube.ENTITY_IDENTIFICATION zone on zone.ENTITY_STRUCTURE_FK = v.CUST_ENTITY_STRUCTURE_FK and zone.DATA_NAME_FK = (
select value from epacube.EPACUBE_PARAMS where APPLICATION_SCOPE_FK = (
select CODE_REF_ID from epacube.CODE_REF where CODE_TYPE = 'ENTITY_CLASS' and Code = 'ZONE') 
and Name = 'SEARCH2')
Left join epacube.PRODUCT_DESCRIPTION pd on pd.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK and pd.DATA_NAME_FK = 110401
Left join epacube.PRODUCT_ATTRIBUTE pa on pa.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK and pa.DATA_NAME_FK = 500001
Left join epacube.PRODUCT_ATTRIBUTE pa1 on pa1.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK and pa1.DATA_NAME_FK = 500002
inner join synchronizer.EVENT_DATA ED on ed.event_id = v.EVENT_ID
left join [synchronizer].[V_EVENT_ERRORS] ER on ER.EVENT_ID = v.EVENT_ID







