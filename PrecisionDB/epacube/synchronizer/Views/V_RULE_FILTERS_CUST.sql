﻿




CREATE view [synchronizer].[V_RULE_FILTERS_CUST]
( 
	ENTITY_STRUCTURE_FK,
	DATA_NAME_FK,
	ENTITY_CLASS_CR_FK,
	ENTITY_DATA_NAME_FK,
	PARENT_ENTITY_STRUCTURE_FK,
	CHILD_ENTITY_STRUCTURE_FK,
	RULES_FILTER_FK,
	RULES_CONTRACT_CUSTOMER_FK,
	ENTITY_FILTER_HIERARCHY )
  AS
------
------ ENTITY CATEGORY
    SELECT ec.ENTITY_structure_fk, 
           f.data_name_fk,    
		   ES.ENTITY_CLASS_CR_FK,
		   ES.DATA_NAME_FK,
		   ES.PARENT_ENTITY_STRUCTURE_FK,
		   ES.ENTITY_STRUCTURE_ID AS CHILD_ENTITY_STRUCTURE_FK,
           f.rules_filter_id filter_fk,
           NULL AS RULES_CONTRACT_CUSTOMER_FK,
           NULL AS ENTITY_FILTER_HEIRARCHY  -- REVISIT LATER                  
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = dn.data_name_id )
    INNER JOIN epacube.data_value dv WITH (NOLOCK) on ( dv.data_name_fk = f.data_name_fk 
												   and  dv.value = f.value1 )	
	INNER JOIN epacube.entity_category ec WITH (NOLOCK)  
                  ON ( ec.data_value_fk = dv.data_value_id ) 
	INNER JOIN epacube.entity_structure es WITH (NOLOCK)
	              ON ( es.entity_structure_id = ec.entity_structure_fk )                        
	WHERE 1 = 1
	AND   ds.table_name = 'ENTITY_CATEGORY'    -- ENTITY Category
	AND   ds.data_type_cr_fk = 134 		
	

-----      PARENT ENTITY STRUCTURE'S ENTITY CATEGORY
UNION ALL

   SELECT  ES.ENTITY_STRUCTURE_ID AS ENTITY_STRUCTURE_FK,  ---emt.ENTITY_structure_fk, 
           f.data_name_fk,    
		   ES.ENTITY_CLASS_CR_FK,
		   ES.DATA_NAME_FK,
		   ES.PARENT_ENTITY_STRUCTURE_FK,
		   ES.ENTITY_STRUCTURE_ID AS CHILD_ENTITY_STRUCTURE_FK,
           f.rules_filter_id filter_fk,
           NULL AS RULES_CONTRACT_CUSTOMER_FK,
           NULL AS ENTITY_FILTER_HEIRARCHY  -- REVISIT LATER       
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = dn.data_name_id )
    INNER JOIN epacube.data_value dv WITH (NOLOCK) on ( dv.data_name_fk = f.data_name_fk 
												   and  dv.value = f.value1 )	
	INNER JOIN epacube.entity_category ec WITH (NOLOCK)  
                  ON (  ec.data_value_fk = dv.data_value_id )       
	INNER JOIN epacube.entity_structure es WITH (NOLOCK)
	              ON ( es.PARENT_ENTITY_STRUCTURE_FK = ec.entity_structure_fk )												   
	WHERE 1 = 1
	AND   ds.table_name = 'ENTITY_CATEGORY'    -- ENTITY Category
	AND   ds.data_type_cr_fk = 134 

	
------
------ ENTITY_MULT_TYPE
	UNION ALL
    SELECT emt.ENTITY_structure_fk, 
           f.data_name_fk,    
		   ES.ENTITY_CLASS_CR_FK,
		   ES.DATA_NAME_FK,
		   ES.PARENT_ENTITY_STRUCTURE_FK,
		   ES.ENTITY_STRUCTURE_ID AS CHILD_ENTITY_STRUCTURE_FK,
           f.rules_filter_id filter_fk,
           NULL AS RULES_CONTRACT_CUSTOMER_FK,
           NULL AS ENTITY_FILTER_HEIRARCHY  -- REVISIT LATER       
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = dn.data_name_id )
    INNER JOIN epacube.data_value dv WITH (NOLOCK) on ( dv.data_name_fk = f.data_name_fk 
												   and  dv.value = f.value1 )	
	INNER JOIN epacube.ENTITY_mult_type emt WITH (NOLOCK)  
                  ON (  emt.data_value_fk = dv.data_value_id )       
	INNER JOIN epacube.entity_structure es WITH (NOLOCK)
	              ON ( es.entity_structure_id = emt.entity_structure_fk )												   
	WHERE 1 = 1
	AND   ds.table_name = 'ENTITY_MULT_TYPE'    -- ENTITY MULT TYPE
	AND   ds.data_type_cr_fk = 134 

-----      PARENT ENTITY STRUCTURE'S EMT
UNION ALL

   SELECT  ES.ENTITY_STRUCTURE_ID AS ENTITY_STRUCTURE_FK,  ---emt.ENTITY_structure_fk, 
           f.data_name_fk,    
		   ES.ENTITY_CLASS_CR_FK,
		   ES.DATA_NAME_FK,
		   ES.PARENT_ENTITY_STRUCTURE_FK,
		   ES.ENTITY_STRUCTURE_ID AS CHILD_ENTITY_STRUCTURE_FK,
           f.rules_filter_id filter_fk,
           NULL AS RULES_CONTRACT_CUSTOMER_FK,
           NULL AS ENTITY_FILTER_HEIRARCHY  -- REVISIT LATER       
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = dn.data_name_id )
    INNER JOIN epacube.data_value dv WITH (NOLOCK) on ( dv.data_name_fk = f.data_name_fk 
												   and  dv.value = f.value1 )	
	INNER JOIN epacube.ENTITY_mult_type emt WITH (NOLOCK)  
                  ON (  emt.data_value_fk = dv.data_value_id )       
	INNER JOIN epacube.entity_structure es WITH (NOLOCK)
	              ON ( es.PARENT_ENTITY_STRUCTURE_FK = emt.entity_structure_fk )												   
	WHERE 1 = 1
	AND   ds.table_name = 'ENTITY_MULT_TYPE'    -- ENTITY MULT TYPE
	AND   ds.data_type_cr_fk = 134 














