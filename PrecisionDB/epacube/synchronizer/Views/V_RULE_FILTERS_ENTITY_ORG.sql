﻿





CREATE view [synchronizer].[V_RULE_FILTERS_ENTITY_ORG]
( 
	ENTITY_STRUCTURE_FK,
	DATA_NAME_FK,
	ENTITY_CLASS_CR_FK,
	ENTITY_DATA_NAME_FK,
	PARENT_ENTITY_STRUCTURE_FK,
	CHILD_ENTITY_STRUCTURE_FK,
	RULES_FILTER_FK,
	RULES_CONTRACT_CUSTOMER_FK,
	ENTITY_FILTER_HIERARCHY )
  AS
------		
    SELECT ei.ENTITY_structure_fk, 
           f.data_name_fk,    
		   ES.ENTITY_CLASS_CR_FK,
		   ES.DATA_NAME_FK,
		   ES.PARENT_ENTITY_STRUCTURE_FK,
		   ES.ENTITY_STRUCTURE_ID AS CHILD_ENTITY_STRUCTURE_FK,
           f.rules_filter_id filter_fk,
           NULL AS RULES_CONTRACT_CUSTOMER_FK,
           NULL AS ENTITY_FILTER_HEIRARCHY  -- REVISIT LATER
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = dn.data_name_id )
	INNER JOIN epacube.entity_identification ei WITH (NOLOCK)
                  ON ( ei.entity_data_name_fk = f.entity_data_name_fk
                  AND  ei.data_name_fk = f.data_name_fk
                  AND  ei.value = f.value1 )
	INNER JOIN epacube.entity_structure es WITH (NOLOCK)
	              ON ( es.entity_structure_id = ei.entity_structure_fk )
	WHERE 1 = 1	
	AND   ds.table_name = 'ENTITY_IDENTIFICATION'   -- ENTITY Identifier 	
	AND   ds.entity_class_cr_fk = 10101
------
------ 	ENTITY IDENTIFICATION WITH PARENT HEIRARCHY
------
------				revisit to inlcude later function epacube.Get_Entity_CrossRef
	UNION ALL
    SELECT 
       ES.ENTITY_structure_id ENTITY_structure_fk,   --- MATCHING DATA ROW ON CHILD that belongs to PARENT
       f.data_name_fk,       
       ES.ENTITY_CLASS_CR_FK,
       ESP.DATA_NAME_FK,             ---- USE Parent's Entity Data Name --- ie BILL TO CUSTOMER
       ES.PARENT_ENTITY_STRUCTURE_FK,
       ES.ENTITY_STRUCTURE_ID AS CHILD_ENTITY_STRUCTURE_FK,
       f.rules_filter_id filter_fk,                   --- MATCHING FILTER AND RULE ON PARENT Identification
       NULL AS RULES_CONTRACT_CUSTOMER_FK,
       NULL AS ENTITY_FILTER_HEIRARCHY  -- REVISIT LATER
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = dn.data_name_id )
	INNER JOIN epacube.entity_identification ei   WITH (NOLOCK)        -- FIND PARENT BY IDENTIFICATION
                  ON ( ei.entity_data_name_fk = f.entity_data_name_fk
                  AND  ei.data_name_fk = f.data_name_fk
                  AND  ei.value = f.value1 )
	INNER JOIN epacube.entity_structure esp WITH (NOLOCK)     --- Match Parent Entity ID for Entity Data Name
	              ON ( esp.entity_structure_id = ei.entity_structure_fk )
	INNER JOIN epacube.entity_structure es WITH (NOLOCK)     --- Match Parent Entity to all Children Entities
	              ON ( es.parent_entity_structure_fk = ei.entity_structure_fk )
	WHERE 1 = 1
	AND   ds.table_name = 'ENTITY_IDENTIFICATION'    -- ENTITY Parent Identifiers	
	AND   ds.entity_class_cr_fk = 10101
------




