﻿create VIEW synchronizer.v_rules
AS
SELECT r.NAME,
       psegdn.NAME PSEG_DN,
       csegdn.NAME CSEG_DN,
       rrdn1.NAME RESULT_DN1,
       rops.CODE OP,
       ropfilt.CODE OPND_FILT,
       revt_act.CODE EVT_ACT,
       revt_cond.CODE EVT_COND,
       r.EFFECTIVE_DATE EFF_DT,
       r.END_DATE END_DT,
       r.Promo_Ind promo,
       r.HOST_RULE_XREF,
       r.REFERENCE,
       r.CONTRACT_NO,
  r.RULES_ID
  FROM connector.synchronizer.rules r
 INNER JOIN epacube.DATA_NAME rrdn1 on (rrdn1.DATA_NAME_ID=r.RESULT_DATA_NAME_FK)
 LEFT JOIN epacube.CODE_REF rops on (rops.CODE_REF_ID=r.OPERATIONS_CR_FK)
 LEFT JOIN epacube.CODE_REF ropfilt on (ropfilt.CODE_REF_ID=r.OPERAND_FILTER_CR_FK)
 LEFT JOIN epacube.CODE_REF revt_act on (revt_act.CODE_REF_ID=r.EVENT_ACTION_CR_FK)
 LEFT JOIN epacube.CODE_REF revt_cond on (revt_cond.CODE_REF_ID=r.EVENT_CONDITION_CR_FK)
 LEFT JOIN epacube.DATA_NAME psegdn on (psegdn.DATA_NAME_ID=r.Prod_Segment_Data_Name_FK)
 LEFT JOIN epacube.DATA_NAME csegdn on (csegdn.DATA_NAME_ID=r.Cust_Segment_Data_Name_FK)