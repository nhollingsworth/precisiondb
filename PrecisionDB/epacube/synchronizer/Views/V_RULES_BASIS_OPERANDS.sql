﻿



CREATE VIEW [synchronizer].[V_RULES_BASIS_OPERANDS]
AS
SELECT      
       A.RULES_ACTION_FK
      ,A.BASIS_POSITION      
      ,A.OPERAND_FILTER_DN_FK
      ,A.OPERAND_FILTER_OPERATOR_CR_FK
      ,A.BASIS_OPERAND_POSITION
      ,A.BASIS_OPERAND    
      ,A.BASIS_OPERAND_FILTER_VALUE
      ,A.BASIS_EFFECTIVE_DATE
------   
FROM (         
SELECT 
       [RULES_ACTION_FK]
      ,[BASIS_POSITION]
      ,[OPERAND_FILTER_DN_FK]
      ,[OPERAND_FILTER_OPERATOR_CR_FK]
      ,1               AS BASIS_OPERAND_POSITION
      ,[OPERAND1]      AS BASIS_OPERAND
      ,[FILTER_VALUE1] AS BASIS_OPERAND_FILTER_VALUE
      ,( SELECT R.EFFECTIVE_DATE FROM synchronizer.RULES R WITH (NOLOCK) 
         INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON RA.RULES_FK = R.RULES_ID
         WHERE RA.RULES_ACTION_ID = RULES_ACTION_FK ) AS BASIS_EFFECTIVE_DATE
FROM [synchronizer].[RULES_ACTION_OPERANDS]  WITH (NOLOCK)
WHERE OPERAND_FILTER_OPERATOR_CR_FK IN ( 9000, 9019 )
AND   OPERAND1 IS NOT NULL 
OR    FILTER_VALUE1 IS NOT NULL 
UNION ALL 
SELECT 
       [RULES_ACTION_FK]
      ,[BASIS_POSITION]
      ,[OPERAND_FILTER_DN_FK]
      ,[OPERAND_FILTER_OPERATOR_CR_FK]
      ,2               AS BASIS_OPERAND_POSITION
      ,CASE OPERAND_FILTER_OPERATOR_CR_FK
       WHEN 9000 THEN OPERAND2
       WHEN 9019 THEN COALESCE( OPERAND2, OPERAND1 ) 
       ELSE OPERAND2
       END  AS BASIS_OPERAND
      ,[FILTER_VALUE2] AS BASIS_OPERAND_FILTER_VALUE
      ,( SELECT R.EFFECTIVE_DATE FROM synchronizer.RULES R WITH (NOLOCK) 
         INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON RA.RULES_FK = R.RULES_ID
         WHERE RA.RULES_ACTION_ID = RULES_ACTION_FK ) AS BASIS_EFFECTIVE_DATE
FROM [synchronizer].[RULES_ACTION_OPERANDS]  WITH (NOLOCK)
WHERE OPERAND_FILTER_OPERATOR_CR_FK IN ( 9000, 9019 )
AND   OPERAND2 IS NOT NULL 
OR    FILTER_VALUE2 IS NOT NULL 
UNION ALL 
SELECT 
       [RULES_ACTION_FK]
      ,[BASIS_POSITION]
      ,[OPERAND_FILTER_DN_FK]
      ,[OPERAND_FILTER_OPERATOR_CR_FK]
      ,3               AS BASIS_OPERAND_POSITION
      ,CASE OPERAND_FILTER_OPERATOR_CR_FK
       WHEN 9000 THEN OPERAND3
       WHEN 9019 THEN COALESCE( OPERAND3, OPERAND2, OPERAND1 ) 
       ELSE OPERAND3
       END  AS BASIS_OPERAND
      ,[FILTER_VALUE3] AS BASIS_OPERAND_FILTER_VALUE
      ,( SELECT R.EFFECTIVE_DATE FROM synchronizer.RULES R WITH (NOLOCK) 
         INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON RA.RULES_FK = R.RULES_ID
         WHERE RA.RULES_ACTION_ID = RULES_ACTION_FK ) AS BASIS_EFFECTIVE_DATE
FROM [synchronizer].[RULES_ACTION_OPERANDS]  WITH (NOLOCK)
WHERE OPERAND_FILTER_OPERATOR_CR_FK IN ( 9000, 9019 )
AND   OPERAND3 IS NOT NULL 
OR    FILTER_VALUE3 IS NOT NULL 
UNION ALL 
SELECT 
       [RULES_ACTION_FK]
      ,[BASIS_POSITION]
      ,[OPERAND_FILTER_DN_FK]
      ,[OPERAND_FILTER_OPERATOR_CR_FK]
      ,4               AS BASIS_OPERAND_POSITION
      ,CASE OPERAND_FILTER_OPERATOR_CR_FK
       WHEN 9000 THEN OPERAND4
       WHEN 9019 THEN COALESCE( OPERAND4, OPERAND3, OPERAND2, OPERAND1 ) 
       ELSE OPERAND4
       END  AS BASIS_OPERAND
      ,[FILTER_VALUE4] AS BASIS_OPERAND_FILTER_VALUE
      ,( SELECT R.EFFECTIVE_DATE FROM synchronizer.RULES R WITH (NOLOCK) 
         INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON RA.RULES_FK = R.RULES_ID
         WHERE RA.RULES_ACTION_ID = RULES_ACTION_FK ) AS BASIS_EFFECTIVE_DATE
FROM [synchronizer].[RULES_ACTION_OPERANDS]  WITH (NOLOCK)
WHERE OPERAND_FILTER_OPERATOR_CR_FK IN ( 9000, 9019 )
AND   OPERAND4 IS NOT NULL 
OR    FILTER_VALUE4 IS NOT NULL 
UNION ALL 
SELECT 
       [RULES_ACTION_FK]
      ,[BASIS_POSITION]
      ,[OPERAND_FILTER_DN_FK]
      ,[OPERAND_FILTER_OPERATOR_CR_FK]
      ,5               AS BASIS_OPERAND_POSITION      
      ,CASE OPERAND_FILTER_OPERATOR_CR_FK
       WHEN 9000 THEN OPERAND5
       WHEN 9019 THEN COALESCE( OPERAND5, OPERAND4, OPERAND3, OPERAND2, OPERAND1 ) 
       ELSE OPERAND5
       END  AS BASIS_OPERAND
      ,[FILTER_VALUE5] AS BASIS_OPERAND_FILTER_VALUE
      ,( SELECT R.EFFECTIVE_DATE FROM synchronizer.RULES R WITH (NOLOCK) 
         INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON RA.RULES_FK = R.RULES_ID
         WHERE RA.RULES_ACTION_ID = RULES_ACTION_FK ) AS BASIS_EFFECTIVE_DATE
FROM [synchronizer].[RULES_ACTION_OPERANDS]  WITH (NOLOCK)
WHERE OPERAND_FILTER_OPERATOR_CR_FK IN ( 9000, 9019 )
AND   OPERAND5 IS NOT NULL 
OR    FILTER_VALUE5 IS NOT NULL 
UNION ALL 
SELECT 
       [RULES_ACTION_FK]
      ,[BASIS_POSITION]
      ,[OPERAND_FILTER_DN_FK]
      ,[OPERAND_FILTER_OPERATOR_CR_FK]
      ,6               AS BASIS_OPERAND_POSITION      
      ,CASE OPERAND_FILTER_OPERATOR_CR_FK
       WHEN 9000 THEN OPERAND6
       WHEN 9019 THEN COALESCE( OPERAND6, OPERAND5, OPERAND4, OPERAND3, OPERAND2, 
			                          OPERAND1 )
       ELSE OPERAND6
       END  AS BASIS_OPERAND
      ,[FILTER_VALUE6] AS BASIS_OPERAND_FILTER_VALUE
      ,( SELECT R.EFFECTIVE_DATE FROM synchronizer.RULES R WITH (NOLOCK) 
         INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON RA.RULES_FK = R.RULES_ID
         WHERE RA.RULES_ACTION_ID = RULES_ACTION_FK ) AS BASIS_EFFECTIVE_DATE
FROM [synchronizer].[RULES_ACTION_OPERANDS]  WITH (NOLOCK)
WHERE OPERAND_FILTER_OPERATOR_CR_FK IN ( 9000, 9019 )
AND   OPERAND6 IS NOT NULL 
OR    FILTER_VALUE6 IS NOT NULL 
UNION ALL 
SELECT 
       [RULES_ACTION_FK]
      ,[BASIS_POSITION]
      ,[OPERAND_FILTER_DN_FK]
      ,[OPERAND_FILTER_OPERATOR_CR_FK]
      ,7               AS BASIS_OPERAND_POSITION
      ,CASE OPERAND_FILTER_OPERATOR_CR_FK
       WHEN 9000 THEN OPERAND7
       WHEN 9019 THEN COALESCE( OPERAND7, OPERAND6, OPERAND5, OPERAND4, OPERAND3, 
			                          OPERAND2, OPERAND1 )
       ELSE OPERAND7
       END  AS BASIS_OPERAND
      ,[FILTER_VALUE7] AS BASIS_OPERAND_FILTER_VALUE
      ,( SELECT R.EFFECTIVE_DATE FROM synchronizer.RULES R WITH (NOLOCK) 
         INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON RA.RULES_FK = R.RULES_ID
         WHERE RA.RULES_ACTION_ID = RULES_ACTION_FK ) AS BASIS_EFFECTIVE_DATE
FROM [synchronizer].[RULES_ACTION_OPERANDS]  WITH (NOLOCK)
WHERE OPERAND_FILTER_OPERATOR_CR_FK IN ( 9000, 9019 )
AND   OPERAND7 IS NOT NULL 
OR    FILTER_VALUE7 IS NOT NULL 
UNION ALL 
SELECT 
       [RULES_ACTION_FK]
      ,[BASIS_POSITION]
      ,[OPERAND_FILTER_DN_FK]
      ,[OPERAND_FILTER_OPERATOR_CR_FK]
      ,8               AS BASIS_OPERAND_POSITION
      ,CASE OPERAND_FILTER_OPERATOR_CR_FK
       WHEN 9000 THEN OPERAND8
       WHEN 9019 THEN COALESCE( OPERAND8, OPERAND7, OPERAND6, OPERAND5, OPERAND4, 
			                          OPERAND3, OPERAND2, OPERAND1 )
       ELSE OPERAND8
       END  AS BASIS_OPERAND
      ,[FILTER_VALUE8] AS BASIS_OPERAND_FILTER_VALUE
      ,( SELECT R.EFFECTIVE_DATE FROM synchronizer.RULES R WITH (NOLOCK) 
         INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON RA.RULES_FK = R.RULES_ID
         WHERE RA.RULES_ACTION_ID = RULES_ACTION_FK ) AS BASIS_EFFECTIVE_DATE
FROM [synchronizer].[RULES_ACTION_OPERANDS]  WITH (NOLOCK)
WHERE OPERAND_FILTER_OPERATOR_CR_FK IN ( 9000, 9019 )
AND   OPERAND8 IS NOT NULL 
OR    FILTER_VALUE8 IS NOT NULL 
UNION ALL 
SELECT 
       [RULES_ACTION_FK]
      ,[BASIS_POSITION]
      ,[OPERAND_FILTER_DN_FK]
      ,[OPERAND_FILTER_OPERATOR_CR_FK]
      ,9               AS BASIS_OPERAND_POSITION
      ,CASE OPERAND_FILTER_OPERATOR_CR_FK
       WHEN 9000 THEN OPERAND9
       WHEN 9019 THEN COALESCE( OPERAND9, OPERAND8, OPERAND7, OPERAND6, OPERAND5, 
			                          OPERAND4, OPERAND3, OPERAND2, OPERAND1 ) 
       ELSE OPERAND9
       END  AS BASIS_OPERAND
      ,[FILTER_VALUE9] AS BASIS_OPERAND_FILTER_VALUE
      ,( SELECT R.EFFECTIVE_DATE FROM synchronizer.RULES R WITH (NOLOCK) 
         INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON RA.RULES_FK = R.RULES_ID
         WHERE RA.RULES_ACTION_ID = RULES_ACTION_FK ) AS BASIS_EFFECTIVE_DATE
FROM [synchronizer].[RULES_ACTION_OPERANDS]  WITH (NOLOCK)
WHERE OPERAND_FILTER_OPERATOR_CR_FK IN ( 9000, 9019 )
AND   OPERAND9 IS NOT NULL 
OR    FILTER_VALUE9 IS NOT NULL 
UNION ALL 
SELECT 
       [RULES_ACTION_FK]
      ,[BASIS_POSITION]
      ,[OPERAND_FILTER_DN_FK]
      ,[OPERAND_FILTER_OPERATOR_CR_FK]
      ,10               AS BASIS_OPERAND_POSITION
      ,CASE OPERAND_FILTER_OPERATOR_CR_FK
       WHEN 9000 THEN OPERAND10
       WHEN 9019 THEN COALESCE( OPERAND10, OPERAND9, OPERAND8, OPERAND7, OPERAND6, 
									  OPERAND5, OPERAND4, OPERAND3, OPERAND2, OPERAND1 ) 
       ELSE OPERAND10
       END  AS BASIS_OPERAND
      ,[FILTER_VALUE10] AS BASIS_OPERAND_FILTER_VALUE
      ,( SELECT R.EFFECTIVE_DATE FROM synchronizer.RULES R WITH (NOLOCK) 
         INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON RA.RULES_FK = R.RULES_ID
         WHERE RA.RULES_ACTION_ID = RULES_ACTION_FK ) AS BASIS_EFFECTIVE_DATE
FROM [synchronizer].[RULES_ACTION_OPERANDS]  WITH (NOLOCK)
WHERE OPERAND_FILTER_OPERATOR_CR_FK IN ( 9000, 9019 )
AND   OPERAND10 IS NOT NULL 
OR    FILTER_VALUE10 IS NOT NULL 
  ) A



