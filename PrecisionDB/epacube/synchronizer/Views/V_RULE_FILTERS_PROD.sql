﻿


---   Entity Structure and Org Entity Structure in result set for outside filtering

---   In Future Releases 
---   add other operators ( examples between; like.. etc. )

CREATE view [synchronizer].[V_RULE_FILTERS_PROD]
( 
PRODUCT_STRUCTURE_FK,
DATA_NAME_FK,
ORG_IND,
CORP_IND,
PROD_FILTER_HIERARCHY,
ORG_ENTITY_STRUCTURE_FK,
ENTITY_CLASS_CR_FK,
ENTITY_STRUCTURE_FK,
RULES_FILTER_FK )
  AS
------  PRODUCT_CATEGORY
    SELECT pc.product_structure_fk, 
           f.data_name_fk,
           ds.org_ind,
           ds.corp_ind,
           9 AS PROD_FILTER_HIERARCHY,
           pc.org_entity_structure_fk, 
           NULL AS entity_class_cr_fk,
           pc.entity_structure_fk,  
           f.rules_filter_id filter_fk
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = dn.data_name_id )
    INNER JOIN epacube.data_value dv WITH (NOLOCK) on ( dv.data_name_fk = f.data_name_fk 
												   and  dv.value = f.value1 )	
	INNER JOIN epacube.product_category pc WITH (NOLOCK)  
                  ON ( pc.data_value_fk = dv.data_value_id )       
	WHERE 1 = 1
	AND   ds.table_name = 'PRODUCT_CATEGORY'    -- Product Category
	AND   ds.data_type_cr_fk = 134 	
------ ENTITY Product Category
	UNION ALL
    SELECT pc.product_structure_fk, 
           f.data_name_fk,    
           ds.ORG_IND,
           ds.CORP_IND,     
           9 AS PROD_FILTER_HIERARCHY,
           pc.org_entity_structure_fk, 
           ds.entity_structure_ec_cr_fk AS entity_class_cr_fk,
           pc.entity_structure_fk,  
           f.rules_filter_id filter_fk
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = dn.data_name_id )
    INNER JOIN epacube.entity_data_value edv WITH (NOLOCK) on ( edv.data_name_fk = f.data_name_fk 
												           and  edv.value = f.value1 )
		--- entity_structure_fk will be filtered out of result later
	INNER JOIN epacube.product_category pc WITH (NOLOCK)  
                  ON ( pc.entity_data_value_fk = edv.entity_data_value_id )       
	WHERE 1 = 1
	AND   ds.table_name = 'PRODUCT_CATEGORY'    -- Product Category
	AND   ds.data_type_cr_fk = 135 	

------  PRODUCT_MULT_TYPE
    UNION ALL
    SELECT pmt.product_structure_fk, 
           f.data_name_fk,
           ds.org_ind,
           ds.corp_ind,
           9 AS PROD_FILTER_HIERARCHY,
           pmt.org_entity_structure_fk, 
           NULL AS entity_class_cr_fk,
           pmt.entity_structure_fk,  
           f.rules_filter_id filter_fk
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = dn.data_name_id )
    INNER JOIN epacube.data_value dv WITH (NOLOCK) on ( dv.data_name_fk = f.data_name_fk 
												   and  dv.value = f.value1 )	
	INNER JOIN epacube.PRODUCT_MULT_TYPE pmt WITH (NOLOCK)  
                  ON ( pmt.data_value_fk = dv.data_value_id )       
	WHERE 1 = 1
	AND   ds.table_name = 'PRODUCT_MULT_TYPE'    -- Product Mult Type
	AND   ds.data_type_cr_fk = 134 	




------ PRODUCT_IDENTIFICATION
	UNION ALL
    SELECT pi.product_structure_fk, 
           f.data_name_fk,    
		   ds.org_ind,
           ds.corp_ind,
           9 AS PROD_FILTER_HIERARCHY,
           PI.org_entity_structure_fk,
           ds.entity_structure_ec_cr_fk, 
           pi.entity_structure_fk,  f.rules_filter_id filter_fk
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = dn.data_name_id )
	INNER JOIN epacube.product_identification pi 
                  ON ( pi.data_name_fk = f.data_name_fk
                  AND  PI.value = f.value1 )
	WHERE 1 = 1
	AND   ds.table_name = 'PRODUCT_IDENTIFICATION'   -- Product Identifier ( Global or Entity )
------
------------  ECLIPSE HARDCODED 311900 SELL GROUPS TO USE DN.DISPLAY_SEQ
------
	UNION ALL
    SELECT pc.product_structure_fk, 
           f.data_name_fk,
           ds.org_ind,
           ds.corp_ind,
           DN.DISPLAY_SEQ AS PROD_FILTER_HIERARCHY,
           pc.org_entity_structure_fk, 
           NULL AS entity_class_cr_fk,
           pc.entity_structure_fk,  
           f.rules_filter_id filter_fk
	FROM epacube.data_name dn WITH (NOLOCK) 
	INNER JOIN epacube.data_set ds WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
       and  f.data_name_fk = 311900 )    -----  CHANGE LATER DN.PARENT_DATA_NAME_FK --- 311900 
    LEFT JOIN epacube.data_value dv WITH (NOLOCK) on ( dv.data_name_fk = DN.data_name_ID 
												   and  dv.value = f.value1 )	
	LEFT JOIN epacube.product_category pc WITH (NOLOCK)  
                  ON ( pc.data_value_fk = dv.data_value_id )       
	WHERE 1 = 1
	AND   DN.DATA_NAME_ID IN ( 	311901, 311904,311905, 311906, 311907, 311908, 311909, 311910, 311911 ) 
	
	
------
------  IMPORTANT >>>>>>
------
------  ASSOCIATIONS MOVED UP ON LEVEL ON THE RULES_FILTER_SET
------
------	NOTE:   ALL ASSOCIATIONS ARE WITH THE PRODUCT AND COORESPONDING ENTITY
------				IE.. ORG_FILTER_ASSOC_DN WOULD BE THE PRODUCT WAREHOUSE
------					 SUPL_FILTER_ASSOC_DN WOULD BE THE ARP VENDOR
------					 ETC..
------	
------  CODE BELOW IS NO LONGER USED... WILL DELETE IT LATER
------
------ 	Product Association  -- ENTITY ( WAREHOUSE )
----------	UNION ALL
----------    SELECT pas.product_structure_fk, 
----------       ds.ORG_IND,
----------       ds.CORP_IND,       
----------       pas.org_entity_structure_fk, 
----------       ds.ENTITY_STRUCTURE_EC_CR_FK AS entity_class_cr_fk, 
----------       pas.entity_structure_fk,
----------       f.rules_filter_id filter_fk
----------	FROM epacube.data_set ds WITH (NOLOCK)
----------	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
----------	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
----------       on ( f.record_status_cr_fk = 1
----------       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
----------       and  f.data_name_fk = dn.data_name_id )
----------	INNER JOIN epacube.entity_identification ei 
----------                  ON ( ei.entity_data_name_fk = f.entity_data_name_fk
----------                  AND  ei.data_name_fk = f.data_name_fk
----------                  AND  ei.value = f.value1 )
----------	INNER JOIN epacube.product_association pas WITH (NOLOCK) 
----------		ON ( pas.data_name_fk = f.assoc_data_name_fk 
----------		AND  pas.org_entity_structure_fk = ei.entity_structure_fk )
----------	WHERE 1 = 1
----------	AND   ds.table_name = 'PRODUCT_ASSOCIATION'    -- Product Association	
----------	AND   ds.ENTITY_STRUCTURE_EC_CR_FK = 10101     -- Warehouse
----------------
---------------- 	Product Association  -- ENTITY ( NON WAREHOUSE )  -- org will be filtered later
----------	UNION ALL
----------    SELECT pas.product_structure_fk, 
----------       ds.ORG_IND,
----------       ds.CORP_IND,       
----------       pas.org_entity_structure_fk, 
----------       ds.ENTITY_STRUCTURE_EC_CR_FK AS entity_class_cr_fk, 
----------       pas.entity_structure_fk,
----------       f.rules_filter_id filter_fk
----------	FROM epacube.data_set ds WITH (NOLOCK)
----------	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
----------	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
----------       on ( f.record_status_cr_fk = 1
----------       and  f.entity_class_cr_fk = ds.entity_class_cr_fk
----------       and  f.data_name_fk = dn.data_name_id )
----------	INNER JOIN epacube.entity_identification ei
----------                  ON ( ei.entity_data_name_fk = f.entity_data_name_fk
----------                  AND  ei.data_name_fk = f.data_name_fk
----------                  AND  ei.value = f.value1 )
----------	INNER JOIN epacube.product_association pas WITH (NOLOCK) 
----------		ON ( pas.data_name_fk = f.assoc_data_name_fk 
----------		AND  pas.entity_structure_fk = ei.entity_structure_fk )
----------	WHERE 1 = 1
----------	AND   ds.table_name = 'PRODUCT_ASSOCIATION'    -- Product Association	
----------	AND   ds.ENTITY_STRUCTURE_EC_CR_FK <> 10101     -- Warehouse	
---------------------------	
------ add other tables later


