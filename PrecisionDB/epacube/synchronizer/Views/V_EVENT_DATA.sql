﻿


CREATE view [synchronizer].[V_EVENT_DATA] as
select
   cret.CODE EVENT_TYPE
  ,crec.CODE EVENT_CONDITION
  ,crstat.CODE EVENT_STATUS
  ,cretc.CODE EVENT_ENTITY_CLASS
  ,cract.CODE EVENT_ACTION
  ,dn.NAME DATA_NAME
  ,dn.LABEL DN_LABEL
  ,dset.CODE DATA_TYPE
  ,ed.EVENT_ID
  ,ed.PRODUCT_STRUCTURE_FK
  ,ed.ORG_ENTITY_STRUCTURE_FK
  ,ed.ENTITY_STRUCTURE_FK
  ,ed.ZONE_ENTITY_STRUCTURE_FK
  ,ed.CUST_ENTITY_STRUCTURE_FK
  ,ed.VENDOR_ENTITY_STRUCTURE_FK
  ,ed.EVENT_EFFECTIVE_DATE
  ,ed.PRECEDENCE
  ,ed.CALC_JOB_FK
  ,ed.IMPORT_JOB_FK
  from synchronizer.EVENT_DATA ed
       inner join epacube.CODE_REF cret on (cret.CODE_REF_ID=ed.EVENT_TYPE_CR_FK)
       inner join epacube.CODE_REF crec on (crec.CODE_REF_ID=ed.EVENT_CONDITION_CR_FK)
	   inner join epacube.CODE_REF crstat on (crstat.CODE_REF_ID=ed.EVENT_STATUS_CR_FK)
       inner join epacube.CODE_REF cract on (cract.CODE_REF_ID=ed.EVENT_ACTION_CR_FK)
       left join epacube.CODE_REF cretc on (cretc.CODE_REF_ID=ed.EVENT_ENTITY_CLASS_CR_FK)
       inner join epacube.DATA_NAME dn on (ed.DATA_NAME_FK=dn.DATA_NAME_ID)
       inner join epacube.DATA_SET ds on (dn.DATA_SET_FK=ds.DATA_SET_ID)
       inner join epacube.CODE_REF dset on (dset.CODE_REF_ID=ds.DATA_TYPE_CR_FK)


