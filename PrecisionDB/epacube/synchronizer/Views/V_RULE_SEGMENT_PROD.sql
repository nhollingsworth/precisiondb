﻿



---   Entity Structure and Org Entity Structure in result set for outside filtering

---   In Future Releases 
---   add other operators ( examples between; like.. etc. )

CREATE view [synchronizer].[V_RULE_SEGMENT_PROD]
( 
PRODUCT_STRUCTURE_FK,
DATA_NAME_FK,
ORG_IND,
CORP_IND,
PROD_FILTER_HIERARCHY,
ORG_ENTITY_STRUCTURE_FK,
ENTITY_CLASS_CR_FK,
ENTITY_STRUCTURE_FK,
RULES_FILTER_FK )
  AS


  ---PRODuCT SEGMENT

     SELECT SP.product_structure_fk, 
           SP.data_name_fk,
           ds.org_ind,
           ds.corp_ind,
           9 AS PROD_FILTER_HIERARCHY,
           SP.org_entity_structure_fk, 
           NULL AS entity_class_cr_fk,
           SP.entity_structure_fk,  
           r.Prod_Segment_FK filter_fk
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )
	INNER JOIN synchronizer.rules R  WITH (NOLOCK) 
       on ( R.record_status_cr_fk = 1)
	   inner join synchronizer.rules_action RA On (R.RULES_ID = RA.RULES_FK
	   AND RA.BASIS1_DN_FK  = dn.data_name_id )	
	INNER JOIN epacube.SEGMENTS_PRODUCT SP WITH (NOLOCK)  
                  ON ( r.Prod_Segment_FK = SP.SEGMENTS_PRODUCT_ID)
 INNER JOIN epacube.data_value dv WITH (NOLOCK) on ( dv.data_name_fk = sp.data_name_fk 
				  )       
	WHERE 1 = 1



