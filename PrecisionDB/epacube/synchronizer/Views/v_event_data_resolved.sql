﻿
CREATE view [synchronizer].[v_event_data_resolved] as
select
 job.name JOB_NAME
 ,event_data_name.NAME    as event_data_name
 ,event_data_name.LABEL    as event_data_name_lbl
 ,entity_data_name.NAME    as entity_data_name
 ,entity_data_name.LABEL    as entity_data_name_lbl
 ,event_entity_class.code as event_entity_class
 ,entity_class.code as entity_class
 ,event_type.code as event_type
 ,event_condition.code as event_condition
 ,event_status.code as event_status
 ,event_priority.code as event_priority
 ,event_source.code as event_source
 ,event_action.code as event_action
 ,event_reason.code as event_reason
 ,result_type.code as result_type
 ,CASE WHEN ED.EVENT_STATUS_CR_FK = 80 AND ED.EVENT_CONDITION_CR_FK = 97 THEN 1 ELSE 0 END AS PENDING_GREEN
 ,CASE WHEN ED.EVENT_STATUS_CR_FK = 80 AND ED.EVENT_CONDITION_CR_FK = 98 THEN 1 ELSE 0 END AS PENDING_YELLOW
 ,CASE WHEN ED.EVENT_STATUS_CR_FK = 80 AND ED.EVENT_CONDITION_CR_FK = 99 THEN 1 ELSE 0 END AS PENDING_RED
 ,CASE WHEN ED.EVENT_STATUS_CR_FK = 85 THEN 1 ELSE 0 END AS HOLD
 ,CASE WHEN ED.EVENT_STATUS_CR_FK = 86 THEN 1  ELSE 0 END AS READY_STATUS
 ,0 AS APPROVED 
 ,CASE WHEN ED.EVENT_STATUS_CR_FK = 84 THEN 1 ELSE 0 END AS FUTURE
 ,CASE WHEN ED.EVENT_STATUS_CR_FK = 87 THEN 1 ELSE 0 END AS SUBMITTED
 ,CASE WHEN ED.EVENT_STATUS_CR_FK = 88 THEN 1 ELSE 0 END AS PROCESSING
 ,0 AS REJECTED 
 ,CASE WHEN ED.EVENT_STATUS_CR_FK = 89 THEN 1 ELSE 0 END AS WHATIF  
 ,CASE ED.EVENT_STATUS_CR_FK
            WHEN 80 THEN 'USER'
            WHEN 84 THEN 'CMPL'
            WHEN 85 THEN 'USER'
            WHEN 86 THEN 'USER'
            ELSE 'SYNC'
            END AS EVENT_PROCESS
,  (  SELECT top 1 isnull (EI.VALUE, 'NULL')
                               FROM EPACUBE.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
                               INNER JOIN EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
                               ON (    EI.ENTITY_DATA_NAME_FK = 144020
                               AND  EI.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK
                                and ei.data_name_fk in (147111, 144111, 148111, 145111, 142111, 146111, 143111, 141111))
                               WHERE 1 = 1
                                  AND   PAS.DATA_NAME_FK = 159905
                                  AND   PAS.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
                                  AND   PAS.ORG_ENTITY_STRUCTURE_FK = 1
                                    and ei.data_name_fk = 144111)
            
            
            
        as EVENT_GROUP
 ,ei.value AS ENTITY
 ,ed.*
 from synchronizer.EVENT_DATA ed
      LEFT JOIN epacube.DATA_NAME entity_data_name on (entity_data_name.DATA_NAME_ID=ed.ENTITY_DATA_NAME_FK)
      LEFT JOIN epacube.DATA_NAME event_data_name on (event_data_name.DATA_NAME_ID = ed.DATA_NAME_FK)
      LEFT JOIN epacube.CODE_REF event_entity_class on (event_entity_class.CODE_REF_ID=ed.EVENT_ENTITY_CLASS_CR_FK)
      LEFT JOIN epacube.CODE_REF event_type on (event_type.CODE_REF_ID=ed.EVENT_TYPE_CR_FK)
      LEFT JOIN epacube.CODE_REF entity_class on (entity_class.CODE_REF_ID=ed.ENTITY_CLASS_CR_FK)
      LEFT JOIN epacube.CODE_REF event_condition on (event_condition.CODE_REF_ID=ed.EVENT_CONDITION_CR_FK)
      LEFT JOIN epacube.CODE_REF event_status on (event_status.CODE_REF_ID=ed.EVENT_STATUS_CR_FK)
      LEFT JOIN epacube.CODE_REF event_priority on (event_priority.CODE_REF_ID=ed.EVENT_PRIORITY_CR_FK)
      LEFT JOIN epacube.CODE_REF event_source on (event_source.CODE_REF_ID=ed.EVENT_SOURCE_CR_FK)
      LEFT JOIN epacube.CODE_REF event_action on (event_action.CODE_REF_ID=ed.EVENT_ACTION_CR_FK)
      LEFT JOIN epacube.CODE_REF event_reason on (event_reason.CODE_REF_ID=ed.EVENT_ACTION_CR_FK)
      LEFT JOIN epacube.CODE_REF result_type on (result_type.CODE_REF_ID=ed.EVENT_ACTION_CR_FK)
      LEFT JOIN epacube.ENTITY_IDENTIFICATION ei  on (ei.entity_structure_fk = ed.entity_structure_fk and ei.entity_data_name_fk = 144020)
	  LEFT JOIN common.job   on (ed.IMPORT_JOB_FK=job.JOB_ID)
