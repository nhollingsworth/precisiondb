﻿-- =============================================
-- Author:		Gary Stone
-- Create date: May 1, 2014
-- Description:	Update Events with Data_Name-Specific Effective Dates
-- =============================================
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- GS        07/27/2014  Initial SQL Version
-- CV        09/02/2014  Update the correct table (TEMP TABLE)
-- CV        01/16/2015  using actual date vs effective date when comparing todays date




CREATE PROCEDURE [synchronizer].[EVENT_DATA_EFFECTIVE_DATES_UPDATE]
	@In_Job_FK Bigint
AS
BEGIN
	SET NOCOUNT ON;



----	UPDATE  EC
----	Set RESULT_EFFECTIVE_DATE = edbd.Actual_Effective_Date
----			from Synchronizer.event_CALC EC
----		inner join MARGINMGR.EFFECTIVE_DATES_BY_DATA_NAME EDBD 
----	on ec.RESULT_DATA_NAME_FK = edbd.Data_Name_To_Update_FK
----	and ec.product_structure_fk = edbd.product_structure_fk
----	and ec.org_entity_structure_fk = edbd.org_entity_structure_fk
----		inner join
----(
----	Select data_name_to_update_fk, product_structure_fk, org_entity_structure_fk, Min(event_effective_date) MinEffDate
----	from MARGINMGR.EFFECTIVE_DATES_BY_DATA_NAME
----	where CAst(event_effective_date as date) >= Cast(getdate() as date)
----	group by data_name_to_update_fk, product_structure_fk, org_entity_structure_fk
----) NxtChgDate on EDBD.data_name_to_update_fk = NxtChgDate.data_name_to_update_fk 
----	and EDBD.Product_Structure_FK = NxtChgDate.Product_Structure_FK
----	and EDBD.org_entity_structure_fk = NxtChgDate.Org_entity_structure_fk
----	and EDBD.event_effective_date = NxtChgDate.MinEffDate
----		where  1 = 1
----		and cast(ec.RESULT_EFFECTIVE_DATE as date) >= Cast(getdate() as date)




	--select * from synchronizer.event_calc
	--select * from epacube.DATA_NAME where DATA_NAME_ID = 111501




	----Update ed
	----Set Event_Effective_Date = EDBDN.Actual_Effective_Date
	----, Value_Effective_Date = EDBDN.Actual_Effective_Date
	----from Synchronizer.event_data ed
	----inner join MARGINMGR.EFFECTIVE_DATES_BY_DATA_NAME EDBDN with (nolock) 
	----	on Cast(ed.Event_Effective_Date as Date) = Cast(EDBDN.Event_Effective_Date as Date)
	----	and ed.Data_Name_FK = EDBDN.Data_Name_To_Update_FK
	----	and ed.Product_Structure_FK = EDBDN.Product_Structure_FK
	----	and ed.Org_Entity_Structure_FK = EDBDN.Org_Entity_Structure_FK
	----Where ed.Import_Job_FK = @in_Job_FK


--	UPDATE  ED
--	Set Event_Effective_Date = edbd.Actual_Effective_Date
--			from #TS_EVENT_DATA ed
--		inner join MARGINMGR.EFFECTIVE_DATES_BY_DATA_NAME EDBD 
--	on ed.data_name_fk = edbd.Data_Name_To_Update_FK
--	and ed.product_structure_fk = edbd.product_structure_fk
--	and ed.org_entity_structure_fk = edbd.org_entity_structure_fk
--		inner join
--(
--	Select data_name_to_update_fk, product_structure_fk, org_entity_structure_fk, Min(event_effective_date) MinEffDate
--	from MARGINMGR.EFFECTIVE_DATES_BY_DATA_NAME
--	where CAst(event_effective_date as date) >= Cast(getdate() as date)
--	group by data_name_to_update_fk, product_structure_fk, org_entity_structure_fk
--) NxtChgDate on EDBD.data_name_to_update_fk = NxtChgDate.data_name_to_update_fk 
--	and EDBD.Product_Structure_FK = NxtChgDate.Product_Structure_FK
--	and EDBD.org_entity_structure_fk = NxtChgDate.Org_entity_structure_fk
--	and EDBD.event_effective_date = NxtChgDate.MinEffDate
--		where  1 = 1
--		and cast(ed.EVENT_EFFECTIVE_DATE as date) >= Cast(getdate() as date)
	

	UPDATE  ED
	Set Event_Effective_Date = edbd.Actual_Effective_Date
			from #TS_EVENT_DATA ed
		inner join MARGINMGR.EFFECTIVE_DATES_BY_DATA_NAME EDBD 
	on ed.data_name_fk = edbd.Data_Name_To_Update_FK
	and ed.product_structure_fk = edbd.product_structure_fk
	and ed.org_entity_structure_fk = edbd.org_entity_structure_fk
		inner join
(
	Select data_name_to_update_fk, product_structure_fk, org_entity_structure_fk, Min(Actual_Effective_Date) MinEffDate
	from MARGINMGR.EFFECTIVE_DATES_BY_DATA_NAME
	where CAst(Actual_Effective_Date as date) >= Cast(getdate() as date)
	group by data_name_to_update_fk, product_structure_fk, org_entity_structure_fk
) NxtChgDate on EDBD.data_name_to_update_fk = NxtChgDate.data_name_to_update_fk 
	and EDBD.Product_Structure_FK = NxtChgDate.Product_Structure_FK
	and EDBD.org_entity_structure_fk = NxtChgDate.Org_entity_structure_fk
	and EDBD.Actual_Effective_Date = NxtChgDate.MinEffDate
		where  1 = 1
		and cast(ed.event_EFFECTIVE_DATE as date) >= Cast(getdate() as date)
	

END

