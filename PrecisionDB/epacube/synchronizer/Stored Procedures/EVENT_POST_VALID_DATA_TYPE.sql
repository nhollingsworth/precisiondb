﻿

 
 
 
 
 
 
 
 
 
 
-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform all data type validations
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010   Initial SQL Version
-- CV        01/27/2011   jira 3153 - able to null a value and not get lenght check
-- CV        06/10/2011   Do not check Length for -999999999 data
-- CV        04/08/2013   Excluded data set 10300 for checking length. product text
-- CV        05/04/2015   Delete blank events
 
CREATE PROCEDURE [synchronizer].[EVENT_POST_VALID_DATA_TYPE]
AS
BEGIN
 
DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count               bigint
 
DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime
 
DECLARE  @status_desc           varchar (max)
DECLARE  @ErrorMessage          nvarchar(4000)
DECLARE  @ErrorSeverity         int
DECLARE  @ErrorState            int
 
 
BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_DATA_TYPE.'
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                                                              @exec_id   = @l_exec_no OUTPUT;
 
 -----------------------------------------------------------------------------
 ---- REMOVE THE EVENTS WITH BLANK = ' ' data
 ---------------------------------------------------------------------------

            DELETE from #TS_EVENT_DATA 
					where NEW_DATA = ' ' 
					and event_type_CR_FK = 150
 
---------------------------------NULL NEW DATA
 
                  INSERT INTO #TS_EVENT_DATA_ERRORS (
                        EVENT_FK,
                        RULE_FK,
                        EVENT_CONDITION_CR_FK,
                        ERROR_NAME,
                        ERROR_DATA1,
                        ERROR_DATA2,
                        ERROR_DATA3,
                        ERROR_DATA4,
                        ERROR_DATA5,
                        ERROR_DATA6,
                        UPDATE_TIMESTAMP)
            (     SELECT
                   TSED.EVENT_FK
                  ,NULL
                  ,99
                  ,'NEW DATA IS NULL'  --ERROR NAME
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,@l_sysdate
                  FROM #TS_EVENT_DATA TSED
                  WHERE ISNULL ( new_data, 'NULL DATA' )= 'NULL DATA' )
 
 
 
 
 
---------------------------------NULL ORG ENTITY STRUCTURE
 
                  INSERT INTO #TS_EVENT_DATA_ERRORS (
                        EVENT_FK,
                        RULE_FK,
                        EVENT_CONDITION_CR_FK,
                        ERROR_NAME,
                        ERROR_DATA1,
                        ERROR_DATA2,
                        ERROR_DATA3,
                        ERROR_DATA4,
                        ERROR_DATA5,
                        ERROR_DATA6,
                        UPDATE_TIMESTAMP)
            (     SELECT
                   TSED.EVENT_FK
                  ,NULL
                  ,99
                  ,'NO WAREHOUSE FOUND'  --ERROR NAME
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,@l_sysdate
                  FROM #TS_EVENT_DATA TSED
                  WHERE ORG_ENTITY_STRUCTURE_FK IS NULL )

				  -------------------------------------------------------------------

				   INSERT INTO #TS_EVENT_DATA_ERRORS (
                        EVENT_FK,
                        RULE_FK,
                        EVENT_CONDITION_CR_FK,
                        ERROR_NAME,
                        --ERROR_DATA1,
                        --ERROR_DATA2,
                        ERROR_DATA3,
                        ERROR_DATA4,
                        ERROR_DATA5,
                        ERROR_DATA6,
                        UPDATE_TIMESTAMP)
            (     SELECT
                   TSED.EVENT_FK
                  ,NULL
                  ,99
                  ,'NUMERIC REQUIRED'  --ERROR NAME        
                  --,'DATA TYPE IN NOT NUMERIC'--ERROR1
                  --,'CHECK FOR DATA SET FOR DATA TYPE'
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,@l_sysdate
                  FROM #TS_EVENT_DATA TSED
                  WHERE import_filename = '#PBV'
                  AND isnumeric( new_data)= 0 )
 
 
 
---------------------------------NUMERIC
 
                  INSERT INTO #TS_EVENT_DATA_ERRORS (
                        EVENT_FK,
                        RULE_FK,
                        EVENT_CONDITION_CR_FK,
                        ERROR_NAME,
                        --ERROR_DATA1,
                        --ERROR_DATA2,
                        ERROR_DATA3,
                        ERROR_DATA4,
                        ERROR_DATA5,
                        ERROR_DATA6,
                        UPDATE_TIMESTAMP)
            (     SELECT
                   TSED.EVENT_FK
                  ,NULL
                  ,99
                  ,'NUMERIC REQUIRED'  --ERROR NAME        
                  --,'DATA TYPE IN NOT NUMERIC'--ERROR1
                  --,'CHECK FOR DATA SET FOR DATA TYPE'
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,@l_sysdate
                  FROM #TS_EVENT_DATA TSED
                  WHERE data_type_cr_fk = 131
                  AND isnumeric( new_data)= 0 
				  and new_data <> '-999999999')
 
---------------------------------DATE
 
 
--- FORMAT DATE FIRST BEFORE VALIDATION
 
UPDATE #TS_EVENT_DATA
SET NEW_DATA =
      (SELECT CONVERT ( VARCHAR, #TS_EVENT_DATA.NEW_DATA,
                                             ( SELECT CAST ( CODE AS SMALLINT ) FROM EPACUBE.CODE_REF
                                                 WHERE CODE_REF_ID = ( SELECT ISNULL (DN.DATE_FORMAT_CR_FK, 733)
                                                 FROM epacube.data_name DN
                                                WHERE DN.DATA_NAME_ID = #TS_EVENT_DATA.DATA_NAME_FK )
                              ) ) )
WHERE data_type_cr_fk = 132
 
 
                  INSERT INTO #TS_EVENT_DATA_ERRORS (
                        EVENT_FK,
                        RULE_FK,
                        EVENT_CONDITION_CR_FK,
                        ERROR_NAME,
                        --ERROR_DATA1,
                        --ERROR_DATA2,
                        ERROR_DATA3,
                        ERROR_DATA4,
                        ERROR_DATA5,
                        ERROR_DATA6,
                        UPDATE_TIMESTAMP)
            (     SELECT
                   TSED.EVENT_FK
                  ,NULL
                  ,99
                  ,'DATE REQUIRED'  --ERROR NAME      
                  --,'DATA TYPE IN NOT A DATE'--ERROR1
                  --,'CHECK FOR DATA SET FOR DATA TYPE'
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,@l_sysdate
                  FROM #TS_EVENT_DATA TSED
                  WHERE data_type_cr_fk = 132
                  AND isdate( new_data)= 0 
				  and new_Data <> '-999999999')

 
---------------------------------Y/N
 
                  INSERT INTO #TS_EVENT_DATA_ERRORS (
                        EVENT_FK,
                        RULE_FK,
                        EVENT_CONDITION_CR_FK,
                        ERROR_NAME,
                        --ERROR_DATA1,
                        --ERROR_DATA2,
                        ERROR_DATA3,
                        ERROR_DATA4,
                        ERROR_DATA5,
                        ERROR_DATA6,
                        UPDATE_TIMESTAMP)
            (     SELECT
                   TSED.EVENT_FK
                  ,NULL
                  ,99
                  ,'Y OR N REQUIRED'  --ERROR NAME       
                  --,'DATA TYPE IN NOT YES/NO'--ERROR1
                  --,'CHECK FOR DATA SET FOR DATA TYPE'
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,@l_sysdate
                  FROM #TS_EVENT_DATA TSED
                  WHERE data_type_cr_fk = 133
                  AND new_data NOT IN  ('Y', 'N','-999999999' ))
 
 
---------------------------------epaCUBE STATUS
 
                  INSERT INTO #TS_EVENT_DATA_ERRORS (
                        EVENT_FK,
                        RULE_FK,
                        EVENT_CONDITION_CR_FK,
                        ERROR_NAME,
                        ERROR_DATA1,
                        ERROR_DATA2,
                        ERROR_DATA3,
                        ERROR_DATA4,
                        ERROR_DATA5,
                        ERROR_DATA6,
                        UPDATE_TIMESTAMP)
            (     SELECT
                   TSED.EVENT_FK
                  ,NULL
                  ,99
                  ,'INCORRECT epaCUBE Status Code'  --ERROR NAME       
                  ,NULL  --ERROR1
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,@l_sysdate
                  FROM #TS_EVENT_DATA TSED
                  LEFT JOIN epacube.CODE_REF CR WITH (NOLOCK)
                    ON ( CR.code_type = 'PRODUCT_STATUS'
                    AND CR.code = ISNULL ( TSED.new_data, 'NULL DATA' )  )
                  WHERE TSED.table_name = 'PRODUCT_STRUCTURE'
                  AND   TSED.data_name_fk = 110001   --- epacube STATUS
                  AND   CR.code_ref_id IS NULL
        )
 
 
 
 
------------------------------------------------------------------------------------------
--  CHECK LENGTH    -- ADD TRUNC LATER
------------------------------------------------------------------------------------------
 
 
                  INSERT INTO #TS_EVENT_DATA_ERRORS (
                        EVENT_FK,
                        RULE_FK,
                        EVENT_CONDITION_CR_FK,
                        ERROR_NAME,
                        ERROR_DATA1,
                        ERROR_DATA2,
                        ERROR_DATA3,
                        ERROR_DATA4,
                        ERROR_DATA5,
                        ERROR_DATA6,
                        UPDATE_TIMESTAMP)
            (     SELECT
                   TSED.EVENT_FK
                  ,NULL
                  ,CASE ISNULL ( DN.TRUNC_METHOD_CR_FK, 0 )
                  WHEN 0 THEN 99
                  ELSE 98
                  END
                  ,CASE ISNULL ( DN.MAX_OR_EQ, 'MAX' )
                   WHEN 'MAX' THEN 'Length has exceeded maximum'
                   ELSE 'Length is not equal to'
                   END  --- error name  
                  ,DN.LENGTH  --ERROR1
                  ,'Current Length '
                  ,LEN ( TSED.NEW_DATA )
                  ,CASE ISNULL ( DN.TRUNC_METHOD_CR_FK, 0 )
                  WHEN 0 THEN NULL
                  ELSE 'Truncated'
                  END
                  ,NULL
                  ,NULL
                  ,@l_sysdate
                  FROM #TS_EVENT_DATA TSED
            INNER JOIN epacube.DATA_NAME dn  WITH (NOLOCK) ON (dn.DATA_NAME_ID = tsed.DATA_NAME_FK)          
                  WHERE tsed.data_type_cr_fk in (130 , 134, 135 ) -- STRING, data value
                  AND dn.DATA_SET_FK <> 10300
                  AND   DN.LENGTH IS NOT NULL
AND  (TSED.NEW_DATA <> 'NULL'    -----Jira 3153
AND  TSED.NEW_DATA <> '-999999999')
                  AND   (
                          (     dn.max_or_eq = 'EQ'
                          AND  LEN ( TSED.NEW_DATA ) <> ISNULL (DN.LENGTH, 256) )
                        OR
                            (     LEN ( TSED.NEW_DATA ) > ISNULL (DN.LENGTH, 256) )                 
                          )
                  )
                       
 
            UPDATE #TS_EVENT_DATA
            SET NEW_DATA = LEFT ( NEW_DATA, DN.LENGTH )
                  FROM EPACUBE.DATA_NAME DN  WITH (NOLOCK)
                  WHERE DN.DATA_NAME_ID = #TS_EVENT_DATA.DATA_NAME_FK
                  AND   DN.LENGTH IS NOT NULL
                  AND   DN.TRUNC_METHOD_CR_FK = 8501   -- LENGTH
                  AND   #TS_EVENT_DATA.NEW_DATA <> '-999999999'
 
 
 
------------------------------------------------------------------------------------------
--  CHECK SCALE OF DECIMAL    -- ADD ROUND LATER
------------------------------------------------------------------------------------------
 
 
 
                  INSERT INTO #TS_EVENT_DATA_ERRORS (
                        EVENT_FK,
                        RULE_FK,
                        EVENT_CONDITION_CR_FK,
                        ERROR_NAME,
                        ERROR_DATA1,
                        ERROR_DATA2,
                        ERROR_DATA3,
                        ERROR_DATA4,
                        ERROR_DATA5,
                        ERROR_DATA6,
                        UPDATE_TIMESTAMP)
            (     SELECT
                   A.EVENT_FK
                  ,NULL
                  ,CASE ISNULL ( A.ROUNDING_METHOD_CR_FK, 0 )
                  WHEN 0 THEN 99
                  ELSE 98
                  END
                  ,'Decimal Scale has exceeded Maximum'  --- error name  
                  ,A.SCALE  --ERROR1
                  ,'Current Scale '
                  ,A.NEW_SCALE
                  ,CASE ISNULL ( A.ROUNDING_METHOD_CR_FK, 0 )
                  WHEN 0 THEN NULL
                   ELSE 'Rounded'
                  END
                  ,NULL
                  ,NULL
                  ,@l_sysdate
                  FROM (
                  SELECT TSED.EVENT_FK,
                         DN.ROUNDING_METHOD_CR_FK,
                         DN.SCALE,
                           ( SELECT LEN ( (
                             SELECT SUBSTRING ( epacube.removeZeros ( TSED.NEW_DATA ),(
                                    CASE ( SELECT PATINDEX ( '%.%', epacube.removeZeros ( TSED.NEW_DATA ) ) )
                                    WHEN 0 THEN 99
                                    ELSE  ( SELECT PATINDEX ( '%.%', epacube.removeZeros ( TSED.NEW_DATA ) ) + 1 )
                                    END ) ,98 ) ) ) ) AS NEW_SCALE                        
                  FROM #TS_EVENT_DATA TSED
            INNER JOIN epacube.DATA_NAME dn  WITH (NOLOCK) ON (dn.DATA_NAME_ID = tsed.DATA_NAME_FK)          
                  WHERE DN.SCALE IS NOT NULL
                  AND   TSED.DATA_TYPE_CR_FK = 131   -- MUST BE A NUMBER
                  AND  TSED.NEW_DATA <> '-999999999'
                  ) A
            WHERE A.NEW_SCALE > A.SCALE )
          
 
/*
ROUNDING NOTES:
      Just doing standard rounding here... no rouding to value because all we have is SCALE
     
*/
 
 
 
UPDATE #TS_EVENT_DATA
SET    NEW_DATA   = (  SELECT CASE ISNULL ( DN.ROUNDING_METHOD_CR_FK, 0 )
                           WHEN 0   THEN NEW_DATA
                              WHEN 720 THEN ( select common.RoundNearest ( CAST ( NEW_DATA AS NUMERIC(18,6)  ), DN.SCALE ) )
                              WHEN 721 THEN ( select common.RoundUp ( CAST ( NEW_DATA AS NUMERIC(18,6)  ), DN.SCALE ) )
                              WHEN 722 THEN ( select common.RoundDown ( CAST ( NEW_DATA AS NUMERIC(18,6)  ), DN.SCALE ) )
                              ELSE NEW_DATA
                           END
                          FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)
                          WHERE DN.DATA_NAME_ID = #TS_EVENT_DATA.DATA_NAME_FK
                          AND  #TS_EVENT_DATA.NEW_DATA <> '-999999999' )
-----
WHERE 1 = 1
AND   ISNUMERIC(NEW_DATA) = 1
AND   DATA_TYPE_CR_FK = 131   -- MUST BE A NUMBER
AND   DATA_NAME_FK IN ( SELECT DNX.DATA_NAME_ID
                        FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
                        WHERE DNX.SCALE IS NOT NULL )
 
 
 


UPDATE #TS_EVENT_DATA
SET    NEW_DATA	= (select epacube.StripCharacters(NEW_DATA, '^.0-9')
					
			        FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)
			        WHERE DN.DATA_NAME_ID = #TS_EVENT_DATA.DATA_NAME_FK
			        AND  #TS_EVENT_DATA.NEW_DATA <> '-999999999' )


-----
WHERE 1 = 1
AND   ISNUMERIC(NEW_DATA) = 1
AND   DATA_TYPE_CR_FK = 131   -- MUST BE A NUMBER
AND   DATA_NAME_FK IN ( SELECT DNX.DATA_NAME_ID
                        FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)
						inner join epacube.data_Set ds with (nolock)
						on (dnx.DATA_SET_FK = ds.DATA_SET_ID
						and ds.DATA_TYPE_CR_FK = 131))
                       


 
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------
 
SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_DATA_TYPE'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;
 
END TRY
BEGIN CATCH
 
            SELECT
                  @ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_DATA_TYPE has failed ' + ERROR_MESSAGE(),
                  @ErrorSeverity = ERROR_SEVERITY(),
                  @ErrorState = ERROR_STATE();
 
 
         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
                            @ErrorSeverity, -- Severity.
                            @ErrorState -- State.
                            );
END CATCH
 
END
 
 

