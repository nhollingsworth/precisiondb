﻿



CREATE PROCEDURE [synchronizer].[PURGE_FINAL_EVENTS_BY_JOB] 
		(@in_import_job_fk bigint)
    AS
BEGIN
      DECLARE 
        /* Copyright 2005, epaCUBE, Inc. */
        /* $Header: /sdi/development/db/update/1.06.06.03/proc_sheet_calc_insert_results.sql,v 1.1 2005/05/11 18:43:34 cnoe Exp $ 
		

			Purpose: This procedure is called on an as-needed basis via the Import Jobs UI


			Modification History

			Name____Date___________________Mod_Comments______________________________________			
			RG	 10/14/2009                 First Cut 


*/
        @l_sysdate datetime,
        @ls_stmt varchar(1000),
        @l_exec_no bigint,
        @status_desc varchar(max),             
		@ApprovedStatus int,
		@RejectedStatus int

		SET @l_sysdate = getdate()
		set @RejectedStatus = epacube.getCodeRefId('EVENT_STATUS','REJECTED')
		set @ApprovedStatus = epacube.getCodeRefId('EVENT_STATUS','APPROVED')
        EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of EPACUBE.PURGE_FINAL_EVENTS_BY_JOB' , 
												@exec_id = @l_exec_no OUTPUT;
        



        /* ----------------------------------------------------------------------------------- */
        --    Procedure for deletion of final status events
        /* ----------------------------------------------------------------------------------- */
BEGIN TRY



	    delete synchronizer.event_data_errors
        where  event_fk in
				(select event_id From synchronizer.event_data with (NOLOCK)
				 where event_status_cr_fk = @ApprovedStatus
				 and import_job_fk = @in_import_job_fk) 

		delete synchronizer.event_data_errors
        where  event_fk in
				(select event_id From synchronizer.event_data with (NOLOCK)
				 where event_status_cr_fk = @RejectedStatus
				 and import_job_fk = @in_import_job_fk) 
        
		delete synchronizer.event_data_rules
        where  event_fk in
				(select event_id From synchronizer.event_data with (NOLOCK)
				 where event_status_cr_fk = @ApprovedStatus
				 and import_job_fk = @in_import_job_fk) 

		delete synchronizer.event_data_rules
        where  event_fk in
				(select event_id From synchronizer.event_data with (NOLOCK)
				 where event_status_cr_fk = @RejectedStatus
				 and import_job_fk = @in_import_job_fk) 		    
        
		delete synchronizer.event_data
        where  event_status_cr_fk  = @ApprovedStatus
				and import_job_fk = @in_import_job_fk 

		delete synchronizer.event_data
        where  event_status_cr_fk  = @RejectedStatus
				and import_job_fk = @in_import_job_fk 

---------------------------------------------------------------------------------------
----   End of Processing PURGE_FINAL_EVENTS_BY_JOB
---------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of SYNCHRONIZER.PURGE_FINAL_EVENTS_BY_JOB';
     --EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.PURGE_FINAL_EVENTS_BY_JOB has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


        -- EXEC exec_monitor.Report_Error_sp @l_exec_no;
        declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
END



