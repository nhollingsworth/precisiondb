﻿



-- Copyright 2008
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to add, modify, and delete Taxonomy Node Properties.
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG       09/18/2008  Initial SQL Version
-- RG		03/14/2013	EPA-3521 Changing value updates/inserts
-- MS       05/13/2014	Add processing for attribute value display sequence
--
CREATE PROCEDURE [synchronizer].[TAXONOMY_NODE_ATTRIBUTE_DV_EDITOR_IMPORT]
      (
        @in_taxonomy_node_attribute_id int
      , @in_data_value varchar(875)
      , @in_ui_action_cr_fk int
      , @in_update_user varchar(64)
	  , @in_attrvalue_display_seq INT
      )
AS
/***************  Parameter Defintions  **********************

Simple for Now.... Only CREATE (8003) 

***************************************************************/

      BEGIN

            DECLARE @ErrorMessage nvarchar(4000)
            DECLARE @ErrorSeverity int
            DECLARE @ErrorState int
            DECLARE @TNA_data_name_id int
            DECLARE @TNA_data_value_id bigint


            SET NOCOUNT ON ;
            set @ErrorMessage = null
            BEGIN TRY
----------------------------------------------
-- VALIDATIONS
-- #1. Node Attribute ID and Data Value may not be null
----------------------------------------------

----------------------------------------------
-- #1. Must Exist validations
----------------------------------------------
                  IF @in_ui_action_cr_fk in (
                     8003 ) 
                     BEGIN
                           IF @in_taxonomy_node_attribute_id IS NULL 
                              BEGIN
                                    SET @ErrorMessage = 'Taxonomy Node Attribute ID cannot be null'
                                    set @ErrorSeverity = 16
                                    set @ErrorState = 1
                                    RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState )
                              END	

                           IF @in_data_value IS NULL 
                              BEGIN
                                    SET @ErrorMessage = 'Data Value cannot be null'
                                    set @ErrorSeverity = 16
                                    set @ErrorState = 1
                                    RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState ) 
                              END


                     END --end validations

                  IF @in_ui_action_cr_fk = 8003 --create
                     BEGIN
		
                           set @TNA_data_name_id = ( select data_name_fk
                                                     from   epacube.taxonomy_node_attribute
                                                     where  taxonomy_node_attribute_id = @in_taxonomy_node_attribute_id
                                                   )
		


----------------------------
-- Check to see if data value exists. If it does not, create one.
-- Then assign value to variable for future use
----------------------------
	 
                           -- added data_name_fk = @TNA_data_name_id to the where clause. Yuri Nogin 11/5/2008
                           IF NOT EXISTS ( select   1
                                           from     epacube.data_value
                                           where    value = @in_data_value AND
                                                    data_name_fk = @TNA_data_name_id ) 
                              BEGIN			
                                    INSERT  INTO EPACUBE.DATA_VALUE
                                            (
                                              data_name_fk
                                            , value
                                            , description
                                            , record_status_cr_fk
                                            , create_timestamp
                                            , update_timestamp
                                            , update_user
                                            )
                                            select  @TNA_data_name_id
													,CASE 
														WHEN (@TNA_data_name_id in (select data_name_id from epacube.data_name
																					where UPPER_CASE_IND = 1)) 
														THEN upper(@in_data_value)
														ELSE @in_data_value 
													END
                                                  , @in_data_value
                                                  , 1
                                                  , getdate()
                                                  , getdate()
                                                  , @in_update_user					  
                              END -- end data_value_insertion

	
----------------------------------------------------------------------
-- Convert passed Data Value into a data value id
----------------------------------------------------------------------
                           set @TNA_data_value_id = ( select    data_value_id
                                                      from      epacube.data_value
                                                      WHERE     value = @in_data_value AND
                                                                data_name_fk = @TNA_data_name_id
                                                    )
										--where description = @in_data_value)	commented by Yuri Nogin 11/5/2008
-------------------------------------------------------------------------------------
--   Edits passed Insert Row
-------------------------------------------------------------------------------------

                           INSERT   INTO EPACUBE.TAXONOMY_NODE_ATTRIBUTE_DV
                                    (
                                      taxonomy_node_attribute_fk
                                    , data_value_fk
                                    , record_status_cr_fk
                                    , create_timestamp
                                    , update_timestamp
									, DISPLAY_SEQ
                                    )
                                    SELECT  @in_taxonomy_node_attribute_id
                                          , @TNA_data_value_id
                                          , 1
                                          , getdate()
                                          , getdate()
										  , @in_attrvalue_display_seq
		
                     END --- IF @in_ui_action_cr_fk = 8003

            END TRY


            BEGIN CATCH
                  BEGIN
                        IF ISNULL(@ErrorMessage, '') = '' 
                           SELECT   @ErrorMessage = ERROR_MESSAGE()
                        set @ErrorSeverity = 16
                        set @ErrorState = 1
                        RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState ) 
                        return ( -1 )
                  END
                  RETURN ( -1 )
               
            END CATCH		
      END





