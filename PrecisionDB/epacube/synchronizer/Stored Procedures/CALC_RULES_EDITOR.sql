﻿



-- Copyright 2013
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to create/modify Calculation Rules
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        11/08/2013   Initial SQL Version


CREATE PROCEDURE [synchronizer].[CALC_RULES_EDITOR] 
( @in_rules_id bigint, 
  @in_rule_name varchar(128),
  @in_rule_structure_id int,
  @in_rule_schedule_id int,
  @in_result_DNID int,
  @in_product_filter_DNID int,
  @in_product_filter_value1 varchar(64),
  @in_vendor_filter_DNID int,
  @in_vendor_filter_value1 varchar(64),
  @in_vendor_filter_assoc_DNID int,
  @in_warehouse_filter_DNID int,
  @in_warehouse_filter_value1 varchar(64),
  @in_warehouse_filter_assoc_DNID int,
  @in_basis_calc_DNID int,
  @in_effective_date DATETIME,
  @in_end_date DATETIME,
  @in_rule_action_operation1 int,
  @in_basis1_DNID int,
  @in_basis1_number numeric(18,6),
  @in_rule_action_operation2 int,
  @in_basis2_DNID int,
  @in_basis2_number numeric(18,6),
  @in_rule_action_operation3 int,
  @in_basis3_DNID int,
  @in_basis3_number numeric(18,6),
  @in_rounding_method_cr_fk int,
  @in_rounding_precision numeric(18,6),
  @in_rounding_adder_value numeric(18,6),
  @in_basis_use_corp_only_ind smallint,
  @in_rule_precedence smallint,
  @in_record_status_cr_fk int,  
  @in_ui_action_cr_fk int,
  @in_update_user varchar(64) )
												
AS

/***************  Parameter Defintions  **********************

***************************************************************/

BEGIN

DECLARE @l_sysdate					datetime
DECLARE @ls_stmt					varchar (1000)
DECLARE @l_exec_no					bigint
DECLARE @l_rows_processed			bigint
DECLARE @ErrorMessage				nvarchar(4000)
DECLARE @ErrorSeverity				int
DECLARE @ErrorState					int


DECLARE @createUIActionCodeRefID	int
DECLARE @changeUIActionCodeRefID	int
DECLARE @insertedRuleID				bigint

DECLARE @insertedProductFilterID	bigint
DECLARE @insertedOrgFilterID		bigint
DECLARE	@insertedVendorFilterID		bigint

DECLARE @activeStatusCRID			int
DECLARE @equalsOperatorCRID			int
DECLARE @productEntityClassCRID		int
DECLARE @vendorEntityClassCRID		int
DECLARE @orgEntityClassCRID			int
DECLARE @productEntityDNID			int
DECLARE @vendorEntityDNID			int
DECLARE @orgEntityDNID				int

DECLARE @prodFilterID				int
DECLARE @orgFilterID				int
DECLARE @vendorFilterID				int



SET NOCOUNT ON;
SET @l_sysdate = getdate ()
set @ErrorMessage = null

set @activeStatusCRID = (select epacube.getCodeRefId('RECORD_STATUS','ACTIVE'))
set @productEntityClassCRID = (select epacube.getCodeRefId('ENTITY_CLASS','PRODUCT'))
set @vendorEntityClassCRID = (select epacube.getCodeRefId('ENTITY_CLASS','VENDOR'))
set @orgEntityClassCRID = (select epacube.getCodeRefId('ENTITY_CLASS','WAREHOUSE'))
set @productEntityDNID = (select data_name_id from epacube.data_name where name = 'PRODUCT')
set @vendorEntityDNID = (select data_name_id from epacube.data_name where name = 'VENDOR')
set @orgEntityDNID = (select data_name_id from epacube.data_name where name = 'WAREHOUSE')


set @createUIActionCodeRefID = (select epacube.getCodeRefId('UI_ACTION','CREATE'))
set @changeUIActionCodeRefID = (select epacube.getCodeRefId('UI_ACTION','CHANGE'))
set @equalsOperatorCRID = (select epacube.getCodeRefId('OPERATOR','='))
set @in_rule_precedence = isnull(@in_rule_precedence, 9999)



-------------------------------------------------------------------------------------
-- Perform Server-side validation 
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--   Check for Duplicate Name 
-------------------------------------------------------------------------------------	
	select @ErrorMessage = 'Duplicate Rule Name'
	from synchronizer.RULES
	where @in_rule_name = name
	and (
		-- Change Test: If the names are equal, and it's a change UI action, but the rules id are not equal, 
		-- then the user is clearly trying to change this rule's name such that it duplicates another's.
		(@in_rules_id <> rules_id
		 and @in_rules_id is not null
		 and @in_ui_action_cr_fk = @changeUIActionCodeRefID)		
		or 
		-- Create test: If the names are equal, and the rules_id is null, and it's a 
		-- create UI action, the user is trying to create a new rule with a pre-existing rule's name.
		(@in_rules_id is null
		and @in_ui_action_cr_fk = @createUIActionCodeRefID)
	)
			 IF @@rowcount > 0
					GOTO RAISE_ERROR


					
-------------------------------------------------------------------------------------
-- If Product Filter Data Name is populated, Product Filter Value1 must be as well, and vice-versa
-------------------------------------------------------------------------------------
	select @ErrorMessage = 'Data Name and Product Filter Value1 inputs must be used in tandem.'
	from synchronizer.RULES
	where (@in_product_filter_DNID is null and @in_product_filter_value1 is not null)
	or 
	(@in_product_filter_DNID is not null and @in_product_filter_value1 is null)
			
			IF @ErrorMessage is not null
				GOTO RAISE_ERROR


-------------------------------------------------------------------------------------
-- If Vendor Filter Data Name is populated, Vendor Filter Value1 and Vendor filter assoc must be as well, and vice-versa
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Vendor DNID, Vendor Filter Value1, and Vendor Filter Assoc must be used in tandem.'
	from synchronizer.RULES
	
	where ((@in_vendor_filter_assoc_DNID is null or @in_vendor_filter_DNID is null or @in_vendor_filter_value1 is null)
			and 
		   (@in_vendor_filter_assoc_DNID is not null or @in_vendor_filter_DNID is not null or @in_vendor_filter_value1 is not null)
		  )

		  IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- If Whse Filter Data Name is populated, Whse Filter Value1 and Whse filter assoc must be as well, and vice-versa
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Warehouse DNID, Warehouse Filter Value1, and Warehouse Filter Assoc must be used in tandem.'
	from synchronizer.RULES
	
	where ((@in_warehouse_filter_assoc_DNID is null or @in_warehouse_filter_DNID is null or @in_warehouse_filter_value1 is null)
			and 
		   (@in_warehouse_filter_assoc_DNID is not null or @in_warehouse_filter_DNID is not null or @in_warehouse_filter_value1 is not null)
		  )

		  IF @ErrorMessage is not null
				GOTO RAISE_ERROR


-------------------------------------------------------------------------------------
-- If Basis1 Data Name is populated, Basis 1 number cannot be
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Basis 1 Number and Basis 1 Data Name are not used in conjunction. Select one or the other'
from synchronizer.rules
where @in_basis1_DNID is not null and @in_basis1_number is not null

		IF @ErrorMessage is not null
			GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- If Basis2 Data Name is populated, Basis 2 number cannot be
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Basis 2 Number and Basis 2 Data Name are not used in conjunction. Select one or the other'
from synchronizer.rules
where @in_basis2_DNID is not null and @in_basis2_number is not null

		IF @ErrorMessage is not null
			GOTO RAISE_ERROR


-------------------------------------------------------------------------------------
-- If Basis3 Data Name is populated, Basis 3 number cannot be
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Basis 3 Number and Basis 3 Data Name are not used in conjunction. Select one or the other'
from synchronizer.rules
where @in_basis3_DNID is not null and @in_basis3_number is not null

		IF @ErrorMessage is not null
			GOTO RAISE_ERROR



-------------------------------------------------------------------------------------
-- Perform UI Create Action
-------------------------------------------------------------------------------------


  IF @in_ui_action_cr_fk = @createUIActionCodeRefID
  BEGIN

	--First, insert into the Rules table so we can get a primary key for the dependent inserts
	INSERT INTO synchronizer.rules
	(NAME, RULES_STRUCTURE_FK, RULES_SCHEDULE_FK, RESULT_DATA_NAME_FK, EFFECTIVE_DATE, END_DATE
		,RECORD_STATUS_CR_FK, RULE_PRECEDENCE, CREATE_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_USER, CREATE_USER)
	SELECT
	@in_rule_name, @in_rule_structure_id, @in_rule_schedule_id, @in_result_DNID, @in_effective_date, @in_end_date
		, @in_record_status_cr_fk, @in_rule_precedence, getdate(), getdate(), @in_update_user, @in_update_user

	--Now we need the primary key for the rule we just inserted in order to continue...
	set @insertedRuleID = (select rules_id from synchronizer.rules where name = @in_rule_name)
	
	
	--Next, insert into Rules Action:
	INSERT INTO synchronizer.RULES_ACTION
	(RULES_FK, BASIS_CALC_DN_FK, RULES_ACTION_OPERATION1_FK, BASIS1_DN_FK, BASIS1_NUMBER
		,RULES_ACTION_OPERATION2_FK,BASIS2_DN_FK, BASIS2_NUMBER
		,RULES_ACTION_OPERATION3_FK,BASIS3_DN_FK, BASIS3_NUMBER
		,ROUNDING_METHOD_CR_FK, ROUNDING_TO_VALUE, ROUNDING_ADDER_VALUE, BASIS_USE_CORP_ONLY_IND
		,CREATE_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_USER )
	SELECT
	@insertedRuleID, @in_basis_calc_DNID, @in_rule_action_operation1, @in_basis1_DNID, @in_basis1_number
		, @in_rule_action_operation2, @in_basis2_DNID, @in_basis2_number
		, @in_rule_action_operation3, @in_basis3_DNID, @in_basis3_number
		,@in_rounding_method_cr_fk, @in_rounding_precision, @in_rounding_adder_value, @in_basis_use_corp_only_ind
		,getdate(), getdate(), @in_update_user
	

	------------------------------------------------------------------------------------------------------------
	-- Rules Filter and Rules Filter Set inserts
	-- For each supported filter type with a non-null input value:
	--			Prod
	--			Org (Whse)
	--			Supl (Vend(?))
	-- Insert into Rule Filter first
	-- Then, insert into Rules Filter Set
	------------------------------------------------------------------------------------------------------------


	-- Check and see if Prod Filter stuff is null
	-- If it's not null, insert that bad beast, and then get the primary key back for the corresponding Rules Filter Set insert to follow
	--Insert into Rules Filter
	IF ((@in_product_filter_DNID is not null) and  (@in_product_filter_value1 is not null))
	BEGIN

	------------------------------------------------------------------------------------------------------------

	select @insertedProductFilterID = rules_filter_id
	from synchronizer.rules_filter rf
	where ((@in_product_filter_DNID = DATA_NAME_FK) or (@in_product_filter_DNID is null and DATA_NAME_FK is null))
	and ((@in_product_filter_value1 = VALUE1) or (@in_product_filter_value1 is null and VALUE1 is null))
	and @productEntityDNID = ENTITY_DATA_NAME_FK
	and @equalsOperatorCRID = OPERATOR_CR_FK
	

		--check unique constraint and set inserted product filter ID to that instead.
		---[ENTITY_DATA_NAME_FK] ASC,
	--[DATA_NAME_FK] ASC,
	--[VALUE1] ASC,
	--[VALUE2] ASC,
	--[OPERATOR_CR_FK] ASC
	------------------------------------------------------------------------------------------------------------
	IF @insertedProductFilterID is null
	BEGIN
		INSERT INTO synchronizer.RULES_FILTER
		(ENTITY_CLASS_CR_FK, NAME, 
				 DATA_NAME_FK, ENTITY_DATA_NAME_FK, VALUE1
				,OPERATOR_CR_FK, RECORD_STATUS_CR_FK, CREATE_TIMESTAMP, UPDATE_USER)
		SELECT
		@productEntityClassCRID, ((select label from epacube.data_name where data_name_id = @in_product_filter_DNID ) +': ' + @in_product_filter_value1)
				,@in_product_filter_DNID, @productEntityDNID, @in_product_filter_value1
				,@equalsOperatorCRID, @activeStatusCRID, getdate(), @in_update_user
	
	-- now, fetch the Rules Filter ID for this freshly inserted entry for use in the Rules Filter Set insert:
		set @insertedProductFilterID = (select rules_filter_id from synchronizer.RULES_FILTER 
										where name =  ((select label from epacube.data_name where data_name_id = @in_product_filter_DNID ) +': ' + @in_product_filter_value1)
										and ENTITY_DATA_NAME_FK = @productEntityDNID
										and ENTITY_CLASS_CR_FK = @productEntityClassCRID
										and DATA_NAME_FK = @in_product_filter_DNID
										and VALUE1 = @in_product_filter_value1)
	END -- END test to see if we should look it up instead of inserting.
	END	-- END Product Filter work

	-- Check and see if Whse/Org Filter stuff is null
	-- If it's not null, insert that bad beast, and then get the primary key back for the corresponding Rules Filter Set insert to follow
	--Insert into Rules Filter
	IF ((@in_warehouse_filter_DNID is not null) and  (@in_warehouse_filter_value1 is not null) and (@in_warehouse_filter_assoc_DNID is not null))
	BEGIN

	select @insertedOrgFilterID = rules_filter_id
	from synchronizer.rules_filter rf
	where ((@in_warehouse_filter_DNID = DATA_NAME_FK) or (@in_warehouse_filter_DNID is null and DATA_NAME_FK is null))
	and ((@in_warehouse_filter_value1 = VALUE1) or (@in_warehouse_filter_value1 is null and VALUE1 is null))
	and @orgEntityDNID = ENTITY_DATA_NAME_FK
	and @equalsOperatorCRID = OPERATOR_CR_FK

	IF @insertedOrgFilterID is null
	BEGIN
		INSERT INTO synchronizer.RULES_FILTER
		(ENTITY_CLASS_CR_FK, NAME, 
				 DATA_NAME_FK, ENTITY_DATA_NAME_FK, VALUE1
				,OPERATOR_CR_FK, RECORD_STATUS_CR_FK, CREATE_TIMESTAMP, UPDATE_USER)
		SELECT
		@orgEntityClassCRID, ((select label from epacube.data_name where data_name_id = @in_warehouse_filter_DNID ) +': ' + @in_warehouse_filter_value1)
				,@in_warehouse_filter_DNID, @orgEntityDNID, @in_warehouse_filter_value1
				,@equalsOperatorCRID, @activeStatusCRID, getdate(), @in_update_user
	
	-- now, fetch the Rules Filter ID for this freshly inserted entry for use in the Rules Filter Set insert:
		set @insertedOrgFilterID = (select rules_filter_id from synchronizer.RULES_FILTER 
										where name =  ((select label from epacube.data_name where data_name_id = @in_warehouse_filter_DNID) +': ' + @in_warehouse_filter_value1)
										and ENTITY_DATA_NAME_FK = @orgEntityDNID
										and ENTITY_CLASS_CR_FK = @orgEntityClassCRID
										and DATA_NAME_FK = @in_warehouse_filter_DNID
										and VALUE1 = @in_warehouse_filter_value1)
	END -- END check to see if we should look up or insert
	END	-- END Whse/Org Filter work

	-- Check and see if Vendor/Supl Filter stuff is null
	-- If it's not null, insert that bad beast, and then get the primary key back for the corresponding Rules Filter Set insert to follow
	--Insert into Rules Filter
	IF ((@in_vendor_filter_DNID is not null) and  (@in_vendor_filter_value1 is not null) and (@in_vendor_filter_assoc_DNID is not null))
	BEGIN

		select @insertedVendorFilterID = rules_filter_id
		from synchronizer.rules_filter rf
		where ((@in_vendor_filter_DNID = DATA_NAME_FK) or (@in_vendor_filter_DNID is null and DATA_NAME_FK is null))
		and ((@in_vendor_filter_value1 = VALUE1) or (@in_vendor_filter_value1 is null and VALUE1 is null))
		and @vendorEntityDNID = ENTITY_DATA_NAME_FK
		and @equalsOperatorCRID = OPERATOR_CR_FK

		IF @insertedVendorFilterID is null
		BEGIN

		INSERT INTO synchronizer.RULES_FILTER
		(ENTITY_CLASS_CR_FK, NAME, 
				 DATA_NAME_FK, ENTITY_DATA_NAME_FK, VALUE1
				,OPERATOR_CR_FK, RECORD_STATUS_CR_FK, CREATE_TIMESTAMP, UPDATE_USER)
		SELECT
		@vendorEntityClassCRID, ((select label from epacube.data_name where data_name_id = @in_vendor_filter_DNID ) +': ' + @in_vendor_filter_value1)
				,@in_vendor_filter_DNID, @vendorEntityDNID, @in_vendor_filter_value1
				,@equalsOperatorCRID, @activeStatusCRID, getdate(), @in_update_user
	
	-- now, fetch the Rules Filter ID for this freshly inserted entry for use in the Rules Filter Set insert:
		set @insertedVendorFilterID = (select rules_filter_id from synchronizer.RULES_FILTER 
										where name =  ((select label from epacube.data_name where data_name_id = @in_vendor_filter_DNID) +': ' + @in_vendor_filter_value1)
										and ENTITY_DATA_NAME_FK = @vendorEntityDNID
										and ENTITY_CLASS_CR_FK = @vendorEntityClassCRID
										and DATA_NAME_FK = @in_vendor_filter_DNID
										and VALUE1 = @in_vendor_filter_value1)
	END	-- END Vendor Filter check for update/Insert
	END --End Vendor filter work


	--Now, for the Rules Filter Set insert.

--------------EPA-3585 SN Testing
-- Put the flag for ALL ORGS or ALL PRODUCTS or ALL VENDORS if nothing is there
---------------------------------


	--IF (((@insertedOrgFilterID) is not null) or (@insertedProductFilterID is not null) or (@insertedVendorFilterID is not null))
	--BEGIN
		INSERT INTO synchronizer.RULES_FILTER_SET
		(RULES_FK, PROD_FILTER_FK, ORG_FILTER_FK, SUPL_FILTER_FK, ORG_FILTER_ASSOC_DN_FK, SUPL_FILTER_ASSOC_DN_FK
			,INCL_EXCL, RECORD_STATUS_CR_FK, CREATE_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_USER)
		SELECT
		@insertedRuleID, @insertedProductFilterID, @insertedOrgFilterID, @insertedVendorFilterID, @in_warehouse_filter_assoc_DNID, @in_vendor_filter_assoc_DNID  --@insertedOrgFilterID, @insertedVendorFilterID
			,'INCL', @activeStatusCRID, getdate(), getdate(), @in_update_user
--	END --END insert of Rules filter set

-------------------------------------------------------------------------------------
--   Edits passed Insert Row
-------------------------------------------------------------------------------------
	--insert into rules
    
  END --- IF @in_ui_action_cr_fk = @createUIActionCodeRefID


-------------------------------------------------------------------------------------
--   Perform UI Change Action
-------------------------------------------------------------------------------------
  IF @in_ui_action_cr_fk = @changeUIActionCodeRefID
  BEGIN
	
	--Update the Rules Table
	UPDATE synchronizer.rules
	set  RULES_STRUCTURE_FK = @in_rule_structure_id
		,RULES_SCHEDULE_FK = @in_rule_schedule_id
		,RESULT_DATA_NAME_FK = @in_result_DNID
		,EFFECTIVE_DATE = @in_effective_date
		,END_DATE = @in_end_date
		,RECORD_STATUS_CR_FK = @in_record_status_cr_fk		
		,RULE_PRECEDENCE = @in_rule_precedence
		,UPDATE_TIMESTAMP = getdate()
		,UPDATE_USER = @in_update_user
	where rules_id = @in_rules_id

	------------------------------------
	-- Update the Rules Action
	------------------------------------
	

	update synchronizer.RULES_ACTION
	set  BASIS_CALC_DN_FK = @in_basis_calc_DNID
		,RULES_ACTION_OPERATION1_FK = @in_rule_action_operation1
		,BASIS1_DN_FK = @in_basis1_DNID
		,BASIS1_NUMBER = @in_basis1_number
		,RULES_ACTION_OPERATION2_FK = @in_rule_action_operation2
		,BASIS2_DN_FK = @in_basis2_DNID
		,BASIS2_NUMBER = @in_basis2_number
		,RULES_ACTION_OPERATION3_FK = @in_rule_action_operation3
		,BASIS3_DN_FK = @in_basis3_DNID
		,BASIS3_NUMBER = @in_basis3_number
		,ROUNDING_METHOD_CR_FK = @in_rounding_method_cr_fk
		,ROUNDING_TO_VALUE = @in_rounding_precision
		,ROUNDING_ADDER_VALUE = @in_rounding_adder_value
		,BASIS_USE_CORP_ONLY_IND = @in_basis_use_corp_only_ind
		,UPDATE_TIMESTAMP = getdate()
		,UPDATE_USER = @in_update_user
	where rules_fk = @in_rules_id


	------------------------------------
	--update synchronizer.rules_filter
	------------------------------------
	-- First, get the Rules Filter IDs for each of the three filter types (Product, Warehouse/Org, Vendor/Supplier
	-- from Rules Filter Set

	set @prodFilterID = (select prod_filter_fk from synchronizer.rules_filter_set where rules_fk = @in_rules_id)
	set @vendorFilterID = (select supl_filter_fk from synchronizer.rules_filter_set where rules_fk = @in_rules_id)
	set @orgFilterID = (select org_filter_fk from synchronizer.rules_filter_set where rules_fk = @in_rules_id)

	-----------------------------------
	-- If @prodFilterID is not null, update the corresponding Rule Filter row
	-----------------------------------

	IF (@prodFilterID is not null)
	BEGIN
		update synchronizer.RULES_FILTER
		set  DATA_NAME_FK = @in_product_filter_DNID
			,VALUE1 = @in_product_filter_value1	
			,UPDATE_USER = @in_update_user
		where RULES_FILTER_ID = @prodFilterID
	END

	IF (@vendorFilterID is not null)
	BEGIN
		update synchronizer.RULES_FILTER
		set  DATA_NAME_FK = @in_vendor_filter_DNID
			,VALUE1 = @in_vendor_filter_value1	
			,UPDATE_USER = @in_update_user
		where RULES_FILTER_ID = @vendorFilterID
	END

	IF (@orgFilterID is not null)
	BEGIN
		update synchronizer.RULES_FILTER
		set  DATA_NAME_FK = @in_warehouse_filter_DNID
			,VALUE1 = @in_warehouse_filter_value1	
			,UPDATE_USER = @in_update_user
		where RULES_FILTER_ID = @orgFilterID
	END

	------------------------------------
	--finally, update synchronizer.rules_filter_set
	------------------------------------

	update synchronizer.rules_filter_set
		set  ORG_FILTER_ASSOC_DN_FK = @in_warehouse_filter_assoc_DNID
			,SUPL_FILTER_ASSOC_DN_FK = @in_vendor_filter_assoc_DNID
			,UPDATE_TIMESTAMP = getdate()
			,UPDATE_USER = @in_update_user
	where rules_fk = @in_rules_id

	
-------------------------------------------------------------------------------------
--	
-------------------------------------------------------------------------------------

---Make sure it's not getting changed to something that already exists

			 IF @@rowcount > 0
					GOTO RAISE_ERROR
	
  End -- IF @in_ui_action_cr_fk = @changeUIActionCodeRefID

RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END







