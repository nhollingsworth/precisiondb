﻿










-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform duplicate checking of events
--				Events either rejected or placed as conficts
--          Also verify that previously changed data 
--				has already been exported to Host when 
--				newly imported data is coming from same Host.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- KS        02/15/2010   Initial SQL Version
--						    Rewritten from original logic.. very bad stuff
--							Left original logic commented at end
-- CV        05/17/2010    Removed line of data name = data name from check exported logic
-- KS		 05/24/2010    Made adjustments to Cindy's change mentioned above;  
--								Need to restrict by data name for performance so placed back
--                              Also took this logic out as placed in SYNCHRONIZER.EVENT_POST_VALID_IMPORT
-- KS        06/15/2010   epa-3025 Added Future Event Status to duplicate check
-- CV        03/07/2011   epa -   Remove check if event on HOLD - this was causing issues with future events to 
--							and current events to not be able to approve current events.  Made user reject one in order
--							to get the current event approved.
-- CV        08/31/2011   expanded the temp table newdata and dupl data to 256 
-- CV        05/02/2012   Exclude check of Product Mult type data.
-- CV        05/20/2014   Added Parent structure to the mix

CREATE PROCEDURE [synchronizer].[EVENT_POST_VALID_CONFLICT] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime
DECLARE  @v_check_export_ind  bigint
DECLARE  @v_export_name       varchar (50)
DECLARE  @v_import_name       varchar (50)
DECLARE  @v_Future_eff_days int

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_CONFLICT.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;


SET @v_FUTURE_EFF_DAYS = ( SELECT ISNULL ( value, 1 ) from epacube.epacube_params
									    where name = 'FUTURE_EFF_DAYS') 


------------------------------------------------------------------------------------------
-- TEMPORARY TABLE TO CHECK FOR DUPLICATE EVENTS 
------------------------------------------------------------------------------------------


----drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DUPLICATES') is not null
	   drop table #TS_EVENT_DUPLICATES
	   
	   
	-- create temp table
    CREATE TABLE #TS_EVENT_DUPLICATES(
	TS_EVENT_DUPLICATES_ID bigint IDENTITY(1000,1) NOT NULL,
	EVENT_FK bigint NULL,
	EVENT_BATCH_NO BIGINT NULL,
	EVENT_SOURCE_CR_FK INT NULL,
	DUPLICATE_EVENT_FK BIGINT NULL, 
	DUPLICATE_BATCH_NO BIGINT NULL,
    DUPLICATE_EVENT_SOURCE_CR_FK INT NULL,
	EVENT_STATUS_CR_FK INT NULL,
	EVENT_CONDITION_CR_FK INT NULL,
	CONFLICT_IND SMALLINT NULL,
	NEW_DATA  VARCHAR(max) NULL,
	DUPL_NEW_DATA VARCHAR(max) NULL,
	DRANK SMALLINT NULL
 )



------------------------------------------------------------------------------------------
-- FIRST CHECK FOR OTHER EVENTS THAT ARE FOR THE SAME  DATA_NAME/PRODUCT/ORG/ENITTY
--   Reject duplicate matching events that is outside the batch 
--   If values are different then mark mark matching events as CONFLICT;
--
--   DO NOT INCLUDE: APPROVED, REJECTED, PRE-POST, WHATIF )
--   DO INCLUDE FUTURE DATED EVENTS.... 
------------------------------------------------------------------------------------------

-- Insert duplicate or conflict events 

INSERT INTO #TS_EVENT_DUPLICATES
( EVENT_FK
 ,EVENT_BATCH_NO
 ,DUPLICATE_EVENT_FK
 ,DUPLICATE_EVENT_SOURCE_CR_FK
 ,DUPLICATE_BATCH_NO
 ,EVENT_SOURCE_CR_FK
 ,CONFLICT_IND
 ,NEW_DATA
 ,DUPL_NEW_DATA
 )
SELECT 
  A.EVENT_FK
 ,A.BATCH_NO
 ,A.DUPLICATE_EVENT_FK
 ,A.DUPLICATE_EVENT_SOURCE_CR_FK
 ,A.DUPLICATE_BATCH_NO
 ,A.EVENT_SOURCE_CR_FK
 ,A.CONFLICT_IND
 ,A.NEW_DATA
 ,A.DUPL_NEW_DATA
FROM (
SELECT
  TSED.EVENT_FK
 ,TSED.BATCH_NO
 ,ED.EVENT_ID   AS DUPLICATE_EVENT_FK
 ,ED.EVENT_SOURCE_CR_FK AS DUPLICATE_EVENT_SOURCE_CR_FK
 ,ED.BATCH_NO   AS DUPLICATE_BATCH_NO
 ,TSED.EVENT_SOURCE_CR_FK
 ,CASE WHEN ( RTRIM ( LTRIM ( ED.NEW_DATA ) ) = RTRIM ( LTRIM ( TSED.NEW_DATA ) ) ) 
       THEN 99
       ELSE 1
  END AS CONFLICT_IND
 ,TSED.NEW_DATA
 ,ED.NEW_DATA AS DUPL_NEW_DATA 
-----------------  
FROM #TS_EVENT_DATA TSED
INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK) 
 ON ( ED.DATA_NAME_FK = TSED.DATA_NAME_FK
 AND  ED.PRODUCT_STRUCTURE_FK = TSED.PRODUCT_STRUCTURE_FK
 AND  ED.ORG_ENTITY_STRUCTURE_FK = TSED.ORG_ENTITY_STRUCTURE_FK 
 AND  ISNULL ( ED.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) 
  AND  ISNULL ( ED.PARENT_STRUCTURE_FK, 0 ) = ISNULL ( TSED.PARENT_STRUCTURE_FK, 0 ) 
 AND  ED.EVENT_STATUS_CR_FK IN ( 80, 84, 86, 87, 88 ) --removed 85 hold
 )  
 INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK) -------------------------------JASCO
 ON (DN.DATA_NAME_ID = ed.DATA_NAME_FK)
 INNER JOIN epacube.DATA_SET DS WITH (NOLOCK)
 ON (DN.DATA_SET_FK = DS.DATA_SET_ID
 AND ds.TABLE_NAME <> 'PRODUCT_MULT_TYPE') 
 WHERE TSED.EVENT_FK <> ED.EVENT_ID
 AND   TSED.EVENT_TYPE_CR_FK IN ( 150, 153, 154, 155, 156 )
) A
 
 
------------------------------------------------------------------------------------------------------------------------
---- DELETE OUT NULL DATA EVENTS FOR DATA NAME - 100100520  johnstone
-------------------------------------------------------------------------------------------------------------------------
delete from #ts_event_data where NET_VALUE1_NEW is NULL and data_name_fk = 100100520
--------------------------------------------------------------------------------------------------------------------------


 

INSERT INTO #TS_EVENT_DUPLICATES
( EVENT_FK
 ,EVENT_BATCH_NO
 ,DUPLICATE_EVENT_FK
,DUPLICATE_EVENT_SOURCE_CR_FK
 ,DUPLICATE_BATCH_NO
 ,EVENT_SOURCE_CR_FK
 ,CONFLICT_IND
 )
SELECT 
  A.EVENT_FK
 ,A.BATCH_NO
 ,A.DUPLICATE_EVENT_FK
 ,A.DUPLICATE_EVENT_SOURCE_CR_FK
 ,A.DUPLICATE_BATCH_NO
 ,A.EVENT_SOURCE_CR_FK
 ,A.CONFLICT_IND
FROM (
SELECT
  TSED.EVENT_FK
 ,TSED.BATCH_NO
 ,ED.EVENT_ID   AS DUPLICATE_EVENT_FK
 ,ED.EVENT_SOURCE_CR_FK AS DUPLICATE_EVENT_SOURCE_CR_FK
 ,ED.BATCH_NO   AS DUPLICATE_BATCH_NO
 ,TSED.EVENT_SOURCE_CR_FK 
 ,CASE WHEN  (   ISNULL ( ED.NET_VALUE1_NEW, -99999 ) = ISNULL ( TSED.NET_VALUE1_NEW, -99999 )
			 AND ISNULL ( ED.NET_VALUE1_NEW, -99999 ) = ISNULL ( TSED.NET_VALUE1_NEW, -99999 )
			 AND ISNULL ( ED.NET_VALUE2_NEW, -99999 ) = ISNULL ( TSED.NET_VALUE2_NEW, -99999 )
			 AND ISNULL ( ED.NET_VALUE3_NEW, -99999 ) = ISNULL ( TSED.NET_VALUE3_NEW, -99999 )
			 AND ISNULL ( ED.NET_VALUE4_NEW, -99999 ) = ISNULL ( TSED.NET_VALUE4_NEW, -99999 )
			 AND ISNULL ( ED.NET_VALUE5_NEW, -99999 ) = ISNULL ( TSED.NET_VALUE5_NEW, -99999 )
			 AND ISNULL ( ED.NET_VALUE6_NEW, -99999 ) = ISNULL ( TSED.NET_VALUE6_NEW, -99999 ))
			
  THEN 99
  ELSE 1
  END AS CONFLICT_IND
FROM #TS_EVENT_DATA TSED
INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK) 
 ON ( ED.DATA_NAME_FK = TSED.DATA_NAME_FK
 AND  ED.PRODUCT_STRUCTURE_FK = TSED.PRODUCT_STRUCTURE_FK
 AND  ED.ORG_ENTITY_STRUCTURE_FK = TSED.ORG_ENTITY_STRUCTURE_FK 
 AND  ISNULL ( ED.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) 
 AND  ED.EVENT_STATUS_CR_FK IN ( 80, 84, 86, 87, 88 )  )  --removed 85 hold
 WHERE TSED.EVENT_FK <> ED.EVENT_ID
 AND   TSED.EVENT_TYPE_CR_FK IN ( 151, 152 )
) A 



-------- SET @v_count = ( SELECT COUNT(1) FROM #TS_EVENT_DUPLICATES WHERE CONFLICT_IND = 99 )
--------SET @ls_stmt = 'Total number DUPLICATES:  '  
--------                 + CAST ( @v_count AS VARCHAR(16) )
--------EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
--------						   				  @exec_id   = @l_exec_no OUTPUT;


-------- SET @v_count = ( SELECT COUNT(1) FROM #TS_EVENT_DUPLICATES WHERE CONFLICT_IND = 1 )
--------SET @ls_stmt = 'Total number CONFLICTS:  '  
--------                 + CAST ( @v_count AS VARCHAR(16) )
--------EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
--------						   				  @exec_id   = @l_exec_no OUTPUT;
 
 
--------SET @v_count = ( SELECT COUNT(1) FROM #TS_EVENT_DUPLICATES )
--------SET @ls_stmt = 'Total number #TS_EVENT_DUPLICATES:  '  
--------                 + CAST ( @v_count AS VARCHAR(16) )
--------EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
--------						   				  @exec_id   = @l_exec_no OUTPUT;



--------------------------------------------------------------------------------------------
--  CREATE INDEXES 
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSDUP_TSDUPD ON #TS_EVENT_DUPLICATES
	(TS_EVENT_DUPLICATES_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSDUP_EF_TSDUPID_ICOMPARE ON #TS_EVENT_DUPLICATES
	(EVENT_FK ASC)
    INCLUDE ( TS_EVENT_DUPLICATES_ID, DUPLICATE_EVENT_FK, CONFLICT_IND, EVENT_BATCH_NO, DUPLICATE_BATCH_NO  )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSDUP_EEF_TSDUPID_ICOMPARE ON #TS_EVENT_DUPLICATES
	(DUPLICATE_EVENT_FK ASC )
    INCLUDE ( TS_EVENT_DUPLICATES_ID, EVENT_FK, CONFLICT_IND, EVENT_BATCH_NO, DUPLICATE_BATCH_NO  )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



------------------------------------------------------------------------------------------
-- CONFLICTING EVENTS ARE DUPLICATE EVENTS WITH DIFFERENT VALUES
--		ON ALL EVENTS LOG AND ERROR AND SET STATUS TO HOLD
------------------------------------------------------------------------------------------


--- INSERT CONFLICT ERRORS ON EVENT_DATA    ( OUTSIDE BATCH )
INSERT INTO synchronizer.EVENT_DATA_ERRORS 
		(EVENT_FK
	   ,RULE_FK
	   ,EVENT_CONDITION_CR_FK
	   ,DATA_NAME_FK
	   ,ERROR_NAME
	   ,ERROR_DATA1
	   ,ERROR_DATA2
	   ,UPDATE_TIMESTAMP)
	SELECT 
		 ED.EVENT_ID
		,NULL  --TEDE.RULE_FK
		,98
		,NULL  --TEDE.DATA_NAME_FK
		,'CONFLICT EVENT PRIOR EVENT ON HOLD'
		,TSEDUP.NEW_DATA
		,TSEDUP.DUPL_NEW_DATA
		,@l_sysdate  --TEDE.UPDATE_TIMESTAMP
FROM #TS_EVENT_DUPLICATES TSEDUP
INNER JOIN synchronizer.EVENT_DATA ED 
 ON ( ED.EVENT_ID = TSEDUP.DUPLICATE_EVENT_FK )  --- USE TSEDUP.DUPLICATE_EVENT_FK FOR OUTSIDE EVENT_ID
WHERE ISNULL ( TSEDUP.CONFLICT_IND, 0 ) = 1
AND   TSEDUP.EVENT_BATCH_NO <> TSEDUP.DUPLICATE_BATCH_NO  -- NOT IN SAME BATCH 


--- CONFLICT UPDATE ON EVENT_DATA   --- OUTSIDE THE BATCH
UPDATE synchronizer.EVENT_DATA
 SET EVENT_STATUS_CR_FK = 85
    ,EVENT_CONDITION_CR_FK = 98
WHERE EVENT_ID IN (
		SELECT ED.EVENT_ID
		FROM #TS_EVENT_DUPLICATES TSEDUP
		INNER JOIN synchronizer.EVENT_DATA ED
		 ON ( ED.EVENT_ID = TSEDUP.DUPLICATE_EVENT_FK )  --- USE TSEDUP.DUPLICATE_EVENT_FK FOR OUTSIDE EVENT_ID
		WHERE ISNULL ( TSEDUP.CONFLICT_IND, 0 ) = 1
		AND   TSEDUP.EVENT_BATCH_NO <> TSEDUP.DUPLICATE_BATCH_NO  -- NOT IN SAME BATCH 
       )




--- INSERT CONFLICT ERRORS ON TEMP  ( INSIDE BATCH )
INSERT INTO #TS_EVENT_DATA_ERRORS 
		(EVENT_FK
	   ,RULE_FK
	   ,EVENT_CONDITION_CR_FK
	   ,DATA_NAME_FK
	   ,ERROR_NAME
	   ,ERROR_DATA1
	   ,ERROR_DATA2
	   ,UPDATE_TIMESTAMP)
	SELECT 
		 TSEDUP.EVENT_FK
		,NULL  --TEDE.RULE_FK
		,98
		,NULL  --TEDE.DATA_NAME_FK
		,'CONFLICT EVENT LATEST EVENT ON HOLD'
		,TSEDUP.NEW_DATA
		,TSEDUP.DUPL_NEW_DATA		
		,@l_sysdate  --TEDE.UPDATE_TIMESTAMP
FROM #TS_EVENT_DUPLICATES TSEDUP
INNER JOIN #TS_EVENT_DATA TSED
 ON ( TSED.EVENT_FK = TSEDUP.EVENT_FK )        --- USE TSEDUP.EVENT_FK FOR INSIDE EVENT_ID
WHERE ISNULL ( TSEDUP.CONFLICT_IND, 0 ) = 1


--- CONFLICT UPDATE ON TEMP   -- INSIDE BATCH
UPDATE #TS_EVENT_DATA
 SET EVENT_STATUS_CR_FK = 85
    ,EVENT_CONDITION_CR_FK = 98
WHERE EVENT_FK IN (
		SELECT TSED.EVENT_FK
		FROM #TS_EVENT_DUPLICATES TSEDUP
		INNER JOIN #TS_EVENT_DATA TSED
		 ON ( TSED.EVENT_FK = TSEDUP.EVENT_FK )        --- USE TSEDUP.EVENT_FK FOR INSIDE EVENT_ID
		WHERE ISNULL ( TSEDUP.CONFLICT_IND, 0 ) = 1
        )



-------------------------------------------------------------------------------------------------
--  DUPLICATE EVENTS - SAME EVENTS BUT VALUES ARE DIFFERENT 
--  ACTION IS TO REJECT ALL DUPLICATE EVENTS OUTSIDE OF THE BATCH   
-------------------------------------------------------------------------------------------------


--- INSERT DUPLICATE ERRORS ON EVENT_DATA    ( OUTSIDE BATCH )
INSERT INTO synchronizer.EVENT_DATA_ERRORS 
		(EVENT_FK
	   ,RULE_FK
	   ,EVENT_CONDITION_CR_FK
	   ,DATA_NAME_FK
	   ,ERROR_NAME
	   ,ERROR_DATA1
	   ,ERROR_DATA2
	   ,UPDATE_TIMESTAMP)
	SELECT 
		 ED.EVENT_ID
		,NULL  --TEDE.RULE_FK
		,98
		,NULL  --TEDE.DATA_NAME_FK
		,'DUPLICATE EVENT PRIOR EVENT REJECTED'
		,TSEDUP.NEW_DATA
		,TSEDUP.DUPL_NEW_DATA		
		,@l_sysdate  --TEDE.UPDATE_TIMESTAMP
FROM #TS_EVENT_DUPLICATES TSEDUP
INNER JOIN synchronizer.EVENT_DATA ED
 ON ( ED.EVENT_ID = TSEDUP.DUPLICATE_EVENT_FK )  --- USE TSEDUP.DUPLICATE_EVENT_FK FOR OUTSIDE EVENT_ID
WHERE ISNULL ( TSEDUP.CONFLICT_IND, 0 ) = 99
AND   TSEDUP.EVENT_BATCH_NO <> TSEDUP.DUPLICATE_BATCH_NO  -- NOT IN SAME BATCH 


--- DUPLICATE UPDATE ON EVENT_DATA   --- OUTSIDE THE BATCH
UPDATE synchronizer.EVENT_DATA
 SET  EVENT_STATUS_CR_FK = 82
     ,EVENT_CONDITION_CR_FK = 98
WHERE EVENT_ID IN (
		SELECT ED.EVENT_ID
		FROM #TS_EVENT_DUPLICATES TSEDUP
		INNER JOIN synchronizer.EVENT_DATA ED
		 ON ( ED.EVENT_ID = TSEDUP.DUPLICATE_EVENT_FK )  --- USE TSEDUP.DUPLICATE_EVENT_FK FOR OUTSIDE EVENT_ID
		WHERE ISNULL ( TSEDUP.CONFLICT_IND, 0 ) = 99
		AND   TSEDUP.EVENT_BATCH_NO <> TSEDUP.DUPLICATE_BATCH_NO  -- NOT IN SAME BATCH 
       )



------------------------------------------------------------------------------------------
--  RANK LIKE DUPLICATE EVENTS WITHIN THE BATCH
--	   ONLY LOG ERROR AND CHANGE STATUS FOR EVENTS THAT ARE NOT RANKED 1
------------------------------------------------------------------------------------------



/**********************************************************************************/
--   ******  DENSE RANK  ******  
/**********************************************************************************/

UPDATE #TS_EVENT_DUPLICATES
SET DRANK = 1
WHERE TS_EVENT_DUPLICATES_ID IN (
--
SELECT
A.TS_EVENT_DUPLICATES_ID
FROM (
--
SELECT
TS_EVENT_DUPLICATES_ID,
DENSE_RANK() OVER ( PARTITION BY TSED.DATA_NAME_FK, TSED.PRODUCT_STRUCTURE_FK, 
                                 TSED.ORG_ENTITY_STRUCTURE_FK, TSED.ENTITY_STRUCTURE_FK
                    ORDER BY TSED.EVENT_FK ASC ) AS DRANK
FROM #TS_EVENT_DUPLICATES TSEDUP 
INNER JOIN #TS_EVENT_DATA TSED
 ON ( TSED.EVENT_FK = TSEDUP.EVENT_FK )        --- USE TSEDUP.EVENT_FK FOR INSIDE EVENT_ID
WHERE ISNULL ( TSEDUP.CONFLICT_IND, 0 ) = 99
) A
WHERE A.DRANK = 1
)


--- INSERT DUPLICATE ERRORS ON TEMP  ( INSIDE BATCH )  
INSERT INTO #TS_EVENT_DATA_ERRORS 
		(EVENT_FK
	   ,RULE_FK
	   ,EVENT_CONDITION_CR_FK
	   ,DATA_NAME_FK
	   ,ERROR_NAME
	   ,ERROR_DATA1
	   ,ERROR_DATA2
	   ,UPDATE_TIMESTAMP)
	SELECT 
		 TSEDUP.DUPLICATE_EVENT_FK
		,NULL  --TEDE.RULE_FK
		,98
		,NULL  --TEDE.DATA_NAME_FK
		,'DUPLICATE EVENT LATEST EVENT'
		,TSEDUP.NEW_DATA
		,TSEDUP.DUPL_NEW_DATA		
		,@l_sysdate  --TEDE.UPDATE_TIMESTAMP
FROM #TS_EVENT_DUPLICATES TSEDUP
INNER JOIN #TS_EVENT_DATA TSED
 ON ( TSED.EVENT_FK = TSEDUP.EVENT_FK )        --- USE TSEDUP.EVENT_FK FOR INSIDE EVENT_ID
WHERE ISNULL ( TSEDUP.CONFLICT_IND, 0 ) = 99


--- CONFLICT UPDATE ON TEMP   -- INSIDE BATCH
UPDATE #TS_EVENT_DATA
 SET EVENT_STATUS_CR_FK = 82             ---- REJECT EVENT 
    ,EVENT_CONDITION_CR_FK = 98
WHERE EVENT_FK IN (
		SELECT TSED.EVENT_FK
		FROM #TS_EVENT_DUPLICATES TSEDUP
		INNER JOIN #TS_EVENT_DATA TSED
		 ON ( TSED.EVENT_FK = TSEDUP.EVENT_FK )        --- USE TSEDUP.EVENT_FK FOR INSIDE EVENT_ID
		WHERE ISNULL ( TSEDUP.CONFLICT_IND, 0 ) = 99
		AND   TSEDUP.DRANK <> 1		
        )


--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DUPLICATES') is not null
	   drop table #TS_EVENT_DUPLICATES


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_CONFLICT'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_CONFLICT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END


















