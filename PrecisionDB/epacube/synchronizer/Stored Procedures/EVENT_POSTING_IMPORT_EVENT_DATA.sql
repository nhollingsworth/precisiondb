﻿


-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing for imports.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/28/2010   Initial SQL Version
-- CV        05/29/2011   no change events changed from OR to AND
-- CV        10/01/2013   Add the entity inserts for errors
--							



CREATE PROCEDURE [synchronizer].[EVENT_POSTING_IMPORT_EVENT_DATA] 
        ( @in_batch_no BIGINT, @in_import_job_fk BIGINT, 
          @in_event_type_cr_fk INT,
          @in_table_name VARCHAR(64),
          @in_data_name_fk BIGINT,
          @in_org_entity_structure_fk VARCHAR(16) )


AS
BEGIN

 DECLARE @ls_exec            nvarchar (max)
 DECLARE @ls_exec2           nvarchar (max)
 DECLARE @ls_exec3           nvarchar (max) 
 DECLARE @l_exec_no          bigint
 DECLARE @l_rows_processed   bigint
 DECLARE @l_sysdate          datetime


DECLARE  @v_input_rows       bigint
DECLARE  @v_output_rows      bigint
DECLARE  @v_output_total     bigint
DECLARE  @v_exception_rows   bigint
DECLARE  @v_job_date         DATETIME
DECLARE  @v_count			 BIGINT
DECLARE  @v_entity_class_cr_fk  int
DECLARE  @v_import_package_fk   int;
DECLARE  @v_nopost_history_ind  smallint;

DECLARE @l_stmt_no           bigint
DECLARE @ls_stmt             varchar(4000)
DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_EVENT_DATA.' 
					 + 'Batch: ' + cast(isnull(@in_batch_no, 0) as varchar(30))
					 + 'Import: ' + cast(isnull(@in_import_job_fk, 0) as varchar(30))	
					 + 'Event Type: ' + cast(isnull(@in_event_type_cr_fk, 0) as varchar(30))					 				 
					 + 'Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))					 				 					 					 
					 + 'Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))							 
					 + 'Whse: ' + @in_org_entity_structure_fk 							 					 

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_job_id = @in_import_job_fk,
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_job_id = @in_import_job_fk,
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;


------------------------------------------------------------------------------------------
--  INSERT synchronizer.EVENT_DATA from #TS_EVENT_DATA  
--		ONLY IF EVENT_STATUS_CR_FK IN ( 81, 83 )
------------------------------------------------------------------------------------------

SET @v_nopost_history_ind = ( SELECT ISNULL ( CAST ( EEP.VALUE AS INT ), 0 )
                            FROM epacube.EPACUBE_PARAMS EEP 
                            WHERE APPLICATION_SCOPE_FK = 100
                            AND   NAME = 'INITIAL LOAD NO HISTORY' )

IF  @v_nopost_history_ind = 0
BEGIN


------DELETE FROM #TS_EVENT_DATA
------where new_data = '-999999999'
------and event_action_cr_fk = 51



INSERT INTO [synchronizer].[EVENT_DATA_HISTORY_HEADER]
           ([EVENT_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[CUST_ENTITY_STRUCTURE_FK]
           ,[COMP_ENTITY_STRUCTURE_FK]
           ,[ZONE_ENTITY_STRUCTURE_FK]
           ,[DEPT_ENTITY_STRUCTURE_FK]
           ,[VENDOR_ENTITY_STRUCTURE_FK]
           ,[CO_ENTITY_STRUCTURE_FK]
           ,[PARENT_STRUCTURE_FK]
           ,[CHAIN_ID_DATA_VALUE_FK]
           ,[PROD_DATA_VALUE_FK]
           ,[ASSOC_ENTITY_STRUCTURE_FK]
		   ,IMPORT_JOB_FK
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
   SELECT  TSED.EVENT_FK
           ,TSED.EVENT_EFFECTIVE_DATE
           ,TSED.EPACUBE_ID
           ,TSED.PRODUCT_STRUCTURE_FK
           ,TSED.ENTITY_STRUCTURE_FK
           ,TSED.ORG_ENTITY_STRUCTURE_FK
           ,TSED.CUST_ENTITY_STRUCTURE_FK
           ,TSED.CO_ENTITY_STRUCTURE_FK
           ,TSED.ZONE_ENTITY_STRUCTURE_FK
           ,TSED.DEPT_ENTITY_STRUCTURE_FK
           ,TSED.VENDOR_ENTITY_STRUCTURE_FK
           ,TSED.CO_ENTITY_STRUCTURE_FK
           ,TSED.PARENT_STRUCTURE_FK
           ,TSED.CHAIN_ID_DATA_VALUE_FK
           ,TSED.PROD_DATA_VALUE_FK
           ,TSED.ASSOC_ENTITY_STRUCTURE_FK
		   ,TSED.IMPORT_JOB_FK
           ,TSED.RECORD_STATUS_CR_FK
           ,TSED.UPDATE_TIMESTAMP
           ,TSED.UPDATE_USER
	---------------------------------        	    
FROM #TS_EVENT_DATA  TSED
WHERE 1 = 1
---------------- KS MOVED SAME LOGIC AS WAS IN EVENT_POSTING
---

AND   (    TSED.event_status_cr_fk IN ( 81 )  --- approved 
	OR    (     TSED.event_status_cr_fk = 82   -- rejected
		   AND  TSED.event_source_cr_fk = 79   -- host import
		  )
	AND    (     TSED.event_status_cr_fk = 83
		   AND  TSED.event_status_cr_fk <> 56  --- and not a no change
			  )	
			  AND    (     TSED.event_action_cr_fk = 51
		   AND  TSED.new_data <> '-999999999')			  
	
	  )
	  

	 
INSERT INTO [synchronizer].[EVENT_DATA_HISTORY_DETAILS]
           ([EVENT_DATA_HISTORY_HEADER_FK]
           ,[EVENT_ID]
           ,[EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[ENTITY_CLASS_CR_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[LEVEL_SEQ]
           ,[ASSOC_PARENT_CHILD]
           ,[NEW_DATA]
           ,[CURRENT_DATA]
           ,[NET_VALUE1_NEW]
           ,[NET_VALUE1_CUR]
           ,[PERCENT_CHANGE1]
           ,[MARGIN_PERCENT]
           ,[MARGIN_DOLLARS]
           ,[VALUES_SHEET_NAME]
           ,[VALUES_SHEET_DATE]
           ,[VALUE_UOM_CODE_FK]
           ,[ASSOC_UOM_CODE_FK]
           ,[UOM_CONVERSION_FACTOR]
           ,[DATA_VALUE_FK]
           ,[ENTITY_DATA_VALUE_FK]
           ,[VALUE_EFFECTIVE_DATE]
           ,[EFFECTIVE_DATE_CUR]
           ,[END_DATE_NEW]
           ,[END_DATE_CUR]
           ,[DATA_LEVEL]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[EVENT_REASON_CR_FK]
           ,[RESULT_TYPE_CR_FK]
           --,[CUR_RESULT_TYPE_CR_FK]
           ,[SINGLE_ASSOC_IND]
           --,[AUDIT_FILENAME]
           ,[JOB_CLASS_FK]
           --,[CALC_JOB_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[IMPORT_BATCH_NO]
           ,[BATCH_NO]
           ,[STG_RECORD_FK]
           ,[TABLE_ID_FK]
           ,[RULES_FK]
           --,[PREV_RULES_FK]
           --,[RELATED_EVENT_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
           --,[CUST_DATA_VALUE_FK]
           --,[DEPT_ENTITY_DATA_VALUE_FK]
           --,[RESULT_DATA_VALUE_FK]
           --,[PRECEDENCE]
           --,[ZONE_TYPE_CR_FK]
           --,[SEARCH1]
		   )
		   SELECT 
		   hh.EVENT_DATA_HISTORY_HEADER_ID
           ,hh.EVENT_FK
           ,[EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,tsed.EVENT_EFFECTIVE_DATE
           ,tsed.EPACUBE_ID
           ,[DATA_NAME_FK]
           ,[ENTITY_CLASS_CR_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[LEVEL_SEQ]
           ,[ASSOC_PARENT_CHILD]
           ,[NEW_DATA]
           ,[CURRENT_DATA]
           ,[NET_VALUE1_NEW]
           ,[NET_VALUE1_CUR]
           ,[PERCENT_CHANGE1]
           ,[MARGIN_PERCENT]
           ,[MARGIN_DOLLARS]
           ,[VALUES_SHEET_NAME]
           ,[VALUES_SHEET_DATE]
           ,[VALUE_UOM_CODE_FK]
           ,[ASSOC_UOM_CODE_FK]
           ,[UOM_CONVERSION_FACTOR]
           ,[DATA_VALUE_FK]
           ,[ENTITY_DATA_VALUE_FK]
           ,[VALUE_EFFECTIVE_DATE]
           ,[EFFECTIVE_DATE_CUR]
           ,[END_DATE_NEW]
           ,[END_DATE_CUR]
           ,[DATA_LEVEL]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[EVENT_REASON_CR_FK]
           ,[RESULT_TYPE_CR_FK]
           --,[CUR_RESULT_TYPE_CR_FK]
           ,[SINGLE_ASSOC_IND]
           --,[AUDIT_FILENAME]
           ,[JOB_CLASS_FK]
           --,[CALC_JOB_FK]
           ,tsed.IMPORT_JOB_FK
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[IMPORT_BATCH_NO]
           ,[BATCH_NO]
           ,[STG_RECORD_FK]
           ,[TABLE_ID_FK]
           ,[RULES_FK]
           --,[PREV_RULES_FK]
           --,[RELATED_EVENT_FK]
           ,tsed.RECORD_STATUS_CR_FK
           ,tsed.UPDATE_TIMESTAMP
           ,tsed.UPDATE_USER
           --,[CUST_DATA_VALUE_FK]
           --,[DEPT_ENTITY_DATA_VALUE_FK]
           --,[RESULT_DATA_VALUE_FK]
           --,[PRECEDENCE]
           --,[ZONE_TYPE_CR_FK]
           --,[SEARCH1]
		   	---------------------------------        	    
FROM #TS_EVENT_DATA  TSED
 inner join synchronizer.event_data_history_header hh on hh.event_fk = tsed.event_fk
	 and tsed.event_fk = hh.event_fk
WHERE 1 = 1
---------------- KS MOVED SAME LOGIC AS WAS IN EVENT_POSTING
---

AND   (    TSED.event_status_cr_fk IN ( 81 )  --- approved 
	OR    (     TSED.event_status_cr_fk = 82   -- rejected
		   AND  TSED.event_source_cr_fk = 79   -- host import
		  )
	AND    (     TSED.event_status_cr_fk = 83
		   AND  TSED.event_status_cr_fk <> 56  --- and not a no change
			  )	
			  AND    (     TSED.event_action_cr_fk = 51
		   AND  TSED.new_data <> '-999999999')			  
	
	  )
	  

	  





----INSERT INTO synchronizer.EVENT_DATA_HISTORY (
----		 EVENT_TYPE_CR_FK
----		,EVENT_ENTITY_CLASS_CR_FK 
----		,EPACUBE_ID    
----		,IMPORT_JOB_FK     
----		,IMPORT_PACKAGE_FK 
----		,DATA_NAME_FK	
----	    ,PRODUCT_STRUCTURE_FK
----	    ,ORG_ENTITY_STRUCTURE_FK
----	    ,ENTITY_STRUCTURE_FK    
----		,ENTITY_CLASS_CR_FK     
----		,ENTITY_DATA_NAME_FK    
----        ,NEW_DATA         
----        ,CURRENT_DATA     
----		,DATA_VALUE_FK    
----		,ENTITY_DATA_VALUE_FK
----        ,NET_VALUE1_NEW      
----        ,NET_VALUE2_NEW      
----        ,NET_VALUE3_NEW      
----        ,NET_VALUE4_NEW      
----        ,NET_VALUE5_NEW      
----        ,NET_VALUE6_NEW         
----        ,NET_VALUE1_CUR      
----        ,NET_VALUE2_CUR      
----        ,NET_VALUE3_CUR     
----        ,NET_VALUE4_CUR     
----        ,NET_VALUE5_CUR     
----        ,NET_VALUE6_CUR         
----		,PERCENT_CHANGE1	 
----		,PERCENT_CHANGE2	
----		,PERCENT_CHANGE3	
----		,PERCENT_CHANGE4	
----		,PERCENT_CHANGE5	
----		,PERCENT_CHANGE6	
----        ,VALUE_UOM_CODE_FK 
----        ,ASSOC_UOM_CODE_FK 
----        ,VALUES_SHEET_NAME
----		,UOM_CONVERSION_FACTOR 
----		,END_DATE_NEW          
----        ,END_DATE_CUR          
----		,EFFECTIVE_DATE_CUR    
----		,EVENT_EFFECTIVE_DATE 
----		,VALUE_EFFECTIVE_DATE
----        ,RELATED_EVENT_FK	
----        ,RULES_FK			
----		,EVENT_ACTION_CR_FK   
----		,EVENT_STATUS_CR_FK   
----		,EVENT_CONDITION_CR_FK
----		,EVENT_PRIORITY_CR_FK   
----		,EVENT_SOURCE_CR_FK   
----		,RESULT_TYPE_CR_FK	 
----		,SINGLE_ASSOC_IND     
----		,TABLE_ID_FK          
----		,BATCH_NO			
----		,IMPORT_BATCH_NO     
----        ,JOB_CLASS_FK		
----        ,IMPORT_DATE         
----        ,IMPORT_FILENAME     
----        ,STG_RECORD_FK       
----		,RECORD_STATUS_CR_FK 
----        ,UPDATE_TIMESTAMP	
----        ,UPDATE_USER		    
----     )
----SELECT 
----		 EVENT_TYPE_CR_FK
----		,EVENT_ENTITY_CLASS_CR_FK 
----		,EPACUBE_ID    
----		,IMPORT_JOB_FK     
----		,IMPORT_PACKAGE_FK 
----		,DATA_NAME_FK	
----	    ,PRODUCT_STRUCTURE_FK
----	    ,ORG_ENTITY_STRUCTURE_FK
----	    ,ENTITY_STRUCTURE_FK    
----		,ENTITY_CLASS_CR_FK     
----		,ENTITY_DATA_NAME_FK    
----        ,NEW_DATA         
----        ,CURRENT_DATA     
----		,DATA_VALUE_FK    
----		,ENTITY_DATA_VALUE_FK
----        ,NET_VALUE1_NEW      
----        ,NET_VALUE2_NEW      
----        ,NET_VALUE3_NEW      
----        ,NET_VALUE4_NEW      
----        ,NET_VALUE5_NEW      
----        ,NET_VALUE6_NEW           
----        ,NET_VALUE1_CUR      
----        ,NET_VALUE2_CUR      
----        ,NET_VALUE3_CUR     
----        ,NET_VALUE4_CUR     
----        ,NET_VALUE5_CUR     
----        ,NET_VALUE6_CUR        
----		,PERCENT_CHANGE1	 
----		,PERCENT_CHANGE2	
----		,PERCENT_CHANGE3	
----		,PERCENT_CHANGE4	
----		,PERCENT_CHANGE5	
----		,PERCENT_CHANGE6	
----        ,VALUE_UOM_CODE_FK 
----        ,ASSOC_UOM_CODE_FK 
----        ,VALUES_SHEET_NAME
----		,UOM_CONVERSION_FACTOR 
----		,END_DATE_NEW          
----        ,END_DATE_CUR          
----		,EFFECTIVE_DATE_CUR    
----		,ISNULL ( EVENT_EFFECTIVE_DATE, GETDATE() )
----		,VALUE_EFFECTIVE_DATE
----        ,RELATED_EVENT_FK	
----        ,RULES_FK			
----		,EVENT_ACTION_CR_FK   
----		,EVENT_STATUS_CR_FK   
----		,EVENT_CONDITION_CR_FK
----		,EVENT_PRIORITY_CR_FK   
----		,EVENT_SOURCE_CR_FK   
----		,RESULT_TYPE_CR_FK	 
----		,SINGLE_ASSOC_IND     
----		,TABLE_ID_FK          
----		,BATCH_NO			
----		,IMPORT_BATCH_NO     
----        ,JOB_CLASS_FK		
----        ,IMPORT_DATE         
----        ,IMPORT_FILENAME     
----        ,STG_RECORD_FK       
----		,RECORD_STATUS_CR_FK 
----        ,UPDATE_TIMESTAMP	
----        ,UPDATE_USER	
---------------------------------        	    
----FROM #TS_EVENT_DATA  TSED
----WHERE 1 = 1
-------------------- KS MOVED SAME LOGIC AS WAS IN EVENT_POSTING
-------

----AND   (    TSED.event_status_cr_fk IN ( 81 )  --- approved 
----	OR    (     TSED.event_status_cr_fk = 82   -- rejected
----		   AND  TSED.event_source_cr_fk = 79   -- host import
----		  )
----	AND    (     TSED.event_status_cr_fk = 83
----		   AND  TSED.event_status_cr_fk <> 56  --- and not a no change
----			  )	
----			  AND    (     TSED.event_action_cr_fk = 51
----		   AND  TSED.new_data <> '-999999999')			  
	
----	  )
	  
	  
	  
  
	


       SET @l_rows_processed = @@ROWCOUNT

	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows Logged To History = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
       END

END  --- NO POST HISTORY


------------------------------------------------------------------------------------------
--  INSERT synchronizer.EVENT_DATA from #TS_EVENT_DATA  
--		ONLY IF EVENT_STATUS_CR_FK NOT IN ( 81, 83 )
------------------------------------------------------------------------------------------


INSERT INTO synchronizer.EVENT_DATA (
		 EVENT_TYPE_CR_FK
		,EVENT_ENTITY_CLASS_CR_FK 
		,EPACUBE_ID    
		,IMPORT_JOB_FK     
		,IMPORT_PACKAGE_FK 
		,DATA_NAME_FK	
	    ,PRODUCT_STRUCTURE_FK
	    ,ORG_ENTITY_STRUCTURE_FK
	    ,ENTITY_STRUCTURE_FK  
		,PARENT_STRUCTURE_FK 
		,ASSOC_PARENT_CHILD 
		,ENTITY_CLASS_CR_FK     
		,ENTITY_DATA_NAME_FK    
        ,NEW_DATA         
        ,CURRENT_DATA     
		,DATA_VALUE_FK    
		,ENTITY_DATA_VALUE_FK
		,DATA_LEVEL
        ,NET_VALUE1_NEW      
        ,NET_VALUE2_NEW      
        ,NET_VALUE3_NEW      
        ,NET_VALUE4_NEW      
        ,NET_VALUE5_NEW      
        ,NET_VALUE6_NEW        
        ,NET_VALUE1_CUR      
        ,NET_VALUE2_CUR      
        ,NET_VALUE3_CUR     
        ,NET_VALUE4_CUR     
        ,NET_VALUE5_CUR     
        ,NET_VALUE6_CUR         
		,PERCENT_CHANGE1	 
		,PERCENT_CHANGE2	
		,PERCENT_CHANGE3	
		,PERCENT_CHANGE4	
		,PERCENT_CHANGE5	
		,PERCENT_CHANGE6	
        ,VALUE_UOM_CODE_FK 
        ,ASSOC_UOM_CODE_FK 
        ,VALUES_SHEET_NAME
		,UOM_CONVERSION_FACTOR 
		,END_DATE_NEW          
        ,END_DATE_CUR          
		,EFFECTIVE_DATE_CUR    
		,EVENT_EFFECTIVE_DATE 
		,VALUE_EFFECTIVE_DATE
        ,RELATED_EVENT_FK	
        ,RULES_FK			
		,EVENT_ACTION_CR_FK   
		,EVENT_STATUS_CR_FK   
		,EVENT_CONDITION_CR_FK
		,EVENT_PRIORITY_CR_FK   
		,EVENT_SOURCE_CR_FK   
		,RESULT_TYPE_CR_FK	 
		,SINGLE_ASSOC_IND     
		,TABLE_ID_FK          
		,BATCH_NO			
		,IMPORT_BATCH_NO     
        ,JOB_CLASS_FK		
        ,IMPORT_DATE         
        ,IMPORT_FILENAME     
        ,STG_RECORD_FK       
		,RECORD_STATUS_CR_FK 
        ,UPDATE_TIMESTAMP	
        ,UPDATE_USER
		,CO_ENTITY_STRUCTURE_FK
		,PACK		    
     )
SELECT 
		 EVENT_TYPE_CR_FK
		,EVENT_ENTITY_CLASS_CR_FK 
		,EPACUBE_ID    
		,IMPORT_JOB_FK     
		,IMPORT_PACKAGE_FK 
		,DATA_NAME_FK	
	    ,PRODUCT_STRUCTURE_FK
	    ,ORG_ENTITY_STRUCTURE_FK
	    ,ENTITY_STRUCTURE_FK   
		,PARENT_ENTITY_STRUCTURE_FK
		,Child_entity_structure_fk 
		,ENTITY_CLASS_CR_FK     
		,ENTITY_DATA_NAME_FK    
        ,NEW_DATA         
        ,CURRENT_DATA     
		,DATA_VALUE_FK    
		,ENTITY_DATA_VALUE_FK
		,DATA_LEVEL
        ,NET_VALUE1_NEW      
        ,NET_VALUE2_NEW      
        ,NET_VALUE3_NEW      
        ,NET_VALUE4_NEW      
        ,NET_VALUE5_NEW      
        ,NET_VALUE6_NEW          
        ,NET_VALUE1_CUR      
        ,NET_VALUE2_CUR      
        ,NET_VALUE3_CUR     
        ,NET_VALUE4_CUR     
        ,NET_VALUE5_CUR     
        ,NET_VALUE6_CUR        
		,PERCENT_CHANGE1	 
		,PERCENT_CHANGE2	
		,PERCENT_CHANGE3	
		,PERCENT_CHANGE4	
		,PERCENT_CHANGE5	
		,PERCENT_CHANGE6	
        ,VALUE_UOM_CODE_FK 
        ,ASSOC_UOM_CODE_FK 
        ,VALUES_SHEET_NAME
		,UOM_CONVERSION_FACTOR 
		,END_DATE_NEW          
        ,END_DATE_CUR          
		,EFFECTIVE_DATE_CUR    
		,ISNULL ( EVENT_EFFECTIVE_DATE, GETDATE () )
		,VALUE_EFFECTIVE_DATE
        ,TSED.EVENT_FK	            --- RELATED EVENT_FK
        ,RULES_FK			
		,EVENT_ACTION_CR_FK   
		,EVENT_STATUS_CR_FK   
		,EVENT_CONDITION_CR_FK
		,EVENT_PRIORITY_CR_FK   
		,EVENT_SOURCE_CR_FK   
		,RESULT_TYPE_CR_FK	 
		,SINGLE_ASSOC_IND     
		,TABLE_ID_FK          
		,BATCH_NO			
		,IMPORT_BATCH_NO     
        ,JOB_CLASS_FK		
        ,IMPORT_DATE         
        ,IMPORT_FILENAME     
        ,STG_RECORD_FK       
		,RECORD_STATUS_CR_FK 
        ,UPDATE_TIMESTAMP	
        ,UPDATE_USER
		,CO_ENTITY_STRUCTURE_FK
		,PACK			    
-----------------------------
FROM #TS_EVENT_DATA  TSED
WHERE 1 = 1
AND   TSED.EVENT_STATUS_CR_FK NOT IN ( 81, 83 )   --- ALLOW CREATION OF REJECTED FOR VISIBILITY 


       SET @l_rows_processed = @@ROWCOUNT

	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows Pending Events = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
       END


------------------------------------------------------------------------------------------
--   INSERT INTO synchronizer.EVENT_DATA_ERRORS 
------------------------------------------------------------------------------------------


		INSERT INTO synchronizer.EVENT_DATA_ERRORS 
				(EVENT_FK
			   ,RULE_FK
			   ,EVENT_CONDITION_CR_FK
			   ,DATA_NAME_FK
			   ,ERROR_NAME
			   ,ERROR_DATA1
			   ,ERROR_DATA2
			   ,ERROR_DATA3
			   ,ERROR_DATA4
			   ,ERROR_DATA5
			   ,ERROR_DATA6
			   ,UPDATE_TIMESTAMP)
		   SELECT 
				 ED.EVENT_ID
				,TEDE.RULE_FK
				,TEDE.EVENT_CONDITION_CR_FK
				,TEDE.DATA_NAME_FK
				,TEDE.ERROR_NAME
				,TEDE.ERROR_DATA1
				,TEDE.ERROR_DATA2
				,TEDE.ERROR_DATA3
				,TEDE.ERROR_DATA4
				,TEDE.ERROR_DATA5
				,TEDE.ERROR_DATA6
				,TEDE.UPDATE_TIMESTAMP
			FROM #TS_EVENT_DATA_ERRORS TEDE
			INNER JOIN #TS_EVENT_DATA  TSED
			  ON ( TEDE.EVENT_FK = TSED.EVENT_FK )
			INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
			  ON ( ED.IMPORT_JOB_FK = TSED.IMPORT_JOB_FK
			  AND  ED.DATA_NAME_FK     = TSED.DATA_NAME_FK 
	          AND  ED.PRODUCT_STRUCTURE_FK = TSED.PRODUCT_STRUCTURE_FK
	          AND  ED.ORG_ENTITY_STRUCTURE_FK = TSED.ORG_ENTITY_STRUCTURE_FK
	          AND  ISNULL ( ED.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, -999 )
              )
-----------------------------
WHERE 1 = 1
AND   TSED.EVENT_STATUS_CR_FK NOT IN ( 81, 83 )
------ KS ADDED BELOW FOR SPEEDY.. DO NOT CREATE NO CHANGE EVENTS PER JAN EPA-3022
AND   TSED.EVENT_ACTION_CR_FK NOT IN ( 56 ) --- NO CHANGE

----------------------------------------------------------------
---added for new entity association events

INSERT INTO synchronizer.EVENT_DATA_ERRORS 
				(EVENT_FK
			   ,RULE_FK
			   ,EVENT_CONDITION_CR_FK
			   ,DATA_NAME_FK
			   ,ERROR_NAME
			   ,ERROR_DATA1
			   ,ERROR_DATA2
			   ,ERROR_DATA3
			   ,ERROR_DATA4
			   ,ERROR_DATA5
			   ,ERROR_DATA6
			   ,UPDATE_TIMESTAMP)
		   SELECT 
				 ED.EVENT_ID
				,TEDE.RULE_FK
				,TEDE.EVENT_CONDITION_CR_FK
				,TEDE.DATA_NAME_FK
				,TEDE.ERROR_NAME
				,TEDE.ERROR_DATA1
				,TEDE.ERROR_DATA2
				,TEDE.ERROR_DATA3
				,TEDE.ERROR_DATA4
				,TEDE.ERROR_DATA5
				,TEDE.ERROR_DATA6
				,TEDE.UPDATE_TIMESTAMP
			FROM #TS_EVENT_DATA_ERRORS TEDE
			INNER JOIN #TS_EVENT_DATA  TSED
			  ON ( TEDE.EVENT_FK = TSED.EVENT_FK )
			  INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
			  ON ( ED.IMPORT_JOB_FK = TSED.IMPORT_JOB_FK
			  AND  ED.DATA_NAME_FK     = TSED.DATA_NAME_FK 
	          AND  ED.ENTITY_STRUCTURE_FK = TSED.ENTITY_STRUCTURE_FK
	          AND  ED.ORG_ENTITY_STRUCTURE_FK = TSED.ORG_ENTITY_STRUCTURE_FK
	                       )
-----------------------------
WHERE 1 = 1
AND   TSED.EVENT_STATUS_CR_FK NOT IN ( 81, 83 )
------ KS ADDED BELOW FOR SPEEDY.. DO NOT CREATE NO CHANGE EVENTS PER JAN EPA-3022
AND   TSED.EVENT_ACTION_CR_FK NOT IN ( 56 ) --- NO CHANGE
AND   TSED.event_type_cr_Fk in ( 160, 154)




---------------------------------------------------------------------------------------------

       SET @l_rows_processed = @@ROWCOUNT

	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows Logged Errors = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
       END


------------------------------------------------------------------------------------------
--   INSERT INTO synchronizer.EVENT_DATA_RULES 
-----------------------------------------------------------------------------------------

truncate table synchronizer.event_data_rules -----cindy here

			INSERT INTO [synchronizer].[EVENT_DATA_RULES]
			 (   EVENT_FK      		
				,RULES_FK      
				,RESULT_DATA_NAME_FK      		
				,RESULT_TYPE_CR_FK      	
				,RESULT_EFFECTIVE_DATE      	
				,RULES_EFFECTIVE_DATE 
				,RULES_ACTION_OPERATION_FK     
				,RULE_PRECEDENCE
				,SCHED_PRECEDENCE				
				 ) 			
				SELECT DISTINCT 
				 ED.EVENT_ID
				,TEDR.RULES_FK      
				,TEDR.RESULT_DATA_NAME_FK      		
				,TEDR.RESULT_TYPE_CR_FK      	
				,TEDR.RESULT_EFFECTIVE_DATE      	
				,TEDR.RULES_EFFECTIVE_DATE  
				,TEDR.RULES_ACTION_OPERATION_FK    
				,TEDR.RULE_PRECEDENCE
				,TEDR.SCHED_PRECEDENCE			
			FROM #TS_EVENT_DATA_RULES TEDR
			INNER JOIN #TS_EVENT_DATA  TSED
			  ON ( TEDR.EVENT_FK = TSED.EVENT_FK )
			INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
			  ON ( ED.IMPORT_JOB_FK = TSED.IMPORT_JOB_FK
			  AND  ED.DATA_NAME_FK     = TSED.DATA_NAME_FK 
	          AND  ED.PRODUCT_STRUCTURE_FK = TSED.PRODUCT_STRUCTURE_FK
	          AND  ED.ORG_ENTITY_STRUCTURE_FK = TSED.ORG_ENTITY_STRUCTURE_FK
	          AND  ISNULL ( ED.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, -999 )
              )

-----------------------------
WHERE 1 = 1
AND   TSED.EVENT_STATUS_CR_FK NOT IN ( 81, 83 )


       SET @l_rows_processed = @@ROWCOUNT

	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows Logged Rules = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
       END


------------------------------------------------------------------------------------------
---  CLEAR TABLE FOR NEXT BATCH_NO, PRIORITY, JOB_FK, EVENT_TYPE
------------------------------------------------------------------------------------------


   TRUNCATE TABLE #TS_EVENT_DATA
   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
   TRUNCATE TABLE #TS_EVENT_DATA_RULES      


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_EVENT_DATA'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_EVENT_DATA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

  EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END




















