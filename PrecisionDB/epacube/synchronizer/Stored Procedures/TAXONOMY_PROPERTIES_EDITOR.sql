﻿








-- Copyright 2008
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to add, activate, and inactivate Taxonomy Tree Properties.
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG       02/05/2008  Initial SQL Version
--
CREATE PROCEDURE [synchronizer].[TAXONOMY_PROPERTIES_EDITOR] 
( @in_taxonomy_properties_id int,  
  @in_taxonomy_tree_fk int,  
  @in_name varchar(64),   
  @in_ui_action_cr_fk int)
												
AS

/***************  Parameter Defintions  **********************
CURRENTLY USED IN THE DATA MASTER EDITOR....

Simple for Now.... Only CREATE (8003) and CHANGE (8006); DELETE (8004) to follow

***************************************************************/

BEGIN

DECLARE @v_product_structure_fk bigint
DECLARE @v_taxonomy_properties_fk int
DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int


SET NOCOUNT ON;
set @ErrorMessage = null

IF @in_ui_action_cr_fk in (8003)
BEGIN
	 IF   @in_name IS NULL 
     BEGIN
		SET @ErrorMessage = 'Name must be entered'
        GOTO RAISE_ERROR
	 END
	 IF   @in_taxonomy_tree_fk IS NULL 
     BEGIN
		SET @ErrorMessage = 'Tree must be selected'
        GOTO RAISE_ERROR
	 END
END
--
IF @in_ui_action_cr_fk = 8003 --create
BEGIN

-------------------------------------------------------------------------
--   Check for Duplicate Prior to Insert
-------------------------------------------------------------------------------------
	SELECT @ErrorMessage = 'Duplicate Taxonomy Tree Property. '
        + 'Name already exists for taxonomy_properties_id on that node'
        + CAST(isnull(taxonomy_properties_id,0) AS VARCHAR(30))
    FROM epacube.taxonomy_properties
    WHERE taxonomy_tree_fk = @in_taxonomy_tree_fk
    and   upper(name) = upper(@in_name)

	 IF @@rowcount > 0
			GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
--   Edits passed Insert Row
-------------------------------------------------------------------------------------

	INSERT INTO EPACUBE.TAXONOMY_PROPERTIES
          ( taxonomy_tree_fk
			,name
			,record_status_cr_fk			
			,create_timestamp
			,update_timestamp)
	SELECT            
              @in_taxonomy_tree_fk,
			  upper(@in_name),
			  1,				  
              getdate(),
			  getdate()

END --- IF @in_ui_action_cr_fk = 8003


-------------------------------------------------------------------------------------
--   Peform UI Activate Action 
-------------------------------------------------------------------------------------
IF @in_ui_action_cr_fk = 8007 --Activate
BEGIN
	UPDATE EPACUBE.TAXONOMY_PROPERTIES
		set	record_status_cr_fk = 1
		   ,update_timestamp = getdate()
		where taxonomy_properties_id = @in_taxonomy_properties_id
END -- IF @in_ui_action_cr_fk = 8007


-------------------------------------------------------------------------------------
--   Peform UI Inactivate Action 
-------------------------------------------------------------------------------------

IF @in_ui_action_cr_fk = 8005  -- activate
BEGIN
	UPDATE EPACUBE.TAXONOMY_PROPERTIES
		set	record_status_cr_fk = 2
	   ,update_timestamp = getdate()
		where taxonomy_properties_id = @in_taxonomy_properties_id
END -- IF @in_ui_action_cr_fk = 8005

RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END






























































