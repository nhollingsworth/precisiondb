﻿











-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform USED DEFINED RULE event validations to all event types
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        10/19/2008   Initial SQL Version
--

CREATE PROCEDURE [synchronizer].[EVENT_POST_VALID_TYPE_156] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_156.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;




------------------------------------------------------------------------------------------
--  INSERT MISSING UOMS
------------------------------------------------------------------------------------------


INSERT INTO [epacube].[UOM_CODE]
           ([UOM_CODE]
           ,[UOM_DESCRIPTION]
           ,[LOWER_QTY]
           ,[LOWER_UOM_CODE]
           ,[PRIMARY_QTY]
           )
    SELECT  a.UOM_CODE, a.UOM_CODE, 1, 'E', 1  -- LOWER IS PRIMARY  --- hardcoded to E ???
      FROM 
          ( 
            SELECT DISTINCT T_1.UOM_CODE
              FROM 
                  ( 
                    SELECT tsed.NEW_DATA AS UOM_CODE
                      FROM #TS_EVENT_DATA tsed  WITH (NOLOCK)
					  WHERE tsed.VALUE_UOM_CODE_FK IS NULL
                      AND   tsed.NEW_DATA <> 'NULL'
					  AND   tsed.DATA_TYPE_CR_FK = 139
                      AND   tsed.INSERT_MISSING_VALUES = 1
                  ) T_1
          ) a
      WHERE  NOT( EXISTS
                  ( 
                    SELECT 1
                      FROM epacube.UOM_CODE UC WITH (NOLOCK)
                      WHERE (UC.UOM_CODE = a.UOM_CODE)
                  ))


      UPDATE #TS_EVENT_DATA
        SET VALUE_UOM_CODE_FK =   
          (
            SELECT UC.UOM_CODE_ID
              FROM epacube.UOM_CODE UC  WITH (NOLOCK)
			  WHERE UC.UOM_CODE        = #TS_EVENT_DATA.NEW_DATA
          )
           ,ASSOC_UOM_CODE_FK = 
          (
            SELECT UC.UOM_CODE_ID
              FROM epacube.UOM_CODE UC  WITH (NOLOCK)
			  WHERE UC.UOM_CODE        = #TS_EVENT_DATA.CURRENT_DATA
          )
           ,UOM_CONVERSION_FACTOR =  ISNULL (
			( SELECT  ISNULL ( 
               ( SELECT UC.PRIMARY_QTY
					  FROM epacube.UOM_CODE UC  WITH (NOLOCK)
					  WHERE UC.UOM_CODE        = #TS_EVENT_DATA.NEW_DATA
				      ), 1 )
			   /
              ( SELECT ISNULL (
					( SELECT UC.PRIMARY_QTY
					  FROM epacube.UOM_CODE UC  WITH (NOLOCK)
					  WHERE UC.UOM_CODE        = ISNULL ( #TS_EVENT_DATA.CURRENT_DATA, #TS_EVENT_DATA.NEW_DATA )
				      ), 1 )			   			   
			   )    )
			   ,1  )
        WHERE EVENT_TYPE_CR_FK = 156

        

------------------------------------------------------------------------------------------
--   Insert Errors into Event Error Tables 
------------------------------------------------------------------------------------------

							INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,99
								,'MISSING UOM VALUE'  --ERROR NAME   
								,'UOM CODE WAS NOT INSERTED'--ERROR1
								,'CHECK INSERT MISSING FLAG FOR DATA NAME'
								,NULL
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								WHERE data_type_cr_fk = 139
								AND   tsed.NEW_DATA <> 'NULL'
								AND   VALUE_UOM_CODE_FK is NULL )

-----------------------------------------------------------------------------------------------
--Check if value is active vs. inactive

							INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,99
								,'UOM VALUE INACTIVE'  --ERROR NAME     
								,'UOM CODE IS INACTIVE'--ERROR1
								,'CHECK THE RECORD STATUS OF UOM CODE'
								,NULL
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								inner join epacube.UOM_CODE uc WITH (NOLOCK) on 
								(uc.uom_code_id = TSED.VALUE_UOM_CODE_FK
								AND uc.record_status_cr_fk = 2)
								WHERE TSED.data_type_cr_fk = 139
								AND   tsed.NEW_DATA <> 'NULL'
								AND   TSED.VALUE_UOM_CODE_FK is NOT NULL )



------------------------------------------------------------------------------------------
--   Insert Errors into Event Error Tables 
------------------------------------------------------------------------------------------

							INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(    SELECT 
								 TSED.EVENT_FK
								,NULL
								,98
								,'UOM CODE HAS CHANGED '  --ERROR NAME   
								,'VERIFY THE EFFECT OF CHANGE ON '
								,DN.LABEL
								,'UOM CONVERSION FACTOR:: '
								,CAST ( UOM_CONVERSION_FACTOR AS VARCHAR(16) )
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
								  ON ( DN.UOM_CLASS_DATA_NAME_FK = TSED.DATA_NAME_FK ) 
								WHERE EVENT_TYPE_CR_FK = 156
								AND   EVENT_ACTION_CR_FK IN ( 51, 52 )
								AND   tsed.NEW_DATA <> 'NULL'
								AND   ISNULL ( UOM_CONVERSION_FACTOR, 1 ) <> 1  )

-----------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_156'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_156 has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END









































