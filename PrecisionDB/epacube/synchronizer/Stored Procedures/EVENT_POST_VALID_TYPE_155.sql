﻿








-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform general event validations to all event types
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        02/11/2000   Initial SQL Version
-- CV        05/28/2009   Added to event post validation to be called.


CREATE PROCEDURE [synchronizer].[EVENT_POST_VALID_TYPE_155] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_155.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;




-----------------------------------------------------------------------------------
---check if association type 155 verify not going to create a duplicate
-----------------------------------------------------------------------------

INSERT INTO #TS_EVENT_DATA_ERRORS (
					EVENT_FK,
					RULE_FK,
					EVENT_CONDITION_CR_FK,
					ERROR_NAME,
					ERROR_DATA1,
					ERROR_DATA2,
					ERROR_DATA3,
					ERROR_DATA4,
					ERROR_DATA5,
					ERROR_DATA6,
					UPDATE_TIMESTAMP)
			(	SELECT 
				 TSED.EVENT_FK
				,NULL
				,99
				,'ASSOCIATION ALREADY EXISTS'  --ERROR NAME  
				,' REJECT EVENT'--ERROR1
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,@l_sysdate
				FROM #TS_EVENT_DATA TSED
				INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = TSED.DATA_NAME_FK )
				INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
				LEFT JOIN epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
				 ON ( PAS.DATA_NAME_FK = TSED.DATA_NAME_FK
				 AND  ISNULL ( PAS.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( TSED.PRODUCT_STRUCTURE_FK, 0 )
				 AND  (  ISNULL ( DS.ORG_IND, 0 ) = 0
					  OR ISNULL ( PAS.ORG_ENTITY_STRUCTURE_FK, 1 ) = ISNULL ( TSED.ORG_ENTITY_STRUCTURE_FK, 1 ) )	 -- WHSE SPECIFIC
				 AND  (  DS.ENTITY_STRUCTURE_EC_CR_FK = 10101          -- IF WHSE IGNORE ENTITY
					  OR (  ISNULL ( DN.SINGLE_ASSOC_IND, 0 ) = 1      -- IF SINGLE ASSOC THEN ENTITY IS VALUE;  UNIQUE KEY IS PROD/WHSE
						 OR (  ISNULL ( PAS.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) ) ) )
				  )
			-----------------
				WHERE tsed.event_type_cr_fk = 155
				AND TSED.NEW_DATA <> '-999999999'   ---inactivate
				AND  (   PAS.PRODUCT_ASSOCIATION_ID IS NOT NULL
						 AND  TSED.NEW_DATA  = ( SELECT CASE DS.ENTITY_STRUCTURE_EC_CR_FK
														   WHEN 10101 THEN epacube.getEntityIdent ( PAS.ORG_ENTITY_STRUCTURE_FK )
														   ELSE epacube.getEntityIdent ( PAS.ENTITY_STRUCTURE_FK )
														   END )
					  )				
			)  




	UPDATE #TS_EVENT_DATA
	SET EVENT_STATUS_CR_FK = 82  -- REJECT
	WHERE EVENT_FK IN (
	            SELECT TSED.EVENT_FK 
				FROM #TS_EVENT_DATA TSED
				INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = TSED.DATA_NAME_FK )
				INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
				LEFT JOIN epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
				 ON ( PAS.DATA_NAME_FK = TSED.DATA_NAME_FK
				 AND  ISNULL ( PAS.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( TSED.PRODUCT_STRUCTURE_FK, 0 )
				 AND  (  ISNULL ( DS.ORG_IND, 0 ) = 0
					  OR ISNULL ( PAS.ORG_ENTITY_STRUCTURE_FK, 1 ) = ISNULL ( TSED.ORG_ENTITY_STRUCTURE_FK, 1 ) )	 -- WHSE SPECIFIC
				 AND  (  DS.ENTITY_STRUCTURE_EC_CR_FK = 10101          -- IF WHSE IGNORE ENTITY
					  OR (  ISNULL ( DN.SINGLE_ASSOC_IND, 0 ) = 1      -- IF SINGLE ASSOC THEN ENTITY IS VALUE;  UNIQUE KEY IS PROD/WHSE
						 OR (  ISNULL ( PAS.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) ) ) )
				  )
			-----------------
				WHERE tsed.event_type_cr_fk = 155
				AND tsed.new_data <> '-999999999'
				AND  (   PAS.PRODUCT_ASSOCIATION_ID IS NOT NULL
						 AND  TSED.NEW_DATA  = ( SELECT CASE DS.ENTITY_STRUCTURE_EC_CR_FK
														   WHEN 10101 THEN epacube.getEntityIdent ( PAS.ORG_ENTITY_STRUCTURE_FK )
														   ELSE epacube.getEntityIdent ( PAS.ENTITY_STRUCTURE_FK )
														   END )
			         )	
			    )


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_155'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_155 has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END





