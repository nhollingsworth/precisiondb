﻿







-- Copyright 2008
--
-- Procedure Called from Synchronizer UI to submit events for approval
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -----------------------------------------------
-- KS        10/27/2008   Revised Original SQL Version to include Queue
--
CREATE PROCEDURE [synchronizer].[UTIL_EVENT_POSTING_LARGE_BATCH]
( @in_batch_no bigint  )
AS
BEGIN

DECLARE @l_sysdate    datetime
DECLARE @ls_stmt      varchar (1000)
DECLARE @ls_queue     nvarchar (4000)
DECLARE @l_exec_no    bigint
DECLARE @l_rows_processed    BIGINT

DECLARE @v_batch_no   bigint
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

DECLARE @status_desc  varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

BEGIN TRY
SET NOCOUNT ON
SET @l_sysdate = getdate ()


SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of SYNCHRONIZER.UTIL_EVENT_POSTING_LARGE_BATCH', 
												@exec_id = @l_exec_no OUTPUT;

------------------------------------------------------------------------------------------
---  LOOP THROUGH BATCH 1525
---
------------------------------------------------------------------------------------------


      DECLARE 
              @vm$event_action_cr_fk   int,      
              @vm$data_name_fk         int 



      DECLARE  cur_v_event cursor local for
			 SELECT 
			        A.event_action_cr_fk,
			        A.data_name_fk
			  FROM (
				SELECT DISTINCT
				ed.event_action_cr_fk, ed.data_name_fk
				from synchronizer.event_data ed with (nolock)
				inner join epacube.data_name dn with (nolock) on dn.data_name_id = ed.data_name_fk
				where 1 = 1
				and   ed.batch_no = @in_batch_no
				group by ed.event_action_cr_fk, ed.data_name_fk, dn.label
              ) A
			  ORDER BY 	a.event_action_cr_fk desc, a.data_name_fk

              
      
        OPEN cur_v_event;
        FETCH NEXT FROM cur_v_event INTO @vm$event_action_cr_fk
										,@vm$data_name_fk



------------------------------------------------------------------------------------------
---  LOOP THROUGH IMPORT, PRIORITY, TABLE
------------------------------------------------------------------------------------------


         WHILE @@FETCH_STATUS = 0 
         BEGIN
                

------------------------------------------------------------------------------------------
---  LOOP THROUGH DATA_NAMES IF ROWS > 250000
------------------------------------------------------------------------------------------


	EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'batch_seq', @v_batch_no output


	SET @status_desc = 'SYNCHRONIZER.UTIL_EVENT_POSTING_LARGE_BATCH >> '
                     + 'ACTION:: ' + CAST ( @vm$event_action_cr_fk AS VARCHAR(16) )
                     + 'DATA NAME:: ' + CAST ( @vm$data_name_fk AS VARCHAR(16) )
                     + 'BATCH:: ' + CAST ( @v_batch_no AS VARCHAR(16) )
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


    UPDATE synchronizer.EVENT_DATA
    SET BATCH_NO = @v_batch_no
       ,EVENT_STATUS_CR_FK = 87
    WHERE BATCH_NO = @in_batch_no
    AND   EVENT_ACTION_CR_FK = @vm$event_action_cr_fk
    AND   DATA_NAME_FK = @vm$data_name_fk

	EXEC synchronizer.EVENT_POSTING @v_batch_no
	


        FETCH NEXT FROM cur_v_event INTO @vm$event_action_cr_fk
										,@vm$data_name_fk


     END --cur_v_event LOOP
     CLOSE cur_v_event;
	 DEALLOCATE cur_v_event; 


--------------------------------------------------------------------------------------------------
---    END OF PERFORMACE LOOP   /  JOB, PRIORITY, TABLE NAME, EVENT TYPE
--------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.UTIL_EVENT_POSTING_LARGE_BATCH'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;
     


END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.UTIL_EVENT_POSTING_LARGE_BATCH has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END





















