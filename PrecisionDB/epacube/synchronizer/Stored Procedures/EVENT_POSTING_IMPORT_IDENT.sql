﻿









-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing for imports.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/28/2010   Initial SQL Version
-- CV        06/13/2011   Do not create -999999999 data 
-- CV        02/23/2014   Insert -999999999 as inactivate

CREATE PROCEDURE [synchronizer].[EVENT_POSTING_IMPORT_IDENT] 
        ( @in_batch_no BIGINT, @in_import_job_fk BIGINT, 
          @in_event_type_cr_fk INT,
          @in_table_name VARCHAR(64),
          @in_data_name_fk BIGINT,
          @in_org_entity_structure_fk VARCHAR(16) )



AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_IDENT.'
					 + 'Batch: ' + cast(isnull(@in_batch_no, 0) as varchar(30))
					 + 'Import: ' + cast(isnull(@in_import_job_fk, 0) as varchar(30))	
					 + 'Event Type: ' + cast(isnull(@in_event_type_cr_fk, 0) as varchar(30))					 				 
					 + 'Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))					 				 					 					 
					 + 'Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))
					 + 'Whse: ' + @in_org_entity_structure_fk 							 					 
					 							 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_job_id = @in_import_job_fk,
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;




------------------------------------------------------------------------------------------
--	PRODUCT IDENTIFICATION EVENTS DIRECTLY FROM STG_RECORD_IDENT
--		Use these tables instead of Metadata in case user changed data in Cleanser
------------------------------------------------------------------------------------------

IF @in_table_name = 'PRODUCT_IDENTIFICATION'

INSERT INTO #TS_EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   --,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[JOB_CLASS_FK]
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			   ,[STG_RECORD_FK]
			   ,[BATCH_NO]
			   ,[IMPORT_BATCH_NO]			   			   
			   ,[TABLE_ID_FK]			   
			   ,[VALUE_EFFECTIVE_DATE]
			   ,[END_DATE_NEW]
			   ,[RESULT_TYPE_CR_FK]
--------- ADDED FOR POSTING PROCESS
			   ,[DATA_SET_FK]
			   ,[DATA_TYPE_CR_FK]
			   ,[ORG_IND]
			   ,[INSERT_MISSING_VALUES]
			   ,[TABLE_NAME]
			   ,[EFFECTIVE_DATE_CUR]
			   ,[DATA_VALUE_FK]
			   ,[ENTITY_DATA_VALUE_FK]
			   ,[RULES_FK]	
			   ,[RECORD_STATUS_CR_FK]			   					   
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])			   
-----------------------------
SELECT DISTINCT
	 (	CASE ( SREC.ENTITY_CLASS_CR_FK )
		 WHEN (select code_ref_id from epacube.code_ref WITH (NOLOCK)
				where code_type = 'ENTITY_CLASS' 
				and Code = 'PRODUCT') 
		 THEN 150
		 ELSE 154
		 END )
	,SREC.ENTITY_CLASS_CR_FK
	,CASE WHEN ( SREC.EFFECTIVE_DATE < @L_SYSDATE )
	      THEN @L_SYSDATE
	      ELSE SREC.EFFECTIVE_DATE
	 END
	,SRECS.SEARCH_STRUCTURE_FK
	,SRECI.ID_DATA_NAME_FK
	,SRECI.ID_VALUE
	,PI.VALUE
	,(	CASE ( SREC.ENTITY_CLASS_CR_FK )
		 WHEN (select code_ref_id from epacube.code_ref WITH (NOLOCK)
				where code_type = 'ENTITY_CLASS' 
				and Code = 'PRODUCT')
		 THEN SRECS.SEARCH_STRUCTURE_FK
		 ELSE NULL
		 END )		 
    ,SRECI.ORG_ENTITY_STRUCTURE_FK
	,( SELECT ENTITY_CLASS_CR_FK
	   FROM EPACUBE.ENTITY_STRUCTURE WITH (NOLOCK)
	   WHERE ENTITY_STRUCTURE_ID = SRECI.ID_ENTITY_STRUCTURE_FK )
	--,SRECI.ID_ENTITY_DATA_NAME_FK	
	,SRECI.ID_ENTITY_STRUCTURE_FK
	--
	,97
	,88  -- PROCESSING
	,61  -- AWAITING APPROVAL  AUTO SYNC RULES WILL SET OTHERWISE
	,SREC.EVENT_SOURCE_CR_FK
	,CASE ISNULL ( PI.PRODUCT_IDENTIFICATION_ID, 0 )
	 WHEN 0 THEN 51
	 ELSE 52
	 END
	--
	,IP.JOB_CLASS_FK
	,SREC.JOB_FK
	,SREC.CREATE_TIMESTAMP
	,SREC.IMPORT_FILENAME
	,SREC.IMPORT_PACKAGE_FK
	,SREC.STG_RECORD_ID
	,@IN_BATCH_NO
	,@IN_BATCH_NO
	,PI.PRODUCT_IDENTIFICATION_ID
	,SREC.EFFECTIVE_DATE
	,SREC.END_DATE
	,SREC.RESULT_TYPE_CR_FK
--------- ADDED FOR POSTING PROCESS
    ,DS.DATA_SET_ID
    ,DS.DATA_TYPE_CR_FK
    ,DS.ORG_IND
    ,DN.INSERT_MISSING_VALUES
    ,DS.TABLE_NAME
    ,PI.UPDATE_TIMESTAMP
    ,NULL  ---VCPD.DATA_VALUE_FK
    ,NULL  ---VCPD.ENTITY_DATA_VALUE_FK
    ,NULL  ---[RULES_FK]
    ,1  -- RECORD_STATUS_CR_FK				   	    						   	
	,@L_SYSDATE
	,'EPACUBE'
----------------	
	FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
	INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
	  ON ( SRECS.STG_RECORD_FK = SREC.STG_RECORD_ID )		
	INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
	  ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID
	AND (ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL'
	AND SRECI.ID_VALUE <> '-999999999'))
    LEFT JOIN STAGE.STG_RECORD_ERRORS SRE WITH (NOLOCK)                 --- CHECK FOR NO ERRORS
     ON ( SRE.STG_RECORD_IDENT_FK =  SRECI.STG_RECORD_IDENT_ID)	
    INNER JOIN 	epacube.DATA_NAME  DN WITH (NOLOCK)
      ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK  
			AND ISNULL ( DN.NO_EVENT_IND, 0 ) = 0 )
    INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) 
      ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )	  
	INNER JOIN IMPORT.IMPORT_PACKAGE IP WITH (NOLOCK)
	  ON ( IP.IMPORT_PACKAGE_ID = SREC.IMPORT_PACKAGE_FK )
	LEFT JOIN epacube.PRODUCT_IDENTIFICATION PI WITH (NOLOCK)
	  ON ( PI.DATA_NAME_FK = DN.DATA_NAME_ID
	  AND  PI.PRODUCT_STRUCTURE_FK = SRECS.SEARCH_STRUCTURE_FK
	  AND  (     ISNULL ( DS.ORG_IND, 0 ) = 0
	        OR   ISNULL ( PI.ORG_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( SRECI.ORG_ENTITY_STRUCTURE_FK, -999 ) ) 
	  AND  (     ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 ) = 0 
	        OR   ISNULL ( PI.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK, -999 ) ) )	        
    WHERE 1 = 1
    AND   SREC.BATCH_NO = @IN_BATCH_NO
    AND   SRECI.ID_DATA_NAME_FK = @in_data_name_fk
    AND   (  CAST ( @in_org_entity_structure_fk AS BIGINT ) = -999 
          OR SRECI.ORG_ENTITY_STRUCTURE_FK = CAST ( @in_org_entity_structure_fk AS BIGINT ) )
    AND   DS.TABLE_NAME = 'PRODUCT_IDENTIFICATION'
    AND   SREC.EVENT_ACTION_CR_FK IN ( 51, 52, 54 ) 
	AND   SREC.EVENT_CONDITION_CR_FK IN ( 97, 98 )
    AND   SREC.ENTITY_CLASS_CR_FK = (select code_ref_id from epacube.code_ref
									where code_type = 'ENTITY_CLASS' 
									and Code = 'PRODUCT')
	AND   STG_RECORD_ERRORS_ID IS NULL   --- NO ERRORS EXIST
-------
-------  Future Release will parameterize valiation below based on Event or No Event  ( No Event Right Now )
-------	
	AND   (   PI.PRODUCT_IDENTIFICATION_ID IS NULL 
		   OR PI.VALUE <> SRECI.ID_VALUE   )


INSERT INTO #TS_EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   --,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[JOB_CLASS_FK]
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			   ,[STG_RECORD_FK]
			   ,[BATCH_NO]
			   ,[IMPORT_BATCH_NO]			   			   
			   ,[TABLE_ID_FK]			   
			   ,[VALUE_EFFECTIVE_DATE]
			   ,[END_DATE_NEW]
			   ,[RESULT_TYPE_CR_FK]
--------- ADDED FOR POSTING PROCESS
			   ,[DATA_SET_FK]
			   ,[DATA_TYPE_CR_FK]
			   ,[ORG_IND]
			   ,[INSERT_MISSING_VALUES]
			   ,[TABLE_NAME]
			   ,[EFFECTIVE_DATE_CUR]
			   ,[DATA_VALUE_FK]
			   ,[ENTITY_DATA_VALUE_FK]
			   ,[RULES_FK]	
			   ,[RECORD_STATUS_CR_FK]			   					   
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])			   
-----------------------------
SELECT DISTINCT
	 (	CASE ( SREC.ENTITY_CLASS_CR_FK )
		 WHEN (select code_ref_id from epacube.code_ref WITH (NOLOCK)
				where code_type = 'ENTITY_CLASS' 
				and Code = 'PRODUCT') 
		 THEN 150
		 ELSE 154
		 END )
	,SREC.ENTITY_CLASS_CR_FK
	,CASE WHEN ( SREC.EFFECTIVE_DATE < @L_SYSDATE )
	      THEN @L_SYSDATE
	      ELSE SREC.EFFECTIVE_DATE
	 END
	,SRECS.SEARCH_STRUCTURE_FK
	,SRECI.ID_DATA_NAME_FK
	,SRECI.ID_VALUE
	,PI.VALUE
	,(	CASE ( SREC.ENTITY_CLASS_CR_FK )
		 WHEN (select code_ref_id from epacube.code_ref WITH (NOLOCK)
				where code_type = 'ENTITY_CLASS' 
				and Code = 'PRODUCT')
		 THEN SRECS.SEARCH_STRUCTURE_FK
		 ELSE NULL
		 END )		 
    ,SRECI.ORG_ENTITY_STRUCTURE_FK
	,( SELECT ENTITY_CLASS_CR_FK
	   FROM EPACUBE.ENTITY_STRUCTURE WITH (NOLOCK)
	   WHERE ENTITY_STRUCTURE_ID = SRECI.ID_ENTITY_STRUCTURE_FK )
	--,SRECI.ID_ENTITY_DATA_NAME_FK	
	,SRECI.ID_ENTITY_STRUCTURE_FK
	--
	,97
	,88  -- PROCESSING
	,61  -- AWAITING APPROVAL  AUTO SYNC RULES WILL SET OTHERWISE
	,SREC.EVENT_SOURCE_CR_FK
	,53  ----inactivate
	--
	,IP.JOB_CLASS_FK
	,SREC.JOB_FK
	,SREC.CREATE_TIMESTAMP
	,SREC.IMPORT_FILENAME
	,SREC.IMPORT_PACKAGE_FK
	,SREC.STG_RECORD_ID
	,@IN_BATCH_NO
	,@IN_BATCH_NO
	,PI.PRODUCT_IDENTIFICATION_ID
	,SREC.EFFECTIVE_DATE
	,SREC.END_DATE
	,SREC.RESULT_TYPE_CR_FK
--------- ADDED FOR POSTING PROCESS
    ,DS.DATA_SET_ID
    ,DS.DATA_TYPE_CR_FK
    ,DS.ORG_IND
    ,DN.INSERT_MISSING_VALUES
    ,DS.TABLE_NAME
    ,PI.UPDATE_TIMESTAMP
    ,NULL  ---VCPD.DATA_VALUE_FK
    ,NULL  ---VCPD.ENTITY_DATA_VALUE_FK
    ,NULL  ---[RULES_FK]
    ,1  -- RECORD_STATUS_CR_FK				   	    						   	
	,@L_SYSDATE
	,'EPACUBE'
----------------	
	FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
	INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
	  ON ( SRECS.STG_RECORD_FK = SREC.STG_RECORD_ID )		
	INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
	  ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID
	AND (ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL'
	AND SRECI.ID_VALUE ='-999999999'))
    LEFT JOIN STAGE.STG_RECORD_ERRORS SRE WITH (NOLOCK)                 --- CHECK FOR NO ERRORS
     ON ( SRE.STG_RECORD_IDENT_FK =  SRECI.STG_RECORD_IDENT_ID)	
    INNER JOIN 	epacube.DATA_NAME  DN WITH (NOLOCK)
      ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK  
			AND ISNULL ( DN.NO_EVENT_IND, 0 ) = 0 )
    INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) 
      ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )	  
	INNER JOIN IMPORT.IMPORT_PACKAGE IP WITH (NOLOCK)
	  ON ( IP.IMPORT_PACKAGE_ID = SREC.IMPORT_PACKAGE_FK )
	INNER JOIN epacube.PRODUCT_IDENTIFICATION PI WITH (NOLOCK)  -------to only create if exist
	  ON ( PI.DATA_NAME_FK = DN.DATA_NAME_ID
	  AND  PI.PRODUCT_STRUCTURE_FK = SRECS.SEARCH_STRUCTURE_FK
	  AND  (     ISNULL ( DS.ORG_IND, 0 ) = 0
	        OR   ISNULL ( PI.ORG_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( SRECI.ORG_ENTITY_STRUCTURE_FK, -999 ) ) 
	  AND  (     ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 ) = 0 
	        OR   ISNULL ( PI.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK, -999 ) ) )	        
    WHERE 1 = 1
    AND   SREC.BATCH_NO = @IN_BATCH_NO
    AND   SRECI.ID_DATA_NAME_FK = @in_data_name_fk
    AND   (  CAST ( @in_org_entity_structure_fk AS BIGINT ) = -999 
          OR SRECI.ORG_ENTITY_STRUCTURE_FK = CAST ( @in_org_entity_structure_fk AS BIGINT ) )
    AND   DS.TABLE_NAME = 'PRODUCT_IDENTIFICATION'
    AND   SREC.EVENT_ACTION_CR_FK IN ( 51, 52, 54 ) 
	AND   SREC.EVENT_CONDITION_CR_FK IN ( 97, 98 )
    AND   SREC.ENTITY_CLASS_CR_FK = (select code_ref_id from epacube.code_ref
									where code_type = 'ENTITY_CLASS' 
									and Code = 'PRODUCT')
	AND   STG_RECORD_ERRORS_ID IS NULL   --- NO ERRORS EXIST
-------
-------  Future Release will parameterize valiation below based on Event or No Event  ( No Event Right Now )
-------	
	AND   (   PI.PRODUCT_IDENTIFICATION_ID IS NULL 
		   OR PI.VALUE <> SRECI.ID_VALUE   )



------------------------------------------------------------------------------------------
--	PRODUCT IDENT_NONUNIQUE EVENTS DIRECTLY FROM STG_RECORD_IDENT
--		Use these tables instead of Metadata in case user changed data in Cleanser
------------------------------------------------------------------------------------------

IF @in_table_name = 'PRODUCT_IDENT_NONUNIQUE'

INSERT INTO #TS_EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[JOB_CLASS_FK]
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			   ,[STG_RECORD_FK]
			   ,[BATCH_NO]
			   ,[IMPORT_BATCH_NO]			   			   
			   ,[TABLE_ID_FK]			   
			   ,[VALUE_EFFECTIVE_DATE]
			   ,[END_DATE_NEW]
			   ,[RESULT_TYPE_CR_FK]
--------- ADDED FOR POSTING PROCESS
			   ,[DATA_SET_FK]
			   ,[DATA_TYPE_CR_FK]
			   ,[ORG_IND]
			   ,[INSERT_MISSING_VALUES]
			   ,[TABLE_NAME]
			   ,[EFFECTIVE_DATE_CUR]
			   ,[DATA_VALUE_FK]
			   ,[ENTITY_DATA_VALUE_FK]
			   ,[RULES_FK]		
			   ,[RECORD_STATUS_CR_FK]			   				   
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])			   
-----------------------------
SELECT DISTINCT
	 (	CASE ( SREC.ENTITY_CLASS_CR_FK )
		 WHEN (select code_ref_id from epacube.code_ref WITH (NOLOCK)
				where code_type = 'ENTITY_CLASS' 
				and Code = 'PRODUCT') 
		 THEN 150
		 ELSE 154
		 END )
	,SREC.ENTITY_CLASS_CR_FK
	,CASE WHEN ( SREC.EFFECTIVE_DATE < @L_SYSDATE )
	      THEN @L_SYSDATE
	      ELSE SREC.EFFECTIVE_DATE
	 END
	,SRECS.SEARCH_STRUCTURE_FK
	,SRECI.ID_DATA_NAME_FK
	,SRECI.ID_VALUE
	,PI.VALUE
	,(	CASE ( SREC.ENTITY_CLASS_CR_FK )
		 WHEN (select code_ref_id from epacube.code_ref WITH (NOLOCK)
				where code_type = 'ENTITY_CLASS' 
				and Code = 'PRODUCT')
		 THEN SRECS.SEARCH_STRUCTURE_FK
		 ELSE NULL
		 END )		 
    ,SRECI.ORG_ENTITY_STRUCTURE_FK
	,( SELECT ENTITY_CLASS_CR_FK
	   FROM EPACUBE.ENTITY_STRUCTURE WITH (NOLOCK)
	   WHERE ENTITY_STRUCTURE_ID = SRECI.ID_ENTITY_STRUCTURE_FK )
	,SRECI.ID_ENTITY_DATA_NAME_FK	
	,SRECI.ID_ENTITY_STRUCTURE_FK
	--
	,97
	,88  -- PROCESSING
	,61  -- AWAITING APPROVAL  AUTO SYNC RULES WILL SET OTHERWISE
	,SREC.EVENT_SOURCE_CR_FK
	,CASE ISNULL ( PI.PRODUCT_IDENT_NONUNIQUE_ID, 0 )
	 WHEN 0 THEN 51
	 ELSE 52
	 END
	--
	,IP.JOB_CLASS_FK
	,SREC.JOB_FK
	,SREC.CREATE_TIMESTAMP
	,SREC.IMPORT_FILENAME
	,SREC.IMPORT_PACKAGE_FK
	,SREC.STG_RECORD_ID
	,@IN_BATCH_NO
	,@IN_BATCH_NO
	,PI.PRODUCT_IDENT_NONUNIQUE_ID
	,SREC.EFFECTIVE_DATE
	,SREC.END_DATE
	,SREC.RESULT_TYPE_CR_FK
--------- ADDED FOR POSTING PROCESS
    ,DS.DATA_SET_ID
    ,DS.DATA_TYPE_CR_FK
    ,DS.ORG_IND
    ,DN.INSERT_MISSING_VALUES
    ,DS.TABLE_NAME
    ,PI.UPDATE_TIMESTAMP
    ,NULL  ---VCPD.DATA_VALUE_FK
    ,NULL  ---VCPD.ENTITY_DATA_VALUE_FK
    ,NULL  ---[RULES_FK]	
    ,1  -- RECORD_STATUS_CR_FK				   	    					   	
	,@L_SYSDATE
	,'EPACUBE'
----------------	
	FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
	INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
	  ON ( SRECS.STG_RECORD_FK = SREC.STG_RECORD_ID )		
	INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
	  ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID
	AND (ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL'
	AND SRECI.ID_VALUE <> '-999999999'))
    LEFT JOIN STAGE.STG_RECORD_ERRORS SRE WITH (NOLOCK)                 --- CHECK FOR NO ERRORS
     ON ( SRE.STG_RECORD_IDENT_FK =  SRECI.STG_RECORD_IDENT_ID)	
    INNER JOIN 	epacube.DATA_NAME  DN WITH (NOLOCK)
      ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK  
			AND ISNULL ( DN.NO_EVENT_IND, 0 ) = 0 )
    INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) 
      ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )	  
	INNER JOIN IMPORT.IMPORT_PACKAGE IP WITH (NOLOCK)
	  ON ( IP.IMPORT_PACKAGE_ID = SREC.IMPORT_PACKAGE_FK )
	LEFT JOIN epacube.PRODUCT_IDENT_NONUNIQUE PI WITH (NOLOCK)
	  ON ( PI.DATA_NAME_FK = DN.DATA_NAME_ID
	  AND  PI.PRODUCT_STRUCTURE_FK = SRECS.SEARCH_STRUCTURE_FK
	  AND  (     ISNULL ( DS.ORG_IND, 0 ) = 0
	        OR   ISNULL ( PI.ORG_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( SRECI.ORG_ENTITY_STRUCTURE_FK, -999 ) ) 
	  AND  (     ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 ) = 0 
	        OR   ISNULL ( PI.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK, -999 ) ) )	        
    WHERE 1 = 1
    AND   SREC.BATCH_NO = @IN_BATCH_NO
    AND   SRECI.ID_DATA_NAME_FK = @in_data_name_fk
    AND   (  CAST ( @in_org_entity_structure_fk AS BIGINT ) = -999 
          OR SRECI.ORG_ENTITY_STRUCTURE_FK = CAST ( @in_org_entity_structure_fk AS BIGINT ) )
    AND   DS.TABLE_NAME = 'PRODUCT_IDENT_NONUNIQUE'
    AND   SREC.EVENT_ACTION_CR_FK IN ( 51, 52, 54 ) 
	AND   SREC.EVENT_CONDITION_CR_FK IN ( 97, 98 )
    AND   SREC.ENTITY_CLASS_CR_FK = (select code_ref_id from epacube.code_ref
									where code_type = 'ENTITY_CLASS' 
									and Code = 'PRODUCT')
	AND   STG_RECORD_ERRORS_ID IS NULL   --- NO ERRORS EXIST
-------
-------  Future Release will parameterize valiation below based on Event or No Event  ( No Event Right Now )
-------	
	AND   (   PI.PRODUCT_IDENT_NONUNIQUE_ID IS NULL 
		   OR PI.VALUE <> SRECI.ID_VALUE   )


-----------------inactiveate

INSERT INTO #TS_EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[JOB_CLASS_FK]
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			   ,[STG_RECORD_FK]
			   ,[BATCH_NO]
			   ,[IMPORT_BATCH_NO]			   			   
			   ,[TABLE_ID_FK]			   
			   ,[VALUE_EFFECTIVE_DATE]
			   ,[END_DATE_NEW]
			   ,[RESULT_TYPE_CR_FK]
--------- ADDED FOR POSTING PROCESS
			   ,[DATA_SET_FK]
			   ,[DATA_TYPE_CR_FK]
			   ,[ORG_IND]
			   ,[INSERT_MISSING_VALUES]
			   ,[TABLE_NAME]
			   ,[EFFECTIVE_DATE_CUR]
			   ,[DATA_VALUE_FK]
			   ,[ENTITY_DATA_VALUE_FK]
			   ,[RULES_FK]		
			   ,[RECORD_STATUS_CR_FK]			   				   
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])			   
-----------------------------
SELECT DISTINCT
	 (	CASE ( SREC.ENTITY_CLASS_CR_FK )
		 WHEN (select code_ref_id from epacube.code_ref WITH (NOLOCK)
				where code_type = 'ENTITY_CLASS' 
				and Code = 'PRODUCT') 
		 THEN 150
		 ELSE 154
		 END )
	,SREC.ENTITY_CLASS_CR_FK
	,CASE WHEN ( SREC.EFFECTIVE_DATE < @L_SYSDATE )
	      THEN @L_SYSDATE
	      ELSE SREC.EFFECTIVE_DATE
	 END
	,SRECS.SEARCH_STRUCTURE_FK
	,SRECI.ID_DATA_NAME_FK
	,SRECI.ID_VALUE
	,PI.VALUE
	,(	CASE ( SREC.ENTITY_CLASS_CR_FK )
		 WHEN (select code_ref_id from epacube.code_ref WITH (NOLOCK)
				where code_type = 'ENTITY_CLASS' 
				and Code = 'PRODUCT')
		 THEN SRECS.SEARCH_STRUCTURE_FK
		 ELSE NULL
		 END )		 
    ,SRECI.ORG_ENTITY_STRUCTURE_FK
	,( SELECT ENTITY_CLASS_CR_FK
	   FROM EPACUBE.ENTITY_STRUCTURE WITH (NOLOCK)
	   WHERE ENTITY_STRUCTURE_ID = SRECI.ID_ENTITY_STRUCTURE_FK )
	,SRECI.ID_ENTITY_DATA_NAME_FK	
	,SRECI.ID_ENTITY_STRUCTURE_FK
	--
	,97
	,88  -- PROCESSING
	,61  -- AWAITING APPROVAL  AUTO SYNC RULES WILL SET OTHERWISE
	,SREC.EVENT_SOURCE_CR_FK
	,53
	--
	,IP.JOB_CLASS_FK
	,SREC.JOB_FK
	,SREC.CREATE_TIMESTAMP
	,SREC.IMPORT_FILENAME
	,SREC.IMPORT_PACKAGE_FK
	,SREC.STG_RECORD_ID
	,@IN_BATCH_NO
	,@IN_BATCH_NO
	,PI.PRODUCT_IDENT_NONUNIQUE_ID
	,SREC.EFFECTIVE_DATE
	,SREC.END_DATE
	,SREC.RESULT_TYPE_CR_FK
--------- ADDED FOR POSTING PROCESS
    ,DS.DATA_SET_ID
    ,DS.DATA_TYPE_CR_FK
    ,DS.ORG_IND
    ,DN.INSERT_MISSING_VALUES
    ,DS.TABLE_NAME
    ,PI.UPDATE_TIMESTAMP
    ,NULL  ---VCPD.DATA_VALUE_FK
    ,NULL  ---VCPD.ENTITY_DATA_VALUE_FK
    ,NULL  ---[RULES_FK]	
    ,1  -- RECORD_STATUS_CR_FK				   	    					   	
	,@L_SYSDATE
	,'EPACUBE'
----------------	
	FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
	INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
	  ON ( SRECS.STG_RECORD_FK = SREC.STG_RECORD_ID )		
	INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
	  ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID
	AND (ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL'
	AND SRECI.ID_VALUE = '-999999999'))
    LEFT JOIN STAGE.STG_RECORD_ERRORS SRE WITH (NOLOCK)                 --- CHECK FOR NO ERRORS
     ON ( SRE.STG_RECORD_IDENT_FK =  SRECI.STG_RECORD_IDENT_ID)	
    INNER JOIN 	epacube.DATA_NAME  DN WITH (NOLOCK)
      ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK  
			AND ISNULL ( DN.NO_EVENT_IND, 0 ) = 0 )
    INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) 
      ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )	  
	INNER JOIN IMPORT.IMPORT_PACKAGE IP WITH (NOLOCK)
	  ON ( IP.IMPORT_PACKAGE_ID = SREC.IMPORT_PACKAGE_FK )
	INNER JOIN epacube.PRODUCT_IDENT_NONUNIQUE PI WITH (NOLOCK)  ----only create if exist
	  ON ( PI.DATA_NAME_FK = DN.DATA_NAME_ID
	  AND  PI.PRODUCT_STRUCTURE_FK = SRECS.SEARCH_STRUCTURE_FK
	  AND  (     ISNULL ( DS.ORG_IND, 0 ) = 0
	        OR   ISNULL ( PI.ORG_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( SRECI.ORG_ENTITY_STRUCTURE_FK, -999 ) ) 
	  AND  (     ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 ) = 0 
	        OR   ISNULL ( PI.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK, -999 ) ) )	        
    WHERE 1 = 1
    AND   SREC.BATCH_NO = @IN_BATCH_NO
    AND   SRECI.ID_DATA_NAME_FK = @in_data_name_fk
    AND   (  CAST ( @in_org_entity_structure_fk AS BIGINT ) = -999 
          OR SRECI.ORG_ENTITY_STRUCTURE_FK = CAST ( @in_org_entity_structure_fk AS BIGINT ) )
    AND   DS.TABLE_NAME = 'PRODUCT_IDENT_NONUNIQUE'
    AND   SREC.EVENT_ACTION_CR_FK IN ( 51, 52, 54 ) 
	AND   SREC.EVENT_CONDITION_CR_FK IN ( 97, 98 )
    AND   SREC.ENTITY_CLASS_CR_FK = (select code_ref_id from epacube.code_ref
									where code_type = 'ENTITY_CLASS' 
									and Code = 'PRODUCT')
	AND   STG_RECORD_ERRORS_ID IS NULL   --- NO ERRORS EXIST
-------
-------  Future Release will parameterize valiation below based on Event or No Event  ( No Event Right Now )
-------	
	AND   (   PI.PRODUCT_IDENT_NONUNIQUE_ID IS NULL 
		   OR PI.VALUE <> SRECI.ID_VALUE   )



------------------------------------------------------------------------------------------
--	PRODUCT IDENT_NONUNIQUE EVENTS DIRECTLY FROM STG_RECORD_IDENT
--		Use these tables instead of Metadata in case user changed data in Cleanser
------------------------------------------------------------------------------------------

IF @in_table_name = 'PRODUCT_IDENT_MULT'

INSERT INTO #TS_EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[JOB_CLASS_FK]
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			   ,[STG_RECORD_FK]
			   ,[BATCH_NO]
			   ,[IMPORT_BATCH_NO]			   			   
			   ,[TABLE_ID_FK]			   
			   ,[VALUE_EFFECTIVE_DATE]
			   ,[END_DATE_NEW]
			   ,[RESULT_TYPE_CR_FK]
--------- ADDED FOR POSTING PROCESS
			   ,[DATA_SET_FK]
			   ,[DATA_TYPE_CR_FK]
			   ,[ORG_IND]
			   ,[INSERT_MISSING_VALUES]
			   ,[TABLE_NAME]
			   ,[EFFECTIVE_DATE_CUR]
			   ,[DATA_VALUE_FK]
			   ,[ENTITY_DATA_VALUE_FK]
			   ,[RULES_FK]	
			   ,[RECORD_STATUS_CR_FK]			   					   
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])			   
-----------------------------
SELECT DISTINCT
	 (	CASE ( SREC.ENTITY_CLASS_CR_FK )
		 WHEN (select code_ref_id from epacube.code_ref WITH (NOLOCK)
				where code_type = 'ENTITY_CLASS' 
				and Code = 'PRODUCT') 
		 THEN 150
		 ELSE 154
		 END )
	,SREC.ENTITY_CLASS_CR_FK
	,CASE WHEN ( SREC.EFFECTIVE_DATE < @L_SYSDATE )
	      THEN @L_SYSDATE
	      ELSE SREC.EFFECTIVE_DATE
	 END
	,SRECS.SEARCH_STRUCTURE_FK
	,SRECI.ID_DATA_NAME_FK
	,SRECI.ID_VALUE
	,PI.VALUE
	,(	CASE ( SREC.ENTITY_CLASS_CR_FK )
		 WHEN (select code_ref_id from epacube.code_ref WITH (NOLOCK)
				where code_type = 'ENTITY_CLASS' 
				and Code = 'PRODUCT')
		 THEN SRECS.SEARCH_STRUCTURE_FK
		 ELSE NULL
		 END )		 
    ,SRECI.ORG_ENTITY_STRUCTURE_FK
	,( SELECT ENTITY_CLASS_CR_FK
	   FROM EPACUBE.ENTITY_STRUCTURE WITH (NOLOCK)
	   WHERE ENTITY_STRUCTURE_ID = SRECI.ID_ENTITY_STRUCTURE_FK )
	,SRECI.ID_ENTITY_DATA_NAME_FK	
	,SRECI.ID_ENTITY_STRUCTURE_FK
	--
	,97
	,88  -- PROCESSING
	,61  -- AWAITING APPROVAL  AUTO SYNC RULES WILL SET OTHERWISE
	,SREC.EVENT_SOURCE_CR_FK
	,CASE ISNULL ( PI.PRODUCT_IDENT_MULT_ID, 0 )
	 WHEN 0 THEN 51
	 ELSE 52
	 END
	--
	,IP.JOB_CLASS_FK
	,SREC.JOB_FK
	,SREC.CREATE_TIMESTAMP
	,SREC.IMPORT_FILENAME
	,SREC.IMPORT_PACKAGE_FK
	,SREC.STG_RECORD_ID
	,@IN_BATCH_NO
	,@IN_BATCH_NO
	,PI.PRODUCT_IDENT_MULT_ID
	,SREC.EFFECTIVE_DATE
	,SREC.END_DATE
	,SREC.RESULT_TYPE_CR_FK
--------- ADDED FOR POSTING PROCESS
    ,DS.DATA_SET_ID
    ,DS.DATA_TYPE_CR_FK
    ,DS.ORG_IND
    ,DN.INSERT_MISSING_VALUES
    ,DS.TABLE_NAME
    ,PI.UPDATE_TIMESTAMP
    ,NULL  ---VCPD.DATA_VALUE_FK
    ,NULL  ---VCPD.ENTITY_DATA_VALUE_FK
    ,NULL  ---[RULES_FK]		
    ,1  -- RECORD_STATUS_CR_FK				   	
	,@L_SYSDATE
	,'EPACUBE'
----------------	
	FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
	INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
	  ON ( SRECS.STG_RECORD_FK = SREC.STG_RECORD_ID )		
	INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
	  ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID
	AND (ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL'
	AND SRECI.ID_VALUE <> '-999999999'))
    LEFT JOIN STAGE.STG_RECORD_ERRORS SRE WITH (NOLOCK)                 --- CHECK FOR NO ERRORS
     ON ( SRE.STG_RECORD_IDENT_FK =  SRECI.STG_RECORD_IDENT_ID)	
    INNER JOIN 	epacube.DATA_NAME  DN WITH (NOLOCK)
      ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK  
			AND ISNULL ( DN.NO_EVENT_IND, 0 ) = 0 )
    INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) 
      ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )	  
	INNER JOIN IMPORT.IMPORT_PACKAGE IP WITH (NOLOCK)
	  ON ( IP.IMPORT_PACKAGE_ID = SREC.IMPORT_PACKAGE_FK )
	LEFT JOIN epacube.PRODUCT_IDENT_MULT PI WITH (NOLOCK)
	  ON ( PI.DATA_NAME_FK = DN.DATA_NAME_ID
	  AND  PI.PRODUCT_STRUCTURE_FK = SRECS.SEARCH_STRUCTURE_FK
	  AND  (     ISNULL ( DS.ORG_IND, 0 ) = 0
	        OR   ISNULL ( PI.ORG_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( SRECI.ORG_ENTITY_STRUCTURE_FK, -999 ) ) 
	  AND  (     ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 ) = 0 
	        OR   ISNULL ( PI.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK, -999 ) ) )	        
    WHERE 1 = 1
    AND   SREC.BATCH_NO = @IN_BATCH_NO
    AND   SRECI.ID_DATA_NAME_FK = @in_data_name_fk
    AND   (  CAST ( @in_org_entity_structure_fk AS BIGINT ) = -999 
          OR SRECI.ORG_ENTITY_STRUCTURE_FK = CAST ( @in_org_entity_structure_fk AS BIGINT ) )
    AND   DS.TABLE_NAME = 'PRODUCT_IDENT_MULT'
    AND   SREC.EVENT_ACTION_CR_FK IN ( 51, 52, 54 ) 
	AND   SREC.EVENT_CONDITION_CR_FK IN ( 97, 98 )
    AND   SREC.ENTITY_CLASS_CR_FK = (select code_ref_id from epacube.code_ref
									where code_type = 'ENTITY_CLASS' 
									and Code = 'PRODUCT')
	AND   STG_RECORD_ERRORS_ID IS NULL   --- NO ERRORS EXIST
-------
-------  Future Release will parameterize valiation below based on Event or No Event  ( No Event Right Now )
-------	
	AND   (   PI.PRODUCT_IDENT_MULT_ID IS NULL 
		   OR PI.VALUE <> SRECI.ID_VALUE   )



  			                             
--------------------------------------------------------------------------------------------
--   CALL VALIDATIONS
--------------------------------------------------------------------------------------------
    
	TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
	TRUNCATE TABLE #TS_EVENT_DATA_RULES	


    EXEC [synchronizer].[EVENT_POST_VALID_PROD_ENTITY] 

	EXEC SYNCHRONIZER.EVENT_POST_VALID_IMPORT

    EXEC [synchronizer].[EVENT_POST_VALID_DATA_TYPE]     

    IF @in_table_name IN ( 'PRODUCT_IDENTIFICATION', 'PRODUCT_IDENT_NONUNIQUE', 'PRODUCT_IDENT_MULT'   )
		EXEC SYNCHRONIZER.EVENT_POST_VALID_IDENTS

	EXEC [synchronizer].[EVENT_POST_VALID_TYPE_ALL] 		    


------------------------------------------------------------------------------------------
-- CALL PROC TO SET EVENT PRIORITY ACTION AND STATUS   ( KEEP IN ONE PLACE WITH CODE )
------------------------------------------------------------------------------------------

	EXEC  [synchronizer].[EVENT_POST_COND_PRIORITY_ACTION_STATUS] @in_batch_no


--------------------------------------------------------------------------------------------
--   CALL POST AND COMPLETE 
--------------------------------------------------------------------------------------------

		EXEC SYNCHRONIZER.EVENT_POST_DATA  

		UPDATE #TS_EVENT_DATA
		SET EVENT_STATUS_CR_FK = 80   -- RETURN TO PENDING
		WHERE EVENT_STATUS_CR_FK in ( 86, 87, 88 ) 


       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows Returned to Pending = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
       END
		

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_IDENT'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_IDENT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END



