﻿

-- Copyright 2012
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To Allow the creation of derived data from Current Values in the Database
--             This can be utilized to test derivation rules or during initial loads
--             where the history logging is turned off
--             The import_job_fk is only used to group the events for the Synchronizer View
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        10/24/2012   Initial SQL Version
-- GHS	     05/27/2015	  Restructured

CREATE PROCEDURE [synchronizer].[CURRENT_POST_DERIVATION] 
               ( @in_import_job_fk BIGINT, @in_result_data_name_fk INT
                ,@in_product_structure_fk BIGINT, @in_new_import_prods_only INT   )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int

DECLARE @v_batch_no   bigint

BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.CURRENT_POST_DERIVATION' 
				 + ' JOB:: '
				 + CAST ( ISNULL ( @in_import_job_fk, 999 ) AS VARCHAR(16) )	
				 + ' RESULT:: '	     
				 + CAST ( ISNULL ( @in_result_data_name_fk, 999 ) AS VARCHAR(16) )	     	          	     
				 + ' PRODUCT:: '	     
				 + CAST ( ISNULL ( @in_product_structure_fk, 999 ) AS VARCHAR(16) )	     	          	     
				 + ' NEW PRODS ONLY:: '	     
				 + CAST ( ISNULL ( @in_new_import_prods_only, 999 ) AS VARCHAR(16) )	     	          	     

         
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;



------------------------------------------------------------------------------------------
--   QUALIFY RULES ( DEFAULTS and DERIVATIONS ) 
--	    Upon return of this procedure 
--		Event_Data Source = 72 ( Derived Data ) Event Status = 88 
--		Event_Data_Rules only once with Result Rank = 1
------------------------------------------------------------------------------------------



		
------------------------------------------------------------------------------------------
--   Create Temp Table for Events to be assigned Rules
--		Note this temp table is used for all rule routines
--		If changed it must be changed in all places !!!!
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_EVENTS') is not null
	   drop table #TS_RULE_EVENTS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_RULE_EVENTS(
	    TS_RULE_EVENTS_ID        BIGINT IDENTITY(1000,1) NOT NULL,
		RULE_TYPE_CR_FK          INT NULL,	    
		RESULT_DATA_NAME_FK		 INT NULL,						
		RESULT_TYPE_CR_FK        INT NULL,
		RESULT_EFFECTIVE_DATE    DATETIME NULL,		
	    PRODUCT_STRUCTURE_FK     BIGINT NULL,
	    ORG_ENTITY_STRUCTURE_FK  BIGINT NULL,
	    CUST_ENTITY_STRUCTURE_FK BIGINT NULL,
	    SUPL_ENTITY_STRUCTURE_FK BIGINT NULL,
        JOB_FK                   BIGINT NULL,	    
        TRIGGER_EVENT_FK         BIGINT NULL,
        EVENT_TABLE_NAME         VARCHAR(32) NULL
		 )



--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT OF #TS_RULE_EVENTS
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSRE_TSREID ON #TS_RULE_EVENTS 
	(TS_RULE_EVENTS_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSRE_RDNF_REDT_IEID] ON #TS_RULE_EVENTS 
	(RESULT_DATA_NAME_FK ASC
	,RESULT_EFFECTIVE_DATE ASC
	,RESULT_TYPE_CR_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRE_PSF_IEF ON #TS_RULE_EVENTS 
	(PRODUCT_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRE_OESF_IEF ON #TS_RULE_EVENTS 
	(ORG_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRE_CESF_IEF ON #TS_RULE_EVENTS 
	(CUST_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRE_SESF_IEF ON #TS_RULE_EVENTS 
	(SUPL_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

----- ONLY USED WITH DERIVATIONS BECAUSE NEED TO LINK WITH NEWLY CREATE EVENT 
	CREATE NONCLUSTERED INDEX IDX_TSRE_INSERT_EVENT_DATA_RULES ON #TS_RULE_EVENTS 
	(JOB_FK ASC,
     TS_RULE_EVENTS_ID ASC,
     RESULT_DATA_NAME_FK ASC,
     PRODUCT_STRUCTURE_FK ASC,
	 ORG_ENTITY_STRUCTURE_FK ASC
	)
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

----- NEXT TWO USED IN DRANK
	CREATE NONCLUSTERED INDEX [IDX_TSRE_RDN_PRD_ORG_IEID] ON #TS_RULE_EVENTS 
	(RESULT_DATA_NAME_FK ASC
	,PRODUCT_STRUCTURE_FK ASC
	,ORG_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSRE_EFFDT_IEID] ON #TS_RULE_EVENTS 
	(RESULT_EFFECTIVE_DATE DESC
	,TS_RULE_EVENTS_ID DESC
	)
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)





TRUNCATE TABLE #TS_RULE_EVENTS


--------------------------------------------------------------------------------------------
--  CREATE ENTRIES FOR RELATED CURRENT DATA ( IMPORT_JOB_FK IS ONLY INFORMATIONAL )
--------------------------------------------------------------------------------------------
	
			
INSERT INTO #TS_RULE_EVENTS(
		 RULE_TYPE_CR_FK	     
		,RESULT_DATA_NAME_FK	
		,RESULT_TYPE_CR_FK	     
		,RESULT_EFFECTIVE_DATE	
	    ,PRODUCT_STRUCTURE_FK   
	    ,ORG_ENTITY_STRUCTURE_FK 
	    ,CUST_ENTITY_STRUCTURE_FK
	    --------,SUPL_ENTITY_STRUCTURE_FK
	    ,JOB_FK
        --------,TRIGGER_EVENT_FK
        ,EVENT_TABLE_NAME
		 )
	SELECT DISTINCT
	     RH.RULE_TYPE_CR_FK
		,R.RESULT_DATA_NAME_FK
		,710  -- CURRENT RESULT TYPE 
		,GETDATE()  ---ED.EVENT_EFFECTIVE_DATE   		   
		,ED.PRODUCT_STRUCTURE_FK   
		,ED.ORG_ENTITY_STRUCTURE_FK 
		,NULL  -- CUST_ENTITY_STRUCTURE_FK
		--------,ED.ENTITY_STRUCTURE_FK  -- as supplier
		,@in_import_job_fk          
		--------,ED.TABLE_ID_FK   ---ED.EVENT_ID
		,'CURRENT_DATA'
---------------------------	
FROM synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
			 ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
			 AND  RS.RECORD_STATUS_CR_FK = 1 )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)
	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID 
    AND  R.RECORD_STATUS_CR_FK = 1 )
INNER JOIN synchronizer.V_RULE_RELATED_DATA_NAMES VRDN
    ON ( R.RESULT_DATA_NAME_FK = VRDN.RESULT_DATA_NAME_FK ) 
INNER JOIN epacube.V_CURRENT_PRODUCT_DATA  ED WITH (NOLOCK)  --- USE CURRENT VIEW HERE
	ON  ( ED.DATA_NAME_FK = VRDN.RELATED_DATA_NAME_FK ) 
------------
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
    ON ( RFS.RULES_FK = R.RULES_ID 
	AND  (  ( RFS.PROD_FILTER_FK IS NULL AND RFS.SUPL_FILTER_FK IS NULL )
		 OR ( RFS.SUPL_FILTER_FK IS NULL AND RFS.PROD_FILTER_FK IN 
                                   (  SELECT VRFP.RULES_FILTER_FK
									  FROM synchronizer.V_RULE_FILTERS_PROD VRFP
									  WHERE VRFP.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK ) )
		 OR ( RFS.PROD_FILTER_FK IS NULL AND RFS.SUPL_FILTER_FK IN 
                                   (  SELECT VRFE.RULES_FILTER_FK
									  FROM epacube.product_association pas WITH (NOLOCK) 
									  INNER JOIN synchronizer.V_RULE_FILTERS_ENTITY VRFE
										  ON ( VRFE.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK 
										  AND  VRFE.ENTITY_CLASS_CR_FK = 10103 )
                                      WHERE pas.product_structure_fk = ED.product_structure_fk 
                                      and   pas.ORG_ENTITY_STRUCTURE_FK = ed.ORG_ENTITY_STRUCTURE_FK
								      AND   pas.DATA_NAME_FK  = RFS.SUPL_FILTER_ASSOC_DN_FK  ) )
		 OR ( ( RFS.PROD_FILTER_FK IN 
                                   (  SELECT VRFP.RULES_FILTER_FK
									  FROM synchronizer.V_RULE_FILTERS_PROD VRFP
									  WHERE VRFP.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK ) )
            AND
		      ( RFS.SUPL_FILTER_FK IN 
                                   (  SELECT VRFE.RULES_FILTER_FK
									  FROM epacube.product_association pas WITH (NOLOCK) 
									  INNER JOIN synchronizer.V_RULE_FILTERS_ENTITY VRFE
										  ON ( VRFE.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK 
										  AND  VRFE.ENTITY_CLASS_CR_FK = 10103 ) 
                                      WHERE pas.product_structure_fk = ED.product_structure_fk 
                                      AND   pas.ORG_ENTITY_STRUCTURE_FK = ed.ORG_ENTITY_STRUCTURE_FK
								      AND   pas.DATA_NAME_FK  = RFS.SUPL_FILTER_ASSOC_DN_FK  ) )
             )
      )  )
-------------- check related event and result event are at same org level --- see below
INNER JOIN EPACUBE.DATA_NAME DNE ON DNE.DATA_NAME_ID = ED.DATA_NAME_FK
INNER JOIN EPACUBE.DATA_SET DSE ON DSE.DATA_SET_ID = DNE.DATA_SET_FK
--------------
INNER JOIN EPACUBE.DATA_NAME DN ON DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK
INNER JOIN EPACUBE.DATA_SET DS ON DS.DATA_SET_ID = DN.DATA_SET_FK
--------------
WHERE 1 = 1
AND   RH.RULE_TYPE_CR_FK IN ( 302 )    -- DERIVATION ONLY
AND   RH.RECORD_STATUS_CR_FK = 1
----
-----------
AND  (    R.TRIGGER_EVENT_ACTION_CR_FK IS NULL
     OR   R.TRIGGER_EVENT_ACTION_CR_FK = 51 )  --- ADDs only
-----------------
-------  make sure related event and result are at the same org levels
AND (  (  ED.ORG_ENTITY_STRUCTURE_FK = 1
     AND  ISNULL ( DSE.CORP_IND, -999 ) =  ISNULL ( DS.CORP_IND, -999 ) 
     AND  ISNULL ( DSE.CORP_IND, -999 ) = 1 )  
    OR   
       (  ED.ORG_ENTITY_STRUCTURE_FK <> 1
     AND  ISNULL ( DSE.ORG_IND, -999 ) =  ISNULL ( DS.ORG_IND, -999 )
     AND  ISNULL ( DSE.ORG_IND, -999 ) = 1 )
     )
-------
AND  R.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
-------
AND  (  ISNULL ( @in_product_structure_fk, -999 ) = -999
     OR ISNULL ( @in_product_structure_fk, -999 ) = ED.PRODUCT_STRUCTURE_FK )
-------
AND  (  ISNULL ( @in_new_import_prods_only, 0 ) = 0
     OR (    ISNULL ( @in_new_import_prods_only, 0 ) = 1
        AND  ED.PRODUCT_STRUCTURE_FK IN ( SELECT PRODUCT_STRUCTURE_ID FROM epacube.PRODUCT_STRUCTURE WITH (NOLOCK)
                                          WHERE JOB_FK = @in_import_job_fk )  )
     )




    SET @V_COUNT = ( SELECT COUNT(1) FROM #TS_RULE_EVENTS  )
	   
	SET @status_desc = 'synchronizer.CURRENT_POST_DERIVATION:: BEFORE DRANK DELETE '
	     + ' RESULT:: '	     
	     + CAST ( ISNULL ( @in_result_data_name_fk, 999999999 ) AS VARCHAR(16) )	     	          	     
	     + ' COUNT:: '	     
	     + CAST ( ISNULL ( @V_COUNT, 999999999 ) AS VARCHAR(16) )	     	          	     

    EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;




--------------------------------------------------------------------------------------------
--   DELETE DUPLICATE #TS_RULE_EVENTS
--      SET OLDEST EFFECTIVE DATE EVENT DRANK = 1
--		FOR UNIQUE RESULT DATA NAME FK, PRODUCT/ORG
--------------------------------------------------------------------------------------------

	DELETE 
    FROM #TS_RULE_EVENTS
	WHERE TS_RULE_EVENTS_ID IN (
		SELECT A.TS_RULE_EVENTS_ID
		FROM (
				SELECT
				TS_RULE_EVENTS_ID,
				( DENSE_RANK() OVER ( PARTITION BY RESULT_DATA_NAME_FK
												  ,PRODUCT_STRUCTURE_FK
												  ,ORG_ENTITY_STRUCTURE_FK
							   ORDER BY RESULT_EFFECTIVE_DATE DESC
									   ,TS_RULE_EVENTS_ID DESC ) )AS DRANK
				FROM #TS_RULE_EVENTS
				WHERE 1 = 1
			) A
			WHERE A.DRANK <> 1   --- REMOVE ALL BUT THE TOP ROW FOR EACH RESULT/PROD/ORG
	  )


SET @V_COUNT = ( SELECT COUNT(1) FROM #TS_RULE_EVENTS  )
	   
IF @V_COUNT = 0
BEGIN
	SET @status_desc = 'synchronizer.CURRENT_POST_DERIVATION:: CANCEL NO DERIVATIONS '
	     + ' RESULT:: '	     
	     + CAST ( ISNULL ( @in_result_data_name_fk, 999999999 ) AS VARCHAR(16) )	     	          	     
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
END
ELSE
BEGIN


	SET @status_desc = 'synchronizer.CURRENT_POST_DERIVATION:: QUALIFY DERIVATIONS '
	     + ' RESULT:: '	     
	     + CAST ( ISNULL ( @in_result_data_name_fk, 999999999 ) AS VARCHAR(16) )	     	          	     
	     + ' COUNT:: '	     
	     + CAST ( ISNULL ( @V_COUNT, 999999999 ) AS VARCHAR(16) )	     	          	     

    EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;



-------------------------------------------------------------------------------------------------------
----  resume traditional logic flow for Event_Post_Qualify routines

---------------------------------------------------
-- Temp Table for all Qualified Rules
---------------------------------------------------


--drop temp tables
IF object_id('tempdb..#TS_QUALIFIED_RULES') is not null
   drop table #TS_QUALIFIED_RULES

CREATE TABLE #TS_QUALIFIED_RULES(
	[TS_QUALIFIED_RULES_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[TS_RULE_EVENTS_FK] [bigint] NULL,		
	[TRIGGER_EVENT_FK] [bigint] NULL,			
	[RULES_FK] [bigint] NULL,
	[RESULT_DATA_NAME_FK] [int] NULL,			
	[RESULT_TYPE_CR_FK] [int] NULL,	
	[RESULT_EFFECTIVE_DATE] [DATETIME] NULL,	
	[PROD_DATA_NAME_FK] [int] NULL,	
	[PROD_FILTER_FK] [bigint] NULL,
	[PROD_RESULT_TYPE_CR_FK] [int] NULL,
	[PROD_EFFECTIVE_DATE] [DATETIME] NULL,
	[PROD_EVENT_FK] [BIGINT] NULL,
	[PROD_ORG_ENTITY_STRUCTURE_FK] [BIGINT] NULL,	
	[PROD_CORP_IND] [SMALLINT] NULL,	
	[PROD_ORG_IND] [SMALLINT] NULL,	
	[PROD_FILTER_HIERARCHY] [SMALLINT] NULL,	
	[PROD_FILTER_ORG_PRECEDENCE] [SMALLINT] NULL,	
	[ORG_DATA_NAME_FK] [int] NULL,		
	[ORG_FILTER_FK] [bigint] NULL,
	[ORG_RESULT_TYPE_CR_FK] [int] NULL,	
	[ORG_EFFECTIVE_DATE] [DATETIME] NULL,
	[ORG_EVENT_FK] [BIGINT] NULL,	
	[CUST_DATA_NAME_FK] [int] NULL,		
	[CUST_FILTER_FK] [bigint] NULL,
	[CUST_FILTER_HIERARCHY] [smallint] NULL,	
	[CUST_RESULT_TYPE_CR_FK] [int] NULL,	
	[CUST_EFFECTIVE_DATE] [DATETIME] NULL,
	[CUST_EVENT_FK] [BIGINT] NULL,
	[SUPL_DATA_NAME_FK] [int] NULL,			
	[SUPL_FILTER_FK] [bigint] NULL,
	[SUPL_RESULT_TYPE_CR_FK] [int] NULL,
	[SUPL_EFFECTIVE_DATE] [DATETIME] NULL,
	[SUPL_EVENT_FK] [BIGINT] NULL,
	[RULES_RESULT_TYPE_CR_FK] [int] NULL,	
	[RULES_EFFECTIVE_DATE] [DATETIME] NULL,
	[RULES_END_DATE] [DATETIME] NULL,	
	[RULES_RECORD_STATUS_CR_FK] [int] NULL,
	[RELATED_RULES_FK] [BIGINT] NULL,			
	[PROD_FILTER_ASSOC_DN_FK] [INT] NULL, 		
	[ORG_FILTER_ASSOC_DN_FK] [INT] NULL, 		
	[CUST_FILTER_ASSOC_DN_FK] [INT] NULL, 		
	[SUPL_FILTER_ASSOC_DN_FK] [INT] NULL, 	
    [CONTRACT_PRECEDENCE]  [INT] NULL, 	
    [RULE_PRECEDENCE]  [INT] NULL, 	
	[SCHED_PRECEDENCE]  [INT] NULL, 					
	[STRUC_PRECEDENCE]	 [INT] NULL, 		
    [SCHED_MATRIX_ACTION_CR_FK]  [INT] NULL, 
    [STRUC_MATRIX_ACTION_CR_FK]  [INT] NULL,
    [RESULT_MATRIX_ACTION_CR_FK]  [INT] NULL,
    [PROMOTIONAL_IND]  [INT] NULL,   
    [HOST_RULE_XREF] VARCHAR(128) NULL,
	[RULES_STRUCTURE_FK] [bigint] NULL,					    
	[RULES_SCHEDULE_FK] [bigint] NULL,					    	
	[PRODUCT_STRUCTURE_FK] [bigint] NULL,					
	[ORG_ENTITY_STRUCTURE_FK] [bigint] NULL,						
	[CUST_ENTITY_STRUCTURE_FK] [bigint] NULL,						
	[SUPL_ENTITY_STRUCTURE_FK] [bigint] NULL,	
	[RULES_CONTRACT_CUSTOMER_FK] [bigint] NULL,			
	[DISQUALIFIED_IND] [smallint] NULL,
	[DISQUALIFIED_COMMENT] VARCHAR(64) NULL,
    [CUST_ENTITY_HIERARCHY] INT NULL
)



--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSQR_TSQRID ON #TS_QUALIFIED_RULES 
	(TS_QUALIFIED_RULES_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSRE_RDNF_REDT_IEID] ON #TS_QUALIFIED_RULES
	(RULES_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_FK, TS_QUALIFIED_RULES_ID, DISQUALIFIED_IND  )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


------------------------------------------------------------------------------------------
---  Call Routines to Quailify Rules
------------------------------------------------------------------------------------------

	EXEC  synchronizer.EVENT_RULE_QUALIFY  WITH RECOMPILE 



----------------------------------------------------------------------------------------------------
--   Assign Batch_No  ( but leave Submitted... Event_Posting will set to Processing )
---------------------------------------------------------------------------------------------------


	EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'batch_seq', @v_batch_no output


------------------------------------------------------------------------------------------
--	INSERT DERIVATION EVENTS --- DERIVED FROM EVENT_DATA 
------------------------------------------------------------------------------------------

	INSERT INTO [synchronizer].[EVENT_DATA]
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_ACTION_CR_FK]			   
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[RESULT_TYPE_CR_FK]				   
			   ,[JOB_CLASS_FK]
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_BATCH_NO]			   
			   ,[BATCH_NO]
			   ,[STG_RECORD_FK]		   
			   ,[RELATED_EVENT_FK]
			   ,[RECORD_STATUS_CR_FK]		   
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])
      SELECT DISTINCT     --- NEED DISTINCT BECAUSE COULD QUALIFY FOR MANY RULES			   		   
			    DS.EVENT_TYPE_CR_FK
			   ,DS.ENTITY_CLASS_CR_FK
			   ,TSRE.RESULT_EFFECTIVE_DATE
			   ,TSRE.PRODUCT_STRUCTURE_FK  -- NOT DOING ENTITY DERIVATIONS YET
			   ,TSRE.RESULT_DATA_NAME_FK
			   ,TSRE.PRODUCT_STRUCTURE_FK	
			   ,CASE ISNULL ( DS.ORG_IND, 0 )
			    WHEN 1 THEN TSRE.ORG_ENTITY_STRUCTURE_FK			   		   
			    ELSE 1
			    END
			   ,DS.ENTITY_STRUCTURE_EC_CR_FK
			   ,CASE ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 )
			    WHEN 0 THEN NULL
			    WHEN 10101 THEN NULL 
			    ELSE DN.ENTITY_DATA_NAME_FK
			    END 		   			   
			   ,CASE ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 )
			    WHEN 0 THEN NULL
			    WHEN 10101 THEN NULL
			    WHEN 10103 THEN TSRE.SUPL_ENTITY_STRUCTURE_FK
			    WHEN 10104 THEN TSRE.CUST_ENTITY_STRUCTURE_FK
			    ELSE NULL
			    END 		   			   
			   ,97
               ,CASE ( 	ISNULL ( VCPD.TABLE_ID_FK, 0 ) )
                WHEN 0 THEN 51
                ELSE 52
                END		   
			   ,87   --- CREATE SUBMITTED IN BATCH
			   ,66   --- SET TO AUTO APPROVE
			   ----,CASE WHEN TSRE.RESULT_DATA_NAME_FK IN ( SELECT DISTINCT RX.RESULT_DATA_NAME_FK 
						----							   FROM synchronizer.RULES RX WITH (NOLOCK)
						----							   INNER JOIN synchronizer.RULES_STRUCTURE RSX WITH (NOLOCK)
						----								ON ( RX.RULES_STRUCTURE_FK = RSX.RULES_STRUCTURE_ID 
						----								AND  RX.RECORD_STATUS_CR_FK = 1 
						----								AND  RSX.RULE_TYPE_CR_FK IN (309 )  )  )  -- COMPLETENESS  
					 ----THEN 69  -- SET PRIORITY COMPLETENESS
					 ----ELSE 61  -- AWAITING APPROVAL
			   ---- END			   
			   ,72
			   ,TSRE.RESULT_TYPE_CR_FK
			   ,( SELECT J.JOB_CLASS_FK FROM common.JOB J WITH (NOLOCK)
                  WHERE J.JOB_ID = TSRE.JOB_FK  )			   
			   ,TSRE.JOB_FK
			   ,NULL  ---IMPORT_BATCH_NO   --- MAKE SURE THERE IS NO IMPORT_BATCH_NO SO EVENTS WILL AUTO APPROVE          
			   ,@v_batch_no  ---BATCH_NO   --- MAKE SURE THAT NEW EVENTS HAVE THE BATCH           
			   ,TSRE.TS_RULE_EVENTS_ID     --- STG_RECORD_FK IS THE QUALIFIED RULE EVENT  ( IMPORTANT NEEDED FOR EVENT_DATA_RULES BELOW )
			   ,TSRE.TRIGGER_EVENT_FK
               ,1  -- RECORD STATUS 			   
			   ,GETDATE ()   --- UPDATE_TIMESTAMP
			   ,'EPACUBE' --- UPDATE_USER
      FROM #TS_RULE_EVENTS TSRE 
      INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
        ON ( DN.DATA_NAME_ID = TSRE.RESULT_DATA_NAME_FK )
      INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
        ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
	  LEFT JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD
		ON ( VCPD.DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK
		AND  VCPD.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK
		AND  VCPD.ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK )
      WHERE 1 = 1
      AND   TSRE.TS_RULE_EVENTS_ID IN 
                ( SELECT TSQR.TS_RULE_EVENTS_FK 
                  FROM #TS_QUALIFIED_RULES TSQR 
                  WHERE ISNULL ( DISQUALIFIED_IND, 0 ) = 0    )   --- EVENT HAS QUALIFIED RULES



/**********************************************************************************/
--   Insert into sychronizer.EVENT_DATA_RULES
--		Insert Current Rules... ONLY if not already there ??
--		Later look into DRANK ?? or something to remove duplicates
--      Also need to include a SQL for Future / Whatif Rules
/**********************************************************************************/

--- SHOULD NOT HAVE TO DELETE PRIOR TO INSERT BECAUSE DERIVATION
--     EVENTS ARE ALL NEW EVENTS....

	INSERT INTO [synchronizer].[EVENT_DATA_RULES] (
			 EVENT_FK
			,RULES_FK 
			,RESULT_DATA_NAME_FK
			,RESULT_TYPE_CR_FK
			,RESULT_EFFECTIVE_DATE 
	        ,RULES_ACTION_OPERATION_FK
	        ,RULES_EFFECTIVE_DATE
	        ,RULE_PRECEDENCE
	        ,SCHED_PRECEDENCE
            ,UPDATE_TIMESTAMP   ) 
    SELECT DISTINCT
			 ED.EVENT_ID  ---  FROM NEWLY INSERTED EVENTS ABOVE
			,TSQR.RULES_FK   
			,TSQR.RESULT_DATA_NAME_FK      		   
			,TSQR.RESULT_TYPE_CR_FK      	
			,TSQR.RESULT_EFFECTIVE_DATE      	
            ,RA.RULES_ACTION_OPERATION1_FK
		-------- FROM RULE TABLES
		    ,R.EFFECTIVE_DATE
			,R.RULE_PRECEDENCE
			,RSCH.SCHEDULE_PRECEDENCE				
			,GETDATE()  
------
		FROM  #TS_QUALIFIED_RULES TSQR
		INNER JOIN #TS_RULE_EVENTS TSRE
		 ON ( TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK )
		INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
		 ON ( ISNULL(ED.IMPORT_JOB_FK,0) = ISNULL(TSRE.JOB_FK,0)
         ------------AND  ED.BATCH_NO = @v_batch_no
         AND  ED.STG_RECORD_FK = TSRE.TS_RULE_EVENTS_ID 
		 AND  ED.DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK
		 AND  ED.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK
		 AND  ED.ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK
	     AND  ED.EVENT_SOURCE_CR_FK = 72
		    )		 
		INNER JOIN synchronizer.RULES R
		 ON ( R.RULES_ID = TSQR.RULES_FK )
		INNER JOIN synchronizer.RULES_ACTION RA
		 ON ( RA.RULES_FK = R.RULES_ID )
		INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
		  ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK )
		INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
		 ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
		INNER JOIN synchronizer.RULES_SCHEDULE RSCH WITH (NOLOCK) 
		 ON ( RSCH.RULES_SCHEDULE_ID = R.RULES_SCHEDULE_FK )
		WHERE ISNULL ( DISQUALIFIED_IND, 0 ) = 0         



------------------------------------------------------------------------------------------
--  DENSE RANK ON PRECEDENCE
------------------------------------------------------------------------------------------


	UPDATE synchronizer.EVENT_DATA_RULES
	SET RESULT_RANK = 1
	WHERE EVENT_RULES_ID IN (
	SELECT A.EVENT_RULES_ID
	FROM (
	SELECT
		EDR.EVENT_RULES_ID,
		( DENSE_RANK() OVER ( PARTITION BY EDR.EVENT_FK
						ORDER BY EDR.STRUC_PRECEDENCE ASC, EDR.SCHED_PRECEDENCE ASC, EDR.RULE_PRECEDENCE ASC, 
								 EDR.RULES_FK DESC ) ) AS DRANK
	FROM  #TS_QUALIFIED_RULES TSQR
			INNER JOIN #TS_RULE_EVENTS TSRE
			 ON ( TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK )
			INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
			 ON ( ISNULL(ED.IMPORT_JOB_FK,0) = ISNULL(TSRE.JOB_FK,0) 
             AND  ED.STG_RECORD_FK = TSRE.TS_RULE_EVENTS_ID
			 AND  ED.DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK
			 AND  ED.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK
			 AND  ED.ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK
			 AND  ED.EVENT_SOURCE_CR_FK = 72
			    )		 
		    INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
	         ON ( EDR.EVENT_FK = ED.EVENT_ID )
	WHERE 1 = 1
	) A 
	WHERE DRANK = 1
	)


END --- End for #TS_RULE_EVENTS > 0

------------------------------------------------------------------------------------------
--  Drop Temp Tables
------------------------------------------------------------------------------------------


--drop temp tables
IF object_id('tempdb..#TS_QUALIFIED_RULES') is not null
   drop table #TS_QUALIFIED_RULES


----drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_EVENTS') is not null
	   drop table #TS_RULE_EVENTS;






------------------------------------------------------------------------------------------
--   QUALIFY RULES ( DEFAULTS and DERIVATIONS ) 
--	    Upon return of this procedure 
--		Event_Data Source = 72 ( Derived Data ) Event Status = 88 
--		Event_Data_Rules only once with Result Rank = 1
------------------------------------------------------------------------------------------

SET @l_rows_processed = 
               (	SELECT COUNT(1)
					FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
					INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
						ON ( EDR.EVENT_FK = ED.EVENT_ID 
						AND  EDR.RESULT_RANK = 1  )
					INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
						ON ( EDR.RULES_FK = R.RULES_ID )
					INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
					  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )						
					INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
						ON RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
						AND RH.RECORD_STATUS_CR_FK = 1 
					WHERE 1 = 1
					AND   ED.BATCH_NO = @v_batch_no
					AND   ED.EVENT_SOURCE_CR_FK = 72 
					AND   RH.RULE_TYPE_CR_FK IN ( 301, 302, 303 )	
					-----------
					AND  R.RESULT_DATA_NAME_FK	 = @in_result_data_name_fk						
                )
	   
IF @l_rows_processed = 0
BEGIN
	SET @status_desc = 'No Defaults or Derivations to be Processed for Batch:: '
		 + cast(isnull(@v_batch_no, 0) as varchar(30))	     
	     + ' Result:: '
	     + CAST ( ISNULL (  @in_result_data_name_fk, 999999999 ) AS VARCHAR(16) )	     
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
END
ELSE
BEGIN

------------------------------------------------------------------------------------------
---  LOOP THROUGH RESULTS BY CALC GROUP SEQ
------------------------------------------------------------------------------------------

      DECLARE @vm$calc_group_seq			 INT
             ,@vm$result_data_name_fk        INT
			
			DECLARE  cur_v_result_group cursor local for
			SELECT DISTINCT ISNULL ( RH.calc_group_seq, 999999999 ) AS calc_group_seq	
			               ,r.result_data_name_fk 
			FROM synchronizer.RULES R WITH (NOLOCK)
			INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
				ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID  
				AND  RS.RULE_TYPE_CR_FK IN ( 301, 302, 303 ) 
				AND  RS.RECORD_STATUS_CR_FK = 1 )  -- DEFAULT OR DERIVATION 
			INNER JOIN Synchronizer.rules_hierarchy RH WITH (NOLOCK) 
			    ON RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
			    AND RH.RECORD_STATUS_CR_FK = 1 
			WHERE R.RECORD_STATUS_CR_FK = 1
			AND   R.RESULT_DATA_NAME_FK = @in_result_data_name_fk
			ORDER BY  calc_group_seq ASC

                    
    OPEN cur_v_result_group;
    FETCH NEXT FROM cur_v_result_group INTO  @vm$calc_group_seq	
											,@vm$result_data_name_fk	


------------------------------------------------------------------------------------------
---  LOOP THROUGH EVENTS BY RESULT DATA NAME CALC SEQ
------------------------------------------------------------------------------------------


     WHILE @@FETCH_STATUS = 0 
     BEGIN


------------------------------------------------------------------------------------------
--  PERFORM DEFAULT ID SEQ RULES FIRST EVEN THOUGH DEFAULTS USUALLY RUN AFTER DERIVATIONS
------------------------------------------------------------------------------------------

SET @V_COUNT = (	SELECT COUNT(1)
					FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
					INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
						ON ( EDR.EVENT_FK = ED.EVENT_ID 
						AND  EDR.RESULT_RANK = 1  )
					INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
						ON ( EDR.RULES_FK = R.RULES_ID )
					INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
					  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )						
					INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
						ON RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
						AND RH.RECORD_STATUS_CR_FK = 1 
					INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
					  ON ( RA.RULES_FK = R.RULES_ID )
					WHERE 1 = 1
					AND   ED.BATCH_NO = @v_batch_no
					AND   ED.EVENT_SOURCE_CR_FK = 72 
					AND   ED.NEW_DATA IS NULL 
					AND   RS.RULE_TYPE_CR_FK = 301	 -- DEFAULT		
					AND   RA.RULES_ACTION_OPERATION1_FK = 350  -- ACTION ID SEQUENCE
					-----------
					AND  ISNULL ( RH.CALC_GROUP_SEQ, 999999999 ) = @vm$calc_group_seq
					AND  R.RESULT_DATA_NAME_FK	 = @vm$result_data_name_fk						
                )
		
IF @V_COUNT > 0	 
BEGIN  
 EXEC  synchronizer.EVENT_POST_DERIVE_PRODUCT_ID @v_batch_no, @vm$calc_group_seq, @vm$result_data_name_fk
 END



------------------------------------------------------------------------------------------
--  NEXT PERFORM CONCATENATION DERIVATIONS
------------------------------------------------------------------------------------------

SET @V_COUNT = (	SELECT COUNT(1)
					FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
					INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
						ON ( EDR.EVENT_FK = ED.EVENT_ID 
						AND  EDR.RESULT_RANK = 1  )
					INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
						ON ( EDR.RULES_FK = R.RULES_ID )
					INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
					  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
					INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
						ON RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
						AND RH.RECORD_STATUS_CR_FK = 1 
					INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
					  ON ( RA.RULES_FK = R.RULES_ID )
					WHERE 1 = 1
					AND   RS.RULE_TYPE_CR_FK IN ( 301, 302 )	 --DERIVATION or DEFAULT
					AND   RA.RULES_ACTION_OPERATION1_FK IN ( 300 ) --- CONCATENATION					
					AND   ED.BATCH_NO = @v_batch_no	
					-----------
					AND  ISNULL ( RH.CALC_GROUP_SEQ, 999999999 ) = @vm$calc_group_seq	
					AND  R.RESULT_DATA_NAME_FK = @vm$result_data_name_fk														
		        )
		        
		
IF @V_COUNT > 0	 
BEGIN  
                
		 EXEC  synchronizer.EVENT_POST_DERIVE_NEW_DATA_CONCATENATE @v_batch_no, @vm$calc_group_seq, @vm$result_data_name_fk
END



------------------------------------------------------------------------------------------
--  NEXT PERFORM CONCATENATION DERIVATIONS
------------------------------------------------------------------------------------------

SET @V_COUNT = (	SELECT COUNT(1)
					FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
					INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
						ON ( EDR.EVENT_FK = ED.EVENT_ID 
						AND  EDR.RESULT_RANK = 1  )
					INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
						ON ( EDR.RULES_FK = R.RULES_ID )
					INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
					  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
					INNER JOIN synchronizer.Rules_HIERARCHY RH WITH (NOLOCK)
						ON RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
						AND RH.RECORD_STATUS_CR_FK = 1 
					INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
					  ON ( RA.RULES_FK = R.RULES_ID )
					WHERE 1 = 1
					AND   RS.RULE_TYPE_CR_FK IN ( 301, 302 )  -- DERIVATION or DEFAULT
					AND   RA.RULES_ACTION_OPERATION1_FK IN ( 310 ) --- SUBSTRING				
					AND   ED.BATCH_NO = @v_batch_no	
					-----------
					AND  ISNULL ( RH.CALC_GROUP_SEQ, 999999999 ) = @vm$calc_group_seq	
					AND R.RESULT_DATA_NAME_FK = @vm$result_data_name_fk														
		        )
		        
		
IF @V_COUNT > 0	 
BEGIN  
                
		 EXEC  synchronizer.EVENT_POST_DERIVE_NEW_DATA_SUBSTRING @v_batch_no, @vm$calc_group_seq, @vm$result_data_name_fk
END



------------------------------------------------------------------------------------------
---  NO ACTION OPERATION
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_DATA 
SET EVENT_ACTION_CR_FK = 58  -- NO ACTION
   ,EVENT_STATUS_CR_FK = 83  -- PREPOST
WHERE EVENT_ID IN ( 
       SELECT ED.EVENT_ID
       FROM synchronizer.EVENT_DATA  ED WITH (NOLOCK)
		INNER JOIN synchronizer.EVENT_DATA_RULES EDR  WITH (NOLOCK) 
		  ON ( EDR.EVENT_FK = ED.EVENT_ID
		  AND  EDR.RESULT_RANK = 1  )      --- ONLY THE SELECTED RULE
		INNER JOIN synchronizer.RULES R  WITH (NOLOCK) 
		  ON ( EDR.RULES_FK = R.RULES_ID )
        INNER JOIN EPACUBE.DATA_NAME DN ON DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK		  
		INNER JOIN synchronizer.RULES_STRUCTURE RS  WITH (NOLOCK) 
		  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
		INNER JOIN synchronizer.RULES_SCHEDULE RSCHED  WITH (NOLOCK)
				  ON ( RSCHED.RULES_SCHEDULE_ID = R.RULES_SCHEDULE_FK )	
	   INNER JOIN synchronizer.Rules_hierarchy RH WITH (NOLOCK) 
						ON RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
						AND RH.RECORD_STATUS_CR_FK = 1 
		INNER JOIN synchronizer.RULES_ACTION RA  WITH (NOLOCK) 
		  ON ( RA.RULES_FK = R.RULES_ID )
		INNER JOIN synchronizer.RULES_ACTION_OPERATION RAOP  WITH (NOLOCK) 
		  ON ( RA.RULES_ACTION_OPERATION1_FK = RAOP.RULES_ACTION_OPERATION_ID )		  
		WHERE 1 = 1
		AND   RS.RULE_TYPE_CR_FK IN ( 301, 302 )    
		AND   RAOP.NAME = 'NO ACTION' 
		AND   ED.BATCH_NO = @v_batch_no
		AND   ED.EVENT_SOURCE_CR_FK = 72  -- DERIVATION EVENT
        AND  ISNULL ( RH.CALC_GROUP_SEQ, 999999999 ) = @vm$calc_group_seq	
        AND  R.RESULT_DATA_NAME_FK = @vm$result_data_name_fk	
       )



------------------------------------------------------------------------------------------
--  IF RULE IS TO BE EXPANDED TO ALL WAREHOUSES ( APPLY_TO_ORG_CHILDREN_IND = 1 ) 
--		THEN SET EVENT SOURCE ( CODE_REF_ID = 73 ) TO COPY
--    epa-2924
------------------------------------------------------------------------------------------

INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_CLASS_CR_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[NEW_DATA]
           --,[CURRENT_DATA]
           ,ENTITY_DATA_VALUE_FK
           ,DATA_VALUE_FK
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[EVENT_REASON_CR_FK]
           ,[RESULT_TYPE_CR_FK]
           ,[JOB_CLASS_FK]
           ,[CALC_JOB_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[IMPORT_BATCH_NO]
           ,[BATCH_NO]
           ,[RULES_FK]
           ,[RELATED_EVENT_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]  )
SELECT
            ED.EVENT_TYPE_CR_FK
           ,ED.EVENT_ENTITY_CLASS_CR_FK
           ,ED.EVENT_EFFECTIVE_DATE
           ,ED.EPACUBE_ID
           ,ED.DATA_NAME_FK
           ,ED.PRODUCT_STRUCTURE_FK
           ,PAS.ORG_ENTITY_STRUCTURE_FK  --- PRODUCT/WHSE ASSOC
           ,ED.ENTITY_CLASS_CR_FK
           ,ED.ENTITY_DATA_NAME_FK
           ,ED.ENTITY_STRUCTURE_FK
           ,ED.NEW_DATA
           --,ED.CURRENT_DATA
           ,ED.ENTITY_DATA_VALUE_FK
           ,ED.DATA_VALUE_FK
           ,ED.EVENT_CONDITION_CR_FK
           ,80                          --- EVENT_STATUS_CR_FK
           ,ED.EVENT_PRIORITY_CR_FK
           ,73                           --- COPY IS THE EVENT SOURCE
           ,58 --ED.EVENT_ACTION_CR_FK   --- COPY
           ,ED.EVENT_REASON_CR_FK
           ,ED.RESULT_TYPE_CR_FK
           ,ED.JOB_CLASS_FK
           ,ED.CALC_JOB_FK
           ,ED.IMPORT_JOB_FK
           ,ED.IMPORT_DATE
           ,ED.IMPORT_PACKAGE_FK
           ,ED.IMPORT_FILENAME
           ,ED.IMPORT_BATCH_NO
           ,@v_batch_no
           ,ED.RULES_FK
           ,ED.EVENT_ID                  --- RELATED EVENT IS ORIGINAL EVENT COPIED FROM
           ,ED.RECORD_STATUS_CR_FK
           ,ED.UPDATE_TIMESTAMP
           ,ED.UPDATE_USER
-------------
	FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
	INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
		ON ( EDR.EVENT_FK = ED.EVENT_ID
		AND  EDR.RESULT_RANK = 1  )
	INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
		ON ( EDR.RULES_FK = R.RULES_ID )
    --INNER JOIN EPACUBE.DATA_NAME DN ON DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK		  		
	INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
	  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
	  INNER JOIN synchronizer.Rules_hierarchy RH WITH (NOLOCK) 
						ON RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
						AND RH.RECORD_STATUS_CR_FK = 1 
	INNER JOIN epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK) 
	  ON  ( PAS.DATA_NAME_FK = 159100  --- PRODUCT WHSE
	  AND   PAS.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK )
	WHERE 1 = 1
	AND   ED.BATCH_NO = @v_batch_no
	AND   ED.NEW_DATA IS NOT NULL 	
------------	AND   ED.EVENT_SOURCE_CR_FK = 72  just removed this to see if it will work for GLOBAL COPY
	AND   R.APPLY_TO_ORG_CHILDREN_IND = 1  -- APPLY DERIVED VALUE TO ALL WAREHOUSES    
-----------
    AND  (    
			 (    RS.RULE_TYPE_CR_FK IN ( 301, 302 )    ----  DEFAULT OR DERIVATION 
			 AND  ED.ORG_ENTITY_STRUCTURE_FK = 1 )
          OR
			 (    RS.RULE_TYPE_CR_FK = 303    ----  GLOBAL COPY
			AND  (  ED.ORG_ENTITY_STRUCTURE_FK = 1    
				  OR ED.DATA_NAME_FK = 159100 )  )  --- ONLY APPLY RULE TO CORP EVENTS OR WHSE ASSOC
	      )
	--------
    AND  ISNULL ( RH.CALC_GROUP_SEQ, 999999999 ) = @vm$calc_group_seq	
    AND  R.RESULT_DATA_NAME_FK = @vm$result_data_name_fk	
	
	
	
	
END  --- DERIVATION PROCESSING




------------------------------------------------------------------------------------------
---  GET NEXT Warehouse and Result Grouping 
------------------------------------------------------------------------------------------

    FETCH NEXT FROM cur_v_result_group INTO  @vm$calc_group_seq	
											,@vm$result_data_name_fk

     END --cur_v_result_group  LOOP
     CLOSE cur_v_result_group
	 DEALLOCATE cur_v_result_group; 
     

	


/* ----------------------------------------------------------------------------------- */
--    DO NOT Use the same batch no for both import_batch_no and batch_no 
--		So that every event will automatically go to 66  ( which bypassed Auto Sync )
--      ie EVERY EVENT WILL BE SET TO AUTO APPROVE
/* ----------------------------------------------------------------------------------- */


			EXEC synchronizer.EVENT_POSTING @v_batch_no



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.CURRENT_POST_DERIVATION'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.CURRENT_POST_DERIVATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END








