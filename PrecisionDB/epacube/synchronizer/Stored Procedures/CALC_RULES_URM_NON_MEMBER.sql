﻿



-- Copyright 2013
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to create/modify Calculation Rules
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        11/08/2013   Initial SQL Version


CREATE PROCEDURE [synchronizer].[CALC_RULES_URM_NON_MEMBER] 
( @Job_FK bigint , @Rule_Eff_Date as date = Null
)
												
AS

/***************  Parameter Defintions  **********************

***************************************************************/

If object_id('tempdb..#CalcResults') is not null
Drop table #CalcResults

If object_id('tempdb..#PBV') is not null
Drop Table #PBV

If object_id('tempdb..#Rules') is not null
Drop Table #Rules

--Declare @Rule_Eff_Date as date

If isnull(@Rule_Eff_Date, '1900-01-01') = '1900-01-01'
Set @Rule_Eff_Date = getdate()

--Declare @Job_FK bigint
--Set @Job_FK = 1160

If object_id('tempdb..#Rules') is not null
Drop Table #Rules

	Select product_structure_fk, effective_date, value, Label, data_name_fk, cast(0 as int) 'Inserted', Cast(Null as bigint) Cust_Segment_Data_Name_FK 
	into #PBV from 
			(Select pbv.product_structure_fk, pbv.effective_date, pbv.value, DN.Label, pbv.data_name_fk
			, Dense_Rank()over(partition by pbv.product_structure_fk, pbv.data_name_fk order by pbv.effective_date desc) DRank_Cost 
				from epacube.marginmgr.PRICESHEET_BASIS_VALUES pbv 
				inner join epacube.epacube.data_name dn with (nolock) on pbv.Data_Name_FK = dn.DATA_NAME_ID
				inner join epacube.synchronizer.event_calc ec on pbv.product_structure_fk = ec.PRODUCT_STRUCTURE_FK
				where effective_date is not null and effective_date <= @Rule_Eff_Date
				and pbv.data_name_fk = 503102
				and ec.job_fk = @Job_FK
			) A 
			where DRank_Cost = 1

	Create index idx_pbv1 on #PBV(product_structure_fk, data_name_fk)
	Create index idx_pbv2 on #PBV(Inserted)

	Select * into #Rules from (
	Select R.*, Dense_Rank()over(partition by r.result_data_name_Fk, r.Precedence, r.Rules_Type_Cust_Segment_FK, r.Prod_Segment_FK, r.Cust_Segment_FK, isnull(r.Org_Segment_FK, 0), isnull(r.Vendor_Segment_FK, 0) order by r.effective_date desc) DRank_Eff_Date
	from synchronizer.rules r
	where 1 = 1
	and r.result_data_name_fk in (503201, 503110, 503202)
	) A where DRank_Eff_Date = 1

	Create Index idx_r1 on #Rules(RESULT_DATA_NAME_FK, Rules_type_cust_segment_fk, cust_segment_fk, prod_segment_fk, rules_id)
	Create Index idx_r2 on #Rules(result_data_name_Fk, Precedence, Rules_Type_Cust_Segment_FK, Prod_Segment_FK, Cust_Segment_FK, Org_Segment_FK, Vendor_Segment_FK, Effective_date)

	Select * 
	into #CalcResults 
	from (
	Select distinct [Schedule], Rules_Option, Rules_Type, Result_Data_Name_FK, Result_to_Calc, rules_id, [Rule Name], [Rule Effective Date]--, [Cost Basis]
	, Calc_Basis, Calc_Basis_DN_FK, [Basis_Value], [Basis_Eff_Date], Operand, Operation
	, Round(Whsle + Case when ((ltrim(rtrim(Operation)) like '%MULTIPLIER%' and Cast(Operand as float) = 1) or (Whsle = Round(Whsle, 2))) then 0 else 0.005 end, 2) Calculated_Result
	, Precedence, [Item ID], [Customer]
	, Product_Structure_fk, Case when Cust_Entity_Structure_FK is null then null else Cust_Entity_Structure_fk end Cust_Entity_Structure_FK, Cust_Segment_Data_Name_FK, Operation_FK, DRank_Unique_Rule, Cast(Null as datetime) Update_Timestamp
	, Case when event_fk is null then Null else Event_FK end 'Event_FK'
	From(
	Select r.Result_Data_Name_FK, r.rules_id, r.name 'Rule Name', Cast(r.EFFECTIVE_DATE as date) 'Rule Effective Date'
	, dn_cost.label 'Calc_Basis', dn_cost.data_name_id 'Calc_Basis_DN_FK', Case when ra.BASIS_CALC_DN_FK = 503201 then null else pbv.value end 'Basis_Value', ra.basis1_Number 'Operand', ltrim(rtrim(rao.name)) 'Operation'
	, Case when sc.Data_Name_FK = 502036 or ra.BASIS_CALC_DN_FK in (503111, 503201) then 2 when r.result_data_name_fk = 503110 then 0 else 1 end 'Schedule'
	, Case when ra.BASIS_CALC_DN_FK = 503201 then null else
			Cast(Case	when rules_action_operation1_fk = 1071 then pbv.value + ra.Basis1_Number 
				When rules_action_operation1_fk = 1031 then pbv.value * ra.Basis1_Number
				When rules_action_operation1_fk = 1051 then pbv.value / (1 - ra.Basis1_Number)
				When rules_action_operation1_fk = 1061 then pbv.value * (1 + ra.Basis1_Number) end as Numeric(18, 8)) end Whsle
	, pbv.Effective_Date 'Basis_Eff_Date', r.precedence
	, Dense_Rank()over(partition by sp.product_structure_fk, sc.cust_entity_structure_fk, r.rules_id order by precedence_Rules_scheme) 'DRank_Unique_Rule'
	, pi.value 'Item ID', ei.value 'Customer'
	, sp.PRODUCT_STRUCTURE_FK, sc.CUST_ENTITY_STRUCTURE_FK
	, r.Cust_Segment_Data_Name_FK
	, dn_result.label 'Result_to_Calc'
	, dv_rs.value 'Rules_Option'
	, dv_rt.value 'Rules_Type'
--	, r.comment 'Rule_Type'
	, ra.rules_action_operation1_fk Operation_FK
	, ec.event_id event_fk
	from #Rules r
	inner join synchronizer.rules_scheme rs on r.Rules_type_cust_segment_fk = rs.Rules_type_Data_Value_FK
	inner join synchronizer.rules_action ra on r.rules_id = ra.rules_fk
	inner join epacube.segments_product sp on r.prod_segment_fk = sp.prod_segment_fk
	inner join epacube.segments_product sp_rs on sp.PRODUCT_STRUCTURE_FK = sp_rs.product_structure_fk and rs.rules_scheme_prod_segment_fk = sp_rs.PROD_SEGMENT_FK
	inner join epacube.segments_customer sc on r.cust_segment_fk = sc.cust_segment_fk 
	inner join epacube.segments_customer sc_rs on sc.cust_entity_structure_fk = sc_rs.CUST_ENTITY_STRUCTURE_FK
			and sc_rs.CUST_SEGMENT_FK = rs.Rules_scheme_Data_value_fk
	inner join epacube.data_value dv_rs on sc_rs.cust_segment_fk = dv_rs.DATA_VALUE_ID and sc_rs.data_name_fk = dv_rs.data_name_fk and (dv_rs.data_name_fk = 502038 or (dv_rs.data_name_fk = 502043 and dv_rs.value = '10'))
	inner join epacube.product_identification pi with (nolock) on sp.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110103
	inner join epacube.entity_identification ei with (nolock) on sc.CUST_ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
	inner join synchronizer.RULES_ACTION_OPERATION rao on ra.RULES_ACTION_OPERATION1_FK = rao.RULES_ACTION_OPERATION_ID
	left join #PBV PBV on sp.PRODUCT_STRUCTURE_FK = PBV.PRODUCT_STRUCTURE_FK and ra.BASIS_CALC_DN_FK = pbv.data_name_fk
	inner join epacube.epacube.data_name dn_result on r.RESULT_DATA_NAME_FK = dn_result.data_name_id
	left join epacube.epacube.data_name dn_cost on ra.BASIS_CALC_DN_FK = dn_cost.data_name_id
	left join epacube.epacube.data_value dv_rt on rs.Rules_Type_Data_value_fk = dv_rt.DATA_VALUE_ID
	inner join epacube.synchronizer.event_calc ec on sp.product_structure_fk = ec.product_structure_fk and sc.CUST_ENTITY_STRUCTURE_FK = ec.CUST_ENTITY_STRUCTURE_FK
	where 1 = 1
	and ec.job_fk = @Job_FK
	) Wholesales 
	Where Wholesales.DRank_Unique_Rule = 1
	) Z where 1 = 1

	Create index idx_bc on #CalcResults(Product_Structure_FK, Cust_Entity_Structure_FK, schedule, result_data_name_fk, result_to_calc, Calc_Basis)

	Insert into #CalcResults 
	Select * 
	from (
	Select distinct [Schedule], Rules_Option, Rules_Type, Result_Data_Name_FK, Result_to_Calc, rules_id, [Rule Name], [Rule Effective Date]--, [Cost Basis]
	, Calc_Basis, Calc_Basis_DN_FK, [Basis_Value], [Basis_Eff_Date], Operand, Operation
	, Round(Whsle + Case when ((ltrim(rtrim(Operation)) like '%MULTIPLIER%' and Cast(Operand as float) = 1) or (Whsle = Round(Whsle, 2))) then 0 else 0.005 end, 2) Calculated_Result
	, Precedence, [Item ID], [Customer]
	, Product_Structure_fk, Cust_Entity_Structure_fk
	, Cust_Segment_Data_Name_FK, Operation_FK, DRank_Winning_Member_Rule, Cast(Null as datetime) Update_Timestamp, event_fk
	From(
	Select r.Result_Data_Name_FK, r.rules_id, r.name 'Rule Name', Cast(r.EFFECTIVE_DATE as date) 'Rule Effective Date'
	, dn_cost.label 'Calc_Basis', dn_cost.data_name_id 'Calc_Basis_DN_FK', pbv.value 'Basis_Value', ra.basis1_Number 'Operand', ltrim(rtrim(rao.name)) 'Operation'
	, 0 'Schedule'
	, Cast(Case	when rules_action_operation1_fk = 1071 then pbv.value + ra.Basis1_Number 
			When rules_action_operation1_fk = 1031 then pbv.value * ra.Basis1_Number
			When rules_action_operation1_fk = 1051 then pbv.value / (1 - ra.Basis1_Number)
			When rules_action_operation1_fk = 1061 then pbv.value * (1 + ra.Basis1_Number) end as Numeric(18, 8)) Whsle
	, pbv.Effective_Date 'Basis_Eff_Date', r.precedence
	, Dense_Rank()over(partition by sp.product_structure_fk order by r.precedence) 'DRank_Winning_Member_Rule'
	, pi.value 'Item ID', Null 'Customer'
	, sp.PRODUCT_STRUCTURE_FK, Null 'CUST_ENTITY_STRUCTURE_FK'
	, r.Cust_Segment_Data_Name_FK
	, dn_result.label 'Result_to_Calc'
	, dv_rs.value 'Rules_Option'
	, dv_rt.value 'Rules_Type'
--	, r.comment 'Rule_Type'
	, ra.rules_action_operation1_fk Operation_FK
	, Null event_fk
	from #Rules r
	inner join synchronizer.rules_scheme rs on r.Rules_type_cust_segment_fk = rs.Rules_type_Data_Value_FK
	inner join synchronizer.rules_action ra on r.rules_id = ra.rules_fk
	inner join epacube.segments_product sp on r.prod_segment_fk = sp.prod_segment_fk
	inner join epacube.segments_product sp_rs on sp.PRODUCT_STRUCTURE_FK = sp_rs.product_structure_fk and rs.rules_scheme_prod_segment_fk = sp_rs.PROD_SEGMENT_FK
	inner join epacube.segments_customer sc on r.cust_segment_fk = sc.cust_segment_fk 
	--inner join epacube.segments_customer sc_rs on sc.cust_entity_structure_fk = sc_rs.CUST_ENTITY_STRUCTURE_FK
	--		and sc_rs.CUST_SEGMENT_FK = rs.Rules_scheme_Data_value_fk
	inner join epacube.data_value dv_rs on r.cust_segment_fk = dv_rs.DATA_VALUE_ID and r.Cust_Segment_Data_Name_FK = dv_rs.data_name_fk and dv_rs.data_name_fk = 502043 and dv_rs.value = '10'
	inner join epacube.product_identification pi with (nolock) on sp.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110103
	--inner join epacube.entity_identification ei with (nolock) on sc.CUST_ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
	inner join synchronizer.RULES_ACTION_OPERATION rao on ra.RULES_ACTION_OPERATION1_FK = rao.RULES_ACTION_OPERATION_ID
	left join #PBV PBV on sp.PRODUCT_STRUCTURE_FK = PBV.PRODUCT_STRUCTURE_FK and ra.BASIS_CALC_DN_FK = pbv.data_name_fk
	inner join epacube.epacube.data_name dn_result on r.RESULT_DATA_NAME_FK = dn_result.data_name_id
	left join epacube.epacube.data_name dn_cost on ra.BASIS_CALC_DN_FK = dn_cost.data_name_id
	left join epacube.epacube.data_value dv_rt on rs.Rules_Type_Data_value_fk = dv_rt.DATA_VALUE_ID
	where 1 = 1
			and pi.PRODUCT_STRUCTURE_FK in (Select Product_structure_fk from #CalcResults where Calc_Basis_DN_FK = 503201)
			and dv_rs.data_name_fk = 502043 
			and dv_rs.value = '10'
	) Wholesales 
	Where Wholesales.DRank_Winning_Member_Rule = 1
	) Z where 1 = 1

	Alter Table #CalcResults Add [RowNum] Bigint identity(1, 1) Not Null
	Alter Table #CalcResults Add [indScheduleResult] Smallint Null

	Update CR
	Set Update_Timestamp = getdate()
	from #CalcResults CR where Calculated_Result is not null

	Update CR1
	Set	Basis_Value = CR0.[Calculated_Result]
	, [Basis_Eff_Date] = CR0.[Basis_Eff_Date]
	, Calculated_Result = Cast(Case	when CR1.Operation_FK = 1071 then CR0.[Calculated_Result] + CR1.Operand 
								When CR1.Operation_FK = 1031 then CR0.[Calculated_Result] * CR1.Operand
								When CR1.Operation_FK = 1051 then CR0.[Calculated_Result] / (1 - CR1.Operand)
								When CR1.Operation_FK = 1061 then CR0.[Calculated_Result] * (1 + CR1.Operand) end as Numeric(18, 8)) 
	from #CalcResults CR1
	inner join 
		(
			Select * from (
			Select *,
			Dense_Rank()over(partition by Product_Structure_FK, Cust_Entity_Structure_FK, schedule, result_data_name_fk, result_to_calc, Calc_Basis order by Precedence, [rule effective date] desc) DRank_Eff
			from #CalcResults where schedule = 0
			) A where DRank_Eff = 1
		) CR0 on cr1.calc_basis_dn_fk = CR0.result_data_name_fk and CR0.product_structure_fk = CR1.PRODUCT_STRUCTURE_FK
	where 1 = 1
	and CR1.Calculated_Result is null
	and CR1.Calc_Basis_DN_FK <> 503111

		Declare @Event_FK bigint, @Rules_ID bigint, @Operand Numeric(18, 6), @Operation_FK bigint

		DECLARE ScheduleResultCursor CURSOR FOR 
		SELECT Event_FK, Rules_ID, Operand, Operation_FK from #CalcResults where schedule = 2 and Calculated_Result is null and Calc_Basis_DN_FK = 503111

		OPEN ScheduleResultCursor 

			FETCH NEXT FROM ScheduleResultCursor INTO @Event_FK, @Rules_ID, @Operand, @Operation_FK
			WHILE @@FETCH_STATUS = 0 
			BEGIN 

			Update CR_s
			Set Basis_Value = CR2.Basis_Value
			, [Basis_Eff_Date] = CR2.[Basis_Eff_Date]
			, Calculated_Result = Cast(Case	when @Operation_FK = 1071 then CR2.[Calculated_Result] + @Operand 
											When @Operation_FK = 1031 then CR2.[Calculated_Result] * @Operand
											When @Operation_FK = 1051 then CR2.[Calculated_Result] / (1 - @Operand)
											When @Operation_FK = 1061 then CR2.[Calculated_Result] * (1 + @Operand) end as Numeric(18, 8)) 
			, Update_Timestamp = getdate()
			From #CalcResults CR_s
			inner join 
				(
					Select * from (
					Select *,
					Dense_Rank()over(partition by Product_Structure_FK, Cust_Entity_Structure_FK, schedule, result_data_name_fk, result_to_calc order by Precedence, [rule effective date] desc) DRank_Eff
					from #CalcResults where schedule = 2 and Calculated_Result is not null
					) A where DRank_Eff = 1 
				) CR2 on CR_s.PRODUCT_STRUCTURE_FK = CR2.PRODUCT_STRUCTURE_FK and CR_s.CUST_ENTITY_STRUCTURE_FK = CR2.CUST_ENTITY_STRUCTURE_FK			
			where CR_s.Event_FK = @Event_FK	 
			
			Update CR_s
				Set Calculated_Result = Round(Calculated_Result + Case when ((ltrim(rtrim(@Operation_FK)) = 1031 and Cast(@Operand as float) = 1) or (Calculated_Result = Round(Calculated_Result, 2))) then 0 else 0.005 end, 2)
			From #CalcResults CR_s
			where CR_s.Event_FK = @Event_FK	
			
			FETCH NEXT FROM ScheduleResultCursor INTO @Event_FK, @Rules_ID, @Operand, @Operation_FK 
			END 

		CLOSE ScheduleResultCursor 
		DEALLOCATE ScheduleResultCursor

		Update CR
		Set indScheduleResult = 1
		from #CalcResults CR where Schedule = 2 and event_fk in (Select event_fk from #CalcResults where schedule = 2 and Calc_Basis_DN_FK = 503111)

		Update ec
		Set Result = CR.Calculated_Result
		, Rules_fk = CR.Rules_ID
		, Pricing_Cost_Basis_Amt = CR.Basis_Value
		, Margin_Cost_Basis_Amt = Case when cr.calc_Basis_DN_FK in (503102, 503110) then cr.Basis_Value When (Select count(*) from #CalcResults where Schedule = 2 and Event_fk = ec.event_id and Rules_Option in ('FSD', 'P1P')) > 0 then Cr0.Calculated_Result end
		from synchronizer.event_calc ec 
		inner join 
			(
				Select * from (
				Select Dense_rank()over(partition by event_fk order by Schedule, Case isnull(indScheduleResult, 0) when 0 then 10000 - Precedence else Update_timestamp end desc, calculated_result) 'DRank_WinningRule'
				, * from #CalcResults where schedule <> 0) A where DRank_WinningRule = 1
			) CR on ec.event_id = CR.event_fk
		inner join 
			(
				Select * from (
				Select Dense_rank()over(partition by event_fk order by Schedule, Case isnull(indScheduleResult, 0) when 0 then 10000 - Precedence else Update_timestamp end desc, calculated_result) 'DRank_WinningRule'
				, * from #CalcResults where schedule = 0) A where DRank_WinningRule = 1
			) CR0 on ec.event_id = CR0.event_fk
		where ec.job_fk = @Job_FK

		Update ec
		Set Result = CR.Calculated_Result
		, Rules_fk = CR.Rules_ID
		, Pricing_Cost_Basis_Amt = CR.Basis_Value
		, Margin_Cost_Basis_Amt = cr.Basis_Value
		from synchronizer.event_calc ec 
		inner join 
			(
				Select * from (
				Select Dense_rank()over(partition by event_fk order by Schedule, Case isnull(indScheduleResult, 0) when 0 then 10000 - Precedence else Update_timestamp end desc, calculated_result) 'DRank_WinningRule'
				, * from #CalcResults where schedule <> 0) A where DRank_WinningRule = 1
			) CR on ec.event_id = CR.event_fk
		where ec.job_fk = @Job_FK and ec.result is null

		--Select event_fk, [Item ID], Customer, Rules_Option, Rules_Type, [Rule Name], [Rule Effective Date], Calc_Basis, [Basis_Eff_Date], Basis_Value, operand, operation, Calculated_Result 
		--, Case when calc_Basis_DN_FK in (503102, 503110) then Basis_Value When Rules_Option in ('FSD', 'P1P') then (Select top 1 Calculated_Result from #CalcResults where schedule = 0 and event_fk = A.event_fk order by precedence) end 'Cost on Order Line'
		--, PRODUCT_STRUCTURE_FK, CUST_ENTITY_STRUCTURE_FK
		--from (
		--Select Dense_rank()over(partition by event_fk order by Schedule, Case isnull(indScheduleResult, 0) when 0 then 10000 - Precedence else Update_timestamp end desc, calculated_result) 'DRank_WinningRule'
		--, * from #CalcResults where schedule <> 0) A where DRank_WinningRule = 1
		--and Calculated_Result is not null
		--order by event_fk






