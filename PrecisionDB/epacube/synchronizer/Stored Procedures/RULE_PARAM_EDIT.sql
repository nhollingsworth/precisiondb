﻿








-- Copyright 2007
--
-- Procedure created by Rob Gannon
--
--
-- Purpose: Called from the UI to create/change/activate/inactivate rule parameters
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        08/27/2007   Initial SQL Version

--
CREATE PROCEDURE [synchronizer].[RULE_PARAM_EDIT] 
( @in_rule_params_id int, -- rule params id
  @in_application_scope_fk int , -- rule application scope
  @in_data_name_fk int, -- data name fk
  @in_precedence smallint, -- precedence
  @in_display_seq smallint, -- display sequence
  @in_record_status_cr_fk int,-- record status
  @in_ui_action_cr_fk int ) -- ui action	
												
AS

/***************  Parameter Defintions  **********************
CURRENTLY USED IN THE DATA MASTER EDITOR....

***************************************************************/

BEGIN

DECLARE @l_sysdate				datetime
DECLARE @ls_stmt				varchar (1000)
DECLARE @l_exec_no				bigint
DECLARE @l_rows_processed		bigint

DECLARE @in_org_filter_value    varchar (128)
DECLARE	@in_cust_filter_dn_fk   int
DECLARE @in_cust_filter_value   varchar (128)
DECLARE @in_supl_filter_dn_fk   int
DECLARE @in_supl_filter_value   varchar (128)

DECLARE @v_filter_data_value_fk int
DECLARE @v_product_structure_fk  int
DECLARE @v_mfr_entity_structure_fk int
DECLARE @v_org_entity_structure_fk int
DECLARE @v_cust_entity_structure_fk int
DECLARE @v_cust_filter_dv_fk int
DECLARE @v_supl_entity_structure_fk int
DECLARE @v_supl_filter_dv_fk int
DECLARE @v_table_name varchar(64)
DECLARE @v_filter_data_type_cr_fk int

DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int


SET NOCOUNT ON;
SET @l_sysdate = getdate ()
set @ErrorMessage = null


-------------------------------------------------------------------------------------
--   Peform UI Delete Action 
--   Currently (8/28/2007) The UI does not allow deletion; more validation is required
-------------------------------------------------------------------------------------

--
IF @in_ui_action_cr_fk = 8004  -- delete
BEGIN
	DELETE FROM epacube.rule_params
	WHERE rule_params_id = @in_rule_params_id	
END 
-- IF @in_ui_action_cr_fk = 8004

IF @in_ui_action_cr_fk in (8003,8006) --8003=create,8006=change
BEGIN
-------------------------------------------------------------------------------------
--   Check for Duplicate Prior to Insert
-------------------------------------------------------------------------------------
	IF @in_ui_action_cr_fk = 8003
	BEGIN
		select @ErrorMessage = 'Duplicate Rule Name. Rule Param ID = ' + 
         			+ CAST(isnull(RP.RULE_PARAMS_ID,0) AS VARCHAR(30)) 
		from epacube.rule_params RP
		where rp.application_scope_fk = @in_application_scope_fk
		and   rp.data_name_fk  = @in_data_name_fk		

		 IF @@rowcount > 0
				GOTO RAISE_ERROR
-------------------------------------------------------------------------------------
--   Edits passed Insert Row
-------------------------------------------------------------------------------------

		INSERT INTO epacube.rule_params
		   (application_scope_fk
		   ,data_name_fk
		   ,precedence		   
		   ,display_seq
		   ,record_status_cr_fk)
		SELECT		
			 @in_application_scope_fk , 
			 @in_data_name_fk , 
			 @in_precedence , 
             @in_display_seq , 
			 @in_record_status_cr_fk 
  
	END -- 	IF @in_ui_action_cr_fk = 8003
-------------------------------------------------------------------------------------
--   Edit only allows change to precedence/display_sequence. Precedence is a required
--	 input on the client side
-------------------------------------------------------------------------------------
	IF @in_ui_action_cr_fk = 8006
	BEGIN
-------------------------------------------------------------------------------------
--   Edits passed Update Row
-------------------------------------------------------------------------------------
		UPDATE epacube.rule_params
		SET precedence = @in_precedence
		   ,display_seq = @in_display_seq		  
	    WHERE rule_params_id = @in_rule_params_id

	END -- 	IF @in_ui_action_cr_fk = 8006


END --- IF @in_ui_action_cr_fk in (8003,8006)


-------------------------------------------------------------------------------------
--   Peform UI Activate Action 
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = 8007  -- activate
BEGIN

	UPDATE epacube.rule_params
	       set record_status_cr_fk = 1           
	       where rule_params_id = @in_rule_params_id	
		   return (0)
END -- IF @in_ui_action_cr_fk = 8007

-------------------------------------------------------------------------------------
--   Peform UI Inactivate Action. Currently no validation if Rule Param is in use.
-------------------------------------------------------------------------------------


IF @in_ui_action_cr_fk = 8005  -- inactivate
BEGIN

	UPDATE epacube.rule_params
	       set record_status_cr_fk = 2           
	       where rule_params_id = @in_rule_params_id	
		   return (0)
END -- IF @in_ui_action_cr_fk = 8005


RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END

































































