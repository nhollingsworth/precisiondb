﻿






-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing of derivations
--				Carried over V3 String Functions
--
--		NOTE::  OPERATION 300: DERIVE CONCATENATE ONLY
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010   Initial SQL Version
-- CV        09/09/2010   fixed type for substring rules
-- CV        03/20/2012   changed varchar128 to 256 in temp table
-- CV        01/20/2014   Adding UI logging
-- CV        01/18/2017   Changing Rules type and Adding Rules Control

CREATE PROCEDURE [synchronizer].[EVENT_POST_DERIVE_NEW_DATA_CONCATENATE] 
        ( @in_batch_no BIGINT, @in_calc_group_seq INT, @in_result_data_name_fk INT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT
DECLARE  @ls_exec             nvarchar(4000)

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime
Declare  @in_job_fk bigint

     DECLARE @V_START_TIMESTAMP  DATETIME
     DECLARE @V_END_TIMESTAMP    DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_NEW_DATA_CONCATENATE.' + ' Batch No = '
					 + cast(isnull(@in_batch_no, 0) as varchar(30))
					 +' Calc Seq: '	 + cast(isnull(@in_calc_group_seq, 0) as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;

		


SET @V_START_TIMESTAMP = getdate()

set @in_job_fk = (select top 1 import_job_fk from synchronizer.event_data where BATCH_NO = @in_batch_no)

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'START EVENT POST DERIVE CONCATENATE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @v_START_TIMESTAMP, @V_END_TIMESTAMP;




---------------------------------------------------
-- Temp Table for derivation columns
---------------------------------------------------


--drop temp tables
IF object_id('tempdb..#TS_DERIVE_DATA') is not null
   drop table #TS_DERIVE_DATA

CREATE TABLE #TS_DERIVE_DATA(
	[TS_DERIVE_DATA_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[EVENT_FK] [bigint] NULL,		
	[RULES_FK] [bigint] NULL,
	[RESULT_DATA_NAME_FK] [int] NULL,
	[RESULT_DATA_NAME2_FK] [int] NULL,									
	[RESULT] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		
	[BASIS1_IND] [smallint] NULL,	
	[BASIS2_IND] [smallint] NULL,	
	[BASIS3_IND] [smallint] NULL,			
	[BASIS1_RESULT] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		
	[BASIS2_RESULT] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		
	[BASIS3_RESULT] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,			
	[BASIS1_STRING] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		
	[BASIS2_STRING] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		
	[BASIS3_STRING] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,				
	[BASIS1_DN_VALUE] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BASIS2_DN_VALUE] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,	
	[BASIS3_DN_VALUE] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		
	[BASIS1_VALUE] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BASIS2_VALUE] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,	
	[BASIS3_VALUE] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		
	[PREFIX1] [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,			
	[PREFIX2] [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,			
	[PREFIX3] [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,					
	[RULES_ACTION_STRFUNCTION_FK1] [bigint] NULL,
	[RULES_ACTION_STRFUNCTION_FK2] [bigint] NULL,		
	[RULES_ACTION_STRFUNCTION_FK3] [bigint] NULL
    )



---------------------------------------------------
--  Insert basis data
---------------------------------------------------

TRUNCATE TABLE #TS_DERIVE_DATA

INSERT INTO #TS_DERIVE_DATA (
	EVENT_FK,
	RULES_FK,
	RESULT_DATA_NAME_FK,	
	RESULT_DATA_NAME2_FK,		
	BASIS1_IND,
	BASIS2_IND,
	BASIS3_IND,		
	BASIS1_STRING,
	BASIS2_STRING,
	BASIS3_STRING,	
	BASIS1_DN_VALUE,
	BASIS2_DN_VALUE,
	BASIS3_DN_VALUE,	
	BASIS1_VALUE,
	BASIS2_VALUE,
	BASIS3_VALUE,	
    PREFIX1,
    PREFIX2, 
    PREFIX3,   	
    RULES_ACTION_STRFUNCTION_FK1,
    RULES_ACTION_STRFUNCTION_FK2,
    RULES_ACTION_STRFUNCTION_FK3
) 
--------------------------
SELECT 
     A.EVENT_ID
    ,A.RULES_ID
    ,A.RESULT_DATA_NAME_FK
    ,A.RESULT_DATA_NAME2_FK    
    ,CASE ISNULL ( COALESCE ( A.BASIS1_VALUE, A.BASIS1_DN_VALUE ), 'NULL DATA' )
     WHEN 'NULL DATA' THEN 0
     ELSE 1
     END AS BASIS1_IND
    ,CASE ISNULL ( COALESCE ( A.BASIS2_VALUE, A.BASIS2_DN_VALUE ), 'NULL DATA' )
     WHEN 'NULL DATA' THEN 0
     ELSE 1
     END AS BASIS2_IND
    ,CASE ISNULL ( COALESCE ( A.BASIS3_VALUE, A.BASIS3_DN_VALUE ), 'NULL DATA' )
     WHEN 'NULL DATA' THEN 0
     ELSE 1
     END AS BASIS3_IND
    ,ISNULL ( A.BASIS1_VALUE, A.BASIS1_DN_VALUE ) 
    ,ISNULL ( A.BASIS2_VALUE, A.BASIS2_DN_VALUE )
    ,ISNULL ( A.BASIS3_VALUE, A.BASIS3_DN_VALUE )          
	,A.BASIS1_VALUE
	,A.BASIS2_VALUE
	,A.BASIS3_VALUE
    ,A.BASIS1_DN_VALUE
    ,A.BASIS2_DN_VALUE
	,A.BASIS3_DN_VALUE    
    ,A.PREFIX1
    ,A.PREFIX2
    ,A.PREFIX3    
    ,A.RULES_ACTION_STRFUNCTION_FK1 
    ,A.RULES_ACTION_STRFUNCTION_FK2
    ,A.RULES_ACTION_STRFUNCTION_FK3
--------------------------
FROM (
SELECT 
     ED.EVENT_ID
    ,R.RULES_ID
    ,R.RESULT_DATA_NAME_FK
    ,R.RESULT_DATA_NAME2_FK    
	,RA.BASIS1_VALUE
	,RA.BASIS2_VALUE
	,RA.BASIS3_VALUE
	,RA.BASIS1_DN_FK
	,RA.BASIS2_DN_FK
	,RA.BASIS3_DN_FK
    ,CASE ISNULL ( RA.BASIS1_DN_FK, 0 )
	 WHEN 0 THEN NULL
	 ELSE ( 
	        SELECT ISNULL (
				( SELECT TOP 1 EDX.NEW_DATA
				  FROM synchronizer.EVENT_DATA EDX  --- BASIS EVENT
				  INNER JOIN EPACUBE.DATA_NAME DNX ON DNX.DATA_NAME_ID = EDX.DATA_NAME_FK
				  INNER JOIN EPACUBE.DATA_SET DSX ON DSX.DATA_SET_ID = DNX.DATA_SET_FK
				  WHERE EDX.DATA_NAME_FK = RA.BASIS1_DN_FK
				  AND   EDX.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
				  AND   (   EDX.ORG_ENTITY_STRUCTURE_FK = 1 AND ED.ORG_ENTITY_STRUCTURE_FK <> 1
				         OR EDX.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK )
				  AND   (   ISNULL ( EDX.IMPORT_JOB_FK, -999 ) = ISNULL ( ED.IMPORT_JOB_FK, -999 )
				         OR ISNULL ( EDX.BATCH_NO, -999 ) = ISNULL ( ED.BATCH_NO, -999 ) )
				  AND   EDX.NEW_DATA IS NOT NULL 
				  ORDER BY EDX.ORG_ENTITY_STRUCTURE_FK DESC )   --- PICK WHSE ORG IF BOTH CORP AND WHSE BASIS VALUES 
		  ,
	      ( SELECT ISNULL (
				( SELECT TOP 1 VCPDX.CURRENT_DATA
				  FROM EPACUBE.V_CURRENT_PRODUCT_DATA VCPDX
				  INNER JOIN EPACUBE.DATA_NAME DNX ON DNX.DATA_NAME_ID = VCPDX.DATA_NAME_FK
				  INNER JOIN EPACUBE.DATA_SET DSX ON DSX.DATA_SET_ID = DNX.DATA_SET_FK
				  WHERE VCPDX.DATA_NAME_FK = RA.BASIS1_DN_FK
				  AND   VCPDX.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
				  AND   (   VCPDX.ORG_ENTITY_STRUCTURE_FK = 1 AND ED.ORG_ENTITY_STRUCTURE_FK <> 1
				         OR VCPDX.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK )
				  AND   VCPDX.CURRENT_DATA IS NOT NULL 
				  ORDER BY VCPDX.ORG_ENTITY_STRUCTURE_FK DESC )   --- PICK WHSE ORG IF BOTH CORP AND WHSE BASIS VALUES 				  
	     ,
	     ( SELECT TOP 1  VCEDX.CURRENT_DATA   -- SUPPLIER ONLY FOR RIGHT NOW 
				  FROM EPACUBE.V_CURRENT_ENTITY_DATA VCEDX
				  WHERE VCEDX.DATA_NAME_FK = RA.BASIS1_DN_FK
				  AND   VCEDX.ENTITY_STRUCTURE_FK IN  (
							 SELECT ISNULL (
							 ( SELECT TOP 1 EDZ.ENTITY_STRUCTURE_FK
							   FROM synchronizer.EVENT_DATA EDZ WITH (NOLOCK)
							    WHERE EDZ.DATA_NAME_FK = R.Vendor_Segment_Data_Name_FK
							   --WHERE EDZ.DATA_NAME_FK = RFS.SUPL_FILTER_ASSOC_DN_FK
							   AND   EDZ.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
							   AND   (   EDZ.ORG_ENTITY_STRUCTURE_FK = 1 AND ED.ORG_ENTITY_STRUCTURE_FK <> 1
									  OR EDZ.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK ) )
							  ,
								  ( SELECT TOP 1 PAS.ENTITY_STRUCTURE_FK 
									FROM EPACUBE.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
									 WHERE PAS.DATA_NAME_FK = R.Vendor_Segment_Data_Name_FK
									----WHERE PAS.DATA_NAME_FK = RFS.SUPL_FILTER_ASSOC_DN_FK 
									AND   PAS.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
									AND   (   PAS.ORG_ENTITY_STRUCTURE_FK = 1 AND ED.ORG_ENTITY_STRUCTURE_FK <> 1
										   OR PAS.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK ) )
								   ) )
				  AND   VCEDX.CURRENT_DATA IS NOT NULL )
	))))
	END BASIS1_DN_VALUE
----------------
	,CASE ISNULL ( RA.BASIS2_DN_FK, 0 )
	 WHEN 0 THEN NULL
	 ELSE ( 
	        SELECT ISNULL (
				( SELECT TOP 1 EDX.NEW_DATA
				  FROM synchronizer.EVENT_DATA EDX  --- BASIS EVENT
				  INNER JOIN EPACUBE.DATA_NAME DNX ON DNX.DATA_NAME_ID = EDX.DATA_NAME_FK
				  INNER JOIN EPACUBE.DATA_SET DSX ON DSX.DATA_SET_ID = DNX.DATA_SET_FK
				  WHERE EDX.DATA_NAME_FK = RA.BASIS2_DN_FK
				  AND   EDX.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
				  AND   (   EDX.ORG_ENTITY_STRUCTURE_FK = 1 AND ED.ORG_ENTITY_STRUCTURE_FK <> 1
				         OR EDX.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK )
				  AND   (   ISNULL ( EDX.IMPORT_JOB_FK, -999 ) = ISNULL ( ED.IMPORT_JOB_FK, -999 )
				         OR ISNULL ( EDX.BATCH_NO, -999 ) = ISNULL ( ED.BATCH_NO, -999 ) )
				  AND   EDX.NEW_DATA IS NOT NULL 
				  ORDER BY EDX.ORG_ENTITY_STRUCTURE_FK DESC )   --- PICK WHSE ORG IF BOTH CORP AND WHSE BASIS VALUES 
		  ,
	      ( SELECT ISNULL (
				( SELECT TOP 1 VCPDX.CURRENT_DATA
				  FROM EPACUBE.V_CURRENT_PRODUCT_DATA VCPDX
				  INNER JOIN EPACUBE.DATA_NAME DNX ON DNX.DATA_NAME_ID = VCPDX.DATA_NAME_FK
				  INNER JOIN EPACUBE.DATA_SET DSX ON DSX.DATA_SET_ID = DNX.DATA_SET_FK
				  WHERE VCPDX.DATA_NAME_FK = RA.BASIS2_DN_FK
				  AND   VCPDX.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
				  AND   (   VCPDX.ORG_ENTITY_STRUCTURE_FK = 1 AND ED.ORG_ENTITY_STRUCTURE_FK <> 1
				         OR VCPDX.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK )
				  AND   VCPDX.CURRENT_DATA IS NOT NULL 
				  ORDER BY VCPDX.ORG_ENTITY_STRUCTURE_FK DESC )   --- PICK WHSE ORG IF BOTH CORP AND WHSE BASIS VALUES 				  
	     ,
	     ( SELECT TOP 1  VCEDX.CURRENT_DATA   -- SUPPLIER ONLY FOR RIGHT NOW 
				  FROM EPACUBE.V_CURRENT_ENTITY_DATA VCEDX
				  WHERE VCEDX.DATA_NAME_FK = RA.BASIS2_DN_FK
				  AND   VCEDX.ENTITY_STRUCTURE_FK IN  (
							 SELECT ISNULL (
							 ( SELECT TOP 1 EDZ.ENTITY_STRUCTURE_FK
							   FROM synchronizer.EVENT_DATA EDZ WITH (NOLOCK)
							   --WHERE EDZ.DATA_NAME_FK = RFS.SUPL_FILTER_ASSOC_DN_FK
							   WHERE EDZ.DATA_NAME_FK = R.Vendor_Segment_Data_Name_FK
							   AND   EDZ.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
							   AND   (   EDZ.ORG_ENTITY_STRUCTURE_FK = 1 AND ED.ORG_ENTITY_STRUCTURE_FK <> 1
									  OR EDZ.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK ) )
							  ,
								  ( SELECT TOP 1 PAS.ENTITY_STRUCTURE_FK 
									FROM EPACUBE.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
									 WHERE PAS.DATA_NAME_FK = R.Vendor_Segment_Data_Name_FK
									--WHERE PAS.DATA_NAME_FK = RFS.SUPL_FILTER_ASSOC_DN_FK 
									AND   PAS.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
									AND   (   PAS.ORG_ENTITY_STRUCTURE_FK = 1 AND ED.ORG_ENTITY_STRUCTURE_FK <> 1
										   OR PAS.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK ) )
								   ) )
				  AND   VCEDX.CURRENT_DATA IS NOT NULL )
	))))
	END  BASIS2_DN_VALUE
----------------
	,CASE ISNULL ( RA.BASIS3_DN_FK, 0 )
	 WHEN 0 THEN NULL
	 ELSE ( 
	        SELECT ISNULL (
				( SELECT TOP 1 EDX.NEW_DATA
				  FROM synchronizer.EVENT_DATA EDX  --- BASIS EVENT
				  INNER JOIN EPACUBE.DATA_NAME DNX ON DNX.DATA_NAME_ID = EDX.DATA_NAME_FK
				  INNER JOIN EPACUBE.DATA_SET DSX ON DSX.DATA_SET_ID = DNX.DATA_SET_FK
				  WHERE EDX.DATA_NAME_FK = RA.BASIS3_DN_FK
				  AND   EDX.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
				  AND   (   EDX.ORG_ENTITY_STRUCTURE_FK = 1 AND ED.ORG_ENTITY_STRUCTURE_FK <> 1
				         OR EDX.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK )
				  AND   (   ISNULL ( EDX.IMPORT_JOB_FK, -999 ) = ISNULL ( ED.IMPORT_JOB_FK, -999 )
				         OR ISNULL ( EDX.BATCH_NO, -999 ) = ISNULL ( ED.BATCH_NO, -999 ) )
				  AND   EDX.NEW_DATA IS NOT NULL 
				  ORDER BY EDX.ORG_ENTITY_STRUCTURE_FK DESC )   --- PICK WHSE ORG IF BOTH CORP AND WHSE BASIS VALUES 
		  ,
	      ( SELECT ISNULL (
				( SELECT TOP 1 VCPDX.CURRENT_DATA
				  FROM EPACUBE.V_CURRENT_PRODUCT_DATA VCPDX
				  INNER JOIN EPACUBE.DATA_NAME DNX ON DNX.DATA_NAME_ID = VCPDX.DATA_NAME_FK
				  INNER JOIN EPACUBE.DATA_SET DSX ON DSX.DATA_SET_ID = DNX.DATA_SET_FK
				  WHERE VCPDX.DATA_NAME_FK = RA.BASIS3_DN_FK
				  AND   VCPDX.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
				  AND   (   VCPDX.ORG_ENTITY_STRUCTURE_FK = 1 AND ED.ORG_ENTITY_STRUCTURE_FK <> 1
				         OR VCPDX.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK )
				  AND   VCPDX.CURRENT_DATA IS NOT NULL 
				  ORDER BY VCPDX.ORG_ENTITY_STRUCTURE_FK DESC )   --- PICK WHSE ORG IF BOTH CORP AND WHSE BASIS VALUES 				  
	     ,
	     ( SELECT TOP 1  VCEDX.CURRENT_DATA   -- SUPPLIER ONLY FOR RIGHT NOW 
				  FROM EPACUBE.V_CURRENT_ENTITY_DATA VCEDX
				  WHERE VCEDX.DATA_NAME_FK = RA.BASIS3_DN_FK
				  AND   VCEDX.ENTITY_STRUCTURE_FK IN  (
							 SELECT ISNULL (
							 ( SELECT TOP 1 EDZ.ENTITY_STRUCTURE_FK
							   FROM synchronizer.EVENT_DATA EDZ WITH (NOLOCK)
							   WHERE EDZ.DATA_NAME_FK = r.Vendor_Segment_Data_Name_FK
							   --WHERE EDZ.DATA_NAME_FK = RFS.SUPL_FILTER_ASSOC_DN_FK
							   AND   EDZ.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
							   AND   (   EDZ.ORG_ENTITY_STRUCTURE_FK = 1 AND ED.ORG_ENTITY_STRUCTURE_FK <> 1
									  OR EDZ.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK ) )
							  ,
								  ( SELECT TOP 1 PAS.ENTITY_STRUCTURE_FK 
									FROM EPACUBE.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
									 WHERE PAS.DATA_NAME_FK = R.Vendor_Segment_Data_Name_FK
									--WHERE PAS.DATA_NAME_FK = RFS.SUPL_FILTER_ASSOC_DN_FK 
									AND   PAS.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
									AND   (   PAS.ORG_ENTITY_STRUCTURE_FK = 1 AND ED.ORG_ENTITY_STRUCTURE_FK <> 1
										   OR PAS.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK ) )
								   ) )
				  AND   VCEDX.CURRENT_DATA IS NOT NULL )
	))))
	END  BASIS3_DN_VALUE 
----------------
,RA.PREFIX1
,RA.PREFIX2
,RA.PREFIX3
,RASF1.RULES_ACTION_STRFUNCTION_ID AS RULES_ACTION_STRFUNCTION_FK1 
,RASF2.RULES_ACTION_STRFUNCTION_ID AS RULES_ACTION_STRFUNCTION_FK2 
,RASF3.RULES_ACTION_STRFUNCTION_ID AS RULES_ACTION_STRFUNCTION_FK3 
---------------
	FROM synchronizer.EVENT_DATA ED  WITH (NOLOCK) 
	INNER JOIN synchronizer.EVENT_DATA_RULES EDR  WITH (NOLOCK) 
	  ON ( EDR.EVENT_FK = ED.EVENT_ID
	  AND  EDR.RESULT_RANK = 1  )      --- ONLY THE SELECTED RULE
	INNER JOIN synchronizer.RULES R  WITH (NOLOCK) 
	  ON ( EDR.RULES_FK = R.RULES_ID )
	--INNER JOIN synchronizer.RULES_STRUCTURE RS  WITH (NOLOCK) 
	--  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
	--INNER JOIN synchronizer.RULES_HIERARCHY RH  WITH (NOLOCK) 
	--  ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
	--  AND  RH.RECORD_STATUS_CR_FK = 1 )
	--INNER JOIN synchronizer.RULES_FILTER_SET RFS  WITH (NOLOCK) 
	--  ON ( RFS.RULES_FK = R.RULES_ID )

	--left JOIN Epacube.SEGMENTS_VENDOR SV on isNULL(R.Vendor_Segment_FK,999) = ISNULL(sv.VENDOR_SEGMENT_FK,999)

	INNER JOIN synchronizer.RULES_ACTION RA  WITH (NOLOCK) 
	  ON ( RA.RULES_FK = R.RULES_ID )
	INNER JOIN 	epacube.DATA_NAME  DN1 WITH (NOLOCK)
	  ON ( DN1.DATA_NAME_ID = R.RESULT_DATA_NAME_FK  )	  
	LEFT JOIN synchronizer.RULES_ACTION_STRFUNCTION RASF1 WITH (NOLOCK) 
	  ON ( RASF1.RULES_ACTION_FK = RA.RULES_ACTION_ID
	  AND  RASF1.BASIS_POSITION = 1 )
	LEFT JOIN synchronizer.RULES_ACTION_STRFUNCTION RASF2 WITH (NOLOCK) 
	  ON ( RASF2.RULES_ACTION_FK = RA.RULES_ACTION_ID
	  AND  RASF2.BASIS_POSITION = 2 )
	LEFT JOIN synchronizer.RULES_ACTION_STRFUNCTION RASF3 WITH (NOLOCK) 
	  ON ( RASF3.RULES_ACTION_FK = RA.RULES_ACTION_ID
	  AND  RASF3.BASIS_POSITION = 3 )	
	  	LEFT JOIN Epacube.controls C on c.controls_id = r.controls_FK  
	WHERE 1 = 1
	AND   ED.BATCH_NO = @IN_BATCH_NO
	AND   ED.EVENT_SOURCE_CR_FK = 72 	
	AND   RA.RULES_ACTION_OPERATION1_FK = 300  --- DERIVE CONCATENATE 
	AND   ISNULL (c.control_precedence, 999999999 ) = @in_calc_group_seq
    --AND   ISNULL ( RH.CALC_GROUP_SEQ, 999999999 ) = @in_calc_group_seq	
    AND   R.RESULT_DATA_NAME_FK = @in_result_data_name_fk
) A 




---------------------------------------------------
-- Create Indexes  #TS_DERIVE_DATA
---------------------------------------------------








---------------------------------------------------
--  Perform String Functions  -- BASIS1
---------------------------------------------------

UPDATE #TS_DERIVE_DATA
	SET BASIS1_RESULT = CASE WHEN BASIS1_STRING IS NULL
	                    THEN NULL
	                    ELSE CASE ISNULL ( RULES_ACTION_STRFUNCTION_FK1, 0 )
							 WHEN 0 THEN CASE ISNULL ( PREFIX1, ' ' )
										WHEN ' ' THEN  BASIS1_STRING
										WHEN '?' THEN  ' ' + BASIS1_STRING
										ELSE PREFIX1 + BASIS1_STRING
										END
							 ELSE  ( SELECT epacube.ADD_BASIS_TOKEN 
											( 1, NULL, PREFIX1,  BASIS1_STRING, RULES_ACTION_STRFUNCTION_FK1 ) )
							 END
	                    END 
WHERE BASIS1_IND = 1  
             

		
----           TRUNCATE TABLE COMMON.T_SQL  

       SET @ls_exec = ( SELECT TOP 1 BASIS1_RESULT
                            FROM #TS_DERIVE_DATA 
                            WHERE BASIS1_IND = 1 )
     
       INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
       VALUES ( GETDATE(), ISNULL ( @ls_exec, 'SYNCHRONIZER.EVENT_POST_DERIVE_NEW_DATA BASIS1_RESULT IS NULL' )  )

	                   

----------------------------------------------------------------------
--  Perform String Functions  -- BASIS2
-----------------------------------------------------------------------

UPDATE #TS_DERIVE_DATA
	SET BASIS2_RESULT = CASE WHEN BASIS2_STRING IS NULL
	                    THEN NULL
	                    ELSE CASE ISNULL ( RULES_ACTION_STRFUNCTION_FK2, 0 )
							 WHEN 0 THEN CASE ISNULL ( PREFIX2, ' ' )
										WHEN ' ' THEN  BASIS1_RESULT + BASIS2_STRING
										WHEN '?' THEN  BASIS1_RESULT + ' ' + BASIS2_STRING
										ELSE BASIS1_RESULT + PREFIX2 + BASIS2_STRING
										END
							 ELSE  ( SELECT epacube.ADD_BASIS_TOKEN 
											( 2, BASIS1_RESULT, PREFIX2, BASIS2_STRING, RULES_ACTION_STRFUNCTION_FK2 ) )
							 END
	                    END 
WHERE BASIS2_IND = 1  
	                    

       SET @ls_exec = ( SELECT TOP 1 BASIS2_RESULT
                            FROM #TS_DERIVE_DATA 
                            WHERE BASIS2_IND = 1 )
     
       INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
       VALUES ( GETDATE(), ISNULL ( @ls_exec, 'SYNCHRONIZER.EVENT_POST_DERIVE_NEW_DATA BASIS2_RESULT IS NULL' )  )
	         
	                    
----------------------------------------------------------------------------------------------------------
--  Perform String Functions  -- BASIS3 
----------------------------------------------------------------------------------------------------------

UPDATE #TS_DERIVE_DATA
	SET BASIS3_RESULT = CASE WHEN BASIS3_STRING IS NULL
	                    THEN NULL
	                    ELSE CASE ISNULL ( RULES_ACTION_STRFUNCTION_FK3, 0 )
							 WHEN 0 THEN CASE ISNULL ( PREFIX3, ' ' )
										WHEN ' ' THEN  BASIS2_RESULT + BASIS3_STRING
										WHEN '?' THEN  BASIS2_RESULT + ' ' + BASIS3_STRING
										ELSE BASIS2_RESULT + PREFIX3 + BASIS3_STRING
										END
							 ELSE  ( SELECT epacube.ADD_BASIS_TOKEN 
											( 3, BASIS2_RESULT, PREFIX3, BASIS3_STRING, RULES_ACTION_STRFUNCTION_FK3  ) )
							 END
	                    END 
WHERE BASIS3_IND = 1  



       SET @ls_exec = ( SELECT TOP 1 BASIS3_RESULT
                            FROM #TS_DERIVE_DATA 
                            WHERE BASIS3_IND = 1 )
     
       INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
       VALUES ( GETDATE(), ISNULL ( @ls_exec, 'SYNCHRONIZER.EVENT_POST_DERIVE_NEW_DATA BASIS3_RESULT IS NULL' )  )


---------------------------------------------------
--  Update Event
---------------------------------------------------


UPDATE #TS_DERIVE_DATA   
SET RESULT = BASIS1_RESULT   ----  RTRIM ( LTRIM ( BASIS1_RESULT ) )  DO NOT TRIM SPACES
WHERE BASIS1_IND = 1
AND   BASIS2_IND = 0

UPDATE #TS_DERIVE_DATA   
SET RESULT = BASIS2_RESULT   ----  RTRIM ( LTRIM ( BASIS2_RESULT ) )  DO NOT TRIM SPACES
WHERE BASIS2_IND = 1
AND   BASIS3_IND = 0

UPDATE #TS_DERIVE_DATA   
SET RESULT = BASIS3_RESULT   ----  RTRIM ( LTRIM ( BASIS3_RESULT ) )  DO NOT TRIM SPACES
WHERE BASIS3_IND = 1



------------------------------------------------------------------------------------------
--   DERIVATIONS GO DIRECTLY INTO PERSTED TABLES
--      IF UNSUCCESSFUL... ( IE NEW_DATA IS NULL ) AUTO REJECT; LOG ERROR
--		OTHERWISE SET TO APPROVE
------------------------------------------------------------------------------------------


UPDATE synchronizer.EVENT_DATA
SET EVENT_STATUS_CR_FK = 82
   ,EVENT_PRIORITY_CR_FK = 60
   ,EVENT_CONDITION_CR_FK = 99
WHERE EVENT_ID IN ( 
		SELECT TSDD.EVENT_FK  
		FROM #TS_DERIVE_DATA   TSDD
		WHERE 1 = 1
		AND   TSDD.RESULT IS NULL  )

INSERT INTO synchronizer.EVENT_DATA_ERRORS (
	EVENT_FK,
	RULE_FK,
	EVENT_CONDITION_CR_FK,
	ERROR_NAME,
	UPDATE_TIMESTAMP)
SELECT 
		 TSDD.EVENT_FK
		,TSDD.RULES_FK
		,99
		,'DERIVED DATA IS NULL'  --ERROR NAME        
		,@l_sysdate
FROM #TS_DERIVE_DATA   TSDD
WHERE 1 = 1
AND   TSDD.RESULT IS NULL


UPDATE synchronizer.EVENT_DATA 
SET  NEW_DATA = TSDD.RESULT
    ,RULES_FK = TSDD.RULES_FK
    ,UPDATE_TIMESTAMP = GETDATE ()
FROM #TS_DERIVE_DATA TSDD
WHERE 1 = 1
AND   TSDD.EVENT_FK = synchronizer.EVENT_DATA.EVENT_ID
AND   TSDD.RESULT IS NOT NULL 




 
--drop temp tables
IF object_id('tempdb..#TS_DERIVE_DATA') is not null
   drop table #TS_DERIVE_DATA


SET @V_END_TIMESTAMP = getdate()



	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'END EVENT POST DERIVE CONCATENATE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @v_START_TIMESTAMP, @V_END_TIMESTAMP;




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_NEW_DATA_CONCATENATE'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_NEW_DATA_CONCATENATE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END





























