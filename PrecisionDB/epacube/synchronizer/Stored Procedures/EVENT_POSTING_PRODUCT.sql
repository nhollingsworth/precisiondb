﻿












-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Event processing for product events
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010   Initial SQL Version
-- KS		 03/04/2010   Comment out call to Conflict Validation until Tuned
-- KS        09/27/2011   Added Purge Action for UNDO 
-- CV        05/01/2012   Add Product mult type
-- KS        10/15/2012   EPA-3400 Added Delete for EVENT_DATA_RULES to remove duplicates
-- CV        05/14/2013   Add new procedure to product mult type
-- CV        06/28/2013   Tightened down around mult type logic
-- CV        08/20/2013   more issues around mult type
-- CV        08/30/2013   Needed to add table name to mult type section
-- CV        05/14/2014   Adding parent structure to hold the taxonomy node fk to product category section and to dynamic sql
-- CV        08/25/2014   adding end date to selection.
-- CV        01/13/2017   Rules type cr fk from rules table
-- CV        02/07/2017   Added Product Recipe Attribute Table


CREATE PROCEDURE [synchronizer].[EVENT_POSTING_PRODUCT] 
        ( @in_event_fk BIGINT, @in_post_ind SMALLINT, 
          @in_batch_no BIGINT, @in_import_job_fk BIGINT, 
          @in_event_priority_cr_fk INT, @in_event_type_cr_fk INT,
          @in_table_name VARCHAR(64), @in_data_name_fk BIGINT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime


DECLARE  @ls_exec1            nvarchar (max)
DECLARE  @ls_exec2            nvarchar (max)
DECLARE  @ls_exec3            nvarchar (max)

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_PRODUCT.'
					 + 'Event: ' + cast(isnull(@in_event_fk, 0) as varchar(30))
					 + 'Post Ind: ' + cast(isnull(@in_post_ind, 0) as varchar(30))					 
					 + 'Batch: ' + cast(isnull(@in_batch_no, 0) as varchar(30))
					 + 'Import: ' + cast(isnull(@in_import_job_fk, 0) as varchar(30))	
					 + 'Priority: ' + cast(isnull(@in_event_priority_cr_fk, 0) as varchar(30))					 				 
					 + 'Event Type: ' + cast(isnull(@in_event_type_cr_fk, 0) as varchar(30))					 				 
					 + 'Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(128))					 				 					 					 
					 + 'Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))							 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;



------            exec sp_executesql 	@ls_stmt  USE TO THROW ERROR

SET @l_sysdate = GETDATE()




------------------------------------------------------------------------------------------
--   Save this code snippet for causing immediate stop
------------------------------------------------------------------------------------------

----            SET @ls_exec = 'STOP CODE HERE'            
----            INSERT INTO COMMON.T_SQL
----                ( TS,  SQL_TEXT )
----            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
----            exec sp_executesql 	@ls_exec;



------------------------------------------------------------------------------------------
---  CLEAR TABLE FOR NEXT BATCH_NO, PRIORITY, JOB_FK, EVENT_TYPE
------------------------------------------------------------------------------------------

      TRUNCATE TABLE #TS_EVENT_DATA


--------------------------------------------------------------------------------------------
--   PRODUCT EVENTS  -- PRODUCT STRUCTURE
--------------------------------------------------------------------------------------------

IF @in_table_name = 'PRODUCT_STRUCTURE'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			IMPORT_PACKAGE_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.IMPORT_PACKAGE_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
		    PS.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  ( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			( SELECT CR.CODE FROM EPACUBE.CODE_REF CR
			  WHERE CR.CODE_REF_ID = PS.PRODUCT_STATUS_CR_FK  ) AS CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
			CASE WHEN ED.EVENT_ACTION_CR_FK = 59
			THEN 59
			ELSE CASE WHEN PS.UPDATE_TIMESTAMP IS NULL
				 THEN 51
				 ELSE 52
				 END
			END,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,
			ED.EVENT_SOURCE_CR_FK,
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			PS.PRODUCT_STRUCTURE_ID,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			LEFT JOIN epacube.PRODUCT_STRUCTURE PS WITH (NOLOCK)
			 ON ( PS.PRODUCT_STRUCTURE_ID = ED.PRODUCT_STRUCTURE_FK )
		WHERE 1 = 1 
		AND  ED.EVENT_STATUS_CR_FK = 88
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )




--------------------------------------------------------------------------------------------
--   PRODUCT ASS0CIATIONS 
--   CHECK SINGLE ASSOC IF SINGLE THEN CHANGE  ELSE ADD
--   CHECK IF ENTITY STRUCTURE FK IS NULL AND NEW_DATA IS NOT THEN TRY LOOKUP; ELSE USE ES_ID
--------------------------------------------------------------------------------------------

ELSE
IF @in_table_name = 'PRODUCT_ASSOCIATION'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			DATA_NAME_FK,
			IMPORT_JOB_FK,
			IMPORT_PACKAGE_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,			
			ENTITY_STRUCTURE_FK,
		    PARENT_ENTITY_STRUCTURE_FK,	
            CHILD_ENTITY_STRUCTURE_FK,
			CHILD_PRODUCT_STRUCTURE_FK,	
			ENTITY_DATA_VALUE_FK,							
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,		
			SINGLE_ASSOC_IND,			
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,	
			END_DATE_NEW,	
			RULES_FK,		
			RECORD_STATUS_CR_FK,		
			UPDATE_TIMESTAMP,
			UPDATE_USER
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.DATA_NAME_FK,
			ED.IMPORT_JOB_FK,
			ED.IMPORT_PACKAGE_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
			DS.ENTITY_STRUCTURE_EC_CR_FK AS ENTITY_CLASS_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			 CASE ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 )
			 WHEN 0 THEN NULL
			 WHEN 10101 THEN NULL
			 ELSE CASE ISNULL( ED.ENTITY_STRUCTURE_FK, 0 )		
					WHEN 0 THEN ( SELECT EI.ENTITY_STRUCTURE_FK
								  FROM EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
								  WHERE EI.VALUE = ED.NEW_DATA
								  AND   EI.ENTITY_DATA_NAME_FK = DN.ENTITY_DATA_NAME_FK
								  AND   EI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
															FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK)
															WHERE EEP.APPLICATION_SCOPE_FK = 100
															AND   EEP.NAME = ( SELECT CODE 
																			   FROM epacube.CODE_REF WITH (NOLOCK)
																			   WHERE CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
														 ) )
					ELSE ED.ENTITY_STRUCTURE_FK
					END			
			 END AS ENTITY_STRUCTURE_FK,
			--
            ED.PARENT_STRUCTURE_FK,  
            ED.ENTITY_STRUCTURE_FK,	
			ED.CHILD_PRODUCT_STRUCTURE_FK,		
			ED.ENTITY_DATA_VALUE_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
			PAS.UPDATE_TIMESTAMP,
			ED.NEW_DATA,
			 ( SELECT CASE DS.ENTITY_STRUCTURE_EC_CR_FK
			   WHEN 10101 THEN epacube.getEntityIdent ( PAS.ORG_ENTITY_STRUCTURE_FK )
			   ELSE epacube.getEntityIdent ( PAS.ENTITY_STRUCTURE_FK )
			   END ) AS  CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
			CASE WHEN ED.EVENT_ACTION_CR_FK = 59
			THEN 59
			ELSE
			 CASE ISNULL ( DN.SINGLE_ASSOC_IND, 0 )
			 WHEN 0 THEN CASE WHEN ED.NEW_DATA <> 
								( SELECT CASE DS.ENTITY_STRUCTURE_EC_CR_FK
								   WHEN 10101 THEN epacube.getEntityIdent ( PAS.ORG_ENTITY_STRUCTURE_FK )
								   ELSE epacube.getEntityIdent ( PAS.ENTITY_STRUCTURE_FK )
								   END )			 
			             THEN 51  --- THERE SHOULD NOT BE ANY CHANGES... ONLY ADDS
			             ELSE 58
			             END
			 WHEN 1 THEN CASE ISNULL ( PAS.PRODUCT_ASSOCIATION_ID, 0 )
						 WHEN 0 THEN 51
						 ELSE 52
						 END
			 END
			END,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),			
			DN.SINGLE_ASSOC_IND,
			ED.Table_id_FK,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,	
			ED.END_DATE_NEW,	
			ED.RULES_FK,			
			ED.RECORD_STATUS_CR_FK,			
			@L_SYSDATE,
			ED.UPDATE_USER
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		    LEFT JOIN EPACUBE.CODE_REF CR WITH (NOLOCK) ON ( CR.CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
	LEFT JOIN epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
	 ON ( PAS.DATA_NAME_FK = ED.DATA_NAME_FK
	 AND  ISNULL ( PAS.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( ED.PRODUCT_STRUCTURE_FK, 0 )
	 AND  (  ISNULL ( DS.ORG_IND, 0 ) = 0
	      OR ISNULL ( PAS.ORG_ENTITY_STRUCTURE_FK, 1 ) = ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 ) )	 -- WHSE SPECIFIC
	 AND  (  DS.ENTITY_STRUCTURE_EC_CR_FK = 10101          -- IF WHSE IGNORE ENTITY
	      OR (  ISNULL ( DN.SINGLE_ASSOC_IND, 0 ) = 1      -- IF SINGLE ASSOC THEN ENTITY IS VALUE;  UNIQUE KEY IS PROD/WHSE
             OR (  ISNULL ( PAS.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( ED.ENTITY_STRUCTURE_FK, 0 ) ) ) )
	  )
-----------------
------		    			 
------			LEFT JOIN epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
------			 ON ( PAS.DATA_NAME_FK = ED.DATA_NAME_FK
------			 AND  ISNULL ( PAS.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( ED.PRODUCT_STRUCTURE_FK, 0 )
------			 AND  ISNULL ( PAS.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
------			 AND  (   ISNULL ( DN.SINGLE_ASSOC_IND, 0 ) = 1
------			      OR
------			          ISNULL ( PAS.ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 )
------			      )
------			  )
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )


			  update #TS_EVENT_DATA
			  set record_status_cr_fk = 2 
			  ,event_action_Cr_fk = 53
			  where new_data = '-999999999'

			   update #TS_EVENT_DATA
			  set event_action_Cr_fk = 59
			  where new_data = 'PURGE'
					  


--------------------------------------------------------------------------------------------
--  PRODUCT MULT TYPE
---  Only inserts
-------------------------------------------------------------------------------------------


 DECLARE 
              @V_in_event_fk          bigint
             



      DECLARE  cur_v_event cursor local for
	  				select ed.event_id FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88	
		AND CHARINDEX(',', NEW_DATA) > 0	--------if there is a comma in the new data then go down the path....other wise do not.
		AND (	
			   (   ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL (  'PRODUCT_MULT_TYPE', 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )

			   
        OPEN cur_v_event;
        FETCH NEXT FROM cur_v_event INTO @v_in_event_fk

		
         WHILE @@FETCH_STATUS = 0 
         BEGIN
										
			  
IF @in_table_name = 'PRODUCT_MULT_TYPE'			  

BEGIN

SET @status_desc = ISNULL('START execution of SYNCHRONIZER.EVENT_FACTORY-PRODUCT_MULT_TYPE ' + CAST(@V_in_event_fk as varchar(20))
+ ' ' + cast(@in_data_name_fk as varchar(50)) + ' ' + Cast(@in_table_name as Varchar(200)), 'WE have issue')
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @in_batch_no
EXEC exec_monitor.print_status_sp  @l_exec_no;
			  			

EXEC [synchronizer].[EVENT_FACTORY_PROD_MULT_TYPE]
 @V_in_event_fk , @in_post_ind , 
          @in_batch_no, @in_import_job_fk, 
          @in_event_priority_cr_fk , @in_event_type_cr_fk,
          @in_table_name , @in_data_name_fk

	END	
	
--------------------------------------------------------------------------------------------------	



			 FETCH NEXT FROM cur_v_event INTO @v_in_event_fk									


     END --cur_v_event LOOP
     CLOSE cur_v_event;
	 DEALLOCATE cur_v_event; 



	
--------------------------------------------------------------------------------------------------	

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP,
			UPDATE_USER,
			DATA_LEVEL
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
			GETDATE(), --EFFECTIVE_DATE_CUR
			ED.data_value_fk  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			ED.Current_data AS CURRENT_DATA,
			
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			ED.TABLE_ID_FK,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE,
			ED.UPDATE_USER,
			ED.DATA_LEVEL

--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			----LEFT JOIN epacube.PRODUCT_MULT_TYPE EMT WITH (NOLOCK)
			---- ON ( EMT.DATA_NAME_FK = ED.DATA_NAME_FK
			 --AND  ISNULL ( EMT.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL (ED.PRODUCT_STRUCTURE_FK, 0 ) 
			----  AND  ISNULL ( EMT.ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 ) )
------	
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   --(    ISNULL ( @in_event_fk, -999 ) = -999
				--AND 
				(ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( 'PRODUCT_MULT_TYPE', 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )
			

------------------------------------------------------------------------------------------
---PRODUCT RECIPE ATTRIBUTES
------------------------------------------------------------------------------------------

--ELSE

IF @in_table_name = 'PRODUCT_RECIPE_ATTRIBUTE'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			DATA_NAME_FK,
			IMPORT_JOB_FK,
			IMPORT_PACKAGE_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,			
			ENTITY_STRUCTURE_FK,
		    PARENT_ENTITY_STRUCTURE_FK,	
            CHILD_ENTITY_STRUCTURE_FK,	
			PRODUCT_RECIPE_FK,							
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,		
			SINGLE_ASSOC_IND,			
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,	
			END_DATE_NEW,	
			RULES_FK,		
			RECORD_STATUS_CR_FK,		
			UPDATE_TIMESTAMP,
			UPDATE_USER
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.DATA_NAME_FK,
			ED.IMPORT_JOB_FK,
			ED.IMPORT_PACKAGE_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
			DS.ENTITY_STRUCTURE_EC_CR_FK AS ENTITY_CLASS_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			 CASE ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 )
			 WHEN 0 THEN NULL
			 WHEN 10101 THEN NULL
			 ELSE CASE ISNULL( ED.ENTITY_STRUCTURE_FK, 0 )		
					WHEN 0 THEN ( SELECT EI.ENTITY_STRUCTURE_FK
								  FROM EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
								  WHERE EI.VALUE = ED.NEW_DATA
								  AND   EI.ENTITY_DATA_NAME_FK = DN.ENTITY_DATA_NAME_FK
								  AND   EI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
															FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK)
															WHERE EEP.APPLICATION_SCOPE_FK = 100
															AND   EEP.NAME = ( SELECT CODE 
																			   FROM epacube.CODE_REF WITH (NOLOCK)
																			   WHERE CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
														 ) )
					ELSE ED.ENTITY_STRUCTURE_FK
					END			
			 END AS ENTITY_STRUCTURE_FK,
			--
            ED.PARENT_STRUCTURE_FK,  
            ED.ENTITY_STRUCTURE_FK,	
			ED.TABLE_ID_FK,		
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
			PR.UPDATE_TIMESTAMP,
			ED.NEW_DATA,
			pr.attribute_event_DATA  AS  CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
		    ED.EVENT_ACTION_CR_FK, 
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),			
			DN.SINGLE_ASSOC_IND,
			PR.PRODUCT_RECIPE_ATTRIBUTE_ID,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,	
			ED.END_DATE_NEW,	
			ED.RULES_FK,			
			ED.RECORD_STATUS_CR_FK,			
			@L_SYSDATE,
			ED.update_User
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		    LEFT JOIN EPACUBE.CODE_REF CR WITH (NOLOCK) ON ( CR.CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
	LEFT JOIN epacube.PRODUCT_RECIPE_ATTRIBUTE PR  WITH (NOLOCK)
	 ON ( PR.DATA_NAME_FK = ED.DATA_NAME_FK
	  AND  ISNULL ( PR.PRODUCT_Recipe_FK, 0 ) = ISNULL ( ED.TABLE_ID_FK, 0 )
	 AND  ISNULL ( PR.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( ED.PRODUCT_STRUCTURE_FK, 0 )
	 AND  ISNULL ( PR.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( ED.ENTITY_STRUCTURE_FK, 0 )  
	 )

		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )

------------------------------------------------------------------------------------------
---'PRODUCT_RECIPE_INGREDIENT_XREF'
------------------------------------------------------------------------------------------



IF @in_table_name in ( 'PRODUCT_RECIPE_INGREDIENT_XREF', 'PRODUCT_RECIPE_INGREDIENT')


			  INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			IMPORT_PACKAGE_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			--EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			 
			NEW_DATA,
			CURRENT_DATA,			
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			Chain_id_DATA_VALUE_fk,
			Search1,
			RULES_FK,	 	
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP,
			TABLE_ID_FK,
			PARENT_STRUCTURE_FK			
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			ED.EVENT_TYPE_CR_FK,
			ED.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.IMPORT_PACKAGE_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			ED.Import_filename,
			--CUR_DATA.UPDATE_TIMESTAMP,  
			ED.data_value_fk  AS DATA_VALUE_FK,
			 
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END  
			,ED.current_data
			,ED.EVENT_EFFECTIVE_DATE
			,ED.EVENT_ACTION_CR_FK  
			,ED.EVENT_STATUS_CR_FK,
			ED.EVENT_CONDITION_CR_FK,												
			ED.EVENT_SOURCE_CR_FK,
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.CHAIN_ID_DATA_VALUE_FK,
			ED.SEARCH1,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			GETDATE(),
			ED.table_id_fk  ,ED.PARENT_STRUCTURE_FK FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			 inner join epacube.DATA_NAME_GROUP_REFS R ON R.Data_Name_FK = ED.DATA_NAME_FK
				 left join epacube.code_ref cr on cr.code_type = 'DATA_LEVEL'
				 and isNULL(cr.code,999) = isNULL(ed.Data_level,999)
				 and ISNULL(r.data_level_cr_fk,999) = ISNULL(cr.code_ref_id,999)
					 
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			--LEFT JOIN epacube.PRODUCT_RECIPE_INGREDIENT_XREF AS CUR_DATA WITH (NOLOCK)
			-- ON ( CUR_DATA.DATA_NAME_FK = ED.DATA_NAME_FK
			-- AND  ISNULL ( CUR_DATA.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( ED.PRODUCT_STRUCTURE_FK, 0 )
			-- AND  ISNULL ( CUR_DATA.CHAIN_ID_DATA_VALUE_FK, 0 ) = ISNULL ( ED.CHAIN_ID_DATA_VALUE_FK, 0 )
			-- )
			  WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88
			AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( R.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )

			  
------------------------------------------------------------------------------------------
---SETTINGS_POS_PROD_STORE_VALUES
------------------------------------------------------------------------------------------

--ELSE

IF @in_table_name = 'SETTINGS_POS_PROD_STORE_HEADER'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			DATA_NAME_FK,
			IMPORT_JOB_FK,
			IMPORT_PACKAGE_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,			
			ENTITY_STRUCTURE_FK,
		    PARENT_ENTITY_STRUCTURE_FK,	
            CHILD_ENTITY_STRUCTURE_FK,	
			PRODUCT_RECIPE_FK,							
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,		
			SINGLE_ASSOC_IND,
			ASSOC_PARENT_CHILD,			
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,	
			END_DATE_NEW,	
			RULES_FK,		
			RECORD_STATUS_CR_FK,		
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			ED.EVENT_TYPE_CR_FK,
			ED.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.DATA_NAME_FK,
			ED.IMPORT_JOB_FK,
			ED.IMPORT_PACKAGE_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
			DS.ENTITY_STRUCTURE_EC_CR_FK AS ENTITY_CLASS_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			 CASE ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 )
			 WHEN 0 THEN NULL
			 WHEN 10101 THEN NULL
			 ELSE CASE ISNULL( ED.ENTITY_STRUCTURE_FK, 0 )		
					WHEN 0 THEN ( SELECT EI.ENTITY_STRUCTURE_FK
								  FROM EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
								  WHERE EI.VALUE = ED.NEW_DATA
								  AND   EI.ENTITY_DATA_NAME_FK = DN.ENTITY_DATA_NAME_FK
								  AND   EI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
															FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK)
															WHERE EEP.APPLICATION_SCOPE_FK = 100
															AND   EEP.NAME = ( SELECT CODE 
																			   FROM epacube.CODE_REF WITH (NOLOCK)
																			   WHERE CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
														 ) )
					ELSE ED.ENTITY_STRUCTURE_FK
					END			
			 END AS ENTITY_STRUCTURE_FK,
			--
            ED.PARENT_STRUCTURE_FK,  
            ED.ENTITY_STRUCTURE_FK,	
			ED.TABLE_ID_FK,		
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
			PR.UPDATE_TIMESTAMP,
			ED.NEW_DATA,
			pr.attribute_event_DATA  AS  CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
		    ED.EVENT_ACTION_CR_FK, 
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),			
			DN.SINGLE_ASSOC_IND,
			ED.ASSOC_PARENT_CHILD,
			PR.SETTINGS_POS_PROD_STORE_VALUES_ID,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,	
			ED.END_DATE_NEW,	
			ED.RULES_FK,			
			ED.RECORD_STATUS_CR_FK,			
			@L_SYSDATE
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		    LEFT JOIN EPACUBE.CODE_REF CR WITH (NOLOCK) ON ( CR.CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
	LEFT JOIN epacube.SETTINGS_POS_PROD_STORE_VALUES PR  WITH (NOLOCK)
	 ON ( PR.DATA_NAME_FK = ED.DATA_NAME_FK
	  AND  ISNULL ( PR.SETTINGS_POS_PROD_STORE_VALUES_ID, 0 ) = ISNULL ( ED.TABLE_ID_FK, 0 )
	 --AND  ISNULL ( PR.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( ED.PRODUCT_STRUCTURE_FK, 0 )
	 --AND  ISNULL ( PR.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( ED.ENTITY_STRUCTURE_FK, 0 )  
	 )

		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				--AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	
				)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )
   
   
--------------------------------------------------------------------------------------------
--   PRODUCT EVENTS  --  ALL OTHER TABLE NAMES
--------------------------------------------------------------------------------------------

 
BEGIN

 DECLARE @v_event_fk              AS VARCHAR(16)
        ,@v_batch_no              AS VARCHAR(16)
        ,@v_import_job_fk         AS VARCHAR (16)
        ,@v_event_priority_cr_fk  AS VARCHAR (16)
        
 SET     @v_event_fk              = CASE WHEN ( ISNULL ( @in_event_fk, 0 ) = 0 ) THEN '-999' ELSE @in_event_fk END
 SET     @v_batch_no              = CASE WHEN ( ISNULL ( @in_batch_no, 0 ) = 0 ) THEN '-999' ELSE @in_batch_no END
 SET     @v_import_job_fk         = CASE WHEN ( ISNULL ( @in_import_job_fk, 0 ) =  0 ) THEN '-999' ELSE @in_import_job_fk END
 SET     @v_event_priority_cr_fk  = CASE WHEN ( ISNULL ( @in_event_priority_cr_fk, 0 ) = 0 ) THEN '-999' ELSE @in_event_priority_cr_fk END

 SET @ls_exec2 = 	CASE @IN_TABLE_NAME
					WHEN 'PRODUCT_ATTRIBUTE' THEN ' CUR_DATA.ATTRIBUTE_EVENT_DATA '
			        WHEN 'PRODUCT_CATEGORY'  THEN ' CASE DS.DATA_TYPE_CR_FK                
													WHEN 134 THEN  ( SELECT DV.VALUE                    
																	 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)                    
																	 WHERE DV.DATA_VALUE_ID = CUR_DATA.DATA_VALUE_FK )                    
													WHEN 135 THEN  ( SELECT EDV.VALUE                    
																	 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)                    
																	 WHERE EDV.ENTITY_DATA_VALUE_ID = CUR_DATA.ENTITY_DATA_VALUE_FK )                    
													END '
					WHEN 'PRODUCT_IDENTIFICATION' THEN ' CUR_DATA.VALUE '
			        WHEN 'PRODUCT_IDENT_NONUNIQUE' THEN ' CUR_DATA.VALUE '
					WHEN 'PRODUCT_IDENT_MULT' THEN ' CUR_DATA.VALUE '
			        WHEN 'PRODUCT_DESCRIPTION' THEN ' CUR_DATA.DESCRIPTION '
			        WHEN 'PRODUCT_TEXT' THEN ' CUR_DATA.TEXT '
					WHEN 'PRODUCT_GTIN' THEN ' CUR_DATA.UPC_CODE '
			        WHEN 'PRODUCT_STATUS' THEN ' ( SELECT DV.VALUE FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
													WHERE DV.DATA_VALUE_ID = CUR_DATA.DATA_VALUE_FK  ) '
					WHEN 'PRODUCT_UOM_CLASS' THEN ' ( SELECT uom_code FROM epacube.uom_code WITH (NOLOCK) 
														WHERE uom_code_id = CUR_DATA.uom_code_fk ) '
				
					
  			        END 


SET @ls_exec3 = 	CASE @IN_TABLE_NAME
					WHEN 'PRODUCT_ATTRIBUTE' THEN ' ) '
			        WHEN 'PRODUCT_CATEGORY'  THEN '   AND  ISNULL ( CUR_DATA.Taxonomy_node_FK, 0 ) = ISNULL (ED.Parent_STRUCTURE_FK, 0 )) '
					WHEN 'PRODUCT_IDENTIFICATION' THEN ' ) '
			        WHEN 'PRODUCT_IDENT_NONUNIQUE' THEN ' ) '
					WHEN 'PRODUCT_IDENT_MULT' THEN ' ) '
			        WHEN 'PRODUCT_DESCRIPTION' THEN ' ) '
			        WHEN 'PRODUCT_TEXT' THEN ' ) '
			        WHEN 'PRODUCT_STATUS' THEN ' ) '
					WHEN 'PRODUCT_UOM_CLASS' THEN ' ) '
					WHEN 'PRODUCT_GTIN' THEN ' ) '
				
					END



---- DECLARE @v_org_entity_structure_fk VARCHAR(16)
---- SET  @v_org_entity_structure_fk = CAST ( @in_org_entity_structure_fk AS VARCHAR(16)

 SET @ls_exec1 = 
 'INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			IMPORT_PACKAGE_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,			
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
				END_DATE_CUR,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP,
			TABLE_ID_FK,
			PARENT_STRUCTURE_FK			
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.IMPORT_PACKAGE_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
			CUR_DATA.UPDATE_TIMESTAMP,  
			CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  ( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END, '
		    +   @ls_exec2
		    +  ', ED.EVENT_EFFECTIVE_DATE,
		    CASE WHEN ED.EVENT_ACTION_CR_FK = 59
		    THEN 59
		    ELSE
			CASE WHEN CUR_DATA.UPDATE_TIMESTAMP IS NULL
			THEN 51
			ELSE 52
			END
			END,
			ED.EVENT_STATUS_CR_FK,
			ED.EVENT_CONDITION_CR_FK,												
			ED.EVENT_SOURCE_CR_FK,
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
				CUR_DATA.END_DATE,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			GETDATE(),
			CUR_DATA.'
			+  @in_table_name
			+ '_ID  '
			+ ',ED.PARENT_STRUCTURE_FK '			
 +      'FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			LEFT JOIN epacube.'
		+  @in_table_name
		+  ' AS CUR_DATA WITH (NOLOCK)
			 ON ( CUR_DATA.DATA_NAME_FK = ED.DATA_NAME_FK
			 AND  ISNULL ( CUR_DATA.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( ED.PRODUCT_STRUCTURE_FK, 0 )
			 AND  ISNULL ( CUR_DATA.ORG_ENTITY_STRUCTURE_FK, 1 ) = ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			 AND  ISNULL ( CUR_DATA.ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 ) '
			 + @ls_exec3 + 
			 

		' WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( ' + @v_event_fk + ', -999 ) = -999 '
		+	   'AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( ' + @v_batch_no + ', -999 ) '
		+	   'AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL ( ' + @v_import_job_fk + ', -999 ) '
		+	   'AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull ( ' + @v_event_priority_cr_fk + ', -999) '
		+	   'AND ISNULL ( ds.table_name, ''NULL DATA'' )  = ISNULL ( ''' + @in_table_name + ''', ''NULL DATA'' )	) '
		+  'OR  ED.EVENT_ID = ISNULL ( ' + @v_event_fk + ', -999 ) ) '
		+  '		AND   (   '
        +  CAST ( @in_data_name_fk AS VARCHAR(16) )
        + ' = -999 OR  ED.data_name_fk = '
        +  CAST ( @in_data_name_fk AS VARCHAR(16) )
        + '  ) '


		update ed
set Parent_structure_FK= tn.TAXONOMY_NODE_ID 
from #TS_EVENT_DATA ED
inner join epacube.DATA_VALUE dv on (dv.DATA_VALUE_ID = ed.DATA_VALUE_FK)
inner join epacube.TAXONOMY_NODE TN on (tn.TAX_NODE_DV_FK = ed.DATA_VALUE_FK
and ed.DATA_NAME_FK = 311912
and ed.Parent_structure_FK is NULL)

		
------------------------------------------------------------------------------------------
--  Execute Dynamic SQL 
------------------------------------------------------------------------------------------

----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS,  SQL_TEXT )
            VALUES ( GETDATE(), ISNULL ( @ls_exec, @ls_exec1 ) )

            exec sp_executesql 	@ls_exec1;

END   -- REMAINING TABLES



					                             
--------------------------------------------------------------------------------------------
--   CALL VALIDATIONS
--------------------------------------------------------------------------------------------
    
	TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
	TRUNCATE TABLE #TS_EVENT_DATA_RULES	

------------------------------------------------------------------------------------------
--  DELETE ROWS  --- ERRORS ARE REMOVED THEN RE-INSERTED
--					 TIMIMG MUST DO HERE BECAUSE OF CONFLICT AND DUPLICATE VALIDATIONS
--					 DO NOT REMOVE THE EVENT_DATA_RULES  ( ONCE SET FOR AN EVENT.. SET )
------------------------------------------------------------------------------------------
----URM CHANGE
	----DELETE
	----FROM synchronizer.EVENT_DATA_ERRORS
	----WHERE event_fk IN (
	----SELECT EVENT_FK
	----FROM #TS_EVENT_DATA )

-- KS        10/15/2012   EPA-3400 Added Delete for EVENT_DATA_RULES to remove duplicates
	DELETE
	FROM synchronizer.EVENT_DATA_RULES
	WHERE event_fk IN (
	SELECT EVENT_FK
	FROM #TS_EVENT_DATA )


	------------------------------------------------------------------------------------------
	--   QUALIFY GOVERNANCE RULES 
	------------------------------------------------------------------------------------------
	SET  @v_count = ( SELECT COUNT(1)
					 FROM #TS_EVENT_DATA WITH (NOLOCK)
					 WHERE BATCH_NO = @in_batch_no
					 AND   EVENT_STATUS_CR_FK = 88 
					 AND   EVENT_TYPE_CR_FK IN ( 150, 151, 152, 153, 154, 155, 156, 160, 158 )   			 
					 AND   (  EVENT_SOURCE_CR_FK = 71        -- WHEN UI CALLS VALIDATION DIRECTLY WILL REMOVE 71 
						   OR ISNULL ( IMPORT_BATCH_NO, -999 ) = @in_batch_no )    
					 AND   DATA_NAME_FK IN ( SELECT R.RESULT_DATA_NAME_FK   
											 FROM synchronizer.RULES R WITH (NOLOCK)
											 --INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
											 --  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK 
											 --  AND  RS.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE
											 WHERE R.RECORD_STATUS_CR_FK = 1 
											 AND  R.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE)
					)			                         
	IF @v_count > 0
		EXEC synchronizer.EVENT_POST_QUALIFY_RULES_GOVERNANCE  @in_batch_no


    IF ISNULL ( @in_event_priority_cr_fk, 0 ) = 69
		EXEC SYNCHRONIZER.EVENT_POST_VALID_COMPLETENESS

    EXEC [synchronizer].[EVENT_POST_VALID_PROD_ENTITY] 

    EXEC [synchronizer].[EVENT_POST_VALID_DATA_TYPE] 
    
    IF @in_table_name IN ( 'PRODUCT_CATEGORY', 'PRODUCT_ATTRIBUTE', 'PRODUCT_TAX_ATTRIBUTE','PRODUCT_RECIPE_ATTRIBUTE', 'PRODUCT_MULT_TYPE'
							,'SETTINGS_POS_PROD_STORE_VALUES'	 )
		EXEC SYNCHRONIZER.EVENT_POST_VALID_DATA_VALUE

    IF @in_table_name IN ( 'PRODUCT_IDENTIFICATION', 'PRODUCT_IDENT_NONUNIQUE', 'PRODUCT_IDENT_MULT'   )
		EXEC SYNCHRONIZER.EVENT_POST_VALID_IDENTS

    IF @in_event_type_cr_fk = 153   --- TAX ATTRIBUTE
		EXEC [synchronizer].[EVENT_POST_VALID_TYPE_153] 

    IF @in_event_type_cr_fk = 155   --- ASSOC
		EXEC [synchronizer].[EVENT_POST_VALID_TYPE_155] 
		    
    IF @in_event_type_cr_fk = 156   --- UOMS
		EXEC [synchronizer].[EVENT_POST_VALID_TYPE_156] 

	EXEC [synchronizer].[EVENT_POST_VALID_TYPE_ALL] 		        

------------------------------------------------------------------------------------------
-- CALL PROC TO SET EVENT PRIORITY ACTION AND STATUS   ( KEEP IN ONE PLACE WITH CODE )
------------------------------------------------------------------------------------------

	EXEC  [synchronizer].[EVENT_POST_COND_PRIORITY_ACTION_STATUS] @in_batch_no



--------------------------------------------------------------------------------------------
--   CALL POST AND COMPLETE ... added @in_table_lock
--------------------------------------------------------------------------------------------

----	IF @in_post_ind = 1
		EXEC SYNCHRONIZER.EVENT_POST_DATA  


------------------------------------------------------------------------------------------
--  Update synchronizer.EVENT_DATA from #TS_EVENT_DATA  ( POST_COMPLETE )
------------------------------------------------------------------------------------------


UPDATE synchronizer.EVENT_DATA
   SET EVENT_TYPE_CR_FK			= TSED.EVENT_TYPE_CR_FK
      ,EVENT_ENTITY_CLASS_CR_FK = TSED.EVENT_ENTITY_CLASS_CR_FK
      ,EVENT_EFFECTIVE_DATE		= TSED.EVENT_EFFECTIVE_DATE
      ,EPACUBE_ID				= TSED.EPACUBE_ID
      ,DATA_NAME_FK				= TSED.DATA_NAME_FK
      ,NEW_DATA                 = TSED.NEW_DATA      
      ,CURRENT_DATA             = TSED.CURRENT_DATA
      ,PRODUCT_STRUCTURE_FK		= TSED.PRODUCT_STRUCTURE_FK
      ,ORG_ENTITY_STRUCTURE_FK	= TSED.ORG_ENTITY_STRUCTURE_FK
      ,ENTITY_CLASS_CR_FK		= TSED.ENTITY_CLASS_CR_FK
      ,ENTITY_DATA_NAME_FK		= TSED.ENTITY_DATA_NAME_FK
      ,ENTITY_STRUCTURE_FK		= TSED.ENTITY_STRUCTURE_FK
      ,EVENT_CONDITION_CR_FK	= TSED.EVENT_CONDITION_CR_FK
      ,EVENT_STATUS_CR_FK		= TSED.EVENT_STATUS_CR_FK 
      ,EVENT_PRIORITY_CR_FK		= TSED.EVENT_PRIORITY_CR_FK
      ,RESULT_TYPE_CR_FK		= TSED.RESULT_TYPE_CR_FK
      ,EVENT_ACTION_CR_FK		= TSED.EVENT_ACTION_CR_FK
      ,TABLE_ID_FK				= TSED.TABLE_ID_FK
      ,EFFECTIVE_DATE_CUR	    = TSED.EFFECTIVE_DATE_CUR
      ,END_DATE_NEW				= TSED.END_DATE_NEW
      ,END_DATE_CUR				= TSED.END_DATE_CUR
      ,DATA_VALUE_FK            = TSED.DATA_VALUE_FK
      ,ENTITY_DATA_VALUE_FK     = TSED.ENTITY_DATA_VALUE_FK
      ,VALUE_UOM_CODE_FK		= TSED.VALUE_UOM_CODE_FK
      ,ASSOC_UOM_CODE_FK		= TSED.ASSOC_UOM_CODE_FK   
      ,UOM_CONVERSION_FACTOR    = TSED.UOM_CONVERSION_FACTOR   
      ,RECORD_STATUS_CR_FK		= TSED.RECORD_STATUS_CR_FK      
      ,UPDATE_TIMESTAMP			= TSED.UPDATE_TIMESTAMP
	   ,PARENT_STRUCTURE_FK      = TSED.PARENT_STRUCTURE_FK
FROM #TS_EVENT_DATA  TSED
WHERE TSED.EVENT_FK = synchronizer.EVENT_DATA.EVENT_ID



------------------------------------------------------------------------------------------
--   INSERT INTO synchronizer.EVENT_DATA_ERRORS 
------------------------------------------------------------------------------------------


		INSERT INTO synchronizer.EVENT_DATA_ERRORS 
				(EVENT_FK
			   ,RULE_FK
			   ,EVENT_CONDITION_CR_FK
			   ,DATA_NAME_FK
			   ,ERROR_NAME
			   ,ERROR_DATA1
			   ,ERROR_DATA2
			   ,ERROR_DATA3
			   ,ERROR_DATA4
			   ,ERROR_DATA5
			   ,ERROR_DATA6
			   ,UPDATE_TIMESTAMP)
		   SELECT 
				 TEDE.EVENT_FK
				,TEDE.RULE_FK
				,TEDE.EVENT_CONDITION_CR_FK
				,TEDE.DATA_NAME_FK
				,TEDE.ERROR_NAME
				,TEDE.ERROR_DATA1
				,TEDE.ERROR_DATA2
				,TEDE.ERROR_DATA3
				,TEDE.ERROR_DATA4
				,TEDE.ERROR_DATA5
				,TEDE.ERROR_DATA6
				,TEDE.UPDATE_TIMESTAMP
			FROM #TS_EVENT_DATA_ERRORS TEDE


------------------------------------------------------------------------------------------
--   INSERT INTO synchronizer.EVENT_DATA_RULES 
------------------------------------------------------------------------------------------


	INSERT INTO [synchronizer].[EVENT_DATA_RULES]
	 (   EVENT_FK      		
		,RULES_FK      
		,RESULT_DATA_NAME_FK      		
		,RESULT_TYPE_CR_FK      	
		,RESULT_EFFECTIVE_DATE      	
		,RULES_EFFECTIVE_DATE 
		,RULES_ACTION_OPERATION_FK     
		,RULE_PRECEDENCE
		,SCHED_PRECEDENCE				
		 ) 			
		SELECT DISTINCT 
         EVENT_FK      		
		,RULES_FK      
		,RESULT_DATA_NAME_FK      		
		,RESULT_TYPE_CR_FK      	
		,RESULT_EFFECTIVE_DATE      	
		,RULES_EFFECTIVE_DATE  
		,RULES_ACTION_OPERATION_FK    
		,RULE_PRECEDENCE
		,SCHED_PRECEDENCE			
	FROM #TS_EVENT_DATA_RULES 
	

------------------------------------------------------------------------------------------
---  CLEAR TABLE FOR NEXT BATCH_NO, PRIORITY, JOB_FK, EVENT_TYPE
------------------------------------------------------------------------------------------

   
   TRUNCATE TABLE #TS_EVENT_DATA
   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
   TRUNCATE TABLE #TS_EVENT_DATA_RULES      


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_PRODUCT'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @in_batch_no
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_PRODUCT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END





























































