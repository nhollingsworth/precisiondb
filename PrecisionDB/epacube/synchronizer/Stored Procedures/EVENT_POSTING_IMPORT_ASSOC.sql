﻿












-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing for imports.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/28/2010   Initial SQL Version
-- CV        08/25/2011   Rules Type CR FK Moved from Structure table to Hiarchy
-- CV        12/03/2012   Added a distinct in the select so we didn't duplicte 
--                        Assoication events at a whse level import.
-- CV        04/25/2013   Add logic to use data name for region WHSE also    
-- CV        01/13/2017   Replaced where we get Rule type cr fk.  now on rule table


CREATE PROCEDURE [synchronizer].[EVENT_POSTING_IMPORT_ASSOC] 
        ( @in_batch_no BIGINT, @in_import_job_fk BIGINT, 
          @in_event_type_cr_fk INT, 
          @in_table_name VARCHAR(64),
          @in_data_name_fk BIGINT,
          @in_org_entity_structure_fk VARCHAR(16) )



AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()

SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_IMPORT_ASSOC.'
					 + 'Batch: ' + cast(isnull(@in_batch_no, 0) as varchar(30))
					 + 'Import: ' + cast(isnull(@in_import_job_fk, 0) as varchar(30))	
					 + 'Event Type: ' + cast(isnull(@in_event_type_cr_fk, 0) as varchar(30))					 				 
					 + 'Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))					 				 					 					 
					 + 'Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))							 
					 + 'Whse: ' + @in_org_entity_structure_fk 							 					 
										 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
										  @epa_job_id = @in_import_job_fk,
                                          @epa_batch_no = @in_batch_no,                                          
						   				  @exec_id   = @l_exec_no OUTPUT;




------------------------------------------------------------------------------------------
---  CLEAR TABLE FOR NEXT BATCH_NO, PRIORITY, JOB_FK, EVENT_TYPE
------------------------------------------------------------------------------------------

      TRUNCATE TABLE #TS_EVENT_DATA



------------------------------------------------------------------------------------------
--	PRODUCT ASSOCIATION EVENTS DIRECTLY FROM STG_RECORD_ENTITY
--		WHERE ASSOC_DATA_NAME_FK IS NOT NULL
--      NOTE:  Prod/Whse;  Entity Assoc with SINGLE_ASSOC_IND = 1/0 
------------------------------------------------------------------------------------------

 INSERT INTO #TS_EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[TABLE_ID_FK]
			   ,[JOB_CLASS_FK]
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			   ,[STG_RECORD_FK]
			   ,[BATCH_NO]
			   ,[IMPORT_BATCH_NO]	
--------- ADDED FOR POSTING PROCESS
			   ,[DATA_SET_FK]
			   ,[DATA_TYPE_CR_FK]
			   ,[ORG_IND]
			   ,[INSERT_MISSING_VALUES]		   
			   ,[TABLE_NAME]
			   ,[SINGLE_ASSOC_IND]
---------			   
			   ,[VALUE_EFFECTIVE_DATE]
			   ,[END_DATE_NEW]
			   ,[RESULT_TYPE_CR_FK]	
			   ,[RECORD_STATUS_CR_FK]
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])
SELECT DISTINCT
	 155  -- ASSOC EVENT
	,SREC.ENTITY_CLASS_CR_FK
	,CASE WHEN ( SREC.EFFECTIVE_DATE < @L_SYSDATE )
	      THEN @L_SYSDATE
	      ELSE SREC.EFFECTIVE_DATE
	 END
	,SRECS.SEARCH_STRUCTURE_FK
	,DN.DATA_NAME_ID
	,SRECEI.ENTITY_ID_VALUE
	,( SELECT CASE DS.ENTITY_STRUCTURE_EC_CR_FK
       WHEN 10101 THEN epacube.getEntityIdent ( PAS.ORG_ENTITY_STRUCTURE_FK )
       ELSE epacube.getEntityIdent ( PAS.ENTITY_STRUCTURE_FK )
       END ) AS  CURRENT_DATA
	,(	CASE ( SREC.ENTITY_CLASS_CR_FK )
		 WHEN (select code_ref_id from epacube.code_ref 
				where code_type = 'ENTITY_CLASS'
				and Code = 'PRODUCT') 
         THEN SRECS.SEARCH_STRUCTURE_FK
		 ELSE NULL
		 END ) AS PRODUCT_STRUCTURE_FK
    ,CASE ISNULL ( DS.ORG_IND, 0 )
     WHEN 0 THEN 1       --- DEFAULT TO CORP ORG_ENTITY_STRUCTURE_FK
     ELSE SRECEO.ENTITY_STRUCTURE_FK 
     END AS ORG_ENTITY_STRUCTURE_FK
    ,CASE ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 )
     WHEN 0 THEN NULL
     WHEN 10101 THEN NULL
     ELSE SRECE.ENTITY_CLASS_CR_FK
     END AS ENTITY_CLASS_CR_FK 
    ,CASE ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 )
     WHEN 0 THEN NULL
     WHEN 10101 THEN NULL
     ELSE SRECE.ENTITY_DATA_NAME_FK
     END AS ENTITY_DATA_NAME_FK
    ,CASE ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 )
     WHEN 0 THEN NULL
     WHEN 10101 THEN NULL
     ELSE SRECEI.ENTITY_STRUCTURE_FK
     END AS ENTITY_STRUCTURE_FK
	--
	,97
	,88  -- PROCESSING
	,61  -- AWAITING APPROVAL  AUTO SYNC RULES WILL SET OTHERWISE
	,SREC.EVENT_SOURCE_CR_FK
	,CASE ISNULL ( DN.SINGLE_ASSOC_IND, 0 )
     WHEN 0 THEN 51  --- THERE SHOULD NOT BE ANY CHANGES... ONLY ADDS
     WHEN 1 THEN CASE ISNULL ( PAS.PRODUCT_ASSOCIATION_ID, 0 )
				 WHEN 0 THEN 51
				 ELSE 52
				 END
     END
    ,PAS.PRODUCT_ASSOCIATION_ID
	--
	,IP.JOB_CLASS_FK
	,SREC.JOB_FK
	,SREC.CREATE_TIMESTAMP
	,SREC.IMPORT_FILENAME
	,SREC.IMPORT_PACKAGE_FK
	,SREC.STG_RECORD_ID
	,@IN_BATCH_NO
	,@IN_BATCH_NO
--------- ADDED FOR POSTING PROCESS
    ,DS.DATA_SET_ID
    ,DS.DATA_TYPE_CR_FK
    ,DS.ORG_IND
    ,DN.INSERT_MISSING_VALUES
    ,DS.TABLE_NAME
    ,DN.SINGLE_ASSOC_IND
----------	
	,SREC.EFFECTIVE_DATE
	,SREC.END_DATE
    ,SREC.RESULT_TYPE_CR_FK
    ,1  -- RECORD_STATUS_CR_FK    
	,@L_SYSDATE
	,'EPACUBE'  
-----------------       
	FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
	INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
	  ON ( SRECS.STG_RECORD_FK = SREC.STG_RECORD_ID )		
	INNER JOIN STAGE.STG_RECORD_ENTITY SRECE WITH (NOLOCK)
	  ON ( SRECE.STG_RECORD_FK = SREC.STG_RECORD_ID
      AND  SRECE.ASSOC_DATA_NAME_FK IS NOT NULL )
    INNER JOIN stage.STG_RECORD_ENTITY_IDENT SRECEI WITH (NOLOCK)
	   ON ( SRECEI.STG_RECORD_ENTITY_FK = SRECE.STG_RECORD_ENTITY_ID
	   AND   SRECEI.COLUMN_NAME = SRECE.COLUMN_NAME)  
    INNER JOIN 	epacube.DATA_NAME  DN WITH (NOLOCK)
      ON ( DN.DATA_NAME_ID = SRECE.ASSOC_DATA_NAME_FK  )
    INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) 
      ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
    INNER JOIN epacube.CODE_REF CR WITH (NOLOCK)
      ON ( CR.CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )  --- ENTITY CLASS OF THE ASSOC 
	INNER JOIN IMPORT.IMPORT_PACKAGE IP WITH (NOLOCK)
	  ON ( IP.IMPORT_PACKAGE_ID = SREC.IMPORT_PACKAGE_FK )
    LEFT JOIN stage.STG_RECORD_ENTITY SRECEO WITH (NOLOCK)
       ON (  SRECEO.STG_RECORD_FK = SREC.STG_RECORD_ID
       AND   SRECEO.SEQ_ID = SRECE.SEQ_ID
     AND  SRECEO.ENTITY_DATA_NAME_FK in (select data_name_id from epacube.data_name WITH (NOLOCK) where name in ('REGION WHSE', 'WAREHOUSE') ))
	LEFT JOIN epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
	 ON ( PAS.DATA_NAME_FK = SRECE.ASSOC_DATA_NAME_FK
	 AND  ISNULL ( PAS.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( SRECS.SEARCH_STRUCTURE_FK, 0 )
	 AND  (  ISNULL ( DS.ORG_IND, 0 ) = 0
	      OR ISNULL ( PAS.ORG_ENTITY_STRUCTURE_FK, 1 ) = ISNULL ( SRECEO.ENTITY_STRUCTURE_FK, 1 ) )	 -- WHSE SPECIFIC
	 AND  (  DS.ENTITY_STRUCTURE_EC_CR_FK = 10101          -- IF WHSE IGNORE ENTITY
	      OR (  ISNULL ( DN.SINGLE_ASSOC_IND, 0 ) = 1      -- IF SINGLE ASSOC THEN ENTITY IS VALUE;  UNIQUE KEY IS PROD/WHSE
             OR (  ISNULL ( PAS.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( SRECEI.ENTITY_STRUCTURE_FK, 0 ) ) ) )
	  )
-----------------
    WHERE 1 = 1
    AND   SREC.BATCH_NO = @IN_BATCH_NO
    AND   SRECE.ASSOC_DATA_NAME_FK =  @in_data_name_fk  
--------   NOTE >>> ORG IS SRECEO.ENTITY_STRUCTURE_FK 
    AND   (  CAST ( @in_org_entity_structure_fk AS BIGINT ) = -999 
          OR SRECEO.ENTITY_STRUCTURE_FK = CAST ( @in_org_entity_structure_fk AS BIGINT ) )
	AND   SREC.EVENT_ACTION_CR_FK IN ( 52, 51, 53, 54 )
	AND   SREC.EVENT_CONDITION_CR_FK IN ( 97, 98 )	
------------------
    AND  (   PAS.PRODUCT_ASSOCIATION_ID IS NULL     
         OR (     PAS.PRODUCT_ASSOCIATION_ID IS NOT NULL
             AND  SRECEI.ENTITY_ID_VALUE  <> ( SELECT CASE DS.ENTITY_STRUCTURE_EC_CR_FK
                                               WHEN 10101 THEN epacube.getEntityIdent ( PAS.ORG_ENTITY_STRUCTURE_FK )
                                               ELSE epacube.getEntityIdent ( PAS.ENTITY_STRUCTURE_FK )
                                               END )
          )  )  



       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN

			SET @status_desc = 'total rows Associations = '
				+ 'Data Name: ' + CAST(isnull(@in_data_name_fk, 0 ) as varchar(30))
				+ 'Cnt: ' + CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))

			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no

  			                             
--------------------------------------------------------------------------------------------
--   CALL VALIDATIONS
--------------------------------------------------------------------------------------------
    

    EXEC [synchronizer].[EVENT_POST_VALID_PROD_ENTITY] 
	
    EXEC [synchronizer].[EVENT_POST_VALID_DATA_TYPE] 
    
	EXEC [synchronizer].[EVENT_POST_VALID_TYPE_155] 

	EXEC [synchronizer].[EVENT_POST_VALID_TYPE_ALL] 		    

------------------------------------------------------------------------------------------
-- CALL PROC TO SET EVENT PRIORITY ACTION AND STATUS   ( KEEP IN ONE PLACE WITH CODE )
------------------------------------------------------------------------------------------

	EXEC  [synchronizer].[EVENT_POST_COND_PRIORITY_ACTION_STATUS] @in_batch_no



------------------------------------------------------------------------------------------
--   SET THE PRIOIRTY FOR THE COMPLETENESS EVENTS
------------------------------------------------------------------------------------------
--CINDY HERE FOR TESTING
	--UPDATE #TS_EVENT_DATA
	--  SET EVENT_PRIORITY_CR_FK = 69
	--WHERE EVENT_FK IN ( 
	--		SELECT EDX.EVENT_FK
	--		FROM #TS_EVENT_DATA EDX WITH (NOLOCK)
	--		INNER JOIN synchronizer.RULES R WITH (NOLOCK)
	--		 ON ( R.RESULT_DATA_NAME_FK = EDX.DATA_NAME_FK )
	--		INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	--		 ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
	--		 AND  RS.RULE_TYPE_CR_FK = 309 ) 
	--		WHERE R.RECORD_STATUS_CR_FK = 1
	--		AND   EDX.BATCH_NO = @in_batch_no
	--		AND   EDX.EVENT_STATUS_CR_FK IN ( 88 )  ---- STATUS CONSIDERED TO BE APPROVED OR STILL 88 
	--		)

		----select * from [epacube].[CONTROLS]   need the rules type cr fk
		--select * from synchronizer.RULES where RESULT_DATA_NAME_FK like '159%'

		UPDATE #TS_EVENT_DATA
	  SET EVENT_PRIORITY_CR_FK = 69
	WHERE EVENT_FK IN ( 
			SELECT EDX.EVENT_FK
			FROM #TS_EVENT_DATA EDX WITH (NOLOCK)
			INNER JOIN synchronizer.RULES R WITH (NOLOCK)
			 ON R.RESULT_DATA_NAME_FK = EDX.DATA_NAME_FK 	  
			WHERE R.RECORD_STATUS_CR_FK = 1
			AND   R.RULE_TYPE_CR_FK = 309
			AND   EDX.BATCH_NO = @in_batch_no
			AND   EDX.EVENT_STATUS_CR_FK IN ( 88 )  ---- STATUS CONSIDERED TO BE APPROVED OR STILL 88 
			)


--------------------------------------------------------------------------------------------
--   CALL POST AND COMPLETE 
--------------------------------------------------------------------------------------------

		EXEC SYNCHRONIZER.EVENT_POST_DATA  

		UPDATE #TS_EVENT_DATA
		SET EVENT_STATUS_CR_FK = 80   -- RETURN TO PENDING
		WHERE EVENT_STATUS_CR_FK in ( 86, 87, 88 ) 


       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows Returned to Pending = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
       END
		

   END   --- @l_rows_processed FOR ASSOCIATIONS





------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_ASSOC'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_ASSOC has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END














