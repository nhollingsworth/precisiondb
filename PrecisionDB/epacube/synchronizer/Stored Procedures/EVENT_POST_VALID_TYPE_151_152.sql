﻿











-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform USED DEFINED RULE event validations to all event types
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        10/19/2008   Initial SQL Version
-- KS		 08/24/2009   Added Validation for Decimal Scale and Rounding
-- CV        06/20/2014   Add error for missing org

CREATE PROCEDURE [synchronizer].[EVENT_POST_VALID_TYPE_151_152] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_151_152.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;




------------------------------------------------------------------------------------------
--  VALIDATE UOMS  
--  151 AND 152.. REMEMBER VALUE UOM CODE... is the UOM DATA NAME within the same IMPORT...
---				  The ASSOC UOM CODE is the UOM DATA NAME that is the CURRENT VALUE
--				  AND CONVERSION Factor is the difference of the TWO  ( If Value is NULL 1 )
--									If Both are NULL then 1 but flag with ERROR.
------------------------------------------------------------------------------------------




------------------------------------------------------------------------------------------
--   Insert Errors into Event Error Tables 
------------------------------------------------------------------------------------------

							INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,99
								,'MISSING UOM VALUE'  --ERROR NAME   
								,'CHECK CURRENT VALUE OR EVENT FOR '
								,DNX.LABEL 
								,NULL
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								 ON ( DN.DATA_NAME_ID = TSED.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK)
								 ON ( DNX.DATA_NAME_ID = DN.UOM_CLASS_DATA_NAME_FK )
								WHERE EVENT_TYPE_CR_FK IN ( 151, 152 )
								AND VALUE_UOM_CODE_FK IS NULL 
								AND ASSOC_UOM_CODE_FK IS NULL )




------------------------------------------------------------------------------------------
--   VALUE UOM NOT SAME AS ASSOC UOM
------------------------------------------------------------------------------------------

							INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,99
								,'VALUE UOM IS NOT THE SAME AS ASSOC UOM'  --ERROR NAME   
								,' CHECK CURRENT VALUE OR EVENT FOR '
								,DNX.LABEL 
								,NULL
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								 ON ( DN.DATA_NAME_ID = TSED.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK)
								 ON ( DNX.DATA_NAME_ID = DN.UOM_CLASS_DATA_NAME_FK )
								WHERE EVENT_TYPE_CR_FK IN ( 151, 152 )
								AND VALUE_UOM_CODE_FK IS NOT NULL 
								AND ASSOC_UOM_CODE_FK IS NOT NULL  --added as Null was getting error.
								AND ISNULL ( VALUE_UOM_CODE_FK, 0 ) <> ISNULL ( ASSOC_UOM_CODE_FK, 0 )  )



------------------------------------------------------------------------------------------
--   MISSING ASSOC UOM
------------------------------------------------------------------------------------------

							INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,99
								,'MISSING ASSOC UOM'  --ERROR NAME   
								,'APPROVE UOM EVENTS PRIOR TO APPROVING VALUE EVENTS FOR '
								,DNX.LABEL 
								,NULL
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								 ON ( DN.DATA_NAME_ID = TSED.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK)
								 ON ( DNX.DATA_NAME_ID = DN.UOM_CLASS_DATA_NAME_FK )
								WHERE EVENT_TYPE_CR_FK IN ( 151, 152 )
								AND ASSOC_UOM_CODE_FK IS NULL  -- NO ASSOC UOM HAS BEEN APPROVED PRIOR
                          )

------------------------------------------------------------------------------------------

							INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,99
								,'NUMERIC REQUIRED'  --ERROR NAME   
								,'VALUE MUST BE NUMBERIC - '
								,DNX.LABEL 
								,NULL
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED 
								INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK)
								 ON ( DNX.DATA_NAME_ID = TSED.data_name_fk)
								where tsed.event_type_cr_fk = 158
								AND isnumeric(tsed.NET_VALUE1_NEW) = 0)


/*

NOT SURE I WANT TO AUTO ADJUST AT THIS TIME....   COMMENTING OUT UNTIL WE WORK THRU REQUIREMENTS

-----------------------------------------------------------------------------------------------
--Check if CONVERSION FACTOR <> 1 THEN WARNING THAT UOM CHANGE OCCURED AND CURRENT VALUES ADJUSTED

							INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,99
								,'UOM CODE CHANGE IN EFFECT '  --ERROR NAME     
								,'CURRENT VALUES HAVE BEEN ADJUSTED VIA '--ERROR1
								,'UOM CONVERSION FACTOR '
								,CAST ( UOM_CONVERSION_FACTOR AS VARCHAR(16) )
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								inner join epacube.UOM_CODE uc WITH (NOLOCK) on 
								(uc.uom_code_id = TSED.VALUE_UOM_CODE_FK
								AND uc.record_status_cr_fk = 2)
								WHERE EVENT_TYPE_CR_FK IN ( 151, 152 )
								AND ISNULL ( UOM_CONVERSION_FACTOR, 1 ) <> 1 )


		UPDATE #TS_EVENT_DATA
		SET 
			NET_VALUE1_CUR = NET_VALUE1_CUR * ISNULL ( UOM_CONVERSION_FACTOR, 1 )
		   ,NET_VALUE2_CUR = NET_VALUE2_CUR * ISNULL ( UOM_CONVERSION_FACTOR, 1 )
		   ,NET_VALUE3_CUR = NET_VALUE3_CUR * ISNULL ( UOM_CONVERSION_FACTOR, 1 )
		   ,NET_VALUE4_CUR = NET_VALUE4_CUR * ISNULL ( UOM_CONVERSION_FACTOR, 1 )
		   ,NET_VALUE5_CUR = NET_VALUE5_CUR * ISNULL ( UOM_CONVERSION_FACTOR, 1 )
		   ,NET_VALUE6_CUR = NET_VALUE6_CUR * ISNULL ( UOM_CONVERSION_FACTOR, 1 )

		WHERE EVENT_TYPE_CR_FK IN ( 151, 152 )
		AND ISNULL ( UOM_CONVERSION_FACTOR, 1 ) <> 1 
		   

*/

------------------------------------------------------------------------------------------
--  CHECK SCALE OF DECIMAL    -- UTILIZE TEMP TABLE
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_NET_VALUES') is not null
	   drop table #TS_EVENT_NET_VALUES
	   
	   
	-- create temp table
CREATE TABLE #TS_EVENT_NET_VALUES(
	TS_EVENT_NET_VALUES_ID bigint IDENTITY(1000,1) NOT NULL,
	EVENT_FK bigint NULL,
	DATA_NAME_FK int NULL,
	COLUMN_NAME varchar(64) NULL,
	COLUMN_VALUE varchar(32) NULL,
	ROUNDED_VALUE NUMERIC (18,6),
	DECIMAL_POSITION INT NULL,
	SCALE INT NULL,
	NEW_SCALE INT NULL,
	ROUNDING_METHOD_CR_FK INT NULL
)


INSERT INTO #TS_EVENT_NET_VALUES
(
	EVENT_FK,
	DATA_NAME_FK,
	COLUMN_NAME,
	COLUMN_VALUE,
	ROUNDED_VALUE,
    DECIMAL_POSITION,	 	
	SCALE,
	NEW_SCALE,
    ROUNDING_METHOD_CR_FK	
)
----
		SELECT 
			 A.EVENT_FK
			,A.DATA_NAME_FK
			,A.COLUMN_NAME
			,A.COLUMN_VALUE
			,CAST ( A.COLUMN_VALUE AS NUMERIC(18,6) )
            ,( SELECT PATINDEX ( '%.%', A.COLUMN_VALUE ) ) AS DECIMAL_POSITION
			,A.SCALE			
			,( SELECT LEN ( ( 
				     SELECT SUBSTRING ( A.COLUMN_VALUE,( 
						CASE ( SELECT PATINDEX ( '%.%', A.COLUMN_VALUE ) ) 
						WHEN 0 THEN 99 
						ELSE  ( SELECT PATINDEX ( '%.%',A.COLUMN_VALUE ) + 1 ) 
						END ) ,99 ) ) ) ) AS NEW_SCALE			       			
			,A.ROUNDING_METHOD_CR_FK						
			FROM (
			SELECT TSED.EVENT_FK,
			       DN.DATA_NAME_ID AS DATA_NAME_FK,
			       DS.COLUMN_NAME,
			       CASE DS.COLUMN_NAME
			       WHEN 'NET_VALUE1' THEN CAST ( epacube.removeZeros ( TSED.NET_VALUE1_NEW ) AS VARCHAR(32) )
			       WHEN 'NET_VALUE2' THEN CAST ( epacube.removeZeros ( TSED.NET_VALUE2_NEW ) AS VARCHAR(32) )
			       WHEN 'NET_VALUE3' THEN CAST ( epacube.removeZeros ( TSED.NET_VALUE3_NEW ) AS VARCHAR(32) )
			       WHEN 'NET_VALUE4' THEN CAST ( epacube.removeZeros ( TSED.NET_VALUE4_NEW ) AS VARCHAR(32) )
			       WHEN 'NET_VALUE5' THEN CAST ( epacube.removeZeros ( TSED.NET_VALUE5_NEW ) AS VARCHAR(32) )
			       WHEN 'NET_VALUE6' THEN CAST ( epacube.removeZeros ( TSED.NET_VALUE6_NEW ) AS VARCHAR(32) )
			      			       
			       END  AS COLUMN_VALUE,
			       DN.ROUNDING_METHOD_CR_FK,
			       DN.SCALE			       
			FROM #TS_EVENT_DATA TSED
			INNER JOIN epacube.data_name dn WITH (NOLOCK)
			  ON (dn.data_name_id = tsed.data_name_fk ) 
            --INNER JOIN epacube.DATA_NAME dn  WITH (NOLOCK) 
            --  ON (dn.parent_data_name_fk = parentdn.data_name_id 
            --  AND dn.record_status_cr_fk = 1 )		
            INNER JOIN epacube.DATA_SET ds WITH (NOLOCK) 
              ON ( ds.data_set_id = dn.data_set_fk )
			WHERE DN.SCALE IS NOT NULL 
			) A
			WHERE ISNULL ( A.COLUMN_VALUE, 'NULL DATA' ) <> 'NULL DATA'




------------------------------------------------------------------------------------------
--  INSERT ERROR FOR CHECK SCALE OF DECIMAL   
------------------------------------------------------------------------------------------


			INSERT INTO #TS_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		SELECT 
			 EVENT_FK
			,NULL
			,CASE ISNULL ( ROUNDING_METHOD_CR_FK, 0 )
			 WHEN 0 THEN 99
			 ELSE 98 
			 END 
			,'Decimal Scale has exceeded Maximum'  --- error name   
			,SCALE  --ERROR1
			,'Current Scale '
			,NEW_SCALE
			,CASE ISNULL ( ROUNDING_METHOD_CR_FK, 0 )
			 WHEN 0 THEN NULL 
			 ELSE 'Rounded to '
			 END
			,CAST ( ROUNDED_VALUE AS VARCHAR(32) )
			,NULL
			,@l_sysdate
		FROM #TS_EVENT_NET_VALUES
		WHERE NEW_SCALE > SCALE                     
           



/*
ROUNDING NOTES:
	Just doing standard rounding here... no rouding to value because all we have is SCALE
	
*/



UPDATE #TS_EVENT_NET_VALUES
SET ROUNDED_VALUE = ( CASE ISNULL ( ROUNDING_METHOD_CR_FK, 0 )
			          WHEN 0   THEN CAST ( COLUMN_VALUE AS NUMERIC(18,6) )
					  WHEN 720 THEN ( select common.RoundNearest ( CAST ( COLUMN_VALUE AS NUMERIC(18,6)  ), SCALE ) )
					  WHEN 721 THEN ( select common.RoundUp ( CAST ( COLUMN_VALUE AS NUMERIC(18,6)  ), SCALE ) )
					  WHEN 722 THEN ( select common.RoundDown ( CAST ( COLUMN_VALUE AS NUMERIC(18,6)  ), SCALE ) )
					  ELSE CAST ( COLUMN_VALUE AS NUMERIC(18,6) )
			          END )
-----
WHERE 1 = 1
AND   NEW_SCALE > SCALE                     

/*

------------------------------------------------------------------------------------------
--  Perform ROUNDING  ---- OLD APPROACH DELETE AFTER QA CERTIFICATION 
------------------------------------------------------------------------------------------



UPDATE #TS_EVENT_NET_VALUES
SET ROUNDED_VALUE = CASE ISNULL ( ROUNDING_METHOD_CR_FK, 0 )
                    WHEN 0 THEN CAST ( COLUMN_VALUE AS NUMERIC(18,6) )
                    WHEN 721 -- NEAREST
                         THEN ROUND ( CAST ( COLUMN_VALUE AS NUMERIC(18,6) ), SCALE ) 
                    WHEN 722 -- ROUND UP
                         THEN CASE WHEN ( SELECT SUBSTRING ( COLUMN_VALUE, (DECIMAL_POSITION + SCALE + 1 ), 1 ) ) = '0'
                                   THEN CAST ( LEFT ( COLUMN_VALUE, ( DECIMAL_POSITION + SCALE ) ) AS NUMERIC(18,6) )
                                   ELSE CASE ISNULL ( SCALE, 0 )
                                        WHEN 0 THEN CAST ( LEFT ( COLUMN_VALUE, ( DECIMAL_POSITION + SCALE ) ) AS NUMERIC(18,6) )
                                        WHEN 1 THEN ( CAST ( LEFT ( COLUMN_VALUE, ( DECIMAL_POSITION + SCALE ) ) AS NUMERIC(18,6) ) + .1 )
                                        WHEN 2 THEN ( CAST ( LEFT ( COLUMN_VALUE, ( DECIMAL_POSITION + SCALE ) ) AS NUMERIC(18,6) ) + .01 )
                                        WHEN 3 THEN ( CAST ( LEFT ( COLUMN_VALUE, ( DECIMAL_POSITION + SCALE ) ) AS NUMERIC(18,6) ) + .001 )
                                        WHEN 4 THEN ( CAST ( LEFT ( COLUMN_VALUE, ( DECIMAL_POSITION + SCALE ) ) AS NUMERIC(18,6) ) + .0001 )
                                        WHEN 5 THEN ( CAST ( LEFT ( COLUMN_VALUE, ( DECIMAL_POSITION + SCALE ) ) AS NUMERIC(18,6) ) + .00001 )
                                        END
                              END
                    WHEN 723 --- ROUND DOWN
                         THEN  CAST ( LEFT ( COLUMN_VALUE, ( DECIMAL_POSITION + SCALE ) ) AS NUMERIC(18,6) )                                   
                    ELSE CAST ( COLUMN_VALUE AS NUMERIC(18,6) )
                    END
WHERE NEW_SCALE > SCALE                     


*/



------------------------------------------------------------------------------------------
--  UPDATE #TS_EVENT_DATA WITH NEW ROUNDED VALUES  ( ONLY 6 POSITIONS FOR NOW )
------------------------------------------------------------------------------------------


UPDATE #TS_EVENT_DATA
SET NET_VALUE1_NEW  = ISNULL (
					  ( SELECT ROUNDED_VALUE
					 	FROM #TS_EVENT_NET_VALUES
                        WHERE #TS_EVENT_NET_VALUES.EVENT_FK = #TS_EVENT_DATA.EVENT_FK
                        AND   #TS_EVENT_NET_VALUES.COLUMN_NAME = 'NET_VALUE1'  )
                      , #TS_EVENT_DATA.NET_VALUE1_NEW )  -- added new
   ,NET_VALUE2_NEW  = ISNULL (
					  ( SELECT ROUNDED_VALUE
                        FROM #TS_EVENT_NET_VALUES
                        WHERE #TS_EVENT_NET_VALUES.EVENT_FK = #TS_EVENT_DATA.EVENT_FK
                        AND   #TS_EVENT_NET_VALUES.COLUMN_NAME = 'NET_VALUE2'  )
                      , #TS_EVENT_DATA.NET_VALUE2_NEW )                        
   ,NET_VALUE3_NEW  = ISNULL (
					  ( SELECT ROUNDED_VALUE
                        FROM #TS_EVENT_NET_VALUES
                        WHERE #TS_EVENT_NET_VALUES.EVENT_FK = #TS_EVENT_DATA.EVENT_FK
                        AND   #TS_EVENT_NET_VALUES.COLUMN_NAME = 'NET_VALUE3'  )
                      , #TS_EVENT_DATA.NET_VALUE3_NEW )                        
   ,NET_VALUE4_NEW  = ISNULL (
					  ( SELECT ROUNDED_VALUE
                        FROM #TS_EVENT_NET_VALUES
                        WHERE #TS_EVENT_NET_VALUES.EVENT_FK = #TS_EVENT_DATA.EVENT_FK
                        AND   #TS_EVENT_NET_VALUES.COLUMN_NAME = 'NET_VALUE4'  )
                      , #TS_EVENT_DATA.NET_VALUE4_NEW )                        
   ,NET_VALUE5_NEW  = ISNULL (
					  ( SELECT ROUNDED_VALUE
                        FROM #TS_EVENT_NET_VALUES
                        WHERE #TS_EVENT_NET_VALUES.EVENT_FK = #TS_EVENT_DATA.EVENT_FK
                        AND   #TS_EVENT_NET_VALUES.COLUMN_NAME = 'NET_VALUE5'  )
                      , #TS_EVENT_DATA.NET_VALUE5_NEW )                        
   ,NET_VALUE6_NEW  = ISNULL (
					  ( SELECT ROUNDED_VALUE
                        FROM #TS_EVENT_NET_VALUES
                        WHERE #TS_EVENT_NET_VALUES.EVENT_FK = #TS_EVENT_DATA.EVENT_FK
                        AND   #TS_EVENT_NET_VALUES.COLUMN_NAME = 'NET_VALUE6'  )
                      , #TS_EVENT_DATA.NET_VALUE6_NEW )                        




			                             
--------------------------------------------------------------------------------------------
--   NO RESULT
--			ALL VALUES ARE NULL 
--------------------------------------------------------------------------------------------


		INSERT INTO #TS_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		SELECT 
			 EVENT_FK
			,NULL
			,99
			,'EVENT REJECTED:: ALL VALUES are NULL'  --- error name   
			,NULL
			,NULL
			,NULL
			,NULL
			,NULL
			,NULL
			,@l_sysdate
		FROM  #TS_EVENT_DATA
		WHERE EVENT_TYPE_CR_FK IN ( 151, 152 )
		AND   NET_VALUE1_NEW IS NULL
		AND   NET_VALUE2_NEW IS NULL
		AND   NET_VALUE3_NEW IS NULL
		AND   NET_VALUE4_NEW IS NULL
		AND   NET_VALUE5_NEW IS NULL
		AND   NET_VALUE6_NEW IS NULL

           
UPDATE #TS_EVENT_DATA
SET EVENT_ACTION_CR_FK = 57   -- NO RESULT
   ,EVENT_STATUS_CR_FK = 82   -- REJECTED 
WHERE EVENT_TYPE_CR_FK IN ( 151, 152 )
AND   NET_VALUE1_NEW IS NULL
AND   NET_VALUE2_NEW IS NULL
AND   NET_VALUE3_NEW IS NULL
AND   NET_VALUE4_NEW IS NULL
AND   NET_VALUE5_NEW IS NULL
AND   NET_VALUE6_NEW IS NULL


---------------------------------------------------------------------------------------------------
---check if missing org entity structure
---------------------------------------------------------------------------------------------------

	INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,99
								,'MISSING WHSE'  --ERROR NAME   
								,'MISSING WHSE INFO FOR '
								,DN.LABEL 
								,NULL
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								 ON ( DN.DATA_NAME_ID = TSED.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
								 ON ( DN.DATA_set_Fk  = DS.DATA_SET_ID
								   AND  DS.ORG_IND = 1)
								WHERE tsed.EVENT_TYPE_CR_FK IN ( 151, 152 )
								AND tsed.ORG_ENTITY_STRUCTURE_FK IS NULL)  --

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_151_152'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_151_152 has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END






