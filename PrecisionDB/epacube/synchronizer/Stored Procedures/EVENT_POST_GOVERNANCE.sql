﻿















-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To determine all qualified rules for Event_Data records in Batch
--				Utilizes Dynamic SQL to create the Goverance Rules
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        10/19/2008   Initial SQL Version
-- CV        01/20/2014   Adding UI logging 
-- CV        01/1/2017    Rules Type Cr fk from rules table

CREATE PROCEDURE [synchronizer].[EVENT_POST_GOVERNANCE] 
               ( @in_batch_no BIGINT )

AS
BEGIN

 DECLARE @ls_exec            nvarchar (max);
 DECLARE @ls_exec2           nvarchar (max); 
 DECLARE @l_exec_no          bigint;
 DECLARE @l_rows_processed   bigint;
 DECLARE @l_sysdate			 DATETIME
 
 DECLARE  @ls_stmt			  VARCHAR(MAX)
 DECLARE  @v_count			  BIGINT
 DECLARE @V_START_TIMESTAMP  DATETIME
 DECLARE @V_END_TIMESTAMP    DATETIME
 DECLARE @v_input_rows       bigint;               
 DECLARE @v_output_rows      bigint; 
DECLARE @v_exception_rows		 bigint;         
DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_GOVERNANCE ' 
              + 'BATCH NO:: '  + CAST ( ISNULL ( @in_batch_no, 0 ) AS VARCHAR(16) )
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;
						   				  


SET @V_START_TIMESTAMP = getdate()

 

------------------------------------------------------------------------------------------
--   Loop Through Qualified Rule ( RESULT_RANK = 1 ) 
------------------------------------------------------------------------------------------


      DECLARE 
              @vm$rules_action_operation_fk    int,      
              @vm$rules_fk                     VARCHAR(32),        
              @vm$result_name                  VARCHAR(32),                          
              @vm$basis1_name                  VARCHAR(32),              
              @vm$basis2_name                  VARCHAR(32),
              @vm$basis3_name                  VARCHAR(32),   
              @vm$pct_chg_name                 VARCHAR(32),                 
              @vm$compare_operator             VARCHAR(32),                           
              @vm$basis1_number                VARCHAR(32),                
              @vm$basis2_number                VARCHAR(32)

      DECLARE  cur_v_rule cursor local for
              
				
				SELECT DISTINCT 
			       RA.rules_action_operation1_fk
			      ,RA.RULES_FK						      
				  , 'NET_VALUE1_NEW' AS RESULT_NAME				      
				  , 'NET_VALUE1_NEW' AS BASIS1_NAME
				  , 'NET_VALUE1_NEW' AS BASIS2_NAME
				  , 'NET_VALUE1_NEW' AS BASIS3_NAME
				  ,'PERCENT_CHANGE1' AS PCT_CHG_NAME
 
			      ,( SELECT CR.CODE FROM EPACUBE.CODE_REF CR WITH (NOLOCK) WHERE CODE_REF_ID = RA.COMPARE_OPERATOR_CR_FK ) AS COMPARE_OPERATOR				      				  				  
			      ,CAST ( RA.BASIS1_NUMBER AS VARCHAR(16) ) AS BASIS1_NUMBER				  
			      ,CAST ( RA.BASIS2_NUMBER AS VARCHAR(16) ) AS BASIS2_NUMBER	
				FROM #TS_EVENT_DATA TSED WITH (NOLOCK)
				INNER JOIN #TS_EVENT_DATA_RULES TSEDR WITH (NOLOCK) 
				  ON ( TSEDR.EVENT_FK = TSED.EVENT_FK )
				INNER JOIN synchronizer.RULES R WITH (NOLOCK)
				  ON ( R.RULES_ID = TSEDR.RULES_FK )
				--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
				--  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )				  
				INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
				  ON ( RA.RULES_FK = R.RULES_ID )
				INNER JOIN synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
				  ON ( RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION1_FK )		
				WHERE 1 = 1
				AND   TSEDR.RESULT_RANK = 1
				AND   R.RULE_TYPE_CR_FK = 307    --- GOVERNANCE
				AND   RAOP.NAME <> 'NO ACTION'      --- IGNORE NO ACTION RULE ACTIONS
                ORDER BY RA.RULES_ACTION_OPERATION1_FK, RA.RULES_FK  
				
				
				
			--ONLY	ONE VALUE NOW
				
				----SELECT DISTINCT 
			 ----      RA.rules_action_operation1_fk
			 ----     ,RA.RULES_FK						      
				----  ,case when tsed.event_type_cr_fk = 158 THEN 'NET_VALUE1_NEW'
				----  ELSE
				----  ( SELECT DSX.COLUMN_NAME + '_NEW' 
				----			FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) 
    ----                        INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK )
    ----                        WHERE DNX.DATA_NAME_ID = R.RESULT_DATA_NAME_FK ) 
				----	END AS RESULT_NAME				      
				 
				----  ,case when tsed.event_type_cr_fk = 158 THEN 'NET_VALUE2_NEW'
				----  ELSE
				----   ( SELECT DSX.COLUMN_NAME + '_NEW' 
				----			FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) 
    ----                        INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK )				  
				----			WHERE DNX.DATA_NAME_ID = RA.BASIS1_DN_FK )
							
				----	END AS BASIS1_NAME
				
				----  ,( SELECT DSX.COLUMN_NAME + '_NEW' 
				----			FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) 
    ----                        INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK )				  
				----			WHERE DNX.DATA_NAME_ID = RA.BASIS2_DN_FK ) AS BASIS2_NAME
				----  ,( SELECT DSX.COLUMN_NAME + '_NEW' 
				----			FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) 
    ----                        INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK )				  
				----			WHERE DNX.DATA_NAME_ID = RA.BASIS3_DN_FK ) AS BASIS3_NAME
				----  ,( SELECT CASE DSX.COLUMN_NAME
				----			WHEN 'NET_VALUE1' THEN 'PERCENT_CHANGE1'
				----			WHEN 'NET_VALUE2' THEN 'PERCENT_CHANGE2'
				----			WHEN 'NET_VALUE3' THEN 'PERCENT_CHANGE3'				   				   
				----			WHEN 'NET_VALUE4' THEN 'PERCENT_CHANGE4'
				----			WHEN 'NET_VALUE5' THEN 'PERCENT_CHANGE5'
				----			WHEN 'NET_VALUE6' THEN 'PERCENT_CHANGE6'				   				   				   
				----			END 
				----     FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK)    
				----     INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK )	
    ----                 WHERE DNX.DATA_NAME_ID = RA.BASIS1_DN_FK ) AS PCT_CHG_NAME
			 ----     ,( SELECT CR.CODE FROM EPACUBE.CODE_REF CR WITH (NOLOCK) WHERE CODE_REF_ID = RA.COMPARE_OPERATOR_CR_FK ) AS COMPARE_OPERATOR				      				  				  
			 ----     ,CAST ( RA.BASIS1_NUMBER AS VARCHAR(16) ) AS BASIS1_NUMBER				  
			 ----     ,CAST ( RA.BASIS2_NUMBER AS VARCHAR(16) ) AS BASIS2_NUMBER	
				----FROM #TS_EVENT_DATA TSED WITH (NOLOCK)
				----INNER JOIN #TS_EVENT_DATA_RULES TSEDR WITH (NOLOCK) 
				----  ON ( TSEDR.EVENT_FK = TSED.EVENT_FK )
				----INNER JOIN synchronizer.RULES R WITH (NOLOCK)
				----  ON ( R.RULES_ID = TSEDR.RULES_FK )
				------INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
				------  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )				  
				----INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
				----  ON ( RA.RULES_FK = R.RULES_ID )
				----INNER JOIN synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
				----  ON ( RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION1_FK )		
				----WHERE 1 = 1
				----AND   TSEDR.RESULT_RANK = 1
				----AND   R.RULE_TYPE_CR_FK = 307    --- GOVERNANCE
				----AND   RAOP.NAME <> 'NO ACTION'      --- IGNORE NO ACTION RULE ACTIONS
    ----            ORDER BY RA.RULES_ACTION_OPERATION1_FK, RA.RULES_FK  
              
            
        OPEN cur_v_rule;
        FETCH NEXT FROM cur_v_rule INTO   @vm$rules_action_operation_fk
										 ,@vm$rules_fk										        
										 ,@vm$result_name
										 ,@vm$basis1_name
										 ,@vm$basis2_name
										 ,@vm$basis3_name
										 ,@vm$pct_chg_name										 
										 ,@vm$compare_operator										 
										 ,@vm$basis1_number
										 ,@vm$basis2_number										 



------------------------------------------------------------------------------------------
---  LOOP THROUGH GOVERNANACE RULE BY RULE 
------------------------------------------------------------------------------------------

         WHILE @@FETCH_STATUS = 0 
         BEGIN


------------------------------------------------------------------------------------------
---  COMPARE (NET VALUES)     RA.RULES_ACTION_OPERATION1_FK = 241  	
------------------------------------------------------------------------------------------

IF  @vm$rules_action_operation_fk = 241

	 SET @ls_exec =	'INSERT INTO #TS_EVENT_DATA_ERRORS (
						EVENT_FK,
						RULE_FK,
						EVENT_CONDITION_CR_FK,
						ERROR_NAME,
						ERROR_DATA1,
						ERROR_DATA2,
						ERROR_DATA3,
						ERROR_DATA4,
						ERROR_DATA5,
						ERROR_DATA6,
						UPDATE_TIMESTAMP)
					SELECT 
						 TSED.EVENT_FK
						,RA.RULES_FK
						,RA.EVENT_CONDITION_CR_FK 
						,RA.ERROR_NAME 
						,( SELECT DNX.LABEL FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.DATA_NAME_ID = RA.BASIS1_DN_FK ) AS DATA1
						,( SELECT CR.CODE FROM EPACUBE.CODE_REF CR WITH (NOLOCK) WHERE CODE_REF_ID = RA.COMPARE_OPERATOR_CR_FK ) AS DATA2 
						,( SELECT DNX.LABEL FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.DATA_NAME_ID = RA.BASIS2_DN_FK ) AS DATA3
						,NULL AS DATA4
						,NULL AS DATA5
						,NULL AS DATA6
						,GETDATE() AS UPDATE_TIMESTAMP 
					 FROM #TS_EVENT_DATA TSED 
					 INNER JOIN #TS_EVENT_DATA_RULES TSEDR WITH (NOLOCK) ON ( TSED.EVENT_FK = TSEDR.EVENT_FK ) 
					 INNER JOIN synchronizer.RULES R WITH (NOLOCK) ON ( R.RULES_ID = TSEDR.RULES_FK )
					 INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON ( RA.RULES_FK = R.RULES_ID )
					 WHERE 1 = 1
					 AND   R.RULES_ID = '
					+ @vm$rules_fk   
					+ ' AND ' + @vm$basis1_name + ' ' + @vm$compare_operator + ' ' + @vm$basis2_name
					+ ' AND ' + @vm$basis1_name + ' IS NOT NULL '
					+ ' AND ' + @vm$basis2_name + ' IS NOT NULL '					                			
					+ ' AND ' + @vm$basis1_name + ' > 0 '
					+ ' AND ' + @vm$basis2_name + ' > 0 '					                			
			

ELSE
------------------------------------------------------------------------------------------
---  COMPARE (NET TO NUMERIC)     RA.RULES_ACTION_OPERATION1_FK = 242
------------------------------------------------------------------------------------------

IF  @vm$rules_action_operation_fk = 242

	 SET @ls_exec =	'INSERT INTO #TS_EVENT_DATA_ERRORS (
						EVENT_FK,
						RULE_FK,
						EVENT_CONDITION_CR_FK,
						ERROR_NAME,
						ERROR_DATA1,
						ERROR_DATA2,
						ERROR_DATA3,
						ERROR_DATA4,
						ERROR_DATA5,
						ERROR_DATA6,
						UPDATE_TIMESTAMP)
					SELECT 
						 TSED.EVENT_FK
						,RA.RULES_FK
						,RA.EVENT_CONDITION_CR_FK 
						,RA.ERROR_NAME 
						,(SELECT DNX.LABEL FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.DATA_NAME_ID = RA.BASIS1_DN_FK) AS DATA1
						,(SELECT CR.CODE FROM EPACUBE.CODE_REF CR WITH (NOLOCK) WHERE CODE_REF_ID = RA.COMPARE_OPERATOR_CR_FK) AS DATA2 
						,CAST( RA.BASIS1_NUMBER AS VARCHAR(16)) AS DATA3
						,NULL AS DATA4
						,NULL AS DATA5
						,NULL AS DATA6
						,GETDATE() AS UPDATE_TIMESTAMP 
					 FROM #TS_EVENT_DATA TSED 
					 INNER JOIN #TS_EVENT_DATA_RULES TSEDR WITH (NOLOCK) ON ( TSED.EVENT_FK = TSEDR.EVENT_FK ) 
					 INNER JOIN synchronizer.RULES R WITH (NOLOCK) ON ( R.RULES_ID = TSEDR.RULES_FK )
					 INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON ( RA.RULES_FK = R.RULES_ID )
					 WHERE 1 = 1
					 AND   R.RULES_ID = '
					+ @vm$rules_fk   
					+ ' AND ' + @vm$basis1_name + ' ' + @vm$compare_operator + ' ' + @vm$basis1_number
					+ ' AND ' + @vm$basis1_name + ' IS NOT NULL '



ELSE
------------------------------------------------------------------------------------------
---  PCT CHG TOLERANCE (NET SET)     RA.RULES_ACTION_OPERATION1_FK = 251
------------------------------------------------------------------------------------------

IF  @vm$rules_action_operation_fk = 251

	 SET @ls_exec =	'INSERT INTO #TS_EVENT_DATA_ERRORS (
						EVENT_FK,
						RULE_FK,
						EVENT_CONDITION_CR_FK,
						ERROR_NAME,
					
						ERROR_DATA5,
						ERROR_DATA6,
						UPDATE_TIMESTAMP)
					SELECT 
						 TSED.EVENT_FK
						,RA.RULES_FK
						,RA.EVENT_CONDITION_CR_FK 
						,RA.ERROR_NAME 
									
						,NULL AS DATA5
						,NULL AS DATA6
						,GETDATE() AS UPDATE_TIMESTAMP 
					 FROM #TS_EVENT_DATA TSED 
					 INNER JOIN #TS_EVENT_DATA_RULES TSEDR WITH (NOLOCK) ON ( TSED.EVENT_FK = TSEDR.EVENT_FK ) 
					 INNER JOIN synchronizer.RULES R WITH (NOLOCK) ON ( R.RULES_ID = TSEDR.RULES_FK )
					 INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON ( RA.RULES_FK = R.RULES_ID )
					 WHERE 1 = 1
					 AND   R.RULES_ID = '
					+ @vm$rules_fk   
					+ ' AND ( ISNULL ( TSED.PERCENT_CHANGE1, 0 ) ' + @vm$compare_operator + ' RA.BASIS1_NUMBER AND RA.BASIS2_NUMBER '
					+ '    OR ISNULL ( TSED.PERCENT_CHANGE2, 0 ) ' + @vm$compare_operator + ' RA.BASIS1_NUMBER AND RA.BASIS2_NUMBER '
					+ '    OR ISNULL ( TSED.PERCENT_CHANGE3, 0 ) ' + @vm$compare_operator + ' RA.BASIS1_NUMBER AND RA.BASIS2_NUMBER '
					+ '    OR ISNULL ( TSED.PERCENT_CHANGE4, 0 ) ' + @vm$compare_operator + ' RA.BASIS1_NUMBER AND RA.BASIS2_NUMBER '
					+ '    OR ISNULL ( TSED.PERCENT_CHANGE5, 0 ) ' + @vm$compare_operator + ' RA.BASIS1_NUMBER AND RA.BASIS2_NUMBER '
					+ '    OR ISNULL ( TSED.PERCENT_CHANGE6, 0 ) ' + @vm$compare_operator + ' RA.BASIS1_NUMBER AND RA.BASIS2_NUMBER '
					+ ' ) '



ELSE
------------------------------------------------------------------------------------------
---  PCT CHG TOLERANCE (NET VALUE)     RA.RULES_ACTION_OPERATION1_FK = 252
------------------------------------------------------------------------------------------

IF  @vm$rules_action_operation_fk = 252

	 SET @ls_exec =	'INSERT INTO #TS_EVENT_DATA_ERRORS (
						EVENT_FK,
						RULE_FK,
						EVENT_CONDITION_CR_FK,
						ERROR_NAME,
						ERROR_DATA1,
						ERROR_DATA2,
						ERROR_DATA3,
						ERROR_DATA4,
						ERROR_DATA5,
						ERROR_DATA6,
						UPDATE_TIMESTAMP)
					SELECT 
						 TSED.EVENT_FK
						,RA.RULES_FK
						,RA.EVENT_CONDITION_CR_FK 
						,RA.ERROR_NAME 
						,( SELECT DNX.LABEL FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.DATA_NAME_ID = RA.BASIS1_DN_FK ) AS DATA1
						,( SELECT CR.CODE FROM EPACUBE.CODE_REF CR WITH (NOLOCK) WHERE CODE_REF_ID = RA.COMPARE_OPERATOR_CR_FK ) AS DATA2 
						,CAST( RA.BASIS1_NUMBER AS VARCHAR(16)) AS DATA3
						,CAST( RA.BASIS2_NUMBER AS VARCHAR(16)) AS DATA4						
						,NULL AS DATA5
						,NULL AS DATA6
						,GETDATE() AS UPDATE_TIMESTAMP 
					 FROM #TS_EVENT_DATA TSED 
					 INNER JOIN #TS_EVENT_DATA_RULES TSEDR WITH (NOLOCK) ON ( TSED.EVENT_FK = TSEDR.EVENT_FK ) 
					 INNER JOIN synchronizer.RULES R WITH (NOLOCK) ON ( R.RULES_ID = TSEDR.RULES_FK )
					 INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) ON ( RA.RULES_FK = R.RULES_ID )
					 WHERE 1 = 1
					 AND   R.RULES_ID = '
					+ @vm$rules_fk   
					+ ' AND ' + @vm$pct_chg_name + ' ' + @vm$compare_operator + ' RA.BASIS1_NUMBER AND RA.BASIS2_NUMBER ' 
					+ ' AND ' + @vm$pct_chg_name + ' IS NOT NULL '
					 



------------------------------------------------------------------------------------------
--- Execute Dynamic SQL
------------------------------------------------------------------------------------------


      IF @ls_exec IS NOT NULL
      BEGIN
	  	
      SET @status_desc =  'synchronizer.EVENT_POST_EXECUTE_RULE:  RULE ID:: '
                        + CAST ( @vm$rules_fk AS VARCHAR(32) )
                                                        
      EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), @ls_exec )

      EXEC sp_executesql 	@ls_exec;


      END



------------------------------------------------------------------------------------------
---  GET NEXT RULE
------------------------------------------------------------------------------------------

        FETCH NEXT FROM cur_v_rule INTO   @vm$rules_action_operation_fk
										 ,@vm$rules_fk										        
										 ,@vm$result_name
										 ,@vm$basis1_name
										 ,@vm$basis2_name
										 ,@vm$basis3_name
										 ,@vm$pct_chg_name										 
										 ,@vm$compare_operator										 
										 ,@vm$basis1_number
										 ,@vm$basis2_number										 
										 
										 
     END --cur_v_rule LOOP
     CLOSE cur_v_rule;
	 DEALLOCATE cur_v_rule; 


--------------------------------------------------------------------------------------------------------------


SET @V_end_TIMESTAMP = getdate()

--      SELECT @v_input_rows = COUNT(1)
--        FROM import.import_Record
--       WHERE job_fk = @in_job_fk;
--
--      SELECT @v_output_rows = COUNT(1)
--        FROM stage.STG_Record
--       WHERE job_fk = @in_job_fk;

	  --SET @v_exception_rows = @v_input_rows - @v_output_rows;
   --   SET @V_END_TIMESTAMP = getdate()
   --   EXEC common.job_execution_create  @in_batch_no, 'END EVENT POST GOVERNANCE',
		 --                                @v_input_rows, @v_output_rows, @v_exception_rows,
			--							 201, @v_START_TIMESTAMP, @V_END_TIMESTAMP;




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_GOVERNANCE'
              + 'BATCH NO:: '  + CAST ( ISNULL ( @in_batch_no, 0 ) AS VARCHAR(16) )
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_GOVERNANCE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END




































































