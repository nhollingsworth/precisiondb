﻿








-- Copyright 2010
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: To support event processing for imports.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/09/2016   Initial SQL Version
-- CV        01/13/2017   Rule type Cr Fk from Rules table
-- CV        03/27/2017   Adjusting to accomadate changes to set up and table no net_value1, but now Value



CREATE PROCEDURE [synchronizer].[EVENT_POSTING_IMPORT_BASIS_VALUES] 
        ( @in_batch_no BIGINT, @in_import_job_fk BIGINT, 
          @in_event_type_cr_fk INT,
          @in_table_name VARCHAR(64),
          @in_data_name_fk BIGINT,
          @in_org_entity_structure_fk VARCHAR(16) )


AS
BEGIN


DECLARE  @ls_exec             NVARCHAR(max)
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @l_sysdate           DATETIME
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME
DECLARE  @v_import_package_fk int
DECLARE @V_START_TIMESTAMP    datetime
DECLARE @V_END_TIMESTAMP      datetime

DECLARE  @v_net_value1		  varchar(32)
DECLARE  @v_net_value2		  varchar(32)
DECLARE  @v_net_value3		  varchar(32)
DECLARE  @v_net_value4		  varchar(32)
DECLARE  @v_net_value5		  varchar(32)
DECLARE  @v_store_pack			varchar(32)


DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int



BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_BASIS_VALUES.' 
					 + 'Batch: ' + cast(isnull(@in_batch_no, 0) as varchar(30))
					 + 'Import: ' + cast(isnull(@in_import_job_fk, 0) as varchar(30))	
					 + 'Event Type: ' + cast(isnull(@in_event_type_cr_fk, 0) as varchar(30))					 				 
					 + 'Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))					 				 					 					 
					 + 'Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))							 
					 + 'Whse: ' + @in_org_entity_structure_fk 							 					 

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_job_id = @in_import_job_fk,
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;
SELECT @v_input_rows = COUNT(1)
FROM import.import_Record WITH (NOLOCK)
WHERE job_fk = @in_import_job_fk;

SELECT @v_output_rows = COUNT(1)
FROM stage.STG_Record WITH (NOLOCK)
WHERE job_fk = @in_import_job_fk;

SET @v_exception_rows = @v_input_rows - @v_output_rows;
SET @V_END_TIMESTAMP = getdate()
EXEC common.job_execution_create  @in_import_job_fk, 'EVENT POSTING BASIS VALUES -  ',
                                 @v_input_rows, @v_output_rows, @v_exception_rows,
								 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;




------------------------------------------------------------------------------------------
--  Create the Dynamic SQL from the DATA SET table
------------------------------------------------------------------------------------------

SET @v_import_package_fk = ( SELECT TOP 1 srec.import_package_fk
                             FROM stage.stg_record srec
                             WHERE srec.batch_no = @in_batch_no )



SET @v_net_value1 =  ISNULL (
					'ID.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
								  ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK  
								  AND DN.RECORD_STATUS_CR_FK = 1)
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) 
								  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								WHERE IMM.IMPORT_PACKAGE_FK = @v_import_package_fk
								AND   DN.DATA_NAME_id = @in_data_name_fk
								AND   DS.COLUMN_NAME = 'VALUE' )
                     , 'NULL' )




SET @v_store_pack =  ISNULL (
					'ID.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)			
								WHERE IMM.IMPORT_PACKAGE_FK = @v_import_package_fk
								AND IMM.RECORD_STATUS_CR_FK = 1
								AND IMM.SOURCE_FIELD_NAME = 'Store Pack' )
                     , 'NULL' )



----SET @v_sheet_name =  ISNULL (
----					'ID.' +	(	SELECT RTRIM ( LTRIM ( IMM.COLUMN_NAME ) )
----								FROM IMPORT.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
----								WHERE IMM.IMPORT_PACKAGE_FK = @v_import_package_fk
----								AND   IMM.SOURCE_FIELD_NAME = 'PRICE SHEET ID' )
----                     , 'NULL' )


-------------------------------------------------------------------
-- INSERT EVENTS  - BASIS VALUES
-------------------------------------------------------------------	


	
	INSERT INTO #TS_EVENT_DATA 
		(   
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			DATA_NAME_FK,			
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			ENTITY_STRUCTURE_FK,
			CO_ENTITY_STRUCTURE_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			TABLE_NAME,
			NET_VALUE1_NEW,
		NET_VALUE2_NEW,
			NET_VALUE1_CUR,
			NET_VALUE2_CUR,
		PACK,
			VALUE_UOM_CODE_FK,
			ASSOC_UOM_CODE_FK,
			END_DATE_CUR,							
			END_DATE_NEW,							
			EFFECTIVE_DATE_CUR,
			EVENT_EFFECTIVE_DATE,
			VALUE_EFFECTIVE_DATE,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,
			EVENT_SOURCE_CR_FK,
			EVENT_PRIORITY_CR_FK,
			EVENT_ACTION_CR_FK,
			TABLE_ID_FK,
		    JOB_CLASS_FK,
		    IMPORT_JOB_FK ,
		    IMPORT_DATE,
		    IMPORT_FILENAME,
		    IMPORT_PACKAGE_FK,
		    STG_RECORD_FK,
		    BATCH_NO,
		    IMPORT_BATCH_NO,
			RESULT_TYPE_CR_FK,
			RECORD_STATUS_CR_FK,		
			UPDATE_TIMESTAMP,
            UPDATE_USER
			 ) 
	SELECT
		       DS.EVENT_TYPE_CR_FK
		      ,DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK
			  ,SRECS.SEARCH_STRUCTURE_FK AS EPACUBE_ID
			  ,DN.DATA_NAME_ID AS DATA_NAME_FK
              ,SRECS.SEARCH_STRUCTURE_FK AS PRODUCT_STRUCTURE_FK
		 	  ,CASE ISNULL ( DS.ORG_IND, 1 )
			   WHEN 1 THEN CASE ISNULL ( SRECEO.ENTITY_STRUCTURE_FK, -999 )
			             WHEN -999 THEN 1
			             ELSE SRECEO.ENTITY_STRUCTURE_FK
			             END
			   ELSE CASE WHEN ISNULL ( DS.CORP_IND, -999 ) = 1
			      THEN 1
			      END
			   END AS ORG_ENTITY_STRUCTURE_FK
              ,DS.ENTITY_STRUCTURE_EC_CR_FK AS ENTITY_CLASS_CR_FK
              ,DN.ENTITY_DATA_NAME_FK
			  ,SRECE.ENTITY_STRUCTURE_FK 
			  ,101 as CO_ENTITY_STRUCTURE_FK
              ,DN.DATA_SET_FK
              ,DS.DATA_TYPE_CR_FK
              ,DS.ORG_IND
              ,DS.TABLE_NAME		
			, [epacube].[getValueFromImportData] (ID.import_record_data_id, SUBSTRING(@v_net_value1,4, LEN(@v_net_value1)-3 )) AS NET_VALUE1_NEW  
	
			  ,PBV.Value  AS  NET_VALUE1_CUR 
			  ,NULL  unit_cost
			  ,PBV.Pack as net_value2_cur

			
			  ,[epacube].[getValueFromImportData] (ID.import_record_data_id, SUBSTRING(@v_store_pack,4, LEN(@v_store_pack)-3 )) AS Pack --store pack
			,NULL as VALUE_UOM_CODE_FK
			,( SELECT TOP 1 PUC.UOM_CODE_FK
			  FROM epacube.PRODUCT_UOM_CLASS PUC WITH (NOLOCK)
			  WHERE ( PUC.DATA_NAME_FK = DN.UOM_CLASS_DATA_NAME_FK
			  AND     PUC.PRODUCT_STRUCTURE_FK = SRECS.SEARCH_STRUCTURE_FK
			  AND   (  (   DS.ORG_IND = 1
					   AND PUC.ORG_ENTITY_STRUCTURE_FK = ISNULL ( SRECEO.ENTITY_STRUCTURE_FK, 1 ))
					  OR PUC.ORG_ENTITY_STRUCTURE_FK = 1 ) 
              AND ISNULL ( PUC.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( SRECE.ENTITY_STRUCTURE_FK, -999 )   )
			) AS ASSOC_UOM_CODE_FK  
			,NULL AS END_DATE
		   ,ID.END_DATE AS END_DATE_NEW
			,PBV.effective_date
           ,CASE WHEN ( SREC.EFFECTIVE_DATE < GETDATE() )
	        THEN GETDATE() 
	        ELSE SREC.EFFECTIVE_DATE
	        END		   
		   ,SREC.EFFECTIVE_DATE
		   ,88 AS EVENT_STATUS_CR_FK	
		   ,97 AS EVENT_CONDITION_CR_FK
		   ,SREC.EVENT_SOURCE_CR_FK
		   ,61 AS EVENT_PRIORITY_CR_FK
           ,CASE ISNULL ( PBV.Pricesheet_Basis_Values_ID, 0 )
            WHEN 0 THEN 51
            ELSE 52
            END AS EVENT_ACTION_CR_FK			
		   ,PBV.Pricesheet_Basis_Values_ID AS TABLE_ID_FK
		   ,IP.JOB_CLASS_FK
		   ,SREC.JOB_FK AS IMPORT_JOB_FK
		   ,SREC.CREATE_TIMESTAMP AS IMPORT_DATE
		   ,SREC.IMPORT_FILENAME
		   ,SREC.IMPORT_PACKAGE_FK
		   ,SREC.STG_RECORD_ID AS STG_RECORD_FK
		   ,SREC.BATCH_NO
		   ,SREC.BATCH_NO AS IMPORT_BATCH_NO
		   ,711 AS RESULT_TYPE_CR_FK
		   ,1 AS RECORD_STATUS_CR_FK
		   ,GETDATE()
           ,'EPACUBE'
        FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
		INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
		  ON ( SRECS.STG_RECORD_FK = SREC.STG_RECORD_ID )	                	        
        INNER JOIN IMPORT.IMPORT_RECORD_DATA ID WITH (NOLOCK)
              ON ( ID.JOB_FK = SREC.JOB_FK
              AND  ID.RECORD_NO = SREC.RECORD_NO 
		AND ISNULL ( ID.DO_NOT_IMPORT_IND, 0 ) <> 1)
        INNER JOIN IMPORT.IMPORT_PACKAGE IP WITH (NOLOCK)
			  ON ( IP.IMPORT_PACKAGE_ID = ID.IMPORT_PACKAGE_FK )  
		INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)                
		      ON ( DN.RECORD_STATUS_CR_FK = IP.RECORD_STATUS_CR_FK )
        INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)                
		      ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )       
		LEFT JOIN  STAGE.STG_RECORD_ENTITY SRECEO WITH (NOLOCK)
		ON ( SRECEO.STG_RECORD_FK = SREC.STG_RECORD_ID 
		AND  SRECEO.SEQ_ID = ID.SEQ_ID              
		AND  SRECEO.ENTITY_CLASS_CR_FK  = 10101 
		AND  DS.ORG_IND = 1) 
       LEFT JOIN  STAGE.STG_RECORD_ENTITY SRECE WITH (NOLOCK)
		ON ( SRECE.STG_RECORD_FK = SREC.STG_RECORD_ID 
		AND  SRECE.SEQ_ID = ID.SEQ_ID              
		AND  SRECE.ENTITY_DATA_NAME_FK = DN.ENTITY_DATA_NAME_FK )
    -------  
	 LEFT JOIN marginmgr.PRICESHEET_BASIS_VALUES PBV WITH (NOLOCK)
         ON ( PBV.DATA_NAME_FK = DN.DATA_NAME_ID
         AND  PBV.PRODUCT_STRUCTURE_FK = SRECS.SEARCH_STRUCTURE_FK
		 AND  PBV.Effective_Date =  cast(SREC.EFFECTIVE_DATE as date)
         AND  PBV.ORG_ENTITY_STRUCTURE_FK = ISNULL ( SRECEO.ENTITY_STRUCTURE_FK, 1 ) 
         --AND  ISNULL ( PBV.VENDOR_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( SRECE.ENTITY_STRUCTURE_FK, -999 )   
		 ) 
		 WHERE 1 = 1 
		 AND ISNULL(SREC.BATCH_NO,-999) = cast(isnull (@in_batch_no, -999) as varchar(20))
		 AND   SREC.EVENT_ACTION_CR_FK IN ( 51, 52, 53, 54 )  
			   AND   SREC.EVENT_CONDITION_CR_FK IN ( 97, 98 ) 
			AND   DN.DATA_NAME_ID = cast(isnull (@in_data_name_fk, -999) as varchar(20))
		 AND   ( @in_org_entity_structure_fk = -999  OR ISNULL ( SRECEO.ENTITY_STRUCTURE_FK, 1 ) =  @in_org_entity_structure_fk
		 ) 


----           TRUNCATE TABLE COMMON.T_SQL


         --exec sp_executesql 	@ls_exec;



				                             
--------------------------------------------------------------------------------------------
--   UPDATE PERCENT_CHANGE AND UOM VALUES
--			SPLIT OUT FROM DYNAMIC SQL BECAUSE TEXT WAS TOO LONG
--          CV Added WHEN -999 THEN 0 WHEN 0 THEN .001 in order to create change event when 
--          current value is zero.
--------------------------------------------------------------------------------------------
---Not needed.
------UPDATE #TS_EVENT_DATA
------SET VALUE_UOM_CODE_FK = (select top 1 puc.uom_code_fk
------FROM   #TS_EVENT_DATA TSED
------INNER JOIN epacube.product_uom_class puc with (nolock)
------ON (puc.product_structure_fk = TSED.product_structure_fk)
------INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
------ON ( DN.DATA_NAME_ID = TSED.DATA_NAME_FK )
------INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK)
------ ON ( DNX.DATA_NAME_ID = DN.UOM_CLASS_DATA_NAME_FK )
------AND puc.data_name_fk = DN.UOM_CLASS_DATA_NAME_FK
------AND VALUE_UOM_CODE_FK IS NULL)

UPDATE #TS_EVENT_DATA
set net_value2_new = 
case when @in_data_name_fk = 503201 then (net_value1_new/isNULL(PACK,1)) else NULL END



UPDATE #TS_EVENT_DATA
SET  PERCENT_CHANGE1 = ( CASE ISNULL ( NET_VALUE1_CUR, -999 )
						 WHEN -999 THEN NULL
						 WHEN 0 THEN 0 
						 ELSE ( ( NET_VALUE1_NEW  - NET_VALUE1_CUR ) / NET_VALUE1_CUR )
						 END )
    
  --  ,UOM_CONVERSION_FACTOR = 
		--( SELECT CASE ISNULL ( VALUE_UOM_CODE_FK, 0 ) 
		--			WHEN 0 THEN 1
		--			ELSE
		--			CASE ISNULL ( ASSOC_UOM_CODE_FK, 0 ) 
		--			WHEN 0 THEN 1
		--			ELSE
		--			( ISNULL (
		--			( SELECT 
		--			( SELECT ISNULL (
		--			   (SELECT uc.primary_qty
		--				FROM epacube.UOM_CODE uc WITH (NOLOCK)
		--				WHERE uc.UOM_CODE_ID = VALUE_UOM_CODE_FK  ), 1 )
		--			)
		--			/
		--			ISNULL (   (SELECT uc.primary_qty
		--						FROM epacube.UOM_CODE uc WITH (NOLOCK)
		--						WHERE uc.UOM_CODE_ID = ASSOC_UOM_CODE_FK )
		--					 , ISNULL ( (SELECT uc.primary_qty
		--								 FROM epacube.UOM_CODE uc WITH (NOLOCK)
		--								 WHERE uc.UOM_CODE_ID = VALUE_UOM_CODE_FK) , 1 ) 
		--					 )
		--			) , 1 ) )
		--	END END
		--	) 



--------------------------------------------------------------------------------------------
--   EVENTS WHERE ALL VALUES ARE NULL
--		UNDERSTANDING IS THAT THE INTENT IS TO NULL OUT ALL EXISTING EVENTS
--      ACTION WILL BE TO CHANGE RECORD STATUS OF EVENT TO 53  -- INACTIVATE
--------------------------------------------------------------------------------------------


UPDATE #TS_EVENT_DATA
SET RECORD_STATUS_CR_FK = 53
WHERE EVENT_FK IN (
	SELECT EVENT_FK
	FROM #TS_EVENT_DATA
	WHERE 1 = 1
	AND   EVENT_ACTION_CR_FK = 52   -- CHANGE
	AND    ISNULL ( NET_VALUE1_NEW, -999999999 ) = -999999999  	   
	     )

	

				                             
--------------------------------------------------------------------------------------------
--   REMOVE CHANGE EVENTS IF THERE ARE NO CHANGES IN THE NET VALUES
----  Need to add end date as we would like to have an end dated event
--------------------------------------------------------------------------------------------

DELETE 
FROM #TS_EVENT_DATA
WHERE EVENT_FK IN (
	SELECT EVENT_FK
	FROM #TS_EVENT_DATA
	WHERE 1 = 1
	AND   EVENT_ACTION_CR_FK = 52   -- CHANGE
AND    ISNULL ( NET_VALUE1_NEW, -999 )  =   -999 )
           
   

				                             
--------------------------------------------------------------------------------------------
--   CALL VALIDATIONS
--------------------------------------------------------------------------------------------
    

	------------------------------------------------------------------------------------------
	--   QUALIFY GOVERNANCE RULES 
	------------------------------------------------------------------------------------------
	SET  @v_count = ( SELECT COUNT(1)
					 FROM #TS_EVENT_DATA WITH (NOLOCK)
					 WHERE BATCH_NO = @in_batch_no
					 AND   EVENT_STATUS_CR_FK = 88 
					 AND   EVENT_TYPE_CR_FK IN ( 158 )   			 
					 AND   DATA_NAME_FK IN ( SELECT R.RESULT_DATA_NAME_FK   
											 FROM synchronizer.RULES R WITH (NOLOCK)
											 --INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
											 --  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK 
											 --  AND  RS.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE
											 WHERE R.RECORD_STATUS_CR_FK = 1 
											 AND  R.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE)
					OR DATA_NAME_FK IN (SELECT DISTINCT DN.DATA_NAME_ID  
								FROM synchronizer.RULES R WITH (NOLOCK)
							--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
							--ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK 
							--AND  RS.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE
							INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
							ON ( R.RESULT_DATA_NAME_FK = DN.DATA_NAME_id)
							--AND DN.PARENT_DATA_NAME_FK IS NOT NULL)
							WHERE R.RECORD_STATUS_CR_FK = 1 
							AND  R.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE)
					)			                         
	IF @v_count > 0
		EXEC synchronizer.EVENT_POST_QUALIFY_RULES_GOVERNANCE  @in_batch_no


 --   EXEC [synchronizer].[EVENT_POST_VALID_PROD_ENTITY] 
	
	EXEC [synchronizer].[EVENT_POST_VALID_TYPE_151_152] 
		    
	--EXEC [synchronizer].[EVENT_POST_VALID_TYPE_ALL] 
		    



------------------------------------------------------------------------------------------
-- CALL PROC TO SET EVENT PRIORITY ACTION AND STATUS   ( KEEP IN ONE PLACE WITH CODE )
------------------------------------------------------------------------------------------

	EXEC  [synchronizer].[EVENT_POST_COND_PRIORITY_ACTION_STATUS] @in_batch_no




--------------------------------------------------------------------------------------------
--   CALL POST AND COMPLETE 
--------------------------------------------------------------------------------------------

		EXEC SYNCHRONIZER.EVENT_POST_DATA  

		UPDATE #TS_EVENT_DATA
		SET EVENT_STATUS_CR_FK = 80   -- RETURN TO PENDING
		WHERE EVENT_STATUS_CR_FK in ( 86, 87, 88 ) 


       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows Returned to Pending = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
       END






------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_BASIS_VALUES'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_BASIS_VALUES has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END






























