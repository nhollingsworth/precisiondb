﻿







-- Copyright 2006 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott (original Oracle version by Kathi Scott)
--
--
-- Purpose: Approve future dated events, then call the maintenance procedures which
--			remove products flagged for purging as well as orphan products.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        07/12/2006   Initial SQL Version
-- WT        09/27/2006   Added Select Counts to determine if procedures need to be called
--						  instead of always calling them.
-- KS        09/28/2006   Added Distinct to select (1) for Conditional Procedure calling
-- KS        10/08/2006   Leaving DRank for right now  ( small sets of data.. should be okay )
-- KS        07/01/2008   Added Sheet Results 
-- KS        12/30/2008	  Updated to V5
-- RG		 03/10/2009   Added call to nightly purge
-- RG		 07/13/2009   V5-ification
-- CV        11/06/2009   Adding event posting and effective date logic EPA -2649
-- CV        10/27/2009   Adding rules inactivation based on end_date 
-- CV        04/22/2010   EPA - 2922  - updated the partition by to include org and entity
-- CV        04/28/2010   EPA - 2942  call purge import data 
-- CV        04/26/2011   Remove code to call purge import data
--						  Add Code to remove inactive records once data exported to HOST	
-- KS        10/31/2012   According to Gary we should add an extra day before Inactivating Rules ( Issue reported at DCA )
-- KS        11/19/2012   Gary had me change the inactivation of Rules yet again... see below
-- CV        12/17/2012   Add in the beginning to Purge the logs ..  EPA - 3447 - fixed code
-- CV        09/10/2013   Added code to set product values and results to inactive if end date  <=  @l_sysdate
-- CV        03/17/2014   Added check to try and process HOLD events..  and new call to archive future sheet results
-- CV        04/14/2014   Add product mult type table to set record status cr fk = 2 with end date.
-- CV        04/24/2014   Add run of Calc job 
-- CV        04/28/2014   Add event data history for results inactivated
-- CV        04/29/2014   Adding end date check to for product attribute
-- CV        05/07/2014   Removed the future processing to it's own procedure
-- CV        09/18/2014   added to not check record status so not updating end data rules each day


  CREATE PROCEDURE [synchronizer].[NIGHTLY_SYNCHRONIZER] 
    AS
    BEGIN
      DECLARE 
        @l_sysdate datetime,
        @ls_stmt varchar(1000),
        @l_exec_no bigint,
        @status_desc varchar(max),     
        @v_batch_no bigint, 
        @v_Future_eff_days int, 
        @v_approve_event_date datetime, 
	    @FutureEventStatus int;
      SET @l_sysdate = getdate()

--------------------------------------------------------------------------

  IF (select ISNULL(count(*),0) from exec_monitor.errlog) <= 0
   
BEGIN

TRUNCATE TABLE exec_monitor.errlog
TRUNCATE TABLE exec_monitor.statuslog
TRUNCATE TABLE job_manager.JobQueue
DELETE FROM exec_manager.StatementQueue
DELETE from exec_monitor.ExecLog
truncate table common.t_sql

END

  
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'THERE ARE STILL ERRORS IN LOG. PLEASE CLEAR LOGS IF SOLVED ' ,
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
 



---------------------------------------------------------------------------

 EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of SYNCHRONIZER.NIGHTLY_SYNCHRONIZER' , 
												@exec_id = @l_exec_no OUTPUT;
        

        /* ----------------------------------------------------------------------------------- */
        --    Procedure for all the Nightly Housekeeping 
        /* ----------------------------------------------------------------------------------- */
BEGIN TRY

 		
		SET @FutureEventStatus = epacube.getCodeRefId('EVENT_STATUS','FUTURE')
		SET @v_FUTURE_EFF_DAYS = (select value from epacube.epacube_params
									where name = 'FUTURE_EFF_DAYS')
       

-----------------------------------------------------------------------------------------
------   FUTURE event processing  APPROVE events
-----------------------------------------------------------------------------------------

----NEW PROCESS FOR ALL CUSTOMERS TO SCHEDULE  

EXECUTE [synchronizer].[EVENT_POSTING_FUTURE] 

---------------------------------------------------------------------------------------------
------------ 
------------------------------------------------------------------------------------------------------
Declare @CustomerName varchar(25);
SET @CustomerName = (SELECT Value from epacube.EPACUBE_PARAMS where name = 'EPACUBE CUSTOMER')
IF (@CustomerName = 'Johnstone')
   
   BEGIN
		
		EXECUTE  [epacube].[NIGHTLY_ARCHIVE_SHEET_RESULTS] 

	END

-----------------------------------------------------------------------------------------------------

		EXEC epacube.NIGHTLY_PURGE



---------------------------------------------------------------------------------------
----   Inactivate Expired Rules and change FUTURE rules to CURRENT
-----   added to update the timestamp so calcs will qualify for recalc
---------------------------------------------------------------------------------------

	UPDATE synchronizer.rules
	SET record_status_cr_fk = 2
	,UPDATE_TIMESTAMP =  @l_sysdate
	WHERE end_date <= @l_sysdate          ----   <= @l_sysdate + 1   --- added an extra day
	and RECORD_STATUS_CR_FK = 1

	--UPDATE synchronizer.rules
	--SET    rule_result_type_cr_fk = 710
	--,UPDATE_TIMESTAMP =  @l_sysdate  
	--WHERE  rule_result_type_cr_fk = 711
 --   and    effective_date <= @l_sysdate    -----  <= @l_sysdate + 1  -- added an extra day

	
---------------------------------------------------------------------------------------
----   Inactivate Expired results and values according to end date on record
----   Created new procedure to do this
---------------------------------------------------------------------------------------

    --EXECUTE  [epacube].[NIGHTLY_INACTIVE_END_DATED] 
    
---------------------------------------------------------------------------------------
----   Remove Inactivated / -999999999 data from epacube tables if exported
---add block of code to remove from all epacube tables where value = -999999999
--- record status cr fk = 2 and update timestamp before last export.

---------------------------------------------------------------------------------------

	--EXECUTE  [marginmgr].[RUN_CALC_RESULTS_SINCE_LAST_JOB_QUARTZ]  ---epa - 3770


	--EXECUTE  [synchronizer].[PURGE_INACTIVE_AFTER_EXPORT] 


---------------------------------------------------------------------------------------
----   End of Processing Nightly Synchronizer
---------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of SYNCHRONIZER.NIGHTLY_SYNCHRONIZER';
     EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
      
END TRY
BEGIN CATCH   
   --   DECLARE @ErrorMessage NVARCHAR(4000);
	  --DECLARE @ErrorSeverity INT;
	  --DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.NIGHTLY_SYNCHRONIZER has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
        
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
END





