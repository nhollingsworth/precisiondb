﻿




-- Copyright 2006
--
-- Procedure created by Walt Tucholski
--
--
-- Purpose: Called from the UI to perform PRODUCT_TAX_PROPERTIES
--			table maintenance.
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- WT        04/03/2007  Initial SQL Version
--
CREATE PROCEDURE [synchronizer].[PRODUCT_TAX_PROPERTIES_EDITOR] 
( @in_product_tax_properties_id bigint,
  @in_value varchar (256),
  @in_product_structure_fk bigint,
  @in_taxonomy_properties_fk int,
  @in_ui_action_cr_fk int)
												
AS

/***************  Parameter Defintions  **********************
CURRENTLY USED IN THE DATA MASTER EDITOR....

Simple for Now.... Only CREATE and CHANGE;

***************************************************************/

BEGIN

DECLARE @v_product_structure_fk bigint
DECLARE @v_taxonomy_properties_fk int
DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int


SET NOCOUNT ON;
set @ErrorMessage = null

IF @in_ui_action_cr_fk in (8003,8006)
BEGIN
	 IF   @in_value IS NULL 
     BEGIN
		SET @ErrorMessage = 'Value must be entered'
        GOTO RAISE_ERROR
	 END
END
--
IF @in_ui_action_cr_fk = 8003 --create
BEGIN

-------------------------------------------------------------------------
--   Check for Duplicate Prior to Insert
-------------------------------------------------------------------------------------
	SELECT @ErrorMessage = 'Duplicate Product Taxonomy Property. '
        + 'Value already exists for product_tax_properties_id '
        + CAST(isnull(product_tax_properties_id,0) AS VARCHAR(30))
    FROM epacube.product_tax_properties
    WHERE product_structure_fk = @in_product_structure_fk
    and   taxonomy_properties_fk = @in_taxonomy_properties_fk
    and   upper(value) = upper(@in_value)

	 IF @@rowcount > 0
			GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
--   Edits passed Insert Row
-------------------------------------------------------------------------------------

    INSERT INTO epacube.PRODUCT_TAX_PROPERTIES
       (product_structure_fk
       ,taxonomy_properties_fk
       ,value
  	   ,create_timestamp
	   ,update_timestamp)
	SELECT
		@in_product_structure_fk,
		@in_taxonomy_properties_fk,
		upper(@in_value),
		getdate(),
		getdate()

END --- IF @in_ui_action_cr_fk = 8003

IF @in_ui_action_cr_fk = 8006 --change
BEGIN
-------------------------------------------------------------------------------------
--   Check for Duplicate Prior to update
-------------------------------------------------------------------------------------
    select @v_product_structure_fk = product_structure_fk
		   ,@v_taxonomy_properties_fk = taxonomy_properties_fk
	from epacube.product_tax_properties
	where product_tax_properties_id = @in_product_tax_properties_id

	SELECT @ErrorMessage = 'Duplicate Product Taxonomy Property. '
        + 'Value already exists for product_tax_properties_id '
        + CAST(isnull(product_tax_properties_id,0) AS VARCHAR(30))
    FROM epacube.product_tax_properties
    WHERE product_structure_fk = @v_product_structure_fk
    and   taxonomy_properties_fk = @v_taxonomy_properties_fk
    and   upper(value) = upper(@in_value)
    and   product_tax_properties_id <> @in_product_tax_properties_id

	 IF @@rowcount > 0
			GOTO RAISE_ERROR
-------------------------------------------------------------------------------------
--   Edits passed Update Row
-------------------------------------------------------------------------------------
     UPDATE epacube.product_tax_properties
       SET value = upper(@in_value)
		  ,update_timestamp = getdate()
	 WHERE product_tax_properties_id = @in_product_tax_properties_id

END --- IF @in_ui_action_cr_fk = 8006


RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END























































