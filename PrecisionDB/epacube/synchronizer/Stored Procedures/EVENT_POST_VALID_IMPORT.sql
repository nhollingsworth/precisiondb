﻿

-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform validations on import files ONLY
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- KS        05/15/2010   Initial SQL Version
-- CV        05/25/2010   Add insert for results/values
-- CV        02/23/2011   Added to not use event action 56 or 59 -no change
-- CV        06/26/2011   Changed bigint to varchar for curser config fk  epa-3167
-- CV        11/21/2017   Added to check for ARP Vend and Vend Prod changes sup-11136
-- CV        03/04/2020   Added change had to be approved and preposted.


CREATE  PROCEDURE [synchronizer].[EVENT_POST_VALID_IMPORT] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime
DECLARE  @v_check_export_ind  bigint
DECLARE  @v_export_name       varchar (50)
DECLARE  @v_import_name       varchar (50)
DECLARE  @v_Future_eff_days int

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_IMPORT.' 
EXEC epacube_dev.exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;


----------------------------------------------------------------------
---USED TO PARAMARTERIZE IF EXPORTED DATA NEEDS TO BE CHECKED--
    -- CAN ONLY USE ONE EXPORT NAME WHEN PASSED TO FUNCTION --
----------------------------------------------------------------------

SET @v_check_export_ind = (select value from epacube.epacube_params
where name = 'CHECK_EXPORTED');


------------------------------------------------------------------------------------------
-- CHECK EVENT IF SHOULD BE PLACED ON HOLD AND YELLOW CONFLICT ERROR
--		DUE TO PREVIOUS CHANGE IN EPACUBE HAS NOT YET BEEN EXPORTED TO HOST 
--		AND HOST IMPORT CHANGE IS NOW BEING APPLIED 
------------------------------------------------------------------------------------------

IF @v_check_export_ind = 1 

BEGIN

------------------------------------------------------------------------------------------
-- CHECK EXPORT CONFIGS FOR CHANGE EXPORTS ONLY
------------------------------------------------------------------------------------------


      DECLARE 
              @vm$config_fk				varchar(256),  --bigint,   
              @vm$config_name           varchar(256)

      DECLARE  cur_v_chg_export cursor local FOR
			 SELECT 
					A.config_fk,
					A.config_name
			  FROM (
					SELECT CNFJ.CONFIG_ID as CONFIG_FK,  CNFJ.NAME AS CONFIG_NAME
					FROM epacube.CONFIG CNFJ WITH (NOLOCK)
					INNER JOIN epacube.CONFIG_PROPERTY CNFJPCHG
						ON ( CNFJ.CONFIG_ID = CNFJPCHG.CONFIG_FK
						AND  CNFJPCHG.NAME = 'FILTER_CHANGES'   -- CHANGES ONLY 
						AND  CNFJPCHG.VALUE = 'TRUE'  ) 
			) A
			  ORDER BY A.config_name


        OPEN cur_v_chg_export
        FETCH NEXT FROM cur_v_chg_export INTO @vm$config_fk
                                             ,@vm$config_name


         WHILE @@FETCH_STATUS = 0 
         BEGIN

--PRODUCT DATA


		INSERT INTO #TS_EVENT_DATA_ERRORS 
			(EVENT_FK
		   ,RULE_FK
		   ,EVENT_CONDITION_CR_FK
		   ,ERROR_NAME
			,ERROR_DATA1
		   ,UPDATE_TIMESTAMP)
		SELECT 
			 TSED.EVENT_FK
			,NULL  --TEDE.RULE_FK
			,98   -- HOLD WARNING
			,'HOST IMPORT OVERRIDING POSTED DATA NOT YET EXPORTED'
			,@vm$config_name
			,GETDATE ()  --TEDE.UPDATE_TIMESTAMP
		FROM #TS_EVENT_DATA TSED
		WHERE TSED.EVENT_SOURCE_CR_FK <> 78 --- UNDO DO NOT CHECK
		AND   TSED.EVENT_FK IN (   
			SELECT TSED2.EVENT_FK
			FROM EPACUBE.CONFIG_DATA_NAME CDN WITH (NOLOCK)
			INNER JOIN #TS_EVENT_DATA TSED2 
			 ON (  TSED2.DATA_NAME_FK = CDN.DATA_NAME_FK 
			 AND   TSED2.EVENT_SOURCE_CR_FK = 79  -- HOST IMPORT
			 AND   TSED2.EVENT_ACTION_CR_FK = 52  )
			INNER JOIN SYNCHRONIZER.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
			 ON   (EDH.DATA_NAME_FK = TSED2.DATA_NAME_FK 
			 AND   EDH.PRODUCT_STRUCTURE_FK = TSED2.PRODUCT_STRUCTURE_FK 
			 AND   EDH.ORG_ENTITY_STRUCTURE_FK = TSED2.ORG_ENTITY_STRUCTURE_FK 
			 AND   EDH.EVENT_ACTION_CR_FK NOT IN ( 56, 59 )    --- no change and purge should not export
			 AND   ISNULL ( EDH.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED2.ENTITY_STRUCTURE_FK, 0 ) 
			 AND   EDH.UPDATE_TIMESTAMP >= 
										( SELECT MAX ( JOB.JOB_COMPLETE_TIMESTAMP )
											FROM COMMON.JOB JOB WITH (NOLOCK)
											WHERE NAME = @vm$config_name  ) )
		 WHERE CDN.CONFIG_FK = ( SELECT CAST ( VALUE AS INT )
								 FROM epacube.CONFIG CNFJ WITH (NOLOCK)
								 INNER JOIN EPACUBE.CONFIG_PROPERTY CNFJP WITH (NOLOCK)
								 ON ( CNFJP.CONFIG_FK = CNFJ.CONFIG_ID
								 AND  CNFJP.NAME = 'COLUMNS_CONFIG_ID' )
								 WHERE CNFJ.NAME = @vm$config_name ) 
		 )


---- Values and Results

INSERT INTO #TS_EVENT_DATA_ERRORS 
			(EVENT_FK
		   ,RULE_FK
		   ,EVENT_CONDITION_CR_FK
		   ,ERROR_NAME
			,ERROR_DATA1
		   ,UPDATE_TIMESTAMP)
		SELECT 
			 TSED.EVENT_FK
			,NULL  --TEDE.RULE_FK
			,98   -- HOLD WARNING
			,'HOST IMPORT OVERRIDING POSTED DATA NOT YET EXPORTED'
			,@vm$config_name
			,GETDATE ()  --TEDE.UPDATE_TIMESTAMP
		FROM #TS_EVENT_DATA TSED
		WHERE TSED.EVENT_SOURCE_CR_FK <> 78 --- UNDO DO NOT CHECK
		AND  TSED.EVENT_FK IN (   
				SELECT TSED2.EVENT_FK
			FROM EPACUBE.CONFIG_DATA_NAME CDN WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN
				ON ( CDN.DATA_NAME_FK = DN.DATA_NAME_ID)
			INNER JOIN #TS_EVENT_DATA TSED2
				ON ( TSED2.DATA_NAME_FK = DN.PARENT_DATA_NAME_FK
				AND   TSED2.EVENT_SOURCE_CR_FK = 79  -- HOST IMPORT
				AND   TSED2.EVENT_ACTION_CR_FK = 52 )				
			INNER JOIN SYNCHRONIZER.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
			 ON   (EDH.DATA_NAME_FK = TSED2.DATA_NAME_FK
			 AND   EDH.EVENT_ACTION_CR_FK NOT IN ( 56, 59 )    --- no change and purge should not export
			 AND   EDH.PRODUCT_STRUCTURE_FK = TSED2.PRODUCT_STRUCTURE_FK 
			 AND   EDH.ORG_ENTITY_STRUCTURE_FK = TSED2.ORG_ENTITY_STRUCTURE_FK 
			 AND   ISNULL ( EDH.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED2.ENTITY_STRUCTURE_FK, 0 ) 
			 AND   EDH.EVENT_STATUS_CR_FK in ( 81, 83)
			 AND   EDH.UPDATE_TIMESTAMP >= 
										( SELECT MAX ( JOB.JOB_COMPLETE_TIMESTAMP )
											FROM COMMON.JOB JOB WITH (NOLOCK)
											WHERE NAME = @vm$config_name  ) )
			AND CDN.CONFIG_FK = ( SELECT CAST ( VALUE AS INT )
								 FROM epacube.CONFIG CNFJ WITH (NOLOCK)
								 INNER JOIN EPACUBE.CONFIG_PROPERTY CNFJP WITH (NOLOCK)
								 ON ( CNFJP.CONFIG_FK = CNFJ.CONFIG_ID
								 AND  CNFJP.NAME = 'COLUMNS_CONFIG_ID' )
								 WHERE CNFJ.NAME = @vm$config_name ) )


------------ CHECK ARP VEND and VEND PROD CHANGES   SUP - 11136

INSERT INTO #TS_EVENT_DATA_ERRORS 
			(EVENT_FK
		   ,RULE_FK
		   ,EVENT_CONDITION_CR_FK
		   ,ERROR_NAME
			,ERROR_DATA1
		   ,UPDATE_TIMESTAMP)
		SELECT 
			 TSED.EVENT_FK
			,NULL  --TEDE.RULE_FK
			,98   -- HOLD WARNING
			,'HOST IMPORT OVERRIDING POSTED DATA NOT YET EXPORTED'
			,'ICSW VPN Export CHANGES'
			,GETDATE ()  --TEDE.UPDATE_TIMESTAMP
		FROM #TS_EVENT_DATA TSED
		WHERE TSED.EVENT_SOURCE_CR_FK <> 78 --- UNDO DO NOT CHECK
		AND   TSED.EVENT_FK IN (   
			SELECT TSED2.EVENT_FK
			FROM #TS_EVENT_DATA TSED2 
			INNER JOIN SYNCHRONIZER.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
			 ON   (EDH.DATA_NAME_FK = TSED2.DATA_NAME_FK 
			 AND   EDH.PRODUCT_STRUCTURE_FK = TSED2.PRODUCT_STRUCTURE_FK 
			 AND   EDH.ORG_ENTITY_STRUCTURE_FK = TSED2.ORG_ENTITY_STRUCTURE_FK 
			 AND   EDH.EVENT_ACTION_CR_FK NOT IN ( 56, 59 )    --- no change and purge should not export
			 AND   ISNULL ( EDH.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED2.ENTITY_STRUCTURE_FK, 0 ) )
	WHERE TSED2.DATA_NAME_FK in (110111, 253501)
			 AND   TSED2.EVENT_SOURCE_CR_FK = 79  -- HOST IMPORT
			 AND   TSED2.EVENT_ACTION_CR_FK = 52 
			 AND   EDH.EVENT_STATUS_CR_FK in ( 81, 83)
			 AND   EDH.UPDATE_TIMESTAMP >= 
										( SELECT MAX ( JOB.JOB_COMPLETE_TIMESTAMP )
											FROM COMMON.JOB JOB WITH (NOLOCK)
											WHERE NAME = 'ICSW VPN Export CHANGES') 

		)






     FETCH NEXT FROM cur_v_chg_export INTO @vm$config_fk
                                          ,@vm$config_name


     END --cur_v_chg_export
     CLOSE cur_v_chg_export;
	 DEALLOCATE cur_v_chg_export; 



UPDATE #TS_EVENT_DATA
SET EVENT_STATUS_CR_FK = 85  -- HOLD
   ,EVENT_CONDITION_CR_FK = 98  -- YELLOW
WHERE EVENT_FK IN (   
		SELECT TSED.EVENT_FK
        FROM #TS_EVENT_DATA TSED
        INNER JOIN #TS_EVENT_DATA_ERRORS TSEDE
           ON ( TSEDE.EVENT_FK = TSED.EVENT_FK )
        WHERE TSEDE.ERROR_NAME = 'HOST IMPORT OVERRIDING POSTED DATA NOT YET EXPORTED' )


END



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_IMPORT'
Exec epacube_dev.exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
Exec epacube_dev.exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_IMPORT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC epacube_dev.exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC epacube_dev.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
















