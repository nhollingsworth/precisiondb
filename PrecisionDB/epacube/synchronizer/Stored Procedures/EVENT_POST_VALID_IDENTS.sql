﻿






-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform ident validations
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010   Initial SQL Version
-- CV        06/11/2011   Do not validate -999999999 data
-- KS        11/26/2012   Added a TOP 1 for a subquery error dbexceptions
--


CREATE PROCEDURE [synchronizer].[EVENT_POST_VALID_IDENTS] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_IDENTS.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;



------------------------------------------------------------------------------------------
--   CHECK FOR DUPLICATE UNIQUE IDENTIFIERS
------------------------------------------------------------------------------------------


			INSERT INTO #TS_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		(	SELECT 
			 TSED.EVENT_FK
			,NULL
			,99
			,'THIS UNIQUE IDENTIFIER ALREADY EXISTS.'  --ERROR NAME        
			,'SEE PRODUCT_ID: '--ERROR1
            ,(SELECT PI.VALUE FROM EPACUBE.PRODUCT_IDENTIFICATION PI
			  WHERE PI.PRODUCT_STRUCTURE_FK = (SELECT TOP 1 product_structure_fk FROM epacube.product_identification  WITH (NOLOCK)
												WHERE PRODUCT_STRUCTURE_FK <> TSED.PRODUCT_STRUCTURE_FK  
												  AND  ORG_ENTITY_STRUCTURE_FK = TSED.ORG_ENTITY_STRUCTURE_FK
												  AND  ISNULL ( ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) 
												  AND  VALUE = TSED.NEW_DATA)
				AND  PI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
			                          FROM EPACUBE.EPACUBE_PARAMS EEP
			                          WHERE EEP.APPLICATION_SCOPE_FK = 100
			                          AND   EEP.NAME = 'PRODUCT' ) )
            ,'ATEMPTING TO UPDATE: '
			,( SELECT VALUE FROM EPACUBE.PRODUCT_IDENTIFICATION  WITH (NOLOCK)
			   WHERE PRODUCT_STRUCTURE_FK = TSED.PRODUCT_STRUCTURE_FK
			   AND   DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
			                          FROM EPACUBE.EPACUBE_PARAMS EEP
			                          WHERE EEP.APPLICATION_SCOPE_FK = 100
			                          AND   EEP.NAME = 'PRODUCT' )  ) -- PRIMARY IDENTIFIER
			,NULL
			,NULL
			,@l_sysdate
			FROM #TS_EVENT_DATA TSED	
			INNER JOIN epacube.PRODUCT_IDENTIFICATION VCPD WITH (NOLOCK) 
			  ON ( VCPD.DATA_NAME_FK = TSED.DATA_NAME_FK 
			  AND  VCPD.PRODUCT_STRUCTURE_FK <> TSED.PRODUCT_STRUCTURE_FK ---- KS ADDED THIS TO INGORE NO CHANGE EVENTS 
			  AND  VCPD.ORG_ENTITY_STRUCTURE_FK = TSED.ORG_ENTITY_STRUCTURE_FK
			  AND  ISNULL ( VCPD.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) 
			  AND  VCPD.VALUE = TSED.NEW_DATA
			  AND TSED.NEW_DATA <> '-999999999' )
			WHERE TSED.TABLE_NAME IN ( 'PRODUCT_IDENTIFICATION' ) )




			INSERT INTO #TS_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		(	SELECT 
			 TSED.EVENT_FK
			,NULL
			,99
			,'UNIQUE IDENTIFIER ALREADY EXISTS. '  --ERROR NAME
			,'SEE PRODUCT_ID: '--ERROR1
            ,(SELECT PI.VALUE FROM EPACUBE.PRODUCT_IDENTIFICATION PI
			  WHERE PI.PRODUCT_STRUCTURE_FK = (SELECT TOP 1 product_structure_fk FROM epacube.PRODUCT_IDENT_MULT  WITH (NOLOCK)
												WHERE PRODUCT_STRUCTURE_FK <> TSED.PRODUCT_STRUCTURE_FK  
												  AND  ORG_ENTITY_STRUCTURE_FK = TSED.ORG_ENTITY_STRUCTURE_FK
												  AND  ISNULL ( ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) 
												  AND  VALUE = TSED.NEW_DATA)
				AND  PI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
			                          FROM EPACUBE.EPACUBE_PARAMS EEP
			                          WHERE EEP.APPLICATION_SCOPE_FK = 100
			                          AND   EEP.NAME = 'PRODUCT' ) )
            ,'ATEMPTING TO UPDATE: '
			,( SELECT VALUE FROM EPACUBE.PRODUCT_IDENTIFICATION  WITH (NOLOCK)
			   WHERE PRODUCT_STRUCTURE_FK = TSED.PRODUCT_STRUCTURE_FK
			   AND   DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
			                          FROM EPACUBE.EPACUBE_PARAMS EEP
			                          WHERE EEP.APPLICATION_SCOPE_FK = 100
			                          AND   EEP.NAME = 'PRODUCT' )  ) -- PRIMARY IDENTIFIER
			,NULL
			,NULL
			,@l_sysdate
			FROM #TS_EVENT_DATA TSED	
			INNER JOIN epacube.PRODUCT_IDENT_MULT VCPD WITH (NOLOCK)
			  ON ( VCPD.DATA_NAME_FK = TSED.DATA_NAME_FK 
			  AND  VCPD.PRODUCT_STRUCTURE_FK <> TSED.PRODUCT_STRUCTURE_FK ---- KS ADDED THIS TO INGORE NO CHANGE EVENTS 
			  AND  VCPD.ORG_ENTITY_STRUCTURE_FK = TSED.ORG_ENTITY_STRUCTURE_FK
			  AND  ISNULL ( VCPD.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) 
			  AND  VCPD.VALUE = TSED.NEW_DATA
			  AND TSED.NEW_DATA <> '-999999999' )
			WHERE TSED.TABLE_NAME IN ( 'PRODUCT_IDENT_MULT' ) )



------------------------------------------------------------------------------------------
--  Create Temp Table for TS BATCH DUPS  Include Batch and other Events 
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_BATCH_DUPS') is not null
	   drop table #TS_BATCH_DUPS
	   
	   
	-- create temp table
	CREATE TABLE #TS_BATCH_DUPS(
	    DATA_NAME_FK             INT NULL,
	    ORG_ENTITY_STRUCTURE_FK  BIGINT NULL,
	    ENTITY_STRUCTURE_FK      BIGINT NULL,	    
		NEW_DATA		         varchar(128)  NULL
		 )


    INSERT INTO #TS_BATCH_DUPS
     (  DATA_NAME_FK
       ,ORG_ENTITY_STRUCTURE_FK
       ,ENTITY_STRUCTURE_FK
       ,NEW_DATA )
    SELECT DISTINCT 
               TSED.DATA_NAME_FK 
              ,ISNULL ( TSED.ORG_ENTITY_STRUCTURE_FK, 1 )
              ,ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 )
              ,TSED.NEW_DATA
    FROM #TS_EVENT_DATA TSED
    WHERE TSED.TABLE_NAME IN ( 'PRODUCT_IDENTIFICATION', 'PRODUCT_IDENT_MULT' )
    AND TSED.NEW_DATA <> '-999999999'
    GROUP BY TSED.DATA_NAME_FK, TSED.ORG_ENTITY_STRUCTURE_FK, TSED.ENTITY_STRUCTURE_FK, TSED.NEW_DATA
    HAVING COUNT(*) > 1


--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------


	CREATE NONCLUSTERED INDEX IDX_TSBD_DNF_OESF_ESF_IND ON #TS_BATCH_DUPS
	(   DATA_NAME_FK	ASC
       ,ORG_ENTITY_STRUCTURE_FK ASC
       ,ENTITY_STRUCTURE_FK     ASC )
	INCLUDE ( NEW_DATA )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


			INSERT INTO #TS_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				DATA_NAME_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		(	SELECT 
			 TSED.EVENT_FK
			,NULL
			,99
			,TSED.DATA_NAME_FK
			,'UNIQUE IDENTIFIER EXISTS FOR TWO DIFFERENT PRODUCTS'  --ERROR NAME         
			,'SEE PRODUCT_ID '--ERROR1
			,( SELECT VALUE FROM EPACUBE.PRODUCT_IDENTIFICATION  WITH (NOLOCK)
			   WHERE PRODUCT_STRUCTURE_FK = TSED.PRODUCT_STRUCTURE_FK
			   AND   DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
			                          FROM EPACUBE.EPACUBE_PARAMS EEP  WITH (NOLOCK)
			                          WHERE EEP.APPLICATION_SCOPE_FK = 100
			                          AND   EEP.NAME = 'PRODUCT' )  ) -- PRIMARY IDENTIFIER
			,'DUPLICATED PRODUCT_ID'
			,( SELECT TOP 1 PI.VALUE
			   FROM SYNCHRONIZER.EVENT_DATA TSEDX
			   INNER JOIN  EPACUBE.PRODUCT_IDENTIFICATION PI  WITH (NOLOCK) ON
			      (   PI.PRODUCT_STRUCTURE_FK = TSEDX.PRODUCT_STRUCTURE_FK
			      AND PI. DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
			                          FROM EPACUBE.EPACUBE_PARAMS EEP  WITH (NOLOCK)
			                          WHERE EEP.APPLICATION_SCOPE_FK = 100
			                          AND   EEP.NAME = 'PRODUCT' )  ) -- PRIMARY IDENTIFIER
			   WHERE TSEDX.DATA_NAME_FK = TSED.DATA_NAME_FK
			   AND   ISNULL ( TSEDX.ORG_ENTITY_STRUCTURE_FK, 1) = ISNULL ( TSED.ORG_ENTITY_STRUCTURE_FK, 1 )
			   AND   ISNULL ( TSEDX.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) 			   
			   AND   TSEDX.NEW_DATA = TSED.NEW_DATA 
			   AND   TSEDX.PRODUCT_STRUCTURE_FK <> TSED.PRODUCT_STRUCTURE_FK )
			,NULL
			,NULL
			,@l_sysdate
			FROM #TS_EVENT_DATA TSED	
			INNER JOIN #TS_BATCH_DUPS TSBD
			  ON ( TSBD.DATA_NAME_FK = TSED.DATA_NAME_FK 
			  AND  TSBD.ORG_ENTITY_STRUCTURE_FK = ISNULL ( TSED.ORG_ENTITY_STRUCTURE_FK, 1 )
			  AND  ISNULL ( TSBD.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) 
			  AND  TSBD.NEW_DATA = TSED.NEW_DATA )
			WHERE TSED.TABLE_NAME IN ( 'PRODUCT_IDENTIFICATION', 'PRODUCT_IDENT_MULT' ) )


--drop temp table if it exists
	IF object_id('tempdb..#TS_BATCH_DUPS') is not null
	   drop table #TS_BATCH_DUPS






------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_IDENTS'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_IDENTS has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END




































