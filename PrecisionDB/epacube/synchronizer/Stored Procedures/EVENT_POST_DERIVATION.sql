﻿








-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing of derivations and defaults
--				Carried over V3 String Functions
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010   Initial SQL Version
-- CV        09/09/2010	  tweaking the copy to child section
-- CV        08/26/2011   Added Rules Hierarchy to innerjoin for calc seq
-- KS        10/13/2012   SUP-586 Reworked the Logic to Qualify the Products to tighten down
--                         the number of rows in the #TS_RULE_DERIVE_TRIGGER_EVENTS table
--                         Example at FG 1,176,540 rows inserted instead of expected 23,527
--                         Risk is logic is tightened down significantly so hopefully will 
--                         will not exclude wanted derivations so must test thoroughly.
--                         Addl this code has been moved to synchronizer.EVENT_POST_QUALIFY_RULES_DERIVE
--                         to free up valueable memory used by these temp tables.
--                         See Commented Code.... will remove later
-- CV       12/28/2012    Add call to Custom Product ID Jira.  
-- CV       01/20/2014    Adding UI logging
-- CV       02/17/2017    New Control Table

CREATE PROCEDURE [synchronizer].[EVENT_POST_DERIVATION] 
        ( @in_batch_no bigint )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT
DECLARE  @ls_exec             nvarchar(4000)

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime
     
     DECLARE @V_START_TIMESTAMP  DATETIME
     DECLARE @V_END_TIMESTAMP    DATETIME
  
DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int
DECLARE @in_job_fk   int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_DERIVATION.'
                     + ' Batch No = '
					 + cast(isnull(@in_batch_no, 0) as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;



---------  UNCOMMENT TO BREAK CODE FOR TROUBLESHOOTING  		
----   SET @ls_exec = 'BREAK CODE'									
----         exec sp_executesql 	@ls_exec;
		

/*

----   START OF CODE REWORK.... COMMENTED OUT THIS CODE
	
*/

------------------------------------------------------------------------------------------
---  LOOP THROUGH RESULTS BY CALC GROUP SEQ
------------------------------------------------------------------------------------------

      DECLARE @vm$calc_group_seq			 INT
             ,@vm$result_data_name_fk        INT
			
			----DECLARE  cur_v_result_group cursor local for
			----SELECT DISTINCT ISNULL ( RH.calc_group_seq, 999999999 ) AS calc_group_seq	
			----               ,r.result_data_name_fk 
			----FROM synchronizer.RULES R WITH (NOLOCK)
			----INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
			----	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID  
			----	AND  RS.RULE_TYPE_CR_FK IN ( 301, 302, 303 ) 
			----	AND  RS.RECORD_STATUS_CR_FK = 1 )  -- DEFAULT OR DERIVATION 
			----INNER JOIN Synchronizer.rules_hierarchy RH WITH (NOLOCK) 
			----    ON RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
			----    AND RH.RECORD_STATUS_CR_FK = 1 
			----WHERE R.RECORD_STATUS_CR_FK = 1
			----ORDER BY  calc_group_seq ASC


				DECLARE  cur_v_result_group cursor local for
			SELECT DISTINCT ISNULL ( C.CONTROL_PRECEDENCE, 999999999 ) AS calc_group_seq	
			               ,r.result_data_name_fk 
			FROM synchronizer.RULES R WITH (NOLOCK)
		   INNER JOIN epacube.controls C WITH (NOLOCK) 
			    ON c.Controls_id = R.CONTROLS_FK
			    AND C.RECORD_STATUS_CR_FK = 1 
			WHERE R.RULE_TYPE_CR_FK IN ( 301, 302, 303 ) 
				AND  R.RECORD_STATUS_CR_FK = 1   -- DEFAULT OR DERIVATION
			ORDER BY  calc_group_seq ASC


                    
    OPEN cur_v_result_group;
    FETCH NEXT FROM cur_v_result_group INTO  @vm$calc_group_seq	
											,@vm$result_data_name_fk	


------------------------------------------------------------------------------------------
---  LOOP THROUGH EVENTS BY RESULT DATA NAME CALC SEQ
------------------------------------------------------------------------------------------


     WHILE @@FETCH_STATUS = 0 
     BEGIN

	SET @status_desc = 'DERIVATIONS Processed for Calc Seq:: '
	     + CAST ( ISNULL ( @vm$calc_group_seq, 999999999 ) AS VARCHAR(16) )
         + ' Batch No = '
		 + cast(isnull(@in_batch_no, 0) as varchar(30))	     
	     + ' Result:: '
	     + CAST ( ISNULL (  @vm$result_data_name_fk, 999999999 ) AS VARCHAR(16) )	     
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc

                
  
/*

-----    COMMENTED CODE CONTINUES....




*/     ---- END OF COMMENTED CODE


------------------------------------------------------------------------------------------
--  PERFORM QUALIFICATION OF DERIVATION RULES.. CREATE DERIVATION EVENTS
------------------------------------------------------------------------------------------

	EXEC SYNCHRONIZER.EVENT_POST_QUALIFY_RULES_DERIVE  @in_batch_no, @vm$result_data_name_fk	


------------------------------------------------------------------------------------------
--  CAN TRUNCATE TEMP TABLE BECAUSE EVENTS ARE ALREADY CREATED.. SAVE ON MEMORY
------------------------------------------------------------------------------------------
                
-----     TRUNCATE TABLE #TS_RULE_DERIVE_TRIGGER_EVENTS



------------------------------------------------------------------------------------------
--   QUALIFY RULES ( DEFAULTS and DERIVATIONS ) 
--	    Upon return of this procedure 
--		Event_Data Source = 72 ( Derived Data ) Event Status = 88 
--		Event_Data_Rules only once with Result Rank = 1
------------------------------------------------------------------------------------------

SET @l_rows_processed = 
               (	SELECT COUNT(1)
					FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
					INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
						ON ( EDR.EVENT_FK = ED.EVENT_ID 
						AND  EDR.RESULT_RANK = 1  )
					INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
						ON ( EDR.RULES_FK = R.RULES_ID )
					INNER JOIN epacube.controls C WITH (NOLOCK) 
			    ON c.Controls_id = R.CONTROLS_FK
			    AND C.RECORD_STATUS_CR_FK = 1 
					WHERE 1 = 1
					AND   ED.BATCH_NO = @IN_BATCH_NO
					AND   ED.EVENT_SOURCE_CR_FK = 72 
					AND   R.RULE_TYPE_CR_FK IN ( 301, 302, 303 )	
					-----------
					AND  ISNULL ( C.CONTROL_PRECEDENCE, 999999999 ) = @vm$calc_group_seq
					AND  R.RESULT_DATA_NAME_FK	 = @vm$result_data_name_fk						
                )
	   



	    --------    (	SELECT COUNT(1)
					--------FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
					--------INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
					--------	ON ( EDR.EVENT_FK = ED.EVENT_ID 
					--------	AND  EDR.RESULT_RANK = 1  )
					--------INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
					--------	ON ( EDR.RULES_FK = R.RULES_ID )
					--------INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
					--------  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )						
					--------INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
					--------	ON RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
					--------	AND RH.RECORD_STATUS_CR_FK = 1 
					--------WHERE 1 = 1
					--------AND   ED.BATCH_NO = @IN_BATCH_NO
					--------AND   ED.EVENT_SOURCE_CR_FK = 72 
					--------AND   RH.RULE_TYPE_CR_FK IN ( 301, 302, 303 )	
					-------------------
					--------AND  ISNULL ( RH.CALC_GROUP_SEQ, 999999999 ) = @vm$calc_group_seq
					--------AND  R.RESULT_DATA_NAME_FK	 = @vm$result_data_name_fk						
     --------           )



IF @l_rows_processed = 0
BEGIN
	SET @status_desc = 'No Defaults or Derivations to be Processed for Calc Seq:: '
	     + CAST ( ISNULL ( @vm$calc_group_seq, 999999999 ) AS VARCHAR(16) )
         + ' Batch No = '
		 + cast(isnull(@in_batch_no, 0) as varchar(30))	     
	     + ' Result:: '
	     + CAST ( ISNULL (  @vm$result_data_name_fk, 999999999 ) AS VARCHAR(16) )	     
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
END
ELSE
BEGIN


------------------------------------------------------------------------------------------
--  PERFORM DEFAULT ID SEQ RULES FIRST EVEN THOUGH DEFAULTS USUALLY RUN AFTER DERIVATIONS
------------------------------------------------------------------------------------------

SET @V_COUNT = (	SELECT COUNT(1)
					FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
					INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
						ON ( EDR.EVENT_FK = ED.EVENT_ID 
						AND  EDR.RESULT_RANK = 1  )
					INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
						ON ( EDR.RULES_FK = R.RULES_ID )
						INNER JOIN epacube.controls C WITH (NOLOCK) 
			    ON c.Controls_id = R.CONTROLS_FK
			    AND C.RECORD_STATUS_CR_FK = 1 
					INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
					  ON ( RA.RULES_FK = R.RULES_ID )
					WHERE 1 = 1
					AND   ED.BATCH_NO = @IN_BATCH_NO
					AND   ED.EVENT_SOURCE_CR_FK = 72 
					AND   ED.NEW_DATA IS NULL 
					AND   R.RULE_TYPE_CR_FK = 301	 -- DEFAULT		
					AND   RA.RULES_ACTION_OPERATION1_FK = 350  -- ACTION ID SEQUENCE
					-----------
					AND  ISNULL ( C.CONTROL_PRECEDENCE, 999999999 ) = @vm$calc_group_seq
					AND  R.RESULT_DATA_NAME_FK	 = @vm$result_data_name_fk						
                )



				 ----(	SELECT COUNT(1)
					----FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
					----INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
					----	ON ( EDR.EVENT_FK = ED.EVENT_ID 
					----	AND  EDR.RESULT_RANK = 1  )
					----INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
					----	ON ( EDR.RULES_FK = R.RULES_ID )
					----INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
					----  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )						
					----INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
					----	ON RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
					----	AND RH.RECORD_STATUS_CR_FK = 1 
					----INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
					----  ON ( RA.RULES_FK = R.RULES_ID )
					----WHERE 1 = 1
					----AND   ED.BATCH_NO = @IN_BATCH_NO
					----AND   ED.EVENT_SOURCE_CR_FK = 72 
					----AND   ED.NEW_DATA IS NULL 
					----AND   RS.RULE_TYPE_CR_FK = 301	 -- DEFAULT		
					----AND   RA.RULES_ACTION_OPERATION1_FK = 350  -- ACTION ID SEQUENCE
					---------------
					----AND  ISNULL ( RH.CALC_GROUP_SEQ, 999999999 ) = @vm$calc_group_seq
					----AND  R.RESULT_DATA_NAME_FK	 = @vm$result_data_name_fk						
     ----           )
		
IF @V_COUNT > 0	 
BEGIN  
 EXEC  synchronizer.EVENT_POST_DERIVE_PRODUCT_ID @in_batch_no, @vm$calc_group_seq, @vm$result_data_name_fk
 END


------------------------------------------------------------------------------------------
-- CUSTOM  ADDD NOTes
------------------------------------------------------------------------------------------

SET @V_COUNT = (	SELECT COUNT(1)
					FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
					INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
						ON ( EDR.EVENT_FK = ED.EVENT_ID 
						AND  EDR.RESULT_RANK = 1  )
					INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
						ON ( EDR.RULES_FK = R.RULES_ID )
					INNER JOIN epacube.controls C WITH (NOLOCK) 
			    ON c.Controls_id = R.CONTROLS_FK
			    AND C.RECORD_STATUS_CR_FK = 1 
					INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
					  ON ( RA.RULES_FK = R.RULES_ID )
					WHERE 1 = 1
					AND   ED.BATCH_NO = @IN_BATCH_NO
					AND   ED.EVENT_SOURCE_CR_FK = 72 
					AND   ED.NEW_DATA IS NULL 
					AND   R.RULE_TYPE_CR_FK = 301	 -- DEFAULT		
					AND   RA.RULES_ACTION_OPERATION1_FK = 399  -- CUSTOM ACTION ID SEQUENCE
					-----------
					AND  ISNULL ( C.CONTROL_PRECEDENCE, 999999999 ) = @vm$calc_group_seq
					AND  R.RESULT_DATA_NAME_FK	 = @vm$result_data_name_fk						
                )
		
IF @V_COUNT > 0	 
BEGIN  
 EXEC  synchronizer.EVENT_POST_DERIVE_PRODUCT_ID_CUSTOM @in_batch_no, @vm$calc_group_seq, @vm$result_data_name_fk
 END



------------------------------------------------------------------------------------------
--  NEXT PERFORM CONCATENATION DERIVATIONS
------------------------------------------------------------------------------------------

SET @V_COUNT = (	SELECT COUNT(1)
					FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
					INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
						ON ( EDR.EVENT_FK = ED.EVENT_ID 
						AND  EDR.RESULT_RANK = 1  )
					INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
						ON ( EDR.RULES_FK = R.RULES_ID )
					INNER JOIN epacube.controls C WITH (NOLOCK) 
			    ON c.Controls_id = R.CONTROLS_FK
			    AND C.RECORD_STATUS_CR_FK = 1 
					INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
					  ON ( RA.RULES_FK = R.RULES_ID )
					WHERE 1 = 1
					AND   R.RULE_TYPE_CR_FK IN ( 301, 302 )	 --DERIVATION or DEFAULT
					AND   RA.RULES_ACTION_OPERATION1_FK IN ( 300 ) --- CONCATENATION					
					AND   ED.BATCH_NO = @IN_BATCH_NO	
					-----------
					AND  ISNULL ( C.CONTROL_PRECEDENCE, 999999999 ) = @vm$calc_group_seq	
					AND  R.RESULT_DATA_NAME_FK = @vm$result_data_name_fk														
		        )
		        
		
IF @V_COUNT > 0	 
BEGIN  
                
		 EXEC  synchronizer.EVENT_POST_DERIVE_NEW_DATA_CONCATENATE @in_batch_no, @vm$calc_group_seq, @vm$result_data_name_fk
END



------------------------------------------------------------------------------------------
--  NEXT PERFORM CONCATENATION DERIVATIONS
------------------------------------------------------------------------------------------

SET @V_COUNT = (	SELECT COUNT(1)
					FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
					INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
						ON ( EDR.EVENT_FK = ED.EVENT_ID 
						AND  EDR.RESULT_RANK = 1  )
					INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
						ON ( EDR.RULES_FK = R.RULES_ID )
					INNER JOIN epacube.controls C WITH (NOLOCK) 
			    ON c.Controls_id = R.CONTROLS_FK
			    AND C.RECORD_STATUS_CR_FK = 1 
					INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
					  ON ( RA.RULES_FK = R.RULES_ID )
					WHERE 1 = 1
					AND   R.RULE_TYPE_CR_FK IN ( 301, 302 )  -- DERIVATION or DEFAULT
					AND   RA.RULES_ACTION_OPERATION1_FK IN ( 310 ) --- SUBSTRING				
					AND   ED.BATCH_NO = @IN_BATCH_NO	
					-----------
					AND  ISNULL ( C.CONTROL_PRECEDENCE, 999999999 ) = @vm$calc_group_seq	
					AND R.RESULT_DATA_NAME_FK = @vm$result_data_name_fk														
		        )
		        
		
IF @V_COUNT > 0	 
BEGIN  
                
		 EXEC  synchronizer.EVENT_POST_DERIVE_NEW_DATA_SUBSTRING @in_batch_no, @vm$calc_group_seq, @vm$result_data_name_fk
END



------------------------------------------------------------------------------------------
---  NO ACTION OPERATION
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_DATA 
SET EVENT_ACTION_CR_FK = 58  -- NO ACTION
   ,EVENT_STATUS_CR_FK = 83  -- PREPOST
WHERE EVENT_ID IN ( 
       SELECT ED.EVENT_ID
       FROM synchronizer.EVENT_DATA  ED WITH (NOLOCK)
		INNER JOIN synchronizer.EVENT_DATA_RULES EDR  WITH (NOLOCK) 
		  ON ( EDR.EVENT_FK = ED.EVENT_ID
		  AND  EDR.RESULT_RANK = 1  )      --- ONLY THE SELECTED RULE
		INNER JOIN synchronizer.RULES R  WITH (NOLOCK) 
		  ON ( EDR.RULES_FK = R.RULES_ID )
        INNER JOIN EPACUBE.DATA_NAME DN ON DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK		  
		INNER JOIN epacube.controls C WITH (NOLOCK) 
			    ON c.Controls_id = R.CONTROLS_FK
			    AND C.RECORD_STATUS_CR_FK = 1 
		INNER JOIN synchronizer.RULES_ACTION RA  WITH (NOLOCK) 
		  ON ( RA.RULES_FK = R.RULES_ID )
		INNER JOIN synchronizer.RULES_ACTION_OPERATION RAOP  WITH (NOLOCK) 
		  ON ( RA.RULES_ACTION_OPERATION1_FK = RAOP.RULES_ACTION_OPERATION_ID )		  
		WHERE 1 = 1
		AND   R.RULE_TYPE_CR_FK IN ( 301, 302 )    
		AND   RAOP.NAME = 'NO ACTION' 
		AND   ED.BATCH_NO = @IN_BATCH_NO
		AND   ED.EVENT_SOURCE_CR_FK = 72  -- DERIVATION EVENT
        AND  ISNULL ( c.CONTROL_PRECEDENCE, 999999999 ) = @vm$calc_group_seq	
        AND  R.RESULT_DATA_NAME_FK = @vm$result_data_name_fk	
       )



------------------------------------------------------------------------------------------
--  IF RULE IS TO BE EXPANDED TO ALL WAREHOUSES ( APPLY_TO_ORG_CHILDREN_IND = 1 ) 
--		THEN SET EVENT SOURCE ( CODE_REF_ID = 73 ) TO COPY
--    epa-2924
------------------------------------------------------------------------------------------

INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_CLASS_CR_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[NEW_DATA]
           --,[CURRENT_DATA]
           ,ENTITY_DATA_VALUE_FK
           ,DATA_VALUE_FK
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[EVENT_REASON_CR_FK]
           ,[RESULT_TYPE_CR_FK]
           ,[JOB_CLASS_FK]
           ,[CALC_JOB_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[IMPORT_BATCH_NO]
           ,[BATCH_NO]
           ,[RULES_FK]
           ,[RELATED_EVENT_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]  )
SELECT
            ED.EVENT_TYPE_CR_FK
           ,ED.EVENT_ENTITY_CLASS_CR_FK
           ,ED.EVENT_EFFECTIVE_DATE
           ,ED.EPACUBE_ID
           ,ED.DATA_NAME_FK
           ,ED.PRODUCT_STRUCTURE_FK
           ,PAS.ORG_ENTITY_STRUCTURE_FK  --- PRODUCT/WHSE ASSOC
           ,ED.ENTITY_CLASS_CR_FK
           ,ED.ENTITY_DATA_NAME_FK
           ,ED.ENTITY_STRUCTURE_FK
           ,ED.NEW_DATA
           --,ED.CURRENT_DATA
           ,ED.ENTITY_DATA_VALUE_FK
           ,ED.DATA_VALUE_FK
           ,ED.EVENT_CONDITION_CR_FK
           ,80                          --- EVENT_STATUS_CR_FK
           ,ED.EVENT_PRIORITY_CR_FK
           ,73                           --- COPY IS THE EVENT SOURCE
           ,58 --ED.EVENT_ACTION_CR_FK   --- COPY
           ,ED.EVENT_REASON_CR_FK
           ,ED.RESULT_TYPE_CR_FK
           ,ED.JOB_CLASS_FK
           ,ED.CALC_JOB_FK
           ,ED.IMPORT_JOB_FK
           ,ED.IMPORT_DATE
           ,ED.IMPORT_PACKAGE_FK
           ,ED.IMPORT_FILENAME
           ,ED.IMPORT_BATCH_NO
           ,@IN_BATCH_NO
           ,ED.RULES_FK
           ,ED.EVENT_ID                  --- RELATED EVENT IS ORIGINAL EVENT COPIED FROM
           ,ED.RECORD_STATUS_CR_FK
           ,ED.UPDATE_TIMESTAMP
           ,ED.UPDATE_USER
-------------
	FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
	INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
		ON ( EDR.EVENT_FK = ED.EVENT_ID
		AND  EDR.RESULT_RANK = 1  )
	INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
		ON ( EDR.RULES_FK = R.RULES_ID )
    INNER JOIN epacube.controls C WITH (NOLOCK) 
			    ON c.Controls_id = R.CONTROLS_FK
			    AND C.RECORD_STATUS_CR_FK = 1 
	INNER JOIN epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK) 
	  ON  ( PAS.DATA_NAME_FK = 159100  --- PRODUCT WHSE
	  AND   PAS.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK )
	WHERE 1 = 1
	AND   ED.BATCH_NO = @IN_BATCH_NO
	AND   ED.NEW_DATA IS NOT NULL 	
------------	AND   ED.EVENT_SOURCE_CR_FK = 72  just removed this to see if it will work for GLOBAL COPY
	AND   R.APPLY_TO_ORG_CHILDREN_IND = 1  -- APPLY DERIVED VALUE TO ALL WAREHOUSES    
-----------
    AND  (    
			 (    R.RULE_TYPE_CR_FK IN ( 301, 302 )    ----  DEFAULT OR DERIVATION 
			 AND  ED.ORG_ENTITY_STRUCTURE_FK = 1 )
          OR
			 (    R.RULE_TYPE_CR_FK = 303    ----  GLOBAL COPY
			AND  (  ED.ORG_ENTITY_STRUCTURE_FK = 1    
				  OR ED.DATA_NAME_FK = 159100 )  )  --- ONLY APPLY RULE TO CORP EVENTS OR WHSE ASSOC
	      )
	--------
    AND  ISNULL ( c.CONTROL_PRECEDENCE, 999999999 ) = @vm$calc_group_seq	
    AND  R.RESULT_DATA_NAME_FK = @vm$result_data_name_fk	
	
	

	
	
END  --- DERIVATION PROCESSING




------------------------------------------------------------------------------------------
---  GET NEXT Warehouse and Result Grouping 
------------------------------------------------------------------------------------------

    FETCH NEXT FROM cur_v_result_group INTO  @vm$calc_group_seq	
											,@vm$result_data_name_fk

     END --cur_v_result_group  LOOP
     CLOSE cur_v_result_group
	 DEALLOCATE cur_v_result_group; 
     



--------drop temp table if it exists
------	IF object_id('tempdb..#TS_RULE_DERIVE_TRIGGER_EVENTS') is not null
------	   drop table #TS_RULE_DERIVE_TRIGGER_EVENTS;
	   



------------------------------------------------------------------------------------------
--   DERIVATIONS NO ACTION ... DELETE FROM EVENT DATA TABLES
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_DATA
SET NEW_DATA = 'NO ACTION *** DELETE EVENT ***'
WHERE EVENT_ID IN (
		SELECT ED.EVENT_ID
		FROM synchronizer.EVENT_DATA ED WITH (NOLOCK)		
		INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK)		
		 ON ( ED.EVENT_ID = EDR.EVENT_FK
		 AND  EDR.RESULT_RANK = 1 )
		INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
		 ON ( RA.RULES_FK = EDR.RULES_FK
		 AND  RA.RULES_ACTION_OPERATION1_FK = 1000 )   --- NO ACTION
		WHERE 1 = 1
		AND   ED.BATCH_NO = @in_batch_no
		AND   ED.EVENT_TYPE_CR_FK = 150		
		AND   ED.EVENT_SOURCE_CR_FK = 72   --- DERIVATION 
		)


DELETE
FROM synchronizer.EVENT_DATA_ERRORS
WHERE EVENT_FK IN ( 
		SELECT ED.EVENT_ID
		FROM synchronizer.EVENT_DATA ED WITH (NOLOCK)		
		INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK)		
		 ON ( ED.EVENT_ID = EDR.EVENT_FK
		 AND  EDR.RESULT_RANK = 1 )
		INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
		 ON ( RA.RULES_FK = EDR.RULES_FK
		 AND  RA.RULES_ACTION_OPERATION1_FK = 1000 )   --- NO ACTION
		WHERE 1 = 1
		AND   ED.BATCH_NO = @in_batch_no
		AND   ED.EVENT_TYPE_CR_FK = 150		
		AND   ED.EVENT_SOURCE_CR_FK = 72   --- DERIVATION 
		)


DELETE
FROM synchronizer.EVENT_DATA_RULES
WHERE EVENT_FK IN ( 
		SELECT ED.EVENT_ID
		FROM synchronizer.EVENT_DATA ED WITH (NOLOCK)		
		INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK)		
		 ON ( ED.EVENT_ID = EDR.EVENT_FK
		 AND  EDR.RESULT_RANK = 1 )
		INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
		 ON ( RA.RULES_FK = EDR.RULES_FK
		 AND  RA.RULES_ACTION_OPERATION1_FK = 1000 )   --- NO ACTION
		WHERE 1 = 1
		AND   ED.BATCH_NO = @in_batch_no
		AND   ED.EVENT_TYPE_CR_FK = 150		
		AND   ED.EVENT_SOURCE_CR_FK = 72   --- DERIVATION 
		)

DELETE
FROM synchronizer.EVENT_DATA
		WHERE 1 = 1
		AND   BATCH_NO = @in_batch_no
		AND   EVENT_TYPE_CR_FK = 150		
		AND   EVENT_SOURCE_CR_FK = 72   --- DERIVATION 
		AND   NEW_DATA = 'NO ACTION *** DELETE EVENT ***'

--------------------------------------------------------------------


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_DERIVATION'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_DERIVATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END

















