﻿











-- Copyright 2010
--
-- Procedure created by CVoutour
--
--
-- Purpose: To create the event Data for Entity ident mult type events 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        09/10/2013   Initial version
-- CV        10/24/2013   Updated table name



CREATE PROCEDURE [synchronizer].[EVENT_FACTORY_ENTITY_IDENT_MULT] (
		  @V_in_event_fk BIGINT,          @in_post_ind SMALLINT, 
          @in_batch_no BIGINT,          @in_import_job_fk BIGINT, 
          @in_event_priority_cr_fk INT, @in_event_type_cr_fk INT,
          @in_table_name VARCHAR(64),   @in_data_name_fk BIGINT  )

AS
BEGIN




     DECLARE @ls_exec            nvarchar (max)
     DECLARE @l_exec_no          bigint
     DECLARE @l_rows_processed   bigint
     DECLARE @l_sysdate          datetime

	DECLARE @l_stmt_no           bigint
    DECLARE @ls_stmt             varchar(1000)
	DECLARE @status_desc		 varchar (max)
	DECLARE @ErrorMessage		 nvarchar(4000)
	DECLARE @ErrorSeverity		 int
	DECLARE @ErrorState			 int

    DECLARE @v_prod_seq_ind      smallint
    DECLARE  @v_nopost_history_ind  smallint;

   SET NOCOUNT ON;

   BEGIN TRY

   INSERT INTO COMMON.T_SQL values( getdate(),  'Start -  Event_ID:  ' +  cast(ISNULL(@V_in_event_fk,0) as varchar(100)) + ' -- Batch No: ' + cast( ISNULL(@in_batch_no,0) as varchar(100)))

      SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_FACTORY_ENTITY_IDENT_MULT. '
				 + ', Batch NO: ' + cast(isnull(@IN_BATCH_NO,0) as varchar(16))

      EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;

      SET @l_sysdate   = getdate()



      SET @status_desc =  'synchronizer.EVENT_FACTORY_NEW_ENTITY_IDENT_MULT: inserting - '
                        + 'NEW EVENTS'

      EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


------------------------------------------------------------------------------------------
--   COMPARE THE EVENT MULT TYPE NEW DATA TO THE CURRENT PRODUCT DATA CURRENT VALUE  
------------------------------------------------------------------------------------------
--select event_id,BATCH_NO,EVENT_PRIORITY_CR_FK, EVENT_TYPE_CR_FK, IMPORT_JOB_FK, DATA_NAME_FK,
--'Product_mult_type'  from synchronizer.event_data where PRODUCT_STRUCTURE_FK = 1004


--------STOP HERE TO SEE IF WE HAVE DUPLICATE EVENTS AT THIS TIME

--		SET @ls_exec = 'STOP CODE HERE'            
--            INSERT INTO COMMON.T_SQL
--                ( TS,  SQL_TEXT )
--            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
--            exec sp_executesql 	@ls_exec;

--			  exec sp_executesql 	@ls_exec;




DECLARE @NEWDATA	VARCHAR(1000)
		--SET 		   @NEWDATA = 'AME,ABC,123'
DECLARE @OLDDATA	VARCHAR(1000)
		--SET 		   @OLDDATA = 'AME,ITA'
DECLARE @ENTITY_IDENT_MULT_ID_List	VARCHAR(1000)
;WITH CurrentDataFromEvent as 
		(
			select 
		EIM.ENTITY_IDENT_MULT_ID,
		EIM.ENTITY_STRUCTURE_FK,
		EIM.DATA_NAME_FK,
		EIM.VALUE AS CURRENT_DATA
				from epacube.ENTITY_IDENT_MULT EIM
				inner join epacube.data_name dn
				on (dn.DATA_NAME_ID = eim.DATA_NAME_FK)
				INNER JOIN SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
					  ON ( eim.ENTITY_STRUCTURE_FK = ED.EPACUBE_ID
					  and eim.DATA_NAME_FK = ed.DATA_NAME_FK )  ---------added this
		WHERE
			ED.EVENT_ID = ISNULL (@V_in_event_fk, -999) 
			--AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
			--AND  EVENT_STATUS_CR_FK = 88	
			AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )     
			) 
		select distinct 
		@OLDDATA = stuff((select ', ' + CURRENT_DATA as [text()]
        from CurrentDataFromEvent xt
        where xt.ENTITY_STRUCTURE_FK= pd.ENTITY_STRUCTURE_FK
        for xml path('')), 1, 2, '') 
		,@ENTITY_IDENT_MULT_ID_List = stuff((select ', ' + cast(ENTITY_IDENT_MULT_ID as varchar(20)) as [text()]
        from CurrentDataFromEvent xt
        where xt.ENTITY_STRUCTURE_FK = pd.ENTITY_STRUCTURE_FK
        for xml path('')), 1, 2, '') 
from CurrentDataFromEvent pd
group by ENTITY_STRUCTURE_FK


 SELECT
			--ed.PRODUCT_STRUCTURE_FK,
			--ed.DATA_NAME_FK,
			 @NEWDATA = ed.NEW_DATA
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
	
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @V_in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@V_in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )

		--COMPARE TO
		
		--Goal: to determine which item for deletion and insertion

		--select * from dbo.entityActionTable

		IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'EntityActionTable'))
		BEGIN
			DROP TABLE dbo.EntityActionTable
		END

		SELECT 
			ROW_NUMBER() over (order by items) ID
			, t.*
			,0 processed
			INTO dbo.EntityActionTable
			from (
					--Compare each item of old data to new data
					select a.items, b.items ENTITY_IDENT_MULT_ID, @NEWDATA DataComparedAgainst,'Compare Current against New Data' Note, CASE WHEN CHARINDEX(ltrim(rtrim(a.items)), @NEWDATA) > 0 THEN 'PASS' ELSE 'DELETE' END [ACTION] from
						(
						(select   row_number() over(order by items) ID, aa.items
							from  dbo.Split(@OLDDATA,',') AS AA ) a inner join 
						(select   row_number() over(order by items) ID, bb.items
							from dbo.Split(@ENTITY_IDENT_MULT_ID_List,',') AS bb) b on a.id = b.ID
						)
					union all
					--Compare new data to old data
					select   aa.*, 0 ENTITY_IDENT_MULT_ID, @OLDDATA DataComparedAgainst,'Compare New against Current Data' Note, CASE WHEN CHARINDEX(ltrim(rtrim(aa.items)), @OLDDATA) > 0 THEN 'PASS' ELSE 'ADD' END [ACTION]
			from  dbo.Split(@NEWDATA ,',') AS AA
						) t
		WHERE
			t.Action <> 'PASS'


		--Michael's part
		Declare @Id int, @ENTITY_IDENT_MULT_ID BIGINT
		Declare @Action varchar(20), @Item varchar(1000)
		While (Select Count(*) From dbo.EntityActionTable WHERE processed = 0) > 0
		Begin
			Select Top 1 @Id = id, @Action = [Action], @ENTITY_IDENT_MULT_ID = ENTITY_IDENT_MULT_ID
			, @Item = Items From dbo.EntityActionTable WHERE processed = 0

			IF @Action = 'DELETE' BEGIN
				DELETE FROM epacube.ENTITY_IDENT_MULT where ENTITY_IDENT_MULT_ID =  @ENTITY_IDENT_MULT_ID
				
			END ELSE IF @Action = 'ADD' BEGIN 

				INSERT INTO [synchronizer].[EVENT_DATA]
						   ([EVENT_TYPE_CR_FK]
						   ,[EVENT_ENTITY_CLASS_CR_FK]
						   ,[EPACUBE_ID]
						   ,[DATA_NAME_FK]
						   ,[PRODUCT_STRUCTURE_FK]
						   ,[ORG_ENTITY_STRUCTURE_FK]
						   ,[ENTITY_CLASS_CR_FK]
						   ,[ENTITY_DATA_NAME_FK]
						   ,[ENTITY_STRUCTURE_FK]
						   ,[NEW_DATA]
						   ,[DATA_VALUE_FK]
						   ,[ENTITY_DATA_VALUE_FK]
						   ,[EVENT_CONDITION_CR_FK]
						   ,[EVENT_STATUS_CR_FK]
						   ,[EVENT_PRIORITY_CR_FK]
						   ,[EVENT_SOURCE_CR_FK]
						   ,[EVENT_ACTION_CR_FK]
						   ,[EVENT_REASON_CR_FK]
						   ,[BATCH_NO]
						   ,[UPDATE_USER])
				(SELECT 
						  ED.event_type_cr_fk
						  ,ED.Event_entity_class_cr_fk
						  ,ED.epacube_id
						  ,ED.data_name_fk
						  ,ED.product_structure_fk
						  ,ED.org_entity_structure_FK
						  ,ED.entity_class_cr_fk
						  ,ED.entity_data_name_Fk
						  ,ED.entity_structure_fk
						  ,LTRIM(RTRIM(@Item))  --NEW_DATA as one value
						   ,NULL  --[DATA_VALUE_FK]
						   ,NULL  ---[ENTITY_DATA_VALUE_FK]
						   ,ED.EVENT_CONDITION_CR_FK
						   ,ED.EVENT_STATUS_CR_FK
						   ,ED.EVENT_PRIORITY_CR_FK
						   ,ED.EVENT_SOURCE_CR_FK
						   ,51
						   ,ED.EVENT_REASON_CR_FK
						   ,ED.BATCH_NO
						   ,'UI CREATED' --[UPDATE_USER]
		  
						   --select ed.*
							FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
							INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
							 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
							INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
							 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
						WHERE 1 = 1 
						AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @V_in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@V_in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )
			AND ISNULL(CHARINDEX(',', NEW_DATA),0) > 0	)

						
----for testing
						
						
						--AND  EVENT_STATUS_CR_FK = 88		
						--AND (	
						--	   (    ISNULL (33188084, -999 ) = -999
						--		AND ISNULL ( 14486, -999 ) = ISNULL ( 14486, -999 )
						--		AND ISNULL ( NULL, -999 ) = ISNULL (NULL, -999 )
						--		AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (66, -999)
						--		AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( 'PRODUCT_MULT_TYPE', 'NULL DATA' )	)
						--	OR  ED.EVENT_ID = ISNULL (33188084, -999 ) 
						--	)
						--AND   (   1000000008 = -999           
						--	  OR  ED.data_name_fk =1000000008 ))

				
				
				END
			
				Update dbo.EntityActionTable Set Processed = 1 Where id = @Id 
				INSERT INTO COMMON.T_SQL values( getdate(),  'Action:  ' +  @Action + ' -- Value: ' + LTRIM(RTRIM(@Item)))

		End


		------------------

		---now need to delete original event...
		
		
		delete from synchronizer.event_data where event_id = @V_in_event_Fk


--STOP HERE TO SEE IF WE HAVE DUPLICATE EVENTS AT THIS TIME

		----SET @ls_exec = 'STOP CODE HERE'            
  ----          INSERT INTO COMMON.T_SQL
  ----              ( TS,  SQL_TEXT )
  ----          VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
  ----          exec sp_executesql 	@ls_exec;

		----	  exec sp_executesql 	@ls_exec;




	
----- HEAD BACK TO EVENT POSTING   


--Execute synchronizer.event_posting @in_batch_no

	

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_FACTORY_ENTITY_IDENT_MULT'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.SYNCHRONIZER.EVENT_FACTORY_ENTITY_IDENT_MULT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END








