﻿





-- Copyright 2017
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing from UI.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        03/20/2017   Initial SQL Version
 


CREATE PROCEDURE [synchronizer].[EVENT_POSTING_COMPONENT_PRODUCT_DATA] 
        ( @in_table_id BIGINT,
		  @in_product_structure_fk BIGINT,
		  @in_Product_Recipe_fk BIGINT,
          @in_data_name_fk BIGINT,
		  @in_data_level_cr_fk INT,
		  @in_batch_no bigint,
		  @in_New_Data Varchar(256),
		  @in_entity_data_value_fk BIGINT,
		  @in_pack bigint,
		  @UserID Varchar(64))


		
 

AS
BEGIN

 DECLARE @ls_exec            nvarchar (max)
 DECLARE @ls_exec2           nvarchar (max)
 DECLARE @ls_exec3           nvarchar (max) 
 DECLARE @ls_exec4           nvarchar (max) 
 DECLARE @l_exec_no          bigint
 DECLARE @l_rows_processed   bigint
 DECLARE @l_sysdate          datetime


DECLARE  @v_input_rows       bigint
DECLARE  @v_output_rows      bigint
DECLARE  @v_output_total     bigint
DECLARE  @v_exception_rows   bigint
DECLARE  @v_job_date         DATETIME
DECLARE  @v_count			 BIGINT
DECLARE  @v_entity_class_cr_fk  int
DECLARE  @v_import_package_fk   int;

DECLARE @l_stmt_no           bigint
DECLARE @ls_stmt             varchar(4000)
DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

Declare @in_Table_name varchar(128)
 

 DECLARE  
			@in_Vendor_entity_structure_fk BIGINT, 
			@in_cust_entity_structure_fk BIGINT,
			@in_zone_entity_structure_fk BIGINT,
			@in_dept_entity_structure_fk BIGINT,
			@in_assoc_entity_structure_fk BIGINT,
			@in_org_entity_structure_fk BIGINT,
			@in_entity_structure_fk BIGINT,
			--@in_batch_no  BIGINT,
			@in_event_type_cr_fk INT,
			@in_SETTINGS_POS_PROD_STORE_HEADER_ID varchar(128),
			@in_current_data varchar(256),
			@in_data_value_fk BIGINT,
			@in_Precedence INT 
			 

-----------------------------------------------------------------------------
			--FIND CURRENT DATA IN EACH TABLE
-----------------------------------------------------------------------------


set @in_table_name = (select TABLE_NAME from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk
and ISNULL( @in_data_level_cr_fk,0) = ISNULL(data_level_cr_fk,0)  )

if @in_table_name is NULL
BEGIN  

set @in_table_name = (select TABLE_NAME from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk)

END

-------------------------------------------------------------------------------


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of Synchronizer.EVENT_POSTING_COMPONENT_PRODUCT_DATA.'  
					 + ' Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))					 				 					 					 
					 + ' Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))							 
					 + ' Data Level: ' + cast(isnull(@in_data_level_cr_fk, 0 ) as varchar(30))							 					 


EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_job_id = NULL,
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;


Set @in_Precedence = (select isNULL(PRECEDENCE,NULL) from epacube.CODE_REF where CODE_REF_ID =   @in_data_level_cr_fk)
-------------------------------------------------------------------------------------------------------------------------------

Set @v_entity_class_cr_fk =  (select Entity_Class_CR_FK from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk
and ISNULL( @in_data_level_cr_fk,0) = ISNULL(data_level_cr_fk,0)  )

if @v_entity_class_cr_fk is NULL
BEGIN  

set @v_entity_class_cr_fk = (select Entity_Class_CR_FK from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk)

END

---------------------------------------------------------------------------------------------------------------------------------------------

SET @in_event_type_cr_fk =  (select event_type_cr_fk from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk
and ISNULL( @in_data_level_cr_fk,0) = ISNULL(data_level_cr_fk,0)  )

if @in_event_type_cr_fk is NULL
BEGIN  

set @in_event_type_cr_fk = (select EVENT_TYPE_CR_FK from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk)

END


 ----------------------------------------------------------------------------------------------
 -------GET TABLE NAME WHERE DATA LIVES FROM DATA SET --------------------------------
 ----------------------------------------------------------------------------------------------
  if @in_table_name = 'PRODUCT_GTIN'   BEGIN
--set @in_Vendor_entity_structure_fk = (select  ENTITY_STRUCTURE_FK from   epacube.PRODUCT_DESCRIPTION where PRODUCT_DESCRIPTION_ID = @in_table_id)
set @in_product_structure_fk =  (select Product_STRUCTURE_FK from epacube.PRODUCT_GTIN where PRODUCT_GTIN_ID = @in_table_id)
set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from epacube.PRODUCT_GTIN where PRODUCT_GTIN_ID = @in_table_id)
set @in_current_data  = (select UPC_CODE from epacube.PRODUCT_GTIN where PRODUCT_GTIN_ID = @in_table_id)
 END

 if @in_table_name = 'PRODUCT_DESCRIPTION'   BEGIN
set @in_Vendor_entity_structure_fk = (select  ENTITY_STRUCTURE_FK from   epacube.PRODUCT_DESCRIPTION where PRODUCT_DESCRIPTION_ID = @in_table_id)
set @in_product_structure_fk =  (select Product_STRUCTURE_FK from epacube.PRODUCT_DESCRIPTION where PRODUCT_DESCRIPTION_ID = @in_table_id)
set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from epacube.PRODUCT_DESCRIPTION where PRODUCT_DESCRIPTION_ID = @in_table_id)
set @in_current_data  = (select DESCRIPTION from epacube.PRODUCT_DESCRIPTION where PRODUCT_DESCRIPTION_ID = @in_table_id)
 END
 
 if @in_table_name = 'PRODUCT_ATTRIBUTE'   BEGIN
set @in_Vendor_entity_structure_fk = (select ENTITY_STRUCTURE_FK from epacube.PRODUCT_ATTRIBUTE where PRODUCT_ATTRIBUTE_ID = @in_table_id)
set @in_product_structure_fk =  (select Product_STRUCTURE_FK from epacube.PRODUCT_ATTRIBUTE where PRODUCT_ATTRIBUTE_ID = @in_table_id)
set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from epacube.PRODUCT_ATTRIBUTE where PRODUCT_ATTRIBUTE_ID = @in_table_id)
set @in_current_data  = (select attribute_event_data from epacube.PRODUCT_ATTRIBUTE where PRODUCT_ATTRIBUTE_ID = @in_table_id)
 
END
 
 if @in_table_name = 'PRODUCT_CATEGORY'   BEGIN
set @in_product_structure_fk =  (select Product_STRUCTURE_FK from epacube.PRODUCT_CATEGORY where PRODUCT_CATEGORY_ID = @in_table_id)
set @in_Vendor_entity_structure_fk = (select ENTITY_STRUCTURE_FK from epacube.PRODUCT_CATEGORY where PRODUCT_CATEGORY_ID = @in_table_id)
set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from epacube.PRODUCT_CATEGORY where PRODUCT_CATEGORY_ID = @in_table_id)
set @in_current_data  = (select dv.value  from epacube.PRODUCT_CATEGORY pc Inner Join Epacube.data_value dv on dv.DATA_VALUE_ID = pc.DATA_VALUE_FK
where pc.PRODUCT_CATEGORY_ID = @in_table_id)
 
END

 if @in_table_name = 'PRODUCT_TEXT'   BEGIN
set @in_product_structure_fk =  (select Product_STRUCTURE_FK from epacube.PRODUCT_TEXT where PRODUCT_TEXT_ID = @in_table_id)
set @in_Vendor_entity_structure_fk = (select ENTITY_STRUCTURE_FK from epacube.PRODUCT_TEXT where PRODUCT_TEXT_ID = @in_table_id)
set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from epacube.PRODUCT_TEXT where PRODUCT_TEXT_ID = @in_table_id)
set @in_current_data  = (select TEXT  from epacube.PRODUCT_TEXT where PRODUCT_TEXT_ID = @in_table_id)
 
END


if @in_table_name = 'PRODUCT_STATUS'   BEGIN
set @in_product_structure_fk =  (select Product_STRUCTURE_FK from epacube.PRODUCT_STATUS where PRODUCT_STATUS_ID = @in_table_id)
set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from epacube.PRODUCT_STATUS where PRODUCT_STATUS_ID = @in_table_id)
set @in_Vendor_entity_structure_fk = (select ENTITY_STRUCTURE_FK from epacube.PRODUCT_STATUS where PRODUCT_STATUS_ID = @in_table_id)
set @in_current_data  = (select dv.value  from epacube.PRODUCT_STATUS pc Inner Join Epacube.data_value dv on dv.DATA_VALUE_ID = pc.DATA_VALUE_FK
where pc.PRODUCT_STATUS_ID = @in_table_id)
 
END


if @in_table_name = 'PRODUCT_ASSOCIATION'   BEGIN
set @in_product_structure_fk =  (select Product_STRUCTURE_FK from epacube.PRODUCT_ASSOCIATION where PRODUCT_ASSOCIATION_ID = @in_table_id)
set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from epacube.PRODUCT_ASSOCIATION where PRODUCT_ASSOCIATION_ID = @in_table_id)
set @in_cust_entity_structure_fk = (select ENTITY_STRUCTURE_FK from epacube.PRODUCT_ASSOCIATION where PRODUCT_ASSOCIATION_ID = @in_table_id)
set @in_entity_structure_fk = (select ENTITY_STRUCTURE_FK from epacube.PRODUCT_ASSOCIATION where PRODUCT_ASSOCIATION_ID = @in_table_id)
 
END




---PRODUCT LEVEL

 if @in_table_name = 'PRODUCT_MULT_TYPE' and  @in_data_level_cr_fk = 510    BEGIN
set @in_product_structure_fk =  (select Product_STRUCTURE_FK from epacube.PRODUCT_MULT_TYPE where PRODUCT_MULT_TYPE_ID = @in_table_id)
--set @in_cust_entity_structure_fk = (select ENTITY_STRUCTURE_FK from epacube.PRODUCT_MULT_TYPE where PRODUCT_MULT_TYPE_ID = @in_table_id)
--set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from epacube.PRODUCT_MULT_TYPE where PRODUCT_MULT_TYPE_ID = @in_table_id)
set @in_current_data  = (select dv.value  from epacube.PRODUCT_MULT_TYPE PMT Inner Join Epacube.data_value dv on dv.DATA_VALUE_ID = PMT.DATA_VALUE_FK
where PMT.PRODUCT_MULT_TYPE_ID = @in_table_id)
 
END

--CUSTOMER LEVEL

 if @in_table_name = 'PRODUCT_MULT_TYPE' and  @in_data_level_cr_fk = 504    BEGIN
set @in_product_structure_fk =  (select Product_STRUCTURE_FK from epacube.PRODUCT_MULT_TYPE where PRODUCT_MULT_TYPE_ID = @in_table_id)
set @in_cust_entity_structure_fk = (select ENTITY_STRUCTURE_FK from epacube.PRODUCT_MULT_TYPE where PRODUCT_MULT_TYPE_ID = @in_table_id)
--set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from epacube.PRODUCT_MULT_TYPE where PRODUCT_MULT_TYPE_ID = @in_table_id)
set @in_current_data  = (select dv.value  from epacube.PRODUCT_MULT_TYPE PMT Inner Join Epacube.data_value dv on dv.DATA_VALUE_ID = PMT.DATA_VALUE_FK
where PMT.PRODUCT_MULT_TYPE_ID = @in_table_id)
 
END

--ZONE LEVEL

 if @in_table_name = 'PRODUCT_MULT_TYPE' and  @in_data_level_cr_fk = 502    BEGIN
set @in_product_structure_fk =  (select Product_STRUCTURE_FK from epacube.PRODUCT_MULT_TYPE where PRODUCT_MULT_TYPE_ID = @in_table_id)
--set @in_cust_entity_structure_fk = (select ENTITY_STRUCTURE_FK from epacube.PRODUCT_MULT_TYPE where PRODUCT_MULT_TYPE_ID = @in_table_id)
set @in_zone_entity_structure_fk =  (select ENTITY_STRUCTURE_FK from epacube.PRODUCT_MULT_TYPE where PRODUCT_MULT_TYPE_ID = @in_table_id)
set @in_current_data  = (select dv.value  from epacube.PRODUCT_MULT_TYPE PMT Inner Join Epacube.data_value dv on dv.DATA_VALUE_ID = PMT.DATA_VALUE_FK
where PMT.PRODUCT_MULT_TYPE_ID = @in_table_id)
 
END

if @in_table_name   = '#PBV'   ---'PRICESHEET_BASIS_VALUES' or/
 BEGIN


DECLARE @effective_Date Date,
		@in_net_value1_new numeric(18,6),
		@in_net_value1_cur numeric(18,6),
		@in_net_value2_new numeric(18,6),
		@in_net_value2_cur numeric(18,6)


set @in_net_value1_new = case when isnumeric(@in_New_data) = 1 then @in_new_data else NULL end

set @in_product_structure_fk =  (select Product_STRUCTURE_FK from marginmgr.PRICESHEET_BASIS_VALUES where PRICESHEET_BASIS_VALUES_ID = @in_table_id)
set @in_Vendor_entity_structure_fk = (select  Vendor_entity_structure_fk from marginmgr.PRICESHEET_BASIS_VALUES where PRICESHEET_BASIS_VALUES_ID = @in_table_id)
set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from marginmgr.PRICESHEET_BASIS_VALUES where PRICESHEET_BASIS_VALUES_ID = @in_table_id)
set @in_cust_entity_structure_fk = (select  @in_cust_entity_structure_fk from marginmgr.PRICESHEET_BASIS_VALUES where PRICESHEET_BASIS_VALUES_ID = @in_table_id)
set @in_zone_entity_structure_fk = (select  @in_zone_entity_structure_fk from marginmgr.PRICESHEET_BASIS_VALUES where PRICESHEET_BASIS_VALUES_ID = @in_table_id)
set @in_net_value1_cur   = (select value from marginmgr.PRICESHEET_BASIS_VALUES where PRICESHEET_BASIS_VALUES_ID = @in_table_id)
set @in_net_value2_cur   = (select UNIT_VALUE from marginmgr.PRICESHEET_BASIS_VALUES where PRICESHEET_BASIS_VALUES_ID = @in_table_id)
set @effective_Date = (select Effective_Date from marginmgr.PRICESHEET_BASIS_VALUES where PRICESHEET_BASIS_VALUES_ID = @in_table_id)
set @in_pack = (select pack from marginmgr.PRICESHEET_BASIS_VALUES where PRICESHEET_BASIS_VALUES_ID = @in_table_id)
 
set @in_net_value2_new = case when @in_data_name_fk = 503201 then (@in_net_value1_new/isNULL(@in_pack,1)) else NULL END
set @in_New_Data = NULL

END
 

 
--SET @v_count =  (select count(1) from epacube.data_name_group_refs where table_name = 'PRODUCT_RECIPE_INGREDIENT' and data_name_Fk =  @in_data_name_fk and ind_rec_ingredient_xref = 1)

--IF @in_table_name =  'PRODUCT_RECIPE_ATTRIBUTE'   and @v_count > 0

 
  if @in_table_name = 'PRODUCT_RECIPE_ATTRIBUTE'   BEGIN
set @in_product_structure_fk =  (select Product_STRUCTURE_FK from epacube.PRODUCT_RECIPE_ATTRIBUTE where PRODUCT_RECIPE_ATTRIBUTE_ID = @in_table_id)
set @in_Vendor_entity_structure_fk = (select entity_STRUCTURE_FK from epacube.PRODUCT_RECIPE_ATTRIBUTE where PRODUCT_RECIPE_ATTRIBUTE_ID = @in_table_id)
set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from epacube.PRODUCT_RECIPE_ATTRIBUTE where PRODUCT_RECIPE_ATTRIBUTE_ID = @in_table_id)
set @in_current_data  = (select ATTRIBUTE_EVENT_DATA from epacube.PRODUCT_RECIPE_ATTRIBUTE where PRODUCT_RECIPE_ATTRIBUTE_ID = @in_table_id)
set @in_table_id  = (select PRODUCT_RECIPE_FK from epacube.PRODUCT_RECIPE_ATTRIBUTE where PRODUCT_RECIPE_ATTRIBUTE_ID = @in_table_id)
 
END
 --'SETTINGS_POS_PROD_STORE_VALUES'

 IF @in_table_name = 'SETTINGS_POS_PROD_STORE_HEADER'    BEGIN
 
 set @in_SETTINGS_POS_PROD_STORE_HEADER_ID = @in_table_id
 set @in_product_structure_fk =  (select PRODUCT_STRUCTURE_FK from epacube.SETTINGS_POS_PROD_STORE_HEADER where SETTINGS_POS_PROD_STORE_HEADER_ID = @in_table_id)
set @in_cust_entity_structure_fk =  (select cust_ENTITY_STRUCTURE_FK from epacube.SETTINGS_POS_PROD_STORE_HEADER where SETTINGS_POS_PROD_STORE_HEADER_ID  = @in_table_id)
--set @in_dept_entity_DATA_VALUE_fk =  (select DEPT_ENTITY_DATA_VALUE_FK from epacube.SETTINGS_POS_PROD_STORE_HEADER where SETTINGS_POS_PROD_STORE_HEADER_ID  = @in_table_id)
--set prod entity data value fk
set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from epacube.SETTINGS_POS_PROD_STORE_HEADER where SETTINGS_POS_PROD_STORE_HEADER_ID = @in_table_id)
set @in_current_data  = (select ATTRIBUTE_EVENT_DATA from epacube.SETTINGS_POS_PROD_STORE_VALUES where SETTINGS_POS_PROD_STORE_HEADER_FK  = @in_table_id and DATA_NAME_FK = @in_data_name_fk)
--set @in_data_level_cr_fk  = (select DATA_LEVEL from epacube.SETTINGS_POS_PROD_STORE_HEADER where SETTINGS_POS_PROD_STORE_HEADER_ID  = @in_table_id)
set @in_Precedence  = (select Precedence from epacube.SETTINGS_POS_PROD_STORE_VALUES where SETTINGS_POS_PROD_STORE_HEADER_FK = @in_table_id and DATA_NAME_FK = @in_data_name_fk) 
set @in_table_id  =	(select SETTINGS_POS_PROD_STORE_VALUES_ID from epacube.SETTINGS_POS_PROD_STORE_VALUES where SETTINGS_POS_PROD_STORE_HEADER_FK = @in_table_id and DATA_NAME_FK = @in_data_name_fk) 

END


IF @in_table_name =  'PRODUCT_RECIPE_INGREDIENT_XREF'   BEGIN
  
set @in_current_data  = (select value  from epacube.product_identification where product_structure_fk = @in_product_structure_fk and data_name_fk = 110100 )
--(select dv.value from epacube.PRODUCT_RECIPE_INGREDIENT_XREF X inner join epacube.data_value dv on dv.data_value_id = x.data_value_fk
--where PRODUCT_RECIPE_INGREDIENT_XREF_ID  = @in_table_id and dv.DATA_NAME_FK = @in_data_name_fk)
set @in_product_structure_fk = (select product_structure_fk from epacube.product_identification where value = @in_new_data and data_name_fk = 110100 )

END 

-------------------------------------------------------------------------------------------------------------------------- 
 ---- are we dealing with the ingredient product or the recipe product if count greater than 0 it's ingredient product

SET @v_count =  (select count(1) from epacube.data_name_group_refs where table_name = 'PRODUCT_RECIPE_INGREDIENT' and data_name_Fk =  @in_data_name_fk and ind_rec_ingredient_xref = 1)

IF @in_table_name =  'PRODUCT_RECIPE_INGREDIENT'   and @v_count > 0

--select I.* from epacube.product_recipe_ingredient I 
--inner join epacube.product_recipe_ingredient_xref X on X.product_recipe_ingredient_xref_id = I.product_recipe_ingredient_xref_fk 
--where I.PRODUCT_RECIPE_INGREDIENT_id = 5150
 
BEGIN
  
set @in_product_structure_fk = (select x.product_structure_fk from epacube.product_recipe_ingredient I 
inner join epacube.product_recipe_ingredient_xref X on X.product_recipe_ingredient_xref_id = I.product_recipe_ingredient_xref_fk where I.PRODUCT_RECIPE_INGREDIENT_id = @in_table_id )
--set @in_product_Recipe_fk = (select PRODUCT_RECIPE_FK from epacube.product_recipe_ingredient where PRODUCT_RECIPE_INGREDIENT_id = @in_table_id)

END

---------------------------------------------------------------------------------------------
---  Batch NO  ---UI to CREATE and BATCH all events together.
----------------------------------------------------------------------------------------
		   
	--EXEC seq_manager.db_get_next_sequence_value_impl 'common',
 --                   'batch_seq', @in_batch_no output



INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
		   ,[PRODUCT_STRUCTURE_FK]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]        
           ,[NEW_DATA]
           ,[CURRENT_DATA]
		   ,[NET_VALUE1_NEW]
		   ,[NET_VALUE1_CUR]
		    ,[NET_VALUE2_NEW]
		   ,[NET_VALUE2_CUR]
		   ,PACK
		   ,ENTITY_STRUCTURE_FK
		   ,[ORG_ENTITY_STRUCTURE_FK]
		   ,[CUST_ENTITY_STRUCTURE_FK]
           ,[ZONE_ENTITY_STRUCTURE_FK]
           ,[DEPT_ENTITY_STRUCTURE_FK]
           ,[VENDOR_ENTITY_STRUCTURE_FK]
		   ,CO_ENTITY_STRUCTURE_FK
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
		   ,ASSOC_PARENT_CHILD
           ,[TABLE_ID_FK]
		   ,[IMPORT_FILENAME]
           ,[DATA_LEVEL]
		   ,SEARCH1
           ,[BATCH_NO]
           ,[DATA_VALUE_FK]
		   ,ENTITY_DATA_VALUE_FK
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
 SELECT 
 ISNULL(@in_EVENT_TYPE_CR_FK,150)
,ISNULL(@v_ENTITY_CLASS_CR_FK,10109)
,getdate() 
 ,@in_product_structure_fk
 ,@in_product_structure_fk 
 ,@in_data_name_fk
, @in_new_data
 ,@in_current_data
,ISNULL(@in_net_value1_new,-999)
,ISNULL(@in_net_value1_cur,-999)
,ISNULL(@in_net_value2_new,-999)
,ISNULL(@in_net_value2_cur,-999)
,@in_PACK
,@in_entity_structure_fk
,case WHEN @in_org_entity_structure_fk = 0 THEN ISNULL(@in_org_entity_structure_fk,1) ELSE ISNULL(@in_org_entity_structure_fk,1) END
,@in_cust_entity_structure_fk
,@in_Zone_entity_structure_fk
,@in_dept_entity_structure_fk
 ,@in_Vendor_entity_structure_fk
 ,101 co_entity_structure_fk
,97
,88
,66
,71
,case when @in_new_data = 'PURGE' then 59  when @in_new_data = 'ACTIVATE' then 50 when  @in_table_id is NULL then 51 else 52 End 
,@in_SETTINGS_POS_PROD_STORE_HEADER_ID 
,@in_table_id
,@in_Table_name
,@in_data_level_cr_fk
,@in_product_Recipe_fk
,@in_batch_no 
,@in_data_value_fk
,@in_entity_DATA_value_fk 
 ,case when @in_new_data = '-999999999' then 2 else 1 END
 ,getdate() 
,@UserID
 


-------------------------------------------------------------------------------------------
---setting new data data values

							UPDATE ED 
							set ED.data_value_fk =   dv.DATA_VALUE_ID 
							from epacube.data_name DN 
							 inner join epacube.data_value DV on DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK
							 inner join synchronizer.event_data ED on dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and ed.NEW_DATA = dv.VALUE and ed.EVENT_STATUS_CR_FK = 88
							 where DN.source_data_dn_fk is not NULL

							UPDATE ED 
							 SET ED.data_value_fk = DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 inner join synchronizer.EVENT_DATA ED WITH (NOLOCK)  
							 ON DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA
							 INNER JOIN Epacube.data_name DN WITH (nolock) on dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NULL 
							 where ed.EVENT_STATUS_CR_FK = 88
------------------------------------------------------------------------------------------------------------------
--clean up if not net values type of events

update synchronizer.event_data
set NET_VALUE1_NEW = NULL
where NET_VALUE1_NEW = -999.000000
and EVENT_STATUS_CR_FK = 88
and Event_type_cr_Fk <> 158

update synchronizer.event_data
set NET_VALUE1_cur = NULL
where NET_VALUE1_cur = -999.000000
and EVENT_STATUS_CR_FK = 88
and Event_type_cr_Fk <> 158

---------check for numeric if Pricesheet basis values 

            INSERT INTO synchronizer.EVENT_DATA_ERRORS (
                        EVENT_FK,
                        EVENT_CONDITION_CR_FK,
                        ERROR_NAME,
                        UPDATE_TIMESTAMP)
            SELECT
                   EVENT_ID
                  ,99
                  ,'NUMERIC REQUIRED'  --ERROR NAME        
                  ,getdate()
               from synchronizer.event_data where import_filename = '#PBV'
			   and @in_table_name = '#PBV'
			   ----AND isnumeric(NET_VALUE1_NEW ) = 0
						 AND isnumeric(NEW_DATA ) = 0
						 and NET_VALUE1_NEW = '-999'
						 AND event_status_Cr_fk = 88
						 AND EVENT_TYPE_CR_FK = 158
						 --AND BATCH_NO = @in_batch_no


						   
 if  @in_table_name   = '#PBV'  and  isnumeric(@in_New_data) = 0 
 BEGIN

 SET @l_sysdate = getdate ()
SET @ls_stmt = 'BAD DATA.  NUMERIC REQUIRED - stopping procedure'  
					 + ' Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))					 				 					 					 
					 + ' Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))							 
												 					 


EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_job_id = NULL,
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;

---added so event doesn't fail tolerance rule.
update synchronizer.event_data
set NET_VALUE1_NEW = NULL
where NET_VALUE1_NEW = -999.000000
and EVENT_STATUS_CR_FK = 88
and Event_type_cr_Fk = 158
 
 
 --PRINT 'before raiserror'
   
 --raiserror ('NUMERIC REQUIRED', 10,1) with nowait

            RETURN
     END 


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_COMPONENT_PRODUCT_DATA'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_COMPONENT_PRODUCT_DATA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END



















