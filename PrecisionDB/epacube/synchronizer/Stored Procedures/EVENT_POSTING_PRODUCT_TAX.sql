﻿






-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Event processing for product events
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010   Initial SQL Version
-- KS        09/27/2011   Added Purge Action for UNDO 
-- KS        10/15/2012   EPA-3400 Added Delete for EVENT_DATA_RULES to remove duplicates
-- CV        01/13/2017   Rules type cr fk from Rules table
--
--

CREATE PROCEDURE [synchronizer].[EVENT_POSTING_PRODUCT_TAX] 
        ( @in_event_fk BIGINT, @in_post_ind SMALLINT, 
          @in_batch_no BIGINT, @in_import_job_fk BIGINT, 
          @in_event_priority_cr_fk INT, @in_event_type_cr_fk INT,
          @in_table_name VARCHAR(64), @in_data_name_fk BIGINT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime


DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_PRODUCT_TAX.'
					 + cast(isnull(@in_batch_no, 0) as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;



------            exec sp_executesql 	@ls_stmt  USE TO THROW ERROR

SET @l_sysdate = GETDATE()


------------------------------------------------------------------------------------------
--   Save this code snippet for causing immediate stop
------------------------------------------------------------------------------------------

----            SET @ls_exec = 'STOP CODE HERE'            
----            INSERT INTO COMMON.T_SQL
----                ( TS,  SQL_TEXT )
----            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
----            exec sp_executesql 	@ls_exec;



------------------------------------------------------------------------------------------
---  CLEAR TABLE FOR NEXT BATCH_NO, PRIORITY, JOB_FK, EVENT_TYPE
------------------------------------------------------------------------------------------

   
   TRUNCATE TABLE #TS_EVENT_DATA



------------------------------------------------------------------------------------------
---   PRODUCT TAXONOMY    --- EVENT_TYPE = 153
------------------------------------------------------------------------------------------


IF @in_event_type_cr_fk IN ( 153 ) 

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			SINGLE_ASSOC_IND,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
			PTAX.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  ( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			PTAX.ATTRIBUTE_EVENT_DATA,  --VCEP.CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
			CASE WHEN ED.EVENT_ACTION_CR_FK = 59
			THEN 59
			ELSE
			CASE WHEN PTAX.UPDATE_TIMESTAMP IS NULL
			THEN 51
			ELSE 52
			END
			END,
			ED.EVENT_STATUS_CR_FK,
			ED.EVENT_CONDITION_CR_FK,
			ED.EVENT_SOURCE_CR_FK,
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			DN.SINGLE_ASSOC_IND,
			PTAX.PRODUCT_TAX_ATTRIBUTE_ID,  ----VCEP.TABLE_ID_FK,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE
------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
LEFT JOIN epacube.PRODUCT_STRUCTURE PS WITH (NOLOCK)
			 ON ( PS.PRODUCT_STRUCTURE_ID = ED.PRODUCT_STRUCTURE_FK )
			LEFT JOIN epacube.PRODUCT_TAX_ATTRIBUTE PTAX WITH (NOLOCK)
			 ON ( PTAX.DATA_NAME_FK = ED.DATA_NAME_FK
			 AND  ISNULL ( PTAX.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( ED.PRODUCT_STRUCTURE_FK, 0 )  )
		WHERE 1 = 1 
		AND  ED.EVENT_STATUS_CR_FK = 88
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )




					                             
--------------------------------------------------------------------------------------------
--   CALL VALIDATIONS
--------------------------------------------------------------------------------------------
    
	TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
	TRUNCATE TABLE #TS_EVENT_DATA_RULES	

------------------------------------------------------------------------------------------
--  DELETE ROWS  --- ERRORS ARE REMOVED THEN RE-INSERTED
--					 TIMIMG MUST DO HERE BECAUSE OF CONFLICT AND DUPLICATE VALIDATIONS
--					 DO NOT REMOVE THE EVENT_DATA_RULES  ( ONCE SET FOR AN EVENT.. SET )
------------------------------------------------------------------------------------------

	DELETE
	FROM synchronizer.EVENT_DATA_ERRORS
	WHERE event_fk IN (
	SELECT EVENT_FK
	FROM #TS_EVENT_DATA )

-- KS        10/15/2012   EPA-3400 Added Delete for EVENT_DATA_RULES to remove duplicates
	DELETE
	FROM synchronizer.EVENT_DATA_RULES
	WHERE event_fk IN (
	SELECT EVENT_FK
	FROM #TS_EVENT_DATA )


	------------------------------------------------------------------------------------------
	--   QUALIFY GOVERNANCE RULES 
	------------------------------------------------------------------------------------------
    
	SET  @v_count = ( SELECT COUNT(1)
					 FROM #TS_EVENT_DATA WITH (NOLOCK)
					 WHERE BATCH_NO = @in_batch_no
					 AND   EVENT_STATUS_CR_FK = 88 
					 AND   EVENT_TYPE_CR_FK IN ( 150, 151, 152, 153, 154, 155, 156, 160 )   			 
					 AND   (  EVENT_SOURCE_CR_FK = 71        -- WHEN UI CALLS VALIDATION DIRECTLY WILL REMOVE 71 
						   OR ISNULL ( IMPORT_BATCH_NO, -999 ) = @in_batch_no )    
					 AND   DATA_NAME_FK IN ( SELECT R.RESULT_DATA_NAME_FK   
											 FROM synchronizer.RULES R WITH (NOLOCK)
											 --INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
											 --  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK 
											 --  AND  RS.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE
											 WHERE R.RECORD_STATUS_CR_FK = 1 
											 AND  R.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE)
					)			                         
	IF @v_count > 0
		EXEC synchronizer.EVENT_POST_QUALIFY_RULES_GOVERNANCE  @in_batch_no


    EXEC [synchronizer].[EVENT_POST_VALID_DATA_TYPE] 

    EXEC [synchronizer].[EVENT_POST_VALID_TYPE_153] 
	
	EXEC SYNCHRONIZER.EVENT_POST_VALID_DATA_VALUE
		    
	EXEC [synchronizer].[EVENT_POST_VALID_TYPE_ALL] 		    


------------------------------------------------------------------------------------------
--  EVENT is a TAXONOMY TREE NODE REASSIGMENT
------------------------------------------------------------------------------------------

SET @V_COUNT =   ( SELECT COUNT(1)
					FROM #TS_EVENT_DATA TSED WITH (NOLOCK)
					INNER JOIN epacube.TAXONOMY_TREE TT WITH (NOLOCK)
					 ON ( TT.TAX_NODE_DN_FK = TSED.DATA_NAME_FK )
					WHERE TSED.CURRENT_DATA IS NOT NULL
					AND   TSED.NEW_DATA <> ISNULL ( TSED.CURRENT_DATA, 'NULL DATA' )
				 )
               
IF @V_COUNT > 0               
EXEC synchronizer.EVENT_POST_VALID_TAX_REASSIGN



------------------------------------------------------------------------------------------
-- CALL PROC TO SET EVENT PRIORITY ACTION AND STATUS   ( KEEP IN ONE PLACE WITH CODE )
------------------------------------------------------------------------------------------

	EXEC  [synchronizer].[EVENT_POST_COND_PRIORITY_ACTION_STATUS] @in_batch_no



--------------------------------------------------------------------------------------------
--   CALL POST AND COMPLETE ... added @in_table_lock
--------------------------------------------------------------------------------------------

----	IF @in_post_ind = 1
		EXEC SYNCHRONIZER.EVENT_POST_DATA  


------------------------------------------------------------------------------------------
--  Update synchronizer.EVENT_DATA from #TS_EVENT_DATA  ( POST_COMPLETE )
------------------------------------------------------------------------------------------


UPDATE synchronizer.EVENT_DATA
   SET EVENT_TYPE_CR_FK			= TSED.EVENT_TYPE_CR_FK
      ,EVENT_ENTITY_CLASS_CR_FK = TSED.EVENT_ENTITY_CLASS_CR_FK
      ,EVENT_EFFECTIVE_DATE		= TSED.EVENT_EFFECTIVE_DATE
      ,EPACUBE_ID				= TSED.EPACUBE_ID
      ,DATA_NAME_FK				= TSED.DATA_NAME_FK
      ,NEW_DATA                 = TSED.NEW_DATA      
      ,CURRENT_DATA             = TSED.CURRENT_DATA
      ,PRODUCT_STRUCTURE_FK		= TSED.PRODUCT_STRUCTURE_FK
      ,ORG_ENTITY_STRUCTURE_FK	= TSED.ORG_ENTITY_STRUCTURE_FK
      ,ENTITY_CLASS_CR_FK		= TSED.ENTITY_CLASS_CR_FK
      ,ENTITY_DATA_NAME_FK		= TSED.ENTITY_DATA_NAME_FK
      ,ENTITY_STRUCTURE_FK		= TSED.ENTITY_STRUCTURE_FK
      ,EVENT_CONDITION_CR_FK	= TSED.EVENT_CONDITION_CR_FK
      ,EVENT_STATUS_CR_FK		= TSED.EVENT_STATUS_CR_FK 
      ,EVENT_PRIORITY_CR_FK		= TSED.EVENT_PRIORITY_CR_FK
      ,RESULT_TYPE_CR_FK		= TSED.RESULT_TYPE_CR_FK
      ,EVENT_ACTION_CR_FK		= TSED.EVENT_ACTION_CR_FK
      ,TABLE_ID_FK				= TSED.TABLE_ID_FK
      ,EFFECTIVE_DATE_CUR	    = TSED.EFFECTIVE_DATE_CUR
      ,END_DATE_NEW				= TSED.END_DATE_NEW
      ,END_DATE_CUR				= TSED.END_DATE_CUR
      ,DATA_VALUE_FK            = TSED.DATA_VALUE_FK
      ,ENTITY_DATA_VALUE_FK     = TSED.ENTITY_DATA_VALUE_FK
      ,VALUE_UOM_CODE_FK		= TSED.VALUE_UOM_CODE_FK
      ,ASSOC_UOM_CODE_FK		= TSED.ASSOC_UOM_CODE_FK   
      ,UOM_CONVERSION_FACTOR    = TSED.UOM_CONVERSION_FACTOR   
      ,RECORD_STATUS_CR_FK		= TSED.RECORD_STATUS_CR_FK      
      ,UPDATE_TIMESTAMP			= TSED.UPDATE_TIMESTAMP
FROM #TS_EVENT_DATA  TSED
WHERE TSED.EVENT_FK = synchronizer.EVENT_DATA.EVENT_ID



------------------------------------------------------------------------------------------
--   INSERT INTO synchronizer.EVENT_DATA_ERRORS 
------------------------------------------------------------------------------------------


		INSERT INTO synchronizer.EVENT_DATA_ERRORS 
				(EVENT_FK
			   ,RULE_FK
			   ,EVENT_CONDITION_CR_FK
			   ,DATA_NAME_FK
			   ,ERROR_NAME
			   ,ERROR_DATA1
			   ,ERROR_DATA2
			   ,ERROR_DATA3
			   ,ERROR_DATA4
			   ,ERROR_DATA5
			   ,ERROR_DATA6
			   ,UPDATE_TIMESTAMP)
		   SELECT 
				 TEDE.EVENT_FK
				,TEDE.RULE_FK
				,TEDE.EVENT_CONDITION_CR_FK
				,TEDE.DATA_NAME_FK
				,TEDE.ERROR_NAME
				,TEDE.ERROR_DATA1
				,TEDE.ERROR_DATA2
				,TEDE.ERROR_DATA3
				,TEDE.ERROR_DATA4
				,TEDE.ERROR_DATA5
				,TEDE.ERROR_DATA6
				,TEDE.UPDATE_TIMESTAMP
			FROM #TS_EVENT_DATA_ERRORS TEDE


------------------------------------------------------------------------------------------
--   INSERT INTO synchronizer.EVENT_DATA_RULES 
--    CHECK EXISTANCE IN EVENT_DATA_RULE TABLE PRIOR TO INSERTING....
------------------------------------------------------------------------------------------

	INSERT INTO [synchronizer].[EVENT_DATA_RULES]
	 (   EVENT_FK      		
		,RULES_FK      
		,RESULT_DATA_NAME_FK      		
		,RESULT_TYPE_CR_FK      	
		,RESULT_EFFECTIVE_DATE      	
		,RULES_EFFECTIVE_DATE 
		,RULES_ACTION_OPERATION_FK     
		,RULE_PRECEDENCE
		,SCHED_PRECEDENCE				
		 ) 			
		SELECT DISTINCT 
         EVENT_FK      		
		,RULES_FK      
		,RESULT_DATA_NAME_FK      		
		,RESULT_TYPE_CR_FK      	
		,RESULT_EFFECTIVE_DATE      	
		,RULES_EFFECTIVE_DATE  
		,RULES_ACTION_OPERATION_FK    
		,RULE_PRECEDENCE
		,SCHED_PRECEDENCE			
	FROM #TS_EVENT_DATA_RULES TSEDR
    WHERE NOT EXISTS ( SELECT 1
                       FROM synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK)
                       WHERE EDR.EVENT_FK  = TSEDR.EVENT_FK
                       AND   EDR.RULES_FK  = TSEDR.RULES_FK )



------------------------------------------------------------------------------------------
---  CLEAR TABLE FOR NEXT BATCH_NO, PRIORITY, JOB_FK, EVENT_TYPE
------------------------------------------------------------------------------------------

   
   TRUNCATE TABLE #TS_EVENT_DATA
   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
   TRUNCATE TABLE #TS_EVENT_DATA_RULES      



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_PRODUCT_TAX'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @in_batch_no
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_PRODUCT_TAX has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END































