﻿



--1011	66	#PBV	158









-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Event processing for product events
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010   Initial SQL Version
-- KS		 03/04/2010   Comment out call to Conflict Validation until Tuned
-- CV        09/21/2010   Governance Rule added parent data name to count 
-- KS        09/27/2011   Added Purge Action for UNDO 
-- KS        10/15/2012   EPA-3400 Added Delete for EVENT_DATA_RULES to remove duplicates
-- RG		 12/20/2013   SUP-2633 Refactore where clause for results events 
-- RG		 01/29/2014	  SUP-2633 cont'd + SUP-2788 unable to approve from UI 
-- CV        02/06/2014   Working on Sheet Results section
-- CV		 09/16/2016   Adding Pricesheet basis values section 158

CREATE PROCEDURE [synchronizer].[EVENT_POSTING_PRODUCT_151_152] 
        ( @in_event_fk BIGINT, @in_post_ind SMALLINT, 
          @in_batch_no BIGINT, @in_import_job_fk BIGINT, 
          @in_event_priority_cr_fk INT, @in_event_type_cr_fk INT,
          @in_table_name VARCHAR(64), @in_data_name_fk BIGINT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime


DECLARE  @ls_exec1            nvarchar (max)
DECLARE  @ls_exec2            nvarchar (max)
DECLARE  @ls_exec3            nvarchar (max)

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_PRODUCT_151_152.'
					 + 'Event: ' + cast(isnull(@in_event_fk, 0) as varchar(30))
					 + 'Post Ind: ' + cast(isnull(@in_post_ind, 0) as varchar(30))					 
					 + 'Batch: ' + cast(isnull(@in_batch_no, 0) as varchar(30))
					 + 'Import: ' + cast(isnull(@in_import_job_fk, 0) as varchar(30))	
					 + 'Priority: ' + cast(isnull(@in_event_priority_cr_fk, 0) as varchar(30))					 				 
					 + 'Event Type: ' + cast(isnull(@in_event_type_cr_fk, 0) as varchar(30))					 				 
					 + 'Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))	
					 + 'Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))							 					 				 				 					 					 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;



------            exec sp_executesql 	@ls_stmt  USE TO THROW ERROR

SET @l_sysdate = GETDATE()




------------------------------------------------------------------------------------------
--   Save this code snippet for causing immediate stop
------------------------------------------------------------------------------------------

----            SET @ls_exec = 'STOP CODE HERE'            
----            INSERT INTO COMMON.T_SQL
----                ( TS,  SQL_TEXT )
----            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
----            exec sp_executesql 	@ls_exec;



------------------------------------------------------------------------------------------
---  CLEAR TABLE FOR NEXT BATCH_NO, PRIORITY, JOB_FK, EVENT_TYPE
------------------------------------------------------------------------------------------

      TRUNCATE TABLE #TS_EVENT_DATA


	  

--------------------------------------------------------------------------------------------
--   PRODUCT_VALUES EVENTS  
--------------------------------------------------------------------------------------------

--IF @in_event_type_cr_fk = 151

--	INSERT INTO #TS_EVENT_DATA 
--		(   EVENT_FK,
--			EVENT_TYPE_CR_FK,
--			EVENT_ENTITY_CLASS_CR_FK,
--			EPACUBE_ID,
--			DATA_NAME_FK,			
--			IMPORT_JOB_FK,			
--			PRODUCT_STRUCTURE_FK,
--			ORG_ENTITY_STRUCTURE_FK,
--			ENTITY_CLASS_CR_FK,
--			ENTITY_DATA_NAME_FK,
--			ENTITY_STRUCTURE_FK,
--			DATA_SET_FK,
--			DATA_TYPE_CR_FK,
--			ORG_IND,
--			TABLE_NAME,
--			NET_VALUE1_NEW,
--			NET_VALUE2_NEW,
--			NET_VALUE3_NEW,
--			NET_VALUE4_NEW,
--			NET_VALUE5_NEW,
--			NET_VALUE6_NEW,													
--			NET_VALUE1_CUR,
--			NET_VALUE2_CUR,
--			NET_VALUE3_CUR,
--			NET_VALUE4_CUR,
--			NET_VALUE5_CUR,
--			NET_VALUE6_CUR,							
--			PERCENT_CHANGE1,
--			PERCENT_CHANGE2,
--			PERCENT_CHANGE3,
--			PERCENT_CHANGE4,
--			PERCENT_CHANGE5,
--			PERCENT_CHANGE6,							
--			VALUE_UOM_CODE_FK,
--			ASSOC_UOM_CODE_FK,
--			VALUES_SHEET_NAME,
--			UOM_CONVERSION_FACTOR,
--			SEARCH1,
--			END_DATE_CUR,							
--			END_DATE_NEW,							
--			EFFECTIVE_DATE_CUR,
--			EVENT_EFFECTIVE_DATE,
--			EVENT_ACTION_CR_FK,
--			EVENT_STATUS_CR_FK,	
--			EVENT_CONDITION_CR_FK,
--			EVENT_SOURCE_CR_FK,
--			EVENT_PRIORITY_CR_FK,
--			RESULT_TYPE_CR_FK,
--			TABLE_ID_FK,
--			BATCH_NO,
--			IMPORT_BATCH_NO,
--			RULES_FK,	
--			RECORD_STATUS_CR_FK,		
--			UPDATE_TIMESTAMP
--			 ) 	
--	SELECT
--		    A.EVENT_ID,                    
--			A.EVENT_TYPE_CR_FK,
--			A.EVENT_ENTITY_CLASS_CR_FK,
--			A.EPACUBE_ID,
--			A.DATA_NAME_FK,			
--			A.IMPORT_JOB_FK,
--			A.PRODUCT_STRUCTURE_FK,
--			A.ORG_ENTITY_STRUCTURE_FK,
--			A.ENTITY_CLASS_CR_FK,
--			A.ENTITY_DATA_NAME_FK,
--			A.ENTITY_STRUCTURE_FK,							
--			A.DATA_SET_FK,
--			A.DATA_TYPE_CR_FK,
--			A.ORG_IND,
--			A.TABLE_NAME,
--			A.NET_VALUE1_NEW,
--			A.NET_VALUE2_NEW,
--			A.NET_VALUE3_NEW,
--			A.NET_VALUE4_NEW,
--			A.NET_VALUE5_NEW,
--			A.NET_VALUE6_NEW,													
--			A.NET_VALUE1,
--			A.NET_VALUE2,
--			A.NET_VALUE3,
--			A.NET_VALUE4,
--			A.NET_VALUE5,
--			A.NET_VALUE6,							
--			CASE ISNULL (A.NET_VALUE1, 0 )
--			 WHEN 0 THEN 0  
--			 ELSE ( ( A.NET_VALUE1_NEW  - A.NET_VALUE1 ) / A.NET_VALUE1 )
--			 END,
--			CASE ISNULL (A.NET_VALUE2, 0 )
--			 WHEN 0 THEN 0  
--			 ELSE ( ( A.NET_VALUE2_NEW  - A.NET_VALUE2 ) / A.NET_VALUE2 )
--			 END,
--			CASE ISNULL (A.NET_VALUE3, 0 )
--			 WHEN 0 THEN 0  
--			 ELSE ( ( A.NET_VALUE3_NEW  - A.NET_VALUE3 ) / A.NET_VALUE3)
--			 END,
--			CASE ISNULL (A.NET_VALUE4, 0 )
--			 WHEN 0 THEN 0  
--			 ELSE ( ( A.NET_VALUE4_NEW  - A.NET_VALUE4 ) / A.NET_VALUE4 )
--			 END,
--			CASE ISNULL (A.NET_VALUE5, 0 )
--			 WHEN 0 THEN 0  
--			 ELSE ( ( A.NET_VALUE5_NEW  - A.NET_VALUE5 ) / A.NET_VALUE5 )
--			 END,
--			CASE ISNULL (A.NET_VALUE6, 0 )
--			 WHEN 0 THEN 0  
--			 ELSE ( ( A.NET_VALUE6_NEW  - A.NET_VALUE6 ) / A.NET_VALUE6 )
--			 END,
--			A.VALUE_UOM_CODE_FK,
--			A.ASSOC_UOM_CODE_FK,
--			A.VALUES_SHEET_NAME,
--			( SELECT CASE ISNULL ( A.VALUE_UOM_CODE_FK, 0 ) 
--			WHEN 0 THEN 1
--			ELSE
--			CASE ISNULL ( A.ASSOC_UOM_CODE_FK, 0 ) 
--			WHEN 0 THEN 1
--			ELSE
--			( ISNULL (
--			( SELECT 
--			( SELECT ISNULL (
--			   (SELECT uc.primary_qty
--				FROM epacube.UOM_CODE uc WITH (NOLOCK)
--				WHERE uc.UOM_CODE_ID = A.VALUE_UOM_CODE_FK  ), 1 )
--			)
--			/
--			ISNULL (   (SELECT uc.primary_qty
--						FROM epacube.UOM_CODE uc WITH (NOLOCK)
--						WHERE uc.UOM_CODE_ID = A.ASSOC_UOM_CODE_FK )
--					 , ISNULL ( (SELECT uc.primary_qty
--					             FROM epacube.UOM_CODE uc WITH (NOLOCK)
--					             WHERE uc.UOM_CODE_ID = A.VALUE_UOM_CODE_FK) , 1 ) 
--					 )
--			) , 1 ) )
--			END END
--			)  AS UOM_CONVERSION_FACTOR,
--			A.SEARCH1,
--			A.END_DATE,
--			A.END_DATE_NEW,							
--			A.EFFECTIVE_DATE,			
--			A.EVENT_EFFECTIVE_DATE,
--			A.EVENT_ACTION_CR_FK,
--			A.EVENT_STATUS_CR_FK,	
--			A.EVENT_CONDITION_CR_FK,
--			A.EVENT_SOURCE_CR_FK,
--			A.EVENT_PRIORITY_CR_FK,
--			A.RESULT_TYPE_CR_FK,			
--			A.PRODUCT_VALUES_ID,
--			A.BATCH_NO,
--			A.IMPORT_BATCH_NO,
--			A.RULES_FK,
--			A.RECORD_STATUS_CR_FK,
--			@L_SYSDATE
----------
--	FROM ( SELECT
--		    ED.EVENT_ID,                    
--			DS.EVENT_TYPE_CR_FK,
--			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
--			ED.PRODUCT_STRUCTURE_FK AS EPACUBE_ID,
--			ED.DATA_NAME_FK,
--			ED.IMPORT_JOB_FK,			
--			ED.PRODUCT_STRUCTURE_FK,
--			CASE ISNULL ( DS.ORG_IND, 0 )
--			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
--			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
--			END AS ORG_ENTITY_STRUCTURE_FK,
--			DS.ENTITY_STRUCTURE_EC_CR_FK AS ENTITY_CLASS_CR_FK,
--			DN.ENTITY_DATA_NAME_FK,
--			ED.ENTITY_STRUCTURE_FK,																					
--			DN.DATA_SET_FK,
--			DS.DATA_TYPE_CR_FK,
--			DS.ORG_IND,
--			DS.TABLE_NAME,
--			ED.NET_VALUE1_NEW,
--			ED.NET_VALUE2_NEW,
--			ED.NET_VALUE3_NEW,
--			ED.NET_VALUE4_NEW,
--			ED.NET_VALUE5_NEW,
--			ED.NET_VALUE6_NEW,													
--			PV.NET_VALUE1,
--			PV.NET_VALUE2,
--			PV.NET_VALUE3,
--			PV.NET_VALUE4,
--			PV.NET_VALUE5,
--			PV.NET_VALUE6,		
--			ED.VALUE_UOM_CODE_FK,   --- EITHER HAVE IT OR DO NOT .. IF NOT USE ASSOC_UOM_CODE_FK					
--		    ( SELECT TOP 1 PUC.UOM_CODE_FK
--			  FROM epacube.PRODUCT_UOM_CLASS PUC WITH (NOLOCK)
--			  WHERE ( PUC.DATA_NAME_FK = DN.UOM_CLASS_DATA_NAME_FK
--			  AND     PUC.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
--			  AND   (  (   DS.ORG_IND = 1
--					   AND PUC.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK )
--					  OR PUC.ORG_ENTITY_STRUCTURE_FK = 1 ) 
--              AND ISNULL ( PUC.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ED.ENTITY_STRUCTURE_FK, -999 )   )
--			) AS ASSOC_UOM_CODE_FK,
--			ED.VALUES_SHEET_NAME,
--			ED.SEARCH1,
--			PV.END_DATE,
--			ED.END_DATE_NEW,
--			PV.EFFECTIVE_DATE,
--			ED.EVENT_EFFECTIVE_DATE, 
--			CASE WHEN ED.EVENT_ACTION_CR_FK = 59
--			THEN 59
--			ELSE
--			CASE WHEN PV.PRODUCT_VALUES_ID IS NULL
--			THEN 51
--			ELSE 52
--			END
--			END AS EVENT_ACTION_CR_FK,
--			ED.EVENT_STATUS_CR_FK,	
--			ED.EVENT_CONDITION_CR_FK,
--			ED.EVENT_SOURCE_CR_FK,
--			ED.EVENT_PRIORITY_CR_FK,
--			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ) AS RESULT_TYPE_CR_FK,			
--			PV.PRODUCT_VALUES_ID,
--			ED.BATCH_NO,
--			ED.IMPORT_BATCH_NO,
--			ED.RULES_FK,
--			ED.RECORD_STATUS_CR_FK
--	FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
--		INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
--		 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
--		INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
--		 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
--		LEFT JOIN epacube.PRODUCT_VALUES PV WITH (NOLOCK)
--		 ON ( PV.DATA_NAME_FK = ED.DATA_NAME_FK
--		 AND  ISNULL ( PV.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( ED.PRODUCT_STRUCTURE_FK, 0 )
--		 AND  ISNULL ( PV.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
--		 AND  ISNULL ( PV.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( ED.ENTITY_STRUCTURE_FK, 0 )  )
--		WHERE 1 = 1 
--		AND  EVENT_STATUS_CR_FK = 88		
--		AND (	
--			   (    ISNULL ( @in_event_fk, -999 ) = -999
--				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
--				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
--				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
--				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
--			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
--			)
--		AND   (   @in_data_name_fk = -999           
--			  OR  ED.data_name_fk = @in_data_name_fk )			
--	) A 



--	------------------------------------------------------------------------------------------
-----  Run ISNULL comparisons here, instead of in monster insert/select where clause SUP-2633
--------------------------------------------------------------------------------------------
--	--AND (	
--	--		   (    ISNULL ( @in_event_fk, -999 ) = -999
--	--			AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
--	--			AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
--	--			AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
--	--			AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
--	--		OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
--	--		)
--	set @in_event_fk = ISNULL(@in_event_fk, -999)
--	set @in_batch_no = ISNULL(@in_batch_no, -999)
--	set @in_import_job_fk = ISNULL(@in_import_job_fk, -999)
--	set @in_event_priority_cr_fk = ISNULL(@in_event_priority_cr_fk, -999)
--	set @in_table_name = ISNULL(@in_table_name, 'NULL DATA')

----------------------------------------------------------------------------------------------
----   EVENT_SHEET RESULTS events
----------------------------------------------------------------------------------------------


--IF @in_event_type_cr_fk = 152

--	INSERT INTO #TS_EVENT_DATA 
--			(   EVENT_FK,
--				EVENT_TYPE_CR_FK,
--				EVENT_ENTITY_CLASS_CR_FK,
--				EPACUBE_ID,
--				DATA_NAME_FK,				
--			    IMPORT_JOB_FK,				
--				PRODUCT_STRUCTURE_FK,
--				ORG_ENTITY_STRUCTURE_FK,
--				ENTITY_CLASS_CR_FK,
--				ENTITY_DATA_NAME_FK,
--				ENTITY_STRUCTURE_FK,				
--				DATA_SET_FK,
--				DATA_TYPE_CR_FK,
--				ORG_IND,
--				TABLE_NAME,
--				NET_VALUE1_NEW,
--				NET_VALUE2_NEW,
--				NET_VALUE3_NEW,
--				NET_VALUE4_NEW,
--				NET_VALUE5_NEW,
--				NET_VALUE6_NEW,														
--				NET_VALUE1_CUR,
--				NET_VALUE2_CUR,
--				NET_VALUE3_CUR,
--				NET_VALUE4_CUR,
--				NET_VALUE5_CUR,
--				NET_VALUE6_CUR,							
--				PERCENT_CHANGE1,
--				PERCENT_CHANGE2,
--				PERCENT_CHANGE3,
--				PERCENT_CHANGE4,
--				PERCENT_CHANGE5,
--				PERCENT_CHANGE6,							
--				VALUE_UOM_CODE_FK,
--				ASSOC_UOM_CODE_FK,
--				VALUES_SHEET_NAME,
--				UOM_CONVERSION_FACTOR,
--				END_DATE_CUR,							
--				END_DATE_NEW,	
--				EFFECTIVE_DATE_CUR,										
--				EVENT_EFFECTIVE_DATE,
--				EVENT_ACTION_CR_FK,
--				EVENT_STATUS_CR_FK,	
--				EVENT_CONDITION_CR_FK,
--				EVENT_SOURCE_CR_FK,
--				EVENT_PRIORITY_CR_FK,
--				RESULT_TYPE_CR_FK,
--				TABLE_ID_FK,
--			    BATCH_NO,
--			    IMPORT_BATCH_NO,
--			    RULES_FK,
--			    RECORD_STATUS_CR_FK,				
--				UPDATE_TIMESTAMP
--				 ) 	
--	   SELECT
--			    A.EVENT_ID,                    
--				A.EVENT_TYPE_CR_FK,
--				A.EVENT_ENTITY_CLASS_CR_FK,
--				A.EPACUBE_ID,
--				A.DATA_NAME_FK,				
--				A.IMPORT_JOB_FK,
--				A.PRODUCT_STRUCTURE_FK,
--				A.ORG_ENTITY_STRUCTURE_FK,
--				A.ENTITY_CLASS_CR_FK,
--				A.ENTITY_DATA_NAME_FK,
--				A.ENTITY_STRUCTURE_FK,
--				A.DATA_SET_FK,
--				A.DATA_TYPE_CR_FK,
--				A.ORG_IND,
--				A.TABLE_NAME,
--				A.NET_VALUE1_NEW,
--				A.NET_VALUE2_NEW,
--				A.NET_VALUE3_NEW,
--				A.NET_VALUE4_NEW,
--				A.NET_VALUE5_NEW,
--				A.NET_VALUE6_NEW,													
--				A.NET_VALUE1,  
--				A.NET_VALUE2,
--				A.NET_VALUE3,
--				A.NET_VALUE4,
--				A.NET_VALUE5,
--				A.NET_VALUE6,						
--				CASE ISNULL (A.NET_VALUE1, 0 )
--				 WHEN 0 THEN 0  
--				 ELSE ( ( A.NET_VALUE1_NEW  - A.NET_VALUE1) / A.NET_VALUE1 )
--				 END,
--				CASE ISNULL (A.NET_VALUE2, 0 )
--				 WHEN 0 THEN 0  
--				 ELSE ( ( A.NET_VALUE2_NEW  - A.NET_VALUE2 ) / A.NET_VALUE2 )
--				 END,
--				CASE ISNULL (A.NET_VALUE3, 0 )
--				 WHEN 0 THEN 0  
--				 ELSE ( ( A.NET_VALUE3_NEW  - A.NET_VALUE3 ) / A.NET_VALUE3 )
--				 END,
--				CASE ISNULL (A.NET_VALUE4, 0 )
--				 WHEN 0 THEN 0  
--				 ELSE ( ( A.NET_VALUE4_NEW  - A.NET_VALUE4 ) / A.NET_VALUE4 )
--				 END,
--				CASE ISNULL (A.NET_VALUE5, 0 )
--				 WHEN 0 THEN 0  
--				 ELSE ( ( A.NET_VALUE5_NEW  - A.NET_VALUE5 ) / A.NET_VALUE5 )
--				 END,
--				CASE ISNULL (A.NET_VALUE6, 0 )
--				 WHEN 0 THEN 0  
--				 ELSE ( ( A.NET_VALUE6_NEW  - A.NET_VALUE6 ) / A.NET_VALUE6 )
--				 END,
--			A.VALUE_UOM_CODE_FK,
--			A.ASSOC_UOM_CODE_FK,
--			A.VALUES_SHEET_NAME,
--			( SELECT CASE ISNULL ( A.VALUE_UOM_CODE_FK, 0 ) 
--			WHEN 0 THEN 1
--			ELSE
--			CASE ISNULL ( A.ASSOC_UOM_CODE_FK, 0 ) 
--			WHEN 0 THEN 1
--			ELSE
--			( ISNULL (
--			( SELECT 
--			( SELECT ISNULL (
--			   (SELECT uc.primary_qty
--				FROM epacube.UOM_CODE uc WITH (NOLOCK)
--				WHERE uc.UOM_CODE_ID = A.VALUE_UOM_CODE_FK  ), 1 )
--			)
--			/
--			ISNULL (   (SELECT uc.primary_qty
--						FROM epacube.UOM_CODE uc WITH (NOLOCK)
--						WHERE uc.UOM_CODE_ID = A.ASSOC_UOM_CODE_FK )
--					 , ISNULL ( (SELECT uc.primary_qty
--					             FROM epacube.UOM_CODE uc WITH (NOLOCK)
--					             WHERE uc.UOM_CODE_ID = A.VALUE_UOM_CODE_FK) , 1 ) 
--					 )
--			) , 1 ) )
--			END END
--			)  AS UOM_CONVERSION_FACTOR,
--				A.END_DATE,
--				A.END_DATE_NEW,	
--				A.EFFECTIVE_DATE,										
--				A.EVENT_EFFECTIVE_DATE,
--				A.EVENT_ACTION_CR_FK,
--				A.EVENT_STATUS_CR_FK,	
--				A.EVENT_CONDITION_CR_FK,
--				A.EVENT_SOURCE_CR_FK,
--				A.EVENT_PRIORITY_CR_FK,
--				A.RESULT_TYPE_CR_FK,
--				A.SHEET_RESULTS_ID,
--			    A.BATCH_NO,
--			    A.IMPORT_BATCH_NO,
--			    A.RULES_FK,
--			    A.RECORD_STATUS_CR_FK,				
--			@L_SYSDATE
----------
--	FROM ( SELECT
--				ED.EVENT_ID,                    
--				DS.EVENT_TYPE_CR_FK,
--				DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
--				ED.PRODUCT_STRUCTURE_FK AS EPACUBE_ID,
--				ED.DATA_NAME_FK,
--				ED.IMPORT_JOB_FK,			
--				ED.PRODUCT_STRUCTURE_FK,
--				CASE ISNULL ( DS.ORG_IND, 0 )
--				WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
--				ELSE ED.ORG_ENTITY_STRUCTURE_FK			
--				END AS ORG_ENTITY_STRUCTURE_FK,
--				DS.ENTITY_STRUCTURE_EC_CR_FK AS ENTITY_CLASS_CR_FK,
--				DN.ENTITY_DATA_NAME_FK,
--				ED.ENTITY_STRUCTURE_FK,																					
--				DN.DATA_SET_FK,
--				DS.DATA_TYPE_CR_FK,
--				DS.ORG_IND,
--				DS.TABLE_NAME,
--				ED.NET_VALUE1_NEW,
--				ED.NET_VALUE2_NEW,
--				ED.NET_VALUE3_NEW,
--				ED.NET_VALUE4_NEW,
--				ED.NET_VALUE5_NEW,
--				ED.NET_VALUE6_NEW,														
--				sr.NET_VALUE1 as NET_VALUE1,  -----CV change
--				sr.NET_VALUE2 as NET_VALUE2,
--				sr.NET_VALUE3 as NET_VALUE3,
--				sr.NET_VALUE4 as NET_VALUE4,
--				sr.NET_VALUE5 as NET_VALUE5,
--				sr.NET_VALUE6 as NET_VALUE6,	
--			ED.VALUE_UOM_CODE_FK,   --- EITHER HAVE IT OR DO NOT .. IF NOT USE ASSOC_UOM_CODE_FK					
--		    ( SELECT TOP 1 PUC.UOM_CODE_FK
--			  FROM epacube.PRODUCT_UOM_CLASS PUC WITH (NOLOCK)
--			  WHERE ( PUC.DATA_NAME_FK = DN.UOM_CLASS_DATA_NAME_FK
--			  AND     PUC.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
--			  AND   (  (   DS.ORG_IND = 1
--					   AND PUC.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK )
--					  OR PUC.ORG_ENTITY_STRUCTURE_FK = 1 ) )
--              --AND ISNULL ( PUC.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ED.ENTITY_STRUCTURE_FK, -999 )   )
--			) AS ASSOC_UOM_CODE_FK,
--			ED.VALUES_SHEET_NAME,
--				dateadd(mi, 40, getdate()) as END_DATE, --SR.END_DATE,
--				ED.END_DATE_NEW,
--				getdate() as EFFECTIVE_DATE,  --SR.EFFECTIVE_DATE,				
--				ED.EVENT_EFFECTIVE_DATE, 
--				CASE WHEN ED.EVENT_ACTION_CR_FK = 59
--				THEN 59
--				ELSE 52								
--				END  AS EVENT_ACTION_CR_FK,
--				ED.EVENT_STATUS_CR_FK,	
--				ED.EVENT_CONDITION_CR_FK,
--				ED.EVENT_SOURCE_CR_FK,
--				ED.EVENT_PRIORITY_CR_FK,
--			    ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ) AS RESULT_TYPE_CR_FK,					
--				SR.SHEET_RESULTS_ID as SHEET_RESULTS_ID,      ---cv change
--			    ED.BATCH_NO,
--			    ED.IMPORT_BATCH_NO,
--			    ED.RULES_FK,
--				ED.RECORD_STATUS_CR_FK    		
--     FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
--		INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
--		 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
--		INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
--		 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
--		LEFT JOIN marginmgr.SHEET_RESULTS_Z SR WITH (NOLOCK)
--		 ON ( SR.DATA_NAME_FK = ED.DATA_NAME_FK
--		-- AND  ISNULL ( SR.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( ED.PRODUCT_STRUCTURE_FK, 0 )
--		and (((sr.product_structure_fk is null) and (ed.PRODUCT_STRUCTURE_FK is null)) or (ed.PRODUCT_STRUCTURE_FK = sr.PRODUCT_STRUCTURE_FK))
--		-- AND  ISNULL ( SR.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
--		and ((sr.product_structure_fk is not null) and (ed.PRODUCT_STRUCTURE_FK is not null) and (SR.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK))
--		-- AND  ISNULL ( SR.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( ED.ENTITY_STRUCTURE_FK, 0 )  
--		and (((SR.ENTITY_STRUCTURE_FK is null) and (ED.ENTITY_STRUCTURE_FK is null)) or (ED.ENTITY_STRUCTURE_FK = SR.ENTITY_STRUCTURE_FK))
--        )							         				 
--		WHERE 1 = 1 
--		AND  EVENT_STATUS_CR_FK = 88		




--		--------------------RG to replace
--		AND (	
--			   (   ((@in_event_fk = -999) or (@in_event_fk = 0))
--				AND 
--				((ED.BATCH_NO = @in_batch_no) or (ED.BATCH_NO = -999))
--				-- ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )

--				AND ((ED.IMPORT_JOB_FK = @in_import_job_fk) or ((ED.IMPORT_JOB_FK is null) and @in_import_job_fk = -999))
--				--ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )				
				
--				AND ((ED.EVENT_PRIORITY_CR_FK = @in_event_priority_cr_fk) or ((ED.EVENT_PRIORITY_CR_FK is null) and @in_import_job_fk = -999))
--				--isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				
--				AND ((ds.TABLE_NAME = @in_table_name) or (ds.TABLE_NAME = 'NULL DATA'))
--				--ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
--				)

--			OR  ED.EVENT_ID = @in_event_fk -- Will be set to -999 above            (@in_event_fk, -999 ) 
--			)
--		---------------------End update






--		AND   (   @in_data_name_fk = -999           
--			  OR  ED.data_name_fk = @in_data_name_fk )			
--	) A 



--------------------------------------------------------------------------------------------
--   EVENT_PRICESHEET_BASIS_VALUES events  #PBV
--------------------------------------------------------------------------------------------
--Some facts:
-- Product Structure FK, Effective Date is Key.
-- Existing Product = A effective Date = 1/1/2014 Value = 3

--if product = A comes in with Effective date =2/2/2014 and value = 3 do not update. No change event. Do not insert
-- if product = A comes in with Effective date =1/1/2014 and value = 5 update. create change event. 
-- If product = A comes in with Effective date= 2/2/2014 and the value = 5. New basis value event. Insert new record.

IF @in_event_type_cr_fk = 158

	INSERT INTO #TS_EVENT_DATA 
			(   EVENT_FK,
				EVENT_TYPE_CR_FK,
				EVENT_ENTITY_CLASS_CR_FK,
				EPACUBE_ID,
				DATA_NAME_FK,				
			    IMPORT_JOB_FK,				
				PRODUCT_STRUCTURE_FK,
				ORG_ENTITY_STRUCTURE_FK,
				CUST_ENTITY_STRUCTURE_FK,
				CO_ENTITY_STRUCTURE_FK,
				ENTITY_CLASS_CR_FK,
				ENTITY_DATA_NAME_FK,
				ENTITY_STRUCTURE_FK,				
				DATA_SET_FK,
				DATA_TYPE_CR_FK,
				ORG_IND,
				TABLE_NAME,
				VALUES_SHEET_NAME,
				VALUES_SHEET_DATE,
				NEW_DATA,
				NET_VALUE1_NEW,
				NET_VALUE2_NEW,
										
				NET_VALUE1_CUR,
				NET_VALUE2_CUR,
					PACK,				
				PERCENT_CHANGE1,
										
				VALUE_UOM_CODE_FK,
				ASSOC_UOM_CODE_FK,
				
				UOM_CONVERSION_FACTOR,
				SEARCH1,
				END_DATE_CUR,							
				END_DATE_NEW,	
				EFFECTIVE_DATE_CUR,										
				EVENT_EFFECTIVE_DATE,
				VALUE_EFFECTIVE_DATE,
				EVENT_ACTION_CR_FK,
				EVENT_STATUS_CR_FK,	
				EVENT_CONDITION_CR_FK,
				EVENT_SOURCE_CR_FK,
				EVENT_PRIORITY_CR_FK,
				RESULT_TYPE_CR_FK,
				TABLE_ID_FK,
			    BATCH_NO,
			    IMPORT_BATCH_NO,
			    RULES_FK,
			    RECORD_STATUS_CR_FK,				
				UPDATE_TIMESTAMP,
				UPDATE_USER

				 ) 	
	   SELECT
			    A.EVENT_ID,                    
				A.EVENT_TYPE_CR_FK,
				A.EVENT_ENTITY_CLASS_CR_FK,
				A.EPACUBE_ID,
				A.DATA_NAME_FK,				
				A.IMPORT_JOB_FK,
				A.PRODUCT_STRUCTURE_FK,
				A.ORG_ENTITY_STRUCTURE_FK,
				A.CUST_ENTITY_STRUCTURE_FK,
				A.CO_ENTITY_STRUCTURE_FK,
				A.ENTITY_CLASS_CR_FK,
				A.ENTITY_DATA_NAME_FK,
				A.ENTITY_STRUCTURE_FK,
				A.DATA_SET_FK,
				A.DATA_TYPE_CR_FK,
				A.ORG_IND,
				A.TABLE_NAME,
				A.VALUES_SHEET_NAME,
				A.VALUES_SHEET_DATE,
			    A.NEW_DATA,
				A.NET_VALUE1_NEW,
					A.NET_VALUE2_NEW,
																	
				A.NET_VALUE1,  
				A.NET_VALUE2_CUR,
				A.PACK,						
				CASE ISNULL (A.NET_VALUE1, 0 )
				 WHEN 0 THEN 0  
				 ELSE ( ( A.NET_VALUE1_NEW  - A.NET_VALUE1) / A.NET_VALUE1 )
				 END,
			
			A.VALUE_UOM_CODE_FK,
			A.ASSOC_UOM_CODE_FK,
		
			a.uom_conversion_factor AS UOM_CONVERSION_FACTOR,
			A.Search1,
				A.END_DATE,
				A.END_DATE_NEW,	
				A.EFFECTIVE_DATE,										
				A.EVENT_EFFECTIVE_DATE,
				A.VALUE_EFFECTIVE_DATE,
				A.EVENT_ACTION_CR_FK,
				A.EVENT_STATUS_CR_FK,	
				A.EVENT_CONDITION_CR_FK,
				A.EVENT_SOURCE_CR_FK,
				A.EVENT_PRIORITY_CR_FK,
				A.RESULT_TYPE_CR_FK,
				A.TABLE_ID_FK,
			    A.BATCH_NO,
			    A.IMPORT_BATCH_NO,
			    A.RULES_FK,
			    A.RECORD_STATUS_CR_FK,				
			@L_SYSDATE,
			A.update_User
--------
	FROM ( SELECT
				ED.EVENT_ID,                    
				DS.EVENT_TYPE_CR_FK,
				DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
				ED.PRODUCT_STRUCTURE_FK AS EPACUBE_ID,
				ED.DATA_NAME_FK,
				ED.IMPORT_JOB_FK,			
				ED.PRODUCT_STRUCTURE_FK,
				CASE ISNULL ( DS.ORG_IND, 0 )
				WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
				ELSE ED.ORG_ENTITY_STRUCTURE_FK			
				END AS ORG_ENTITY_STRUCTURE_FK,
				ED.CUST_ENTITY_STRUCTURE_FK,
				'101' AS CO_ENTITY_STRUCTURE_FK,
				DS.ENTITY_STRUCTURE_EC_CR_FK AS ENTITY_CLASS_CR_FK,
				DN.ENTITY_DATA_NAME_FK,
				ED.ENTITY_STRUCTURE_FK,																					
				DN.DATA_SET_FK,
				DS.DATA_TYPE_CR_FK,
				DS.ORG_IND,
				DS.TABLE_NAME,
				ED.VALUES_SHEET_NAME,
				ED.VALUES_SHEET_DATE,
				ED.NEW_DATA,
			
				ED.NET_VALUE1_NEW,
				ED.NET_VALUE2_NEW,
				ISNULL(ED.NET_VALUE1_CUR, PBV.Value)	as NET_VALUE1,												
				ED.NET_VALUE2_CUR,
				ED.PACK,
			ED.VALUE_UOM_CODE_FK,   --- EITHER HAVE IT OR DO NOT .. IF NOT USE ASSOC_UOM_CODE_FK					
		    ( SELECT TOP 1 PUC.UOM_CODE_FK
			  FROM epacube.PRODUCT_UOM_CLASS PUC WITH (NOLOCK)
			  WHERE ( PUC.DATA_NAME_FK = DN.UOM_CLASS_DATA_NAME_FK
			  AND     PUC.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
			  AND   (  (   DS.ORG_IND = 1
					   AND PUC.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK )
					  OR PUC.ORG_ENTITY_STRUCTURE_FK = 1 ) )
              --AND ISNULL ( PUC.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ED.ENTITY_STRUCTURE_FK, -999 )   )
			) AS ASSOC_UOM_CODE_FK,
			ED.UOM_CONVERSION_FACTOR,
				ED.SEARCH1,
				dateadd(mi, 40, getdate()) as END_DATE, --SR.END_DATE,
				ED.END_DATE_NEW,
				getdate() as EFFECTIVE_DATE,  --SR.EFFECTIVE_DATE,				
				ED.EVENT_EFFECTIVE_DATE, 
				ED.VALUE_EFFECTIVE_DATE,
				CASE WHEN ED.EVENT_ACTION_CR_FK = 59 THEN 59
				WHEN ED.net_value1_new = PBV.value and cast(ED.Value_effective_date as date) <> cast(PBV.effective_date as date) THEN 56  ---no action
				WHEN ED.TABLE_ID_FK IS NULL THEN 51 ELSE 52 END								
				   AS EVENT_ACTION_CR_FK,
				ED.EVENT_STATUS_CR_FK,	
				ED.EVENT_CONDITION_CR_FK,
				ED.EVENT_SOURCE_CR_FK,
				ED.EVENT_PRIORITY_CR_FK,
			    ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ) AS RESULT_TYPE_CR_FK,					
				ISNULL(ED.TABLE_ID_FK, PBV.Pricesheet_Basis_values_ID) as Table_ID_FK,      ---cv change
			    ED.BATCH_NO,
			    ED.IMPORT_BATCH_NO,
			    ED.RULES_FK,
				ED.RECORD_STATUS_CR_FK,
				ED.UPDATE_USER  
				FROM synchronizer.EVENT_DATA ED WITH (NOLOCK)
				 inner join epacube.DATA_NAME_GROUP_REFS R ON R.Data_Name_FK = ED.DATA_NAME_FK
				 left join epacube.code_ref cr on cr.code_type = 'DATA_LEVEL'
				 and isNULL(cr.code,999) = isNULL(ed.Data_level,999)
				 and ISNULL(r.data_level_cr_fk,999) = ISNULL(cr.code_ref_id,999)
	 
		INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
		 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
		INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
		 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		
		LEFT JOIN marginmgr.PRICESHEET_BASIS_VALUES PBV WITH (NOLOCK)
		 ON (ED.DATA_NAME_FK = PBV.DATA_NAME_FK  
			AND ED.PRODUCT_STRUCTURE_FK = PBV.PRODUCT_STRUCTURE_FK 
		AND ISNULL(CAST(ED.VALUE_EFFECTIVE_DATE as DATE),'01/01/1900') = ISNULL(Cast(PBV.Effective_Date AS DATE),'01/01/1900') 
		AND ISNULL(ED.VALUES_SHEET_NAME,'-999')  = ISNULL(PBV.PriceSheetID,'-999') 
		AND ISNULL(ED.ORG_ENTITY_STRUCTURE_FK,'-999') =  ISNULL(PBV.ORG_ENTITY_STRUCTURE_FK,'-999')  
		AND ISNULL(ED.CUST_ENTITY_STRUCTURE_FK,'-999') = ISNULL(PBV.CUST_ENTITY_STRUCTURE_FK,'-999')
		AND ISNULL(ED.ZONE_ENTITY_STRUCTURE_FK,'-999') = ISNULL(PBV.ZONE_ENTITY_STRUCTURE_FK,'-999')
		       )							         				 
		WHERE 1 = 1 
		AND ED.EVENT_STATUS_CR_FK = 88
		AND ED.BATCH_NO = @in_batch_no	
		AND ED.EVENT_PRIORITY_CR_FK = @in_event_priority_cr_fk	

		--------------------RG to replace
		--AND (	
		--	   (   ((@in_event_fk = -999) or (@in_event_fk = 0))
		--		AND 				
		--		((ED.BATCH_NO = @in_batch_no) or (ED.BATCH_NO = -999))
		--			AND ((ED.IMPORT_JOB_FK = @in_import_job_fk) or ((ED.IMPORT_JOB_FK is null) and @in_import_job_fk = -999))
		--			AND ((ED.EVENT_PRIORITY_CR_FK = @in_event_priority_cr_fk) or ((ED.EVENT_PRIORITY_CR_FK is null) and @in_import_job_fk = -999))
		--			--AND ((ds.TABLE_NAME = '#PBV') or (ds.TABLE_NAME = 'NULL DATA'))
		--			)

		--	OR  ED.EVENT_ID = @in_event_fk 
		--	)
		---------------------End update
		--AND   (   @in_data_name_fk = -999           
		--	  OR  ED.data_name_fk = @in_data_name_fk )			
	) A 


	
		--and (((sr.Effective_Date is null) and (ed.VALUE_EFFECTIVE_DATE is null)) or (ed.VALUE_EFFECTIVE_DATE = sr.Effective_Date))
		--and (((sr.value is null) and (ed.NET_VALUE1_NEW is null)) or (ed.NET_VALUE1_NEW  = sr.value))
		--and (((sr.Effective_Date is null) and (ed.VALUES_SHEET_DATE is null)) or (ed.VALUES_SHEET_DATE = sr.Effective_Date))
	--56
	--update  
	--set event_action_cr_fk = no change of value = the same

	 SET @l_sysdate   = getdate()

            SET @V_COUNT = ( SELECT COUNT(1) FROM #TS_EVENT_DATA )

            SET @status_desc =  'synchronizer.EVENT_POSTING_PRODUCT_151_152: COUNT '
                                + CAST  ( @V_COUNT AS VARCHAR (16) )
                                
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
				                             
--------------------------------------------------------------------------------------------
--   CALL VALIDATIONS
--------------------------------------------------------------------------------------------
    
	TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
	TRUNCATE TABLE #TS_EVENT_DATA_RULES	

------------------------------------------------------------------------------------------
--  DELETE ROWS  --- ERRORS ARE REMOVED THEN RE-INSERTED
--					 TIMIMG MUST DO HERE BECAUSE OF CONFLICT AND DUPLICATE VALIDATIONS
--					 DO NOT REMOVE THE EVENT_DATA_RULES  ( ONCE SET FOR AN EVENT.. SET )
------------------------------------------------------------------------------------------
---chaned because of URM
	--DELETE
	--FROM synchronizer.EVENT_DATA_ERRORS
	--WHERE event_fk IN (
	--SELECT EVENT_FK
	--FROM #TS_EVENT_DATA )

-- KS        10/15/2012   EPA-3400 Added Delete for EVENT_DATA_RULES to remove duplicates
	DELETE
	FROM synchronizer.EVENT_DATA_RULES
	WHERE event_fk IN (
	SELECT EVENT_FK
	FROM #TS_EVENT_DATA )



	------------------------------------------------------------------------------------------
	--   QUALIFY GOVERNANCE RULES 
	------------------------------------------------------------------------------------------
	SET  @v_count = ( SELECT COUNT(1)
					 FROM #TS_EVENT_DATA WITH (NOLOCK)
					 WHERE BATCH_NO = @in_batch_no
					 AND   EVENT_STATUS_CR_FK = 88 
					 AND   EVENT_TYPE_CR_FK IN ( 150, 151, 152, 153, 154, 155, 156, 160, 158 )   			 
					 AND   (  EVENT_SOURCE_CR_FK = 71        -- WHEN UI CALLS VALIDATION DIRECTLY WILL REMOVE 71 
						   OR ISNULL ( IMPORT_BATCH_NO, -999 ) = @in_batch_no )    
					 AND   DATA_NAME_FK IN ( SELECT R.RESULT_DATA_NAME_FK   
											 FROM synchronizer.RULES R WITH (NOLOCK)
											 WHERE R.RECORD_STATUS_CR_FK = 1
											 AND  R.RULE_TYPE_CR_FK = 307 )
					OR DATA_NAME_FK IN (SELECT DISTINCT DN.DATA_NAME_ID 
								FROM synchronizer.RULES R WITH (NOLOCK)							
							INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
							ON ( R.RESULT_DATA_NAME_FK = DN.DATA_NAME_id)
							--AND DN.PARENT_DATA_NAME_FK IS NOT NULL)
							WHERE R.RECORD_STATUS_CR_FK = 1
							AND  R.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE )
					)	
					
							                         
	IF @v_count > 0
		EXEC synchronizer.EVENT_POST_QUALIFY_RULES_GOVERNANCE  @in_batch_no

		--URM CINDY TEST
    --EXEC [synchronizer].[EVENT_POST_VALID_PROD_ENTITY] 
	
	EXEC [synchronizer].[EVENT_POST_VALID_TYPE_151_152] 
		    

	--EXEC [synchronizer].[EVENT_POST_VALID_TYPE_ALL] 		    


------------------------------------------------------------------------------------------
-- CALL PROC TO SET EVENT PRIORITY ACTION AND STATUS   ( KEEP IN ONE PLACE WITH CODE )
------------------------------------------------------------------------------------------

	EXEC  [synchronizer].[EVENT_POST_COND_PRIORITY_ACTION_STATUS] @in_batch_no



--------------------------------------------------------------------------------------------
--   CALL POST AND COMPLETE ... added @in_table_lock
--------------------------------------------------------------------------------------------

----	IF @in_post_ind = 1
		EXEC SYNCHRONIZER.EVENT_POST_DATA  



------------------------------------------------------------------------------------------
--  Update synchronizer.EVENT_DATA from #TS_EVENT_DATA  ( POST_COMPLETE )
------------------------------------------------------------------------------------------



UPDATE synchronizer.EVENT_DATA
   SET EVENT_EFFECTIVE_DATE		= TSED.EVENT_EFFECTIVE_DATE
      ,DATA_NAME_FK				= TSED.DATA_NAME_FK
      ,PRODUCT_STRUCTURE_FK		= TSED.PRODUCT_STRUCTURE_FK
      ,ORG_ENTITY_STRUCTURE_FK	= TSED.ORG_ENTITY_STRUCTURE_FK
      ,ENTITY_CLASS_CR_FK		= TSED.ENTITY_CLASS_CR_FK
      ,ENTITY_DATA_NAME_FK		= TSED.ENTITY_DATA_NAME_FK
      ,ENTITY_STRUCTURE_FK		= TSED.ENTITY_STRUCTURE_FK      
      ,NET_VALUE1_NEW		    = TSED.NET_VALUE1_NEW
      ,NET_VALUE2_NEW           = TSED.NET_VALUE2_NEW
      ,NET_VALUE3_NEW           = TSED.NET_VALUE3_NEW
      ,NET_VALUE4_NEW           = TSED.NET_VALUE4_NEW
      ,NET_VALUE5_NEW           = TSED.NET_VALUE5_NEW
      ,NET_VALUE6_NEW           = TSED.NET_VALUE6_NEW
      ,END_DATE_NEW             = TSED.END_DATE_NEW
      ,NET_VALUE1_CUR           = TSED.NET_VALUE1_CUR
      ,NET_VALUE2_CUR           = TSED.NET_VALUE2_CUR
      ,NET_VALUE3_CUR           = TSED.NET_VALUE3_CUR
      ,NET_VALUE4_CUR           = TSED.NET_VALUE4_CUR
      ,NET_VALUE5_CUR           = TSED.NET_VALUE5_CUR
      ,NET_VALUE6_CUR           = TSED.NET_VALUE6_CUR
      ,END_DATE_CUR             = TSED.END_DATE_CUR
      ,PERCENT_CHANGE1          = TSED.PERCENT_CHANGE1
      ,PERCENT_CHANGE2          = TSED.PERCENT_CHANGE2
      ,PERCENT_CHANGE3          = TSED.PERCENT_CHANGE3
      ,PERCENT_CHANGE4          = TSED.PERCENT_CHANGE4
      ,PERCENT_CHANGE5          = TSED.PERCENT_CHANGE5
      ,PERCENT_CHANGE6          = TSED.PERCENT_CHANGE6     
----
      ,EVENT_CONDITION_CR_FK	= TSED.EVENT_CONDITION_CR_FK
      ,EVENT_STATUS_CR_FK		= (CASE WHEN TSED.EVENT_STATUS_CR_FK = 87
										THEN 80
										ELSE TSED.EVENT_STATUS_CR_FK
										END )
      ,EVENT_PRIORITY_CR_FK		= TSED.EVENT_PRIORITY_CR_FK
      ,RESULT_TYPE_CR_FK		= TSED.RESULT_TYPE_CR_FK
      ,EVENT_ACTION_CR_FK		= TSED.EVENT_ACTION_CR_FK
      ,TABLE_ID_FK				= TSED.TABLE_ID_FK
      ,EFFECTIVE_DATE_CUR	    = TSED.EFFECTIVE_DATE_CUR
      ,VALUE_UOM_CODE_FK		= TSED.VALUE_UOM_CODE_FK
      ,ASSOC_UOM_CODE_FK		= TSED.ASSOC_UOM_CODE_FK 
      ,VALUES_SHEET_NAME		= TSED.VALUES_SHEET_NAME
	  ,VALUES_SHEET_DATE		= TSED.VALUES_SHEET_DATE
	  ,SEARCH1					= TSED.SEARCH1
      ,UOM_CONVERSION_FACTOR    = TSED.UOM_CONVERSION_FACTOR   
      ,RECORD_STATUS_CR_FK		= TSED.RECORD_STATUS_CR_FK            
      ,UPDATE_TIMESTAMP			= TSED.UPDATE_TIMESTAMP
FROM #TS_EVENT_DATA  TSED
WHERE TSED.EVENT_FK = synchronizer.EVENT_DATA.EVENT_ID



------------------------------------------------------------------------------------------
--   INSERT INTO synchronizer.EVENT_DATA_ERRORS 
------------------------------------------------------------------------------------------


		INSERT INTO synchronizer.EVENT_DATA_ERRORS 
				(EVENT_FK
			   ,RULE_FK
			   ,EVENT_CONDITION_CR_FK
			   ,DATA_NAME_FK
			   ,ERROR_NAME
			   ,ERROR_DATA1
			   ,ERROR_DATA2
			   ,ERROR_DATA3
			   ,ERROR_DATA4
			   ,ERROR_DATA5
			   ,ERROR_DATA6
			   ,UPDATE_TIMESTAMP)
		   SELECT 
				 TEDE.EVENT_FK
				,TEDE.RULE_FK
				,TEDE.EVENT_CONDITION_CR_FK
				,TEDE.DATA_NAME_FK
				,TEDE.ERROR_NAME
				,TEDE.ERROR_DATA1
				,TEDE.ERROR_DATA2
				,TEDE.ERROR_DATA3
				,TEDE.ERROR_DATA4
				,TEDE.ERROR_DATA5
				,TEDE.ERROR_DATA6
				,TEDE.UPDATE_TIMESTAMP
			FROM #TS_EVENT_DATA_ERRORS TEDE


------------------------------------------------------------------------------------------
--   INSERT INTO synchronizer.EVENT_DATA_RULES 
------------------------------------------------------------------------------------------


	INSERT INTO [synchronizer].[EVENT_DATA_RULES]
	 (   EVENT_FK      		
		,RULES_FK      
		,RESULT_DATA_NAME_FK      		
		,RESULT_TYPE_CR_FK      	
		,RESULT_EFFECTIVE_DATE      	
		,RULES_EFFECTIVE_DATE 
		,RULES_ACTION_OPERATION_FK     
		,RULE_PRECEDENCE
		,SCHED_PRECEDENCE				
		 ) 			
		SELECT DISTINCT 
         EVENT_FK      		
		,RULES_FK      
		,RESULT_DATA_NAME_FK      		
		,RESULT_TYPE_CR_FK      	
		,RESULT_EFFECTIVE_DATE      	
		,RULES_EFFECTIVE_DATE  
		,RULES_ACTION_OPERATION_FK    
		,RULE_PRECEDENCE
		,SCHED_PRECEDENCE			
	FROM #TS_EVENT_DATA_RULES 
	



------------------------------------------------------------------------------------------
---  CLEAR TABLE FOR NEXT BATCH_NO, PRIORITY, JOB_FK, EVENT_TYPE
------------------------------------------------------------------------------------------

   
   TRUNCATE TABLE #TS_EVENT_DATA
   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
   TRUNCATE TABLE #TS_EVENT_DATA_RULES      



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_PRODUCT_151_152'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @in_batch_no
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_PRODUCT_151_152 has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END






























































