﻿








-- Copyright 2007
--
-- Procedure created by Kathi Scott
--
--
------------------------------------------------------------------------------------------
--   This Procedure is currently scheduled to run every hour ( 5 min after ) by QUARTZ
--      to pick up Re-Calculations for the Sheet Engine
--
------------------------------------------------------------------------------------------
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        10/05/2006   Initial SQL Version
--
CREATE PROCEDURE [synchronizer].[HOURLY_JOB]
AS
BEGIN

DECLARE @l_sysdate    datetime
DECLARE @ls_stmt      varchar (1000)
DECLARE @l_exec_no    bigint
DECLARE @v_batch_no   bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_count      int
DECLARE @ep_count     int
DECLARE @epv_count    int
DECLARE @esr_count    int
DECLARE @v_out_job_fk bigint
DECLARE @status_desc  varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

BEGIN TRY
SET NOCOUNT ON
SET @l_sysdate = getdate ()

/*

----------------------------------------------------------------------------------------------------
--   Verify if any other processes are running... If Yes then End Immediately
---------------------------------------------------------------------------------------------------
IF (select count(1) from master.dbo.sysprocesses
	where loginame = 'epacube'
	  and cmd in ('INSERT', 'UPDATE', 'DELETE')
	  and open_tran > 0) > 0
	return

----------------------------------------------------------------------------------------------------
--   Verify if this Job is Already Running... If Yes then End Immediately
--    END OF DAY ROUTINE NEEDS TO REMOVE ANY "UN-FINISHED" JOBS
---------------------------------------------------------------------------------------------------

  SELECT @v_count = count(1)
  FROM COMMON.JOB
  WHERE job_class_fk  = 221
  AND   JOB_COMPLETE_TIMESTAMP IS NULL   -- NULL timestamp means still running


  IF @v_count > 0
  BEGIN

    SET @l_exec_no = 0;
    SET @ls_stmt = 'Could Not Start execution of marginmgr.HOURLY_JOB Already Running a Current Job '

    EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt
											  ,@exec_id = @l_exec_no OUTPUT;

  END

  ELSE
  BEGIN

    SET @l_exec_no = 0;
    SET @ls_stmt = 'started execution of marginmgr.HOURLY_JOB '
    EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt
											  ,@exec_id = @l_exec_no OUTPUT;

-------------------------------------------------------------------------------------
--   RUN SHEET ACTIVITY FOR PRODUCT MANAGER
-------------------------------------------------------------------------------------

   EXEC MARGINMGR.SHEET_ACTIVITY_IMPORT_HOURLY

  END  -- Currently another Job is Running

*/


------------------------------------------------------------------------------------------
--   Finish Execution Log for Job
------------------------------------------------------------------------------------------

	SET @status_desc = 'finished execution of SYNCHRONIZER.HOURLY_JOB'
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
	EXEC exec_monitor.print_status_sp  @l_exec_no;





END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.HOURLY_JOB has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END

















































































