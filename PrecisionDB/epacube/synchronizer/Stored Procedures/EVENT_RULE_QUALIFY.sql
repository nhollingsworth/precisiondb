﻿


-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Find Qualifying Rules for Events
--			Generic Routine call from all Rule Procedures
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------



CREATE PROCEDURE [synchronizer].[EVENT_RULE_QUALIFY]
         

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of synchronizer.EVENT_RULE_QUALIFY' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;
DECLARE @ERP VARCHAR(32)
SET @ERP = ISNULL((SELECT VALUE FROM EPACUBE.EPACUBE.EPACUBE_PARAMS WHERE NAME = 'ERP HOST'), 'SXE')

Declare @InhibitChildInheritance Int
Set @InhibitChildInheritance = isnull((Select value from epacube.epacube_params where name = 'CHILD STORES NOT TO INHERIT PARENT RULE ATTRIBUTES'), 0)

------------------------------------------------------------------------------------------
--   Temporary Table for all Qualified Filters for the Event Data
------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
---GET RULES THAT QUALIFY FROM EVENT  ---
---put them in Qualified rules table
----------------------------------------------------------------------------------------

--select * from synchronizer.rules


TRUNCATE TABLE  #TS_QUALIFIED_RULES

select
	R.RULES_ID
	,R.RESULT_DATA_NAME_FK
	--,R.RULE_TYPE_CR_FK -- RULES_RESULT_TYPE_CR_FK
	,R.EFFECTIVE_DATE
	,R.END_DATE
	,R.RECORD_STATUS_CR_FK			
	,R.RULE_PRECEDENCE	
    ,R.PROMO_IND
    ,R.HOST_RULE_XREF
    ,R.RULES_STRUCTURE_FK
    ,R.RULES_SCHEDULE_FK	
    ,R.RULE_TYPE_CR_FK 
	,R.Prod_Segment_FK
	,R.Prod_Segment_Data_Name_FK
	,R.Vendor_Segment_FK
	,R.Vendor_Segment_Data_Name_FK
	,R.Org_Segment_FK
	,R.Org_Segment_Data_Name_FK
	,R.Cust_Segment_FK
	,R.Cust_Segment_Data_Name_FK    
INTO #RULES
from synchronizer.RULES R WITH (NOLOCK)
where r.record_status_cr_fk = 1
And r.result_data_name_fk in (select result_data_name_fk from #TS_RULE_EVENTS group by result_data_name_fk)

Create Index idx_rls on #rules(rules_id, Result_data_name_fk, RULE_TYPE_CR_FK, prod_segment_fk, org_segment_fk, cust_segment_fk, vendor_segment_fk)

SET @status_desc = 'Ln 1743 Event_Rule_Qualify - Create #Rules'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;




	----Getting products for a customer and rule ??
		--select pa.product_structure_fk from synchronizer.RULES R
		--inner join epacube.SEGMENTS_CUSTOMER SC on sc.CUST_SEGMENT_FK = r.Cust_Segment_FK
		--and sc.DATA_NAME_FK = r.Cust_Segment_Data_Name_FK
		--inner join epacube.PRODUCT_ASSOCIATION pa on pa.ENTITY_STRUCTURE_FK = r.Cust_Segment_FK
		--and pa.DATA_NAME_FK = 159905
		--where r.RECORD_STATUS_CR_FK = 1
		--order by PRODUCT_STRUCTURE_FK asc

	



		--select * from epacube.data_name where DATA_NAME_ID = 159905




INSERT INTO #TS_QUALIFIED_RULES
   ( TS_RULE_EVENTS_FK
    ,TRIGGER_EVENT_FK
	,RULES_FK      
	,RESULT_DATA_NAME_FK      	
	,RESULT_TYPE_CR_FK      		
	,RESULT_EFFECTIVE_DATE  
	--,PROD_DATA_NAME_FK	   
	--,PROD_FILTER_FK      
	--,PROD_RESULT_TYPE_CR_FK      
	--,PROD_EFFECTIVE_DATE      
	--,PROD_EVENT_FK     
	--,PROD_ORG_ENTITY_STRUCTURE_FK
	--,PROD_CORP_IND
	--,PROD_ORG_IND
	--,PROD_FILTER_HIERARCHY
	--,PROD_FILTER_ORG_PRECEDENCE
	--,ORG_DATA_NAME_FK	
	--,ORG_FILTER_FK      
	--,ORG_RESULT_TYPE_CR_FK      	
	--,ORG_EFFECTIVE_DATE      
	--,ORG_EVENT_FK   
	--,CUST_DATA_NAME_FK		
	--,CUST_FILTER_FK   
	--,CUST_FILTER_HIERARCHY  	   
	--,CUST_RESULT_TYPE_CR_FK      	
	--,CUST_EFFECTIVE_DATE      
	--,CUST_EVENT_FK 
	--,RULES_CONTRACT_CUSTOMER_FK     				
	--,SUPL_DATA_NAME_FK		  	
	--,SUPL_FILTER_FK      
	--,SUPL_RESULT_TYPE_CR_FK      
	--,SUPL_EFFECTIVE_DATE      
	--,SUPL_EVENT_FK   
	--,RULES_RESULT_TYPE_CR_FK      	
	--,RULES_EFFECTIVE_DATE      
	--,RULES_END_DATE      	
	--,RULES_RECORD_STATUS_CR_FK 
	--,RELATED_RULES_FK	
	--,PROD_FILTER_ASSOC_DN_FK
	--,ORG_FILTER_ASSOC_DN_FK
	--,CUST_FILTER_ASSOC_DN_FK
	--,SUPL_FILTER_ASSOC_DN_FK
 --   ,CONTRACT_PRECEDENCE 
    ,RULE_PRECEDENCE  
	--,SCHED_PRECEDENCE 
	--,STRUC_PRECEDENCE
 --   ,SCHED_MATRIX_ACTION_CR_FK
 --   ,STRUC_MATRIX_ACTION_CR_FK
 --   ,RESULT_MATRIX_ACTION_CR_FK 
 --   ,PROMOTIONAL_IND 
 --   ,HOST_RULE_XREF
	--,RULES_STRUCTURE_FK
	--,RULES_SCHEDULE_FK    
	,RULE_TYPE_CR_FK
	,PRODUCT_STRUCTURE_FK					
	,ORG_ENTITY_STRUCTURE_FK
	,CUST_ENTITY_STRUCTURE_FK
	,SUPL_ENTITY_STRUCTURE_FK	
	--,CUST_ENTITY_HIERARCHY
   )
SELECT DISTINCT     ---- ADDED BACK TO AVOID DUPLICATES
       TSRE.TS_RULE_EVENTS_ID
      ,TSRE.TRIGGER_EVENT_FK
      ,R.RULES_ID AS RULES_FK
      ,R.RESULT_DATA_NAME_FK
   	  ,TSRE.RESULT_TYPE_CR_FK      	
	  ,TSRE.RESULT_EFFECTIVE_DATE   
--	  ,TSFP.PROD_DATA_NAME_FK
--      ,CASE TSRE.PROD_Segment_FK
--       WHEN -999 THEN NULL
--       ELSE TSRE.PROD_segment_FK
--       END
--      ,TSFP.PROD_RESULT_TYPE_CR_FK
--      ,TSFP.PROD_EFFECTIVE_DATE
--      ,TSFP.PROD_EVENT_FK
--	  ,TSFP.PROD_ORG_ENTITY_STRUCTURE_FK
--	  ,TSFP.PROD_CORP_IND
--	  ,TSFP.PROD_ORG_IND  
--------------------------------------------------------------------------------------------
------   ECLISPE PRODUCT SELL GROUPS ADD PROD_FILTER_HIERARCHY
--      ,TSFP.PROD_FILTER_HIERARCHY
--      ,( SELECT ECT.PRECEDENCE FROM EPACUBE.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
--         WHERE ECT.DATA_NAME_FK = ( SELECT ES.DATA_NAME_FK FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
--                                    WHERE ECT.RECORD_STATUS_CR_FK = 1
--                                    AND   ES.ENTITY_STRUCTURE_ID = TSFP.PROD_ORG_ENTITY_STRUCTURE_FK ) 
--            ) AS PROD_FILTER_ORG_PRECEDENCE                                                  
--	  ,TSFO.ORG_DATA_NAME_FK	   
--      ,CASE TSFO.ORG_FILTER_FK
--       WHEN -999 THEN NULL
--       ELSE TSFO.ORG_FILTER_FK
--       END      
--      ,TSFO.ORG_RESULT_TYPE_CR_FK
--      ,TSFO.ORG_EFFECTIVE_DATE
--      ,TSFO.ORG_EVENT_FK
--	  ,TSFC.CUST_DATA_NAME_FK      
--      ,CASE TSFC.CUST_FILTER_FK
--       WHEN -999 THEN NULL
--       ELSE TSFC.CUST_FILTER_FK
--       END   
--      ,TSFC.CUST_FILTER_HIERARCHY     
--      ,TSFC.CUST_RESULT_TYPE_CR_FK
--      ,TSFC.CUST_EFFECTIVE_DATE
--      ,TSFC.CUST_EVENT_FK
--	  ,TSFC.RULES_CONTRACT_CUSTOMER_FK   			            
--	  ,TSFS.SUPL_DATA_NAME_FK      
--      ,CASE TSFS.SUPL_FILTER_FK
--       WHEN -999 THEN NULL
--       ELSE TSFS.SUPL_FILTER_FK
--       END      
--      ,TSFS.SUPL_RESULT_TYPE_CR_FK
--      ,TSFS.SUPL_EFFECTIVE_DATE
--      ,TSFS.SUPL_EVENT_FK
--      ,R.RESULT_TYPE_CR_FK -- RULES_RESULT_TYPE_CR_FK
--      ,R.EFFECTIVE_DATE AS RULES_EFFECTIVE_DATE
--      ,R.END_DATE AS RULES_END_DATE
--	  ,R.RECORD_STATUS_CR_FK   
--      ,NULL  -- RELATED_RULES_FK  --- USE WITH WHATIF	  
--	  ,R.PROD_FILTER_ASSOC_DN_FK
--	  ,R.ORG_FILTER_ASSOC_DN_FK
--	  ,R.CUST_FILTER_ASSOC_DN_FK
--	  ,R.SUPL_FILTER_ASSOC_DN_FK	 
--		-----	PRECEDENCES
--		    ,CASE (	SELECT ISNULL ( RFC.DATA_NAME_FK, 0 )
--					FROM synchronizer.RULES_FILTER RFC WITH (NOLOCK)  
--					WHERE RFC.RULES_FILTER_ID = TSFC.CUST_FILTER_FK )
--			 WHEN 105000 THEN  ISNULL (
--			               ( SELECT ( (TSFC.CUST_FILTER_HIERARCHY * 1000 ) +  RCC.PRECEDENCE_ADJUSTMENT )
--			                 FROM synchronizer.RULES_CONTRACT_CUSTOMER RCC WITH (NOLOCK)
--			                 WHERE RCC.RULES_CONTRACT_CUSTOMER_ID = TSFC.RULES_CONTRACT_CUSTOMER_FK )
--			               , 9990 )
--			 WHEN 0 THEN 9999
--			 ELSE 9999
--			 END AS CONTRACT_PRECEDENCE
--    ---
			,R.RULE_PRECEDENCE
--			--,R.SCHEDULE_PRECEDENCE     
--			--,R.STRUCTURE_PRECEDENCE     	       
--			--,R.SCHEDULE_MATRIX_ACTION_CR_FK 
--			--,R.STRUCTURE_MATRIX_ACTION_CR_FK 
--			--,R.HIERARCHY_MATRIX_ACTION_CR_FK 						
--	        ,R.PROMO_IND 
--            ,R.HOST_RULE_XREF
--	        ,R.RULES_STRUCTURE_FK
--	        ,R.RULES_SCHEDULE_FK	
	        ,R.RULE_TYPE_CR_FK
			 
	  ,TSRE.PRODUCT_STRUCTURE_FK					
	  ,TSRE.ORG_ENTITY_STRUCTURE_FK
	  ,TSRE.CUST_ENTITY_STRUCTURE_FK
	  ,TSRE.SUPL_ENTITY_STRUCTURE_FK
	  --,ISNULL(TSFC.CUST_FILTER_HIERARCHY, 9) 'CUST_ENTITY_HIERARCHY'
----
FROM #TS_RULE_EVENTS TSRE
INNER JOIN #Rules R WITH (NOLOCK)
	ON R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
	left JOIN epacube.SEGMENTS_PRODUCT SP ON SP.PRODUCT_STRUCTURE_FK = TSRE.product_structure_fk
	left JOIN epacube.SEGMENTS_VENDOR SV ON SV.VENDOR_ENTITY_STRUCTURE_FK = TSRE.SUPL_ENTITY_STRUCTURE_FK
	left Join epacube.SEGMENTS_CUSTOMER SC ON SC.CUST_ENTITY_STRUCTURE_FK = TSRE.Cust_Entity_structure_Fk
	left Join epacube.SEGMENTS_ZONE_ASSOCIATION SZ On SZ.Zone_Entity_Structure_FK = TSRE.Org_entity_structure_fk


	  
--	AND ISNULL ( TSRE.PROD_FILTER_FK, -999 ) = ISNULL ( R.PROD_segment_FK, -999 )
--INNER JOIN #TS_FILTERS_ORG TSFO
--    ON ( TSFO.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID
--    AND  ISNULL ( TSFO.ORG_FILTER_FK, -999 ) = ISNULL ( R.ORG_FILTER_FK, -999 ) )
--INNER JOIN #TS_FILTERS_CUST TSFC
--    ON ( TSFC.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID
--    AND  ISNULL ( TSFC.CUST_FILTER_FK, -999 ) = ISNULL ( R.CUST_FILTER_FK, -999 ) )
--INNER JOIN #TS_FILTERS_SUPL TSFS
--    ON ( TSFS.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID
--    AND  ISNULL ( TSFS.SUPL_FILTER_FK, -999 ) = ISNULL ( R.SUPL_FILTER_FK, -999 ) )
WHERE 1 = 1
AND  R.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK
--------------
----  ORG SPECIFIC PRODUCT ATTRIBUTE VALIDATIONS
--------------
--AND   (  (    ISNULL ( TSFP.PROD_CORP_IND, 0 ) = 1
--         AND  TSFP.PROD_ORG_ENTITY_STRUCTURE_FK = 1 )
--      OR  (    ISNULL ( TSFP.PROD_ORG_IND, 0 ) = 1
--         AND  TSFP.PROD_ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK )
--      )







----INSERT INTO #TS_QUALIFIED_RULES
----   ( TS_RULE_EVENTS_FK
----    ,TRIGGER_EVENT_FK
----	,RULES_FK      
----	,RESULT_DATA_NAME_FK      	
----	,RESULT_TYPE_CR_FK      		
----	,RESULT_EFFECTIVE_DATE  
----	,PROD_DATA_NAME_FK	   
----	,PROD_FILTER_FK      
----	,PROD_RESULT_TYPE_CR_FK      
----	,PROD_EFFECTIVE_DATE      
----	,PROD_EVENT_FK     
----	,PROD_ORG_ENTITY_STRUCTURE_FK
----	,PROD_CORP_IND
----	,PROD_ORG_IND
----	,PROD_FILTER_HIERARCHY
----	,PROD_FILTER_ORG_PRECEDENCE
----	,ORG_DATA_NAME_FK	
----	,ORG_FILTER_FK      
----	,ORG_RESULT_TYPE_CR_FK      	
----	,ORG_EFFECTIVE_DATE      
----	,ORG_EVENT_FK   
----	,CUST_DATA_NAME_FK		
----	,CUST_FILTER_FK   
----	,CUST_FILTER_HIERARCHY  	   
----	,CUST_RESULT_TYPE_CR_FK      	
----	,CUST_EFFECTIVE_DATE      
----	,CUST_EVENT_FK 
----	,RULES_CONTRACT_CUSTOMER_FK     				
----	,SUPL_DATA_NAME_FK		  	
----	,SUPL_FILTER_FK      
----	,SUPL_RESULT_TYPE_CR_FK      
----	,SUPL_EFFECTIVE_DATE      
----	,SUPL_EVENT_FK   
----	,RULES_RESULT_TYPE_CR_FK      	
----	,RULES_EFFECTIVE_DATE      
----	,RULES_END_DATE      	
----	,RULES_RECORD_STATUS_CR_FK 
----	,RELATED_RULES_FK	
----	,PROD_FILTER_ASSOC_DN_FK
----	,ORG_FILTER_ASSOC_DN_FK
----	,CUST_FILTER_ASSOC_DN_FK
----	,SUPL_FILTER_ASSOC_DN_FK
----    ,CONTRACT_PRECEDENCE 
----    ,RULE_PRECEDENCE  
----	,SCHED_PRECEDENCE 
----	,STRUC_PRECEDENCE
----    ,SCHED_MATRIX_ACTION_CR_FK
----    ,STRUC_MATRIX_ACTION_CR_FK
----    ,RESULT_MATRIX_ACTION_CR_FK 
----    ,PROMOTIONAL_IND 
----    ,HOST_RULE_XREF
----	,RULES_STRUCTURE_FK
----	,RULES_SCHEDULE_FK    
----	,RULE_TYPE_CR_FK
----	,PRODUCT_STRUCTURE_FK					
----	,ORG_ENTITY_STRUCTURE_FK
----	,CUST_ENTITY_STRUCTURE_FK
----	,SUPL_ENTITY_STRUCTURE_FK	
----	,CUST_ENTITY_HIERARCHY
----   )
----SELECT DISTINCT     ---- ADDED BACK TO AVOID DUPLICATES
----       TSRE.TS_RULE_EVENTS_ID
----      ,TSRE.TRIGGER_EVENT_FK
----      ,R.RULES_ID AS RULES_FK
----      ,R.RESULT_DATA_NAME_FK
----   	  ,TSRE.RESULT_TYPE_CR_FK      	
----	  ,TSRE.RESULT_EFFECTIVE_DATE   
----	  ,TSFP.PROD_DATA_NAME_FK
----      ,CASE TSFP.PROD_FILTER_FK
----       WHEN -999 THEN NULL
----       ELSE TSFP.PROD_FILTER_FK
----       END
----      ,TSFP.PROD_RESULT_TYPE_CR_FK
----      ,TSFP.PROD_EFFECTIVE_DATE
----      ,TSFP.PROD_EVENT_FK
----	  ,TSFP.PROD_ORG_ENTITY_STRUCTURE_FK
----	  ,TSFP.PROD_CORP_IND
----	  ,TSFP.PROD_ORG_IND  
----------------------------------------------------------------------------------------------
--------   ECLISPE PRODUCT SELL GROUPS ADD PROD_FILTER_HIERARCHY
----      ,TSFP.PROD_FILTER_HIERARCHY
----      ,( SELECT ECT.PRECEDENCE FROM EPACUBE.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
----         WHERE ECT.DATA_NAME_FK = ( SELECT ES.DATA_NAME_FK FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
----                                    WHERE ECT.RECORD_STATUS_CR_FK = 1
----                                    AND   ES.ENTITY_STRUCTURE_ID = TSFP.PROD_ORG_ENTITY_STRUCTURE_FK ) 
----            ) AS PROD_FILTER_ORG_PRECEDENCE                                                  
----	  ,TSFO.ORG_DATA_NAME_FK	   
----      ,CASE TSFO.ORG_FILTER_FK
----       WHEN -999 THEN NULL
----       ELSE TSFO.ORG_FILTER_FK
----       END      
----      ,TSFO.ORG_RESULT_TYPE_CR_FK
----      ,TSFO.ORG_EFFECTIVE_DATE
----      ,TSFO.ORG_EVENT_FK
----	  ,TSFC.CUST_DATA_NAME_FK      
----      ,CASE TSFC.CUST_FILTER_FK
----       WHEN -999 THEN NULL
----       ELSE TSFC.CUST_FILTER_FK
----       END   
----      ,TSFC.CUST_FILTER_HIERARCHY     
----      ,TSFC.CUST_RESULT_TYPE_CR_FK
----      ,TSFC.CUST_EFFECTIVE_DATE
----      ,TSFC.CUST_EVENT_FK
----	  ,TSFC.RULES_CONTRACT_CUSTOMER_FK   			            
----	  ,TSFS.SUPL_DATA_NAME_FK      
----      ,CASE TSFS.SUPL_FILTER_FK
----       WHEN -999 THEN NULL
----       ELSE TSFS.SUPL_FILTER_FK
----       END      
----      ,TSFS.SUPL_RESULT_TYPE_CR_FK
----      ,TSFS.SUPL_EFFECTIVE_DATE
----      ,TSFS.SUPL_EVENT_FK
----      ,R.RULE_RESULT_TYPE_CR_FK -- RULES_RESULT_TYPE_CR_FK
----      ,R.EFFECTIVE_DATE AS RULES_EFFECTIVE_DATE
----      ,R.END_DATE AS RULES_END_DATE
----	  ,R.RECORD_STATUS_CR_FK   
----      ,NULL  -- RELATED_RULES_FK  --- USE WITH WHATIF	  
----	  ,R.PROD_FILTER_ASSOC_DN_FK
----	  ,R.ORG_FILTER_ASSOC_DN_FK
----	  ,R.CUST_FILTER_ASSOC_DN_FK
----	  ,R.SUPL_FILTER_ASSOC_DN_FK	 
----		-----	PRECEDENCES
----		    ,CASE (	SELECT ISNULL ( RFC.DATA_NAME_FK, 0 )
----					FROM synchronizer.RULES_FILTER RFC WITH (NOLOCK)  
----					WHERE RFC.RULES_FILTER_ID = TSFC.CUST_FILTER_FK )
----			 WHEN 105000 THEN  ISNULL (
----			               ( SELECT ( (TSFC.CUST_FILTER_HIERARCHY * 1000 ) +  RCC.PRECEDENCE_ADJUSTMENT )
----			                 FROM synchronizer.RULES_CONTRACT_CUSTOMER RCC WITH (NOLOCK)
----			                 WHERE RCC.RULES_CONTRACT_CUSTOMER_ID = TSFC.RULES_CONTRACT_CUSTOMER_FK )
----			               , 9990 )
----			 WHEN 0 THEN 9999
----			 ELSE 9999
----			 END AS CONTRACT_PRECEDENCE
----    ---
----			,R.RULE_PRECEDENCE
----			,R.SCHEDULE_PRECEDENCE     
----			,R.STRUCTURE_PRECEDENCE     	       
----			,R.SCHEDULE_MATRIX_ACTION_CR_FK 
----			,R.STRUCTURE_MATRIX_ACTION_CR_FK 
----			,R.HIERARCHY_MATRIX_ACTION_CR_FK 						
----	        ,R.PROMO_IND 
----            ,R.HOST_RULE_XREF
----	        ,R.RULES_STRUCTURE_FK
----	        ,R.RULES_SCHEDULE_FK	
----	        ,R.HIERARCHY_RULE_TYPE_CR_FK
----	  ,TSRE.PRODUCT_STRUCTURE_FK					
----	  ,TSRE.ORG_ENTITY_STRUCTURE_FK
----	  ,TSRE.CUST_ENTITY_STRUCTURE_FK
----	  ,TSRE.SUPL_ENTITY_STRUCTURE_FK
----	  ,ISNULL(TSFC.CUST_FILTER_HIERARCHY, 9) 'CUST_ENTITY_HIERARCHY'
--------
----FROM #TS_RULE_EVENTS TSRE
----INNER JOIN #TS_FILTERS_PROD TSFP 
----    ON ( TSFP.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID )
----INNER JOIN #Rules R WITH (NOLOCK)
------   ON R.RULES_ID = RFS.RULES_FK
----	ON R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK     
----	AND ISNULL ( TSFP.PROD_FILTER_FK, -999 ) = ISNULL ( R.PROD_FILTER_FK, -999 )
----INNER JOIN #TS_FILTERS_ORG TSFO
----    ON ( TSFO.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID
----    AND  ISNULL ( TSFO.ORG_FILTER_FK, -999 ) = ISNULL ( R.ORG_FILTER_FK, -999 ) )
----INNER JOIN #TS_FILTERS_CUST TSFC
----    ON ( TSFC.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID
----    AND  ISNULL ( TSFC.CUST_FILTER_FK, -999 ) = ISNULL ( R.CUST_FILTER_FK, -999 ) )
----INNER JOIN #TS_FILTERS_SUPL TSFS
----    ON ( TSFS.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID
----    AND  ISNULL ( TSFS.SUPL_FILTER_FK, -999 ) = ISNULL ( R.SUPL_FILTER_FK, -999 ) )
----WHERE 1 = 1
----AND  R.HIERARCHY_RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK
------------------
--------  ORG SPECIFIC PRODUCT ATTRIBUTE VALIDATIONS
------------------
----AND   (  (    ISNULL ( TSFP.PROD_CORP_IND, 0 ) = 1
----         AND  TSFP.PROD_ORG_ENTITY_STRUCTURE_FK = 1 )
----      OR  (    ISNULL ( TSFP.PROD_ORG_IND, 0 ) = 1
----         AND  TSFP.PROD_ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK )
----      )



SET @status_desc = 'Ln 1923 Event_Rule_Qualify'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

----Check for Duplicates and get rid of all but last one inserted

	Select TS_RULE_EVENTS_FK, [RULES_FK], max(TS_QUALIFIED_RULES_ID) Max_TS_QUALIFIED_RULES_ID 
	into #ecr_dups
	from #TS_QUALIFIED_RULES with (nolock)
	group by TS_RULE_EVENTS_FK, [RULES_FK]
	Having count(*) > 1

	Create index idx_dups on #ecr_dups(TS_RULE_EVENTS_FK, rules_fk)
	
	If (select count(*) from #ecr_dups) > 0
		Delete ecr 
		from #TS_QUALIFIED_RULES ecr
		inner join #ecr_dups dups on ecr.TS_RULE_EVENTS_FK = dups.TS_RULE_EVENTS_FK and ecr.rules_fk = dups.rules_fk
		where ecr.TS_QUALIFIED_RULES_ID <> dups.Max_TS_QUALIFIED_RULES_ID

SET @status_desc = 'Ln 1950 ECR - Remove ' + Cast((select count(*) from #ecr_dups) as varchar(16)) + ' Duplicates'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

-----------------------------------------------------------------------------------------
-- Drop  Temporary Tables created in this procedure...
--	DO NOT DROP #TS_RULE_EVENTS this table will get truncated and droped elsewhere
-----------------------------------------------------------------------------------------


------drop temp tables
IF object_id('tempdb..#Rules') is not null
   drop table #Rules
      
---------------------------------------------------
-- DISQUALIFY BASED ON RESULT TYPE AND DATES
----   need to revisit the future rule logic later
---------------------------------------------------

	UPDATE #TS_QUALIFIED_RULES
	SET DISQUALIFIED_IND = 1
	   ,DISQUALIFIED_COMMENT = 'DISQUALIFY RESULT TYPE DOES NOT COMPLY'
	WHERE 1 = 1
    AND TS_QUALIFIED_RULES_ID IN (
                SELECT TSQR.TS_QUALIFIED_RULES_ID  --- ,EFFECTIVE_DATE, TSRE.TRIGGER_EVENT_FK
				FROM #TS_RULE_EVENTS TSRE
				INNER JOIN #TS_QUALIFIED_RULES TSQR ON TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK 
                WHERE 1 = 1
	            AND  TSQR.RULES_RESULT_TYPE_CR_FK > TSRE.RESULT_TYPE_CR_FK ) 


	UPDATE #TS_QUALIFIED_RULES
	SET DISQUALIFIED_IND = 1
	   ,DISQUALIFIED_COMMENT = 'DISQUALIFY RESULT TYPE DOES NOT COMPLY WITH DATES'
	WHERE 1 = 1
    AND TS_QUALIFIED_RULES_ID IN (
                SELECT TSQR.TS_QUALIFIED_RULES_ID  --- ,EFFECTIVE_DATE, TSRE.TRIGGER_EVENT_FK
				FROM #TS_RULE_EVENTS TSRE
				INNER JOIN #TS_QUALIFIED_RULES TSQR ON TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK 
                WHERE 1 = 1
	            AND  (   TSQR.RULES_EFFECTIVE_DATE IS NOT NULL
		             AND  TSQR.RULES_EFFECTIVE_DATE > TSRE.RESULT_EFFECTIVE_DATE )
                )


	UPDATE #TS_QUALIFIED_RULES
	SET DISQUALIFIED_IND = 1
	   ,DISQUALIFIED_COMMENT = 'DISQUALIFY RESULT TYPE DOES NOT COMPLY WITH DATES'
	WHERE 1 = 1
    AND TS_QUALIFIED_RULES_ID IN (
                SELECT TSQR.TS_QUALIFIED_RULES_ID  --- ,EFFECTIVE_DATE, TSRE.TRIGGER_EVENT_FK
				FROM #TS_RULE_EVENTS TSRE
				INNER JOIN #TS_QUALIFIED_RULES TSQR ON TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK 
                WHERE 1 = 1
				AND ( TSRE.RESULT_TYPE_CR_FK  IN ( 711, 712 )
						AND ISNULL ( TSQR.RULES_END_DATE, '01/01/2999' ) < TSRE.RESULT_EFFECTIVE_DATE ) 
				)
               
SET @status_desc = 'Ln 2001 Event_Rule_Qualify'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

---------------------------------------------------------------------------------
-- DISQUALIFY DUPLICATE RULE for same EVENT
---------------------------------------------------------------------------------

--		UPDATE #TS_QUALIFIED_RULES 
--		SET DISQUALIFIED_IND = 1
--		   ,DISQUALIFIED_COMMENT = 'DISQUALIFY DUPLICATE RULE for the same EVENT'
--        WHERE 1 = 1 
--        AND   TS_QUALIFIED_RULES_ID  IN  (
--			SELECT A.TS_QUALIFIED_RULES_ID
--			FROM (
--                SELECT
--                    TSQR.TS_QUALIFIED_RULES_ID,
--				(	DENSE_RANK() OVER ( PARTITION BY TSQR.TS_RULE_EVENTS_FK, TSQR.RULES_FK
--										ORDER BY	 TSQR.PROD_FILTER_ORG_PRECEDENCE ASC, TSQR.TS_QUALIFIED_RULES_ID DESC ) 
--                     ) AS DRANK
--					FROM #TS_QUALIFIED_RULES TSQR WITH (NOLOCK) 
--					WHERE 1 = 1
--					AND   ISNULL ( DISQUALIFIED_IND, 0 ) <> 1					
--                ) A
--            WHERE A.DRANK > 1 )

--SET @status_desc = 'Ln 2040 Event_Rule_Qualify'
--EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


-----------------------------------------------------------------
---- DISQUALIFY DUPLICATE STRUCTURE and FILTERED RULES
-----------------------------------------------------------------



--		UPDATE #TS_QUALIFIED_RULES 
--		SET DISQUALIFIED_IND = 1
--		   ,DISQUALIFIED_COMMENT = 'DUPLICATE Structure and Filtered RULES'
--        WHERE 1 = 1 
--        AND   TS_QUALIFIED_RULES_ID  IN  (
--			SELECT A.TS_QUALIFIED_RULES_ID
--			FROM (
--                SELECT
--                    TSQR.TS_QUALIFIED_RULES_ID  
--				,(	DENSE_RANK() OVER ( PARTITION BY TSQR.TRIGGER_EVENT_FK, TSQR.STRUC_PRECEDENCE, TSQR.SCHED_PRECEDENCE, TSQR.RULE_PRECEDENCE
--				                                    ,TSQR.PROD_FILTER_FK, TSQR.CUST_FILTER_FK, TSQR.ORG_FILTER_FK, TSQR.SUPL_FILTER_FK
--										ORDER BY	 TSQR.RULES_EFFECTIVE_DATE DESC, TSQR.PROD_FILTER_ORG_PRECEDENCE ASC, TSQR.RULES_FK ASC ) 
--                     ) AS DRANK
--					FROM #TS_QUALIFIED_RULES TSQR 
--					WHERE 1 = 1
--					AND   TSQR.RULE_TYPE_CR_FK IN ( 312, 313 )
--					AND   ISNULL ( DISQUALIFIED_IND, 0 ) <> 1					
--                ) A
--            WHERE A.DRANK > 1 )

--SET @status_desc = 'Ln 2070 Event_Rule_Qualify'
--EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of synchronizer.EVENT_RULE_QUALIFY'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of synchronizer.EVENT_RULE_QUALIFY has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
