﻿






-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Find Qualifying Rules for Events
--			Generic Routine call from all Rule Procedures
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        01/20/2010   Initial SQL Version
-- KS        08/29/2010   Added Product Filter Hierarchy for Eclipse Sell Groups
-- KS        09/27/2011   Added the logic for Future and Whatif Rules to Qualify
-- KS        10/04/2011   Added check for end data for future and whatif
-- KS		 09/07/2012   Need to include pending events for qualifications on filters 
--                           for Rule Types NOT  IN ( 311, 312, 313 )
-- KS        10/04/2012   EPA-3407 PERFORMANCE ENHANCEMENT
--                         Added check for qualifying only rules that are same result dn
--                        ( Note this routine only called from calculations, derivations, govern )
-- KS        10/18/2012   Further Performance Enhancement with SUPL filter to limit Future Events 
--                          to only PRODUCT_ASSOC  Supplier Events
-- KS        11/11/2012   For Performance removed all the Filter Qualfied Views replaced with SQL Queries
--                            Also changed to remove duplicates here before returning to calling Proc
--
-- GHS		01/28/2014	Optimized code to deal with Product Category Filters - Starting line 390
-- GHS		02/02/2014	Added code to accommodate WilsonArt mod to XRef Table [import.import_sxe_pricing_mod_WA]

CREATE PROCEDURE [synchronizer].[Z_EVENT_RULE_QUALIFY_WA] 
         

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of synchronizer.EVENT_RULE_QUALIFY' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;




------------------------------------------------------------------------------------------
--   Temporary Table for all Qualified Filters for the Event Data
------------------------------------------------------------------------------------------



--drop temp tables
IF object_id('tempdb..#TS_FILTERS_PROD') is not null
   drop table #TS_FILTERS_PROD

CREATE TABLE #TS_FILTERS_PROD(
	[TS_FILTERS_PROD_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[TS_RULE_EVENTS_FK] [bigint] NOT NULL,
	[PROD_DATA_NAME_FK] [int] NULL,			
	[PROD_FILTER_FK] [bigint] NULL,	
    [PROD_RESULT_TYPE_CR_FK] [int] NULL,		
    [PROD_EFFECTIVE_DATE] [DATETIME] NULL,
    [PROD_EVENT_FK] [bigint] NULL,
    [PROD_ORG_ENTITY_STRUCTURE_FK] [bigint] NULL,    
    [PROD_CORP_IND] [smallint] NULL,
    [PROD_ORG_IND] [smallint] NULL,
    [PROD_FILTER_HIERARCHY] [smallint] NULL
)


--drop temp tables
IF object_id('tempdb..#TS_FILTERS_ORG') is not null
   drop table #TS_FILTERS_ORG

CREATE TABLE #TS_FILTERS_ORG(
	[TS_FILTERS_ORG_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[TS_RULE_EVENTS_FK] [bigint] NOT NULL,
	[ORG_DATA_NAME_FK] [int] NULL,			
	[ORG_ENTITY_DATA_NAME_FK] [int] NULL,				
	[ORG_FILTER_FK] [bigint] NULL,		
    [ORG_RESULT_TYPE_CR_FK] [int] NULL,
    [ORG_EFFECTIVE_DATE] [DATETIME] NULL,
    [ORG_EVENT_FK] [bigint] NULL		        		        		        			
)


--drop temp tables
IF object_id('tempdb..#TS_FILTERS_CUST') is not null
   drop table #TS_FILTERS_CUST

CREATE TABLE #TS_FILTERS_CUST(
	[TS_FILTERS_CUST_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[TS_RULE_EVENTS_FK] [bigint] NOT NULL,
	[CUST_DATA_NAME_FK] [int] NULL,			
	[CUST_ENTITY_DATA_NAME_FK] [int] NULL,				
	[CUST_FILTER_FK] [bigint] NULL,	
    [CUST_RESULT_TYPE_CR_FK] [int] NULL,
    [CUST_EFFECTIVE_DATE] [DATETIME] NULL,
    [CUST_EVENT_FK] [bigint] NULL,
	[RULES_CONTRACT_CUSTOMER_FK]  [bigint] NULL,
	[CUST_FILTER_HIERARCHY]	[smallint] NULL,
)


--drop temp tables
IF object_id('tempdb..#TS_FILTERS_SUPL') is not null
   drop table #TS_FILTERS_SUPL

CREATE TABLE #TS_FILTERS_SUPL(
	[TS_FILTERS_SUPL_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[TS_RULE_EVENTS_FK] [bigint] NOT NULL,
	[SUPL_DATA_NAME_FK] [int] NULL,			
	[SUPL_ENTITY_DATA_NAME_FK] [int] NULL,				
	[SUPL_FILTER_FK] [bigint] NULL,
    [SUPL_RESULT_TYPE_CR_FK] [int] NULL,
    [SUPL_EFFECTIVE_DATE] [DATETIME] NULL,
    [SUPL_EVENT_FK] [bigint] NULL		        		        		        			    					
)



/**********************************************************************************/
--      Insert Qualified Supplier Filters for each Calc Data Row
--		FOR PERFORMANCE
--		Omit Nulls or ALL Filters will address below separately
--		     Also do not worry about selecting Rules right now.. Just Filters
/**********************************************************************************/


-- SUPLLIER_FILTERS -- ( INCLUDES PARENT ENTITIES )
INSERT INTO #TS_FILTERS_SUPL
           ([TS_RULE_EVENTS_FK]
	       ,[SUPL_DATA_NAME_FK]                      
	       ,[SUPL_ENTITY_DATA_NAME_FK]                      	       
		   ,[SUPL_FILTER_FK] 
		   ,[SUPL_RESULT_TYPE_CR_FK] 
		   ,[SUPL_EFFECTIVE_DATE]   
		   ,[SUPL_EVENT_FK]     		   		   		           		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK       
      ,F.ENTITY_DATA_NAME_FK             
      ,F.RULES_FILTER_ID AS SUPL_FILTER_FK
      ,710  -- CURRENT RESULT TYPE 
      ,NULL
      ,NULL
----
FROM #TS_RULE_EVENTS TSRE
INNER JOIN epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
 ON (  PAS.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK 
 AND   PAS.ENTITY_CLASS_CR_FK = 10103 ) -- SUPPLIERS ONLY
INNER JOIN epacube.ENTITY_IDENTIFICATION EI with (nolock)
       on (  EI.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK )       
INNER JOIN synchronizer.RULES_FILTER F WITH (NOLOCK) 
       on ( F.RECORD_STATUS_CR_FK = 1
       AND  F.DATA_NAME_FK = EI.DATA_NAME_FK
       and  F.VALUE1 = EI.VALUE  )
WHERE 1 = 1
-- KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
AND   F.RULES_FILTER_ID IN (
		SELECT DISTINCT RFS.SUPL_FILTER_FK 
		FROM synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK) 
		INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
		  ON ( R.RULES_ID = RFS.RULES_FK
		  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
		  AND  R.RECORD_STATUS_CR_FK = 1)
		INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
			ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
			AND  RS.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK )      		  
		WHERE RFS.SUPL_FILTER_ASSOC_DN_FK = PAS.DATA_NAME_FK 
		)



---------------------------------------------------
-- Insert Future Supplier Assoc Events
---------------------------------------------------

INSERT INTO #TS_FILTERS_SUPL
           ([TS_RULE_EVENTS_FK]
	       ,[SUPL_DATA_NAME_FK]                      
	       ,[SUPL_ENTITY_DATA_NAME_FK]                      	       
		   ,[SUPL_FILTER_FK] 
		   ,[SUPL_RESULT_TYPE_CR_FK] 
		   ,[SUPL_EFFECTIVE_DATE]   
		   ,[SUPL_EVENT_FK]     		   		   		           		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK       
      ,F.ENTITY_DATA_NAME_FK             
      ,F.RULES_FILTER_ID AS SUPL_FILTER_FK
      ,711  -- FUTURE RESULT TYPE 
      ,ED.EVENT_EFFECTIVE_DATE 
      ,ED.EVENT_ID
----
--------- KS   20121018 -- FURTHER ADJUSTMENTS TO SQL TO LIMIT EVENTS TO PROD ASSOC ONLY
FROM #TS_RULE_EVENTS TSRE
INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
	ON ( ED.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK 
	AND  ED.DATA_NAME_FK IN ( SELECT DNX.DATA_NAME_ID
							  FROM epacube.DATA_NAME DNX
							  INNER JOIN epacube.DATA_SET DSX ON DSX.DATA_SET_ID = DNX.DATA_SET_FK 
							  WHERE DSX.TABLE_NAME = 'PRODUCT_ASSOCIATION'
							  AND   DSX.ENTITY_STRUCTURE_EC_CR_FK = 10103  -- ONLY SUPPLIER ONES
							 )                           
	AND  ED.EVENT_STATUS_CR_FK  IN  ( 80, 84, 85, 86, 87, 88 ) 
	AND  ED.EVENT_EFFECTIVE_DATE <= TSRE.RESULT_EFFECTIVE_DATE )
--------
INNER JOIN epacube.ENTITY_IDENTIFICATION EI with (nolock)
       on (  EI.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK )       
INNER JOIN synchronizer.RULES_FILTER F WITH (NOLOCK) 
       on ( F.RECORD_STATUS_CR_FK = 1
       AND  F.DATA_NAME_FK = EI.DATA_NAME_FK
       and  F.VALUE1 = EI.VALUE  )
WHERE 1 = 1 
AND   TSRE.RULE_TYPE_CR_FK IN ( 311, 312, 313 )
-- KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
AND   F.RULES_FILTER_ID IN (
		SELECT DISTINCT RFS.SUPL_FILTER_FK 
		FROM synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK) 
		INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
		  ON ( R.RULES_ID = RFS.RULES_FK
		  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
		  AND  R.RECORD_STATUS_CR_FK = 1)
		INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
			ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
			AND  RS.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK )      		  		  
		WHERE RFS.SUPL_FILTER_ASSOC_DN_FK = ED.DATA_NAME_FK 
		)



---------------------------------------------------
-- Insert Future Product Events  --- PDM 
-- KS		 09/07/2012   Need to include pending events for qualifications on filters 
--                           for Rule Types NOT  IN ( 311, 312, 313 )
---------------------------------------------------

INSERT INTO #TS_FILTERS_SUPL
           ([TS_RULE_EVENTS_FK]
	       ,[SUPL_DATA_NAME_FK]                      
	       ,[SUPL_ENTITY_DATA_NAME_FK]                      	       
		   ,[SUPL_FILTER_FK] 
		   ,[SUPL_RESULT_TYPE_CR_FK] 
		   ,[SUPL_EFFECTIVE_DATE]   
		   ,[SUPL_EVENT_FK]     		   		   		           		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK       
      ,F.ENTITY_DATA_NAME_FK             
      ,F.RULES_FILTER_ID AS SUPL_FILTER_FK
      ,711  -- CURRENT RESULT TYPE 
      ,ED.EVENT_EFFECTIVE_DATE 
      ,ED.EVENT_ID
----
--
 --------- KS   20121018 -- FURTHER ADJUSTMENTS TO SQL TO LIMIT EVENTS TO PROD ASSOC ONLY
FROM #TS_RULE_EVENTS TSRE
INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
	ON ( ED.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK 
	AND  ED.DATA_NAME_FK IN ( SELECT DNX.DATA_NAME_ID
							  FROM epacube.DATA_NAME DNX
							  INNER JOIN epacube.DATA_SET DSX ON DSX.DATA_SET_ID = DNX.DATA_SET_FK 
							  WHERE DSX.TABLE_NAME = 'PRODUCT_ASSOCIATION'
							  AND   DSX.ENTITY_STRUCTURE_EC_CR_FK = 10103  -- ONLY SUPPLIER ONES
							 )                           
	AND  ED.EVENT_STATUS_CR_FK  IN  ( 80, 84, 85, 86, 87, 88 ) 
	AND  ED.IMPORT_JOB_FK = TSRE.JOB_FK )
--------
INNER JOIN epacube.ENTITY_IDENTIFICATION EI with (nolock)
       on (  EI.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK )       
INNER JOIN synchronizer.RULES_FILTER F WITH (NOLOCK) 
       on ( F.RECORD_STATUS_CR_FK = 1
       AND  F.DATA_NAME_FK = EI.DATA_NAME_FK
       and  F.VALUE1 = EI.VALUE  )
WHERE 1 = 1 
AND   TSRE.RULE_TYPE_CR_FK NOT IN ( 311, 312, 313 )
-- KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
AND   F.RULES_FILTER_ID IN (
		SELECT DISTINCT RFS.SUPL_FILTER_FK 
		FROM synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK) 
		INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
		  ON ( R.RULES_ID = RFS.RULES_FK
		  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
		  AND  R.RECORD_STATUS_CR_FK = 1)
		INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
			ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
			AND  RS.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK )      		  		  
		WHERE RFS.SUPL_FILTER_ASSOC_DN_FK = ED.DATA_NAME_FK 
		)



---------------------------------------------------
-- Insert ALL SUPPLIERS for each row
---------------------------------------------------

INSERT INTO #TS_FILTERS_SUPL
           ([TS_RULE_EVENTS_FK]
	       ,[SUPL_DATA_NAME_FK]                      
	       ,[SUPL_ENTITY_DATA_NAME_FK]                      	       
		   ,[SUPL_FILTER_FK] 
		   ,[SUPL_RESULT_TYPE_CR_FK] 
		   ,[SUPL_EFFECTIVE_DATE]   
		   ,[SUPL_EVENT_FK]     		   		   		           		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,NULL
      ,143099    --- ALL SUPPLIERS
      ,-999 AS SUPL_FILTER_FK
      ,710  -- CURRENT RESULT TYPE 
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
----
FROM #TS_RULE_EVENTS TSRE


---------------------------------------------------
-- create all indexes for  #TS_FILTERS_SUPL
---------------------------------------------------

CREATE NONCLUSTERED INDEX [IDX_TSFP_TSREF_PFF_ITSFPI]
ON #TS_FILTERS_SUPL
(
	[TS_RULE_EVENTS_FK] ASC
   ,[SUPL_FILTER_FK] ASC
)
INCLUDE ( [TS_FILTERS_SUPL_ID])

CREATE CLUSTERED INDEX [IDX_TSFP_TSFPI] 
ON #TS_FILTERS_SUPL (
	[TS_FILTERS_SUPL_ID] ASC
)

---------------------------------------------------
-- end of create all indexes for  #TS_FILTERS_SUPL
---------------------------------------------------



/**********************************************************************************/
--      Insert CURRENT Qualified Product Filters for each Calc Data Row
--		FOR PERFORMANCE
--		Omit Nulls or ALL Filters will address below separately
--		 and removed the reference to the views... straight sql
--
/**********************************************************************************/



-- PRODUCT_FILTERS -- PRODUCT_CATEGORY

IF object_id('tempdb..#TS_prods') is not null
   drop table #TS_prods

If (select count(*) from #TS_RULE_EVENTS where result_data_name_fk in (111602, 111501, 111502, 111401)) > 0
Begin
	select TSRE.TS_Rule_Events_ID, pc.org_entity_structure_fk, pc.product_structure_fk, pc.data_name_fk, pc.data_value_fk, ec.result_data_name_fk, ec.RULE_TYPE_CR_FK, ec.cust_entity_structure_fk, ec.supl_entity_structure_FK 
	into #TS_prods 
	from synchronizer.event_calc ec with (nolock)
	inner join epacube.product_category pc with (nolock) on ec.PRODUCT_STRUCTURE_FK = pc.PRODUCT_STRUCTURE_FK and (ec.ORG_ENTITY_STRUCTURE_FK = pc.ORG_ENTITY_STRUCTURE_FK or pc.ORG_ENTITY_STRUCTURE_FK = 1)
	inner join #TS_Rule_Events TSRE on ec.job_fk = TSRE.job_fk and ec.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK and (pc.ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK or pc.ORG_ENTITY_STRUCTURE_FK = 1)
	and ec.Cust_ENTITY_STRUCTURE_FK = TSRE.Cust_ENTITY_STRUCTURE_FK and ec.Supl_ENTITY_STRUCTURE_FK = TSRE.Supl_ENTITY_STRUCTURE_FK
	group by TSRE.TS_Rule_Events_ID, pc.org_entity_structure_fk, pc.product_structure_fk, pc.data_name_fk, pc.data_value_fk, ec.result_data_name_fk, ec.RULE_TYPE_CR_FK, ec.cust_entity_structure_fk, ec.supl_entity_structure_FK

	create index idx_prods on #TS_prods(TS_Rule_Events_ID, org_entity_structure_fk, product_structure_fk, data_name_fk, data_value_fk, result_data_name_fk, RULE_TYPE_CR_FK)


	INSERT INTO #TS_FILTERS_PROD
			   ([TS_RULE_EVENTS_FK]
			   ,[PROD_DATA_NAME_FK]           
			   ,[PROD_FILTER_FK]         		   
			   ,[PROD_RESULT_TYPE_CR_FK]  
			   ,[PROD_EFFECTIVE_DATE]   
			   ,[PROD_EVENT_FK] 
			   ,[PROD_ORG_ENTITY_STRUCTURE_FK]		   
			   ,[PROD_CORP_IND]
			   ,[PROD_ORG_IND] 	
			   ,[PROD_FILTER_HIERARCHY]	      		   		   
				)
	select p.TS_RULE_EVENTS_ID, P.DATA_NAME_FK, rf.RULES_FILTER_ID Prod_FIlter_FK, 710 'Current Result Type', Null 'Effective_Date', Null 'Event_FK', P.ORG_ENTITY_STRUCTURE_FK, ds.CORP_IND, ds.ORG_IND, 9 'PROD_FILTER_HIERARCHY'
	from #TS_prods P
		inner join epacube.data_value dv with (nolock) on P.DATA_NAME_FK = dv.DATA_NAME_FK and P.DATA_VALUE_FK = dv.DATA_VALUE_ID
		inner join synchronizer.rules_filter rf with (nolock) on P.DATA_NAME_FK = rf.DATA_NAME_FK and dv.value = rf.VALUE1
		inner join epacube.data_name dn with (nolock) on p.data_name_fk = dn.DATA_NAME_ID
		inner join epacube.data_set ds with (nolock) on dn.data_set_fk = ds.DATA_SET_ID
		where rf.rules_filter_id in 
			(select prod_filter_fk from synchronizer.rules_filter_set rfs with (nolock)
				inner join synchronizer.rules r with (nolock) on rfs.rules_fk = r.rules_id and P.RESULT_DATA_NAME_FK = r.RESULT_DATA_NAME_FK)
End
Else
Begin
	INSERT INTO #TS_FILTERS_PROD
			   ([TS_RULE_EVENTS_FK]
			   ,[PROD_DATA_NAME_FK]           
			   ,[PROD_FILTER_FK]         		   
			   ,[PROD_RESULT_TYPE_CR_FK]  
			   ,[PROD_EFFECTIVE_DATE]   
			   ,[PROD_EVENT_FK] 
			   ,[PROD_ORG_ENTITY_STRUCTURE_FK]		   
			   ,[PROD_CORP_IND]
			   ,[PROD_ORG_IND] 	
			   ,[PROD_FILTER_HIERARCHY]	      		   		   
				)
	SELECT DISTINCT
		   TSRE.TS_RULE_EVENTS_ID
		  ,F.DATA_NAME_FK
		  ,RFS.PROD_FILTER_FK
		  ,710  -- CURRENT RESULT TYPE
		  ,NULL -- EFFECTIVE_DATE 
		  ,NULL -- EVENT_FK
		  ,pc.ORG_ENTITY_STRUCTURE_FK
		  ,ds.CORP_IND
		  ,ds.ORG_IND
		  ,9 as PROD_FILTER_HIERARCHY
	----
		FROM epacube.data_set ds WITH (NOLOCK)
		INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )		
		INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
		   on ( f.record_status_cr_fk = 1
		   and  f.entity_class_cr_fk = ds.ENTITY_CLASS_CR_FK
		   and  f.data_name_fk = dn.data_name_id )	
		INNER JOIN epacube.data_value dv WITH (NOLOCK) 
			on ( dv.data_name_fk = f.data_name_fk 
			and  dv.value = f.value1 )
	-----
		INNER JOIN epacube.product_category pc WITH (NOLOCK) 
			on ( dv.data_value_id = pc.DATA_VALUE_FK )	    	
		INNER JOIN 	#TS_RULE_EVENTS TSRE  		 
			 ON ( tsre.PRODUCT_STRUCTURE_FK = pc.PRODUCT_STRUCTURE_FK 
				AND   (  (    ISNULL ( DS.CORP_IND, 0 ) = 1
					 AND  PC.ORG_ENTITY_STRUCTURE_FK = 1 )
				  OR  (    ISNULL ( DS.ORG_IND, 0 ) = 1
					 AND  PC.ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK )
					)
				 )   
	-----
	-- KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
	INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
	  ON ( RFS.PROD_FILTER_FK = F.RULES_FILTER_ID )
	INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
	  ON ( R.RULES_ID = RFS.RULES_FK
	  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
	  AND  R.RECORD_STATUS_CR_FK = 1)
	INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
		ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
		AND  RS.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK )      		  
	-----
	WHERE 1 = 1
		AND   ds.entity_class_cr_fk = 10109
		AND   ds.table_name = 'PRODUCT_CATEGORY'    -- Product Category
		AND   ds.data_type_cr_fk = 134
End




-- PRODUCT_FILTERS -- PRODUCT_CATEGORY   ( PRODUCT-ENTITY CATEGORIES )

INSERT INTO #TS_FILTERS_PROD
           ([TS_RULE_EVENTS_FK]
	       ,[PROD_DATA_NAME_FK]           
		   ,[PROD_FILTER_FK]         		   
		   ,[PROD_RESULT_TYPE_CR_FK]  
		   ,[PROD_EFFECTIVE_DATE]   
		   ,[PROD_EVENT_FK] 
		   ,[PROD_ORG_ENTITY_STRUCTURE_FK]		   
		   ,[PROD_CORP_IND]
		   ,[PROD_ORG_IND] 	
		   ,[PROD_FILTER_HIERARCHY]	      		   		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK
      ,RFS.PROD_FILTER_FK
      ,710  -- CURRENT RESULT TYPE
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
      ,pc.ORG_ENTITY_STRUCTURE_FK
      ,ds.CORP_IND
      ,ds.ORG_IND
      ,9 as PROD_FILTER_HIERARCHY
----
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )		
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.ENTITY_CLASS_CR_FK
       and  f.data_name_fk = dn.data_name_id )	
    INNER JOIN epacube.entity_data_value edv WITH (NOLOCK) 
        on ( edv.data_name_fk = f.data_name_fk 
        and  edv.value = f.value1 )
-----
	INNER JOIN epacube.product_category pc WITH (NOLOCK) 
	    on  ( edv.entity_data_value_id = pc.ENTITY_DATA_VALUE_FK )
    INNER JOIN 	#TS_RULE_EVENTS TSRE  		 
         ON ( tsre.PRODUCT_STRUCTURE_FK = pc.PRODUCT_STRUCTURE_FK )
-----
-- KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( RFS.PROD_FILTER_FK = F.RULES_FILTER_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
  ON ( R.RULES_ID = RFS.RULES_FK
  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
  AND  R.RECORD_STATUS_CR_FK = 1)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
	AND  RS.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK )      		  
-----
WHERE 1 = 1
    AND   ds.entity_class_cr_fk = 10109
	AND   ds.table_name = 'PRODUCT_CATEGORY'    -- Product Category
	AND   ds.data_type_cr_fk = 135





-- PRODUCT_FILTERS -- PRODUCT_MULT_TYPE

INSERT INTO #TS_FILTERS_PROD
           ([TS_RULE_EVENTS_FK]
	       ,[PROD_DATA_NAME_FK]           
		   ,[PROD_FILTER_FK]         		   
		   ,[PROD_RESULT_TYPE_CR_FK]  
		   ,[PROD_EFFECTIVE_DATE]   
		   ,[PROD_EVENT_FK] 
		   ,[PROD_ORG_ENTITY_STRUCTURE_FK]		   
		   ,[PROD_CORP_IND]
		   ,[PROD_ORG_IND] 	
		   ,[PROD_FILTER_HIERARCHY]	      		   		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK
      ,RFS.PROD_FILTER_FK
      ,710  -- CURRENT RESULT TYPE
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
      ,PMT.ORG_ENTITY_STRUCTURE_FK
      ,ds.CORP_IND
      ,ds.ORG_IND
      ,9 as PROD_FILTER_HIERARCHY
----
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )		
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.ENTITY_CLASS_CR_FK
       and  f.data_name_fk = dn.data_name_id )	
    INNER JOIN epacube.data_value dv WITH (NOLOCK) 
        on ( dv.data_name_fk = f.data_name_fk 
        and  dv.value = f.value1 )
-----
	INNER JOIN epacube.PRODUCT_MULT_TYPE PMT WITH (NOLOCK)
	    on  ( dv.data_value_id = pmt.DATA_VALUE_FK )
    INNER JOIN 	#TS_RULE_EVENTS TSRE  		 
         ON ( tsre.PRODUCT_STRUCTURE_FK = pmt.PRODUCT_STRUCTURE_FK )
-----
-- KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( RFS.PROD_FILTER_FK = F.RULES_FILTER_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
  ON ( R.RULES_ID = RFS.RULES_FK
  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
  AND  R.RECORD_STATUS_CR_FK = 1)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
	AND  RS.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK )      		  
-----
WHERE 1 = 1
    AND   ds.entity_class_cr_fk = 10109
	AND   ds.table_name = 'PRODUCT_MULT_TYPE'    -- Product Mult Type
	AND   ds.data_type_cr_fk = 134 	






-- PRODUCT_FILTERS -- PRODUCT_IDENTIFICATION

INSERT INTO #TS_FILTERS_PROD
           ([TS_RULE_EVENTS_FK]
	       ,[PROD_DATA_NAME_FK]           
		   ,[PROD_FILTER_FK]         		   
		   ,[PROD_RESULT_TYPE_CR_FK]  
		   ,[PROD_EFFECTIVE_DATE]   
		   ,[PROD_EVENT_FK] 
		   ,[PROD_ORG_ENTITY_STRUCTURE_FK]		   
		   ,[PROD_CORP_IND]
		   ,[PROD_ORG_IND] 	
		   ,[PROD_FILTER_HIERARCHY]	      		   		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK
      ,RFS.PROD_FILTER_FK
      ,710  -- CURRENT RESULT TYPE
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
      ,pi.ORG_ENTITY_STRUCTURE_FK
      ,ds.CORP_IND
      ,ds.ORG_IND
      ,9 as PROD_FILTER_HIERARCHY
----
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )		
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.ENTITY_CLASS_CR_FK
       and  f.data_name_fk = dn.data_name_id )	
-----
	INNER JOIN epacube.product_identification pi 
                  ON ( pi.data_name_fk = f.data_name_fk
                  AND  pi.value = f.value1 )
     INNER JOIN 	#TS_RULE_EVENTS TSRE  		 
         ON ( tsre.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK )
-----
-- KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( RFS.PROD_FILTER_FK = F.RULES_FILTER_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
  ON ( R.RULES_ID = RFS.RULES_FK
  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
  AND  R.RECORD_STATUS_CR_FK = 1)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
	AND  RS.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK )      		  
-----
WHERE 1 = 1
    AND   ds.entity_class_cr_fk = 10109
	AND   ds.table_name = 'PRODUCT_IDENTIFICATION'   -- Product Identifier ( Global or Entity )

	
	

-- PRODUCT_FILTERS -- PRODUCT_CATEGORY  ----  ECLIPSE HARDCODED 311900 SELL GROUPS TO USE DN.DISPLAY_SEQ

INSERT INTO #TS_FILTERS_PROD
           ([TS_RULE_EVENTS_FK]
	       ,[PROD_DATA_NAME_FK]           
		   ,[PROD_FILTER_FK]         		   
		   ,[PROD_RESULT_TYPE_CR_FK]  
		   ,[PROD_EFFECTIVE_DATE]   
		   ,[PROD_EVENT_FK] 
		   ,[PROD_ORG_ENTITY_STRUCTURE_FK]		   
		   ,[PROD_CORP_IND]
		   ,[PROD_ORG_IND] 	
		   ,[PROD_FILTER_HIERARCHY]	      		   		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK
      ,RFS.PROD_FILTER_FK
      ,710  -- CURRENT RESULT TYPE
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
      ,pc.ORG_ENTITY_STRUCTURE_FK
      ,ds.CORP_IND
      ,ds.ORG_IND
      ,DN.DISPLAY_SEQ AS PROD_FILTER_HIERARCHY
----
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )		
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.ENTITY_CLASS_CR_FK
       and  f.data_name_fk = DN.PARENT_DATA_NAME_FK  )    ---- 311900  ECLIPSE SELL GROUPS
    INNER JOIN epacube.data_value dv WITH (NOLOCK) 
        on ( dv.data_name_fk = dn.data_name_id 
        and  dv.value = f.value1 )
-----
	INNER JOIN epacube.product_category pc WITH (NOLOCK) 
	    on ( dv.data_value_id = pc.DATA_VALUE_FK )	    	
    INNER JOIN 	#TS_RULE_EVENTS TSRE  		 
         ON ( tsre.PRODUCT_STRUCTURE_FK = pc.PRODUCT_STRUCTURE_FK 
			AND   (  (    ISNULL ( DS.CORP_IND, 0 ) = 1
				 AND  PC.ORG_ENTITY_STRUCTURE_FK = 1 )
			  OR  (    ISNULL ( DS.ORG_IND, 0 ) = 1
				 AND  PC.ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK )
				)
			 )   
-----
-- KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( RFS.PROD_FILTER_FK = F.RULES_FILTER_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
  ON ( R.RULES_ID = RFS.RULES_FK
  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
  AND  R.RECORD_STATUS_CR_FK = 1)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
	AND  RS.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK )      		  
-----
WHERE 1 = 1
    AND   ds.entity_class_cr_fk = 10109
	AND   ds.table_name = 'PRODUCT_CATEGORY'    -- Product Category
	AND   ds.data_type_cr_fk = 134
    AND   DN.DATA_NAME_ID IN ( 	311901, 311904,311905, 311906, 311907, 311908, 311909, 311910, 311911 ) 






---------------------------------------------------
-- Insert Future Product Events  --- calculations
---------------------------------------------------

INSERT INTO #TS_FILTERS_PROD
           ([TS_RULE_EVENTS_FK]
	       ,[PROD_DATA_NAME_FK]                      
		   ,[PROD_FILTER_FK]         		   
		   ,[PROD_RESULT_TYPE_CR_FK]  
		   ,[PROD_EFFECTIVE_DATE]   
		   ,[PROD_EVENT_FK]   
		   ,[PROD_ORG_ENTITY_STRUCTURE_FK]		   		   
		   ,[PROD_CORP_IND]
		   ,[PROD_ORG_IND] 	
		   ,[PROD_FILTER_HIERARCHY]	 		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK
      ,F.RULES_FILTER_ID AS PROD_FILTER_FK
      ,711  -- FUTURE RESULT TYPE
      ,ED.EVENT_EFFECTIVE_DATE 
      ,ED.EVENT_ID
      ,ED.ORG_ENTITY_STRUCTURE_FK
      ,DS.CORP_IND
      ,DS.ORG_IND
      ,9   --- HARDCODE PROD_FILTER_HEIRARCHY IF NOT ECLIPSE SELL GROUPS
----
FROM #TS_RULE_EVENTS TSRE
INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
 ON ( ED.EVENT_EFFECTIVE_DATE <= TSRE.RESULT_EFFECTIVE_DATE
 AND  ED.EVENT_STATUS_CR_FK = 84   ---- IN ( 80, 84, 85, 86, 87, 88 ) CHANGED TO FUTURE PENDING APPROVED
 AND  ED.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK )
INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.data_name_fk = ed.data_name_fk
       AND  f.value1 = ed.new_data )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
 ON (DN.DATA_NAME_ID = ED.DATA_NAME_FK )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
 ON (DS.DATA_SET_ID = DN.DATA_SET_FK )
--
-- KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( RFS.PROD_FILTER_FK = F.RULES_FILTER_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
  ON ( R.RULES_ID = RFS.RULES_FK
  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
  AND  R.RECORD_STATUS_CR_FK = 1)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
	AND  RS.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK )      		  
----
WHERE 1 = 1
AND   TSRE.RULE_TYPE_CR_FK IN ( 311, 312, 313 )
AND   TSRE.RESULT_TYPE_CR_FK = 711   ---- FUTURE
        


---------------------------------------------------
-- Insert Future Product Events  --- PDM 
-- KS		 09/07/2012   Need to include pending events for qualifications on filters 
--                           for Rule Types NOT  IN ( 311, 312, 313 )
---------------------------------------------------

INSERT INTO #TS_FILTERS_PROD
           ([TS_RULE_EVENTS_FK]
	       ,[PROD_DATA_NAME_FK]                      
		   ,[PROD_FILTER_FK]         		   
		   ,[PROD_RESULT_TYPE_CR_FK]  
		   ,[PROD_EFFECTIVE_DATE]   
		   ,[PROD_EVENT_FK]   
		   ,[PROD_ORG_ENTITY_STRUCTURE_FK]		   		   
		   ,[PROD_CORP_IND]
		   ,[PROD_ORG_IND] 	
		   ,[PROD_FILTER_HIERARCHY]	 		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK
      ,F.RULES_FILTER_ID AS PROD_FILTER_FK
      ,711  -- CURRENT RESULT TYPE
      ,ED.EVENT_EFFECTIVE_DATE 
      ,ED.EVENT_ID
      ,ED.ORG_ENTITY_STRUCTURE_FK
      ,DS.CORP_IND
      ,DS.ORG_IND
      ,9   --- HARDCODE PROD_FILTER_HEIRARCHY IF NOT ECLIPSE SELL GROUPS
----
FROM #TS_RULE_EVENTS TSRE
INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
 ON ( ED.IMPORT_JOB_FK = TSRE.JOB_FK
 AND  ED.EVENT_STATUS_CR_FK IN ( 80, 84, 85, 86, 87, 88 )
 AND  ED.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK )
INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.data_name_fk = ed.data_name_fk
       AND  f.value1 = ed.new_data )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
 ON (DN.DATA_NAME_ID = ED.DATA_NAME_FK )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
 ON (DS.DATA_SET_ID = DN.DATA_SET_FK )
--
-- KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( RFS.PROD_FILTER_FK = F.RULES_FILTER_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
  ON ( R.RULES_ID = RFS.RULES_FK
  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
  AND  R.RECORD_STATUS_CR_FK = 1)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
	AND  RS.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK )      		    
---- 
WHERE 1 = 1
AND   TSRE.RULE_TYPE_CR_FK NOT IN ( 311, 312, 313 )
        


---------------------------------------------------
-- Insert ALL PRODUCTS for each row
---------------------------------------------------

INSERT INTO #TS_FILTERS_PROD
           ([TS_RULE_EVENTS_FK]
	       ,[PROD_DATA_NAME_FK]                      
		   ,[PROD_FILTER_FK]         		   
		   ,[PROD_RESULT_TYPE_CR_FK]  
		   ,[PROD_EFFECTIVE_DATE]   
		   ,[PROD_EVENT_FK]   
		   ,[PROD_ORG_ENTITY_STRUCTURE_FK]		   		   
		   ,[PROD_CORP_IND]
		   ,[PROD_ORG_IND] 	
		   ,[PROD_FILTER_HIERARCHY]	 		   		   	      		   		   		    		   		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,149099      --  ALL PRODUCTS
      ,-999 AS PROD_FILTER_FK
      ,710  -- CURRENT RESULT TYPE
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
      ,1    -- CORP
      ,1    -- PROD_CORP_IND
      ,NULL
      ,9   --- HARDCODE PROD_FILTER_HEIRARCHY IF NOT ECLIPSE SELL GROUPS      
----
FROM #TS_RULE_EVENTS TSRE



---------------------------------------------------
-- create all indexes for  #TS_FILTERS_PROD
---------------------------------------------------

CREATE NONCLUSTERED INDEX [IDX_TSFP_TSREF_PFF_ITSFPI]
ON #TS_FILTERS_PROD
(
	[TS_RULE_EVENTS_FK] ASC
   ,[PROD_FILTER_FK] ASC
)
INCLUDE ( [TS_FILTERS_PROD_ID])

CREATE CLUSTERED INDEX [IDX_TSFP_TSFPI] 
ON #TS_FILTERS_PROD (
	[TS_FILTERS_PROD_ID] ASC
)

---------------------------------------------------
-- end of create all indexes for  #TS_FILTERS_PROD
---------------------------------------------------



/**********************************************************************************/
--      Insert Qualified Customer Filters for each Calc Data Row
--		FOR PERFORMANCE
--		Omit Nulls or ALL Filters will address below separately
--		     Replaced View with actual SQL for performance
/**********************************************************************************/


-- CUSTOMER_FILTERS -- ENTITY_CATAGORY   SALES CUST_ENTITY_STRUCTURE_FK
INSERT INTO #TS_FILTERS_CUST
           ([TS_RULE_EVENTS_FK]
	       ,[CUST_DATA_NAME_FK]                      
	       ,[CUST_ENTITY_DATA_NAME_FK]                      	       
		   ,[CUST_FILTER_FK] 
		   ,[CUST_RESULT_TYPE_CR_FK]
		   ,[CUST_EFFECTIVE_DATE]    
		   ,[CUST_EVENT_FK] 
		   ,[RULES_CONTRACT_CUSTOMER_FK]
		   ,[CUST_FILTER_HIERARCHY]    		   		   		           		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK       
      ,F.ENTITY_DATA_NAME_FK             
      ,RFS.CUST_FILTER_FK
      ,710  -- CURRENT RESULT TYPE    
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
      ,NULL -- RULES_CONTRACT_CUSTOMER_FK
      ,9 as ENTITY_FILTER_HIERARCHY
----
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )		
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.ENTITY_CLASS_CR_FK
       and  f.data_name_fk = dn.data_name_id  )  
    INNER JOIN epacube.data_value dv WITH (NOLOCK) 
        on ( dv.data_name_fk = f.data_name_fk 
        and  dv.value = f.value1 )
-----
	INNER JOIN epacube.entity_category ec WITH (NOLOCK)  
        on ( dv.DATA_VALUE_ID = ec.DATA_VALUE_FK )	       
    INNER JOIN 	#TS_RULE_EVENTS TSRE  		 
       ON  ( EC.ENTITY_STRUCTURE_FK = TSRE.CUST_ENTITY_STRUCTURE_FK )
-----
-- KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( RFS.CUST_FILTER_FK = F.RULES_FILTER_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
  ON ( R.RULES_ID = RFS.RULES_FK
  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
  AND  R.RECORD_STATUS_CR_FK = 1)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
	AND  RS.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK )      		  
-----
WHERE 1 = 1
    AND   ds.entity_class_cr_fk = 10104
	AND   ds.table_name = 'ENTITY_CATEGORY'    -- Customer Category
	AND   ds.data_type_cr_fk = 134




-- CUSTOMER_CATEGORY FILTERS -- ENTITY_CATEGORIES  PARENT CUSTOMER ENTITY
INSERT INTO #TS_FILTERS_CUST
           ([TS_RULE_EVENTS_FK]
	       ,[CUST_DATA_NAME_FK]                      
	       ,[CUST_ENTITY_DATA_NAME_FK]                      	       
		   ,[CUST_FILTER_FK] 
		   ,[CUST_RESULT_TYPE_CR_FK]
		   ,[CUST_EFFECTIVE_DATE]    
		   ,[CUST_EVENT_FK] 
		   ,[RULES_CONTRACT_CUSTOMER_FK]
		   ,[CUST_FILTER_HIERARCHY]    		   		   		           		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK       
      ,F.ENTITY_DATA_NAME_FK             
      ,RFS.CUST_FILTER_FK
      ,710  -- CURRENT RESULT TYPE    
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
      ,NULL -- RULES_CONTRACT_CUSTOMER_FK
      ,9 as ENTITY_FILTER_HIERARCHY
----
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )		
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.ENTITY_CLASS_CR_FK
       and  f.data_name_fk = dn.data_name_id  )  
    INNER JOIN epacube.data_value dv WITH (NOLOCK) 
        on ( dv.data_name_fk = f.data_name_fk 
        and  dv.value = f.value1 )
-----
	INNER JOIN epacube.entity_category ec WITH (NOLOCK)  
        on ( dv.DATA_VALUE_ID = ec.DATA_VALUE_FK )	       
	INNER JOIN epacube.entity_structure es WITH (NOLOCK)
	              ON ( es.parent_entity_structure_fk = ec.entity_structure_fk )		-- category is parent's										   
    INNER JOIN 	#TS_RULE_EVENTS TSRE  		 
       ON  ( TSRE.CUST_ENTITY_STRUCTURE_FK = es.entity_structure_id )  -- Event is Child
-----
-- KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( RFS.CUST_FILTER_FK = F.RULES_FILTER_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
  ON ( R.RULES_ID = RFS.RULES_FK
  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
  AND  R.RECORD_STATUS_CR_FK = 1)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
	AND  RS.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK )      		  
-----
WHERE 1 = 1
    AND   ds.entity_class_cr_fk = 10104
	AND   ds.table_name = 'ENTITY_CATEGORY'    -- Customer Category
	AND   ds.data_type_cr_fk = 134



-----*****-----WilsonArt Alternative CPT

		IF object_id('tempdb..#mod_CPT') is not null
		   drop table #mod_CPT

		select WA.cust_entity_structure_fk, WA.CPT 
		into #mod_CPT
		from import.import_sxe_pricing_mod_WA WA with (nolock)
		inner join epacube.entity_structure es with (nolock) on es.data_name_fk in (144020) and wa.Cust_Entity_Structure_FK = es.PARENT_ENTITY_STRUCTURE_FK
		inner join synchronizer.event_calc ec with (nolock) on es.ENTITY_STRUCTURE_ID = ec.CUST_ENTITY_STRUCTURE_FK
		group by WA.cust_entity_structure_fk, WA.CPT

		create index idx_cpt on #mod_CPT(cust_entity_structure_fk, CPT)

		Insert into #mod_CPT (cust_entity_structure_fk, CPT)
		select WA.cust_entity_structure_fk, WA.CPT 
		from import.import_sxe_pricing_mod_WA WA with (nolock)
		inner join epacube.entity_structure es with (nolock) on es.data_name_fk in (144010) and wa.Cust_Entity_Structure_FK = es.ENTITY_STRUCTURE_ID
		inner join synchronizer.event_calc ec with (nolock) on es.ENTITY_STRUCTURE_ID = ec.CUST_ENTITY_STRUCTURE_FK
		group by WA.cust_entity_structure_fk, WA.CPT


--Add the WilsonArt-specific CPT Filters

		-- CUSTOMER_FILTERS -- ENTITY_MULT_TYPE   SALES CUST_ENTITY_STRUCTURE_FK
		INSERT INTO #TS_FILTERS_CUST
				   ([TS_RULE_EVENTS_FK]
				   ,[CUST_DATA_NAME_FK]                      
				   ,[CUST_ENTITY_DATA_NAME_FK]                      	       
				   ,[CUST_FILTER_FK] 
				   ,[CUST_RESULT_TYPE_CR_FK]
				   ,[CUST_EFFECTIVE_DATE]    
				   ,[CUST_EVENT_FK] 
				   ,[RULES_CONTRACT_CUSTOMER_FK]
				   ,[CUST_FILTER_HIERARCHY]    		   		   		           		   
					)
		SELECT DISTINCT
			   TSRE.TS_RULE_EVENTS_ID
			  ,F.DATA_NAME_FK       
			  ,F.ENTITY_DATA_NAME_FK             
			  ,F.RULES_FILTER_ID 
			  ,710  -- CURRENT RESULT TYPE    
			  ,NULL -- EFFECTIVE_DATE 
			  ,NULL -- EVENT_FK
			  ,NULL -- RULES_CONTRACT_CUSTOMER_FK
			  ,9 as ENTITY_FILTER_HIERARCHY
		----
			FROM epacube.data_set ds WITH (NOLOCK)
			INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )		
			INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
			   on ( f.record_status_cr_fk = 1
			   and  f.entity_class_cr_fk = ds.ENTITY_CLASS_CR_FK
			   and  f.data_name_fk = dn.data_name_id  )  
		--    INNER JOIN epacube.data_value dv WITH (NOLOCK) 
		--        on ( dv.data_name_fk = f.data_name_fk 
		--        and  dv.value = f.value1 )
		-------
		--	INNER JOIN epacube.entity_mult_type emt WITH (NOLOCK)  
		--        on ( dv.DATA_VALUE_ID = emt.DATA_VALUE_FK )
	
			INNER JOIN #mod_CPT CPT on f.VALUE1 = CPT.CPT
	       
			INNER JOIN 	#TS_RULE_EVENTS TSRE  		 
			   ON  ( TSRE.CUST_ENTITY_STRUCTURE_FK = CPT.cust_entity_structure_fk ) 
	
			Left Join #TS_FILTERS_CUST TFC on F.RULES_FILTER_ID = TFC.[CUST_FILTER_FK] and tsre.TS_RULE_EVENTS_ID = TFC.TS_RULE_EVENTS_FK
		WHERE 1 = 1
			AND   ds.entity_class_cr_fk = 10104
			AND   ds.table_name = 'ENTITY_MULT_TYPE'    -- ENTITY MULT TYPE
			AND   ds.data_type_cr_fk = 134
			and	  TFC.[CUST_FILTER_FK] is null

		-- CUSTOMER_CATEGORY FILTERS -- ENTITY_MULT_TYPES  PARENT CUSTOMER ENTITY
		INSERT INTO #TS_FILTERS_CUST
				   ([TS_RULE_EVENTS_FK]
				   ,[CUST_DATA_NAME_FK]                      
				   ,[CUST_ENTITY_DATA_NAME_FK]                      	       
				   ,[CUST_FILTER_FK] 
				   ,[CUST_RESULT_TYPE_CR_FK]
				   ,[CUST_EFFECTIVE_DATE]    
				   ,[CUST_EVENT_FK] 
				   ,[RULES_CONTRACT_CUSTOMER_FK]
				   ,[CUST_FILTER_HIERARCHY]    		   		   		           		   
					)
		SELECT DISTINCT
			   TSRE.TS_RULE_EVENTS_ID
			  ,F.DATA_NAME_FK       
			  ,F.ENTITY_DATA_NAME_FK             
			  ,F.RULES_FILTER_ID 
			  ,710  -- CURRENT RESULT TYPE    
			  ,NULL -- EFFECTIVE_DATE 
			  ,NULL -- EVENT_FK
			  ,NULL -- RULES_CONTRACT_CUSTOMER_FK
			  ,9 as ENTITY_FILTER_HIERARCHY
		----
			FROM epacube.data_set ds WITH (NOLOCK)
			INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )		
			INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
			   on ( f.record_status_cr_fk = 1
			   and  f.entity_class_cr_fk = ds.ENTITY_CLASS_CR_FK
			   and  f.data_name_fk = dn.data_name_id  )  

			INNER JOIN #mod_CPT CPT on f.VALUE1 = CPT.CPT

			INNER JOIN epacube.entity_structure es WITH (NOLOCK)
						  ON ( es.parent_entity_structure_fk = CPT.cust_entity_structure_fk )		-- category is parent's										   
			INNER JOIN 	#TS_RULE_EVENTS TSRE  		 
			   ON  ( TSRE.CUST_ENTITY_STRUCTURE_FK = es.entity_structure_id )  -- Event is Child

			Left Join #TS_FILTERS_CUST TFC on F.RULES_FILTER_ID = TFC.[CUST_FILTER_FK] and tsre.TS_RULE_EVENTS_ID = TFC.TS_RULE_EVENTS_FK

		WHERE 1 = 1
			AND   ds.entity_class_cr_fk = 10104
			AND   ds.table_name = 'ENTITY_MULT_TYPE'    -- ENTITY MULT TYPE
			AND   ds.data_type_cr_fk = 134
			and	  TFC.[CUST_FILTER_FK] is null


---*****---





-- CUSTOMER_ FILTERS -- ( CONTRACTS -- INCLUDES PARENT ENTITIES )
INSERT INTO #TS_FILTERS_CUST
           ([TS_RULE_EVENTS_FK]
	       ,[CUST_DATA_NAME_FK]                      
	       ,[CUST_ENTITY_DATA_NAME_FK]                      	       
		   ,[CUST_FILTER_FK] 
		   ,[CUST_RESULT_TYPE_CR_FK]
		   ,[CUST_EFFECTIVE_DATE]    
		   ,[CUST_EVENT_FK] 
		   ,[RULES_CONTRACT_CUSTOMER_FK]
		   ,[CUST_FILTER_HIERARCHY]    		   		   		           		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK       
      ,F.ENTITY_DATA_NAME_FK             
      ,F.RULES_FILTER_FK AS CUST_FILTER_FK
      ,710  -- CURRENT RESULT TYPE    
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
      ,F.RULES_CONTRACT_CUSTOMER_FK
      ,F.ENTITY_FILTER_HIERARCHY
----
FROM #TS_RULE_EVENTS TSRE
INNER JOIN synchronizer.V_RULE_FILTERS_CONTRACTS F 
  ON ( F.ENTITY_STRUCTURE_FK = TSRE.CUST_ENTITY_STRUCTURE_FK
  AND  F.ENTITY_CLASS_CR_FK = 10104 )
----  
WHERE 1 = 1



-- CUSTOMER_FILTERS -- ( IDENTIFICATION  -- INCLUDES PARENT ENTITIES )
INSERT INTO #TS_FILTERS_CUST
           ([TS_RULE_EVENTS_FK]
	       ,[CUST_DATA_NAME_FK]                      
	       ,[CUST_ENTITY_DATA_NAME_FK]                      	       
		   ,[CUST_FILTER_FK] 
		   ,[CUST_RESULT_TYPE_CR_FK]
		   ,[CUST_EFFECTIVE_DATE]    
		   ,[CUST_EVENT_FK] 
		   ,[RULES_CONTRACT_CUSTOMER_FK]
		   ,[CUST_FILTER_HIERARCHY]    		   		   		           		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK       
      ,F.ENTITY_DATA_NAME_FK             
      ,F.RULES_FILTER_FK AS CUST_FILTER_FK
      ,710  -- CURRENT RESULT TYPE    
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
      ,F.RULES_CONTRACT_CUSTOMER_FK
      ,F.ENTITY_FILTER_HIERARCHY
----
FROM #TS_RULE_EVENTS TSRE
INNER JOIN synchronizer.V_RULE_FILTERS_ENTITY_CUST F 
  ON ( F.ENTITY_STRUCTURE_FK = TSRE.CUST_ENTITY_STRUCTURE_FK )
----  
WHERE 1 = 1




---------------------------------------------------
-- Insert ALL CUSTOMERS for each row
---------------------------------------------------

INSERT INTO #TS_FILTERS_CUST
           ([TS_RULE_EVENTS_FK]
	       ,[CUST_DATA_NAME_FK]                      
	       ,[CUST_ENTITY_DATA_NAME_FK]                      	       
		   ,[CUST_FILTER_FK] 
		   ,[CUST_RESULT_TYPE_CR_FK]
		   ,[CUST_EFFECTIVE_DATE]    
		   ,[CUST_EVENT_FK]     		   		   		           		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID 
      ,NULL
      ,144099      -- ALL CUSTOMERS
      ,-999 AS CUST_FILTER_FK
      ,710  -- CURRENT RESULT TYPE    
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
----
FROM #TS_RULE_EVENTS TSRE


---------------------------------------------------
-- create all indexes for  #TS_FILTERS_CUST
---------------------------------------------------

CREATE NONCLUSTERED INDEX [IDX_TSFP_TSREF_PFF_ITSFPI]
ON #TS_FILTERS_CUST
(
	[TS_RULE_EVENTS_FK] ASC
   ,[CUST_FILTER_FK] ASC
)
INCLUDE ( [TS_FILTERS_CUST_ID])

CREATE CLUSTERED INDEX [IDX_TSFP_TSFPI] 
ON #TS_FILTERS_CUST (
	[TS_FILTERS_CUST_ID] ASC
)

---------------------------------------------------
-- end of create all indexes for  #TS_FILTERS_CUST
---------------------------------------------------




/**********************************************************************************/
--      Insert Qualified Warehouse Filters for each Calc Data Row
--		FOR PERFORMANCE
--		Omit Nulls or ALL Filters will address below separately
--		     Also do not worry about selecting Rules right now.. Just Filters
/**********************************************************************************/





-- ORGANIZATION_FILTERS -- ( INCLUDES PARENT ENTITIES )
INSERT INTO #TS_FILTERS_ORG
           ([TS_RULE_EVENTS_FK]
	       ,[ORG_DATA_NAME_FK]                      
	       ,[ORG_ENTITY_DATA_NAME_FK]                      	       
		   ,[ORG_FILTER_FK]      
		   ,[ORG_RESULT_TYPE_CR_FK]  
		   ,[ORG_EFFECTIVE_DATE]    
		   ,[ORG_EVENT_FK]   		   		   		      		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK   
      ,F.ENTITY_DATA_NAME_FK    
      ,F.RULES_FILTER_FK AS ORG_FILTER_FK
      ,710  -- CURRENT RESULT TYPE   
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
----
FROM #TS_RULE_EVENTS TSRE
INNER JOIN synchronizer.V_RULE_FILTERS_ENTITY_ORG F 
  ON ( F.ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK )
--
--- code below actually made it run slower..
------------ KS 10/04/2012 ADDED TO REDUCE QUALIFIED FILTERS BY INCLUDING ONLY THOSE RULES WITH SAME RESULT DATA NAME  
----------INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
----------  ON ( RFS.ORG_FILTER_FK = F.RULES_FILTER_FK )
----------INNER JOIN synchronizer.RULES R WITH (NOLOCK)  
----------  ON ( R.RULES_ID = RFS.RULES_FK
----------  AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
----------  AND  R.RECORD_STATUS_CR_FK = 1)
------------  
WHERE 1 = 1




---------------------------------------------------
-- Insert ALL WAREHOUSES for each row
---------------------------------------------------

INSERT INTO #TS_FILTERS_ORG
           ([TS_RULE_EVENTS_FK]
	       ,[ORG_DATA_NAME_FK]                      
	       ,[ORG_ENTITY_DATA_NAME_FK]                      	       
		   ,[ORG_FILTER_FK]      
		   ,[ORG_RESULT_TYPE_CR_FK]  
		   ,[ORG_EFFECTIVE_DATE]    
		   ,[ORG_EVENT_FK]   		   		   		      		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,NULL
      ,141099           --- ALL ORGS      
      ,-999 AS ORG_FILTER_FK
      ,710  -- CURRENT RESULT TYPE   
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
----
FROM #TS_RULE_EVENTS TSRE



---------------------------------------------------
-- create all indexes for  #TS_FILTERS_ORG
---------------------------------------------------

CREATE NONCLUSTERED INDEX [IDX_TSFP_TSREF_PFF_ITSFPI]
ON #TS_FILTERS_ORG
(
	[TS_RULE_EVENTS_FK] ASC
   ,[ORG_FILTER_FK] ASC
)
INCLUDE ( [TS_FILTERS_ORG_ID])

CREATE CLUSTERED INDEX [IDX_TSFP_TSFPI] 
ON #TS_FILTERS_ORG (
	[TS_FILTERS_ORG_ID] ASC
)

---------------------------------------------------
-- end of create all indexes for  #TS_FILTERS_ORG
---------------------------------------------------




/**********************************************************************************/
--  Select Qualifing Rules from Rules which Intersect or qualify for ALL filters
--		Verify Filters, Result Data Name, Effective Date and Record Status as well
--      Include Basis DN1 and Basis DN2... 
--            ???  and OPERAND, OPERATION_ACTION ??? do later for now.. revist 
/**********************************************************************************/

--- IF NO RESULT_TYPE SPECIFIED DEFAULT TO CURRENT
update #TS_RULE_EVENTS
set RESULT_TYPE_CR_FK = 710
where RESULT_TYPE_CR_FK IS NULL

--WilsonArt Mod - Begin

--- IF NO RESULT_TYPE SPECIFIED DEFAULT TO CURRENT

insert into dbo.maui_log Select 1530, 'Load TS_Qualified_Rules_1 Begin', GetDate(), Null, Null

update #TS_RULE_EVENTS
set RESULT_TYPE_CR_FK = 710
where RESULT_TYPE_CR_FK IS NULL

TRUNCATE TABLE  #TS_QUALIFIED_RULES

INSERT INTO #TS_QUALIFIED_RULES
   ( TS_RULE_EVENTS_FK
    ,TRIGGER_EVENT_FK
	,RULES_FK      
	,RESULT_DATA_NAME_FK      	
	,RESULT_TYPE_CR_FK      		
	,RESULT_EFFECTIVE_DATE  
	,PROD_DATA_NAME_FK	   
	,PROD_FILTER_FK      
	,PROD_RESULT_TYPE_CR_FK      
	,PROD_EFFECTIVE_DATE      
	,PROD_EVENT_FK     
	,PROD_ORG_ENTITY_STRUCTURE_FK
	,PROD_CORP_IND
	,PROD_ORG_IND
	,PROD_FILTER_HIERARCHY
	,PROD_FILTER_ORG_PRECEDENCE
	,ORG_DATA_NAME_FK	
	,ORG_FILTER_FK      
	,ORG_RESULT_TYPE_CR_FK      	
	,ORG_EFFECTIVE_DATE      
	,ORG_EVENT_FK   
	,CUST_DATA_NAME_FK		
	,CUST_FILTER_FK   
	,CUST_FILTER_HIERARCHY  	   
	,CUST_RESULT_TYPE_CR_FK      	
	,CUST_EFFECTIVE_DATE      
	,CUST_EVENT_FK 
	,RULES_CONTRACT_CUSTOMER_FK     				
	,SUPL_DATA_NAME_FK		  	
	,SUPL_FILTER_FK      
	,SUPL_RESULT_TYPE_CR_FK      
	,SUPL_EFFECTIVE_DATE      
	,SUPL_EVENT_FK   
	,RULES_RESULT_TYPE_CR_FK      	
	,RULES_EFFECTIVE_DATE      
	,RULES_END_DATE      	
	,RULES_RECORD_STATUS_CR_FK 
	,RELATED_RULES_FK	
	,PROD_FILTER_ASSOC_DN_FK
	,ORG_FILTER_ASSOC_DN_FK
	,CUST_FILTER_ASSOC_DN_FK
	,SUPL_FILTER_ASSOC_DN_FK
    ,CONTRACT_PRECEDENCE 
    ,RULE_PRECEDENCE  
	,SCHED_PRECEDENCE 
	,STRUC_PRECEDENCE
    ,SCHED_MATRIX_ACTION_CR_FK
    ,STRUC_MATRIX_ACTION_CR_FK
    ,RESULT_MATRIX_ACTION_CR_FK 
    ,PROMOTIONAL_IND 
    ,HOST_RULE_XREF
	,RULES_STRUCTURE_FK
	,RULES_SCHEDULE_FK    
	,RULE_TYPE_CR_FK
	,PRODUCT_STRUCTURE_FK					
	,ORG_ENTITY_STRUCTURE_FK
	,CUST_ENTITY_STRUCTURE_FK
	,SUPL_ENTITY_STRUCTURE_FK	
   )
SELECT DISTINCT     ---- ADDED BACK TO AVOID DUPLICATES
       TSRE.TS_RULE_EVENTS_ID
      ,TSRE.TRIGGER_EVENT_FK
      ,R.RULES_ID AS RULES_FK
      ,R.RESULT_DATA_NAME_FK
   	  ,TSRE.RESULT_TYPE_CR_FK      	
	  ,TSRE.RESULT_EFFECTIVE_DATE   
	  ,TSFP.PROD_DATA_NAME_FK
      ,CASE TSFP.PROD_FILTER_FK
       WHEN -999 THEN NULL
       ELSE TSFP.PROD_FILTER_FK
       END
      ,TSFP.PROD_RESULT_TYPE_CR_FK
      ,TSFP.PROD_EFFECTIVE_DATE
      ,TSFP.PROD_EVENT_FK
	  ,TSFP.PROD_ORG_ENTITY_STRUCTURE_FK
	  ,TSFP.PROD_CORP_IND
	  ,TSFP.PROD_ORG_IND  
------------------------------------------------------------------------------------------
----   ECLISPE PRODUCT SELL GROUPS ADD PROD_FILTER_HIERARCHY
      ,TSFP.PROD_FILTER_HIERARCHY
      ,( SELECT ECT.PRECEDENCE FROM EPACUBE.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
         WHERE ECT.DATA_NAME_FK = ( SELECT ES.DATA_NAME_FK FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
                                    WHERE ECT.RECORD_STATUS_CR_FK = 1
                                    AND   ES.ENTITY_STRUCTURE_ID = TSFP.PROD_ORG_ENTITY_STRUCTURE_FK ) 
            ) AS PROD_FILTER_ORG_PRECEDENCE                                                  
	  ,TSFO.ORG_DATA_NAME_FK	   
      ,CASE TSFO.ORG_FILTER_FK
       WHEN -999 THEN NULL
       ELSE TSFO.ORG_FILTER_FK
       END      
      ,TSFO.ORG_RESULT_TYPE_CR_FK
      ,TSFO.ORG_EFFECTIVE_DATE
      ,TSFO.ORG_EVENT_FK
	  ,TSFC.CUST_DATA_NAME_FK      
      ,CASE TSFC.CUST_FILTER_FK
       WHEN -999 THEN NULL
       ELSE TSFC.CUST_FILTER_FK
       END   
      ,TSFC.CUST_FILTER_HIERARCHY     
      ,TSFC.CUST_RESULT_TYPE_CR_FK
      ,TSFC.CUST_EFFECTIVE_DATE
      ,TSFC.CUST_EVENT_FK
	  ,TSFC.RULES_CONTRACT_CUSTOMER_FK   			            
	  ,TSFS.SUPL_DATA_NAME_FK      
      ,CASE TSFS.SUPL_FILTER_FK
       WHEN -999 THEN NULL
       ELSE TSFS.SUPL_FILTER_FK
       END      
      ,TSFS.SUPL_RESULT_TYPE_CR_FK
      ,TSFS.SUPL_EFFECTIVE_DATE
      ,TSFS.SUPL_EVENT_FK
      ,R.RULE_RESULT_TYPE_CR_FK -- RULES_RESULT_TYPE_CR_FK
      ,R.EFFECTIVE_DATE AS RULES_EFFECTIVE_DATE
      ,R.END_DATE AS RULES_END_DATE
	  ,R.RECORD_STATUS_CR_FK   
      ,NULL  -- RELATED_RULES_FK  --- USE WITH WHATIF	  
	  ,RFS.PROD_FILTER_ASSOC_DN_FK
	  ,RFS.ORG_FILTER_ASSOC_DN_FK
	  ,RFS.CUST_FILTER_ASSOC_DN_FK
	  ,RFS.SUPL_FILTER_ASSOC_DN_FK	 
		-----	PRECEDENCES
		    ,CASE (	SELECT ISNULL ( RFC.DATA_NAME_FK, 0 )
					FROM synchronizer.RULES_FILTER RFC WITH (NOLOCK)  
					WHERE RFC.RULES_FILTER_ID = TSFC.CUST_FILTER_FK )
			 WHEN 105000 THEN  ISNULL (
			               ( SELECT ( (TSFC.CUST_FILTER_HIERARCHY * 1000 ) +  RCC.PRECEDENCE_ADJUSTMENT )
			                 FROM synchronizer.RULES_CONTRACT_CUSTOMER RCC WITH (NOLOCK)
			                 WHERE RCC.RULES_CONTRACT_CUSTOMER_ID = TSFC.RULES_CONTRACT_CUSTOMER_FK )
			               , 9990 )
			 WHEN 0 THEN 9999
			 ELSE 9999
			 END AS CONTRACT_PRECEDENCE
    ---
			,R.RULE_PRECEDENCE
			,RSCH.SCHEDULE_PRECEDENCE     
			,RS.PRECEDENCE     	       
			,RSCH.MATRIX_ACTION_CR_FK 
			,RS.MATRIX_ACTION_CR_FK 
			,RH.MATRIX_ACTION_CR_FK 						
	        ,RSCH.PROMOTIONAL_IND 
            ,R.HOST_RULE_XREF
	        ,R.RULES_STRUCTURE_FK
	        ,R.RULES_SCHEDULE_FK	
	        ,RH.RULE_TYPE_CR_FK        
	  ,TSRE.PRODUCT_STRUCTURE_FK					
	  ,TSRE.ORG_ENTITY_STRUCTURE_FK
	  ,TSRE.CUST_ENTITY_STRUCTURE_FK
	  ,TSRE.SUPL_ENTITY_STRUCTURE_FK	
----
FROM #TS_RULE_EVENTS TSRE
INNER JOIN #TS_FILTERS_PROD TSFP 
    ON ( TSFP.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID )
INNER JOIN synchronizer.RULES_FILTER_SET RFS  WITH (NOLOCK) 
    ON ( ISNULL ( TSFP.PROD_FILTER_FK, -999 ) = ISNULL ( RFS.PROD_FILTER_FK, -999 ) )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)
   ON ( R.RULES_ID = RFS.RULES_FK
   AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
   AND  R.RECORD_STATUS_CR_FK = 1 )
--------------
INNER JOIN #TS_FILTERS_ORG TSFO
    ON ( TSFO.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID
    AND  ISNULL ( TSFO.ORG_FILTER_FK, -999 ) = ISNULL ( RFS.ORG_FILTER_FK, -999 ) )
INNER JOIN #TS_FILTERS_CUST TSFC
    ON ( TSFC.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID
    AND  ISNULL ( TSFC.CUST_FILTER_FK, -999 ) = ISNULL ( RFS.CUST_FILTER_FK, -999 ) )
INNER JOIN #TS_FILTERS_SUPL TSFS
    ON ( TSFS.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID
    AND  ISNULL ( TSFS.SUPL_FILTER_FK, -999 ) = ISNULL ( RFS.SUPL_FILTER_FK, -999 ) )
--------------    
INNER JOIN SYNCHRONIZER.RULES_STRUCTURE RS 
   ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
INNER JOIN SYNCHRONIZER.RULES_SCHEDULE RSCH 
   ON ( RSCH.RULES_SCHEDULE_ID = R.RULES_SCHEDULE_FK )
INNER JOIN SYNCHRONIZER.RULES_HIERARCHY RH
   ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK )

-----*****-----WilsonArt Mod
Inner Join import.import_sxe_pricing_mod_WA WA on TSFC.CUST_FILTER_FK = WA.Cust_Filter_FK and TSFP.PROD_FILTER_FK = WA.Prod_Filter_FK
-----*****-----WilsonArt Mod

WHERE 1 = 1
AND   RH.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK
--------------
----  ORG SPECIFIC PRODUCT ATTRIBUTE VALIDATIONS
--------------
AND   (  (    ISNULL ( TSFP.PROD_CORP_IND, 0 ) = 1
         AND  TSFP.PROD_ORG_ENTITY_STRUCTURE_FK = 1 )
      OR  (    ISNULL ( TSFP.PROD_ORG_IND, 0 ) = 1
         AND  TSFP.PROD_ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK )
      )

insert into dbo.maui_log Select 1530, 'Load TS_Qualified_Rules_1 Complete', GetDate(), Null, Null

-----*****-----Compensate for WilsonArt Mod

--Add the Standard CPT filters that do not conflict with those WilsonArt-specific CPT filters already loaded

-- CUSTOMER_FILTERS -- ENTITY_MULT_TYPE   SALES CUST_ENTITY_STRUCTURE_FK
INSERT INTO #TS_FILTERS_CUST
           ([TS_RULE_EVENTS_FK]
	       ,[CUST_DATA_NAME_FK]                      
	       ,[CUST_ENTITY_DATA_NAME_FK]                      	       
		   ,[CUST_FILTER_FK] 
		   ,[CUST_RESULT_TYPE_CR_FK]
		   ,[CUST_EFFECTIVE_DATE]    
		   ,[CUST_EVENT_FK] 
		   ,[RULES_CONTRACT_CUSTOMER_FK]
		   ,[CUST_FILTER_HIERARCHY]    		   		   		           		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK       
      ,F.ENTITY_DATA_NAME_FK             
      ,F.RULES_FILTER_ID 
      ,710  -- CURRENT RESULT TYPE    
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
      ,NULL -- RULES_CONTRACT_CUSTOMER_FK
      ,9 as ENTITY_FILTER_HIERARCHY
----
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )		
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.ENTITY_CLASS_CR_FK
       and  f.data_name_fk = dn.data_name_id  )  
    INNER JOIN epacube.data_value dv WITH (NOLOCK) 
        on ( dv.data_name_fk = f.data_name_fk 
        and  dv.value = f.value1 )
-----
	INNER JOIN epacube.entity_mult_type emt WITH (NOLOCK)  
        on ( dv.DATA_VALUE_ID = emt.DATA_VALUE_FK )	       
    INNER JOIN 	#TS_RULE_EVENTS TSRE  		 
       ON  ( TSRE.CUST_ENTITY_STRUCTURE_FK = emt.entity_structure_fk ) 
	Left Join #TS_FILTERS_CUST TFC on F.RULES_FILTER_ID = TFC.[CUST_FILTER_FK] and tsre.TS_RULE_EVENTS_ID = TFC.TS_RULE_EVENTS_FK
WHERE 1 = 1
    AND   ds.entity_class_cr_fk = 10104
	AND   ds.table_name = 'ENTITY_MULT_TYPE'    -- ENTITY MULT TYPE
	AND   ds.data_type_cr_fk = 134
	and	  TFC.[CUST_FILTER_FK] is null




-- CUSTOMER_CATEGORY FILTERS -- ENTITY_MULT_TYPES  PARENT CUSTOMER ENTITY
INSERT INTO #TS_FILTERS_CUST
           ([TS_RULE_EVENTS_FK]
	       ,[CUST_DATA_NAME_FK]                      
	       ,[CUST_ENTITY_DATA_NAME_FK]                      	       
		   ,[CUST_FILTER_FK] 
		   ,[CUST_RESULT_TYPE_CR_FK]
		   ,[CUST_EFFECTIVE_DATE]    
		   ,[CUST_EVENT_FK] 
		   ,[RULES_CONTRACT_CUSTOMER_FK]
		   ,[CUST_FILTER_HIERARCHY]    		   		   		           		   
            )
SELECT DISTINCT
       TSRE.TS_RULE_EVENTS_ID
      ,F.DATA_NAME_FK       
      ,F.ENTITY_DATA_NAME_FK             
      ,F.RULES_FILTER_ID 
      ,710  -- CURRENT RESULT TYPE    
      ,NULL -- EFFECTIVE_DATE 
      ,NULL -- EVENT_FK
      ,NULL -- RULES_CONTRACT_CUSTOMER_FK
      ,9 as ENTITY_FILTER_HIERARCHY
----
	FROM epacube.data_set ds WITH (NOLOCK)
	INNER JOIN epacube.data_name dn WITH (NOLOCK) on ( ds.data_set_id = dn.data_set_fk )		
	INNER JOIN synchronizer.rules_filter f WITH (NOLOCK) 
       on ( f.record_status_cr_fk = 1
       and  f.entity_class_cr_fk = ds.ENTITY_CLASS_CR_FK
       and  f.data_name_fk = dn.data_name_id  )  
    INNER JOIN epacube.data_value dv WITH (NOLOCK) 
        on ( dv.data_name_fk = f.data_name_fk 
        and  dv.value = f.value1 )
-----
	INNER JOIN epacube.entity_mult_type emt WITH (NOLOCK)  
        on ( dv.DATA_VALUE_ID = emt.DATA_VALUE_FK )	       
	INNER JOIN epacube.entity_structure es WITH (NOLOCK)
	              ON ( es.parent_entity_structure_fk = emt.entity_structure_fk )		-- category is parent's										   
    INNER JOIN 	#TS_RULE_EVENTS TSRE  		 
       ON  ( TSRE.CUST_ENTITY_STRUCTURE_FK = es.entity_structure_id )  -- Event is Child
	Left Join #TS_FILTERS_CUST TFC on F.RULES_FILTER_ID = TFC.[CUST_FILTER_FK] and tsre.TS_RULE_EVENTS_ID = TFC.TS_RULE_EVENTS_FK
WHERE 1 = 1
    AND   ds.entity_class_cr_fk = 10104
	AND   ds.table_name = 'ENTITY_MULT_TYPE'    -- ENTITY MULT TYPE
	AND   ds.data_type_cr_fk = 134
	and	  TFC.[CUST_FILTER_FK] is null


INSERT INTO #TS_QUALIFIED_RULES
   ( TS_RULE_EVENTS_FK
    ,TRIGGER_EVENT_FK
	,RULES_FK      
	,RESULT_DATA_NAME_FK      	
	,RESULT_TYPE_CR_FK      		
	,RESULT_EFFECTIVE_DATE  
	,PROD_DATA_NAME_FK	   
	,PROD_FILTER_FK      
	,PROD_RESULT_TYPE_CR_FK      
	,PROD_EFFECTIVE_DATE      
	,PROD_EVENT_FK     
	,PROD_ORG_ENTITY_STRUCTURE_FK
	,PROD_CORP_IND
	,PROD_ORG_IND
	,PROD_FILTER_HIERARCHY
	,PROD_FILTER_ORG_PRECEDENCE
	,ORG_DATA_NAME_FK	
	,ORG_FILTER_FK      
	,ORG_RESULT_TYPE_CR_FK      	
	,ORG_EFFECTIVE_DATE      
	,ORG_EVENT_FK   
	,CUST_DATA_NAME_FK		
	,CUST_FILTER_FK   
	,CUST_FILTER_HIERARCHY  	   
	,CUST_RESULT_TYPE_CR_FK      	
	,CUST_EFFECTIVE_DATE      
	,CUST_EVENT_FK 
	,RULES_CONTRACT_CUSTOMER_FK     				
	,SUPL_DATA_NAME_FK		  	
	,SUPL_FILTER_FK      
	,SUPL_RESULT_TYPE_CR_FK      
	,SUPL_EFFECTIVE_DATE      
	,SUPL_EVENT_FK   
	,RULES_RESULT_TYPE_CR_FK      	
	,RULES_EFFECTIVE_DATE      
	,RULES_END_DATE      	
	,RULES_RECORD_STATUS_CR_FK 
	,RELATED_RULES_FK	
	,PROD_FILTER_ASSOC_DN_FK
	,ORG_FILTER_ASSOC_DN_FK
	,CUST_FILTER_ASSOC_DN_FK
	,SUPL_FILTER_ASSOC_DN_FK
    ,CONTRACT_PRECEDENCE 
    ,RULE_PRECEDENCE  
	,SCHED_PRECEDENCE 
	,STRUC_PRECEDENCE
    ,SCHED_MATRIX_ACTION_CR_FK
    ,STRUC_MATRIX_ACTION_CR_FK
    ,RESULT_MATRIX_ACTION_CR_FK 
    ,PROMOTIONAL_IND 
    ,HOST_RULE_XREF
	,RULES_STRUCTURE_FK
	,RULES_SCHEDULE_FK    
	,RULE_TYPE_CR_FK
	,PRODUCT_STRUCTURE_FK					
	,ORG_ENTITY_STRUCTURE_FK
	,CUST_ENTITY_STRUCTURE_FK
	,SUPL_ENTITY_STRUCTURE_FK	
   )
SELECT DISTINCT     ---- ADDED BACK TO AVOID DUPLICATES
       TSRE.TS_RULE_EVENTS_ID
      ,TSRE.TRIGGER_EVENT_FK
      ,R.RULES_ID AS RULES_FK
      ,R.RESULT_DATA_NAME_FK
   	  ,TSRE.RESULT_TYPE_CR_FK      	
	  ,TSRE.RESULT_EFFECTIVE_DATE   
	  ,TSFP.PROD_DATA_NAME_FK
      ,CASE TSFP.PROD_FILTER_FK
       WHEN -999 THEN NULL
       ELSE TSFP.PROD_FILTER_FK
       END
      ,TSFP.PROD_RESULT_TYPE_CR_FK
      ,TSFP.PROD_EFFECTIVE_DATE
      ,TSFP.PROD_EVENT_FK
	  ,TSFP.PROD_ORG_ENTITY_STRUCTURE_FK
	  ,TSFP.PROD_CORP_IND
	  ,TSFP.PROD_ORG_IND  
------------------------------------------------------------------------------------------
----   ECLISPE PRODUCT SELL GROUPS ADD PROD_FILTER_HIERARCHY
      ,TSFP.PROD_FILTER_HIERARCHY
      ,( SELECT ECT.PRECEDENCE FROM EPACUBE.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
         WHERE ECT.DATA_NAME_FK = ( SELECT ES.DATA_NAME_FK FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
                                    WHERE ECT.RECORD_STATUS_CR_FK = 1
                                    AND   ES.ENTITY_STRUCTURE_ID = TSFP.PROD_ORG_ENTITY_STRUCTURE_FK ) 
            ) AS PROD_FILTER_ORG_PRECEDENCE                                                  
	  ,TSFO.ORG_DATA_NAME_FK	   
      ,CASE TSFO.ORG_FILTER_FK
       WHEN -999 THEN NULL
       ELSE TSFO.ORG_FILTER_FK
       END      
      ,TSFO.ORG_RESULT_TYPE_CR_FK
      ,TSFO.ORG_EFFECTIVE_DATE
      ,TSFO.ORG_EVENT_FK
	  ,TSFC.CUST_DATA_NAME_FK      
      ,CASE TSFC.CUST_FILTER_FK
       WHEN -999 THEN NULL
       ELSE TSFC.CUST_FILTER_FK
       END   
      ,TSFC.CUST_FILTER_HIERARCHY     
      ,TSFC.CUST_RESULT_TYPE_CR_FK
      ,TSFC.CUST_EFFECTIVE_DATE
      ,TSFC.CUST_EVENT_FK
	  ,TSFC.RULES_CONTRACT_CUSTOMER_FK   			            
	  ,TSFS.SUPL_DATA_NAME_FK      
      ,CASE TSFS.SUPL_FILTER_FK
       WHEN -999 THEN NULL
       ELSE TSFS.SUPL_FILTER_FK
       END      
      ,TSFS.SUPL_RESULT_TYPE_CR_FK
      ,TSFS.SUPL_EFFECTIVE_DATE
      ,TSFS.SUPL_EVENT_FK
      ,R.RULE_RESULT_TYPE_CR_FK -- RULES_RESULT_TYPE_CR_FK
      ,R.EFFECTIVE_DATE AS RULES_EFFECTIVE_DATE
      ,R.END_DATE AS RULES_END_DATE
	  ,R.RECORD_STATUS_CR_FK   
      ,NULL  -- RELATED_RULES_FK  --- USE WITH WHATIF	  
	  ,RFS.PROD_FILTER_ASSOC_DN_FK
	  ,RFS.ORG_FILTER_ASSOC_DN_FK
	  ,RFS.CUST_FILTER_ASSOC_DN_FK
	  ,RFS.SUPL_FILTER_ASSOC_DN_FK	 
		-----	PRECEDENCES
		    ,CASE (	SELECT ISNULL ( RFC.DATA_NAME_FK, 0 )
					FROM synchronizer.RULES_FILTER RFC WITH (NOLOCK)  
					WHERE RFC.RULES_FILTER_ID = TSFC.CUST_FILTER_FK )
			 WHEN 105000 THEN  ISNULL (
			               ( SELECT ( (TSFC.CUST_FILTER_HIERARCHY * 1000 ) +  RCC.PRECEDENCE_ADJUSTMENT )
			                 FROM synchronizer.RULES_CONTRACT_CUSTOMER RCC WITH (NOLOCK)
			                 WHERE RCC.RULES_CONTRACT_CUSTOMER_ID = TSFC.RULES_CONTRACT_CUSTOMER_FK )
			               , 9990 )
			 WHEN 0 THEN 9999
			 ELSE 9999
			 END AS CONTRACT_PRECEDENCE
    ---
			,R.RULE_PRECEDENCE
			,RSCH.SCHEDULE_PRECEDENCE     
			,RS.PRECEDENCE     	       
			,RSCH.MATRIX_ACTION_CR_FK 
			,RS.MATRIX_ACTION_CR_FK 
			,RH.MATRIX_ACTION_CR_FK 						
	        ,RSCH.PROMOTIONAL_IND 
            ,R.HOST_RULE_XREF
	        ,R.RULES_STRUCTURE_FK
	        ,R.RULES_SCHEDULE_FK	
	        ,RH.RULE_TYPE_CR_FK        
	  ,TSRE.PRODUCT_STRUCTURE_FK					
	  ,TSRE.ORG_ENTITY_STRUCTURE_FK
	  ,TSRE.CUST_ENTITY_STRUCTURE_FK
	  ,TSRE.SUPL_ENTITY_STRUCTURE_FK	
----
FROM #TS_RULE_EVENTS TSRE
INNER JOIN #TS_FILTERS_PROD TSFP 
    ON ( TSFP.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID )
INNER JOIN synchronizer.RULES_FILTER_SET RFS  WITH (NOLOCK) 
    ON ( ISNULL ( TSFP.PROD_FILTER_FK, -999 ) = ISNULL ( RFS.PROD_FILTER_FK, -999 ) )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)
   ON ( R.RULES_ID = RFS.RULES_FK
   AND  R.RESULT_DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK 
   AND  R.RECORD_STATUS_CR_FK = 1 )
--------------
INNER JOIN #TS_FILTERS_ORG TSFO
    ON ( TSFO.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID
    AND  ISNULL ( TSFO.ORG_FILTER_FK, -999 ) = ISNULL ( RFS.ORG_FILTER_FK, -999 ) )
INNER JOIN #TS_FILTERS_CUST TSFC
    ON ( TSFC.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID
    AND  ISNULL ( TSFC.CUST_FILTER_FK, -999 ) = ISNULL ( RFS.CUST_FILTER_FK, -999 ) )
INNER JOIN #TS_FILTERS_SUPL TSFS
    ON ( TSFS.TS_RULE_EVENTS_FK = TSRE.TS_RULE_EVENTS_ID
    AND  ISNULL ( TSFS.SUPL_FILTER_FK, -999 ) = ISNULL ( RFS.SUPL_FILTER_FK, -999 ) )
--------------    
INNER JOIN SYNCHRONIZER.RULES_STRUCTURE RS 
   ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
INNER JOIN SYNCHRONIZER.RULES_SCHEDULE RSCH 
   ON ( RSCH.RULES_SCHEDULE_ID = R.RULES_SCHEDULE_FK )
INNER JOIN SYNCHRONIZER.RULES_HIERARCHY RH
   ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK )
Left Join #TS_QUALIFIED_RULES TQR
	on TSRE.PRODUCT_STRUCTURE_FK = TQR.PRODUCT_STRUCTURE_FK
	and TSRE.ORG_ENTITY_STRUCTURE_FK  = TQR.ORG_ENTITY_STRUCTURE_FK
	and TSRE.CUST_ENTITY_STRUCTURE_FK  = TQR.CUST_ENTITY_STRUCTURE_FK
	and TSRE.SUPL_ENTITY_STRUCTURE_FK  = TQR.SUPL_ENTITY_STRUCTURE_FK
	and r.RULE_PRECEDENCE = TQR.rule_precedence
WHERE 1 = 1
AND   RH.RULE_TYPE_CR_FK = TSRE.RULE_TYPE_CR_FK
--------------
----  ORG SPECIFIC PRODUCT ATTRIBUTE VALIDATIONS
--------------
AND   (  (    ISNULL ( TSFP.PROD_CORP_IND, 0 ) = 1
         AND  TSFP.PROD_ORG_ENTITY_STRUCTURE_FK = 1 )
      OR  (    ISNULL ( TSFP.PROD_ORG_IND, 0 ) = 1
         AND  TSFP.PROD_ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK ) )

And TQR.RULES_FK is null

insert into dbo.maui_log Select 1530, 'Load TS_Qualified_Rules_2 Complete', GetDate(), Null, Null

---End of WilsonArt Mod

-----------------------------------------------------------------------------------------
-- Drop all Temporary Tables created in this procedure...
--	DO NOT DROP #TS_RULE_EVENTS this table will get truncated and droped elsewhere
-----------------------------------------------------------------------------------------

------drop temp tables
IF object_id('tempdb..#TS_FILTERS_PROD') is not null
   drop table #TS_FILTERS_PROD

------drop temp tables
IF object_id('tempdb..#TS_FILTERS_ORG') is not null
   drop table #TS_FILTERS_ORG

------drop temp tables
IF object_id('tempdb..#TS_FILTERS_CUST') is not null
   drop table #TS_FILTERS_CUST

------drop temp tables
IF object_id('tempdb..#TS_FILTERS_SUPL') is not null
   drop table #TS_FILTERS_SUPL

------drop temp tables
IF object_id('tempdb..#TS_prods') is not null
   drop table #TS_prods
---------------------------------------------------
-- DISQUALIFY BASED ON RESULT TYPE AND DATES
----   need to revisit the future rule logic later
---------------------------------------------------

	UPDATE #TS_QUALIFIED_RULES
	SET DISQUALIFIED_IND = 1
	   ,DISQUALIFIED_COMMENT = 'DISQUALIFY RESULT TYPE DOES NOT COMPLY'
	WHERE 1 = 1
    AND TS_QUALIFIED_RULES_ID IN (
                SELECT TSQR.TS_QUALIFIED_RULES_ID  --- ,EFFECTIVE_DATE, TSRE.TRIGGER_EVENT_FK
				FROM #TS_RULE_EVENTS TSRE
				INNER JOIN #TS_QUALIFIED_RULES TSQR ON TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK 
                WHERE 1 = 1
	            AND  TSQR.RULES_RESULT_TYPE_CR_FK > TSRE.RESULT_TYPE_CR_FK ) 


	UPDATE #TS_QUALIFIED_RULES
	SET DISQUALIFIED_IND = 1
	   ,DISQUALIFIED_COMMENT = 'DISQUALIFY RESULT TYPE DOES NOT COMPLY WITH DATES'
	WHERE 1 = 1
    AND TS_QUALIFIED_RULES_ID IN (
                SELECT TSQR.TS_QUALIFIED_RULES_ID  --- ,EFFECTIVE_DATE, TSRE.TRIGGER_EVENT_FK
				FROM #TS_RULE_EVENTS TSRE
				INNER JOIN #TS_QUALIFIED_RULES TSQR ON TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK 
                WHERE 1 = 1
	            AND  (   TSQR.RULES_EFFECTIVE_DATE IS NOT NULL
		             AND  TSQR.RULES_EFFECTIVE_DATE > TSRE.RESULT_EFFECTIVE_DATE )
                )


	UPDATE #TS_QUALIFIED_RULES
	SET DISQUALIFIED_IND = 1
	   ,DISQUALIFIED_COMMENT = 'DISQUALIFY RESULT TYPE DOES NOT COMPLY WITH DATES'
	WHERE 1 = 1
    AND TS_QUALIFIED_RULES_ID IN (
                SELECT TSQR.TS_QUALIFIED_RULES_ID  --- ,EFFECTIVE_DATE, TSRE.TRIGGER_EVENT_FK
				FROM #TS_RULE_EVENTS TSRE
				INNER JOIN #TS_QUALIFIED_RULES TSQR ON TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK 
                WHERE 1 = 1
				AND ( TSRE.RESULT_TYPE_CR_FK  IN ( 711, 712 )
						AND ISNULL ( TSQR.RULES_END_DATE, '01/01/2999' ) < TSRE.RESULT_EFFECTIVE_DATE ) 
				)
               

/*

---- Save the SQL for TESTING

SELECT TS_RULE_EVENTS_FK, PROD_FILTER_FK, CUST_FILTER_FK, ORG_FILTER_FK, SUPL_FILTER_FK, PROD_FILTER_HIERARCHY, PROD_FILTER_ORG_PRECEDENCE
,EFFECTIVE_DATE, DISQUALIFIED_COMMENT
FROM #TS_QUALIFIED_RULES 
INNER JOIN synchronizer.RULES ON RULES_ID = RULES_FK
WHERE TS_RULE_EVENTS_FK IN ( SELECT TS_RULE_EVENTS_FK FROM #TS_QUALIFIED_RULES where ISNULL ( DISQUALIFIED_IND, 0 ) = 1 )
ORDER BY TS_RULE_EVENTS_FK, PROD_FILTER_FK, CUST_FILTER_FK, ORG_FILTER_FK, SUPL_FILTER_FK, DISQUALIFIED_COMMENT

*/


---------------------------------------------------------------------------------
-- DISQUALIFY DUPLICATE RULE for same EVENT
---------------------------------------------------------------------------------

		UPDATE #TS_QUALIFIED_RULES 
		SET DISQUALIFIED_IND = 1
		   ,DISQUALIFIED_COMMENT = 'DISQUALIFY DUPLICATE RULE for the same EVENT'
        WHERE 1 = 1 
        AND   TS_QUALIFIED_RULES_ID  IN  (
			SELECT A.TS_QUALIFIED_RULES_ID
			FROM (
                SELECT
                    TSQR.TS_QUALIFIED_RULES_ID,
				(	DENSE_RANK() OVER ( PARTITION BY TSQR.TS_RULE_EVENTS_FK, TSQR.RULES_FK
										ORDER BY	 TSQR.PROD_FILTER_ORG_PRECEDENCE ASC, TSQR.TS_QUALIFIED_RULES_ID DESC ) 
                     ) AS DRANK
					FROM #TS_QUALIFIED_RULES TSQR WITH (NOLOCK) 
					WHERE 1 = 1
					AND   ISNULL ( DISQUALIFIED_IND, 0 ) <> 1					
                ) A
            WHERE A.DRANK > 1 )



---------------------------------------------------------------
-- DISQUALIFY DUPLICATE STRUCTURE and FILTERED RULES
---------------------------------------------------------------



		UPDATE #TS_QUALIFIED_RULES 
		SET DISQUALIFIED_IND = 1
		   ,DISQUALIFIED_COMMENT = 'DUPLICATE Structure and Filtered RULES'
        WHERE 1 = 1 
        AND   TS_QUALIFIED_RULES_ID  IN  (
			SELECT A.TS_QUALIFIED_RULES_ID
			FROM (
                SELECT
                    TSQR.TS_QUALIFIED_RULES_ID  
				,(	DENSE_RANK() OVER ( PARTITION BY TSQR.TRIGGER_EVENT_FK, TSQR.STRUC_PRECEDENCE, TSQR.SCHED_PRECEDENCE, TSQR.RULE_PRECEDENCE
				                                    ,TSQR.PROD_FILTER_FK, TSQR.CUST_FILTER_FK, TSQR.ORG_FILTER_FK, TSQR.SUPL_FILTER_FK
										ORDER BY	 TSQR.RULES_EFFECTIVE_DATE DESC, TSQR.PROD_FILTER_ORG_PRECEDENCE ASC, TSQR.RULES_FK ASC ) 
                     ) AS DRANK
					FROM #TS_QUALIFIED_RULES TSQR 
					WHERE 1 = 1
					AND   TSQR.RULE_TYPE_CR_FK IN ( 312, 313 )
					AND   ISNULL ( DISQUALIFIED_IND, 0 ) <> 1					
                ) A
            WHERE A.DRANK > 1 )




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of synchronizer.EVENT_RULE_QUALIFY'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of synchronizer.EVENT_RULE_QUALIFY has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END