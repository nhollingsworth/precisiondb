﻿










-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing of derivations
--				Carried over V3 String Functions
--
--		NOTE::  OPERATION 310: DERIVE SUBSTRING
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010   Initial SQL Version
-- CV		 01/18/2017   Using Controls table and not structure etc.
--

CREATE PROCEDURE [synchronizer].[EVENT_POST_DERIVE_NEW_DATA_SUBSTRING] 
        ( @in_batch_no BIGINT, @in_calc_group_seq INT, @in_result_data_name_fk INT  )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT
DECLARE  @ls_exec             nvarchar(4000)

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_DERIVE_NEW_DATA_SUBSTRING.' + ' Batch No = '
					 + cast(isnull(@in_batch_no, 0) as varchar(30))
					 +' Calc Seq: '	 + cast(isnull(@in_calc_group_seq, 0) as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;



---------------------------------------------------
-- Temp Table for derivation columns
---------------------------------------------------


--drop temp tables
IF object_id('tempdb..#TS_DERIVE_SUBSTRING') is not null
   drop table #TS_DERIVE_SUBSTRING

CREATE TABLE #TS_DERIVE_SUBSTRING(
	[TS_DERIVE_SUBSTRING_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[EVENT_FK] [bigint] NULL,		
	[RULES_FK] [bigint] NULL,
	[RESULT_DATA_NAME_FK] [int] NULL,
	[RESULT_DATA_NAME2_FK] [int] NULL,
	[BASIS1_DN_FK] [int] NULL,	
	[PREFIX1] 	[varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,								
	[BASIS1_STRING] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,			
	[CLEANSED_STRING] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,				
	[RESULT1] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,		
	[RESULT2] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,			
	[MAX_LENGTH1] [smallint] NULL,		
	[MAX_LENGTH2] [smallint] NULL,
	[WHOLE_TOKEN1] [smallint] NULL,	
    [SUBSTRING_MAX_IND] [smallint] NULL,	
	[RULES_ACTION_STRFUNCTION_FK1] [bigint] NULL
    )

---------------------------------------------------
--  Insert data for TEMP TABLE
---------------------------------------------------

TRUNCATE TABLE #TS_DERIVE_SUBSTRING

INSERT INTO #TS_DERIVE_SUBSTRING (
	EVENT_FK,
	RULES_FK,	
	RESULT_DATA_NAME_FK,	
	RESULT_DATA_NAME2_FK,		
	MAX_LENGTH1,	
	MAX_LENGTH2,
	BASIS1_DN_FK,
	PREFIX1,		
	BASIS1_STRING,
	WHOLE_TOKEN1,
    RULES_ACTION_STRFUNCTION_FK1
    ) 
--------------------------
SELECT 
     A.EVENT_ID
    ,A.RULES_ID
    ,A.RESULT_DATA_NAME_FK
    ,A.RESULT_DATA_NAME2_FK    
    ,A.MAX_LENGTH1
    ,A.MAX_LENGTH2  
    ,A.BASIS1_DN_FK  
    ,A.PREFIX1
    ,A.BASIS1_STRING
    ,A.WHOLE_TOKEN1
    ,A.RULES_ACTION_STRFUNCTION_FK1 
--------------------------
FROM (
SELECT 
     ED.EVENT_ID
    ,R.RULES_ID
    ,R.RESULT_DATA_NAME_FK
    ,R.RESULT_DATA_NAME2_FK    
    ,DN1.LENGTH AS MAX_LENGTH1
    ,DN2.LENGTH AS MAX_LENGTH2   
	,RA.BASIS1_DN_FK
	,RA.PREFIX1
    ,CASE ISNULL ( RA.BASIS1_DN_FK, 0 )
	 WHEN 0 THEN NULL
	 ELSE ( 
	        SELECT ISNULL (
				( SELECT TOP 1 EDX.NEW_DATA
				  FROM synchronizer.EVENT_DATA EDX
				  INNER JOIN EPACUBE.DATA_NAME DNX ON DNX.DATA_NAME_ID = EDX.DATA_NAME_FK
				  INNER JOIN EPACUBE.DATA_SET DSX ON DSX.DATA_SET_ID = DNX.DATA_SET_FK
				  WHERE EDX.DATA_NAME_FK = RA.BASIS1_DN_FK
				  AND   EDX.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
				  AND   (   EDX.ORG_ENTITY_STRUCTURE_FK = 1
				         OR ED.ORG_ENTITY_STRUCTURE_FK = 1
				         OR EDX.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK )
--------				  AND   ( 
--------						 (    DSX.ENTITY_STRUCTURE_EC_CR_FK IS NULL  )
--------						OR   
--------						 (    ISNULL ( EDX.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ED.ENTITY_STRUCTURE_FK, -999) )
--------						 )
				  AND   EDX.BATCH_NO = ED.BATCH_NO 
				  AND   EDX.NEW_DATA IS NOT NULL )
		  ,
	( SELECT ISNULL (
				( SELECT VCPDX.CURRENT_DATA
				  FROM EPACUBE.V_CURRENT_PRODUCT_DATA VCPDX
				  INNER JOIN EPACUBE.DATA_NAME DNX ON DNX.DATA_NAME_ID = VCPDX.DATA_NAME_FK
				  INNER JOIN EPACUBE.DATA_SET DSX ON DSX.DATA_SET_ID = DNX.DATA_SET_FK
				  WHERE VCPDX.DATA_NAME_FK = RA.BASIS1_DN_FK
				  AND   VCPDX.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
		  AND   (   VCPDX.ORG_ENTITY_STRUCTURE_FK = 1
		         OR ED.ORG_ENTITY_STRUCTURE_FK = 1
		         OR VCPDX.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK )
--------		  AND   ( 
--------				 (    DSX.ENTITY_STRUCTURE_EC_CR_FK IS NULL  )
--------				OR   
--------				 (    ISNULL ( VCPDX.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ED.ENTITY_STRUCTURE_FK, -999) )
--------				 )
				  AND   VCPDX.CURRENT_DATA IS NOT NULL )
	,
	( SELECT TOP 1  VCEDX.CURRENT_DATA   -- SUPPLIER ONLY FOR RIGHT NOW 
				  FROM EPACUBE.V_CURRENT_ENTITY_DATA VCEDX
				  WHERE VCEDX.DATA_NAME_FK = RA.BASIS1_DN_FK
				  AND   VCEDX.ENTITY_STRUCTURE_FK IN  (
            SELECT ISNULL (
             ( SELECT TOP 1 EDZ.ENTITY_STRUCTURE_FK
               FROM synchronizer.EVENT_DATA EDZ WITH (NOLOCK)
			   WHERE EDZ.DATA_NAME_FK = R.Vendor_Segment_Data_Name_FK
               --WHERE EDZ.DATA_NAME_FK = RFS.SUPL_FILTER_ASSOC_DN_FK
               AND   EDZ.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
               AND   (   EDZ.ORG_ENTITY_STRUCTURE_FK = 1
                      OR ED.ORG_ENTITY_STRUCTURE_FK = 1
                      OR ED.ORG_ENTITY_STRUCTURE_FK = EDZ.ORG_ENTITY_STRUCTURE_FK ) )
         ,
				  ( SELECT TOP 1 PAS.ENTITY_STRUCTURE_FK 
				                                       FROM EPACUBE.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
													   WHERE PAS.DATA_NAME_FK = R.Vendor_Segment_Data_Name_FK
				                                       --WHERE PAS.DATA_NAME_FK = RFS.SUPL_FILTER_ASSOC_DN_FK 
				                                       AND   PAS.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
				                                       AND   (   PAS.ORG_ENTITY_STRUCTURE_FK = 1
				                                              OR ED.ORG_ENTITY_STRUCTURE_FK = 1
				                                              OR ED.ORG_ENTITY_STRUCTURE_FK = PAS.ORG_ENTITY_STRUCTURE_FK ) )
				   ) )
				  AND   VCEDX.CURRENT_DATA IS NOT NULL )
	))))
	END BASIS1_STRING
----------------
,ISNULL ( RASF1.SUBSTR_WHOLE_TOKEN_IND, 0 ) AS WHOLE_TOKEN1
,RASF1.RULES_ACTION_STRFUNCTION_ID AS RULES_ACTION_STRFUNCTION_FK1 
---------------
	FROM synchronizer.EVENT_DATA ED  WITH (NOLOCK) 
	INNER JOIN synchronizer.EVENT_DATA_RULES EDR  WITH (NOLOCK) 
	  ON ( EDR.EVENT_FK = ED.EVENT_ID
	  AND  EDR.RESULT_RANK = 1  )      --- ONLY THE SELECTED RULE
	INNER JOIN synchronizer.RULES R  WITH (NOLOCK) 
	  ON ( EDR.RULES_FK = R.RULES_ID )
	  inner join epacube.controls C on C.controls_id = r.controls_fk
	--INNER JOIN synchronizer.RULES_STRUCTURE RS  WITH (NOLOCK) 
	--  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
	--INNER JOIN synchronizer.RULES_HIERARCHY RH  WITH (NOLOCK) 
	--  ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
	--  AND  RH.RECORD_STATUS_CR_FK = 1 )	  
	--INNER JOIN synchronizer.RULES_FILTER_SET RFS  WITH (NOLOCK) 
	--  ON ( RFS.RULES_FK = R.RULES_ID )
	INNER JOIN 	epacube.DATA_NAME  DN1 WITH (NOLOCK)
	  ON ( DN1.DATA_NAME_ID = R.RESULT_DATA_NAME_FK  )
	INNER JOIN 	epacube.DATA_NAME  DN2 WITH (NOLOCK)
	  ON ( DN2.DATA_NAME_ID = R.RESULT_DATA_NAME2_FK  )	  
--------	INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) 
--------	  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )	  	  
	INNER JOIN synchronizer.RULES_ACTION RA  WITH (NOLOCK) 
	  ON ( RA.RULES_FK = R.RULES_ID )
	LEFT JOIN synchronizer.RULES_ACTION_STRFUNCTION RASF1 WITH (NOLOCK) 
	  ON ( RASF1.RULES_ACTION_FK = RA.RULES_ACTION_ID
	  AND  RASF1.BASIS_POSITION = 1 )
	WHERE 1 = 1
	AND   ED.BATCH_NO = @IN_BATCH_NO	
	AND   RA.RULES_ACTION_OPERATION1_FK = 310  --- DERIVE SUBSTRING 
	 AND   ISNULL ( c.control_precedence, 999999999 ) = @in_calc_group_seq	
    --AND   ISNULL ( RH.CALC_GROUP_SEQ, 999999999 ) = @in_calc_group_seq	
    AND   R.RESULT_DATA_NAME_FK = @in_result_data_name_fk
) A 


---------------------------------------------------
-- Create Indexes  #TS_DERIVE_SUBSTRING
---------------------------------------------------


---- MAY NEED TO ADD LATER FOR PERFORMANCE
   
   


---------------------------------------------------
--  Perform String Functions  -- CLEANSED_STRING
---------------------------------------------------

UPDATE #TS_DERIVE_SUBSTRING
	SET CLEANSED_STRING = CASE WHEN BASIS1_STRING IS NULL
	                      THEN NULL
	                      ELSE CASE ISNULL ( RULES_ACTION_STRFUNCTION_FK1, 0 )
							   WHEN 0 THEN CASE ISNULL ( PREFIX1, 'N' )
											WHEN 'N' THEN  BASIS1_STRING
											WHEN '?' THEN  ' ' + BASIS1_STRING
											ELSE PREFIX1 + BASIS1_STRING
											END
							   ELSE ( SELECT epacube.ADD_BASIS_TOKEN 
										 ( 1, NULL, PREFIX1, BASIS1_STRING, RULES_ACTION_STRFUNCTION_FK1 ) )
	                           END
	                      END
WHERE 1 = 1  
             



---------------------------------------------------
--  Perform String Functions  -- RESULT1
---------------------------------------------------

UPDATE #TS_DERIVE_SUBSTRING
	SET RESULT1 =   RTRIM ( ( SELECT epacube.ADD_BASIS_TOKEN_BY_SUBSTRING
						   ( NULL, NULL, MAX_LENGTH1, WHOLE_TOKEN1, NULL, 1
							  ,CLEANSED_STRING ) ) )
WHERE CLEANSED_STRING IS NOT NULL 
OR    CLEANSED_STRING <> 'NULL' 
             


---------------------------------------------------
--  Perform String Functions  -- RESULT1 -- IF NULL
--    SUBSTRING (value_expression ,start_expression ,length_expression )
---------------------------------------------------

UPDATE #TS_DERIVE_SUBSTRING
	SET RESULT1 =   SUBSTRING ( CLEANSED_STRING, 1, MAX_LENGTH1 )
        ,SUBSTRING_MAX_IND = 1
WHERE RESULT1 IS NULL  
OR    RESULT1 = 'NULL'              



---------------------------------------------------
--  Perform String Functions  -- RESULT2
---------------------------------------------------

UPDATE #TS_DERIVE_SUBSTRING
	SET RESULT2 =  LTRIM ( ( SELECT epacube.ADD_BASIS_TOKEN_BY_SUBSTRING
						   ( NULL, NULL, MAX_LENGTH2, NULL, NULL,            --- DO NOT ASSUME WHOLE_TOKEN2 FOR THE SECOND KS
						      ( SELECT LEN ( RESULT1 ) + 1 )
							  ,CLEANSED_STRING ) ) )
WHERE 1 = 1
AND   (  CLEANSED_STRING IS NOT NULL 
      OR CLEANSED_STRING <> 'NULL'  )
OR    (  RESULT1 IS NOT NULL  
	  OR RESULT1 <> 'NULL'  )




----		
--------           TRUNCATE TABLE COMMON.T_SQL  
----
----       SET @ls_exec = ( SELECT TOP 1 BASIS1_RESULT
----                            FROM #TS_DERIVE_SUBSTRING 
----                            WHERE BASIS1_IND = 1 )
----     
----       INSERT INTO COMMON.T_SQL
----                ( TS, SQL_TEXT )
----       VALUES ( GETDATE(), ISNULL ( @ls_exec, 'SYNCHRONIZER.EVENT_POST_DERIVE_NEW_DATA BASIS1_RESULT IS NULL' )  )
	                   


------------------------------------------------------------------------------------------
--   DERIVATIONS GO DIRECTLY INTO PERSTED TABLES
--      IF UNSUCCESSFUL... ( IE NEW_DATA IS NULL ) AUTO REJECT; LOG ERROR
--		OTHERWISE SET TO APPROVE
------------------------------------------------------------------------------------------


UPDATE synchronizer.EVENT_DATA
SET EVENT_STATUS_CR_FK = 82
   ,EVENT_PRIORITY_CR_FK = 60
   ,EVENT_CONDITION_CR_FK = 99
WHERE EVENT_ID IN ( 
		SELECT TSDD.EVENT_FK  
		FROM #TS_DERIVE_SUBSTRING   TSDD
		WHERE 1 = 1
		AND   TSDD.RESULT1 IS NULL 
        OR    TSDD.RESULT1 = 'NULL' )

INSERT INTO synchronizer.EVENT_DATA_ERRORS (
	EVENT_FK,
	RULE_FK,
	EVENT_CONDITION_CR_FK,
	ERROR_NAME,
	UPDATE_TIMESTAMP)
SELECT 
		 TSDD.EVENT_FK
		,TSDD.RULES_FK
		,99
		,'DERIVED DATA IS NULL'  --ERROR NAME        
		,@l_sysdate
FROM #TS_DERIVE_SUBSTRING   TSDD
WHERE 1 = 1
AND   TSDD.RESULT1 IS NULL
OR    TSDD.RESULT1 = 'NULL'

------------------------------------------------------------------------------------------
--   IF RESULT1 USED MAX LENGTH SUBSTRING THEN WARNING CREATED
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_DATA
SET EVENT_CONDITION_CR_FK = 98
WHERE EVENT_ID IN ( 
		SELECT TSDD.EVENT_FK  
		FROM #TS_DERIVE_SUBSTRING   TSDD
		WHERE 1 = 1
		AND   TSDD.SUBSTRING_MAX_IND = 1 ) 

INSERT INTO synchronizer.EVENT_DATA_ERRORS (
	EVENT_FK,
	RULE_FK,
	EVENT_CONDITION_CR_FK,
	ERROR_NAME,
	UPDATE_TIMESTAMP)
SELECT 
		 TSDD.EVENT_FK
		,TSDD.RULES_FK
		,98
		,'DERIVED DATA WAS CUT OFF AT MAX_LENGTH CHECK RESULTS'  --ERROR NAME        
		,@l_sysdate
FROM #TS_DERIVE_SUBSTRING   TSDD
WHERE 1 = 1
AND   TSDD.SUBSTRING_MAX_IND = 1 




------------------------------------------------------------------------------------------
--   UPDATE EVENT_DATA FOR RESULT1
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_DATA 
SET  NEW_DATA = TSDD.RESULT1
    ,RULES_FK = TSDD.RULES_FK
    ,UPDATE_TIMESTAMP = GETDATE ()
FROM #TS_DERIVE_SUBSTRING TSDD
WHERE 1 = 1
AND   TSDD.EVENT_FK = synchronizer.EVENT_DATA.EVENT_ID
AND   (   TSDD.RESULT1 IS NOT NULL 
      OR  TSDD.RESULT1 <> 'NULL' )


------------------------------------------------------------------------------------------
--   IF RESULT2 IS NOT NULL THEN CREATE EVENT_DATA FOR THIS RESULT TOO
------------------------------------------------------------------------------------------

	INSERT INTO [synchronizer].[EVENT_DATA]
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[NEW_DATA]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_ACTION_CR_FK]			   
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[JOB_CLASS_FK]
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			   ,[STG_RECORD_FK]
			   ,[BATCH_NO]
			   ,[IMPORT_BATCH_NO]			   
			   ,[VALUE_EFFECTIVE_DATE]
			   ,[RELATED_EVENT_FK]
			   ,[RULES_FK]
			   ,[RESULT_TYPE_CR_FK]	
			   ,[RECORD_STATUS_CR_FK]		   
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])
      SELECT 
			    ED.EVENT_TYPE_CR_FK
			   ,ED.EVENT_ENTITY_CLASS_CR_FK
			   ,ED.EVENT_EFFECTIVE_DATE
			   ,ED.EPACUBE_ID
			   ,TSDD.RESULT_DATA_NAME2_FK
			   ,ED.PRODUCT_STRUCTURE_FK	
			   ,ED.ORG_ENTITY_STRUCTURE_FK			   		   
			   ,ED.ENTITY_CLASS_CR_FK
			   ,ED.ENTITY_DATA_NAME_FK			   			   
			   ,ED.ENTITY_STRUCTURE_FK
			   ,TSDD.RESULT2
			   ,ED.EVENT_CONDITION_CR_FK
			   ,ED.EVENT_ACTION_CR_FK			   
			   ,ED.EVENT_STATUS_CR_FK
			   ,ED.EVENT_PRIORITY_CR_FK
			   ,ED.EVENT_SOURCE_CR_FK
			   ,ED.JOB_CLASS_FK
			   ,ED.IMPORT_JOB_FK
			   ,ED.IMPORT_DATE
			   ,ED.IMPORT_FILENAME
			   ,ED.IMPORT_PACKAGE_FK
			   ,ED.STG_RECORD_FK
			   ,ED.BATCH_NO
			   ,ED.IMPORT_BATCH_NO			   
			   ,ED.VALUE_EFFECTIVE_DATE
			   ,TSDD.EVENT_FK
			   ,TSDD.RULES_FK
			   ,ED.RESULT_TYPE_CR_FK	
			   ,ED.RECORD_STATUS_CR_FK		   
			   ,ED.UPDATE_TIMESTAMP
			   ,ED.UPDATE_USER
      FROM #TS_DERIVE_SUBSTRING TSDD
      INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
        ON ( ED.EVENT_ID = TSDD.EVENT_FK )
      WHERE 1 = 1
	  AND   (   TSDD.RESULT2 IS NOT NULL 
            OR  TSDD.RESULT2 <> 'NULL' )



/*

---   KS  commenting out the insertion for the rule assignment for the second result
---			rules will be assigned to first result only for now




/**********************************************************************************/
--   Insert into sychronizer.EVENT_DATA_RULES
--		Insert Current Rules... ONLY if not already there ??
--		Later look into DRANK ?? or something to remove duplicates
--      Also need to include a SQL for Future / Whatif Rules
/**********************************************************************************/


	INSERT INTO [synchronizer].[EVENT_DATA_RULES] (
			 EVENT_FK
			,RULES_FK 
			,RESULT_DATA_NAME_FK
			,RESULT_TYPE_CR_FK
			,RESULT_EFFECTIVE_DATE 
	        ,RULES_ACTION_OPERATION_FK
	        ,RULES_EFFECTIVE_DATE
	        ,RULE_PRECEDENCE
	        ,SCHED_PRECEDENCE
	        ,RESULT_RANK
            ,UPDATE_TIMESTAMP   ) 
    SELECT 
			 ED.EVENT_ID  ---  FROM NEWLY INSERTED EVENTS ABOVE
			,TSDD.RULES_FK   
			,TSDD.RESULT_DATA_NAME2_FK      		   
			,711  --RESULT_TYPE_CR_FK      	
			,ED.EVENT_EFFECTIVE_DATE      	
            ,RA.RULES_ACTION_OPERATION1_FK
		-------- FROM RULE TABLES
		    ,R.EFFECTIVE_DATE
			,R.RULE_PRECEDENCE
			,RSCH.SCHEDULE_PRECEDENCE	
			,1			
			,GETDATE()  
------
	  FROM  #TS_DERIVE_SUBSTRING TSDD
      INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
        ON ( ED.RELATED_EVENT_FK = TSDD.EVENT_FK )
      INNER JOIN synchronizer.RULES R WITH (NOLOCK)
        ON ( R.RULES_ID = TSDD.RULES_FK )
      INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
        ON ( RA.RULES_FK =  R.RULES_ID )
	  INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
		 ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
	  INNER JOIN synchronizer.RULES_SCHEDULE RSCH WITH (NOLOCK) 
		 ON ( RSCH.RULES_SCHEDULE_ID = R.RULES_SCHEDULE_FK )
      WHERE TSDD.RESULT2 IS NOT NULL 


*/  


 
--drop temp tables
IF object_id('tempdb..#TS_DERIVE_SUBSTRING') is not null
   drop table #TS_DERIVE_SUBSTRING




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_DERIVE_NEW_DATA_SUBSTRING'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_DERIVE_NEW_DATA_SUBSTRING has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END









