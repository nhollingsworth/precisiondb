﻿









-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing for very large imports.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/26/2010   Initial SQL Version
-- CV        04/28/2010   Adding Entity Posting Import to process
-- CV        01/26/2011   Adding new procedure call for status event
-- CV        05/23/2012   Increased temp table size from 64 to 256
-- CV        04/01/2013   Commented out call for derivations - moved to submit to synchronizer.
-- CV        05/06/2013   Added back the entity posting import to process
-- CV        05/18/2014   Adding parent structure fk for taxonomy node id
-- CV        09/08/2016   Adding Section for new PriceSheet_Basis_Values Table and comment out Product Values




CREATE PROCEDURE [synchronizer].[EVENT_POSTING_IMPORT] 
        ( @in_batch_no BIGINT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime
DECLARE  @V_END_TIMESTAMP datetime
DECLARE  @V_START_TIMESTAMP datetime
DECLARE  @v_entity_class_cr_fk  int

DECLARE  @v_batch_no             bigint
DECLARE  @in_job_fk              BIGINT
DECLARE  @v_import_batch_size    INT
DECLARE  @v_import_max_prods     INT
DECLARE  @v_import_max_whse      INT

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_IMPORT.'
					 + cast(isnull(@in_batch_no, 0) as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;



------            exec sp_executesql 	@ls_stmt  USE TO THROW ERROR

SET @l_sysdate = GETDATE()

set @in_job_fk = (select top 1 job_fk from stage.stg_record
where batch_no = @in_batch_no)

SET @v_import_batch_size =  ISNULL (
                            ( SELECT CAST ( EEP.VALUE AS INT )
                              FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK)
                              WHERE APPLICATION_SCOPE_FK = 100
                              AND   NAME = 'IMPORT BATCH SIZE' ), 500000 )


SET @V_ENTITY_CLASS_CR_FK = ( SELECT TOP 1 SR.ENTITY_CLASS_CR_FK
                              FROM STAGE.STG_RECORD SR WITH (NOLOCK)
                              WHERE SR.BATCH_NO = @IN_BATCH_NO )

------------------------------------------------------------------------------------------
--   Save this code snippet for causing immediate stop
------------------------------------------------------------------------------------------

----            SET @ls_exec = 'STOP CODE HERE'            
----            INSERT INTO COMMON.T_SQL
----                ( TS,  SQL_TEXT )
----            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
----            exec sp_executesql 	@ls_exec;



------------------------------------------------------------------------------------------
---  RAN OUT OF TIME SO JUST DO OLD WAY FOR VALUES AND RESULTS
------------------------------------------------------------------------------------------
IF @V_ENTITY_CLASS_CR_FK = (select code_ref_id from epacube.code_ref 
							where code_type = 'ENTITY_CLASS'
							and Code = 'PRODUCT')  -- PRODUCT
BEGIN

    EXEC synchronizer.EVENT_FACTORY_NEW_HISTORY @in_batch_no

END

IF @V_ENTITY_CLASS_CR_FK  in (select code_ref_id from epacube.code_ref
							   where code_type = 'ENTITY_CLASS'
							   AND CODE in ('CUSTOMER', 'SALESREP', 'TERRITORY',
							  'BUYER', 'VENDOR', 'WAREHOUSE'))  -- ENTITY

BEGIN

    EXEC synchronizer.EVENT_FACTORY_ENTITY_NEW_HISTORY @in_batch_no

END

------------------------------------------------------------------------------------------
--   Create Temp Table for Events
--		This table will help with record locking during processing
--
--		Before posting will check for current value <> new value
--		However no longer care about duplicate events because
--		Processing events within a Job.. and each job will not
--      allow for a duplicate event. ( even UI entries ) 
------------------------------------------------------------------------------------------



--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA') is not null
	   drop table #TS_EVENT_DATA;
	   
	   
	-- create temp table  >>>>  VERY IMPORTANT EVENT_FK MUST BE IDENTIFY COLUMN IN EVENT_POSTING_IMPORT
	
		CREATE TABLE #TS_EVENT_DATA(
		EVENT_FK bigint IDENTITY(1000,1) NOT NULL,
		EVENT_TYPE_CR_FK         INT NULL,
		EVENT_ENTITY_CLASS_CR_FK INT NULL,
		EPACUBE_ID               INT NULL,	
		IMPORT_JOB_FK            BIGINT NULL,	
		IMPORT_PACKAGE_FK        BIGINT NULL,
		DATA_NAME_FK			 INT NULL,	
	    PRODUCT_STRUCTURE_FK     BIGINT NULL,
	    ORG_ENTITY_STRUCTURE_FK  BIGINT NULL,
	    ENTITY_STRUCTURE_FK      BIGINT NULL,
		ENTITY_CLASS_CR_FK        INT NULL,
		ENTITY_DATA_NAME_FK       INT NULL,	 
        PARENT_ENTITY_STRUCTURE_FK  INT NULL,
        CHILD_ENTITY_STRUCTURE_FK  INT NULL,   
	    CUST_ENTITY_STRUCTURE_FK BIGINT NULL,
	    SUPL_ENTITY_STRUCTURE_FK BIGINT NULL,
		ZONE_ENTITY_STRUCTURE_FK BIGINT NULL,	
		DEPT_ENTITY_STRUCTURE_FK BIGINT NULL,
		VENDOR_ENTITY_STRUCTURE_FK BIGINT NULL,		
		DATA_LEVEL				 VARCHAR (64) NULL,
		DATA_SET_FK			     INT NULL,				
		DATA_TYPE_CR_FK			 INT NULL,	
		ORG_IND                  SMALLINT NULL,
		INSERT_MISSING_VALUES    INT NULL,		
		TABLE_NAME               VARCHAR (64) NULL,					
        NEW_DATA                  VARCHAR(max) NULL,		
        CURRENT_DATA              VARCHAR(max) NULL,
		DATA_VALUE_FK             BIGINT NULL,
		ENTITY_DATA_VALUE_FK      BIGINT NULL,
        NET_VALUE1_NEW            NUMERIC (18,6) NULL,
        NET_VALUE2_NEW            NUMERIC (18,6) NULL,
        NET_VALUE3_NEW            NUMERIC (18,6) NULL,
        NET_VALUE4_NEW            NUMERIC (18,6) NULL,
        NET_VALUE5_NEW            NUMERIC (18,6) NULL,                                
        NET_VALUE6_NEW            NUMERIC (18,6) NULL,                                       
        NET_VALUE1_CUR            NUMERIC (18,6) NULL,
        NET_VALUE2_CUR            NUMERIC (18,6) NULL,
        NET_VALUE3_CUR            NUMERIC (18,6) NULL,
        NET_VALUE4_CUR            NUMERIC (18,6) NULL,
        NET_VALUE5_CUR            NUMERIC (18,6) NULL,                                
        NET_VALUE6_CUR            NUMERIC (18,6) NULL,                              
		PERCENT_CHANGE1			  numeric(18, 6) NULL,
		PERCENT_CHANGE2			  numeric(18, 6) NULL,
		PERCENT_CHANGE3			  numeric(18, 6) NULL,
		PERCENT_CHANGE4			  numeric(18, 6) NULL,
		PERCENT_CHANGE5			  numeric(18, 6) NULL,
		PERCENT_CHANGE6			  numeric(18, 6) NULL,         
        VALUE_UOM_CODE_FK         INT NULL, 
        ASSOC_UOM_CODE_FK         INT NULL,
        VALUES_SHEET_NAME          VARCHAR(256) NULL,
		VALUES_SHEET_DATE         DATETIME NULL,
		UOM_CONVERSION_FACTOR     NUMERIC (18,6) NULL,		
		END_DATE_NEW              DATETIME NULL,				
        END_DATE_CUR              DATETIME NULL,   		
		EFFECTIVE_DATE_CUR        DATETIME NULL,
		EVENT_EFFECTIVE_DATE	  DATETIME NULL,
		VALUE_EFFECTIVE_DATE	  DATETIME NULL,		
        RELATED_EVENT_FK		  BIGINT NULL,
        RULES_FK			      BIGINT NULL,
		EVENT_PRIORITY_CR_FK      INT NOT NULL,		        
		EVENT_ACTION_CR_FK        INT NOT NULL,
		EVENT_STATUS_CR_FK        INT NOT NULL,								        
		EVENT_CONDITION_CR_FK     INT NOT NULL,
		EVENT_SOURCE_CR_FK        INT NOT NULL,				
		RESULT_TYPE_CR_FK	      INT NULL,
		SINGLE_ASSOC_IND          SMALLINT NULL,				
		TABLE_ID_FK               BIGINT NULL,
		BATCH_NO				  BIGINT NULL,
		IMPORT_BATCH_NO           BIGINT NULL,
        JOB_CLASS_FK			  INT NULL,
        IMPORT_DATE               DATETIME NULL,
        IMPORT_FILENAME           VARCHAR(128) NULL,
        STG_RECORD_FK             BIGINT NULL,		
		RECORD_STATUS_CR_FK       INT NULL,
        UPDATE_TIMESTAMP		  DATETIME NULL,
        UPDATE_USER		          VARCHAR(64) NULL,
		PARENT_STRUCTURE_FK        BIGINT NULL,
		CHILD_PRODUCT_STRUCTURE_FK  INT NULL,
		CO_ENTITY_STRUCTURE_FK  INT NULL,
		PACK bigint NULL,
		CHAIN_ID_DATA_VALUE_FK BIGINT NULL,
        PROD_DATA_VALUE_FK  BIGINT NULL,
		ASSOC_ENTITY_STRUCTURE_FK  BIGINT NULL,
		LEVEL_SEQ BIGINT NULL,
		ASSOC_PARENT_CHILD VARCHAR(64) NULL,
		MARGIN_PERCENT VARCHAR(64) NULL,
		MARGIN_DOLLARS VARCHAR(64) NULL,
		EVENT_REASON_CR_FK Bigint NULL,
		PRECEDENCE VARCHAR(64) NULL,
		 )
	

	
	
	
	----CREATE TABLE #TS_EVENT_DATA(
	----	EVENT_FK bigint IDENTITY(1000,1) NOT NULL,
	----	EVENT_TYPE_CR_FK         INT NULL,
	----	EVENT_ENTITY_CLASS_CR_FK INT NULL,
	----	EPACUBE_ID               INT NULL,	
	----	IMPORT_JOB_FK            BIGINT NULL,	
	----	IMPORT_PACKAGE_FK        BIGINT NULL,
	----	DATA_NAME_FK			 INT NULL,	
	----    PRODUCT_STRUCTURE_FK     BIGINT NULL,
	----    ORG_ENTITY_STRUCTURE_FK  BIGINT NULL,
	----    ENTITY_STRUCTURE_FK      BIGINT NULL,
	----	ENTITY_CLASS_CR_FK        INT NULL,
	----	ENTITY_DATA_NAME_FK       INT NULL,	 
 ----       PARENT_ENTITY_STRUCTURE_FK  INT NULL,
 ----       CHILD_ENTITY_STRUCTURE_FK  INT NULL,   
	----    CUST_ENTITY_STRUCTURE_FK BIGINT NULL,
	----    SUPL_ENTITY_STRUCTURE_FK BIGINT NULL,					
	----	DATA_SET_FK			     INT NULL,				
	----	DATA_TYPE_CR_FK			 INT NULL,	
	----	ORG_IND                  SMALLINT NULL,
	----	INSERT_MISSING_VALUES    INT NULL,		
	----	TABLE_NAME               VARCHAR (64) NULL,			
 ----       NEW_DATA                  VARCHAR(max) NULL,		
 ----       CURRENT_DATA              VARCHAR(max) NULL,
	----	DATA_VALUE_FK             BIGINT NULL,
	----	ENTITY_DATA_VALUE_FK      BIGINT NULL,
 ----       NET_VALUE1_NEW            NUMERIC (18,6) NULL,
 ----       NET_VALUE2_NEW            NUMERIC (18,6) NULL,
 ----       NET_VALUE3_NEW            NUMERIC (18,6) NULL,
 ----       NET_VALUE4_NEW            NUMERIC (18,6) NULL,
 ----       NET_VALUE5_NEW            NUMERIC (18,6) NULL,                                
 ----       NET_VALUE6_NEW            NUMERIC (18,6) NULL,                                       
 ----       NET_VALUE1_CUR            NUMERIC (18,6) NULL,
 ----       NET_VALUE2_CUR            NUMERIC (18,6) NULL,
 ----       NET_VALUE3_CUR            NUMERIC (18,6) NULL,
 ----       NET_VALUE4_CUR            NUMERIC (18,6) NULL,
 ----       NET_VALUE5_CUR            NUMERIC (18,6) NULL,                                
 ----       NET_VALUE6_CUR            NUMERIC (18,6) NULL,                              
	----	PERCENT_CHANGE1			  numeric(18, 6) NULL,
	----	PERCENT_CHANGE2			  numeric(18, 6) NULL,
	----	PERCENT_CHANGE3			  numeric(18, 6) NULL,
	----	PERCENT_CHANGE4			  numeric(18, 6) NULL,
	----	PERCENT_CHANGE5			  numeric(18, 6) NULL,
	----	PERCENT_CHANGE6			  numeric(18, 6) NULL,         
 ----       VALUE_UOM_CODE_FK         INT NULL, 
 ----       ASSOC_UOM_CODE_FK         INT NULL,
 ----       VALUES_SHEET_NAME          VARCHAR(256) NULL,
	----	VALUES_SHEET_DATE         DATETIME NULL,
	----	UOM_CONVERSION_FACTOR     NUMERIC (18,6) NULL,	
	----	SEARCH1					Varchar(128) NULL,		
	----	END_DATE_NEW              DATETIME NULL,				
 ----       END_DATE_CUR              DATETIME NULL,   		
	----	EFFECTIVE_DATE_CUR        DATETIME NULL,
	----	EVENT_EFFECTIVE_DATE	  DATETIME NULL,
	----	VALUE_EFFECTIVE_DATE	  DATETIME NULL,		
 ----       RELATED_EVENT_FK		  BIGINT NULL,
 ----       RULES_FK			      BIGINT NULL,
	----	EVENT_PRIORITY_CR_FK      INT NOT NULL,		        
	----	EVENT_ACTION_CR_FK        INT NOT NULL,
	----	EVENT_STATUS_CR_FK        INT NOT NULL,								        
	----	EVENT_CONDITION_CR_FK     INT NOT NULL,
	----	EVENT_SOURCE_CR_FK        INT NOT NULL,				
	----	RESULT_TYPE_CR_FK	      INT NULL,
	----	SINGLE_ASSOC_IND          SMALLINT NULL,				
	----	TABLE_ID_FK               BIGINT NULL,
	----	BATCH_NO				  BIGINT NULL,
	----	IMPORT_BATCH_NO           BIGINT NULL,
 ----       JOB_CLASS_FK			  INT NULL,
 ----       IMPORT_DATE               DATETIME NULL,
 ----       IMPORT_FILENAME           VARCHAR(128) NULL,
 ----       STG_RECORD_FK             BIGINT NULL,		
	----	RECORD_STATUS_CR_FK       INT NULL,
 ----       UPDATE_TIMESTAMP		  DATETIME NULL,
 ----       UPDATE_USER		          VARCHAR(64) NULL,
	----	PARENT_STRUCTURE_FK        BIGINT NULL
	----	 )



--------------------------------------------------------------------------------------------
--  CREATE INDEXES 
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSED_EF ON #TS_EVENT_DATA 
	(EVENT_FK ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSED_EPA ON #TS_EVENT_DATA 
	(EPACUBE_ID ASC)
	INCLUDE ( EVENT_FK )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX IDX_TSED_EF_ETCRF ON #TS_EVENT_DATA 
	(EVENT_FK ASC, EVENT_TYPE_CR_FK ASC)
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX IDX_TSED_DNF_OESF_ESF_IND ON #TS_EVENT_DATA 
	(
		DATA_NAME_FK ASC,
		ORG_ENTITY_STRUCTURE_FK ASC,
		ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( NEW_DATA, EVENT_FK )
		WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX IDX_TESSRCH ON #TS_EVENT_DATA 
	(
		DATA_NAME_FK ASC,
		PRODUCT_STRUCTURE_FK ASC,
		ORG_ENTITY_STRUCTURE_FK ASC,
		ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( NEW_DATA, EVENT_FK )
		WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX TESD_20100524_VALID_IMPORT_HOST_CHG ON #TS_EVENT_DATA 
	(
		DATA_NAME_FK ASC,
		EVENT_SOURCE_CR_FK ASC,
		EVENT_ACTION_CR_FK ASC
	)
	INCLUDE ( EVENT_FK )
		WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX TSED_20100120_EVENT_POST_DATA ON #TS_EVENT_DATA 
	(
		EVENT_STATUS_CR_FK ASC,
		EVENT_CONDITION_CR_FK ASC,
		EVENT_ACTION_CR_FK ASC,
		EVENT_TYPE_CR_FK ASC,		
		TABLE_NAME ASC,
		TABLE_ID_FK ASC
	)
	INCLUDE ( [NEW_DATA], [RECORD_STATUS_CR_FK], [UPDATE_TIMESTAMP], [EVENT_FK] )
		WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA_ERRORS') is not null
	   drop table #TS_EVENT_DATA_ERRORS
	   
	   
	-- create temp table
CREATE TABLE #TS_EVENT_DATA_ERRORS(
	TS_EVENT_DATA_ERRORS_ID bigint IDENTITY(1000,1) NOT NULL,
	EVENT_FK bigint NULL,
	RULE_FK bigint NULL,
	EVENT_CONDITION_CR_FK int NULL,
	DATA_NAME_FK int NULL,
	ERROR_NAME varchar(64) NULL,
	DATA_NAME varchar(64) NULL,
	ERROR_DATA1 varchar(MAX) NULL,
	ERROR_DATA2 varchar(MAX) NULL,
	ERROR_DATA3 varchar(MAX) NULL,
	ERROR_DATA4 varchar(MAX) NULL,
	ERROR_DATA5 varchar(MAX) NULL,
	ERROR_DATA6 varchar(MAX) NULL,
    UPDATE_TIMESTAMP DATETIME NULL
		 )


--------------------------------------------------------------------------------------------
--  CREATE INDEXES 
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSEDE_TEDEID ON #TS_EVENT_DATA_ERRORS 
	(TS_EVENT_DATA_ERRORS_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSEDE_EF_TEDEID ON #TS_EVENT_DATA_ERRORS
	(EVENT_FK ASC)
	INCLUDE ( TS_EVENT_DATA_ERRORS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX IDX_TSEDE_EF_ECCRF ON #TS_EVENT_DATA_ERRORS 
	(EVENT_FK ASC, EVENT_CONDITION_CR_FK ASC)
    INCLUDE ( TS_EVENT_DATA_ERRORS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA_RULES') is not null
	   drop table #TS_EVENT_DATA_RULES
	   
	   
	-- create temp table
CREATE TABLE #TS_EVENT_DATA_RULES(
	[TS_EVENT_RULES_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[EVENT_FK] [bigint] NULL,
	[RULES_FK] [bigint] NULL,
	[RESULT_DATA_NAME_FK] [int] NULL,
	[RESULT_TYPE_CR_FK] [int] NULL,
	[RESULT_EFFECTIVE_DATE] [datetime] NULL,
	[RESULT_RANK] [smallint] NULL,
	[RESULT] [numeric](18, 6) NULL,
	[BASIS1] [numeric](18, 6) NULL,
	[BASIS2] [numeric](18, 6) NULL,
	[OPERAND] [numeric](18, 6) NULL,
	[OPERATION] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BASIS1_DN_FK] [int] NULL,
	[BASIS2_DN_FK] [int] NULL,
	[RULES_ACTION_OPERATION_FK] [int] NULL,
	[RULES_EFFECTIVE_DATE] [DATETIME] NULL,
	[RULE_PRECEDENCE] [smallint] NULL,
	[SCHED_PRECEDENCE] [smallint] NULL,
	[STRUC_PRECEDENCE] [smallint] NULL,
    [RESULT_MTX_ACTION_CR_FK] [int] NULL,
    [SCHED_MTX_ACTION_CR_FK] [int] NULL,
    [UPDATE_TIMESTAMP] [datetime] NULL     
	 ) 			


--------------------------------------------------------------------------------------------
--  CREATE INDEXES 
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSEDR_TEDRID ON #TS_EVENT_DATA_RULES 
	(TS_EVENT_RULES_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSEDR_EF_TEDRID ON #TS_EVENT_DATA_RULES
	(EVENT_FK ASC)
	INCLUDE ( TS_EVENT_RULES_ID, RULES_FK  )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSEDR_EF_ECCRF ON #TS_EVENT_DATA_RULES 
	(EVENT_FK ASC, RESULT_RANK ASC )
    INCLUDE ( TS_EVENT_RULES_ID, RULES_FK, RESULT_DATA_NAME_FK  )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSEDR_EF_SCHP_RP_RF ON #TS_EVENT_DATA_RULES 
	(EVENT_FK ASC, SCHED_PRECEDENCE ASC, RULE_PRECEDENCE ASC, RULES_FK DESC )
    INCLUDE ( TS_EVENT_RULES_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)




------------------------------------------------------------------------------------------
---  LOOP THROUGH IMPORT, BATCH_NO, EVENT_TYPE, TABLE, DATA_NAME, WHSE
---		IF IMPORT_RECORD_DATA HAS < @v_import_batch_size ROWS THEN DO NOT BREAK BY WHSE
---     ORDER IS			ASSOCIATIONS; PRODUCT EVENTS, RESULTS THEN VALUES
---							NEXT DERIVATIONS
------------------------------------------------------------------------------------------


      DECLARE 
              @vm$import_job_fk				bigint,   
              @vm$batch_no					bigint,                 
              @vm$event_type_cr_fk			INT,  
              @vm$event_priority_cr_fk		INT,            
              @vm$table_name				VARCHAR(64), 
              @vm$data_name_fk				INT,
              @vm$org_entity_structure_fk   BIGINT              


     


    SET @V_IMPORT_MAX_PRODS = ( SELECT COUNT(1)
								FROM stage.STG_RECORD SREC
							   WHERE SREC.BATCH_NO = @IN_BATCH_NO )


    SET @V_IMPORT_MAX_WHSE = ISNULL (
                              ( select avg ( a.cnt )
								from (
								SELECT record_no, count(1) as cnt
								from import.IMPORT_RECORD_DATA IRD
								group by record_no
								) a )
                               , 1 )
     
    SET @V_COUNT = @V_IMPORT_MAX_PRODS * @V_IMPORT_MAX_WHSE 


	SET @ls_stmt = '@V_COUNT FOR PROCESSING LOOP = '
						 + cast(isnull(@V_COUNT, 0) as varchar(30))
	EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
											  @epa_batch_no = @in_batch_no,
						   					  @exec_id   = @l_exec_no OUTPUT;




------------------------------------------------------------------------------------------
--- PRODUCT ASSOCIATIONS
------------------------------------------------------------------------------------------

IF @V_ENTITY_CLASS_CR_FK = (select code_ref_id from epacube.code_ref 
							where code_type = 'ENTITY_CLASS'
							and Code = 'PRODUCT')  -- PRODUCT
BEGIN

      SELECT @v_input_rows = COUNT(1)
						FROM stage.STG_RECORD SREC
						INNER JOIN import.IMPORT_RECORD_DATA IRD
						ON ( IRD.JOB_FK = SREC.JOB_FK
						AND  IRD.RECORD_NO = SREC.RECORD_NO )
                       WHERE SREC.BATCH_NO = @IN_BATCH_NO;

      SELECT @v_output_rows =COUNT(1)
						FROM stage.STG_RECORD SREC
						WHERE SREC.BATCH_NO = @IN_BATCH_NO;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_START_TIMESTAMP = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'START PRODUCT ASSOCIATION EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
	
-----



      DECLARE  cur_v_assoc cursor local FOR
			 SELECT 
					A.import_job_fk,
					A.batch_no,			        
					A.event_type_cr_fk,
					A.table_name,
					A.data_name_fk,	
					A.org_entity_structure_fk				
			  FROM (
				SELECT DISTINCT 
					   ISNULL ( SREC.job_fk, -999 ) import_job_fk
					  ,ISNULL ( SREC.batch_no, -999 ) batch_no				       
					  ,ISNULL ( DS.event_type_cr_fk, 711 ) event_type_cr_fk
					  ,ds.TABLE_NAME AS table_name	
					  ,ISNULL ( dn.data_name_id, -999 ) data_name_fk	
					  ,( SELECT CASE WHEN ( @V_COUNT < @v_import_batch_size ) THEN -999
							         ELSE CASE ISNULL ( DS.ORG_IND, 0 ) 
							              WHEN 1 THEN SRECO.ENTITY_STRUCTURE_FK
							              ELSE 1
                                     END
					             END ) AS org_entity_structure_fk	
				FROM STAGE.stg_record srec WITH (NOLOCK)
                INNER JOIN STAGE.STG_RECORD_ENTITY SRECE WITH (NOLOCK)
                  ON ( SREC.STG_RECORD_ID = SRECE.STG_RECORD_FK )
				INNER JOIN epacube.data_name dn with (nolock) 
				   ON ( DN.DATA_NAME_ID = SRECE.ASSOC_DATA_NAME_FK  )
				INNER JOIN epacube.data_set ds with (nolock) on ( ds.data_set_id = dn.data_set_fk )
                LEFT JOIN STAGE.STG_RECORD_ENTITY SRECO WITH (NOLOCK)
                  ON ( SREC.STG_RECORD_ID = SRECO.STG_RECORD_FK 
                  AND  SRECO.ENTITY_CLASS_CR_FK = 10101 )												
				WHERE srec.batch_no = @in_batch_no
				AND   dn.record_status_cr_fk = 1   
				AND   ds.event_type_cr_fk IN ( 155 )  --- include only product associations         
			) A
			  ORDER BY A.import_job_fk DESC, A.event_type_cr_fk ASC, A.table_name ASC, A.data_name_fk ASC       
      
              
        OPEN cur_v_assoc;
        FETCH NEXT FROM cur_v_assoc INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk


         WHILE @@FETCH_STATUS = 0 
         BEGIN

		   TRUNCATE TABLE #TS_EVENT_DATA
		   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
		   TRUNCATE TABLE #TS_EVENT_DATA_RULES      
                         
		   EXEC synchronizer.EVENT_POSTING_IMPORT_ASSOC 
				 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
				,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk

		   EXEC synchronizer.EVENT_POSTING_IMPORT_EVENT_DATA
				 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
				,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk


        FETCH NEXT FROM cur_v_assoc INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk


     END --cur_v_assoc LOOP
     CLOSE cur_v_assoc;
	 DEALLOCATE cur_v_assoc; 
    

---

SELECT @v_input_rows = COUNT(1)
						FROM stage.STG_RECORD SREC
						INNER JOIN import.IMPORT_RECORD_DATA IRD
						ON ( IRD.JOB_FK = SREC.JOB_FK
						AND  IRD.RECORD_NO = SREC.RECORD_NO )
                       WHERE SREC.BATCH_NO = @IN_BATCH_NO;

      SELECT @v_output_rows =COUNT(1)
						FROM stage.STG_RECORD SREC
						WHERE SREC.BATCH_NO = @IN_BATCH_NO;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
--      SET @V_START_TIMESTAMP = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'END PRODUCT ASSOCIATION EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
	 

END

------------------------------------------------------------------------------------------
---  ENTITY ASSOCIATIONS
------------------------------------------------------------------------------------------
IF @V_ENTITY_CLASS_CR_FK  in (select code_ref_id from epacube.code_ref
							   where code_type = 'ENTITY_CLASS'
							   AND CODE in ('CUSTOMER', 'SALESREP', 'TERRITORY',
							  'BUYER', 'VENDOR', 'WAREHOUSE','PROFIT CENTER'))  -- ENTITY

BEGIN

      SELECT @v_input_rows = COUNT(1)
						FROM stage.STG_RECORD SREC
						INNER JOIN import.IMPORT_RECORD_DATA IRD
						ON ( IRD.JOB_FK = SREC.JOB_FK
						AND  IRD.RECORD_NO = SREC.RECORD_NO )
                       WHERE SREC.BATCH_NO = @IN_BATCH_NO;

      SELECT @v_output_rows =COUNT(1)
						FROM stage.STG_RECORD SREC
						WHERE SREC.BATCH_NO = @IN_BATCH_NO;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_START_TIMESTAMP = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'START ENTITY ASSOCIATION EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
	
-----



      DECLARE  cur_v_assoc cursor local FOR
			 SELECT 
					A.import_job_fk,
					A.batch_no,			        
					A.event_type_cr_fk,
					A.table_name,
					A.data_name_fk,	
					A.org_entity_structure_fk				
			  FROM (
				SELECT DISTINCT 
					   ISNULL ( SREC.job_fk, -999 ) import_job_fk
					  ,ISNULL ( SREC.batch_no, -999 ) batch_no				       
					  ,ISNULL ( DS.event_type_cr_fk, 711 ) event_type_cr_fk
					  ,ds.TABLE_NAME AS table_name	
					  ,ISNULL ( dn.data_name_id, -999 ) data_name_fk	
					  ,( SELECT CASE WHEN ( @V_COUNT < @v_import_batch_size ) THEN -999
							         ELSE CASE ISNULL ( DS.ORG_IND, 0 ) 
							              WHEN 1 THEN SRECO.ENTITY_STRUCTURE_FK
							              ELSE 1
                                     END
					             END ) AS org_entity_structure_fk	
				FROM STAGE.stg_record srec WITH (NOLOCK)
                INNER JOIN STAGE.STG_RECORD_ENTITY SRECE WITH (NOLOCK)
                  ON ( SREC.STG_RECORD_ID = SRECE.STG_RECORD_FK )
				INNER JOIN epacube.data_name dn with (nolock) 
				   ON ( DN.DATA_NAME_ID = SRECE.ASSOC_DATA_NAME_FK  )
				INNER JOIN epacube.data_set ds with (nolock) on ( ds.data_set_id = dn.data_set_fk )
                LEFT JOIN STAGE.STG_RECORD_ENTITY SRECO WITH (NOLOCK)
                  ON ( SREC.STG_RECORD_ID = SRECO.STG_RECORD_FK 
                  AND  SRECO.ENTITY_CLASS_CR_FK = 10101 )												
				WHERE srec.batch_no = @in_batch_no
				AND   dn.record_status_cr_fk = 1   
				AND   ds.event_type_cr_fk IN ( 160 )  --- include only entity associations         
			) A
			  ORDER BY A.import_job_fk DESC, A.event_type_cr_fk ASC, A.table_name ASC, A.data_name_fk ASC       
      
              
        OPEN cur_v_assoc;
        FETCH NEXT FROM cur_v_assoc INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk


         WHILE @@FETCH_STATUS = 0 
         BEGIN

		   TRUNCATE TABLE #TS_EVENT_DATA
		   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
		   TRUNCATE TABLE #TS_EVENT_DATA_RULES      
                         
		   EXEC synchronizer.EVENT_POSTING_IMPORT_ENTITY_ASSOC 
				 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
				,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk

		   EXEC synchronizer.EVENT_POSTING_IMPORT_EVENT_DATA
				 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
				,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk


        FETCH NEXT FROM cur_v_assoc INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk


     END --cur_v_assoc LOOP
     CLOSE cur_v_assoc;
	 DEALLOCATE cur_v_assoc; 
    

---

SELECT @v_input_rows = COUNT(1)
						FROM stage.STG_RECORD SREC
						INNER JOIN import.IMPORT_RECORD_DATA IRD
						ON ( IRD.JOB_FK = SREC.JOB_FK
						AND  IRD.RECORD_NO = SREC.RECORD_NO )
                       WHERE SREC.BATCH_NO = @IN_BATCH_NO;

      SELECT @v_output_rows =COUNT(1)
						FROM stage.STG_RECORD SREC
						WHERE SREC.BATCH_NO = @IN_BATCH_NO;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
--      SET @V_START_TIMESTAMP = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'END ENTITY ASSOCIATION EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
END	 


------------------------------------------------------------------------------------------
---  PRODUCT EVENTS
------------------------------------------------------------------------------------------
IF @V_ENTITY_CLASS_CR_FK = (select code_ref_id from epacube.code_ref 
							where code_type = 'ENTITY_CLASS'
							and Code = 'PRODUCT')  -- PRODUCT
BEGIN


      SELECT @v_input_rows = COUNT(1)
						FROM stage.STG_RECORD SREC
						INNER JOIN import.IMPORT_RECORD_DATA IRD
						ON ( IRD.JOB_FK = SREC.JOB_FK
						AND  IRD.RECORD_NO = SREC.RECORD_NO )
                       WHERE SREC.BATCH_NO = @IN_BATCH_NO;

      SELECT @v_output_rows =COUNT(1)
						FROM stage.STG_RECORD SREC
						WHERE SREC.BATCH_NO = @IN_BATCH_NO;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_START_TIMESTAMP = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'START PRODUCT EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
	
-----

      DECLARE  cur_v_event cursor local FOR
			 SELECT 
					A.import_job_fk,
					A.batch_no,			        
					A.event_type_cr_fk,
					A.table_name,
					A.data_name_fk,
					A.org_entity_structure_fk				
			  FROM (
				SELECT DISTINCT
					   ISNULL ( SREC.job_fk, -999 ) import_job_fk
					  ,ISNULL ( SREC.batch_no, -999 ) batch_no				       
					  ,ISNULL ( DS.event_type_cr_fk, 711 ) event_type_cr_fk
					  ,ds.TABLE_NAME AS table_name	
					  ,ISNULL ( dn.data_name_id, -999 ) AS data_name_fk
					  ,( SELECT CASE WHEN ( @V_COUNT < @v_import_batch_size ) THEN -999
							         ELSE CASE ISNULL ( DS.ORG_IND, 0 ) 
							              WHEN 1 THEN SRECO.ENTITY_STRUCTURE_FK
							              ELSE 1
                                     END
					             END ) AS org_entity_structure_fk	
				FROM import.v_import vm
				INNER JOIN STAGE.stg_record srec WITH (NOLOCK) ON ( srec.import_package_fk = vm.import_package_fk )
				INNER JOIN epacube.data_name dn with (nolock) on ( dn.data_name_id = vm.data_name_fk )
				INNER JOIN epacube.data_set ds with (nolock) on ( ds.data_set_id = dn.data_set_fk )
                LEFT JOIN STAGE.STG_RECORD_ENTITY SRECO WITH (NOLOCK)
                  ON ( SREC.STG_RECORD_ID = SRECO.STG_RECORD_FK 
                  AND  SRECO.ENTITY_CLASS_CR_FK = 10101 )								
				WHERE srec.batch_no = @in_batch_no
				AND   dn.record_status_cr_fk = 1
				AND   ds.event_type_cr_fk IN ( 150, 156 )  --- include only product and uoms
                AND   dn.data_name_id not in ( 149000 )
			) A
			  ORDER BY A.import_job_fk DESC, A.event_type_cr_fk ASC, A.table_name ASC, A.data_name_fk ASC, A.org_entity_structure_fk ASC     
      
              
      
        OPEN cur_v_event;
        FETCH NEXT FROM cur_v_event INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk


         WHILE @@FETCH_STATUS = 0 
         BEGIN
                    

		   TRUNCATE TABLE #TS_EVENT_DATA
		   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
		   TRUNCATE TABLE #TS_EVENT_DATA_RULES      

     
			IF @vm$table_name IN ( 'PRODUCT_IDENTIFICATION','PRODUCT_IDENT_MULT','PRODUCT_IDENT_NONUNIQUE' )
			   EXEC synchronizer.EVENT_POSTING_IMPORT_IDENT
					 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
					,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk
					
		    IF @vm$table_name IN ( 'PRODUCT_STRUCTURE' ) ------JIRA 
			   EXEC synchronizer.EVENT_POSTING_IMPORT_STATUS   ----
					 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
					,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk			
					

			IF @vm$table_name IN ( 'PRODUCT_ATTRIBUTE','PRODUCT_CATEGORY','PRODUCT_DESCRIPTION','PRODUCT_STATUS','PRODUCT_UOM_CLASS', 'PRODUCT_TEXT','SEGMENTS_PRODUCT','PRODUCT_MEDIA_ASSOCIATION')
			   EXEC synchronizer.EVENT_POSTING_IMPORT_PRODUCT
					 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
					,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk
					
					IF @vm$table_name IN ( 'PRODUCT_MULT_TYPE' )
			   EXEC synchronizer.EVENT_POSTING_IMPORT_PRODUCT_MULT_TYPE
					 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
					,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk


		EXEC synchronizer.EVENT_POSTING_IMPORT_EVENT_DATA
					 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
					,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk


        FETCH NEXT FROM cur_v_event INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk
										


     END --cur_v_event LOOP
     CLOSE cur_v_event;
	 DEALLOCATE cur_v_event; 
     

SELECT @v_input_rows = COUNT(1)
						FROM stage.STG_RECORD SREC
						INNER JOIN import.IMPORT_RECORD_DATA IRD
						ON ( IRD.JOB_FK = SREC.JOB_FK
						AND  IRD.RECORD_NO = SREC.RECORD_NO )
                       WHERE SREC.BATCH_NO = @IN_BATCH_NO;

      SELECT @v_output_rows =COUNT(1)
						FROM stage.STG_RECORD SREC
						WHERE SREC.BATCH_NO = @IN_BATCH_NO;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
--      SET @V_START_TIMESTAMP = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'END PRODUCT EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
	
END
 
------------------------------------------------------------------------------------------
---  ENTITY EVENTS
------------------------------------------------------------------------------------------
IF @V_ENTITY_CLASS_CR_FK  in (select code_ref_id from epacube.code_ref
							   where code_type = 'ENTITY_CLASS'
							   AND CODE in ('CUSTOMER', 'SALESREP', 'TERRITORY',
							  'BUYER', 'VENDOR', 'WAREHOUSE'))  -- ENTITY

BEGIN


      SELECT @v_input_rows = COUNT(1)
						FROM stage.STG_RECORD SREC
						INNER JOIN import.IMPORT_RECORD_DATA IRD
						ON ( IRD.JOB_FK = SREC.JOB_FK
						AND  IRD.RECORD_NO = SREC.RECORD_NO )
                       WHERE SREC.BATCH_NO = @IN_BATCH_NO;

      SELECT @v_output_rows =COUNT(1)
						FROM stage.STG_RECORD SREC
						WHERE SREC.BATCH_NO = @IN_BATCH_NO;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_START_TIMESTAMP = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'START ENTITY EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
	
-----

      DECLARE  cur_v_event cursor local FOR
			 SELECT 
					A.import_job_fk,
					A.batch_no,			        
					A.event_type_cr_fk,
					A.table_name,
					A.data_name_fk,
					A.org_entity_structure_fk				
			  FROM (
				SELECT DISTINCT
					   ISNULL ( SREC.job_fk, -999 ) import_job_fk
					  ,ISNULL ( SREC.batch_no, -999 ) batch_no				       
					  ,ISNULL ( DS.event_type_cr_fk, 711 ) event_type_cr_fk
					  ,ds.TABLE_NAME AS table_name	
					  ,ISNULL ( dn.data_name_id, -999 ) AS data_name_fk
					  ,( SELECT CASE WHEN ( @V_COUNT < @v_import_batch_size ) THEN -999
							         ELSE CASE ISNULL ( DS.ORG_IND, 0 ) 
							              WHEN 1 THEN SRECO.ENTITY_STRUCTURE_FK
							              ELSE 1
                                     END
					             END ) AS org_entity_structure_fk	
				FROM import.v_import vm
				INNER JOIN STAGE.stg_record srec WITH (NOLOCK) ON ( srec.import_package_fk = vm.import_package_fk )
				INNER JOIN epacube.data_name dn with (nolock) on ( dn.data_name_id = vm.data_name_fk )
				INNER JOIN epacube.data_set ds with (nolock) on ( ds.data_set_id = dn.data_set_fk )
                LEFT JOIN STAGE.STG_RECORD_ENTITY SRECO WITH (NOLOCK)
                  ON ( SREC.STG_RECORD_ID = SRECO.STG_RECORD_FK 
                  AND  SRECO.ENTITY_CLASS_CR_FK = 10101 )								
				WHERE srec.batch_no = @in_batch_no
				AND   dn.record_status_cr_fk = 1
				AND   ds.event_type_cr_fk IN ( 154 )  --- include only entity
                AND   dn.data_name_id not in ( 149000 )
			) A
			  ORDER BY A.import_job_fk DESC, A.event_type_cr_fk ASC, A.table_name ASC, A.data_name_fk ASC, A.org_entity_structure_fk ASC     
      
              
      
        OPEN cur_v_event;
        FETCH NEXT FROM cur_v_event INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk


         WHILE @@FETCH_STATUS = 0 
         BEGIN
                    

		   TRUNCATE TABLE #TS_EVENT_DATA
		   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
		   TRUNCATE TABLE #TS_EVENT_DATA_RULES      

     
			
			IF @vm$table_name IN ( 'ENTITY_IDENTIFICATION','ENTITY_IDENT_MULT','ENTITY_IDENT_NONUNIQUE' )
			   EXEC synchronizer.EVENT_POSTING_IMPORT_ENTITY_IDENT
					 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
					,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk

			IF @vm$table_name IN ( 'ENTITY_ATTRIBUTE','ENTITY_CATEGORY','ENTITY_DESCRIPTION','ENTITY_STATUS','SETTINGS_SEGMENTS','SEGMENTS_CONTACTS','SEGMENTS_CUSTOMER' )
			   EXEC synchronizer.EVENT_POSTING_IMPORT_ENTITY_DATA
					 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
					,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk

			IF @vm$table_name IN ( 'ENTITY_MULT_TYPE' )
			   EXEC synchronizer.EVENT_POSTING_IMPORT_ENTITY_MULT_TYPE
					 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
					,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk



		EXEC synchronizer.EVENT_POSTING_IMPORT_EVENT_DATA
					 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
					,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk


        FETCH NEXT FROM cur_v_event INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk
										


     END --cur_v_event LOOP
     CLOSE cur_v_event;
	 DEALLOCATE cur_v_event; 
     

SELECT @v_input_rows = COUNT(1)
						FROM stage.STG_RECORD SREC
						INNER JOIN import.IMPORT_RECORD_DATA IRD
						ON ( IRD.JOB_FK = SREC.JOB_FK
						AND  IRD.RECORD_NO = SREC.RECORD_NO )
                       WHERE SREC.BATCH_NO = @IN_BATCH_NO;

      SELECT @v_output_rows =COUNT(1)
						FROM stage.STG_RECORD SREC
						WHERE SREC.BATCH_NO = @IN_BATCH_NO;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
--      SET @V_START_TIMESTAMP = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'END ENTITY EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
	 
END

------------------------------------------------------------------------------------------
---  PRODUCT TAX ATTRIBUTE EVENTS
------------------------------------------------------------------------------------------


      DECLARE  cur_v_taxatt cursor local FOR
			 SELECT 
					A.import_job_fk,
					A.batch_no,			        
					A.event_type_cr_fk,
					A.table_name,
					A.data_name_fk,	
					a.org_entity_structure_fk	
			  FROM (
				SELECT DISTINCT
					   ISNULL ( SREC.job_fk, -999 ) import_job_fk
					  ,ISNULL ( SREC.batch_no, -999 ) batch_no				       
					  ,ISNULL ( DS.event_type_cr_fk, 711 ) event_type_cr_fk
					  ,ds.TABLE_NAME AS table_name	
					  ,ISNULL ( dn.data_name_id, -999 ) AS data_name_fk
					  ,1 AS org_entity_structure_fk
				FROM STAGE.stg_record srec WITH (NOLOCK) 
				INNER JOIN import.IMPORT_RECORD_NAME_VALUE VM  WITH (NOLOCK) 
				  ON ( VM.JOB_FK = SREC.JOB_FK 
				  AND  VM.RECORD_NO = SREC.RECORD_NO )
				INNER JOIN epacube.data_name dn with (nolock) on ( dn.name = vm.data_name )
				INNER JOIN epacube.data_set ds with (nolock) on ( ds.data_set_id = dn.data_set_fk )
				WHERE srec.batch_no = @in_batch_no
				AND   dn.record_status_cr_fk = 1
				AND   ds.event_type_cr_fk IN ( 153 )  --- include only product tax attributes
			) A
			  ORDER BY A.import_job_fk DESC, A.event_type_cr_fk ASC, A.table_name ASC, A.data_name_fk ASC
      
              
      
        OPEN cur_v_taxatt;
        FETCH NEXT FROM cur_v_taxatt INTO  @vm$import_job_fk
                                          ,@vm$batch_no
									      ,@vm$event_type_cr_fk
										  ,@vm$table_name 										
										  ,@vm$data_name_fk
										  ,@vm$org_entity_structure_fk										  


         WHILE @@FETCH_STATUS = 0 
         BEGIN
                    

		   TRUNCATE TABLE #TS_EVENT_DATA
		   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
		   TRUNCATE TABLE #TS_EVENT_DATA_RULES      

			   
	    EXEC synchronizer.EVENT_POSTING_IMPORT_PRODUCT_TAX
					 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
					,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk
			

		EXEC synchronizer.EVENT_POSTING_IMPORT_EVENT_DATA
					 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
					,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk


        FETCH NEXT FROM cur_v_taxatt INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk

     END --cur_v_taxatt LOOP
     CLOSE cur_v_taxatt;
	 DEALLOCATE cur_v_taxatt; 
     



-------------------------------------------------------------------
---  SHEET RESULTS
-------------------------------------------------------------------	
     

      DECLARE  cur_v_results cursor local FOR
			 SELECT 
					A.import_job_fk,
					A.batch_no,			        
					A.event_type_cr_fk,
					A.table_name,
					A.data_name_fk,
					A.org_entity_structure_fk				
			  FROM (
				SELECT DISTINCT
					   ISNULL ( SREC.job_fk, -999 ) import_job_fk
					  ,ISNULL ( SREC.batch_no, -999 ) batch_no				       
					  ,ISNULL ( DS.event_type_cr_fk, 711 ) event_type_cr_fk
					  ,ds.TABLE_NAME AS table_name	
					  ,ISNULL ( dn.data_name_id, -999 ) AS data_name_fk
					  ,( SELECT CASE WHEN ( @V_COUNT < @v_import_batch_size ) THEN -999
							         ELSE CASE ISNULL ( DS.ORG_IND, 0 ) 
							              WHEN 1 THEN SRECO.ENTITY_STRUCTURE_FK
							              ELSE 1
                                     END
					             END ) AS org_entity_structure_fk	
				FROM import.v_import vm
				INNER JOIN STAGE.stg_record srec WITH (NOLOCK) ON ( srec.import_package_fk = vm.import_package_fk )
				INNER JOIN epacube.data_name dn with (nolock) on ( dn.data_name_id = vm.data_name_fk )
				INNER JOIN epacube.data_set ds with (nolock) on ( ds.data_set_id = dn.data_set_fk )
                LEFT JOIN STAGE.STG_RECORD_ENTITY SRECO WITH (NOLOCK)
                  ON ( SREC.STG_RECORD_ID = SRECO.STG_RECORD_FK 
                  AND  SRECO.ENTITY_CLASS_CR_FK = 10101 )								
				WHERE srec.batch_no = @in_batch_no
				AND   dn.record_status_cr_fk = 1
				AND   ds.event_type_cr_fk IN ( 152 )  --- include only sheet results
			) A
			  ORDER BY A.import_job_fk DESC, A.event_type_cr_fk ASC, A.table_name ASC, A.data_name_fk ASC, A.org_entity_structure_fk ASC     
      
              
      
        OPEN cur_v_results;
        FETCH NEXT FROM cur_v_results INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk


         WHILE @@FETCH_STATUS = 0 
         BEGIN

		   TRUNCATE TABLE #TS_EVENT_DATA
		   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
		   TRUNCATE TABLE #TS_EVENT_DATA_RULES      
                         
		  -- EXEC synchronizer.EVENT_POSTING_IMPORT_RESULTS 
				-- @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
				--,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk 

		   EXEC synchronizer.EVENT_POSTING_IMPORT_EVENT_DATA
				 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
				,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk

        FETCH NEXT FROM cur_v_results INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk


     END --cur_v_results LOOP
     CLOSE cur_v_results;
	 DEALLOCATE cur_v_results; 


SELECT @v_input_rows = COUNT(1)
						FROM stage.STG_RECORD SREC
						INNER JOIN import.IMPORT_RECORD_DATA IRD
						ON ( IRD.JOB_FK = SREC.JOB_FK
						AND  IRD.RECORD_NO = SREC.RECORD_NO )
                       WHERE SREC.BATCH_NO = @IN_BATCH_NO;

      SELECT @v_output_rows =COUNT(1)
						FROM stage.STG_RECORD SREC
						WHERE SREC.BATCH_NO = @IN_BATCH_NO;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
--      SET @V_START_TIMESTAMP = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'END SHEET RESULT EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
	 

-------------------------------------------------------------------
---  PRODUCT VALUES 
-------------------------------------------------------------------	


--      DECLARE  cur_v_values cursor local FOR
--			 SELECT 
--					A.import_job_fk,
--					A.batch_no,			        
--					A.event_type_cr_fk,
--					A.table_name,
--					A.data_name_fk,
--					A.org_entity_structure_fk				
--			  FROM (
--				SELECT DISTINCT
--					   ISNULL ( SREC.job_fk, -999 ) import_job_fk
--					  ,ISNULL ( SREC.batch_no, -999 ) batch_no				       
--					  ,ISNULL ( DS.event_type_cr_fk, 711 ) event_type_cr_fk
--					  ,ds.TABLE_NAME AS table_name	
--					  ,ISNULL ( dn.parent_data_name_fk, -999 ) AS data_name_fk
--					  ,( SELECT CASE WHEN ( @V_COUNT < @v_import_batch_size ) THEN -999
--							         ELSE CASE ISNULL ( DS.ORG_IND, 0 ) 
--							              WHEN 1 THEN SRECO.ENTITY_STRUCTURE_FK
--							              ELSE 1
--                                     END
--					             END ) AS org_entity_structure_fk	
--				FROM import.v_import vm
--				INNER JOIN STAGE.stg_record srec WITH (NOLOCK) ON ( srec.import_package_fk = vm.import_package_fk )
--				INNER JOIN epacube.data_name dn with (nolock) on ( dn.data_name_id = vm.data_name_fk )
--				INNER JOIN epacube.data_set ds with (nolock) on ( ds.data_set_id = dn.data_set_fk )
--                LEFT JOIN STAGE.STG_RECORD_ENTITY SRECO WITH (NOLOCK)
--                  ON ( SREC.STG_RECORD_ID = SRECO.STG_RECORD_FK 
--                  AND  SRECO.ENTITY_CLASS_CR_FK = 10101 )								
--				WHERE srec.batch_no = @in_batch_no
--				AND   dn.record_status_cr_fk = 1
--				AND   ds.event_type_cr_fk IN ( 151 )  --- include only PRODUCT VALUES
--			) A
--			  ORDER BY A.import_job_fk DESC, A.event_type_cr_fk ASC, A.table_name ASC, A.data_name_fk ASC, A.org_entity_structure_fk ASC     
      
              
      
--        OPEN cur_v_values;
--        FETCH NEXT FROM cur_v_values INTO @vm$import_job_fk
--                                        ,@vm$batch_no
--										,@vm$event_type_cr_fk
--										,@vm$table_name 										
--										,@vm$data_name_fk
--										,@vm$org_entity_structure_fk


--         WHILE @@FETCH_STATUS = 0 
--         BEGIN

--		   TRUNCATE TABLE #TS_EVENT_DATA
--		   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
--		   TRUNCATE TABLE #TS_EVENT_DATA_RULES      
                         
--		   EXEC synchronizer.EVENT_POSTING_IMPORT_VALUES 
--				 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
--				,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk  

--		   EXEC synchronizer.EVENT_POSTING_IMPORT_EVENT_DATA
--				 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
--				,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk

--        FETCH NEXT FROM cur_v_values INTO @vm$import_job_fk
--                                        ,@vm$batch_no
--										,@vm$event_type_cr_fk
--										,@vm$table_name 										
--										,@vm$data_name_fk
--										,@vm$org_entity_structure_fk


--     END --cur_v_values LOOP
--     CLOSE cur_v_values;
--	 DEALLOCATE cur_v_values; 


--SELECT @v_input_rows = COUNT(1)
--						FROM stage.STG_RECORD SREC
--						INNER JOIN import.IMPORT_RECORD_DATA IRD
--						ON ( IRD.JOB_FK = SREC.JOB_FK
--						AND  IRD.RECORD_NO = SREC.RECORD_NO )
--                       WHERE SREC.BATCH_NO = @IN_BATCH_NO;

--      SELECT @v_output_rows =COUNT(1)
--						FROM stage.STG_RECORD SREC
--						WHERE SREC.BATCH_NO = @IN_BATCH_NO;

--	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @V_START_TIMESTAMP = getdate()
--	  SET @V_END_TIMESTAMP = getdate()
--      EXEC common.job_execution_create  @in_job_fk, 'END PRODUCT VALUE EVENTS',
--		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
--										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
	 


-------------------------------------------------------------------
---  PRODUCT PRICESHEET BASIS VALUES 
-------------------------------------------------------------------	


      DECLARE  cur_v_values cursor local FOR
			 SELECT 
					A.import_job_fk,
					A.batch_no,			        
					A.event_type_cr_fk,
					A.table_name,
					A.data_name_fk,
					A.org_entity_structure_fk				
			  FROM (
				SELECT DISTINCT
					   ISNULL ( SREC.job_fk, -999 ) import_job_fk
					  ,ISNULL ( SREC.batch_no, -999 ) batch_no				       
					  ,ISNULL ( DS.event_type_cr_fk, 711 ) event_type_cr_fk
					  ,ds.TABLE_NAME AS table_name	
					  ,ISNULL ( dn.data_name_id, -999 ) AS data_name_fk
					  ,( SELECT CASE WHEN ( @V_COUNT < @v_import_batch_size ) THEN -999
							         ELSE CASE ISNULL ( DS.ORG_IND, 0 ) 
							              WHEN 1 THEN SRECO.ENTITY_STRUCTURE_FK
							              ELSE 1
                                     END
					             END ) AS org_entity_structure_fk	
				FROM import.v_import vm
				INNER JOIN STAGE.stg_record srec WITH (NOLOCK) ON ( srec.import_package_fk = vm.import_package_fk )
				INNER JOIN epacube.data_name dn with (nolock) on ( dn.data_name_id = vm.data_name_fk )
				INNER JOIN epacube.data_set ds with (nolock) on ( ds.data_set_id = dn.data_set_fk )
                LEFT JOIN STAGE.STG_RECORD_ENTITY SRECO WITH (NOLOCK)
                  ON ( SREC.STG_RECORD_ID = SRECO.STG_RECORD_FK 
                  AND  SRECO.ENTITY_CLASS_CR_FK = 10101 )								
				WHERE srec.batch_no = @in_batch_no
				AND   dn.record_status_cr_fk = 1
				AND   ds.event_type_cr_fk IN ( 151,158 )  --- include only PRODUCT BASIS VALUES
			) A
			  ORDER BY A.import_job_fk DESC, A.event_type_cr_fk ASC, A.table_name ASC, A.data_name_fk ASC, A.org_entity_structure_fk ASC     
      
              
      
        OPEN cur_v_values;
        FETCH NEXT FROM cur_v_values INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk


         WHILE @@FETCH_STATUS = 0 
         BEGIN

		   TRUNCATE TABLE #TS_EVENT_DATA
		   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
		   TRUNCATE TABLE #TS_EVENT_DATA_RULES      
                         
		   EXEC synchronizer.EVENT_POSTING_IMPORT_BASIS_VALUES 
				 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
				,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk  

		   EXEC synchronizer.EVENT_POSTING_IMPORT_EVENT_DATA
				 @in_batch_no, @vm$import_job_fk, @vm$event_type_cr_fk
				,@vm$table_name, @vm$data_name_fk, @vm$org_entity_structure_fk

        FETCH NEXT FROM cur_v_values INTO @vm$import_job_fk
                                        ,@vm$batch_no
										,@vm$event_type_cr_fk
										,@vm$table_name 										
										,@vm$data_name_fk
										,@vm$org_entity_structure_fk


     END --cur_v_values LOOP
     CLOSE cur_v_values;
	 DEALLOCATE cur_v_values; 


SELECT @v_input_rows = COUNT(1)
						FROM stage.STG_RECORD SREC
						INNER JOIN import.IMPORT_RECORD_DATA IRD
						ON ( IRD.JOB_FK = SREC.JOB_FK
						AND  IRD.RECORD_NO = SREC.RECORD_NO )
                       WHERE SREC.BATCH_NO = @IN_BATCH_NO;

      SELECT @v_output_rows =COUNT(1)
						FROM stage.STG_RECORD SREC
						WHERE SREC.BATCH_NO = @IN_BATCH_NO;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
--      SET @V_START_TIMESTAMP = getdate()
	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'END PRODUCT BASIS VALUE EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;
	 



--------------------------------------------------------------------------------------------
--  DROP TEMP TABLES
--------------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA') is not null
	   drop table #TS_EVENT_DATA;

--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA_ERRORS') is not null
	   drop table #TS_EVENT_DATA_ERRORS


--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA_RULES') is not null
	   drop table #TS_EVENT_DATA_RULES




------------------------------------------------------------------------------------------
---  DEFAULTS and DERIVATIONS 
---			( Same Rrocedure called from synchronizer.EVENT_POST_EDITOR_DATA )
------------------------------------------------------------------------------------------

--    EXEC synchronizer.EVENT_POST_DERIVATION @in_batch_no


------------------------------------------------------------------------------------------
---  REMAINING EVENTS FOR PROCESSING
--		COMPLETENESS, DERIVATIONS, RESULTS AND VALUE EVENTS SHOULD BE AT PENDING STATUS
--		ASSIGN NEW BATCH_NO AND IMPORT_BATCH_NO TO SEPARATE FROM OTHER EVENTS
--		PROCESS THESE EVENTS VIA EVENT_POSTING PASSING NEW BATCH_NO
------------------------------------------------------------------------------------------


/* ----------------------------------------------------------------------------------- */
--    Use the same batch no for both import_batch_no and batch_no 
--		So that every event will not automatically go to 66  ( which bypassed Auto Sync )
/* ----------------------------------------------------------------------------------- */


------	EXEC seq_manager.db_get_next_sequence_value_impl 'common','batch_seq', @v_batch_no output
--------       ,BATCH_NO = @v_batch_no    --- leave the batch the same for the derivations


    UPDATE synchronizer.EVENT_DATA
    SET EVENT_STATUS_CR_FK = 87  --- SUBMITTED
       ,IMPORT_BATCH_NO = @in_batch_no    --- this was previously commented out ( not sure effect on completeness ?? )
    WHERE EVENT_ID IN (
                SELECT ED.EVENT_ID
				FROM synchronizer.EVENT_DATA ED WITH (NOLOCK)
				WHERE ED.batch_no = @in_batch_no
				AND   ED.EVENT_PRIORITY_CR_FK = 69  
                UNION 								
                SELECT ED.EVENT_ID
				FROM synchronizer.EVENT_DATA ED WITH (NOLOCK)
				WHERE ED.batch_no = @in_batch_no
				AND   ED.EVENT_SOURCE_CR_FK = 72  -- DERIVATIONS 
               )     


       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows sent to EVENT_POSTING = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
       
			EXEC synchronizer.EVENT_POSTING @in_batch_no
           
       END





------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_IMPORT'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @in_batch_no
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_IMPORT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END



























































































