﻿





-- Copyright 2017
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing from UI.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        03/20/2017   Initial SQL Version
-- CV        12/18/2017   Fixed Action type to look at the code ref table
 


CREATE PROCEDURE [synchronizer].[EVENT_POSTING_COMPONENT_UI] 
        ( @in_product_structure_fk BIGINT,		@in_Vendor_entity_structure_fk BIGINT, 
		  @in_cust_entity_structure_fk BIGINT,	@in_zone_entity_structure_fk Bigint,  
		  @in_dept_entity_structure_fk Bigint,  @in_org_entity_structure_fk bigint,
		  @in_assoc_entity_structure_fk Bigint, 
		  @in_batch_no BIGINT,
          @in_table_id bigint,
          @in_data_name_fk BIGINT,
		  @in_New_Data Varchar(256),	
		  @in_DV_DESC Varchar(256),	 
		  @in_data_value_fk bigint,
		  @in_entity_data_value_fk bigint,
		  @in_prod_data_value_fk bigint,
		  @in_cust_data_value_fk bigint,
		  @in_result_data_value_fk bigint,
		  @in_chain_id_data_value_fk bigint,
		  @in_data_level_cr_fk int,
		  @in_Precedence INT,  -----probably can take this out getting it from data level
		  @in_Pack int,
		  @in_Product_Recipe_fk BIGINT,
		  @in_effective_date varchar(50),
		  @UserID Varchar(64))


		 
AS
BEGIN

 DECLARE @ls_exec            nvarchar (max)
 DECLARE @ls_exec2           nvarchar (max)
 DECLARE @ls_exec3           nvarchar (max) 
 DECLARE @ls_exec4           nvarchar (max) 
 DECLARE @l_exec_no          bigint
 DECLARE @l_rows_processed   bigint
 DECLARE @l_sysdate          datetime


DECLARE  @v_input_rows       bigint
DECLARE  @v_output_rows      bigint
DECLARE  @v_output_total     bigint
DECLARE  @v_exception_rows   bigint
DECLARE  @v_job_date         DATETIME
DECLARE  @v_count			 BIGINT
DECLARE  @v_entity_class_cr_fk  int
DECLARE  @v_import_package_fk   int;

DECLARE @l_stmt_no           bigint
DECLARE @ls_stmt             varchar(4000)
DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

Declare @in_Table_name varchar(128)
Declare @in_current_data varchar(256)
Declare @in_event_type_cr_fk int

  

 ----------------------------------------------------------------------------------------------
 -------GET TABLE NAME WHERE DATA LIVES FROM DATA NAME LEVELS --------------------------------
 ----------------------------------------------------------------------------------------------

 
set @in_table_name = (select TABLE_NAME from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk
and ISNULL( @in_data_level_cr_fk,0) = ISNULL(data_level_cr_fk,0)  )

if @in_table_name is NULL
BEGIN  

set @in_table_name = (select TABLE_NAME from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk)

END

--select *  from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK = 550301
----and data_level_cr_fk =509

--select * from epacube.data_set where data_set_id = 1008

---------------------------------------------------------------------------------------------------
----PRODUCT ASSOCIATIONS AND VIRTUAL DATA NAMES 
--------------------------------------------------------------------------------------------------

if @in_table_name = 'PRODUCT_ASSOCIATION'   

-----add ACTION TYPE----

--CheckBoxTransform  select * from epacube.data_name_virtual where Action_type = 'CheckBoxTransform'

BEGIN
			if @in_new_data = 'N'  AND @in_data_name_fk = 550003  ----- inactivate record code 53
				begin
			set @in_new_data = '-999999999'
				end
			
			if @in_new_data = 'Y'  AND @in_data_name_fk = 550003  ----- inactivate record code 53
				begin
			set @in_new_data = 'ACTIVATE'
				end

--WhenRecordExists  select * from epacube.data_name_virtual where Action_type = 'WhenRecordExists'
select * from epacube.code_ref where code_type ='VIRTUAL_ACTION_TYPE' and code = 'WhenRecordExists'

			--if @in_new_data = 'N'  AND @in_data_name_fk = 550001  ----- purge record code 59
		if @in_new_data = 'N'  AND @in_data_name_fk in (select data_name_fk from epacube.data_name_virtual where Virtual_Action_type_cr_fk  = (select code_ref_id from epacube.code_ref where code_type ='VIRTUAL_ACTION_TYPE' and code = 'WhenRecordExists'))
				begin
			set @in_new_data = 'PURGE'
				end
				
			if @in_new_data = 'Y'  
			AND @in_data_name_fk in (select data_name_fk from epacube.data_name_virtual where Virtual_Action_type_cr_fk  = (select code_ref_id from epacube.code_ref where code_type ='VIRTUAL_ACTION_TYPE' and code = 'WhenRecordExists'))
			--AND @in_data_name_fk = 550001  ----- purge record code 59
				begin
			set @in_new_data = (select value from epacube.entity_identification where entity_structure_fk = @in_cust_entity_structure_fk and data_name_fk =144111 )
				end

				
END

--DECLARE @in_VDATA_NAME bigint

--select * from epacube.DATA_NAME_GROUP_REFS

--set @in_VDATA_NAME = (select ISNULL(v_ref_data_name_fk,999) from epacube.DATA_NAME_GROUP_REFS where Data_Name_FK =  @in_data_name_fk)

--set @in_data_name_fk = (select case when @in_Vdata_name =999 then @in_data_name_fk else @in_vdata_name end) 

-------------------------------------------------------------------------------

----BEGIN TRY
----SET NOCOUNT ON;
----SET @l_sysdate = getdate ()
----SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_COMPONENT_UI.'								 				 
----					 + ' Event Type: ' + cast(isnull(@in_event_type_cr_fk, 0) as varchar(30))					 				 
----					 + ' Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))	
----					 + ' Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))	
					 						 					 				 				 					 					 
----EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
----                                          @epa_batch_no = @in_batch_no,
----						   				  @exec_id   = @l_exec_no OUTPUT;


Set @in_Precedence = (select isNULL(PRECEDENCE,NULL) from epacube.CODE_REF where CODE_REF_ID =   @in_data_level_cr_fk)
-------------------------------------------------------------------------------------------------------------------------------

Set @v_entity_class_cr_fk =  (select Entity_Class_CR_FK from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk
and ISNULL( @in_data_level_cr_fk,0) = ISNULL(data_level_cr_fk,0)  )

if @v_entity_class_cr_fk is NULL
BEGIN  

set @v_entity_class_cr_fk = (select Entity_Class_CR_FK from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk)

END

---------------------------------------------------------------------------------------------------------------------------------------------

SET @in_event_type_cr_fk =  (select event_type_cr_fk from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk
and ISNULL( @in_data_level_cr_fk,0) = ISNULL(data_level_cr_fk,0)  )

if @in_event_type_cr_fk is NULL
BEGIN  

set @in_event_type_cr_fk = (select EVENT_TYPE_CR_FK from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk)

END


--------------------------------------------------------------------------------------------------------------

BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_COMPONENT_UI.'								 				 
					 + ' Event Type: ' + cast(isnull(@in_event_type_cr_fk, 0) as varchar(30))					 				 
					 + ' Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))	
					 + ' Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))	
					 						 					 				 				 					 					 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUT

----------------------------------------------------------------------------------------------

---2 PATHS TO TAKE 1. Existing Data  2. New Data

-----------------------------------------------------------------------------------------------
----PATH 1 

IF  @in_table_id is NOT NULL 

BEGIN

IF @in_table_name IN ( 'PRODUCT_ATTRIBUTE','PRODUCT_CATEGORY','PRODUCT_DESCRIPTION', 'PRODUCT_TEXT'
					,'PRODUCT_RECIPE_ATTRIBUTE','SETTINGS_POS_PROD_STORE_VALUES','SETTINGS_POS_PROD_STORE_HEADER','PRICESHEET_BASIS_VALUES','#PBV'
					,'PRODUCT_STATUS','SEGMENTS_PRODUCT','PRODUCT_RECIPE_INGREDIENT_XREF','PRODUCT_RECIPE_INGREDIENT','PRODUCT_ASSOCIATION','PRODUCT_MULT_TYPE','PRODUCT_GTIN' )   
--,'PRODUCT_UOM_CLASS',MARGIN_RESULTS,PRICESHEET,PRICESHEET_PROMOTIONAL_ALLOWANCES,PRICESHEET_PURCHASE_ALLOWANCES,PRODUCT_FACTOR
		
		EXECUTE  [synchronizer].[EVENT_POSTING_COMPONENT_PRODUCT_DATA] 
															   @in_table_id
															  ,@in_product_structure_fk 
															  ,@in_Product_Recipe_fk
															  ,@in_data_name_fk
															  ,@in_data_level_cr_fk
															  ,@in_batch_no
															  ,@in_New_Data
															  ,@in_entity_data_value_fk
															  ,@in_Pack
															  ,@UserID
 


IF @in_table_name IN ('ENTITY_IDENT_NONUNIQUE','ENTITY_ATTRIBUTE','ENTITY_CATEGORY','ENTITY_MULT_TYPE','SEGMENTS_SETTINGS','SETTINGS_POS_STORE' )
--SETTINGS_TAGS,RULES_CONTRACT,SEGMENTS_COMPETITOR,SEGMENTS_VENDOR,SEGMENTS_ZONE_ASSOCIATION
			   
			   EXEC synchronizer.EVENT_POSTING_COMPONENT_ENTITY_DATA
															   @in_table_id
															  ,@in_data_name_fk
															  ,@in_data_level_cr_fk
															  ,@in_batch_no
															  ,@in_New_Data
															  ,@UserID




--IF @in_table_name IN ('DATA_VALUE')
			   
--			   EXEC synchronizer.EVENT_POSTING_COMPONENT_ENTITY_DATA
--															   @in_table_id
--															  ,@in_data_name_fk
--															  ,@in_data_level_cr_fk
--															  ,@in_batch_no
--															  ,@in_New_Data
--															  ,@UserID


END

----------------------------------------------------------------------------------------------------------------------------------------------------------
---PATH 2.  NEW DATA EVENT
-----------------------------------------------------------------------------------------------------------------------------------------------------------


IF  @in_table_id is NULL   BEGIN

-------------------------------------------------------------------------------------------------------------
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_COMPONENT_UI -- NEW DATA EVENT.'								 				 
					 + ' Event Type: ' + cast(isnull(@in_event_type_cr_fk, 0) as varchar(30))					 				 
					 + ' Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))	
					 + ' Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))
					 
					 						 					 				 				 					 					 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;

------------------------------------------------------------------------------------------------------------------

if @in_table_name   = '#PBV'   ---'PRICESHEET_BASIS_VALUES' or/
 BEGIN
  
 if  isnumeric(@in_New_data) = 0 

 raiserror ('NUMERIC REQUIRED', 10,1) with nowait

 END


 if @in_table_name   = '#PBV'   ---'PRICESHEET_BASIS_VALUES' or/
 BEGIN


DECLARE 
		@in_net_value1_new numeric(18,6),
		@in_net_value1_cur numeric(18,6),
		@in_net_value2_new numeric(18,6)   ---- Unit Value 



set @in_net_value1_new = @in_New_Data
set @in_New_Data = NULL
set @in_net_value2_new = case when @in_data_name_fk = 503201 then (@in_net_value1_new/isNULL(@in_pack,1)) else NULL END

 END

-------------------------------------------------------------------------------------------------------------------





--get batch number - UI will batch them all up and post at one time
	
	--EXEC seq_manager.db_get_next_sequence_value_impl 'common',
 --                   'batch_seq', @in_batch_no output



	INSERT INTO [synchronizer].[EVENT_DATA]
           (
		   [EVENT_TYPE_CR_FK]
         ,[EVENT_ENTITY_CLASS_CR_FK]
            ,[EVENT_EFFECTIVE_DATE]
		   ,[PRODUCT_STRUCTURE_FK]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]        
           ,[NEW_DATA]
           ,[CURRENT_DATA]
		   ,NET_VALUE1_NEW
		   ,NET_VALUE2_NEW
		   ,NET_VALUE1_CUR
		   ,NET_VALUE2_CUR
		   ,PACK
		   ,[ORG_ENTITY_STRUCTURE_FK]
		   ,[CUST_ENTITY_STRUCTURE_FK]
           ,[ZONE_ENTITY_STRUCTURE_FK]
           ,[DEPT_ENTITY_STRUCTURE_FK]
           ,[VENDOR_ENTITY_STRUCTURE_FK]
		   ,CO_ENTITY_STRUCTURE_FK
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[TABLE_ID_FK]
           ,[DATA_LEVEL]
           ,[BATCH_NO]
           ,[DATA_VALUE_FK]
           ,[ENTITY_DATA_VALUE_FK]
		   ,[PROD_DATA_VALUE_FK]
           ,[CUST_DATA_VALUE_FK]
		   ,CHAIN_ID_DATA_VALUE_FK
		   ,SEARCH1
           ,[RESULT_DATA_VALUE_FK]
           ,[PRECEDENCE]
		   ,IMPORT_FILENAME
		   ,VALUE_EFFECTIVE_DATE
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
		  )
 SELECT 
 ISNULL(@in_EVENT_TYPE_CR_FK,150)
,ISNULL(@v_ENTITY_CLASS_CR_FK,10109)
,ISNULL(cast(@in_effective_date as date),getdate())
,case when @v_entity_class_cr_fk = 10109 then @in_product_structure_fk else @in_cust_entity_structure_fk END
,case when @v_entity_class_cr_fk = 10109 then @in_product_structure_fk else @in_cust_entity_structure_fk END
,@in_data_name_fk
,@in_new_data
,@in_current_data
,ISNULL(@in_net_value1_new,-999)
,@in_net_value2_new 
,ISNULL(@in_net_value1_cur,-999)
,NULL Net_value2_cur
,@in_pack AS PACK
,case WHEN @in_org_entity_structure_fk = 0 THEN ISNULL(@in_org_entity_structure_fk,1) ELSE @in_org_entity_structure_fk END
,@in_cust_entity_structure_fk
,@in_Zone_entity_structure_fk
,@in_dept_entity_structure_fk
,@in_Vendor_entity_structure_fk
,101
,97
,88
,66
,71
,case when @in_table_id is NULL then 51 else 52 End 
,@in_table_id
,@in_data_level_cr_fk
,@in_batch_no -- batch
,@in_data_value_fk
,@in_entity_data_value_fk  
,@in_prod_data_value_fk  
,@in_CUST_DATA_VALUE_FK
,@in_chain_id_data_value_fk
,@in_Product_Recipe_fk
,@in_RESULT_DATA_VALUE_FK
,@in_PRECEDENCE
,@in_Table_name
,ISNULL(cast(@in_effective_date as date),getdate())
,1
,getdate()
,@UserID



-------------------------------------------------------------------------------------------
---setting new data data values

							UPDATE ED 
							set ED.data_value_fk =   dv.DATA_VALUE_ID 
							from epacube.data_name DN 
							 inner join epacube.data_value DV on DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK
							 inner join synchronizer.event_data ED on dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and ed.NEW_DATA = dv.VALUE and ed.EVENT_STATUS_CR_FK = 88
							 where DN.source_data_dn_fk is not NULL

							UPDATE ED 
							 SET ED.data_value_fk = DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 inner join synchronizer.EVENT_DATA ED WITH (NOLOCK)  
							 ON DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA
							 INNER JOIN Epacube.data_name DN WITH (nolock) on dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NULL 
							 where ed.EVENT_STATUS_CR_FK = 88
------------------------------------------------------------------------------------------------------------------
--clean up if not net values type of events

update synchronizer.event_data
set NET_VALUE1_NEW = NULL
where NET_VALUE1_NEW = -999.000000
and EVENT_STATUS_CR_FK = 88

update synchronizer.event_data
set NET_VALUE1_cur = NULL
where NET_VALUE1_cur = -999.000000
and EVENT_STATUS_CR_FK = 88

-----check for numeric

            INSERT INTO synchronizer.EVENT_DATA_ERRORS (
                        EVENT_FK,
                        EVENT_CONDITION_CR_FK,
                        ERROR_NAME,
                        UPDATE_TIMESTAMP)
            SELECT
                   EVENT_ID
                  ,99
                  ,'NUMERIC REQUIRED'  --ERROR NAME        
                  ,getdate()
               from synchronizer.event_data where import_filename = '#PBV'
			  and @in_Table_name =  '#PBV'
						 AND isnumeric(new_data) = 0
						  and NET_VALUE1_NEW is NULL
						 and event_status_Cr_fk = 88
						 --AND BATCH_NO = @in_batch_no




----------------------------------------------------------------------------------------------
----   CALL POST  
----------------------------------------------------------------------------------------------

		--EXEC [synchronizer].[EVENT_POSTING] @in_batch_no
		
		 

		--UPDATE  EVENT_DATA
		--SET EVENT_STATUS_CR_FK = 80   -- RETURN TO PENDING
		--WHERE EVENT_STATUS_CR_FK in ( 86, 87, 88 ) 

   --    SET @l_rows_processed = @@ROWCOUNT
	   
	  -- IF @l_rows_processed > 0
	  -- BEGIN
			--SET @status_desc = 'total rows Returned to Pending = '
			--	+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			--EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
   --    END
		

 END

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_COMPONENT_UI'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_UI_DATA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END


















