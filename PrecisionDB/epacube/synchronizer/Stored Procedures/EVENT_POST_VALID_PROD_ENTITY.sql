﻿












-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform validations for entity relationships
--				on product events
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010   Initial SQL Version



CREATE PROCEDURE [synchronizer].[EVENT_POST_VALID_PROD_ENTITY] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_PROD_ENTITY.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;


---------------------------------NULL ORG ENTITY STRUCTURE

			INSERT INTO #TS_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		(	SELECT 
			 TSED.EVENT_FK
			,NULL
			,99
			,'NO WAREHOUSE FOUND'  --ERROR NAME
			,NULL
			,NULL
			,NULL
			,NULL
			,NULL
			,NULL
			,@l_sysdate
			FROM #TS_EVENT_DATA TSED
			WHERE ORG_ENTITY_STRUCTURE_FK IS NULL )


------------------------------------------------------------------------------------------
--  CHECK TO SEE ENTITY REQUIRED. 
------------------------------------------------------------------------------------------

									INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,99
								,'ENTITY REQUIRED'  --ERROR NAME        
								,'DATA REQUIRES AN ENTITY'--ERROR1
								,'CHECK THE DATA SET'
								,NULL
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
								ON (DN.data_name_id = TSED.data_Name_Fk
								AND dn.entity_data_name_fk is not null) --epa 2744 
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
								ON (dn.data_Set_fk = ds.data_Set_Id
								AND ds.entity_structure_ec_cr_FK is not NULL)
								AND tsed.ENTITY_STRUCTURE_FK is NULL
								AND tsed.NEW_DATA <> '-999999999')

-------------------------------------------------------------------------------------------------------
--CHECK IF ORG SPECIFIC DATA SET AND DATA IS VALID
--------------------------------------------------------------------------------------------------------

									INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,99
								,'ALL ORGS ONLY'  --ERROR NAME        
								,'DATA NOT ORG SPECIFIC-VALID FOR ALL ORGS ONLY'--ERROR1
								,'CHECK THE DATA SET ORG FLAG'
								,NULL
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								WHERE tsed.org_ind is null
								AND ISNULL ( tsed.ORG_ENTITY_STRUCTURE_FK, 999 )  > 1 )
								
--------------------------------------------------------------------------------------------------------------
									
									INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,99
								,'ORG SPECIFIC ONLY'  --ERROR NAME        
								,'DATA IS ORG SPECIFIC-NOT VALAD FOR ALL ORGS'--ERROR1
								,'CHECK THE DATA SET CORP IND FLAG'
								,NULL
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								INNER JOIN epacube.data_set ds WITH (NOLOCK) on (ds.data_set_id = tsed.data_set_fk)
								AND ds.corp_ind is null
								AND ISNULL ( tsed.ORG_ENTITY_STRUCTURE_FK, 999 ) = 1 )


------------------------------------------------------------------------------------------
--   Insert Errors for Data Names that expect ASSOCIATION Data Names 
--
--    This SQL CAN NOT address Vendor Relations.. ( Obviously Entity is commented out !!!)
--		Cindy I will have to visit later
------------------------------------------------------------------------------------------

							INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,98
								,'ASSOCIATION FOR EVENT IS MISSING'   --ERROR NAME   
								,'ASSOCIATION NAME'  --ERROR1
								,( SELECT LABEL FROM EPACUBE.DATA_NAME WHERE DATA_NAME_ID = DN.RELATED_ASSOC_DATA_NAME_FK )
								,NULL
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = TSED.DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
								--- ASSOC SETUP
								INNER JOIN EPACUBE.DATA_NAME DNAS WITH (NOLOCK) ON ( DNAS.DATA_NAME_ID = DN.RELATED_ASSOC_DATA_NAME_FK )
								INNER JOIN EPACUBE.DATA_SET DSAS WITH (NOLOCK)  ON ( DSAS.DATA_SET_ID = DNAS.DATA_SET_FK )																
								INNER JOIN EPACUBE.CODE_REF CRAS WITH (NOLOCK) ON ( CRAS.CODE_REF_ID = DSAS.ENTITY_STRUCTURE_EC_CR_FK )																
								--- current value
								LEFT JOIN EPACUBE.PRODUCT_ASSOCIATION PAS WITH (NOLOCK) ON ( PAS.DATA_NAME_FK = DN.RELATED_ASSOC_DATA_NAME_FK
										  AND  PAS.PRODUCT_STRUCTURE_FK = TSED.PRODUCT_STRUCTURE_FK
										  AND  ISNULL ( PAS.ORG_ENTITY_STRUCTURE_FK, 1 ) = ISNULL ( TSED.ORG_ENTITY_STRUCTURE_FK, 1 ))
										 -- AND  ISNULL ( PAS.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) )  --CV commented for testing.
--										  AND  ISNULL ( PAS.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, -999 ) )  --CV 11/02/2009 JIRA ????									 
								--- event in same import
								LEFT JOIN synchronizer.EVENT_DATA EDX WITH (NOLOCK) ON ( EDX.DATA_NAME_FK = DN.RELATED_ASSOC_DATA_NAME_FK
											AND  EDX.PRODUCT_STRUCTURE_FK = TSED.PRODUCT_STRUCTURE_FK
											AND  ISNULL ( EDX.ORG_ENTITY_STRUCTURE_FK, 1 )  = ISNULL ( TSED.ORG_ENTITY_STRUCTURE_FK, 1 )
--											AND  ISNULL ( EDX.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) 
											AND  EDX.EVENT_STATUS_CR_FK IN ( 80, 81, 83, 84, 85, 86, 87, 88, 89 ) )								
								WHERE 1 = 1
								AND   DN.RELATED_ASSOC_DATA_NAME_FK IS NOT NULL
								AND   PAS.PRODUCT_ASSOCIATION_ID IS NULL
								AND   EDX.EVENT_ID IS NULL 
								AND   (
								       CRAS.CODE <> 'WAREHOUSE'
								       OR (
								               CRAS.CODE = 'WAREHOUSE'
								           AND TSED.ORG_ENTITY_STRUCTURE_FK <> 1  )
								      )
                         )





-----------------------------------------------------------------------------
---check that we have a valid entity structure fk for the entity class
-----------------------------------------------------------------------------

INSERT INTO #TS_EVENT_DATA_ERRORS (
									EVENT_FK,
									RULE_FK,
									EVENT_CONDITION_CR_FK,
									ERROR_NAME,
									ERROR_DATA1,
									ERROR_DATA2,
									ERROR_DATA3,
									ERROR_DATA4,
									ERROR_DATA5,
									ERROR_DATA6,
									UPDATE_TIMESTAMP)
							(	SELECT 
								 TSED.EVENT_FK
								,NULL
								,99
								,'INVALID ENTITY VALUE '  --ERROR NAME  
								,( SELECT CODE FROM EPACUBE.CODE_REF WHERE CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )       
								,' DOES NOT EXIST'--ERROR1
								,'VERIFY DATA ENTERED'
								,NULL
								,NULL
								,NULL
								,@l_sysdate
								FROM #TS_EVENT_DATA TSED
								INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = TSED.DATA_SET_FK )
								WHERE tsed.event_type_cr_fk = 155
								AND tsed.ENTITY_STRUCTURE_FK is NULL
								AND tsed.new_data <> '-999999999'
								AND DS.ENTITY_STRUCTURE_EC_CR_FK <> ( SELECT CODE_REF_ID FROM EPACUBE.CODE_REF WHERE CODE_TYPE = 'ENTITY_CLASS' AND CODE = 'WAREHOUSE' )
							)	




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_PROD_ENTITY'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_PROD_ENTITY has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END





























