﻿










-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform general event validations for all events
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        04/16/2010   Initial SQL Version
-- CV		 04/21/2010   EPA - 2932 nulling is done with -999999999 from DMU
-- CV        05/02/2012   Allow for product mult type table. 
-- CV        05/20/2014   Added Parent structure to dupl temp table 
-- CV        09/29/2014   Adding entity structure to dup check
-- CV        01/13/2017   changed rule type cr fk to come from rules table


CREATE PROCEDURE [synchronizer].[EVENT_POST_VALID_TYPE_ALL] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int

DECLARE @GreenCondition		int
DECLARE @YellowCondition	int
DECLARE @RedCondition		int
DECLARE @BatchNo			bigint
DECLARE @ThisEvent		    bigint
DECLARE @EventsTree			int
DECLARE @OldNode			bigint
DECLARE @NewNode			bigint
DECLARE @ProductEventType   int


set @GreenCondition = epacube.getCodeRefId('EVENT_CONDITION','GREEN')
set @YellowCondition = epacube.getCodeRefId('EVENT_CONDITION','YELLOW')
set @RedCondition = epacube.getCodeRefId('EVENT_CONDITION','RED')
set @BatchNo = (select distinct batch_no from #TS_EVENT_DATA)
set @ProductEventType = epacube.getCodeRefId('EVENT_TYPE','PRODUCT')

BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_ALL.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;
						   				  
						   				  
------------------------------------------------------------------------------------------
-- TEMPORARY TABLE TO CHECK FOR COMPLETENESS EVENTS  --- ALLOWED TO NULL IF COMPLETENSS
-- RESULT DATA NAME FK DOES NOT EXIST FOR THE PRODUCT
------------------------------------------------------------------------------------------

----------drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_COMPLETE') is not null
	   drop table #TS_EVENT_COMPLETE
	   
	   
	-- create temp table
    CREATE TABLE #TS_EVENT_COMPLETE(
	TS_EVENT_COMPLETE_ID bigint IDENTITY(1000,1) NOT NULL,
	EVENT_DATA_NAME_FK INT NULL,
	TABLE_NAME Varchar(128),
	RESULT_DATA_NAME_FK INT NULL,
	PRODUCT_STRUCTURE_FK BIGINT NULL, 
	ORG_ENTITY_STRUCTURE_FK BIGINT NULL
 )

-------------------------------------------------------------------------
--Loop by table name

DECLARE 
              
			  @v_table_name       varchar(64),               
              @v_Result_data_name      varchar(64)
                                                    

      DECLARE  cur_v_import cursor local FOR
			 select distinct ds.TABLE_NAME
							,dnr.DATA_NAME_ID 
							FROM epacube.DATA_NAME  dnr
							inner join epacube.DATA_SET ds
							on (ds.DATA_SET_ID = dnr.DATA_SET_FK)
							where dnr.DATA_NAME_ID in 
							(SELECT DISTINCT R.RESULT_DATA_NAME_FK
																	FROM synchronizer.RULES R WITH (NOLOCK) 
																	--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
																	--   ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK 
																	   WHERE  R.RULE_TYPE_CR_FK = 309
																	   AND R.RECORD_STATUS_CR_FK = 1)
							
      
        OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO 
                                          
										  @v_table_name,            
										  @v_Result_data_name                                

         WHILE @@FETCH_STATUS = 0 
         BEGIN
            SET @status_desc =  'synchronizer.Check Completness - '
                                + @v_Result_data_name
                                + ' '
                                + @v_table_name

            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


            SET @ls_exec =
						'INSERT INTO #TS_EVENT_COMPLETE
							( EVENT_DATA_NAME_FK,
								PRODUCT_STRUCTURE_FK, 
								ORG_ENTITY_STRUCTURE_FK
							)
						(SELECT TSED.DATA_NAME_FK
							  ,TSED.PRODUCT_STRUCTURE_FK
							  ,TSED.ORG_ENTITY_STRUCTURE_FK
					    FROM #TS_EVENT_DATA TSED WITH (NOLOCK)
						INNER JOIN EPACUBE.'
						+@v_table_name
						+ ' ID WITH (NOLOCK)
						ON (TSED.EPACUBE_ID = ID.PRODUCT_STRUCTURE_FK 
						AND TSED.ORG_ENTITY_STRUCTURE_FK = ID.ORG_ENTITY_STRUCTURE_FK)
						AND ID.DATA_NAME_FK = '
						+ @v_Result_data_name
						+ ')'
						



----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), @ls_exec )

         exec sp_executesql 	@ls_exec;

------------------------------------------------------------------------------------------
--  IF DATA NAME EXISTS ON A COMPLETENESS RULE DO NOT ALLOW NULL
---but do allow null if the result data name is not present for the product....
------------------------------------------------------------------------------------------

			INSERT INTO #TS_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		(	SELECT 
			 TSED.EVENT_FK
			,NULL
			,99
			,'CAN NOT NULL OUT COMPLETNESS DATA NAME '  --ERROR NAME
			,DN.LABEL
			,NULL
			,NULL
			,NULL
			,NULL
			,NULL
			,@l_sysdate
			FROM #TS_EVENT_COMPLETE TEC
			inner join #TS_EVENT_DATA TSED 
			on (TSED.Product_structure_fk = TEC.PRODUCT_STRUCTURE_FK
			and TSED.Org_entity_structure_fk = TEC.ORG_ENTITY_STRUCTURE_FK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
			ON ( DN.DATA_NAME_ID = TSED.DATA_NAME_FK )
			WHERE 1 = 1 
			AND TSED.EVENT_TYPE_CR_FK IN ( 150, 153, 156 )
            AND TSED.DATA_NAME_FK IN  ( SELECT DISTINCT RA.BASIS1_DN_FK
                                        FROM synchronizer.RULES R WITH (NOLOCK) 
										--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
										--   ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK 
										--   AND  RS.RULE_TYPE_CR_FK = 309 )  --- COMPLETENESS
										INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
										   ON ( RA.RULES_FK = R.RULES_ID
										   AND  RA.RECORD_STATUS_CR_FK = 1 ) 
										   AND R.Record_status_cr_fk = 1
										   AND  R.RULE_TYPE_CR_FK = 309  --- COMPLETENESS
										   AND R.RESULT_DATA_NAME_FK = TEC.EVENT_DATA_NAME_FK)
										   								   
			AND ISNULL ( TSED.NEW_DATA, 'NULL DATA' ) IN ( 'NULL DATA', 'NULL', '-999999999' )
		)
 


------------------------------------------------------------------------------------------
--  IF DATA NAME RESULT OR VALUE IS ON A COMPLETENESS RULE DO NOT ALLOW NULL
------------------------------------------------------------------------------------------

			INSERT INTO #TS_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		(	SELECT 
			 TSED.EVENT_FK
			,NULL
			,99
			,'CAN NOT NULL OUT COMPLETNESS RESULT/VALUE DATA NAME '  --ERROR NAME
			,DN.LABEL 
			,NULL
			,NULL
			,NULL
			,NULL
			,NULL
			,@l_sysdate
			FROM #TS_EVENT_COMPLETE TEC
			inner join #TS_EVENT_DATA TSED 
			on (TSED.Product_structure_fk = TEC.PRODUCT_STRUCTURE_FK
			and TSED.Org_entity_structure_fk = TEC.ORG_ENTITY_STRUCTURE_FK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
			   ON ( DN.PARENT_DATA_NAME_FK = TSED.DATA_NAME_FK 
			   AND  DN.RECORD_STATUS_CR_FK = 1 )
            INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
               ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			WHERE 1 = 1 
			AND TSED.EVENT_TYPE_CR_FK IN ( 151, 152 )   --- VALUE OR RESULT 
            AND DN.DATA_NAME_ID IN (SELECT DISTINCT RA.BASIS1_DN_FK
                                    FROM synchronizer.RULES R WITH (NOLOCK) 
									--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
									--   ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK 
									--   AND  RS.RULE_TYPE_CR_FK = 309 )  --- COMPLETENESS
									INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
									   ON ( RA.RULES_FK = R.RULES_ID
									   AND  RA.RECORD_STATUS_CR_FK = 1 ) 
									   AND R.Record_status_cr_fk = 1
									   AND  R.RULE_TYPE_CR_FK = 309 )  --- COMPLETENESS)
------            AND ( 
------                     ( DS.COLUMN_NAME = 'NET_VALUE1' AND ISNULL ( TSED.NET_VALUE1_NEW, 0 ) <= 0 )
------                 OR  ( DS.COLUMN_NAME = 'NET_VALUE2' AND ISNULL ( TSED.NET_VALUE2_NEW, 0 ) <= 0 )
------                 OR  ( DS.COLUMN_NAME = 'NET_VALUE3' AND ISNULL ( TSED.NET_VALUE3_NEW, 0 ) <= 0 )
------                 OR  ( DS.COLUMN_NAME = 'NET_VALUE4' AND ISNULL ( TSED.NET_VALUE4_NEW, 0 ) <= 0 )
------                 OR  ( DS.COLUMN_NAME = 'NET_VALUE5' AND ISNULL ( TSED.NET_VALUE5_NEW, 0 ) <= 0 )
------                 OR  ( DS.COLUMN_NAME = 'NET_VALUE6' AND ISNULL ( TSED.NET_VALUE6_NEW, 0 ) <= 0 )                                  
------                )
 AND ( 
                     ( DS.COLUMN_NAME = 'NET_VALUE1' AND  TSED.NET_VALUE1_NEW = -999999999 )
                 OR  ( DS.COLUMN_NAME = 'NET_VALUE2' AND  TSED.NET_VALUE2_NEW = -999999999 )
                 OR  ( DS.COLUMN_NAME = 'NET_VALUE3' AND  TSED.NET_VALUE3_NEW = -999999999 )
                 OR  ( DS.COLUMN_NAME = 'NET_VALUE4' AND  TSED.NET_VALUE4_NEW = -999999999 )
                 OR  ( DS.COLUMN_NAME = 'NET_VALUE5' AND  TSED.NET_VALUE5_NEW = -999999999 )
                 OR  ( DS.COLUMN_NAME = 'NET_VALUE6' AND  TSED.NET_VALUE6_NEW = -999999999 )                                 
                )
          )



        FETCH NEXT FROM cur_v_import INTO 
                                          @v_table_name,            
										  @v_Result_data_name                                     


     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 
										   


------------------------------------------------------------------------------------------
-- TEMPORARY TABLE TO CHECK FOR DUPLICATE EVENTS  --- ignoring entity for now
------------------------------------------------------------------------------------------


----drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DUPLICATES') is not null
	   drop table #TS_EVENT_DUPLICATES
	   
	   
	-- create temp table
    CREATE TABLE #TS_EVENT_DUPLICATES(
	TS_EVENT_DUPLICATES_ID bigint IDENTITY(1000,1) NOT NULL,
	DATA_NAME_FK INT NULL,
	PRODUCT_STRUCTURE_FK BIGINT NULL, 
	ORG_ENTITY_STRUCTURE_FK BIGINT NULL,
	PARENT_STRUCTURE_FK BIGINT NULL,
	ENTITY_STRUCTURE_FK BIGINT NULL
 )

INSERT INTO #TS_EVENT_DUPLICATES
( DATA_NAME_FK
 ,PRODUCT_STRUCTURE_FK 
 ,ORG_ENTITY_STRUCTURE_FK 
 ,PARENT_STRUCTURE_FK
 ,entity_structure_fk
 )
SELECT ed.data_name_fk, ed.product_structure_fk, ed.org_entity_structure_fk, ed.parent_structure_fk, ed.entity_structure_fk
FROM #TS_EVENT_DATA ed 
	INNER JOIN epacube.data_name dn ON ( dn.data_name_id = ed.data_name_fk )
	INNER JOIN epacube.data_set ds ON (ds.DATA_SET_ID = dn.DATA_SET_FK )
WHERE 1 = 1
AND   ds.entity_structure_ec_cr_fk IS NULL 
GROUP BY  ed.data_name_fk, ed.product_structure_fk, ed.org_entity_structure_fk, ed.parent_structure_fk, ed.entity_structure_fk
HAVING COUNT(*) > 1

---------NO DUPLICATE for Product VALUE
INSERT INTO #TS_EVENT_DUPLICATES
( DATA_NAME_FK
 ,PRODUCT_STRUCTURE_FK 
 ,ORG_ENTITY_STRUCTURE_FK 
 ,entity_structure_fk
 )
SELECT ed.data_name_fk, ed.product_structure_fk, ed.org_entity_structure_fk, ed.entity_structure_fk --, ed.new_data
FROM #TS_EVENT_DATA ed 
	INNER JOIN epacube.data_name dn ON ( dn.data_name_id = ed.data_name_fk )
	INNER JOIN epacube.data_set ds ON (ds.DATA_SET_ID = dn.DATA_SET_FK
	and ds.TABLE_NAME = 'PRODUCT_MULT_TYPE' )
WHERE 1 = 1
AND   ds.entity_structure_ec_cr_fk IS NULL 
GROUP BY  ed.data_name_fk, ed.product_structure_fk, ed.org_entity_structure_fk ,ed.entity_structure_fk, ed.new_data
HAVING COUNT(*) > 1



------------------------------------------------------------------------------------------
--   VALIDATION for Corp ONLY Data Name but may have conflicting values during a WHSE load
------------------------------------------------------------------------------------------

			INSERT INTO #TS_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		(	SELECT 
			 TSED.EVENT_FK
			,NULL
			,99
			,'DATA NAME has more than one Value in Import for the Org'  --ERROR NAME
			,DN.LABEL
			,NULL
			,NULL
			,NULL
			,NULL
			,NULL
			,@l_sysdate
			FROM #TS_EVENT_DATA TSED
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
			ON ( DN.DATA_NAME_ID = TSED.DATA_NAME_FK )
			INNER JOIN epacube.DATA_SET ds WITH (NOLOCK)
			ON (dn.DATA_SET_FK = ds.DATA_SET_ID    -----------------------JASCO
			and ds.TABLE_NAME <> 'PRODUCT_MULT_TYPE')		
			INNER JOIN #TS_EVENT_DUPLICATES TSDUP
			  ON ( TSDUP.DATA_NAME_FK = TSED.DATA_NAME_FK
			  AND  TSDUP.PRODUCT_STRUCTURE_FK = TSED.PRODUCT_STRUCTURE_FK
			  AND  TSDUP.ORG_ENTITY_STRUCTURE_FK = TSED.ORG_ENTITY_STRUCTURE_FK 
			   AND  TSDUP.ENTITY_STRUCTURE_FK = TSED.ENTITY_STRUCTURE_FK			  )
			WHERE 1 = 1 
		)
 


----drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DUPLICATES') is not null
	   drop table #TS_EVENT_DUPLICATES
	   






------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_ALL'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_ALL has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END











