﻿










-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To create events for sequence product ids
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        12/28/2012   Initial version
-- CV        01/16/2017   Uses Control Table instead of Hiearchy table
--						  


CREATE PROCEDURE [synchronizer].[EVENT_POST_DERIVE_PRODUCT_ID_CUSTOM] (
      @in_batch_no  bigint, @in_calc_group_seq INT, @in_result_data_name_fk INT  )

AS
BEGIN


     DECLARE @ls_exec            nvarchar (max)
     DECLARE @l_exec_no          bigint
     DECLARE @l_rows_processed   bigint
     DECLARE @l_sysdate          datetime

	DECLARE @l_stmt_no           bigint
    DECLARE @ls_stmt             varchar(1000)
	DECLARE @status_desc		 varchar (max)
	DECLARE @ErrorMessage		 nvarchar(4000)
	DECLARE @ErrorSeverity		 int
	DECLARE @ErrorState			 int

    DECLARE @v_product_id_seq    bigint
    DECLARE @v_product_id_string VARCHAR(16)
    
   SET NOCOUNT ON;

   BEGIN TRY
      SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_DERIVE_PRODUCT_ID_CUSTOM '
				 + ', Batch NO: ' + cast(isnull(@IN_BATCH_NO,0) as varchar(16))

      EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;

      SET @l_sysdate   = getdate()



------------------------------------------------------------------------------------------
---  LOOP THROUGH DERIVATION EVENTS
------------------------------------------------------------------------------------------


      DECLARE 
              @vm$event_fk               BIGINT,
              @vm$result_data_name_fk    INT


      DECLARE  cur_v_event cursor local for
				SELECT ed.event_id, r.result_data_name_fk
				FROM synchronizer.EVENT_DATA ED WITH (NOLOCK)
				INNER JOIN synchronizer.EVENT_DATA_RULES EDR  WITH (NOLOCK)
					ON ( EDR.EVENT_FK = ED.EVENT_ID )
			    INNER JOIN synchronizer.RULES R  WITH (NOLOCK)
			        ON ( R.RULES_ID = EDR.RULES_FK )
				INNER JOIN synchronizer.RULES_ACTION RA	 WITH (NOLOCK)	
					ON ( RA.RULES_FK = R.RULES_ID  )
				INNER JOIN 	epacube.DATA_NAME  DN1 WITH (NOLOCK)
				  ON ( DN1.DATA_NAME_ID = R.RESULT_DATA_NAME_FK  )
				INNER JOIN Epacube.controls C on c.controls_id = R.controls_fK  	
				--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
				--	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID  
				--	AND  RS.RULE_TYPE_CR_FK IN ( 301, 302 )  )  -- DEFAULT OR DERIVATION 
				--INNER JOIN Synchronizer.rules_hierarchy RH WITH (NOLOCK) 
				--	ON RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
				--	AND RH.RECORD_STATUS_CR_FK = 1 
				WHERE 1 = 1
				AND   ED.BATCH_NO = @IN_BATCH_NO
				AND   ED.DATA_NAME_FK = 110100  -- PRODUCT_ID
				AND   ED.DATA_NAME_FK = R.RESULT_DATA_NAME_FK 
				AND   RA.RULES_ACTION_OPERATION1_FK = 399  -- DERIVE PRODUCT ID CUSTOM for KAMAN... may add more later
				AND   ISNULL ( C.CONTROL_PRECEDENCE, 999999999 ) = ISNULL(@in_calc_group_seq, 999999999)	
				AND  R.RULE_TYPE_CR_FK IN ( 301, 302 ) 
				AND   R.RESULT_DATA_NAME_FK = @in_result_data_name_fk  
                ORDER BY ED.PRODUCT_STRUCTURE_FK
      
        OPEN cur_v_event;
        FETCH NEXT FROM cur_v_event INTO  @vm$event_fk 
                                         ,@vm$result_data_name_fk


------------------------------------------------------------------------------------------
---  LOOP THROUGH PRODUCT_STRUCTURE_FK... GET SEQ_ID
------------------------------------------------------------------------------------------


         WHILE @@FETCH_STATUS = 0 
         BEGIN


/* ----------------------------------------------------------------------------------- */
--    Assign Next PRODUCT_ID_SEQ to all stg_product_structures within Import Job 
/* ----------------------------------------------------------------------------------- */


	EXEC seq_manager.db_get_next_sequence_value_impl 'common','product_id_seq', @v_product_id_seq output

    SET @v_product_id_string = CAST(@v_product_id_seq AS VARCHAR(16) )

    UPDATE synchronizer.EVENT_DATA 
    SET NEW_DATA = 'K' + SUBSTRING(@v_product_id_string, 1, 3)
                       + '-'
                       + SUBSTRING(@v_product_id_string, 4, 4) 
    WHERE EVENT_ID = @vm$event_fk 


------------------------------------------------------------------------------------------
---  GET NEXT IMPORT_JOB_FK, PRODUCT_STRUCTURE_FK
------------------------------------------------------------------------------------------

        FETCH NEXT FROM cur_v_event INTO  @vm$event_fk 
                                         ,@vm$result_data_name_fk

     END --cur_v_event LOOP
     CLOSE cur_v_event;
	 DEALLOCATE cur_v_event; 



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_DERIVE_PRODUCT_ID_CUSTOM '
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.SYNCHRONIZER.EVENT_POST_DERIVE_PRODUCT_ID_CUSTOM has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END





































































































