﻿












-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To determine all qualified Auto Sycn rules for Event_Data records in Batch
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        10/19/2008   Initial SQL Version
-- CV        01/20/2014   Add Logging for UI
-- CV		 01/16/2017   Rule type cr fk from Rule table

CREATE PROCEDURE [synchronizer].[EVENT_POST_QUALIFY_RULES_AUTO_SYNCH] 
       ( @in_batch_no BIGINT )
       
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

  DECLARE @V_START_TIMESTAMP  DATETIME
  DECLARE @V_END_TIMESTAMP    DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_QUALIFY_RULES_AUTO_SYNC.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;


SET @V_START_TIMESTAMP = getdate()

--      SELECT @v_input_rows = COUNT(1)
--        FROM import.import_Record
--       WHERE job_fk = @in_job_fk;
--
--      SELECT @v_output_rows = COUNT(1)
--        FROM stage.STG_Record
--       WHERE job_fk = @in_job_fk;

	  --SET @v_exception_rows = @v_input_rows - @v_output_rows;
   --   SET @V_END_TIMESTAMP = getdate()
   --   EXEC common.job_execution_create  @in_batch_no, 'START EVENT POST AUTO SYNC RULES',
		 --                                @v_input_rows, @v_output_rows, @v_exception_rows,
			--							 201, @v_START_TIMESTAMP, @V_END_TIMESTAMP;




------------------------------------------------------------------------------------------
--   PRIOR TO COMING INTO THIS PROCEDURE
--
--	  ALL ERRORS HAVE BEEN LOGGED INTO EVENT_DATA_ERRORS
--	  EVENT_CONDITION HAS BEEN SET ON THE #TS_EVENT_DATA TABLE
--
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_AUTO') is not null
	   drop table #TS_RULE_AUTO;
	   
	   
	-- create temp table
	CREATE TABLE #TS_RULE_AUTO(
	    TS_RULE_AUTO_ID          BIGINT IDENTITY(1000,1) NOT NULL,
		EVENT_FK                 BIGINT NULL,	    
		RULES_FK		         BIGINT NULL,
		EVENT_CONDITION_CR_FK    INT NULL,						
        STRUC_PRECEDENCE         SMALLINT,
        SCHED_PRECEDENCE         SMALLINT,
        RULE_PRECEDENCE          SMALLINT,
        DRANK                    INT
		 )


	----INSERT INTO #TS_RULE_AUTO (
	----		 EVENT_FK
	----		,RULES_FK 
	----		,EVENT_CONDITION_CR_FK
	----		,STRUC_PRECEDENCE
	----		,SCHED_PRECEDENCE
	----		,RULE_PRECEDENCE	
 ----            ) 
 ----   SELECT 
	----		 ED.EVENT_FK
	----	    ,R.RULES_ID
	----	    ,ED.EVENT_CONDITION_CR_FK
	----		,RS.PRECEDENCE
	----		,RSCHED.SCHEDULE_PRECEDENCE
	----		,R.RULE_PRECEDENCE
	----------
	----FROM synchronizer.RULES R WITH (NOLOCK)
	----INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	----	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID 
	----	AND  RS.RULE_TYPE_CR_FK = 308 )                --- AUTO SYNC
	----INNER JOIN #TS_EVENT_DATA ED WITH (NOLOCK)
	----	ON  ( ED.RECORD_STATUS_CR_FK = R.RECORD_STATUS_CR_FK )
	-------------------
	----INNER JOIN synchronizer.RULES_SCHEDULE RSCHED  WITH (NOLOCK) 
	----	ON  ( RSCHED.RULES_SCHEDULE_ID  = R.RULES_SCHEDULE_FK )   
	------------------    
	----WHERE 1 = 1
	----AND   ED.BATCH_NO = @in_batch_no
	----AND   ED.RECORD_STATUS_CR_FK = 1
	----AND   ED.EVENT_TYPE_CR_FK IN ( 150, 151, 152, 153, 154, 155, 156, 160 )   --- ALL PRODUCT ENTITY TYPES
	---------------
	----AND   R.RECORD_STATUS_CR_FK = 1
	----AND  (    R.TRIGGER_EVENT_TYPE_CR_FK IS NULL
	----	 OR   R.TRIGGER_EVENT_TYPE_CR_FK = ED.EVENT_TYPE_CR_FK  )
	----AND  (    R.TRIGGER_EVENT_DN_FK IS NULL
	----	 OR   R.TRIGGER_EVENT_DN_FK = ED.DATA_NAME_FK  )
	----AND   (   R.TRIGGER_EVENT_VALUE1 IS NULL
	----	  OR  ISNULL ( R.TRIGGER_EVENT_VALUE1, 'NULL DATA') = ISNULL ( ED.NEW_DATA, 'NULL DATA')  ) 
	----AND  (    R.TRIGGER_EVENT_ACTION_CR_FK IS NULL
	----	 OR   R.TRIGGER_EVENT_ACTION_CR_FK = ED.EVENT_ACTION_CR_FK )
	----AND  (    R.TRIGGER_IMPORT_PACKAGE_FK IS NULL
	----	 OR   R.TRIGGER_IMPORT_PACKAGE_FK = ED.IMPORT_PACKAGE_FK )
	----AND  (    R.TRIGGER_EVENT_SOURCE_CR_FK IS NULL
	----	 OR   R.TRIGGER_EVENT_SOURCE_CR_FK = ED.EVENT_SOURCE_CR_FK )    


	
	INSERT INTO #TS_RULE_AUTO (
			 EVENT_FK
			,RULES_FK 
			,EVENT_CONDITION_CR_FK
			--,STRUC_PRECEDENCE
			--,SCHED_PRECEDENCE
			,RULE_PRECEDENCE	
             ) 
    SELECT 
			 ED.EVENT_FK
		    ,R.RULES_ID
		    ,ED.EVENT_CONDITION_CR_FK
			--,RS.PRECEDENCE
			--,RSCHED.SCHEDULE_PRECEDENCE
			,R.RULE_PRECEDENCE
	------
	FROM synchronizer.RULES R WITH (NOLOCK)
	--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	--	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID 
	--	AND  RS.RULE_TYPE_CR_FK = 308 )                --- AUTO SYNC
	INNER JOIN #TS_EVENT_DATA ED WITH (NOLOCK)
		ON  ( ED.RECORD_STATUS_CR_FK = R.RECORD_STATUS_CR_FK )
	---------------
	--INNER JOIN synchronizer.RULES_SCHEDULE RSCHED  WITH (NOLOCK) 
	--	ON  ( RSCHED.RULES_SCHEDULE_ID  = R.RULES_SCHEDULE_FK )   
	--------------    
	WHERE 1 = 1
	AND   ED.BATCH_NO = @in_batch_no
	AND   ED.RECORD_STATUS_CR_FK = 1
	AND   ED.EVENT_TYPE_CR_FK IN ( 150, 151, 152, 153, 154, 155, 156, 160 )   --- ALL PRODUCT ENTITY TYPES
	-----------
	AND   R.RECORD_STATUS_CR_FK = 1
	AND  R.RULE_TYPE_CR_FK = 308                --- AUTO SYNC
	AND  (    R.TRIGGER_EVENT_TYPE_CR_FK IS NULL
		 OR   R.TRIGGER_EVENT_TYPE_CR_FK = ED.EVENT_TYPE_CR_FK  )
	AND  (    R.TRIGGER_EVENT_DN_FK IS NULL
		 OR   R.TRIGGER_EVENT_DN_FK = ED.DATA_NAME_FK  )
	AND   (   R.TRIGGER_EVENT_VALUE1 IS NULL
		  OR  ISNULL ( R.TRIGGER_EVENT_VALUE1, 'NULL DATA') = ISNULL ( ED.NEW_DATA, 'NULL DATA')  ) 
	AND  (    R.TRIGGER_EVENT_ACTION_CR_FK IS NULL
		 OR   R.TRIGGER_EVENT_ACTION_CR_FK = ED.EVENT_ACTION_CR_FK )
	AND  (    R.TRIGGER_IMPORT_PACKAGE_FK IS NULL
		 OR   R.TRIGGER_IMPORT_PACKAGE_FK = ED.IMPORT_PACKAGE_FK )
	AND  (    R.TRIGGER_EVENT_SOURCE_CR_FK IS NULL
		 OR   R.TRIGGER_EVENT_SOURCE_CR_FK = ED.EVENT_SOURCE_CR_FK )    


--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT OF #TS_RULE_AUTO
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSRA_TSRAID ON #TS_RULE_AUTO
	(TS_RULE_AUTO_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSRA_PRECEDENCE] ON #TS_RULE_AUTO
	( STRUC_PRECEDENCE ASC, SCHED_PRECEDENCE ASC, RULE_PRECEDENCE ASC )
	INCLUDE ( TS_RULE_AUTO_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRA_EVENT ON #TS_RULE_AUTO 
	(EVENT_FK ASC
	)
	INCLUDE ( TS_RULE_AUTO_ID, EVENT_CONDITION_CR_FK )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



------------------------------------------------------------------------------------------
--  DENSE RANK BY PRECEDENCE 
--			PARTITION BY EVENT_FK, RESULT_DATA_NAME_FK, BASIS1_DN_FK, BASIS2_DN_FK
------------------------------------------------------------------------------------------


		UPDATE #TS_RULE_AUTO
		SET   DRANK = 1 
        WHERE TS_RULE_AUTO_ID IN ( 
			SELECT A.TS_RULE_AUTO_ID
			FROM (
                SELECT
                TSRA.TS_RULE_AUTO_ID
                ,TSRA.EVENT_FK
				,DENSE_RANK() OVER ( PARTITION BY EVENT_FK
						ORDER BY STRUC_PRECEDENCE ASC, SCHED_PRECEDENCE ASC, RULE_PRECEDENCE ASC,
								 RULES_FK DESC  ) DRANK
				FROM #TS_RULE_AUTO TSRA WITH (NOLOCK) 				
                ) A
            WHERE A.DRANK = 1 )




------------------------------------------------------------------------------------------
--   ONLY LOG THE AUTO REJECT IN DIRECTLY IN EVENT_DATA_ERROR TABLE
------------------------------------------------------------------------------------------

	INSERT INTO #TS_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)   
	SELECT 
				 TSRA.EVENT_FK 
				,TSRA.RULES_FK
				,RA.EVENT_CONDITION_CR_FK 
				,RA.ERROR_NAME 
				,DNX.LABEL AS DATA1
				,NULL AS DATA2
				,NULL AS DATA3
				,NULL AS DATA4
				,NULL AS DATA5
				,NULL AS DATA6
				,GETDATE() AS UPDATE_TIMESTAMP 
	------
	FROM  #TS_RULE_AUTO TSRA
	INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
		ON ( R.RULES_ID = TSRA.RULES_FK )  	
	INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
		ON ( RA.RULES_FK = R.RULES_ID )  	
	INNER JOIN synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
	  ON ( RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION1_FK )	
	LEFT JOIN epacube.DATA_NAME DNX WITH (NOLOCK) ON ( DNX.DATA_NAME_ID = R.TRIGGER_EVENT_DN_FK )		  	
	WHERE 1 = 1
	AND   TSRA.DRANK = 1
	AND   RA.EVENT_STATUS_CR_FK = 82  -- REJECT  
	AND   RAOP.NAME <> 'NO ACTION'      --- IGNORE NO ACTION RULE ACTIONS
					
				

------------------------------------------------------------------------------------------
--   REJECT SET STATUS AND PRIORITY TO APPROVAL DENIED
------------------------------------------------------------------------------------------


	UPDATE #TS_EVENT_DATA 
	SET EVENT_PRIORITY_CR_FK = 60   -- APPROVAL DENIED
	   ,EVENT_STATUS_CR_FK   = 82   -- REJECTED
	WHERE EVENT_FK IN (				
			SELECT TSRA.EVENT_FK 
			FROM  #TS_RULE_AUTO TSRA
			INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
				ON ( R.RULES_ID = TSRA.RULES_FK )  	
			INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
				ON ( RA.RULES_FK = R.RULES_ID )  	
			INNER JOIN synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
			  ON ( RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION1_FK )		
			WHERE 1 = 1
			AND   TSRA.DRANK = 1
			AND   RA.EVENT_STATUS_CR_FK = 82  -- REJECT  
			AND   RAOP.NAME <> 'NO ACTION'      --- IGNORE NO ACTION RULE ACTIONS
          )

------------------------------------------------------------------------------------------
--   SET PRIORITY TO APPROVE
------------------------------------------------------------------------------------------


	UPDATE #TS_EVENT_DATA 
	SET EVENT_PRIORITY_CR_FK = 66
	WHERE EVENT_FK IN (
			SELECT TSRA.EVENT_FK 
			FROM  #TS_RULE_AUTO TSRA
			INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
				ON ( R.RULES_ID = TSRA.RULES_FK )  	
			INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
				ON ( RA.RULES_FK = R.RULES_ID )  	
			INNER JOIN synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
			  ON ( RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION1_FK )		
			WHERE 1 = 1
			AND   TSRA.DRANK = 1
		    AND   TSRA.EVENT_CONDITION_CR_FK <= ISNULL ( RA.EVENT_CONDITION_CR_FK, 97 ) 			
			AND   RA.EVENT_PRIORITY_CR_FK = 66  -- APPROVE  
	        AND   RAOP.NAME <> 'NO ACTION'      --- IGNORE NO ACTION RULE ACTIONS
		)					
		


------------------------------------------------------------------------------------------
--   SET PRIORITY TO AWAITING APPROVAL  ( if NO ACTION )
------------------------------------------------------------------------------------------


	UPDATE #TS_EVENT_DATA 
	SET EVENT_PRIORITY_CR_FK = 61
	WHERE EVENT_FK IN (
			SELECT TSRA.EVENT_FK 
			FROM  #TS_RULE_AUTO TSRA
			INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
				ON ( R.RULES_ID = TSRA.RULES_FK )  	
			INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK) 
				ON ( RA.RULES_FK = R.RULES_ID )  	
			INNER JOIN synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
			  ON ( RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION1_FK )		
			WHERE 1 = 1
			AND   TSRA.DRANK = 1
	        AND   RAOP.NAME = 'NO ACTION'      --- NO ACTION RULE
		)					
		



--drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_AUTO') is not null
	   drop table #TS_RULE_AUTO;
	

SET @V_END_TIMESTAMP = getdate()



	  --SET @v_exception_rows = @v_input_rows - @v_output_rows;
   --   SET @V_END_TIMESTAMP = getdate()
   --   EXEC common.job_execution_create  @in_batch_no, 'END EVENT POST AUTO SYNC RULES',
		 --                                @v_input_rows, @v_output_rows, @v_exception_rows,
			--							 201, @v_START_TIMESTAMP, @V_END_TIMESTAMP;




	
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_QUALIFY_RULES_AUTO_SYNC'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_QUALIFY_RULES_AUTO_SYNC has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END












































