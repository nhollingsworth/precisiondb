﻿







-- Copyright 2007
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to add, modify, and delete Taxonomy Node Properties.
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG       05/20/2007  Initial SQL Version
--
CREATE PROCEDURE [synchronizer].[TAXONOMY_NODE_PROPERTIES_EDITOR] 
( @in_taxonomy_node bigint,
  @in_taxonomy_node_properties_id bigint,
  @in_taxonomy_properties_id bigint,
  @in_name varchar (256),
  @in_value varchar (256),
  @in_ui_action_cr_fk int)
												
AS

/***************  Parameter Defintions  **********************
CURRENTLY USED IN THE DATA MASTER EDITOR....

Simple for Now.... Only CREATE (8003) and CHANGE (8006); DELETE (8004) to follow

***************************************************************/

BEGIN

DECLARE @v_product_structure_fk bigint
DECLARE @v_taxonomy_properties_fk int
DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int


SET NOCOUNT ON;
set @ErrorMessage = null

IF @in_ui_action_cr_fk in (8003,8006)
BEGIN
	 IF   @in_name IS NULL 
     BEGIN
		SET @ErrorMessage = 'Name must be entered'
        GOTO RAISE_ERROR
	 END
END
--
IF @in_ui_action_cr_fk = 8003 --create
BEGIN

-------------------------------------------------------------------------
--   Check for Duplicate Prior to Insert
-------------------------------------------------------------------------------------
	SELECT @ErrorMessage = 'Duplicate Taxonomy Node Property. '
        + 'Value already exists for taxonomy_node_properties_id on that node'
        + CAST(isnull(taxonomy_node_properties_id,0) AS VARCHAR(30))
    FROM epacube.taxonomy_node_properties
    WHERE taxonomy_node_fk = @in_taxonomy_node
    and   upper(display_name) = upper(@in_name)

	 IF @@rowcount > 0
			GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
--   Edits passed Insert Row
-------------------------------------------------------------------------------------

	INSERT INTO EPACUBE.TAXONOMY_NODE_PROPERTIES
          ( taxonomy_node_fk
			,display_name
			,value 
			,taxonomy_properties_fk
			,create_timestamp
			,update_timestamp)
	SELECT
            
              @in_taxonomy_node,
			  @in_name,	
			  @in_value,	
			  @in_taxonomy_properties_id,
              getdate(),
			  getdate()

END --- IF @in_ui_action_cr_fk = 8003

IF @in_ui_action_cr_fk = 8006 --change
BEGIN

-------------------------------------------------------------------------------------
--   Edits passed Update Row
-------------------------------------------------------------------------------------
      UPDATE EPACUBE.TAXONOMY_NODE_PROPERTIES
       set display_name = @in_name,
       value = @in_value,
	   update_timestamp = getdate()
       where taxonomy_node_properties_id = @in_taxonomy_node_properties_id
	   and taxonomy_node_fk = @in_taxonomy_node

END 

-------------------------------------------------------------------------------------
--   Peform UI Delete Action 
-------------------------------------------------------------------------------------

IF @in_ui_action_cr_fk = 8004  -- delete
BEGIN
	DELETE FROM EPACUBE.TAXONOMY_NODE_PROPERTIES
	WHERE taxonomy_node_properties_id = @in_taxonomy_node_properties_id
	return (0)
END -- IF @in_ui_action_cr_fk = 8004

RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END





























































