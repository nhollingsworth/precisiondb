﻿



-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: For DUPLICATE DATA CHECK
--			
--			
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        01/18/2016   Initial SQL Version
 


CREATE PROCEDURE [synchronizer].[POST_TAXONOMY_DN_ATTRIBUTE_EVENTS] 
      
      
AS
BEGIN



DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint

DECLARE @v_filename_count INT
DECLARE @epa_job_id			 INT


DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

DECLARE @V_TREE_id        bigint

DECLARE @in_job_fk  bigint


set @in_job_fk = (select top 1 job_id from common.job where name = 'TAXONOMY DATA NAMES' order by job_id desc)

BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of [synchronizer].[POST_TAXONOMY_DN_ATTRIBUTE_EVENTS]', 
                                                @epa_job_id = @in_job_FK
                                                , @epa_batch_no = @in_job_FK,
												@exec_id = @l_exec_no OUTPUT;
												
SET @l_sysdate = GETDATE()


 
/*
 -- ---------------------------------------------------------------------------------------------------
 --     Create fake event for missing data names.
 ----   user descides to approve or reject events.
 User takes action by rejecting events.  and then running the Add to tax tree 
 if approve - create the data name and add to the tax tree node
 if reject - no worries.
 
 -- ---------------------------------------------------------------------------------------------------
 --*/
 

----------For events that are not rejected.  loop and added data name and blank data values. ------------------------------------------------------
---INSERT DATA INTO IMPORT MULTI VALUE TABLE
------------------------------------------------------
---drop table if exist

IF object_id('tempdb..#TS_DATA_NAMES') is not null
	   drop table #TS_DATA_NAMES;
	   
 
-- create temp table
	CREATE TABLE #TS_DATA_NAMES(
	[NAME]	varchar(50),
	[DATA_NAME_ID] bigint,
	)


insert into #TS_DATA_NAMES(
[NAME],
	 [DATA_NAME_ID] 
	)
  select  distinct    NEW_DATA ,(select MAX(data_name_id) + 1   from epacube.data_name)
            from synchronizer.EVENT_DATA 
           where UPDATE_USER = 'Add new data name' and EVENT_STATUS_CR_FK = 80 and import_job_fk = @in_job_fk
           and new_data not in (select distinct name from epacube.data_name)



 while EXISTS( select * from #TS_DATA_NAMES)

 BEGIN



DECLARE                                            
               @vm$data_name varchar(64)
			   

 	DECLARE cur_v_import CURSOR local for
	SELECT   
			    temp.NAME
			   from  #TS_DATA_NAMES temp  
		   
	   
		   OPEN cur_v_import;
         FETCH NEXT FROM cur_v_import INTO                                            
                                           @vm$data_name 
		
		


		
INSERT INTO [epacube].[DATA_NAME]
           ([DATA_NAME_ID]
           ,[NAME]
           ,[LABEL]
           ,[SHORT_NAME]
           ,[DISPLAY_SEQ]
           ,[DATA_SET_FK]
           ,[FILTER_IND]
           ,[USER_EDITIBLE]
           ,[USER_EDITIBLE_VALUES]
           ,[INSERT_MISSING_VALUES]
           ,[UPPER_CASE_IND]
           ,[TRIM_IND]
           ,[SYSTEM_PROTECTED]
           ,[NO_EVENT_IND]
           ,[POST_IND]
           ,[SINGLE_ASSOC_IND]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_USER])
                     
        values( (select MAX(data_name_id) + 1   from epacube.data_name),
           upper(@vm$data_name) , upper(@vm$data_name), upper(@vm$data_name), 999999999, 90001, 0, 1,1,1,1,0,0,0,1,0,1
           ,GETDATE(), 'TAX' )

	delete from #TS_DATA_NAMES  
	 where NAME in (select NAME from epacube.DATA_NAME)
	 
	 

 FETCH NEXT FROM cur_v_import Into                                           
                                   @vm$data_name
                                   


						
CLOSE cur_v_import
DEALLOCATE cur_v_import   


END -- 
----------------------------------------------------------------------------------------------------
INSERT INTO [epacube].[DATA_VALUE]
           ([DATA_NAME_FK]
           ,[VALUE]
           ,UPDATE_USER
                  )
select DATA_NAME_ID, ' ', 'TAX'  from epacube.data_name where UPDATE_USER = 'TAX'
and data_name_id not in (
select data_name_fk from epacube.data_value where value = ' ' )

 
--select * from epacube.taxonomy_node where taxonomy_node_id = 1325
--select * from epacube.taxonomy_Node_attribute where taxonomy_node_fk = 1325

--Once this goes in.. have to add it to the tree.  


INSERT INTO [epacube].[TAXONOMY_NODE_ATTRIBUTE]
           ([TAXONOMY_NODE_FK]
           ,[DATA_NAME_FK]
           
           ,[DATA_VALUE_FK]
          )
 select ed.Parent_structure_fk,    dn.DATA_NAME_ID, dv.DATA_VALUE_ID  from epacube.DATA_NAME dn with (nolock)
 inner join epacube.DATA_VALUE dv with (nolock) on dn.DATA_NAME_ID = dv.DATA_NAME_FK and VALUE = ' ' 
 inner join synchronizer.EVENT_DATA ed with (nolock) on ed.NEW_DATA = dn.NAME
 and ed.event_status_cr_fk = 80
 
 
 and ed.IMPORT_JOB_FK = @in_job_fk

-----------------------------------------
--mark event approved and move to history



  INSERT INTO [synchronizer].[EVENT_DATA_history]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
		   ,ORG_ENTITY_STRUCTURE_FK
           ,[NEW_DATA]
           ,[CURRENT_DATA]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])

select [EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
		   ,ORG_ENTITY_STRUCTURE_FK
           ,[NEW_DATA]
           ,[CURRENT_DATA]
           ,[EVENT_CONDITION_CR_FK]
           ,81
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
from synchronizer.event_data  
where event_status_cr_fk = 80
and import_job_fk = @in_job_fk



 update synchronizer.event_data 
set event_status_cr_fk = 81
where event_status_cr_fk = 80
and import_job_fk = @in_job_fk



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of [synchronizer].[POST_TAXONOMY_DN_ATTRIBUTE_EVENTS]'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk,  @epa_batch_no = @in_job_FK
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of [synchronizer].[POST_TAXONOMY_DN_ATTRIBUTE_EVENTS]' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END
