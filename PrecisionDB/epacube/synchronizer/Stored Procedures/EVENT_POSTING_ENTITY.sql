﻿






-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Event processing for product events
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010   Initial SQL Version
-- CV		 05/10/2010   EPA - 2939 removed Begin End statements
-- CV        09/16/2011   Never need to update entity mult type table 
--	                      only inserts
-- KS        10/15/2012   EPA-3400 Added Delete for EVENT_DATA_RULES to remove duplicates
-- CV        05/28/2013   Added Child entity structure
-- CV        06/25/2013   Adding new entity association attribute table
-- CV        09/10/2013   Adding entity ident mult section
-- CV        10/28/2013   had to add loop for mutliply data names and events
-- CV        01/27/2017   Added new segments Contacts section


CREATE PROCEDURE [synchronizer].[EVENT_POSTING_ENTITY] 
        ( @in_event_fk BIGINT, @in_post_ind SMALLINT, 
          @in_batch_no BIGINT, @in_import_job_fk BIGINT, 
          @in_event_priority_cr_fk INT, @in_event_type_cr_fk INT,
          @in_table_name VARCHAR(64), @in_data_name_fk BIGINT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime


DECLARE  @ls_exec1            nvarchar (max)
DECLARE  @ls_exec2            nvarchar (max)
DECLARE  @ls_exec3            nvarchar (max)

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_ENTITY.'
					 + ' Event: ' + cast(isnull(@in_event_fk, 0) as varchar(30))
					 + ' Post Ind: ' + cast(isnull(@in_post_ind, 0) as varchar(30))					 
					 + ' Batch: ' + cast(isnull(@in_batch_no, 0) as varchar(30))
					 + ' Import: ' + cast(isnull(@in_import_job_fk, 0) as varchar(30))	
					 + ' Priority: ' + cast(isnull(@in_event_priority_cr_fk, 0) as varchar(30))					 				 
					 + ' Event Type: ' + cast(isnull(@in_event_type_cr_fk, 0) as varchar(30))					 				 
					 + ' Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))	
					 + ' Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))							 					 				 				 					 					 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;



------            exec sp_executesql 	@ls_stmt  USE TO THROW ERROR

SET @l_sysdate = GETDATE()




------------------------------------------------------------------------------------------
--   Save this code snippet for causing immediate stop
------------------------------------------------------------------------------------------

--            SET @ls_exec = 'STOP CODE HERE'            
--            INSERT INTO COMMON.T_SQL
--                ( TS,  SQL_TEXT )
--            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
--            exec sp_executesql 	@ls_exec;
--


------------------------------------------------------------------------------------------
---  CLEAR TABLE FOR NEXT BATCH_NO, PRIORITY, JOB_FK, EVENT_TYPE
------------------------------------------------------------------------------------------
  
   TRUNCATE TABLE #TS_EVENT_DATA


--------------------------------------------------------------------------------------------
--   ENTITY EVENTS   --- CINDY TO ADD LATER  EVENT_TYPE_CR_FK = 154
--------------------------------------------------------------------------------------------

--IF @in_event_type_cr_fk IN ( 154 )  


--------------------------------------------------------------------------------------------
--   ENTITY STRUCTURE  -- EPACUBE ENTITY STATUS
--------------------------------------------------------------------------------------------


--BEGIN
		IF @in_table_name = 'ENTITY_STRUCTURE'

  INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
ED.ENTITY_DATA_NAME_FK,
--			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
		    ES.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
				CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  isNULL(( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) ,
					( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 INNER JOIN Epacube.data_name dn with (nolock) on 
			                  DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK and dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NOT NULL
			                 AND   DV.VALUE = ED.NEW_DATA ))
			
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			( SELECT CR.CODE FROM EPACUBE.CODE_REF CR
			  WHERE CR.CODE_REF_ID = ES.ENTITY_STATUS_CR_FK  ) AS CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			NULL,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			LEFT JOIN epacube.ENTITY_STRUCTURE ES WITH (NOLOCK)
			 ON ( ES.ENTITY_STRUCTURE_ID = ED.ENTITY_STRUCTURE_FK )
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )
			
			 	 
--END 
    
--------------------------------------------------------------------------------------------
--   ENTITY IDENTIFICATION
--------------------------------------------------------------------------------------------

--ELSE
IF @in_table_name = 'ENTITY_IDENTIFICATION'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			ED.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
		    EI.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
				CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  isNULL(( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) ,
					( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 INNER JOIN Epacube.data_name dn with (nolock) on 
			                  DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK and dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NOT NULL
			                 AND   DV.VALUE = ED.NEW_DATA ))
			
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			EI.VALUE AS CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			EI.ENTITY_IDENTIFICATION_ID,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			LEFT JOIN epacube.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
			 ON ( EI.DATA_NAME_FK = ED.DATA_NAME_FK
			 AND  ISNULL ( EI.ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 ) )
------		
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )
			

--------------------------------------------------------------------------------------------
--   ENTITY EVENTS  -- ENTITY_IDENT_NONUNIQUE
--------------------------------------------------------------------------------------------


IF @in_table_name = 'ENTITY_IDENT_NONUNIQUE'
 
--BEGIN 
 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			NULL,  --PRODUCT STRUCTURE
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			ED.ENTITY_DATA_NAME_FK,   ----was coming from dn..cindy we were loseng entity data name from event.
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
		    EIN.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  isNULL(( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) ,
					( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 INNER JOIN Epacube.data_name dn with (nolock) on 
			                  DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK and dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NOT NULL
			                 AND   DV.VALUE = ED.NEW_DATA ))
			
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			EIN.VALUE AS CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			EIN.ENTITY_IDENT_NONUNIQUE_ID,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			LEFT JOIN epacube.ENTITY_IDENT_NONUNIQUE EIN WITH (NOLOCK)
			 ON ( EIN.DATA_NAME_FK = ED.DATA_NAME_FK
			 AND  ISNULL ( EIN.ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 ) )
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )
			

--END

--------------------------------------------------------------------------------------------
--   PRODUCT EVENTS  -- ENTITY CATEGORY
--------------------------------------------------------------------------------------------

IF @in_table_name = 'ENTITY_CATEGORY'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
			EC.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
				CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  isNULL(( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) ,
					( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 INNER JOIN Epacube.data_name dn with (nolock) on 
			                  DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK and dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NOT NULL
			                 AND   DV.VALUE = ED.NEW_DATA ))
			
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			CASE DS.DATA_TYPE_CR_FK
			WHEN   134 THEN ( SELECT ISNULL ( dv.value, 'NULL' )
								FROM epacube.data_value dv  WITH (NOLOCK) 
								WHERE dv.data_value_id = ec.data_value_fk  )
			WHEN   135 THEN ( SELECT ISNULL ( edv.VALUE, 'NULL' )
								FROM epacube.entity_data_value edv  WITH (NOLOCK)  
								WHERE edv.entity_data_value_id = ec.data_value_fk )
			ELSE   NULL
  			END AS CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			EC.ENTITY_CATEGORY_ID,  --.TABLE_ID_FK,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			LEFT JOIN epacube.ENTITY_CATEGORY EC WITH (NOLOCK)
			 ON ( EC.DATA_NAME_FK = ED.DATA_NAME_FK
			  AND  ISNULL ( EC.ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 ) )
------	
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
					AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )



--------------------------------------------------------------------------------------------
--  SEGMENT_CUSTOMER
--------------------------------------------------------------------------------------------

IF @in_table_name = 'SEGMENTS_CUSTOMER'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK, 
			ISNULL(DN.ENTITY_DATA_NAME_FK,ES.DATA_NAME_FK),
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
			EC.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
				CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  isNULL(( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) ,
					( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 INNER JOIN Epacube.data_name dn with (nolock) on 
			                  DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK and dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NOT NULL
			                 AND   DV.VALUE = ED.NEW_DATA ))
			 WHEN 128 THEN   ( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA )
			
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			CASE DS.DATA_TYPE_CR_FK
			WHEN   134 THEN ( SELECT ISNULL ( dv.value, 'NULL' )
								FROM epacube.data_value dv  WITH (NOLOCK) 
								WHERE dv.data_value_id = ec.data_value_fk  )
			WHEN 128 THEN   ( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA )
			WHEN   135 THEN ( SELECT ISNULL ( edv.VALUE, 'NULL' )
								FROM epacube.entity_data_value edv  WITH (NOLOCK)  
								WHERE edv.entity_data_value_id = ec.data_value_fk )
			ELSE   NULL
  			END AS CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			EC.SEGMENTS_CUSTOMER_ID,  --.TABLE_ID_FK,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			 INNER JOIN Epacube.ENTITY_STRUCTURE ES
			 on ES.entity_structure_id = ED.epacube_id
			LEFT JOIN epacube.SEGMENTS_CUSTOMER EC WITH (NOLOCK)
			 ON ( EC.DATA_NAME_FK = ED.DATA_NAME_FK
			  AND  ISNULL ( EC.CUST_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 ) )
------	
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
					AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )

			 			 

--------------------------------------------------------------------------------------------
--   ENTITY ATTRIBUTE
--------------------------------------------------------------------------------------------

--ELSE
IF @in_table_name = 'ENTITY_ATTRIBUTE'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			DATA_LEVEL,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
		    SC.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
				CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  isNULL(( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) ,
					( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 INNER JOIN Epacube.data_name dn with (nolock) on 
			                  DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK and dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NOT NULL
			                 AND   DV.VALUE = ED.NEW_DATA ))
			
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			SC.VALUE AS CURRENT_DATA,
			ED.DATA_LEVEL,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			SC.segments_contacts_Id,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			LEFT JOIN epacube.SEGMENTS_CONTACTS SC WITH (NOLOCK)
			 ON ( SC.DATA_NAME_FK = ED.DATA_NAME_FK
			 AND  ISNULL ( SC.CUST_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 ) )
------			
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )
			

--------------------------------------------------------------------------------------------
--   ***SEGMENTS_CONTACTS
--------------------------------------------------------------------------------------------

--ELSE
IF @in_table_name = 'SEGMENTS_CONTACTS'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			--CURRENT_DATA,
			DATA_LEVEL,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP,
			UPDATE_USER
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			ED.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
		    EA.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
				CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  isNULL(( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) ,
					( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 INNER JOIN Epacube.data_name dn with (nolock) on 
			                  DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK and dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NOT NULL
			                 AND   DV.VALUE = ED.NEW_DATA ))
			
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			--SS.ATTRIBUTE_EVENT_DATA AS CURRENT_DATA,
			ED.DATA_LEVEL,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			EA.Segments_Contacts_ID,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE,
			ED.UPDATE_USER
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			LEFT JOIN epacube.SEGMENTS_CONTACTS EA WITH (NOLOCK)
			 ON ( EA.DATA_NAME_FK = ED.DATA_NAME_FK
			 AND  ISNULL ( EA.Cust_Entity_Structure_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 ) )
------			
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		--AND   (   @in_data_name_fk = -999           
		--	  OR  ED.data_name_fk = @in_data_name_fk )

		--select * from epacube.SEGMENTS_CONTACTS

		
--------------------------------------------------------------------------------------------
--   ***SEGMENTS_SETTINGS
--------------------------------------------------------------------------------------------

--ELSE
IF @in_table_name = 'SEGMENTS_SETTINGS'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			 VENDOR_ENTITY_STRUCTURE_FK,
			 CUST_ENTITY_STRUCTURE_FK,
			 DEPT_ENTITY_STRUCTURE_FK,
			 ZONE_ENTITY_STRUCTURE_FK,
			prod_data_value_fk,
			cust_data_value_fk,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			DATA_LEVEL,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP,
			UPDATE_USER
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			ED.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			ED.VENDOR_ENTITY_STRUCTURE_FK,
			ED.CUST_ENTITY_STRUCTURE_FK,
			ED.DEPT_ENTITY_STRUCTURE_FK,
			ED.ZONE_ENTITY_STRUCTURE_FK,
			ED.prod_data_value_fk,
			ED.cust_data_value_fk,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
		    SS.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  isNULL(( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) ,
					( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 INNER JOIN Epacube.data_name dn with (nolock) on 
			                  DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK and dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NOT NULL
			                 AND   DV.VALUE = ED.NEW_DATA ))
			
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			SS.ATTRIBUTE_EVENT_DATA AS CURRENT_DATA,
			ED.DATA_LEVEL,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			ed.TABLE_ID_FK,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE,
			ED.UPDATE_USER
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			LEFT JOIN epacube.SEGMENTS_SETTINGS SS WITH (NOLOCK)
			 ON ( SS.DATA_NAME_FK = ED.DATA_NAME_FK
			 and ss.DATA_LEVEL_CR_FK = ed.DATA_LEVEL
			 AND  ISNULL ( SS.CUST_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 ) 
			 --AND  ISNULL ( SS.VENDOR_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.VENdO_ENTITY_STRUCTURE_FK, 0 ) 
			 
			 )
------			
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		--AND   (   @in_data_name_fk = -999           
		--	  OR  ED.data_name_fk = @in_data_name_fk )

		
--------------------------------------------------------------------------------------------
--   *** SETTINGS_POS_STORE
--------------------------------------------------------------------------------------------
IF @in_table_name = 'SETTINGS_POS_STORE'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			VENDOR_ENTITY_STRUCTURE_FK,
			CUST_ENTITY_STRUCTURE_FK,
			DEPT_ENTITY_STRUCTURE_FK,
			ZONE_ENTITY_STRUCTURE_FK,
			ASSOC_ENTITY_STRUCTURE_FK,
			prod_data_value_fk,
			cust_data_value_fk,
			Dept_entity_data_value_fk,
			Zone_Type_cr_fk,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			DATA_LEVEL,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP,
			UPDATE_USER
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			ED.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			ED.VENDOR_ENTITY_STRUCTURE_FK,
			ED.CUST_ENTITY_STRUCTURE_FK,
			ED.DEPT_ENTITY_STRUCTURE_FK,
			ED.ZONE_ENTITY_STRUCTURE_FK,
			ED.ASSOC_ENTITY_STRUCTURE_FK,
			ED.prod_data_value_fk,
			ED.cust_data_value_fk,
			ED.dept_entity_data_value_fk,
			ED.Zone_type_cr_fk,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
		    SS.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  isNULL(( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) ,
					( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 INNER JOIN Epacube.data_name dn with (nolock) on 
			                  DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK and dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NOT NULL
			                 AND   DV.VALUE = ED.NEW_DATA ))
			
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			SS.ATTRIBUTE_EVENT_DATA AS CURRENT_DATA,
			ED.DATA_LEVEL,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			ed.TABLE_ID_FK,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE,
			ED.UPDATE_USER
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			LEFT JOIN epacube.SETTINGS_POS_STORE SS WITH (NOLOCK)
			 ON ( SS.DATA_NAME_FK = ED.DATA_NAME_FK
			 and ss.DATA_LEVEL_CR_FK = ed.DATA_LEVEL
			 AND  ISNULL ( SS.CUST_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 ) 
			 --AND  ISNULL ( SS.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ORG_ENTITY_STRUCTURE_FK, 0 ) 
			 AND  ISNULL ( SS.ASSOC_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.Assoc_ENTITY_STRUCTURE_FK, 0 ) 
			 
			 )
------			
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		--AND   (   @in_data_name_fk = -999           
		--	  OR  ED.data_name_fk = @in_data_name_fk )

--------------------------------------------------------------------------------------------
--  ENTITY MULT TYPE
---  Only inserts
--------------------------------------------------------------------------------------------

IF @in_table_name = 'ENTITY_MULT_TYPE'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
			GETDATE(), --EFFECTIVE_DATE_CUR
				CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  isNULL(( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) ,
					( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 INNER JOIN Epacube.data_name dn with (nolock) on 
			                  DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK and dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NOT NULL
			                 AND   DV.VALUE = ED.NEW_DATA ))
			
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			NULL AS CURRENT_DATA,
			----( SELECT ISNULL ( dv.value, 'NULL' )
			----					FROM epacube.data_value dv  WITH (NOLOCK) 
			----					WHERE dv.data_value_id = emt.data_value_fk  ) AS CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			NULL,  --.TABLE_ID_FK,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			----LEFT JOIN epacube.ENTITY_MULT_TYPE EMT WITH (NOLOCK)
			---- ON ( EMT.DATA_NAME_FK = ED.DATA_NAME_FK
			----  AND  ISNULL ( EMT.ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 ) )
------	
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )
			
			 

--------------------------------------------------------------------------------------------
--  ENTITY_IDENT MULT
---  need to add loop because of multiple events.
--------------------------------------------------------------------------------------------



 DECLARE 
              @V_in_event_fk          bigint
             



      DECLARE  cur_v_event cursor local for
	  				select ed.event_id FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88	
		AND CHARINDEX(',', NEW_DATA) > 0	--------if there is a comma in the new data then go down the path....other wise do not.
		AND (	
			   (   ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL (  'ENTITY_IDENT_MULT', 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )

			   
        OPEN cur_v_event;
        FETCH NEXT FROM cur_v_event INTO @v_in_event_fk

		
         WHILE @@FETCH_STATUS = 0 
         BEGIN
										
			  
IF @in_table_name = 'ENTITY_IDENT_MULT'			  

BEGIN

SET @status_desc = ISNULL('START execution of SYNCHRONIZER.EVENT_FACTORY-ENTITY_IDENT_MULT ' + CAST(@V_in_event_fk as varchar(20))
+ ' ' + cast(@in_data_name_fk as varchar(50)) + ' ' + Cast(@in_table_name as Varchar(200)), 'WE have issue')
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @in_batch_no
EXEC exec_monitor.print_status_sp  @l_exec_no;
			  			
			  			
			  			

EXEC [synchronizer].[EVENT_FACTORY_ENTITY_IDENT_MULT]
 @V_in_event_fk , @in_post_ind , 
          @in_batch_no, @in_import_job_fk, 
          @in_event_priority_cr_fk , @in_event_type_cr_fk,
          @in_table_name , @in_data_name_fk

	END	
	
--------------------------------------------------------------------------------------------------	



			 FETCH NEXT FROM cur_v_event INTO @v_in_event_fk									


     END --cur_v_event LOOP
     CLOSE cur_v_event;
	 DEALLOCATE cur_v_event; 


	  INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
		    EIM.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  isNULL(( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) ,
					( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 INNER JOIN Epacube.data_name dn with (nolock) on 
			                  DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK and dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NOT NULL
			                 AND   DV.VALUE = ED.NEW_DATA ))
			
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			EIM.VALUE AS CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			EIM.ENTITY_IDENT_MULT_ID,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			LEFT JOIN epacube.ENTITY_IDENT_MULT EIM WITH (NOLOCK)
			 ON ( EIM.DATA_NAME_FK = ED.DATA_NAME_FK
			 AND  ISNULL ( EIM.ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ENTITY_STRUCTURE_FK, 0 ) 
			 AND  EIM.VALUE =  ED.NEW_DATA ) 
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		--AND (	
		--	   (    ISNULL ( @in_event_fk, -999 ) = -999
		--		AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
		--		AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
		--		AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ('ENTITY_IDENT_MULT', 'NULL DATA' )	
			--	)
			--OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			--)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk  = @in_data_name_fk) 


--------------------------------------------------------------------------------------------
--   ENTITY ASS0CIATIONS 
--   CHECK SINGLE ASSOC IF SINGLE THEN CHANGE  ELSE ADD
--   CHECK IF ENTITY STRUCTURE FK IS NULL AND NEW_DATA IS NOT THEN TRY LOOKUP; ELSE USE ES_ID
--------------------------------------------------------------------------------------------

IF @in_event_type_cr_fk = 160

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			DATA_NAME_FK,
			IMPORT_JOB_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,			
			ENTITY_STRUCTURE_FK,
		    PARENT_ENTITY_STRUCTURE_FK,	
			CHILD_ENTITY_STRUCTURE_FK,								
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			--TAXONOMY_NODE_FK,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,		
			SINGLE_ASSOC_IND,			
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,	
			END_DATE_NEW,	
			RULES_FK,		
			RECORD_STATUS_CR_FK,		
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.DATA_NAME_FK,
			ED.IMPORT_JOB_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
			DS.ENTITY_STRUCTURE_EC_CR_FK AS ENTITY_CLASS_CR_FK,
			DN.ENTITY_DATA_NAME_FK,	
			CASE ISNULL( ED.ENTITY_STRUCTURE_FK, 0 )		
			WHEN 0 THEN		CASE ISNULL ( CR.CODE, 'NULL DATA' )
			                WHEN 'NULL DATA' THEN NULL
							WHEN 'WAREHOUSE' THEN NULL
							ELSE (    SELECT EI.ENTITY_STRUCTURE_FK
									  FROM EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
									  WHERE EI.VALUE = ED.NEW_DATA
									  AND   EI.ENTITY_DATA_NAME_FK = DN.ENTITY_DATA_NAME_FK
									  AND   EI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
																FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK)
																WHERE EEP.APPLICATION_SCOPE_FK = 100
																AND   EEP.NAME = ( SELECT CODE 
																				   FROM epacube.CODE_REF WITH (NOLOCK)
																				   WHERE CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
									 ) )
							END
			ELSE ED.ENTITY_STRUCTURE_FK
			END,														
			ED.PARENT_STRUCTURE_FK,   --CV added for entity assoc
			isNULL(ED.Assoc_parent_child, (select ei.entity_structure_fk from epacube.ENTITY_IDENTIFICATION  EI
			where ED.new_data = ei.value)),		
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
			EAS.UPDATE_TIMESTAMP,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  isNULL(( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) ,
					( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 INNER JOIN Epacube.data_name dn with (nolock) on 
			                  DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK and dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NOT NULL
			                 AND   DV.VALUE = ED.NEW_DATA ))
			
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			ED.NEW_DATA,
			         
			( SELECT EI.VALUE
			  FROM EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
			  WHERE EI.ENTITY_STRUCTURE_FK = EAS.PARENT_ENTITY_STRUCTURE_FK
			  AND   EI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
										FROM EPACUBE.EPACUBE_PARAMS EEP
										WHERE EEP.APPLICATION_SCOPE_FK = 100
										AND   EEP.NAME = CR.CODE ) )  AS CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),			
			DN.SINGLE_ASSOC_IND,
			EAS.ENTITY_ASSOCIATION_ID,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,	
			ED.END_DATE_NEW,	
			ED.RULES_FK,			
			ED.RECORD_STATUS_CR_FK,			
			@L_SYSDATE
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		    LEFT JOIN EPACUBE.CODE_REF CR WITH (NOLOCK) ON ( CR.CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )			 
			LEFT JOIN epacube.ENTITY_ASSOCIATION EAS WITH (NOLOCK)
			 ON ( EAS.DATA_NAME_FK = ED.DATA_NAME_FK
AND ISNULL ( EAS.PARENT_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.PARENT_STRUCTURE_FK, 0 )
--AND ISNULL ( EAS.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ORG_ENTITY_STRUCTURE_FK, 0 )
		AND ISNULL ( EAS.CHILD_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ASSOC_PARENT_CHILD, 0 )	 
AND  (   ISNULL ( DN.SINGLE_ASSOC_IND, 0 ) = 1
			      OR
			          ISNULL ( EAS.PARENT_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.PARENT_STRUCTURE_FK, 0 )
			      )
			  )
----			
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )
			 

			 
--------------------------------------------------------------------------------------------
--   ENTITY ATTRIBUTE
--------------------------------------------------------------------------------------------

--ELSE
IF @in_table_name = 'ENTITY_ASSOCIATION_ATTRIBUTE'

 INSERT INTO #TS_EVENT_DATA 
		(   EVENT_FK,
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			IMPORT_JOB_FK,
			DATA_NAME_FK,
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			ENTITY_STRUCTURE_FK,
			ENTITY_CLASS_CR_FK,
			ENTITY_DATA_NAME_FK,
			ENTITY_ASSOCIATION_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			INSERT_MISSING_VALUES,
			TABLE_NAME,
			EFFECTIVE_DATE_CUR,
			DATA_VALUE_FK ,
			ENTITY_DATA_VALUE_FK,
			NEW_DATA,
			CURRENT_DATA,
			EVENT_EFFECTIVE_DATE,
			EVENT_ACTION_CR_FK,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,											
			EVENT_SOURCE_CR_FK,							
			EVENT_PRIORITY_CR_FK,
			RESULT_TYPE_CR_FK,
			TABLE_ID_FK,
			BATCH_NO,
			IMPORT_BATCH_NO,
			END_DATE_NEW,
			RULES_FK,			
			RECORD_STATUS_CR_FK,
			UPDATE_TIMESTAMP
			 ) 							 							 
   SELECT
		    ED.EVENT_ID,                    
			DS.EVENT_TYPE_CR_FK,
			DS.ENTITY_CLASS_CR_FK AS EVENT_ENTITY_CLASS_CR_FK,
			ED.EPACUBE_ID,
			ED.IMPORT_JOB_FK,
			ED.DATA_NAME_FK,
			ED.PRODUCT_STRUCTURE_FK,
			CASE ISNULL ( DS.ORG_IND, 0 )
			WHEN 0 THEN ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, 1 )
			ELSE ED.ORG_ENTITY_STRUCTURE_FK			
			END,
            ED.ENTITY_STRUCTURE_FK,
			DS.ENTITY_STRUCTURE_EC_CR_FK,
			DN.ENTITY_DATA_NAME_FK,
			EA.ENTITY_ASSOCIATION_ID,
			DN.DATA_SET_FK,
			DS.DATA_TYPE_CR_FK,
			DS.ORG_IND,
			DN.INSERT_MISSING_VALUES,
			DS.TABLE_NAME,
		    EA.UPDATE_TIMESTAMP,  --VCEP.EFFECTIVE_DATE_CUR,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 134 THEN  ( SELECT DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
			                 WHERE DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS DATA_VALUE_FK,
			CASE DS.DATA_TYPE_CR_FK
			WHEN 135 THEN  ( SELECT EDV.ENTITY_DATA_VALUE_ID
			                 FROM EPACUBE.ENTITY_DATA_VALUE EDV WITH (NOLOCK)
			                 WHERE EDV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   EDV.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK
			                 AND   EDV.VALUE = ED.NEW_DATA ) 
			ELSE NULL
			END  AS ENTITY_DATA_VALUE_FK,
			CASE ISNULL ( DN.TRIM_IND, 0 )
			WHEN 0 THEN ED.NEW_DATA
			ELSE RTRIM( LTRIM ( ED.NEW_DATA ) )
			END,
			EAA.ATTRIBUTE_EVENT_DATA AS CURRENT_DATA,
			ED.EVENT_EFFECTIVE_DATE,
			ED.EVENT_ACTION_CR_FK,
			ED.EVENT_STATUS_CR_FK,	
			ED.EVENT_CONDITION_CR_FK,											
			ED.EVENT_SOURCE_CR_FK,							
			ED.EVENT_PRIORITY_CR_FK,
			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
			EAA.ENTITY_ASSOCIATION_ATTRIBUTE_ID,
			ED.BATCH_NO,
			ED.IMPORT_BATCH_NO,
			ED.END_DATE_NEW,
			ED.RULES_FK,
			ED.RECORD_STATUS_CR_FK,
			@L_SYSDATE
--------
	   FROM SYNCHRONIZER.EVENT_DATA ED WITH (NOLOCK)
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			 ON ( DN.DATA_NAME_ID = ED.DATA_NAME_FK )
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
			 ----
			LEFT JOIN epacube.ENTITY_ASSOCIATION EA WITH (NOLOCK)
			 ON ( ISNULL ( EA.PARENT_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.PARENT_STRUCTURE_FK, 0 ) 
			  AND ISNULL ( EA.CHILD_ENTITY_STRUCTURE_FK, 0 ) = ISNULL (ED.ASSOC_PARENT_CHILD, 0 ))
			  LEFT JOIN epacube.ENTITY_ASSOCIATION_ATTRIBUTE EAA WITH (NOLOCK)
			  ON (EA.ENTITY_ASSOCIATION_ID = EAA.ENTITY_ASSOCIATION_FK
			  AND dn.DATA_SET_FK = EAA.DATA_NAME_FK)
------			
		WHERE 1 = 1 
		AND  EVENT_STATUS_CR_FK = 88		
		AND (	
			   (    ISNULL ( @in_event_fk, -999 ) = -999
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@in_import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@in_event_priority_cr_fk, -999)
				AND ISNULL ( ds.table_name, 'NULL DATA' )  = ISNULL ( @in_table_name, 'NULL DATA' )	)
			OR  ED.EVENT_ID = ISNULL (@in_event_fk, -999 ) 
			)
		AND   (   @in_data_name_fk = -999           
			  OR  ED.data_name_fk = @in_data_name_fk )
			



					                             
--------------------------------------------------------------------------------------------
--   CALL VALIDATIONS
--------------------------------------------------------------------------------------------
    
	TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
	TRUNCATE TABLE #TS_EVENT_DATA_RULES	

------------------------------------------------------------------------------------------
--  DELETE ROWS  --- ERRORS ARE REMOVED THEN RE-INSERTED
--					 TIMIMG MUST DO HERE BECAUSE OF CONFLICT AND DUPLICATE VALIDATIONS
--					 DO NOT REMOVE THE EVENT_DATA_RULES  ( ONCE SET FOR AN EVENT.. SET )
------------------------------------------------------------------------------------------

	DELETE
	FROM synchronizer.EVENT_DATA_ERRORS
	WHERE event_fk IN (
	SELECT EVENT_FK
	FROM #TS_EVENT_DATA )

-- KS        10/15/2012   EPA-3400 Added Delete for EVENT_DATA_RULES to remove duplicates
	DELETE
	FROM synchronizer.EVENT_DATA_RULES
	WHERE event_fk IN (
	SELECT EVENT_FK
	FROM #TS_EVENT_DATA )



--------	------------------------------------------------------------------------------------------
--------	--   QUALIFY GOVERNANCE RULES 
--------	------------------------------------------------------------------------------------------
--------	SET  @v_count = ( SELECT COUNT(1)
--------					 FROM #TS_EVENT_DATA WITH (NOLOCK)
--------					 WHERE BATCH_NO = @in_batch_no
--------					 AND   EVENT_STATUS_CR_FK = 88 
--------					 AND   EVENT_TYPE_CR_FK IN ( 150, 151, 152, 153, 154, 155, 156, 160 )   			 
--------					 AND   (  EVENT_SOURCE_CR_FK = 71        -- WHEN UI CALLS VALIDATION DIRECTLY WILL REMOVE 71 
--------						   OR ISNULL ( IMPORT_BATCH_NO, -999 ) = @in_batch_no )    
--------					 AND   DATA_NAME_FK IN ( SELECT R.RESULT_DATA_NAME_FK   
--------											 FROM synchronizer.RULES R WITH (NOLOCK)
--------											 INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
--------											   ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK 
--------											   AND  RS.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE
--------											 WHERE R.RECORD_STATUS_CR_FK = 1 )
--------					)			                         
--------	IF @v_count > 0
--------		EXEC synchronizer.EVENT_POST_QUALIFY_RULES_GOVERNANCE  @in_batch_no

					                             
--------------------------------------------------------------------------------------------
--   CALL VALIDATIONS
--------------------------------------------------------------------------------------------

 --   EXEC synchronizer.EVENT_POST_VALID_ENTITY_IDENTS 
    
 --   EXEC synchronizer.EVENT_POST_VALID_DATA_VALUE
    
 --   EXEC synchronizer.EVENT_POST_VALID_DATA_TYPE
            
	--EXEC synchronizer.EVENT_POST_VALID_TYPE_ALL

	--  IF @in_event_type_cr_fk = 154   --- NOT USED FOR NOW...
	--	EXEC [synchronizer].[EVENT_POST_VALID_TYPE_154] 
		    
    
 --   IF ISNULL ( @in_event_priority_cr_fk, 0 ) = 69
	--	EXEC SYNCHRONIZER.EVENT_POST_VALID_COMPLETENESS


 


------------------------------------------------------------------------------------------
-- CALL PROC TO SET EVENT PRIORITY ACTION AND STATUS   ( KEEP IN ONE PLACE WITH CODE )
------------------------------------------------------------------------------------------

	EXEC  [synchronizer].[EVENT_POST_COND_PRIORITY_ACTION_STATUS] @in_batch_no



--------------------------------------------------------------------------------------------
--   CALL POST AND COMPLETE ... added @in_table_lock
--------------------------------------------------------------------------------------------

------	IF @in_post_ind = 1


		EXEC SYNCHRONIZER.EVENT_POST_DATA   

 

------------------------------------------------------------------------------------------
--  Update synchronizer.EVENT_DATA from #TS_EVENT_DATA  ( POST_COMPLETE )
------------------------------------------------------------------------------------------


UPDATE synchronizer.EVENT_DATA
   SET EVENT_TYPE_CR_FK			= TSED.EVENT_TYPE_CR_FK
      ,EVENT_ENTITY_CLASS_CR_FK = TSED.EVENT_ENTITY_CLASS_CR_FK
      ,EVENT_EFFECTIVE_DATE		= TSED.EVENT_EFFECTIVE_DATE
      ,EPACUBE_ID				= TSED.EPACUBE_ID
      ,DATA_NAME_FK				= TSED.DATA_NAME_FK
      ,NEW_DATA                 = TSED.NEW_DATA      
      ,CURRENT_DATA             = TSED.CURRENT_DATA
      ,PRODUCT_STRUCTURE_FK		= TSED.PRODUCT_STRUCTURE_FK
      ,ORG_ENTITY_STRUCTURE_FK	= TSED.ORG_ENTITY_STRUCTURE_FK
      ,ENTITY_CLASS_CR_FK		= TSED.ENTITY_CLASS_CR_FK
      ,ENTITY_DATA_NAME_FK		= TSED.ENTITY_DATA_NAME_FK
      ,ENTITY_STRUCTURE_FK		= TSED.ENTITY_STRUCTURE_FK
      ,EVENT_CONDITION_CR_FK	= TSED.EVENT_CONDITION_CR_FK
      ,EVENT_STATUS_CR_FK		= TSED.EVENT_STATUS_CR_FK 
      ,EVENT_PRIORITY_CR_FK		= TSED.EVENT_PRIORITY_CR_FK
      ,RESULT_TYPE_CR_FK		= TSED.RESULT_TYPE_CR_FK
      ,EVENT_ACTION_CR_FK		= TSED.EVENT_ACTION_CR_FK
      ,TABLE_ID_FK				= TSED.TABLE_ID_FK
      ,EFFECTIVE_DATE_CUR	    = TSED.EFFECTIVE_DATE_CUR
      ,END_DATE_NEW				= TSED.END_DATE_NEW
      ,END_DATE_CUR				= TSED.END_DATE_CUR
      ,DATA_VALUE_FK            = TSED.DATA_VALUE_FK
      ,ENTITY_DATA_VALUE_FK     = TSED.ENTITY_DATA_VALUE_FK
      ,VALUE_UOM_CODE_FK		= TSED.VALUE_UOM_CODE_FK
      ,ASSOC_UOM_CODE_FK		= TSED.ASSOC_UOM_CODE_FK   
      ,UOM_CONVERSION_FACTOR    = TSED.UOM_CONVERSION_FACTOR   
      ,RECORD_STATUS_CR_FK		= TSED.RECORD_STATUS_CR_FK      
      ,UPDATE_TIMESTAMP			= TSED.UPDATE_TIMESTAMP
FROM #TS_EVENT_DATA  TSED
WHERE TSED.EVENT_FK = synchronizer.EVENT_DATA.EVENT_ID



------------------------------------------------------------------------------------------
--   INSERT INTO synchronizer.EVENT_DATA_ERRORS 
------------------------------------------------------------------------------------------


		INSERT INTO synchronizer.EVENT_DATA_ERRORS 
				(EVENT_FK
			   ,RULE_FK
			   ,EVENT_CONDITION_CR_FK
			   ,DATA_NAME_FK
			   ,ERROR_NAME
			   ,ERROR_DATA1
			   ,ERROR_DATA2
			   ,ERROR_DATA3
			   ,ERROR_DATA4
			   ,ERROR_DATA5
			   ,ERROR_DATA6
			   ,UPDATE_TIMESTAMP)
		   SELECT 
				 TEDE.EVENT_FK
				,TEDE.RULE_FK
				,TEDE.EVENT_CONDITION_CR_FK
				,TEDE.DATA_NAME_FK
				,TEDE.ERROR_NAME
				,TEDE.ERROR_DATA1
				,TEDE.ERROR_DATA2
				,TEDE.ERROR_DATA3
				,TEDE.ERROR_DATA4
				,TEDE.ERROR_DATA5
				,TEDE.ERROR_DATA6
				,TEDE.UPDATE_TIMESTAMP
			FROM #TS_EVENT_DATA_ERRORS TEDE


------------------------------------------------------------------------------------------
--   INSERT INTO synchronizer.EVENT_DATA_RULES
--    CHECK EXISTANCE IN EVENT_DATA_RULE TABLE PRIOR TO INSERTING.... 
------------------------------------------------------------------------------------------

	INSERT INTO [synchronizer].[EVENT_DATA_RULES]
	 (   EVENT_FK      		
		,RULES_FK      
		,RESULT_DATA_NAME_FK      		
		,RESULT_TYPE_CR_FK      	
		,RESULT_EFFECTIVE_DATE      	
		,RULES_EFFECTIVE_DATE 
		,RULES_ACTION_OPERATION_FK     
		,RULE_PRECEDENCE
		,SCHED_PRECEDENCE				
		 ) 			
		SELECT DISTINCT 
         EVENT_FK      		
		,RULES_FK      
		,RESULT_DATA_NAME_FK      		
		,RESULT_TYPE_CR_FK      	
		,RESULT_EFFECTIVE_DATE      	
		,RULES_EFFECTIVE_DATE  
		,RULES_ACTION_OPERATION_FK    
		,RULE_PRECEDENCE
		,SCHED_PRECEDENCE			
	FROM #TS_EVENT_DATA_RULES TSEDR
    WHERE NOT EXISTS ( SELECT 1
                       FROM synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK)
                       WHERE EDR.EVENT_FK  = TSEDR.EVENT_FK
                       AND   EDR.RULES_FK  = TSEDR.RULES_FK )




------------------------------------------------------------------------------------------
---  CLEAR TABLE FOR NEXT BATCH_NO, PRIORITY, JOB_FK, EVENT_TYPE
------------------------------------------------------------------------------------------

   
   TRUNCATE TABLE #TS_EVENT_DATA
   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
   TRUNCATE TABLE #TS_EVENT_DATA_RULES      



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_ENTITY'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @in_batch_no
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_ENTITY has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END

















































