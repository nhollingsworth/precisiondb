﻿

















-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform general event validations to all event types
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010   Initial SQL Version
-- CV        06/10/2011   Delete events that are action of New  NULL and -999999999
-- CV        06/28/2011   Moved down the above delete statement 
-- RG		 04/15/2014	  EPA-3720 setting taxonomy tree assignments to New Value to support multiple trees
-- CV         02/06/2015   Adding to keep from creating blank events or newdate = ''


CREATE PROCEDURE [synchronizer].[EVENT_POST_COND_PRIORITY_ACTION_STATUS] 
      ( @in_batch_no BIGINT  )
      
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  BIGINT
DECLARE  @v_Future_eff_days   int

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int



BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_CON_PRIORITY_ACTION_STATUS.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;




SET @v_FUTURE_EFF_DAYS = ( SELECT ISNULL ( value, 1 ) from epacube.epacube_params
									    where name = 'FUTURE_EFF_DAYS') 


/* ------------------------------------------------------------------------------------------------- */
/*    Check for Duplicate and Conflict Events                                                        */
/* ------------------------------------------------------------------------------------------------- */


	EXEC synchronizer.EVENT_POST_VALID_CONFLICT


/* ------------------------------------------------------------------------------------------------- */
/*    Set Condition in order or RED, YELLOW then GREEN                                               */
/* ------------------------------------------------------------------------------------------------- */

		UPDATE #TS_EVENT_DATA
		SET event_condition_cr_fk = ISNULL ( 
			 (SELECT top 1 TSEDE.event_condition_cr_fk
				FROM #TS_EVENT_DATA_ERRORS TSEDE
				WHERE TSEDE.event_fk = #TS_EVENT_DATA.event_fk
				ORDER BY TSEDE.event_condition_cr_fk desc, TSEDE.ts_event_data_errors_id asc )
			, 97 ) 



------------------------------------------------------------------------------------------
--  RULE ENGINE AUTO SYNCHRONIZE  
--    FOR NOW AUTO APPROVING EVERYTHING EXCEPT WHAT COMES FIRST TIME VIA IMPORT
------------------------------------------------------------------------------------------


UPDATE #TS_EVENT_DATA
SET EVENT_PRIORITY_CR_FK = 60  -- APPROVAL DENIED
WHERE EVENT_CONDITION_CR_FK = 99 


------------------------------------------------------------------------------------------
--  COMPLETENESS EVENT ( STILL 69.. HAS NOT BEEN CHANGED FROM ABOVE )
--    IE NO ERRORS FROM ABOVE  THEN SET TO 66
------------------------------------------------------------------------------------------

UPDATE #TS_EVENT_DATA
SET EVENT_PRIORITY_CR_FK = 66  -- APPROVE TO POST
WHERE EVENT_PRIORITY_CR_FK = 69
AND   EVENT_CONDITION_CR_FK < 99


    
------------------------------------------------------------------------------------------
-- NO CHANGE  ... EVENT DATA IS THE SAME AS THE CURRENT DATA
--				      SET EVENT TO PREPOST AND ACTION NO CHANGE
------------------------------------------------------------------------------------------


      UPDATE #TS_EVENT_DATA
        SET EVENT_ACTION_CR_FK = 56
           ,EVENT_STATUS_CR_FK = 83
      WHERE RECORD_STATUS_CR_FK = 1   --- NOT IN ( 0,2,3 )
      AND   EVENT_STATUS_CR_FK = 88
      AND   EVENT_CONDITION_CR_FK < 99   --- ADD FOR NOW ???
      AND  (    
             (   EVENT_TYPE_CR_FK in (150, 153, 155, 154, 156 )
			 AND ISNULL ( NEW_DATA, 'NULL DATA' ) = ISNULL ( CURRENT_DATA, 'NULL DATA' )
			 AND ISNULL (END_DATE_NEW, '01/01/1968' ) = ISNULL ( END_DATE_CUR, '01/01/1968' ) )
			  )
			OR 
             (   EVENT_TYPE_CR_FK in (154, 160)
			 AND ISNULL ( NEW_DATA, 'NULL DATA' ) = ISNULL ( CURRENT_DATA, 'NULL DATA' ) 
			 AND ISNULL ( END_DATE_NEW, '01/01/1968' ) = ISNULL ( END_DATE_CUR, '01/01/1968' ))

			 


------------------------------------------------------------------------------------------
--  SET AUTO SYNC RULES ONLY FOR FIRST TIME IMPORT ROWS 
--		ALL OTHERS SHOULD ALREADY BE SET TO 66
------------------------------------------------------------------------------------------

SET @V_COUNT =   ( SELECT COUNT(1)
				   FROM #TS_EVENT_DATA TSEDX
				   WHERE 1 = 1
				   AND   EVENT_STATUS_CR_FK = 88
				   AND   EVENT_CONDITION_CR_FK < 99
				   AND   EVENT_PRIORITY_CR_FK BETWEEN 61 AND 69  ---  ALL BECAUSE OF AUTO REJECT
				   AND   (   ISNULL ( IMPORT_BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 ) 
				          OR ISNULL ( IMPORT_BATCH_NO, -999 ) = -999 )   -- NOT IN AN IMPORT
				  )
				   
IF @V_COUNT > 0               
	EXEC synchronizer.EVENT_POST_QUALIFY_RULES_AUTO_SYNCH @in_batch_no
	



------------------------------------------------------------------------------------------
--  UPDATE ACTION AND STATUS 
---
---  NOTE FOR FUTURE WHEN WE DO UNDO ( NEED TO FOLLOW CHANGE FLOW BUT DO NOT CHANGE THE ACTION CODE )

--- future need to research priority 60 - Last... what used for ??
---
------------------------------------------------------------------------------------------







UPDATE #TS_EVENT_DATA
  SET EVENT_ACTION_CR_FK = 
--( CASE WHEN ( EVENT_EFFECTIVE_DATE > ( GETDATE() + 1 ) ) THEN 84  -- FUTURE  looks like this was a mistake
--								   ELSE 
( CASE ISNULL ( RECORD_STATUS_CR_FK, 0 )
									      WHEN 2 THEN 53  -- INACTIVATE
									      ELSE 
							
										  
										  
										  ( CASE ISNULL (EVENT_ACTION_CR_FK, 0)
											     WHEN 0 THEN CASE ISNULL ( TABLE_NAME, 'NULL' )
														   WHEN 'NULL' THEN 99
														   WHEN 'PRODUCT_STRUCTURE' THEN CASE WHEN DATA_NAME_FK = (select data_name_id from epacube.data_name
																													where name = 'EPACUBE STATUS') 
																					THEN 52
																					ELSE 54  -- NEW ADD
																					END
														   WHEN 'ENTITY_STRUCTURE'  THEN CASE WHEN DATA_NAME_FK = (select data_name_id from epacube.data_name
																													where name = 'EPACUBE ENTITY STATUS')
																					THEN 52
																					ELSE 54  -- NEW ADD
																					END										                                          
														   ELSE CASE ISNULL ( TABLE_ID_FK, 0 )
																WHEN 0 THEN 51  -- NEW VALUE
																ELSE 52         -- CHANGE
								   								END
								   						   END
											     WHEN 50 THEN 50   -- ACTIVATE
											     WHEN 53 THEN 53   -- INACTIVATE
											     WHEN 54 THEN 54   -- ACTION NEW ADD
											     WHEN 56 THEN 56   -- IF NO CHANGE LEAVE NO CHANGE
											     WHEN 59 THEN 59   -- PURGE
											     ELSE CASE ISNULL ( TABLE_NAME, 'NULL' )
														   WHEN 'NULL' THEN 99
														   WHEN 'PRODUCT_STRUCTURE' THEN CASE WHEN DATA_NAME_FK = (select data_name_id from epacube.data_name
																													where name = 'EPACUBE STATUS')
																					THEN 52
																					ELSE 54  -- NEW DATA
																					END
														   WHEN 'ENTITY_STRUCTURE'  THEN CASE WHEN DATA_NAME_FK = (select data_name_id from epacube.data_name
																													where name = 'EPACUBE ENTITY STATUS')
																							  THEN 52
																					ELSE 54  -- NEW DATA
																					END
														   ELSE CASE ISNULL ( TABLE_ID_FK, 0 )
																WHEN 0 THEN 51  -- NEW VALUE
																ELSE 52         -- CHANGE
					   											END
					   									   END
								   			  END )
								   	     END )
--								   END )								   
		  ,EVENT_STATUS_CR_FK  =  CASE ISNULL(EVENT_STATUS_CR_FK, 0)
								    WHEN 0  THEN 89   -- JUST MARK AS WHATIF SO WE CAN SEE THAT WE HAVE A PROBLEM
		  							WHEN 82 THEN 82   -- REJECTED
									WHEN 83 THEN 83   -- PREPOST
									WHEN 85 THEN 85   -- HOLD 
									ELSE ( CASE ISNULL ( EVENT_PRIORITY_CR_FK, 0 )
								           WHEN 0  THEN 89   -- JUST MARK AS DENIED SO WE CAN SEE THAT WE HAVE A PROBLEM									
										   WHEN 66 THEN CASE WHEN utilities.TRUNC(EVENT_EFFECTIVE_DATE)  > ( utilities.TRUNC(GETDATE()) + @v_FUTURE_EFF_DAYS )
														 THEN 84 -- FUTURE
														 ELSE 88 -- POST
														 END
										   ELSE 80
										   END  )
									END 


-------------
-- EPA-3720 If it's a taxonomy assignment, make it a New Value
-------------


update #TS_EVENT_DATA
  SET EVENT_ACTION_CR_FK = epacube.getCodeRefId('EVENT_ACTION','NEW VALUE')
  WHERE ORG_ENTITY_STRUCTURE_FK  = 1
  AND EVENT_SOURCE_CR_FK = epacube.getCodeRefId('EVENT_SOURCE','USER ENTRY')
  AND DATA_NAME_FK IN (SELECT DATA_NAME_FK from epacube.TAXONOMY_TREE)
  AND DATA_VALUE_FK in (SELECT data_value_FK from epacube.TAXONOMY_NODE)
  AND DATEDIFF(d,EVENT_EFFECTIVE_DATE, getdate()) = 0
  AND EPACUBE_ID is null
  AND ENTITY_CLASS_CR_FK is null
  AND ENTITY_STRUCTURE_FK is null
  AND VALUE_EFFECTIVE_DATE is null
  AND EVENT_ENTITY_CLASS_CR_FK = epacube.getCodeRefId('ENTITY_CLASS','PRODUCT')
  AND EVENT_TYPE_CR_FK = epacube.getCodeRefId('EVENT_TYPE','PRODUCT')


  ----cindy adding from import
  
update #TS_EVENT_DATA
  SET EVENT_ACTION_CR_FK = 51
  ,CURRENT_DATA = NULL
  WHERE ORG_ENTITY_STRUCTURE_FK  = 1
  AND EVENT_SOURCE_CR_FK =70
  and record_status_Cr_fk = 1
  AND DATA_NAME_FK IN (SELECT DATA_NAME_FK from epacube.TAXONOMY_TREE)
AND PARENT_STRUCTURE_FK is not NULL

-------------
-- End EPA-3720
-------------


-----------------------------------------------------------------
----NULLING OUT DATA  ---- 
-----------------------------------------------------------------

DELETE FROM  #TS_EVENT_DATA
WHERE EVENT_ACTION_CR_FK = 51
 
AND (NEW_DATA = '-999999999'
OR isnull(NEW_DATA,'NULL') = 'NULL'
OR NEW_DATA = '')

AND  EVENT_TYPE_CR_FK in (150, 153, 155, 154, 156 )  --need this because results and values event
														-- have new data = NULL



------------------------------------------------------------------------------------------
--   Save this code snippet for causing immediate stop
------------------------------------------------------------------------------------------

----            SET @ls_exec = 'STOP CODE HERE'            
----            INSERT INTO COMMON.T_SQL
----                ( TS,  SQL_TEXT )
----            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
----            exec sp_executesql 	@ls_exec;





------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_COND_PRIORITY_ACTION_STATUS'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_COND_PRIORITY_ACTION_STATUS has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END