﻿



-- Copyright 2013
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to create/modify Non-Calculation Rules
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        03/17/2014   Initial SQL Version


CREATE PROCEDURE [synchronizer].[RULES_EDITOR] 
( @in_rules_id bigint, 
  @in_rule_name varchar(128),
  @in_rule_structure_id int,
  @in_rule_schedule_id int,
  @in_result_DNID1 int,
  @in_result_DNID2 int,
  @in_effective_date DATETIME,
  @in_end_date DATETIME,
  @in_apply_to_org_children smallint,
  @in_product_filter_DNID int,
  @in_product_filter_value1 varchar(64),
  @in_vendor_filter_DNID int,
  @in_vendor_filter_value1 varchar(64),
  @in_vendor_filter_assoc_DNID int,

  @in_warehouse_filter_DNID int,
  @in_warehouse_filter_value1 varchar(64),
  @in_warehouse_filter_assoc_DNID int,

  @in_trigger_event_type_cr_fk int,
  @in_trigger_event_DNID int,
  @in_trigger_operator_cr_fk int, 
  @in_trigger_event_value1 varchar(64),
  @in_trigger_event_value2 varchar(64),
  @in_trigger_import_package_fk bigint,
  @in_trigger_event_source_cr_fk int,
  @in_trigger_event_action_cr_fk int,
  @in_rule_action_operation1 int,
  @in_prefix1 varchar(64),
  @in_basis1_value varchar(64),
  @in_basis1_DNID int,
  @in_basis1_number numeric(18,6),
  @in_prefix2 varchar(64),
  @in_basis2_value varchar(64),
  @in_basis2_DNID int,
  @in_basis2_number numeric(18,6),

  @in_prefix3 varchar(64),
  @in_basis3_value varchar(64),
  @in_basis3_DNID int,

  @in_substring_start_pos1 smallint,
  @in_substring_length1 smallint,
  @in_substring_whole_token_ind1 smallint,
  @in_truncate_first_token_ind1 smallint,
  @in_remove_spec_chars_ind1 smallint,
  @in_special_characters_set1 varchar(256),
  @in_remove_spaces_ind1 smallint,
  @in_replace_space_char1 char(1),	
  @in_replace_char1 char(1),
  @in_replacing_char1 char(1),
  @in_replace_string1 varchar(16),
  @in_replacing_string1 varchar(16),

  @in_substring_start_pos2 smallint,
  @in_substring_length2 smallint,
  @in_substring_whole_token_ind2 smallint,
  @in_truncate_first_token_ind2 smallint,
  @in_remove_spec_chars_ind2 smallint,
  @in_special_characters_set2 varchar(256),
  @in_remove_spaces_ind2 smallint,
  @in_replace_space_char2 char(1),	
  @in_replace_char2 char(1),
  @in_replacing_char2 char(1),
  @in_replace_string2 varchar(16),
  @in_replacing_string2 varchar(16),


  @in_substring_start_pos3 smallint,
  @in_substring_length3 smallint,
  @in_substring_whole_token_ind3 smallint,
  @in_truncate_first_token_ind3 smallint,
  @in_remove_spec_chars_ind3 smallint,
  @in_special_characters_set3 varchar(256),
  @in_remove_spaces_ind3 smallint,
  @in_replace_space_char3 char(1),	
  @in_replace_char3 char(1),
  @in_replacing_char3 char(1),
  @in_replace_string3 varchar(16),
  @in_replacing_string3 varchar(16),

  @in_compare_operator_cr_fk int,
  @in_event_priority_cr_fk int,
  @in_event_status_cr_fk int,
  @in_event_condition_cr_fk int,
  @in_error_name varchar(128),

  @in_rule_precedence smallint,
  @in_record_status_cr_fk int,
  
  @in_ui_action_cr_fk int,
  @in_update_user varchar(64)
  )
												
AS

/***************  Parameter Defintions  **********************

***************************************************************/

BEGIN

DECLARE @l_sysdate					datetime
DECLARE @ls_stmt					varchar (1000)
DECLARE @l_exec_no					bigint
DECLARE @l_rows_processed			bigint
DECLARE @ErrorMessage				nvarchar(4000)
DECLARE @ErrorSeverity				int
DECLARE @ErrorState					int


DECLARE @createUIActionCodeRefID	int
DECLARE @changeUIActionCodeRefID	int

DECLARE @insertedRuleID				bigint
DECLARE @insertedRuleActionID		bigint
DECLARE @insertedProductFilterID	bigint
DECLARE @insertedOrgFilterID		bigint
DECLARE	@insertedVendorFilterID		bigint

DECLARE @activeStatusCRID			int
DECLARE @equalsOperatorCRID			int
DECLARE @productEntityClassCRID		int
DECLARE @vendorEntityClassCRID		int
DECLARE @orgEntityClassCRID			int
DECLARE @productEntityDNID			int
DECLARE @vendorEntityDNID			int
DECLARE @orgEntityDNID				int

DECLARE @prodFilterID				int
DECLARE @orgFilterID				int
DECLARE @vendorFilterID				int



SET NOCOUNT ON;
SET @l_sysdate = getdate ()
set @ErrorMessage = null

set @activeStatusCRID = (select epacube.getCodeRefId('RECORD_STATUS','ACTIVE'))
set @productEntityClassCRID = (select epacube.getCodeRefId('ENTITY_CLASS','PRODUCT'))
set @vendorEntityClassCRID = (select epacube.getCodeRefId('ENTITY_CLASS','VENDOR'))
set @orgEntityClassCRID = (select epacube.getCodeRefId('ENTITY_CLASS','WAREHOUSE'))
set @productEntityDNID = (select data_name_id from epacube.data_name where name = 'PRODUCT')
set @vendorEntityDNID = (select data_name_id from epacube.data_name where name = 'VENDOR')
set @orgEntityDNID = (select data_name_id from epacube.data_name where name = 'WAREHOUSE')


set @createUIActionCodeRefID = (select epacube.getCodeRefId('UI_ACTION','CREATE'))
set @changeUIActionCodeRefID = (select epacube.getCodeRefId('UI_ACTION','CHANGE'))
set @equalsOperatorCRID = (select epacube.getCodeRefId('OPERATOR','='))

set @in_rule_precedence = isnull(@in_rule_precedence, 9999)



-------------------------------------------------------------------------------------
-- Perform Server-side validation 
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
--   Check for Duplicate Name 
-------------------------------------------------------------------------------------	
	select @ErrorMessage = 'Duplicate Rule Name'
	from synchronizer.RULES
	where @in_rule_name = name
	and (
		-- Change Test: If the names are equal, and it's a change UI action, but the rules id are not equal, 
		-- then the user is clearly trying to change this rule's name such that it duplicates another's.
		(@in_rules_id <> rules_id
		 and @in_rules_id is not null
		 and @in_ui_action_cr_fk = @changeUIActionCodeRefID)		
		or 
		-- Create test: If the names are equal, and the rules_id is null, and it's a 
		-- create UI action, the user is trying to create a new rule with a pre-existing rule's name.
		(@in_rules_id is null
		and @in_ui_action_cr_fk = @createUIActionCodeRefID)
	)
			 IF @@rowcount > 0
					GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- If Product Filter Data Name is populated, Product Filter Value1 must be as well, and vice-versa
-------------------------------------------------------------------------------------
	select @ErrorMessage = 'Data Name and Product Filter Value1 inputs must be used in tandem.'
	from synchronizer.RULES
	where (@in_product_filter_DNID is null and @in_product_filter_value1 is not null)
	or 
	(@in_product_filter_DNID is not null and @in_product_filter_value1 is null)
			
			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

	 --@in_product_filter_DNID int,
  --@in_product_filter_value1 varchar(64),


-------------------------------------------------------------------------------------
-- If Vendor Filter Data Name is populated, Vendor Filter Value1 and Vendor filter assoc must be as well, and vice-versa
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Vendor DNID, Vendor Filter Value1, and Vendor Filter Assoc must be used in tandem.'
	from synchronizer.RULES
	
	where ((@in_vendor_filter_assoc_DNID is null or @in_vendor_filter_DNID is null or @in_vendor_filter_value1 is null)
			and 
		   (@in_vendor_filter_assoc_DNID is not null or @in_vendor_filter_DNID is not null or @in_vendor_filter_value1 is not null)
		  )

		  IF @ErrorMessage is not null
				GOTO RAISE_ERROR
---------------------------------------------------------------------------------------------
-- If Product Filter Data Name is Vendor-related, Vendor Filter Association must be populated
---------------------------------------------------------------------------------------------

select @ErrorMessage = 'Vendor Filter Association is required for Vendor-related Product Filter Data Name: ' + epacube.getDataNameLabel(@in_product_filter_DNID)
where @in_vendor_filter_assoc_DNID is null and  @in_product_filter_DNID in (select data_name_id from epacube.data_name dn
																			inner join epacube.data_set ds on (ds.data_set_id = dn.data_set_fk )
																			where dn.ENTITY_DATA_NAME_FK = epacube.getDataNameId('VENDOR')
																			and (ds.ENTITY_STRUCTURE_EC_CR_FK = epacube.getCodeRefId('ENTITY_CLASS','VENDOR') 
																				or ds.ENTITY_CLASS_CR_FK = epacube.getCodeRefId('ENTITY_CLASS','VENDOR')))

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

---------------------------------------------------------------------------------------------
-- If Any Basis Data Names are Vendor-related, Vendor Filter Association must be populated
---------------------------------------------------------------------------------------------

select @ErrorMessage = 'Vendor Filter Association is required for Vendor-related Basis 1 Data Name: ' + epacube.getDataNameLabel(@in_basis1_DNID)
where @in_vendor_filter_assoc_DNID is null and  @in_basis1_DNID in (select data_name_id from epacube.data_name dn
																			inner join epacube.data_set ds on (ds.data_set_id = dn.data_set_fk )
																			where dn.ENTITY_DATA_NAME_FK = epacube.getDataNameId('VENDOR')
																			and (ds.ENTITY_STRUCTURE_EC_CR_FK = epacube.getCodeRefId('ENTITY_CLASS','VENDOR') 
																				or ds.ENTITY_CLASS_CR_FK = epacube.getCodeRefId('ENTITY_CLASS','VENDOR')))

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

select @ErrorMessage = 'Vendor Filter Association is required for Vendor-related Basis 2 Data Name: ' + epacube.getDataNameLabel(@in_basis2_DNID)
where @in_vendor_filter_assoc_DNID is null and  @in_basis2_DNID in (select data_name_id from epacube.data_name dn
																			inner join epacube.data_set ds on (ds.data_set_id = dn.data_set_fk )
																			where dn.ENTITY_DATA_NAME_FK = epacube.getDataNameId('VENDOR')
																			and (ds.ENTITY_STRUCTURE_EC_CR_FK = epacube.getCodeRefId('ENTITY_CLASS','VENDOR') 
																				or ds.ENTITY_CLASS_CR_FK = epacube.getCodeRefId('ENTITY_CLASS','VENDOR')))

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR																							

select @ErrorMessage = 'Vendor Filter Association is required for Vendor-related Basis 3 Data Name: ' + epacube.getDataNameLabel(@in_basis3_DNID)
where @in_vendor_filter_assoc_DNID is null and  @in_basis3_DNID in (select data_name_id from epacube.data_name dn
																			inner join epacube.data_set ds on (ds.data_set_id = dn.data_set_fk )
																			where dn.ENTITY_DATA_NAME_FK = epacube.getDataNameId('VENDOR')
																			and (ds.ENTITY_STRUCTURE_EC_CR_FK = epacube.getCodeRefId('ENTITY_CLASS','VENDOR') 
																				or ds.ENTITY_CLASS_CR_FK = epacube.getCodeRefId('ENTITY_CLASS','VENDOR')))

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR


-------------------------------------------------------------------------------------
-- If Whse Filter Data Name is populated, Whse Filter Value1 and Whse filter assoc must be as well, and vice-versa
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Warehouse DNID, Warehouse Filter Value1, and Warehouse Filter Assoc must be used in tandem.'
	from synchronizer.RULES
	
	where ((@in_warehouse_filter_assoc_DNID is null or @in_warehouse_filter_DNID is null or @in_warehouse_filter_value1 is null)
			and 
		   (@in_warehouse_filter_assoc_DNID is not null or @in_warehouse_filter_DNID is not null or @in_warehouse_filter_value1 is not null)
		  )

		  IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- Basis1 Data Name:  Drop down (one of these two fields is required Basis1 Value OR Basis1 Data Name): 

--However, Neither Basis1 Value or Basis1 Data Name is required in Auto Sync rules or Product ID Sequence type rules.
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Basis1 Value or Basis1 Data Name must be entered for this rule type'
from synchronizer.rules r
inner join synchronizer.rules_action ra on (ra.rules_fk = r.rules_id)
where @in_basis1_DNID is null and @in_basis1_value is null
and (@in_rule_structure_id not IN (SELECT RULES_STRUCTURE_id from synchronizer.rules_structure where rule_type_cr_fk = epacube.getCodeRefId('RULE_TYPE','AUTO SYNC')) 
and  ----id sequence stuff
(@in_rule_action_operation1 <> (select RULES_ACTION_OPERATION_ID from synchronizer.rules_action_operation where name = 'DERIVE ID SEQ'))
and ( (ra.RULES_ACTION_OPERATION1_FK <> (select RULES_ACTION_OPERATION_ID from synchronizer.rules_action_operation where name = 'DERIVE ID SEQ'))
and (ra.RULES_ACTION_OPERATION2_FK <> (select RULES_ACTION_OPERATION_ID from synchronizer.rules_action_operation where name = 'DERIVE ID SEQ'))
and (ra.RULES_ACTION_OPERATION3_FK <> (select RULES_ACTION_OPERATION_ID from synchronizer.rules_action_operation where name = 'DERIVE ID SEQ'))
))
		IF @ErrorMessage is not null
			GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- If trigger event value 2 is filled in, trigger event value 1 MUST be
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Trigger Event Value1 must be entered if Trigger Event Value2 is to be used.'
from synchronizer.rules where @in_trigger_event_value1 is null and @in_trigger_event_value2 is not null

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR
-------------------------------------------------------------------------------------
-- No level 2 fields allowed without something in level 1 fields:
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Must populate some Level 1 fields before moving to Level 2 fields'
from synchronizer.rules
where 
--if all the first level fields are null

  ( @in_substring_start_pos1 is null and
  @in_substring_length1 is null and
  @in_substring_whole_token_ind1 is null and
  @in_truncate_first_token_ind1 is null and
  @in_remove_spec_chars_ind1 is null and
  @in_remove_spaces_ind1 is null and
  @in_special_characters_set1 is null and
  @in_replace_space_char1 is null and
  @in_replace_char1 is null and
  @in_replacing_char1 is null and
  @in_replace_string1 is null and
  @in_replacing_string1 is null)

  and 

--But not all the level 2 fields are null:
  
  ( @in_substring_start_pos2 is not null or
  @in_substring_length2 is not null or
  @in_substring_whole_token_ind2 is not null or
  @in_truncate_first_token_ind2 is not null or
  @in_remove_spec_chars_ind2 is not null or
  @in_remove_spaces_ind2 is not null or
  @in_special_characters_set2 is not null or
  @in_replace_space_char2 is not null or
  @in_replace_char2 is not null or
  @in_replacing_char2 is not null or
  @in_replace_string2 is not null or
  @in_replacing_string2 is not null)

  IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- Basis2 Data Name:  Drop down (one of these two fields is required Basis2 Value OR Basis2 Data Name): 

--However, Neither Basis2 Value or Basis2 Data Name is required in Auto Sync rules or Product ID Sequence type rules.
-------------------------------------------------------------------------------------
select @ErrorMessage = 'Basis2 Value or Basis2 Data Name must be entered for this rule type'
from synchronizer.rules r
inner join synchronizer.rules_action ra on (ra.rules_fk = r.rules_id)
where @in_basis2_DNID is null and @in_basis2_value is null
and (@in_rule_structure_id not IN (SELECT RULES_STRUCTURE_id from synchronizer.rules_structure where rule_type_cr_fk = epacube.getCodeRefId('RULE_TYPE','AUTO SYNC')) 
and  ----id sequence stuff
(@in_rule_action_operation1 <> (select RULES_ACTION_OPERATION_ID from synchronizer.rules_action_operation where name = 'DERIVE ID SEQ'))
and ( (ra.RULES_ACTION_OPERATION1_FK <> (select RULES_ACTION_OPERATION_ID from synchronizer.rules_action_operation where name = 'DERIVE ID SEQ'))
and (ra.RULES_ACTION_OPERATION2_FK <> (select RULES_ACTION_OPERATION_ID from synchronizer.rules_action_operation where name = 'DERIVE ID SEQ'))
and (ra.RULES_ACTION_OPERATION3_FK <> (select RULES_ACTION_OPERATION_ID from synchronizer.rules_action_operation where name = 'DERIVE ID SEQ'))
))
		IF @ErrorMessage is not null
			GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- No level 3 fields allowed without something in level 2 fields:
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Must populate some Level 2 fields before moving to Level 3 fields'
from synchronizer.rules
where 

--If all the level 2 fields are null...
  ( @in_substring_start_pos2 is null and
  @in_substring_length2 is null and
  @in_substring_whole_token_ind2 is null and
  @in_truncate_first_token_ind2 is null and
  @in_remove_spec_chars_ind2 is null and
  @in_remove_spaces_ind2 is null and
  @in_special_characters_set2 is null and
  @in_replace_space_char2 is null and
  @in_replace_char2 is null and
  @in_replacing_char2 is null and
  @in_replace_string2 is null and
  @in_replacing_string2 is null)

  and 

  --But there are values entered for Third level fields.

  ( @in_substring_start_pos3 is not null or
  @in_substring_length3 is not null or
  @in_substring_whole_token_ind3 is not null or
  @in_truncate_first_token_ind3 is not null or
  @in_remove_spec_chars_ind3 is not null or
  @in_remove_spaces_ind1 is not null or
  @in_special_characters_set3 is not null or
  @in_replace_space_char3 is not null or
  @in_replace_char3 is not null or
  @in_replacing_char3 is not null or
  @in_replace_string3 is not null or
  @in_replacing_string3 is not null)

  IF @ErrorMessage is not null
				GOTO RAISE_ERROR

---------------------------------------------------------------------------------------
---- Basis3 Data Name:  Drop down (one of these two fields is required Basis3 Value OR Basis3 Data Name): 

----However, Neither Basis3 Value or Basis3 Data Name is required in Auto Sync rules or Product ID Sequence type rules.
---------------------------------------------------------------------------------------

--select @ErrorMessage = 'Basis3 Value or Basis3 Data Name must be entered for this rule type'
--from synchronizer.rules r
--inner join synchronizer.rules_action ra on (ra.rules_fk = r.rules_id)
--where @in_basis3_DNID is null and @in_basis3_value is null
--and (@in_rule_structure_id not IN (SELECT RULES_STRUCTURE_id from synchronizer.rules_structure where rule_type_cr_fk = epacube.getCodeRefId('RULE_TYPE','AUTO SYNC')) 
--and  ----id sequence stuff
--(@in_rule_action_operation1 <> (select RULES_ACTION_OPERATION_ID from synchronizer.rules_action_operation where name = 'DERIVE ID SEQ'))
--and ( (ra.RULES_ACTION_OPERATION1_FK <> (select RULES_ACTION_OPERATION_ID from synchronizer.rules_action_operation where name = 'DERIVE ID SEQ'))
--and (ra.RULES_ACTION_OPERATION2_FK <> (select RULES_ACTION_OPERATION_ID from synchronizer.rules_action_operation where name = 'DERIVE ID SEQ'))
--and (ra.RULES_ACTION_OPERATION3_FK <> (select RULES_ACTION_OPERATION_ID from synchronizer.rules_action_operation where name = 'DERIVE ID SEQ'))
--))
--		IF @ErrorMessage is not null
--			GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- If whole token indicator set to Y/1, Rule Result Data Name 2 CANNOT be null.
-------------------------------------------------------------------------------------

	select @ErrorMessage = 'If Whole Token Indicator is set to Y, Rule Result Data Name 2 CANNOT be null'
	from synchronizer.rules where @in_substring_whole_token_ind1 = 1
	and @in_result_DNID2 is null

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- Substring length 1: numeric required field if Substring Start Position 1 is used otherwise it is not used
-------------------------------------------------------------------------------------

	select @ErrorMessage = 'Substring Length 1 and Substring Start Position 1 must be used in tandem.'
	from synchronizer.rules
	where (@in_substring_length1 is null and @in_substring_start_pos1 is not null)
			or
			(@in_substring_length1 is not null and @in_substring_start_pos1 is null)

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

---------------------------------------------------------------------------------------
-- Is  basis 1 data name required for this ^^^^^^^^^^^^^^^^??????
---------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
-- If Prefix 1 is used, Basis1 Data Name is required
-------------------------------------------------------------------------------------

select 'If Prefix 1 is used, Basis1 Data Name is required'
from synchronizer.rules
where @in_prefix1 is not null and @in_basis1_DNID is null

	IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- Replacing character1 is required if Replace Character1 is used
-------------------------------------------------------------------------------------

	select @ErrorMessage = 'Replacing Character 1 and Replace Character 1 must be used in tandem'
	from synchronizer.rules
	where (@in_replace_char1 is null and @in_replacing_char1 is not null) or (@in_replace_char1 is not null and @in_replacing_char1 is null)

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- Replacing String is required if Replace String is used
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Replacing String 1 and Replace String 1 must be used in tandem'
	from synchronizer.rules
	where (@in_replace_string1 is null and @in_replacing_string1 is not null) or (@in_replace_string1 is not null and @in_replacing_string1 is null)

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR


-------------------------------------------------------------------------------------
-- Special Characters Set 1: Required Free form text box if Remove Special Characters Indicator 1 is YES.
-------------------------------------------------------------------------------------
select @ErrorMessage = 'Special Characters Set 1 must be provided if Remove Special Characters 1 is set to Yes'
from synchronizer.rules
where @in_remove_spec_chars_ind1 = 1 and @in_special_characters_set1 is null

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR


-------------------------------------------------------------------------------------
-- If Substring Start Position 2 is used, Basis 2 Data Name is required
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Basis 2 Data Name must be supplied if Substring Start Position 2 is used.'
from synchronizer.rules where @in_substring_start_pos2 is not null and @in_basis2_DNID is null

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- Substring length 2: numeric required field if Substring Start Position 2 is used otherwise it is not used
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Substring Length 2 and Substring Start Position 2 must be used in tandem.'
	from synchronizer.rules
	where (@in_substring_length2 is null and @in_substring_start_pos2 is not null)
			or
			(@in_substring_length2 is not null and @in_substring_start_pos2 is null)

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- If Prefix 2 is used, Basis2 Data Name is required
-------------------------------------------------------------------------------------

select 'If Prefix 2 is used, Basis2 Data Name is required'
from synchronizer.rules
where @in_prefix2 is not null and @in_basis2_DNID is null

	IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- Replacing character2 is required if Replace Character2 is used
-------------------------------------------------------------------------------------

	select @ErrorMessage = 'Replacing Character 2 and Replace Character 2 must be used in tandem'
	from synchronizer.rules
	where (@in_replace_char2 is null and @in_replacing_char2 is not null) or (@in_replace_char2 is not null and @in_replacing_char2 is null)

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- Replacing String is required if Replace String is used
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Replacing String 2 and Replace String 2 must be used in tandem'
	from synchronizer.rules
	where (@in_replace_string2 is null and @in_replacing_string2 is not null) or (@in_replace_string2 is not null and @in_replacing_string2 is null)

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR


-------------------------------------------------------------------------------------
-- Special Characters Set 2: Required Free form text box if Remove Special Characters Indicator 2 is YES.
-------------------------------------------------------------------------------------
select @ErrorMessage = 'Special Characters Set 2 must be provided if Remove Special Characters 2 is set to Yes'
from synchronizer.rules
where @in_remove_spec_chars_ind2 = 2 and @in_special_characters_set2 is null

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- Substring length 3: numeric required field if Substring Start Position 3 is used otherwise it is not used
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Substring Length 3 and Substring Start Position 3 must be used in tandem.'
	from synchronizer.rules
	where (@in_substring_length3 is null and @in_substring_start_pos3 is not null)
			or
			(@in_substring_length3 is not null and @in_substring_start_pos3 is null)

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR


-------------------------------------------------------------------------------------
-- If Prefix 3 is used, Basis3 Data Name is required
-------------------------------------------------------------------------------------

select 'If Prefix 3 is used, Basis3 Data Name is required'
from synchronizer.rules
where @in_prefix3 is not null and @in_basis3_DNID is null

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- Replacing character3 is required if Replace Character3 is used
-------------------------------------------------------------------------------------

	select @ErrorMessage = 'Replacing Character 3 and Replace Character 3 must be used in tandem'
	from synchronizer.rules
	where (@in_replace_char3 is null and @in_replacing_char3 is not null) or (@in_replace_char3 is not null and @in_replacing_char3 is null)

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- Replacing String is required if Replace String is used
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Replacing String 3 and Replace String 3 must be used in tandem'
	from synchronizer.rules
	where (@in_replace_string3 is null and @in_replacing_string3 is not null) or (@in_replace_string3 is not null and @in_replacing_string3 is null)

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR


-------------------------------------------------------------------------------------
-- Special Characters Set 3: Required Free form text box if Remove Special Characters Indicator 3 is YES.
-------------------------------------------------------------------------------------
select @ErrorMessage = 'Special Characters Set 3 must be provided if Remove Special Characters 3 is set to Yes'
from synchronizer.rules
where @in_remove_spec_chars_ind3 = 3 and @in_special_characters_set3 is null

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR


-------------------------------------------------------------------------------------
-- When Compare operator is used, Basis 1 Number can only be null if Basis 1 Data Name is used
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Compare operator requires Basis 1 Number or Basis 1 Data Name'
from synchronizer.rules where @in_compare_operator_cr_fk is not null and @in_basis1_DNID is null and @in_basis1_number is null 

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR

-------------------------------------------------------------------------------------
-- When compare operator is used, Basis 2 Number can only be null if Basis 2 Data Name is used
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Compare operator requires Basis 2 Number or Basis 2 Data Name'
from synchronizer.rules where @in_compare_operator_cr_fk is not null and @in_basis2_DNID is null and @in_basis2_number is null 

			IF @ErrorMessage is not null
				GOTO RAISE_ERROR


-------------------------------------------------------------------------------------
-- If Warehouse Filter Data Name is populated, Warehouse Filter Value1 and Warehouse filter assoc must be as well, and vice-versa
-------------------------------------------------------------------------------------

select @ErrorMessage = 'Warehouse DNID, Warehouse Filter Value1, and Warehouse Filter Assoc must be used in tandem.'
	from synchronizer.RULES
	
	where ((@in_warehouse_filter_assoc_DNID is null or @in_warehouse_filter_DNID is null or @in_warehouse_filter_value1 is null)
			and 
		   (@in_warehouse_filter_assoc_DNID is not null or @in_warehouse_filter_DNID is not null or @in_warehouse_filter_value1 is not null)
		  )

		  IF @ErrorMessage is not null
				GOTO RAISE_ERROR


-------------------------------------------------------------
-- Some rules (Auto sync) have null result data names. Hardcoded 00000 as corresponding input for NULL
-------------------------------------------------------------

IF @in_result_DNID1 = 00000
	set @in_result_DNID1 = null

-------------------------------------------------------------
-- Some of the indicator fields have client-side validations, however null is a valid value in some cases
-- since these client side validations don't play well with logical null, 0 will be translated as null (1 being true)
-- with isnulls in the editor form population query, and logic to set the 0 to null in here
-------------------------------------------------------------

IF @in_apply_to_org_children = 0
	set @in_apply_to_org_children = null


IF @in_substring_whole_token_ind1 = 0
	set @in_substring_whole_token_ind1 = null

IF @in_truncate_first_token_ind1 = 0
	set @in_truncate_first_token_ind1 = null

IF @in_remove_spec_chars_ind1 = 0
	set @in_remove_spec_chars_ind1 = null

IF @in_remove_spaces_ind1 = 0
	set @in_remove_spaces_ind1 = null

IF @in_substring_whole_token_ind2 = 0
	set @in_substring_whole_token_ind2 = null

IF @in_truncate_first_token_ind2 = 0
	set @in_truncate_first_token_ind2 = null

IF @in_remove_spec_chars_ind2 = 0
	set @in_remove_spec_chars_ind2 = null

IF @in_remove_spaces_ind2 = 0
	set @in_remove_spaces_ind2 = null	

IF @in_substring_whole_token_ind3 = 0
	set @in_substring_whole_token_ind3 = null

IF @in_truncate_first_token_ind3 = 0
	set @in_truncate_first_token_ind3 = null

IF @in_remove_spec_chars_ind3 = 0
	set @in_remove_spec_chars_ind3 = null

IF @in_remove_spaces_ind3 = 0
	set @in_remove_spaces_ind3 = null


-------------------------------------------------------------------------------------
-- Perform UI Create Action
-------------------------------------------------------------------------------------


  IF @in_ui_action_cr_fk = @createUIActionCodeRefID
  BEGIN

	--First, insert into the Rules table so we can get a primary key for the dependent inserts
	INSERT INTO synchronizer.rules
	(NAME, RULES_STRUCTURE_FK, RULES_SCHEDULE_FK, RESULT_DATA_NAME_FK, RESULT_DATA_NAME2_FK,
	TRIGGER_EVENT_TYPE_CR_FK, TRIGGER_EVENT_DN_FK, TRIGGER_OPERATOR_CR_FK, TRIGGER_EVENT_VALUE1,TRIGGER_EVENT_VALUE2
	,TRIGGER_IMPORT_PACKAGE_FK, TRIGGER_EVENT_SOURCE_CR_FK, TRIGGER_EVENT_ACTION_CR_FK, APPLY_TO_ORG_CHILDREN_IND, EVENT_CONDITION_CR_FK,	
	EFFECTIVE_DATE, END_DATE ,RECORD_STATUS_CR_FK, RULE_PRECEDENCE, CREATE_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_USER, CREATE_USER)
	SELECT
	@in_rule_name, @in_rule_structure_id, @in_rule_schedule_id, @in_result_DNID1, @in_result_DNID2, @in_trigger_event_type_cr_fk, @in_trigger_event_DNID
	,@in_trigger_operator_cr_fk, @in_trigger_event_value1, @in_trigger_event_value2 ,@in_trigger_import_package_fk
	,@in_trigger_event_source_cr_fk, @in_trigger_event_action_cr_fk, @in_apply_to_org_children, @in_event_condition_cr_fk,
	 @in_effective_date, @in_end_date, @in_record_status_cr_fk, @in_rule_precedence, getdate(), getdate(), @in_update_user, @in_update_user

	--Now we need the primary key for the rule we just inserted in order to continue...
	set @insertedRuleID = (select rules_id from synchronizer.rules where name = @in_rule_name)
	
	
	--Next, insert into Rules Action:
	INSERT INTO synchronizer.RULES_ACTION
	(RULES_FK,  RULES_ACTION_OPERATION1_FK, COMPARE_OPERATOR_CR_FK, BASIS1_DN_FK, BASIS1_NUMBER, BASIS1_VALUE, PREFIX1
		,BASIS2_DN_FK, BASIS2_NUMBER, BASIS2_VALUE, PREFIX2
		,BASIS3_DN_FK, BASIS3_VALUE, PREFIX3		
		,EVENT_PRIORITY_CR_FK, EVENT_STATUS_CR_FK, EVENT_CONDITION_CR_FK, ERROR_NAME
		,CREATE_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_USER )
	SELECT
	@insertedRuleID, @in_rule_action_operation1, @in_compare_operator_cr_fk, @in_basis1_DNID, @in_basis1_number, @in_basis1_value, @in_prefix1
		 ,@in_basis2_DNID, @in_basis2_number, @in_basis2_value, @in_prefix2
		 ,@in_basis3_DNID, @in_basis3_value, @in_prefix3
		 ,@in_event_priority_cr_fk, @in_event_status_cr_fk, @in_event_condition_cr_fk, @in_error_name
		 ,getdate(), getdate(), @in_update_user

	------------------------------------------------------------------------------------------------------------
	-- During an insert, there should only be one insert with one newly inserted rule id, so it is safe to assume
	-- using only that in the where clause will fetch the correct rule_action_id, vs. having to deal with many other
	-- fields which may commonly be null
	------------------------------------------------------------------------------------------------------------

	set @insertedRuleActionID = (select rules_action_id from synchronizer.rules_action where rules_fk = @insertedRuleId)
	

	------------------------------------------------------------------------------------------------------------
	-- Rules Filter and Rules Filter Set inserts
	-- For each supported filter type with a non-null input value:
	--			Prod
	--			Org (Whse)
	--			Supl (Vend(?))
	-- Insert into Rule Filter first
	-- Then, insert into Rules Filter Set
	------------------------------------------------------------------------------------------------------------


	-- Check and see if Prod Filter stuff is null
	-- If it's not null, insert that bad beast, and then get the primary key back for the corresponding Rules Filter Set insert to follow
	--Insert into Rules Filter
	IF ((@in_product_filter_DNID is not null) and  (@in_product_filter_value1 is not null))
	BEGIN

	select @insertedProductFilterID = rules_filter_id
	from synchronizer.rules_filter rf
	where ((@in_product_filter_DNID = DATA_NAME_FK) or (@in_product_filter_DNID is null and DATA_NAME_FK is null))
	and ((@in_product_filter_value1 = VALUE1) or (@in_product_filter_value1 is null and VALUE1 is null))	
	and @productEntityDNID = ENTITY_DATA_NAME_FK
	and @equalsOperatorCRID = OPERATOR_CR_FK


	IF @insertedProductFilterID is null
	BEGIN

		INSERT INTO synchronizer.RULES_FILTER
		(ENTITY_CLASS_CR_FK, NAME, 
				 DATA_NAME_FK, ENTITY_DATA_NAME_FK, VALUE1
				,OPERATOR_CR_FK, RECORD_STATUS_CR_FK, CREATE_TIMESTAMP, UPDATE_USER)
		SELECT
		@productEntityClassCRID, ((select label from epacube.data_name where data_name_id = @in_product_filter_DNID ) +': ' + @in_product_filter_value1)
				,@in_product_filter_DNID, @productEntityDNID, @in_product_filter_value1
				,@equalsOperatorCRID, @activeStatusCRID, getdate(), @in_update_user
	
	-- now, fetch the Rules Filter ID for this freshly inserted entry for use in the Rules Filter Set insert:
		set @insertedProductFilterID = (select rules_filter_id from synchronizer.RULES_FILTER 
										where name =  ((select label from epacube.data_name where data_name_id = @in_product_filter_DNID ) +': ' + @in_product_filter_value1)
										and ENTITY_DATA_NAME_FK = @productEntityDNID
										and ENTITY_CLASS_CR_FK = @productEntityClassCRID
										and DATA_NAME_FK = @in_product_filter_DNID
										and VALUE1 = @in_product_filter_value1)
	END	-- END Product Filter Insert
	END -- End check to see if we should insert or fetch existing
	-- Check and see if Whse/Org Filter stuff is null
	-- If it's not null, insert that bad beast, and then get the primary key back for the corresponding Rules Filter Set insert to follow
	--Insert into Rules Filter
	IF ((@in_warehouse_filter_DNID is not null) and  (@in_warehouse_filter_value1 is not null) and (@in_warehouse_filter_assoc_DNID is not null))
	BEGIN

	select @insertedOrgFilterID = rules_filter_id
	from synchronizer.rules_filter rf
	where ((@in_warehouse_filter_DNID = DATA_NAME_FK) or (@in_warehouse_filter_DNID is null and DATA_NAME_FK is null))
	and ((@in_warehouse_filter_value1 = VALUE1) or (@in_warehouse_filter_value1 is null and VALUE1 is null))
	and @orgEntityDNID = ENTITY_DATA_NAME_FK
	and @equalsOperatorCRID = OPERATOR_CR_FK

	IF @insertedOrgFilterID is null
	BEGIN

		INSERT INTO synchronizer.RULES_FILTER
		(ENTITY_CLASS_CR_FK, NAME, 
				 DATA_NAME_FK, ENTITY_DATA_NAME_FK, VALUE1
				,OPERATOR_CR_FK, RECORD_STATUS_CR_FK, CREATE_TIMESTAMP, UPDATE_USER)
		SELECT
		@orgEntityClassCRID, ((select label from epacube.data_name where data_name_id = @in_warehouse_filter_DNID ) +': ' + @in_warehouse_filter_value1)
				,@in_warehouse_filter_DNID, @orgEntityDNID, @in_warehouse_filter_value1
				,@equalsOperatorCRID, @activeStatusCRID, getdate(), @in_update_user
	
	-- now, fetch the Rules Filter ID for this freshly inserted entry for use in the Rules Filter Set insert:
		set @insertedOrgFilterID = (select rules_filter_id from synchronizer.RULES_FILTER 
										where name =  ((select label from epacube.data_name where data_name_id = @in_warehouse_filter_DNID) +': ' + @in_warehouse_filter_value1)
										and ENTITY_DATA_NAME_FK = @orgEntityDNID
										and ENTITY_CLASS_CR_FK = @orgEntityClassCRID
										and DATA_NAME_FK = @in_warehouse_filter_DNID
										and VALUE1 = @in_warehouse_filter_value1)
	END	-- END Whse/Org Filter Insert
	END -- End check to see if we should insert or fetch existing

	-- Check and see if Vendor/Supl Filter stuff is null
	-- If it's not null, insert that bad beast, and then get the primary key back for the corresponding Rules Filter Set insert to follow
	--Insert into Rules Filter
	IF ((@in_vendor_filter_DNID is not null) and  (@in_vendor_filter_value1 is not null) and (@in_vendor_filter_assoc_DNID is not null))
	BEGIN

	select @insertedVendorFilterID = rules_filter_id
		from synchronizer.rules_filter rf
		where ((@in_vendor_filter_DNID = DATA_NAME_FK) or (@in_vendor_filter_DNID is null and DATA_NAME_FK is null))
		and ((@in_vendor_filter_value1 = VALUE1) or (@in_vendor_filter_value1 is null and VALUE1 is null))
		and @vendorEntityDNID = ENTITY_DATA_NAME_FK
		and @equalsOperatorCRID = OPERATOR_CR_FK

		IF @insertedVendorFilterID is null
		BEGIN

		INSERT INTO synchronizer.RULES_FILTER
		(ENTITY_CLASS_CR_FK, NAME, 
				 DATA_NAME_FK, ENTITY_DATA_NAME_FK, VALUE1
				,OPERATOR_CR_FK, RECORD_STATUS_CR_FK, CREATE_TIMESTAMP, UPDATE_USER)
		SELECT
		@vendorEntityClassCRID, ((select label from epacube.data_name where data_name_id = @in_vendor_filter_DNID ) +': ' + @in_vendor_filter_value1)
				,@in_vendor_filter_DNID, @vendorEntityDNID, @in_vendor_filter_value1
				,@equalsOperatorCRID, @activeStatusCRID, getdate(), @in_update_user
	
	-- now, fetch the Rules Filter ID for this freshly inserted entry for use in the Rules Filter Set insert:
		set @insertedVendorFilterID = (select rules_filter_id from synchronizer.RULES_FILTER 
										where name =  ((select label from epacube.data_name where data_name_id = @in_vendor_filter_DNID) +': ' + @in_vendor_filter_value1)
										and ENTITY_DATA_NAME_FK = @vendorEntityDNID
										and ENTITY_CLASS_CR_FK = @vendorEntityClassCRID
										and DATA_NAME_FK = @in_vendor_filter_DNID
										and VALUE1 = @in_vendor_filter_value1)
	END	-- END Vendor Filter Insert
	END -- End check to see if we should insert or fetch existing



	--Now, for the Rules Filter Set insert.

--	IF (((@insertedOrgFilterID) is not null) or (@insertedProductFilterID is not null) or (@insertedVendorFilterID is not null))
--	BEGIN
		INSERT INTO synchronizer.RULES_FILTER_SET
		(RULES_FK, PROD_FILTER_FK, ORG_FILTER_FK, SUPL_FILTER_FK, ORG_FILTER_ASSOC_DN_FK, SUPL_FILTER_ASSOC_DN_FK
			,INCL_EXCL, RECORD_STATUS_CR_FK, CREATE_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_USER)
		SELECT
		@insertedRuleID, @insertedProductFilterID, @insertedOrgFilterID, @insertedVendorFilterID, @in_warehouse_filter_assoc_DNID, @in_vendor_filter_assoc_DNID  --@insertedOrgFilterID, @insertedVendorFilterID
			,'INCL', @activeStatusCRID, getdate(), getdate(), @in_update_user
--	END --END insert of Rules filter set

	-------------------------------------------------------------------------------------
	--   Insert into Rules Action STRFunction for first set of values
	-------------------------------------------------------------------------------------
	
	IF( @in_substring_start_pos1 is not null or @in_substring_length1 is not null or @in_substring_whole_token_ind1 is not null or
		@in_truncate_first_token_ind1 is not null or @in_remove_spec_chars_ind1 is not null or @in_remove_spec_chars_ind1 is not null or
	    @in_remove_spaces_ind1 is not null or @in_replace_space_char1 is not null or @in_replace_char1 is not null or
		@in_replacing_char1 is not null or @in_replace_string1 is not null or @in_replacing_string1 is not null)
	BEGIN
		INSERT INTO synchronizer.RULES_ACTION_STRFUNCTION
		(RULES_ACTION_FK, SUBSTR_START_POS, SUBSTR_LENGTH, SUBSTR_WHOLE_TOKEN_IND, TRUNC_FIRST_TOKEN_IND, REMOVE_SPEC_CHARS_IND
		 ,SPECIAL_CHARS_SET, REMOVE_SPACES_IND, REPLACING_SPACE_CHAR, REPLACE_CHAR, REPLACING_CHAR, REPLACE_STRING, REPLACING_STRING
		 ,RECORD_STATUS_CR_FK,CREATE_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_USER)
		select 
		 @insertedRuleActionID, @in_substring_start_pos1, @in_substring_length1, @in_substring_whole_token_ind1, @in_truncate_first_token_ind1, @in_remove_spec_chars_ind1
		 ,@in_special_characters_set1, @in_remove_spaces_ind1, @in_replace_space_char1, @in_replace_char1, @in_replacing_char1, @in_replace_string1, @in_replacing_string1
		 ,@in_record_status_cr_fk, getdate(), getdate(), @in_update_user
	END

	-------------------------------------------------------------------------------------
	--   Insert into Rules Action STRFunction for second set of values
	-------------------------------------------------------------------------------------
	--insert into rules
	IF( @in_substring_start_pos2 is not null or @in_substring_length2 is not null or @in_substring_whole_token_ind2 is not null or
		@in_truncate_first_token_ind2 is not null or @in_remove_spec_chars_ind2 is not null or @in_remove_spec_chars_ind2 is not null or
	    @in_remove_spaces_ind2 is not null or @in_replace_space_char2 is not null or @in_replace_char2 is not null or
		@in_replacing_char2 is not null or @in_replace_string2 is not null or @in_replacing_string2 is not null)
	BEGIN
		INSERT INTO synchronizer.RULES_ACTION_STRFUNCTION
		(RULES_ACTION_FK, SUBSTR_START_POS, SUBSTR_LENGTH, SUBSTR_WHOLE_TOKEN_IND, TRUNC_FIRST_TOKEN_IND, REMOVE_SPEC_CHARS_IND
		 ,SPECIAL_CHARS_SET, REMOVE_SPACES_IND, REPLACING_SPACE_CHAR, REPLACE_CHAR, REPLACING_CHAR, REPLACE_STRING, REPLACING_STRING
		 ,RECORD_STATUS_CR_FK,CREATE_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_USER)
		SELECT
		 @insertedRuleActionID, @in_substring_start_pos2, @in_substring_length2, @in_substring_whole_token_ind2, @in_truncate_first_token_ind2, @in_remove_spec_chars_ind2
		 ,@in_special_characters_set2, @in_remove_spaces_ind2, @in_replace_space_char2, @in_replace_char2, @in_replacing_char2, @in_replace_string2, @in_replacing_string2
		 ,@in_record_status_cr_fk, getdate(), getdate(), @in_update_user
	END

	-------------------------------------------------------------------------------------
	--   Insert into Rules Action STRFunction for third set of values
	-------------------------------------------------------------------------------------
	--insert into rules
	IF( @in_substring_start_pos3 is not null or @in_substring_length3 is not null or @in_substring_whole_token_ind3 is not null or
		@in_truncate_first_token_ind3 is not null or @in_remove_spec_chars_ind3 is not null or @in_remove_spec_chars_ind3 is not null or
	    @in_remove_spaces_ind3 is not null or @in_replace_space_char3 is not null or @in_replace_char3 is not null or
		@in_replacing_char3 is not null or @in_replace_string3 is not null or @in_replacing_string3 is not null)
	BEGIN
		INSERT INTO synchronizer.RULES_ACTION_STRFUNCTION
		(RULES_ACTION_FK, SUBSTR_START_POS, SUBSTR_LENGTH, SUBSTR_WHOLE_TOKEN_IND, TRUNC_FIRST_TOKEN_IND, REMOVE_SPEC_CHARS_IND
		 ,SPECIAL_CHARS_SET, REMOVE_SPACES_IND, REPLACING_SPACE_CHAR, REPLACE_CHAR, REPLACING_CHAR, REPLACE_STRING, REPLACING_STRING
		 ,RECORD_STATUS_CR_FK,CREATE_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_USER)
		 SELECT
		 @insertedRuleActionID, @in_substring_start_pos3, @in_substring_length3, @in_substring_whole_token_ind3, @in_truncate_first_token_ind3, @in_remove_spec_chars_ind3
		 ,@in_special_characters_set3, @in_remove_spaces_ind3, @in_replace_space_char3, @in_replace_char3, @in_replacing_char3, @in_replace_string3, @in_replacing_string3
		 ,@in_record_status_cr_fk, getdate(), getdate(), @in_update_user
	END
    
  END --- IF @in_ui_action_cr_fk = @createUIActionCodeRefID


-------------------------------------------------------------------------------------
--   Perform UI Change Action
-------------------------------------------------------------------------------------
  IF @in_ui_action_cr_fk = @changeUIActionCodeRefID
  BEGIN
	
	--Update the Rules Table
	UPDATE synchronizer.rules
	set  RULES_STRUCTURE_FK = @in_rule_structure_id
		,RULES_SCHEDULE_FK = @in_rule_schedule_id		
		,EFFECTIVE_DATE = @in_effective_date
		,END_DATE = @in_end_date
		,RECORD_STATUS_CR_FK = @in_record_status_cr_fk
		,RULE_PRECEDENCE = @in_rule_precedence
		,UPDATE_TIMESTAMP = getdate()
		,UPDATE_USER = @in_update_user
	where rules_id = @in_rules_id

	------------------------------------
	-- Update the Rules Action
	------------------------------------
	

	update synchronizer.RULES_ACTION
	set  RULES_ACTION_OPERATION1_FK = @in_rule_action_operation1
		,BASIS1_DN_FK = @in_basis1_DNID
		,BASIS1_NUMBER = @in_basis1_number
		,BASIS1_VALUE = @in_basis1_value		
		,PREFIX1 = @in_prefix1
		,BASIS2_DN_FK = @in_basis2_DNID
		,BASIS2_NUMBER = @in_basis2_number		
		,BASIS2_VALUE = @in_basis2_value
		,PREFIX2 = @in_prefix2
		,BASIS3_DN_FK = @in_basis3_DNID			
		,BASIS3_VALUE = @in_basis3_value	
		,ERROR_NAME = @in_error_name
		,UPDATE_TIMESTAMP = getdate()
		,UPDATE_USER = @in_update_user
	where rules_fk = @in_rules_id


	


	------------------------------------
	--update synchronizer.rules_filter
	------------------------------------
	-- First, get the Rules Filter IDs for each of the three filter types (Product, Warehouse/Org, Vendor/Supplier
	-- from Rules Filter Set

	set @prodFilterID = (select prod_filter_fk from synchronizer.rules_filter_set where rules_fk = @in_rules_id)
	set @vendorFilterID = (select supl_filter_fk from synchronizer.rules_filter_set where rules_fk = @in_rules_id)
	set @orgFilterID = (select org_filter_fk from synchronizer.rules_filter_set where rules_fk = @in_rules_id)

	-----------------------------------
	-- If @prodFilterID is not null, update the corresponding Rule Filter row
	-----------------------------------

	IF (@prodFilterID is not null)
	BEGIN
		update synchronizer.RULES_FILTER
		set  DATA_NAME_FK = @in_product_filter_DNID
			,VALUE1 = @in_product_filter_value1				
			,UPDATE_USER = @in_update_user
		where RULES_FILTER_ID = @prodFilterID
	END

	IF (@vendorFilterID is not null)
	BEGIN
		update synchronizer.RULES_FILTER
		set  DATA_NAME_FK = @in_vendor_filter_DNID
			,VALUE1 = @in_vendor_filter_value1				
			,UPDATE_USER = @in_update_user
		where RULES_FILTER_ID = @vendorFilterID
	END

	IF (@orgFilterID is not null)
	BEGIN
		update synchronizer.RULES_FILTER
		set  DATA_NAME_FK = @in_warehouse_filter_DNID
			,VALUE1 = @in_warehouse_filter_value1				
			,UPDATE_USER = @in_update_user
		where RULES_FILTER_ID = @orgFilterID
	END

	------------------------------------
	--update synchronizer.rules_filter_set
	------------------------------------

	update synchronizer.rules_filter_set
		set  ORG_FILTER_ASSOC_DN_FK = @in_warehouse_filter_assoc_DNID
			,SUPL_FILTER_ASSOC_DN_FK = @in_vendor_filter_assoc_DNID			
			,UPDATE_TIMESTAMP = getdate()
			,UPDATE_USER = @in_update_user
	where rules_fk = @in_rules_id


	--------------------------------------
	-- update STRFunction 1
	--------------------------------------
	IF( @in_substring_start_pos1 is not null or @in_substring_length1 is not null or @in_substring_whole_token_ind1 is not null or
		@in_truncate_first_token_ind1 is not null or @in_remove_spec_chars_ind1 is not null or @in_remove_spec_chars_ind1 is not null or
	    @in_remove_spaces_ind1 is not null or @in_replace_space_char1 is not null or @in_replace_char1 is not null or
		@in_replacing_char1 is not null or @in_replace_string1 is not null or @in_replacing_string1 is not null)
	BEGIN
	update synchronizer.RULES_ACTION_STRFUNCTION
		set  SUBSTR_START_POS = @in_substring_start_pos1
			,SUBSTR_LENGTH = @in_substring_length1
			,SUBSTR_WHOLE_TOKEN_IND = @in_substring_whole_token_ind1
			,TRUNC_FIRST_TOKEN_IND = @in_truncate_first_token_ind1
			,REMOVE_SPEC_CHARS_IND = @in_remove_spec_chars_ind1
			,SPECIAL_CHARS_SET = @in_special_characters_set1
			,REMOVE_SPACES_IND = @in_remove_spaces_ind1
			,REPLACING_SPACE_CHAR = @in_replace_space_char1
			,REPLACE_CHAR = @in_replace_char1
			,REPLACING_CHAR = @in_replacing_char1
			,REPLACE_STRING = @in_replace_string1
			,REPLACING_STRING = @in_replacing_string1
			,RECORD_STATUS_CR_FK = @in_record_status_cr_fk
			,UPDATE_TIMESTAMP = getdate()
			,UPDATE_USER = @in_update_user
	END

	--------------------------------------
	-- update STRFunction 2
	--------------------------------------
	IF( @in_substring_start_pos2 is not null or @in_substring_length2 is not null or @in_substring_whole_token_ind2 is not null or
		@in_truncate_first_token_ind2 is not null or @in_remove_spec_chars_ind2 is not null or @in_remove_spec_chars_ind2 is not null or
	    @in_remove_spaces_ind2 is not null or @in_replace_space_char2 is not null or @in_replace_char2 is not null or
		@in_replacing_char2 is not null or @in_replace_string2 is not null or @in_replacing_string2 is not null)
	BEGIN
	update synchronizer.RULES_ACTION_STRFUNCTION
		set  SUBSTR_START_POS = @in_substring_start_pos2
			,SUBSTR_LENGTH = @in_substring_length2
			,SUBSTR_WHOLE_TOKEN_IND = @in_substring_whole_token_ind2
			,TRUNC_FIRST_TOKEN_IND = @in_truncate_first_token_ind2
			,REMOVE_SPEC_CHARS_IND = @in_remove_spec_chars_ind2
			,SPECIAL_CHARS_SET = @in_special_characters_set2
			,REMOVE_SPACES_IND = @in_remove_spaces_ind2
			,REPLACING_SPACE_CHAR = @in_replace_space_char2
			,REPLACE_CHAR = @in_replace_char2
			,REPLACING_CHAR = @in_replacing_char2
			,REPLACE_STRING = @in_replace_string2
			,REPLACING_STRING = @in_replacing_string2
			,RECORD_STATUS_CR_FK = @in_record_status_cr_fk
			,UPDATE_TIMESTAMP = getdate()
			,UPDATE_USER = @in_update_user
	END

	--------------------------------------
	-- update STRFunction 3
	--------------------------------------
	IF( @in_substring_start_pos3 is not null or @in_substring_length3 is not null or @in_substring_whole_token_ind3 is not null or
		@in_truncate_first_token_ind3 is not null or @in_remove_spec_chars_ind3 is not null or @in_remove_spec_chars_ind3 is not null or
	    @in_remove_spaces_ind3 is not null or @in_replace_space_char3 is not null or @in_replace_char3 is not null or
		@in_replacing_char3 is not null or @in_replace_string3 is not null or @in_replacing_string3 is not null)
	BEGIN
	update synchronizer.RULES_ACTION_STRFUNCTION
		set  SUBSTR_START_POS = @in_substring_start_pos3
			,SUBSTR_LENGTH = @in_substring_length3
			,SUBSTR_WHOLE_TOKEN_IND = @in_substring_whole_token_ind3
			,TRUNC_FIRST_TOKEN_IND = @in_truncate_first_token_ind3
			,REMOVE_SPEC_CHARS_IND = @in_remove_spec_chars_ind3
			,SPECIAL_CHARS_SET = @in_special_characters_set3
			,REMOVE_SPACES_IND = @in_remove_spaces_ind3
			,REPLACING_SPACE_CHAR = @in_replace_space_char3
			,REPLACE_CHAR = @in_replace_char3
			,REPLACING_CHAR = @in_replacing_char3
			,REPLACE_STRING = @in_replace_string3
			,REPLACING_STRING = @in_replacing_string3
			,RECORD_STATUS_CR_FK = @in_record_status_cr_fk
			,UPDATE_TIMESTAMP = getdate()
			,UPDATE_USER = @in_update_user
	END

	--(RULES_ACTION_FK, SUBSTR_START_POS, SUBSTR_LENGTH, SUBSTR_WHOLE_TOKEN_IND, TRUNC_FIRST_TOKEN_IND, REMOVE_SPEC_CHARS_IND
	--	 ,SPECIAL_CHARS_SET, REMOVE_SPACES_IND, REPLACING_SPACE_CHAR, REPLACE_CHAR, REPLACING_CHAR, REPLACE_STRING, REPLACING_STRING
	--	 ,RECORD_STATUS_CR_FK,CREATE_TIMESTAMP, UPDATE_TIMESTAMP, UPDATE_USER)


-------------------------------------------------------------------------------------
--	
-------------------------------------------------------------------------------------

---Make sure it's not getting changed to something that already exists

			 IF @@rowcount > 0
					GOTO RAISE_ERROR
	
  End -- IF @in_ui_action_cr_fk = @changeUIActionCodeRefID

RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END
