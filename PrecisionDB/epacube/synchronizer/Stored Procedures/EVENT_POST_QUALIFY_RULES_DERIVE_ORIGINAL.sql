﻿--A_epaMAUI_EVENT_POST_QUALIFY_RULES_DERIVE


-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To determine all qualified rules for Event_Data records in Batch
--				Validation and Calculations Rules assigned to Events
--				Derivation Rules will be created with NEW DATA = @
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        10/19/2008   Initial SQL Version
-- CV		 03/11/2010   Create event data type from result data name
-- CV        08/20/2012   Add is null around import job fk 
-- KS        10/13/2012   SUP-586 Rewrote logic to reduce the number of rows in 
--                        the #TS_RULE_DERIVE_TRIGGER_EVENTS table and moved the
--                        logic from EVENT_POST_DERIVATION here to save on memory
-- GHS		 9/12/2013    Added temp tables to minimize records to search within "IN' clauses.
-- GHS		 5/27/2015    Restructured

CREATE PROCEDURE [synchronizer].[EVENT_POST_QUALIFY_RULES_DERIVE_ORIGINAL] 
               ( @in_batch_no BIGINT, @in_result_data_name_fk INT  )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_QUALIFY_RULES_DERIVE.' 
				 + ' BATCH:: '
				 + CAST ( ISNULL ( @in_batch_no, 999999999 ) AS VARCHAR(16) )	
				 + ' RESULT:: '	     
				 + CAST ( ISNULL ( @in_result_data_name_fk, 999999999 ) AS VARCHAR(16) )	     	          	     
         
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;



------------------------------------------------------------------------------------------
--   QUALIFY RULES ( DEFAULTS and DERIVATIONS ) 
--	    Upon return of this procedure 
--		Event_Data Source = 72 ( Derived Data ) Event Status = 88 
--		Event_Data_Rules only once with Result Rank = 1
------------------------------------------------------------------------------------------



		
------------------------------------------------------------------------------------------
--   Create Temp Table for Events to be assigned Rules
--		Note this temp table is used for all rule routines
--		If changed it must be changed in all places !!!!
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_EVENTS') is not null
	   drop table #TS_RULE_EVENTS;
	   
	IF object_id('tempdb..#P_FKs') is not null
	   drop table #P_FKs;
	   
	IF object_id('tempdb..#V_FKs') is not null
	   drop table #V_FKs;

	select PROD_FILTER_FK into #P_FKs from synchronizer.RULES_FILTER_SET rfs
	inner join synchronizer.RULES r on rfs.RULES_FK = r.RULES_ID
	where RESULT_DATA_NAME_FK = @in_result_data_name_fk
	Group by PROD_FILTER_FK

	Create index idx_pfks on #P_FKs(PROD_FILTER_FK)

	select SUPL_FILTER_FK into #V_FKs from synchronizer.RULES_FILTER_SET rfs
	inner join synchronizer.RULES r on rfs.RULES_FK = r.RULES_ID
	where RESULT_DATA_NAME_FK = @in_result_data_name_fk
	Group by SUPL_FILTER_FK

	Create index idx_vfks on #V_FKs(SUPL_FILTER_FK)
		   
	-- create temp table
	CREATE TABLE #TS_RULE_EVENTS(
	    TS_RULE_EVENTS_ID        BIGINT IDENTITY(1000,1) NOT NULL,
		RULE_TYPE_CR_FK          INT NULL,	    
		RESULT_DATA_NAME_FK		 INT NULL,						
		RESULT_TYPE_CR_FK        INT NULL,
		RESULT_EFFECTIVE_DATE    DATETIME NULL,		
	    PRODUCT_STRUCTURE_FK     BIGINT NULL,
	    ORG_ENTITY_STRUCTURE_FK  BIGINT NULL,
	    CUST_ENTITY_STRUCTURE_FK BIGINT NULL,
	    SUPL_ENTITY_STRUCTURE_FK BIGINT NULL,
        JOB_FK                   BIGINT NULL,	    
        TRIGGER_EVENT_FK         BIGINT NULL,
        EVENT_TABLE_NAME         VARCHAR(32) NULL,
		 )


--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT OF #TS_RULE_EVENTS
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSRE_TSREID ON #TS_RULE_EVENTS 
	(TS_RULE_EVENTS_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSRE_RDNF_REDT_IEID] ON #TS_RULE_EVENTS 
	(RESULT_DATA_NAME_FK ASC
	,RESULT_EFFECTIVE_DATE ASC
	,RESULT_TYPE_CR_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRE_PSF_IEF ON #TS_RULE_EVENTS 
	(PRODUCT_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRE_OESF_IEF ON #TS_RULE_EVENTS 
	(ORG_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRE_CESF_IEF ON #TS_RULE_EVENTS 
	(CUST_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRE_SESF_IEF ON #TS_RULE_EVENTS 
	(SUPL_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

----- ONLY USED WITH DERIVATIONS BECAUSE NEED TO LINK WITH NEWLY CREATE EVENT 
	CREATE NONCLUSTERED INDEX IDX_TSRE_INSERT_EVENT_DATA_RULES ON #TS_RULE_EVENTS 
	(JOB_FK ASC,
     TS_RULE_EVENTS_ID ASC,
     RESULT_DATA_NAME_FK ASC,
     PRODUCT_STRUCTURE_FK ASC,
	 ORG_ENTITY_STRUCTURE_FK ASC
	)
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

----- NEXT TWO USED IN DRANK
	CREATE NONCLUSTERED INDEX [IDX_TSRE_RDN_PRD_ORG_IEID] ON #TS_RULE_EVENTS 
	(RESULT_DATA_NAME_FK ASC
	,PRODUCT_STRUCTURE_FK ASC
	,ORG_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSRE_EFFDT_IEID] ON #TS_RULE_EVENTS 
	(RESULT_EFFECTIVE_DATE DESC
	,TS_RULE_EVENTS_ID DESC
	)
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)





TRUNCATE TABLE #TS_RULE_EVENTS


--------------------------------------------------------------------------------------------
--  CREATE ENTRIES FOR RELATED EVENT_DATA_HISTORY EVENTS IN THE SAME BATCH
--------------------------------------------------------------------------------------------
			
INSERT INTO #TS_RULE_EVENTS(
		 RULE_TYPE_CR_FK	     
		,RESULT_DATA_NAME_FK	
		,RESULT_TYPE_CR_FK	     
		,RESULT_EFFECTIVE_DATE	
	    ,PRODUCT_STRUCTURE_FK   
	    ,ORG_ENTITY_STRUCTURE_FK 
	    ,CUST_ENTITY_STRUCTURE_FK
	    ,SUPL_ENTITY_STRUCTURE_FK
	    ,JOB_FK
        ,TRIGGER_EVENT_FK
        ,EVENT_TABLE_NAME
		)
	SELECT DISTINCT
	     RH.RULE_TYPE_CR_FK
		,R.RESULT_DATA_NAME_FK
		,710  -- CURRENT RESULT TYPE 
		,ED.EVENT_EFFECTIVE_DATE   		   
		,ED.PRODUCT_STRUCTURE_FK   
		,ED.ORG_ENTITY_STRUCTURE_FK 
		,NULL  -- CUST_ENTITY_STRUCTURE_FK
		,ED.ENTITY_STRUCTURE_FK  -- as supplier
		,ED.IMPORT_JOB_FK          
		,ED.EVENT_ID
		,'EVENT_DATA_HISTORY'
---------------------------	
FROM synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
			 ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
			 AND  RS.RECORD_STATUS_CR_FK = 1 )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)
	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID 
    AND  R.RECORD_STATUS_CR_FK = 1 )
INNER JOIN synchronizer.V_RULE_RELATED_DATA_NAMES VRDN
    ON ( R.RESULT_DATA_NAME_FK = VRDN.RESULT_DATA_NAME_FK ) 
INNER JOIN synchronizer.EVENT_DATA_HISTORY ED WITH (NOLOCK)
	ON  ( ED.DATA_NAME_FK = VRDN.RELATED_DATA_NAME_FK ) 
--------------
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
    ON ( RFS.RULES_FK = R.RULES_ID 
	AND  (  ( RFS.PROD_FILTER_FK IS NULL AND RFS.SUPL_FILTER_FK IS NULL )
		 OR ( RFS.SUPL_FILTER_FK IS NULL AND RFS.PROD_FILTER_FK IN 
                                   (  SELECT VRFP.RULES_FILTER_FK
									  FROM synchronizer.V_RULE_FILTERS_PROD VRFP
									  INNER JOIN #P_FKs pfks on VRFP.RULES_FILTER_FK = pfks.PROD_FILTER_FK
									  WHERE VRFP.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK ) )
		 OR ( RFS.PROD_FILTER_FK IS NULL AND RFS.SUPL_FILTER_FK IN 
                                   (  SELECT VRFE.RULES_FILTER_FK
									  FROM epacube.product_association pas WITH (NOLOCK) 
									  INNER JOIN synchronizer.V_RULE_FILTERS_ENTITY VRFE
										  ON ( VRFE.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK 
										  AND  VRFE.ENTITY_CLASS_CR_FK = 10103 )
									  INNER JOIN #V_FKs VFKS ON VRFE.RULES_FILTER_FK = VFKS.SUPL_FILTER_FK
                                      WHERE pas.product_structure_fk = ED.product_structure_fk 
                                      and   pas.ORG_ENTITY_STRUCTURE_FK = ed.ORG_ENTITY_STRUCTURE_FK
								      AND   pas.DATA_NAME_FK  = RFS.SUPL_FILTER_ASSOC_DN_FK  ) )
		 OR ( ( RFS.PROD_FILTER_FK IN 
                                   (  SELECT VRFP.RULES_FILTER_FK
									  FROM synchronizer.V_RULE_FILTERS_PROD VRFP
									  INNER JOIN #P_FKs pfks on VRFP.RULES_FILTER_FK = pfks.PROD_FILTER_FK
									  WHERE VRFP.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK ) )
            AND
		      ( RFS.SUPL_FILTER_FK IN 
                                   (  SELECT VRFE.RULES_FILTER_FK
									  FROM epacube.product_association pas WITH (NOLOCK) 
									  INNER JOIN synchronizer.V_RULE_FILTERS_ENTITY VRFE
										  ON ( VRFE.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK 
										  AND  VRFE.ENTITY_CLASS_CR_FK = 10103 ) 
									  INNER JOIN #V_FKs VFKS ON VRFE.RULES_FILTER_FK = VFKS.SUPL_FILTER_FK
                                      WHERE pas.product_structure_fk = ED.product_structure_fk 
                                      AND   pas.ORG_ENTITY_STRUCTURE_FK = ed.ORG_ENTITY_STRUCTURE_FK
								      AND   pas.DATA_NAME_FK  = RFS.SUPL_FILTER_ASSOC_DN_FK  ) )
             )
      )  )
-------------- check related event and result event are at same org level --- see below
INNER JOIN EPACUBE.DATA_NAME DNE ON DNE.DATA_NAME_ID = ED.DATA_NAME_FK
INNER JOIN EPACUBE.DATA_SET DSE ON DSE.DATA_SET_ID = DNE.DATA_SET_FK
--------------
INNER JOIN EPACUBE.DATA_NAME DN ON DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK
INNER JOIN EPACUBE.DATA_SET DS ON DS.DATA_SET_ID = DN.DATA_SET_FK
--------------
LEFT JOIN synchronizer.event_data edx WITH (NOLOCK)
    ON ( edx.data_name_fk = r.result_data_name_fk
    AND  edx.product_structure_fk = ed.product_structure_fk
    AND  (   ds.org_ind IS NULL 
          OR ISNULL ( edx.org_entity_structure_fk, -999 ) = ISNULL ( ed.org_entity_structure_fk, -999 ) )
    AND  (   ds.entity_structure_ec_cr_fk IS NULL
          OR ISNULL ( edx.entity_structure_fk, -999 ) = ISNULL ( ed.entity_structure_fk, -999 ) ) 
    AND  edx.event_status_cr_fk IN ( 80, 84, 86, 87, 88, 89 ) 
)
-------
LEFT JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD
    ON ( VCPD.DATA_NAME_FK = R.RESULT_DATA_NAME_FK
    AND  VCPD.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
    AND  (   ds.org_ind IS NULL 
          OR ISNULL ( VCPD.ORG_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ed.org_entity_structure_fk, -999 ) )
    AND  (   ds.entity_structure_ec_cr_fk IS NULL
          OR ISNULL ( VCPD.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ed.entity_structure_fk, -999 ) )
       )
----------------       
WHERE 1 = 1
AND   RH.RULE_TYPE_CR_FK IN ( 301, 302, 303 )    -- DEFAULT, DERIVATION OR GLOBAL COPY
AND   RH.RECORD_STATUS_CR_FK = 1
----
AND   ED.BATCH_NO = @in_batch_no
AND   ED.EVENT_STATUS_CR_FK IN ( 81,83 )                        --- STATUS APPROVED
AND   ED.EVENT_TYPE_CR_FK IN ( 150, 153, 154, 155, 156, 160 )   --- DERIVATION TYPES MUST HAVE NEW DATA
-----------
AND  (    R.TRIGGER_EVENT_TYPE_CR_FK IS NULL
     OR   R.TRIGGER_EVENT_TYPE_CR_FK = ED.EVENT_TYPE_CR_FK  )
AND  (    R.TRIGGER_EVENT_DN_FK IS NULL
     OR   R.TRIGGER_EVENT_DN_FK = ED.DATA_NAME_FK  )
AND   (   R.TRIGGER_EVENT_VALUE1 IS NULL
      OR  ISNULL ( R.TRIGGER_EVENT_VALUE1, 'NULL DATA') = ISNULL ( ED.NEW_DATA, 'NULL DATA')  ) 
AND  (    R.TRIGGER_EVENT_ACTION_CR_FK IS NULL
     OR   R.TRIGGER_EVENT_ACTION_CR_FK = ED.EVENT_ACTION_CR_FK )
AND  (    R.TRIGGER_IMPORT_PACKAGE_FK IS NULL
     OR   R.TRIGGER_IMPORT_PACKAGE_FK = ED.IMPORT_PACKAGE_FK )
AND  (    R.TRIGGER_EVENT_SOURCE_CR_FK IS NULL
     OR   R.TRIGGER_EVENT_SOURCE_CR_FK = ED.EVENT_SOURCE_CR_FK )    
-----------------
AND  (    
     (    RS.RULE_TYPE_CR_FK = 301    ----  DEFAULT 
     AND  VCPD.TABLE_ID_FK IS NULL    ----  NOT CURRENT VALUE EXISTS
     AND  EDX.EVENT_ID IS NULL   )    ----  AND event does not already exists in same batch or import     
     OR
     (    RS.RULE_TYPE_CR_FK = 302    ----  DERIVATION
     AND  EDX.EVENT_ID IS NULL   )    ----  AND event does not already exists in same batch or import     
     OR
     (    RS.RULE_TYPE_CR_FK = 303    ----  GLOBAL COPY
     AND  (  ED.ORG_ENTITY_STRUCTURE_FK = 1    
          OR ED.DATA_NAME_FK = 159100 )  )  --- ONLY APPLY RULE TO CORP EVENTS OR WHSE ASSOC
     )
-------  make sure related event and result are at the same org levels
AND (  (  ED.ORG_ENTITY_STRUCTURE_FK = 1
     AND  ISNULL ( DSE.CORP_IND, -999 ) =  ISNULL ( DS.CORP_IND, -999 ) 
     AND  ISNULL ( DSE.CORP_IND, -999 ) = 1 )  
    OR   
       (  ED.ORG_ENTITY_STRUCTURE_FK <> 1
     AND  ISNULL ( DSE.ORG_IND, -999 ) =  ISNULL ( DS.ORG_IND, -999 )
     AND  ISNULL ( DSE.ORG_IND, -999 ) = 1 )
     )
-------
AND  R.RESULT_DATA_NAME_FK = @in_result_data_name_fk 



--------------------------------------------------------------------------------------------
--  CREATE ENTRIES FOR RELATED EVENT_DATA EVENTS IN THE SAME BATCH
--------------------------------------------------------------------------------------------


			 
INSERT INTO #TS_RULE_EVENTS(
		 RULE_TYPE_CR_FK	     
		,RESULT_DATA_NAME_FK	
		,RESULT_TYPE_CR_FK	     
		,RESULT_EFFECTIVE_DATE	
	    ,PRODUCT_STRUCTURE_FK   
	    ,ORG_ENTITY_STRUCTURE_FK 
	    ,CUST_ENTITY_STRUCTURE_FK
	    ,SUPL_ENTITY_STRUCTURE_FK
	    ,JOB_FK
        ,TRIGGER_EVENT_FK
        ,EVENT_TABLE_NAME
		 )
	SELECT DISTINCT
	     RH.RULE_TYPE_CR_FK
		,R.RESULT_DATA_NAME_FK
		,710  -- CURRENT RESULT TYPE 
		,ED.EVENT_EFFECTIVE_DATE   		   
		,ED.PRODUCT_STRUCTURE_FK   
		,ED.ORG_ENTITY_STRUCTURE_FK 
		,NULL  -- CUST_ENTITY_STRUCTURE_FK
		,ED.ENTITY_STRUCTURE_FK  -- as supplier
		,ED.IMPORT_JOB_FK          
		,ED.EVENT_ID
		,'EVENT_DATA'
---------------------------	
FROM synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
			 ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
			 AND  RS.RECORD_STATUS_CR_FK = 1 )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)
	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID 
    AND  R.RECORD_STATUS_CR_FK = 1 )
INNER JOIN synchronizer.V_RULE_RELATED_DATA_NAMES VRDN
    ON ( R.RESULT_DATA_NAME_FK = VRDN.RESULT_DATA_NAME_FK ) 
INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
	ON  ( ED.DATA_NAME_FK = VRDN.RELATED_DATA_NAME_FK ) 
--------------
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
    ON ( RFS.RULES_FK = R.RULES_ID 
	AND  (  ( RFS.PROD_FILTER_FK IS NULL AND RFS.SUPL_FILTER_FK IS NULL )
		 OR ( RFS.SUPL_FILTER_FK IS NULL AND RFS.PROD_FILTER_FK IN 
                                   (  SELECT VRFP.RULES_FILTER_FK
									  FROM synchronizer.V_RULE_FILTERS_PROD VRFP
									  INNER JOIN #P_FKs pfks on VRFP.RULES_FILTER_FK = pfks.PROD_FILTER_FK
									  WHERE VRFP.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK ) )
		 OR ( RFS.PROD_FILTER_FK IS NULL AND RFS.SUPL_FILTER_FK IN 
                                   (  SELECT VRFE.RULES_FILTER_FK
									  FROM epacube.product_association pas WITH (NOLOCK) 
									  INNER JOIN synchronizer.V_RULE_FILTERS_ENTITY VRFE
										  ON ( VRFE.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK 
										  AND  VRFE.ENTITY_CLASS_CR_FK = 10103 )
									  INNER JOIN #V_FKs VFKS ON VRFE.RULES_FILTER_FK = VFKS.SUPL_FILTER_FK
                                      WHERE pas.product_structure_fk = ED.product_structure_fk 
                                      and   pas.ORG_ENTITY_STRUCTURE_FK = ed.ORG_ENTITY_STRUCTURE_FK
								      AND   pas.DATA_NAME_FK  = RFS.SUPL_FILTER_ASSOC_DN_FK  ) )
		 OR ( ( RFS.PROD_FILTER_FK IN 
                                   (  SELECT VRFP.RULES_FILTER_FK
									  FROM synchronizer.V_RULE_FILTERS_PROD VRFP
									  INNER JOIN #P_FKs pfks on VRFP.RULES_FILTER_FK = pfks.PROD_FILTER_FK
									  WHERE VRFP.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK ) )
            AND
		      ( RFS.SUPL_FILTER_FK IN 
                                   (  SELECT VRFE.RULES_FILTER_FK
									  FROM epacube.product_association pas WITH (NOLOCK) 
									  INNER JOIN synchronizer.V_RULE_FILTERS_ENTITY VRFE
										  ON ( VRFE.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK 
										  AND  VRFE.ENTITY_CLASS_CR_FK = 10103 ) 
									  INNER JOIN #V_FKs VFKS ON VRFE.RULES_FILTER_FK = VFKS.SUPL_FILTER_FK
                                      WHERE pas.product_structure_fk = ED.product_structure_fk 
                                      AND   pas.ORG_ENTITY_STRUCTURE_FK = ed.ORG_ENTITY_STRUCTURE_FK
								      AND   pas.DATA_NAME_FK  = RFS.SUPL_FILTER_ASSOC_DN_FK  ) )
             )
      )  )
-------------- check related event and result event are at same org level --- see below
INNER JOIN EPACUBE.DATA_NAME DNE ON DNE.DATA_NAME_ID = ED.DATA_NAME_FK
INNER JOIN EPACUBE.DATA_SET DSE ON DSE.DATA_SET_ID = DNE.DATA_SET_FK
--------------
INNER JOIN EPACUBE.DATA_NAME DN ON DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK
INNER JOIN EPACUBE.DATA_SET DS ON DS.DATA_SET_ID = DN.DATA_SET_FK
--------------
LEFT JOIN synchronizer.event_data edx WITH (NOLOCK)
    ON ( edx.data_name_fk = r.result_data_name_fk
    AND  edx.product_structure_fk = ed.product_structure_fk
    AND  (   ds.org_ind IS NULL 
          OR ISNULL ( edx.org_entity_structure_fk, -999 ) = ISNULL ( ed.org_entity_structure_fk, -999 ) )
    AND  (   ds.entity_structure_ec_cr_fk IS NULL
          OR ISNULL ( edx.entity_structure_fk, -999 ) = ISNULL ( ed.entity_structure_fk, -999 ) ) 
    AND  edx.event_status_cr_fk IN ( 80, 84, 86, 87, 88, 89 ) 
)
-------
LEFT JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD
    ON ( VCPD.DATA_NAME_FK = R.RESULT_DATA_NAME_FK
    AND  VCPD.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
    AND  (   ds.org_ind IS NULL 
          OR ISNULL ( VCPD.ORG_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ed.org_entity_structure_fk, -999 ) )
    AND  (   ds.entity_structure_ec_cr_fk IS NULL
          OR ISNULL ( VCPD.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ed.entity_structure_fk, -999 ) )
       )
----------------       
WHERE 1 = 1
AND   RH.RULE_TYPE_CR_FK IN ( 301, 302, 303 )    -- DEFAULT, DERIVATION OR GLOBAL COPY
AND   RH.RECORD_STATUS_CR_FK = 1
----
AND   ED.BATCH_NO = @in_batch_no
AND   ED.EVENT_STATUS_CR_FK IN ( 80, 84, 85, 86, 87, 88 )       --- not a final status
AND   ED.EVENT_TYPE_CR_FK IN ( 150, 153, 154, 155, 156, 160 )   --- DERIVATION TYPES MUST HAVE NEW DATA
-----------
AND  (    R.TRIGGER_EVENT_TYPE_CR_FK IS NULL
     OR   R.TRIGGER_EVENT_TYPE_CR_FK = ED.EVENT_TYPE_CR_FK  )
AND  (    R.TRIGGER_EVENT_DN_FK IS NULL
     OR   R.TRIGGER_EVENT_DN_FK = ED.DATA_NAME_FK  )
AND   (   R.TRIGGER_EVENT_VALUE1 IS NULL
      OR  ISNULL ( R.TRIGGER_EVENT_VALUE1, 'NULL DATA') = ISNULL ( ED.NEW_DATA, 'NULL DATA')  ) 
AND  (    R.TRIGGER_EVENT_ACTION_CR_FK IS NULL
     OR   R.TRIGGER_EVENT_ACTION_CR_FK = ED.EVENT_ACTION_CR_FK )
AND  (    R.TRIGGER_IMPORT_PACKAGE_FK IS NULL
     OR   R.TRIGGER_IMPORT_PACKAGE_FK = ED.IMPORT_PACKAGE_FK )
AND  (    R.TRIGGER_EVENT_SOURCE_CR_FK IS NULL
     OR   R.TRIGGER_EVENT_SOURCE_CR_FK = ED.EVENT_SOURCE_CR_FK )    
-----------------
AND  (    
     (    RS.RULE_TYPE_CR_FK = 301    ----  DEFAULT 
     AND  VCPD.TABLE_ID_FK IS NULL    ----  NOT CURRENT VALUE EXISTS
     AND  EDX.EVENT_ID IS NULL   )    ----  AND event does not already exists in same batch or import     
     OR
     (    RS.RULE_TYPE_CR_FK = 302    ----  DERIVATION
     AND  EDX.EVENT_ID IS NULL   )    ----  AND event does not already exists in same batch or import     
     OR
     (    RS.RULE_TYPE_CR_FK = 303    ----  GLOBAL COPY
     AND  (  ED.ORG_ENTITY_STRUCTURE_FK = 1    
          OR ED.DATA_NAME_FK = 159100 )  )  --- ONLY APPLY RULE TO CORP EVENTS OR WHSE ASSOC
     )
-------  make sure related event and result are at the same org levels
AND (  (  ED.ORG_ENTITY_STRUCTURE_FK = 1
     AND  ISNULL ( DSE.CORP_IND, -999 ) =  ISNULL ( DS.CORP_IND, -999 ) 
     AND  ISNULL ( DSE.CORP_IND, -999 ) = 1 )  
    OR   
       (  ED.ORG_ENTITY_STRUCTURE_FK <> 1
     AND  ISNULL ( DSE.ORG_IND, -999 ) =  ISNULL ( DS.ORG_IND, -999 )
     AND  ISNULL ( DSE.ORG_IND, -999 ) = 1 )
     )
-------
AND  R.RESULT_DATA_NAME_FK = @in_result_data_name_fk 



--------------------------------------------------------------------------------------------
--  CREATE ENTRIES FOR RELATED PRODUCT SUPPLIER ASSOC EVENT_DATA EVENTS IN THE SAME BATCH
--------------------------------------------------------------------------------------------


			 
INSERT INTO #TS_RULE_EVENTS(
		 RULE_TYPE_CR_FK	     
		,RESULT_DATA_NAME_FK	
		,RESULT_TYPE_CR_FK	     
		,RESULT_EFFECTIVE_DATE	
	    ,PRODUCT_STRUCTURE_FK   
	    ,ORG_ENTITY_STRUCTURE_FK 
	    ,CUST_ENTITY_STRUCTURE_FK
	    ,SUPL_ENTITY_STRUCTURE_FK
	    ,JOB_FK
        ,TRIGGER_EVENT_FK
        ,EVENT_TABLE_NAME
		 )
	SELECT DISTINCT
	     RH.RULE_TYPE_CR_FK
		,R.RESULT_DATA_NAME_FK
		,710  -- CURRENT RESULT TYPE 
		,ED.EVENT_EFFECTIVE_DATE   		   
		,ED.PRODUCT_STRUCTURE_FK   
		,ED.ORG_ENTITY_STRUCTURE_FK 
		,NULL  -- CUST_ENTITY_STRUCTURE_FK
		,ED.ENTITY_STRUCTURE_FK  -- as supplier
		,ED.IMPORT_JOB_FK          
		,ED.EVENT_ID
		,'EVENT_DATA'
---------------------------	
FROM synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
			 ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
			 AND  RS.RECORD_STATUS_CR_FK = 1 )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)
	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID 
    AND  R.RECORD_STATUS_CR_FK = 1 )
INNER JOIN synchronizer.V_RULE_RELATED_DATA_NAMES VRDN
    ON ( R.RESULT_DATA_NAME_FK = VRDN.RESULT_DATA_NAME_FK ) 
INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
	ON  ( ED.DATA_NAME_FK = VRDN.RELATED_DATA_NAME_FK ) 
-------------- FUTURE PRODUCT SUPPLIER ASSOC EVENTS
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
    ON ( RFS.RULES_FK = R.RULES_ID 
    AND  RFS.SUPL_FILTER_ASSOC_DN_FK = ED.DATA_NAME_FK
    AND  RFS.SUPL_FILTER_FK IN (  SELECT F.RULES_FILTER_ID
                                  FROM synchronizer.rules_filter f WITH (NOLOCK)
                                  INNER JOIN #V_FKs VFKS ON F.RULES_FILTER_ID = VFKS.SUPL_FILTER_FK
                                  INNER JOIN epacube.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
                                   ON (  F.DATA_NAME_FK = EI.DATA_NAME_FK
                                   AND   F.VALUE1 = EI.VALUE )
                                  WHERE F.RECORD_STATUS_CR_FK = 1
                                  AND   EI.ENTITY_STRUCTURE_FK = ED.ENTITY_STRUCTURE_FK  ) )
-------------- check related event and result event are at same org level --- see below
INNER JOIN EPACUBE.DATA_NAME DNE ON DNE.DATA_NAME_ID = ED.DATA_NAME_FK
INNER JOIN EPACUBE.DATA_SET DSE ON DSE.DATA_SET_ID = DNE.DATA_SET_FK
--------------
INNER JOIN EPACUBE.DATA_NAME DN ON DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK
INNER JOIN EPACUBE.DATA_SET DS ON DS.DATA_SET_ID = DN.DATA_SET_FK
--------------
LEFT JOIN synchronizer.event_data edx WITH (NOLOCK)
    ON ( edx.data_name_fk = r.result_data_name_fk
    AND  edx.product_structure_fk = ed.product_structure_fk
    AND  (   ds.org_ind IS NULL 
          OR ISNULL ( edx.org_entity_structure_fk, -999 ) = ISNULL ( ed.org_entity_structure_fk, -999 ) )
    AND  (   ds.entity_structure_ec_cr_fk IS NULL
          OR ISNULL ( edx.entity_structure_fk, -999 ) = ISNULL ( ed.entity_structure_fk, -999 ) ) 
    AND  edx.event_status_cr_fk IN ( 80, 86, 87, 88, 89 ) 
)
-------
LEFT JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD
    ON ( VCPD.DATA_NAME_FK = R.RESULT_DATA_NAME_FK
    AND  VCPD.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
    AND  (   ds.org_ind IS NULL 
          OR ISNULL ( VCPD.ORG_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ed.org_entity_structure_fk, -999 ) )
    AND  (   ds.entity_structure_ec_cr_fk IS NULL
          OR ISNULL ( VCPD.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ed.entity_structure_fk, -999 ) )
       )
----------------       
WHERE 1 = 1
AND   RS.RULE_TYPE_CR_FK IN ( 301, 302, 303 )    -- DEFAULT, DERIVATION OR GLOBAL COPY
AND   RH.RECORD_STATUS_CR_FK = 1
----
AND   ED.BATCH_NO = @in_batch_no
AND   ED.EVENT_STATUS_CR_FK IN ( 80, 84, 85, 86, 87, 88 )       --- not a final status
AND   ED.EVENT_TYPE_CR_FK IN ( 150, 153, 154, 155, 156, 160 )   --- DERIVATION TYPES MUST HAVE NEW DATA
-----------
AND  (    R.TRIGGER_EVENT_TYPE_CR_FK IS NULL
     OR   R.TRIGGER_EVENT_TYPE_CR_FK = ED.EVENT_TYPE_CR_FK  )
AND  (    R.TRIGGER_EVENT_DN_FK IS NULL
     OR   R.TRIGGER_EVENT_DN_FK = ED.DATA_NAME_FK  )
AND   (   R.TRIGGER_EVENT_VALUE1 IS NULL
      OR  ISNULL ( R.TRIGGER_EVENT_VALUE1, 'NULL DATA') = ISNULL ( ED.NEW_DATA, 'NULL DATA')  ) 
AND  (    R.TRIGGER_EVENT_ACTION_CR_FK IS NULL
     OR   R.TRIGGER_EVENT_ACTION_CR_FK = ED.EVENT_ACTION_CR_FK )
AND  (    R.TRIGGER_IMPORT_PACKAGE_FK IS NULL
     OR   R.TRIGGER_IMPORT_PACKAGE_FK = ED.IMPORT_PACKAGE_FK )
AND  (    R.TRIGGER_EVENT_SOURCE_CR_FK IS NULL
     OR   R.TRIGGER_EVENT_SOURCE_CR_FK = ED.EVENT_SOURCE_CR_FK )    
-----------------
AND  (    
     (    RS.RULE_TYPE_CR_FK = 301    ----  DEFAULT 
     AND  VCPD.TABLE_ID_FK IS NULL    ----  NOT CURRENT VALUE EXISTS
     AND  EDX.EVENT_ID IS NULL   )    ----  AND event does not already exists in same batch or import     
     OR
     (    RS.RULE_TYPE_CR_FK = 302    ----  DERIVATION
     AND  EDX.EVENT_ID IS NULL   )    ----  AND event does not already exists in same batch or import     
     OR
     (    RS.RULE_TYPE_CR_FK = 303    ----  GLOBAL COPY
     AND  (  ED.ORG_ENTITY_STRUCTURE_FK = 1    
          OR ED.DATA_NAME_FK = 159100 )  )  --- ONLY APPLY RULE TO CORP EVENTS OR WHSE ASSOC
     )
-------  make sure related event and result are at the same org levels
AND (  (  ED.ORG_ENTITY_STRUCTURE_FK = 1
     AND  ISNULL ( DSE.CORP_IND, -999 ) =  ISNULL ( DS.CORP_IND, -999 ) 
     AND  ISNULL ( DSE.CORP_IND, -999 ) = 1 )  
    OR   
       (  ED.ORG_ENTITY_STRUCTURE_FK <> 1
     AND  ISNULL ( DSE.ORG_IND, -999 ) =  ISNULL ( DS.ORG_IND, -999 )
     AND  ISNULL ( DSE.ORG_IND, -999 ) = 1 )
     )
-------
AND  R.RESULT_DATA_NAME_FK = @in_result_data_name_fk 




    SET @V_COUNT = ( SELECT COUNT(1) FROM #TS_RULE_EVENTS  )
	   
	SET @status_desc = 'synchronizer.EVENT_POST_QUALIFY_RULES_DERIVE:: BEFORE DRANK DELETE '
	     + ' BATCH:: '
	     + CAST ( ISNULL ( @in_batch_no, 999999999 ) AS VARCHAR(16) )	
	     + ' RESULT:: '	     
	     + CAST ( ISNULL ( @in_result_data_name_fk, 999999999 ) AS VARCHAR(16) )	     	          	     
	     + ' COUNT:: '	     
	     + CAST ( ISNULL ( @V_COUNT, 999999999 ) AS VARCHAR(16) )	     	          	     

    EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;




--------------------------------------------------------------------------------------------
--   DELETE DUPLICATE #TS_RULE_EVENTS
--      SET OLDEST EFFECTIVE DATE EVENT DRANK = 1
--		FOR UNIQUE RESULT DATA NAME FK, PRODUCT/ORG
--------------------------------------------------------------------------------------------

	DELETE 
    FROM #TS_RULE_EVENTS
	WHERE TS_RULE_EVENTS_ID IN (
		SELECT A.TS_RULE_EVENTS_ID
		FROM (
				SELECT
				TS_RULE_EVENTS_ID,
				( DENSE_RANK() OVER ( PARTITION BY RESULT_DATA_NAME_FK
												  ,PRODUCT_STRUCTURE_FK
												  ,ORG_ENTITY_STRUCTURE_FK
							   ORDER BY RESULT_EFFECTIVE_DATE DESC
									   ,TS_RULE_EVENTS_ID DESC ) )AS DRANK
				FROM #TS_RULE_EVENTS
				WHERE 1 = 1
			) A
			WHERE A.DRANK <> 1   --- REMOVE ALL BUT THE TOP ROW FOR EACH RESULT/PROD/ORG
	  )


SET @V_COUNT = ( SELECT COUNT(1) FROM #TS_RULE_EVENTS  )
	   
IF @V_COUNT = 0
BEGIN
	SET @status_desc = 'synchronizer.EVENT_POST_QUALIFY_RULES_DERIVE:: CANCEL NO DERIVATIONS '
	     + ' BATCH:: '
	     + CAST ( ISNULL ( @in_batch_no, 999999999 ) AS VARCHAR(16) )	
	     + ' RESULT:: '	     
	     + CAST ( ISNULL ( @in_result_data_name_fk, 999999999 ) AS VARCHAR(16) )	     	          	     
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
END
ELSE
BEGIN


	SET @status_desc = 'synchronizer.EVENT_POST_QUALIFY_RULES_DERIVE:: QUALIFY DERIVATIONS '
	     + ' BATCH:: '
	     + CAST ( ISNULL ( @in_batch_no, 999999999 ) AS VARCHAR(16) )	
	     + ' RESULT:: '	     
	     + CAST ( ISNULL ( @in_result_data_name_fk, 999999999 ) AS VARCHAR(16) )	     	          	     
	     + ' COUNT:: '	     
	     + CAST ( ISNULL ( @V_COUNT, 999999999 ) AS VARCHAR(16) )	     	          	     

    EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;



-------------------------------------------------------------------------------------------------------
----  resume traditional logic flow for Event_Post_Qualify routines

---------------------------------------------------
-- Temp Table for all Qualified Rules
---------------------------------------------------


--drop temp tables
IF object_id('tempdb..#TS_QUALIFIED_RULES') is not null
   drop table #TS_QUALIFIED_RULES

CREATE TABLE #TS_QUALIFIED_RULES(
	[TS_QUALIFIED_RULES_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[TS_RULE_EVENTS_FK] [bigint] NULL,		
	[TRIGGER_EVENT_FK] [bigint] NULL,			
	[RULES_FK] [bigint] NULL,
	[RESULT_DATA_NAME_FK] [int] NULL,			
	[RESULT_TYPE_CR_FK] [int] NULL,	
	[RESULT_EFFECTIVE_DATE] [DATETIME] NULL,	
	[PROD_DATA_NAME_FK] [int] NULL,	
	[PROD_FILTER_FK] [bigint] NULL,
	[PROD_RESULT_TYPE_CR_FK] [int] NULL,
	[PROD_EFFECTIVE_DATE] [DATETIME] NULL,
	[PROD_EVENT_FK] [BIGINT] NULL,
	[PROD_ORG_ENTITY_STRUCTURE_FK] [BIGINT] NULL,	
	[PROD_CORP_IND] [SMALLINT] NULL,	
	[PROD_ORG_IND] [SMALLINT] NULL,	
	[PROD_FILTER_HIERARCHY] [SMALLINT] NULL,	
	[PROD_FILTER_ORG_PRECEDENCE] [SMALLINT] NULL,	
	[ORG_DATA_NAME_FK] [int] NULL,		
	[ORG_FILTER_FK] [bigint] NULL,
	[ORG_RESULT_TYPE_CR_FK] [int] NULL,	
	[ORG_EFFECTIVE_DATE] [DATETIME] NULL,
	[ORG_EVENT_FK] [BIGINT] NULL,	
	[CUST_DATA_NAME_FK] [int] NULL,		
	[CUST_FILTER_FK] [bigint] NULL,
	[CUST_FILTER_HIERARCHY] [smallint] NULL,	
	[CUST_RESULT_TYPE_CR_FK] [int] NULL,	
	[CUST_EFFECTIVE_DATE] [DATETIME] NULL,
	[CUST_EVENT_FK] [BIGINT] NULL,
	[SUPL_DATA_NAME_FK] [int] NULL,			
	[SUPL_FILTER_FK] [bigint] NULL,
	[SUPL_RESULT_TYPE_CR_FK] [int] NULL,
	[SUPL_EFFECTIVE_DATE] [DATETIME] NULL,
	[SUPL_EVENT_FK] [BIGINT] NULL,
	[RULES_RESULT_TYPE_CR_FK] [int] NULL,	
	[RULES_EFFECTIVE_DATE] [DATETIME] NULL,
	[RULES_END_DATE] [DATETIME] NULL,	
	[RULES_RECORD_STATUS_CR_FK] [int] NULL,
	[RELATED_RULES_FK] [BIGINT] NULL,			
	[PROD_FILTER_ASSOC_DN_FK] [INT] NULL, 		
	[ORG_FILTER_ASSOC_DN_FK] [INT] NULL, 		
	[CUST_FILTER_ASSOC_DN_FK] [INT] NULL, 		
	[SUPL_FILTER_ASSOC_DN_FK] [INT] NULL, 	
    [CONTRACT_PRECEDENCE]  [INT] NULL, 	
    [RULE_PRECEDENCE]  [INT] NULL, 	
	[SCHED_PRECEDENCE]  [INT] NULL, 					
	[STRUC_PRECEDENCE]	 [INT] NULL, 		
    [SCHED_MATRIX_ACTION_CR_FK]  [INT] NULL, 
    [STRUC_MATRIX_ACTION_CR_FK]  [INT] NULL,
    [RESULT_MATRIX_ACTION_CR_FK]  [INT] NULL,
    [PROMOTIONAL_IND]  [INT] NULL,   
    [HOST_RULE_XREF] VARCHAR(128) NULL,
	[RULES_STRUCTURE_FK] [bigint] NULL,					    
	[RULES_SCHEDULE_FK] [bigint] NULL,		
    [RULE_TYPE_CR_FK]  [INT] NULL, 						    					    	
	[PRODUCT_STRUCTURE_FK] [bigint] NULL,					
	[ORG_ENTITY_STRUCTURE_FK] [bigint] NULL,						
	[CUST_ENTITY_STRUCTURE_FK] [bigint] NULL,						
	[SUPL_ENTITY_STRUCTURE_FK] [bigint] NULL,	
	[RULES_CONTRACT_CUSTOMER_FK] [bigint] NULL,			
	[DISQUALIFIED_IND] [smallint] NULL,
	[DISQUALIFIED_COMMENT] VARCHAR(64) NULL,
    [CUST_ENTITY_HIERARCHY] INT NULL
)



--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSQR_TSQRID ON #TS_QUALIFIED_RULES 
	(TS_QUALIFIED_RULES_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSRE_RDNF_REDT_IEID] ON #TS_QUALIFIED_RULES
	(RULES_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_FK, TS_QUALIFIED_RULES_ID, DISQUALIFIED_IND  )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


------------------------------------------------------------------------------------------
---  Call Routines to Quailify Rules
------------------------------------------------------------------------------------------

	EXEC  synchronizer.EVENT_RULE_QUALIFY  WITH RECOMPILE 



------------------------------------------------------------------------------------------
--	INSERT DERIVATION EVENTS --- DERIVED FROM EVENT_DATA 
------------------------------------------------------------------------------------------

	INSERT INTO [synchronizer].[EVENT_DATA]
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_ACTION_CR_FK]			   
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[RESULT_TYPE_CR_FK]				   
			   ,[JOB_CLASS_FK]
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_BATCH_NO]			   
			   ,[BATCH_NO]
			   ,[STG_RECORD_FK]		   
			   ,[RELATED_EVENT_FK]
			   ,[RECORD_STATUS_CR_FK]		   
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])
      SELECT DISTINCT     --- NEED DISTINCT BECAUSE COULD QUALIFY FOR MANY RULES			   		   
			    DS.EVENT_TYPE_CR_FK
			   ,DS.ENTITY_CLASS_CR_FK
			   ,TSRE.RESULT_EFFECTIVE_DATE
			   ,TSRE.PRODUCT_STRUCTURE_FK  -- NOT DOING ENTITY DERIVATIONS YET
			   ,TSRE.RESULT_DATA_NAME_FK
			   ,TSRE.PRODUCT_STRUCTURE_FK	
			   ,CASE ISNULL ( DS.ORG_IND, 0 )
			    WHEN 1 THEN TSRE.ORG_ENTITY_STRUCTURE_FK			   		   
			    ELSE 1
			    END
			   ,DS.ENTITY_STRUCTURE_EC_CR_FK
			   ,CASE ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 )
			    WHEN 0 THEN NULL
			    WHEN 10101 THEN NULL 
			    ELSE DN.ENTITY_DATA_NAME_FK
			    END 		   			   
			   ,CASE ISNULL ( DS.ENTITY_STRUCTURE_EC_CR_FK, 0 )
			    WHEN 0 THEN NULL
			    WHEN 10101 THEN NULL
			    WHEN 10103 THEN TSRE.SUPL_ENTITY_STRUCTURE_FK
			    WHEN 10104 THEN TSRE.CUST_ENTITY_STRUCTURE_FK
			    ELSE NULL
			    END 		   			   
			   ,97
               ,CASE ( 	ISNULL ( VCPD.TABLE_ID_FK, 0 ) )
                WHEN 0 THEN 51
                ELSE 52
                END		   
			   ,87   --- CREATE SUBMITTED IN BATCH
			   ,CASE WHEN TSRE.RESULT_DATA_NAME_FK IN ( SELECT DISTINCT RX.RESULT_DATA_NAME_FK 
													   FROM synchronizer.RULES RX WITH (NOLOCK)
													   INNER JOIN synchronizer.RULES_STRUCTURE RSX WITH (NOLOCK)
														ON ( RX.RULES_STRUCTURE_FK = RSX.RULES_STRUCTURE_ID 
														AND  RX.RECORD_STATUS_CR_FK = 1 
														AND  RSX.RULE_TYPE_CR_FK IN (309 )  )  )  -- COMPLETENESS  
					 THEN 69  -- SET PRIORITY COMPLETENESS
					 ELSE 61  -- AWAITING APPROVAL
			    END			   
			   ,72
			   ,TSRE.RESULT_TYPE_CR_FK
			   ,( SELECT J.JOB_CLASS_FK FROM common.JOB J WITH (NOLOCK)
                  WHERE J.JOB_ID = TSRE.JOB_FK  )			   
			   ,TSRE.JOB_FK
			   ,@in_batch_no             --- MAKE SURE THAT NEW EVENTS HAVE THE PASSED BATCH
			   ,@in_batch_no            
			   ,TSRE.TS_RULE_EVENTS_ID   --- STG_RECORD_FK IS THE QUALIFIED RULE EVENT  ( IMPORTANT NEEDED FOR EVENT_DATA_RULES BELOW )
			   ,TSRE.TRIGGER_EVENT_FK
               ,1  -- RECORD STATUS 			   
			   ,GETDATE ()   --- UPDATE_TIMESTAMP
			   ,'EPACUBE' --- UPDATE_USER
      FROM #TS_RULE_EVENTS TSRE 
      INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
        ON ( DN.DATA_NAME_ID = TSRE.RESULT_DATA_NAME_FK )
      INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
        ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
	  LEFT JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD
		ON ( VCPD.DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK
		AND  VCPD.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK
		AND  VCPD.ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK )
      WHERE 1 = 1
      AND   TSRE.TS_RULE_EVENTS_ID IN 
                ( SELECT TSQR.TS_RULE_EVENTS_FK 
                  FROM #TS_QUALIFIED_RULES TSQR 
                  WHERE ISNULL ( DISQUALIFIED_IND, 0 ) = 0    )   --- EVENT HAS QUALIFIED RULES



/**********************************************************************************/
--   Insert into sychronizer.EVENT_DATA_RULES
--		Insert Current Rules… ONLY if not already there ??
--		Later look into DRANK ?? Or something to remove duplicates
--      Also need to include a SQL for Future / Whatif Rules
/**********************************************************************************/

--- SHOULD NOT HAVE TO DELETE PRIOR TO INSERT BECAUSE DERIVATION
--     EVENTS ARE ALL NEW EVENTS….

	INSERT INTO [synchronizer].[EVENT_DATA_RULES] (
			 EVENT_FK
			,RULES_FK 
			,RESULT_DATA_NAME_FK
			,RESULT_TYPE_CR_FK
			,RESULT_EFFECTIVE_DATE 
	        ,RULES_ACTION_OPERATION_FK
	        ,RULES_EFFECTIVE_DATE
	        ,RULE_PRECEDENCE
	        ,SCHED_PRECEDENCE
            ,UPDATE_TIMESTAMP   ) 
    SELECT DISTINCT
			 ED.EVENT_ID  ---  FROM NEWLY INSERTED EVENTS ABOVE
			,TSQR.RULES_FK   
			,TSQR.RESULT_DATA_NAME_FK      		   
			,TSQR.RESULT_TYPE_CR_FK      	
			,TSQR.RESULT_EFFECTIVE_DATE      	
            ,RA.RULES_ACTION_OPERATION1_FK
		-------- FROM RULE TABLES
		    ,R.EFFECTIVE_DATE
			,R.RULE_PRECEDENCE
			,RSCH.SCHEDULE_PRECEDENCE				
			,GETDATE()  
------
		FROM  #TS_QUALIFIED_RULES TSQR
		INNER JOIN #TS_RULE_EVENTS TSRE
		 ON ( TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK )
		INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
		 ON ( ISNULL(ED.IMPORT_JOB_FK,0) = ISNULL(TSRE.JOB_FK,0)
         AND  ED.BATCH_NO = @in_batch_no
         AND  ED.STG_RECORD_FK = TSRE.TS_RULE_EVENTS_ID 
		 AND  ED.DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK
		 AND  ED.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK
		 AND  ED.ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK
	     AND  ED.EVENT_SOURCE_CR_FK = 72
		    )		 
		INNER JOIN synchronizer.RULES R
		 ON ( R.RULES_ID = TSQR.RULES_FK )
		INNER JOIN synchronizer.RULES_ACTION RA
		 ON ( RA.RULES_FK = R.RULES_ID )
		INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
		  ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK )
		INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
		 ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
		INNER JOIN synchronizer.RULES_SCHEDULE RSCH WITH (NOLOCK) 
		 ON ( RSCH.RULES_SCHEDULE_ID = R.RULES_SCHEDULE_FK )
		WHERE ISNULL ( DISQUALIFIED_IND, 0 ) = 0         



------------------------------------------------------------------------------------------
--  DENSE RANK ON PRECEDENCE
------------------------------------------------------------------------------------------


	UPDATE synchronizer.EVENT_DATA_RULES
	SET RESULT_RANK = 1
	WHERE EVENT_RULES_ID IN (
	SELECT A.EVENT_RULES_ID
	FROM (
	SELECT
		EDR.EVENT_RULES_ID,
		( DENSE_RANK() OVER ( PARTITION BY EDR.EVENT_FK
						ORDER BY EDR.STRUC_PRECEDENCE ASC, EDR.SCHED_PRECEDENCE ASC, EDR.RULE_PRECEDENCE ASC, 
								 EDR.RULES_FK DESC ) ) AS DRANK
	FROM  #TS_QUALIFIED_RULES TSQR
			INNER JOIN #TS_RULE_EVENTS TSRE
			 ON ( TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK )
			INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
			 ON ( ISNULL(ED.IMPORT_JOB_FK,0) = ISNULL(TSRE.JOB_FK,0) 
             AND  ED.STG_RECORD_FK = TSRE.TS_RULE_EVENTS_ID
			 AND  ED.DATA_NAME_FK = TSRE.RESULT_DATA_NAME_FK
			 AND  ED.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK
			 AND  ED.ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK
			 AND  ED.EVENT_SOURCE_CR_FK = 72
			    )		 
		    INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK) 
	         ON ( EDR.EVENT_FK = ED.EVENT_ID )
	WHERE 1 = 1
	) A 
	WHERE DRANK = 1
	)


END --- End for #TS_RULE_EVENTS > 0

------------------------------------------------------------------------------------------
--  Drop Temp Tables
------------------------------------------------------------------------------------------


--drop temp tables
IF object_id('tempdb..#TS_QUALIFIED_RULES') is not null
   drop table #TS_QUALIFIED_RULES


----drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_EVENTS') is not null
	   drop table #TS_RULE_EVENTS;

	IF object_id('tempdb..#P_FKs') is not null
	   drop table #P_FKs;
	   
	IF object_id('tempdb..#V_FKs') is not null
	   drop table #V_FKs;

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_QUALIFY_RULES_DERIVE'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_QUALIFY_RULES_DERIVE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
