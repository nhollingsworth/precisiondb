﻿--A_epaMAUI_EVENT_POST_QUALIFY_RULES_CALC

-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To determine all qualified rules for Calculations
--                Review Basis and Filter Values to determine if changed
--                If so then qualify the Event_Data for calculation
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        01/20/2010   Initial SQL Version
-- KS        02/14/2012   Added CALC BATCH SIZE to epaCUBE Parms for Memory and Disk Management
-- KS        05/22/2012   Added the Disqualify Logic if EXCLUDE_PROMO_IND = 1
-- KS        06/28/2012   EPA-3354:  Last Minute Modification request for V516; FOR SXE ONLY
--                        Disqualify Rules all the rules EXCEPT the latest effective dated rule
--                        that have the exact same filter levels and values
--                        Commented out Disqualify check in where clause to include all rules for certification 
--                          may want to add back in V517
-- KS       07/12/2012    Exclude the all filters from the change right above
-- KS       10/11/2012    EPA-3402 Disqualify Rules when a rule qualified using a global product cateory and a warehouse existed.
--                        Jim Brown "If there is stocked in the OE warehouse then use the ICSW PPT, if it is not then use the ICSC PPT"
-- KS       10/16/2012    EPA-3717 Disqualify a Global Rule if a matching Whse Rule exists with same Cust, Prod and Supl filters
-- KS       11/11/2012    Moved generic Duplicate EDR checking into EVENT_RULES_QUALIFY
-- KS       11/18/2012    Made adjustments to the DisQualify Global Rules Logic... was disqualifying too many rules
--GHS       12/09/2012    Added parameters to distinguish between customers that use products in both their
--                                        Product and Catalog files simultaneously, and those that don't
--GHS       4/16/2013     Added Disqualification Indicator to Rules by vendor that were qualifying for different vendors
--GHS       5/2/2013      CAPTURE DISQUALIFIED RULES
--GHS       10/2013	      Refined capture of Disqualified Rules
--GHS		10/14/2013	  Tied Promotional Exclusion to EXCLUDE_PROMO_IND

CREATE PROCEDURE [synchronizer].[EVENT_POST_QUALIFY_RULES_CALC] 
               ( @in_analysis_job_fk BIGINT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count               BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

DECLARE  @status_desc           varchar (max)
DECLARE  @ErrorMessage          nvarchar(4000)
DECLARE  @ErrorSeverity         int
DECLARE  @ErrorState            int

Declare @Exclude_Promos int
Set @Exclude_Promos = isnull((Select exclude_promos_ind from marginmgr.analysis_job where analysis_job_id = @in_analysis_job_fk), 0)

BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of synchronizer.EVENT_POST_QUALIFY_RULES_CALC.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                                              @exec_id   = @l_exec_no OUTPUT;


--Added by GHS 12/9/2012 to enable appropriate filtering
      IF isnull((SELECT COUNT(*) FROM EPACUBE.EPACUBE_PARAMS WHERE EPACUBE_PARAMS_ID = 113000), 0) = 0
      BEGIN
            INSERT INTO EPACUBE.EPACUBE_PARAMS
            Select 113000, 100, 'PRODUCT AND CATALOG FILE', 0 , NULL, '0 when not, 1 when products can be used in both the PRODUCT and CATALOG files simultaneously'
      END



---------------------------------------------------
-- Temp Table for all Qualified Rules
---------------------------------------------------


--drop temp tables
IF object_id('tempdb..#TS_QUALIFIED_RULES') is not null
   drop table #TS_QUALIFIED_RULES

CREATE TABLE #TS_QUALIFIED_RULES(
      [TS_QUALIFIED_RULES_ID] [bigint] IDENTITY(1000,1) NOT NULL,
      [TS_RULE_EVENTS_FK] [bigint] NULL,        
      [TRIGGER_EVENT_FK] [bigint] NULL,               
      [RULES_FK] [bigint] NULL,
      [RESULT_DATA_NAME_FK] [int] NULL,               
      [RESULT_TYPE_CR_FK] [int] NULL,     
      [RESULT_EFFECTIVE_DATE] [DATETIME] NULL,  
      [PROD_DATA_NAME_FK] [int] NULL,     
      [PROD_FILTER_FK] [bigint] NULL,
      [PROD_RESULT_TYPE_CR_FK] [int] NULL,
      [PROD_EFFECTIVE_DATE] [DATETIME] NULL,
      [PROD_EVENT_FK] [BIGINT] NULL,
      [PROD_ORG_ENTITY_STRUCTURE_FK] [BIGINT] NULL,   
      [PROD_CORP_IND] [SMALLINT] NULL,    
      [PROD_ORG_IND] [SMALLINT] NULL,     
      [PROD_FILTER_HIERARCHY] [SMALLINT] NULL,  
      [PROD_FILTER_ORG_PRECEDENCE] [SMALLINT] NULL,   
      [ORG_DATA_NAME_FK] [int] NULL,            
      [ORG_FILTER_FK] [bigint] NULL,
      [ORG_RESULT_TYPE_CR_FK] [int] NULL, 
      [ORG_EFFECTIVE_DATE] [DATETIME] NULL,
      [ORG_EVENT_FK] [BIGINT] NULL, 
      [CUST_DATA_NAME_FK] [int] NULL,           
      [CUST_FILTER_FK] [bigint] NULL,
      [CUST_FILTER_HIERARCHY] [smallint] NULL,  
      [CUST_RESULT_TYPE_CR_FK] [int] NULL,      
      [CUST_EFFECTIVE_DATE] [DATETIME] NULL,
      [CUST_EVENT_FK] [BIGINT] NULL,
      [SUPL_DATA_NAME_FK] [int] NULL,                 
      [SUPL_FILTER_FK] [bigint] NULL,
      [SUPL_RESULT_TYPE_CR_FK] [int] NULL,
      [SUPL_EFFECTIVE_DATE] [DATETIME] NULL,
      [SUPL_EVENT_FK] [BIGINT] NULL,
      [RULES_RESULT_TYPE_CR_FK] [int] NULL,     
      [RULES_EFFECTIVE_DATE] [DATETIME] NULL,
      [RULES_END_DATE] [DATETIME] NULL,   
      [RULES_RECORD_STATUS_CR_FK] [int] NULL,
      [RELATED_RULES_FK] [BIGINT] NULL,               
      [PROD_FILTER_ASSOC_DN_FK] [INT] NULL,           
      [ORG_FILTER_ASSOC_DN_FK] [INT] NULL,            
      [CUST_FILTER_ASSOC_DN_FK] [INT] NULL,           
      [SUPL_FILTER_ASSOC_DN_FK] [INT] NULL,     
    [CONTRACT_PRECEDENCE]  [INT] NULL,    
    [RULE_PRECEDENCE]  [INT] NULL, 
      [SCHED_PRECEDENCE]  [INT] NULL,                             
      [STRUC_PRECEDENCE]      [INT] NULL,            
    [SCHED_MATRIX_ACTION_CR_FK]  [INT] NULL, 
    [STRUC_MATRIX_ACTION_CR_FK]  [INT] NULL,
    [RESULT_MATRIX_ACTION_CR_FK]  [INT] NULL,
    [PROMOTIONAL_IND]  [INT] NULL,   
    [HOST_RULE_XREF] VARCHAR(128) NULL,
      [RULES_STRUCTURE_FK] [bigint] NULL,                             
      [RULES_SCHEDULE_FK] [bigint] NULL,  
    [RULE_TYPE_CR_FK]  [INT] NULL,                                      
      [PRODUCT_STRUCTURE_FK] [bigint] NULL,                             
      [ORG_ENTITY_STRUCTURE_FK] [bigint] NULL,                                
      [CUST_ENTITY_STRUCTURE_FK] [bigint] NULL,                               
      [SUPL_ENTITY_STRUCTURE_FK] [bigint] NULL, 
      [RULES_CONTRACT_CUSTOMER_FK] [bigint] NULL,                 
      [DISQUALIFIED_IND] [smallint] NULL,
      [DISQUALIFIED_COMMENT] VARCHAR(64) NULL
)



--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------

      CREATE CLUSTERED INDEX IDX_TSQR_TSQRID ON #TS_QUALIFIED_RULES 
      (TS_QUALIFIED_RULES_ID ASC)   
      WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

      CREATE NONCLUSTERED INDEX [IDX_TSRE_RDNF_REDT_IEID] ON #TS_QUALIFIED_RULES
      (RULES_FK ASC
      )
      INCLUDE ( TS_RULE_EVENTS_FK, TS_QUALIFIED_RULES_ID )  
      WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)




------------------------------------------------------------------------------------------
--   Create Temp Table for Events to be assigned Rules
--          Note this temp table is used for all rule routines
--          If changed it must be changed in all places !!!!
------------------------------------------------------------------------------------------


--drop temp table if it exists
      IF object_id('tempdb..#TS_RULE_EVENTS') is not null
         drop table #TS_RULE_EVENTS;


      -- create temp table
      CREATE TABLE #TS_RULE_EVENTS(
          TS_RULE_EVENTS_ID             BIGINT IDENTITY(1000,1) NOT NULL,
            RULE_TYPE_CR_FK          INT NULL,      
            RESULT_DATA_NAME_FK           INT NULL,                                
            RESULT_TYPE_CR_FK        INT NULL,
            RESULT_EFFECTIVE_DATE    DATETIME NULL,         
          PRODUCT_STRUCTURE_FK     BIGINT NULL,
          ORG_ENTITY_STRUCTURE_FK  BIGINT NULL,
          CUST_ENTITY_STRUCTURE_FK BIGINT NULL,
          SUPL_ENTITY_STRUCTURE_FK BIGINT NULL,
        JOB_FK                   BIGINT NULL,       
        TRIGGER_EVENT_FK         BIGINT NULL,
        EVENT_TABLE_NAME         VARCHAR(32) NULL
            )


--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT OF #TS_RULE_EVENTS
--------------------------------------------------------------------------------------------

      CREATE CLUSTERED INDEX IDX_TSRE_TSREID ON #TS_RULE_EVENTS 
      (TS_RULE_EVENTS_ID ASC) 
      WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

      CREATE NONCLUSTERED INDEX [IDX_TSRE_RDNF_REDT_IEID] ON #TS_RULE_EVENTS 
      (RESULT_DATA_NAME_FK ASC
      ,RESULT_EFFECTIVE_DATE ASC
      ,RESULT_TYPE_CR_FK ASC
      )
      INCLUDE ( TS_RULE_EVENTS_ID ) 
      WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

      CREATE NONCLUSTERED INDEX IDX_TSRE_PSF_IEF ON #TS_RULE_EVENTS 
      (PRODUCT_STRUCTURE_FK ASC
      )
      INCLUDE ( TS_RULE_EVENTS_ID ) 
      WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

      CREATE NONCLUSTERED INDEX IDX_TSRE_OESF_IEF ON #TS_RULE_EVENTS 
      (ORG_ENTITY_STRUCTURE_FK ASC
      )
      INCLUDE ( TS_RULE_EVENTS_ID ) 
      WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

      CREATE NONCLUSTERED INDEX IDX_TSRE_CESF_IEF ON #TS_RULE_EVENTS 
      (CUST_ENTITY_STRUCTURE_FK ASC
      )
      INCLUDE ( TS_RULE_EVENTS_ID ) 
      WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

      CREATE NONCLUSTERED INDEX IDX_TSRE_SESF_IEF ON #TS_RULE_EVENTS 
      (SUPL_ENTITY_STRUCTURE_FK ASC
      )
      INCLUDE ( TS_RULE_EVENTS_ID ) 
      WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)




------------------------------------------------------------------------------------------
---  LOOP THROUGH RULES BY ORGS IS COUNT IS LARGE
------------------------------------------------------------------------------------------

SET @v_count = (   SELECT COUNT(1)
                           FROM  synchronizer.EVENT_CALC
                           WHERE  JOB_FK = @in_analysis_job_fk )

      DECLARE  @vm$org_entity_structure_fk  bigint                   

      DECLARE  cur_v_rules_group cursor local for
                  SELECT DISTINCT
                          A.org_entity_structure_fk                        
                    FROM (
                        SELECT 
                               CASE WHEN @v_count >  ISNULL (
                                                     ( select cast ( eep.value as integer )
                                              from epacube.epacube_params eep
                                              where application_scope_fk = 100
                                              and   name = 'CALC BATCH SIZE' )
                                              , 100000 )
                               THEN org_entity_structure_fk
                               ELSE -999
                               END AS org_entity_structure_fk
                        FROM  synchronizer.EVENT_CALC
                        WHERE  JOB_FK = @in_analysis_job_fk
                        GROUP BY org_entity_structure_fk
              ) A
                    ORDER BY  A.org_entity_structure_fk ASC


    OPEN cur_v_rules_group;
    FETCH NEXT FROM cur_v_rules_group INTO   @vm$org_entity_structure_fk 

------------------------------------------------------------------------------------------
---  LOOP THROUGH BASIS STACK OF QUALIFED RULES DATA
------------------------------------------------------------------------------------------


         WHILE @@FETCH_STATUS = 0 
         BEGIN


--------------------------------------------------------------------------------------------
--   INSERT INTO TEMP TABLE FOR INSERT LATER
--------------------------------------------------------------------------------------------

    TRUNCATE TABLE #TS_RULE_EVENTS

      INSERT INTO #TS_RULE_EVENTS(
            RULE_TYPE_CR_FK 
            ,RESULT_DATA_NAME_FK    
            ,RESULT_TYPE_CR_FK           
            ,RESULT_EFFECTIVE_DATE  
          ,PRODUCT_STRUCTURE_FK   
          ,ORG_ENTITY_STRUCTURE_FK 
          ,CUST_ENTITY_STRUCTURE_FK
          ,SUPL_ENTITY_STRUCTURE_FK
          ,JOB_FK
          ,TRIGGER_EVENT_FK 
          ,EVENT_TABLE_NAME
            )
    SELECT DISTINCT 
         EC.RULE_TYPE_CR_FK      ----  RULE_TYPE_CR_FK SHEET RESULTS BY CALCULATION
            ,EC.RESULT_DATA_NAME_FK 
            ,EC.RESULT_TYPE_CR_FK        
            ,EC.RESULT_EFFECTIVE_DATE     
          ,EC.PRODUCT_STRUCTURE_FK   
          ,EC.ORG_ENTITY_STRUCTURE_FK 
          ,EC.CUST_ENTITY_STRUCTURE_FK
          ,EC.SUPL_ENTITY_STRUCTURE_FK
          ,EC.JOB_FK
          ,EC.EVENT_ID
          ,'EVENT_CALC'
    FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
    WHERE EC.JOB_FK = @in_analysis_job_fk
    AND   EC.RECORD_STATUS_CR_FK = 1               --- ONLY ACTIVE EVENTS
    AND   (   @vm$org_entity_structure_fk = -999           
          OR  EC.org_entity_structure_fk = @vm$org_entity_structure_fk )


    SET @v_count = 
        ( SELECT COUNT(1)
          FROM #TS_RULE_EVENTS )

    IF @v_count > 0
    BEGIN

    SET @status_desc =  'synchronizer.EVENT_POST_QUALIFY_RULES_CALC:  '
                        + ' JOB: ' 
                        + CAST ( @in_analysis_job_fk AS VARCHAR(16) )                        
                        + ' WAREHOUSE: ' 
                        + ( ISNULL ( ( SELECT ei.value FROM epacube.entity_identification ei
                            WHERE ei.entity_structure_fk = @vm$org_entity_structure_fk
                            AND   ei.data_name_fk = ( SELECT CAST ( eep.value AS INT ) FROM epacube.epacube_params eep
                                                      WHERE application_scope_fk = 100 AND NAME = 'WAREHOUSE' ) ), 'GRP ALL' ) )
                        + ' EVENT COUNT: '  
                        + CAST (@V_COUNT AS VARCHAR(16) )                              
    EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;



------------------------------------------------------------------------------------------
---  Call Routines to Quailify Rules
--          FUTURE AND WHATIF first..
--          IF RULE not previously qualified then add with CURRENT
--   NOTE:
--          Qualified Rules should be UNIQUE For each EVENT_CALC and Rule FK 
------------------------------------------------------------------------------------------


      EXEC  synchronizer.EVENT_RULE_QUALIFY  WITH RECOMPILE 



    SET @v_count = 
        ( SELECT COUNT(1)
          FROM #TS_QUALIFIED_RULES )

    IF @v_count > 0
    BEGIN

    SET @status_desc =  'synchronizer.EVENT_POST_QUALIFY_RULES_CALC:  '
                        + ' JOB: ' 
                        + CAST ( @in_analysis_job_fk AS VARCHAR(16) )                        
                        + ' WAREHOUSE: ' 
                        + ( ISNULL ( ( SELECT ei.value FROM epacube.entity_identification ei
                            WHERE ei.entity_structure_fk = @vm$org_entity_structure_fk
                            AND   ei.data_name_fk = ( SELECT CAST ( eep.value AS INT ) FROM epacube.epacube_params eep
                                                      WHERE application_scope_fk = 100 AND NAME = 'WAREHOUSE' ) ), 'GRP ALL' ) )
                        + ' QUALIFIED RULES COUNT: '  
                        + CAST (@V_COUNT AS VARCHAR(16) )                              
    EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

    END 


----------------------------------------------------------------------------------
---------- DISQUALIFY FILTER PROD/SUPL ASSOC --- NOT SURE NEED ANYMORE 11/11/2012
----------------------------------------------------------------------------------
--GHS Disqualify Vendor-Specific rules that qualified for the wrong vendor 

      Update TS
      SET DISQUALIFIED_IND = 1
         ,DISQUALIFIED_COMMENT = 'DISQUALIFY SUPL FILTER ASSOC'
      from #TS_QUALIFIED_RULES TS
      inner join synchronizer.RULES_FILTER_SET rfs on TS.RULES_FK = rfs.RULES_FK
      inner join synchronizer.RULES_FILTER rf on rfs.SUPL_FILTER_FK = rf.RULES_FILTER_ID
      inner join epacube.ENTITY_IDENTIFICATION ei on rf.VALUE1 = ei.VALUE and rf.DATA_NAME_FK = ei.DATA_NAME_FK and rf.ENTITY_DATA_NAME_FK = ei.ENTITY_DATA_NAME_FK
      inner join epacube.PRODUCT_ASSOCIATION pa on ts.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK 
                                                                        and TS.org_entity_structure_fk = pa.ORG_ENTITY_STRUCTURE_FK
                                                                        and pa.DATA_NAME_FK = 253501
      where TS.supl_filter_fk is not null and TS.SUPL_ENTITY_STRUCTURE_FK <> ei.ENTITY_STRUCTURE_FK
      
      --UPDATE #TS_QUALIFIED_RULES
      --WHERE 1 = 1
      --AND   SUPL_FILTER_ASSOC_DN_FK IS NOT NULL     --- 253501 FOR TESTING
      --AND   PROD_FILTER_FK  IN (        
      --                SELECT TSQR.PROD_FILTER_FK
      --                FROM #TS_QUALIFIED_RULES TSQR
      --                INNER JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD WITH (NOLOCK)
      --                  ON ( VCPD.DATA_NAME_FK = TSQR.SUPL_FILTER_ASSOC_DN_FK
      --                  AND  VCPD.PRODUCT_STRUCTURE_FK = TSQR.PRODUCT_STRUCTURE_FK
      --                  AND  (     ISNULL ( VCPD.ORG_ENTITY_STRUCTURE_FK, 1 ) = 1 
      --                            OR ( VCPD.ORG_ENTITY_STRUCTURE_FK = TSQR.ORG_ENTITY_STRUCTURE_FK ) )
      --                  AND  (     ISNULL ( VCPD.ENTITY_STRUCTURE_FK, -999 ) = -999
      --                            OR ( VCPD.ENTITY_STRUCTURE_FK = TSQR.SUPL_ENTITY_STRUCTURE_FK ) )
      --                        )
      --                WHERE 1 = 1
      --                AND   VCPD.ENTITY_STRUCTURE_FK IS NULL
      --                      AND   ISNULL ( DISQUALIFIED_IND, 0 ) <> 1                         
      --                )



----------------------------------------------------------------------------------
---------- DISQUALIFY FILTER PROD/ORG ASSOC --- NOT SURE NEED ANYMORE 11/11/2012
----------------------------------------------------------------------------------

--------    UPDATE #TS_QUALIFIED_RULES
--------    SET DISQUALIFIED_IND = 1
--------       ,DISQUALIFIED_COMMENT = 'DISQUALIFY FILTER PROD/ORG/CUST ASSOC'
--------    WHERE 1 = 1
--------    AND   ORG_FILTER_ASSOC_DN_FK IS NOT NULL     --- 159100 FOR TESTING
--------    AND   PROD_FILTER_FK  IN (          
--------                      SELECT TSQR.PROD_FILTER_FK
--------                      FROM #TS_QUALIFIED_RULES TSQR
--------                        INNER JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD WITH (NOLOCK)
--------                        ON ( VCPD.DATA_NAME_FK = 159100
--------                        AND  VCPD.PRODUCT_STRUCTURE_FK = TSQR.PRODUCT_STRUCTURE_FK
--------                        AND  VCPD.ORG_ENTITY_STRUCTURE_FK = TSQR.ORG_ENTITY_STRUCTURE_FK )
--------                      WHERE 1 = 1
--------                      AND   VCPD.ORG_ENTITY_STRUCTURE_FK IS NULL
--------                            AND   ISNULL ( DISQUALIFIED_IND, 0 ) <> 1                          
--------                      )



--------------------------------------------------------------------------
--
--  SET DISQUALFIED GLOBAL RULES WHERE A MATCHING WHSE SPECIFIC RULE EXISTS
-- KS       10/16/2012    EPA-3717 Disqualify a Global Rule if a matching Whse Rule exists 
--                             with same Cust, Prod and Supl filters
--                        Gary said Structure and Schedule do no matter.
--
--------------------------------------------------------------------------

IF ( select eep.value from epacube.EPACUBE_PARAMS eep with (nolock) where eep.application_scope_fk = 100 and eep.name = 'ERP HOST' ) = 'SXE'
BEGIN

--drop temp tables
IF object_id('tempdb..#TS_WHSE_SPECIFIC_RULES') is not null
   drop table #TS_WHSE_SPECIFIC_RULES

CREATE TABLE #TS_WHSE_SPECIFIC_RULES(
      [TS_WHSE_SPECIFIC_RULES_ID] [bigint] IDENTITY(1000,1) NOT NULL,
      [TRIGGER_EVENT_FK] [bigint] NULL,               
      [TS_RULE_EVENTS_FK] [bigint] NULL,
      [PROD_FILTER_FK] [bigint] NULL,     
      [CUST_FILTER_FK] [bigint] NULL,           
      [SUPL_FILTER_FK] [bigint] NULL
)



--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------

      CREATE CLUSTERED INDEX IDX_TSQRD_TSQRDID ON #TS_WHSE_SPECIFIC_RULES
      (TS_WHSE_SPECIFIC_RULES_ID ASC)     
      WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

      CREATE NONCLUSTERED INDEX [IDX_TSQRD_RFS_IEID] ON #TS_WHSE_SPECIFIC_RULES
      (     PROD_FILTER_FK ASC ,CUST_FILTER_FK ASC ,SUPL_FILTER_FK ASC 
      ) 
      INCLUDE ( TS_WHSE_SPECIFIC_RULES_ID )     
      WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

--------------------------------------------------------------------------------------------


            --------------UPDATE #TS_QUALIFIED_RULES 
            --------------SET DISQUALIFIED_IND = NULL
            --------------   ,DISQUALIFIED_COMMENT = NULL


    TRUNCATE TABLE #TS_WHSE_SPECIFIC_RULES

      INSERT INTO #TS_WHSE_SPECIFIC_RULES(
           TRIGGER_EVENT_FK
          ,TS_RULE_EVENTS_FK  
            ,PROD_FILTER_FK
            ,CUST_FILTER_FK
            ,SUPL_FILTER_FK   
            )
            select 
                     TSRE.TRIGGER_EVENT_FK      
                    ,TSRE.TS_RULE_EVENTS_ID     
                    ,TSQR.PROD_FILTER_FK
                    ,TSQR.CUST_FILTER_FK
                    ,TSQR.SUPL_FILTER_FK
            from #TS_RULE_EVENTS TSRE
            inner join #TS_QUALIFIED_RULES TSQR ON TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK
            where 1 = 1
            AND   TSQR.RULE_TYPE_CR_FK IN ( 312, 313 )            
        AND ISNULL ( TSQR.DISQUALIFIED_IND, 0 ) <> 1  
        AND TSQR.ORG_FILTER_FK IS NOT NULL  --- A RULE WHSE FILTER EXISTS      
            group by
                     TSRE.TRIGGER_EVENT_FK      
                    ,TSRE.TS_RULE_EVENTS_ID     
                    ,TSQR.PROD_FILTER_FK
                    ,TSQR.CUST_FILTER_FK
                    ,TSQR.SUPL_FILTER_FK




            UPDATE #TS_QUALIFIED_RULES 
            SET DISQUALIFIED_IND = 1
               ,DISQUALIFIED_COMMENT = 'DisQualify Global Rule when Matching Whse Rule Exists'
        WHERE 1 = 1 
        AND   TS_QUALIFIED_RULES_ID  IN  (        
                    SELECT TSQR.TS_QUALIFIED_RULES_ID 
                              FROM #TS_RULE_EVENTS TSRE
                              INNER JOIN #TS_QUALIFIED_RULES TSQR ON TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK 
                              INNER JOIN #TS_WHSE_SPECIFIC_RULES TSWSR 
                               ON (  TSWSR.TRIGGER_EVENT_FK = TSRE.TRIGGER_EVENT_FK   
                                 AND ISNULL ( TSQR.PROD_FILTER_FK, -999 ) = ISNULL ( TSWSR.PROD_FILTER_FK, -999 )
                                 AND ISNULL ( TSQR.CUST_FILTER_FK, -999 ) = ISNULL ( TSWSR.CUST_FILTER_FK, -999 )
                                 AND ISNULL ( TSQR.SUPL_FILTER_FK, -999 ) = ISNULL ( TSWSR.SUPL_FILTER_FK, -999 )
                         )
                              WHERE 1 = 1
                              AND   TSQR.RULE_TYPE_CR_FK IN ( 312, 313 )                              
                              AND   ISNULL ( DISQUALIFIED_IND, 0 ) <> 1                         
                              AND   TSQR.ORG_FILTER_FK IS NULL   ----   GLOBAL RULE is chosen to DISQUALIFY
                )



/*

---- Save the SQL for TESTING

SELECT TS_RULE_EVENTS_FK, PROD_FILTER_FK, CUST_FILTER_FK, ORG_FILTER_FK, SUPL_FILTER_FK, PROD_FILTER_HIERARCHY, PROD_FILTER_ORG_PRECEDENCE
,EFFECTIVE_DATE, DISQUALIFIED_COMMENT
FROM #TS_QUALIFIED_RULES 
INNER JOIN synchronizer.RULES ON RULES_ID = RULES_FK
WHERE TS_RULE_EVENTS_FK IN ( SELECT TS_RULE_EVENTS_FK FROM #TS_QUALIFIED_RULES where ISNULL ( DISQUALIFIED_IND, 0 ) = 1 )
ORDER BY TS_RULE_EVENTS_FK, PROD_FILTER_FK, CUST_FILTER_FK, ORG_FILTER_FK, SUPL_FILTER_FK, DISQUALIFIED_COMMENT

*/


--------------------------------------------------------------------------
-- DISQUALIFY INAPPROPRIATE RULES … SXE if Product exists at WHSE 
--     EPA-3402                       DISQUALIFY ALL CORP LEVEL Product Filtered 
--                                    RULES for DN's ( 211901, 211903, 211904, 211910 )
--------------------------------------------------------------------------

--Added by GHS on 12/9/2012 for the following code block to be used only where customers 
--use the same products in their Product and Catalog files simultaneously

If (Select Value from epacube.epacube_params where name = 'PRODUCT AND CATALOG FILE') = 1
Begin

            UPDATE #TS_QUALIFIED_RULES 
            SET DISQUALIFIED_IND = 1
               ,DISQUALIFIED_COMMENT = 'ICSW Product Categories To be used ONLY'
        WHERE 1 = 1 
        AND TS_QUALIFIED_RULES_ID IN (
                  SELECT TS_QUALIFIED_RULES_ID
                  FROM #TS_QUALIFIED_RULES 
                  WHERE 1 = 1 
                  AND   RULE_TYPE_CR_FK IN ( 312, 313 )                 
                  AND   ISNULL ( DISQUALIFIED_IND, 0 ) <> 1                                           
                  AND   ISNULL ( PROD_ORG_ENTITY_STRUCTURE_FK, 1 ) = 1
                  AND   PROD_DATA_NAME_FK IN ( 211901, 211902, 211903, 211904, 211910    )
                  AND   TS_RULE_EVENTS_FK IN (
                        SELECT TSRQ.TS_RULE_EVENTS_FK
                        FROM #TS_QUALIFIED_RULES TSRQ
                        INNER JOIN epacube.PRODUCT_ASSOCIATION PAS
                              ON    PAS.PRODUCT_STRUCTURE_FK = TSRQ.PRODUCT_STRUCTURE_FK
                              AND   PAS.DATA_NAME_FK = 159100 -- PROD WHSE
                              AND   PAS.ORG_ENTITY_STRUCTURE_FK = TSRQ.ORG_ENTITY_STRUCTURE_FK
                     )  -- PRODUCT exists at Whse level ignore ALL CORP Product Filters for named DN's
          )
End

--drop temp tables
IF object_id('tempdb..#TS_WHSE_SPECIFIC_RULES') is not null
   drop table #TS_WHSE_SPECIFIC_RULES


END   --- end of SX special DISQUALIFY

--------------------------------------------------------------------------
-- DISQUALIFY DUPLICATE RULES … 
--
--          IE    ECLIPSE SELL GROUP SETS 
--                AND QUALIFIED FILTERS WHERE CATEGORY IS BOTH CORP AND ORG 
--
--------------------------------------------------------------------------

            UPDATE #TS_QUALIFIED_RULES 
            SET DISQUALIFIED_IND = 1
               ,DISQUALIFIED_COMMENT = 'QUALIFIED FILTERS WHERE CATEGORY IS BOTH CORP AND ORG'
        WHERE 1 = 1 
        AND   TS_QUALIFIED_RULES_ID  IN  (
                  SELECT A.TS_QUALIFIED_RULES_ID
                  FROM (
                SELECT
                    TSQR.TS_QUALIFIED_RULES_ID,
                        (     DENSE_RANK() OVER ( PARTITION BY TSQR.TS_RULE_EVENTS_FK, TSQR.RULES_FK
                                                            ORDER BY    TSQR.PROD_FILTER_HIERARCHY ASC, TSQR.PROD_FILTER_ORG_PRECEDENCE ASC, 
                                                                               TSQR.PROD_RESULT_TYPE_CR_FK DESC,  TSQR.PROD_EFFECTIVE_DATE DESC, 
                                                                               TSQR.TS_QUALIFIED_RULES_ID DESC ) 
                     ) AS DRANK
                              FROM #TS_QUALIFIED_RULES TSQR WITH (NOLOCK) 
                              WHERE 1 = 1
                              AND   TSQR.RULE_TYPE_CR_FK IN ( 312, 313 )                              
                              AND   ISNULL ( DISQUALIFIED_IND, 0 ) <> 1                         
                ) A
            WHERE A.DRANK > 1 )



--------------------------------------------------------------------------
-- DISQUALIFY DUPLICATE RULES … 
--
--          IE    ECLIPSE CONTRACT QUALFIES FOR BOTH BILL TO ANS SHIP TO
--      SHOULD NEVER HAPPEN… SO LEAVE OUT FOR NOW.
--
--    NEVER SAY NEVER…. EVEN THOUGH VERY RARE.. OCCURRED AT NCE
--     ADDED 5/2/2012
--
--------------------------------------------------------------------------



            UPDATE #TS_QUALIFIED_RULES 
            SET DISQUALIFIED_IND = 1
               ,DISQUALIFIED_COMMENT = 'QUALIFIED FILTERS WHERE BOTH SHIPTO AND BILLTO'            
        WHERE 1 = 1 
        AND   TS_QUALIFIED_RULES_ID  IN  (
                  SELECT A.TS_QUALIFIED_RULES_ID
                  FROM (
                SELECT
                    TSQR.TS_QUALIFIED_RULES_ID,
                        (     DENSE_RANK() OVER ( PARTITION BY TSQR.TS_RULE_EVENTS_FK, TSQR.RULES_FK
                                                            ORDER BY    TSQR.CUST_FILTER_HIERARCHY ASC, TSQR.CUST_RESULT_TYPE_CR_FK DESC
                                                                        ,TSQR.CUST_EFFECTIVE_DATE DESC,  TSQR.TS_QUALIFIED_RULES_ID DESC ) 
                     ) AS DRANK
                              FROM #TS_QUALIFIED_RULES TSQR WITH (NOLOCK) 
                              WHERE 1 = 1
                              AND   TSQR.RULE_TYPE_CR_FK IN ( 312, 313 )                              
                              AND   ISNULL ( DISQUALIFIED_IND, 0 ) <> 1                         
                ) A
            WHERE A.DRANK > 1 )




-------------------------------------------------------
-- DISQUALIFY PROMOTIONAL RULES IF EXCLUDE PROMOS = 1
-------------------------------------------------------

      UPDATE #TS_QUALIFIED_RULES
      SET DISQUALIFIED_IND = 1
         ,DISQUALIFIED_COMMENT = 'DISQUALIFY PROMOTIONAL RULES IF EXCLUDE PROMOS = 1' 
      WHERE 1 = 1
      AND  RULE_TYPE_CR_FK IN ( 312, 313 )      
      AND  PROMOTIONAL_IND = 1 
      AND  ISNULL ( DISQUALIFIED_IND, 0 ) <> 1                          
	  AND @Exclude_Promos = 1




/**********************************************************************************/
--   Insert into sychronizer.EVENT_CALC_RULES
--          Insert Current Rules… ONLY if not already there ??
--          Later look into DRANK ?? Or something to remove duplicates
--      Also need to include a SQL for Future / Whatif Rules
/**********************************************************************************/


            INSERT INTO [synchronizer].[EVENT_CALC_RULES]
            (   EVENT_FK                 
                  ,RULES_FK      
                  ,RESULT_DATA_NAME_FK                
                  ,RESULT_TYPE_CR_FK            
                  ,RESULT_EFFECTIVE_DATE  
                  ,RULES_ACTION_OPERATION1_FK
                  ,RULES_ACTION_OPERATION2_FK
                  ,RULES_ACTION_OPERATION3_FK 
                  ,OPERATION1
                  ,OPERATION2
                  ,OPERATION3
                ,BASIS_CALC_NAME
                ,BASIS1_NAME
                ,BASIS2_NAME
                ,BASIS3_NAME
                  ,BASIS_SOURCE1
                  ,BASIS_SOURCE2
                  ,BASIS_SOURCE3  
                  ,NUMBER1
                  ,NUMBER2
                  ,NUMBER3
                  ,RULES_ACTION_FK 
            ---- FILTERS
                ,PROD_FILTER
                ,CUST_FILTER
                ,SUPL_FILTER
                ,ORG_FILTER             
                ,RULES_CONTRACT_CUSTOMER_FK     
                  ,PROD_FILTER_FK      
                  ,PROD_RESULT_TYPE_CR_FK      
                  ,PROD_EFFECTIVE_DATE      
                  ,PROD_EVENT_FK  
                  ,PROD_ORG_ENTITY_STRUCTURE_FK
                  ,PROD_CORP_IND
                  ,PROD_ORG_IND           
                  ,PROD_FILTER_HIERARCHY
                  ,PROD_FILTER_ORG_PRECEDENCE
                  ,ORG_FILTER_FK      
                  ,ORG_RESULT_TYPE_CR_FK        
                  ,ORG_EFFECTIVE_DATE      
                  ,ORG_EVENT_FK           
                  ,CUST_FILTER_FK      
                  ,CUST_RESULT_TYPE_CR_FK      
                  ,CUST_EFFECTIVE_DATE      
                  ,CUST_EVENT_FK   
                  ,SUPL_FILTER_FK      
                  ,SUPL_RESULT_TYPE_CR_FK      
                  ,SUPL_EFFECTIVE_DATE      
                  ,SUPL_EVENT_FK  
                  ,RELATED_RULES_FK               
            ---- PRECEDENCES
                ,CONTRACT_PRECEDENCE
                  ,RULE_PRECEDENCE
                  ,SCHED_PRECEDENCE                   
                  ,STRUC_PRECEDENCE                 
            ---- FROM RULE TABLES
                ,HOST_RULE_XREF
                ,RULES_RESULT_TYPE_CR_FK
                ,RULES_EFFECTIVE_DATE           
                  ,RESULT_MTX_ACTION_CR_FK      
                  ,STRUC_MTX_ACTION_CR_FK     
                  ,SCHED_MTX_ACTION_CR_FK       
                  ,RULES_SCHEDULE_FK
                  ,RULES_STRUCTURE_FK
                  ,DISQUALIFIED_IND
                  ,DISQUALIFIED_COMMENT
                  )                
            SELECT   --- KATHI REMOVED DISTINCT  FOR PERFORMANCE
                  TSQR.TRIGGER_EVENT_FK 
                  ,TSQR.RULES_FK   
                  ,TSQR.RESULT_DATA_NAME_FK                    
                  ,TSQR.RESULT_TYPE_CR_FK      
                  ,TSQR.RESULT_EFFECTIVE_DATE  
                  ,RA.RULES_ACTION_OPERATION1_FK
                  ,RA.RULES_ACTION_OPERATION2_FK
                  ,RA.RULES_ACTION_OPERATION3_FK
            ,(SELECT CR.CODE FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
              INNER JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
                ON ( CR.CODE_REF_ID = RAOP.OPERATOR_CR_FK )
              WHERE RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION1_FK ) AS OPERATION1
            ,(SELECT CR.CODE FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
              INNER JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
                ON ( CR.CODE_REF_ID = RAOP.OPERATOR_CR_FK )
              WHERE RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION2_FK ) AS OPERATION2
            ,(SELECT CR.CODE FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
              INNER JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
                ON ( CR.CODE_REF_ID = RAOP.OPERATOR_CR_FK )
              WHERE RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION3_FK ) AS OPERATION3
            ,(SELECT DN.LABEL FROM epacube.DATA_NAME DN 
              WHERE DN.DATA_NAME_ID = RA.BASIS_CALC_DN_FK ) AS BASIS_CALC_NAME
            ,(SELECT DN.LABEL FROM epacube.DATA_NAME DN 
              WHERE DN.DATA_NAME_ID = RA.BASIS1_DN_FK ) AS BASIS1_NAME
            ,(SELECT DN.LABEL FROM epacube.DATA_NAME DN 
              WHERE DN.DATA_NAME_ID = RA.BASIS2_DN_FK ) AS BASIS2_NAME
            ,(SELECT DN.LABEL FROM epacube.DATA_NAME DN 
              WHERE DN.DATA_NAME_ID = RA.BASIS3_DN_FK ) AS BASIS3_NAME
            ,(SELECT CR.CODE FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
                    INNER JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
                        ON ( CR.CODE_REF_ID = RAOP.BASIS_SOURCE_CR_FK )
                    WHERE RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION1_FK ) AS BASIS_SOURCE1
            ,(SELECT CR.CODE FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
                    INNER JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
                        ON ( CR.CODE_REF_ID = RAOP.BASIS_SOURCE_CR_FK )
                    WHERE RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION2_FK ) AS BASIS_SOURCE2
            ,(SELECT CR.CODE FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
                    INNER JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
                        ON ( CR.CODE_REF_ID = RAOP.BASIS_SOURCE_CR_FK )
                    WHERE RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION3_FK ) AS BASIS_SOURCE3
            ,RA.BASIS1_NUMBER
                  ,RA.BASIS2_NUMBER
                  ,RA.BASIS3_NUMBER
                  ,RA.RULES_ACTION_ID AS RULES_ACTION_FK    
            ----- FILTERS     
                ,( SELECT RF.NAME FROM synchronizer.RULES_FILTER RF
                   WHERE RF.RULES_FILTER_ID = TSQR.PROD_FILTER_FK   ) AS PROD_FILTER
                ,( SELECT RF.NAME FROM synchronizer.RULES_FILTER RF
                   WHERE RF.RULES_FILTER_ID = TSQR.CUST_FILTER_FK   ) AS CUST_FILTER
                ,( SELECT RF.NAME FROM synchronizer.RULES_FILTER RF
                   WHERE RF.RULES_FILTER_ID = TSQR.SUPL_FILTER_FK   ) AS SUPL_FILTER
                ,( SELECT RF.NAME FROM synchronizer.RULES_FILTER RF 
                   WHERE RF.RULES_FILTER_ID = TSQR.ORG_FILTER_FK   ) AS ORG_FILTER  
                ,TSQR.RULES_CONTRACT_CUSTOMER_FK                                                 
                  ,TSQR.PROD_FILTER_FK      
                  ,TSQR.PROD_RESULT_TYPE_CR_FK      
                  ,TSQR.PROD_EFFECTIVE_DATE      
                  ,TSQR.PROD_EVENT_FK  
                  ,TSQR.PROD_ORG_ENTITY_STRUCTURE_FK
                  ,TSQR.PROD_CORP_IND
                  ,TSQR.PROD_ORG_IND
                  ,TSQR.PROD_FILTER_HIERARCHY
                  ,TSQR.PROD_FILTER_ORG_PRECEDENCE
                  ,TSQR.ORG_FILTER_FK      
                  ,TSQR.ORG_RESULT_TYPE_CR_FK         
                  ,TSQR.ORG_EFFECTIVE_DATE      
                  ,TSQR.ORG_EVENT_FK            
                  ,TSQR.CUST_FILTER_FK      
                  ,TSQR.CUST_RESULT_TYPE_CR_FK        
                  ,TSQR.CUST_EFFECTIVE_DATE      
                  ,TSQR.CUST_EVENT_FK  
                  ,TSQR.SUPL_FILTER_FK      
                  ,TSQR.SUPL_RESULT_TYPE_CR_FK      
                  ,TSQR.SUPL_EFFECTIVE_DATE      
                  ,TSQR.SUPL_EVENT_FK 
                  ,TSQR.RELATED_RULES_FK  
            ----- PRECEDENCES
                ,TSQR.CONTRACT_PRECEDENCE
                  ,TSQR.RULE_PRECEDENCE
                  ,TSQR.SCHED_PRECEDENCE     
                  ,TSQR.STRUC_PRECEDENCE     
            -----  FROM RULES TABLES    
                ,TSQR.HOST_RULE_XREF
                ,TSQR.RULES_RESULT_TYPE_CR_FK
                ,TSQR.RULES_EFFECTIVE_DATE            
                  ,TSQR.RESULT_MATRIX_ACTION_CR_FK 
                  ,TSQR.STRUC_MATRIX_ACTION_CR_FK 
                  ,TSQR.SCHED_MATRIX_ACTION_CR_FK 
                  ,TSQR.RULES_SCHEDULE_FK
                  ,TSQR.RULES_STRUCTURE_FK
                  ,TSQR.DISQUALIFIED_IND
                  ,TSQR.DISQUALIFIED_COMMENT                
          -------------------------
            FROM  #TS_QUALIFIED_RULES TSQR
            INNER JOIN synchronizer.RULES_ACTION RA  WITH (NOLOCK)
            ON ( RA.RULES_FK = TSQR.RULES_FK )
  -----  do not include disqualified because constraint error…        
WHERE ISNULL ( DISQUALIFIED_IND, 0 ) = 0   --- leave for now    KS FOR V516 GOING TO GO AHEAD AND WRITE THE DISQUALIFIED FOR CERTIFICATION MAY ADD BACK WHERE CLAUSE IN V517    

            
--GHS MAY 2, 2013 - CAPTURE DISQUALIFIED RULES

            INSERT INTO [synchronizer].[EVENT_CALC_RULES_DISQUALIFIED]
            (   EVENT_FK                 
                  ,RULES_FK      
                  ,RESULT_DATA_NAME_FK                
                  ,RESULT_TYPE_CR_FK            
                  ,RESULT_EFFECTIVE_DATE  
                  ,RULES_ACTION_OPERATION1_FK
                  ,RULES_ACTION_OPERATION2_FK
                  ,RULES_ACTION_OPERATION3_FK 
                  ,OPERATION1
                  ,OPERATION2
                  ,OPERATION3
                ,BASIS_CALC_NAME
                ,BASIS1_NAME
                ,BASIS2_NAME
                ,BASIS3_NAME
                  ,BASIS_SOURCE1
                  ,BASIS_SOURCE2
                  ,BASIS_SOURCE3  
                  ,NUMBER1
                  ,NUMBER2
                  ,NUMBER3
                  ,RULES_ACTION_FK 
            ---- FILTERS
                ,PROD_FILTER
                ,CUST_FILTER
                ,SUPL_FILTER
                ,ORG_FILTER             
                ,RULES_CONTRACT_CUSTOMER_FK     
                  ,PROD_FILTER_FK      
                  ,PROD_RESULT_TYPE_CR_FK      
                  ,PROD_EFFECTIVE_DATE      
                  ,PROD_EVENT_FK  
                  ,PROD_ORG_ENTITY_STRUCTURE_FK
                  ,PROD_CORP_IND
                  ,PROD_ORG_IND           
                  ,PROD_FILTER_HIERARCHY
                  ,PROD_FILTER_ORG_PRECEDENCE
                  ,ORG_FILTER_FK      
                  ,ORG_RESULT_TYPE_CR_FK        
                  ,ORG_EFFECTIVE_DATE      
                  ,ORG_EVENT_FK           
                  ,CUST_FILTER_FK      
                  ,CUST_RESULT_TYPE_CR_FK      
                  ,CUST_EFFECTIVE_DATE      
                  ,CUST_EVENT_FK   
                  ,SUPL_FILTER_FK      
                  ,SUPL_RESULT_TYPE_CR_FK      
                  ,SUPL_EFFECTIVE_DATE      
                  ,SUPL_EVENT_FK  
                  ,RELATED_RULES_FK               
            ---- PRECEDENCES
                ,CONTRACT_PRECEDENCE
                  ,RULE_PRECEDENCE
                  ,SCHED_PRECEDENCE                   
                  ,STRUC_PRECEDENCE                 
            ---- FROM RULE TABLES
                ,HOST_RULE_XREF
                ,RULES_RESULT_TYPE_CR_FK
                ,RULES_EFFECTIVE_DATE           
                  ,RESULT_MTX_ACTION_CR_FK      
                  ,STRUC_MTX_ACTION_CR_FK     
                  ,SCHED_MTX_ACTION_CR_FK       
                  ,RULES_SCHEDULE_FK
                  ,RULES_STRUCTURE_FK
                  ,DISQUALIFIED_IND
                  ,DISQUALIFIED_COMMENT
                  )                
            SELECT   --- KATHI REMOVED DISTINCT  FOR PERFORMANCE
                  TSQR.TRIGGER_EVENT_FK 
                  ,TSQR.RULES_FK   
                  ,TSQR.RESULT_DATA_NAME_FK                    
                  ,TSQR.RESULT_TYPE_CR_FK      
                  ,TSQR.RESULT_EFFECTIVE_DATE  
                  ,RA.RULES_ACTION_OPERATION1_FK
                  ,RA.RULES_ACTION_OPERATION2_FK
                  ,RA.RULES_ACTION_OPERATION3_FK
            ,(SELECT CR.CODE FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
              INNER JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
                ON ( CR.CODE_REF_ID = RAOP.OPERATOR_CR_FK )
              WHERE RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION1_FK ) AS OPERATION1
            ,(SELECT CR.CODE FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
              INNER JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
                ON ( CR.CODE_REF_ID = RAOP.OPERATOR_CR_FK )
              WHERE RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION2_FK ) AS OPERATION2
            ,(SELECT CR.CODE FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
              INNER JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
                ON ( CR.CODE_REF_ID = RAOP.OPERATOR_CR_FK )
              WHERE RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION3_FK ) AS OPERATION3
            ,(SELECT DN.LABEL FROM epacube.DATA_NAME DN 
              WHERE DN.DATA_NAME_ID = RA.BASIS_CALC_DN_FK ) AS BASIS_CALC_NAME
            ,(SELECT DN.LABEL FROM epacube.DATA_NAME DN 
              WHERE DN.DATA_NAME_ID = RA.BASIS1_DN_FK ) AS BASIS1_NAME
            ,(SELECT DN.LABEL FROM epacube.DATA_NAME DN 
              WHERE DN.DATA_NAME_ID = RA.BASIS2_DN_FK ) AS BASIS2_NAME
            ,(SELECT DN.LABEL FROM epacube.DATA_NAME DN 
              WHERE DN.DATA_NAME_ID = RA.BASIS3_DN_FK ) AS BASIS3_NAME
            ,(SELECT CR.CODE FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
                    INNER JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
                        ON ( CR.CODE_REF_ID = RAOP.BASIS_SOURCE_CR_FK )
                    WHERE RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION1_FK ) AS BASIS_SOURCE1
            ,(SELECT CR.CODE FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
                    INNER JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
                        ON ( CR.CODE_REF_ID = RAOP.BASIS_SOURCE_CR_FK )
                    WHERE RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION2_FK ) AS BASIS_SOURCE2
            ,(SELECT CR.CODE FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
                    INNER JOIN EPACUBE.CODE_REF CR WITH (NOLOCK)
                        ON ( CR.CODE_REF_ID = RAOP.BASIS_SOURCE_CR_FK )
                    WHERE RAOP.RULES_ACTION_OPERATION_ID = RA.RULES_ACTION_OPERATION3_FK ) AS BASIS_SOURCE3
            ,RA.BASIS1_NUMBER
                  ,RA.BASIS2_NUMBER
                  ,RA.BASIS3_NUMBER
                  ,RA.RULES_ACTION_ID AS RULES_ACTION_FK    
            ----- FILTERS     
                ,( SELECT RF.NAME FROM synchronizer.RULES_FILTER RF
                   WHERE RF.RULES_FILTER_ID = TSQR.PROD_FILTER_FK   ) AS PROD_FILTER
                ,( SELECT RF.NAME FROM synchronizer.RULES_FILTER RF
                   WHERE RF.RULES_FILTER_ID = TSQR.CUST_FILTER_FK   ) AS CUST_FILTER
                ,( SELECT RF.NAME FROM synchronizer.RULES_FILTER RF
                   WHERE RF.RULES_FILTER_ID = TSQR.SUPL_FILTER_FK   ) AS SUPL_FILTER
                ,( SELECT RF.NAME FROM synchronizer.RULES_FILTER RF 
                   WHERE RF.RULES_FILTER_ID = TSQR.ORG_FILTER_FK   ) AS ORG_FILTER  
                ,TSQR.RULES_CONTRACT_CUSTOMER_FK                                                 
                  ,TSQR.PROD_FILTER_FK      
                  ,TSQR.PROD_RESULT_TYPE_CR_FK      
                  ,TSQR.PROD_EFFECTIVE_DATE      
                  ,TSQR.PROD_EVENT_FK  
                  ,TSQR.PROD_ORG_ENTITY_STRUCTURE_FK
                  ,TSQR.PROD_CORP_IND
                  ,TSQR.PROD_ORG_IND
                  ,TSQR.PROD_FILTER_HIERARCHY
                  ,TSQR.PROD_FILTER_ORG_PRECEDENCE
                  ,TSQR.ORG_FILTER_FK      
                  ,TSQR.ORG_RESULT_TYPE_CR_FK         
                  ,TSQR.ORG_EFFECTIVE_DATE      
                  ,TSQR.ORG_EVENT_FK            
                  ,TSQR.CUST_FILTER_FK      
                  ,TSQR.CUST_RESULT_TYPE_CR_FK        
                  ,TSQR.CUST_EFFECTIVE_DATE      
                  ,TSQR.CUST_EVENT_FK  
                  ,TSQR.SUPL_FILTER_FK      
                  ,TSQR.SUPL_RESULT_TYPE_CR_FK      
                  ,TSQR.SUPL_EFFECTIVE_DATE      
                  ,TSQR.SUPL_EVENT_FK 
                  ,TSQR.RELATED_RULES_FK  
            ----- PRECEDENCES
                ,TSQR.CONTRACT_PRECEDENCE
                  ,TSQR.RULE_PRECEDENCE
                  ,TSQR.SCHED_PRECEDENCE     
                  ,TSQR.STRUC_PRECEDENCE     
            -----  FROM RULES TABLES    
                ,TSQR.HOST_RULE_XREF
                ,TSQR.RULES_RESULT_TYPE_CR_FK
                ,TSQR.RULES_EFFECTIVE_DATE            
                  ,TSQR.RESULT_MATRIX_ACTION_CR_FK 
                  ,TSQR.STRUC_MATRIX_ACTION_CR_FK 
                  ,TSQR.SCHED_MATRIX_ACTION_CR_FK 
                  ,TSQR.RULES_SCHEDULE_FK
                  ,TSQR.RULES_STRUCTURE_FK
                  ,TSQR.DISQUALIFIED_IND
                  ,TSQR.DISQUALIFIED_COMMENT                
          -------------------------
            FROM  #TS_QUALIFIED_RULES TSQR
            INNER JOIN synchronizer.RULES_ACTION RA  WITH (NOLOCK) ON ( RA.RULES_FK = TSQR.RULES_FK )
			Left Join Synchronizer.event_calc_rules ecr on TSQR.TRIGGER_EVENT_FK = ecr.EVENT_FK and TSQR.Rules_FK = ecr.RULES_FK
			WHERE ISNULL ( TSQR.DISQUALIFIED_IND, 0 ) <> 0
				and ecr.RESULT_DATA_NAME_FK is null

END 

------------------------------------------------------------------------------------------
---  GET NEXT Warehouse and Result Grouping 
------------------------------------------------------------------------------------------

    FETCH NEXT FROM cur_v_rules_group INTO   @vm$org_entity_structure_fk 


     END --cur_v_rules_group  LOOP
     CLOSE cur_v_rules_group
      DEALLOCATE cur_v_rules_group; 



------------------------------------------------------------------------------------------
--  Drop Temp Tables
------------------------------------------------------------------------------------------

------drop temp tables
IF object_id('tempdb..#TS_QUALIFIED_RULES') is not null
   drop table #TS_QUALIFIED_RULES 


--drop temp table if it exists
      IF object_id('tempdb..#TS_RULE_EVENTS') is not null
         drop table #TS_RULE_EVENTS;


--drop temp tables
IF object_id('tempdb..#TS_WHSE_SPECIFIC_RULES') is not null
   drop table #TS_WHSE_SPECIFIC_RULES


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of synchronizer.EVENT_POST_QUALIFY_RULES_CALC'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

            SELECT 
                  @ErrorMessage = 'Execution of synchronizer.EVENT_POST_QUALIFY_RULES_CALC has failed ' + ERROR_MESSAGE(),
                  @ErrorSeverity = ERROR_SEVERITY(),
                  @ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
                            @ErrorSeverity, -- Severity.
                            @ErrorState -- State.
                            );
END CATCH

END
