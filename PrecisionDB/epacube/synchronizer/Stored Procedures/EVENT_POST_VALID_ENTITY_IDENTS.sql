﻿








-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform ident validations
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        10/15/2010   Initial SQL Version
--

CREATE PROCEDURE [synchronizer].[EVENT_POST_VALID_ENTITY_IDENTS] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_ENTITY_IDENTS.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;






------------------------------------------------------------------------------------------
--   CHECK FOR DUPLICATE UNIQUE IDENTIFIERS
------------------------------------------------------------------------------------------

			INSERT INTO #ts_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		(	SELECT 
			 TSED.EVENT_FK
			,NULL
			,99
			,'THIS UNIQUE IDENTIFIER ALREADY EXISTS.'  --ERROR NAME        
			,'FOR ' + DNX.LABEL
            ,TSED.NEW_DATA 
            ,'ATEMPTING TO UPDATE: '
			,NULL
			,NULL
			,NULL
			,@l_sysdate
			FROM #TS_EVENT_DATA TSED	
			INNER JOIN epacube.ENTITY_IDENTIFICATION VCPD WITH (NOLOCK) 
			  ON ( VCPD.DATA_NAME_FK = TSED.DATA_NAME_FK
			  AND  VCPD.ENTITY_DATA_NAME_FK = TSED.ENTITY_DATA_NAME_FK  
			  AND  VCPD.VALUE = TSED.NEW_DATA )
			INNER JOIN epacube.DATA_NAME DNX ON DNX.DATA_NAME_ID = TSED.ENTITY_DATA_NAME_FK 
			WHERE TSED.TABLE_NAME IN ( 'ENTITY_IDENTIFICATION' ) )


			INSERT INTO #ts_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		(	SELECT 
			 TSED.EVENT_FK
			,NULL
			,99
			,'THIS UNIQUE IDENTIFIER ALREADY EXISTS.'  --ERROR NAME        
			,'FOR ' + DNX.LABEL
            ,TSED.NEW_DATA 
            ,'ATEMPTING TO UPDATE: '
			,NULL
			,NULL
			,NULL
			,@l_sysdate
			FROM #TS_EVENT_DATA TSED	
			INNER JOIN epacube.ENTITY_IDENT_MULT VCPD WITH (NOLOCK) 
			  ON ( VCPD.DATA_NAME_FK = TSED.DATA_NAME_FK
			  AND  VCPD.ENTITY_DATA_NAME_FK = TSED.ENTITY_DATA_NAME_FK  
			  AND  VCPD.VALUE = TSED.NEW_DATA )
			INNER JOIN epacube.DATA_NAME DNX ON DNX.DATA_NAME_ID = TSED.ENTITY_DATA_NAME_FK 
			WHERE TSED.TABLE_NAME IN ('ENTITY_IDENT_MULT' ) )



/*    BELIEVE THIS LOGIC IS COVERED IN CLEANSER......

------------------------------------------------------------------------------------------
--  Create Temp Table for TS BATCH DUPS  Include Batch and other Events 
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_BATCH_DUPS') is not null
	   drop table #TS_BATCH_DUPS
	   
	   
	-- create temp table
	CREATE TABLE #TS_BATCH_DUPS(
	    DATA_NAME_FK             INT NULL,
	    ORG_ENTITY_STRUCTURE_FK  BIGINT NULL,
	    ENTITY_STRUCTURE_FK      BIGINT NULL,	    
		NEW_DATA		         varchar(128)  NULL
		 )


    INSERT INTO #TS_BATCH_DUPS
     (  DATA_NAME_FK
       ,ORG_ENTITY_STRUCTURE_FK
       ,ENTITY_STRUCTURE_FK
       ,NEW_DATA )
    SELECT DISTINCT 
               TSED.DATA_NAME_FK 
              ,ISNULL ( TSED.ORG_ENTITY_STRUCTURE_FK, 1 )
              ,ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 )
              ,TSED.NEW_DATA
    FROM #TS_EVENT_DATA TSED
    WHERE TSED.TABLE_NAME IN ( 'ENTITY_IDENTIFICATION', 'ENTITY_IDENT_MULT' )
    GROUP BY TSED.DATA_NAME_FK, TSED.ORG_ENTITY_STRUCTURE_FK, TSED.ENTITY_STRUCTURE_FK, TSED.NEW_DATA
    HAVING COUNT(*) > 1


--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------


	CREATE NONCLUSTERED INDEX IDX_TSBD_DNF_OESF_ESF_IND ON #TS_BATCH_DUPS
	(   DATA_NAME_FK	ASC
       ,ORG_ENTITY_STRUCTURE_FK ASC
       ,ENTITY_STRUCTURE_FK     ASC )
	INCLUDE ( NEW_DATA )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


			INSERT INTO #TS_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				DATA_NAME_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		(	SELECT 
			 TSED.EVENT_FK
			,NULL
			,99
			,TSED.DATA_NAME_FK
			,'UNIQUE IDENTIFIER EXISTS FOR TWO DIFFERENT ENTITYS'  --ERROR NAME         
			,'SEE ENTITY_ID '--ERROR1
			,( SELECT VALUE FROM EPACUBE.ENTITY_IDENTIFICATION  WITH (NOLOCK)
			   WHERE ENTITY_STRUCTURE_FK = TSED.ENTITY_STRUCTURE_FK
			   AND   DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
			                          FROM EPACUBE.EPACUBE_PARAMS EEP  WITH (NOLOCK)
			                          WHERE EEP.APPLICATION_SCOPE_FK = 100
			                          AND   EEP.NAME = 'ENTITY' )  ) -- PRIMARY IDENTIFIER
			,'DUPLICATED ENTITY_ID'
			,( SELECT TOP 1 EI.VALUE
			   FROM SYNCHRONIZER.EVENT_DATA TSEDX
			   INNER JOIN  EPACUBE.ENTITY_IDENTIFICATION PI  WITH (NOLOCK) ON
			      (   EI.ENTITY_STRUCTURE_FK = TSEDX.ENTITY_STRUCTURE_FK
			      AND EI. DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
			                          FROM EPACUBE.EPACUBE_PARAMS EEP  WITH (NOLOCK)
			                          WHERE EEP.APPLICATION_SCOPE_FK = 100
			                          AND   EEP.NAME = 'ENTITY' )  ) -- PRIMARY IDENTIFIER
			   WHERE TSEDX.DATA_NAME_FK = TSED.DATA_NAME_FK
			   AND   ISNULL ( TSEDX.ORG_ENTITY_STRUCTURE_FK, 1) = ISNULL ( TSED.ORG_ENTITY_STRUCTURE_FK, 1 )
			   AND   ISNULL ( TSEDX.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) 			   
			   AND   TSEDX.NEW_DATA = TSED.NEW_DATA 
			   AND   TSEDX.ENTITY_STRUCTURE_FK <> TSED.ENTITY_STRUCTURE_FK )
			,NULL
			,NULL
			,@l_sysdate
			FROM #TS_EVENT_DATA TSED	
			INNER JOIN #TS_BATCH_DUPS TSBD
			  ON ( TSBD.DATA_NAME_FK = TSED.DATA_NAME_FK 
			  AND  TSBD.ORG_ENTITY_STRUCTURE_FK = ISNULL ( TSED.ORG_ENTITY_STRUCTURE_FK, 1 )
			  AND  ISNULL ( TSBD.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.ENTITY_STRUCTURE_FK, 0 ) 
			  AND  TSBD.NEW_DATA = TSED.NEW_DATA )
			WHERE TSED.TABLE_NAME IN ( 'ENTITY_IDENTIFICATION', 'ENTITY_IDENT_MULT' ) )


--drop temp table if it exists
	IF object_id('tempdb..#TS_BATCH_DUPS') is not null
	   drop table #TS_BATCH_DUPS


*/



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_ENTITY_IDENTS'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_ENTITY_IDENTS has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END




































