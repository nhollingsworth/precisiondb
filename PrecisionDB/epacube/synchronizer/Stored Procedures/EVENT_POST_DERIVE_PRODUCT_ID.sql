﻿









-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To create events for sequence product ids
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/11/2009   Initial version
-- KS		 02/15/2010   In reorganization of code.. Events created during Qualify
-- MN		 10/30/2013		Fixed auto-increment product_id, auto search for available product_id and assignment	
-- CV        10/15/2015   Added to check the event data for product id existing						  
-- CV        01/17/2017   Using Control Table instead of Hiearchy

CREATE PROCEDURE [synchronizer].[EVENT_POST_DERIVE_PRODUCT_ID] (
      @in_batch_no  bigint, @in_calc_group_seq INT, @in_result_data_name_fk INT  )

AS
BEGIN


     DECLARE @ls_exec            nvarchar (max)
     DECLARE @l_exec_no          bigint
     DECLARE @l_rows_processed   bigint
     DECLARE @l_sysdate          datetime

	DECLARE @l_stmt_no           bigint
    DECLARE @ls_stmt             varchar(1000)
	DECLARE @status_desc		 varchar (max)
	DECLARE @ErrorMessage		 nvarchar(4000)
	DECLARE @ErrorSeverity		 int
	DECLARE @ErrorState			 int

    DECLARE @v_product_id_seq    bigint
    
   SET NOCOUNT ON;

   BEGIN TRY
      SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_DERIVE_PRODUCT_ID '
				 + ', Batch NO: ' + cast(isnull(@IN_BATCH_NO,0) as varchar(16))

      EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;

      SET @l_sysdate   = getdate()



------------------------------------------------------------------------------------------
---  LOOP THROUGH DERIVATION EVENTS
------------------------------------------------------------------------------------------


      DECLARE 
              @vm$event_fk               BIGINT,
              @vm$result_data_name_fk    INT


      DECLARE  cur_v_event cursor local for
				SELECT ed.event_id, r.result_data_name_fk
				FROM synchronizer.EVENT_DATA ED WITH (NOLOCK)
				INNER JOIN synchronizer.EVENT_DATA_RULES EDR  WITH (NOLOCK)
					ON ( EDR.EVENT_FK = ED.EVENT_ID )
			    INNER JOIN synchronizer.RULES R  WITH (NOLOCK)
			        ON ( R.RULES_ID = EDR.RULES_FK )
				INNER JOIN synchronizer.RULES_ACTION RA	 WITH (NOLOCK)	
					ON ( RA.RULES_FK = R.RULES_ID  )
				INNER JOIN 	epacube.DATA_NAME  DN1 WITH (NOLOCK)
				  ON ( DN1.DATA_NAME_ID = R.RESULT_DATA_NAME_FK  )
				  INNER JOIN Epacube.controls C On C.controls_id = R.controls_fk	
				--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
				--	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID  
				--	AND  RS.RULE_TYPE_CR_FK IN ( 301, 302 )  )  -- DEFAULT OR DERIVATION 
				--INNER JOIN Synchronizer.rules_hierarchy RH WITH (NOLOCK) 
				--	ON RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
				--	AND RH.RECORD_STATUS_CR_FK = 1 
				WHERE 1 = 1
				AND   ED.BATCH_NO = @IN_BATCH_NO
				AND   ED.DATA_NAME_FK = 110100  -- PRODUCT_ID
				AND   ED.DATA_NAME_FK = R.RESULT_DATA_NAME_FK 
				AND   RA.RULES_ACTION_OPERATION1_FK = 350  -- DERIVE PRODUCT ID SEQ
				AND  R.RULE_TYPE_CR_FK IN ( 301, 302 ) 
				--AND   ISNULL ( RH.CALC_GROUP_SEQ, 999999999 ) = ISNULL(@in_calc_group_seq, 999999999)	
				AND   ISNULL ( C.CONTROL_PRECEDENCE, 999999999 ) = ISNULL(@in_calc_group_seq, 999999999)	
				AND   R.RESULT_DATA_NAME_FK = @in_result_data_name_fk  
                ORDER BY ED.PRODUCT_STRUCTURE_FK
      
        OPEN cur_v_event;
        FETCH NEXT FROM cur_v_event INTO  @vm$event_fk 
                                         ,@vm$result_data_name_fk


------------------------------------------------------------------------------------------
---  LOOP THROUGH PRODUCT_STRUCTURE_FK... GET SEQ_ID
------------------------------------------------------------------------------------------


         WHILE @@FETCH_STATUS = 0 
         BEGIN


/* ----------------------------------------------------------------------------------- */
--    Assign Next PRODUCT_ID_SEQ to all stg_product_structures within Import Job 
/* ----------------------------------------------------------------------------------- */

	--Get Current Value and see if it's available
	EXEC seq_manager.db_get_curr_sequence_value_impl 'common','product_id_seq', @v_product_id_seq OUTPUT

	declare @maxvalue numeric(38), @minvalue numeric(38), @MaxAttempt tinyint, @AttemptCount tinyint

	set @MaxAttempt = 3
	set @AttemptCount = 1

	--  get info
	select 
			@maxvalue = maxvalue
			,@minvalue = minvalue
		from epacube.common.db_sequence_list		
		where 
			schema_name = 'common' and sequence_name = 'product_id_seq'

	--Check for existed product_id 
	--WHILE (@AttemptCount < @MaxAttempt AND EXISTS(select '' from epacube.product_identification where data_name_fk = 110100 and value = CAST(@v_product_id_seq AS VARCHAR(50))))
	--added to check existing events

	WHILE (@AttemptCount < @MaxAttempt AND EXISTS(select '' from synchronizer.event_data where DATA_NAME_FK = 110100
			and new_data = CAST(@v_product_id_seq AS VARCHAR(50)))
			OR EXISTS(select '' from epacube.product_identification where data_name_fk = 110100 and value = CAST(@v_product_id_seq AS VARCHAR(50))))
 
	BEGIN
		--Product ID already exists --> generate new id
		--Check if current value is equal to max value, if so restart current value
		if (@v_product_id_seq >= @maxvalue) begin
			update common.db_sequence_list
			set current_value = NULL
			where sequence_name = 'product_id_seq'
			set @AttemptCount = @AttemptCount + 1
		end

		EXEC seq_manager.db_get_next_sequence_value_impl 'common','product_id_seq', @v_product_id_seq output

		if (@AttemptCount >= @MaxAttempt) BEGIN
			--throw 60000, 'Execution of SYNCHRONIZER.SYNCHRONIZER.EVENT_POST_DERIVE_PRODUCT_ID has failed: There is no available product_id to be assigned. ', 1; 
			SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.SYNCHRONIZER.EVENT_POST_DERIVE_PRODUCT_ID has failed: There is no available product_id to be assigned. ',
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

			 RAISERROR (@ErrorMessage, -- Message text.
						@ErrorSeverity, -- Severity.
						@ErrorState -- State.
						);
		END

	END

	

    UPDATE synchronizer.EVENT_DATA 
    SET NEW_DATA = @v_product_id_seq  -- AS NEW_DATA
    WHERE EVENT_ID = @vm$event_fk 


------------------------------------------------------------------------------------------
---  GET NEXT IMPORT_JOB_FK, PRODUCT_STRUCTURE_FK
------------------------------------------------------------------------------------------

        FETCH NEXT FROM cur_v_event INTO  @vm$event_fk 
                                         ,@vm$result_data_name_fk

     END --cur_v_event LOOP
     CLOSE cur_v_event;
	 DEALLOCATE cur_v_event; 



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_DERIVE_PRODUCT_ID '
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.SYNCHRONIZER.EVENT_POST_DERIVE_PRODUCT_ID has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );

					return;
END CATCH

END

