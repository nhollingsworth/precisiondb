﻿










-- Copyright 2007
--
-- Procedure created by Walt Tucholski
--
--
-- Purpose: Called from the UI to perform TAXONOMY_NODE
--			maintenance.

--VALIDATIONS
--1. Taxonomy Node Name may not be null (epacube.data_value.description)
--2. Parent Taxonomy Node may not be null
--3. Taxonomy Tree may not be null
--4. Taxonomy Node Code may not be null (epacube.data_value.value)
--5. Taxonomy Code must be unique to that Tree (one tax_node_dv_fk per tree)
--6. Do not allow deletion of node that has child nodes




-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- WT        06/25/2007  Initial SQL Version - EPA-879
-- RG		 01/09/2007  EPA-1453 Add support for multiple Taxonomy Trees
-- RG		 06/09/2008  EPA-1598 Global Taxonomy Changes
-- RG	     06/27/2008  Adding Code handling/validation
-- YN        10/10/2008  Added Image - optional column and parameter
-- YN        10/11/2008  Added Sequence - optional parameter
-- YN        10/12/2008  Added optional Parent_Taxonomy_node_code parameter
-- YN        10/12/2008  Added optional description parameter
-- RG		 10/14/2008  Updating validations/error handling
-- RG		 10/31/2008  Commented out image for time being; moved a deletion validation 
-- RG		 10/31/2008  Commented out expired PTA validation 
-- RG		 11/23/2008	 EPA-1968 Ability to Activate/Inactivate Nodes


CREATE PROCEDURE [synchronizer].[TAXONOMY_NODE_EDITOR_IMPORT]
    (
      @in_taxonomy_node_id INT,
      @in_taxonomy_node_name VARCHAR(128),
      @in_taxonomy_code VARCHAR(64),
      @in_parent_taxonomy_node_fk INT,
      @in_taxonomy_tree_fk INT,
      @in_update_user VARCHAR(64),
      @in_ui_action_cr_fk INT,
      @in_image VARCHAR(255) = NULL,
      @in_level_seq INT = NULL,
      @in_parent_taxonomy_node_code VARCHAR(64) = NULL,
      @in_description VARCHAR(128) = NULL		
      
    )
AS /***************  Parameter Defintions  **********************
CURRENTLY USED IN THE DATA MASTER EDITOR....

Simple for Now.... Only CREATE (8003), DELETE (8004)

***************************************************************/

    BEGIN

        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @DataValueID BIGINT
        DECLARE @level_seq INT
            


        SET NOCOUNT ON ;
        SET @ErrorMessage = ''
        BEGIN TRY

			--VALIDATIONS
--1. Taxonomy Node Name may not be null (epacube.data_value.description)
--2. Parent Taxonomy Node may not be null
--3. Taxonomy Tree may not be null
--4. Taxonomy Node Code may not be null (epacube.data_value.value)
--5. Taxonomy Code must be unique to that Tree (one tax_node_dv_fk per tree)
--6. Do not allow deletion of node that has child nodes


            IF @in_ui_action_cr_fk IN ( 8003 ) 
                BEGIN
                    IF @in_taxonomy_node_name IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'Taxonomy Node name cannot be null'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

                    IF @in_parent_taxonomy_node_fk IS NULL
                        AND @in_parent_taxonomy_node_code IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'Taxonomy Parent Node cannot be null'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

                    IF @in_taxonomy_tree_fk IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'Taxonomy Tree cannot be null'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

                    IF @in_taxonomy_code IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'Taxonomy Code cannot be null'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

	----
	--Check to make sure node code is a node on the tree
	----
                    SET @ErrorMessage = 'Taxonomy code does not exist in this tree: ' + @in_taxonomy_code
                    SELECT  @ErrorMessage = ' '
                    FROM    epacube.data_value
                    WHERE   value = @in_taxonomy_code
                            AND @in_level_seq IS NOT NULL
                            AND @in_level_seq <> 0
                            AND data_value_id IN (
                            SELECT  tax_node_dv_fk
                            FROM    epacube.taxonomy_node
                            WHERE   taxonomy_tree_fk = @in_taxonomy_tree_fk ) ;
                    IF @ErrorMessage <> '' 
                        BEGIN 		
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END


   ----
   -- Lets check / get parent_taxonomy_node_fk (YN 10/12/2008)
   ----                     
                    SET @ErrorMessage = ''
                    IF ( @in_parent_taxonomy_node_fk IS NULL ) 
                        SELECT TOP 1
                                @in_parent_taxonomy_node_fk = tn.TAXONOMY_NODE_ID
                        FROM    epacube.TAXONOMY_NODE tn
                                INNER JOIN epacube.data_value dv ON tn.TAX_NODE_DV_FK = dv.DATA_VALUE_ID
                        WHERE   dv.value = @in_parent_taxonomy_node_code
                                AND dv.data_name_fk = ( SELECT  tax_node_dn_fk
                                                        FROM    epacube.taxonomy_tree
                                                        WHERE   taxonomy_tree_id = @in_taxonomy_tree_fk
                                                      ) ;
                                               
                    IF ( @in_parent_taxonomy_node_fk IS NULL
                         AND @in_level_seq IS NOT NULL
                         AND @in_level_seq <> 0
                       ) 
                        BEGIN
                            SET @ErrorMessage = 'Taxonomy Parent Node cannot be null and Parent Taxonomy Node Code wasn''t found in the Data_Value table'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END
                   
---------------------
-- If the validations pass, then check the entered code. If the code is unique,
-- create a new data value. If the code already exists as a data name, simply assign 
-- that data value to the new node
---------------------



-------------------------------------------------------------------------------------
--   Edits passed Insert Row
-------------------------------------------------------------------------------------
--IF @in_taxonomy_code in (select value from epacube.data_value where value = @in_taxonomy_code)
--BEGIN
--
--END --end case where code exists on different tree

                    IF @in_taxonomy_code NOT IN (
                        SELECT  value
                        FROM    epacube.data_value
                        WHERE   value = @in_taxonomy_code
                                AND data_name_fk = ( SELECT tax_node_dn_fk
                                                     FROM   epacube.taxonomy_tree
                                                     WHERE  taxonomy_tree_id = @in_taxonomy_tree_fk
                                                   ) ) 
                        BEGIN
                        
                             
                                    
                            INSERT  INTO EPACUBE.DATA_VALUE
                                    (
                                      value,
                                      data_name_fk,
                                      description,
                                      record_status_cr_fk,
                                      create_timestamp,
                                      update_timestamp,
                                      update_user
                                            
                                    )
                                    SELECT  UPPER(@in_taxonomy_code),
                                            ( SELECT    tax_node_dn_fk
                                              FROM      epacube.taxonomy_tree
                                              WHERE     taxonomy_tree_id = @in_taxonomy_tree_fk
                                            ),
                                            CASE WHEN @in_Description IS NULL
                                                 THEN UPPER(@in_taxonomy_node_name)
                                                 ELSE @in_description
                                            END,
                                            1,
                                            GETDATE(),
                                            GETDATE(),
                                            @in_update_user
                        END -- end of possible insert into Data Values
                              
                    -- per Jan's recommendation updating the description if the Data Value already exists
                    IF EXISTS ( SELECT  1
                                FROM    epacube.data_value
                                WHERE   value = @in_taxonomy_code
                                        AND data_name_fk = ( SELECT tax_node_dn_fk
                                                             FROM   epacube.taxonomy_tree
                                                             WHERE  taxonomy_tree_id = @in_taxonomy_tree_fk
                                                           )
                                        AND ISNULL(description, '') <> ISNULL(@in_description, '')
                                        AND @in_description IS NOT  NULL ) 
                        BEGIN
                            UPDATE  epacube.DATA_VALUE
                            SET     description = @in_description
                            WHERE   value = @in_taxonomy_code
                                    AND data_name_fk = ( SELECT tax_node_dn_fk
                                                         FROM   epacube.taxonomy_tree
                                                         WHERE  taxonomy_tree_id = @in_taxonomy_tree_fk
                                                       )
							  	
                        END                                                 
                              
   ----
   -- Lets check / get level_seq (YN 10/12/2008)
   ---- 
                    IF @in_level_seq IS NOT NULL 
                        SET @level_seq = @in_level_seq
                    ELSE 
                        SELECT  @level_seq = ( level_seq + 1 )
                        FROM    epacube.taxonomy_node
                        WHERE   taxonomy_node_id = @in_parent_taxonomy_node_fk ;
                        
                        
 --  don't do the insert if the taxonomy code node already exists.
                        
                    IF ( @level_seq IS NOT NULL
                         AND @level_seq <> 0
                         AND @in_taxonomy_code NOT IN (
                        SELECT  value
                        FROM    epacube.data_value
                        WHERE   value = @in_taxonomy_code
                                AND data_name_fk = ( SELECT tax_node_dn_fk
                                                     FROM   epacube.taxonomy_tree
                                                     WHERE  taxonomy_tree_id = @in_taxonomy_tree_fk
                                                   ) ) 
                       ) 
                        INSERT  INTO EPACUBE.TAXONOMY_NODE
                                (
                                  taxonomy_tree_fk,
                                  level_seq,
                                  parent_taxonomy_node_fk,
                                  root_taxonomy_node_fk,
                                  tax_node_dv_fk
                          --  , [image]
                                  ,
                                  record_status_cr_fk,
                                  create_timestamp,
                                  update_timestamp
                                        
                                )
                                SELECT  @in_taxonomy_tree_fk,
                                        @level_seq,
                                        @in_parent_taxonomy_node_fk,
                                        ( SELECT    root_taxonomy_node_fk
                                          FROM      epacube.taxonomy_node
                                          WHERE     taxonomy_node_id = @in_parent_taxonomy_node_fk
                                        ),
                                        ( SELECT TOP 1
                                                    data_value_id
                                          FROM      epacube.data_value dv
                                          WHERE     dv.value = @in_taxonomy_code
                                                    AND dv.data_name_fk = ( SELECT  tax_node_dn_fk
                                                                            FROM    epacube.taxonomy_tree
                                                                            WHERE   taxonomy_tree_id = @in_taxonomy_tree_fk
                                                                          )
                                        )
                                --  , @in_image
                                        ,
                                        1,
                                        GETDATE(),
                                        GETDATE()			  
                END -- IF @in_ui_action_cr_fk = 8003



-------------------------------------------------------------------------------------
--   If the action is deletion;
--   Validations
--	1. A taxonomy_node_id must be supplied




-- 1. Products must have this node assignment removed. (delete from product_category)
-- 2. Relevant product attribute values must be deleted.
-- 3. Possible configured values for this node attribute must be deleted (tax_node_attr_dv_fk).
-- 4. Property assignments to this node must be deleted.
-- 5. The node can then be deleted.
-------------------------------------------------------------------------------------


-- #1 Validation

            IF @in_ui_action_cr_fk IN ( 8004 ) 
                BEGIN

                    SET @DataValueID = ( SELECT tax_node_dv_fk
                                         FROM   epacube.taxonomy_node
                                         WHERE  taxonomy_node_id = @in_taxonomy_node_id
                                       )


                    IF @in_taxonomy_node_id IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'Taxonomy Node ID cannot be null for delete action'                              
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

---
-- #6 Do not allow deletion of node with child nodes
---
                    SET @ErrorMessage = ''
                    SELECT  @ErrorMessage = 'Taxonomy Node has child nodes; please delete children first'
                    FROM    epacube.taxonomy_node
                    WHERE   parent_taxonomy_node_fk = @in_taxonomy_node_id ;
                     
                    IF @ErrorMessage <> '' 
                        BEGIN 		
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

-- #1 Removing Node Assignment
                    DELETE  FROM EPACUBE.PRODUCT_CATEGORY
                    WHERE   data_value_fk = @DataValueID
						--(select tax_node_dv_fk from epacube.taxonomy_node
						  -- where taxonomy_node_id = @in_taxonomy_node_id)

 --#2 Remove Product Attributes
--                     DELETE FROM EPACUBE.PRODUCT_TAX_ATTRIBUTE
--                     WHERE  taxonomy_node_attribute_fk IN ( SELECT  taxonomy_node_attribute_id
--                                                            FROM    epacube.taxonomy_node_attribute
--                                                            WHERE   taxonomy_node_fk = @in_taxonomy_node_id )

-- #3 Delete Configured Values
                    DELETE  FROM EPACUBE.TAXONOMY_NODE_ATTRIBUTE_DV
                    WHERE   taxonomy_node_attribute_fk IN (
                            SELECT  taxonomy_node_attribute_id
                            FROM    epacube.taxonomy_node_attribute
                            WHERE   taxonomy_node_fk = @in_taxonomy_node_id )	

-- #4 Delete Assigned Properties
                    DELETE  FROM EPACUBE.TAXONOMY_NODE_ATTRIBUTE
                    WHERE   taxonomy_node_fk = @in_taxonomy_node_id
	

-- #5 Delete Node itself			
                    DELETE  FROM epacube.TAXONOMY_NODE
                    WHERE   taxonomy_node_id = @in_taxonomy_node_id

--Commented out because one data value could be two codes
---- #6 Remove the Data Value entry
--	DELETE FROM EPACUBE.DATA_VALUE
--	where data_value_id = @DataValueID
--						--(select tax_node_dv_fk from epacube.taxonomy_node
--						--   where taxonomy_node_id = @in_taxonomy_node_id)

                END -- IF @in_ui_action_cr_fk = 8004


----------------------
-- Nov 23 RG Adding Activation/Inactivation
----------------------

		IF @in_ui_action_cr_fk IN ( 8005,8007 ) 
          BEGIN
                IF @in_taxonomy_node_id IS NULL 
                      BEGIN
                            SET @ErrorMessage = 'Tax Node ID cannot be null'                                    
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

			 IF @in_ui_action_cr_fk = 8005 --inactivate
                BEGIN
					UPDATE epacube.taxonomy_node
					set record_status_cr_fk = 2
					, update_timestamp = getdate()
					where taxonomy_node_id = @in_taxonomy_node_id
				END
			  
			 IF @in_ui_action_cr_fk = 8007 --activate
                BEGIN
					UPDATE epacube.taxonomy_node
					set record_status_cr_fk = 1
					, update_timestamp = getdate()
					where taxonomy_node_id = @in_taxonomy_node_id
				END
			END
-----------------------
--- End inactivation
-----------------------

        END TRY

        BEGIN CATCH
            BEGIN
                IF ISNULL(@ErrorMessage, '') = '' 
                    SELECT  @ErrorMessage = ERROR_MESSAGE()
                SET @ErrorSeverity = 16
                SET @ErrorState = 1
                RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState ) 
                RETURN ( -1 )
            END
            RETURN ( -1 )
               
        END CATCH		

    END
