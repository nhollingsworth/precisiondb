﻿











-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Called from the UI to Create New entity
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        04/27/2009  Initial SQL Version
-- CV        04/27/2010  Added level seq to insert statement
-- CV        05/03/2010  Comment out Epacube search 
-- CV        07/15/2010  Added Entity class and entity data name fk.
--
CREATE PROCEDURE [synchronizer].[EVENT_ENTITY_NEW]
(  @in_update_user varchar (64),
   @in_data_name_fk INT,
   @out_entity_structure_fk bigint OUTPUT )

AS
BEGIN

DECLARE @l_sysdate				datetime
DECLARE @ls_stmt				varchar (1000)
DECLARE @l_exec_no				bigint
DECLARE @v_entity_structure_id	BIGINT
DECLARE @v_entity_class_cr_fk	BIGINT
DECLARE @v_entity_data_name     VARCHAR(16)
DECLARE @v_event_id	            bigint
DECLARE @v_batch_no	            bigint
DECLARE @es_id_tbl				table (v_entity_structure_id bigint)
DECLARE @event_id_tbl			table (v_event_id bigint)


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate()


EXEC seq_manager.db_get_next_sequence_value_impl 'common','batch_seq', @v_batch_no output


SELECT @v_entity_class_cr_fk = ds.entity_class_cr_fk
      ,@v_entity_data_name   = dn.name
FROM epacube.data_name dn WITH (NOLOCK) 
INNER JOIN epacube.data_set ds WITH (NOLOCK)
  ON ( ds.data_set_id = dn.data_set_fk ) 
WHERE dn.data_name_id =  @in_data_name_fk


-----------------------------------------------------------------------
--   Create New entity with Status SETUP
-----------------------------------------------------------------------

 INSERT INTO epacube.entity_structure
        (  
         entity_status_cr_fk
        ,entity_class_cr_fk
	    ,data_name_fk
,Level_seq)
	OUTPUT INSERTED.entity_structure_id INTO @es_id_tbl
 SELECT 
	    10200  -- hardcode for now later use epacube_params
	   ,@v_entity_class_cr_fk
	   ,@in_data_name_fk   -- entity
,(select level_seq from epacube.entity_class_tree
where data_name_fk = @in_data_name_fk
and record_status_cr_fk = 1 )

 SELECT top 1 @v_entity_structure_id = v_entity_structure_id from @es_id_tbl


-- -----------------------------------------------------------------
-- --   NEW entity EVENT
-- -----------------------------------------------------------------

	INSERT INTO [synchronizer].[EVENT_DATA]
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]			   
			   ,[DATA_NAME_FK]
			   ,[ENTITY_DATA_NAME_FK]
			   ,[NEW_DATA]
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[BATCH_NO]
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])
--
VALUES (
        154   -- entity event
       ,@v_entity_class_cr_fk
	   ,utilities.TruncMMDDYYYY (@l_sysdate)
	   ,@v_entity_structure_id
	   ,@in_data_name_fk
	   ,@in_data_name_fk
	   ,'**NEW ' + @v_entity_data_name + '**'
	   ,@v_entity_class_cr_fk
	   ,@v_entity_structure_id
	   ,1   -- all orgs
	   ,97  --green
	   ,83  --Pre-post
       ,64  --immediate
	   ,71  --entity manager
	   ,54
	   ,@v_batch_no
	   ,@l_sysdate
	   ,@in_update_user
       )

-- -----------------------------------------------------------------
-- --   CALL EVENT POSTING
-- -----------------------------------------------------------------


--   EXEC SYNCHRONIZER.EVENT_POSTING  @v_batch_no


-- -----------------------------------------------------------------
-- --   RETURN NEW entity ID
-- -----------------------------------------------------------------

SET @out_entity_structure_fk = @v_entity_structure_id

END TRY
BEGIN CATCH

        EXEC exec_monitor.Report_Error_And_Stop_sp
	 		@exec_id = @l_exec_no,
			@user_error_desc = 'SYNCHRONIZER.EVENT_ENTITY_NEW procedure failed'
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;

END CATCH


END










































