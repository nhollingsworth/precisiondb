﻿






-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform general event validations for product_tax attributes
--			Currently in V5 doing edits with Node Changes ONLY... need to add this in future.
--			No procedure is currently call this.
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        07/31/2000   Initial SQL Version



CREATE PROCEDURE [synchronizer].[Z_EVENT_POST_VALID_TYPE_153] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int

DECLARE @GreenCondition		int
DECLARE @YellowCondition	int
DECLARE @RedCondition		int
DECLARE @BatchNo			bigint
DECLARE @ThisEvent		    bigint
DECLARE @EventsTree			int
DECLARE @OldNode			bigint
DECLARE @NewNode			bigint
DECLARE @ProductEventType   int


set @GreenCondition = epacube.getCodeRefId('EVENT_CONDITION','GREEN')
set @YellowCondition = epacube.getCodeRefId('EVENT_CONDITION','YELLOW')
set @RedCondition = epacube.getCodeRefId('EVENT_CONDITION','RED')
set @BatchNo = (select distinct batch_no from #TS_EVENT_DATA)
set @ProductEventType = epacube.getCodeRefId('EVENT_TYPE','PRODUCT')

BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_153.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;



--------------------------------------------------------------------------------------------
--  VERIFY THAT PRODUCT IS ALLOWED TO HAVE ATTRIBUTE BASED ON TREE ASSIGNMENT
--------------------------------------------------------------------------------------------





--------------------------------------------------------------------------------------------
--  VERIFY THAT ATTRIBUTE VALUE IS ALLOWED FOR TREE ASSIGMENT
--------------------------------------------------------------------------------------------






------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_153'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_153 has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END