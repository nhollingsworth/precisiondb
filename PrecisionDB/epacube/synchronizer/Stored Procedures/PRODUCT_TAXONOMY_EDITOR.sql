﻿


-- Copyright 2006
--
-- Procedure created by Walt Tucholski
--
--
-- Purpose: Called from the UI to perform PRODUCT_TAXONOMY
--			table maintenance.
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- WT        04/05/2007  Initial SQL Version
-- RG		 01/02/2008  EPA-1449 Adding delete functionality
--
CREATE PROCEDURE [synchronizer].[PRODUCT_TAXONOMY_EDITOR]
( @in_taxonomy_node_fk int,
  @in_product_structure_fk bigint,
  @in_taxonomy_tree_fk int,
  @in_ui_action_cr_fk int)
												
AS

/***************  Parameter Defintions  **********************
CURRENTLY USED IN THE PRODUCT MANAGER EDITOR....

Simple for Now.... Only CREATE and CHANGE;

***************************************************************/

BEGIN

DECLARE @v_count			    int
DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int


SET NOCOUNT ON
SET @ErrorMessage = null

IF @in_product_structure_fk IS NULL 
BEGIN
	SET @ErrorMessage = 'Product cannot be null'
    GOTO RAISE_ERROR
END

IF @in_ui_action_cr_fk = 8004  -- delete
BEGIN
	DELETE FROM epacube.product_taxonomy
	WHERE product_structure_fk = @in_product_structure_fk

	DELETE FROM epacube.product_tax_properties
	WHERE product_structure_fk = @in_product_structure_fk
	return (0)
END -- IF @in_ui_action_cr_fk = 8004

IF @in_ui_action_cr_fk = 8003 --create
BEGIN
IF @in_taxonomy_node_fk IS NULL 
BEGIN
	SET @ErrorMessage = 'Taxonomy Node must be supplied'
    GOTO RAISE_ERROR
END

IF @in_taxonomy_tree_fk IS NULL 
BEGIN
	SET @ErrorMessage = 'Taxonomy Tree must be supplied'
    GOTO RAISE_ERROR
END

SELECT @v_count = count(1)
FROM epacube.PRODUCT_TAXONOMY
WHERE PRODUCT_STRUCTURE_FK = @in_product_structure_fk

IF @v_count = 0
	INSERT into epacube.PRODUCT_TAXONOMY
	(PRODUCT_STRUCTURE_FK
	 ,TAXONOMY_TREE_FK
	,TAXONOMY_NODE_FK)
	SELECT @in_product_structure_fk
		  ,@in_taxonomy_tree_fk
		  ,@in_taxonomy_node_fk
ELSE
	UPDATE epacube.PRODUCT_TAXONOMY
	  SET TAXONOMY_TREE_FK = @in_taxonomy_tree_fk
		 ,TAXONOMY_NODE_FK = @in_taxonomy_node_fk
		 ,UPDATE_TIMESTAMP = getdate()
	WHERE product_structure_fk = @in_product_structure_fk
END

RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END



























































