﻿



-- Copyright 2010
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: To Remove inactive data once exported
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        03/11/2015   Rewritten to make more performant
-- CV        12/01/2015   tightened up select for non export data names
-- CV		 10/21/2016	  Added union to include deletes for changes from host
-- CV		 04/14/2017   Fixing joins and where claus for desc and attributes.  

CREATE PROCEDURE [synchronizer].[PURGE_INACTIVE_AFTER_EXPORT] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @ls_exec2            varchar(100)
DECLARE  @ls_exec3             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint
DECLARE @test varchar(50)

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime
DECLARE  @v_import_name       varchar (50)
DECLARE  @v_schema_name       varchar (50)


DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.PURGE_INACTIVE_AFTER_EXPORT.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;

--'PRODUCT_MULT_TYPE'
----------------------------------------------------------------------
---USED TO GET CHANGES EXPORT NAMES AND DATA NAMES-
    
----------------------------------------------------------------------
---so UI displays correctly and cleanup logic
update pta
set attribute_event_data = '-999999999'
from epacube.product_tax_attribute  pta
where pta.record_status_cr_fk = 2
---------------------------------------------------------------------
 
--drop temp table if it exists
	IF object_id('tempdb..#TS_INACTIVATION') is not null
	   drop table #TS_INACTIVATION;
	   
	   
	-- create temp table
	CREATE TABLE #TS_INACTIVATION(
	 TABLE_NAME [varchar](128),
	COLUMN_NAME [varchar](128),
    DATA_NAME_FK [bigint],
	DATA_TYPE_CR_FK [bigint],
	PRODUCT_STRUCTURE_FK [bigint],
	ENTITY_STRUCTURE_FK [bigint],
	ORG_ENTITY_STRUCTURE_FK [bigint],
	UPDATE_TIMESTAMP [datetime],
	Export_name [varchar] (128),
	Export_timestamp  [datetime],
	Event_source_cr_fk int,
	EVENT_ID [bigint])

 
  INSERT INTO #TS_INACTIVATION(
	 [TABLE_NAME] ,
	COLUMN_NAME ,
	[DATA_NAME_FK] ,
	[DATA_TYPE_CR_FK] ,
	PRODUCT_STRUCTURE_FK  ,
	ENTITY_STRUCTURE_FK ,
	ORG_ENTITY_STRUCTURE_FK  ,
	UPDATE_TIMESTAMP,
	Export_name,
	Export_timestamp,
	Event_source_cr_fk, 
	EVENT_ID )
		SELECT DISTINCT					 
							 DS.TABLE_NAME
							 ,case when ds.TABLE_NAME = 'PRODUCT_ATTRIBUTE'then 'ATTRIBUTE_EVENT_DATA'
								  when ds.TABLE_NAME = 'PRODUCT_TAX_ATTRIBUTE'then 'ATTRIBUTE_EVENT_DATA' ELSE   ds.COLUMN_NAME END 
						    ,ed.data_name_fk
							 ,ds.data_type_cr_fk
							 ,ed.product_structure_fk
							 ,isnull(ed.entity_structure_fk,0)
							 ,ed.org_entity_structure_fk
							 ,ed.UPDATE_TIMESTAMP
							 ,CNFJ.NAME
							 ,NULL
							 ,ed.EVENT_SOURCE_CR_FK
							 ,event_id
							from synchronizer.EVENT_DATA_HISTORY ED with (nolock)
							INNER JOIN epacube.DATA_NAME dn with (nolock)
							on (dn.DATA_NAME_ID = ED.DATA_NAME_FK) 
							INNER JOIN epacube.DATA_SET DS with (nolock)
							on (ds.data_set_id = dn.data_set_fk)
							Inner join  epacube.CONFIG_DATA_NAME CDN with (nolock)
							on (cdn.DATA_NAME_FK = ed.DATA_NAME_FK)
						inner join  epacube.CONFIG_PROPERTY cp1 with (nolock)
								on (cp1.value= cdn.CONFIG_FK AND cp1.name = 'COLUMNS_CONFIG_ID')
						inner join 	  epacube.CONFIG CNFJ WITH (NOLOCK)
						on (cp1.CONFIG_FK = cnfj.CONFIG_ID)
						INNER JOIN epacube.CONFIG_PROPERTY CNFJPCHG
										ON ( CNFJ.CONFIG_ID = CNFJPCHG.CONFIG_FK
										AND  CNFJPCHG.NAME = 'FILTER_CHANGES'   -- CHANGES ONLY 
										AND  CNFJPCHG.VALUE = 'TRUE'  )

							and (ED.NEW_DATA = '-999999999'
							or ED.NEW_DATA = 'NULL')
							and ED.EVENT_STATUS_CR_FK = 81
							--and ED.EVENT_SOURCE_CR_FK <> 79
							and ED.event_type_cr_fk IN ( 150, 156, 153 )
						UNION
						SELECT DISTINCT					 
							 DS.TABLE_NAME
							 ,case when ds.TABLE_NAME = 'PRODUCT_ATTRIBUTE'then 'ATTRIBUTE_EVENT_DATA'
								  when ds.TABLE_NAME = 'PRODUCT_TAX_ATTRIBUTE'then 'ATTRIBUTE_EVENT_DATA' ELSE   ds.COLUMN_NAME END 
						    ,ed.data_name_fk
							 ,ds.data_type_cr_fk
							 ,ed.product_structure_fk
							 ,isnull(ed.entity_structure_fk,0)
							 ,ed.org_entity_structure_fk
							 ,ed.UPDATE_TIMESTAMP
							 ,CNFJ.NAME
							 ,NULL
							 ,ed.EVENT_ACTION_CR_FK
							 ,event_id
							from synchronizer.EVENT_DATA ED with (nolock)
							INNER JOIN epacube.DATA_NAME dn with (nolock)
							on (dn.DATA_NAME_ID = ED.DATA_NAME_FK) 
							INNER JOIN epacube.DATA_SET DS with (nolock)
							on (ds.data_set_id = dn.data_set_fk)
							Inner join  epacube.CONFIG_DATA_NAME CDN with (nolock)
							on (cdn.DATA_NAME_FK = ed.DATA_NAME_FK)
						inner join  epacube.CONFIG_PROPERTY cp1 with (nolock)
								on (cp1.value= cdn.CONFIG_FK AND cp1.name = 'COLUMNS_CONFIG_ID')
						inner join 	  epacube.CONFIG CNFJ WITH (NOLOCK)
						on (cp1.CONFIG_FK = cnfj.CONFIG_ID)
						INNER JOIN epacube.CONFIG_PROPERTY CNFJPCHG
										ON ( CNFJ.CONFIG_ID = CNFJPCHG.CONFIG_FK
										AND  CNFJPCHG.NAME = 'FILTER_CHANGES'   -- CHANGES ONLY 
										AND  CNFJPCHG.VALUE = 'TRUE'  )

							and (ED.NEW_DATA = '-999999999'
							or ED.NEW_DATA = 'NULL')
							and ED.EVENT_STATUS_CR_FK = 81
							--and ED.EVENT_SOURCE_CR_FK <> 79
							and ED.event_type_cr_fk IN ( 150, 156, 153 )

						UNION
						SELECT DISTINCT					 
							 DS.TABLE_NAME
							 ,case when ds.TABLE_NAME = 'PRODUCT_ATTRIBUTE'then 'ATTRIBUTE_EVENT_DATA'
								  when ds.TABLE_NAME = 'PRODUCT_TAX_ATTRIBUTE'then 'ATTRIBUTE_EVENT_DATA' ELSE   ds.COLUMN_NAME END 
						    ,ed.data_name_fk
							 ,ds.data_type_cr_fk
							 ,ed.product_structure_fk
							 ,isnull(ed.entity_structure_fk,0)
							 ,ed.org_entity_structure_fk
							 ,ed.UPDATE_TIMESTAMP
							 ,NULL    --CNFJ.NAME
							 ,NULL
							 ,ed.EVENT_SOURCE_CR_FK
							 ,event_id
							from synchronizer.EVENT_DATA_HISTORY ED with (nolock)
							INNER JOIN epacube.DATA_NAME dn with (nolock)
							on (dn.DATA_NAME_ID = ED.DATA_NAME_FK) 
							INNER JOIN epacube.DATA_SET DS with (nolock)
							on (ds.data_set_id = dn.data_set_fk)
						--	Inner join  epacube.CONFIG_DATA_NAME CDN with (nolock)
						--	on (cdn.DATA_NAME_FK = ed.DATA_NAME_FK)
						--inner join  epacube.CONFIG_PROPERTY cp1 with (nolock)
						--		on (cp1.value= cdn.CONFIG_FK AND cp1.name = 'COLUMNS_CONFIG_ID')
						--inner join 	  epacube.CONFIG CNFJ WITH (NOLOCK)
						--on (cp1.CONFIG_FK = cnfj.CONFIG_ID)
						--INNER JOIN epacube.CONFIG_PROPERTY CNFJPCHG
						--				ON ( CNFJ.CONFIG_ID = CNFJPCHG.CONFIG_FK
						--				AND  CNFJPCHG.NAME = 'FILTER_CHANGES'   -- CHANGES ONLY 
						--				AND  CNFJPCHG.VALUE = 'TRUE'  )

							and (ED.NEW_DATA = '-999999999'
							or ED.NEW_DATA = 'NULL')
							and ED.EVENT_STATUS_CR_FK = 81
							and ED.EVENT_SOURCE_CR_FK = 79
							and ED.event_type_cr_fk IN ( 150, 156, 153 )							
						  


								UPDATE tmp
								set Export_timestamp =  JOB_COMPLETE_TIMESTAMP 
								from #TS_INACTIVATION tmp
								inner join (select  a.export , max(a.needed) Job_complete_timestamp
								 from (
								select cnfj.name export , JOB_COMPLETE_TIMESTAMP needed from epacube.CONFIG CNFJ WITH (NOLOCK)
														INNER JOIN epacube.CONFIG_PROPERTY CNFJPCHG WITH (NOLOCK)
																		ON ( CNFJ.CONFIG_ID = CNFJPCHG.CONFIG_FK
																		AND  CNFJPCHG.NAME = 'FILTER_CHANGES'   -- CHANGES ONLY 
																		AND  CNFJPCHG.VALUE = 'TRUE'  )
													 Inner join common.job j on j.name = cnfj.name 
													) a 
													group by a.export) b 
													on b.export = tmp.Export_name




--select * from #TS_INACTIVATION


-------------------------------------------------------------------------------------
   delete from epacube.product_category
	where PRODUCT_CATEGORY_ID in (select PRODUCT_CATEGORY_ID
	from epacube.product_category pc 
	inner join #TS_INACTIVATION  TMP on (tmp.PRODUCT_STRUCTURE_FK = pc.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = pc.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(pc.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = pc.DATA_NAME_FK)
	inner join epacube.DATA_VALUE dv on (pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
	and dv.DATA_NAME_FK = tmp.DATA_NAME_FK
	and dv.value = '-999999999')
	and isNULL(tmp.UPDATE_TIMESTAMP, '01/01/1999') < isnull(tmp.Export_timestamp,'01/01/1999')
	or tmp.Event_Source_cr_fk =79)
	
	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Category remove -999999999 exported data ', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;


	 delete from epacube.PRODUCT_ATTRIBUTE
	where PRODUCT_ATTRIBUTE_ID in (select PRODUCT_ATTRIBUTE_ID
	from epacube.PRODUCT_ATTRIBUTE pa
	inner join #TS_INACTIVATION  TMP on (tmp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(pa.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = pa.DATA_NAME_FK)
	inner join epacube.DATA_VALUE dv on (pa.DATA_VALUE_FK = dv.DATA_VALUE_ID
	and dv.DATA_NAME_FK = tmp.DATA_NAME_FK
	and dv.value = '-999999999')
	and isNULL(tmp.UPDATE_TIMESTAMP, '01/01/1999') < isnull(tmp.Export_timestamp,'01/01/1999')
	or tmp.Event_Source_cr_fk =79)


	 delete from epacube.PRODUCT_ATTRIBUTE
	where PRODUCT_ATTRIBUTE_ID in (select PRODUCT_ATTRIBUTE_ID
	from epacube.PRODUCT_ATTRIBUTE pa
	inner join #TS_INACTIVATION  TMP on (tmp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK
    and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(pa.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = pa.DATA_NAME_FK)
	WHERE pa.ATTRIBUTE_EVENT_DATA = '-999999999'
	and isNULL(tmp.UPDATE_TIMESTAMP, '01/01/1999') < isnull(tmp.Export_timestamp,'01/01/1999')
	or tmp.Event_Source_cr_fk =79)


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product attribute remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;

         
	 delete from epacube.PRODUCT_DESCRIPTION
	where PRODUCT_DESCRIPTION_ID in (select PRODUCT_Description_ID
	from epacube.PRODUCT_DESCRIPTION pd
	inner join #TS_INACTIVATION  TMP on (tmp.PRODUCT_STRUCTURE_FK = pd.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = pd.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(pd.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = pd.DATA_NAME_FK)
	WHERE pd.DESCRIPTION = '-999999999'
	and isNULL(tmp.UPDATE_TIMESTAMP, '01/01/1999') < isnull(tmp.Export_timestamp,'01/01/1999')
	or tmp.Event_Source_cr_fk =79)



	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Description remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;

         
	 delete from epacube.PRODUCT_Text
	where PRODUCT_TEXT_ID in (select PRODUCT_TEXT_ID
	from epacube.PRODUCT_Text pt
	inner join #TS_INACTIVATION  TMP on (tmp.PRODUCT_STRUCTURE_FK = pt.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = pt.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(pt.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = pt.DATA_NAME_FK)
	WHERE pt.TEXT = '-999999999'
	and isNULL(tmp.UPDATE_TIMESTAMP, '01/01/1999') < isnull(tmp.Export_timestamp,'01/01/1999')
	or tmp.Event_Source_cr_fk =79)


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Text remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;


	 delete from epacube.PRODUCT_UOM_CLASS
	where PRODUCT_UOM_CLASS_ID in (select PRODUCT_UOM_CLASS_ID
	from epacube.PRODUCT_UOM_CLASS puc
	inner join #TS_INACTIVATION  TMP on (tmp.PRODUCT_STRUCTURE_FK = puc.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = puc.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(puc.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = puc.DATA_NAME_FK)
	inner join epacube.UOM_CODE uc on (puc.UOM_CODE_FK = uc.UOM_CODE_ID
	and uc.UOM_CODE = '-999999999')
	and isNULL(tmp.UPDATE_TIMESTAMP, '01/01/1999') < isnull(tmp.Export_timestamp,'01/01/1999')
	or tmp.Event_Source_cr_fk =79)


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product UOM CLASS remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;


	  delete from epacube.PRODUCT_STATUS
	where PRODUCT_STATUS_ID in (select PRODUCT_STATUS_ID
	from epacube.PRODUCT_STATUS ps
	inner join #TS_INACTIVATION  TMP on (tmp.PRODUCT_STRUCTURE_FK = ps.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = ps.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(ps.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = ps.DATA_NAME_FK)
	inner join epacube.DATA_VALUE dv on (ps.DATA_VALUE_FK = dv.DATA_VALUE_ID
	and dv.DATA_NAME_FK = tmp.DATA_NAME_FK
	and dv.value = '-999999999')
	and isNULL(tmp.UPDATE_TIMESTAMP, '01/01/1999') < isnull(tmp.Export_timestamp,'01/01/1999')
	or tmp.Event_Source_cr_fk =79)


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Status remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;


 delete from epacube.PRODUCT_TAX_ATTRIBUTE
	where PRODUCT_TAX_ATTRIBUTE_ID in (select PRODUCT_TAX_ATTRIBUTE_ID
	from epacube.PRODUCT_TAX_ATTRIBUTE pa
	inner join #TS_INACTIVATION  TMP on (tmp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
	 and tmp.DATA_NAME_FK = pa.DATA_NAME_FK)
	inner join epacube.DATA_VALUE dv on (pa.DATA_VALUE_FK = dv.DATA_VALUE_ID
	and dv.DATA_NAME_FK = tmp.DATA_NAME_FK
	and dv.value = '-999999999')
	and isNULL(tmp.UPDATE_TIMESTAMP, '01/01/1999') < isnull(tmp.Export_timestamp,'01/01/1999')
	or tmp.Event_Source_cr_fk =79)


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Tax Attribute remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;

 
delete from epacube.PRODUCT_MULT_TYPE 
	where PRODUCT_MULT_TYPE_ID in (select  PRODUCT_MULT_TYPE_ID
	from  epacube.PRODUCT_MULT_TYPE pa
	inner join #TS_INACTIVATION  TMP on (tmp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(pa.ENTITY_STRUCTURE_FK,0)
	 and tmp.DATA_NAME_FK = pa.DATA_NAME_FK)
	inner join epacube.DATA_VALUE dv on (pa.DATA_VALUE_FK = dv.DATA_VALUE_ID
	and dv.DATA_NAME_FK = tmp.DATA_NAME_FK
	and dv.value = '-999999999')
	and isNULL(tmp.UPDATE_TIMESTAMP, '01/01/1999') < isnull(tmp.Export_timestamp,'01/01/1999')
	or tmp.Event_Source_cr_fk =79)


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Mult type remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;

-------------------------------------------------------------------------------------------------

   
-------------------------------------------------------------------------------------
--Once data has been removed from epacube tables need to archive the event history 
-- so the event does not qualify next time around
-------------------------------------------------------------------------------------
	INSERT INTO synchronizer.EVENT_DATA_ARCHIVE (
		 EVENT_TYPE_CR_FK
		,EVENT_ENTITY_CLASS_CR_FK 
		,EPACUBE_ID    
		,IMPORT_JOB_FK     
		,IMPORT_PACKAGE_FK 
		,DATA_NAME_FK	
	    ,PRODUCT_STRUCTURE_FK
	    ,ORG_ENTITY_STRUCTURE_FK
	    ,ENTITY_STRUCTURE_FK    
		,ENTITY_CLASS_CR_FK     
		,ENTITY_DATA_NAME_FK    
        ,NEW_DATA         
        ,CURRENT_DATA     
		,DATA_VALUE_FK    
		,ENTITY_DATA_VALUE_FK
        ,NET_VALUE1_NEW      
        ,NET_VALUE2_NEW      
        ,NET_VALUE3_NEW      
        ,NET_VALUE4_NEW      
        ,NET_VALUE5_NEW      
        ,NET_VALUE6_NEW      
           
        ,NET_VALUE1_CUR      
        ,NET_VALUE2_CUR      
        ,NET_VALUE3_CUR     
        ,NET_VALUE4_CUR     
        ,NET_VALUE5_CUR     
        ,NET_VALUE6_CUR     
       
		,PERCENT_CHANGE1	 
		,PERCENT_CHANGE2	
		,PERCENT_CHANGE3	
		,PERCENT_CHANGE4	
		,PERCENT_CHANGE5	
		,PERCENT_CHANGE6	
		
        ,VALUE_UOM_CODE_FK 
        ,ASSOC_UOM_CODE_FK 
		,UOM_CONVERSION_FACTOR 
		,END_DATE_NEW          
        ,END_DATE_CUR          
		,EFFECTIVE_DATE_CUR    
		,EVENT_EFFECTIVE_DATE 
		,VALUE_EFFECTIVE_DATE
        ,RELATED_EVENT_FK	
        ,RULES_FK			
		,EVENT_ACTION_CR_FK   
		,EVENT_STATUS_CR_FK   
		,EVENT_CONDITION_CR_FK
		,EVENT_PRIORITY_CR_FK   
		,EVENT_SOURCE_CR_FK   
		,RESULT_TYPE_CR_FK	 
		,SINGLE_ASSOC_IND     
		,TABLE_ID_FK          
		,BATCH_NO			
		,IMPORT_BATCH_NO     
        ,JOB_CLASS_FK		
        ,IMPORT_DATE         
        ,IMPORT_FILENAME     
        ,STG_RECORD_FK       
		,RECORD_STATUS_CR_FK 
        ,UPDATE_TIMESTAMP	
        ,UPDATE_USER		    
     )
SELECT 
		 EVENT_TYPE_CR_FK
		,EVENT_ENTITY_CLASS_CR_FK 
		,EPACUBE_ID    
		,IMPORT_JOB_FK     
		,IMPORT_PACKAGE_FK 
		,DATA_NAME_FK	
	    ,PRODUCT_STRUCTURE_FK
	    ,ORG_ENTITY_STRUCTURE_FK
	    ,ENTITY_STRUCTURE_FK    
		,ENTITY_CLASS_CR_FK     
		,ENTITY_DATA_NAME_FK    
        ,NEW_DATA         
        ,CURRENT_DATA     
		,DATA_VALUE_FK    
		,ENTITY_DATA_VALUE_FK
        ,NET_VALUE1_NEW      
        ,NET_VALUE2_NEW      
        ,NET_VALUE3_NEW      
        ,NET_VALUE4_NEW      
        ,NET_VALUE5_NEW      
        ,NET_VALUE6_NEW      
       
        ,NET_VALUE1_CUR      
        ,NET_VALUE2_CUR      
        ,NET_VALUE3_CUR     
        ,NET_VALUE4_CUR     
        ,NET_VALUE5_CUR     
        ,NET_VALUE6_CUR     
      
		,PERCENT_CHANGE1	 
		,PERCENT_CHANGE2	
		,PERCENT_CHANGE3	
		,PERCENT_CHANGE4	
		,PERCENT_CHANGE5	
		,PERCENT_CHANGE6	
	
        ,VALUE_UOM_CODE_FK 
        ,ASSOC_UOM_CODE_FK 
		,UOM_CONVERSION_FACTOR 
		,END_DATE_NEW          
        ,END_DATE_CUR          
		,EFFECTIVE_DATE_CUR    
		,EVENT_EFFECTIVE_DATE 
		,VALUE_EFFECTIVE_DATE
        ,RELATED_EVENT_FK	
        ,RULES_FK			
		,EVENT_ACTION_CR_FK   
		,EVENT_STATUS_CR_FK   
		,EVENT_CONDITION_CR_FK
		,EVENT_PRIORITY_CR_FK   
		,EVENT_SOURCE_CR_FK   
		,RESULT_TYPE_CR_FK	 
		,SINGLE_ASSOC_IND     
		,TABLE_ID_FK          
		,BATCH_NO			
		,IMPORT_BATCH_NO     
        ,JOB_CLASS_FK		
        ,IMPORT_DATE         
        ,IMPORT_FILENAME     
        ,STG_RECORD_FK       
		,RECORD_STATUS_CR_FK 
        ,UPDATE_TIMESTAMP	
        ,UPDATE_USER	
         FROM synchronizer.event_data_history 
           where EVENT_ID in (select event_id from #TS_INACTIVATION tmp
		   where isNULL(tmp.UPDATE_TIMESTAMP, '01/01/1999') < isnull(tmp.Export_timestamp,'01/01/1999'))
                   
										
          DELETE FROM synchronizer.event_data_history 
          where EVENT_ID in (select event_id from #TS_INACTIVATION tmp
		   where isNULL(tmp.UPDATE_TIMESTAMP, '01/01/1999') < isnull(tmp.Export_timestamp,'01/01/1999'))

------------------------------------------------------------------------
--REMOVE FROM EPACUBE DATA THAT ARE NOT PART OF EXPORT CONFIG
-- 
------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_INACTIVATION_2') is not null
	   drop table #TS_INACTIVATION;
	   
	   
	-- create temp table
	CREATE TABLE #TS_INACTIVATION_2 (
	 [TABLE_NAME] [varchar](128),
	COLUMN_NAME [varchar](128),
	[DATA_NAME_FK] [bigint],
	[DATA_TYPE_CR_FK] [bigint],
	PRODUCT_STRUCTURE_FK [bigint],
	ENTITY_STRUCTURE_FK [bigint],
	ORG_ENTITY_STRUCTURE_FK [bigint],
	EVENT_ID [bigint],
		)

 

INSERT into #TS_INACTIVATION_2 (
    [TABLE_NAME], 
	COLUMN_NAME,
	[DATA_NAME_FK],
	[DATA_TYPE_CR_FK] ,
	PRODUCT_STRUCTURE_FK ,
	ENTITY_STRUCTURE_FK,
	ORG_ENTITY_STRUCTURE_FK,
	EVENT_ID )
			(SELECT DISTINCT					 
							 DS.TABLE_NAME,
 case when ds.TABLE_NAME = 'PRODUCT_ATTRIBUTE'then 'ATTRIBUTE_EVENT_DATA' 
 when ds.TABLE_NAME = 'PRODUCT_TAX_ATTRIBUTE'then 'ATTRIBUTE_EVENT_DATA' ELSE   ds.COLUMN_NAME END , 
							 ed.data_name_fk
							 ,ds.data_type_cr_fk, 
							 ed.product_structure_fk
							 ,isnull(ed.entity_structure_fk,0)
							 ,ed.org_entity_structure_fk
							 ,ed.event_id
							from synchronizer.EVENT_DATA ED
							INNER JOIN epacube.DATA_NAME dn
							on (dn.DATA_NAME_ID = ED.DATA_NAME_FK)
							INNER JOIN epacube.DATA_SET DS
							on (ds.data_set_id = dn.data_set_fk)
							where 1= 1
							and ED.NEW_DATA = '-999999999'
							and ED.EVENT_STATUS_CR_FK = 81
							and ED.event_type_cr_fk IN ( 150, 156, 153 ) 
							and ED.DATA_NAME_FK NOT IN (
							
							select distinct cdn.DATA_NAME_FK from
							  epacube.CONFIG_DATA_NAME CDN with (nolock)
							inner join  epacube.CONFIG_PROPERTY cp1 with (nolock)
								on (cp1.value= cdn.CONFIG_FK AND cp1.name = 'COLUMNS_CONFIG_ID')
						inner join 	  epacube.CONFIG CNFJ WITH (NOLOCK)
						on (cp1.CONFIG_FK = cnfj.CONFIG_ID)
						INNER JOIN epacube.CONFIG_PROPERTY CNFJPCHG
										ON ( CNFJ.CONFIG_ID = CNFJPCHG.CONFIG_FK
										AND  CNFJPCHG.NAME = 'FILTER_CHANGES'   -- CHANGES ONLY 
										AND  CNFJPCHG.VALUE = 'TRUE'  )
														)
UNION														

SELECT DISTINCT					 
							 DS.TABLE_NAME,
 case when ds.TABLE_NAME = 'PRODUCT_ATTRIBUTE'then 'ATTRIBUTE_EVENT_DATA'
 when ds.TABLE_NAME = 'PRODUCT_TAX_ATTRIBUTE'then 'ATTRIBUTE_EVENT_DATA'  ELSE   ds.COLUMN_NAME END , 
							 ed.data_name_fk, ds.data_type_cr_fk, 
							 ed.product_structure_fk
							 ,isnull(ed.entity_structure_fk,0)
							 ,ed.org_entity_structure_fk
							 ,ed.event_id
							from synchronizer.EVENT_DATA_HISTORY ED
							INNER JOIN epacube.DATA_NAME dn
							on (dn.DATA_NAME_ID = ED.DATA_NAME_FK)
							INNER JOIN epacube.DATA_SET DS
							on (ds.data_set_id = dn.data_set_fk)
							and (ED.NEW_DATA = '-999999999'
							or ED.NEW_DATA = 'NULL')
							and ED.EVENT_STATUS_CR_FK = 81
							and ED.event_type_cr_fk IN ( 150, 156, 153 ) 
						
							and ED.DATA_NAME_FK NOT IN (
							select distinct cdn.DATA_NAME_FK from
							  epacube.CONFIG_DATA_NAME CDN with (nolock)
							inner join  epacube.CONFIG_PROPERTY cp1 with (nolock)
								on (cp1.value= cdn.CONFIG_FK AND cp1.name = 'COLUMNS_CONFIG_ID')
						inner join 	  epacube.CONFIG CNFJ WITH (NOLOCK)
						on (cp1.CONFIG_FK = cnfj.CONFIG_ID)
						INNER JOIN epacube.CONFIG_PROPERTY CNFJPCHG
										ON ( CNFJ.CONFIG_ID = CNFJPCHG.CONFIG_FK
										AND  CNFJPCHG.NAME = 'FILTER_CHANGES'   -- CHANGES ONLY 
										AND  CNFJPCHG.VALUE = 'TRUE'  )
														))



    delete from epacube.product_category
	where PRODUCT_CATEGORY_ID in (select PRODUCT_CATEGORY_ID
	from epacube.product_category pc 
	inner join #TS_INACTIVATION_2  TMP on (tmp.PRODUCT_STRUCTURE_FK = pc.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = pc.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(pc.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = pc.DATA_NAME_FK)
	inner join epacube.DATA_VALUE dv on (pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
	and dv.DATA_NAME_FK = tmp.DATA_NAME_FK
	and dv.value = '-999999999'))
	
	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Category remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;


	 delete from epacube.PRODUCT_ATTRIBUTE
	where PRODUCT_ATTRIBUTE_ID in (select PRODUCT_ATTRIBUTE_ID
	from epacube.PRODUCT_ATTRIBUTE pa
	inner join #TS_INACTIVATION_2  TMP on (tmp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(pa.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = pa.DATA_NAME_FK)
	inner join epacube.DATA_VALUE dv on (pa.DATA_VALUE_FK = dv.DATA_VALUE_ID
	and dv.DATA_NAME_FK = tmp.DATA_NAME_FK
	and dv.value = '-999999999'))


	 delete from epacube.PRODUCT_ATTRIBUTE
	where PRODUCT_ATTRIBUTE_ID in (select PRODUCT_ATTRIBUTE_ID
	from epacube.PRODUCT_ATTRIBUTE pa
	inner join #TS_INACTIVATION_2  TMP on (tmp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(pa.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = pa.DATA_NAME_FK)
	WHERE pa.ATTRIBUTE_EVENT_DATA = '-999999999')


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product attribute remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;

         
	 delete from epacube.PRODUCT_DESCRIPTION
	where PRODUCT_DESCRIPTION_ID in (select PRODUCT_Description_ID
	from epacube.PRODUCT_DESCRIPTION pd
	inner join #TS_INACTIVATION_2  TMP on (tmp.PRODUCT_STRUCTURE_FK = pd.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = pd.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(pd.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = pd.DATA_NAME_FK)
	WHERE pd.DESCRIPTION = '-999999999')


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Description remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;

         
	 delete from epacube.PRODUCT_Text
	where PRODUCT_TEXT_ID in (select PRODUCT_TEXT_ID
	from epacube.PRODUCT_Text pt
	inner join #TS_INACTIVATION_2  TMP on (tmp.PRODUCT_STRUCTURE_FK = pt.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = pt.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(pt.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = pt.DATA_NAME_FK)
	WHERE pt.TEXT = '-999999999')


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Text remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;


	 delete from epacube.PRODUCT_UOM_CLASS
	where PRODUCT_UOM_CLASS_ID in (select PRODUCT_UOM_CLASS_ID
	from epacube.PRODUCT_UOM_CLASS puc
	inner join #TS_INACTIVATION_2  TMP on (tmp.PRODUCT_STRUCTURE_FK = puc.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = puc.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(puc.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = puc.DATA_NAME_FK)
	inner join epacube.UOM_CODE uc on (puc.UOM_CODE_FK = uc.UOM_CODE_ID
	and uc.UOM_CODE = '-999999999'))


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product UOM CLASS remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;


	  delete from epacube.PRODUCT_STATUS
	where PRODUCT_STATUS_ID in (select PRODUCT_STATUS_ID
	from epacube.PRODUCT_STATUS ps
	inner join #TS_INACTIVATION_2  TMP on (tmp.PRODUCT_STRUCTURE_FK = ps.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = ps.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(ps.ENTITY_STRUCTURE_FK,0)
	and tmp.DATA_NAME_FK = ps.DATA_NAME_FK)
	inner join epacube.DATA_VALUE dv on (ps.DATA_VALUE_FK = dv.DATA_VALUE_ID
	and dv.DATA_NAME_FK = tmp.DATA_NAME_FK
	and dv.value = '-999999999'))


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Status remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;

	 delete from epacube.PRODUCT_TAX_ATTRIBUTE
	where PRODUCT_TAX_ATTRIBUTE_ID in (select PRODUCT_TAX_ATTRIBUTE_ID
	from epacube.PRODUCT_TAX_ATTRIBUTE pa
	inner join #TS_INACTIVATION_2  TMP on (tmp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
	and tmp.DATA_NAME_FK = pa.DATA_NAME_FK)
	inner join epacube.DATA_VALUE dv on (pa.DATA_VALUE_FK = dv.DATA_VALUE_ID
	and dv.DATA_NAME_FK = tmp.DATA_NAME_FK
	and dv.value = '-999999999'))


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Tax Attribute remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;
 
delete from epacube.PRODUCT_MULT_TYPE 
	where PRODUCT_MULT_TYPE_ID in (select  PRODUCT_MULT_TYPE_ID
	from  epacube.PRODUCT_MULT_TYPE pa
	inner join #TS_INACTIVATION_2  TMP on (tmp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
	and tmp.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK
	and isNULL(tmp.ENTITY_STRUCTURE_FK, 0) = isnull(pa.ENTITY_STRUCTURE_FK,0)
	 and tmp.DATA_NAME_FK = pa.DATA_NAME_FK)
	inner join epacube.DATA_VALUE dv on (pa.DATA_VALUE_FK = dv.DATA_VALUE_ID
	and dv.DATA_NAME_FK = tmp.DATA_NAME_FK
	and dv.value = '-999999999'))


	EXEC exec_monitor.start_monitor_sp @exec_desc = 'Finished Product Mult type remove -999999999', 
@exec_id = @l_exec_no OUTPUT, @epa_job_id = NULL;


-------------------------------------------------------------------------------------
--Once data has been removed from epacube tables need to archive the event history 
-- so the event does not qualify next time around
-------------------------------------------------------------------------------------
	INSERT INTO synchronizer.EVENT_DATA_ARCHIVE (
		 EVENT_TYPE_CR_FK
		,EVENT_ENTITY_CLASS_CR_FK 
		,EPACUBE_ID    
		,IMPORT_JOB_FK     
		,IMPORT_PACKAGE_FK 
		,DATA_NAME_FK	
	    ,PRODUCT_STRUCTURE_FK
	    ,ORG_ENTITY_STRUCTURE_FK
	    ,ENTITY_STRUCTURE_FK    
		,ENTITY_CLASS_CR_FK     
		,ENTITY_DATA_NAME_FK    
        ,NEW_DATA         
        ,CURRENT_DATA     
		,DATA_VALUE_FK    
		,ENTITY_DATA_VALUE_FK
        ,NET_VALUE1_NEW      
        ,NET_VALUE2_NEW      
        ,NET_VALUE3_NEW      
        ,NET_VALUE4_NEW      
        ,NET_VALUE5_NEW      
        ,NET_VALUE6_NEW      
       
        ,NET_VALUE1_CUR      
        ,NET_VALUE2_CUR      
        ,NET_VALUE3_CUR     
        ,NET_VALUE4_CUR     
        ,NET_VALUE5_CUR     
        ,NET_VALUE6_CUR     
     
		,PERCENT_CHANGE1	 
		,PERCENT_CHANGE2	
		,PERCENT_CHANGE3	
		,PERCENT_CHANGE4	
		,PERCENT_CHANGE5	
		,PERCENT_CHANGE6	
		
        ,VALUE_UOM_CODE_FK 
        ,ASSOC_UOM_CODE_FK 
		,UOM_CONVERSION_FACTOR 
		,END_DATE_NEW          
        ,END_DATE_CUR          
		,EFFECTIVE_DATE_CUR    
		,EVENT_EFFECTIVE_DATE 
		,VALUE_EFFECTIVE_DATE
        ,RELATED_EVENT_FK	
        ,RULES_FK			
		,EVENT_ACTION_CR_FK   
		,EVENT_STATUS_CR_FK   
		,EVENT_CONDITION_CR_FK
		,EVENT_PRIORITY_CR_FK   
		,EVENT_SOURCE_CR_FK   
		,RESULT_TYPE_CR_FK	 
		,SINGLE_ASSOC_IND     
		,TABLE_ID_FK          
		,BATCH_NO			
		,IMPORT_BATCH_NO     
        ,JOB_CLASS_FK		
        ,IMPORT_DATE         
        ,IMPORT_FILENAME     
        ,STG_RECORD_FK       
		,RECORD_STATUS_CR_FK 
        ,UPDATE_TIMESTAMP	
        ,UPDATE_USER		    
     )
SELECT 
		 EVENT_TYPE_CR_FK
		,EVENT_ENTITY_CLASS_CR_FK 
		,EPACUBE_ID    
		,IMPORT_JOB_FK     
		,IMPORT_PACKAGE_FK 
		,DATA_NAME_FK	
	    ,PRODUCT_STRUCTURE_FK
	    ,ORG_ENTITY_STRUCTURE_FK
	    ,ENTITY_STRUCTURE_FK    
		,ENTITY_CLASS_CR_FK     
		,ENTITY_DATA_NAME_FK    
        ,NEW_DATA         
        ,CURRENT_DATA     
		,DATA_VALUE_FK    
		,ENTITY_DATA_VALUE_FK
        ,NET_VALUE1_NEW      
        ,NET_VALUE2_NEW      
        ,NET_VALUE3_NEW      
        ,NET_VALUE4_NEW      
        ,NET_VALUE5_NEW      
        ,NET_VALUE6_NEW      
       
        ,NET_VALUE1_CUR      
        ,NET_VALUE2_CUR      
        ,NET_VALUE3_CUR     
        ,NET_VALUE4_CUR     
        ,NET_VALUE5_CUR     
        ,NET_VALUE6_CUR     
       
		,PERCENT_CHANGE1	 
		,PERCENT_CHANGE2	
		,PERCENT_CHANGE3	
		,PERCENT_CHANGE4	
		,PERCENT_CHANGE5	
		,PERCENT_CHANGE6	
	
        ,VALUE_UOM_CODE_FK 
        ,ASSOC_UOM_CODE_FK 
		,UOM_CONVERSION_FACTOR 
		,END_DATE_NEW          
        ,END_DATE_CUR          
		,EFFECTIVE_DATE_CUR    
		,EVENT_EFFECTIVE_DATE 
		,VALUE_EFFECTIVE_DATE
        ,RELATED_EVENT_FK	
        ,RULES_FK			
		,EVENT_ACTION_CR_FK   
		,EVENT_STATUS_CR_FK   
		,EVENT_CONDITION_CR_FK
		,EVENT_PRIORITY_CR_FK   
		,EVENT_SOURCE_CR_FK   
		,RESULT_TYPE_CR_FK	 
		,SINGLE_ASSOC_IND     
		,TABLE_ID_FK          
		,BATCH_NO			
		,IMPORT_BATCH_NO     
        ,JOB_CLASS_FK		
        ,IMPORT_DATE         
        ,IMPORT_FILENAME     
        ,STG_RECORD_FK       
		,RECORD_STATUS_CR_FK 
        ,UPDATE_TIMESTAMP	
        ,UPDATE_USER	
         FROM synchronizer.event_data_history 
         where EVENT_ID in (select event_id from #TS_INACTIVATION_2)
                   
										
          DELETE FROM synchronizer.event_data_history 
          where EVENT_ID in (select event_id from #TS_INACTIVATION_2)
          

		  
--drop temp table if it exists
	IF object_id('tempdb..#TS_INACTIVATION') is not null
	   drop table #TS_INACTIVATION;

	   
--drop temp table if it exists
	IF object_id('tempdb..#TS_INACTIVATION_2') is not null
	   drop table #TS_INACTIVATION_2;
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.PURGE_INACTIVE_AFTER_EXPORT'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.PURGE_INACTIVE_AFTER_EXPORT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


          EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END


















