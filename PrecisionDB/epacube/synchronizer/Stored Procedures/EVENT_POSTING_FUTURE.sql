﻿






-- Copyright 2006 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott (original Oracle version by Kathi Scott)
--
--
-- Purpose: Approve future dated events, 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        05/07/2014   Initial SQL Version
-- CV        09/15/2014   adding to check events status to set to future if hold vs versa. 
-- CV        03/06/2015   REmoved The 85 HOLD CHECK was causing prmature posting for export check events. 
-- CV        06/12/2015    Added back hold but add a date check to only do this to future dated events


  CREATE PROCEDURE [synchronizer].[EVENT_POSTING_FUTURE] 
    AS
    BEGIN
      DECLARE 
        @l_sysdate datetime,
        @ls_stmt varchar(1000),
        @l_exec_no bigint,
        @status_desc varchar(max),     
        @v_batch_no bigint, 
        @v_Future_eff_days int, 
        @v_approve_event_date datetime, 
	    @FutureEventStatus int;
      SET @l_sysdate = getdate()

--------------------------------------------------------------------------
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;
  
---------------------------------------------------------------------------

 EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of SYNCHRONIZER.EVENT_POSTING_FUTURE' , 
												@exec_id = @l_exec_no OUTPUT;
        

        /* ----------------------------------------------------------------------------------- */
        --    Procedure for posting future dated events
        /* ----------------------------------------------------------------------------------- */
BEGIN TRY

 		

		SET @FutureEventStatus = epacube.getCodeRefId('EVENT_STATUS','FUTURE')
		SET @v_FUTURE_EFF_DAYS = (select value from epacube.epacube_params
									where name = 'FUTURE_EFF_DAYS')
       
--------------------------------------------------------------------------------------------------------
---added section to validate the correct order of event effective dates.
--------------------------------------------------------------------------------------------------------

	   --select * from synchronizer.EVENT_DATA where EVENT_STATUS_CR_FK in (84, 85)


	   UPDATE synchronizer.EVENT_DATA
set event_status_cr_fk = 84
,EVENT_CONDITION_CR_FK = 97
where event_id in (
  select a.event_id
  from (
      select pas.event_id,
      DENSE_RANK() over (Partition By pas.product_structure_fk, 
                                      PAS.data_name_FK,
                                      pas.org_entity_structure_fk
									  --pas.entity_structure_fk
      Order by pas.event_id  asc , pas.event_effective_date asc  ) as drank
      from synchronizer.EVENT_DATA pas with (nolock) where pas.event_status_cr_fk in (84)
  ) a
  where a.drank > 1
)

----------------------------------------------------------------------------------------------------------
update synchronizer.EVENT_DATA
set event_status_cr_fk = 85
,EVENT_CONDITION_CR_FK = 98
where event_id in (
  select a.event_id
  from (
      select pas.event_id,
      DENSE_RANK() over (Partition By pas.product_structure_fk, 
                                      PAS.data_name_FK,
                                      pas.org_entity_structure_fk
									  --pas.entity_structure_fk
      Order by pas.event_id desc, pas.event_effective_date desc  ) as drank
      from synchronizer.EVENT_DATA pas with (nolock) where pas.event_status_cr_fk in (84)
  ) a
  where a.drank > 1
)



-----------------------------------------------------------------------------------------
------   FUTURE event processing  APPROVE events
-----------------------------------------------------------------------------------------
		IF (SELECT DISTINCT (1)
			FROM SYNCHRONIZER.EVENT_DATA
			 WHERE EVENT_STATUS_CR_FK = @FutureEventStatus
				AND utilities.TRUNC(EVENT_EFFECTIVE_DATE) - @v_FUTURE_EFF_DAYS <= utilities.TRUNC(GETDATE())) > 0
		BEGIN
		
			EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'batch_seq', @v_batch_no output


			UPDATE SYNCHRONIZER.EVENT_DATA
			  SET BATCH_NO = @v_batch_no
              ,event_status_cr_fk = 86 --ready
			  WHERE EVENT_ID IN (
                 SELECT A.EVENT_ID
                 FROM ( SELECT EVENT_ID,
                              DENSE_RANK () OVER (PARTITION BY product_structure_fk, 
															   entity_structure_fk, 
															   org_entity_structure_fk,
															   data_name_fk 
														ORDER BY event_id ASC) drank
                        FROM SYNCHRONIZER.EVENT_DATA WITH (NOLOCK)
						WHERE EVENT_STATUS_CR_FK = @FutureEventStatus
				        AND   utilities.TRUNC(EVENT_EFFECTIVE_DATE)- @v_FUTURE_EFF_DAYS <= utilities.TRUNC(GETDATE())  
                      ) A
                 WHERE a.drank = 1
              )

			EXEC SYNCHRONIZER.EVENT_POSTING 
					   @v_batch_no 

END

--------------------------------------------------------------------------------------------
----Checking if there are events on HOLD. and need to be moved to future
---85 = hold
----------------------------------------------------------------------------------------------

IF (SELECT DISTINCT (1)
			FROM SYNCHRONIZER.EVENT_DATA
			 WHERE EVENT_STATUS_CR_FK = 85
			 AND EVENT_EFFECTIVE_DATE > getdate()
			 ) > 0

		BEGIN
		
			EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'batch_seq', @v_batch_no output


			UPDATE SYNCHRONIZER.EVENT_DATA
			  SET BATCH_NO = @v_batch_no
              ,event_status_cr_fk = 86 --ready
			  WHERE EVENT_STATUS_CR_FK in ( 84, 85)
			  	 AND EVENT_EFFECTIVE_DATE > getdate()

			EXEC SYNCHRONIZER.EVENT_POSTING 
					   @v_batch_no 

		
		END


--------------------------------------------------------------------------------------------------------
---added section to validate the correct order of event effective dates.
--------------------------------------------------------------------------------------------------------

	   --select * from synchronizer.EVENT_DATA where EVENT_STATUS_CR_FK in (84, 85)


	   UPDATE synchronizer.EVENT_DATA
set event_status_cr_fk = 84
,EVENT_CONDITION_CR_FK = 97
where event_id in (
  select a.event_id
  from (
      select pas.event_id,
      DENSE_RANK() over (Partition By pas.product_structure_fk, 
                                      PAS.data_name_FK,
                                      pas.org_entity_structure_fk
									  --pas.entity_structure_fk
      Order by pas.event_id  asc , pas.event_effective_date asc  ) as drank
      from synchronizer.EVENT_DATA pas with (nolock) where pas.event_status_cr_fk in (84)
  ) a
  where a.drank > 1
)

----------------------------------------------------------------------------------------------------------
update synchronizer.EVENT_DATA
set event_status_cr_fk = 85
,EVENT_CONDITION_CR_FK = 98
where event_id in (
  select a.event_id
  from (
      select pas.event_id,
      DENSE_RANK() over (Partition By pas.product_structure_fk, 
                                      PAS.data_name_FK,
                                      pas.org_entity_structure_fk
									  --pas.entity_structure_fk
      Order by pas.event_id desc, pas.event_effective_date desc  ) as drank
      from synchronizer.EVENT_DATA pas with (nolock) where pas.event_status_cr_fk in (84)
  ) a
  where a.drank > 1
)




---------------------------------------------------------------------------------------
----   End of Processing  Synchronizer.Event posting future
---------------------------------------------------------------------------------------

     SET @status_desc =  'finished execution of SYNCHRONIZER.EVENT_POSTING_FUTURE';
     EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
      
END TRY
BEGIN CATCH   
   --   DECLARE @ErrorMessage NVARCHAR(4000);
	  --DECLARE @ErrorSeverity INT;
	  --DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_FUTURE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

  EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
END




