﻿










-- Copyright 2008
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to perform Creation and Activation/Inactivation of Taxonomy Trees
--			maintenance. Initial version will not support Deletion, only Inactivation
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG		 01/09/2008  EPA-1453 Add support for multiple Taxonomy Trees
-- RG		 06/27/2008  Updating to new Taxonomy Data Model
-- RG		 10/06/2008	 Removing reference to deprecated fields
-- RG		 10/14/2008  Updating validations
-- RG		 07/21/2009  Fixing setup of Root Node
-- KS		 07/29/2009  Added insert into epacube_params table.. will want to remove later with mutl tree support.


CREATE PROCEDURE [synchronizer].[TAXONOMY_TREE_EDITOR] 
( @in_taxonomy_tree_id int,
  @in_taxonomy_tree_name varchar(32),
  @in_taxonomy_root_node_name varchar(64),	
  @in_tax_node_dn_fk int,
  @in_ui_action_cr_fk int,
  @in_update_user varchar(64))
												
AS

/***************  Parameter Defintions  **********************
CURRENTLY USED IN THE DATA MASTER EDITOR....

Simple for Now.... Only CREATE (8003), ACTIVATE(8007), INACTIVATE(8005)

***************************************************************/

BEGIN

DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int


SET NOCOUNT ON;
set @ErrorMessage = ''
BEGIN TRY

IF @in_ui_action_cr_fk in (8003)
BEGIN
	IF @in_taxonomy_root_node_name IS NULL 
	BEGIN		
		 SET @ErrorMessage = 'Taxonomy Node name cannot be null'
         set @ErrorSeverity = 16
         set @ErrorState = 1
         RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState )
	END
	IF @in_taxonomy_tree_name IS NULL 
	BEGIN
		SET @ErrorMessage = 'Taxonomy Tree Name cannot be null'
		set @ErrorSeverity = 16
        set @ErrorState = 1
        RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState )
	END
	
	IF @in_tax_node_dn_fk IS NULL
	BEGIN
		SET @ErrorMessage = 'A Category must be selected'
		set @ErrorSeverity = 16
        set @ErrorState = 1
        RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState )
	END

	--CHECK FOR DUPLICATE TREE NAME
	set @ErrorMessage = ''
	select @ErrorMessage = 'Duplicate Tree Name. Tree name already exists for Tree ID ' + 
             	+ CAST(isnull(TAXONOMY_TREE_ID,0) AS VARCHAR(30))
    from epacube.TAXONOMY_TREE
    where upper(name) = upper(@in_taxonomy_tree_name)

	 IF @ErrorMessage <> ''
			BEGIN
				set @ErrorSeverity = 16
				set @ErrorState = 1
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
			END
-------------------------------------------------------------------------------------
--   Edits passed Insert Row
-------------------------------------------------------------------------------------

	INSERT INTO EPACUBE.TAXONOMY_TREE
			( name
			,tax_node_dn_fk
			,record_status_cr_fk			
			,create_timestamp
			,update_timestamp)
	SELECT
               upper(@in_taxonomy_tree_name)
			  ,@in_tax_node_dn_fk
			  ,1			  
              ,getdate()
			  ,getdate()
	
	UPDATE epacube.epacube_params
	SET value = CAST (	(select taxonomy_tree_id 
						 from epacube.taxonomy_tree
						 where name = @in_taxonomy_tree_name)  AS VARCHAR(16) )
	WHERE NAME = 'TAX TREE ATTRIBUTE'
	
	--CONFIGURING ROOT NODE OF TREE
		INSERT INTO EPACUBE.DATA_VALUE
		(value
		,data_name_fk
		,description
		,record_status_cr_fk
		,create_timestamp
		,update_timestamp
		,update_user)
	SELECT
		 upper(@in_taxonomy_root_node_name	)
		,@in_tax_node_dn_fk
		,upper(@in_taxonomy_root_node_name	)
		,1
		,getdate()
		,getdate()
		,@in_update_user


	INSERT INTO EPACUBE.TAXONOMY_NODE
			( taxonomy_tree_fk
			  ,level_seq			 
			  ,tax_node_dv_fk
			  ,record_status_cr_fk
			  ,create_timestamp
			  ,update_timestamp	)
	SELECT
			(select taxonomy_tree_id 
			 from epacube.taxonomy_tree
			 where name = @in_taxonomy_tree_name)
			,0			
			,(select top 1 data_value_id from epacube.data_value dv
				where dv.value = @in_taxonomy_root_node_name and dv.data_name_fk = @in_tax_node_dn_fk)
			,1
			,getdate()
			,getdate()

----------------------
-- Now that the root node is created with a primary key, update the node to refer to itself as the root 
-- this is used to derive the parent node id for subsequent children nodes
---------------------

	UPDATE epacube.taxonomy_node
	set root_taxonomy_node_fk = (select taxonomy_node_id
								from epacube.taxonomy_node
								where tax_node_dv_fk in (select data_value_id
														 from epacube.data_value
													where description = @in_taxonomy_root_node_name)
			and taxonomy_tree_fk = (select taxonomy_tree_id 
									from epacube.taxonomy_tree where name = @in_taxonomy_tree_name )
									and level_seq = 0)
		where tax_node_dv_fk in (select data_value_id
														 from epacube.data_value
													where description = @in_taxonomy_root_node_name
			and taxonomy_tree_fk = (select taxonomy_tree_id 
									from epacube.taxonomy_tree where name = @in_taxonomy_tree_name )
									and level_seq = 0
									and parent_taxonomy_node_fk is null)


END -- IF @in_ui_action_cr_fk = 8003
--
IF @in_ui_action_cr_fk in (8007) --Activate
BEGIN
	IF @in_taxonomy_tree_id IS NULL 
	BEGIN
		SET @ErrorMessage = 'Taxonomy Tree ID cannot be null for activate action'
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
	END
	
	UPDATE EPACUBE.TAXONOMY_TREE
		set record_status_cr_fk = 1
		where taxonomy_tree_id = @in_taxonomy_tree_id
END --END ui_action_cr_fk = 8007 (Activate)
--
IF @in_ui_action_cr_fk in (8005) --Inactivate
BEGIN
	IF @in_taxonomy_tree_id IS NULL 
	BEGIN
		SET @ErrorMessage = 'Taxonomy Tree ID cannot be null for activate action'
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
	END
	
	UPDATE epacube.taxonomy_tree
		set record_status_cr_fk = 0
		where taxonomy_tree_id = @in_taxonomy_tree_id
END --END ui_action_cr_fk = 8007 (Inactivate)


END TRY
	BEGIN CATCH
            BEGIN
                  IF ISNULL(@ErrorMessage, '') = '' 
                        SELECT   @ErrorMessage = ERROR_MESSAGE()
                        set @ErrorSeverity = 16
                        set @ErrorState = 1
                        RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState ) 
                        return ( -1 )
                  END	  RETURN ( -1 )
               
	 END CATCH		

END











































































