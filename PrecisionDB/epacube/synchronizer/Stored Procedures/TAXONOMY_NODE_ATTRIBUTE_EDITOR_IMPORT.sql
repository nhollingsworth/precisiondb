﻿





-- Copyright 2007
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Called from the UI to add, modify, and delete Taxonomy Node Properties.
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG       05/20/2007  Initial SQL Version
-- RG	    07/07/2008  Updating to use new data model
-- RG		10/13/2008  Inserting first restricted value 
-- YN		10/28/2008  Adding hook for import call to assign Tax Node
-- RG		10/29/2008  Adding logic to create data value references 
--						for a Node Attribute that already exists
-- RG		10/31/2008	Do not enforce required default value validation
-- YN       11/11/2008  Added default value
-- RG		11/23/2008	EPA-1970 Adding Activation/Inactivation
-- RG		07/09/2009  EPA-2182 Adding is null to new Attr Insertion test.
-- KS		07/22/2009  Added the label column to the Data Name Insert statement
-- KS       07/23/2009  Added fetch of data_name_id from Sequence Table ( Identity turned OFF on Data Name Table )
-- RG       02/21/2014  EPA-3719 Modifying pricebook restriction and adding it to insert
-- RG		03/14/2014  EPA-3521 Uppercase value only if Data Name has upper case flag set
-- MS       05/13/2014  Added processing for the attribute and attribute value display sequence.

CREATE PROCEDURE [synchronizer].[TAXONOMY_NODE_ATTRIBUTE_EDITOR_IMPORT]
    (
      @in_taxonomy_tree_fk INT,
      @in_taxonomy_node_fk BIGINT,
      @in_taxonomy_node_attribute_id INT,
      @in_data_set_fk INT,
      @in_data_name VARCHAR(256),
      @in_data_value VARCHAR(875) --this is for default value for this attribute; goes to attr event data
      ,
      @in_pricebook_attribute_ind SMALLINT,
      @in_restricted_data_values_ind SMALLINT,
      @in_node_code VARCHAR(255),
      @in_update_user VARCHAR(64),
      @in_ui_action_cr_fk INT,
      @in_default_value VARCHAR(100),
	  @in_attribute_display_seq INT,
	  @in_attrvalue_display_seq INT,
	  @in_pricebook_ind_dv_fk bigint = NULL
    )
AS
/***************  Parameter Defintions  **********************
CURRENTLY USED IN THE DATA MASTER EDITOR....

Simple for Now.... Only CREATE (8003) and CHANGE (8006); DELETE (8004) to follow

***************************************************************/

    BEGIN

        DECLARE @v_product_structure_fk BIGINT
        DECLARE @v_taxonomy_properties_fk INT
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @TNA_data_name_id INT
        DECLARE @TNA_data_value_id BIGINT
        DECLARE @TNA_primary_key INT


        SET NOCOUNT ON ;
        SET @ErrorMessage = ''
        BEGIN TRY 
----------------------------------------------
-- VALIDATIONS
-- #1. Tree, node, data name, and data value may not be null for create/update
-- #2. Only one Attribute per node can have pricebook_attribute_ind = 1
-- #3. Check to see if restricted_dv_ind is set and make sure the data_type is correct
-- #4. If data name is pre-existing, verify that the data set/data types match
----------------------------------------------

----------------------------------------------
-- #1. Must Exist validations
----------------------------------------------
            IF @in_ui_action_cr_fk IN ( 8003 ) 
                BEGIN
                    IF @in_taxonomy_tree_fk IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'Tree ID cannot be null'
                                    
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

                    IF @in_taxonomy_node_fk IS NULL 
                        BEGIN
                            SET @ErrorMessage = 'Node ID cannot be null'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END
------------
-- Yuri adding catch for Import. He passes 0 as the node id and resolves it in here
-- because he cannot "know" the primary key before this point
------------          
                    IF @in_taxonomy_node_fk = 0 
                        SELECT TOP 1
                                @in_taxonomy_node_fk = tn.TAXONOMY_NODE_ID
                        FROM    epacube.TAXONOMY_NODE tn
                                INNER JOIN epacube.data_value dv ON tn.TAX_NODE_DV_FK = dv.DATA_VALUE_ID
                        WHERE   dv.value = @in_node_code
                                AND dv.data_name_fk = ( SELECT  tax_node_dn_fk
                                                        FROM    epacube.taxonomy_tree
                                                        WHERE   taxonomy_tree_id = @in_taxonomy_tree_fk
                                                      ) ;

                     
                    IF @in_taxonomy_node_fk = 0 -- could not find the correct node in the Taxonomy_Node table. Should not process as an attribute. Changed by Yuri Nogin 11/13/2008
                        BEGIN
                            SET @ErrorMessage = 'Node ID cannot be 0'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END
                    

                    IF ISNULL(@in_data_name, '') = '' 
                        BEGIN
                            SET @ErrorMessage = 'Data Name cannot be null'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

--                           IF @in_data_value IS NULL 
--                              BEGIN
--                                    SET @ErrorMessage = 'Data Value cannot be null'
--                                    set @ErrorSeverity = 16
--                                    set @ErrorState = 1
--                                    RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState )
--                              END

------------------------------------------------
-- #2. 1 PB Attribute per Node
------------------------------------------------
                    --SET @ErrorMessage = ''
                    --SELECT  @ErrorMessage = 'A Pricebook Attribute already exists on this node '
                    --        + ( SELECT  name
                    --            FROM    epacube.data_name
                    --            WHERE   data_name_id = tna.data_name_fk
                    --          )
                    --FROM    epacube.taxonomy_node_attribute tna
                    --WHERE   taxonomy_node_fk = @in_taxonomy_node_fk
                    --        AND pricebook_attribute_ind = 1
                    --        AND @in_pricebook_attribute_ind = 1
	
                    --IF @ErrorMessage <> '' 
                    --    BEGIN 
                    --        SET @ErrorSeverity = 16
                    --        SET @ErrorState = 1
                    --        RAISERROR ( @ErrorMessage, @ErrorSeverity,
                    --            @ErrorState )
                    --    END 
-------------------------------------------------
-- #3. Restricted DV/Data Type test
-------------------------------------------------

                    IF @in_restricted_data_values_ind > 0
                        AND @in_data_set_fk NOT IN (
                        SELECT  data_set_id
                        FROM    epacube.data_set
                        WHERE   data_type_cr_fk = ( SELECT  CODE_REF_ID
                                                    FROM    epacube.code_ref
                                                    WHERE   code = 'DATA VALUE'
                                                  ) ) 
                        BEGIN
                            SET @ErrorMessage = 'Restricted Data Values flag can only be set for Data Value Attribute Types'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                              
                        END
                END --end validations

--------------------------------------------------
-- #4. Verify that data name and data_types match if data name is pre-existing
--------------------------------------------------
            SET @ErrorMessage = ''
            SELECT  @ErrorMessage = 'Data Name already exists and is of type '
                    + ( SELECT  code
                        FROM    epacube.code_ref
                        WHERE   code_ref_id = ds.data_type_cr_fk
                      )
            FROM    epacube.data_name dn
                    INNER JOIN epacube.data_set ds ON ( data_set_fk = data_set_id )
            WHERE   dn.name = @in_data_name
                    AND ds.data_type_cr_fk != ( SELECT  data_type_cr_fk
                                                FROM    epacube.data_set
                                                WHERE   data_set_id = @in_data_set_fk
                                              ) 
	
            IF @ErrorMessage <> '' 
                BEGIN
                    SET @ErrorMessage = @ErrorMessage + ' ;Data Name - '
                        + @in_data_name + ' ;Data Set ID - '
                        + CAST(@in_data_set_fk AS VARCHAR(50))
                    SET @ErrorSeverity = 16
                    SET @ErrorState = 1
                    RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState )
                            
                END
               
--
            IF @in_ui_action_cr_fk = 8003 --create
                BEGIN

----------------------------
-- Check to see if data name exists. If it does not, create one.
-- Then assign dn_id to variable for future use
----------------------------
	

	 
                    IF NOT EXISTS ( SELECT  data_name_id
                                    FROM    epacube.data_name
                                    WHERE   name = @in_data_name ) 
                        BEGIN

                            --- KS added fetch of data_name_id sequence number ( Identity turned OFF )
						    EXEC seq_manager.db_get_next_sequence_value_impl
						      'common','data_name_id_seq', @TNA_data_name_id output


                            INSERT  INTO EPACUBE.DATA_NAME
                                    (
                                      data_name_id,
                                      name,
                                      label,
                                      data_set_fk,
                                      record_status_cr_fk,
                                      create_timestamp,
                                      update_timestamp 	
                                            
                                            
                                    )
                                    SELECT  
                                            @TNA_data_name_id,
                                            UPPER(@in_data_name),
                                            @in_data_name,                                            
                                            @in_data_set_fk,
                                            1,
                                            GETDATE(),
                                            GETDATE()
                        END -- end data_name_insertion
	


--		BEGIN	
                    SET @TNA_data_name_id = ( SELECT TOP 1
                                                        data_name_id
                                              FROM      epacube.data_name
                                              WHERE     name = @in_data_name
                                            )
	--	END

----------------------------
-- Check to see if data value exists. If it does not, create one.
-- Then assign value to variable for future use
----------------------------
	

                    IF NOT EXISTS ( SELECT  data_value_id
                                    FROM    epacube.data_value
                                    WHERE   (value is NULL or value = @in_data_value) -- EPA-2182
                                            AND data_name_fk = @TNA_data_name_id -- the next section is commented by Yuri Nogin 11/5/2008
--                                           AND 
--                                                    @in_data_set_fk IN ( SELECT DATA_SET_ID
--                                                                         FROM   epacube.data_set
--                                                                         WHERE  data_set_id IN ( SELECT data_set_id
--                                                                                                 FROM   epacube.data_set
--                                                                                                 WHERE  data_type_cr_fk = ( 134 ) ) ) 
                                                                                                 ) 
                        BEGIN
                            INSERT  INTO EPACUBE.DATA_VALUE
                                    (
                                      data_name_fk,
                                      value,
                                      description,
                                      record_status_cr_fk,
                                      create_timestamp,
                                      update_timestamp,
                                      update_user
                                            
                                            
                                    )
                                    SELECT  @TNA_data_name_id,
                                            CASE 
												WHEN (@TNA_data_name_id in (select data_name_id from epacube.data_name
																			where UPPER_CASE_IND = 1)) 
												THEN upper(@in_data_value)
												ELSE @in_data_value 
											END,
                                            @in_data_value,
                                            1,
                                            GETDATE(),
                                            GETDATE(),
                                            @in_update_user	

			
	  
                        END -- end data_value_insertion

                    SET @TNA_data_value_id = ( SELECT TOP 1
                                                        data_value_id
                                               FROM     epacube.data_value
                                               WHERE    (value is NULL or value = @in_data_value) --EPA-2182
                                                        AND data_name_fk = @TNA_data_name_id
                                             )
			

-------------------------------------------------------------------------------------
--   Edits passed Insert Row
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
-- If a Product Tax Attribute already exists of Data Name A, subsequent 
-- PTA should be inserted into DV table with references to the PTA Data Name FK and
-- the passed value 
-------------------------------------------------------------------------------------
                   
						
				
                    IF NOT EXISTS ( SELECT  taxonomy_node_attribute_id
                                    FROM    EPACUBE.TAXONOMY_NODE_ATTRIBUTE
                                    WHERE   data_name_fk = ( @TNA_data_name_id )
                                            AND taxonomy_node_fk = @in_taxonomy_node_fk ) 
                        BEGIN
                            DECLARE @MyTableVar TABLE
                                (
                                  taxonomy_node_attribute_id BIGINT
                                ) ;
                          
                            INSERT  INTO EPACUBE.TAXONOMY_NODE_ATTRIBUTE
                                    (
                                      taxonomy_node_fk,
                                      data_name_fk,
                                      data_value_fk,
                                      default_node_value,
                                      restricted_data_values_ind,
									  PRICEBOOK_ATTRIBUTE_IND,
                                      create_timestamp,
                                      update_timestamp,
									  DATA_NAME_DISPLAY_SEQ,
									  DATA_VALUE_DISPLAY_SEQ,
									  PRICEBOOK_IND_DV_FK
                                            
                                    )
                            OUTPUT  INSERTED.taxonomy_node_attribute_id
                                    INTO @MyTableVar
                                    SELECT  @in_taxonomy_node_fk,
                                            @TNA_data_name_id,
                                            @TNA_data_value_id,
                                            @in_default_value,
                                            @in_restricted_data_values_ind,
											@in_pricebook_attribute_ind,
                                            GETDATE(),
                                            GETDATE(),
											@in_attribute_display_seq,
											@in_attrvalue_display_seq,
											@in_pricebook_ind_dv_fk
                                            
                            SELECT TOP 1
                                    @TNA_Primary_key = taxonomy_node_attribute_id
                            FROM    @MyTableVar
                                                  
                            
                        END


									--added by Yuri Nogin 11/3/2008
                    IF @TNA_Primary_key IS NULL 
                        SELECT TOP 1
                                @TNA_Primary_key = taxonomy_node_attribute_id
                        FROM    EPACUBE.TAXONOMY_NODE_ATTRIBUTE
                        WHERE   data_name_fk = ( @TNA_data_name_id )
                                AND taxonomy_node_fk = @in_taxonomy_node_fk 
                    IF @in_taxonomy_node_attribute_id IS NULL 
                        SET @in_taxonomy_node_attribute_id = @TNA_Primary_key
--IF exists (select taxonomy_node_attribute_id from EPACUBE.TAXONOMY_NODE_ATTRIBUTE
--				where data_name_fk = (@TNA_data_name_id)
--				and taxonomy_node_fk = @in_taxonomy_node_fk)
--BEGIN
--			
--END
-------------------------
-- Call insert to restricted table if restricted flag: RG 10/13/2008
-------------------------
                    IF @in_restricted_data_values_ind = 1 
                        BEGIN
                            IF (@TNA_Primary_key IS NOT NULL and @in_data_value is not NULL)
                                EXEC [synchronizer].[taxonomy_node_attribute_dv_editor_import] @TNA_Primary_key,
                                    @in_data_value, 8003, @in_update_user, @in_attrvalue_display_seq
                        END --insertion into restricted values if it is a restricted value
	
                END --- IF @in_ui_action_cr_fk = 8003
--
            IF @in_ui_action_cr_fk = 8006 --change
                BEGIN
-- 

----------------------------------------------------------------------
-- Maintenance Validations
----------------------------------------------------------------------

--1. Check Pricebook Attr. Ind
                    SET @ErrorMessage = ''
                    SELECT  @ErrorMessage = 'A Pricebook Attribute already exists on this node '
                            + ( SELECT  name
                                FROM    epacube.data_name
                                WHERE   data_name_id = tna.data_name_fk
                              )
                    FROM    epacube.taxonomy_node_attribute tna
                    WHERE   taxonomy_node_fk = @in_taxonomy_node_fk
                            AND pricebook_attribute_ind = 1
                            AND @in_pricebook_attribute_ind = 1

                    IF @ErrorMessage <> '' 
                        BEGIN
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

--2. Check to make sure Data Value is valid

                    IF @in_restricted_data_values_ind IS NOT NULL
                        AND @in_data_set_fk NOT IN (
                        SELECT  data_set_id
                        FROM    epacube.data_set
                        WHERE   data_type_cr_fk = ( SELECT  CODE_REF_ID
                                                    FROM    epacube.code_ref
                                                    WHERE   code = 'DATA VALUE'
                                                  ) ) 
                        BEGIN
                            SET @ErrorMessage = 'Restricted Data Values flag can only be set for Data Value Attribute Types'
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                         
                        END
	

----------------------------------------------------------------------
-- Convert passed Data Value into a data value id
----------------------------------------------------------------------
                    SET @TNA_data_value_id = ( SELECT TOP 1
                                                        data_value_id
                                               FROM     epacube.data_value
                                               WHERE    description = @in_data_value
                                             )
---------------------------------------------------------------------------------------
----   Edits passed Update Row
---------------------------------------------------------------------------------------
                    UPDATE  EPACUBE.TAXONOMY_NODE_ATTRIBUTE
                    SET     data_value_fk = @TNA_data_value_id,
                            default_node_value = @in_data_value,
                            pricebook_attribute_ind = @in_pricebook_attribute_ind,
							PRICEBOOK_IND_DV_FK = @in_pricebook_ind_dv_fk,
                            update_timestamp = GETDATE()
                    WHERE   taxonomy_node_attribute_id = @in_taxonomy_node_attribute_id

--	  UPDATE EPACUBE.DATA_VALUE
--			 set description = @in_data_value
--			,value = upper(@in_data_value)
--			,update_timestamp = getdate()
--			,update_user = @in_update_user
--		where data_value_id = @TNA_data_value_id
--       set data_name_fk = (select data_name_id from epacube.data_name where name = @in_data_name),
--       data_value_fk = @in_data_value_fk,
--	   update_timestamp = getdate()
--       where taxonomy_node_attribute_id = @in_taxonomy_node_attribute_id
--	   and taxonomy_node_fk = @in_taxonomy_node
--
                END 

-------------------------------------------------------------------------------------
--   Peform UI Delete Action 
-------------------------------------------------------------------------------------

            IF @in_ui_action_cr_fk = 8004  -- delete
                BEGIN
					DELETE  FROM EPACUBE.TAXONOMY_NODE_ATTRIBUTE_DV
                    WHERE   taxonomy_node_attribute_fk = @in_taxonomy_node_attribute_id

                    DELETE  FROM EPACUBE.TAXONOMY_NODE_ATTRIBUTE
                    WHERE   taxonomy_node_attribute_id = @in_taxonomy_node_attribute_id
                    RETURN ( 0 )
                END -- IF @in_ui_action_cr_fk = 8004

----------------------
-- Nov 23 RG Adding Activation/Inactivation
----------------------

		IF @in_ui_action_cr_fk IN ( 8005,8007 ) 
          BEGIN
                IF @in_taxonomy_node_attribute_id IS NULL 
                      BEGIN
                            SET @ErrorMessage = 'Tax Node Attribute ID cannot be null'                                    
                            SET @ErrorSeverity = 16
                            SET @ErrorState = 1
                            RAISERROR ( @ErrorMessage, @ErrorSeverity,
                                @ErrorState )
                        END

			 IF @in_ui_action_cr_fk = 8005 --inactivate
                BEGIN
					UPDATE epacube.taxonomy_node_attribute
					set record_status_cr_fk = 2
					, update_timestamp = getdate()
					where taxonomy_node_attribute_id = @in_taxonomy_node_attribute_id
				END
			  
			 IF @in_ui_action_cr_fk = 8007 --activate
                BEGIN
					UPDATE epacube.taxonomy_node_attribute
					set record_status_cr_fk = 1
					, update_timestamp = getdate()
					where taxonomy_node_attribute_id = @in_taxonomy_node_attribute_id
				END
			END
-----------------------
--- End inactivation
-----------------------
                 	
        END TRY 


        BEGIN CATCH

            BEGIN
                IF ISNULL(@ErrorMessage, '') = '' 
                    SELECT  @ErrorMessage = ERROR_MESSAGE()
                SET @ErrorSeverity = 16
                SET @ErrorState = 1
                RAISERROR ( @ErrorMessage, @ErrorSeverity, @ErrorState ) 
                RETURN ( -1 )
            END	
        END CATCH 
    END













