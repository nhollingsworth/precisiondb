﻿











-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Called from the UI to Create New Product
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        12/15/2007   Initial SQL Version
-- CV        08/10/2009   Fixed to work from UI 
-- CV        05/03/2010   Commented Epacube Search


CREATE PROCEDURE [synchronizer].[EVENT_PRODUCT_PM_NEW]
(  @in_update_user varchar (64),
   @out_product_structure_fk bigint OUTPUT )

AS
BEGIN

DECLARE @l_sysdate				datetime
DECLARE @ls_stmt				varchar (1000)
DECLARE @l_exec_no				bigint
DECLARE @v_product_structure_id	bigint
DECLARE @v_event_id	            bigint
DECLARE @v_batch_no	            bigint
DECLARE @ps_id_tbl				table (v_product_structure_id bigint)
DECLARE @event_id_tbl			table (v_event_id bigint)

DECLARE @v_prod_seq_ind      smallint

BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate()


EXEC seq_manager.db_get_next_sequence_value_impl 'common','batch_seq', @v_batch_no output

-----------------------------------------------------------------------
--   Create New Product with Status SETUP
-----------------------------------------------------------------------

 INSERT INTO epacube.product_structure
        (  
         product_status_cr_fk
	    ,data_name_fk)
	OUTPUT INSERTED.product_structure_id INTO @ps_id_tbl
 SELECT 
	    (select code_ref_id from epacube.code_ref 
		 where code_type = 'PRODUCT_STATUS'
		 and code = 'ACTIVE') -- hardcode for now... later move to epacube_params
	   ,(select data_name_id from epacube.data_name
		 where name = 'PRODUCT')  

 SELECT top 1 @v_product_structure_id = v_product_structure_id from @ps_id_tbl


-- -----------------------------------------------------------------
-- --   NEW PRODUCT EVENT
-- -----------------------------------------------------------------

	INSERT INTO [synchronizer].[EVENT_DATA]
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]			   
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[BATCH_NO]
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])
--
(SELECT 
        150
       ,(select code_ref_id from epacube.code_ref 
		where code_type = 'ENTITY_CLASS'
		and code = 'PRODUCT')
	   ,utilities.TruncMMDDYYYY (@l_sysdate)
	   ,@v_product_structure_id
	   ,(select data_name_id from epacube.data_name
		 where name = 'PRODUCT')
	   ,'**NEW ITEM**'
	   ,@v_product_structure_id
	   ,1   -- all orgs
	   ,97  --green
	   ,87   ------  ks moved 87 back 83 --prepost   cindy added 87  --submitted
       ,66  --Approved to Post
	   ,71  --product manager
	   ,54
	   ,@v_batch_no
	   ,@l_sysdate
	   ,@in_update_user
       
FROM epacube.code_ref where code_type = 'ENTITY_CLASS'
		and code = 'PRODUCT')  --added fix



--------  MOVED TO DERIVATIONS... DELETE LATER
--------------------------------------------------------------------------------------------------
----------   IF epacube_params  product_seq_id value = 'TRUE'
--------------------------------------------------------------------------------------------------
--------
--------  SET @v_prod_seq_ind =  ( SELECT CASE ISNULL ( EEP.VALUE, 'FALSE' )
--------                                  WHEN 'TRUE' THEN 1
--------                                  ELSE 0
--------                                  END
--------                           FROM EPACUBE.EPACUBE_PARAMS EEP
--------                           WHERE EEP.APPLICATION_SCOPE_FK = 100
--------                           AND   EEP.NAME = 'PRODUCT_ID_SEQ'  )
--------                           
--------  IF @v_prod_seq_ind = 1
--------	 EXEC  synchronizer.EVENT_FACTORY_DERIVE_PRODUCT_ID @v_batch_no
--------



------------------------------------------------------------------------------------------
--   Create epacube.EPACUBE_SEARCH for new entity or product
------------------------------------------------------------------------------------------
--
--INSERT INTO [epacube].[EPACUBE_SEARCH]
--           ([ENTITY_CLASS_CR_FK]
--           ,[EPACUBE_ID] )
--VALUES (
--            10109
--           ,@v_product_structure_id   )
--



-- -----------------------------------------------------------------
-- --   CALL EVENT POSTING
-- -----------------------------------------------------------------


   EXEC SYNCHRONIZER.EVENT_POSTING  @v_batch_no


   UPDATE synchronizer.EVENT_DATA
   SET EVENT_STATUS_CR_FK = 83 
   WHERE DATA_NAME_FK = 149000
   AND   PRODUCT_STRUCTURE_FK = @v_product_structure_id
   

-- -----------------------------------------------------------------
-- --   RETURN NEW PRODUCT ID
-- -----------------------------------------------------------------

SET @out_product_structure_fk = @v_product_structure_id
 print '@v_product_structure_id: ' + cast(@v_product_structure_id as varchar(20))
  print '@out_product_structure_fk: ' + cast(@out_product_structure_fk as varchar(20))
END TRY
BEGIN CATCH

        EXEC exec_monitor.Report_Error_And_Stop_sp
	 		@exec_id = @l_exec_no,
			@user_error_desc = 'SYNCHRONIZER.EVENT_PRODUCT_PM_NEW procedure failed'
        declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;

END CATCH


END










































