﻿












-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing for imports.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        10/19/2008   Initial SQL Version
-- CV	     05/28/2009   ARP Vendor from UI not updating correctly Fix
-- CV        09/23/2009   EPA - 2701 update entity structure if requiered
-- CV        12/14/2009   EPA - 2735 expanded search 1-6 to Varchar 128
-- CV	     12/15/2009   Removed EPA - 2701 code and addded Epa 2733
--							not set entity structure on associations that do not need them
-- CV        01/25/2010   Add event posting for entity tables
-- CV        02/12/2010   Removed left join with results and values view
-- KS		 02/15/2010   Restructured Event Process for efficiency 
--							Reorganized Rule Qualifications ( either #TS table or not persisted at all )
--                        Just so we NEVER have Submitted or Processing Stuck Events 
--							( Unless Program Aborts ) ..  Last statement updates them to 80 
-- KS        06/14/2010   Moved Derivation Event Creation to synchronizer.EVENT_POST_EDITOR_DATA so ONLY called when
--							events come from editors;  Posting of derivaions happen during product loop
-- KS        02/25/2011   For performance reworked Status updates to EVENT_DATA in the beginning of procedure
-- CV	     05/25/2012   Patch for 515 - make event data error table 256 from 64
-- CV	     08/24/2012   Made event data error table varchar(max)
-- CV        09/27/2012   Added a delete statement to clean out the event data rules table.
-- CV        06/27/2013   Added Entity association fk to temp table
-- CV        10/14/2013   Anded check if rule record status cr fk = 1 for completeness.
-- CV        11/22/2013   Added a not exist when inserting to history so we don't do it twice
-- CV        05/14/2014   Adding taxonomy node fk to ts event data table.
-- CV        01/13/2017   Changed Rules type cr fk to come from rules table
-- CV        03/23/2017  Adding lots of new columns to table - Added lookup to data name group ref table
-- CV        04/25/2017  Inserts to new History Header table


CREATE PROCEDURE [synchronizer].[EVENT_POSTING] 
        ( @in_batch_no BIGINT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime


DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING.'
					 + cast(isnull(@in_batch_no, 0) as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;



------            exec sp_executesql 	@ls_stmt  USE TO THROW ERROR

SET @l_sysdate = GETDATE()


------------------------------------------------------------------------------------------
--   Save this code snippet for causing immediate stop
------------------------------------------------------------------------------------------

----            SET @ls_exec = 'STOP CODE HERE'            
----            INSERT INTO COMMON.T_SQL
----                ( TS,  SQL_TEXT )
----            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
----            exec sp_executesql 	@ls_exec;




------------------------------------------------------------------------------------------
--   Update Event Status to Processing ( 88 )
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_DATA
SET EVENT_STATUS_CR_FK = 88
   ,EVENT_PRIORITY_CR_FK = CASE EVENT_SOURCE_CR_FK
                           WHEN 71 THEN 66  -- APPROVE_TO_POST
                           ELSE CASE WHEN ( ISNULL ( IMPORT_BATCH_NO, -999 ) <> @in_batch_no )
                                THEN 66 -- APPROVE_TO_POST
                                ELSE 61 -- AWAITING APPROVAL
                                END
                           END 
   ,EVENT_CONDITION_CR_FK = 97
WHERE 1 = 1
AND   BATCH_NO = @in_batch_no
AND   EVENT_STATUS_CR_FK in (87, 86)  -- READY OR SUBMITTED 



-----UPDATE FOR COMPLETENESS
UPDATE synchronizer.EVENT_DATA
SET EVENT_STATUS_CR_FK = 88
   ,EVENT_PRIORITY_CR_FK = 69
   ,EVENT_CONDITION_CR_FK = 97
,BATCH_NO = @in_batch_no
WHERE 1 = 1
AND   EVENT_STATUS_CR_FK in (80, 86, 87, 88 )  -- PENDING
AND   DATA_NAME_FK IN  (select result_data_name_fk from synchronizer.rules WITH (NOLOCK)
where RECORD_STATUS_CR_FK = 1
AND RULE_TYPE_CR_FK = 309)
						--and rules_structure_fk = (select rules_structure_id 
						--from synchronizer.rules_structure WITH (NOLOCK)
						--where name = 'COMPLETENESS'))
AND   EPACUBE_ID IN    (select epacube_id from synchronizer.EVENT_DATA ed2 WITH (NOLOCK)
						WHERE ED2.BATCH_NO = @in_batch_no)




------------------------------------------------------------------------------
--   Updates From UI..  Will need to revisit this later with Geoff
--						to see if we can get rid of this call but for now
--						This routine also add any Completeness events to the batch
--		This should also auto reject any previous like events that are pending 
----------------------------------------------------------------------------------------


----CINDY TESTING URM

----SET  @v_count = ( SELECT COUNT(1)
----                 FROM synchronizer.EVENT_DATA WITH (NOLOCK)
----                 WHERE BATCH_NO = @in_batch_no
----				 AND   EVENT_STATUS_CR_FK = 88
----				 AND   EVENT_SOURCE_CR_FK = 71 )   -- FROM THE EDITOR

----IF @v_count > 0
----    EXEC synchronizer.EVENT_POST_EDITOR_DATA  @in_batch_no



------------------------------------------------------------------------------------------
--   SET THE PRIOIRTY FOR THE COMPLETENESS EVENTS
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_DATA
  SET EVENT_PRIORITY_CR_FK = 69
     ,EVENT_CONDITION_CR_FK = 97
WHERE EVENT_ID IN ( 
		SELECT EDX.EVENT_ID
		FROM synchronizer.EVENT_DATA EDX WITH (NOLOCK)
		INNER JOIN synchronizer.RULES R WITH (NOLOCK)
		 ON ( R.RESULT_DATA_NAME_FK = EDX.DATA_NAME_FK )
		--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
		-- ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
		-- AND  RS.RULE_TYPE_CR_FK = 309 ) 
		WHERE R.RECORD_STATUS_CR_FK = 1
		AND  R.RULE_TYPE_CR_FK = 309
		AND   EDX.BATCH_NO = @in_batch_no
		AND   EDX.EVENT_STATUS_CR_FK IN ( 88 )  ---- STATUS CONSIDERED TO BE APPROVED OR STILL 88 
		)

-----UPDATE FOR COMPLETENESS
UPDATE synchronizer.EVENT_DATA
SET EVENT_STATUS_CR_FK = 88
  ,EVENT_PRIORITY_CR_FK = 69
   ,EVENT_CONDITION_CR_FK = 97
,BATCH_NO = @in_batch_no
WHERE EVENT_ID IN (
SELECT ED.EVENT_ID
FROM synchronizer.EVENT_DATA ED WITH (NOLOCK)
WHERE    ED.EVENT_STATUS_CR_FK in (80)  -- PENDING
AND ed.data_name_fk in (select result_data_name_fk from synchronizer.rules
where RECORD_STATUS_CR_FK = 1
AND Rule_TYPE_CR_FK = 309 )
						--and rules_structure_fk = (select rules_structure_id 
						--from synchronizer.rules_structure
						--where name = 'COMPLETENESS'))
AND ED.EPACUBE_ID in (select epacube_id from synchronizer.EVENT_DATA ed2
						WHERE ED2.BATCH_NO = @in_batch_no)
)

------------------------------------------------------------------------------------------
--   Create Temp Table for Events
--		This table will help with record locking during processing
--
--		Before posting will check for current value <> new value
--		However no longer care about duplicate events because
--		Processing events within a Job.. and each job will not
--      allow for a duplicate event. ( even UI entries ) 
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA') is not null
	   drop table #TS_EVENT_DATA;
	   
	   
	-- create temp table
	CREATE TABLE #TS_EVENT_DATA(
		EVENT_FK bigint NULL,
		EVENT_TYPE_CR_FK         INT NULL,
		EVENT_ENTITY_CLASS_CR_FK INT NULL,
		EPACUBE_ID               INT NULL,	
		IMPORT_JOB_FK            BIGINT NULL,	
		IMPORT_PACKAGE_FK        BIGINT NULL,
		DATA_NAME_FK			 INT NULL,	
	    PRODUCT_STRUCTURE_FK     BIGINT NULL,
	    ORG_ENTITY_STRUCTURE_FK  BIGINT NULL,
	    ENTITY_STRUCTURE_FK      BIGINT NULL,
		ENTITY_CLASS_CR_FK        INT NULL,
		ENTITY_DATA_NAME_FK       INT NULL,	 
        PARENT_ENTITY_STRUCTURE_FK  INT NULL,
        CHILD_ENTITY_STRUCTURE_FK  INT NULL, 
		CHILD_PRODUCT_STRUCTURE_FK BIGINT NULL,
		ENTITY_ASSOCIATION_FK  INT NULL,  
	    CUST_ENTITY_STRUCTURE_FK BIGINT NULL,
	    SUPL_ENTITY_STRUCTURE_FK BIGINT NULL,	
		PRODUCT_RECIPE_FK        Bigint NULL,				
		DATA_SET_FK			     INT NULL,				
		DATA_TYPE_CR_FK			 INT NULL,	
		ORG_IND                  SMALLINT NULL,
		INSERT_MISSING_VALUES    INT NULL,		
		TABLE_NAME               VARCHAR (128) NULL,					
        NEW_DATA                  VARCHAR(max) NULL,		
        CURRENT_DATA              VARCHAR(max) NULL,
		DATA_VALUE_FK             BIGINT NULL,
		ENTITY_DATA_VALUE_FK      BIGINT NULL,
        NET_VALUE1_NEW            NUMERIC (18,6) NULL,
        NET_VALUE2_NEW            NUMERIC (18,6) NULL,
        NET_VALUE3_NEW            NUMERIC (18,6) NULL,
        NET_VALUE4_NEW            NUMERIC (18,6) NULL,
        NET_VALUE5_NEW            NUMERIC (18,6) NULL,                                
        NET_VALUE6_NEW            NUMERIC (18,6) NULL,                                      
        NET_VALUE1_CUR            NUMERIC (18,6) NULL,
        NET_VALUE2_CUR            NUMERIC (18,6) NULL,
        NET_VALUE3_CUR            NUMERIC (18,6) NULL,
        NET_VALUE4_CUR            NUMERIC (18,6) NULL,
        NET_VALUE5_CUR            NUMERIC (18,6) NULL,                                
        NET_VALUE6_CUR            NUMERIC (18,6) NULL,                                
		PERCENT_CHANGE1			  numeric(18, 6) NULL,
		PERCENT_CHANGE2			  numeric(18, 6) NULL,
		PERCENT_CHANGE3			  numeric(18, 6) NULL,
		PERCENT_CHANGE4			  numeric(18, 6) NULL,
		PERCENT_CHANGE5			  numeric(18, 6) NULL,
		PERCENT_CHANGE6			  numeric(18, 6) NULL,    
		DATA_LEVEL				  VARCHAR (64) NULL,	
        VALUE_UOM_CODE_FK         INT NULL, 
        ASSOC_UOM_CODE_FK         INT NULL,
        VALUES_SHEET_NAME           VARCHAR(128), 
		VALUES_SHEET_DATE         VARCHAR(128),
		UOM_CONVERSION_FACTOR     NUMERIC (18,6) NULL,						
		END_DATE_NEW              DATETIME NULL,				
        END_DATE_CUR              DATETIME NULL,   		
		EFFECTIVE_DATE_CUR        DATETIME NULL,
		EVENT_EFFECTIVE_DATE	  DATETIME NULL,
		VALUE_EFFECTIVE_DATE	  DATETIME NULL,	
        RELATED_EVENT_FK		  BIGINT NULL,
        RULES_FK			      BIGINT NULL,
		EVENT_PRIORITY_CR_FK      INT NOT NULL,		        
		EVENT_ACTION_CR_FK        INT NOT NULL,
		EVENT_STATUS_CR_FK        INT NOT NULL,								        
		EVENT_CONDITION_CR_FK     INT NOT NULL,
		EVENT_SOURCE_CR_FK        INT NOT NULL,				
		RESULT_TYPE_CR_FK	      INT NULL,
		SINGLE_ASSOC_IND          SMALLINT NULL,	
		ASSOC_PARENT_CHILD		  VARCHAR(128),				
		TABLE_ID_FK               BIGINT NULL,
		BATCH_NO				  BIGINT NULL,
		IMPORT_BATCH_NO           BIGINT NULL,
        JOB_CLASS_FK			  INT NULL,
        IMPORT_DATE               DATETIME NULL,
        IMPORT_FILENAME           VARCHAR(64) NULL,
        STG_RECORD_FK             BIGINT NULL,		
		RECORD_STATUS_CR_FK       INT NULL,
        UPDATE_TIMESTAMP		  DATETIME NULL,
        UPDATE_USER		          VARCHAR(64) NULL,
		PARENT_STRUCTURE_FK          BIGINT NULL,
		[ZONE_ENTITY_STRUCTURE_FK] BIGINT NULL,
        [DEPT_ENTITY_STRUCTURE_FK] BIGINT NULL,
        [VENDOR_ENTITY_STRUCTURE_FK] BIGINT NULL,
		assoc_entity_structure_fk  BIGINT NULL,
        [PROD_DATA_VALUE_FK] BIGINT NULL,
        [CUST_DATA_VALUE_FK] BIGINT NULL,
		CHAIN_ID_DATA_VALUE_FK BIGINT NULL,
		[DEPT_ENTITY_DATA_VALUE_FK] BIGINT NULL,
        RESULT_DATA_VALUE_FK BIGINT NULL,
        PRECEDENCE INT NULL,
		ZONE_TYPE_CR_FK int,
		SEARCH1 VARCHAR(128),
		CO_ENTITY_STRUCTURE_FK bigint NULL,
		PACK int NULL
		 )



--------------------------------------------------------------------------------------------
--  CREATE INDEXES 
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSED_EF ON #TS_EVENT_DATA 
	(EVENT_FK ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSED_EPA ON #TS_EVENT_DATA 
	(EPACUBE_ID ASC)
	INCLUDE ( EVENT_FK )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX IDX_TSED_EF_ETCRF ON #TS_EVENT_DATA 
	(EVENT_FK ASC, EVENT_TYPE_CR_FK ASC)
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX IDX_TSED_DNF_OESF_ESF_IND ON #TS_EVENT_DATA 
	(
		DATA_NAME_FK ASC,
		ORG_ENTITY_STRUCTURE_FK ASC,
		ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( NEW_DATA, EVENT_FK )
		WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX IDX_TESSRCH ON #TS_EVENT_DATA 
	(
		DATA_NAME_FK ASC,
		PRODUCT_STRUCTURE_FK ASC,
		ORG_ENTITY_STRUCTURE_FK ASC,
		ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( NEW_DATA, EVENT_FK )
		WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX TSED_20100120_EVENT_POST_DATA ON #TS_EVENT_DATA 
	(
		EVENT_STATUS_CR_FK ASC,
		EVENT_CONDITION_CR_FK ASC,
		EVENT_ACTION_CR_FK ASC,
		EVENT_TYPE_CR_FK ASC,		
		TABLE_NAME ASC,
		TABLE_ID_FK ASC
	)
	INCLUDE ( [NEW_DATA], [RECORD_STATUS_CR_FK], [UPDATE_TIMESTAMP], [EVENT_FK] )
		WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA_ERRORS') is not null
	   drop table #TS_EVENT_DATA_ERRORS
	   
	   
	-- create temp table
CREATE TABLE #TS_EVENT_DATA_ERRORS(
	TS_EVENT_DATA_ERRORS_ID bigint IDENTITY(1000,1) NOT NULL,
	EVENT_FK bigint NULL,
	RULE_FK BIGint NULL,
	EVENT_CONDITION_CR_FK int NULL,
	DATA_NAME_FK int NULL,
	ERROR_NAME varchar(64) NULL,
	DATA_NAME varchar(64) NULL,
	ERROR_DATA1 varchar(Max) NULL,
	ERROR_DATA2 varchar(Max) NULL,
	ERROR_DATA3 varchar(Max) NULL,
	ERROR_DATA4 varchar(Max) NULL,
	ERROR_DATA5 varchar(Max) NULL,
	ERROR_DATA6 varchar(Max) NULL,
    UPDATE_TIMESTAMP DATETIME NULL
		 )


--------------------------------------------------------------------------------------------
--  CREATE INDEXES 
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSEDE_TEDEID ON #TS_EVENT_DATA_ERRORS 
	(TS_EVENT_DATA_ERRORS_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSEDE_EF_TEDEID ON #TS_EVENT_DATA_ERRORS
	(EVENT_FK ASC)
	INCLUDE ( TS_EVENT_DATA_ERRORS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX IDX_TSEDE_EF_ECCRF ON #TS_EVENT_DATA_ERRORS 
	(EVENT_FK ASC, EVENT_CONDITION_CR_FK ASC)
    INCLUDE ( TS_EVENT_DATA_ERRORS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA_RULES') is not null
	   drop table #TS_EVENT_DATA_RULES
	   
	   
	-- create temp table
CREATE TABLE #TS_EVENT_DATA_RULES(
	[TS_EVENT_RULES_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[EVENT_FK] [bigint] NULL,
	[RULES_FK] [bigint] NULL,
	[RESULT_DATA_NAME_FK] [int] NULL,
	[RESULT_TYPE_CR_FK] [int] NULL,
	[RESULT_EFFECTIVE_DATE] [datetime] NULL,
	[RESULT_RANK] [smallint] NULL,
	[RESULT] [numeric](18, 6) NULL,
	[BASIS1] [numeric](18, 6) NULL,
	[BASIS2] [numeric](18, 6) NULL,
	[OPERAND] [numeric](18, 6) NULL,
	[OPERATION] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BASIS1_DN_FK] [int] NULL,
	[BASIS2_DN_FK] [int] NULL,
	[RULES_ACTION_OPERATION_FK] [int] NULL,
	[RULES_EFFECTIVE_DATE] [DATETIME] NULL,
	[RULE_PRECEDENCE] [smallint] NULL,
	[SCHED_PRECEDENCE] [smallint] NULL,
	[STRUC_PRECEDENCE] [smallint] NULL,
    [RESULT_MTX_ACTION_CR_FK] [int] NULL,
    [SCHED_MTX_ACTION_CR_FK] [int] NULL,
    [UPDATE_TIMESTAMP] [datetime] NULL     
	 ) 			


--------------------------------------------------------------------------------------------
--  CREATE INDEXES 
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSEDR_TEDRID ON #TS_EVENT_DATA_RULES 
	(TS_EVENT_RULES_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSEDR_EF_TEDRID ON #TS_EVENT_DATA_RULES
	(EVENT_FK ASC)
	INCLUDE ( TS_EVENT_RULES_ID, RULES_FK  )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSEDR_EF_ECCRF ON #TS_EVENT_DATA_RULES 
	(EVENT_FK ASC, RESULT_RANK ASC )
    INCLUDE ( TS_EVENT_RULES_ID, RULES_FK, RESULT_DATA_NAME_FK  )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSEDR_EF_SCHP_RP_RF ON #TS_EVENT_DATA_RULES 
	(EVENT_FK ASC, SCHED_PRECEDENCE ASC, RULE_PRECEDENCE ASC, RULES_FK DESC )
    INCLUDE ( TS_EVENT_RULES_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


------------------------------------------------------------------------------------------
---  LOOP THROUGH JOB_FK, PRIORITY,DATA_SET_FK for events within the BATCH_NO
---
---		OMIT COMPLETENESS EVENTS FOR RIGHT NOW
------------------------------------------------------------------------------------------


      DECLARE 
              @vm$import_job_fk          bigint,   
              @vm$event_priority_cr_fk   int,      
              @vm$table_name             VARCHAR(128), 
              @vm$event_type_cr_fk       INT



      DECLARE  cur_v_event cursor local for

	  	 SELECT 
			        A.import_job_fk,
			        A.event_priority_cr_fk,
					A.table_name,
					A.event_type_cr_fk
				  FROM (
				SELECT DISTINCT
				       ISNULL ( ED.import_job_fk, -999 ) import_job_fk
				      ,ISNULL ( ED.event_priority_cr_fk, -999 ) event_priority_cr_fk
				      ,R.TABLE_NAME AS table_name	
				      ,ISNULL ( ED.event_type_cr_fk, 711 ) event_type_cr_fk
				FROM synchronizer.EVENT_DATA ED WITH (NOLOCK)
				 inner join epacube.DATA_NAME_GROUP_REFS R ON R.Data_Name_FK = ED.DATA_NAME_FK
				 left join epacube.code_ref cr on cr.code_type = 'DATA_LEVEL'
				 and isNULL(cr.code,999) = isNULL(ed.Data_level,999)
				 and ISNULL(r.data_level_cr_fk,999) = ISNULL(cr.code_ref_id,999)
						
				WHERE ED.batch_no =   @in_batch_no
				AND   ED.event_status_cr_fk = 88
				AND   ED.EVENT_PRIORITY_CR_FK <> 69  
              ) A
			  ORDER BY A.event_priority_cr_fk ASC, A.import_job_fk DESC, A.event_type_cr_fk ASC, A.table_name ASC


			 ----SELECT 
			 ----       A.import_job_fk,
			 ----       A.event_priority_cr_fk,
				----	A.table_name,
				----	A.event_type_cr_fk
			 ---- FROM (
				----SELECT DISTINCT
				----       ISNULL ( ED.import_job_fk, -999 ) import_job_fk
				----      ,ISNULL ( ED.event_priority_cr_fk, -999 ) event_priority_cr_fk
				----      ,ds.TABLE_NAME AS table_name	
				----      ,ISNULL ( ED.event_type_cr_fk, 711 ) event_type_cr_fk
				----FROM synchronizer.EVENT_DATA ED WITH (NOLOCK)
				----INNER JOIN epacube.DATA_NAME dn WITH (NOLOCK) 
				----   ON ( dn.data_name_id = ED.data_name_fk )
				----INNER JOIN epacube.DATA_SET ds WITH (NOLOCK)
				----   ON ( ds.data_set_id = dn.data_set_fk ) 
				----WHERE ED.batch_no = @in_batch_no
				----AND   ED.event_status_cr_fk = 88
				----AND   ED.EVENT_PRIORITY_CR_FK <> 69  
    ----          ) A
			 ---- ORDER BY A.event_priority_cr_fk ASC, A.import_job_fk DESC, A.event_type_cr_fk ASC, A.table_name ASC

              
      
        OPEN cur_v_event;
        FETCH NEXT FROM cur_v_event INTO @vm$import_job_fk
										,@vm$event_priority_cr_fk
										,@vm$table_name 										
										,@vm$event_type_cr_fk



------------------------------------------------------------------------------------------
---  LOOP THROUGH IMPORT, PRIORITY, TABLE
------------------------------------------------------------------------------------------


         WHILE @@FETCH_STATUS = 0 
         BEGIN
                

------------------------------------------------------------------------------------------
---  LOOP THROUGH DATA_NAMES IF ROWS > 250000
------------------------------------------------------------------------------------------


		SET @v_count = (   SELECT COUNT(1)
						   FROM  synchronizer.EVENT_DATA ED WITH (NOLOCK)
							inner join epacube.DATA_NAME_GROUP_REFS R ON R.Data_Name_FK = ED.DATA_NAME_FK
			 
							WHERE 1 = 1 
							AND  ED.EVENT_STATUS_CR_FK = 88
							AND  ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
							AND  ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@vm$import_job_fk, -999 )
							AND  ISNULL ( ED.event_priority_cr_fk, -999 ) = isnull (@vm$event_priority_cr_fk, -999)
							AND  ISNULL ( R.table_name, 'NULL DATA' )  = ISNULL ( @vm$table_name, 'NULL DATA' )
							--------------- omit Completeness Events until the very end
				            AND   ED.EVENT_PRIORITY_CR_FK <> 69  
									)

      DECLARE  @vm$data_name_fk  bigint			   

      DECLARE  cur_v_perf_group cursor local for
			 SELECT DISTINCT
			        A.data_name_fk			         
			  FROM (
				SELECT 
				       CASE WHEN @v_count > 250000
				       THEN ed.data_name_fk
				       ELSE -999
				       END AS data_name_fk
			   FROM  synchronizer.EVENT_DATA ED WITH (NOLOCK)
			   inner join epacube.DATA_NAME_GROUP_REFS R ON R.Data_Name_FK = ED.DATA_NAME_FK
				 --and  ISNULL(R.data_level_cr_fk,999)  =  (select ISNULL(CODE_REF_ID,999) from epacube.CODE_REF where ed.DATA_LEVEL = CODE and CODE_TYPE = 'DATA_LEVEL') 
				 --INNER JOIN epacube.DATA_NAME dn WITH (NOLOCK) 
				--   ON ( dn.data_name_id = ED.data_name_fk )
				--INNER JOIN epacube.DATA_SET ds WITH (NOLOCK)
				--   ON ( ds.data_set_id = dn.data_set_fk ) 
				WHERE 1 = 1 
				AND  ED.EVENT_STATUS_CR_FK = 88
				AND ISNULL ( ED.BATCH_NO, -999 ) = ISNULL ( @in_batch_no, -999 )
				AND ISNULL ( ED.import_job_fk, -999 ) = ISNULL (@vm$import_job_fk, -999 )
				AND isnull ( ED.event_priority_cr_fk, -999 ) = isnull (@vm$event_priority_cr_fk, -999)
				AND ISNULL ( R.table_name, 'NULL DATA' )  = ISNULL (@vm$table_name, 'NULL DATA' )
				--------------- omit Completeness Events until the very end
				AND   ED.EVENT_PRIORITY_CR_FK <> 69  
				GROUP BY ed.data_name_fk
              ) A
			  ORDER BY  A.data_name_fk ASC

                    
    OPEN cur_v_perf_group;
    FETCH NEXT FROM cur_v_perf_group INTO   @vm$data_name_fk 
												


------------------------------------------------------------------------------------------
---  LOOP THROUGH DATA NAMES IF MORE THAT 900000 ROWS 
---
---	 WOULD LIKE TO VISIT RUNNING THESE IN PARALLEL IN FUTURE RELEASE ( DIFFERENT TABLES )
---
------------------------------------------------------------------------------------------



         WHILE @@FETCH_STATUS = 0 
         BEGIN

         
			IF @vm$event_type_cr_fk IN ( 150, 155, 156 )
			   EXEC synchronizer.EVENT_POSTING_PRODUCT 
					NULL, 1, @in_batch_no, 
					@vm$import_job_fk, @vm$event_priority_cr_fk, @vm$event_type_cr_fk, @vm$table_name
					,@vm$data_name_fk 
			   
			IF @vm$event_type_cr_fk IN ( 153 )
			   EXEC synchronizer.EVENT_POSTING_PRODUCT_TAX
					NULL, 1, @in_batch_no, 
					@vm$import_job_fk, @vm$event_priority_cr_fk, @vm$event_type_cr_fk, @vm$table_name
					,@vm$data_name_fk 
			
			IF @vm$event_type_cr_fk IN ( 151, 152, 158 )
			   EXEC synchronizer.EVENT_POSTING_PRODUCT_151_152
					NULL, 1, @in_batch_no, 
					@vm$import_job_fk, @vm$event_priority_cr_fk, @vm$event_type_cr_fk, @vm$table_name
					,@vm$data_name_fk 
		   						   			
			IF @vm$event_type_cr_fk IN ( 154, 160 )
			   EXEC synchronizer.EVENT_POSTING_ENTITY
					NULL, 1, @in_batch_no, 
					@vm$import_job_fk, @vm$event_priority_cr_fk, @vm$event_type_cr_fk, @vm$table_name
					,@vm$data_name_fk 


		FETCH NEXT FROM cur_v_perf_group INTO   @vm$data_name_fk 

		 END --  cur_v_perf_group LOOP
		 CLOSE cur_v_perf_group
		 DEALLOCATE cur_v_perf_group 



    FETCH NEXT FROM cur_v_event INTO @vm$import_job_fk
									,@vm$event_priority_cr_fk
									,@vm$table_name 
								    ,@vm$event_type_cr_fk									


     END --cur_v_event LOOP
     CLOSE cur_v_event;
	 DEALLOCATE cur_v_event; 


--------------------------------------------------------------------------------------------------
---    END OF PERFORMACE LOOP   /  JOB, PRIORITY, TABLE NAME, EVENT TYPE
--------------------------------------------------------------------------------------------------

     


------------------------------------------------------------------------------------------
---  LOOP THROUGH DATA_NAMES FOR COMPLETENESS   -- PRODUCT ONLY
------------------------------------------------------------------------------------------



      DECLARE  cur_v_complete cursor local for
			 SELECT 
			        A.import_job_fk,
			        A.event_priority_cr_fk,
					A.table_name,
					A.event_type_cr_fk
			  FROM (
				SELECT DISTINCT
				       ISNULL ( ED.import_job_fk, -999 ) import_job_fk
				      ,ISNULL ( ED.event_priority_cr_fk, -999 ) event_priority_cr_fk
				      ,ds.TABLE_NAME AS table_name	
				      ,ISNULL ( ED.event_type_cr_fk, 711 ) event_type_cr_fk
				FROM synchronizer.EVENT_DATA ED WITH (NOLOCK)
				INNER JOIN epacube.DATA_NAME dn WITH (NOLOCK) 
				   ON ( dn.data_name_id = ED.data_name_fk )
				INNER JOIN epacube.DATA_SET ds WITH (NOLOCK)
				   ON ( ds.data_set_id = dn.data_set_fk ) 
				WHERE ED.batch_no = @in_batch_no
				AND   ED.EVENT_STATUS_CR_FK IN ( 81,83,84,88 )  ---- STATUS CONSIDERED TO BE APPROVED OR STILL 88  
				--------------  Cindy... Please test more... why would be include events already approved ????
				--------------- Completeness Events ONLY saved until the very end
				AND   ED.EVENT_PRIORITY_CR_FK = 69  
              ) A
			  ORDER BY A.event_priority_cr_fk ASC, A.import_job_fk DESC, A.event_type_cr_fk ASC, A.table_name ASC

              
      
        OPEN cur_v_complete;
        FETCH NEXT FROM cur_v_complete INTO  @vm$import_job_fk
									    	,@vm$event_priority_cr_fk
									    	,@vm$table_name 										
										    ,@vm$event_type_cr_fk



------------------------------------------------------------------------------------------
---  LOOP THROUGH IMPORT, PRIORITY, TABLE
--		DERIVATIONS CURRENTLY ONLY PRODUCT LEVEL EVENTS
------------------------------------------------------------------------------------------


         WHILE @@FETCH_STATUS = 0 
         BEGIN                
         
			IF @vm$event_type_cr_fk IN ( 150, 155, 156 )
			   EXEC synchronizer.EVENT_POSTING_PRODUCT 
					NULL, 1, @in_batch_no, 
					@vm$import_job_fk, @vm$event_priority_cr_fk, @vm$event_type_cr_fk, @vm$table_name
					,-999 
			   

		FETCH NEXT FROM cur_v_complete INTO @vm$import_job_fk
										   ,@vm$event_priority_cr_fk
										   ,@vm$table_name 
									       ,@vm$event_type_cr_fk									


		 END --cur_v_event LOOP
		 CLOSE cur_v_complete;
		 DEALLOCATE cur_v_complete;  


--------------------------------------------------------------------------------------------
--  DROP TEMP TABLES
--------------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA') is not null
	   drop table #TS_EVENT_DATA;

--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA_ERRORS') is not null
	   drop table #TS_EVENT_DATA_ERRORS


--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA_RULES') is not null
	   drop table #TS_EVENT_DATA_RULES


--------------------------------------------------------------------------------------------
--New history Table
--------------------------------------------------------------------------------------------

IF object_id('tempdb..##HISTORY_EVENT') is not null
Drop Table ##HISTORY_EVENT

Select ed.event_id
into ##HISTORY_EVENT
 FROM synchronizer.event_data ed WITH (NOLOCK)
		INNER JOIN epacube.data_name dn WITH (NOLOCK)
		  ON ( dn.data_name_id = ed.data_name_fk )
		INNER JOIN epacube.data_set ds WITH (NOLOCK)
		  ON ( ds.data_set_id = dn.data_set_fk )
		WHERE ed.batch_no = @in_batch_no
		AND   (    ed.event_status_cr_fk IN ( 81 )  --- approved 
			OR    (     ed.event_status_cr_fk = 82   -- rejected
				   AND  ed.event_source_cr_fk = 79   -- host import
				  )
			OR    (     ed.event_status_cr_fk = 83
				   AND  ed.event_status_cr_fk <> 56  --- and not a no change
				  )				  
			  )
----added so when completeness rules applie we are not inserting the same event twice.
			   AND   NOT EXISTS
	            ( SELECT 1 
	              FROM synchronizer.EVENT_DATA_HISTORY_HEADER edh WITH (NOLOCK)
	              WHERE edh.IMPORT_JOB_FK = ed.IMPORT_JOB_FK
				  AND edh.PRODUCT_STRUCTURE_FK = ed.PARENT_STRUCTURE_FK
				  and edh.ENTITY_STRUCTURE_FK = ed.ENTITY_STRUCTURE_FK
				  and edh.ORG_ENTITY_STRUCTURE_FK = ed.ORG_ENTITY_STRUCTURE_FK
				  and edh.ZONE_ENTITY_STRUCTURE_FK = ed.ZONE_ENTITY_STRUCTURE_FK
				  and edh.CUST_ENTITY_STRUCTURE_FK = ed.CUST_ENTITY_STRUCTURE_FK
				  and edh.COMP_ENTITY_STRUCTURE_FK = ed.comp_entity_structure_fk
                  and edh.DEPT_ENTITY_STRUCTURE_FK = ed.DEPT_ENTITY_STRUCTURE_FK
				  and edh.VENDOR_ENTITY_STRUCTURE_FK = ed.VENDOR_ENTITY_STRUCTURE_FK
				  and edh.CO_ENTITY_STRUCTURE_FK = ed.ASSOC_ENTITY_STRUCTURE_FK )


INSERT INTO [synchronizer].[EVENT_DATA_HISTORY_HEADER]
           ([EVENT_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[CUST_ENTITY_STRUCTURE_FK]
           ,[COMP_ENTITY_STRUCTURE_FK]
           ,[ZONE_ENTITY_STRUCTURE_FK]
           ,[DEPT_ENTITY_STRUCTURE_FK]
           ,[VENDOR_ENTITY_STRUCTURE_FK]
           ,[CO_ENTITY_STRUCTURE_FK]
           ,[PARENT_STRUCTURE_FK]
           ,[CHAIN_ID_DATA_VALUE_FK]
           ,[PROD_DATA_VALUE_FK]
           ,[ASSOC_ENTITY_STRUCTURE_FK]
		   ,IMPORT_JOB_FK
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
   SELECT  ed.EVENT_ID
           ,ed.EVENT_EFFECTIVE_DATE
           ,ed.EPACUBE_ID
           ,ed.PRODUCT_STRUCTURE_FK
           ,ed.ENTITY_STRUCTURE_FK
           ,ed.ORG_ENTITY_STRUCTURE_FK
           ,ed.CUST_ENTITY_STRUCTURE_FK
           ,ed.COMP_ENTITY_STRUCTURE_FK
           ,ed.ZONE_ENTITY_STRUCTURE_FK
           ,ed.DEPT_ENTITY_STRUCTURE_FK
           ,ed.VENDOR_ENTITY_STRUCTURE_FK
           ,ed.CO_ENTITY_STRUCTURE_FK
           ,ed.PARENT_STRUCTURE_FK
           ,ed.CHAIN_ID_DATA_VALUE_FK
           ,ed.PROD_DATA_VALUE_FK
           ,ed.ASSOC_ENTITY_STRUCTURE_FK
		   ,ed.IMPORT_JOB_FK
           ,ed.RECORD_STATUS_CR_FK
           ,ed.UPDATE_TIMESTAMP
           ,ed.UPDATE_USER
	 FROM ##HISTORY_EVENT H
	 inner join synchronizer.event_data ed on ed.event_id = H.event_id

	 
INSERT INTO [synchronizer].[EVENT_DATA_HISTORY_DETAILS]
           ([EVENT_DATA_HISTORY_HEADER_FK]
           ,[EVENT_ID]
           ,[EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[ENTITY_CLASS_CR_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[LEVEL_SEQ]
           ,[ASSOC_PARENT_CHILD]
           ,[NEW_DATA]
           ,[CURRENT_DATA]
           ,[NET_VALUE1_NEW]
           ,[NET_VALUE1_CUR]
           ,[PERCENT_CHANGE1]
           ,[MARGIN_PERCENT]
           ,[MARGIN_DOLLARS]
           ,[VALUES_SHEET_NAME]
           ,[VALUES_SHEET_DATE]
           ,[VALUE_UOM_CODE_FK]
           ,[ASSOC_UOM_CODE_FK]
           ,[UOM_CONVERSION_FACTOR]
           ,[DATA_VALUE_FK]
           ,[ENTITY_DATA_VALUE_FK]
           ,[VALUE_EFFECTIVE_DATE]
           ,[EFFECTIVE_DATE_CUR]
           ,[END_DATE_NEW]
           ,[END_DATE_CUR]
           ,[DATA_LEVEL]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[EVENT_REASON_CR_FK]
           ,[RESULT_TYPE_CR_FK]
           ,[CUR_RESULT_TYPE_CR_FK]
           ,[SINGLE_ASSOC_IND]
           ,[AUDIT_FILENAME]
           ,[JOB_CLASS_FK]
           ,[CALC_JOB_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[IMPORT_BATCH_NO]
           ,[BATCH_NO]
           ,[STG_RECORD_FK]
           ,[TABLE_ID_FK]
           ,[RULES_FK]
           ,[PREV_RULES_FK]
           ,[RELATED_EVENT_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
           ,[CUST_DATA_VALUE_FK]
           ,[DEPT_ENTITY_DATA_VALUE_FK]
           ,[RESULT_DATA_VALUE_FK]
           ,[PRECEDENCE]
           ,[ZONE_TYPE_CR_FK]
           ,[SEARCH1])
		   SELECT 
		   hh.EVENT_DATA_HISTORY_HEADER_ID
           ,hh.EVENT_FK
           ,[EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,ed.EVENT_EFFECTIVE_DATE
           ,ed.EPACUBE_ID
           ,[DATA_NAME_FK]
           ,[ENTITY_CLASS_CR_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[LEVEL_SEQ]
           ,[ASSOC_PARENT_CHILD]
           ,[NEW_DATA]
           ,[CURRENT_DATA]
           ,[NET_VALUE1_NEW]
           ,[NET_VALUE1_CUR]
           ,[PERCENT_CHANGE1]
           ,[MARGIN_PERCENT]
           ,[MARGIN_DOLLARS]
           ,[VALUES_SHEET_NAME]
           ,[VALUES_SHEET_DATE]
           ,[VALUE_UOM_CODE_FK]
           ,[ASSOC_UOM_CODE_FK]
           ,[UOM_CONVERSION_FACTOR]
           ,[DATA_VALUE_FK]
           ,[ENTITY_DATA_VALUE_FK]
           ,[VALUE_EFFECTIVE_DATE]
           ,[EFFECTIVE_DATE_CUR]
           ,[END_DATE_NEW]
           ,[END_DATE_CUR]
           ,[DATA_LEVEL]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[EVENT_REASON_CR_FK]
           ,[RESULT_TYPE_CR_FK]
           ,[CUR_RESULT_TYPE_CR_FK]
           ,[SINGLE_ASSOC_IND]
           ,[AUDIT_FILENAME]
           ,[JOB_CLASS_FK]
           ,[CALC_JOB_FK]
           ,ed.IMPORT_JOB_FK
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[IMPORT_BATCH_NO]
           ,[BATCH_NO]
           ,[STG_RECORD_FK]
           ,[TABLE_ID_FK]
           ,[RULES_FK]
           ,[PREV_RULES_FK]
           ,[RELATED_EVENT_FK]
           ,ed.RECORD_STATUS_CR_FK
           ,ed.UPDATE_TIMESTAMP
           ,ed.UPDATE_USER
           ,[CUST_DATA_VALUE_FK]
           ,[DEPT_ENTITY_DATA_VALUE_FK]
           ,[RESULT_DATA_VALUE_FK]
           ,[PRECEDENCE]
           ,[ZONE_TYPE_CR_FK]
           ,[SEARCH1]
		   	 FROM ##HISTORY_EVENT H
	 inner join synchronizer.event_data ed on ed.event_id = H.event_id
	 inner join synchronizer.event_data_history_header hh on hh.event_fk = ed.event_id
	 and H.event_id = hh.event_fk


		
----INSERT INTO [synchronizer].[EVENT_DATA_HISTORY]
----           ([EVENT_TYPE_CR_FK]
----           ,[EVENT_ENTITY_CLASS_CR_FK]
----           ,[EVENT_EFFECTIVE_DATE]
----           ,[EPACUBE_ID]
----           ,[DATA_NAME_FK]
----           ,[PRODUCT_STRUCTURE_FK]
----           ,[ORG_ENTITY_STRUCTURE_FK]
----           ,[ENTITY_CLASS_CR_FK]
----           ,[ENTITY_DATA_NAME_FK]
----           ,[ENTITY_STRUCTURE_FK]
----           ,[LEVEL_SEQ]
----           ,[PARENT_STRUCTURE_FK]
----           ,[ASSOC_PARENT_CHILD]
----           ,[NEW_DATA]
----           ,[CURRENT_DATA]
----           ,[NET_VALUE1_NEW]
----           ,[NET_VALUE2_NEW]
----           ,[NET_VALUE3_NEW]
----           ,[NET_VALUE4_NEW]
----           ,[NET_VALUE5_NEW]
----           ,[NET_VALUE6_NEW]
----           ,[NET_VALUE1_CUR]
----           ,[NET_VALUE2_CUR]
----           ,[NET_VALUE3_CUR]
----           ,[NET_VALUE4_CUR]
----           ,[NET_VALUE5_CUR]
----           ,[NET_VALUE6_CUR]
----           ,[PERCENT_CHANGE1]
----           ,[PERCENT_CHANGE2]
----           ,[PERCENT_CHANGE3]
----           ,[PERCENT_CHANGE4]
----           ,[PERCENT_CHANGE5]
----           ,[PERCENT_CHANGE6]
----		   ,VALUES_SHEET_NAME 
----           ,[MARGIN_PERCENT]
----           ,[MARGIN_DOLLARS]
----           ,[VALUE_UOM_CODE_FK]
----           ,[ASSOC_UOM_CODE_FK]
----           ,[UOM_CONVERSION_FACTOR]
----           ,[DATA_VALUE_FK]
----           ,[ENTITY_DATA_VALUE_FK]
----           ,[VALUE_EFFECTIVE_DATE]
----           ,[EFFECTIVE_DATE_CUR]
----           ,[END_DATE_NEW]
----           ,[END_DATE_CUR]
----           ,[DATA_LEVEL]
----           ,[SEARCH1]
----           --,[SEARCH3]
----           --,[SEARCH4]
----           --,[SEARCH5]
----           --,[SEARCH6]
----           ,[EVENT_CONDITION_CR_FK]
----           ,[EVENT_STATUS_CR_FK]
----           ,[EVENT_PRIORITY_CR_FK]
----           ,[EVENT_SOURCE_CR_FK]
----           ,[EVENT_ACTION_CR_FK]
----           ,[EVENT_REASON_CR_FK]
----           ,[RESULT_TYPE_CR_FK]
----           ,[CUR_RESULT_TYPE_CR_FK]
----           ,[SINGLE_ASSOC_IND]
----           ,[AUDIT_FILENAME]
----           ,[JOB_CLASS_FK]
----           ,[CALC_JOB_FK]
----           ,[IMPORT_JOB_FK]
----           ,[IMPORT_DATE]
----           ,[IMPORT_PACKAGE_FK]
----           ,[IMPORT_FILENAME]
----           ,[IMPORT_BATCH_NO]
----           ,[BATCH_NO]
----           ,[STG_RECORD_FK]
----           ,[TABLE_ID_FK]
----           ,[RULES_FK]
----           ,[PREV_RULES_FK]
----           ,[RELATED_EVENT_FK]
----           ,[RECORD_STATUS_CR_FK]
----           ,[UPDATE_TIMESTAMP]
----           ,[UPDATE_USER] )
----	SELECT 
----		   ED.EVENT_TYPE_CR_FK
----		  ,ED.EVENT_ENTITY_CLASS_CR_FK
----		  ,ED.EVENT_EFFECTIVE_DATE
----		  ,ED.EPACUBE_ID
----		  ,ED.DATA_NAME_FK
----		  ,ED.PRODUCT_STRUCTURE_FK
----		  ,ED.ORG_ENTITY_STRUCTURE_FK
----		  ,ED.ENTITY_CLASS_CR_FK
----		  ,ED.ENTITY_DATA_NAME_FK
----		  ,ED.ENTITY_STRUCTURE_FK
----		  ,ED.LEVEL_SEQ
----		  ,ED.PARENT_STRUCTURE_FK
----		  ,ED.ASSOC_PARENT_CHILD
----		  ,ED.NEW_DATA
----		  ,ED.CURRENT_DATA
----		  ,ED.NET_VALUE1_NEW
----		  ,ED.NET_VALUE2_NEW
----		  ,ED.NET_VALUE3_NEW
----		  ,ED.NET_VALUE4_NEW
----		  ,ED.NET_VALUE5_NEW
----		  ,ED.NET_VALUE6_NEW
----		  ,ED.NET_VALUE1_CUR
----		  ,ED.NET_VALUE2_CUR
----		  ,ED.NET_VALUE3_CUR
----		  ,ED.NET_VALUE4_CUR
----		  ,ED.NET_VALUE5_CUR
----		  ,ED.NET_VALUE6_CUR
----		  ,ED.PERCENT_CHANGE1
----		  ,ED.PERCENT_CHANGE2
----		  ,ED.PERCENT_CHANGE3
----		  ,ED.PERCENT_CHANGE4
----		  ,ED.PERCENT_CHANGE5
----		  ,ED.PERCENT_CHANGE6
----		  ,ED.VALUES_SHEET_NAME 
----		  ,ED.MARGIN_PERCENT
----		  ,ED.MARGIN_DOLLARS
----		  ,ED.VALUE_UOM_CODE_FK
----		  ,ED.ASSOC_UOM_CODE_FK
----		  ,ED.UOM_CONVERSION_FACTOR
----		  ,ED.DATA_VALUE_FK
----		  ,ED.ENTITY_DATA_VALUE_FK
----		  ,ED.VALUE_EFFECTIVE_DATE
----		  ,ED.EFFECTIVE_DATE_CUR
----		  ,ED.END_DATE_NEW
----		  ,ED.END_DATE_CUR
----		  ,ED.DATA_LEVEL
----		  ,ED.SEARCH1
----		  --,ED.SEARCH3
----		  --,ED.SEARCH4
----		  --,ED.SEARCH5
----		  --,ED.SEARCH6
----		  ,ED.EVENT_CONDITION_CR_FK
----		  ,ED.EVENT_STATUS_CR_FK
----		  ,ED.EVENT_PRIORITY_CR_FK
----		  ,ED.EVENT_SOURCE_CR_FK
----		  ,ED.EVENT_ACTION_CR_FK
----		  ,ED.EVENT_REASON_CR_FK
----		  ,ED.RESULT_TYPE_CR_FK
----		  ,ED.CUR_RESULT_TYPE_CR_FK
----		  ,ED.SINGLE_ASSOC_IND
----		  ,ED.AUDIT_FILENAME
----		  ,ED.JOB_CLASS_FK
----		  ,ED.CALC_JOB_FK
----		  ,ED.IMPORT_JOB_FK
----		  ,ED.IMPORT_DATE
----		  ,ED.IMPORT_PACKAGE_FK
----		  ,ED.IMPORT_FILENAME
----		  ,ED.IMPORT_BATCH_NO
----		  ,ED.BATCH_NO
----		  ,ED.STG_RECORD_FK
----		  ,ED.TABLE_ID_FK
----		  ,ED.RULES_FK
----		  ,ED.PREV_RULES_FK
----		  ,ED.RELATED_EVENT_FK
----		  ,ED.RECORD_STATUS_CR_FK
----		  ,ED.UPDATE_TIMESTAMP
----		  ,ED.UPDATE_USER
----		FROM synchronizer.event_data ed WITH (NOLOCK)
----		INNER JOIN epacube.data_name dn WITH (NOLOCK)
----		  ON ( dn.data_name_id = ed.data_name_fk )
----		INNER JOIN epacube.data_set ds WITH (NOLOCK)
----		  ON ( ds.data_set_id = dn.data_set_fk )
----		WHERE ed.batch_no = @in_batch_no
----		AND   (    ed.event_status_cr_fk IN ( 81 )  --- approved 
----			OR    (     ed.event_status_cr_fk = 82   -- rejected
----				   AND  ed.event_source_cr_fk = 79   -- host import
----				  )
----			OR    (     ed.event_status_cr_fk = 83
----				   AND  ed.event_status_cr_fk <> 56  --- and not a no change
----				  )				  
----			  )
--------added so when completeness rules applie we are not inserting the same event twice.
----			   AND   NOT EXISTS
----	            ( SELECT 1 
----	              FROM synchronizer.EVENT_DATA_HISTORY edh WITH (NOLOCK)
----	              WHERE edh.IMPORT_JOB_FK = ed.IMPORT_JOB_FK
----				  AND edh.PRODUCT_STRUCTURE_FK = ed.PARENT_STRUCTURE_FK
----				  and edh.ENTITY_STRUCTURE_FK = ed.ENTITY_STRUCTURE_FK
----				  and edh.ORG_ENTITY_STRUCTURE_FK = ed.ORG_ENTITY_STRUCTURE_FK
----				  and edh.DATA_NAME_FK = ed.DATA_NAME_FK
----				  and edh.NEW_DATA = ed.NEW_DATA)  

--------			OR    (     ed.event_status_cr_fk = 83
--------				   AND  ds.table_name IN ( 'PRODUCT_IDENTIFICATION', 'PRODUCT_IDENT_MULT', 'PRODUCT_IDENT_NONUNIQUE', 'ENTITY_IDENTIFICATION' )
--------				  )				  


------------------------------------------------------------------------------------------
--   Update Event Status to Pending if Still Submitted (87) or Processing (88)
------------------------------------------------------------------------------------------


UPDATE synchronizer.EVENT_DATA
SET EVENT_STATUS_CR_FK = 80   -- RETURN TO PENDING
WHERE BATCH_NO = @in_batch_no
AND   EVENT_STATUS_CR_FK in ( 86, 87, 88 ) 


       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows Returned to Pending = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
       END


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @in_batch_no
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END










