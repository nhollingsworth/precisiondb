﻿



-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing for imports.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        05/01/2012   Initial SQL Version
-- CV        10/04/2013   delete existing mult type data per product structure
-- CV        08/05/2014   Added check for delete param
-- CV        01/13/2017   Rules type cr fk from rules table
-- CV        06/26/2018   Add block to null entity structure if data set does not have entity structure ec cr fc

CREATE PROCEDURE [synchronizer].[EVENT_POSTING_IMPORT_PRODUCT_MULT_TYPE] 
        ( @in_batch_no BIGINT, @in_import_job_fk BIGINT, 
          @in_event_type_cr_fk INT,
          @in_table_name VARCHAR(64),
          @in_data_name_fk BIGINT,
          @in_org_entity_structure_fk VARCHAR(16) )


AS
BEGIN

 DECLARE @ls_exec            nvarchar (max)
 DECLARE @ls_exec2           nvarchar (max)
 DECLARE @ls_exec3           nvarchar (max) 
 DECLARE @l_exec_no          bigint
 DECLARE @l_rows_processed   bigint
 DECLARE @l_sysdate          datetime


DECLARE  @v_input_rows       bigint
DECLARE  @v_output_rows      bigint
DECLARE  @v_output_total     bigint
DECLARE  @v_exception_rows   bigint
DECLARE  @v_job_date         DATETIME
DECLARE  @v_count			 BIGINT
DECLARE  @v_entity_class_cr_fk  int
DECLARE  @v_import_package_fk   int;

DECLARE @l_stmt_no           bigint
DECLARE @ls_stmt             varchar(4000)
DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_PRODUCT_MULT_TYPE.' 
					 + 'Batch: ' + cast(isnull(@in_batch_no, 0) as varchar(30))
					 + 'Import: ' + cast(isnull(@in_import_job_fk, 0) as varchar(30))	
					 + 'Event Type: ' + cast(isnull(@in_event_type_cr_fk, 0) as varchar(30))					 				 
					 + 'Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))					 				 					 					 
					 + 'Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))							 
					 + 'Whse: ' + @in_org_entity_structure_fk 							 					 

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_job_id = @in_import_job_fk,
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;


------------------------------------------------------------------------------------------
---  CLEAR TABLE FOR NEXT BATCH_NO, PRIORITY, JOB_FK, EVENT_TYPE
------------------------------------------------------------------------------------------

      TRUNCATE TABLE #TS_EVENT_DATA


      DECLARE 
			  @vm$data_value_column          varchar(64),
			  @vm$data_name_fk               varchar(16),
			  @vm$data_type_cr_fk            int


	  SET @vm$data_name_fk = CAST ( @in_data_name_fk AS VARCHAR(16) )
	  SET @vm$data_type_cr_fk = ( SELECT ds.data_type_cr_fk from epacube.data_name dn 
	                                                        inner join epacube.data_set ds on ( ds.data_set_id = dn.data_set_fk )
	                                                        where dn.data_name_id = @in_data_name_fk )
	  SET @vm$data_value_column = ( SELECT case isnull ( dn.upper_case_ind, 0 )
										   when 0 then case isnull ( dn.trim_ind, 0 )
													   when 0 then ( ' ID.' + vm.import_column_name )
													   else ( 'RTRIM(LTRIM( ID.' + vm.import_column_name + ' ))' ) 
													   end
										   else case isnull ( dn.trim_ind, 0 )
													   when 0 then ( 'UPPER ( ID.' + vm.import_column_name + ' )' )
													   else ( 'UPPER(RTRIM(LTRIM( ID.' + vm.import_column_name + ' )))' ) 
													   end
										   end                 
									FROM import.v_import vm
									INNER JOIN epacube.data_name dn with (nolock) on ( dn.data_name_id = vm.data_name_fk )
									WHERE vm.data_name_fk = @in_data_name_fk
									AND vm.import_package_fk = ( select top 1 import_package_fk from stage.STG_RECORD WITH (NOLOCK) 
								                                    where job_fk = @in_import_job_fk )
                                    )



	 SET @ls_exec2 = 	CASE ISNULL ( @in_table_name, 'NULL TABLE' )
						WHEN 'PRODUCT_MULT_TYPE'  THEN ' DV.VALUE '
						ELSE 'NULL DATA'						
						END 


	 SET @ls_exec3 = 	CASE ISNULL ( @in_table_name, 'NULL TABLE' )
						WHEN 'PRODUCT_MULT_TYPE' THEN CASE @vm$data_type_cr_fk
						                             WHEN 134 THEN  'LEFT JOIN EPACUBE.DATA_VALUE DV WITH (NOLOCK) ON ( DV.DATA_VALUE_ID = CUR_DATA.DATA_VALUE_FK ) '
						                             WHEN 135 THEN  'LEFT JOIN EPACUBE.ENTITY_DATA_VALUE DV WITH (NOLOCK) ON ( DV.ENTITY_DATA_VALUE_ID = CUR_DATA.ENTITY_DATA_VALUE_FK ) '
						                             END
						ELSE '  '
						END 



	SET @ls_exec =
			 'INSERT INTO #TS_EVENT_DATA
				   ([EVENT_TYPE_CR_FK]							   							   
				   ,[EVENT_ENTITY_CLASS_CR_FK]
				   ,[EVENT_EFFECTIVE_DATE]
				   ,[EPACUBE_ID]
				   ,[PRODUCT_STRUCTURE_FK]
				   ,[DATA_NAME_FK]
				   ,[NEW_DATA]			   
				   ,[CURRENT_DATA]			   			   
				   ,[ORG_ENTITY_STRUCTURE_FK]	   		   
				   ,[ENTITY_CLASS_CR_FK]
				   ,[ENTITY_DATA_NAME_FK]			   			   
				   ,[ENTITY_STRUCTURE_FK]
				   ,[EVENT_CONDITION_CR_FK]
				   ,[EVENT_STATUS_CR_FK]
				   ,[EVENT_PRIORITY_CR_FK]
				   ,[EVENT_SOURCE_CR_FK]
				   ,[EVENT_ACTION_CR_FK]
				   ,[TABLE_ID_FK]
				   ,[JOB_CLASS_FK]
				   ,[IMPORT_JOB_FK]
				   ,[IMPORT_DATE]
				   ,[IMPORT_FILENAME]
				   ,[IMPORT_PACKAGE_FK]
				   ,[STG_RECORD_FK]
				   ,[BATCH_NO]
				   ,[IMPORT_BATCH_NO]	
				   ,[DATA_SET_FK]
				   ,[DATA_TYPE_CR_FK]
				   ,[ORG_IND]
				   ,[INSERT_MISSING_VALUES]
				   ,[TABLE_NAME]				   		   			   
				   ,[VALUE_EFFECTIVE_DATE]
				   ,[END_DATE_NEW]
				   ,[RESULT_TYPE_CR_FK]
				   ,[RECORD_STATUS_CR_FK]
				   ,[UPDATE_TIMESTAMP]
				   ,[UPDATE_USER]
				   ,[Precedence])
			SELECT DISTINCT
			 DS.EVENT_TYPE_CR_FK  
			,SREC.ENTITY_CLASS_CR_FK      
			,CASE WHEN ( SREC.EFFECTIVE_DATE < GETDATE() )
			  THEN GETDATE()
			  ELSE SREC.EFFECTIVE_DATE
			 END      
			,SRECS.SEARCH_STRUCTURE_FK
			,SRECS.SEARCH_STRUCTURE_FK 
			,DN.DATA_NAME_ID 
,IRNV.DATA_VALUE  '
		  + ' , '					   					   					   
		  + @ls_exec2   					   
		  +	',1           
			,DS.ENTITY_STRUCTURE_EC_CR_FK
			,DN.ENTITY_DATA_NAME_FK
			,SRECS.SEARCH_STRUCTURE_FK 
			,97
			,88
			,61
			,SREC.EVENT_SOURCE_CR_FK
	        ,CASE ISNULL ( CUR_DATA.'
	       +  @in_table_name
		   + '_ID, 0 )
			 WHEN 0 THEN 51
			 ELSE 52
			 END
            ,CUR_DATA.'
	       +  @in_table_name
		   + '_ID 
			,IP.JOB_CLASS_FK
			,SREC.JOB_FK
			,SREC.CREATE_TIMESTAMP        
			,SREC.IMPORT_FILENAME         
			,SREC.IMPORT_PACKAGE_FK         
			,SREC.STG_RECORD_ID  
			,SREC.BATCH_NO            
			,SREC.BATCH_NO   
			,DS.DATA_SET_ID
			,DS.DATA_TYPE_CR_FK
			,DS.ORG_IND
			,DN.INSERT_MISSING_VALUES
			,DS.TABLE_NAME			         
			,SREC.EFFECTIVE_DATE                               
			,SREC.END_DATE                              
			,SREC.RESULT_TYPE_CR_FK    
			,1
			,GETDATE()
			,''EPACUBE''
			,NULL
			FROM STAGE.STG_RECORD SREC WITH (NOLOCK)      
			INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
			ON ( DS.ENTITY_CLASS_CR_FK = SREC.ENTITY_CLASS_CR_FK )
			INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
			ON ( DN.DATA_SET_FK = DS.DATA_SET_ID )
			INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)        
			ON ( SRECS.STG_RECORD_FK = SREC.STG_RECORD_ID ) 
			INNER JOIN IMPORT.IMPORT_RECORD_DATA ID WITH (NOLOCK)                        
			ON ( ID.JOB_FK = SREC.JOB_FK  
				AND  ID.RECORD_NO = SREC.RECORD_NO
				AND ISNULL ( ID.DO_NOT_IMPORT_IND, 0 ) <> 1)                    
			 INNER JOIN IMPORT.IMPORT_RECORD_NAME_VALUE IRNV WITH (NOLOCK)
                      ON ( IRNV.JOB_FK = SREC.JOB_FK
                      AND  IRNV.RECORD_NO = SREC.RECORD_NO 
			AND IRNV.DATA_NAME = DN.NAME )                   
			INNER JOIN IMPORT.IMPORT_PACKAGE IP WITH (NOLOCK)         
			ON ( IP.IMPORT_PACKAGE_ID = ID.IMPORT_PACKAGE_FK )   
			LEFT JOIN  STAGE.STG_RECORD_ENTITY SRECEO WITH (NOLOCK)
			ON ( SRECEO.STG_RECORD_FK = SREC.STG_RECORD_ID 
			AND  SRECEO.SEQ_ID = ID.SEQ_ID              
			AND  SRECEO.ENTITY_CLASS_CR_FK  = 10101 
			AND  DS.ORG_IND = 1)
			LEFT JOIN  STAGE.STG_RECORD_ENTITY SRECEE WITH (NOLOCK)
			ON ( SRECEE.STG_RECORD_FK = SREC.STG_RECORD_ID 
			AND  SRECEE.SEQ_ID = ID.SEQ_ID              
			AND  SRECEE.ENTITY_CLASS_CR_FK  = DS.ENTITY_STRUCTURE_EC_CR_FK )  
			LEFT JOIN epacube.'
			+  @in_table_name
			+  ' AS CUR_DATA WITH (NOLOCK)			    
			ON ( CUR_DATA.DATA_NAME_FK = DN.DATA_NAME_ID
			AND  ISNULL ( CUR_DATA.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( SRECS.SEARCH_STRUCTURE_FK, 0 )
            AND  CUR_DATA.DATA_VALUE_FK  =  ( SELECT DVX.DATA_VALUE_ID FROM EPACUBE.DATA_VALUE DVX WHERE DVX.DATA_NAME_FK = DN.DATA_NAME_ID AND DVX.VALUE = IRNV.DATA_VALUE  )
              ) '			
			+ @ls_exec3
            + ' WHERE 1 = 1 '
            + ' AND ISNULL(SREC.BATCH_NO,-999) = '
            + cast(isnull (@in_batch_no, -999) as varchar(20))
            + ' AND   SREC.EVENT_ACTION_CR_FK IN ( 51, 52, 53, 54 )  
				   AND   SREC.EVENT_CONDITION_CR_FK IN ( 97, 98 ) '
		    + '	AND   DN.DATA_NAME_ID = '
            + @vm$data_name_fk
            + ' AND ISNULL ( ' 				   
		    + @ls_exec2 
		    +   ' , ''NULL DATA'' ) <> ISNULL ( IRNV.DATA_VALUE, ''NULL DATA'' )  
					AND  IRNV.DATA_VALUE IS NOT NULL'




----           TRUNCATE TABLE COMMON.T_SQL
       INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
       VALUES ( GETDATE(), ISNULL ( @ls_exec, 'SYNCHRONIZER.EVENT_POSTING_IMPORT_PRODUCT_MULT_TYPE' )  )

       exec sp_executesql 	@ls_exec;



  			                             
--------------------------------------------------------------------------------------------
--   CALL VALIDATIONS
--------------------------------------------------------------------------------------------
	Declare @DELETE_FLAG int

		set @DELETE_FLAG = (select value from epacube.EPACUBE_PARAMS where name = 'DELETE PROD MULT TYPE')

		If @DELETE_FLAG = 1 
		Begin

 delete from epacube.PRODUCT_MULT_TYPE 
 where PRODUCT_STRUCTURE_FK in (select PRODUCT_STRUCTURE_FK  from #TS_EVENT_DATA
 where @in_table_name = 'PRODUCT_MULT_TYPE')

 END


	------------------------------------------------------------------------------------------
	--   QUALIFY GOVERNANCE RULES 
	------------------------------------------------------------------------------------------
	SET  @v_count = ( SELECT COUNT(1)
					 FROM #TS_EVENT_DATA WITH (NOLOCK)
					 WHERE BATCH_NO = @in_batch_no
					 AND   EVENT_STATUS_CR_FK = 88 
					 AND   EVENT_TYPE_CR_FK IN ( 150, 151, 152, 153, 154, 155, 156, 160 )   			 
					 AND   (  EVENT_SOURCE_CR_FK = 71        -- WHEN UI CALLS VALIDATION DIRECTLY WILL REMOVE 71 
						   OR ISNULL ( IMPORT_BATCH_NO, -999 ) = @in_batch_no )    
					 AND   DATA_NAME_FK IN ( SELECT R.RESULT_DATA_NAME_FK   
											 FROM synchronizer.RULES R WITH (NOLOCK)
											 --INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
											 --  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK 
											 --  AND  RS.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE
											 WHERE R.RECORD_STATUS_CR_FK = 1 
											  AND  R.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE)
					)			                         
	IF @v_count > 0
		EXEC synchronizer.EVENT_POST_QUALIFY_RULES_GOVERNANCE  @in_batch_no

    EXEC [synchronizer].[EVENT_POST_VALID_DATA_TYPE] 
    
    IF @in_table_name IN ( 'PRODUCT_MULT_TYPE' )
		EXEC SYNCHRONIZER.EVENT_POST_VALID_DATA_VALUE

--    IF @in_event_type_cr_fk = 156   --- UOMS
--		EXEC [synchronizer].[EVENT_POST_VALID_TYPE_156] 

	EXEC [synchronizer].[EVENT_POST_VALID_TYPE_ALL] 		    


------------------------------------------------------------------------------------------
-- CALL PROC TO SET EVENT PRIORITY ACTION AND STATUS   ( KEEP IN ONE PLACE WITH CODE )
------------------------------------------------------------------------------------------

	EXEC  [synchronizer].[EVENT_POST_COND_PRIORITY_ACTION_STATUS] @in_batch_no


------------------------------------------------------------------------------------------
--   ALLOW NULL ENTITY STRUCTURE OF DATA SET entity structure ec cr fk is NULL
------------------------------------------------------------------------------------------
UPDATE #TS_EVENT_DATA
  SET ENTITY_STRUCTURE_FK = NULL
WHERE EVENT_FK IN ( SELECT EDX.EVENT_FK
		FROM #TS_EVENT_DATA EDX WITH (NOLOCK)
		INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) on EDX.data_name_Fk = DN.DATA_NAME_ID
		INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) on DS.DATA_SET_ID = DN.DATA_SET_FK
		and DS.TABLE_NAME = 'PRODUCT_MULT_TYPE'
		and DS.ENTITY_STRUCTURE_EC_CR_FK is NULL)


------------------------------------------------------------------------------------------
--   SET THE PRIOIRTY FOR THE COMPLETENESS EVENTS
------------------------------------------------------------------------------------------

UPDATE #TS_EVENT_DATA
  SET EVENT_PRIORITY_CR_FK = 69
WHERE EVENT_FK IN ( 
		SELECT EDX.EVENT_FK
		FROM #TS_EVENT_DATA EDX WITH (NOLOCK)
		INNER JOIN synchronizer.RULES R WITH (NOLOCK)
		 ON ( R.RESULT_DATA_NAME_FK = EDX.DATA_NAME_FK )
		--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
		-- ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK
		-- AND  RS.RULE_TYPE_CR_FK = 309 ) 
		WHERE R.RECORD_STATUS_CR_FK = 1
		AND  R.RULE_TYPE_CR_FK = 309 
		AND   EDX.BATCH_NO = @in_batch_no
		AND   EDX.EVENT_STATUS_CR_FK IN ( 88 )  ---- STATUS CONSIDERED TO BE APPROVED OR STILL 88 
		)

       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows set for COMPLETENESS = '
                + CAST( @in_data_name_fk as varchar(16) )
                + ' COUNT:: '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
       END


--------------------------------------------------------------------------------------------
--   CALL POST AND COMPLETE 
--------------------------------------------------------------------------------------------

		EXEC SYNCHRONIZER.EVENT_POST_DATA  

		UPDATE #TS_EVENT_DATA
		SET EVENT_STATUS_CR_FK = 80   -- RETURN TO PENDING
		WHERE EVENT_STATUS_CR_FK in ( 86, 87, 88 ) 


       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows Returned to Pending = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
       END
		



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_PRODUCT_MULT_TYPE'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_ENTITY_PRODUCT_TYPE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


          EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END








