﻿












-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To validate event data did post by batch no
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        09/13/2008  Initial version


CREATE PROCEDURE [synchronizer].[Z_UTIL_VALIDATE_EVENT_POSTING] 
          @in_batch_no int 
         
AS



     DECLARE @ls_exec            nvarchar (max)
     DECLARE @l_exec_no          BIGINT
     DECLARE @ls_sql_stmt        nvarchar (max)
     DECLARE @ls_sql_stmt2       nvarchar (max)     

     DECLARE @ls_sql_perf1       nvarchar (max)     
     DECLARE @ls_sql_perf2       nvarchar (max)     

     DECLARE @ls_sql_pk          varchar (64)
     DECLARE @l_rows_processed   bigint
     DECLARE @l_sysdate          datetime
		
DECLARE @v_count bigint,
        @v_batch_no bigint,
        @v_job_class_fk  int,
        @v_job_name varchar(64),
        @v_data1 varchar(128),
        @v_data2 varchar(128),
        @v_data3 varchar(128),
        @v_job_user varchar(64),
        @v_out_job_fk bigint,
        @v_input_rows bigint,
        @v_output_rows bigint,
        @v_output_total bigint,
        @v_exception_rows bigint,
        @v_job_date datetime,
        @v_sysdate datetime

	DECLARE @l_stmt_no           bigint
    DECLARE @ls_stmt             varchar(1000)
	DECLARE @status_desc		 varchar (max)
	DECLARE @ErrorMessage		 nvarchar(4000)
	DECLARE @ErrorSeverity		 int
	DECLARE @ErrorState			 int


   SET NOCOUNT ON;

   BEGIN TRY
      SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of SYNCHRONIZER.UTIL_VALIDATE_EVENT_POSTING. '

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;

      SET @l_sysdate   = getdate()

            

            SET @status_desc =  'synchronizer.VALIDATE_EVENT_POSTING '
                                
                                
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

--------------------------------------------

		UPDATE synchronizer.EVENT_DATA 
		SET  update_user = NULL
		WHERE batch_no = @in_batch_no

-------------------------------------------------
--PRODUCT EVENTS THAT ARE MARKED POSTED - BUT DID'NT REALLY POST
-------------------------------------------------
 
    UPDATE synchronizer.EVENT_DATA 
	  SET  update_user = 'ERROR POSTING'
	WHERE event_id NOT IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD WITH (NOLOCK)
				 ON ( VCPD.DATA_NAME_FK = tsed.DATA_NAME_FK
				 AND  ISNULL ( VCPD.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( tsed.PRODUCT_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPD.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ORG_ENTITY_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPD.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ENTITY_STRUCTURE_FK, 0 )  
				 AND  ISNULL ( VCPD.UPDATE_LOG_FK, 0 ) = ISNULL ( tsed.EVENT_ID, 0 ) )
			    WHERE tsed.event_status_cr_fk = 81
				AND   tsed.event_type_cr_fk IN ( 150, 153, 154, 155, 156 )
				AND tsed.batch_no = @in_batch_no
				)
AND batch_no = @in_batch_no
AND event_status_cr_fk = 81
AND event_type_cr_fk IN ( 150, 153, 154, 155, 156 )

----PRE-POST PRODUCT EVENTS

 UPDATE synchronizer.EVENT_DATA 
	  SET  update_user = 'ERROR POSTING'
	WHERE event_id NOT IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD WITH (NOLOCK)
				 ON ( VCPD.DATA_NAME_FK = tsed.DATA_NAME_FK
				 AND  VCPD.CURRENT_DATA = tsed.NEW_DATA 
				 AND  ISNULL ( VCPD.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( tsed.PRODUCT_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPD.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ORG_ENTITY_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPD.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ENTITY_STRUCTURE_FK, 0 )  
				 )
			    WHERE tsed.event_status_cr_fk = 83
                AND   tsed.event_type_cr_fk IN ( 150, 153, 154, 155, 156 )
				AND   tsed.batch_no = @in_batch_no
				)
AND batch_no = @in_batch_no
AND event_status_cr_fk = 83
AND Data_Name_FK <> 149000
AND event_type_cr_fk IN ( 150, 153, 154, 155, 156 )


-------------------------------------------------
--PRODUCT EVENTS THAT ARE NOT MARKED POSTED - BUT DID REALLY POST
-------------------------------------------------
 
    UPDATE synchronizer.EVENT_DATA 
	  SET  update_user = 'ALREADY POSTED'
	WHERE event_id IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD WITH (NOLOCK)
				 ON ( VCPD.DATA_NAME_FK = tsed.DATA_NAME_FK
				 AND  ISNULL ( VCPD.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( tsed.PRODUCT_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPD.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ORG_ENTITY_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPD.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ENTITY_STRUCTURE_FK, 0 )  
				 AND  ISNULL ( VCPD.UPDATE_LOG_FK, 0 ) = ISNULL ( tsed.EVENT_ID, 0 ) )
			    WHERE tsed.event_status_cr_fk IN (80, 88, 86, 87)
				AND   tsed.event_type_cr_fk IN ( 150, 153, 154, 155, 156 )
				AND tsed.batch_no = @in_batch_no
				)
AND batch_no = @in_batch_no
AND event_status_cr_fk IN (80, 88, 86, 87)
AND event_type_cr_fk IN ( 150, 153, 154, 155, 156 )


	 
       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			 SET @status_desc = 'total rows Events APPROVED-BUT NOT POSTED:: '
                                + ' '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END


---------------------------------------------------------
--PROD VALUES THAT ARE MARKED POSTED - BUT DIDN'T REALLY POST - Values
---------------------------------------------------------
 
  			  	  
    UPDATE synchronizer.EVENT_DATA 
	  SET  update_user = 'ERROR POSTING'
	WHERE event_id NOT IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_PRODUCT_VALUES VCPV WITH (NOLOCK)
				 ON ( VCPV.DATA_NAME_FK = tsed.DATA_NAME_FK
				 AND  ISNULL ( VCPV.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( tsed.PRODUCT_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPV.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ORG_ENTITY_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPV.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ENTITY_STRUCTURE_FK, 0 )  
				 AND  ISNULL ( VCPV.UPDATE_LOG_FK, 0 ) = ISNULL ( tsed.EVENT_ID, 0 ) )
			   WHERE tsed.event_status_cr_fk = 81
 AND   tsed.event_type_cr_fk IN ( 151 )
				AND tsed.batch_no = @in_batch_no
				)
AND batch_no = @in_batch_no
AND event_status_cr_fk = 81	
AND event_type_cr_fk IN ( 151 )
    
	 
       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			 SET @status_desc = 'total rows Product Value Events APPROVED-BUT NOT POSTED:: '
                                + ' '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END


---------------------------------------------------------
--PROD RESULTS THAT ARE MARKED POSTED - BUT DIDN'T REALLY POST - RESULTS
---------------------------------------------------------
 
	UPDATE synchronizer.EVENT_DATA 
	SET  update_user = 'ERROR POSTING'
	WHERE event_id NOT IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_SHEET_RESULTS VCSR WITH (NOLOCK)
				 ON ( VCSR.DATA_NAME_FK = tsed.DATA_NAME_FK
				 AND  ISNULL ( VCSR.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( tsed.PRODUCT_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCSR.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ORG_ENTITY_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCSR.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ENTITY_STRUCTURE_FK, 0 )  
				 AND  ISNULL ( VCSR.UPDATE_LOG_FK, 0 ) = ISNULL ( tsed.EVENT_ID, 0 ) )			    
			    WHERE tsed.event_status_cr_fk =81
 AND   tsed.event_type_cr_fk IN ( 152 )
				AND tsed.batch_no = @in_batch_no
				)
AND batch_no = @in_batch_no
AND event_status_cr_fk = 81
AND event_type_cr_fk IN ( 152 )

	 
       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			 SET @status_desc = 'total rows Sheet Result Events APPROVED-BUT NOT POSTED:: '
                                + ' '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END

---------------------------------------------------------
--ENTITY EVENTS THAT ARE MARKED POSTED - BUT DIDN'T REALLY POST - ENTITIES
---------------------------------------------------------
 

    UPDATE synchronizer.EVENT_DATA 
	SET  update_user = 'ERROR POSTING'
	WHERE event_id NOT IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_ENTITY_DATA VCED WITH (NOLOCK)
				 ON ( VCED.DATA_NAME_FK = TSED.DATA_NAME_FK
			AND  ISNULL ( VCED.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.EPACUBE_ID, 0 )  			 
			AND  ISNULL ( VCED.UPDATE_LOG_FK, 0 ) = ISNULL ( TSED.EVENT_ID, 0 ) )	
	        AND   tsed.event_type_cr_fk IN ( 154, 160 )
			AND tsed.event_status_cr_fk = 81
				AND tsed.batch_no = @in_batch_no
				)
AND batch_no = @in_batch_no
AND event_status_cr_fk = 81		
AND event_type_cr_fk IN ( 154, 160 )    

	 
       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			 SET @status_desc = 'total rows Entity Events APPROVED-BUT NOT POSTED:: '
                                + ' '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END

---------------------------------------------------------
--ENTITY EVENTS THAT ARE POSTED - BUT NOT MARKED AS POSTED - ENTITIES
---------------------------------------------------------
 

    UPDATE synchronizer.EVENT_DATA 
	SET  update_user = 'ERROR POSTING'
	WHERE event_id IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_ENTITY_DATA VCED WITH (NOLOCK)
				 ON ( VCED.DATA_NAME_FK = TSED.DATA_NAME_FK
			AND  ISNULL ( VCED.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.EPACUBE_ID, 0 )  			 
			AND  ISNULL ( VCED.UPDATE_LOG_FK, 0 ) = ISNULL ( TSED.EVENT_ID, 0 ) )	
	        AND   tsed.event_type_cr_fk IN ( 154, 160 )
			AND tsed.event_status_cr_fk IN (80, 88, 86, 87)
				AND tsed.batch_no = @in_batch_no
				)
AND batch_no = @in_batch_no
AND event_status_cr_fk IN (80, 88, 86, 87) 
AND event_type_cr_fk IN ( 154, 160 )   





------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.UTIL_VALIDATE_EVENT_POSTING_DATA'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.SYNCHRONIZER.UTIL_VALIDATE_EVENT_POSTING has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH