﻿















-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To post the event data from the #TS_EVENT_DATA table 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/13/2008  Initial version
-- CV        09/10/2009  Added to not set status to approved if on hold
-- CV		 12/10/2009  Added approval of entity events
-- CV        01/08/2010  Added Code for 53 - remove record when inactivate
-- CV        05/03/2010  Comment out Epacube search
-- CV        06/28/2011  do not add -999999999 as new data/ new value
-- CV        09/07/2011  added a null value around new data.
-- CV        09/29/2011  Added the 59 - purge action to the inactivation dynamic sql 

CREATE PROCEDURE [synchronizer].[EVENT_POST_DATA] 

         
AS
BEGIN


     DECLARE @ls_exec            nvarchar (max)
     DECLARE @l_exec_no          BIGINT
     DECLARE @ls_sql_stmt        nvarchar (max)
     DECLARE @ls_sql_stmt2       nvarchar (max)     

     DECLARE @ls_sql_pk          varchar (128)
     DECLARE @l_rows_processed   bigint
     DECLARE @l_sysdate          datetime
		
DECLARE @v_count bigint,
        @v_batch_no bigint,
        @v_job_class_fk  int,
        @v_job_name varchar(64),
        @v_data1 varchar(128),
        @v_data2 varchar(128),
        @v_data3 varchar(128),
        @v_job_user varchar(64),
        @v_out_job_fk bigint,
        @v_input_rows bigint,
        @v_output_rows bigint,
        @v_output_total bigint,
        @v_exception_rows bigint,
        @v_job_date datetime,
        @v_sysdate datetime

	DECLARE @l_stmt_no           bigint
    DECLARE @ls_stmt             varchar(1000)
	DECLARE @status_desc		 varchar (max)
	DECLARE @ErrorMessage		 nvarchar(4000)
	DECLARE @ErrorSeverity		 int
	DECLARE @ErrorState			 int


   SET NOCOUNT ON;

   BEGIN TRY
      SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_DATA. '

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;

      SET @l_sysdate   = getdate()

            SET @V_COUNT = ( SELECT COUNT(1) FROM #TS_EVENT_DATA )

            SET @status_desc =  'synchronizer.EVENT_POST_DATA: COUNT '
                                + CAST  ( @V_COUNT AS VARCHAR (16) )
                                
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

--------
----SET @ls_exec = 'STOP CODE HERE'            
----            INSERT INTO COMMON.T_SQL
----                ( TS,  SQL_TEXT )
----            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
----            exec sp_executesql 	@ls_exec;
--

------------------------------------------------------------------------------------------
--   Perform dynamic INSERTS AND UPDATES ( #TS_EVENT_DATA already grouped by Data Set )
--   SQL if retrived from the TABLE NAME and ACTION 
--   NOTE:  By excluding action 54 both NEW PRODUCT and ENTITY_STRUCTURE are also excluded
------------------------------------------------------------------------------------------


      DECLARE 
              @vm$table_name           varchar(128),
              @vm$event_action_cr_fk   int,
              @vm$event_type_cr_fk     int,              
              @vm$event_action         varchar(64)

      DECLARE  cur_v_data_name cursor local for
			 SELECT 
					a.table_name,
					a.event_action_cr_fk,
					a.event_type_cr_fk,
					( select code from epacube.code_ref WITH (NOLOCK) where code_ref_id = a.event_action_cr_fk )
			 FROM (
             SELECT DISTINCT
					tsed.table_name,
					tsed.event_action_cr_fk,
					tsed.event_type_cr_fk
------					tsed.event_condition_cr_fk,
------					tsed.event_priority_cr_fk,
------					tsed.event_status_cr_fk					
			   FROM #TS_EVENT_DATA tsed
			   WHERE tsed.event_status_cr_fk = 88
			   AND   tsed.event_priority_cr_fk = 66			   
			   AND   tsed.event_condition_cr_fk in ( 97, 98 )  --- NOT RED 99
			   AND   tsed.event_action_cr_fk IN ( 50, 51, 52, 53, 59 )     --- NOT 54, 55, 56, 57 or 58
			  ) a
			  ORDER BY a.event_action_cr_fk, a.table_name


      
        OPEN cur_v_data_name;
        FETCH NEXT FROM cur_v_data_name INTO  @vm$table_name,
											  @vm$event_action_cr_fk,
											  @vm$event_type_cr_fk,
											  @vm$event_action
											  											  																			  
											  
											  
         WHILE @@FETCH_STATUS = 0 
         BEGIN
         
            SET @status_desc =  'synchronizer.EVENT_POST_DATA: posting - '
                                + ISNULL  ( @vm$table_name, 'UNKNOWN Table' )
                                + ' '
                                + ISNULL ( @vm$event_action, 'UNKNOWN Action' )


            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;



------------------------------------------------------------------------------------------
--  Create the Dynamic SQL from the DATA SET table
------------------------------------------------------------------------------------------
			
			
		SET @ls_sql_stmt = ISNULL (
							( SELECT DTP.VALUE 
								FROM EPACUBE.DATA_TABLE_PROPERTIES DTP WITH (NOLOCK)
								WHERE DTP.TABLE_NAME = @vm$table_name
								AND   DTP.NAME =  @vm$event_action )
							, ISNULL ( @vm$table_name, 'NULL_DATA' ) + '::' + ISNULL ( @vm$event_action, 'NULL_DATA' )  )

		SET @ls_sql_stmt2 = ISNULL (
							( SELECT DTP.VALUE2 
								FROM EPACUBE.DATA_TABLE_PROPERTIES DTP WITH (NOLOCK)
								WHERE DTP.TABLE_NAME = @vm$table_name
								AND   DTP.NAME =  @vm$event_action )
							, ' ' )


		SET @ls_sql_pk = ISNULL (
							( SELECT DTP.VALUE 
								FROM EPACUBE.DATA_TABLE_PROPERTIES DTP WITH (NOLOCK)
								WHERE DTP.TABLE_NAME = @vm$table_name
								AND   DTP.NAME =  'PRIMARY KEY' )
							, ISNULL ( @vm$table_name, 'NULL_DATA' ) + '::' + ISNULL ( @vm$event_action, 'NULL_DATA' )  )



		IF @vm$event_action_cr_fk = 51  -- NEW VALUE


SET @ls_exec =  
						@ls_sql_stmt
					+	@ls_sql_stmt2
					+   ' WHERE 1 = 1 
					    AND   tsed.event_status_cr_fk = 88 	
                        AND   tsed.event_priority_cr_fk = 66 					
                        AND   tsed.event_condition_cr_fk in ( 97, 98 ) 
			            AND   tsed.event_action_cr_fk IN ( 51 ) 
			            AND   isNULL(tsed.new_data, -999) <> ''-999999999''
			            AND   tsed.event_type_cr_fk = '	
			        +   CAST ( @vm$event_type_cr_fk AS VARCHAR(16) 	)
					+   ' AND tsed.table_name = '
					+   '''' 							
					+   @vm$table_name
					+   ''''

					
	
IF @vm$event_action_cr_fk  in (50, 52)
  
--
			SET @ls_exec =
						@ls_sql_stmt
					+	@ls_sql_stmt2
					+   ' WHERE tsed.event_status_cr_fk = 88 	
                     AND   tsed.event_priority_cr_fk = 66 					
                     AND   tsed.event_condition_cr_fk in ( 97, 98 ) 
			         AND   tsed.event_action_cr_fk IN ( 50, 52 ) 	
			         AND   tsed.event_type_cr_fk = '	
			        +   CAST ( @vm$event_type_cr_fk AS VARCHAR(16) 	)
					+   ' AND tsed.table_name = '''							
					+   CAST ( ISNULL ( @vm$table_name, 'NULL' ) AS VARCHAR(128) )												
					+   ''''
					+  ' AND tsed.table_id_fk = '
					+   @ls_sql_pk


----
--IF @vm$event_action_cr_fk in ( 53, 59)  --INACTIVATE 53 - REMOVE RECORD
-- and @vm$table_name = 'PRODUCT_MULT_TYPE'
 
-- BEGIN
 
--  SET @ls_exec =        @ls_sql_stmt
--					+	@ls_sql_stmt2
--					+   ' WHERE 1 = 1 AND '  
--					+    @ls_sql_pk 
--					+   '= tsed.table_id_fk' 


--					----	@ls_sql_stmt
--					----+	@ls_sql_stmt2
--					----+   ' WHERE 1 = 1 
--					----    AND tsed.event_status_cr_fk = 88 		
--     ----                   AND   tsed.event_priority_cr_fk = 66 					
--					----	AND   tsed.event_condition_cr_fk in ( 97, 98 ) 
--			  ----          AND   tsed.event_action_cr_fk IN ( 53, 59 ) 
--			  ----          AND   tsed.product_Structure_fk = epacube.product_Mult_type.product_structure_fk
--			  ----          AND   tsed.data_name_fk = epacube.product_Mult_type.data_name_fk
--			  ----          AND   tsed.event_type_cr_fk = '	
--			  ----      +   CAST ( @vm$event_type_cr_fk AS VARCHAR(16) 	)			        					        	
--					----+   ' AND tsed.table_name = '''  							
--					----+   CAST ( ISNULL ( @vm$table_name, 'NULL' ) AS VARCHAR(128) )												
--					----+   ''''
 
-- End
 
 IF @vm$event_action_cr_fk in ( 53, 59) 
  --and @vm$table_name <> 'PRODUCT_MULT_TYPE'

BEGIN
SET @ls_exec =
--- ks added perf sql
						@ls_sql_stmt
					+	@ls_sql_stmt2
					+   ' WHERE 1 = 1 AND '  
					+    @ls_sql_pk 
					+   ' in (select table_id_fk from #TS_EVENT_DATA TSED
					    WHERE tsed.event_status_cr_fk = 88 		
                        AND   tsed.event_priority_cr_fk = 66 					
						AND   tsed.event_condition_cr_fk in ( 97, 98 ) 
			            AND   tsed.event_action_cr_fk IN ( 53, 59 ) 		
			            AND   tsed.event_type_cr_fk = '	
			        +   CAST ( @vm$event_type_cr_fk AS VARCHAR(16) 	)			        					        	
					+   ' AND tsed.table_name = '''  							
					+   CAST ( ISNULL ( @vm$table_name, 'NULL' ) AS VARCHAR(128) )												
					+   ''''
					+   ' AND table_id_fk = '
					+   @ls_sql_pk			
					+ ')'


END

------------------------------------------------------------------------------------------
--  Execute Dynamic SQL 
------------------------------------------------------------------------------------------



----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS,  SQL_TEXT )
            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )


            exec sp_executesql 	@ls_exec;

	   

------------------------------------------------------------------------------------------
--   For now going to assume every Post when according to plan and am setting 
--		The status to APPROVE
------------------------------------------------------------------------------------------

   UPDATE #TS_EVENT_DATA
	  SET    event_status_cr_fk = 81  -- APPROVED 
   WHERE 1 = 1            
   AND   event_status_cr_fk = 88                            
   AND   event_priority_cr_fk = 66                                
   AND   event_condition_cr_fk in ( 97, 98 )                  
   AND   event_action_cr_fk = @vm$event_action_cr_fk                    
   AND   event_type_cr_fk = @vm$event_type_cr_fk 
   AND   table_name = @vm$table_name
	  
	  
	 
       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			 SET @status_desc = 'total rows APPROVED:: '
                                + ' Table: '			 
								+ @vm$table_name
                                + ' Action: '			 
								+ @vm$event_action			 
                                + ' Count: '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))

			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc

       END




------------------------------------------------------------------------------------------
--   Perform dynamic UPDATES to epacube.epacube_search if data is in search columns
------------------------------------------------------------------------------------------


----      DECLARE 
----              @vm$search_name          varchar(16),
----              @vm$search_data_name_fk  varchar(16)
----
----      DECLARE  cur_v_search_name cursor local for
----			 SELECT 
----					a.search_name,
----					a.search_data_name_fk
----			 FROM (
----             SELECT  DISTINCT
----                     EEP.NAME AS search_name
----					,CAST ( tsed.DATA_NAME_FK AS VARCHAR(16) ) AS search_data_name_fk
----					FROM #TS_EVENT_DATA tsed 
----					INNER JOIN epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
----					  ON (  eep.APPLICATION_SCOPE_FK = tsed.event_entity_class_cr_fk 
----					  AND   CAST ( eep.value AS INT ) = tsed.data_name_fk
----					  AND   eep.NAME IN ( 'SEARCH1', 'SEARCH2', 'SEARCH3', 'SEARCH4', 'SEARCH5', 'SEARCH6' ) )
----					WHERE 1 = 1
----					AND   tsed.event_status_cr_fk = 81   --- APPROVED
----					AND   tsed.event_condition_cr_fk in ( 97, 98 )  --- NOT RED 99
----					AND   tsed.event_action_cr_fk IN ( 50, 51, 52, 53, 59 )  --- NOT 54, 55, 56, 57 or 58
----					AND   tsed.event_type_cr_fk IN ( 150, 153, 154, 155, 156 )
----			  ) a
----			  ORDER BY a.search_name
----      
----        
----     OPEN cur_v_search_name;											  
----     FETCH NEXT FROM cur_v_search_name INTO  @vm$search_name
----											,@vm$search_data_name_fk 
----											  											  
----     WHILE @@FETCH_STATUS = 0 
----     BEGIN
----
----
----------------------------------------------------------------------------------------------
------  PERFORM UPDATE
----------------------------------------------------------------------------------------------
----
----
----SET @ls_exec =  'UPDATE epacube.EPACUBE_SEARCH SET '
----			    + @vm$search_name
----			    + ' = tsed.NEW_DATA '
----			    + ' FROM #TS_EVENT_DATA tsed WITH (NOLOCK) '
----			    + ' WHERE 1 = 1 '
----				+ '	AND   tsed.event_status_cr_fk = 81 '
----				+ '	AND   tsed.event_condition_cr_fk in ( 97, 98 )'
----				+ '	AND   tsed.event_action_cr_fk IN ( 50, 51, 52, 53, 59 )'
----				+ '	AND   tsed.event_type_cr_fk IN ( 150, 153, 154, 155, 156 )'
----				+ '	AND   tsed.data_name_fk = '
----				+ @vm$search_data_name_fk
----				+ ' AND   tsed.event_entity_class_cr_fk = epacube.EPACUBE_SEARCH.entity_class_cr_fk '
----				+ ' AND   tsed.epacube_id = epacube.EPACUBE_SEARCH.epacube_id '
----
----																						

------------------------------------------------------------------------------------------
--  Execute Dynamic SQL 
------------------------------------------------------------------------------------------



----           TRUNCATE TABLE COMMON.T_SQL
--            INSERT INTO COMMON.T_SQL
--                ( TS,  SQL_TEXT )
--            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
--
--
--            exec sp_executesql 	@ls_exec;


----			 SET @status_desc = 'UPDATE epacube.EPACUBE_SEARCH:: '
----								+ @vm$search_name
----								+ ' : '
----								+ @vm$search_data_name_fk
----								
----			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
----	   
----
----------------------------------------------------------------------------------------------
------  Get Next Data Name for Posting
----------------------------------------------------------------------------------------------
----
----     FETCH NEXT FROM cur_v_search_name INTO  @vm$search_name
----											,@vm$search_data_name_fk 
----        
----     END --cur_v_search_name LOOP
----     CLOSE cur_v_search_name;
----	 DEALLOCATE cur_v_search_name; 
----     
----
----
----
----
----
----
----
----
----------------------------------------------------------------------------------------------
------  Get Next Data Name for Posting
----------------------------------------------------------------------------------------------
----
----

        FETCH NEXT FROM cur_v_data_name INTO  @vm$table_name,
											  @vm$event_action_cr_fk,
											  @vm$event_type_cr_fk,
											  @vm$event_action


     END --cur_v_data_name LOOP
     CLOSE cur_v_data_name;
	 DEALLOCATE cur_v_data_name; 
     


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------
--
SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_DATA'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.SYNCHRONIZER.EVENT_POST_DATA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END




















