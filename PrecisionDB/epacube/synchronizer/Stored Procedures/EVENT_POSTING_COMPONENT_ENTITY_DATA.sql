﻿





-- Copyright 2017
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing from UI.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        03/20/2017   Initial SQL Version
 


CREATE PROCEDURE [synchronizer].[EVENT_POSTING_COMPONENT_ENTITY_DATA] 
        ( @in_table_id bigint,
          @in_data_name_fk BIGINT,
		  @in_data_level_cr_fk INT,
		  @in_batch_no bigint,
		  @in_New_Data Varchar(256),
		  @UserID Varchar(64))
		


AS
BEGIN

 DECLARE @ls_exec            nvarchar (max)
 DECLARE @ls_exec2           nvarchar (max)
 DECLARE @ls_exec3           nvarchar (max) 
 DECLARE @ls_exec4           nvarchar (max) 
 DECLARE @l_exec_no          bigint
 DECLARE @l_rows_processed   bigint
 DECLARE @l_sysdate          datetime


DECLARE  @v_input_rows       bigint
DECLARE  @v_output_rows      bigint
DECLARE  @v_output_total     bigint
DECLARE  @v_exception_rows   bigint
DECLARE  @v_job_date         DATETIME
DECLARE  @v_count			 BIGINT
DECLARE  @v_entity_class_cr_fk  int
 
DECLARE @l_stmt_no           bigint
DECLARE @ls_stmt             varchar(4000)
DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int


 DECLARE @in_product_structure_fk BIGINT,
			@in_Vendor_entity_structure_fk BIGINT, 
			@in_cust_entity_structure_fk BIGINT,
			@in_zone_entity_structure_fk Bigint,
			@in_dept_entity_structure_fk Bigint,
			@in_assoc_entity_structure_fk Bigint,
			@in_org_entity_structure_fk bigint,
			@in_entity_structure_fk bigint,
			--@in_batch_no BIGINT,
			@in_event_type_cr_fk INT,
			@in_current_data varchar(256),
			@in_data_value_fk bigint,
			@in_entity_data_value_fk bigint,
			@in_Dept_entity_data_value_fk bigint,
			@in_Precedence INT


		 
Declare @in_Table_name varchar(128)

Set @in_Precedence = (select isNULL(PRECEDENCE,NULL) from epacube.CODE_REF where CODE_REF_ID =   @in_data_level_cr_fk)
-------------------------------------------------------------------------------------------------------------------------------

Set @v_entity_class_cr_fk =  (select Entity_Class_CR_FK from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk
and ISNULL( @in_data_level_cr_fk,0) = ISNULL(data_level_cr_fk,0)  )

if @v_entity_class_cr_fk is NULL
BEGIN  

set @v_entity_class_cr_fk = (select Entity_Class_CR_FK from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk)

END

---------------------------------------------------------------------------------------------------------------------------------------------

SET @in_event_type_cr_fk =  (select event_type_cr_fk from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk
and ISNULL( @in_data_level_cr_fk,0) = ISNULL(data_level_cr_fk,0)  )

if @in_event_type_cr_fk is NULL
BEGIN  

set @in_event_type_cr_fk = (select EVENT_TYPE_CR_FK from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk)

END

 ----------------------------------------------------------------------------------------------
 -------GET TABLE NAME WHERE DATA LIVES FROM DATA SET OR DATA NAME LEVEL --------------------------------
 ----------------------------------------------------------------------------------------------

 
set @in_table_name = (select TABLE_NAME from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk
and ISNULL( @in_data_level_cr_fk,0) = ISNULL(data_level_cr_fk,0)  )

if @in_table_name is NULL
BEGIN  

set @in_table_name = (select TABLE_NAME from epacube.DATA_NAME_GROUP_REFS where DATA_NAME_FK =  @in_data_name_fk)

END

 
 

BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_COMPONENT_ENTITY_DATA.' 
					 	 				 
					 + 'Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))					 				 					 					 
					 + 'Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))							 
					 --+ 'Data Level: ' + cast(isnull(@in_data_level_cr_fk,0) as varchar(30))							 					 

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_job_id = NULL,
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;

--------------------------------------------------------------------------------------------------------
   
   
   if @in_table_name = 'ENTITY_IDENT_NONUNIQUE'   BEGIN
set @in_entity_structure_fk = (select ENTITY_STRUCTURE_FK from epacube.ENTITY_IDENT_NONUNIQUE where ENTITY_IDENT_NONUNIQUE_ID = @in_table_id)
set @in_current_data  =  (select value from epacube.ENTITY_IDENT_NONUNIQUE where ENTITY_IDENT_NONUNIQUE_ID = @in_table_id)
 ----May need to look up NEW DATA value
 
END
  
 
 if @in_table_name = 'SEGMENTS_SETTINGS'   BEGIN
set @in_Vendor_entity_structure_fk = (select VENDOR_ENTITY_STRUCTURE_FK from epacube.SEGMENTS_SETTINGS where SEGMENTS_SETTINGS_ID = @in_table_id)
set @in_cust_entity_structure_fk =  (select cust_ENTITY_STRUCTURE_FK from epacube.SEGMENTS_SETTINGS where SEGMENTS_SETTINGS_ID = @in_table_id)
set @in_zone_entity_structure_fk =  (select ZONE_ENTITY_STRUCTURE_FK from epacube.SEGMENTS_SETTINGS where SEGMENTS_SETTINGS_ID = @in_table_id)
set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from epacube.SEGMENTS_SETTINGS where SEGMENTS_SETTINGS_ID = @in_table_id)
set @in_current_data  = (select ATTRIBUTE_EVENT_DATA from epacube.SEGMENTS_SETTINGS where SEGMENTS_SETTINGS_ID = @in_table_id)

 END

 if @in_table_name = 'SETTINGS_POS_STORE'   BEGIN
    
set @in_assoc_entity_structure_fk = (select Assoc_ENTITY_STRUCTURE_FK from epacube.SETTINGS_POS_STORE where SETTINGS_POS_STORE_ID = @in_table_id)
set @in_cust_entity_structure_fk =  (select cust_ENTITY_STRUCTURE_FK from epacube.SETTINGS_POS_STORE where SETTINGS_POS_STORE_ID = @in_table_id)
set @in_dept_entity_data_value_fk =  (select DEPT_ENTITY_DATA_VALUE_FK from epacube.SETTINGS_POS_STORE where SETTINGS_POS_STORE_ID = @in_table_id)
set @in_org_entity_structure_fk =  (select org_ENTITY_STRUCTURE_FK from  epacube.SETTINGS_POS_STORE where SETTINGS_POS_STORE_ID = @in_table_id)
set @in_current_data  = (select ATTRIBUTE_EVENT_DATA from epacube.SETTINGS_POS_STORE where SETTINGS_POS_STORE_ID = @in_table_id)
  
 ---set data value fk and entity_data_value fk for new data   
  
END

   if @in_table_name = 'ENTITY_ATTRIBUTE'   BEGIN
 set @in_Vendor_entity_structure_fk =  (select ENTITY_STRUCTURE_FK from epacube.ENTITY_ATTRIBUTE where ENTITY_ATTRIBUTE_ID = @in_table_id)
 set @in_current_data  = (select attribute_event_data from epacube.ENTITY_ATTRIBUTE where ENTITY_ATTRIBUTE_ID = @in_table_id)
 ----May need to look up NEW DATA value

END

--select * from epacube.ENTITY_CATEGORY
 
 if @in_table_name = 'ENTITY_CATEGORY'   BEGIN
set @in_Vendor_entity_structure_fk = (select entity_STRUCTURE_FK from epacube.ENTITY_CATEGORY where ENTITY_CATEGORY_ID = @in_table_id)
set @in_current_data  = (select dv.value  from epacube.ENTITY_CATEGORY ec Inner Join Epacube.data_value dv on dv.DATA_VALUE_ID = ec.DATA_VALUE_FK
where ec.ENTITY_CATEGORY_ID = @in_table_id)
 ----May need to look up NEW DATA value
 
END
 

 if @in_table_name = 'ENTITY_MULT_TYPE'   BEGIN
set @in_Vendor_entity_structure_fk = (select ENTITY_STRUCTURE_FK from epacube.ENTITY_MULT_TYPE where ENTITY_MULT_TYPE_ID = @in_table_id)
set @in_current_data  = (select dv.value  from epacube.ENTITY_MULT_TYPE emt Inner Join Epacube.data_value dv on dv.DATA_VALUE_ID = emt.DATA_VALUE_FK
where emt.ENTITY_MULT_TYPE_ID = @in_table_id)
 ----May need to look up NEW DATA value
 
END
 


------------------------------------------------------------------------------------------
---  CLEAR TABLE FOR NEXT BATCH_NO, PRIORITY, JOB_FK, EVENT_TYPE
------------------------------------------------------------------------------------------
    
	--EXEC seq_manager.db_get_next_sequence_value_impl 'common',
 --                   'batch_seq', @in_batch_no output


INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
		   ,[PRODUCT_STRUCTURE_FK]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]        
           ,[NEW_DATA]
           ,[CURRENT_DATA]
		   ,[ORG_ENTITY_STRUCTURE_FK]
		   ,[CUST_ENTITY_STRUCTURE_FK]
           ,[ZONE_ENTITY_STRUCTURE_FK]
           ,[DEPT_ENTITY_STRUCTURE_FK]
           ,[VENDOR_ENTITY_STRUCTURE_FK]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[TABLE_ID_FK]
		   ,[IMPORT_FILENAME]
           ,[DATA_LEVEL]
           ,[BATCH_NO]
           ,[DATA_VALUE_FK]
           ,[ENTITY_DATA_VALUE_FK]
		   ,[DEPT_ENTITY_DATA_VALUE_FK]
		   --,[PROD_DATA_VALUE_FK]
     --      ,[CUST_DATA_VALUE_FK]
     --      ,[RESULT_DATA_VALUE_FK]
           ,[PRECEDENCE]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
 SELECT 
 ISNULL(@in_EVENT_TYPE_CR_FK,154)
,ISNULL(@v_ENTITY_CLASS_CR_FK,10104)
,getdate()
,case when @v_entity_class_cr_fk = 10109 then @in_product_structure_fk else @in_cust_entity_structure_fk END
,case when @v_entity_class_cr_fk = 10109 then @in_product_structure_fk else @in_cust_entity_structure_fk END  --epacube_id (may need entity)
,@in_data_name_fk
,@in_new_data
,@in_current_data
,@in_org_entity_structure_fk
,@in_cust_entity_structure_fk
,@in_Zone_entity_structure_fk
,@in_dept_entity_structure_fk
,@in_Vendor_entity_structure_fk
,97
,88
,66
,71
,case when @in_table_id is NULL then 51 else 52 End 
,@in_table_id
,@in_Table_name
,@in_data_level_cr_fk
,@in_batch_no  --batch
,@in_data_value_fk
,@in_ENTITY_DATA_VALUE_FK
,@in_Dept_entity_data_value_fk
--,@in_PROD_DATA_VALUE_FK
--,@in_CUST_DATA_VALUE_FK
--,@in_RESULT_DATA_VALUE_FK
,@in_PRECEDENCE
,case when @in_new_data = '-999999999' then 2 else 1 END
,getdate()
,@UserID

-- from  epacube.data_name dn 
--inner join epacube.data_set ds on ds.DATA_SET_ID = dn.DATA_SET_FK
--and dn.DATA_NAME_ID = @in_data_name_fk



-------------------------------------------------------------------------------------------
---setting new data data values

							UPDATE ED 
							set ED.data_value_fk =   dv.DATA_VALUE_ID 
							from epacube.data_name DN 
							 inner join epacube.data_value DV on DV.DATA_NAME_FK = dn.SOURCE_DATA_DN_FK
							 inner join synchronizer.event_data ED on dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and ed.NEW_DATA = dv.VALUE and ed.EVENT_STATUS_CR_FK = 88
							 where DN.source_data_dn_fk is not NULL



							UPDATE ED 
							 SET ED.data_value_fk = DV.DATA_VALUE_ID
			                 FROM EPACUBE.DATA_VALUE DV WITH (NOLOCK)
							 inner join synchronizer.EVENT_DATA ED WITH (NOLOCK)  
							 ON DV.DATA_NAME_FK = ED.DATA_NAME_FK
			                 AND   DV.VALUE = ED.NEW_DATA
							 INNER JOIN Epacube.data_name DN WITH (nolock) on dn.DATA_NAME_ID = ed.DATA_NAME_FK
							 and dn.SOURCE_DATA_DN_FK is NULL 
							 where ed.EVENT_STATUS_CR_FK = 88


----------------------------------------------------------------------------------------------
----   CALL POST AND COMPLETE 
----------------------------------------------------------------------------------------------

		--EXEC [synchronizer].[EVENT_POSTING] @in_batch_no
		
	 

		--UPDATE  EVENT_DATA
		--SET EVENT_STATUS_CR_FK = 80   -- RETURN TO PENDING
		--WHERE EVENT_STATUS_CR_FK in ( 86, 87, 88 ) 


   --    SET @l_rows_processed = @@ROWCOUNT
	   
	  -- IF @l_rows_processed > 0
	  -- BEGIN
			--SET @status_desc = 'total rows Returned to Pending = '
			--	+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			--EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
   --    END
		



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_COMPONENT_ENTITY_DATA'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_COMPONENT_ENTITY_DATA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END



















