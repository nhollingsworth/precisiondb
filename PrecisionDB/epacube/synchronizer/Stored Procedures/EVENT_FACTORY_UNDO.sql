﻿








-- Copyright 2011
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: For each Event in the batch which are approved, create a reversing event same batch_no status=87
--          Previous Action 51 and not exported to host will be Event_Action PURGE (59)
--          Previous Action 51 and exported to host will be Event Action INACTIVE (53)
--          Previous Action 52 will be Event Action 52 with the Previous Values used as Current 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/23/2011   Initial SQL Version
-- CV        10/04/2011   Treating all as inactivate and then running
--                        purge after export procedure from undo posting 

CREATE PROCEDURE [synchronizer].[EVENT_FACTORY_UNDO] 
        ( @in_batch_no bigint )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime
DECLARE  @v_check_export_ind  bigint

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_FACTORY_UNDO.' + ' Batch No = '
					 + cast(isnull(@in_batch_no, 0) as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;


SET @l_sysdate = GETDATE()



------------------------------------------------------------------------------------------
--   Create INACTIVATE events from previous Approved Statuses
------------------------------------------------------------------------------------------


----------------------------------------------------------------------
---USED TO PARAMARTERIZE IF EXPORTED DATA NEEDS TO BE CHECKED--
    -- CAN ONLY USE ONE EXPORT NAME WHEN PASSED TO FUNCTION --
----------------------------------------------------------------------

--SET @v_check_export_ind = (select value from epacube.epacube_params
--where name = 'CHECK_EXPORTED');


------------------------------------------------------------------------------------------
-- CHECK EVENT IF SHOULD BE PLACED ON HOLD AND YELLOW CONFLICT ERROR
--		DUE TO PREVIOUS CHANGE IN EPACUBE HAS NOT YET BEEN EXPORTED TO HOST 
--		AND HOST IMPORT CHANGE IS NOW BEING APPLIED 
------------------------------------------------------------------------------------------

--IF @v_check_export_ind = 1 

--BEGIN

------------------------------------------------------------------------------------------
-- CHECK EXPORT CONFIGS FOR CHANGE EXPORTS ONLY
------------------------------------------------------------------------------------------

--      DECLARE 
--              @vm$config_data_name_fk	       int, 
--              @vm$config_export_datetime       datetime

--      DECLARE  cur_v_chg_export cursor local FOR
      
--				SELECT 
--				A.DATA_NAME_FK, 
--				MAX ( JOB_COMPLETE_TIMESTAMP  )
--				FROM (
--				SELECT CNFDN.DATA_NAME_FK, JOB.JOB_COMPLETE_TIMESTAMP
--				FROM epacube.CONFIG CNF WITH (NOLOCK)
--				INNER JOIN EPACUBE.CONFIG_PROPERTY CNFJP WITH (NOLOCK)
--					 ON ( CNFJP.CONFIG_FK = CNF.CONFIG_ID
--					 AND  CNFJP.NAME = 'COLUMNS_CONFIG_ID' )
--				INNER JOIN EPACUBE.CONFIG_DATA_NAME CNFDN WITH (NOLOCK) ON ( CNFDN.CONFIG_FK = CAST ( CNFJP.VALUE AS INT )  )
--				LEFT JOIN COMMON.JOB JOB WITH (NOLOCK) ON ( JOB.NAME = CNF.NAME ) 
--				WHERE CNF.NAME IN ( 	SELECT CNFJ.NAME AS CONFIG_NAME
--									FROM epacube.CONFIG CNFJ WITH (NOLOCK)
--									INNER JOIN epacube.CONFIG_PROPERTY CNFJPCHG
--										ON ( CNFJ.CONFIG_ID = CNFJPCHG.CONFIG_FK
--										AND  CNFJPCHG.NAME = 'FILTER_CHANGES'   -- CHANGES ONLY 
--										AND  CNFJPCHG.VALUE = 'TRUE'  )  )
--				) A 
--				GROUP BY A.DATA_NAME_FK, A.JOB_COMPLETE_TIMESTAMP      
--                ORDER BY A.DATA_NAME_FK
      

--        OPEN cur_v_chg_export
--        FETCH NEXT FROM cur_v_chg_export INTO @vm$config_data_name_fk
--                                             ,@vm$config_export_datetime


--         WHILE @@FETCH_STATUS = 0 
--         BEGIN


----  CREATE INACTIVATE EVENT

--		INSERT INTO [synchronizer].[EVENT_DATA]
--				   ([EVENT_TYPE_CR_FK]
--				   ,[EVENT_ENTITY_CLASS_CR_FK]
--				   ,[EVENT_EFFECTIVE_DATE]
--				   ,[EPACUBE_ID]
--				   ,[DATA_NAME_FK]
--				   ,[PRODUCT_STRUCTURE_FK]
--				   ,[ORG_ENTITY_STRUCTURE_FK]
--				   ,[ENTITY_CLASS_CR_FK]
--				   ,[ENTITY_DATA_NAME_FK]
--				   ,[ENTITY_STRUCTURE_FK]
--				   ,[LEVEL_SEQ]
--				   ,[PARENT_STRUCTURE_FK]
--				   ,[ASSOC_PARENT_CHILD]
--				   ,[NEW_DATA]
--				   ,[CURRENT_DATA]
--				   ,[NET_VALUE1_NEW]
--				   ,[NET_VALUE2_NEW]
--				   ,[NET_VALUE3_NEW]
--				   ,[NET_VALUE4_NEW]
--				   ,[NET_VALUE5_NEW]
--				   ,[NET_VALUE6_NEW]
--				   ,[NET_VALUE1_CUR]
--				   ,[NET_VALUE2_CUR]
--				   ,[NET_VALUE3_CUR]
--				   ,[NET_VALUE4_CUR]
--				   ,[NET_VALUE5_CUR]
--				   ,[NET_VALUE6_CUR]
--				   ,[SEARCH1]
--				   ,[SEARCH2]
--				   ,[SEARCH3]
--				   ,[SEARCH4]
--				   ,[SEARCH5]
--				   ,[SEARCH6]
--				   ,[EVENT_CONDITION_CR_FK]
--				   ,[EVENT_STATUS_CR_FK]
--				   ,[EVENT_PRIORITY_CR_FK]
--				   ,[EVENT_SOURCE_CR_FK] 
--				   ,[EVENT_ACTION_CR_FK] 
--				   ,[RESULT_TYPE_CR_FK]
--				   ,[SINGLE_ASSOC_IND]
--				   ,[JOB_CLASS_FK]
--				   ,[CALC_JOB_FK]
--				   ,[IMPORT_JOB_FK]
--				   ,[IMPORT_DATE]
--				   ,[IMPORT_PACKAGE_FK]
--				   ,[IMPORT_FILENAME]
--				   ,[IMPORT_BATCH_NO]
--				   ,[BATCH_NO]
--				   ,[STG_RECORD_FK]
--				   ,[TABLE_ID_FK]
--				   ,[RULES_FK]
--				   ,[RELATED_EVENT_FK]
--				   ,[RECORD_STATUS_CR_FK]
--				   ,[UPDATE_TIMESTAMP]
--				   ,[UPDATE_USER]
--				   )
--		SELECT 
--					EDH.EVENT_TYPE_CR_FK
--				   ,EDH.EVENT_ENTITY_CLASS_CR_FK
--				   ,EDH.EVENT_EFFECTIVE_DATE
--				   ,EDH.EPACUBE_ID
--				   ,EDH.DATA_NAME_FK
--				   ,EDH.PRODUCT_STRUCTURE_FK
--				   ,EDH.ORG_ENTITY_STRUCTURE_FK
--				   ,EDH.ENTITY_CLASS_CR_FK
--				   ,EDH.ENTITY_DATA_NAME_FK
--				   ,EDH.ENTITY_STRUCTURE_FK
--				   ,EDH.LEVEL_SEQ
--				   ,EDH.PARENT_STRUCTURE_FK
--				   ,EDH.ASSOC_PARENT_CHILD
--				   ,'-999999999'    ----  NEW_DATA FOR INACTIVATE
--				   ,EDH.NEW_DATA     ----  CURRENT_DATA
--				   ,-999999999      ----  NET_VALUE1_NEW
--				   ,-999999999      ----  NET_VALUE2_NEW
--				   ,-999999999      ----  NET_VALUE3_NEW
--				   ,-999999999      ----  NET_VALUE4_NEW
--				   ,-999999999      ----  NET_VALUE5_NEW
--				   ,-999999999      ----  NET_VALUE6_NEW
--				   ,EDH.NET_VALUE1_NEW
--				   ,EDH.NET_VALUE2_NEW
--				   ,EDH.NET_VALUE3_NEW
--				   ,EDH.NET_VALUE4_NEW
--				   ,EDH.NET_VALUE5_NEW
--				   ,EDH.NET_VALUE6_NEW
--				   ,EDH.SEARCH1
--				   ,EDH.SEARCH2
--				   ,EDH.SEARCH3
--				   ,EDH.SEARCH4
--				   ,EDH.SEARCH5
--				   ,EDH.SEARCH6
--				   ,97             ----   EVENT_CONDITION_CR_FK
--				   ,87             ----   EVENT_STATUS_CR_FK
--				   ,EDH.EVENT_PRIORITY_CR_FK
--				   ,78             ----   EVENT_SOURCE_CR_FK FOR UNDO
--				   ,53             ----   EVENT_ACTION_CR_FK FOR INACTIVATE
--				   ,710            ----   RESULT_TYPE_CR_FK
--				   ,EDH.SINGLE_ASSOC_IND
--				   ,EDH.JOB_CLASS_FK
--				   ,EDH.CALC_JOB_FK
--				   ,EDH.IMPORT_JOB_FK
--				   ,EDH.IMPORT_DATE
--				   ,EDH.IMPORT_PACKAGE_FK
--				   ,EDH.IMPORT_FILENAME
--				   ,EDH.IMPORT_BATCH_NO
--				   ,EDH.BATCH_NO
--				   ,EDH.STG_RECORD_FK
--				   ,EDH.TABLE_ID_FK
--				   ,EDH.RULES_FK
--				   ,EDH.RELATED_EVENT_FK
--				   ,2              ----  EDH.RECORD_STATUS_CR_FK
--				   ,EDH.UPDATE_TIMESTAMP
--				   ,EDH.UPDATE_USER
--		FROM synchronizer.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
--		WHERE 1 = 1
--		AND   @vm$config_export_datetime IS NOT NULL		
--		AND   EDH.BATCH_NO = @in_batch_no
--		AND   EDH.DATA_NAME_FK = @vm$config_data_name_fk
--		AND   EDH.EVENT_STATUS_CR_FK = 81  -- APPROVED ONLY
--		AND   EDH.EVENT_ACTION_CR_FK = 51  -- NEW VALUES ONLY
--		AND   EDH.EVENT_SOURCE_CR_FK <> 78 -- DO NOT ALLOW UNDO OF UNDO
--		AND   EDH.UPDATE_TIMESTAMP >= @vm$config_export_datetime  ----@vm$config_data_name_fk


--		 FETCH NEXT FROM cur_v_chg_export INTO @vm$config_data_name_fk
--											  ,@vm$config_export_datetime


--		 END --cur_v_chg_export
--		 CLOSE cur_v_chg_export;
--		 DEALLOCATE cur_v_chg_export; 



--END


------------------------------------------------------------------------------------------
--   Create Inactivation events from previous Approved Statuses
--     Exclude any events that where already created as INACTIVE
------------------------------------------------------------------------------------------


--  CREATE INACTIVATION EVENT

INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_CLASS_CR_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[LEVEL_SEQ]
           ,[PARENT_STRUCTURE_FK]
           ,[ASSOC_PARENT_CHILD]
           ,[NEW_DATA]
           ,[CURRENT_DATA]
           ,[NET_VALUE1_NEW]
           ,[NET_VALUE2_NEW]
           ,[NET_VALUE3_NEW]
           ,[NET_VALUE4_NEW]
           ,[NET_VALUE5_NEW]
           ,[NET_VALUE6_NEW]
           ,[NET_VALUE1_CUR]
           ,[NET_VALUE2_CUR]
           ,[NET_VALUE3_CUR]
           ,[NET_VALUE4_CUR]
           ,[NET_VALUE5_CUR]
           ,[NET_VALUE6_CUR]
           ,[SEARCH1]
           --,[SEARCH2]
           --,[SEARCH3]
           --,[SEARCH4]
           --,[SEARCH5]
           --,[SEARCH6]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK] 
           ,[EVENT_ACTION_CR_FK] 
           ,[RESULT_TYPE_CR_FK]
           ,[SINGLE_ASSOC_IND]
           ,[JOB_CLASS_FK]
           ,[CALC_JOB_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[IMPORT_BATCH_NO]
           ,[BATCH_NO]
           ,[STG_RECORD_FK]
           ,[TABLE_ID_FK]
           ,[RULES_FK]
           ,[RELATED_EVENT_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
           )
SELECT 
            EDH.EVENT_TYPE_CR_FK
           ,EDH.EVENT_ENTITY_CLASS_CR_FK
           ,EDH.EVENT_EFFECTIVE_DATE
           ,EDH.EPACUBE_ID
           ,EDH.DATA_NAME_FK
           ,EDH.PRODUCT_STRUCTURE_FK
           ,EDH.ORG_ENTITY_STRUCTURE_FK
           ,EDH.ENTITY_CLASS_CR_FK
           ,EDH.ENTITY_DATA_NAME_FK
           ,EDH.ENTITY_STRUCTURE_FK
           ,EDH.LEVEL_SEQ
           ,EDH.PARENT_STRUCTURE_FK
           ,EDH.ASSOC_PARENT_CHILD
           ,'-999999999'    ----  NEW_DATA FOR PURGE
           ,EDH.NEW_DATA     ----  CURRENT_DATA
           ,-999999999      ----  NET_VALUE1_NEW
           ,-999999999      ----  NET_VALUE2_NEW
           ,-999999999      ----  NET_VALUE3_NEW
           ,-999999999      ----  NET_VALUE4_NEW
           ,-999999999      ----  NET_VALUE5_NEW
           ,-999999999      ----  NET_VALUE6_NEW
           ,EDH.NET_VALUE1_NEW
           ,EDH.NET_VALUE2_NEW
           ,EDH.NET_VALUE3_NEW
           ,EDH.NET_VALUE4_NEW
           ,EDH.NET_VALUE5_NEW
           ,EDH.NET_VALUE6_NEW
           ,EDH.SEARCH1
           --,EDH.SEARCH2
           --,EDH.SEARCH3
           --,EDH.SEARCH4
           --,EDH.SEARCH5
           --,EDH.SEARCH6
           ,97             ----   EVENT_CONDITION_CR_FK
           ,87             ----   EVENT_STATUS_CR_FK
           ,EDH.EVENT_PRIORITY_CR_FK
           ,78             ----   EVENT_SOURCE_CR_FK FOR UNDO
           ,53             ----   EVENT_ACTION_CR_FK FOR INACTIVE
           ,710            ----   RESULT_TYPE_CR_FK
           ,EDH.SINGLE_ASSOC_IND
           ,EDH.JOB_CLASS_FK
           ,EDH.CALC_JOB_FK
           ,EDH.IMPORT_JOB_FK
           ,EDH.IMPORT_DATE
           ,EDH.IMPORT_PACKAGE_FK
           ,EDH.IMPORT_FILENAME
           ,EDH.IMPORT_BATCH_NO
           ,EDH.BATCH_NO
           ,EDH.STG_RECORD_FK
           ,EDH.TABLE_ID_FK
           ,EDH.RULES_FK
           ,EDH.RELATED_EVENT_FK
           ,EDH.RECORD_STATUS_CR_FK
           ,EDH.UPDATE_TIMESTAMP
           ,EDH.UPDATE_USER
FROM synchronizer.EVENT_DATA_HISTORY EDH WITH (NOLOCK)
LEFT JOIN SYNCHRONIZER.EVENT_DATA EDINACTIVE WITH (NOLOCK)
 ON   (EDINACTIVE.BATCH_NO = EDH.BATCH_NO
 AND   EDINACTIVE.DATA_NAME_FK = EDH.DATA_NAME_FK 
 AND   EDINACTIVE.PRODUCT_STRUCTURE_FK = EDH.PRODUCT_STRUCTURE_FK 
 AND   EDINACTIVE.ORG_ENTITY_STRUCTURE_FK = EDH.ORG_ENTITY_STRUCTURE_FK 
 AND   EDINACTIVE.EVENT_ACTION_CR_FK = 53   --- INACTIVE
 AND   ISNULL ( EDINACTIVE.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( EDH.ENTITY_STRUCTURE_FK, 0 ) )
WHERE 1 = 1	
AND   EDH.BATCH_NO = @in_batch_no
AND   EDH.EVENT_STATUS_CR_FK = 81  -- APPROVED ONLY
AND   EDH.EVENT_ACTION_CR_FK = 51  -- NEW VALUES ONLY
AND   EDH.EVENT_SOURCE_CR_FK <> 78 -- DO NOT ALLOW UNDO OF UNDO
AND   EDINACTIVE.EVENT_ID IS NULL -- NEW VALUE EVENT WAS NOT PREVIOUSLY CREATED AS INACTIVE
           


------------------------------------------------------------------------------------------
--   Create CHANGE events from previous Approved Statuses
------------------------------------------------------------------------------------------


--  CREATE CHANGE EVENT

INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[ENTITY_CLASS_CR_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[ENTITY_STRUCTURE_FK]
           ,[LEVEL_SEQ]
           ,[PARENT_STRUCTURE_FK]
           ,[ASSOC_PARENT_CHILD]
           ,[NEW_DATA]
           ,[CURRENT_DATA]
           ,[NET_VALUE1_NEW]
           ,[NET_VALUE2_NEW]
           ,[NET_VALUE3_NEW]
           ,[NET_VALUE4_NEW]
           ,[NET_VALUE5_NEW]
           ,[NET_VALUE6_NEW]
           ,[NET_VALUE1_CUR]
           ,[NET_VALUE2_CUR]
           ,[NET_VALUE3_CUR]
           ,[NET_VALUE4_CUR]
           ,[NET_VALUE5_CUR]
           ,[NET_VALUE6_CUR]
           ,[SEARCH1]
           --,[SEARCH2]
           --,[SEARCH3]
           --,[SEARCH4]
           --,[SEARCH5]
           --,[SEARCH6]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK] 
           ,[EVENT_ACTION_CR_FK] 
           ,[RESULT_TYPE_CR_FK]
           ,[SINGLE_ASSOC_IND]
           ,[JOB_CLASS_FK]
           ,[CALC_JOB_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[IMPORT_BATCH_NO]
           ,[BATCH_NO]
           ,[STG_RECORD_FK]
           ,[TABLE_ID_FK]
           ,[RULES_FK]
           ,[RELATED_EVENT_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
           )
SELECT 
            EDH.EVENT_TYPE_CR_FK
           ,EDH.EVENT_ENTITY_CLASS_CR_FK
           ,EDH.EVENT_EFFECTIVE_DATE
           ,EDH.EPACUBE_ID
           ,EDH.DATA_NAME_FK
           ,EDH.PRODUCT_STRUCTURE_FK
           ,EDH.ORG_ENTITY_STRUCTURE_FK
           ,EDH.ENTITY_CLASS_CR_FK
           ,EDH.ENTITY_DATA_NAME_FK
           ,EDH.ENTITY_STRUCTURE_FK
           ,EDH.LEVEL_SEQ
           ,EDH.PARENT_STRUCTURE_FK
           ,EDH.ASSOC_PARENT_CHILD
           ,EDH.CURRENT_DATA
           ,EDH.NEW_DATA           
           ,EDH.NET_VALUE1_CUR
           ,EDH.NET_VALUE2_CUR
           ,EDH.NET_VALUE3_CUR
           ,EDH.NET_VALUE4_CUR
           ,EDH.NET_VALUE5_CUR
           ,EDH.NET_VALUE6_CUR           
           ,EDH.NET_VALUE1_NEW
           ,EDH.NET_VALUE2_NEW
           ,EDH.NET_VALUE3_NEW
           ,EDH.NET_VALUE4_NEW
           ,EDH.NET_VALUE5_NEW
           ,EDH.NET_VALUE6_NEW
           ,EDH.SEARCH1
           --,EDH.SEARCH2
           --,EDH.SEARCH3
           --,EDH.SEARCH4
           --,EDH.SEARCH5
           --,EDH.SEARCH6
           ,97             ----   EVENT_CONDITION_CR_FK
           ,87             ----   EVENT_STATUS_CR_FK
           ,EDH.EVENT_PRIORITY_CR_FK
           ,78             ----   EVENT_SOURCE_CR_FK FOR UNDO
           ,52             ----   EVENT_ACTION_CR_FK FOR CHANGE
           ,710            ----   RESULT_TYPE_CR_FK
           ,EDH.SINGLE_ASSOC_IND
           ,EDH.JOB_CLASS_FK
           ,EDH.CALC_JOB_FK
           ,EDH.IMPORT_JOB_FK
           ,EDH.IMPORT_DATE
           ,EDH.IMPORT_PACKAGE_FK
           ,EDH.IMPORT_FILENAME
           ,EDH.IMPORT_BATCH_NO
           ,EDH.BATCH_NO
           ,EDH.STG_RECORD_FK
           ,EDH.TABLE_ID_FK
           ,EDH.RULES_FK
           ,EDH.RELATED_EVENT_FK
           ,EDH.RECORD_STATUS_CR_FK
           ,EDH.UPDATE_TIMESTAMP
           ,EDH.UPDATE_USER
FROM synchronizer.EVENT_DATA_HISTORY EDH
WHERE 1 = 1
AND   EDH.BATCH_NO = @in_batch_no
AND   EDH.EVENT_STATUS_CR_FK = 81  -- APPROVED ONLY
AND   EDH.EVENT_ACTION_CR_FK = 52  -- CHANGE EVENTS
AND   EDH.EVENT_SOURCE_CR_FK <> 78 -- DO NOT ALLOW UNDO OF UNDO



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_FACTORY_UNDO'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @in_batch_no
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_FACTORY_UNDO has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END








