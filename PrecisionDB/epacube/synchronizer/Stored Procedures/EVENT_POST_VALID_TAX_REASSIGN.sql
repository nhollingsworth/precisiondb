﻿








-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform general event validations to all event types
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        07/11/2000   Initial SQL Version
-- RG		 02/20/2013	  Removing checks/migrations to support multiple trees



CREATE PROCEDURE [synchronizer].[EVENT_POST_VALID_TAX_REASSIGN] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int

DECLARE @GreenCondition		int
DECLARE @YellowCondition	int
DECLARE @RedCondition		int
DECLARE @BatchNo			bigint
DECLARE @ThisEvent		    bigint
DECLARE @EventsTree			int
DECLARE @OldNode			bigint
DECLARE @NewNode			bigint
DECLARE @ProductEventType   int


set @GreenCondition = epacube.getCodeRefId('EVENT_CONDITION','GREEN')
set @YellowCondition = epacube.getCodeRefId('EVENT_CONDITION','YELLOW')
set @RedCondition = epacube.getCodeRefId('EVENT_CONDITION','RED')
set @BatchNo = (select distinct batch_no from #TS_EVENT_DATA)
set @ProductEventType = epacube.getCodeRefId('EVENT_TYPE','PRODUCT')

BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_TAX_REASSIGN.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;



--------------------------------------------------------------------------------------------
--  CREATE TEMP TABLES
--------------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_TAX_OLD') is not null
	   drop table #TS_TAX_OLD
	   
	   
	-- create temp table
CREATE TABLE #TS_TAX_OLD(
	TS_TAX_OLD_ID bigint IDENTITY(1000,1) NOT NULL,
	EVENT_FK bigint NULL,
	TAXONOMY_NODE_FK int NULL,
	DATA_NAME_FK int NULL,
	PTAX_ATTRIBUTE_EVENT_DATA VARCHAR(256) NULL,
	PTAX_DATA_VALUE_FK  BIGINT NULL,
	PTAX_TABLE_ID_FK BIGINT NULL
    )


--drop temp table if it exists
	IF object_id('tempdb..#TS_TAX_NEW') is not null
	   drop table #TS_TAX_NEW
	   	   
	-- create temp table
CREATE TABLE #TS_TAX_NEW(
	TS_TAX_NEW_ID bigint IDENTITY(1000,1) NOT NULL,
	EVENT_FK bigint NULL,
	TAXONOMY_NODE_FK int NULL,
	DATA_NAME_FK int NULL,
	TAXONOMY_NODE_ATTRIBUTE_ID BIGINT NULL,	
	RESTRICTED_DATA_VALUES_IND SMALLINT NULL
    )



--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------
----
----	CREATE CLUSTERED INDEX IDX_TSED_EF ON #TS_EVENT_DATA 
----	(EVENT_FK ASC)	
----	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)
----
----	CREATE NONCLUSTERED INDEX IDX_TSED_EPA ON #TS_EVENT_DATA 
----	(EPACUBE_ID ASC)
----	INCLUDE ( EVENT_FK )	
----	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)
----
----
----	CREATE NONCLUSTERED INDEX IDX_TSED_DNF_OESF_ESF_IND ON #TS_EVENT_DATA 
----	(
----		DATA_NAME_FK ASC,
----		ORG_ENTITY_STRUCTURE_FK ASC,
----		ENTITY_STRUCTURE_FK ASC
----	)
----	INCLUDE ( NEW_DATA )
----		WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)
----




-----------------------------------------------------------------------------------
--- Insert Temp Rows for OLD Node Assignment
-----------------------------------------------------------------------------------

INSERT INTO #TS_TAX_OLD (
	EVENT_FK,
	TAXONOMY_NODE_FK,
	DATA_NAME_FK,
	PTAX_ATTRIBUTE_EVENT_DATA,
	PTAX_DATA_VALUE_FK,
	PTAX_TABLE_ID_FK
    )
SELECT 
	ED.EVENT_FK,
	TN.TAXONOMY_NODE_ID,
	TNA.DATA_NAME_FK,
	PTAX.ATTRIBUTE_EVENT_DATA,
	PTAX.DATA_VALUE_FK,
	PTAX.PRODUCT_TAX_ATTRIBUTE_ID
----
FROM #TS_EVENT_DATA ed  WITH (NOLOCK)
INNER JOIN epacube.TAXONOMY_TREE tt WITH (NOLOCK) ON ( tt.TAX_NODE_DN_FK = ed.data_name_fk )
INNER JOIN epacube.TAXONOMY_NODE tn WITH (NOLOCK) ON ( tn.TAXONOMY_TREE_FK = tt.TAXONOMY_TREE_ID
                                                  AND  tn.TAX_NODE_DV_FK =  (  SELECT dv.DATA_VALUE_ID 
                                                                               FROM epacube.data_value dv 
                                                                               WHERE dv.DATA_NAME_FK = ed.data_name_fk 
                                                                               AND   dv.value = ed.current_data ))                                   
INNER JOIN epacube.taxonomy_node_attribute tna  WITH (NOLOCK) ON ( tna.TAXONOMY_NODE_FK = tn.TAXONOMY_NODE_ID )
INNER JOIN epacube.product_tax_attribute ptax  WITH (NOLOCK) ON  ( ptax.product_structure_fk = ed.product_structure_fk 
                                                             AND   ptax.data_name_fk = tna.data_name_fk )
WHERE 1 = 1
AND   ED.CURRENT_DATA IS NOT NULL
AND   ED.NEW_DATA <> ISNULL ( ED.CURRENT_DATA, 'NULL DATA' )


-----------------------------------------------------------------------------------
--- Insert Temp Rows for NEW Node Assignment
-----------------------------------------------------------------------------------

INSERT INTO #TS_TAX_NEW (
	EVENT_FK,
	TAXONOMY_NODE_FK,
	DATA_NAME_FK,
	TAXONOMY_NODE_ATTRIBUTE_ID,	
	RESTRICTED_DATA_VALUES_IND
    )
SELECT 
	ED.EVENT_FK,
	TN.TAXONOMY_NODE_ID,
	TNA.DATA_NAME_FK,	
	TNA.TAXONOMY_NODE_ATTRIBUTE_ID,
	TNA.RESTRICTED_DATA_VALUES_IND
----
FROM #TS_EVENT_DATA ed  WITH (NOLOCK)
INNER JOIN epacube.TAXONOMY_TREE tt WITH (NOLOCK) ON ( tt.TAX_NODE_DN_FK = ed.data_name_fk )
INNER JOIN epacube.TAXONOMY_NODE tn WITH (NOLOCK) ON ( tn.TAXONOMY_TREE_FK = tt.TAXONOMY_TREE_ID
                                                  AND  tn.TAX_NODE_DV_FK =  (  SELECT dv.DATA_VALUE_ID 
                                                                               FROM epacube.data_value dv 
                                                                               WHERE dv.DATA_NAME_FK = ed.data_name_fk 
                                                                               AND   dv.value = ed.new_data ))                                   
INNER JOIN epacube.taxonomy_node_attribute tna  WITH (NOLOCK) ON ( tna.TAXONOMY_NODE_FK = tn.TAXONOMY_NODE_ID )
WHERE 1 = 1
AND   ED.CURRENT_DATA IS NOT NULL
AND   ED.NEW_DATA <> ISNULL ( ED.CURRENT_DATA, 'NULL DATA' )






-------------------------------------------------------------------------------------
----- Check to see if Attribute is allowed for new Node Assignment
-------------------------------------------------------------------------------------

--INSERT INTO #TS_EVENT_DATA_ERRORS (
--									EVENT_FK,
--									RULE_FK,
--									EVENT_CONDITION_CR_FK,
--									ERROR_NAME,
--									ERROR_DATA1,
--									ERROR_DATA2,
--									ERROR_DATA3,
--									ERROR_DATA4,
--									ERROR_DATA5,
--									ERROR_DATA6,
--									UPDATE_TIMESTAMP)
							
--SELECT 
-- TTO.EVENT_FK
--,null
--,@YellowCondition
--, 'Taxonomy Node Reassignment Attribute Resulted In>> '
--, 'Product Taxonomy Attribute will not be carried over '
--, (select label from epacube.data_name where data_name_id = TTO.DATA_NAME_FK )
--, TTO.PTAX_ATTRIBUTE_EVENT_DATA
--,NULL
--,null
--,null
--,getdate()
------
--FROM #TS_TAX_OLD  TTO
--LEFT JOIN #TS_TAX_NEW TTN ON ( TTN.EVENT_FK = TTO.EVENT_FK 
--                           AND  TTN.DATA_NAME_FK = TTO.DATA_NAME_FK )
--WHERE 1 = 1
--AND   TTN.DATA_NAME_FK IS NULL   --- OLD NODE ATTRIBUTE DOES NOT EXIST IN LIST OF NEW NODE ATTRIBUTES


------ CREATE a PRE_POST EVENT for the INACTIVATION OF THE PRODUCT_TAX_ATTIBUTE

--INSERT INTO synchronizer.EVENT_DATA 
--		(   
--			EVENT_TYPE_CR_FK,
--			EVENT_ENTITY_CLASS_CR_FK,
--			EPACUBE_ID,
--			IMPORT_JOB_FK,
--			DATA_NAME_FK,
--			PRODUCT_STRUCTURE_FK,
--			ORG_ENTITY_STRUCTURE_FK,
--			ENTITY_STRUCTURE_FK,
--			NEW_DATA,
--			CURRENT_DATA,
--			EVENT_EFFECTIVE_DATE,
--			EVENT_SOURCE_CR_FK,							
--			EVENT_PRIORITY_CR_FK,
--			EVENT_ACTION_CR_FK,
--			EVENT_STATUS_CR_FK,
--			EVENT_CONDITION_CR_FK,									
--			RESULT_TYPE_CR_FK,
--			TABLE_ID_FK,
--			BATCH_NO,
--			IMPORT_BATCH_NO,
--			RELATED_EVENT_FK,
--			RECORD_STATUS_CR_FK,
--			UPDATE_TIMESTAMP		
--			 ) 							 							 
--   SELECT
--			153,   --  EVENT_TYPE_CR_FK = PRODUCT_TAX_ATTRIBUTE
--			ED.EVENT_ENTITY_CLASS_CR_FK,
--			ED.EPACUBE_ID,
--			ED.IMPORT_JOB_FK,
--			TTO.DATA_NAME_FK,    --- PRODUCT_TAX_ATTRIBUTE.DATA_NAME_FK
--			ED.PRODUCT_STRUCTURE_FK,
--			1,           -- ORG_ENTITY_STRUCTURE_FK,
--            NULL,        -- ENTITY_STRUCTURE_FK,
--            -999999999,  -- NEW_DATA
--			( SELECT PTAX.ATTRIBUTE_EVENT_DATA
--			  FROM epacube.PRODUCT_TAX_ATTRIBUTE PTAX 
--			  WHERE PTAX.DATA_NAME_FK = TTO.DATA_NAME_FK
--			  AND  ISNULL ( PTAX.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( ED.PRODUCT_STRUCTURE_FK, 0 ) ),
--			@L_SYSDATE,  -- EVENT_EFFECTIVE_DATE
--			72,          -- EVENT_SOURCE_CR_FK = DERIVED
--			61,          -- EVENT_PRIORITY_CR_FK,
--			53,          -- EVENT_ACTION_CR_FK = INACTIVATE
--			83,          -- EVENT_STATUS_CR_FK =  PREPOST
--			98,          -- EVENT_CONDTION_CR_FK = WARNING
--			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
--			TTO.PTAX_TABLE_ID_FK,
--			ED.BATCH_NO,
--			ED.IMPORT_BATCH_NO,
--			ED.EVENT_FK,
--			2,           -- RECORD_STATUS_CR_FK,
--			@L_SYSDATE
----------
--FROM #TS_EVENT_DATA ed  WITH (NOLOCK)
--INNER JOIN #TS_TAX_OLD  TTO ON ( TTO.EVENT_FK = ED.EVENT_FK )
--LEFT JOIN #TS_TAX_NEW TTN ON ( TTN.EVENT_FK = TTO.EVENT_FK 
--                           AND  TTN.DATA_NAME_FK = TTO.DATA_NAME_FK )
--WHERE 1 = 1
--AND   TTN.DATA_NAME_FK IS NULL   --- OLD NODE ATTRIBUTE DOES NOT EXIST IN LIST OF NEW NODE ATTRIBUTES
			 


------ UPDATE THE PRODUCT_TAX_ATTRIBUTE AS INACTIVE ( WILL PURGE LATER )

--UPDATE epacube.PRODUCT_TAX_ATTRIBUTE
--SET RECORD_STATUS_CR_FK = 2
--WHERE PRODUCT_TAX_ATTRIBUTE_ID IN ( 
----------
--	SELECT TTO.PTAX_TABLE_ID_FK 
--	FROM #TS_EVENT_DATA ed  WITH (NOLOCK)
--	INNER JOIN #TS_TAX_OLD  TTO ON ( TTO.EVENT_FK = ED.EVENT_FK )
--	LEFT JOIN #TS_TAX_NEW TTN ON ( TTN.EVENT_FK = TTO.EVENT_FK 
--							   AND  TTN.DATA_NAME_FK = TTO.DATA_NAME_FK )
--	WHERE 1 = 1
--	AND   TTN.DATA_NAME_FK IS NULL   --- OLD NODE ATTRIBUTE DOES NOT EXIST IN LIST OF NEW NODE ATTRIBUTES
--	)


-------------------------------------------------------------------------------------
----- Notify that new node Attribute does not allow the old data value
-------------------------------------------------------------------------------------

--INSERT INTO #TS_EVENT_DATA_ERRORS (
--									EVENT_FK,
--									RULE_FK,
--									EVENT_CONDITION_CR_FK,
--									ERROR_NAME,
--									ERROR_DATA1,
--									ERROR_DATA2,
--									ERROR_DATA3,
--									ERROR_DATA4,
--									ERROR_DATA5,
--									ERROR_DATA6,
--									UPDATE_TIMESTAMP)
							
--SELECT 
-- TTO.EVENT_FK
--,null
--,@YellowCondition
--, 'Taxonomy Node Reassignment Attribute Resulted In>> '
--, 'Product Taxonomy Attribute Value not in New Node Restricted List '
--, (select label from epacube.data_name where data_name_id = TTO.data_name_fk)
--, TTO.PTAX_ATTRIBUTE_EVENT_DATA
--,NULL
--,null
--,null
--,getdate()
------
--FROM #TS_TAX_OLD  TTO
--INNER JOIN #TS_TAX_NEW TTN ON ( TTN.EVENT_FK = TTO.EVENT_FK 
--                           AND  TTN.DATA_NAME_FK = TTO.DATA_NAME_FK )
--WHERE 1 = 1
--AND   TTN.RESTRICTED_DATA_VALUES_IND = 1   --- OLD AND NEW ATTRIBUTE EXIST BUT CHECK RESTRICTED VALUES
-----
--AND   TTO.PTAX_DATA_VALUE_FK NOT IN (SELECT TNADV.DATA_VALUE_FK
--									 FROM epacube.TAXONOMY_NODE_ATTRIBUTE_DV TNADV WITH (NOLOCK)
--									 WHERE TNADV.TAXONOMY_NODE_ATTRIBUTE_FK = TTN.TAXONOMY_NODE_ATTRIBUTE_ID )



------ CREATE a PRE_POST EVENT for the INACTIVATION OF THE PRODUCT_TAX_ATTIBUTE

--INSERT INTO synchronizer.EVENT_DATA 
--		(   
--			EVENT_TYPE_CR_FK,
--			EVENT_ENTITY_CLASS_CR_FK,
--			EPACUBE_ID,
--			IMPORT_JOB_FK,
--			DATA_NAME_FK,
--			PRODUCT_STRUCTURE_FK,
--			ORG_ENTITY_STRUCTURE_FK,
--			ENTITY_STRUCTURE_FK,
--			NEW_DATA,
--			CURRENT_DATA,
--			EVENT_EFFECTIVE_DATE,
--			EVENT_SOURCE_CR_FK,							
--			EVENT_PRIORITY_CR_FK,
--			EVENT_ACTION_CR_FK,
--			EVENT_STATUS_CR_FK,
--			EVENT_CONDITION_CR_FK,									
--			RESULT_TYPE_CR_FK,
--			TABLE_ID_FK,
--			BATCH_NO,
--			IMPORT_BATCH_NO,
--			RELATED_EVENT_FK,
--			RECORD_STATUS_CR_FK,
--			UPDATE_TIMESTAMP		
--			 ) 							 							 
--   SELECT
--			153,   --  EVENT_TYPE_CR_FK = PRODUCT_TAX_ATTRIBUTE
--			ED.EVENT_ENTITY_CLASS_CR_FK,
--			ED.EPACUBE_ID,
--			ED.IMPORT_JOB_FK,
--			TTO.DATA_NAME_FK,    --- PRODUCT_TAX_ATTRIBUTE.DATA_NAME_FK
--			ED.PRODUCT_STRUCTURE_FK,
--			1,           -- ORG_ENTITY_STRUCTURE_FK,
--            NULL,        -- ENTITY_STRUCTURE_FK,
--            -999999999,  -- NEW_DATA
--			( SELECT PTAX.ATTRIBUTE_EVENT_DATA
--			  FROM epacube.PRODUCT_TAX_ATTRIBUTE PTAX 
--			  WHERE PTAX.DATA_NAME_FK = TTO.DATA_NAME_FK
--			  AND  ISNULL ( PTAX.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( ED.PRODUCT_STRUCTURE_FK, 0 ) ),
--			@L_SYSDATE,  -- EVENT_EFFECTIVE_DATE
--			72,          -- EVENT_SOURCE_CR_FK = DERIVED
--			61,          -- EVENT_PRIORITY_CR_FK,
--			53,          -- EVENT_ACTION_CR_FK = INACTIVATE
--			83,          -- EVENT_STATUS_CR_FK =  PREPOST
--			98,          -- EVENT_CONDTION_CR_FK = WARNING
--			ISNULL ( ED.RESULT_TYPE_CR_FK, 711 ),
--			TTO.PTAX_TABLE_ID_FK,
--			ED.BATCH_NO,
--			ED.IMPORT_BATCH_NO,
--			ED.EVENT_FK,
--			2,           -- RECORD_STATUS_CR_FK,
--			@L_SYSDATE
----------
--FROM #TS_EVENT_DATA ed  WITH (NOLOCK)
--INNER JOIN #TS_TAX_OLD  TTO ON ( TTO.EVENT_FK = ED.EVENT_FK )
--INNER JOIN #TS_TAX_NEW TTN ON ( TTN.EVENT_FK = TTO.EVENT_FK 
--                           AND  TTN.DATA_NAME_FK = TTO.DATA_NAME_FK )
--WHERE 1 = 1
--AND   TTN.RESTRICTED_DATA_VALUES_IND = 1   --- OLD AND NEW ATTRIBUTE EXIST BUT CHECK RESTRICTED VALUES
-----
--AND   TTO.PTAX_DATA_VALUE_FK NOT IN (SELECT TNADV.DATA_VALUE_FK
--									 FROM epacube.TAXONOMY_NODE_ATTRIBUTE_DV TNADV WITH (NOLOCK)
--									 WHERE TNADV.TAXONOMY_NODE_ATTRIBUTE_FK = TTN.TAXONOMY_NODE_ATTRIBUTE_ID )


------ UPDATE THE PRODUCT_TAX_ATTRIBUTE AS INACTIVE ( WILL PURGE LATER )

--UPDATE epacube.PRODUCT_TAX_ATTRIBUTE
--SET RECORD_STATUS_CR_FK = 2
--WHERE PRODUCT_TAX_ATTRIBUTE_ID IN ( 
---------
--	SELECT TTO.PTAX_TABLE_ID_FK
--	FROM #TS_TAX_OLD  TTO
--	INNER JOIN #TS_TAX_NEW TTN ON ( TTN.EVENT_FK = TTO.EVENT_FK 
--							   AND  TTN.DATA_NAME_FK = TTO.DATA_NAME_FK )
--	WHERE 1 = 1
--	AND   TTN.RESTRICTED_DATA_VALUES_IND = 1   --- OLD AND NEW ATTRIBUTE EXIST BUT CHECK RESTRICTED VALUES
--	---
--	AND   TTO.PTAX_DATA_VALUE_FK NOT IN (SELECT TNADV.DATA_VALUE_FK
--										 FROM epacube.TAXONOMY_NODE_ATTRIBUTE_DV TNADV WITH (NOLOCK)
--										 WHERE TNADV.TAXONOMY_NODE_ATTRIBUTE_FK = TTN.TAXONOMY_NODE_ATTRIBUTE_ID )
--    )




-------------------------------------------------------------------------------------
----- Notify that new node Attribute is missing with new Node Assignment
-------------------------------------------------------------------------------------

--INSERT INTO #TS_EVENT_DATA_ERRORS (
--									EVENT_FK,
--									RULE_FK,
--									EVENT_CONDITION_CR_FK,
--									ERROR_NAME,
--									ERROR_DATA1,
--									ERROR_DATA2,
--									ERROR_DATA3,
--									ERROR_DATA4,
--									ERROR_DATA5,
--									ERROR_DATA6,
--									UPDATE_TIMESTAMP)
							
--SELECT 
-- TTN.EVENT_FK
--,null
--,@YellowCondition
--, 'Taxonomy Node Reassignment Attribute Resulted In>> '
--, 'Product Taxonomy Attribute is Missing for New Node '
--, (select label from epacube.data_name where data_name_id = TTN.data_name_fk)
--,NULL
--,null
--,null
--,null
--,getdate()
------
--FROM #TS_TAX_NEW  TTN
--LEFT JOIN #TS_TAX_NEW TTO  ON ( TTN.EVENT_FK = TTO.EVENT_FK 
--                           AND  TTN.DATA_NAME_FK = TTO.DATA_NAME_FK )
--WHERE 1 = 1
--AND   TTN.DATA_NAME_FK <> ISNULL ( TTO.DATA_NAME_FK, 0 )




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_TAX_REASSIGN'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_TAX_REASSIGN has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END









