﻿



-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing for very large imports.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/06/2010   Initial SQL Version
-- KS        02/27/2012   Changed Logic to create Temp Table #TS_RULE_PARENT_EVENTS
--                        within this procedure instead of calling procedure to free-up memory
--CV         06/24/2012   add event_source_cr_fk #TS_RULE_PARENT_EVENTS  
-- KS        10/11/2012   Log Message if not Cost Changss occured EPA-3412
-- KS        10/26/2012   For Performance removing the No Change and All NULL Rows from the Results prior to Posting
-- CV        04/14/2014   added top 1 to get effective data and end date  from event calc table
-- CV        05/02/2014   Adding customer specific calls
-- CV        09/02/2014   moved down call for getting effective dates
-- CV        01/13/2017   Rule type cr fk from rules table

CREATE PROCEDURE [synchronizer].[EVENT_POSTING_EVENT_CALC_RESULTS] 
        ( @in_analysis_job_fk BIGINT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT
DECLARE  @x_count			  BIGINT
DECLARE  @v_batch_no          BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime
DECLARE  @V_END_TIMESTAMP datetime
DECLARE  @V_START_TIMESTAMP datetime
DECLARE  @v_entity_class_cr_fk  int

DECLARE  @v_import_batch_size    INT
DECLARE  @v_import_max_prods     INT
DECLARE  @v_import_max_whse      INT

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_EVENT_CALC_RESULTS.'
					 + ' JOB: '   + cast(isnull(@in_analysis_job_fk, 0) as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;



------            exec sp_executesql 	@ls_stmt  USE TO THROW ERROR

SET @l_sysdate = GETDATE()

SET @v_import_batch_size =  ISNULL (
                            ( SELECT CAST ( EEP.VALUE AS INT )
                              FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK)
                              WHERE APPLICATION_SCOPE_FK = 100
                              AND   NAME = 'IMPORT BATCH SIZE' ), 500000 )


------------------------------------------------------------------------------------------
--   Save this code snippet for causing immediate stop
------------------------------------------------------------------------------------------

----            SET @ls_exec = 'STOP CODE HERE'            
----            INSERT INTO COMMON.T_SQL
----                ( TS,  SQL_TEXT )
----            VALUES ( GETDATE(), ISNULL ( @ls_exec, @status_desc ) )
----            exec sp_executesql 	@ls_exec;




		
------------------------------------------------------------------------------------------
--   Create Temp Table for Parent Events EVENT_DATA 
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_PARENT_EVENTS') is not null
	   drop table #TS_RULE_PARENT_EVENTS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_RULE_PARENT_EVENTS(
	    TS_RULE_PARENT_TRIGGER_EVENTS_ID             BIGINT IDENTITY(1000,1) NOT NULL,
		RESULT_PARENT_DATA_NAME_FK  INT NULL,						    
	    PRODUCT_STRUCTURE_FK		BIGINT NULL,
	    ORG_ENTITY_STRUCTURE_FK		BIGINT NULL
	    ,EVENT_SOURCE_CR_FK          INT NULL
		)


---- CORP_IND = 1
	INSERT INTO #TS_RULE_PARENT_EVENTS (
		RESULT_PARENT_DATA_NAME_FK,
		PRODUCT_STRUCTURE_FK,
		ORG_ENTITY_STRUCTURE_FK
		,EVENT_SOURCE_CR_FK 
		)
	SELECT DISTINCT 
		dn.PARENT_DATA_NAME_FK,
		ec.PRODUCT_STRUCTURE_FK,
		1,  ---- ORG_ENTITY_STRUCTURE_FK
        ec.EVENT_SOURCE_CR_FK
	FROM synchronizer.event_calc ec with (nolock)
    INNER JOIN epacube.data_name dn with (nolock) 
      on dn.data_name_id = ec.result_data_name_fk 
	INNER JOIN epacube.DATA_NAME DNP WITH (NOLOCK)
	 ON ( DNP.DATA_NAME_ID = dn.PARENT_DATA_NAME_FK )
    INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
     ON ( DS.DATA_SET_ID = DNP.DATA_SET_FK )	 	
	WHERE 1 = 1
	AND   DS.CORP_IND = 1
    AND   ec.job_fk = @in_analysis_job_fk


---- ORG_IND = 1
	INSERT INTO #TS_RULE_PARENT_EVENTS (
		RESULT_PARENT_DATA_NAME_FK,
		PRODUCT_STRUCTURE_FK,
		ORG_ENTITY_STRUCTURE_FK
		,EVENT_SOURCE_CR_FK 
		)
	SELECT DISTINCT 
		DN.PARENT_DATA_NAME_FK,
		PAS.PRODUCT_STRUCTURE_FK,
		PAS.ORG_ENTITY_STRUCTURE_FK
		,ec.EVENT_SOURCE_CR_FK 
	FROM synchronizer.event_calc ec with (nolock)
    INNER JOIN epacube.data_name dn with (nolock) 
      on dn.data_name_id = ec.result_data_name_fk 
	INNER JOIN epacube.DATA_NAME DNP WITH (NOLOCK)
	 ON ( DNP.DATA_NAME_ID = dn.PARENT_DATA_NAME_FK )
    INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
     ON ( DS.DATA_SET_ID = DNP.DATA_SET_FK )	 	
     INNER JOIN EPACUBE.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
     ON ( PAS.DATA_NAME_FK = 159100  ---  PRODUCT WHSE
     AND  PAS.PRODUCT_STRUCTURE_FK = EC.PRODUCT_STRUCTURE_FK  )
	WHERE 1 = 1
	AND   DS.ORG_IND = 1
    AND   ec.job_fk = @in_analysis_job_fk



---- ORG_IND = 1  and need to look for future Prod/Whse too Relationships
	INSERT INTO #TS_RULE_PARENT_EVENTS (
		RESULT_PARENT_DATA_NAME_FK,
		PRODUCT_STRUCTURE_FK,
		ORG_ENTITY_STRUCTURE_FK
		,EVENT_SOURCE_CR_FK 
		)
	SELECT DISTINCT 
		DN.PARENT_DATA_NAME_FK,
		EC.PRODUCT_STRUCTURE_FK,
		EC.ORG_ENTITY_STRUCTURE_FK,
		EC.EVENT_SOURCE_CR_FK 
	FROM synchronizer.event_calc ec with (nolock)
    INNER JOIN epacube.data_name dn with (nolock) 
      on dn.data_name_id = ec.result_data_name_fk 
	INNER JOIN epacube.DATA_NAME DNP WITH (NOLOCK)
	 ON ( DNP.DATA_NAME_ID = dn.PARENT_DATA_NAME_FK )
    INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
     ON ( DS.DATA_SET_ID = DNP.DATA_SET_FK )	 	
    INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
     ON ( ED.DATA_NAME_FK = 159100  ---  PRODUCT WHSE
     AND  ED.PRODUCT_STRUCTURE_FK = EC.PRODUCT_STRUCTURE_FK
     AND  ED.ORG_ENTITY_STRUCTURE_FK = EC.ORG_ENTITY_STRUCTURE_FK
     AND  ED.EVENT_STATUS_CR_FK IN (80, 86, 87, 88 ) 
      )
	WHERE 1 = 1
	AND   DS.ORG_IND = 1
    AND   ec.job_fk = @in_analysis_job_fk



------------------------------------------------------------------------------------------
--   Create Temp Table for Events
--		This table will help with record locking during processing
--
--		Before posting will check for current value <> new value
--		However no longer care about duplicate events because
--		Processing events within a Job.. and each job will not
--      allow for a duplicate event. ( even UI entries ) 
------------------------------------------------------------------------------------------



--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA') is not null
	   drop table #TS_EVENT_DATA;
	   
	   
	-- create temp table  >>>>  VERY IMPORTANT EVENT_FK MUST BE IDENTIFY COLUMN IN EVENT_POSTING_IMPORT
	CREATE TABLE #TS_EVENT_DATA(
		EVENT_FK bigint IDENTITY(1000,1) NOT NULL,
		EVENT_TYPE_CR_FK         INT NULL,
		EVENT_ENTITY_CLASS_CR_FK INT NULL,
		EPACUBE_ID               INT NULL,	
		IMPORT_JOB_FK            BIGINT NULL,	
		IMPORT_PACKAGE_FK        BIGINT NULL,
		DATA_NAME_FK			 INT NULL,	
	    PRODUCT_STRUCTURE_FK     BIGINT NULL,
	    ORG_ENTITY_STRUCTURE_FK  BIGINT NULL,
	    ENTITY_STRUCTURE_FK      BIGINT NULL,
		ENTITY_CLASS_CR_FK        INT NULL,
		ENTITY_DATA_NAME_FK       INT NULL,	 
        PARENT_ENTITY_STRUCTURE_FK  INT NULL,
        CHILD_ENTITY_STRUCTURE_FK  INT NULL,   
	    CUST_ENTITY_STRUCTURE_FK BIGINT NULL,
	    SUPL_ENTITY_STRUCTURE_FK BIGINT NULL,					
		DATA_SET_FK			     INT NULL,				
		DATA_TYPE_CR_FK			 INT NULL,	
		ORG_IND                  SMALLINT NULL,
		INSERT_MISSING_VALUES    INT NULL,		
		TABLE_NAME               VARCHAR (64) NULL,					
        NEW_DATA                  VARCHAR(max) NULL,		
        CURRENT_DATA              VARCHAR(max) NULL,
		DATA_VALUE_FK             BIGINT NULL,
		ENTITY_DATA_VALUE_FK      BIGINT NULL,
        NET_VALUE1_NEW            NUMERIC (18,6) NULL,
        NET_VALUE2_NEW            NUMERIC (18,6) NULL,
        NET_VALUE3_NEW            NUMERIC (18,6) NULL,
        NET_VALUE4_NEW            NUMERIC (18,6) NULL,
        NET_VALUE5_NEW            NUMERIC (18,6) NULL,                                
        NET_VALUE6_NEW            NUMERIC (18,6) NULL,
        NET_VALUE1_CUR            NUMERIC (18,6) NULL,
        NET_VALUE2_CUR            NUMERIC (18,6) NULL,
        NET_VALUE3_CUR            NUMERIC (18,6) NULL,
        NET_VALUE4_CUR            NUMERIC (18,6) NULL,
        NET_VALUE5_CUR            NUMERIC (18,6) NULL,                                
        NET_VALUE6_CUR            NUMERIC (18,6) NULL,
		PERCENT_CHANGE1			  numeric(18, 6) NULL,
		PERCENT_CHANGE2			  numeric(18, 6) NULL,
		PERCENT_CHANGE3			  numeric(18, 6) NULL,
		PERCENT_CHANGE4			  numeric(18, 6) NULL,
		PERCENT_CHANGE5			  numeric(18, 6) NULL,
		PERCENT_CHANGE6			  numeric(18, 6) NULL,
        VALUE_UOM_CODE_FK         INT NULL, 
        ASSOC_UOM_CODE_FK         INT NULL,
        VALUES_SHEET_NAME          VARCHAR(256) NULL,
		UOM_CONVERSION_FACTOR     NUMERIC (18,6) NULL,		
		END_DATE_NEW              DATETIME NULL,				
        END_DATE_CUR              DATETIME NULL,   		
		EFFECTIVE_DATE_CUR        DATETIME NULL,
		EVENT_EFFECTIVE_DATE	  DATETIME NULL,
		VALUE_EFFECTIVE_DATE	  DATETIME NULL,		
        RELATED_EVENT_FK		  BIGINT NULL,
        RULES_FK			      INT NULL,
		EVENT_PRIORITY_CR_FK      INT NOT NULL,		        
		EVENT_ACTION_CR_FK        INT NOT NULL,
		EVENT_STATUS_CR_FK        INT NOT NULL,								        
		EVENT_CONDITION_CR_FK     INT NOT NULL,
		EVENT_SOURCE_CR_FK        INT NOT NULL,				
		RESULT_TYPE_CR_FK	      INT NULL,
		SINGLE_ASSOC_IND          SMALLINT NULL,				
		TABLE_ID_FK               BIGINT NULL,
		BATCH_NO				  BIGINT NULL,
		IMPORT_BATCH_NO           BIGINT NULL,
        JOB_CLASS_FK			  INT NULL,
        IMPORT_DATE               DATETIME NULL,
        IMPORT_FILENAME           VARCHAR(128) NULL,
        STG_RECORD_FK             BIGINT NULL,		
		RECORD_STATUS_CR_FK       INT NULL,
        UPDATE_TIMESTAMP		  DATETIME NULL,
        UPDATE_USER		          VARCHAR(64) NULL,
		PARENT_STRUCTURE_FK       BIGINT
		 )



--------------------------------------------------------------------------------------------
--  CREATE INDEXES 
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSED_EF ON #TS_EVENT_DATA 
	(EVENT_FK ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSED_EPA ON #TS_EVENT_DATA 
	(EPACUBE_ID ASC)
	INCLUDE ( EVENT_FK )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX IDX_TSED_EF_ETCRF ON #TS_EVENT_DATA 
	(EVENT_FK ASC, EVENT_TYPE_CR_FK ASC)
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX IDX_TSED_DNF_OESF_ESF_IND ON #TS_EVENT_DATA 
	(
		DATA_NAME_FK ASC,
		ORG_ENTITY_STRUCTURE_FK ASC,
		ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( NEW_DATA, EVENT_FK )
		WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX IDX_TESSRCH ON #TS_EVENT_DATA 
	(
		DATA_NAME_FK ASC,
		PRODUCT_STRUCTURE_FK ASC,
		ORG_ENTITY_STRUCTURE_FK ASC,
		ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( NEW_DATA, EVENT_FK )
		WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX TESD_20100524_VALID_IMPORT_HOST_CHG ON #TS_EVENT_DATA 
	(
		DATA_NAME_FK ASC,
		EVENT_SOURCE_CR_FK ASC,
		EVENT_ACTION_CR_FK ASC
	)
	INCLUDE ( EVENT_FK )
		WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX TSED_20100120_EVENT_POST_DATA ON #TS_EVENT_DATA 
	(
		EVENT_STATUS_CR_FK ASC,
		EVENT_CONDITION_CR_FK ASC,
		EVENT_ACTION_CR_FK ASC,
		EVENT_TYPE_CR_FK ASC,		
		TABLE_NAME ASC,
		TABLE_ID_FK ASC
	)
	INCLUDE ( [NEW_DATA], [RECORD_STATUS_CR_FK], [UPDATE_TIMESTAMP], [EVENT_FK] )
		WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA_ERRORS') is not null
	   drop table #TS_EVENT_DATA_ERRORS
	   
	   
	-- create temp table
CREATE TABLE #TS_EVENT_DATA_ERRORS(
	TS_EVENT_DATA_ERRORS_ID bigint IDENTITY(1000,1) NOT NULL,
	EVENT_FK bigint NULL,
	RULE_FK int NULL,
	EVENT_CONDITION_CR_FK int NULL,
	DATA_NAME_FK int NULL,
	ERROR_NAME varchar(64) NULL,
	DATA_NAME varchar(64) NULL,
	ERROR_DATA1 varchar(256) NULL,
	ERROR_DATA2 varchar(64) NULL,
	ERROR_DATA3 varchar(64) NULL,
	ERROR_DATA4 varchar(64) NULL,
	ERROR_DATA5 varchar(64) NULL,
	ERROR_DATA6 varchar(64) NULL,
    UPDATE_TIMESTAMP DATETIME NULL
		 )


--------------------------------------------------------------------------------------------
--  CREATE INDEXES 
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSEDE_TEDEID ON #TS_EVENT_DATA_ERRORS 
	(TS_EVENT_DATA_ERRORS_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSEDE_EF_TEDEID ON #TS_EVENT_DATA_ERRORS
	(EVENT_FK ASC)
	INCLUDE ( TS_EVENT_DATA_ERRORS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


	CREATE NONCLUSTERED INDEX IDX_TSEDE_EF_ECCRF ON #TS_EVENT_DATA_ERRORS 
	(EVENT_FK ASC, EVENT_CONDITION_CR_FK ASC)
    INCLUDE ( TS_EVENT_DATA_ERRORS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA_RULES') is not null
	   drop table #TS_EVENT_DATA_RULES
	   
	   
	-- create temp table
CREATE TABLE #TS_EVENT_DATA_RULES(
	[TS_EVENT_RULES_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[EVENT_FK] [bigint] NULL,
	[RULES_FK] [bigint] NULL,
	[RESULT_DATA_NAME_FK] [int] NULL,
	[RESULT_TYPE_CR_FK] [int] NULL,
	[RESULT_EFFECTIVE_DATE] [datetime] NULL,
	[RESULT_RANK] [smallint] NULL,
	[RESULT] [numeric](18, 6) NULL,
	[BASIS1] [numeric](18, 6) NULL,
	[BASIS2] [numeric](18, 6) NULL,
	[OPERAND] [numeric](18, 6) NULL,
	[OPERATION] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BASIS1_DN_FK] [int] NULL,
	[BASIS2_DN_FK] [int] NULL,
	[RULES_ACTION_OPERATION_FK] [int] NULL,
	[RULES_EFFECTIVE_DATE] [DATETIME] NULL,
	[RULE_PRECEDENCE] [smallint] NULL,
	[SCHED_PRECEDENCE] [smallint] NULL,
	[STRUC_PRECEDENCE] [smallint] NULL,
    [RESULT_MTX_ACTION_CR_FK] [int] NULL,
    [SCHED_MTX_ACTION_CR_FK] [int] NULL,
    [UPDATE_TIMESTAMP] [datetime] NULL     
	 ) 			


--------------------------------------------------------------------------------------------
--  CREATE INDEXES 
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSEDR_TEDRID ON #TS_EVENT_DATA_RULES 
	(TS_EVENT_RULES_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSEDR_EF_TEDRID ON #TS_EVENT_DATA_RULES
	(EVENT_FK ASC)
	INCLUDE ( TS_EVENT_RULES_ID, RULES_FK  )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSEDR_EF_ECCRF ON #TS_EVENT_DATA_RULES 
	(EVENT_FK ASC, RESULT_RANK ASC )
    INCLUDE ( TS_EVENT_RULES_ID, RULES_FK, RESULT_DATA_NAME_FK  )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSEDR_EF_SCHP_RP_RF ON #TS_EVENT_DATA_RULES 
	(EVENT_FK ASC, SCHED_PRECEDENCE ASC, RULE_PRECEDENCE ASC, RULES_FK DESC )
    INCLUDE ( TS_EVENT_RULES_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


-------------------------------------------------------------------
---  SHEET RESULTS
-------------------------------------------------------------------	


      DECLARE  @vm$result_parent_data_name_fk   INT
              ,@vm$org_entity_structure_fk      BIGINT              

     
     SET @V_COUNT = ( SELECT COUNT(1) 
				FROM  #TS_RULE_PARENT_EVENTS )

      DECLARE  cur_v_results cursor local FOR
			 SELECT 
			        A.result_parent_data_name_fk,
					A.org_entity_structure_fk				
			  FROM (
				SELECT DISTINCT
				         result_parent_data_name_fk 
					  ,( SELECT CASE WHEN ( @V_COUNT < @v_import_batch_size ) THEN -999
							         ELSE ORG_ENTITY_STRUCTURE_FK
                                     END ) AS org_entity_structure_fk	
				FROM  #TS_RULE_PARENT_EVENTS
			) A
			  ORDER BY A.result_parent_data_name_fk ASC, A.org_entity_structure_fk ASC     
      
              
      
        OPEN cur_v_results;
        FETCH NEXT FROM cur_v_results INTO @vm$result_parent_data_name_fk
                                          ,@vm$org_entity_structure_fk


         WHILE @@FETCH_STATUS = 0 
         BEGIN

		   TRUNCATE TABLE #TS_EVENT_DATA
		   TRUNCATE TABLE #TS_EVENT_DATA_ERRORS
		   TRUNCATE TABLE #TS_EVENT_DATA_RULES      
                         

            SET @status_desc =  'synchronizer.EVENT_POSTING_EVENT_CALC_RESULTS - '
                                + CAST ( ISNULL  ( @vm$result_parent_data_name_fk, 0 ) AS VARCHAR(32) )
                                + ' '
                                + CAST ( ISNULL ( @vm$org_entity_structure_fk, 0 ) AS VARCHAR(32) )
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;



-------------------------------------------------------------------
-- INSERT EVENTS FOR SHEET RESULTS
-------------------------------------------------------------------	

	EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'batch_seq', @v_batch_no output

--if johnstone customer then run for QA
--update result in event calc for list prices based on rounding function here for job id
Declare @CustomerName varchar(25);
SET @CustomerName = (SELECT Value from epacube.EPACUBE_PARAMS where name = 'EPACUBE CUSTOMER')
IF (@CustomerName = 'Johnstone')
begin
	UPDATE synchronizer.EVENT_CALC set result = custom.fn_JSRoundList(result) where RESULT_PARENT_DATA_NAME_FK = 100100404 and JOB_FK = @in_analysis_job_fk

-------------------------------------------------------------------------------
--updating effective dates
-------------------------------------------------------------------------------

	--EXECUTE [synchronizer].[EVENT_DATA_EFFECTIVE_DATES_UPDATE] 
 --  @in_analysis_job_fk 

-----------------------------------------------------------------------------------



end


	INSERT INTO #TS_EVENT_DATA 
		(   
			EVENT_TYPE_CR_FK,
			EVENT_ENTITY_CLASS_CR_FK,
			EPACUBE_ID,
			DATA_NAME_FK,			
			PRODUCT_STRUCTURE_FK,
			ORG_ENTITY_STRUCTURE_FK,
			DATA_SET_FK,
			DATA_TYPE_CR_FK,
			ORG_IND,
			TABLE_NAME,
			NET_VALUE1_NEW,
			NET_VALUE2_NEW,
			NET_VALUE3_NEW,
			NET_VALUE4_NEW,
			NET_VALUE5_NEW,
			NET_VALUE6_NEW,
			NET_VALUE1_CUR,
			NET_VALUE2_CUR,
			NET_VALUE3_CUR,
			NET_VALUE4_CUR,
			NET_VALUE5_CUR,
			NET_VALUE6_CUR,
			ASSOC_UOM_CODE_FK,
			END_DATE_NEW,				
			END_DATE_CUR, 
			EFFECTIVE_DATE_CUR,
			EVENT_EFFECTIVE_DATE,
			EVENT_STATUS_CR_FK,	
			EVENT_CONDITION_CR_FK,
			EVENT_SOURCE_CR_FK,
			EVENT_PRIORITY_CR_FK,
			EVENT_ACTION_CR_FK,
			TABLE_ID_FK,
		    JOB_CLASS_FK,
		    IMPORT_JOB_FK,
		    BATCH_NO,
		    IMPORT_BATCH_NO,
		    RELATED_EVENT_FK,
			RESULT_TYPE_CR_FK,
			RECORD_STATUS_CR_FK,		
			UPDATE_TIMESTAMP,
            UPDATE_USER
			 ) 
	SELECT
			DS.EVENT_TYPE_CR_FK
		   ,DS.ENTITY_CLASS_CR_FK 
		   ,TSRPTE.PRODUCT_STRUCTURE_FK 
		   ,TSRPTE.RESULT_PARENT_DATA_NAME_FK
		   ,TSRPTE.PRODUCT_STRUCTURE_FK
		   ,TSRPTE.ORG_ENTITY_STRUCTURE_FK
		   ,DS.DATA_SET_ID
		   ,DS.DATA_TYPE_CR_FK
		   ,DS.ORG_IND
		   ,DS.TABLE_NAME
	-------
		   ,( SELECT EC.RESULT 				               
			  FROM SYNCHRONIZER.EVENT_CALC EC WITH (NOLOCK)			  
			  WHERE 1 = 1
              AND   EC.JOB_FK                     = @in_analysis_job_fk
	          AND   EC.RESULT_PARENT_DATA_NAME_FK = TSRPTE.RESULT_PARENT_DATA_NAME_FK
              AND   EC.PRODUCT_STRUCTURE_FK       = TSRPTE.PRODUCT_STRUCTURE_FK
              AND   EC.ORG_ENTITY_STRUCTURE_FK    = TSRPTE.ORG_ENTITY_STRUCTURE_FK
	          AND   EC.COLUMN_NAME = 'NET_VALUE1' 
             )	AS RESULT1
		   ,( SELECT EC.RESULT 				               
			  FROM SYNCHRONIZER.EVENT_CALC EC WITH (NOLOCK)			  
			  WHERE 1 = 1
              AND   EC.JOB_FK                     = @in_analysis_job_fk
	          AND   EC.RESULT_PARENT_DATA_NAME_FK = TSRPTE.RESULT_PARENT_DATA_NAME_FK
              AND   EC.PRODUCT_STRUCTURE_FK       = TSRPTE.PRODUCT_STRUCTURE_FK
              AND   EC.ORG_ENTITY_STRUCTURE_FK    = TSRPTE.ORG_ENTITY_STRUCTURE_FK
	          AND   EC.COLUMN_NAME = 'NET_VALUE2' 
             )	AS RESULT2
		   ,( SELECT EC.RESULT 				               
			  FROM SYNCHRONIZER.EVENT_CALC EC WITH (NOLOCK)			  
			  WHERE 1 = 1
              AND   EC.JOB_FK                     = @in_analysis_job_fk 
	          AND   EC.RESULT_PARENT_DATA_NAME_FK = TSRPTE.RESULT_PARENT_DATA_NAME_FK
              AND   EC.PRODUCT_STRUCTURE_FK       = TSRPTE.PRODUCT_STRUCTURE_FK
              AND   EC.ORG_ENTITY_STRUCTURE_FK    = TSRPTE.ORG_ENTITY_STRUCTURE_FK
	          AND   EC.COLUMN_NAME = 'NET_VALUE3' 
             )	AS RESULT3
		   ,( SELECT EC.RESULT 				               
			  FROM SYNCHRONIZER.EVENT_CALC EC WITH (NOLOCK)			  
			  WHERE 1 = 1
              AND   EC.JOB_FK                     = @in_analysis_job_fk 
	          AND   EC.RESULT_PARENT_DATA_NAME_FK = TSRPTE.RESULT_PARENT_DATA_NAME_FK
              AND   EC.PRODUCT_STRUCTURE_FK       = TSRPTE.PRODUCT_STRUCTURE_FK
              AND   EC.ORG_ENTITY_STRUCTURE_FK    = TSRPTE.ORG_ENTITY_STRUCTURE_FK
	          AND   EC.COLUMN_NAME = 'NET_VALUE4' 
             )	AS RESULT4
		   ,( SELECT EC.RESULT 				               
			  FROM SYNCHRONIZER.EVENT_CALC EC WITH (NOLOCK)			  
			  WHERE 1 = 1
              AND   EC.JOB_FK                     = @in_analysis_job_fk 
	          AND   EC.RESULT_PARENT_DATA_NAME_FK = TSRPTE.RESULT_PARENT_DATA_NAME_FK
              AND   EC.PRODUCT_STRUCTURE_FK       = TSRPTE.PRODUCT_STRUCTURE_FK
              AND   EC.ORG_ENTITY_STRUCTURE_FK    = TSRPTE.ORG_ENTITY_STRUCTURE_FK
	          AND   EC.COLUMN_NAME = 'NET_VALUE5' 
             )	AS RESULT5
		   ,( SELECT EC.RESULT 				               
			  FROM SYNCHRONIZER.EVENT_CALC EC WITH (NOLOCK)			  
			  WHERE 1 = 1
              AND   EC.JOB_FK                     = @in_analysis_job_fk 
	          AND   EC.RESULT_PARENT_DATA_NAME_FK = TSRPTE.RESULT_PARENT_DATA_NAME_FK
              AND   EC.PRODUCT_STRUCTURE_FK       = TSRPTE.PRODUCT_STRUCTURE_FK
              AND   EC.ORG_ENTITY_STRUCTURE_FK    = TSRPTE.ORG_ENTITY_STRUCTURE_FK
	          AND   EC.COLUMN_NAME = 'NET_VALUE6' 
             )	AS RESULT6
	---------
           ,SR.NET_VALUE1, SR.NET_VALUE2, SR.NET_VALUE3, SR.NET_VALUE4, SR.NET_VALUE5, SR.NET_VALUE6 
		   ,( SELECT TOP 1 PUC.UOM_CODE_FK
			  FROM epacube.PRODUCT_UOM_CLASS PUC WITH (NOLOCK)
			  WHERE PUC.DATA_NAME_FK = DN.UOM_CLASS_DATA_NAME_FK
			  AND   PUC.PRODUCT_STRUCTURE_FK = TSRPTE.PRODUCT_STRUCTURE_FK
			  AND   ( PUC.ORG_ENTITY_STRUCTURE_FK = TSRPTE.ORG_ENTITY_STRUCTURE_FK 
					  OR PUC.ORG_ENTITY_STRUCTURE_FK = 1 ) 
			) AS ASSOC_UOM_CODE_FK
			,SR.END_DATE AS END_DATE_CUR   ------END DATE CURRENT
		   ,(SELECT top 1 EC.EFF_END_DATE 				               
			  FROM SYNCHRONIZER.EVENT_CALC EC WITH (NOLOCK)			  
			  WHERE 1 = 1
              AND   EC.JOB_FK                     = @in_analysis_job_fk 
	          AND   EC.RESULT_PARENT_DATA_NAME_FK = TSRPTE.RESULT_PARENT_DATA_NAME_FK
              AND   EC.PRODUCT_STRUCTURE_FK       = TSRPTE.PRODUCT_STRUCTURE_FK
              AND   EC.ORG_ENTITY_STRUCTURE_FK    = TSRPTE.ORG_ENTITY_STRUCTURE_FK) ---END_DATE_NEW,
		   ,SR.UPDATE_TIMESTAMP AS END_DATE_CUR
		   ,(SELECT top 1 EC.RESULT_EFFECTIVE_DATE 				               
			  FROM SYNCHRONIZER.EVENT_CALC EC WITH (NOLOCK)			  
			  WHERE 1 = 1
              AND   EC.JOB_FK                     = @in_analysis_job_fk 
	          AND   EC.RESULT_PARENT_DATA_NAME_FK = TSRPTE.RESULT_PARENT_DATA_NAME_FK
              AND   EC.PRODUCT_STRUCTURE_FK       = TSRPTE.PRODUCT_STRUCTURE_FK
              AND   EC.ORG_ENTITY_STRUCTURE_FK    = TSRPTE.ORG_ENTITY_STRUCTURE_FK) ---@L_SYSDATE  ---EVENT_EFFECTIVE_DATE,
	       ,88 -- PROCESSING
           ,97 -- GREEN
		   ,TSRPTE.EVENT_SOURCE_CR_FK  
		   ,61
		   ,CASE ISNULL ( SR.SHEET_RESULTS_ID, 0 )
		    WHEN 0 THEN 51
		    ELSE 52
		    END
		   ,SR.SHEET_RESULTS_ID AS TABLE_ID_FK
		   ,( SELECT J.JOB_CLASS_FK FROM COMMON.JOB J WITH (NOLOCK) WHERE JOB_ID = @in_analysis_job_fk )
		   ,@in_analysis_job_fk  --IMPORT_JOB_FK
		   ,@v_batch_no
		   ,@v_batch_no  --IMPORT_BATCH_NO,
		   ,TSRPTE.TS_RULE_PARENT_TRIGGER_EVENTS_ID  --- RELATED_EVENT_FK
		   ,710  --RESULT_TYPE_CR_FK,
		   ,1    --RECORD_STATUS_CR_FK,		
		   ,@L_SYSDATE  --UPDATE_TIMESTAMP,
           ,'EPACUBE'   --UPDATE_USER
	-------------
	FROM #TS_RULE_PARENT_EVENTS TSRPTE 
	INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
	 ON ( DN.DATA_NAME_ID = TSRPTE.RESULT_PARENT_DATA_NAME_FK )
    INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )	 
    LEFT JOIN marginmgr.SHEET_RESULTS_VALUES_FUTURE SR
      ON ( SR.DATA_NAME_FK = TSRPTE.RESULT_PARENT_DATA_NAME_FK 
      AND  SR.PRODUCT_STRUCTURE_FK = TSRPTE.PRODUCT_STRUCTURE_FK 
      AND  SR.ORG_ENTITY_STRUCTURE_FK = TSRPTE.ORG_ENTITY_STRUCTURE_FK )
	WHERE 1 = 1
	AND  TSRPTE.RESULT_PARENT_DATA_NAME_FK = @vm$result_parent_data_name_fk 	
	AND  (   @vm$org_entity_structure_fk  = -999 )
         OR  TSRPTE.ORG_ENTITY_STRUCTURE_FK = @vm$org_entity_structure_fk               
    ---- Added V516
    AND  DN.POST_IND = 1   ---- only data names that are to be posted
    

    SET @V_COUNT = ( SELECT COUNT(1) FROM #TS_EVENT_DATA )

    SET @status_desc =  'synchronizer.EVENT_POSTING_EVENT_CALC_RESULTS - '
                        + CAST ( ISNULL  ( @vm$result_parent_data_name_fk, 0 ) AS VARCHAR(32) )
                        + ' '
                        + CAST ( ISNULL ( @vm$org_entity_structure_fk, 0 ) AS VARCHAR(32) )
                        + ' Count '
                        + CAST ( ISNULL ( @V_COUNT, 0 ) AS VARCHAR(32) )

    EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


-------------------------------------------------------------------------------
--updating effective dates
-------------------------------------------------------------------------------

	EXECUTE [synchronizer].[EVENT_DATA_EFFECTIVE_DATES_UPDATE] 
   @in_analysis_job_fk 

-----------------------------------------------------------------------------------


-------------------------------------------------------------------
-- DELETE ROWS WHERE ALL THE RESULTS ARE NULL
-------------------------------------------------------------------	


 SET @X_COUNT =  ( SELECT COUNT(*) FROM #TS_EVENT_DATA
					WHERE 1 = 1 
					AND  ( ISNULL ( NET_VALUE1_NEW, -999 )  =   -999 )
					AND  ( ISNULL ( NET_VALUE2_NEW, -999 )  =   -999 )
					AND  ( ISNULL ( NET_VALUE3_NEW, -999 )  =   -999 )
					AND  ( ISNULL ( NET_VALUE4_NEW, -999 )  =   -999 )
					AND  ( ISNULL ( NET_VALUE4_NEW, -999 )  =   -999 )
					AND  ( ISNULL ( NET_VALUE5_NEW, -999 )  =   -999 )   )
	


IF @X_COUNT > 0
BEGIN
            SET @status_desc =  'synchronizer.EVENT_POSTING_EVENT_CALC_RESULTS - '
                                + CAST ( ISNULL  ( @vm$result_parent_data_name_fk, 0 ) AS VARCHAR(32) )
                                + ' '
                                + CAST ( ISNULL ( @vm$org_entity_structure_fk, 0 ) AS VARCHAR(32) )
                                + ' ALL RESULTS ON ROW WHERE NULL COUNT:: '
                                + CAST ( ISNULL ( @X_COUNT, 0 ) AS VARCHAR(32) )
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
            
            
			DELETE FROM #TS_EVENT_DATA
					WHERE 1 = 1 
					AND  ( ISNULL ( NET_VALUE1_NEW, -999 )  =   -999 )
					AND  ( ISNULL ( NET_VALUE2_NEW, -999 )  =   -999 )
					AND  ( ISNULL ( NET_VALUE3_NEW, -999 )  =   -999 )
					AND  ( ISNULL ( NET_VALUE4_NEW, -999 )  =   -999 )
					AND  ( ISNULL ( NET_VALUE4_NEW, -999 )  =   -999 )
					AND  ( ISNULL ( NET_VALUE5_NEW, -999 )  =   -999 )            
            
END
	
	
	

UPDATE #TS_EVENT_DATA
SET  PERCENT_CHANGE1 = ( CASE ISNULL ( NET_VALUE1_CUR, -999 )
						 WHEN -999 THEN NULL
						 WHEN 0 THEN 0  
						 ELSE ( ( NET_VALUE1_NEW  - NET_VALUE1_CUR ) / NET_VALUE1_CUR )
						 END )
    ,PERCENT_CHANGE2 = ( CASE ISNULL ( NET_VALUE2_CUR, -999 )
						 WHEN -999 THEN NULL
						 WHEN 0 THEN 0  
						 ELSE ( ( NET_VALUE2_NEW  - NET_VALUE2_CUR ) / NET_VALUE2_CUR )
						 END )
    ,PERCENT_CHANGE3 = ( CASE ISNULL ( NET_VALUE3_CUR, -999)
						 WHEN -999 THEN NULL 
						 WHEN 0 THEN 0
						 ELSE ( ( NET_VALUE3_NEW  - NET_VALUE3_CUR ) / NET_VALUE3_CUR )
						 END )
    ,PERCENT_CHANGE4 = ( CASE ISNULL ( NET_VALUE4_CUR, -999 )
						 WHEN -999 THEN NULL
						 WHEN 0 THEN 0  
						 ELSE ( ( NET_VALUE4_NEW  - NET_VALUE4_CUR ) / NET_VALUE4_CUR )
						 END )
    ,PERCENT_CHANGE5 = ( CASE ISNULL ( NET_VALUE5_CUR, -999 )
						 WHEN -999 THEN NULL
						 WHEN 0 THEN 0  
						 ELSE ( ( NET_VALUE5_NEW  - NET_VALUE5_CUR ) / NET_VALUE5_CUR )
						 END )
	 ,PERCENT_CHANGE6 = ( CASE ISNULL ( NET_VALUE6_CUR, -999 )
						 WHEN -999 THEN NULL
						 WHEN 0 THEN 0  
						 ELSE ( ( NET_VALUE6_NEW  - NET_VALUE6_CUR ) / NET_VALUE6_CUR )
						 END )



--------------------------------------------------------------------------------------------
--   NO CHANGES
--------------------------------------------------------------------------------------------


UPDATE #TS_EVENT_DATA
SET EVENT_ACTION_CR_FK = 56   -- NO CHANGE
   ,EVENT_STATUS_CR_FK = 83   -- PRE POST
WHERE 1 = 1 
AND   EVENT_ACTION_CR_FK = 52   -- CHANGE
AND  (   ---- IF ALL NEW VALUES ARE EQUAL TO CURRENT VALUES  ( EXCLUDING NULL RESULTS )
           (   ( ISNULL ( NET_VALUE1_NEW, -999 )  =   -999 )
           OR  (    ( ISNULL ( NET_VALUE1_NEW, -999 )  <>  -999 )
				AND ( ISNULL ( NET_VALUE1_NEW, -999 )  =   ISNULL ( NET_VALUE1_CUR, -999 ) ) ) )
      AND
           (   ( ISNULL ( NET_VALUE2_NEW, -999 )  =   -999 )
           OR  (    ( ISNULL ( NET_VALUE2_NEW, -999 )  <>  -999 )
				AND ( ISNULL ( NET_VALUE2_NEW, -999 )  =   ISNULL ( NET_VALUE2_CUR, -999 ) ) ) )
      AND
           (   ( ISNULL ( NET_VALUE3_NEW, -999 )  =   -999 )
           OR  (    ( ISNULL ( NET_VALUE3_NEW, -999 )  <>  -999 )
				AND ( ISNULL ( NET_VALUE3_NEW, -999 )  =   ISNULL ( NET_VALUE3_CUR, -999 ) ) ) )
      AND
           (   ( ISNULL ( NET_VALUE4_NEW, -999 )  =   -999 )
           OR  (    ( ISNULL ( NET_VALUE4_NEW, -999 )  <>  -999 )
				AND ( ISNULL ( NET_VALUE4_NEW, -999 )  =   ISNULL ( NET_VALUE4_CUR, -999 ) ) ) )
      AND
           (   ( ISNULL ( NET_VALUE5_NEW, -999 )  =   -999 )
           OR  (    ( ISNULL ( NET_VALUE5_NEW, -999 )  <>  -999 )
				AND ( ISNULL ( NET_VALUE5_NEW, -999 )  =   ISNULL ( NET_VALUE5_CUR, -999 ) ) ) )
      AND
           (   ( ISNULL ( NET_VALUE6_NEW, -999 )  =   -999 )
           OR  (    ( ISNULL ( NET_VALUE6_NEW, -999 )  <>  -999 )
				AND ( ISNULL ( NET_VALUE6_NEW, -999 )  =   ISNULL ( NET_VALUE6_CUR, -999 ) ) ) )
     )


set @v_count = ( select count(1) from #TS_EVENT_DATA )

set @x_count = ( select count(1) from #TS_EVENT_DATA 
					where EVENT_ACTION_CR_FK = 56   -- NO CHANGE
					and   EVENT_STATUS_CR_FK = 83 )


IF @X_COUNT > 0
BEGIN
            SET @status_desc =  'synchronizer.EVENT_POSTING_EVENT_CALC_RESULTS - '
                                + CAST ( ISNULL  ( @vm$result_parent_data_name_fk, 0 ) AS VARCHAR(32) )
                                + ' '
                                + CAST ( ISNULL ( @vm$org_entity_structure_fk, 0 ) AS VARCHAR(32) )
                                + ' No Cost Changes Applied because Status 56: No Change COUNT:: '
                                + CAST ( ISNULL ( @X_COUNT, 0 ) AS VARCHAR(32) )
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

---- REMOVE THE NO CHANGE ROWS
            DELETE from #TS_EVENT_DATA 
					where EVENT_ACTION_CR_FK = 56   -- NO CHANGE
					and   EVENT_STATUS_CR_FK = 83 
					
END
	

IF @X_COUNT = @V_COUNT   --- ALL OF THE EVENTS ARE NO CHANGE
BEGIN
		
INSERT INTO [common].[JOB_ERRORS]
           ([JOB_FK]
           ,[TABLE_NAME]
           ,[TABLE_PK]
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,[ERROR_DATA1]
           ,[CREATE_TIMESTAMP])
     VALUES
           (@in_analysis_job_fk
           ,'EVENT_DATA'
           ,NULL
           ,99
           ,' ALL Requested Cost Changes resulted in NO CHANGE EVENTS count:: '
           ,@x_count
           ,getdate()
           )

END
ELSE
BEGIN
                             
--------------------------------------------------------------------------------------------------
--------   NO RESULT
--------			LATER ADD WHERE ONLY ONE RESULT WAS NOT CALCULATED AND 'NO ACTION' EXISTS
--------------------------------------------------------------------------------------------------

------UPDATE #TS_EVENT_DATA
------SET EVENT_ACTION_CR_FK = 57   -- NO RESULT
------   ,EVENT_STATUS_CR_FK = 85   -- HOLD 
------WHERE RELATED_EVENT_FK IN (
------		SELECT DISTINCT EC.RELATED_EVENT_FK   ---- NOT SURE HOW TO LINK ??? -- TSRPTE.TS_RULE_PARENT_TRIGGER_EVENTS_ID  --- RELATED_EVENT_FK
------		FROM SYNCHRONIZER.EVENT_CALC EC WITH (NOLOCK)
------		LEFT JOIN SYNCHRONIZER.EVENT_CALC_RULES  ECR WITH (NOLOCK)
------		 ON ( ECR.EVENT_FK = EC.EVENT_ID
------		 AND  ECR.RESULT_RANK = 1 )
------		WHERE 1 = 1
------		AND   EC.JOB_FK = @in_analysis_job_fk
------		AND   EC.RESULT IS NULL 
------		AND   EC.PARENT_DATA_NAME_FK = #TS_EVENT_DATA.DATA_NAME_FK
------		AND   EC.PRODUCT_STRUCTURE_FK = #TS_EVENT_DATA.PRODUCT_STRUCTURE_FK
------		AND   EC.ORG_ENTITY_STRUCTURE_FK = #TS_EVENT_DATA.ORG_ENTITY_STRUCTURE_FK
------		AND   (      ECR.EVENT_RULES_ID IS NULL 
------			  OR (   ECR.EVENT_RULES_ID IS NOT NULL 
------				 AND ECR.OPERATION1 <>  'NO ACTION'  )
------			  )
------        )


				                             
--------------------------------------------------------------------------------------------
--   CALL VALIDATIONS
--------------------------------------------------------------------------------------------
    

------------------------------------------------------------------------------------------
--   QUALIFY GOVERNANCE RULES 
------------------------------------------------------------------------------------------
	SET  @v_count = ( SELECT COUNT(1)
					 FROM #TS_EVENT_DATA WITH (NOLOCK)
					 WHERE BATCH_NO = @v_batch_no
					 AND   EVENT_STATUS_CR_FK = 88 
					 AND   EVENT_TYPE_CR_FK IN ( 150, 151, 152, 153, 154, 155, 156, 160 )   			 
					 AND   DATA_NAME_FK IN ( SELECT R.RESULT_DATA_NAME_FK   
											 FROM synchronizer.RULES R WITH (NOLOCK)
											 --INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
											 --  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK 
											 --  AND  RS.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE
											 WHERE R.RECORD_STATUS_CR_FK = 1 
											 AND  R.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE)
					OR DATA_NAME_FK IN (SELECT DISTINCT DN.PARENT_DATA_NAME_FK  
								FROM synchronizer.RULES R WITH (NOLOCK)
							--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
							--ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK 
							--AND  RS.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE
							INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
							ON ( R.RESULT_DATA_NAME_FK = DN.DATA_NAME_id
							AND DN.PARENT_DATA_NAME_FK IS NOT NULL)
							WHERE R.RECORD_STATUS_CR_FK = 1 
							AND  R.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE)
					)			                         
	IF @v_count > 0
	EXEC synchronizer.EVENT_POST_QUALIFY_RULES_GOVERNANCE  @v_batch_no


    EXEC [synchronizer].[EVENT_POST_VALID_PROD_ENTITY] 

	EXEC SYNCHRONIZER.EVENT_POST_VALID_IMPORT
	
	EXEC [synchronizer].[EVENT_POST_VALID_TYPE_151_152] 
		    
	EXEC [synchronizer].[EVENT_POST_VALID_TYPE_ALL] 



------------------------------------------------------------------------------------------
-- CALL PROC TO SET EVENT PRIORITY ACTION AND STATUS   ( KEEP IN ONE PLACE WITH CODE )
------------------------------------------------------------------------------------------

	EXEC  [synchronizer].[EVENT_POST_COND_PRIORITY_ACTION_STATUS] @v_batch_no


--------------------------------------------------------------------------------------------
--   CALL POST AND COMPLETE 
--------------------------------------------------------------------------------------------

		EXEC SYNCHRONIZER.EVENT_POST_DATA  

		UPDATE #TS_EVENT_DATA
		SET EVENT_STATUS_CR_FK = 80   -- RETURN TO PENDING
		WHERE EVENT_STATUS_CR_FK in ( 86, 87, 88 ) 


       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows Returned to Pending = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @v_batch_no
       END


	   EXEC synchronizer.EVENT_POSTING_IMPORT_EVENT_DATA
				 @v_batch_no, @in_analysis_job_fk
				, 152  ---@vm$event_type_cr_fk
				,'SHEET_RESULTS'				---@vm$table_name
				,@vm$result_parent_data_name_fk 
				,@vm$org_entity_structure_fk


END  --- END OF EVENTS REQUIRE POSTING BECAUSE NOT ALL STATUS 56

--------------------------------------------------------------------------------------------
--  GET NEXT WHSE
--------------------------------------------------------------------------------------------


     FETCH NEXT FROM cur_v_results INTO @vm$result_parent_data_name_fk
                                       ,@vm$org_entity_structure_fk


     END --cur_v_results LOOP
     CLOSE cur_v_results;
	 DEALLOCATE cur_v_results; 



--------------------------------------------------------------------------------------------
--  DROP TEMP TABLES
--------------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_PARENT_EVENTS') is not null
	   drop table #TS_RULE_PARENT_EVENTS;	   

--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA') is not null
	   drop table #TS_EVENT_DATA;

--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA_ERRORS') is not null
	   drop table #TS_EVENT_DATA_ERRORS


--drop temp table if it exists
	IF object_id('tempdb..#TS_EVENT_DATA_RULES') is not null
	   drop table #TS_EVENT_DATA_RULES




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_EVENT_CALC_RESULTS'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @v_batch_no
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_EVENT_CALC_RESULTS has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

  EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END













