﻿








-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform general event validations to all event types
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        05/01/2010   Initial SQL Version


CREATE PROCEDURE [synchronizer].[EVENT_POST_VALID_TYPE_160] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_160.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;




-----------------------------------------------------------------------------------
---check if association type 160 verify not going to create a duplicate
-----------------------------------------------------------------------------

INSERT INTO #TS_EVENT_DATA_ERRORS 
					(EVENT_FK,
					RULE_FK,
					EVENT_CONDITION_CR_FK,
					ERROR_NAME,
					ERROR_DATA1,
					ERROR_DATA2,
					ERROR_DATA3,
					ERROR_DATA4,
					ERROR_DATA5,
					ERROR_DATA6,
					UPDATE_TIMESTAMP)
			(	SELECT distinct
				 TSED.EVENT_FK
				,NULL
				,99
				,'ASSOCIATION ALREADY EXISTS'  --ERROR NAME  
				,'REJECT EVENT'--ERROR1
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,@l_sysdate
				FROM #TS_EVENT_DATA TSED
				WHERE tsed.event_type_cr_fk = 160
				and tsed.event_action_cr_fk = 52  )  --Can not change entity associations
				


	UPDATE #TS_EVENT_DATA
	SET EVENT_STATUS_CR_FK = 82  -- REJECT
	WHERE EVENT_FK IN (
	            SELECT TSED.EVENT_FK 
				FROM #TS_EVENT_DATA TSED
			-----------------
				WHERE tsed.event_type_cr_fk = 160
	            AND tsed.event_action_cr_Fk = 52
				)

--------------------------------------------------------------------------------------------
----MISSING CHILD --no UCC 
--------------------------------------------------------------------------------------------
				
INSERT INTO #TS_EVENT_DATA_ERRORS 
					(EVENT_FK,
					RULE_FK,
					EVENT_CONDITION_CR_FK,
					ERROR_NAME,
					ERROR_DATA1,
					ERROR_DATA2,
					ERROR_DATA3,
					ERROR_DATA4,
					ERROR_DATA5,
					ERROR_DATA6,
					UPDATE_TIMESTAMP)
			(	SELECT distinct
				 TSED.EVENT_FK
				,NULL
				,99
				,'MISSING UCC DATA FOR ASSOCIATION'  --ERROR NAME  
				,TSED.NEW_DATA   --ERROR1
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,@l_sysdate
				FROM #TS_EVENT_DATA TSED
				WHERE tsed.event_type_cr_fk = 160
				and tsed.child_entity_structure_fk IS NULL  )  


				UPDATE #TS_EVENT_DATA
	SET EVENT_STATUS_CR_FK = 80 
	WHERE EVENT_FK IN (
	            SELECT TSED.EVENT_FK 
				FROM #TS_EVENT_DATA TSED
			-----------------
				WHERE tsed.event_type_cr_fk = 160
	            and tsed.child_entity_structure_fk IS NULL
				)



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_160'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_160 has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END





