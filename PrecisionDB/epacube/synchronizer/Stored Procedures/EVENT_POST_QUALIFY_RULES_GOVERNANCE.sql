﻿

-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To determine all qualified rules for Event_Data records in Batch
--				Governance Rules assigned to Events
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010   Initial SQL Version
-- KS        10/18/2012   SUP-586 Rewrote logic to reduce the number of rows in 
--                        the #TS_RULE_DERIVE_TRIGGER_EVENTS table and moved the
--                        logic from EVENT_POST_DERIVATION here to save on memory
--                           into both RULES_DERIVE and RULES_GOVERNANCE routines
-- GHS	     5/27/2015	  Restructured
-- CV        01/17/2017   Removed Schedule and structure to use new control table 

CREATE PROCEDURE [synchronizer].[EVENT_POST_QUALIFY_RULES_GOVERNANCE] 
              ( @in_batch_no BIGINT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT
DECLARE  @r_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_QUALIFY_RULES_GOVERNANCE.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;



---------------------------------------------------
-- Temp Table for all Qualified Rules
---------------------------------------------------


--drop temp tables
IF object_id('tempdb..#TS_QUALIFIED_RULES') is not null
   drop table #TS_QUALIFIED_RULES

CREATE TABLE #TS_QUALIFIED_RULES(
	[TS_QUALIFIED_RULES_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[TS_RULE_EVENTS_FK] [bigint] NULL,		
	[TRIGGER_EVENT_FK] [bigint] NULL,			
	[RULES_FK] [bigint] NULL,
	[RESULT_DATA_NAME_FK] [int] NULL,			
	[RESULT_TYPE_CR_FK] [int] NULL,	
	[RESULT_EFFECTIVE_DATE] [DATETIME] NULL,	
	[PROD_DATA_NAME_FK] [int] NULL,	
	[PROD_FILTER_FK] [bigint] NULL,
	[PROD_RESULT_TYPE_CR_FK] [int] NULL,
	[PROD_EFFECTIVE_DATE] [DATETIME] NULL,
	[PROD_EVENT_FK] [BIGINT] NULL,
	[PROD_ORG_ENTITY_STRUCTURE_FK] [BIGINT] NULL,	
	[PROD_CORP_IND] [SMALLINT] NULL,	
	[PROD_ORG_IND] [SMALLINT] NULL,	
	[PROD_FILTER_HIERARCHY] [SMALLINT] NULL,	
	[PROD_FILTER_ORG_PRECEDENCE] [SMALLINT] NULL,	
	[ORG_DATA_NAME_FK] [int] NULL,		
	[ORG_FILTER_FK] [bigint] NULL,
	[ORG_RESULT_TYPE_CR_FK] [int] NULL,	
	[ORG_EFFECTIVE_DATE] [DATETIME] NULL,
	[ORG_EVENT_FK] [BIGINT] NULL,	
	[CUST_DATA_NAME_FK] [int] NULL,		
	[CUST_FILTER_FK] [bigint] NULL,
	[CUST_FILTER_HIERARCHY] [smallint] NULL,	
	[CUST_RESULT_TYPE_CR_FK] [int] NULL,	
	[CUST_EFFECTIVE_DATE] [DATETIME] NULL,
	[CUST_EVENT_FK] [BIGINT] NULL,
	[SUPL_DATA_NAME_FK] [int] NULL,			
	[SUPL_FILTER_FK] [bigint] NULL,
	[SUPL_RESULT_TYPE_CR_FK] [int] NULL,
	[SUPL_EFFECTIVE_DATE] [DATETIME] NULL,
	[SUPL_EVENT_FK] [BIGINT] NULL,
	[RULES_RESULT_TYPE_CR_FK] [int] NULL,	
	[RULES_EFFECTIVE_DATE] [DATETIME] NULL,
	[RULES_END_DATE] [DATETIME] NULL,	
	[RULES_RECORD_STATUS_CR_FK] [int] NULL,
	[RELATED_RULES_FK] [BIGINT] NULL,			
	[PROD_FILTER_ASSOC_DN_FK] [INT] NULL, 		
	[ORG_FILTER_ASSOC_DN_FK] [INT] NULL, 		
	[CUST_FILTER_ASSOC_DN_FK] [INT] NULL, 		
	[SUPL_FILTER_ASSOC_DN_FK] [INT] NULL, 	
    [CONTRACT_PRECEDENCE]  [INT] NULL, 	
    [RULE_PRECEDENCE]  [INT] NULL, 	
	[SCHED_PRECEDENCE]  [INT] NULL, 					
	[STRUC_PRECEDENCE]	 [INT] NULL, 		
    [SCHED_MATRIX_ACTION_CR_FK]  [INT] NULL, 
    [STRUC_MATRIX_ACTION_CR_FK]  [INT] NULL,
    [RESULT_MATRIX_ACTION_CR_FK]  [INT] NULL,
    [PROMOTIONAL_IND]  [INT] NULL,   
    [HOST_RULE_XREF] VARCHAR(128) NULL,
	[RULES_STRUCTURE_FK] [bigint] NULL,					    
	[RULES_SCHEDULE_FK] [bigint] NULL,		
    [RULE_TYPE_CR_FK]  [INT] NULL, 						    					    	
	[PRODUCT_STRUCTURE_FK] [bigint] NULL,					
	[ORG_ENTITY_STRUCTURE_FK] [bigint] NULL,						
	[CUST_ENTITY_STRUCTURE_FK] [bigint] NULL,						
	[SUPL_ENTITY_STRUCTURE_FK] [bigint] NULL,	
	[RULES_CONTRACT_CUSTOMER_FK] [bigint] NULL,			
	[DISQUALIFIED_IND] [smallint] NULL,
	[DISQUALIFIED_COMMENT] VARCHAR(64) NULL,
    [CUST_ENTITY_HIERARCHY] INT NULL
)



--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSQR_TSQRID ON #TS_QUALIFIED_RULES 
	(TS_QUALIFIED_RULES_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSRE_RDNF_REDT_IEID] ON #TS_QUALIFIED_RULES
	(RULES_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_FK, TS_QUALIFIED_RULES_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


		
		
------------------------------------------------------------------------------------------
--   Create Temp Table for Events to be assigned Rules
--		Note this temp table is used for all rule routines
--		If changed it must be changed in all places !!!!
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_EVENTS') is not null
	   drop table #TS_RULE_EVENTS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_RULE_EVENTS(
	    TS_RULE_EVENTS_ID             BIGINT IDENTITY(1000,1) NOT NULL,
		RULE_TYPE_CR_FK          INT NULL,	    
		RESULT_DATA_NAME_FK		 INT NULL,						
		RESULT_TYPE_CR_FK        INT NULL,
		RESULT_EFFECTIVE_DATE    DATETIME NULL,		
	    PRODUCT_STRUCTURE_FK     BIGINT NULL,
	    ORG_ENTITY_STRUCTURE_FK  BIGINT NULL,
	    CUST_ENTITY_STRUCTURE_FK BIGINT NULL,
	    SUPL_ENTITY_STRUCTURE_FK BIGINT NULL,
        JOB_FK                   BIGINT NULL,	    
        TRIGGER_EVENT_FK         BIGINT NULL,
        EVENT_TABLE_NAME         VARCHAR(32) NULL,
		 )


--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT OF #TS_RULE_EVENTS
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSRE_TSREID ON #TS_RULE_EVENTS 
	(TS_RULE_EVENTS_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSRE_RDNF_REDT_IEID] ON #TS_RULE_EVENTS 
	(RESULT_DATA_NAME_FK ASC
	,RESULT_EFFECTIVE_DATE ASC
	,RESULT_TYPE_CR_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRE_PSF_IEF ON #TS_RULE_EVENTS 
	(PRODUCT_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRE_OESF_IEF ON #TS_RULE_EVENTS 
	(ORG_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRE_CESF_IEF ON #TS_RULE_EVENTS 
	(CUST_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX IDX_TSRE_SESF_IEF ON #TS_RULE_EVENTS 
	(SUPL_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_EVENTS_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)




--------------------------------------------------------------------------------------------
--   INSERT INTO TEMP TABLE
--          CINDY... WILL NEED TO REVISIT THIS LATER TO FIGURE OUT HARDCODED DETAILS 
--                   SUPL AND ORG ENTITY and EVENT EFFECTIVE DATE AND RESULT TYPES, STG_RECORD_FK
--------------------------------------------------------------------------------------------

INSERT INTO #TS_RULE_EVENTS(
		 RULE_TYPE_CR_FK	     
		,RESULT_DATA_NAME_FK	
		,RESULT_TYPE_CR_FK	     
		,RESULT_EFFECTIVE_DATE	
	    ,PRODUCT_STRUCTURE_FK   
	    ,ORG_ENTITY_STRUCTURE_FK 
	    ,CUST_ENTITY_STRUCTURE_FK
	    ,SUPL_ENTITY_STRUCTURE_FK
	    ,JOB_FK
        ,TRIGGER_EVENT_FK
        ,EVENT_TABLE_NAME
		 )
 SELECT DISTINCT 
         R.RULE_TYPE_CR_FK
		,R.RESULT_DATA_NAME_FK	
		,710 AS RESULT_TYPE_CR_FK
		,ED.EVENT_EFFECTIVE_DATE
	    ,ED.PRODUCT_STRUCTURE_FK   
	    ,ED.ORG_ENTITY_STRUCTURE_FK 
	    ,NULL AS CUST_ENTITY_STRUCTURE_FK
	    ,NULL AS SUPL_ENTITY_STRUCTURE_FK	    
	    ,ED.IMPORT_JOB_FK
        ,ED.EVENT_FK   --TRIGGER_EVENT_FK
        ,NULL   --EVENT_TABLE_NAME
-------------
	FROM synchronizer.RULES R WITH (NOLOCK)
	--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	--	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID 
	--	AND  RS.RULE_TYPE_CR_FK = 307 )                --- GOVERNANCE
	INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
	    ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK )
	INNER JOIN #TS_EVENT_DATA ED WITH (NOLOCK)
		ON  ( ED.DATA_NAME_FK = R.RESULT_DATA_NAME_FK )
		--OR    ED.DATA_NAME_FK = DN.PARENT_DATA_NAME_FK )    ----- cindy testing
	--------------
	--INNER JOIN synchronizer.RULES_SCHEDULE RSCHED  WITH (NOLOCK) 
	--	ON  ( RSCHED.RULES_SCHEDULE_ID  = R.RULES_SCHEDULE_FK )   
	INNER JOIN synchronizer.RULES_ACTION RA  WITH (NOLOCK) 
		ON  ( RA.RULES_FK = R.RULES_ID )
--------------
--  Added the same exclusions that we used in derivations to consider the prod and supl filters in the rules
--------------
--LEFT JOIN Epacube.SEGMENTS_PRODUCT SP on R.Prod_Segment_FK = SP.PROD_SEGMENT_FK  ---- cindy testing not using filters
--LEFT JOIN epacube.SEGMENTS_VENDOR SV on R.Vendor_Segment_FK = sv.VENDOR_SEGMENT_FK


----INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
----    ON ( RFS.RULES_FK = R.RULES_ID 
----	AND  (  ( RFS.PROD_FILTER_FK IS NULL AND RFS.SUPL_FILTER_FK IS NULL )
----		 OR ( RFS.SUPL_FILTER_FK IS NULL AND RFS.PROD_FILTER_FK IN 
----                                   (  SELECT VRFP.RULES_FILTER_FK
----									  FROM synchronizer.V_RULE_FILTERS_PROD VRFP
----									  WHERE VRFP.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK ) )
----		 OR ( RFS.PROD_FILTER_FK IS NULL AND RFS.SUPL_FILTER_FK IN 
----                                   (  SELECT VRFE.RULES_FILTER_FK
----									  FROM epacube.product_association pas WITH (NOLOCK) 
----									  INNER JOIN synchronizer.V_RULE_FILTERS_ENTITY VRFE
----										  ON ( VRFE.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK 
----										  AND  VRFE.ENTITY_CLASS_CR_FK = 10103 )
----                                      WHERE pas.product_structure_fk = ED.PRODUCT_STRUCTURE_FK
----                                      and   pas.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK
----								      AND   pas.DATA_NAME_FK  = RFS.SUPL_FILTER_ASSOC_DN_FK  ) )
----		 OR ( ( RFS.PROD_FILTER_FK IN 
----                                   (  SELECT VRFP.RULES_FILTER_FK
----									  FROM synchronizer.V_RULE_FILTERS_PROD VRFP
----									  WHERE VRFP.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK ) )
----            AND
----		      ( RFS.SUPL_FILTER_FK IN 
----                                   (  SELECT VRFE.RULES_FILTER_FK
----									  FROM epacube.product_association pas WITH (NOLOCK) 
----									  INNER JOIN synchronizer.V_RULE_FILTERS_ENTITY VRFE
----										  ON ( VRFE.ENTITY_STRUCTURE_FK = PAS.ENTITY_STRUCTURE_FK 
----										  AND  VRFE.ENTITY_CLASS_CR_FK = 10103 ) 
----                                      WHERE pas.product_structure_fk = ED.product_structure_fk 
----                                      AND   pas.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK
----								      AND   pas.DATA_NAME_FK  = RFS.SUPL_FILTER_ASSOC_DN_FK  ) )
----             )
----      )  )
	--------------    
	WHERE 1 = 1
	AND   ED.BATCH_NO = @in_batch_no
	AND   ED.RECORD_STATUS_CR_FK = 1
	AND   ED.EVENT_TYPE_CR_FK IN ( 150, 151, 152, 153, 154, 155, 156, 160, 158 )   --- ALL PRODUCT ENTITY TYPES
	AND   (  ED.IMPORT_BATCH_NO IS NULL 
		  OR ISNULL ( ED.IMPORT_BATCH_NO, -999 ) = ED.BATCH_NO )    
	-----------
	AND   R.RECORD_STATUS_CR_FK = 1
	AND   R.rule_type_cr_fk = 307  --- GOVERNANCE
	AND  (    R.TRIGGER_EVENT_TYPE_CR_FK IS NULL
		 OR   R.TRIGGER_EVENT_TYPE_CR_FK = ED.EVENT_TYPE_CR_FK  )
	AND  (    R.TRIGGER_EVENT_DN_FK IS NULL
		 OR   R.TRIGGER_EVENT_DN_FK = ED.DATA_NAME_FK  )
	AND   (   R.TRIGGER_EVENT_VALUE1 IS NULL
		  OR  ISNULL ( R.TRIGGER_EVENT_VALUE1, 'NULL DATA') = ISNULL ( ED.NEW_DATA, 'NULL DATA')  ) 
	AND  (    R.TRIGGER_EVENT_ACTION_CR_FK IS NULL
		 OR   R.TRIGGER_EVENT_ACTION_CR_FK = ED.EVENT_ACTION_CR_FK )
	AND  (    R.TRIGGER_IMPORT_PACKAGE_FK IS NULL
		 OR   R.TRIGGER_IMPORT_PACKAGE_FK = ED.IMPORT_PACKAGE_FK )
	AND  (    R.TRIGGER_EVENT_SOURCE_CR_FK IS NULL
		 OR   R.TRIGGER_EVENT_SOURCE_CR_FK = ED.EVENT_SOURCE_CR_FK )    
		

------------------------------------------------------------------------------------------
---  Call Routines to Quailify Rules
------------------------------------------------------------------------------------------

	EXEC  synchronizer.EVENT_RULE_QUALIFY  WITH RECOMPILE 


/**********************************************************************************/
--   Insert into sychronizer.EVENT_DATA_RULES
--		Insert Current Rules... ONLY if not already there ??
--		Later look into DRANK ?? or something to remove duplicates
--      Also need to include a SQL for Future / Whatif Rules
/**********************************************************************************/


	INSERT INTO #TS_EVENT_DATA_RULES (
			 EVENT_FK
			,RULES_FK 
			,RESULT_DATA_NAME_FK
			,RESULT_TYPE_CR_FK
			,RESULT_EFFECTIVE_DATE 
	        ,RULES_ACTION_OPERATION_FK
	        ,RULES_EFFECTIVE_DATE
	        ,RULE_PRECEDENCE
	        ,SCHED_PRECEDENCE
            ,UPDATE_TIMESTAMP   ) 
    SELECT DISTINCT
			 ED.EVENT_fk ---  FROM NEWLY INSERTED EVENTS ABOVE
			,TSQR.RULES_FK   
			,TSQR.RESULT_DATA_NAME_FK      		   
			,TSQR.RESULT_TYPE_CR_FK      	
			,TSQR.RESULT_EFFECTIVE_DATE      	
            ,RA.RULES_ACTION_OPERATION1_FK
		-------- FROM RULE TABLES
		    ,R.EFFECTIVE_DATE
			,R.RULE_PRECEDENCE
			,c.control_precedence
			--,RSCH.SCHEDULE_PRECEDENCE				
			,GETDATE()  
------
		FROM  #TS_QUALIFIED_RULES TSQR
		INNER JOIN #TS_RULE_EVENTS TSRE
		 ON ( TSRE.TS_RULE_EVENTS_ID = TSQR.TS_RULE_EVENTS_FK )
		INNER JOIN #TS_EVENT_DATA ED WITH (NOLOCK)
		 ON ( TSRE.TRIGGER_EVENT_FK = ED.EVENT_FK )
----INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
----ON ( ED.RELATED_EVENT_FK = TSQR.TS_RULE_EVENTS_FK 
----AND  ED.DATA_NAME_FK = TSQR.RESULT_DATA_NAME_FK
----AND  ED.PRODUCT_STRUCTURE_FK = TSRE.PRODUCT_STRUCTURE_FK
----AND  ED.ORG_ENTITY_STRUCTURE_FK = TSRE.ORG_ENTITY_STRUCTURE_FK )
		 ----AND  ED.BATCH_NO = @IN_BATCH_NO )		 
		INNER JOIN synchronizer.RULES R
		 ON ( R.RULES_ID = TSQR.RULES_FK )
		INNER JOIN synchronizer.RULES_ACTION RA
		 ON ( RA.RULES_FK = R.RULES_ID )
		INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
		  ON ( DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK )
		 left JOIN Epacube.controls C on R.controls_fk = C.controls_id
		--INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
		-- ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
		--INNER JOIN synchronizer.RULES_SCHEDULE RSCH WITH (NOLOCK) 
		-- ON ( RSCH.RULES_SCHEDULE_ID = R.RULES_SCHEDULE_FK )
		WHERE ISNULL ( DISQUALIFIED_IND, 0 ) = 0         


--------	END
    


------------------------------------------------------------------------------------------
--  PERFORM RULES SELECTION AND EXECUTION
------------------------------------------------------------------------------------------


    SET @v_count = 
        ( SELECT COUNT(1)
          FROM #TS_EVENT_DATA_RULES )

    IF @v_count > 0
    BEGIN
		            

------------------------------------------------------------------------------------------
--  DENSE RANK BY PRECEDENCE 
--			PARTITION BY EVENT_FK, RESULT_DATA_NAME_FK, BASIS1_DN_FK, BASIS2_DN_FK
------------------------------------------------------------------------------------------

	UPDATE #TS_EVENT_DATA_RULES
	SET RESULT_RANK = 1
	WHERE TS_EVENT_RULES_ID IN (
	SELECT A.EVENT_RULES_ID
	FROM (
	SELECT
		EDR.TS_EVENT_RULES_ID AS EVENT_RULES_ID,
		( DENSE_RANK() OVER ( PARTITION BY EDR.EVENT_FK, R.RULES_STRUCTURE_FK, R.RULES_SCHEDULE_FK
						ORDER BY EDR.STRUC_PRECEDENCE ASC, EDR.SCHED_PRECEDENCE ASC, EDR.RULE_PRECEDENCE ASC, 
								 EDR.RULES_FK DESC ) ) AS DRANK
	FROM #TS_EVENT_DATA ED WITH (NOLOCK)
	INNER JOIN #TS_EVENT_DATA_RULES EDR WITH (NOLOCK) 
	  ON ( EDR.EVENT_FK = ED.EVENT_FK )
	INNER JOIN synchronizer.RULES R WITH (NOLOCK) 
	  ON ( EDR.RULES_FK = R.RULES_ID )
	WHERE 1 = 1
	AND   ED.BATCH_NO = @in_batch_no
	) A 
	WHERE A.DRANK = 1
	)

	EXEC SYNCHRONIZER.EVENT_POST_GOVERNANCE @in_batch_no

    END  



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_QUALIFY_RULES_GOVERNANCE'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_QUALIFY_RULES_GOVERNANCE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END





















































