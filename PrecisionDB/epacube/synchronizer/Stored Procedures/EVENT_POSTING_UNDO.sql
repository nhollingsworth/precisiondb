﻿








-- Copyright 2011
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support reversing event values to the previous value on the event.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/23/2011   Initial SQL Version
-- CV        10/04/2011   Added to call purge after export procedure
--

CREATE PROCEDURE [synchronizer].[EVENT_POSTING_UNDO] 
        ( @in_batch_no bigint )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime


DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_UNDO.' + ' Batch No = '
					 + cast(isnull(@in_batch_no, 0) as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;


SET @l_sysdate = GETDATE()



------------------------------------------------------------------------------------------
--   Create UNDO events from previous Approved Statuses
------------------------------------------------------------------------------------------

	EXEC synchronizer.EVENT_FACTORY_UNDO  @in_batch_no


------------------------------------------------------------------------------------------
--   Next Run Posting for other Event Actions 
------------------------------------------------------------------------------------------

	EXEC SYNCHRONIZER.EVENT_POSTING @in_batch_no
	
------------------------------------------------------------------------------------------
--   Next Purge inactive after export.  This will remove any -99999999 that are not part of 
--   export config and leave them if not exported.
------------------------------------------------------------------------------------------

	EXECUTE  [synchronizer].[PURGE_INACTIVE_AFTER_EXPORT] 
	
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_UNDO'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @in_batch_no
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_UNDO has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END























































































