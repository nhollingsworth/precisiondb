﻿












-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing from Editors
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/15/2010  Initial SQL Version
-- CV		 02/26/2010  reject any event regardless of source
-- CV        01/27/2011  update entity structure to null if not needed.
--						 jira 3182
-- CV        12/06/2011  Regression testing from UI added to not wipe out entity structure
--CV         01/13/2017  Getting Rule type cr fk from Rule table


CREATE PROCEDURE [synchronizer].[EVENT_POST_EDITOR_DATA] 
        ( @in_batch_no bigint )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime


DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_EDITOR_DATA.'
					 + cast(isnull(@in_batch_no, 0) as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;



------            exec sp_executesql 	@ls_stmt  USE TO THROW ERROR

SET @l_sysdate = GETDATE()



------------------------------------------------------------------------------------------
--   Updates From UI..  First Thing.. If any other Pending Events for same dn/prod/org/cust
--					    Reject right away
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_DATA
    SET EVENT_PRIORITY_CR_FK = 60
       ,EVENT_STATUS_CR_FK = 82
 WHERE EVENT_ID IN ( 
         SELECT EDX.EVENT_ID
         FROM synchronizer.EVENT_DATA ED   
         INNER JOIN synchronizer.EVENT_DATA EDX   WITH (NOLOCK) 
         ON ( EDX.DATA_NAME_FK = ED.DATA_NAME_FK
         AND  EDX.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
         AND  EDX.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK
         AND  EDX.EVENT_STATUS_CR_FK = 80 )
         WHERE ED.BATCH_NO = @in_batch_no
         AND   ED.EVENT_STATUS_CR_FK = 88)
--		 AND   ED.EVENT_SOURCE_CR_FK = 71  )   --- USER ENTRY  SH



------------------------------------------------------------------------------------------
--     ADD COMPLETNESS EVENTS TO EXISTING EDITOR BATCH -- AUTO CHECK
--  CHECK FOR OUTSTANDING RELATED COMPLETENESS EVENTS FOR EPACUBE ID'S IN BATCH
--	IF NOT THEIR ADD TO BATCH RIGHT BEFORE LOOPING PROCESS AND SET ALL THESE
--  COMPLETENESS EVENTS TO A PRIOIRITY OF 69 COMPLETENESS SO THEY ARE LAST TO PROCESS 
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_DATA
  SET EVENT_STATUS_CR_FK = 88
     ,EVENT_PRIORITY_CR_FK = 69
     ,EVENT_CONDITION_CR_FK = 97
     ,BATCH_NO = @in_batch_no
WHERE EVENT_ID IN ( 
SELECT  EDX.EVENT_ID
		FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)
		INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
		 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK ) 		
		INNER JOIN synchronizer.EVENT_DATA EDX WITH (NOLOCK)
		 ON ( EDX.DATA_NAME_FK = DN.DATA_NAME_ID 
		 AND  EDX.EVENT_STATUS_CR_FK = 80 )  -- ONLY PENDING		
		INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
		 ON ( ED.DATA_NAME_FK <> EDX.DATA_NAME_FK
		 AND  ED.PRODUCT_STRUCTURE_FK = EDX.PRODUCT_STRUCTURE_FK 
		 AND  (   DS.ORG_IND IS NULL
			  OR 
				  ED.ORG_ENTITY_STRUCTURE_FK = EDX.ORG_ENTITY_STRUCTURE_FK ) 
		AND  (   DS.ENTITY_STRUCTURE_EC_CR_FK IS NULL
			  OR 
				  ED.ENTITY_STRUCTURE_FK = EDX.ENTITY_STRUCTURE_FK )
			  )
		WHERE 1 = 1
		AND   ED.BATCH_NO = @in_batch_no
		AND   ED.EVENT_STATUS_CR_FK = 88
		AND   ED.EVENT_SOURCE_CR_FK = 71  --- USER ENTRY
		AND   DN.DATA_NAME_ID IN (  SELECT R.RESULT_DATA_NAME_FK
									FROM synchronizer.RULES R WITH (NOLOCK)
									WHERE R.RULE_TYPE_CR_FK = 309 
									AND R.RECORD_STATUS_CR_FK = 1 ))
		
			
	
------------------------------------------------------------------------------------------
--   Updates From UI..  If any outstanding Completeness Events for Products in UI Batch
--					    Add to the same processing Batch
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_DATA
    SET BATCH_NO = @in_batch_no
       ,EVENT_STATUS_CR_FK = 88
 WHERE EVENT_ID IN ( 
         SELECT EDX.EVENT_ID
         FROM synchronizer.EVENT_DATA ED   
         INNER JOIN synchronizer.EVENT_DATA EDX   WITH (NOLOCK) 
         ON ( EDX.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK
         AND  EDX.ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK
         AND  EDX.EVENT_STATUS_CR_FK = 80 )
         INNER JOIN synchronizer.EVENT_DATA_RULES EDR WITH (NOLOCK)
         ON   ( EDR.EVENT_FK = EDX.EVENT_ID )
         INNER JOIN synchronizer.RULES R   WITH (NOLOCK)
         ON   ( R.RULES_ID = EDR.RULES_FK
		 AND  R.RECORD_STATUS_CR_FK = 1
		 AND    R.RULE_TYPE_CR_FK = 309 ) -------------------------------------------- cindy added
      
         WHERE ED.BATCH_NO = @in_batch_no
		      --- COMPLETENESS RULES
         AND   ED.EVENT_STATUS_CR_FK = 88)
--		 AND   ED.EVENT_SOURCE_CR_FK = 71  )   --- USER ENTRY


------------------------------------------------------------------------------
--   Updates From UI..  NEED ENTITY_STRUCTURE from UI 
--						or look up when set from UI before new data can be set.
----------------------------------------------------------------------------------------


			UPDATE synchronizer.EVENT_DATA 
			SET entity_structure_fk = (select ei.entity_structure_fk 
										from epacube.entity_identification ei
										Where ei.value = New_Data 
										AND   ei.ENTITY_DATA_NAME_FK = dn.ENTITY_DATA_NAME_FK )
			FROM epacube.data_name dn WITH (NOLOCK)
			INNER JOIN epacube.data_set ds WITH (NOLOCK) 
			  ON ( ds.DATA_SET_ID = dn.DATA_SET_FK )
			WHERE synchronizer.EVENT_DATA.BATCH_NO = @in_batch_no
			AND   synchronizer.EVENT_DATA.EVENT_STATUS_CR_FK = 88
			AND   synchronizer.EVENT_DATA.Event_source_cr_fk = 71
			AND   synchronizer.EVENT_DATA.DATA_NAME_FK = DN.DATA_NAME_ID						
			AND   synchronizer.EVENT_DATA.ENTITY_STRUCTURE_FK IS NULL
			AND   ds.ENTITY_STRUCTURE_EC_CR_FK IS NOT NULL
			AND   ds.EVENT_TYPE_CR_FK = 155  -- Associations
			AND   dn.entity_data_name_fk is not null  --added epa 2733 




------------------------------------------------------------------------------------------
--    Updates From UI..  Association Events set New_Data if NULL
------------------------------------------------------------------------------------------


UPDATE synchronizer.EVENT_DATA 
SET new_data = 
		 (  CASE ISNULL ( CR.CODE, 'NULL DATA' )
		    WHEN 'NULL DATA' THEN NULL
		    WHEN 'WAREHOUSE'
			THEN (    SELECT EI.VALUE
								  FROM EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
								  WHERE EI.ENTITY_STRUCTURE_FK = synchronizer.EVENT_DATA.ORG_ENTITY_STRUCTURE_FK
								  AND EI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
												  FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK)
												  WHERE EEP.APPLICATION_SCOPE_FK = 100
												  AND   EEP.NAME = ( SELECT CODE FROM EPACUBE.CODE_REF WITH (NOLOCK)
														 WHERE CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
									 ) )
		    ELSE (    SELECT EI.VALUE
					  FROM EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
					  WHERE EI.ENTITY_STRUCTURE_FK = synchronizer.EVENT_DATA.ENTITY_STRUCTURE_FK
					  AND EI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
									  FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK)
									  WHERE EEP.APPLICATION_SCOPE_FK = 100
									  AND   EEP.NAME = ( SELECT CODE FROM EPACUBE.CODE_REF WITH (NOLOCK)
														 WHERE CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
									 ) )
            END  )   
     ,EVENT_ENTITY_CLASS_CR_FK = (select code_ref_id 
								from epacube.code_ref 
								where code_type = 'ENTITY_CLASS'
								and code = 'PRODUCT')
FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
LEFT JOIN EPACUBE.CODE_REF CR WITH (NOLOCK) ON ( CR.CODE_REF_ID = DS.ENTITY_STRUCTURE_EC_CR_FK )
WHERE synchronizer.EVENT_DATA.BATCH_NO = @in_batch_no
AND   synchronizer.EVENT_DATA.EVENT_STATUS_CR_FK = 88
AND   synchronizer.EVENT_DATA.Event_source_cr_fk = 71
AND   synchronizer.EVENT_DATA.DATA_NAME_FK = DN.DATA_NAME_ID
AND   synchronizer.EVENT_DATA.NEW_DATA IS NULL
AND   ds.EVENT_TYPE_CR_FK = 155  -- Associations

---------------------------------------------------------------------
--JIRA - 3182   -- Remove entity_structure if data is not required
---------------------------------------------------------------------

UPDATE synchronizer.EVENT_DATA
    SET entity_structure_Fk = NULL
FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK
AND DS.ENTITY_STRUCTURE_EC_CR_FK is NULL
AND DS.EVENT_TYPE_CR_FK <> 154 )  --- added so if adding vendor did not wipe out structure

WHERE synchronizer.EVENT_DATA.BATCH_NO = @in_batch_no
AND   synchronizer.EVENT_DATA.EVENT_STATUS_CR_FK = 88
AND   synchronizer.EVENT_DATA.Event_source_cr_fk = 71
AND   synchronizer.EVENT_DATA.DATA_NAME_FK = DN.DATA_NAME_ID


------------------------------------------------------------------------------------------
---  DEFAULTS and DERIVATIONS 
---			( Same Rrocedure called from synchronizer.EVENT_POSTING_IMPORT )
------------------------------------------------------------------------------------------
----CINDY TESTING PERFORMANCE URM
    --EXEC synchronizer.EVENT_POST_DERIVATION @in_batch_no
    

    --UPDATE synchronizer.EVENT_DATA
    --SET EVENT_PRIORITY_CR_FK = 66   --- APPROVE TO POST
    --   ,EVENT_STATUS_CR_FK = 88     --- SET TO PROCESSING
    --WHERE 1 = 1
    --AND   BATCH_NO =  @in_batch_no
    --AND   EVENT_SOURCE_CR_FK = 72    --- KS ADDED BECAUSE DERIVATIONS AND DEFAULTS WERE NOT AUTO APPROVING FROM UI
    
    
    
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_EDITOR_DATA'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @in_batch_no
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_EDITOR_DATA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
























