﻿
















-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To perform USED DEFINED RULE event validations to all event types
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        12/03/2009   Initial SQL Version
-- 

CREATE PROCEDURE [synchronizer].[EVENT_POST_VALID_TYPE_154] 
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_154.' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;





/*    THIS PROCEDURE DOES NOTHING RIGHT NOW.....  LEAVE AS PLACEHOLDER TO ADD LATER SOME ENTITY VALIDATION

------------------------------------------------------------------------------------------
--  INSERT MISSING VALUES
------------------------------------------------------------------------------------------

  INSERT INTO epacube.DATA_VALUE
    (DATA_NAME_FK, VALUE, JOB_FK, UPDATE_LOG_FK, UPDATE_USER )
    SELECT  a.data_name_fk, a.VALUE, a.JOB_FK, a.batch_no, 'Auto Insert'
      FROM 
          ( 
            SELECT DISTINCT T_1.DATA_NAME_FK, T_1.VALUE, T_1.JOB_FK, T_1.BATCH_NO
              FROM 
                  ( 
                    SELECT tsed.DATA_NAME_FK, tsed.NEW_DATA AS VALUE, 
								tsed.IMPORT_JOB_FK AS JOB_FK, tsed.BATCH_NO 
                      FROM #TS_EVENT_DATA tsed  WITH (NOLOCK)
INNER JOIN epacube.DATA_NAME dn  WITH (NOLOCK) 
ON (dn.DATA_NAME_ID = tsed.DATA_NAME_FK)		
			WHERE tsed.data_type_cr_fk = 134   -- data value
--			AND   DN.LENGTH IS NOT NULL 
			AND   (
			        (     dn.max_or_eq = 'EQ' 
			        AND  LEN ( TSED.NEW_DATA ) = ISNULL (DN.LENGTH, 256) )
			      OR
				    (     LEN ( TSED.NEW_DATA ) <= ISNULL (DN.LENGTH, 256) )			
				  )
					  AND tsed.DATA_VALUE_FK IS NULL
                      AND   tsed.NEW_DATA <> 'NULL'
					  AND   tsed.DATA_TYPE_CR_FK = 134
                      AND   tsed.INSERT_MISSING_VALUES = 1
                  ) T_1
          ) a
      WHERE  NOT( EXISTS
                  ( 
                    SELECT 1
                      FROM epacube.DATA_VALUE dv1  WITH (NOLOCK)
                      WHERE ((dv1.DATA_NAME_FK = a.data_name_fk) AND 
                              (dv1.VALUE = a.VALUE))
                  ))


      INSERT INTO epacube.ENTITY_DATA_VALUE
        (
          VALUE, 
          DATA_NAME_FK, 
          ENTITY_STRUCTURE_FK,
          JOB_FK,
          UPDATE_LOG_FK,
          UPDATE_USER
        )
        SELECT 
            a.new_data, 
            a.data_name_fk, 
            a.entity_structure_fk,
            a.job_fk,
            a.batch_no,            
            'Auto Insert'
          FROM 
              ( 
                SELECT T_1.data_name_fk, T_1.NEW_DATA, T_1.ENTITY_STRUCTURE_FK, 
                    T_1.JOB_FK, T_1.BATCH_NO
                  FROM 
                      ( 
                        SELECT DISTINCT tsed.DATA_NAME_FK, tsed.NEW_DATA, tsed.ENTITY_STRUCTURE_FK, 
									tsed.IMPORT_JOB_FK AS JOB_FK, tsed.batch_no                        
                          FROM #TS_EVENT_DATA tsed
						  WHERE tsed.ENTITY_DATA_VALUE_FK IS NULL
                          AND   tsed.NEW_DATA <> 'NULL'
						  AND tsed.entity_structure_fk is not NULL  ---CV added
						  AND   tsed.DATA_TYPE_CR_FK = 135
                          AND   tsed.INSERT_MISSING_VALUES = 1
                      ) T_1
              ) a
          WHERE  NOT( EXISTS
                      ( 
                        SELECT 1
                          FROM epacube.ENTITY_DATA_VALUE epc1  WITH (NOLOCK)
                          WHERE ((epc1.DATA_NAME_FK = a.data_name_fk) AND 
                                  (epc1.VALUE = a.new_data) AND 
                                  (epc1.ENTITY_STRUCTURE_FK = a.entity_structure_fk))
                      ))


------------------------------------------------------------------------------------------
--  WARNING AUTO INSERT DATA VALUES PRIOR TO UPDATING DATA_VALUE_FK
------------------------------------------------------------------------------------------

--DATA VALUE OR ENTITY_DATA VALUE
			INSERT INTO #TS_EVENT_DATA_ERRORS (
				EVENT_FK,
				RULE_FK,
				EVENT_CONDITION_CR_FK,
				ERROR_NAME,
				ERROR_DATA1,
				ERROR_DATA2,
				ERROR_DATA3,
				ERROR_DATA4,
				ERROR_DATA5,
				ERROR_DATA6,
				UPDATE_TIMESTAMP)
		(	SELECT 
			 TSED.EVENT_FK
			,NULL
			,98
			,'MISSING DATA VALUE'  --ERROR NAME
			,NULL
			,'DATA VALUE WAS AUTO INSERTED'--ERROR1
			,NULL
			,NULL
			,NULL
			,NULL
			,@l_sysdate
			FROM #TS_EVENT_DATA TSED
			WHERE tsed.INSERT_MISSING_VALUES = 1 
			AND TSED.RECORD_STATUS_CR_FK <> 2   --- NOT AN INACTIVATE
            AND (    
                 (   data_type_cr_fk = 134 		
			     AND DATA_VALUE_FK is NULL )
			    OR
                 (   data_type_cr_fk = 135 		
			     AND ENTITY_DATA_VALUE_FK is NULL )
			    )
			AND ISNULL ( TSED.NEW_DATA, 'NULL') <> 'NULL' 
AND   ( EXISTS
                  ( 
                    SELECT 1
                      FROM epacube.DATA_VALUE dv1  WITH (NOLOCK)
                      WHERE ((dv1.DATA_NAME_FK = tsed.data_name_fk) AND 
                              (dv1.VALUE = tsed.New_data))
                  )))

				  
*/ 

------------------------------------------------------------------------------------------
--  INITIALIZE DATA 
---  MAY GROUP TOGETHER AS SUBSELECT UPDATES FOR PERFORMANCE
------------------------------------------------------------------------------------------


      UPDATE #TS_EVENT_DATA
        SET DATA_VALUE_FK = 
          (
            SELECT dv.DATA_VALUE_ID
              FROM epacube.DATA_VALUE dv  WITH (NOLOCK)
			  WHERE dv.DATA_NAME_FK = #TS_EVENT_DATA.DATA_NAME_FK
			  AND   dv.VALUE        = #TS_EVENT_DATA.NEW_DATA
          )
        WHERE DATA_TYPE_CR_FK in ( 134, 128)
		AND Data_value_fk is NULL


      UPDATE #TS_EVENT_DATA
        SET ENTITY_DATA_VALUE_FK =   
          (
            SELECT dv.ENTITY_DATA_VALUE_ID
              FROM epacube.ENTITY_DATA_VALUE dv  WITH (NOLOCK)
			  WHERE dv.DATA_NAME_FK = #TS_EVENT_DATA.DATA_NAME_FK
			  AND   dv.ENTITY_STRUCTURE_FK = #TS_EVENT_DATA.ENTITY_STRUCTURE_FK			  
			  AND   dv.VALUE        = #TS_EVENT_DATA.NEW_DATA
          )
        WHERE DATA_TYPE_CR_FK = 135
		and entity_data_value_fk is NULL




------------------------------------------------------------------------------------------
--   Insert Errors into #TS Event Error Tables 
------------------------------------------------------------------------------------------



------------------------------------------------------------------------------------------
--  VALIDATE DATA TYPES
--    NUMERIC 131, DATE 132  Y/N 133 ... ETC.  
--      MISSING DATA_VALUE_FK... TYPE 134
--      MISSING ENTITY_DATA_VALUE_FK... TYPE 135
------------------------------------------------------------------------------------------
--DATA VALUE 
		--	INSERT INTO #TS_EVENT_DATA_ERRORS (
		--		EVENT_FK,
		--		RULE_FK,
		--		EVENT_CONDITION_CR_FK,
		--		ERROR_NAME,
		--		ERROR_DATA1,
		--		ERROR_DATA2,
		--		ERROR_DATA3,
		--		ERROR_DATA4,
		--		ERROR_DATA5,
		--		ERROR_DATA6,
		--		UPDATE_TIMESTAMP)
		--(	SELECT 
		--	 TSED.EVENT_FK
		--	,NULL
		--	,99
		--	,'MISSING DATA VALUE'  --ERROR NAME
		--	,'DATA VALUE WAS NOT INSERTED'--ERROR1
		--	,'CHECK INSERT MISSING FLAG FOR DATA NAME'
		--	,'OR CHECK LENGTH'
		--	,NULL
		--	,NULL
		--	,NULL
		--	,@l_sysdate
		--	FROM #TS_EVENT_DATA TSED
		--	WHERE data_type_cr_fk = 134
		--	AND TSED.RECORD_STATUS_CR_FK <> 2   --- NOT AN INACTIVATE
		--	AND DATA_VALUE_FK is NULL 
		--	AND ISNULL ( TSED.NEW_DATA, 'NULL') <> 'NULL' ) 


--------------------------ENTITY DATA VALUE

--			INSERT INTO #TS_EVENT_DATA_ERRORS (
--				EVENT_FK,
--				RULE_FK,
--				EVENT_CONDITION_CR_FK,
--				ERROR_NAME,
--				ERROR_DATA1,
--				ERROR_DATA2,
--				ERROR_DATA3,
--				ERROR_DATA4,
--				ERROR_DATA5,
--				ERROR_DATA6,
--				UPDATE_TIMESTAMP)
--		(	SELECT 
--			 TSED.EVENT_FK
--			,NULL
--			,99
--			,'MISSING ENTITY DATA VALUE'  --ERROR NAME      
--			,'ENTITY DATA VALUE WAS NOT INSERTED'--ERROR1
--			,'CHECK INSERT MISSING FLAG FOR DATA NAME'
--			,TSED.ENTITY_STRUCTURE_FK
--			,NULL
--			,NULL
--			,NULL
--			,@l_sysdate
--			FROM #TS_EVENT_DATA TSED
--			WHERE data_type_cr_fk = 135
--			AND ENTITY_DATA_VALUE_FK is NULL )

-----------------------------------NULL NEW DATA

--			INSERT INTO #TS_EVENT_DATA_ERRORS (
--				EVENT_FK,
--				RULE_FK,
--				EVENT_CONDITION_CR_FK,
--				ERROR_NAME,
--				ERROR_DATA1,
--				ERROR_DATA2,
--				ERROR_DATA3,
--				ERROR_DATA4,
--				ERROR_DATA5,
--				ERROR_DATA6,
--				UPDATE_TIMESTAMP)
--		(	SELECT 
--			 TSED.EVENT_FK
--			,NULL
--			,99
--			,'NEW DATA IS NULL'  --ERROR NAME
--			,NULL
--			,NULL
--			,NULL
--			,NULL
--			,NULL
--			,NULL
--			,@l_sysdate
--			FROM #TS_EVENT_DATA TSED
--			WHERE ISNULL ( new_data, 'NULL DATA' )= 'NULL DATA' )


-----------------------------------NUMERIC

--			INSERT INTO #TS_EVENT_DATA_ERRORS (
--				EVENT_FK,
--				RULE_FK,
--				EVENT_CONDITION_CR_FK,
--				ERROR_NAME,
--				ERROR_DATA1,
--				ERROR_DATA2,
--				ERROR_DATA3,
--				ERROR_DATA4,
--				ERROR_DATA5,
--				ERROR_DATA6,
--				UPDATE_TIMESTAMP)
--		(	SELECT 
--			 TSED.EVENT_FK
--			,NULL
--			,99
--			,'INCORRECT DATA TYPE'  --ERROR NAME         
--			,'DATA TYPE IN NOT NUMERIC'--ERROR1
--			,'CHECK FOR DATA SET FOR DATA TYPE'
--			,NULL
--			,NULL
--			,NULL
--			,NULL
--			,@l_sysdate
--			FROM #TS_EVENT_DATA TSED
--			WHERE data_type_cr_fk = 131
--			 AND isnumeric( new_data)= 0 )

-----------------------------------DATE

----- CINDY CORRECTED THIS SQL... SEE FOR EXAMPLE

--UPDATE #TS_EVENT_DATA
--SET NEW_DATA = 
--	(SELECT CONVERT ( VARCHAR, #TS_EVENT_DATA.NEW_DATA, 
--							   ( SELECT CAST ( CODE AS SMALLINT ) FROM EPACUBE.CODE_REF 
--								 WHERE CODE_REF_ID = ( SELECT ISNULL (DN.DATE_FORMAT_CR_FK, 733) 
--								 FROM epacube.data_name DN
--								 WHERE DN.DATA_NAME_ID = #TS_EVENT_DATA.DATA_NAME_FK )
--					) ) )
--WHERE data_type_cr_fk = 132 


--			INSERT INTO #TS_EVENT_DATA_ERRORS (
--				EVENT_FK,
--				RULE_FK,
--				EVENT_CONDITION_CR_FK,
--				ERROR_NAME,
--				ERROR_DATA1,
--				ERROR_DATA2,
--				ERROR_DATA3,
--				ERROR_DATA4,
--				ERROR_DATA5,
--				ERROR_DATA6,
--				UPDATE_TIMESTAMP)
--		(	SELECT 
--			 TSED.EVENT_FK
--			,NULL
--			,99
--			,'INCORRECT DATA TYPE'  --ERROR NAME       
--			,'DATA TYPE IN NOT A DATE'--ERROR1
--			,'CHECK FOR DATA SET FOR DATA TYPE'
--			,NULL
--			,NULL
--			,NULL
--			,NULL
--			,@l_sysdate
--			FROM #TS_EVENT_DATA TSED
--			WHERE data_type_cr_fk = 132
--			 AND isdate( new_data)= 0 )

-----------------------------------Y/N

--			INSERT INTO #TS_EVENT_DATA_ERRORS (
--				EVENT_FK,
--				RULE_FK,
--				EVENT_CONDITION_CR_FK,
--				ERROR_NAME,
--				ERROR_DATA1,
--				ERROR_DATA2,
--				ERROR_DATA3,
--				ERROR_DATA4,
--				ERROR_DATA5,
--				ERROR_DATA6,
--				UPDATE_TIMESTAMP)
--		(	SELECT 
--			 TSED.EVENT_FK
--			,NULL
--			,99
--			,'INCORRECT DATA TYPE'  --ERROR NAME        
--			,'DATA TYPE IN NOT YES/NO'--ERROR1
--			,'CHECK FOR DATA SET FOR DATA TYPE'
--			,NULL
--			,NULL
--			,NULL
--			,NULL
--			,@l_sysdate
--			FROM #TS_EVENT_DATA TSED
--			WHERE data_type_cr_fk = 133
--			 AND new_data NOT IN  ('Y', 'N' ))



-------------------------------------epaCUBE STATUS

----			INSERT INTO #TS_EVENT_DATA_ERRORS (
----				EVENT_FK,
----				RULE_FK,
----				EVENT_CONDITION_CR_FK,
----				ERROR_NAME,
----				ERROR_DATA1,
----				ERROR_DATA2,
----				ERROR_DATA3,
----				ERROR_DATA4,
----				ERROR_DATA5,
----				ERROR_DATA6,
----				UPDATE_TIMESTAMP)
----		(	SELECT 
----			 TSED.EVENT_FK
----			,NULL
----			,99
----			,'INCORRECT epaCUBE Entity Status Code'  --ERROR NAME        
----			,NULL  --ERROR1
----			,NULL
----			,NULL
----			,NULL
----			,NULL
----			,NULL
----			,@l_sysdate
----			FROM #TS_EVENT_DATA TSED
----			LEFT JOIN epacube.CODE_REF CR WITH (NOLOCK)
----			  ON ( CR.code_type = 'ENTITY_STATUS' AND CR.code = ISNULL ( TSED.new_data, 'NULL DATA' )  )
----			WHERE TSED.data_name_fk = 140001  --- EPACUBE ENTITY STATUS
----			AND   CR.code_ref_id IS NULL 
----        )




------------------------------------------------------------------------------------------
--  CHECK LENGTH    -- ADD TRUNC LATER
------------------------------------------------------------------------------------------


--			INSERT INTO #TS_EVENT_DATA_ERRORS (
--				EVENT_FK,
--				RULE_FK,
--				EVENT_CONDITION_CR_FK,
--				ERROR_NAME,
--				ERROR_DATA1,
--				ERROR_DATA2,
--				ERROR_DATA3,
--				ERROR_DATA4,
--				ERROR_DATA5,
--				ERROR_DATA6,
--				UPDATE_TIMESTAMP)
--		(	SELECT 
--			 TSED.EVENT_FK
--			,NULL
--			,CASE ISNULL ( DN.TRUNC_METHOD_CR_FK, 0 )
--			 WHEN 0 THEN 99
--			 ELSE 98
--			 END
--			,CASE ISNULL ( DN.MAX_OR_EQ, 'MAX' ) 
--			 WHEN 'MAX' THEN 'Length has exceeded maximum' 
--			 ELSE 'Length is not equal to' 
--			 END  --- error name   
--			,DN.LENGTH  --ERROR1
--			,'Current Length '
--			,LEN ( TSED.NEW_DATA )
--			,CASE ISNULL ( DN.TRUNC_METHOD_CR_FK, 0 )
--			 WHEN 0 THEN NULL
--			 ELSE 'Truncated'
--			 END
--			,NULL
--			,NULL
--			,@l_sysdate
--			FROM #TS_EVENT_DATA TSED
--            INNER JOIN epacube.DATA_NAME dn  WITH (NOLOCK) ON (dn.DATA_NAME_ID = tsed.DATA_NAME_FK)		
--			WHERE tsed.data_type_cr_fk in (130 , 134)  -- STRING, data value
----			AND   DN.LENGTH IS NOT NULL 
--			AND   (
--			        (     dn.max_or_eq = 'EQ' 
--			        AND  LEN ( TSED.NEW_DATA ) <> ISNULL (DN.LENGTH, 256) )
--			      OR
--				    (     LEN ( TSED.NEW_DATA ) > ISNULL (DN.LENGTH, 256) )			
--				  )
--			)
                        

--            UPDATE #TS_EVENT_DATA 
--            SET NEW_DATA = LEFT ( NEW_DATA, DN.LENGTH )
--			FROM EPACUBE.DATA_NAME DN
--			WHERE DN.DATA_NAME_ID = #TS_EVENT_DATA.DATA_NAME_FK
--			AND   DN.LENGTH IS NOT NULL
--			AND   DN.TRUNC_METHOD_CR_FK = 8501   -- LENGTH 


--*/


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_154'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POST_VALID_TYPE_154 has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
		 declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
         EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;		  
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END





































