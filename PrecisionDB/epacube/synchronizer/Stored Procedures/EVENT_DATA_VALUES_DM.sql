﻿
-- Copyright 2006
--
-- Procedure created by Walt Tucholski (original Oracle version by Kathi Scott)
--
--
-- Purpose: To support Data Master Editor functions
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- WT        07/05/2006   Initial SQL Version
-- KS        07/21/2006   Added the Validation for Rules ( String Lengths Only for Now ) 
-- WT        08/04/2006   Fix for EPA-356 
-- WT		 01/16/2007	  Changed to not use rule_validation.operator_cr_fk anymore.
--						  Will use validation_code_cr_fk only.
-- RG		 09/27/2007	  EPA-1202 Prevent delete of data values where user_editible_values <> 1
-- RG		 11/20/2007   EPA-427 Adding description Addition/Modification
-- KS        07/23/2009   Revisit with the SEED Changes... No more Vendor
-- RG		 11/12/2009   Adding EPA-2593 Server-side validations

CREATE PROCEDURE [synchronizer].[EVENT_DATA_VALUES_DM] 
( @in_data_name_fk int,
  @in_value varchar (100),
  @in_description varchar (128),
  @in_Manufacturer varchar (100),
  @in_table_id_fk bigint,
  @in_ui_action_cr_fk int,
  @in_update_user varchar (64) )
												
AS
/***************  Parameter Defintions  **********************
CURRENTLY USED IN THE DATA MASTER EDITOR....

Simple for Now.... Only CREATE and Delete;  No Change or Inactivate 

***************************************************************/
BEGIN

DECLARE @l_sysdate				 datetime
DECLARE @ls_stmt				 varchar (1000);
DECLARE @l_exec_no				 bigint;
DECLARE @l_rows_processed		 bigint
DECLARE @v_table_id_fk			 bigint
DECLARE @v_data_type_cr_fk       varchar (64);
DECLARE @v_entity_structure_fk	 bigint
DECLARE @v_count			     bigint
DECLARE @ErrorMessage			nvarchar(4000)
DECLARE @ErrorSeverity			int
DECLARE @ErrorState				int
--EPA-2593
DECLARE @DeleteUIActionCode		int
DECLARE @CreateUIActionCode		int
DECLARE @DataValueType			int
DECLARE @EntityDataValueType	int
DECLARE @LengthOperator			varchar(3);
DECLARE @Length					smallint		


SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ErrorMessage = NULL
--EPA-2593
SET @DeleteUIActionCode = epacube.getCodeRefId('UI_ACTION','DELETE')
SET @CreateUIActionCode = epacube.getCodeRefId('UI_ACTION','CREATE')
SET @DataValueType = epacube.getCodeRefId('DATA_TYPE','DATA VALUE')
SET @EntityDataValueType = epacube.getCodeRefId('DATA_TYPE','ENTITY DATA VALUE')

SELECT @v_data_type_cr_fk = ds.data_type_cr_fk
FROM epacube.data_name dn
INNER JOIN epacube.data_set ds on ( ds.data_set_id = dn.data_set_fk )
WHERE data_name_id = @in_data_name_fk;
--
---EPA-2593
	IF @in_ui_action_cr_fk = @CreateUIActionCode
	BEGIN select @LengthOperator = max_or_eq
				,@Length = length
		  from epacube.data_name
		  where data_name_id = @in_data_name_fk
		  and length is not null --and @LengthOperator is not null
	   IF @Length is not null
	   BEGIN
			SELECT @v_count = COUNT(1)
			FROM epacube.data_name
			WHERE ((len(@in_value)<> @Length and @LengthOperator = 'EQ')
					or
				  (len(@in_value) > @Length and @LengthOperator = 'MAX'))
					and data_name_id = @in_data_name_fk
	 		IF @v_count > 0
			BEGIN
				set @ErrorMessage = 'Value input length is out of tolerance.'
				GOTO RAISE_ERROR
			END
		END
	END ---end EPA-2593	validations
	
--END

IF  @v_data_type_cr_fk   =  @EntityDataValueType  -- Entity Data Value
BEGIN
	IF @in_ui_action_cr_fk = @DeleteUIActionCode
	BEGIN

		SELECT @v_count = COUNT(1)
		FROM epacube.product_category pec
		WHERE pec.entity_data_value_fk = @in_table_id_fk
	
	 	IF @v_count > 0
		BEGIN
			set @ErrorMessage = 'Cannot Delete Category; Products currently Reference its Value'
			GOTO RAISE_ERROR
		END
---------------------------------------------------------------
--Fixing for EPA 1202 - checking user editible
---------------------------------------------------------------
		select @ErrorMessage = 'Cannot delete value; values for this Data Value Type may not be edited'
		from epacube.data_name
		where ((select user_editible_values 
			   from epacube.data_name
				where data_name_id = @in_data_name_fk) <> 1)
		IF @ErrorMessage is not null
		BEGIN
			GOTO RAISE_ERROR
		END
---------------------------END EPA 1202 fix
	    ELSE
			BEGIN	
				DELETE FROM epacube.entity_data_value
				WHERE entity_data_value_id = @in_table_id_fk
			END
	END -- IF @in_ui_action_cr_fk = @DeleteUIActionCode

	IF @in_ui_action_cr_fk = @CreateUIActionCode	
    BEGIN 
	 	 
	 	IF @in_Manufacturer IS NULL 
		BEGIN
			set @ErrorMessage = 'Vendor is Required; Value is a Vendor Specific Value'
			GOTO RAISE_ERROR
		END
	    ELSE
			BEGIN
		      SELECT @v_entity_structure_fk = ei.entity_structure_fk
			  FROM epacube.entity_identification ei
			  INNER JOIN epacube.epacube_params eep ON ( ei.data_name_fk = CAST ( eep.value as int ) )
			  WHERE ei.VALUE = @in_Manufacturer
			  AND   eep.name = 'VENDOR'
			  AND   eep.application_scope_fk = 100

			  IF @@rowcount = 0
			  BEGIN
					SET @ErrorMessage = 'Unknown Vendor'
					GOTO RAISE_ERROR
			  END
			END

        SELECT @v_table_id_fk = entity_data_value_id
        FROM epacube.entity_data_value edv
        WHERE edv.data_name_fk = @in_data_name_fk
        AND edv.VALUE = @in_value
        AND edv.entity_structure_fk = @v_entity_structure_fk

        IF @v_table_id_fk IS NULL
        BEGIN

  /* -------------------------------------------------------------------------------------------------- */
  /*    String Length Operation Validation for Data Values                               */
  /* ------------------------------------------------------------------------------------------------- */
--Fix for EPA-356
--			  SELECT @ErrorMessage = rv.name
--			  FROM epacube.synchronizer.RULE_VALIDATION rv 
--			  WHERE rv.data_name_fk = @in_data_name_fk
--			  AND   (
--						(rv.VALIDATION_CODE_CR_FK = 10001 AND LEN (@in_value) > rv.number1)
--					  OR
--						(rv.VALIDATION_CODE_CR_FK = 10002 AND LEN (@in_value) <> rv.number1)
--					)

			  IF @ErrorMessage IS NOT NULL
			  BEGIN
				  GOTO RAISE_ERROR
			  END
   
              INSERT INTO epacube.entity_data_value
                          (VALUE,
						   description,
                           data_name_fk,
						   entity_structure_fk,
						   update_user)
                    SELECT @in_value,
						   @in_description,
						   @in_data_name_fk,
						   @v_entity_structure_fk,
						   @in_update_user

        END -- IF @v_table_id_fk IS NULL

--        COMMIT;
	END -- IF @in_ui_action_cr_fk = @CreateUIActionCode	
	 
END -- IF entity data value	

IF @v_data_type_cr_fk   =  @DataValueType  -- Data Value
BEGIN	
	IF @in_ui_action_cr_fk = @DeleteUIActionCode
	BEGIN	

		SELECT @v_count = COUNT(1)
		FROM epacube.product_category pc
		WHERE pc.data_value_fk = @in_table_id_fk 

	 	IF @v_count > 0
		BEGIN
			set @ErrorMessage = 'Cannot Delete Category; Products currently Reference its Value'
			GOTO RAISE_ERROR
		END
	    ELSE
			BEGIN
				DELETE FROM epacube.data_value
				WHERE data_value_id = @in_table_id_fk
			END
	END -- IF @in_ui_action_cr_fk = @DeleteUIActionCode
	
	IF @in_ui_action_cr_fk = @CreateUIActionCode	
    BEGIN

	 	IF @in_Manufacturer IS NOT NULL
		BEGIN
			set @ErrorMessage = 'Do Not enter Vendor; Not a Vendor Specific Value'
			GOTO RAISE_ERROR
		END
	 	 
	     SELECT @v_table_id_fk = data_value_id
		 FROM epacube.data_value dv
		 WHERE dv.data_name_fk = @in_data_name_fk
			AND dv.VALUE = @in_value
		
		 IF @v_table_id_fk IS NULL
		 BEGIN

  /* -------------------------------------------------------------------------------------------------- */
  /*    String Length Operation Validation for Data Values                               */
  /* ------------------------------------------------------------------------------------------------- */
--Fix for EPA-356
--			  SELECT @ErrorMessage = rv.name
--			  FROM epacube.synchronizer.RULE_VALIDATION rv 
--			  WHERE rv.data_name_fk = @in_data_name_fk
--			  AND   (
--						(rv.VALIDATION_CODE_CR_FK = 10001 AND LEN (@in_value) > rv.number1)
--					  OR
--						(rv.VALIDATION_CODE_CR_FK = 10002 AND LEN (@in_value) <> rv.number1)
--					)

			  IF @ErrorMessage IS NOT NULL
			  BEGIN
				  GOTO RAISE_ERROR
			  END	
   
		      INSERT INTO epacube.data_value
                     (data_name_fk,
					  VALUE,
					  description,
					  update_user )
               SELECT @in_data_name_fk,
                      @in_VALUE,
					  @in_description,
					  @in_update_user
         END

--        COMMIT;

	END -- IF @in_ui_action_cr_fk = @CreateUIActionCode
	
	
END -- IF @v_table_name  = 'PRODUCT_CATEGORY'


  /* -------------------------------------------------------------------------------------------------- */
  /*    String Length Operation Validation for Data Values                               */
  /* ------------------------------------------------------------------------------------------------- */
      
--      SELECT @ErrorMessage = rv.name
--	  FROM epacube.synchronizer.RULE_VALIDATION rv 
--      WHERE rv.data_name_fk = @in_data_name_fk
--      AND   rv.VALIDATION_CODE_CR_FK = 10001
--      AND   (
--               (   rv.operator_cr_fk = 9003
--               AND (LEN ( @in_value ) >  rv.number1 ) )
--           OR
--               (   rv.operator_cr_fk = 9005
--               AND ( LEN ( @in_value ) <>  rv.number1 ) )
--            )

RAISE_ERROR:
	IF @ErrorMessage IS NOT NULL
	BEGIN
		set @ErrorSeverity = 16
		set @ErrorState = 1
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState) 
		return(-1)
    END		

END
