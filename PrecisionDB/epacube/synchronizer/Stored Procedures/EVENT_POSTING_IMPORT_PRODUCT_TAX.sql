﻿















-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To support event processing for imports.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        02/28/2010   Initial SQL Version
-- CV        01/13/2017   Rules type cr fk to rules table 

CREATE PROCEDURE [synchronizer].[EVENT_POSTING_IMPORT_PRODUCT_TAX] 
        ( @in_batch_no BIGINT, @in_import_job_fk BIGINT, 
          @in_event_type_cr_fk INT,
          @in_table_name VARCHAR(64),
          @in_data_name_fk BIGINT,
          @in_org_entity_structure_fk VARCHAR(16) )



AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_PRODUCT_TAX.'
					 + 'Batch: ' + cast(isnull(@in_batch_no, 0) as varchar(30))
					 + 'Import: ' + cast(isnull(@in_import_job_fk, 0) as varchar(30))	
					 + 'Event Type: ' + cast(isnull(@in_event_type_cr_fk, 0) as varchar(30))					 				 
					 + 'Table: ' + cast(isnull(@in_table_name, 'UNKNOWN' ) as varchar(30))					 				 					 					 
					 + 'Data Name: ' + cast(isnull(@in_data_name_fk, 0 ) as varchar(30))
					 
					 							 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_job_id = @in_import_job_fk,
                                          @epa_batch_no = @in_batch_no,
						   				  @exec_id   = @l_exec_no OUTPUT;




------------------------------------------------------------------------------------------
--	PRODUCT TAX ATTRIBUTE EVENTS DIRECTLY FROM IMPORT_RECORD_NAME_VALUE
------------------------------------------------------------------------------------------


INSERT INTO #TS_EVENT_DATA 
			   ([EVENT_TYPE_CR_FK]
			   ,[EVENT_ENTITY_CLASS_CR_FK]
			   ,[EVENT_EFFECTIVE_DATE]
			   ,[EPACUBE_ID]
			   ,[DATA_NAME_FK]
			   ,[NEW_DATA]			   
			   ,[CURRENT_DATA]			   			   
			   ,[PRODUCT_STRUCTURE_FK]	
			   ,[ORG_ENTITY_STRUCTURE_FK]			   		   
			   ,[ENTITY_CLASS_CR_FK]
			   ,[ENTITY_DATA_NAME_FK]			   			   
			   ,[ENTITY_STRUCTURE_FK]
			   ,[EVENT_CONDITION_CR_FK]
			   ,[EVENT_STATUS_CR_FK]
			   ,[EVENT_PRIORITY_CR_FK]
			   ,[EVENT_SOURCE_CR_FK]
			   ,[EVENT_ACTION_CR_FK]
			   ,[JOB_CLASS_FK]
			   ,[IMPORT_JOB_FK]
			   ,[IMPORT_DATE]
			   ,[IMPORT_FILENAME]
			   ,[IMPORT_PACKAGE_FK]
			   ,[STG_RECORD_FK]
			   ,[BATCH_NO]
			   ,[IMPORT_BATCH_NO]			   			   
			   ,[TABLE_ID_FK]			   
			   ,[VALUE_EFFECTIVE_DATE]
			   ,[END_DATE_NEW]
			   ,[RESULT_TYPE_CR_FK]
--------- ADDED FOR POSTING PROCESS
			   ,[DATA_SET_FK]
			   ,[DATA_TYPE_CR_FK]
			   ,[ORG_IND]
			   ,[INSERT_MISSING_VALUES]
			   ,[TABLE_NAME]
			   ,[EFFECTIVE_DATE_CUR]
			   ,[DATA_VALUE_FK]
			   ,[ENTITY_DATA_VALUE_FK]
			   ,[RULES_FK]	
			   ,[RECORD_STATUS_CR_FK]			   					   
			   ,[UPDATE_TIMESTAMP]
			   ,[UPDATE_USER])			   
-----------------------------
SELECT 
	 DS.EVENT_TYPE_CR_FK
	,DS.ENTITY_CLASS_CR_FK
	,CASE WHEN ( SREC.EFFECTIVE_DATE < @L_SYSDATE )
	      THEN @L_SYSDATE
	      ELSE SREC.EFFECTIVE_DATE
	 END
	,SRECS.SEARCH_STRUCTURE_FK
	,DN.DATA_NAME_ID
	,IRNV.DATA_VALUE
	,PTAX.ATTRIBUTE_EVENT_DATA
	,(	CASE ( SREC.ENTITY_CLASS_CR_FK )
		 WHEN (select code_ref_id from epacube.code_ref
				where code_type = 'ENTITY_CLASS' 
				and Code = 'PRODUCT')
		 THEN SRECS.SEARCH_STRUCTURE_FK
		 ELSE NULL
		 END )		 
    ,1     --- ORG_ENTITY_STRUCTURE_FK
	,NULL  --- ENTITY_STRUCTURE_FK
	,NULL  --- ENTITY_DATA_NAME_FK	
	,NULL  --- ENTITY_STRUCTURE_FK
	--
	,97
	,88  -- PROCESSING
	,61  -- AWAITING APPROVAL  AUTO SYNC RULES WILL SET OTHERWISE
	,SREC.EVENT_SOURCE_CR_FK
	,CASE ISNULL ( PTAX.PRODUCT_TAX_ATTRIBUTE_ID, 0 )
	 WHEN 0 THEN 51
	 ELSE 52
	 END
	--
	,IP.JOB_CLASS_FK
	,SREC.JOB_FK
	,SREC.CREATE_TIMESTAMP
	,SREC.IMPORT_FILENAME
	,SREC.IMPORT_PACKAGE_FK
	,SREC.STG_RECORD_ID
	,@IN_BATCH_NO
	,@IN_BATCH_NO
	,PTAX.PRODUCT_TAX_ATTRIBUTE_ID
	,SREC.EFFECTIVE_DATE
	,SREC.END_DATE
	,SREC.RESULT_TYPE_CR_FK
--------- ADDED FOR POSTING PROCESS
    ,DS.DATA_SET_ID
    ,DS.DATA_TYPE_CR_FK
    ,DS.ORG_IND
    ,DN.INSERT_MISSING_VALUES
    ,DS.TABLE_NAME
    ,PTAX.UPDATE_TIMESTAMP
    ,NULL  ---VCPD.DATA_VALUE_FK
    ,NULL  ---VCPD.ENTITY_DATA_VALUE_FK
    ,NULL  ---[RULES_FK]
    ,1  -- RECORD_STATUS_CR_FK				   	    						   	
	,@L_SYSDATE
	,'EPACUBE'
----------------	
    FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
	INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
	  ON ( SRECS.STG_RECORD_FK = SREC.STG_RECORD_ID )	                	                
    INNER JOIN IMPORT.IMPORT_RECORD_NAME_VALUE IRNV WITH (NOLOCK)
          ON ( IRNV.JOB_FK = SREC.JOB_FK
          AND  IRNV.RECORD_NO = SREC.RECORD_NO )
    INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
          ON ( DN.NAME = IRNV.DATA_NAME )
    INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
          ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
    INNER JOIN IMPORT.IMPORT_PACKAGE IP WITH (NOLOCK)
		  ON ( IP.IMPORT_PACKAGE_ID = SREC.IMPORT_PACKAGE_FK ) 
	LEFT JOIN epacube.PRODUCT_TAX_ATTRIBUTE PTAX
	  ON ( PTAX.DATA_NAME_FK = DN.DATA_NAME_ID
	  AND  PTAX.PRODUCT_STRUCTURE_FK = SRECS.SEARCH_STRUCTURE_FK )
    WHERE 1 = 1		  
    AND   ISNULL(SREC.BATCH_NO,-999) = isnull (@in_batch_no, -999) 
    AND   SREC.EVENT_ACTION_CR_FK IN ( 51, 52, 53, 54 ) 
	AND   SREC.EVENT_CONDITION_CR_FK IN ( 97, 98 )
	AND   DN.DATA_NAME_ID = @in_data_name_fk
    AND   IRNV.DATA_VALUE IS NOT NULL 
    AND   IRNV.DATA_VALUE  <> ' '
    AND   IRNV.DATA_VALUE <> ISNULL ( PTAX.ATTRIBUTE_EVENT_DATA, 'NULL DATA' )



  			                             
--------------------------------------------------------------------------------------------
--   CALL VALIDATIONS
--------------------------------------------------------------------------------------------
    

	------------------------------------------------------------------------------------------
	--   QUALIFY GOVERNANCE RULES 
	------------------------------------------------------------------------------------------
	SET  @v_count = ( SELECT COUNT(1)
					 FROM #TS_EVENT_DATA WITH (NOLOCK)
					 WHERE BATCH_NO = @in_batch_no
					 AND   EVENT_STATUS_CR_FK = 88 
					 AND   EVENT_TYPE_CR_FK IN ( 150, 151, 152, 153, 154, 155, 156, 160 )   			 
					 AND   (  EVENT_SOURCE_CR_FK = 71        -- WHEN UI CALLS VALIDATION DIRECTLY WILL REMOVE 71 
						   OR ISNULL ( IMPORT_BATCH_NO, -999 ) = @in_batch_no )    
					 AND   DATA_NAME_FK IN ( SELECT R.RESULT_DATA_NAME_FK   
											 FROM synchronizer.RULES R WITH (NOLOCK)
											 --INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
											 --  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK 
											 --  AND  RS.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE
											 WHERE R.RECORD_STATUS_CR_FK = 1 
											 AND  R.RULE_TYPE_CR_FK = 307 )  -- GOVERNANCE)
					)			                         
	IF @v_count > 0
		EXEC synchronizer.EVENT_POST_QUALIFY_RULES_GOVERNANCE  @in_batch_no


    EXEC [synchronizer].[EVENT_POST_VALID_DATA_TYPE] 

    EXEC [synchronizer].[EVENT_POST_VALID_TYPE_153] 
	
	EXEC SYNCHRONIZER.EVENT_POST_VALID_DATA_VALUE
		    
	EXEC [synchronizer].[EVENT_POST_VALID_TYPE_ALL] 		    


------------------------------------------------------------------------------------------
--  EVENT is a TAXONOMY TREE NODE REASSIGMENT
------------------------------------------------------------------------------------------

SET @V_COUNT =   ( SELECT COUNT(1)
					FROM #TS_EVENT_DATA TSED WITH (NOLOCK)
					INNER JOIN epacube.TAXONOMY_TREE TT WITH (NOLOCK)
					 ON ( TT.TAX_NODE_DN_FK = TSED.DATA_NAME_FK )
					WHERE TSED.CURRENT_DATA IS NOT NULL
					AND   TSED.NEW_DATA <> ISNULL ( TSED.CURRENT_DATA, 'NULL DATA' )
				 )
               
IF @V_COUNT > 0               
EXEC synchronizer.EVENT_POST_VALID_TAX_REASSIGN

    
------------------------------------------------------------------------------------------
-- CALL PROC TO SET EVENT PRIORITY ACTION AND STATUS   ( KEEP IN ONE PLACE WITH CODE )
------------------------------------------------------------------------------------------

	EXEC  [synchronizer].[EVENT_POST_COND_PRIORITY_ACTION_STATUS] @in_batch_no


--------------------------------------------------------------------------------------------
--   CALL POST AND COMPLETE 
--------------------------------------------------------------------------------------------

		EXEC SYNCHRONIZER.EVENT_POST_DATA  

		UPDATE #TS_EVENT_DATA
		SET EVENT_STATUS_CR_FK = 80   -- RETURN TO PENDING
		WHERE EVENT_STATUS_CR_FK in ( 86, 87, 88 ) 


       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			SET @status_desc = 'total rows Returned to Pending = '
				+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_batch_no = @in_batch_no
       END
		


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_PRODUCT_TAX'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_POSTING_IMPORT_PRODUCT_TAX has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END















































