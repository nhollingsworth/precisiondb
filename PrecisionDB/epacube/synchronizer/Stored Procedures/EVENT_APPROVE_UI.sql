﻿







-- Copyright 2008
--
-- Procedure Called from Synchronizer UI to submit events for approval
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -----------------------------------------------
-- KS        10/27/2008   Revised Original SQL Version to include Queue
--
CREATE PROCEDURE [synchronizer].[EVENT_APPROVE_UI]
  @in_service_broker smallint = 1
AS
BEGIN

DECLARE @l_sysdate    datetime
DECLARE @ls_stmt      varchar (1000)
DECLARE @ls_queue     nvarchar (4000)
DECLARE @l_exec_no    bigint
DECLARE @l_rows_processed    BIGINT

DECLARE @v_batch_no   bigint
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

DECLARE @status_desc  varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

BEGIN TRY
SET NOCOUNT ON
SET @l_sysdate = getdate ()

SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of SYNCHRONIZER.EVENT_APPROVE_UI', 
												@exec_id = @l_exec_no OUTPUT;


----------------------------------------------------------------------------------------------------
--   Verify There are Events to Process
---------------------------------------------------------------------------------------------------

   SELECT @v_count = count(1) 
   FROM   synchronizer.EVENT_DATA
   WHERE  event_status_cr_fk in (86, 87)  


/**********************************************************************************/
--   Cindy get with YURI on placing this JOB in the QUEUE process....
---      and that ONLY one JOB runs.. such that if a job is already
--       in the queue we do not submit another 
/**********************************************************************************/



   IF @v_count > 0    -- Events Submitted
   BEGIN


----------------------------------------------------------------------------------------------------
--   Assign Batch_No  ( but leave Submitted... Event_Posting will set to Processing )
---------------------------------------------------------------------------------------------------


	EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'batch_seq', @v_batch_no output

    UPDATE synchronizer.EVENT_DATA
    SET BATCH_NO = @v_batch_no
       ,EVENT_STATUS_CR_FK = 87
    WHERE EVENT_STATUS_CR_FK = 86



/**********************************************************************************/
--   CREATE NEW JOB 
/**********************************************************************************/

   
   EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'job_seq', @v_job_fk output


    
    SET @v_job_class_fk  = 620
    SET @v_job_user = 'EPACUBE'
    SET @v_job_name = 'EVENT APPROVAL'
    SET @v_job_date = @l_sysdate
    SET @v_data1   = NULL
    SET @v_data2   = NULL 

    EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
										   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;


    EXEC common.job_execution_create   @v_job_fk, 'APPROVING SUBMITTED EVENTS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate

    

-------------------------------------------------------------------------------------
--   Call Event Posting on the Queue <<<<<------ Need to ENQUEUE HERE YURI & CINDY
-------------------------------------------------------------------------------------

------EXEC synchronizer.EVENT_POSTING @V_batch_no

IF @in_service_broker = 1
BEGIN
	
SET @ls_queue = 'EXEC synchronizer.EVENT_POSTING @in_batch_no = ' 
              + CAST ( @v_batch_no AS VARCHAR(16)) 


EXEC queue_manager.enqueue
	@service_name = N'TargetEpaService', --  nvarchar(50)
	@cmd_text = @ls_queue, --  nvarchar(4000)
	@epa_job_id = @v_job_fk, --  int
	@cmd_type = N'SQL' --  nvarchar(10)

END
ELSE
BEGIN

	EXEC synchronizer.EVENT_POSTING @v_batch_no
	
END


END   -- no submitted events




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.EVENT_APPROVE_UI'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;


END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.EVENT_APPROVE_UI has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
          declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END





















