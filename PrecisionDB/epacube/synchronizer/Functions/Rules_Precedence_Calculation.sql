﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [synchronizer].[Rules_Precedence_Calculation]
(	
	@precedence as int, @org_segment_fk as bigint, @Org_Segment_Data_Name_FK as bigint, @vendor_segment_fk as bigint, @customer_precedence as int, @PRODUCT_PRECEDENCE as int
)
RETURNS
@CalcPrec table
(
  CalcPrec bigint  --varchar(max)
)
AS
Begin
	Declare @Prec as varchar(5)
	Set @Prec = 
				--Cast(@precedence as varchar(1))
				--	+
					Cast(Case (Select Rules_Segment_fk_Name from epacube.controls where Control_Name = 'Precedence_Sequence' and Control_Precedence = 1) 
						when 'cust_segment_fk' then @customer_precedence
						when 'prod_segment_fk' then @PRODUCT_PRECEDENCE
						when 'Org_Segment_FK' then 
							Case when @org_segment_fk <> 0 then 
								isnull((Select Control_Precedence from epacube.controls where Control_Name = 'Org_Segment_Data_Name_FK' 
									and Control_data_name_fk = @Org_Segment_Data_Name_FK), 9) 
								else 9 end
						when 'Vendor_Segment_FK' then Case when @vendor_segment_fk <> 0 then 1 else 9 end
						end as varchar(1))
					+
					Cast(Case (Select Rules_Segment_fk_Name from epacube.controls where Control_Name = 'Precedence_Sequence' and Control_Precedence = 2) 
						when 'cust_segment_fk' then @customer_precedence
						when 'prod_segment_fk' then @PRODUCT_PRECEDENCE
						when 'Org_Segment_FK' then 
							Case when @org_segment_fk <> 0 then 
								isnull((Select Control_Precedence from epacube.controls where Control_Name = 'Org_Segment_Data_Name_FK' 
									and Control_data_name_fk = @Org_Segment_Data_Name_FK), 9) 
								else 9 end
						when 'Vendor_Segment_FK' then Case when @vendor_segment_fk <> 0 then 1 else 9 end
						end as varchar(1))

					+
					Cast(Case (Select Rules_Segment_fk_Name from epacube.controls where Control_Name = 'Precedence_Sequence' and Control_Precedence = 3) 
						when 'cust_segment_fk' then @customer_precedence
						when 'prod_segment_fk' then @PRODUCT_PRECEDENCE
						when 'Org_Segment_FK' then 
							Case when @org_segment_fk <> 0 then 
								isnull((Select Control_Precedence from epacube.controls where Control_Name = 'Org_Segment_Data_Name_FK' 
									and Control_data_name_fk = @Org_Segment_Data_Name_FK), 9) 
								else 9 end
						when 'Vendor_Segment_FK' then Case when @vendor_segment_fk <> 0 then 1 else 9 end
						end as varchar(1))

					+
					Cast(Case (Select Rules_Segment_fk_Name from epacube.controls where Control_Name = 'Precedence_Sequence' and Control_Precedence = 4) 
						when 'cust_segment_fk' then @customer_precedence
						when 'prod_segment_fk' then @PRODUCT_PRECEDENCE
						when 'Org_Segment_FK' then 
							Case when @org_segment_fk <> 0 then 
								isnull((Select Control_Precedence from epacube.controls where Control_Name = 'Org_Segment_Data_Name_FK' 
									and Control_data_name_fk = @Org_Segment_Data_Name_FK), 9) 
								else 9 end
						when 'Vendor_Segment_FK' then Case when @vendor_segment_fk <> 0 then 1 else 9 end
						end as varchar(1))
	INSERT INTO @CalcPrec (CalcPrec) VALUES (@Prec)
	Return
End



