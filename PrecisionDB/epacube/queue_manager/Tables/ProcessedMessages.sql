﻿CREATE TABLE [queue_manager].[ProcessedMessages] (
    [ID]             UNIQUEIDENTIFIER NOT NULL,
    [MessageBody]    XML              NOT NULL,
    [ServiceName]    NVARCHAR (MAX)   NOT NULL,
    [CreateDateTime] DATETIME         CONSTRAINT [DF_ProcessedMessages_CreateDateTime] DEFAULT (getdate()) NULL
);



