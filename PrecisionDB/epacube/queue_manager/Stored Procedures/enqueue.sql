﻿CREATE PROCEDURE [queue_manager].[enqueue]
    (
      @service_name NVARCHAR(50),
      @cmd_text NVARCHAR(4000),
      @epa_job_id INT,
      @cmd_type NVARCHAR(10) = 'SQL'
    )
AS --********************************************************************
--*  Sending a message from the InitiatorService to the TargetService
--********************************************************************
    SET NOCOUNT ON ;
    BEGIN TRY

        DECLARE @ch UNIQUEIDENTIFIER
        DECLARE @msg NVARCHAR(MAX),
            @l_exec_no BIGINT,
            @status_desc VARCHAR(8000) ;
        SET @status_desc = 'Enquing Statement: ' + @cmd_text ;
        EXEC exec_monitor.start_monitor_sp @exec_desc = @status_desc,
            @exec_id = @l_exec_no OUTPUT ;

        BEGIN TRANSACTION ;
        BEGIN DIALOG CONVERSATION @ch FROM SERVICE [InitiatorService] TO
            SERVICE @service_name ON CONTRACT [http://epacube.com/EPAContract]
            WITH ENCRYPTION = OFF ;

        SET @msg = '<epaRequest>' + '<cmdtext>' + @cmd_text + '</cmdtext>'
            + '<cmdtype>' + @cmd_type + '</cmdtype>' + '<epajobid>'
            + CAST(@epa_job_id AS VARCHAR(40)) + '</epajobid>'
            + '</epaRequest>' ;

        SEND ON CONVERSATION @ch MESSAGE TYPE
            [http://epacube.com/RequestMessage] ( @msg ) ;
        COMMIT ;
    END TRY
    BEGIN CATCH   
        DECLARE @ErrorMessage NVARCHAR(4000) ;
        DECLARE @ErrorSeverity INT ;
        DECLARE @ErrorState INT ;

        SELECT  @ErrorMessage = ERROR_MESSAGE(),
                @ErrorSeverity = ERROR_SEVERITY(),
                @ErrorState = ERROR_STATE() ;


        EXEC exec_monitor.Report_Error_sp @l_exec_no ;
        EXEC exec_monitor.email_error_sp @l_exec_no,
            'dbexceptions@epacube.com' ;
        RAISERROR ( @ErrorMessage, -- Message text.
            @ErrorSeverity, -- Severity.
            @ErrorState -- State.
				    ) ;
    END CATCH ;
--PRINT @msg;




/****** Object:  StoredProcedure [queue_manager].[ProcessRequestMessages]    Script Date: 10/04/2008 10:47:12 ******/
    SET ANSI_NULLS ON
