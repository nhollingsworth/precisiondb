﻿--************************************************************************
--*  A stored procedure used for internal activation on the target queue
--************************************************************************
CREATE PROCEDURE [queue_manager].[ProcessRequestMessages]
AS 
    DECLARE @ch UNIQUEIDENTIFIER -- conversation handle
    DECLARE @messagetypename NVARCHAR(256)
    DECLARE @messagebody XML
    DECLARE @responsemessage XML
    DECLARE @queue_id INT
    DECLARE @queue_name NVARCHAR(MAX)
    DECLARE @sql NVARCHAR(MAX)
    DECLARE @param_def NVARCHAR(MAX) ;
    DECLARE @l_exec_no BIGINT,
        @status_desc VARCHAR(8000),
        @job_queue VARCHAR(4000),
        @l_epa_job_id BIGINT ;
    DECLARE @ErrorMessage NVARCHAR(4000) ;
    DECLARE @ErrorSeverity INT ;
    DECLARE @ErrorState INT ;
    

	-- Determining the queue for that the stored procedure was activated
    SELECT  @queue_id = queue_id
    FROM    sys.dm_broker_activated_tasks
    WHERE   spid = @@SPID

    SELECT  @queue_name = [name]
    FROM    sys.service_queues
    WHERE   object_id = @queue_id
    
--    SET @queue_name = 'TargetEpaQueue'

	-- Creating the parameter substitution
    SET @param_def = '
		@ch UNIQUEIDENTIFIER OUTPUT,
		@messagetypename NVARCHAR(MAX) OUTPUT,
		@messagebody XML OUTPUT'

	-- Creating the dynamic T-SQL statement, which does a query on the actual queue
    SET @sql = '
		WAITFOR (
			RECEIVE TOP(1)
				@ch = conversation_handle,
				@messagetypename = message_type_name,
				@messagebody = CAST(message_body AS XML)
			FROM ' + '[queue_manager].' + QUOTENAME(@queue_name) + '
		), TIMEOUT 60000'

    WHILE ( 1 = 1 )
        BEGIN
            BEGIN TRY
                --BEGIN TRANSACTION

			-- Executing the dynamic T-SQL statement that contains the actual queue
                EXEC sp_executesql @sql, @param_def, @ch = @ch OUTPUT,
                    @messagetypename = @messagetypename OUTPUT,
                    @messagebody = @messagebody OUTPUT

                IF ( @@ROWCOUNT = 0 ) 
                    BEGIN
                        --ROLLBACK TRANSACTION;
                        BREAK;
                    END

                IF ( @messagetypename = 'http://epacube.com/ResponseMessage' ) 
                    BEGIN
                        -- End the conversation on the target's side
                        
                        END CONVERSATION @ch ;
                        --COMMIT TRANSACTION;
                        BREAK ;
                    END ;

                IF ( @messagetypename = 'http://epacube.com/RequestMessage' ) 
                    BEGIN
				
				
				----------------------------------------
                -- process the message here YN 7.25.2008
                ----------------------------------------
                
                        DECLARE @s_cmd VARCHAR(8000)
                        SET @s_cmd = @messagebody.value('(/epaRequest/cmdtext/text())[1]',
                                                        'nvarchar(max)')
                        DECLARE @s_cmdtype NVARCHAR(10)
                        SET @s_cmdtype = @messagebody.value('(/epaRequest/cmdtype/text())[1]',
                                                            'nvarchar(10)')
                        DECLARE @s_epa_job_id VARCHAR(20)
                        SET @l_epa_job_id = @messagebody.value('(/epaRequest/epajobid/text())[1]', 'bigint')
                    
                        SET @status_desc = 'Processing Enqueued Statement: '
                            + @s_cmd ;
                        EXEC exec_monitor.start_monitor_sp @exec_desc = @status_desc,
                            @exec_id = @l_exec_no OUTPUT,
                            @epa_job_id = @s_epa_job_id ;
                
                
                        BEGIN TRY 
--                            SET @job_queue = ''
                           
                    
                            BEGIN
                                DECLARE @c_sql_type VARCHAR(10)
                                DECLARE @c_cmd_type VARCHAR(10)
                                SELECT  @c_sql_type = 'SQL',
                                        @c_cmd_type = 'CMD' ; 
                            
                                IF ( UPPER(RTRIM(LTRIM(@s_cmdtype))) = @c_sql_type ) 
                                    EXEC exec_manager.Exec_Immediate_sp @exec_id = @l_exec_no,
                                        @statement = @s_cmd,
                                        @epa_job_id = @l_epa_job_id ;
								   
                                IF ( UPPER(RTRIM(LTRIM(@s_cmdtype))) = @c_cmd_type ) 
                                    BEGIN
                                    
                                        BEGIN TRY 
                                            EXEC master..xp_cmdshell @s_cmd,
                                                NO_OUTPUT
                                        END TRY
                                        BEGIN CATCH
                                            SET @ErrorMessage = 'The EXTERNAL Command ('
                                                + @s_cmd
                                                + ')  failed with Error: '
                                                + ERROR_MESSAGE()
                                            SET @ErrorSeverity = ERROR_SEVERITY()
                                            SET @ErrorState = ERROR_STATE()
                                            RAISERROR ( @ErrorMessage,
                                                @ErrorSeverity, @ErrorState )
                                        END CATCH ;
                                    END
                                    
                                
                                
                                INSERT  INTO queue_manager.ProcessedMessages
                                        (
                                          ID,
                                          MessageBody,
                                          ServiceName
                                        
                                        )
                                VALUES  (
                                          NEWID(),
                                          @messagebody,
                                          @queue_name
                                        
                                        ) ;  
                                      
                            END
                	
                        END TRY    
                        BEGIN CATCH   
                            
                            SELECT  @ErrorMessage = ERROR_MESSAGE(),
                                    @ErrorSeverity = ERROR_SEVERITY(),
                                    @ErrorState = ERROR_STATE() ;


                            EXEC exec_monitor.Report_Error_sp @l_exec_no,
                                @l_epa_job_id ;
                            EXEC exec_monitor.email_error_sp @l_exec_no,
                                'dbexceptions@epacube.com' ;
                            IF @@TRANCOUNT > 0 
                                COMMIT TRANSACTION
                        END CATCH ;

                       
                
                ----------------------------------------
                -- end processing the message
                ----------------------------------------
                       
				-- End the conversation on the target's side
                        END CONVERSATION @ch ;
                        BREAK ;
                    END

                IF ( @messagetypename = 'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog' ) 
                    BEGIN
				-- End the conversation
                        END CONVERSATION @ch ;
                        BREAK ;
                    END


                IF ( @messagetypename = 'http://schemas.microsoft.com/SQL/ServiceBroker/Error' ) 
                    BEGIN
				-- End the conversation
                        END CONVERSATION @ch ;
                        BREAK ;
                    END
                IF @@TRANCOUNT > 0 
                    COMMIT TRANSACTION
            END TRY
            BEGIN CATCH
                SET @l_exec_no = ISNULL(@l_exec_no, 999999) ;
                IF @l_exec_no = 0 
                    SET @l_exec_no = 999999 ;
                EXEC exec_monitor.Report_Error_sp @exec_id = @l_exec_no ; 
                IF @@TRANCOUNT > 0 
                ROLLBACK TRANSACTION
            END CATCH
        END
    IF @@TRANCOUNT > 0 
        COMMIT TRANSACTION
