﻿

/****** Script for SelectTopNRows command from SSMS  ******/

--drop view custom.v_promoPricing
CREATE view [custom].[V_PromoPricing]
with schemabinding
as

SELECT distinct [Company] as [Company #]
      ,immp.[Price_Sheet_id] as [Price Sheet #]
      ,[Price_Sheet_Description] as [Price Sheet Descrip]
      ,[Price_Sheet_Page] as [Promo Page #]
      ,[Flyer_Month]
      ,[Warehouses] as [WHSE Code]
      ,immp.[ProductValue]
	  ,[Whse Costs Effective Date]
	  ,[Whse Costs End Date]
	  ,[Replacement Cost]
	  ,[Standard Cost]
	  ,[Store DC Costs Effective Date]
	  ,[Store DC Costs End Date]
	  ,[Base Price]
	  ,[Corp Costs Effective Date]
	  ,[Corp Costs End Date]
	  ,[Corp Price]
	  ,[End User Price Effective Date]
	  ,[End User Price End Date]
	  ,[End User Each Price]
	  ,[Start Date Corp ICSW]
	  ,[End Date Corp ICSW]

	  ,[Corp Replacement Cost]
	  ,[Corp Standard Cost]
	  ,[CORP COSTS Record_Status]
	  ,[PD Record_Status]
	  ,[END USER COSTS Record_Status]
FROM [import].[IMPORT_MASS_MAINTENANCE_PROMO] immp
LEFT JOIN	  (
	  
--100100500 CORP info	  
SELECT [ProductValue] 
	,[Price_Sheet_id]
      ,[StartDate] as [Whse Costs Effective Date]
      ,[EndDate] as [Whse Costs End Date]
      ,[Replacement_Cost] as [Replacement Cost]
      ,[Standard_Cost] as [Standard Cost]
	  ,[StartDateCorpICSW] as [Start Date Corp ICSW]
	  ,[EndDateCorpICSW] as [End Date Corp ICSW]
	  ,CorpReplacementCost as [Corp Replacement Cost]
	  ,CorpStandardCost as [Corp Standard Cost]
	  ,case when [Action_Status] <> 1 then 'UNPROCESSED' ELSE 'PROCESSED' end as [CORP COSTS Record_Status]
  FROM [import].[IMPORT_MASS_MAINTENANCE_PROMO] where intCreateRecordType = 3) Type_3 on immp.ProductValue = Type_3.ProductValue and immp.Price_Sheet_id = type_3.Price_Sheet_id

LEFT JOIN	  (
SELECT [ProductValue] 
	,[Price_Sheet_id]
	,StartDate as [End User Price Effective Date]
	,EndDate as [End User Price End Date]
	,End_User_Each_Price as [End User Each Price]
	,case when [Action_Status] <> 1 then 'UNPROCESSED' ELSE 'PROCESSED' end as [END USER COSTS Record_Status]
  FROM [import].[IMPORT_MASS_MAINTENANCE_PROMO] where intCreateRecordType = 6) Type_6 on immp.ProductValue = Type_6.ProductValue and immp.Price_Sheet_id = type_6.Price_Sheet_id



LEFT JOIN	  (
  select productvalue 
	,[Price_Sheet_id]
  , StartDate as [Store DC Costs Effective Date]
  , EndDate as [Store DC Costs End Date]
  , Base_Price as [Base Price]
  , StartDateCorp as [Corp Costs Effective Date]
  , EndDateCorp as [Corp Costs End Date]
  , Corp_Price as [Corp Price]
  	,case when [Action_Status] <> 1 then 'UNPROCESSED' ELSE 'PROCESSED' end as [PD Record_Status]
  FROM [import].[IMPORT_MASS_MAINTENANCE_PROMO] 
  where intCreateRecordType = 4) Type_4 on immp.ProductValue = Type_4.ProductValue and immp.Price_Sheet_id = type_4.Price_Sheet_id 

  

  --100100500 = Promo DC From Vednor
  --100100520 = Promo to End User
  --PD record/rules = Promo to Store Base_price = for warehouse and corp_price is corp warehouse

  --create unique clustered index idx_PS_Prod on custom.v_PromoPricing ([Price Sheet #], ProductValue)

