﻿










/*
*/
CREATE VIEW [custom].[V_FuturePrices_View]
WITH SCHEMABINDING
AS
select distinct pa.PRODUCT_STRUCTURE_FK, pa.ORG_ENTITY_STRUCTURE_FK, pid.value as [Product_ID],  eid.value as [Warehouse] ,List_effDate,List_EndDate,[List Each], 
[List Lot Price 1], [List Lot Price 2],RegCost_EffDate, RegCost_EndDate, [Reg Repl Cost],[Reg Std Cost] 
, CEU_EffDate, CEU_EndDate, [CEU Ea Prc], [CEU 1 Prc],[CEU 2 Prc] 
, REU_EffDate, REU_EndDate, [Lot A Prc], [Lot B Prc],[REU Ea Prc],[REU 1 Prc],[REU 2 Prc], 
[RegBase_EffDate], RegBase_EndDate, [Regular Base Price],
 [PrEU_EffDate], [PrEU_EndDate], [Promo End User Each], 
 Curr_EffDate, Curr_EndDate, [Current Repl Cost],[Current Std Cost]
,Vend_EffDate, Vend_EndDate, [Vendor Corp Cost],[Vendor Store Cost],[Vendor List Cost],[Suggested Contractor Price]
,Promo_EffDate, Promo_EndDate, [Promo Repl Cost],[Promo Std Cost], srvf.DATA_NAME_FK
from epacube.PRODUCT_ASSOCIATION pa 

left outer join (

select Product_structure_fk, Org_entity_structure_fk, List_effDate, List_EndDate, [List Lot Price 1], [List Lot Price 2],[List Each] FROM
(select PRODUCT_STRUCTURE_FK, cast(effective_date as date) as List_EffDate, cast(end_date as date) as List_EndDate, Org_entity_structure_fk, 'List Lot Price 1' as DataName, cast(NET_VALUE3 as varchar(1000) ) as Datavalue from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100404 --DN100100408
	UNION select PRODUCT_STRUCTURE_FK, cast(effective_date as date), cast(end_date as date), Org_entity_structure_fk, 'List Each', cast(NET_VALUE4 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100404 --DN231104
	UNION select PRODUCT_STRUCTURE_FK, cast(effective_date as date), cast(end_date as date), Org_entity_structure_fk, 'List Lot Price 2', cast(NET_VALUE5 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100404 --DN100100408
	) As P
	Pivot (Max(P.DataValue) for DataName in ([List Lot Price 1], [List Lot Price 2],[List Each])) As ListTable ) 
lt on lt.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and lt.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK

left outer join(

SELECT PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, RegCost_EffDate, RegCost_EndDate, [Reg Repl Cost],[Reg Std Cost] FROM 
	(select PRODUCT_STRUCTURE_FK, cast(effective_date as date) as RegCost_EffDate, cast(end_date as date) as RegCost_EndDate, Org_entity_structure_fk, 'Reg Repl Cost' as DataName, cast(NET_VALUE1 as varchar(1000) ) as Datavalue from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 231100 --DN231101
	UNION select PRODUCT_STRUCTURE_FK, cast(effective_date as date), cast(end_date as date), Org_entity_structure_fk, 'Reg Std Cost', cast(NET_VALUE2 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 231100 --DN231102
	) As P
	Pivot (Max(P.DataValue) for DataName in ([Reg Repl Cost],[Reg Std Cost])) As SX) 
sxCost on sxCost.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and sxCost.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK

left outer join(

SELECT PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, CEU_EffDate, CEU_EndDate, [CEU Ea Prc], [CEU 1 Prc],[CEU 2 Prc] FROM 
	(select PRODUCT_STRUCTURE_FK, cast(effective_date as date) as CEU_EffDate,  cast(end_date as date) as CEU_EndDate, Org_entity_structure_fk, 'CEU Ea Prc' as DataName, cast(NET_VALUE1 as varchar(1000) ) as Datavalue from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100392 --DN100100393
	UNION select PRODUCT_STRUCTURE_FK, cast(effective_date as date), cast(end_date as date), Org_entity_structure_fk, 'CEU 1 Prc', cast(NET_VALUE2 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100392 --DN100100394
	UNION select PRODUCT_STRUCTURE_FK, cast(effective_date as date), cast(end_date as date), Org_entity_structure_fk, 'CEU 2 Prc', cast(NET_VALUE3 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100392 --DN100100395
	) As P
	Pivot (Max(P.DataValue) for DataName in ([CEU Ea Prc], [CEU 1 Prc],[CEU 2 Prc])) As CEU)
CurEndUse on CurEndUse.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and CurEndUse.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK

left outer join(

SELECT PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, REU_EffDate, REU_EndDate, [Lot A Prc], [Lot B Prc],[REU Ea Prc],[REU 1 Prc],[REU 2 Prc] FROM 
	(select PRODUCT_STRUCTURE_FK, cast(effective_date as date) as REU_EffDate, cast(end_date as date) as REU_EndDate, Org_entity_structure_fk, 'Lot A Prc' as DataName, cast(NET_VALUE5 as varchar(1000) ) as Datavalue from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100398 --DN100100405
	UNION select PRODUCT_STRUCTURE_FK, cast(effective_date as date), cast(end_date as date), Org_entity_structure_fk, 'Lot B Prc', cast(NET_VALUE6 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100398 --DN100100406
	UNION select PRODUCT_STRUCTURE_FK, cast(effective_date as date), cast(end_date as date), Org_entity_structure_fk, 'REU Ea Prc', cast(NET_VALUE1 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100398 --DN100100399
	UNION select PRODUCT_STRUCTURE_FK, cast(effective_date as date), cast(end_date as date), Org_entity_structure_fk, 'REU 1 Prc', cast(NET_VALUE2 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100398 --DN100100400
	UNION select PRODUCT_STRUCTURE_FK, cast(effective_date as date), cast(end_date as date), Org_entity_structure_fk, 'REU 2 Prc', cast(NET_VALUE3 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100398 --DN100100401
	) As P
	Pivot (Max(P.DataValue) for DataName in ([Lot A Prc], [Lot B Prc],[REU Ea Prc],[REU 1 Prc],[REU 2 Prc])) As REU)
RegEndUse on RegEndUse.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and RegEndUse.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK

left outer join (select PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, cast(effective_date as date) as RegBase_EffDate,cast(end_date as date) as RegBase_EndDate, cast(NET_VALUE3 as varchar(1000)) as 'Regular Base Price' from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100416) 
rbp on rbp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and rbp.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK

left outer join (select PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, cast(effective_date as date) as PrEU_EffDate, cast(end_date as date) as PrEU_EndDate, cast(NET_VALUE1 as varchar(1000)) as 'Promo End User Each' from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100520)
peu on peu.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and peu.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK

left outer join (
SELECT PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, Vend_EffDate, Vend_EndDate, [Vendor Corp Cost],[Vendor Store Cost],[Vendor List Cost],[Suggested Contractor Price] FROM
(select PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, cast(effective_date as date) as Vend_EffDate, cast(end_date as date) as Vend_EndDate, 'Vendor Corp Cost' as DataName, cast(NET_VALUE1 as varchar(1000) ) as Datavalue from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100418 --DN100100419
	UNION select PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, cast(effective_date as date), cast(end_date as date), 'Vendor Store Cost', cast(NET_VALUE2 as varchar(1000)) from  marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100418 --DN100100420
	UNION select PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, cast(effective_date as date), cast(end_date as date), 'Vendor List Cost', cast(NET_VALUE3 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100418 --DN100100421
	UNION select PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, cast(effective_date as date), cast(end_date as date), 'Suggested Contractor Price', cast(NET_VALUE4 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100418 --DN100100355
	) As P
	Pivot (Max(P.DataValue) for DataName in ([Vendor Corp Cost],[Vendor Store Cost],[Vendor List Cost],[Suggested Contractor Price])) As VendCost)
vc on vc.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and vc.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK

left outer join (

SELECT PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, Promo_EffDate, Promo_EndDate, [Promo Repl Cost],[Promo Std Cost] FROM 
	(select PRODUCT_STRUCTURE_FK, cast(effective_date as date) as Promo_EffDate,  cast(End_date as date) as Promo_EndDate, Org_entity_structure_fk, 'Promo Repl Cost' as DataName, cast(NET_VALUE1 as varchar(1000) ) as Datavalue from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100500 --DN100100501
	UNION select PRODUCT_STRUCTURE_FK, cast(effective_date as date), cast(end_date as date), Org_entity_structure_fk, 'Promo Std Cost', cast(NET_VALUE2 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100500 --DN100100502 -add end date
	) As P
	Pivot (Max(P.DataValue) for DataName in ([Promo Repl Cost],[Promo Std Cost])) As Promo)
p on p.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and p.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK

left outer join (

SELECT PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, Curr_EffDate, Curr_EndDate, [Current Repl Cost],[Current Std Cost] FROM 
	(select PRODUCT_STRUCTURE_FK, cast(effective_date as date) as Curr_EffDate, cast(end_date as date) as Curr_EndDate,  Org_entity_structure_fk, 'Current Repl Cost' as DataName, cast(NET_VALUE1 as varchar(1000) ) as Datavalue from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100530 --DN100100531
	UNION select PRODUCT_STRUCTURE_FK, cast(effective_date as date), cast(end_date as date), Org_entity_structure_fk, 'Current Std Cost', cast(NET_VALUE2 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100530 --DN100100532
	) As P
	Pivot (Max(P.DataValue) for DataName in ([Current Repl Cost],[Current Std Cost])) As Curr)
c on c.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and c.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK

inner join epacube.entity_identification eid on pa.ORG_ENTITY_STRUCTURE_FK = eid.ENTITY_STRUCTURE_FK
inner join epacube.product_identification pid on pa.PRODUCT_STRUCTURE_FK = pid.PRODUCT_STRUCTURE_FK and pid.DATA_NAME_FK = 110100
inner join marginmgr.SHEET_RESULTS_VALUES_FUTURE srvf on pa.PRODUCT_STRUCTURE_FK = srvf.PRODUCT_STRUCTURE_FK and pa.ORG_ENTITY_STRUCTURE_FK = srvf.ORG_ENTITY_STRUCTURE_FK
where pa.DATA_NAME_FK = 159100







