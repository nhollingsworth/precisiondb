﻿
CREATE VIEW [custom].[V_WAREHOSUE_PROD]
AS
Select 1 'Company #', pi.value 'Product', ei.value 'WHSE Code', pi_vp.value 'Vendor Productuct', ei_v.value 'Vendor #', isnull(uc_211601_1.uom_code, uc_211601.uom_code) 'Corp Corp Standard Pack', isnull(uc_211602_1.uom_code, uc_211602.uom_code) 'Buying Unit',  sr_231100.NET_VALUE1 'Regular Replacement Cost',  sr_231100.NET_VALUE2 'Regular Standard Cost',  sr_100100392.NET_VALUE1 'Current End User Each Price',  sr_100100392.NET_VALUE2 'Current End User 1 Price',  sr_100100392.NET_VALUE3 'Current End User 2 Price',  sr_100100398.NET_VALUE5 'Lot Price A',  sr_100100398.NET_VALUE6 'Lot Price B',  sr_100100398.NET_VALUE1 'Regular End User Each Price',  sr_100100398.NET_VALUE2 'Regular End User 1 Price',  sr_100100398.NET_VALUE3 'Regular End User 2 Price',  sr_100100404.NET_VALUE4 'List Each',  sr_100100404.NET_VALUE3 'List Lot Price 1',  sr_100100404.NET_VALUE5 'List Lot Price 2',  sr_100100416.NET_VALUE3 'Regular Base Price',  sr_100100500.NET_VALUE1 'Promo Repl Cost',  sr_100100500.NET_VALUE2 'Promo Std Cost',  sr_100100520.NET_VALUE1 'Promo End User Each',  sr_100100530.NET_VALUE1 'Current Replacement Cost',  sr_100100530.NET_VALUE2 'Current Standard Cost',  pv_100100418.NET_VALUE4 'Suggested Contractor Price',  pv_100100418.NET_VALUE1 'Vendor Corp Cost',  pv_100100418.NET_VALUE3 'Vendor List Cost',  pv_100100418.NET_VALUE2 'Vendor Store Cost', 
dv_211825.value 'WHSE Status', pas_100100229.ATTRIBUTE_CHAR 'Flyer Month', pas_100100237.ATTRIBUTE_CHAR 'Last Flyer', pas_100100287.ATTRIBUTE_CHAR 'Promo Page #', pas_100100373.ATTRIBUTE_NUMBER 'Case Qty', pas_100100377.ATTRIBUTE_DATE 'Line Discount Begin Date', pas_100100378.ATTRIBUTE_DATE 'Line Discount End Date', pas_100100379.ATTRIBUTE_NUMBER 'Line Discount Percent', pas_100100380.ATTRIBUTE_NUMBER 'Line Discount Qty', pas_100100396.ATTRIBUTE_NUMBER 'Current End User Qty 1', pas_100100397.ATTRIBUTE_NUMBER 'Current End User Qty 2', pas_100100402.ATTRIBUTE_NUMBER 'Regular End User 1 Qty', pas_100100403.ATTRIBUTE_NUMBER 'Regular End User 2 Qty', pas_100100410.ATTRIBUTE_NUMBER 'Lot Qty A', pas_100100411.ATTRIBUTE_NUMBER 'Lot Qty B', pas_100100412.ATTRIBUTE_NUMBER 'List Lot Qty 1', pas_100100413.ATTRIBUTE_NUMBER 'List Lot Qty 2', 
dv_211901.value 'Price Type', dv_100100423.value 'RP Family Group', dv_100100424.value 'ST Family Group', dv_100100425.value 'BP Family Group', dv_100100426.value 'EU Family Group', dv_100100427.value 'EU1 Family Group', dv_100100428.value 'EU2 Family Group'
from epacube.product_identification pi with (nolock) 
left join epacube.product_identification pi_vp with (nolock) on pi.product_structure_fk = pi_vp.product_structure_fk and pi_vp.data_name_fk = 110111
inner join epacube.product_association pa with (nolock) on pi.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and pa.DATA_NAME_FK = 159100 
inner join epacube.entity_identification ei with (nolock) on pa.org_entity_structure_fk = ei.entity_structure_fk and ei.data_name_fk = 141111 and entity_data_name_fk = 141000
inner join epacube.product_association pa_v with (nolock) on pi.product_structure_fk = pa_v.product_structure_fk and pa.org_entity_structure_fk = pa_v.org_entity_structure_fk and pa_v.data_name_fk = 253501
inner join epacube.entity_identification ei_v with (nolock) on pa_v.entity_structure_fk = ei_v.entity_structure_fk and ei_v.entity_data_name_fk = 143000 and ei_v.DATA_NAME_FK = 143110
left join epacube.product_uom_class puc_211601 with (nolock) on pi.product_structure_fk = puc_211601.product_structure_fk and pa.org_entity_structure_fk = puc_211601.org_entity_structure_fk and puc_211601.data_name_fk = 211601
left join epacube.uom_code uc_211601 on puc_211601.uom_code_fk = uc_211601.uom_code_id
left join epacube.product_uom_class puc_211601_1 with (nolock) on pi.product_structure_fk = puc_211601_1.product_structure_fk and puc_211601_1.org_entity_structure_fk = pa.org_entity_structure_fk and puc_211601_1.data_name_fk = 211601
left join epacube.uom_code uc_211601_1 on puc_211601_1.uom_code_fk = uc_211601_1.uom_code_id
left join epacube.product_uom_class puc_211602 with (nolock) on pi.product_structure_fk = puc_211602.product_structure_fk and pa.org_entity_structure_fk = puc_211602.org_entity_structure_fk and puc_211602.data_name_fk = 211602
left join epacube.uom_code uc_211602 on puc_211602.uom_code_fk = uc_211602.uom_code_id
left join epacube.product_uom_class puc_211602_1 with (nolock) on pi.product_structure_fk = puc_211602_1.product_structure_fk and puc_211602_1.org_entity_structure_fk = pa.org_entity_structure_fk and puc_211602_1.data_name_fk = 211602
left join epacube.uom_code uc_211602_1 on puc_211602_1.uom_code_fk = uc_211602_1.uom_code_id
Left Join MarginMgr.Sheet_Results SR_231100 with (nolock) on pa.product_structure_fk = SR_231100.product_structure_fk and pa.org_entity_structure_fk = SR_231100.org_entity_structure_fk and SR_231100.data_name_fk = 231100Left Join MarginMgr.Sheet_Results SR_100100392 with (nolock) on pa.product_structure_fk = SR_100100392.product_structure_fk and pa.org_entity_structure_fk = SR_100100392.org_entity_structure_fk and SR_100100392.data_name_fk = 100100392Left Join MarginMgr.Sheet_Results SR_100100398 with (nolock) on pa.product_structure_fk = SR_100100398.product_structure_fk and pa.org_entity_structure_fk = SR_100100398.org_entity_structure_fk and SR_100100398.data_name_fk = 100100398Left Join MarginMgr.Sheet_Results SR_100100404 with (nolock) on pa.product_structure_fk = SR_100100404.product_structure_fk and pa.org_entity_structure_fk = SR_100100404.org_entity_structure_fk and SR_100100404.data_name_fk = 100100404Left Join MarginMgr.Sheet_Results SR_100100416 with (nolock) on pa.product_structure_fk = SR_100100416.product_structure_fk and pa.org_entity_structure_fk = SR_100100416.org_entity_structure_fk and SR_100100416.data_name_fk = 100100416Left Join MarginMgr.Sheet_Results SR_100100500 with (nolock) on pa.product_structure_fk = SR_100100500.product_structure_fk and pa.org_entity_structure_fk = SR_100100500.org_entity_structure_fk and SR_100100500.data_name_fk = 100100500Left Join MarginMgr.Sheet_Results SR_100100520 with (nolock) on pa.product_structure_fk = SR_100100520.product_structure_fk and pa.org_entity_structure_fk = SR_100100520.org_entity_structure_fk and SR_100100520.data_name_fk = 100100520Left Join MarginMgr.Sheet_Results SR_100100530 with (nolock) on pa.product_structure_fk = SR_100100530.product_structure_fk and pa.org_entity_structure_fk = SR_100100530.org_entity_structure_fk and SR_100100530.data_name_fk = 100100530Left Join epaCUBE.Product_Values PV_100100418 with (nolock) on pa.product_structure_fk = PV_100100418.product_structure_fk and pa.org_entity_structure_fk = PV_100100418.org_entity_structure_fk and PV_100100418.data_name_fk = 100100418
left join epacube.product_category pc_211901 with (nolock) on pa.product_structure_fk = pc_211901.product_structure_fk and pa.org_entity_structure_fk = pc_211901.org_entity_structure_fk and pc_211901.data_name_fk = 211901left join epacube.data_value dv_211901 with (nolock) on pc_211901.data_value_fk = dv_211901.data_value_idleft join epacube.product_category pc_100100423 with (nolock) on pa.product_structure_fk = pc_100100423.product_structure_fk and pa.org_entity_structure_fk = pc_100100423.org_entity_structure_fk and pc_100100423.data_name_fk = 100100423left join epacube.data_value dv_100100423 with (nolock) on pc_100100423.data_value_fk = dv_100100423.data_value_idleft join epacube.product_category pc_100100424 with (nolock) on pa.product_structure_fk = pc_100100424.product_structure_fk and pa.org_entity_structure_fk = pc_100100424.org_entity_structure_fk and pc_100100424.data_name_fk = 100100424left join epacube.data_value dv_100100424 with (nolock) on pc_100100424.data_value_fk = dv_100100424.data_value_idleft join epacube.product_category pc_100100425 with (nolock) on pa.product_structure_fk = pc_100100425.product_structure_fk and pa.org_entity_structure_fk = pc_100100425.org_entity_structure_fk and pc_100100425.data_name_fk = 100100425left join epacube.data_value dv_100100425 with (nolock) on pc_100100425.data_value_fk = dv_100100425.data_value_idleft join epacube.product_category pc_100100426 with (nolock) on pa.product_structure_fk = pc_100100426.product_structure_fk and pa.org_entity_structure_fk = pc_100100426.org_entity_structure_fk and pc_100100426.data_name_fk = 100100426left join epacube.data_value dv_100100426 with (nolock) on pc_100100426.data_value_fk = dv_100100426.data_value_idleft join epacube.product_category pc_100100427 with (nolock) on pa.product_structure_fk = pc_100100427.product_structure_fk and pa.org_entity_structure_fk = pc_100100427.org_entity_structure_fk and pc_100100427.data_name_fk = 100100427left join epacube.data_value dv_100100427 with (nolock) on pc_100100427.data_value_fk = dv_100100427.data_value_idleft join epacube.product_category pc_100100428 with (nolock) on pa.product_structure_fk = pc_100100428.product_structure_fk and pa.org_entity_structure_fk = pc_100100428.org_entity_structure_fk and pc_100100428.data_name_fk = 100100428left join epacube.data_value dv_100100428 with (nolock) on pc_100100428.data_value_fk = dv_100100428.data_value_id
left join epacube.product_Attribute pas_211825 with (nolock) on pa.product_structure_fk = pas_211825.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_211825.org_entity_structure_fk, 1)  and pas_211825.data_name_fk = 211825left join epacube.data_value dv_211825 with (nolock) on pas_211825.data_value_fk = dv_211825.data_value_idleft join epacube.product_Attribute pas_100100229 with (nolock) on pa.product_structure_fk = pas_100100229.product_structure_fk  and pas_100100229.org_entity_structure_fk = 1  and pas_100100229.data_name_fk = 100100229left join epacube.data_value dv_100100229 with (nolock) on pas_100100229.data_value_fk = dv_100100229.data_value_idleft join epacube.product_Attribute pas_100100237 with (nolock) on pa.product_structure_fk = pas_100100237.product_structure_fk  and pas_100100237.org_entity_structure_fk = 1  and pas_100100237.data_name_fk = 100100237left join epacube.data_value dv_100100237 with (nolock) on pas_100100237.data_value_fk = dv_100100237.data_value_idleft join epacube.product_Attribute pas_100100287 with (nolock) on pa.product_structure_fk = pas_100100287.product_structure_fk  and pas_100100287.org_entity_structure_fk = 1  and pas_100100287.data_name_fk = 100100287left join epacube.data_value dv_100100287 with (nolock) on pas_100100287.data_value_fk = dv_100100287.data_value_idleft join epacube.product_Attribute pas_100100373 with (nolock) on pa.product_structure_fk = pas_100100373.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100373.org_entity_structure_fk, 1)  and pas_100100373.data_name_fk = 100100373left join epacube.data_value dv_100100373 with (nolock) on pas_100100373.data_value_fk = dv_100100373.data_value_idleft join epacube.product_Attribute pas_100100377 with (nolock) on pa.product_structure_fk = pas_100100377.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100377.org_entity_structure_fk, 1)  and pas_100100377.data_name_fk = 100100377left join epacube.data_value dv_100100377 with (nolock) on pas_100100377.data_value_fk = dv_100100377.data_value_idleft join epacube.product_Attribute pas_100100378 with (nolock) on pa.product_structure_fk = pas_100100378.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100378.org_entity_structure_fk, 1)  and pas_100100378.data_name_fk = 100100378left join epacube.data_value dv_100100378 with (nolock) on pas_100100378.data_value_fk = dv_100100378.data_value_idleft join epacube.product_Attribute pas_100100379 with (nolock) on pa.product_structure_fk = pas_100100379.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100379.org_entity_structure_fk, 1)  and pas_100100379.data_name_fk = 100100379left join epacube.data_value dv_100100379 with (nolock) on pas_100100379.data_value_fk = dv_100100379.data_value_idleft join epacube.product_Attribute pas_100100380 with (nolock) on pa.product_structure_fk = pas_100100380.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100380.org_entity_structure_fk, 1)  and pas_100100380.data_name_fk = 100100380left join epacube.data_value dv_100100380 with (nolock) on pas_100100380.data_value_fk = dv_100100380.data_value_idleft join epacube.product_Attribute pas_100100396 with (nolock) on pa.product_structure_fk = pas_100100396.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100396.org_entity_structure_fk, 1)  and pas_100100396.data_name_fk = 100100396left join epacube.data_value dv_100100396 with (nolock) on pas_100100396.data_value_fk = dv_100100396.data_value_idleft join epacube.product_Attribute pas_100100397 with (nolock) on pa.product_structure_fk = pas_100100397.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100397.org_entity_structure_fk, 1)  and pas_100100397.data_name_fk = 100100397left join epacube.data_value dv_100100397 with (nolock) on pas_100100397.data_value_fk = dv_100100397.data_value_idleft join epacube.product_Attribute pas_100100402 with (nolock) on pa.product_structure_fk = pas_100100402.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100402.org_entity_structure_fk, 1)  and pas_100100402.data_name_fk = 100100402left join epacube.data_value dv_100100402 with (nolock) on pas_100100402.data_value_fk = dv_100100402.data_value_idleft join epacube.product_Attribute pas_100100403 with (nolock) on pa.product_structure_fk = pas_100100403.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100403.org_entity_structure_fk, 1)  and pas_100100403.data_name_fk = 100100403left join epacube.data_value dv_100100403 with (nolock) on pas_100100403.data_value_fk = dv_100100403.data_value_idleft join epacube.product_Attribute pas_100100410 with (nolock) on pa.product_structure_fk = pas_100100410.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100410.org_entity_structure_fk, 1)  and pas_100100410.data_name_fk = 100100410left join epacube.data_value dv_100100410 with (nolock) on pas_100100410.data_value_fk = dv_100100410.data_value_idleft join epacube.product_Attribute pas_100100411 with (nolock) on pa.product_structure_fk = pas_100100411.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100411.org_entity_structure_fk, 1)  and pas_100100411.data_name_fk = 100100411left join epacube.data_value dv_100100411 with (nolock) on pas_100100411.data_value_fk = dv_100100411.data_value_idleft join epacube.product_Attribute pas_100100412 with (nolock) on pa.product_structure_fk = pas_100100412.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100412.org_entity_structure_fk, 1)  and pas_100100412.data_name_fk = 100100412left join epacube.data_value dv_100100412 with (nolock) on pas_100100412.data_value_fk = dv_100100412.data_value_idleft join epacube.product_Attribute pas_100100413 with (nolock) on pa.product_structure_fk = pas_100100413.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100413.org_entity_structure_fk, 1)  and pas_100100413.data_name_fk = 100100413left join epacube.data_value dv_100100413 with (nolock) on pas_100100413.data_value_fk = dv_100100413.data_value_id
Where pi.data_name_fk = 110100
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 6, @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_WAREHOSUE_PROD';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane6', @value = N'         End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100410"
            Begin Extent = 
               Top = 978
               Left = 786
               Bottom = 1086
               Right = 1012
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100410"
            Begin Extent = 
               Top = 978
               Left = 1050
               Bottom = 1086
               Right = 1254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100411"
            Begin Extent = 
               Top = 978
               Left = 1292
               Bottom = 1086
               Right = 1518
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100411"
            Begin Extent = 
               Top = 978
               Left = 1556
               Bottom = 1086
               Right = 1760
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100412"
            Begin Extent = 
               Top = 1086
               Left = 38
               Bottom = 1194
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100412"
            Begin Extent = 
               Top = 1086
               Left = 302
               Bottom = 1194
               Right = 506
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100413"
            Begin Extent = 
               Top = 1086
               Left = 544
               Bottom = 1194
               Right = 770
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100413"
            Begin Extent = 
               Top = 1086
               Left = 808
               Bottom = 1194
               Right = 1012
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_WAREHOSUE_PROD';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane5', @value = N' DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100378"
            Begin Extent = 
               Top = 762
               Left = 786
               Bottom = 870
               Right = 1012
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100378"
            Begin Extent = 
               Top = 762
               Left = 1050
               Bottom = 870
               Right = 1254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100379"
            Begin Extent = 
               Top = 762
               Left = 1292
               Bottom = 870
               Right = 1518
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100379"
            Begin Extent = 
               Top = 762
               Left = 1556
               Bottom = 870
               Right = 1760
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100380"
            Begin Extent = 
               Top = 870
               Left = 38
               Bottom = 978
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100380"
            Begin Extent = 
               Top = 870
               Left = 302
               Bottom = 978
               Right = 506
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100396"
            Begin Extent = 
               Top = 870
               Left = 544
               Bottom = 978
               Right = 770
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100396"
            Begin Extent = 
               Top = 870
               Left = 808
               Bottom = 978
               Right = 1012
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100397"
            Begin Extent = 
               Top = 870
               Left = 1050
               Bottom = 978
               Right = 1276
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100397"
            Begin Extent = 
               Top = 870
               Left = 1314
               Bottom = 978
               Right = 1518
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100402"
            Begin Extent = 
               Top = 870
               Left = 1556
               Bottom = 978
               Right = 1782
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100402"
            Begin Extent = 
               Top = 978
               Left = 38
               Bottom = 1086
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100403"
            Begin Extent = 
               Top = 978
               Left = 280
               Bottom = 1086
               Right = 506
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100403"
            Begin Extent = 
               Top = 978
               Left = 544
               Bottom = 1086
               Right = 748
   ', @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_WAREHOSUE_PROD';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane4', @value = N' 280
            TopColumn = 0
         End
         Begin Table = "pc_100100428"
            Begin Extent = 
               Top = 546
               Left = 786
               Bottom = 654
               Right = 1012
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100428"
            Begin Extent = 
               Top = 546
               Left = 1050
               Bottom = 654
               Right = 1254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_211825"
            Begin Extent = 
               Top = 546
               Left = 1292
               Bottom = 654
               Right = 1518
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_211825"
            Begin Extent = 
               Top = 546
               Left = 1556
               Bottom = 654
               Right = 1760
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100229"
            Begin Extent = 
               Top = 654
               Left = 38
               Bottom = 762
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100229"
            Begin Extent = 
               Top = 654
               Left = 302
               Bottom = 762
               Right = 506
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100237"
            Begin Extent = 
               Top = 654
               Left = 544
               Bottom = 762
               Right = 770
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100237"
            Begin Extent = 
               Top = 654
               Left = 808
               Bottom = 762
               Right = 1012
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100287"
            Begin Extent = 
               Top = 654
               Left = 1050
               Bottom = 762
               Right = 1276
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100287"
            Begin Extent = 
               Top = 654
               Left = 1314
               Bottom = 762
               Right = 1518
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100373"
            Begin Extent = 
               Top = 654
               Left = 1556
               Bottom = 762
               Right = 1782
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100373"
            Begin Extent = 
               Top = 762
               Left = 38
               Bottom = 870
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pas_100100377"
            Begin Extent = 
               Top = 762
               Left = 280
               Bottom = 870
               Right = 506
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100377"
            Begin Extent = 
               Top = 762
               Left = 544
               Bottom = 870
               Right = 748
            End
           ', @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_WAREHOSUE_PROD';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane3', @value = N'           TopColumn = 0
         End
         Begin Table = "SR_100100530"
            Begin Extent = 
               Top = 330
               Left = 600
               Bottom = 438
               Right = 843
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PV_100100418"
            Begin Extent = 
               Top = 330
               Left = 881
               Bottom = 438
               Right = 1124
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pc_211901"
            Begin Extent = 
               Top = 330
               Left = 1162
               Bottom = 438
               Right = 1388
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_211901"
            Begin Extent = 
               Top = 330
               Left = 1426
               Bottom = 438
               Right = 1630
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pc_100100423"
            Begin Extent = 
               Top = 438
               Left = 38
               Bottom = 546
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100423"
            Begin Extent = 
               Top = 438
               Left = 302
               Bottom = 546
               Right = 506
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pc_100100424"
            Begin Extent = 
               Top = 438
               Left = 544
               Bottom = 546
               Right = 770
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100424"
            Begin Extent = 
               Top = 438
               Left = 808
               Bottom = 546
               Right = 1012
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pc_100100425"
            Begin Extent = 
               Top = 438
               Left = 1050
               Bottom = 546
               Right = 1276
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100425"
            Begin Extent = 
               Top = 438
               Left = 1314
               Bottom = 546
               Right = 1518
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pc_100100426"
            Begin Extent = 
               Top = 438
               Left = 1556
               Bottom = 546
               Right = 1782
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100426"
            Begin Extent = 
               Top = 546
               Left = 38
               Bottom = 654
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pc_100100427"
            Begin Extent = 
               Top = 546
               Left = 280
               Bottom = 654
               Right = 506
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dv_100100427"
            Begin Extent = 
               Top = 546
               Left = 544
               Bottom = 654
               Right = 748
            End
            DisplayFlags =', @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_WAREHOSUE_PROD';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'           TopColumn = 0
         End
         Begin Table = "uc_211601"
            Begin Extent = 
               Top = 114
               Left = 302
               Bottom = 222
               Right = 506
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "puc_211601_1"
            Begin Extent = 
               Top = 114
               Left = 544
               Bottom = 222
               Right = 770
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "uc_211601_1"
            Begin Extent = 
               Top = 114
               Left = 808
               Bottom = 222
               Right = 1012
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "puc_211602"
            Begin Extent = 
               Top = 114
               Left = 1050
               Bottom = 222
               Right = 1276
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "uc_211602"
            Begin Extent = 
               Top = 114
               Left = 1314
               Bottom = 222
               Right = 1518
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "puc_211602_1"
            Begin Extent = 
               Top = 114
               Left = 1556
               Bottom = 222
               Right = 1782
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "uc_211602_1"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SR_231100"
            Begin Extent = 
               Top = 222
               Left = 280
               Bottom = 330
               Right = 523
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SR_100100392"
            Begin Extent = 
               Top = 222
               Left = 561
               Bottom = 330
               Right = 804
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SR_100100398"
            Begin Extent = 
               Top = 222
               Left = 842
               Bottom = 330
               Right = 1085
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SR_100100404"
            Begin Extent = 
               Top = 222
               Left = 1123
               Bottom = 330
               Right = 1366
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SR_100100416"
            Begin Extent = 
               Top = 222
               Left = 1404
               Bottom = 330
               Right = 1647
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SR_100100500"
            Begin Extent = 
               Top = 330
               Left = 38
               Bottom = 438
               Right = 281
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SR_100100520"
            Begin Extent = 
               Top = 330
               Left = 319
               Bottom = 438
               Right = 562
            End
            DisplayFlags = 280
 ', @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_WAREHOSUE_PROD';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "pi"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 272
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pi_vp"
            Begin Extent = 
               Top = 6
               Left = 310
               Bottom = 114
               Right = 544
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pa"
            Begin Extent = 
               Top = 6
               Left = 582
               Bottom = 114
               Right = 829
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ei"
            Begin Extent = 
               Top = 6
               Left = 867
               Bottom = 114
               Right = 1088
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pa_v"
            Begin Extent = 
               Top = 6
               Left = 1126
               Bottom = 114
               Right = 1373
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ei_v"
            Begin Extent = 
               Top = 6
               Left = 1411
               Bottom = 114
               Right = 1632
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "puc_211601"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 264
            End
            DisplayFlags = 280
 ', @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_WAREHOSUE_PROD';

