﻿




/*
*/
CREATE VIEW [custom].[V_Transmission]
AS
SELECT [JS_STRAN_ID]
      ,[Product_Structure_FK]
      ,[job_fk]
      ,[ORG_ENTITY_STRUCTURE_FK]
      ,[cust_entity_structure_fk]
      ,[Store]
      ,[DC]
      ,[Store Transmit Flag]
      ,[RESULT]
      ,[Product Transmit Flag]
      ,[Promo Page #]
      ,[Product Number]
      ,[Changes Flag]
      ,[Lot Pricer Flag]
      ,[Regular Base from DC]
      ,[AOC Vendor]
      ,[Selling Price Each]
      ,[Promo EU Ea]
      ,[Lot 1 Qty]
      ,[Lot 1 Sell Price]
      ,[Lot 2 Qty]
      ,[Lot 2 Sell Price]
      ,[Product Description]
      ,[Standard Pack]
      ,[Catalog Page Number]
      ,[Lot A QTY]
      ,[Lot A Sell Price]
      ,[Lot B QTY]
      ,[Lot B Sell Price]
      ,[CORP Base Price]
      ,[CORP DropShipQty]
      ,[Weight]
      ,[Season Code]
      ,[Customer Description]
      ,[Dept-Cat]
      ,[SSP Indicator]
      ,[MSDS Indicator]
      ,[Memo Field]
      ,[Hazardous Material Ship Code]
      ,[Discontinued Flag]
      ,[Flyer Month Code]
      ,[Buyers Code]
      ,[Freight Code]
      ,[Vendor Bar Code 1]
      ,[Reg Base from DC Eff Date]
      ,[Direct Cost Effective Date]
      ,[End User Each Published Flag]
      ,[End User Lot 1 Published Flag]
      ,[End User Lot 2 Published Flag]
      ,[Manufacturer Part Number]
      ,[Freight Code #2]
      ,[Each Sell Price Spcl Flag]
      ,[Lot 1 Sell Price Spcl Flag]
      ,[Lot 2 Sell Price Spcl Flag]
      ,[Append Date]
      ,[QtyBrk#2]
      ,[QtyBrk$1]
      ,[QtyBrk$2]
      ,[Serialized Product Flag]
      ,[Certification Required Flag]
      ,[Taxable Flag]
      ,[End User Regular Price]
      ,[List Each]
      ,[List Lot 1]
      ,[List Lot 2]
      ,[WEB_NODE_ID]
      ,[Web Node Data]
      ,[EDI Part No]
      ,[Vendor No]
      ,[Country of Origin]
      ,[Return Flag]
      ,[Regular DC Cost]
      ,[DC Sell Qty]
      ,[IsPromoFlag]
      ,[PD_Effective_Date]
      ,[sProdTransFlag]
      ,[calc_Job_FK]
      ,[Rules_FK]
      ,[Basis Calc]
      ,[Host Precedence Level]
      ,[Corp SSP]
	  ,[Store SSP]
  FROM [epacube].[custom].[JS_sTran_History]



