﻿


CREATE VIEW [custom].[vWhse] --[custom].[V_Warehouse]
AS
select --top 5 
	[Company #]						= convert(int, 1)
	, [Product]						= convert(varchar(24),[Product ID])
	, [Vendor Product]				= convert(varchar(24),[Vendor Prod])
	, [Item WHSE Status]			= convert(varchar(1),[Whse Status])
   	, [Vendor #]					= convert(int,[Vendor ID])
	, [WHSE Code]					= convert(varchar(4),[Warehouse] )
	, [Buying Unit]					=  convert(varchar(4),[Buy UOM]) 
	, [Corp Standard Pack]			=  convert(varchar(4),[Standard Pack]) 
	, [Case Qty]					=  convert(int,convert(decimal(9,0),[Case Qty])) 
	, [Current Standard Cost]		=  convert(decimal(10,4),[Current Std Cost]) 
	, [Current Replacement Cost]	=  convert(decimal(10,4),[Current Repl Cost])  
	, [Regular Base Price]			=  convert(decimal(10,4),[regular base price])  
	, [Current End User Each Price]	=  convert(decimal(10,4),[CEU Ea Prc]) 
	, [Current End User 1 Price]	=  convert(decimal(10,4),[CEU 1 Prc])  
	, [Current End User 2 Price]	=  convert(decimal(10,4),[CEU 2 Prc])  
	, [Regular End User Each Price]	=  convert(decimal(10,4),[REU Ea Prc])  
	, [Regular End User 1 Price]	=  convert(decimal(10,4),[REU 1 Prc])  
	, [Regular End User 2 Price]	=  convert(decimal(10,4),[REU 2 Prc])  
	, [Regular End User 1 Qty]		=  convert(int,convert(decimal(9,0),[Regular End User Qty 1]))  
	, [Regular End User 2 Qty]		=  convert(int,convert(decimal(9,0),[Regular End User Qty 2]))  
	, [Regular List Each Price]		=  convert(decimal(10,4),[List Each])  
	, [Regular List Lot 1 Price]	=  convert(decimal(10,4),[List Lot Price 1])  
	, [Regular List Lot 2 Price]	=  convert(decimal(10,4),[List Lot Price 2])  
	, [Regular List Lot 1 Qty]		=  convert(int,convert(decimal(9,0),[List Lot Qty 1])) 
	, [Regular List Lot 2 Qty]		=  convert(int,convert(decimal(9,0),[List Lot Qty 2]))  
	, [Price Type]					=  convert(varchar(8),[Price Type])  
	, [Regular Replacement Cost]	=  convert(decimal(10,4),[Reg Repl Cost])  
	, [Regular Standard Cost]		=  convert(decimal(10,4),[Reg Std Cost])  
	, [Line Discount Begin Dt.]		=  convert(datetime,[Line Discount Begin Date]) 
	, [Line Discount End Dt.]		=  convert(datetime,[Line Discount End Date])  
	, [Line Discount Percent]		=  convert(decimal(10,4),[Line Discount Percent])  
	, [Line Discount Qty]			=  convert(int,convert(decimal(9,0),[Line Discount Qty])) 
	, [Vndr Base Corp Cost]			=  convert(decimal(10,4),[Vendor Corp Cost])  
	, [Vndr Base Store Cost]		=  convert(decimal(10,4),[Vendor Store Cost])  
	, [Vndr List Cost]				=  convert(decimal(10,4),[Vendor List Cost])  
	, [RP Family Group]				=  convert(varchar(25),[RP Family Group])  
	, [ST Family Group]				=  convert(varchar(25),[ST Family Group])   
	, [BP Family Group] 			=  convert(varchar(25),[BP Family Group])  
	, [EU Family Group] 			=  convert(varchar(25),[EU Family Group])  
	, [EU1 Family Group]			=  convert(varchar(25),[EU1 Family Group]) 
	, [EU2 Family Group]			=  convert(varchar(25),[EU2 Family Group])  
	, [Promo Page #]				=  convert(varchar(5),[Promo Page #])  
	, [Flyer Month] 				=  convert(varchar(20),[Flyer Month])  
	, [Last Flyer]					=  convert(varchar(7),[Last Flyer])  
FROM custom.V_AllPrices AP with (nolock) 
INNER JOIN (
	SELECT     ProductKey, [Product ID], [Promo Page #], [Flyer Month], [Last Flyer]
    FROM (
		SELECT     
			PRODUCT_STRUCTURE_FK AS ProductKey, dn.label AS 'DataName', VALUE AS 'DataValue'
        FROM          epacube.product_identification pid with (nolock) 
		INNER JOIN epacube.data_name dn with (nolock) ON dn.data_name_id = pid.data_name_fk
        WHERE      pid.DATA_NAME_FK IN (110100)
        UNION
        SELECT     pa.PRODUCT_STRUCTURE_FK, dn.Label, pa.ATTRIBUTE_EVENT_DATA
        FROM         epacube.PRODUCT_ATTRIBUTE pa with (nolock) 
		INNER JOIN epacube.DATA_NAME dn with (nolock) ON pa.DATA_NAME_FK = dn.DATA_NAME_ID
        WHERE     pa.DATA_NAME_FK IN (100100287, 100100229, 100100237)
		) P 
	PIVOT (Max(p.DataValue) FOR DataName IN ([Product ID], [Promo Page #], 
                    [Flyer Month], [Last Flyer])) AS Pivottable2
	) pt2 ON AP.PRODUCT_STRUCTURE_FK = pt2.ProductKey 
LEFT OUTER JOIN (
	SELECT     ProductKey, org_entity_structure_fk, [Vendor ID], [Vendor Prod], [Case Qty], [Buy UOM], [Standard Pack], [Current End User Qty 1], 
                     [Current End User Qty 2], [Regular End User Qty 1], [Regular End User Qty 2], [Whse Status], [Lot Qty A], [Lot Qty B], [List Lot Qty 1], [List Lot Qty 2], 
                      [Price Type], [Line Discount Begin Date], [Line Discount End Date], [Line Discount Percent], [Line Discount Qty], [RP Family Group], [ST Family Group], 
                      [BP Family Group], [EU Family Group], [EU1 Family Group], [EU2 Family Group]
    FROM (
		SELECT     pa.PRODUCT_STRUCTURE_FK AS ProductKey, org_entity_structure_fk, dn.Label AS 'DataName', 
                                                                           pa.ATTRIBUTE_EVENT_DATA AS 'DataValue'
        FROM          epacube.PRODUCT_ATTRIBUTE pa with (nolock) 
		INNER JOIN epacube.DATA_NAME dn with (nolock) ON pa.DATA_NAME_FK = dn.DATA_NAME_ID
        WHERE      pa.DATA_NAME_FK IN (211825, 100100373, 100100396, 100100397, 100100402, 100100403, 100100410, 100100411, 100100412, 
                            100100413, 100100377, 100100378, 100100379, 100100380)
	    UNION
	    SELECT     pid.Product_Structure_FK, pa.ORG_ENTITY_STRUCTURE_FK, dn.Label, value
	    FROM         epacube.PRODUCT_IDENTification pid with (nolock) 
		INNER JOIN epacube.data_name dn with (nolock) ON dn.data_name_id = pid.data_name_fk 
		INNER JOIN epacube.PRODUCT_ASSOCIATION pa with (nolock) ON 
			pid.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK 
			AND pid.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
	    WHERE     pa.DATA_NAME_FK = 253501
	    UNION
	    SELECT     Product_Structure_FK, pc.ORG_ENTITY_STRUCTURE_FK, dn.Label, dv.value
	    FROM         epacube.product_category pc with (nolock) 
		INNER JOIN epacube.data_name dn with (nolock) ON pc.DATA_NAME_FK = dn.DATA_NAME_ID 
		INNER JOIN epacube.data_value dv with (nolock) ON pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
	    WHERE     pc.DATA_NAME_FK IN (211901, 100100423, 100100424, 100100425, 100100426, 100100427, 100100428)
	    UNION
	    SELECT     puc.PRODUCT_STRUCTURE_FK, puc.org_entity_structure_fk, dn.Label, uc.UOM_CODE
	    FROM         epacube.PRODUCT_UOM_CLASS puc with (nolock) 
		INNER JOIN epacube.DATA_NAME dn with (nolock) ON puc.DATA_NAME_FK = dn.DATA_NAME_ID 
		INNER JOIN epacube.UOM_CODE uc with (nolock) ON puc.UOM_CODE_FK = uc.UOM_CODE_ID
	    WHERE     puc.DATA_NAME_FK IN (211601, 211602)
	    UNION
	    SELECT     PA.PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, 'Vendor ID', ei.value
	    FROM         epacube.PRODUCT_ASSOCIATION pa with (nolock) 
		INNER JOIN epacube.ENTITY_STRUCTURE es with (nolock) ON pa.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID 
		INNER JOIN epacube.ENTITY_IDENTIFICATION ei with (nolock) ON ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID 
		INNER JOIN epacube.DATA_NAME dn with (nolock) ON pa.DATA_NAME_FK = dn.DATA_NAME_ID
		WHERE     pa.DATA_NAME_FK = 253501
		) b 
	PIVOT (Max(b.DataValue) FOR DataName IN ([Vendor Prod], [Case Qty], [Vendor ID], [Buy UOM], 
              [Standard Pack], [Current End User Qty 1], [Current End User Qty 2], [Regular End User Qty 1], [Regular End User Qty 2], [Whse Status], [Lot Qty A], 
              [Lot Qty B], [List Lot Qty 1], [List Lot Qty 2], [Price Type], [Line Discount Begin Date], [Line Discount End Date], [Line Discount Percent], 
              [Line Discount Qty], [RP Family Group], [ST Family Group], [BP Family Group], [EU Family Group], [EU1 Family Group], [EU2 Family Group])) 
              AS Pivottable3) pt3 
    ON AP.PRODUCT_STRUCTURE_FK = pt3.ProductKey AND AP.ORG_ENTITY_STRUCTURE_FK = pt3.ORG_ENTITY_STRUCTURE_FK


