﻿create view
custom.v_SpecialOrder as

select pa.product_structure_fk,[Product ID],[Vendor ID], [Manufacturer Product], [Description 1] , [Description 2] ,[SX Product Category], [Brand Code],
[Model Number], [UPC],[Description 3], [Long Desc], [Stocking UOM], [Length Value],[Width Value], [Height Value], [Weight Value],
[MSDS Flag],[MSDS Sheet Number],[MSDS Change Date],[User6c],[User7c],[User8c],[Web Node ID],[Web Hierarchy Assignment],[Taxonomy Assignment], [Dept],[NoDash],[User Description],[Country of Origin],[Discontinued Ind]
from epacube.PRODUCT_ASSOCIATION pa 
inner join (select PRODUCT_STRUCTURE_FK,[Product ID],[Vendor ID],[Vendor Prod] as [Manufacturer Product], [Description 1] , [Description 2] ,[SX PCAT] as [SX Product Category],[Brand Name]as [Brand Code],
[Model Number], [UPC],[Description 3],[Description] as [Long Desc],[Stk UOM] as [Stocking UOM],[Length] as [Length Value],[Width] as [Width Value],[Height] as [Height Value],[Weight] as [Weight Value],
[MSDS Flag],[MSDS Sheet Number],[MSDS Change Date],[User6c],[User7c],[User8c],[Web Node ID],[Web Hierarchy Assignment],[Taxonomy Assignment], [Dept],[NoDash],[User Description],[Country of Origin],[Discontinued Ind]
	from (
	
	select PRODUCT_STRUCTURE_FK, dn.label as 'DataName', VALUE as 'DataValue' 
	from epacube.product_identification pid 
	inner join epacube.data_name dn on dn.data_name_id = pid.data_name_fk 
    where pid.DATA_NAME_FK in (110100,110111) 
UNION
	select PRODUCT_STRUCTURE_FK, dn.label , VALUE 
	from epacube.PRODUCT_IDENT_NONUNIQUE pin
	inner join epacube.data_name dn on dn.data_name_id = pin.data_name_fk 
	where pin.DATA_NAME_FK in (110102,100100274)
UNION 
	SELECT PA.PRODUCT_STRUCTURE_FK, 'Vendor ID', ei.value 
	FROM epacube.PRODUCT_ASSOCIATION pa 
	inner join epacube.ENTITY_STRUCTURE es on pa.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID 
	inner join epacube.ENTITY_IDENTIFICATION ei on ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID 
	inner join epacube.DATA_NAME dn on pa.DATA_NAME_FK = dn.DATA_NAME_ID where pa.DATA_NAME_FK = 253501 
UNION 
	select pd.PRODUCT_STRUCTURE_FK, dn.Label, pd.DESCRIPTION 
	from epacube.PRODUCT_DESCRIPTION pd 
	inner join epacube.DATA_NAME dn on pd.DATA_NAME_FK = dn.DATA_NAME_ID 
	where pd.DATA_NAME_FK in (110401,110402,210405,100100360)
UNION 
	select pa.PRODUCT_STRUCTURE_FK, dn.Label, pa.ATTRIBUTE_EVENT_DATA 
	from epacube.PRODUCT_ATTRIBUTE pa 
	inner join epacube.DATA_NAME dn on pa.DATA_NAME_FK = dn.DATA_NAME_ID 
	where pa.DATA_NAME_FK in (210843,110815,110812,110813,110811,110816,110817,110818,290806,290807,290808,100100100,100100163,100100158) 
UNION
	SELECT Product_Structure_FK, dn.Label, dv.value 
	from epacube.product_category pc
	inner join epacube.data_name dn on pc.DATA_NAME_FK = dn.DATA_NAME_ID 
	inner join epacube.data_value dv on pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
	where pc.DATA_NAME_FK in (210903,210901,100100100,100100357,100100161)
UNION 
	select pt.PRODUCT_STRUCTURE_FK, dn.Label, pt.TEXT 
	from epacube.PRODUCT_TEXT pt 
	inner join epacube.DATA_NAME dn on pt.DATA_NAME_FK = dn.DATA_NAME_ID 
	where pt.DATA_NAME_FK in (100100162) 
UNION 
	select puc.PRODUCT_STRUCTURE_FK, dn.short_name, uc.UOM_CODE 
	from epacube.PRODUCT_UOM_CLASS puc 
	inner join epacube.DATA_NAME dn on puc.DATA_NAME_FK = dn.DATA_NAME_ID 
	inner join epacube.UOM_CODE uc on puc.UOM_CODE_FK = uc.UOM_CODE_ID
	where puc.DATA_NAME_FK in (110603) 
UNION 
	SELECT Product_Structure_FK, 'Web Hierarchy Assignment', dv.DESCRIPTION from epacube.product_category pc
	inner join epacube.data_name dn on pc.DATA_NAME_FK = dn.DATA_NAME_ID 
	inner join epacube.data_value dv on pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
	where pc.DATA_NAME_FK in (100100100)	
)P

PIVOT (Max(p.DataValue) for DataName in ([Product ID],[Vendor ID],[Vendor Prod],[Description 1], [Description 2] ,[SX PCAT],[Brand Name],[Model Number] , [UPC],
[Description 3],[Description],[Stk UOM],[Length],[Width],[Height],[Weight],[MSDS Flag],[MSDS Sheet Number],[MSDS Change Date],[User6c],[User7c],[User8c],
[Web Node ID],[Web Hierarchy Assignment],[Taxonomy Assignment],[Dept],[NoDash],[User Description],[Country of Origin],[Discontinued Ind]) 
) as ProductPivot) pp on pa.PRODUCT_STRUCTURE_FK = pp.PRODUCT_STRUCTURE_FK
where pa.DATA_NAME_FK = 159200 and pa.ENTITY_STRUCTURE_FK = 16






