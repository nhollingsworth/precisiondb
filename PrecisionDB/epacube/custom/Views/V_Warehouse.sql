﻿
CREATE VIEW [custom].[V_Warehouse]
AS
SELECT     1 AS [Company #], [Product ID] AS [Product], [Vendor Prod], [Whse Status], [Vendor ID], [Warehouse], [Buy UOM], [Standard Pack], [Case Qty], 
                      [Current Std Cost] AS [Standard Cost], [Current Repl Cost] AS [Replacement Cost],[regular base price] as [SX BASE PRICE], [CEU Ea Prc] AS [Current End User Each Price], 
                      [CEU 1 Prc] AS [Current End User Price 1], [CEU 2 Prc] AS [Current End User Price 2], [Current End User Qty 1], [Current End User Qty 2], 
                      [REU Ea Prc] AS [Regular End User Each Price], [REU 1 Prc] AS [Regular End User Price 1], [REU 2 Prc] AS [Regular End User Price 2], [Regular End User Qty 1], 
                      [Regular End User Qty 2], [Lot A Prc] AS [Lot Price A], [Lot B Prc] AS [Lot Price B], [Lot Qty A], [Lot Qty B], [List Each], [List Lot Price 1], [List Lot Price 2], [List Lot Qty 1], 
                      [List Lot Qty 2], [Price Type], [Reg Repl Cost] AS [Regular Replacement Cost], [Reg Std Cost] AS [Regular Standard Cost], [Suggested Contractor Price], 
                      [Line Discount Begin Date], [Line Discount End Date], [Line Discount Percent], [Line Discount Qty], [Vendor Corp Cost], [Vendor Store Cost], [Vendor List Cost], 
                      [RP Family Group], [ST Family Group], [BP Family Group], [EU Family Group], [EU1 Family Group], [EU2 Family Group], [Promo Page #], [Flyer Month], 
                      [Last Flyer]
FROM         custom.V_AllPrices AP with (nolock) INNER JOIN
                          (SELECT     ProductKey, [Product ID], [Promo Page #], [Flyer Month], [Last Flyer]
                            FROM          (SELECT     PRODUCT_STRUCTURE_FK AS ProductKey, dn.label AS 'DataName', VALUE AS 'DataValue'
                                                    FROM          epacube.product_identification pid with (nolock) INNER JOIN
                                                                           epacube.data_name dn with (nolock) ON dn.data_name_id = pid.data_name_fk
                                                    WHERE      pid.DATA_NAME_FK IN (110100)
                                                    UNION
                                                    SELECT     pa.PRODUCT_STRUCTURE_FK, dn.Label, pa.ATTRIBUTE_EVENT_DATA
                                                    FROM         epacube.PRODUCT_ATTRIBUTE pa with (nolock) INNER JOIN
                                                                          epacube.DATA_NAME dn with (nolock) ON pa.DATA_NAME_FK = dn.DATA_NAME_ID
                                                    WHERE     pa.DATA_NAME_FK IN (100100287, 100100229, 100100237)) P PIVOT (Max(p.DataValue) FOR DataName IN ([Product ID], [Promo Page #], 
                                                   [Flyer Month], [Last Flyer])) AS Pivottable2) pt2 ON AP.PRODUCT_STRUCTURE_FK = pt2.ProductKey LEFT OUTER JOIN
                          (SELECT     ProductKey, org_entity_structure_fk, [Vendor ID], [Vendor Prod], [Case Qty], [Buy UOM], [Standard Pack], [Current End User Qty 1], 
                                                   [Current End User Qty 2], [Regular End User Qty 1], [Regular End User Qty 2], [Whse Status], [Lot Qty A], [Lot Qty B], [List Lot Qty 1], [List Lot Qty 2], 
                                                   [Price Type], [Line Discount Begin Date], [Line Discount End Date], [Line Discount Percent], [Line Discount Qty], [RP Family Group], [ST Family Group], 
                                                   [BP Family Group], [EU Family Group], [EU1 Family Group], [EU2 Family Group]
                            FROM          (SELECT     pa.PRODUCT_STRUCTURE_FK AS ProductKey, org_entity_structure_fk, dn.Label AS 'DataName', 
                                                                           pa.ATTRIBUTE_EVENT_DATA AS 'DataValue'
                                                    FROM          epacube.PRODUCT_ATTRIBUTE pa with (nolock) INNER JOIN
                                                                           epacube.DATA_NAME dn with (nolock) ON pa.DATA_NAME_FK = dn.DATA_NAME_ID
                                                    WHERE      pa.DATA_NAME_FK IN (211825, 100100373, 100100396, 100100397, 100100402, 100100403, 100100410, 100100411, 100100412, 
                                                                           100100413, 100100377, 100100378, 100100379, 100100380)
                                                    UNION
                                                    SELECT     pid.Product_Structure_FK, pa.ORG_ENTITY_STRUCTURE_FK, dn.Label, value
                                                    FROM         epacube.PRODUCT_IDENTification pid with (nolock) INNER JOIN
                                                                          epacube.data_name dn with (nolock) ON dn.data_name_id = pid.data_name_fk INNER JOIN
                                                                          epacube.PRODUCT_ASSOCIATION pa with (nolock) ON pid.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK AND 
                                                                          pid.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
                                                    WHERE     pa.DATA_NAME_FK = 253501
                                                    UNION
                                                    SELECT     Product_Structure_FK, pc.ORG_ENTITY_STRUCTURE_FK, dn.Label, dv.value
                                                    FROM         epacube.product_category pc with (nolock) INNER JOIN
                                                                          epacube.data_name dn with (nolock) ON pc.DATA_NAME_FK = dn.DATA_NAME_ID INNER JOIN
                                                                          epacube.data_value dv with (nolock) ON pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
                                                    WHERE     pc.DATA_NAME_FK IN (211901, 100100423, 100100424, 100100425, 100100426, 100100427, 100100428)
                                                    UNION
                                                    SELECT     puc.PRODUCT_STRUCTURE_FK, puc.org_entity_structure_fk, dn.Label, uc.UOM_CODE
                                                    FROM         epacube.PRODUCT_UOM_CLASS puc with (nolock) INNER JOIN
                                                                          epacube.DATA_NAME dn with (nolock) ON puc.DATA_NAME_FK = dn.DATA_NAME_ID INNER JOIN
                                                                          epacube.UOM_CODE uc with (nolock) ON puc.UOM_CODE_FK = uc.UOM_CODE_ID
                                                    WHERE     puc.DATA_NAME_FK IN (211601, 211602)
                                                    UNION
                                                    SELECT     PA.PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, 'Vendor ID', ei.value
                                                    FROM         epacube.PRODUCT_ASSOCIATION pa with (nolock) INNER JOIN
                                                                          epacube.ENTITY_STRUCTURE es with (nolock) ON pa.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID INNER JOIN
                                                                          epacube.ENTITY_IDENTIFICATION ei with (nolock) ON ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID INNER JOIN
                                                                          epacube.DATA_NAME dn with (nolock) ON pa.DATA_NAME_FK = dn.DATA_NAME_ID
                                                    WHERE     pa.DATA_NAME_FK = 253501) b PIVOT (Max(b.DataValue) FOR DataName IN ([Vendor Prod], [Case Qty], [Vendor ID], [Buy UOM], 
                                                   [Standard Pack], [Current End User Qty 1], [Current End User Qty 2], [Regular End User Qty 1], [Regular End User Qty 2], [Whse Status], [Lot Qty A], 
                                                   [Lot Qty B], [List Lot Qty 1], [List Lot Qty 2], [Price Type], [Line Discount Begin Date], [Line Discount End Date], [Line Discount Percent], 
                                                   [Line Discount Qty], [RP Family Group], [ST Family Group], [BP Family Group], [EU Family Group], [EU1 Family Group], [EU2 Family Group])) 
                                                   AS Pivottable3) pt3 ON AP.PRODUCT_STRUCTURE_FK = pt3.ProductKey AND AP.ORG_ENTITY_STRUCTURE_FK = pt3.ORG_ENTITY_STRUCTURE_FK


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_Warehouse';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_Warehouse';

