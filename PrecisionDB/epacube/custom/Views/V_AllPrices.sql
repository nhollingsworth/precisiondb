﻿CREATE VIEW custom.V_AllPrices
AS
SELECT     pa.PRODUCT_STRUCTURE_FK, pa.ORG_ENTITY_STRUCTURE_FK, pid.value AS [Product_ID], eid.value AS [Warehouse], List_effDate, [List Each], [List Lot Price 1], 
                      [List Lot Price 2], RegCost_EffDate, [Reg Repl Cost], [Reg Std Cost], CEU_EffDate, [CEU Ea Prc], [CEU 1 Prc], [CEU 2 Prc], REU_EffDate, [Lot A Prc], [Lot B Prc], 
                      [REU Ea Prc], [REU 1 Prc], [REU 2 Prc], [RegBase_EffDate], [Regular Base Price], [PrEU_EffDate], [PrEU_EndDate], [Promo End User Each], Curr_EffDate, 
                      [Current Repl Cost], [Current Std Cost], Vend_EffDate, [Vendor Corp Cost], [Vendor Store Cost], [Vendor List Cost], [Suggested Contractor Price], Promo_EffDate, 
                      Promo_EndDate, [Promo Repl Cost], [Promo Std Cost]
FROM         epacube.PRODUCT_ASSOCIATION pa LEFT OUTER JOIN
                          (SELECT     Product_structure_fk, Org_entity_structure_fk, List_effDate, [List Lot Price 1], [List Lot Price 2], [List Each]
                            FROM          (SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date) AS List_EffDate, Org_entity_structure_fk, 'List Lot Price 1' AS DataName, 
                                                                           cast(NET_VALUE3 AS varchar(1000)) AS Datavalue
                                                    FROM          marginmgr.SHEET_RESULTS
                                                    WHERE      DATA_NAME_FK = 100100404
                                                    /*DN100100408*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date), Org_entity_structure_fk, 'List Each', cast(NET_VALUE4 AS varchar(1000))
                                                    FROM         marginmgr.SHEET_RESULTS
                                                    WHERE     DATA_NAME_FK = 100100404
                                                    /*DN231104*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date), Org_entity_structure_fk, 'List Lot Price 2', cast(NET_VALUE5 AS varchar(1000))
                                                    FROM         marginmgr.SHEET_RESULTS
                                                    WHERE     DATA_NAME_FK = 100100404/*DN100100408*/ ) AS P PIVOT (Max(P.DataValue) FOR DataName IN ([List Lot Price 1], [List Lot Price 2], 
                                                   [List Each])) AS ListTable) lt ON lt.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK AND 
                      lt.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK LEFT OUTER JOIN
                          (SELECT     PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, RegCost_EffDate, [Reg Repl Cost], [Reg Std Cost]
                            FROM          (SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date) AS RegCost_EffDate, Org_entity_structure_fk, 'Reg Repl Cost' AS DataName, 
                                                                           cast(NET_VALUE1 AS varchar(1000)) AS Datavalue
                                                    FROM          marginmgr.SHEET_RESULTS
                                                    WHERE      DATA_NAME_FK = 231100
                                                    /*DN231101*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date), Org_entity_structure_fk, 'Reg Std Cost', cast(NET_VALUE2 AS varchar(1000))
                                                    FROM         marginmgr.SHEET_RESULTS
                                                    WHERE     DATA_NAME_FK = 231100/*DN231102*/ ) AS P PIVOT (Max(P.DataValue) FOR DataName IN ([Reg Repl Cost], [Reg Std Cost])) AS SX) sxCost ON
                       sxCost.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK AND 
                      sxCost.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK LEFT OUTER JOIN
                          (SELECT     PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, CEU_EffDate, [CEU Ea Prc], [CEU 1 Prc], [CEU 2 Prc]
                            FROM          (SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date) AS CEU_EffDate, Org_entity_structure_fk, 'CEU Ea Prc' AS DataName, 
                                                                           cast(NET_VALUE1 AS varchar(1000)) AS Datavalue
                                                    FROM          marginmgr.SHEET_RESULTS
                                                    WHERE      DATA_NAME_FK = 100100392
                                                    /*DN100100393*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date), Org_entity_structure_fk, 'CEU 1 Prc', cast(NET_VALUE2 AS varchar(1000))
                                                    FROM         marginmgr.SHEET_RESULTS
                                                    WHERE     DATA_NAME_FK = 100100392
                                                    /*DN100100394*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date), Org_entity_structure_fk, 'CEU 2 Prc', cast(NET_VALUE3 AS varchar(1000))
                                                    FROM         marginmgr.SHEET_RESULTS
                                                    WHERE     DATA_NAME_FK = 100100392/*DN100100395*/ ) AS P PIVOT (Max(P.DataValue) FOR DataName IN ([CEU Ea Prc], [CEU 1 Prc], [CEU 2 Prc])) 
                                                   AS CEU) CurEndUse ON CurEndUse.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK AND 
                      CurEndUse.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK LEFT OUTER JOIN
                          (SELECT     PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, REU_EffDate, [Lot A Prc], [Lot B Prc], [REU Ea Prc], [REU 1 Prc], [REU 2 Prc]
                            FROM          (SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date) AS REU_EffDate, Org_entity_structure_fk, 'Lot A Prc' AS DataName, 
                                                                           cast(NET_VALUE5 AS varchar(1000)) AS Datavalue
                                                    FROM          marginmgr.SHEET_RESULTS
                                                    WHERE      DATA_NAME_FK = 100100398
                                                    /*DN100100405*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date), Org_entity_structure_fk, 'Lot B Prc', cast(NET_VALUE6 AS varchar(1000))
                                                    FROM         marginmgr.SHEET_RESULTS
                                                    WHERE     DATA_NAME_FK = 100100398
                                                    /*DN100100406*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date), Org_entity_structure_fk, 'REU Ea Prc', cast(NET_VALUE1 AS varchar(1000))
                                                    FROM         marginmgr.SHEET_RESULTS
                                                    WHERE     DATA_NAME_FK = 100100398
                                                    /*DN100100399*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date), Org_entity_structure_fk, 'REU 1 Prc', cast(NET_VALUE2 AS varchar(1000))
                                                    FROM         marginmgr.SHEET_RESULTS
                                                    WHERE     DATA_NAME_FK = 100100398
                                                    /*DN100100400*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date), Org_entity_structure_fk, 'REU 2 Prc', cast(NET_VALUE3 AS varchar(1000))
                                                    FROM         marginmgr.SHEET_RESULTS
                                                    WHERE     DATA_NAME_FK = 100100398/*DN100100401*/ ) AS P PIVOT (Max(P.DataValue) FOR DataName IN ([Lot A Prc], [Lot B Prc], [REU Ea Prc], 
                                                   [REU 1 Prc], [REU 2 Prc])) AS REU) RegEndUse ON RegEndUse.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK AND 
                      RegEndUse.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK LEFT OUTER JOIN
                          (SELECT     PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, cast(effective_date AS date) AS RegBase_EffDate, cast(NET_VALUE3 AS varchar(1000)) 
                                                   AS 'Regular Base Price'
                            FROM          marginmgr.SHEET_RESULTS
                            WHERE      DATA_NAME_FK = 100100416) rbp ON rbp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK AND 
                      rbp.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK LEFT OUTER JOIN
                          (SELECT     PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, cast(effective_date AS date) AS PrEU_EffDate, cast(end_date AS date) AS PrEU_EndDate, 
                                                   cast(NET_VALUE1 AS varchar(1000)) AS 'Promo End User Each'
                            FROM          marginmgr.SHEET_RESULTS
                            WHERE      DATA_NAME_FK = 100100520) peu ON peu.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK AND 
                      peu.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK LEFT OUTER JOIN
                          (SELECT     PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, Vend_EffDate, [Vendor Corp Cost], [Vendor Store Cost], [Vendor List Cost], 
                                                   [Suggested Contractor Price]
                            FROM          (SELECT     PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, cast(effective_date AS date) AS Vend_EffDate, 'Vendor Corp Cost' AS DataName, 
                                                                           cast(NET_VALUE1 AS varchar(1000)) AS Datavalue
                                                    FROM          epacube.product_values
                                                    WHERE      DATA_NAME_FK = 100100418
                                                    /*DN100100419*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, cast(effective_date AS date), 'Vendor Store Cost', cast(NET_VALUE2 AS varchar(1000))
                                                    FROM         epacube.product_values
                                                    WHERE     DATA_NAME_FK = 100100418
                                                    /*DN100100420*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, cast(effective_date AS date), 'Vendor List Cost', cast(NET_VALUE3 AS varchar(1000))
                                                    FROM         epacube.product_values
                                                    WHERE     DATA_NAME_FK = 100100418
                                                    /*DN100100421*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, cast(effective_date AS date), 'Suggested Contractor Price', 
                                                                          cast(NET_VALUE4 AS varchar(1000))
                                                    FROM         epacube.product_values
                                                    WHERE     DATA_NAME_FK = 100100418/*DN100100355*/ ) AS P PIVOT (Max(P.DataValue) FOR DataName IN ([Vendor Corp Cost], [Vendor Store Cost], 
                                                   [Vendor List Cost], [Suggested Contractor Price])) AS VendCost) vc ON vc.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK AND 
                      vc.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK LEFT OUTER JOIN
                          (SELECT     PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, Promo_EffDate, Promo_EndDate, [Promo Repl Cost], [Promo Std Cost]
                            FROM          (SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date) AS Promo_EffDate, cast(End_date AS date) AS Promo_EndDate, 
                                                                           Org_entity_structure_fk, 'Promo Repl Cost' AS DataName, cast(NET_VALUE1 AS varchar(1000)) AS Datavalue
                                                    FROM          marginmgr.SHEET_RESULTS
                                                    WHERE      DATA_NAME_FK = 100100500
                                                    /*DN100100501*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date), cast(end_date AS date), Org_entity_structure_fk, 'Promo Std Cost', 
                                                                          cast(NET_VALUE2 AS varchar(1000))
                                                    FROM         marginmgr.SHEET_RESULTS
                                                    WHERE     DATA_NAME_FK = 100100500/*DN100100502 -add end date*/ ) AS P PIVOT (Max(P.DataValue) FOR DataName IN ([Promo Repl Cost], 
                                                   [Promo Std Cost])) AS Promo) p ON p.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK AND 
                      p.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK LEFT OUTER JOIN
                          (SELECT     PRODUCT_STRUCTURE_FK, Org_entity_structure_fk, Curr_EffDate, [Current Repl Cost], [Current Std Cost]
                            FROM          (SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date) AS Curr_EffDate, Org_entity_structure_fk, 'Current Repl Cost' AS DataName, 
                                                                           cast(NET_VALUE1 AS varchar(1000)) AS Datavalue
                                                    FROM          marginmgr.SHEET_RESULTS
                                                    WHERE      DATA_NAME_FK = 100100530
                                                    /*DN100100531*/ UNION
                                                    SELECT     PRODUCT_STRUCTURE_FK, cast(effective_date AS date), Org_entity_structure_fk, 'Current Std Cost', cast(NET_VALUE2 AS varchar(1000))
                                                    FROM         marginmgr.SHEET_RESULTS
                                                    WHERE     DATA_NAME_FK = 100100530/*DN100100532*/ ) AS P PIVOT (Max(P.DataValue) FOR DataName IN ([Current Repl Cost], [Current Std Cost])) 
                                                   AS Curr) c ON c.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK AND 
                      c.ORG_ENTITY_STRUCTURE_FK = pa.ORG_ENTITY_STRUCTURE_FK INNER JOIN
                      epacube.entity_identification eid ON pa.ORG_ENTITY_STRUCTURE_FK = eid.ENTITY_STRUCTURE_FK INNER JOIN
                      epacube.product_identification pid ON pa.PRODUCT_STRUCTURE_FK = pid.PRODUCT_STRUCTURE_FK AND pid.DATA_NAME_FK = 110100
WHERE     pa.DATA_NAME_FK = 159100

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_AllPrices';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_AllPrices';

