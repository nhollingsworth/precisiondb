﻿
CREATE VIEW [custom].[v_Product_Details_By_Whse_Static]
AS
SELECT     [View Refresh Date], [Product_Creation_epaDATE], Product, [WHSE Code], [Vendor #], [Vendor Product], [Regular Replacement Cost], [Regular Standard Cost], [Current End User Each Price], [Current End User 1 Price], 
                      [Current End User 2 Price], [Lot Price A], [Lot Price B], [Regular End User Each Price], [Regular End User 1 Price], [Regular End User 2 Price], [List Each], 
                      [List Lot Price 1], [List Lot Price 2], [Regular Base Price], [Promo Repl Cost], [Promo Std Cost], [Promo End User Each], [Current Replacement Cost], 
                      [Current Standard Cost], [Suggested Contractor Price], [Vendor Corp Cost], [Vendor List Cost], [Vendor Store Cost], Length, Width, Height, Weight, 
                      [MSDS Sheet Number], [DC Sell QTY], [WHSE Status], [Cross Dock/User1W], [AOC Vendor#], [Catalog Page], [Country of Origin], [Discontinued Ind], 
                      [End User Each Published Flag ], [End User Lot 1 Published Flag], [End User Lot 2 Published Flag], [Flyer Month], [Last Flyer], Memo, [Promo Price Sheet #], 
                      [Promo Price Sheet Descrip], [Promo Page #], [Regular Drop Ship 1 Qty], [Return Flag], [Seasonal CD], [Serialized Product Flag], [Taxable Tool Flag], [Transmit Flag], 
                      [Case Qty], [Freight Code 1], [Freight Code 2], [z_icsp.u_hazcode], [Line Discount Begin Date], [Line Discount End Date], [Line Discount Percent], [Line Discount Qty], 
                      [Vendor Std Pk], [Regular End User 1 Qty], [Regular End User 2 Qty], [Lot Qty A], [Lot Qty B], [List Lot Qty 1], [List Lot Qty 2], [Certification Code], [Vendor Price Sheet #], 
                      [Vendor Price Sheet Descrip], [Store DC Cost Effective Date], [Corp Cost Effective Date], [End User Price Effective Date], [Stocking UOM], [Corp Standard Pack], 
                      [Buying Unit], [SX PCAT], [Brand Name], [Price Type], [Web Node ID], Dept, [Taxonomy Assignment], [Store Prod Category], [RP Family Group], [ST Family Group], 
                      [BP Family Group], [EU Family Group], [EU1 Family Group], [EU2 Family Group], OnFlyers
FROM         custom.Product_Details_By_Warehouse


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Product_Details_By_Warehouse (custom)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 261
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'v_Product_Details_By_Whse_Static';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'v_Product_Details_By_Whse_Static';

