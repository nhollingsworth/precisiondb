﻿

CREATE VIEW [custom].[V_FuturePricing_View]
AS
SELECT DISTINCT 
                      1 AS CompanyNumber, Warehouse AS [Whse Code], [Vendor ID], [Product ID] AS [Product Number], [Product Description], isNULL(ed.NEW_DATA,pa2.ATTRIBUTE_EVENT_DATA) AS [Price Sheet #], 
                      ISNULL(ed_2.NEW_DATA,pa.ATTRIBUTE_EVENT_DATA) AS [Price Sheet Descrip], [Vendor Corp Cost], [Vendor Store Cost], [Vendor List Cost], RegCost_EffDate AS [Whse Costs Effective Date], 
                      RegCost_EndDate AS [Whse Costs End Date], [ST RULE], [Reg Repl Cost] AS [Replacement Cost], [RP RULE], [Reg Std Cost] AS [Standard Cost], 
                      RegBase_EffDate AS [Store DC Costs Effective Date], RegBase_EndDate AS [Store DC Costs End Date], [BP RULE], [Regular Base Price] AS [Base Price], 
                      REU_EffDate AS [End User Price Effective Date], REU_EndDate AS [End User Price End Date], [EU RULE], [REU Ea Prc] AS [End User Each Price], [EU1 RULE], 
                      [REU 1 Prc] AS [End User 1 Price], [EU2 RULE], [REU 2 Prc] AS [End User 2 Price], [Lot A Prc] AS [End User A], [Lot B Prc] AS [End User B], [List Each], [List Lot Price 1], 
                      [List Lot Price 2]


FROM         custom.V_FuturePrices ap with (Nolock)  LEFT OUTER JOIN
                          (SELECT     PRODUCT_STRUCTURE_FK, org_entity_structure_fk, [Vendor ID], [ST Family Group] AS [ST RULE], [RP Family Group] AS [RP RULE], 
                                                   [EU Family Group] AS [EU RULE], [BP Family Group] AS [BP RULE], [EU1 Family Group] AS [EU1 RULE], [EU2 Family Group] AS [EU2 RULE]
                            FROM          (SELECT     pc.PRODUCT_STRUCTURE_FK, org_entity_structure_fk, dn.Label AS 'DataName', dv.value AS 'DataValue'
                                                    FROM          epacube.PRODUCT_Category pc with (nolock)  INNER JOIN
                                                                           epacube.DATA_NAME dn  with (nolock) ON pc.DATA_NAME_FK = dn.DATA_NAME_ID INNER JOIN
                                                                           epacube.data_value dv  with (nolock)  ON   pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
                                                    WHERE      pc.DATA_NAME_FK IN (100100423, 100100424, 100100425, 100100426, 100100427, 100100428)
                                                    UNION
                                                    SELECT     PA.PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, 'Vendor ID', ei.value
                                                    FROM         epacube.PRODUCT_ASSOCIATION pa  with (nolock)  INNER JOIN
                                                                          epacube.ENTITY_STRUCTURE es with (nolock)  ON pa.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID INNER JOIN
                                                                          epacube.ENTITY_IDENTIFICATION ei  with (nolock)  ON ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID INNER JOIN
                                                                          epacube.DATA_NAME dn  with (nolock)  ON pa.DATA_NAME_FK = dn.DATA_NAME_ID
                                                    WHERE     pa.DATA_NAME_FK = 253501 AND ORG_ENTITY_STRUCTURE_FK <> 1) P PIVOT (Max(p.DataValue) FOR DataName IN ([Vendor ID], 
                                                   [ST Family Group], [RP Family Group], [EU Family Group], [BP Family Group], [EU1 Family Group], [EU2 Family Group])) AS RulePivot) rp ON 
                      ap.PRODUCT_STRUCTURE_FK = rp.PRODUCT_STRUCTURE_FK AND ap.ORG_ENTITY_STRUCTURE_FK = rp.ORG_ENTITY_STRUCTURE_FK LEFT OUTER JOIN
                          (SELECT     PRODUCT_STRUCTURE_FK, [Product ID], [Vendor Prod], [Description 1] AS [Product Description]
                            FROM          (SELECT     PRODUCT_STRUCTURE_FK, dn.label AS 'DataName', VALUE AS 'DataValue'
                                                    FROM          epacube.product_identification pid  with (nolock)  INNER JOIN
                                                                           epacube.data_name dn  with (nolock)  ON dn.data_name_id = pid.data_name_fk
                                                    WHERE      pid.DATA_NAME_FK IN (110100, 110111)
                                                    UNION
                                                    SELECT     pd.PRODUCT_STRUCTURE_FK, dn.Label, pd.DESCRIPTION
                                                    FROM         epacube.PRODUCT_DESCRIPTION pd  with (nolock)  INNER JOIN
                                                                          epacube.DATA_NAME dn with (nolock)  ON pd.DATA_NAME_FK = dn.DATA_NAME_ID
                                                    WHERE     pd.DATA_NAME_FK IN (110401)) P PIVOT (Max(p.DataValue) FOR DataName IN ([Product ID], [Vendor Prod], [Description 1])) AS ProductPivot) 
                      pp ON ap.PRODUCT_STRUCTURE_FK = pp.PRODUCT_STRUCTURE_FK left JOIN
                      synchronizer.EVENT_DATA ed  with (nolock)  ON ap.product_structure_fk = ed.product_Structure_fk AND ap.Vend_EffDate = ed.EVENT_EFFECTIVE_DATE AND 
                      EVENT_STATUS_CR_FK = 84 AND ed.DATA_NAME_FK = (100100435)
					  left JOIN
                      epacube.PRODUCT_ATTRIBUTE pa with (nolock)  ON ap.product_structure_fk = pa.product_Structure_fk AND pa.DATA_NAME_FK = (100100435)

					  left JOIN
                      epacube.PRODUCT_ATTRIBUTE pa2 with (nolock)  ON ap.product_structure_fk = pa2.product_Structure_fk AND pa2.DATA_NAME_FK = (100100436)
					  
					  
					   left JOIN
                      synchronizer.EVENT_DATA ed_2  with (nolock)  ON ap.product_structure_fk = ed_2.product_Structure_fk AND ap.Vend_EffDate = ed_2.EVENT_EFFECTIVE_DATE AND 
                      ed_2.EVENT_STATUS_CR_FK = 84 AND ed_2.DATA_NAME_FK = (100100436)
WHERE     ap.data_name_fk IN (100100418, 231100, 100100416, 100100398, 100100404)




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_FuturePricing_View';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'custom', @level1type = N'VIEW', @level1name = N'V_FuturePricing_View';

