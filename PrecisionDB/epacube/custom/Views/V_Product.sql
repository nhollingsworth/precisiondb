﻿



/*
*/
CREATE VIEW [custom].[V_Product]
AS
SELECT 1 as [Company #], [Product ID],[Transmit Flag],[Discontinued Ind],[Vendor Prod],[EDI Part#] as [EDI Part#],[Vlookup Mfg #] as [Vlookup Mfg#],[UPC],[Description 1],[User Description],[Memo],[NoDash],[AOC Vendor#],[Dept],[Vendor Std Pk],[Store Prod Category] as [PCAT],[Country of Origin],[Brand Name],[Catalog Page],[Return Flag],[Seasonal CD],[Serialized Product Flag],[Certification Code],[Taxable Tool Flag],[Taxonomy Assignment],[Web Node ID],[web hierarchy assignment],[Freight Code 1] as [Freight CD1],[Freight Code 2] as [Freight CD2],[z_icsp.u_hazcode] as [HazMat Code],[Height],[Length],[MSDS Sheet Number] as [MSDS Indicator],[DC Sell QTY] as [Sell Mult],[Stocking UOM],[Weight],[Width],[Vendor No],[SX PCAT],[End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag]
FROM(
select PRODUCT_STRUCTURE_FK as ProductKey, dn.label as 'DataName', VALUE as 'DataValue' from epacube.product_identification pid with (nolock)
inner join epacube.data_name dn with (nolock) on dn.data_name_id = pid.data_name_fk 
where pid.DATA_NAME_FK in (110100)
UNION
select pid.PRODUCT_STRUCTURE_FK as ProductKey, dn.label, VALUE from epacube.product_identification pid with (nolock)
inner join epacube.data_name dn with (nolock) on dn.data_name_id = pid.data_name_fk 
inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on pid.product_structure_fk = pa.PRODUCT_STRUCTURE_FK and pid.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK and pa.DATA_NAME_FK = 159300
where pid.DATA_NAME_FK in (110111)
UNION
select PRODUCT_STRUCTURE_FK as ProductKey, dn.label , VALUE  from epacube.product_ident_nonunique pid with (nolock) inner join epacube.data_name dn with (nolock) on dn.data_name_id = pid.data_name_fk 
where pid.DATA_NAME_FK in (110102,100100274,100100197,100100367)
UNION select pd.PRODUCT_STRUCTURE_FK, dn.label, pd.DESCRIPTION from epacube.PRODUCT_DESCRIPTION pd with (nolock)
inner join epacube.data_name dn with (nolock) on dn.data_name_id = pd.data_name_fk
where pd.DATA_NAME_FK in (110401,100100360) 
UNION select pa.PRODUCT_STRUCTURE_FK, dn.Label, pa.ATTRIBUTE_EVENT_DATA 
from epacube.PRODUCT_ATTRIBUTE pa with (nolock)
inner join epacube.DATA_NAME dn with (nolock) on pa.DATA_NAME_FK = dn.DATA_NAME_ID 
where pa.DATA_NAME_FK in (110811,110812,110813,110815,110817,210831,100100110,100100154,100100158,100100163,100100219,100100220,100100221,100100272,100100341,100100342,100100343,100100356,100100358,100100374,100100375,100100376,100100389,100100414) 
UNION select puc.PRODUCT_STRUCTURE_FK, dn.Label, uc.UOM_CODE from epacube.PRODUCT_UOM_CLASS puc with (nolock)
inner join epacube.DATA_NAME dn with (nolock) on puc.DATA_NAME_FK = dn.DATA_NAME_ID 
inner join epacube.UOM_CODE uc with (nolock) on puc.UOM_CODE_FK = uc.UOM_CODE_ID where puc.DATA_NAME_FK in (110603)
UNION SELECT PA.PRODUCT_STRUCTURE_FK, 'Vendor No', ei.value FROM epacube.PRODUCT_ASSOCIATION pa with (nolock)
inner join epacube.ENTITY_STRUCTURE es with (nolock) on pa.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID 
inner join epacube.ENTITY_IDENTIFICATION ei with (nolock) on ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID 
inner join epacube.DATA_NAME dn with (nolock) on pa.DATA_NAME_FK = dn.DATA_NAME_ID where pa.DATA_NAME_FK = 159300
UNION SELECT Product_Structure_FK, dn.Label, dv.value from epacube.product_category pc with (nolock)
inner join epacube.data_name dn with (nolock) on pc.DATA_NAME_FK = dn.DATA_NAME_ID 
inner join epacube.data_value dv with (nolock) on pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
where pc.DATA_NAME_FK in (210901,210903,100100161,100100100,100100357,100100415) 
UNION SELECT Product_Structure_FK, 'Web Hierarchy Assignment', dv.DESCRIPTION from epacube.product_category pc with (nolock)
inner join epacube.data_name dn with (nolock) on pc.DATA_NAME_FK = dn.DATA_NAME_ID 
inner join epacube.data_value dv with (nolock) on pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
where pc.DATA_NAME_FK in (100100100)) P 
PIVOT (Max(p.DataValue) for DataName in
 ([Product ID],[Description 1],[Vendor No],[UPC],[Vendor Prod],[Stocking UOM],[Length],[Width],[Height],[Weight],[MSDS Sheet Number],[Cmp],[DC Sell QTY],[SX PCAT],[Brand Name],[Web Node ID],[AOC Vendor#],[Catalog Page],[Country of Origin],[Dept],[Discontinued Ind],[EDI Part#],[End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Memo],[NoDash],[Return Flag],[Seasonal CD],[Serialized Product Flag],[Taxable Tool Flag],[Taxonomy Assignment],[Transmit Flag],[User Description],[Vlookup Mfg #],[web hierarchy assignment],[Freight Code 1],[Freight Code 2],[z_icsp.u_hazcode],[Vendor Std Pk],[Certification Code],[Store Prod Category]) ) As pivotTable 



