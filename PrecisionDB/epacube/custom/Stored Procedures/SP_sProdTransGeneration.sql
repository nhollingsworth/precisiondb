﻿
-- ================================================
-- Template generated from Template Explorer using:
-- ALTER PROCEDURE (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		<Author,,Name>
---- Create date: <Create Date,,>
---- Description:	<Description,,>
---- =============================================
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
--added modification history on 10/02/2014 
-- CV        10/02/2014   changed Current to Regular Qty 402,403 instead of 396,397
-- GHS		 09/25/2015	  Added logic to not calculate Corp SSP pricing from Type 1 PD records when the PD record is not tied to the CORP Whse. Ln 155, 165

CREATE PROCEDURE [custom].[SP_sProdTransGeneration] (@In_JobFK_int bigint)
AS
BEGIN

	SET NOCOUNT ON;

Declare @SP_Out_Job_FK bigint
declare @SP_Start_Time DateTime
declare @SP_End_Time DateTime
declare @SP_Current_time datetime
declare @In_JobFK varchar(16)
declare @job_ex_id bigint

DECLARE @ls_exec            nvarchar (max);
DECLARE @l_exec_no          bigint;

DECLARE @status_desc        varchar(max);

begin try
set @SP_Start_Time = getdate()

set @SP_Out_Job_FK = @In_JobFK_int   ------ job from User or UI schedule
set @In_JobFK =  cast (@In_JobFK_int as varchar(16))

------------------------logging from UI on export--------------------------------------------------------------------
Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'START PROCESSING', null, null, null, 200, @SP_Start_Time, null

----------------------------------------------------------------------------------------------------------------------------

SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of custom.SP_sProdTransGeneration', 
		@exec_id = @l_exec_no OUTPUT,
		@epa_job_id = @SP_Out_Job_FK;

SET @status_desc =  'Execution started for custom.SP_sProdTransGeneration - Job ' + @In_JobFK
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

---------------------------------------CREATE CALC JOB-------------------------------------------------------
Declare @Out_Job_FK Bigint
Declare @Job_FK Bigint
Declare @Job_FK_vc Varchar(16)

Exec [common].[JOB_CREATE] Null, 481, 'Johnstone STrans Calcs', Null, Null, Null, Null, @Out_Job_FK Output

Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'START CALC JOB', null, null, null, 200, @SP_Start_Time, null

set @job_ex_id = (select top 1 job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name = 'START CALC JOB')


Set @Job_FK = @SP_Out_Job_FK
Set @Job_FK_vc = Cast(@IN_JobFK as varchar(16))
	
    
    IF OBJECT_ID('tempdb..##sTransProdList_SPT') IS NOT NULL
		DROP TABLE ##sTransProdList_SPT

	IF OBJECT_ID('tempdb..##CustomerWarehouse_SPT') IS NOT NULL
		DROP TABLE ##CustomerWarehouse_SPT

	IF OBJECT_ID('tempdb..##sTransTable_SPT') IS NOT NULL
		Drop table ##sTransTable_SPT

	SET @status_desc =  'sProdTransGeneration Ln 104 - Deleting Previous ProdTrans Records'
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

	delete from custom.JS_sTran_History where ISNULL(sprodtransflag, 0) = 1
	
	SET @status_desc =  'sProdTransGeneration Ln 106 - Getting Stores by Whse'
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

	--get stores by warehouse  --create a temp of warehouse for cross join
	select ei.entity_structure_fk 'cust_entity_structure_fk', ea_3c.ATTRIBUTE_CHAR 'Store', ei_o.entity_structure_fk 'org_entity_structure_fk', ea_o.ATTRIBUTE_EVENT_DATA 'DC', ea_t.ATTRIBUTE_EVENT_DATA 'Transmit'  
	into ##CustomerWarehouse_SPT
	from epacube.entity_identification ei with (nolock)
	inner join epacube.entity_attribute ea_o with (nolock)  on ei.ENTITY_STRUCTURE_FK = ea_o.ENTITY_STRUCTURE_FK and ea_o.DATA_NAME_FK = 144900
	inner join epacube.entity_identification ei_o with (nolock) on ea_o.attribute_event_data = ei_o.value and ei_o.data_name_fk = 141111 and ei_o.ENTITY_DATA_NAME_FK = 141000
	left join epacube.entity_attribute ea_t with (nolock)  on ei.ENTITY_STRUCTURE_FK = ea_T.ENTITY_STRUCTURE_FK and ea_T.DATA_NAME_FK = 244834
	left join epacube.entity_attribute ea_3c with (nolock) on ei.entity_structure_fk = ea_3c.entity_structure_fk and ea_3c.data_name_fk = 244835
	where ei.ENTITY_DATA_NAME_FK in (144010, 144020) and ea_t.ATTRIBUTE_EVENT_DATA is not null
	Union
	select ea_3c.entity_structure_fk 'cust_entity_structure_fk', ea_3c.ATTRIBUTE_CHAR 'Store'
	, ei.entity_structure_fk 'org_entity_structure_fk', 'CORP' 'DC', '1' 'Transmit' 
	from epacube.ENTITY_IDENTIFICATION ei with (nolock) 
	cross join epacube.entity_attribute ea_t with (nolock)   
	left join epacube.entity_attribute ea_3c with (nolock) on ea_t.entity_structure_fk = ea_3c.entity_structure_fk and ea_3c.data_name_fk = 244835
	where ea_t.ATTRIBUTE_EVENT_DATA is not null
	and ei.entity_structure_fk = 11794 and ea_T.DATA_NAME_FK = 244834	

	Create Index idx_cw on ##CustomerWarehouse_SPT(cust_entity_structure_fk, org_entity_structure_fk, Store)

	set @SP_Current_time = getdate()
	update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id
	Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'IDENTIFICATION OF PRODUCTS', null, null, null, 200, @SP_Current_time, null

	set @job_ex_id = (select top 1 job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name = 'IDENTIFICATION OF PRODUCTS')

	Create Table ##sTransProdList_SPT(Product_ID [Varchar](64) Null, Product_Structure_FK Bigint Null, cust_entity_structure_fk Bigint Null, Store [Varchar](8) Null, Org_entity_structure_fk Bigint Null, DC [Varchar](8) Null, Transmit [Int] Null, Job_FK Bigint Null, Rule_Type [Varchar](36) Null, SPT_ID [bigint] Identity(1, 1) Not Null)
	Create Index idx_id on ##sTransProdList_SPT(SPT_ID)
	Create index idx_prod on ##sTransProdList_SPT(PRODUCT_STRUCTURE_FK, cust_entity_structure_fk, org_entity_structure_fk, Product_ID, store)
	Create index idx_rt on ##sTransProdList_SPT(Rule_Type)

------Find all Transactions to Calculate

	SET @status_desc =  'sProdTransGeneration Ln 173 - Get Required Calcs Start'
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

	--All Type 1
	Insert into ##sTransProdList_SPT(Product_ID, Product_Structure_FK, cust_entity_structure_fk, Store, Org_entity_structure_fk, DC, Transmit, Rule_Type)
	Select distinct pi.value 'Product ID'
		, pi.product_structure_fk
		, ea_3c.entity_structure_fk cust_entity_structure_fk
		, ea_3c.attribute_event_data Store
		, cw.org_entity_structure_fk
		, cw.DC
		, ea_t.attribute_event_data Transmit
		, '1' Rule_Type
	from synchronizer.rules r with (nolock)
	inner join synchronizer.rules_filter_set rfs with (nolock) on r.rules_id = rfs.rules_fk
	inner join synchronizer.rules_filter rf_p with (nolock) on rfs.PROD_FILTER_FK = rf_p.rules_filter_id
	left join synchronizer.RULES_FILTER rf_o with (nolock) on rfs.ORG_FILTER_FK = rf_o.RULES_FILTER_ID
	inner join epacube.product_identification pi with (nolock) on rf_p.value1 = pi.value and pi.data_name_fk = 110100
	inner join synchronizer.rules_filter rf_c with (nolock) on rfs.cust_filter_fk = rf_c.rules_filter_id
	inner join epacube.entity_identification ei with (nolock) on rf_c.value1 = ei.value and rf_c.data_name_fk = ei.DATA_NAME_FK
	inner join ##CustomerWarehouse_SPT cw on ei.entity_structure_fk = cw.cust_entity_structure_fk
	inner join epacube.entity_attribute ea_t with (nolock)  on ei.ENTITY_STRUCTURE_FK = ea_T.ENTITY_STRUCTURE_FK and ea_T.DATA_NAME_FK = 244834
	inner join epacube.entity_attribute ea_3c with (nolock) on ei.entity_structure_fk = ea_3c.entity_structure_fk and ea_3c.data_name_fk = 244835
	inner join epacube.product_attribute pTransmit on pi.product_structure_fk = pTransmit.product_structure_fk and pTransmit.data_name_fk = 100100358
	where r.result_data_name_fk = 111602
	and r.Host_Precedence_Level = '1'
	and (rf_o.VALUE1 = cw.DC or (cw.DC <> 'Corp' and rf_o.VALUE1 is null))
	and isnull(r.end_date, getdate() + 1) > getdate()
	and ea_t.ATTRIBUTE_EVENT_DATA = 1
	and pTransmit.attribute_y_n = 'Y'
	and r.record_status_cr_fk = 1


	--All Type 3
	Insert into ##sTransProdList_SPT(Product_ID, Product_Structure_FK, cust_entity_structure_fk, Store, Org_entity_structure_fk, DC, Transmit, Rule_Type)
	Select distinct pi.value 'Product ID'
		, pi.product_structure_fk
		, ea_3c.entity_structure_fk cust_entity_structure_fk
		, ea_3c.attribute_event_data Store
		, cw.org_entity_structure_fk
		, cw.DC
		, ea_t.attribute_event_data Transmit
		, r.host_precedence_level Rule_Type
	from synchronizer.rules r with (nolock)
	inner join synchronizer.rules_filter_set rfs with (nolock) on r.rules_id = rfs.rules_fk
	inner join synchronizer.rules_filter rf_p with (nolock) on rfs.PROD_FILTER_FK = rf_p.rules_filter_id
	inner join epacube.product_identification pi with (nolock) on rf_p.value1 = pi.value and pi.data_name_fk = 110100
	inner join synchronizer.rules_filter rf_c with (nolock) on rfs.cust_filter_fk = rf_c.rules_filter_id
	inner join epacube.data_value dv with (nolock) on rf_c.data_name_fk = dv.data_name_fk and rf_c.value1 = dv.value
	inner join epacube.entity_mult_type emt with (nolock) on dv.data_value_id = emt.DATA_VALUE_FK
	inner join epacube.entity_identification ei with (nolock) on emt.entity_structure_fk = ei.entity_structure_fk and ei.entity_data_name_fk in (144010, 144020)
	inner join ##CustomerWarehouse_SPT cw on ei.entity_structure_fk = cw.cust_entity_structure_fk
	inner join epacube.entity_attribute ea_t with (nolock)  on ei.ENTITY_STRUCTURE_FK = ea_T.ENTITY_STRUCTURE_FK and ea_T.DATA_NAME_FK = 244834
	inner join epacube.entity_attribute ea_3c with (nolock) on ei.entity_structure_fk = ea_3c.entity_structure_fk and ea_3c.data_name_fk = 244835
	inner join epacube.product_attribute pTransmit on pi.product_structure_fk = pTransmit.product_structure_fk and pTransmit.data_name_fk = 100100358
	where r.result_data_name_fk = 111602
	and r.Host_Precedence_Level = '3'
	and isnull(r.end_date, getdate() + 1) > getdate()
	and ea_t.ATTRIBUTE_EVENT_DATA = 1
	and pTransmit.attribute_y_n = 'Y'
	and r.record_status_cr_fk = 1


	--All Type 4
	Insert into ##sTransProdList_SPT(Product_ID, Product_Structure_FK, cust_entity_structure_fk, Store, Org_entity_structure_fk, DC, Transmit, Rule_Type)
	Select distinct pi.value 'Product ID'
		, pi.product_structure_fk
		, ea_3c.entity_structure_fk cust_entity_structure_fk
		, ea_3c.attribute_event_data Store
		, cw.org_entity_structure_fk
		, cw.DC
		, ea_t.attribute_event_data Transmit
		, r.host_precedence_level Rule_Type
	from synchronizer.rules r with (nolock)
	inner join synchronizer.rules_filter_set rfs with (nolock) on r.rules_id = rfs.rules_fk
	inner join synchronizer.rules_filter rf_p with (nolock) on rfs.PROD_FILTER_FK = rf_p.rules_filter_id
	inner join epacube.data_value dv_p with (nolock) on rf_p.DATA_NAME_FK = dv_p.DATA_NAME_FK and rf_p.value1 = dv_p.value
	inner join epacube.product_category pc with (nolock) on dv_p.data_value_id = pc.DATA_VALUE_FK 
	inner join epacube.product_identification pi with (nolock) on pc.product_structure_fk = pi.PRODUCT_STRUCTURE_FK and pi.data_name_fk = 110100
	inner join synchronizer.rules_filter rf_c with (nolock) on rfs.cust_filter_fk = rf_c.rules_filter_id
	inner join epacube.data_value dv with (nolock) on rf_c.data_name_fk = dv.data_name_fk and rf_c.value1 = dv.value
	inner join epacube.entity_mult_type emt with (nolock) on dv.data_value_id = emt.DATA_VALUE_FK
	inner join epacube.entity_identification ei with (nolock) on emt.entity_structure_fk = ei.entity_structure_fk and ei.entity_data_name_fk in (144010, 144020)
	inner join ##CustomerWarehouse_SPT cw on ei.entity_structure_fk = cw.cust_entity_structure_fk and (pc.ORG_ENTITY_STRUCTURE_FK = cw.org_entity_structure_fk or cw.DC = 'Corp')
	inner join epacube.entity_attribute ea_t with (nolock)  on ei.ENTITY_STRUCTURE_FK = ea_T.ENTITY_STRUCTURE_FK and ea_T.DATA_NAME_FK = 244834
	inner join epacube.entity_attribute ea_3c with (nolock) on ei.entity_structure_fk = ea_3c.entity_structure_fk and ea_3c.data_name_fk = 244835
	inner join epacube.product_attribute pTransmit on pi.product_structure_fk = pTransmit.product_structure_fk and pTransmit.data_name_fk = 100100358
	where r.result_data_name_fk = 111602
	and r.Host_Precedence_Level = '4'
	and isnull(r.end_date, getdate() + 1) > getdate()
	and ea_t.ATTRIBUTE_EVENT_DATA = 1
	and pTransmit.attribute_y_n = 'Y'
	and r.record_status_cr_fk = 1

	--Transactions from all other Rule Types
	Insert into ##sTransProdList_SPT(Product_ID, Product_Structure_FK, cust_entity_structure_fk, Store, Org_entity_structure_fk, DC, Transmit, Rule_Type)
	Select Distinct [Product ID], product_structure_fk, cw.cust_entity_structure_fk, cw.Store, cw.org_entity_structure_fk, cw.DC, 1, 'Other' Rule_Type from (
	Select pi.value 'Product ID'
		, pi.product_structure_fk
		--, r.host_precedence_level Rule_Type
	from synchronizer.rules r with (nolock)
	inner join synchronizer.rules_filter_set rfs with (nolock) on r.rules_id = rfs.rules_fk
	inner join synchronizer.rules_filter rf_p with (nolock) on rfs.PROD_FILTER_FK = rf_p.rules_filter_id
	inner join epacube.product_identification pi with (nolock) on rf_p.value1 = pi.value and pi.data_name_fk = 110100
	inner join epacube.product_attribute pTransmit on pi.product_structure_fk = pTransmit.product_structure_fk and pTransmit.data_name_fk = 100100358
	where r.result_data_name_fk = 111602
	--and r.Host_Precedence_Level not in ('1', '3', '4')
	and isnull(r.end_date, getdate() + 1) > getdate()
	and pTransmit.attribute_y_n = 'Y'
	and r.record_status_cr_fk = 1
	Union
	Select pi.value 'Product ID'
		, pi.product_structure_fk
		--, r.host_precedence_level Rule_Type
	from synchronizer.rules r with (nolock)
	inner join synchronizer.rules_filter_set rfs with (nolock) on r.rules_id = rfs.rules_fk
	inner join synchronizer.rules_filter rf_p with (nolock) on rfs.PROD_FILTER_FK = rf_p.rules_filter_id
	inner join epacube.data_value dv_p with (nolock) on rf_p.DATA_NAME_FK = dv_p.DATA_NAME_FK and rf_p.value1 = dv_p.value
	inner join epacube.product_category pc with (nolock) on dv_p.data_value_id = pc.DATA_VALUE_FK 
	inner join epacube.product_identification pi with (nolock) on pc.product_structure_fk = pi.PRODUCT_STRUCTURE_FK and pi.data_name_fk = 110100
	inner join epacube.product_attribute pTransmit on pi.product_structure_fk = pTransmit.product_structure_fk and pTransmit.data_name_fk = 100100358
	where r.result_data_name_fk = 111602
	--and r.Host_Precedence_Level not in ('1', '3', '4')
	and isnull(r.end_date, getdate() + 1) > getdate()
	and pTransmit.attribute_y_n = 'Y'
	and r.record_status_cr_fk = 1
	) A
	Cross Join ##CustomerWarehouse_SPT CW

	SET @status_desc =  'sProdTransGeneration Ln 135 - Get Required Calcs Finish - ' + Cast((Select count(*) from ##sTransProdList_SPT) as varchar(16))
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

	Update ##sTransProdList_SPT Set Job_FK = @Job_fk

	Select Distinct SPT.Product_ID, SPT.Product_Structure_FK, SPT.cust_entity_structure_fk, SPT.Store, SPT.Org_entity_structure_fk, SPT.DC, SPT.Transmit
		into ##sTransProdList_SPT2 
		from ##sTransProdList_SPT SPT 
		inner join epacube.product_attribute pTransmit on SPT.product_structure_fk = pTransmit.product_structure_fk and pTransmit.data_name_fk = 100100358 and pTransmit.attribute_y_n = 'Y'
	Truncate Table ##sTransProdList_SPT
	Insert into ##sTransProdList_SPT (Product_ID, Product_Structure_FK, cust_entity_structure_fk, Store, Org_entity_structure_fk, DC, Transmit)
	Select SPT.Product_ID, SPT.Product_Structure_FK, SPT.cust_entity_structure_fk, SPT.Store, SPT.Org_entity_structure_fk, SPT.DC, SPT.Transmit from ##sTransProdList_SPT2 SPT
	drop table ##sTransProdList_SPT2

	IF OBJECT_ID('AAA_sTransProdList_Calcs_B') IS NOT NULL
		Drop table AAA_sTransProdList_Calcs_B

	Select * into dbo.AAA_sTransProdList_Calcs_B from ##sTransProdList_SPT

	set @SP_Current_time = getdate()
	update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id
	Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'RUN CALC JOB ON PRODUCTS', null, null, null, 200, @SP_Current_time, null

	set @job_ex_id = (select top 1 job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name = 'RUN CALC JOB ON PRODUCTS')

	--run calc job for all the products in ##sTransProdList_SPT

	SET @status_desc =  'sProdTransGeneration Ln 286 - Calc ' + Cast((Select count(*) from ##sTransProdList_SPT) as varchar(16)) + ' Records'
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

	Declare @Eff_Date date
	Declare @Result_Type_CR_FK Int
	Declare @COST_CHANGE_PRIOR_IND Int
	Declare @COST_CHANGE_FIRST_IND int

	Set @Eff_Date = Cast(getdate() + 1 as date)
	Set @Result_Type_CR_FK = Case when ISNULL(@Eff_Date, getdate()) > getdate() then 711 else 710 end

	Set @COST_CHANGE_FIRST_IND = Case When @Result_Type_CR_FK = 711 and @Eff_Date is Null then 1 else 0 end
	Set @COST_CHANGE_PRIOR_IND = Case When @Result_Type_CR_FK = 711 and @Eff_Date is not Null then 1 else 0 end

	Insert into epacube.marginmgr.ANALYSIS_JOB
		(Analysis_Job_ID, Analysis_Name, result_type_cr_fk, result_effective_date, rule_type_filter, Inner_Join_Filter, Product_Filter, Organization_Filter, Customer_Filter, RECORD_STATUS_CR_FK)
		Select 
		@Job_FK
		, 'Johnstone STrans Daily Process'
		, @Result_Type_CR_FK
		, @Eff_Date
		, '(313)'
		, 'Inner Join ##sTransProdList_SPT ST on PS.Product_Structure_ID = ST.Product_Structure_FK 
				And ESC.Entity_Structure_ID = ST.Cust_Entity_Structure_FK 
				And ESO.entity_Structure_ID = ST.org_entity_structure_fk'
		, '(Select distinct Product_Structure_FK from ##sTransProdList_SPT)'
		, '(Select distinct org_entity_structure_fk from ##sTransProdList_SPT)'
		, '(Select distinct cust_entity_structure_fk from ##sTransProdList_SPT)'
		, 1

SET @status_desc =  'Job ' + @Job_FK_vc + ' JS sTrans Calc execution started for custom.SP_sTransGeneration - Job ' + @In_JobFK
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

EXEC  [marginmgr].[CALCULATION_PROCESS] @Job_FK

SET @status_desc =  'Job ' + @Job_FK_vc + ' JS sTrans Calc execution finished for custom.SP_sTransGeneration - Job ' + @In_JobFK
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

set @SP_Current_time = getdate()
	update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id
	Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'INSERT NON-PD PRODUCTS', null, null, null, 200, @SP_Current_time, null

	set @job_ex_id = (select top 1 job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name = 'INSERT NON-PD PRODUCTS')

--insert the product/warehouse/customer combinations that are not driven by PD records

	SET @status_desc =  'sProdTransGeneration Ln 334 - Add No Calc Records Start'
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

	Delete from ##CustomerWarehouse_SPT where org_entity_structure_fk = (select entity_structure_fk from epacube.ENTITY_IDENTIFICATION where value = 'corp' and entity_data_name_fk = 141000 and data_name_fk = 141111)
	
	insert into ##sTransProdList_SPT (Product_ID, PRODUCT_STRUCTURE_FK, cust_entity_structure_fk, store, org_entity_structure_fk, dc, Transmit, Job_FK) 
	select distinct pid.VALUE, pid.PRODUCT_STRUCTURE_FK, cw.cust_entity_structure_fk, cw.store, cw.org_entity_structure_fk, cw.dc, cw.Transmit, @Job_FK 
	from epacube.PRODUCT_IDENTIFICATION pid
	inner join epacube.product_attribute pTransmit on pid.product_structure_fk = pTransmit.product_structure_fk and pTransmit.data_name_fk = 100100358
	cross join ##CustomerWarehouse_SPT cw
	left join ##sTransProdList_SPT pl on pid.PRODUCT_STRUCTURE_FK = pl.PRODUCT_STRUCTURE_FK and cw.ORG_ENTITY_STRUCTURE_FK = pl.ORG_ENTITY_STRUCTURE_FK and CW.CUST_ENTITY_STRUCTURE_FK = pl.CUST_ENTITY_STRUCTURE_FK
	where pid.data_name_fk = 110100 and pl.store is null and pTransmit.attribute_y_n = 'Y' 
	
	Delete SPT from ##sTransProdList_SPT SPT
	Where SPT.org_entity_structure_fk = (select entity_structure_fk from epacube.ENTITY_IDENTIFICATION where value = 'corp' and entity_data_name_fk = 141000 and data_name_fk = 141111)

	SET @status_desc =  'sProdTransGeneration Ln 348 - Add No Calc Records Finish'
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

	set @SP_Current_time = getdate()
	update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id
	Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'RESULTS TO JS_sTran_History', null, null, null, 200, @SP_Current_time, null

	set @job_ex_id = (select top 1 job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name = 'RESULTS TO JS_sTran_History')

	Update SPT
	Set Job_FK = @Job_FK
	from ##sTransProdList_SPT SPT where Job_FK is null

	Select top 1 * into ##sTransTable_SPT from custom.JS_sTran_History where 1 = 2

IF object_id('tempdb..#TransIDs') is not null
drop table #TransIDs;

Declare @MaxID bigint
Declare @Pass int
Declare @TotalRecs Varchar(18)

Set @TotalRecs = (Select count(*) FROM ##sTransProdList_SPT SPT
	inner join epacube.product_attribute pTransmit with (nolock) on SPT.product_structure_fk = pTransmit.product_structure_fk and pTransmit.data_name_fk = 100100358
	WHERE 1 = 1
	and isnull(pTransmit.attribute_y_n, '') = 'Y')
	
	SET @status_desc =  'sProdTransGeneration Ln 372 - Load Temp Table Start: ' + @TotalRecs + ' Records'
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

Create Table #TransIDs(SPT_ID Bigint Not Null)
Create Index idx_id on #TransIDs(SPT_ID)

Insert into #TransIDs
SELECT top 5000000 SPT_ID
    FROM ##sTransProdList_SPT SPT
	inner join epacube.product_attribute pTransmit with (nolock) on SPT.product_structure_fk = pTransmit.product_structure_fk and pTransmit.data_name_fk = 100100358
	WHERE 1 = 1
	and isnull(pTransmit.attribute_y_n, '') = 'Y'  
	Order by SPT_ID
	
Set @Pass = 1

While (Select COUNT(*) from #TransIDs) > 0
	Begin
		Set @MaxID = (Select Max(SPT_ID) from #TransIDs)

--query to get all the data required for the sTrans to be inserted into sTransHistory table
insert into ##sTransTable_SPT ( [Product_Structure_FK],[job_fk], [calc_job_fk],[ORG_ENTITY_STRUCTURE_FK],[cust_entity_structure_fk],[Store],[DC],[Store Transmit Flag],[RESULT],PD_Effective_Date
,[Product Transmit Flag],[Promo Page #],[Product Number],[Changes Flag],[Lot Pricer Flag],[Regular Base from DC],[AOC Vendor],[Selling Price Each],[Promo EU Ea],[Lot 1 Qty],[Lot 1 Sell Price]
,[Lot 2 Qty],[Lot 2 Sell Price],[Product Description],[Standard Pack],[Catalog Page Number],[Lot A QTY],[Lot A Sell Price],[Lot B QTY],[Lot B Sell Price],[CORP Base Price],[CORP DropShipQty]
,[Weight],[Season Code],[Customer Description],[Dept-Cat],[MSDS Indicator],[Memo Field],[Hazardous Material Ship Code],[Discontinued Flag],[Flyer Month Code],[Buyers Code],[Freight Code]
,[Vendor Bar Code 1],[Reg Base from DC Eff Date],[Direct Cost Effective Date],[End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Manufacturer Part Number]
,[Freight Code #2],[Append Date],[QtyBrk#2],[QtyBrk$1],[QtyBrk$2],[Serialized Product Flag],[Certification Required Flag],[Taxable Flag],[End User Regular Price],[List Each],[List Lot 1]
,[List Lot 2],[WEB_NODE_ID],[Web Node Data],[EDI Part No],[Vendor No],[Country of Origin],[Return Flag],[Regular DC Cost],[DC Sell Qty],[Rules_FK], [Basis Calc],[Host Precedence Level], IsPromoFlag, [Corp SSP], [Store SSP], sProdTransFlag)

Select pl.product_structure_fk,@SP_Out_Job_FK , pl.job_fk, pl.org_entity_structure_fk, pl.cust_entity_structure_fk, pl.store, pl.DC, pl.[Transmit],ec.result, ec.RESULT_EFFECTIVE_DATE,[Transmit Flag]
, [Promo Page #],[JS Prod ID],case when [Promo Page #] is not null then 'C' else '*' end as [Changes Flag],
Case when cast([Lot A Prc] as decimal(18,5))  > 0.00000  then '*'  else null end as [Lot Pricer Flag]
, [Regular Base Price],[AOC Vend #],[CEU Ea Prc], [Promo End User Each], [Regular End User Qty 1], [CEU 1 Prc], [Regular End User Qty 2], [CEU 2 Prc], [Descr 1],[Vend Std Pk],[Catalog Page]
,[Lot QTY A], [Lot A prc], [Lot QTY B], [Lot B Prc], [CORP Base Price], [CORP DS 1 QTY],[Wt],[Seasonal CD], [USER Desc], [Store PCAT], [MSDS Sht #], [Memo], [z_icsp.u_hazcode], [Discontinued Ind]
, [On Flyers], [dept]
,[Frt Cd 1]
, UPC, [RegBase_EffDate], [CORP Base Price Effective Date], [End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag]
,[Vend Prod],[Frt Cd 2], cast(getDate() as date), FILTER_VALUE1, OPERAND1, OPERAND2,[Serialized Product Flag],[Cert Cd],[Taxable Tool Flag], [REU Ea Prc], [List Each]
, [List Lot Price 1],[List Lot Price 2],[WEB_NODE_ID],[Web Node Desc],[EDI Part No], right([AOC VEND #],Len([AOC VEND #]) - 1),[Country of Origin], [Return Flag], [Regular Base Price], [Round By], rfs.rules_fk,
 case when rao.RULES_ACTION_OPERANDS_ID is not null then ecr.BASIS_CALC else null end as [Basis Calc],
 r.host_precedence_level, r.Promo_Ind,
 ec_corp.result Corp_Store_Specific_Sell_Price, ec.result Store_Specific_Sell_Price
 , 1
from ##sTransProdList_SPT pl 
Inner Join #TransIDs TID on pl.SPT_ID = TID.SPT_ID
inner join custom.V_AllPrices ap on ap.PRODUCT_STRUCTURE_FK = pl.PRODUCT_STRUCTURE_FK and ap.ORG_ENTITY_STRUCTURE_FK = pl.org_entity_structure_fk
left outer join 
(
select Product_STructure_fk, Org_entity_structure_fk ,[Lot Qty A],[Lot Qty B],[Regular End User Qty 1],[Regular End User Qty 2]
		 FROM
(
		select PRODUCT_STRUCTURE_FK as Product_Structure_FK, Org_entity_structure_fk, dn.label as DataName, ATTRIBUTE_EVENT_DATA as DataValue 
		FROM epacube.PRODUCT_ATTRIBUTE pa 
		INNER JOIN epacube.DATA_NAME dn on pa.DATA_NAME_FK = dn.DATA_NAME_ID 
		where DATA_NAME_FK in (100100410,100100411,100100402,100100403)    ---,100100396,100100397  removed oct 1, 2014

		) As P
		PIVOT (
		Max(P.DataValue) FOR DataName IN ([Lot Qty A],[Lot Qty B],[Regular End User Qty 1],[Regular End User Qty 2])
		) 
		As PivotTable
) w_qty on w_qty.PRODUCT_STRUCTURE_FK = pl.PRODUCT_STRUCTURE_FK and w_qty.ORG_ENTITY_STRUCTURE_FK = pl.org_entity_structure_fk

left outer join (select Product_STructure_fk , [JS Prod ID], [UPC],[Vend Prod],[Promo Page #],[AOC Vend #],[REU 1 QTY],
		[REU 2 QTY],[Descr 1],[Vend Std Pk],[Catalog Page],Round([Wt], 2) as [Wt],[Seasonal CD],[User Desc],[Store PCAT],[MSDS Sht #],[Memo],[z_icsp.u_hazcode],
		[Discontinued Ind],[On Flyers],[Dept],[Frt Cd 1],[End User Each Published Flag],
		[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Frt Cd 2],[Serialized Product Flag],[Cert Cd],[Taxable Tool Flag],[WEB_NODE_ID],
		[Web Node Desc],[EDI Part No],[Country of Origin],[Return Flag],[Round By],[Transmit Flag],[CORP Base Price Effective Date],[CORP Base Price],[CORP DS 1 QTY]
		 FROM
(select distinct pid.Product_structure_FK as Product_Structure_FK, dn.short_name as Dataname, value as DataValue 
		from epacube.PRODUCT_IDENTIFICATION pid 
		inner join epacube.DATA_NAME dn on pid.DATA_NAME_FK = dn.DATA_NAME_ID 
		where pid.DATA_NAME_FK = 110100 
		UNION select pd.product_structure_fk, dn.short_name, pd.description from epacube.PRODUCT_DESCRIPTION pd
		inner join epacube.data_name dn on pd.data_name_fk = dn.DATA_NAME_ID
		where pd.DATA_NAME_FK in (110401,100100360)
		UNION select pid.Product_structure_FK, dn.short_name, value 
		from epacube.PRODUCT_IDENTIFICATION pid inner join epacube.DATA_NAME dn on pid.DATA_NAME_FK = dn.DATA_NAME_ID 
		WHERE pid.DATA_NAME_FK in (110111)
		UNION select pin.Product_structure_FK, dn.short_name, value 
		from epacube.PRODUCT_IDENT_NONUNIQUE pin inner join epacube.DATA_NAME dn on pin.DATA_NAME_FK = dn.DATA_NAME_ID 
		WHERE pin.DATA_NAME_FK in (110102,100100197)
		UNION
		SELECT pc.product_structure_Fk, dn.short_name, dv.value from epacube.PRODUCT_CATEGORY pc
		inner join epacube.DATA_VALUE dv on pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
		inner join epacube.data_name dn on pc.data_name_fk = dn.DATA_NAME_ID
		where pc.DATA_NAME_FK in (100100415,100100161,100100100)
		--change to get from new allprices
		UNION select PRODUCT_STRUCTURE_FK, 'CORP Base Price', cast(NET_VALUE3 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100416 AND ORG_ENTITY_STRUCTURE_FK = 11794--DN100100417
		UNION select PRODUCT_STRUCTURE_FK, 'CORP Base Price Effective Date', cast(cast(effective_date as date) as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE where DATA_NAME_FK = 100100416 AND ORG_ENTITY_STRUCTURE_FK = 11794--DN100100417
		UNION
		select PRODUCT_STRUCTURE_FK as ProductKey, 'CORP DS 1 QTY', ATTRIBUTE_EVENT_DATA  
		FROM epacube.PRODUCT_ATTRIBUTE pa 
		where DATA_NAME_FK in (100100296) and ORG_ENTITY_STRUCTURE_FK = 11794
		UNION
		SELECT pc.product_structure_fk, 'Web Node Desc', dv.DESCRIPTION from epacube.PRODUCT_CATEGORY pc
		inner join epacube.DATA_VALUE dv on pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
		inner join epacube.data_name dn on pc.data_name_fk = dn.DATA_NAME_ID
		where pc.DATA_NAME_FK in (100100100)
		UNION
		select PRODUCT_STRUCTURE_FK as ProductKey, dn.short_name as DataName, ATTRIBUTE_EVENT_DATA as DataValue 
		FROM epacube.PRODUCT_ATTRIBUTE pa 
		INNER JOIN epacube.DATA_NAME dn on pa.DATA_NAME_FK = dn.DATA_NAME_ID 
		where DATA_NAME_FK in (100100287,100100110,100100389,100100154,110815,100100342,110817,100100272,100100376,
		100100163,100100374,100100219,100100220,100100221,100100375,100100343,100100414,100100356,100100158,100100341,210831,100100358)
		UNION
		SELECT PRODUCT_STRUCTURE_FK, 'On Flyers', 
		SUBSTRING( (  SELECT ( ' ' + dv.VALUE)  from epacube.PRODUCT_MULT_TYPE t2
		inner join epacube.DATA_VALUE dv on t2.DATA_VALUE_FK = dv.DATA_VALUE_ID 
		WHERE t1.PRODUCT_STRUCTURE_FK= t2.PRODUCT_STRUCTURE_FK and t2.DATA_NAME_FK = 100100276
		ORDER BY t1.PRODUCT_STRUCTURE_FK, t2.PRODUCT_STRUCTURE_FK 
		FOR XML PATH('') ), 2, 1000) 
		from epacube.PRODUCT_MULT_TYPE t1
		inner join epacube.DATA_VALUE dv on t1.DATA_VALUE_FK = dv.DATA_VALUE_ID 
		GROUP BY PRODUCT_STRUCTURE_FK 

		) As P
		PIVOT (
		Max(P.DataValue) FOR DataName IN ([JS Prod ID], [UPC],[Vend Prod],[Promo Page #],[AOC Vend #],[REU 1 QTY],
		[REU 2 QTY],[Descr 1],[Vend Std Pk],[Catalog Page],[Wt],[Seasonal CD],[User Desc],[Store PCAT],[MSDS Sht #],[Memo],[z_icsp.u_hazcode],
		[Discontinued Ind],[On Flyers],[Dept],[Frt Cd 1],[End User Each Published Flag],
		[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Frt Cd 2],[Serialized Product Flag],[Cert Cd],[Taxable Tool Flag],[WEB_NODE_ID],
		[Web Node Desc],[EDI Part No],[Country of Origin],[Return Flag],[Round By],[Transmit Flag],[CORP Base Price],[CORP DS 1 QTY],
		[CORP Base Price Effective Date])
		) 
		As PivotTable)
		pt on pl.product_structure_fk = pt.Product_Structure_FK

	left outer join synchronizer.event_calc ec with (nolock) on pl.product_structure_fk = ec.product_structure_fk and pl.org_entity_structure_fk = ec.org_entity_structure_fk 
					and pl.cust_entity_structure_fk = ec.cust_entity_structure_fk and ec.RESULT_DATA_NAME_FK = 111602 and ec.JOB_FK = @Job_FK	
	left outer join synchronizer.event_calc ec_corp with (nolock) on pl.product_structure_fk = ec_corp.product_structure_fk and ec_corp.org_entity_structure_fk = 11794
					and pl.cust_entity_structure_fk = ec_corp.cust_entity_structure_fk and ec_corp.RESULT_DATA_NAME_FK = 111602 and ec_corp.JOB_FK = @Job_FK	
						 
	left join synchronizer.rules_action ra with (nolock) on ec.rules_fk = ra.rules_fk
	left join synchronizer.rules_action_operands rao with (nolock) on ra.rules_action_id = rao.rules_action_fk and rao.operand_filter_dn_fk = 107000
	left join synchronizer.rules_filter_set rfs with (nolock) on ec.rules_fk = rfs.rules_fk
	left join synchronizer.rules_filter rf_o with (nolock) on rfs.ORG_FILTER_FK = rf_o.rules_filter_id
	left join synchronizer.rules r with (nolock) on ec.rules_fk = r.RULES_ID 
	left join synchronizer.event_calc_rules ecr with (nolock) on ec.EVENT_ID = ecr.EVENT_FK and ecr.result_rank = 1
	where pl.job_fk = @Job_FK and pl.org_entity_structure_fk <> 11794

	SET @status_desc = 'Load ##sTransTable_SPT Pass ' + Cast(@Pass as varchar(16)) + ' - ' + CAST((Select count(*) from #TransIDs) as varchar(36)) + ' of ' + @TotalRecs + ' Records'
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;		

	Truncate table #TransIDs;
	Drop index idx_id on #TransIDs
		
	Insert into #TransIDs
	SELECT top 5000000 SPT_ID
		FROM ##sTransProdList_SPT SPT
		inner join epacube.product_attribute pTransmit with (nolock) on SPT.product_structure_fk = pTransmit.product_structure_fk and pTransmit.data_name_fk = 100100358
		WHERE 1 = 1
		and isnull(pTransmit.attribute_y_n, '') = 'Y'
		and SPT_ID > @MaxID
		Order by SPT_ID
			
	Create Index idx_id on #TransIDs(SPT_ID)
		
	Set @Pass = @Pass + 1
		
	If (Select COUNT(*) from #TransIDs) = 0
		Break
	else
		Continue
End

	IF OBJECT_ID('tempdb..#TransIDs') IS NOT NULL
	Drop table #TransIDs

	Create index idx_stt on ##sTransTable_SPT([JS_STRAN_ID], [Product_Structure_FK], [ORG_ENTITY_STRUCTURE_FK], [cust_entity_structure_fk])

	SET @status_desc =  'sProdTransGeneration Ln 531 - Load Temp Table Finish'
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

	SET @status_desc =  'sProdTransGeneration Ln 534 - Transfer Recs to JS_sTran Start'
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

	insert into custom.JS_sTran_History 
	( [Product_Structure_FK],[job_fk], [calc_job_fk],[ORG_ENTITY_STRUCTURE_FK],[cust_entity_structure_fk],[Store],[DC],[Store Transmit Flag],[RESULT],PD_Effective_Date
	,[Product Transmit Flag],[Promo Page #],[Product Number],[Changes Flag],[Lot Pricer Flag],[Regular Base from DC],[AOC Vendor],[Selling Price Each],[Promo EU Ea],[Lot 1 Qty],[Lot 1 Sell Price]
	,[Lot 2 Qty],[Lot 2 Sell Price],[Product Description],[Standard Pack],[Catalog Page Number],[Lot A QTY],[Lot A Sell Price],[Lot B QTY],[Lot B Sell Price],[CORP Base Price],[CORP DropShipQty]
	,[Weight],[Season Code],[Customer Description],[Dept-Cat],[MSDS Indicator],[Memo Field],[Hazardous Material Ship Code],[Discontinued Flag],[Flyer Month Code],[Buyers Code],[Freight Code]
	,[Vendor Bar Code 1],[Reg Base from DC Eff Date],[Direct Cost Effective Date],[End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Manufacturer Part Number]
	,[Freight Code #2],[Append Date],[QtyBrk#2],[QtyBrk$1],[QtyBrk$2],[Serialized Product Flag],[Certification Required Flag],[Taxable Flag],[End User Regular Price],[List Each],[List Lot 1]
	,[List Lot 2],[WEB_NODE_ID],[Web Node Data],[EDI Part No],[Vendor No],[Country of Origin],[Return Flag],[Regular DC Cost],[DC Sell Qty],[Rules_FK], [Basis Calc],[Host Precedence Level], IsPromoFlag, [Corp SSP], [Store SSP], sProdTransFlag)
	Select STT.[Product_Structure_FK],[job_fk], [calc_job_fk],STT.[ORG_ENTITY_STRUCTURE_FK], STT.[cust_entity_structure_fk],[Store],[DC],[Store Transmit Flag],[RESULT],PD_Effective_Date
	,[Product Transmit Flag],[Promo Page #],[Product Number],[Changes Flag],[Lot Pricer Flag],[Regular Base from DC],[AOC Vendor],[Selling Price Each],[Promo EU Ea],[Lot 1 Qty],[Lot 1 Sell Price]
	,[Lot 2 Qty],[Lot 2 Sell Price],[Product Description],[Standard Pack],[Catalog Page Number],[Lot A QTY],[Lot A Sell Price],[Lot B QTY],[Lot B Sell Price],[CORP Base Price],[CORP DropShipQty]
	,[Weight],[Season Code],[Customer Description],[Dept-Cat],[MSDS Indicator],[Memo Field],[Hazardous Material Ship Code],[Discontinued Flag],[Flyer Month Code],[Buyers Code],[Freight Code]
	,[Vendor Bar Code 1],[Reg Base from DC Eff Date],[Direct Cost Effective Date],[End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Manufacturer Part Number]
	,[Freight Code #2],[Append Date],[QtyBrk#2],[QtyBrk$1],[QtyBrk$2],[Serialized Product Flag],[Certification Required Flag],[Taxable Flag],[End User Regular Price],[List Each],[List Lot 1]
	,[List Lot 2],[WEB_NODE_ID],[Web Node Data],[EDI Part No],[Vendor No],[Country of Origin],[Return Flag],[Regular DC Cost],[DC Sell Qty],[Rules_FK], [Basis Calc],[Host Precedence Level], IsPromoFlag, [Corp SSP], [Store SSP], 1
	from ##sTransTable_SPT STT
	inner join (Select Max([JS_STRAN_ID]) 'Max_JS_STRAN_ID', [Product_Structure_FK], [ORG_ENTITY_STRUCTURE_FK], [cust_entity_structure_fk] from ##sTransTable_SPT group by [Product_Structure_FK], [ORG_ENTITY_STRUCTURE_FK], [cust_entity_structure_fk]) STT2
		on STT.JS_STRAN_ID = STT2.Max_JS_STRAN_ID and STT.Product_Structure_FK = STT2.Product_Structure_FK and STT.ORG_ENTITY_STRUCTURE_FK = STT2.ORG_ENTITY_STRUCTURE_FK and STT.cust_entity_structure_fk = STT2.cust_entity_structure_fk

	SET @status_desc =  'sProdTransGeneration Ln 556 - Transfer Recs to JS_sTran Finish'
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

	--Capture any opportunity for Duplicate Records; duplicates were not loaded into table
	insert into custom.JS_sTran_History_Dups 
	( [JS_STRAN_ID], [Product_Structure_FK],[job_fk], [calc_job_fk],[ORG_ENTITY_STRUCTURE_FK],[cust_entity_structure_fk],[Store],[DC],[Store Transmit Flag],[RESULT],PD_Effective_Date
	,[Product Transmit Flag],[Promo Page #],[Product Number],[Changes Flag],[Lot Pricer Flag],[Regular Base from DC],[AOC Vendor],[Selling Price Each],[Promo EU Ea],[Lot 1 Qty],[Lot 1 Sell Price]
	,[Lot 2 Qty],[Lot 2 Sell Price],[Product Description],[Standard Pack],[Catalog Page Number],[Lot A QTY],[Lot A Sell Price],[Lot B QTY],[Lot B Sell Price],[CORP Base Price],[CORP DropShipQty]
	,[Weight],[Season Code],[Customer Description],[Dept-Cat],[MSDS Indicator],[Memo Field],[Hazardous Material Ship Code],[Discontinued Flag],[Flyer Month Code],[Buyers Code],[Freight Code]
	,[Vendor Bar Code 1],[Reg Base from DC Eff Date],[Direct Cost Effective Date],[End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Manufacturer Part Number]
	,[Freight Code #2],[Append Date],[QtyBrk#2],[QtyBrk$1],[QtyBrk$2],[Serialized Product Flag],[Certification Required Flag],[Taxable Flag],[End User Regular Price],[List Each],[List Lot 1]
	,[List Lot 2],[WEB_NODE_ID],[Web Node Data],[EDI Part No],[Vendor No],[Country of Origin],[Return Flag],[Regular DC Cost],[DC Sell Qty],[Rules_FK], [Basis Calc],[Host Precedence Level], IsPromoFlag, [Corp SSP], [Store SSP], sProdTransFlag)
	Select STT.JS_STRAN_ID, STT.[Product_Structure_FK],[job_fk], [calc_job_fk],STT.[ORG_ENTITY_STRUCTURE_FK], STT.[cust_entity_structure_fk],[Store],[DC],[Store Transmit Flag],[RESULT],PD_Effective_Date
	,[Product Transmit Flag],[Promo Page #],[Product Number],[Changes Flag],[Lot Pricer Flag],[Regular Base from DC],[AOC Vendor],[Selling Price Each],[Promo EU Ea],[Lot 1 Qty],[Lot 1 Sell Price]
	,[Lot 2 Qty],[Lot 2 Sell Price],[Product Description],[Standard Pack],[Catalog Page Number],[Lot A QTY],[Lot A Sell Price],[Lot B QTY],[Lot B Sell Price],[CORP Base Price],[CORP DropShipQty]
	,[Weight],[Season Code],[Customer Description],[Dept-Cat],[MSDS Indicator],[Memo Field],[Hazardous Material Ship Code],[Discontinued Flag],[Flyer Month Code],[Buyers Code],[Freight Code]
	,[Vendor Bar Code 1],[Reg Base from DC Eff Date],[Direct Cost Effective Date],[End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Manufacturer Part Number]
	,[Freight Code #2],[Append Date],[QtyBrk#2],[QtyBrk$1],[QtyBrk$2],[Serialized Product Flag],[Certification Required Flag],[Taxable Flag],[End User Regular Price],[List Each],[List Lot 1]
	,[List Lot 2],[WEB_NODE_ID],[Web Node Data],[EDI Part No],[Vendor No],[Country of Origin],[Return Flag],[Regular DC Cost],[DC Sell Qty],[Rules_FK], [Basis Calc],[Host Precedence Level], IsPromoFlag, [Corp SSP], [Store SSP], 1
	from ##sTransTable_SPT STT
		inner join (Select [Product_Structure_FK], [ORG_ENTITY_STRUCTURE_FK], [cust_entity_structure_fk] from ##sTransTable_SPT group by [Product_Structure_FK], [ORG_ENTITY_STRUCTURE_FK], [cust_entity_structure_fk] Having count(*) > 1) STT2
		on STT.Product_Structure_FK = STT2.Product_Structure_FK and STT.ORG_ENTITY_STRUCTURE_FK = STT2.ORG_ENTITY_STRUCTURE_FK and STT.cust_entity_structure_fk = STT2.cust_entity_structure_fk

	set @SP_Current_time = getdate()
	update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id
	Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'GENERATE FILES COMMAND ISSUED', null, null, null, 200, @SP_Current_time, null

		set @job_ex_id = (select top 1 job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name =  'GENERATE FILES COMMAND ISSUED')

		SET @status_desc =  'Calling sTransFileGenerator Package - custom.SP_sProdTransGeneration - Job ' + @In_JobFK
		Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

		DECLARE @cmd VARCHAR(4000)

		SET @cmd = 'DTEXEC /ISServer \SSISDB\JohnStoneElectric\sTransFileGenerator\sProdTransFileGenerator.dtsx /SERVER "EPATEST" /set \Package.Variables[Job_FK].Value;' + @Job_FK_vc

		DECLARE @returncode int
		EXEC @returncode = master..xp_cmdshell @cmd
		WAITFOR DELAY '00:00:15'

	set @SP_Current_time = getdate()
	update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id
	Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'PURGE WORK TABLES', null, null, null, 200, @SP_Current_time, null

	set @job_ex_id = (select top 1 job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name =  'PURGE WORK TABLES')

	SET @status_desc =  'Calling marginmgr.util_purge_margin_tables SP from - custom.SP_sProdTransGeneration - Job ' + @In_JobFK
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

	exec marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, null

	set @SP_Current_time = getdate()
	update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id

	set @job_ex_id = (select top 1 job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name = 'START PROCESSING')
	update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id

	set @SP_End_Time = getdate()
	 exec common.job_execution_create  @SP_Out_Job_FK, 'EXPORT COMPLETE', null, null, null,201, @SP_Start_Time, @SP_End_Time;

	 SET @status_desc =  'Execution finished for custom.SP_sProdTransGeneration - Job ' + @In_JobFK
	Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;


 END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

	  set @SP_End_Time = getdate()
	  Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'JOB FAILED', null, null, null, 202, @SP_Current_time, @SP_End_Time


		SELECT 
			@ErrorMessage = 'Execution of custom.SP_sProdTransGeneration has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @SP_Out_Job_FK
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );

		exec marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, null

   END CATCH;

    IF OBJECT_ID('tempdb..##sTransProdList_SPT') IS NOT NULL
		DROP TABLE ##sTransProdList_SPT

	IF OBJECT_ID('tempdb..##CustomerWarehouse_SPT') IS NOT NULL
		DROP TABLE ##CustomerWarehouse_SPT

	IF OBJECT_ID('tempdb..##sTransTable_SPT') IS NOT NULL
		Drop table ##sTransTable_SPT

END









