﻿






-- Copyright 2014 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        07/30/2014   Initial SQL Version



CREATE PROCEDURE [custom].[ARCHIVE_custom.JS_sTran_History] 
AS
BEGIN
        DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @job_timestamp DATETIME

		
		

        BEGIN TRY
            SET NOCOUNT ON ;
            SET @l_exec_no = 0
            SET @l_sysdate = GETDATE()
										  

							
            SET @ls_stmt = 'started execution of [Custom].[ARCHIVE_custom.JS_sTran_History]'
                 EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;
                








INSERT INTO [custom].[JS_sTran_History_archive]
           ([JS_STRAN_ID]
           ,[Product_Structure_FK]
           ,[job_fk]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[cust_entity_structure_fk]
           ,[Store]
           ,[DC]
           ,[Store Transmit Flag]
           ,[RESULT]
           ,[Product Transmit Flag]
           ,[Promo Page #]
           ,[Product Number]
           ,[Changes Flag]
           ,[Lot Pricer Flag]
           ,[Regular Base from DC]
           ,[AOC Vendor]
           ,[Selling Price Each]
           ,[Promo EU Ea]
           ,[Lot 1 Qty]
           ,[Lot 1 Sell Price]
           ,[Lot 2 Qty]
           ,[Lot 2 Sell Price]
           ,[Product Description]
           ,[Standard Pack]
           ,[Catalog Page Number]
           ,[Lot A QTY]
           ,[Lot A Sell Price]
           ,[Lot B QTY]
           ,[Lot B Sell Price]
           ,[CORP Base Price]
           ,[CORP DropShipQty]
           ,[Weight]
           ,[Season Code]
           ,[Customer Description]
           ,[Dept-Cat]
           ,[SSP Indicator]
           ,[MSDS Indicator]
           ,[Memo Field]
           ,[Hazardous Material Ship Code]
           ,[Discontinued Flag]
           ,[Flyer Month Code]
           ,[Buyers Code]
           ,[Freight Code]
           ,[Vendor Bar Code 1]
           ,[Reg Base from DC Eff Date]
           ,[Direct Cost Effective Date]
           ,[End User Each Published Flag]
           ,[End User Lot 1 Published Flag]
           ,[End User Lot 2 Published Flag]
           ,[Manufacturer Part Number]
           ,[Freight Code #2]
           ,[Each Sell Price Spcl Flag]
           ,[Lot 1 Sell Price Spcl Flag]
           ,[Lot 2 Sell Price Spcl Flag]
           ,[Append Date]
           ,[QtyBrk#2]
           ,[QtyBrk$1]
           ,[QtyBrk$2]
           ,[Serialized Product Flag]
           ,[Certification Required Flag]
           ,[Taxable Flag]
           ,[End User Regular Price]
           ,[List Each]
           ,[List Lot 1]
           ,[List Lot 2]
           ,[WEB_NODE_ID]
           ,[Web Node Data]
           ,[EDI Part No]
           ,[Vendor No]
           ,[Country of Origin]
           ,[Return Flag]
           ,[Regular DC Cost]
           ,[DC Sell Qty]
           ,[IsPromoFlag]
           ,[PD_Effective_Date]
           ,[sProdTransFlag]
           ,[calc_Job_FK]
           ,[Rules_FK]
           ,[Basis Calc]
           ,[Host Precedence Level]
           ,[Corp SSP]
           ,[Store SSP])
   ( select * from custom.JS_sTran_History 
	where [Append Date] < getdate() - 45
	and ISNULL(sProdTransFlag, 0) = 0)

----------------------------------------------------------------------------

	delete  from custom.JS_sTran_History 
	where [Append Date] < getdate() - 45

	delete  from custom.JS_sTran_History_archive 
	where [Append Date] < getdate() - 120


	------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

            SET @status_desc = 'finished execution of [Custom].[ARCHIVE_custom.JS_sTran_History]'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;

        END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of [Custom].[ARCHIVE_custom.JS_sTran_History] has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            EXEC exec_monitor.email_error_sp @l_exec_no,
                'dbexceptions@epacube.com' ;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH
		  
    END
