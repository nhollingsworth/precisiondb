﻿-- =============================================
-- Author:		<Gary Stone>
-- Create date: <June 24, 2015>
-- Description:	<Create Static Tables from Views for Performance>
-- =============================================
CREATE PROCEDURE [custom].[FuturePricing_Static_View_Creation]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	
	SET NOCOUNT ON;

	If object_id('custom.V_FuturePrices') is not null
	drop table custom.V_FuturePrices

	If object_id('custom.V_FuturePricing') is not null
	drop table custom.V_FuturePricing

	Select *, getdate() 'View_Refresh_Timestamp' into custom.V_FuturePrices from custom.V_FuturePrices_View

	Create index idx_vfp on custom.V_FuturePrices(data_name_fk,Product_structure_fk, org_entity_structure_fk, product_id, warehouse, list_effDate)

	select *, getdate() 'View_Refresh_Timestamp' into custom.V_FuturePricing from custom.V_FuturePricing_View
	Create index idx_vpf on custom.V_FuturePricing([Whse Code], [Vendor ID], [Product Number])

END
