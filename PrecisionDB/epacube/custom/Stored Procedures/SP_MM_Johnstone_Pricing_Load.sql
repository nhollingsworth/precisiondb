﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [custom].[SP_MM_Johnstone_Pricing_Load]
AS
BEGIN

	SET NOCOUNT ON;

 DECLARE @cmd VARCHAR(4000)

SET @cmd = 'DTEXEC /ISServer \SSISDB\JohnStoneElectric\MassMaintenance\MM_Johnstone_Pricing.dtsx /SERVER "EPATEST"' -- /set'-- \package.variables[JobFK].Value;' + @Job_FK

DECLARE @returncode int
EXEC @returncode = master..xp_cmdshell @cmd
END
