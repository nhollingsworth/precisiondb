﻿

-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		<Author,,Name>
---- Create date: <Create Date,,>
---- Description:	<Description,,>
---- =============================================
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------

-- CV        07/20/2014   fixed Freight Code
-- CV		 07/25/2014   Adding check for rules that went inactive
-- CV        08/25/2014   Fix Date used for dataname selection
-- CV        09/17/2014   Added @LastTransmissionDate to logic  and  logging to dbo event calc tables 
-- CV        09/21/2014   update job name at end for next run. 
-- CV        10/02/2014   changed Current to Regular Qty 402,403 instead of 396,397
-- GHS		 11/10/2014   Added code to delete duplicates in transmission, usually caused by multiple processing per day.
-- GHS		 09/25/2015	  Added logic to not calculate Corp SSP pricing from Type 1 PD records when the PD record is not tied to the CORP Whse. Ln 309

CREATE PROCEDURE [custom].[SP_sTransGeneration] (@In_JobFK_int bigint)
AS
BEGIN

	SET NOCOUNT ON;

--Declare @In_JobFK_int bigint
--Exec [common].[JOB_CREATE] Null, 226, 'sTrans File Export', Null, Null, Null, Null, @In_JobFK_int Output

Declare @SP_Out_Job_FK bigint
declare @SP_Start_Time DateTime
declare @SP_End_Time DateTime
declare @SP_Current_time datetime
declare @In_JobFK varchar(16)
declare @job_ex_id bigint
Declare @RuleDaysCurrent Int

DECLARE @ls_exec            nvarchar (max);
DECLARE @l_exec_no          bigint;

DECLARE @status_desc        varchar(max);
Declare @LastTransmissionDate Datetime

Set @LastTransmissionDate = (SELECT MAX ( JOB.JOB_CREATE_TIMESTAMP)FROM COMMON.JOB JOB WITH (NOLOCK) WHERE NAME = 'sTrans File Export')
Set @RuleDaysCurrent = 1 

begin try
set @SP_Start_Time = getdate()

set @SP_Out_Job_FK = @In_JobFK_int
set @In_JobFK =  cast (@In_JobFK_int as varchar(16))

Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'START PROCESSING', null, null, null, 200, @SP_Start_Time, null

SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of custom.SP_sTransGeneration', 
		@exec_id = @l_exec_no OUTPUT,
		@epa_job_id = @SP_Out_Job_FK;

SET @status_desc =  'Last Transmission Date = ' + Cast(@LastTransmissionDate as Varchar(64))
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

SET @status_desc =  'Execution started for custom.SP_sTransGeneration - Job ' + @In_JobFK
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;


Declare @Out_Job_FK Bigint
Declare @Job_FK Bigint
Declare @Job_FK_vc Varchar(16)

Exec [common].[JOB_CREATE] Null, 481, 'Johnstone STrans Calcs', Null, Null, Null, Null, @Out_Job_FK Output

Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'START CALC JOB', null, null, null, 200, @SP_Start_Time, null

set @job_ex_id = (select job_execution_id from common.JOB_EXECUTION with (nolock) where JOB_FK = @SP_Out_Job_FK and name = 'START CALC JOB')


Set @Job_FK = @Out_Job_FK
Set @Job_FK_vc = Cast(@Job_FK as varchar(16))
	
    
    IF OBJECT_ID('tempdb..##sTransProdList') IS NOT NULL
		DROP TABLE ##sTransProdList

	IF OBJECT_ID('tempdb..##CustomerWarehouse') IS NOT NULL
		DROP TABLE ##CustomerWarehouse

	IF OBJECT_ID('tempdb..##SH_Calcs') IS NOT NULL
		Drop Table ##SH_Calcs

	IF OBJECT_ID('tempdb..#sTransDups0') IS NOT NULL
	Drop Table #sTransDups0

	IF OBJECT_ID('tempdb..#sTransDups') IS NOT NULL
	Drop Table #sTransDups

	IF OBJECT_ID('tempdb..#Dups') IS NOT NULL
	Drop Table #Dups

	IF OBJECT_ID('tempdb..##sTransTable') IS NOT NULL
	Drop table ##sTransTable


	--get stores by warehouse  --create a temp of warehouse for cross join
	select ei.entity_structure_fk 'cust_entity_structure_fk', ea_3c.ATTRIBUTE_CHAR 'Store', ei_o.entity_structure_fk 'org_entity_structure_fk', ea_o.ATTRIBUTE_EVENT_DATA 'DC', ea_t.ATTRIBUTE_EVENT_DATA 'Transmit'  
	into ##CustomerWarehouse
	from epacube.entity_identification ei with (nolock)
	inner join epacube.entity_attribute ea_o with (nolock)  on ei.ENTITY_STRUCTURE_FK = ea_o.ENTITY_STRUCTURE_FK and ea_o.DATA_NAME_FK = 144900
	inner join epacube.entity_identification ei_o with (nolock) on ea_o.attribute_event_data = ei_o.value and ei_o.data_name_fk = 141111 and ei_o.ENTITY_DATA_NAME_FK = 141000
	left join epacube.entity_attribute ea_t with (nolock)  on ei.ENTITY_STRUCTURE_FK = ea_T.ENTITY_STRUCTURE_FK and ea_T.DATA_NAME_FK = 244834
	left join epacube.entity_attribute ea_3c with (nolock) on ei.entity_structure_fk = ea_3c.entity_structure_fk and ea_3c.data_name_fk = 244835
	where ei.ENTITY_DATA_NAME_FK in (144010, 144020) and ea_t.ATTRIBUTE_EVENT_DATA is not null
	-------needed to add corp to the mix.
	
	Union

	select ea_3c.entity_structure_fk 'cust_entity_structure_fk', ea_3c.ATTRIBUTE_CHAR 'Store'
	, ei.entity_structure_fk 'org_entity_structure_fk', 'CORP' 'DC', '1' 'Transmit' 
	from epacube.ENTITY_IDENTIFICATION ei with (nolock) 
	cross join epacube.entity_attribute ea_t with (nolock)   
	left join epacube.entity_attribute ea_3c with (nolock) on ea_t.entity_structure_fk = ea_3c.entity_structure_fk and ea_3c.data_name_fk = 244835
	where ea_t.ATTRIBUTE_EVENT_DATA is not null
	and ei.entity_structure_fk = 11794 and ea_T.DATA_NAME_FK = 244834

	set @SP_Current_time = getdate()

update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id
Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'IDENTIFICATION OF PRODUCTS', null, null, null, 200, @SP_Current_time, null

set @job_ex_id = (select job_execution_id from common.JOB_EXECUTION with (nolock) where JOB_FK = @SP_Out_Job_FK and name = 'IDENTIFICATION OF PRODUCTS')

--Declare @LastTransmissionDate Datetime

--Set @LastTransmissionDate = (SELECT MAX ( JOB.JOB_CREATE_TIMESTAMP)FROM COMMON.JOB JOB WITH (NOLOCK) WHERE NAME = 'sTrans File Export')


    --find all the products that have a qualifying change and create temp table? add to sTrans_History table?
	--Products affected by a price record's new effective date tomorrow where the price record governs a single product (and perhaps customer)
	select  [Product ID], PRODUCT_STRUCTURE_FK, cust_entity_structure_fk, store, org_entity_structure_fk, dc, Transmit, null Job_FK, PD into ##sTransProdList FROM
	(select DISTINCT pi.value 'Product ID', pi.product_structure_fk, 1 PD
	from synchronizer.rules_filter RF_p with (nolock) 
	inner join synchronizer.rules_filter_set rfs with (nolock) on RF_p.rules_filter_id = rfs.PROD_FILTER_FK
	inner join synchronizer.rules r with (nolock) on rfs.rules_fk = r.rules_id
	inner join epacube.product_identification pi with (nolock) on RF_p.data_name_fk = pi.data_name_fk and RF_p.value1 = pi.value
	where RF_p.entity_class_cr_fk = 10109 and RF_p.data_name_fk = 110100 and r.RESULT_DATA_NAME_FK = 111602 
	and (
		(Cast(r.EFFECTIVE_DATE as date) = Cast(getdate() + 1 as date))
		 or 
		 (Cast(r.EFFECTIVE_DATE as date) <= Cast(getdate() + 1 as date) and Cast(r.create_timestamp as date) >= Cast(getdate() - @RuleDaysCurrent as date))
		 )
	Union
	--Products affected by a price record's new effective date tomorrow where the price record governs a group of products (such as product price type)
	select DISTINCT pi.value 'Product ID', pi.product_structure_fk, 1 PD
	from synchronizer.rules_filter RF_p  with (nolock)
	inner join epacube.product_category pc with (nolock) on rf_p.data_name_fk = pc.data_name_fk
	inner join epacube.data_value dv with (nolock) on pc.DATA_NAME_FK = dv.data_name_fk and pc.data_value_fk = dv.data_value_id and RF_p.value1 = dv.VALUE
	inner join synchronizer.rules_filter_set rfs with (nolock) on RF_p.rules_filter_id = rfs.PROD_FILTER_FK
	inner join synchronizer.rules r with (nolock) on rfs.rules_fk = r.rules_id
	inner join epacube.product_identification pi with (nolock) on pc.product_structure_fk = pi.product_structure_fk and pi.DATA_NAME_FK = 110100
	where RF_p.entity_class_cr_fk = 10109 and RF_p.data_name_fk <> 110100 and r.RESULT_DATA_NAME_FK = 111602 
	and (
		(Cast(r.EFFECTIVE_DATE as date) = Cast(getdate() + 1 as date))
		 or 
		 (Cast(r.EFFECTIVE_DATE as date) <= Cast(getdate() + 1 as date) and Cast(r.create_timestamp as date) >= Cast(getdate() - @RuleDaysCurrent as date))
		 )
	Union
	--Products affected by a different type 3 price record than yesterday because the store has a different customer price type than yesterday, or no longer has a customer price type
	select Distinct pi.value Product_id, pi.product_structure_fk, 1 PD from synchronizer.rules_filter rf with (nolock) 
	inner join epacube.product_identification pi with (nolock) on rf.data_name_fk = pi.data_name_fk and rf.value1 = pi.value
	where entity_class_cr_fk = 10109 and rules_filter_id in (
	Select prod_filter_fk from synchronizer.rules_filter_set with (nolock) where cust_filter_fk in (
	select rf.rules_filter_id cust_filter_fk from synchronizer.rules_filter rf with (nolock) --where entity_class_cr_fk = 10104 and data_name_fk = 244900
	Inner Join (

		   select dv.data_name_fk, dv.value cust_price_type, emt.UPDATE_TIMESTAMP Update_timestamp from epacube.entity_mult_type emt with (nolock)
		   inner join epacube.data_value dv with (nolock) on emt.data_name_fk = dv.data_name_fk and emt.DATA_VALUE_FK = dv.DATA_VALUE_ID
		   left join epacube.entity_mult_type_historical emth with (nolock)
				  on emt.ENTITY_STRUCTURE_FK = emth.ENTITY_STRUCTURE_FK
				  and emt.data_name_fk = emth.data_name_fk
				  and dv.value = emth.value
				  and emth.Load_From_EMT_Date = (Select top 1 Load_From_EMT_Date LoadDate from epacube.entity_mult_type_historical with (nolock) order by Load_From_EMT_Date desc)
		   where emth.entity_mult_type_historical_id is null and emt.update_timestamp >=  cast(@LastTransmissionDate as date)
		   Union
		   select dv.data_name_fk, dv.value, Load_From_EMT_Date from epacube.entity_mult_type_Historical emth with (nolock)
		   inner join epacube.data_value dv with (nolock) on emth.data_name_fk = dv.data_name_fk and emth.value = dv.value
		   left join epacube.entity_mult_type emt with (nolock) 
				  on emt.ENTITY_STRUCTURE_FK = emth.ENTITY_STRUCTURE_FK
				  and emt.data_name_fk = emth.data_name_fk
				  and emt.data_value_fk = dv.DATA_VALUE_ID
		   where emt.entity_mult_type_ID is null
		   and emth.Load_From_EMT_Date = (Select top 1 Load_From_EMT_Date LoadDate from epacube.entity_mult_type_historical with (nolock) order by Load_From_EMT_Date desc)
		   and emth.Load_From_EMT_Date >=  cast(@LastTransmissionDate as date)

	) CPT on rf.data_name_fk = cpt.data_name_fk
	where rf.entity_class_cr_fk = 10104 and rf.data_name_fk = 244900 and rf.value1 = cpt.cust_price_type group by rules_filter_id)
	
	and prod_filter_fk is not null group by prod_filter_fk
	)and rf.data_name_fk = 110100

--Products affected by a price record's END DATE 
		UNION
		select DISTINCT pi.value 'Product ID', pi.product_structure_fk, 1 PD
	from synchronizer.rules_filter RF_p with (nolock) 
	inner join synchronizer.rules_filter_set rfs with (nolock) on RF_p.rules_filter_id = rfs.PROD_FILTER_FK
	inner join synchronizer.rules r with (nolock) on rfs.rules_fk = r.rules_id
	inner join epacube.product_identification pi with (nolock) on RF_p.data_name_fk = pi.data_name_fk and RF_p.value1 = pi.value
	where RF_p.entity_class_cr_fk = 10109 and RF_p.data_name_fk = 110100 and r.RESULT_DATA_NAME_FK = 111602  
	and Cast(r.END_DATE as date) >=  cast(@LastTransmissionDate as date)
	and r.RECORD_STATUS_CR_FK = 2
	Union
	--Products affected by a price record's end date tomorrow where the price record governs a group of products (such as product price type)
	select DISTINCT pi.value 'Product ID', pi.product_structure_fk, 1 PD
	from synchronizer.rules_filter RF_p with (nolock) 
	inner join epacube.product_category pc with (nolock) on rf_p.data_name_fk = pc.data_name_fk
	inner join epacube.data_value dv with (nolock) on pc.DATA_NAME_FK = dv.data_name_fk and pc.data_value_fk = dv.data_value_id and RF_p.value1 = dv.VALUE
	inner join synchronizer.rules_filter_set rfs with (nolock) on RF_p.rules_filter_id = rfs.PROD_FILTER_FK
	inner join synchronizer.rules r with (nolock) on rfs.rules_fk = r.rules_id
	inner join epacube.product_identification pi with (nolock) on pc.product_structure_fk = pi.product_structure_fk and pi.DATA_NAME_FK = 110100
	where RF_p.entity_class_cr_fk = 10109 and RF_p.data_name_fk <> 110100 and r.RESULT_DATA_NAME_FK = 111602 
	and Cast(r.END_DATE as date) >= cast(@LastTransmissionDate as date)
	and r.RECORD_STATUS_CR_FK = 2

	--Product and Customer Groups affected by a different price record than the type 4 price record used yesterday because the store has a different customer price type than yesterday, or no longer has a customer price type
	Union
	select DISTINCT pi.value Product_id, pi.product_structure_fk, 1 PD 
	from synchronizer.rules_filter rf_p with (nolock)
	inner join epacube.product_category pc with (nolock) on rf_p.data_name_fk = pc.data_name_fk
	inner join epacube.data_value dv with (nolock) on pc.DATA_NAME_FK = dv.data_name_fk and pc.data_value_fk = dv.data_value_id and RF_p.value1 = dv.VALUE
	inner join epacube.product_identification pi with (nolock) on pc.product_structure_fk = pi.product_structure_fk and pi.DATA_NAME_FK = 110100
	where entity_class_cr_fk = 10109 and rules_filter_id in (
	Select prod_filter_fk from synchronizer.rules_filter_set with (nolock) where cust_filter_fk in (
	select rf.rules_filter_id cust_filter_fk from synchronizer.rules_filter rf with (nolock) --where entity_class_cr_fk = 10104 and data_name_fk = 244900
	Inner Join (select dv.data_name_fk, dv.value cust_price_type from epacube.entity_mult_type emt with (nolock)
			inner join epacube.data_value dv with (nolock) on emt.data_name_fk = dv.data_name_fk and emt.DATA_VALUE_FK = dv.DATA_VALUE_ID
			left join epacube.entity_mult_type_historical emth with (nolock)
				  on emt.ENTITY_STRUCTURE_FK = emth.ENTITY_STRUCTURE_FK
				  and emt.data_name_fk = emth.data_name_fk
				  and dv.value = emth.value
				  and emth.Load_From_EMT_Date = (Select top 1 Load_From_EMT_Date LoadDate from epacube.entity_mult_type_historical with (nolock) order by Load_From_EMT_Date desc)
		   where emth.entity_mult_type_historical_id is null and emth.Load_From_EMT_Date >=  cast(@LastTransmissionDate as date)
		   Union
		   select dv.data_name_fk, dv.value from epacube.entity_mult_type_Historical emth with (nolock)
		   inner join epacube.data_value dv with (nolock) on emth.data_name_fk = dv.data_name_fk and emth.Value = dv.Value
		   left join epacube.entity_mult_type emt with (nolock)
				  on emt.ENTITY_STRUCTURE_FK = emth.ENTITY_STRUCTURE_FK
				  and emt.data_name_fk = emth.data_name_fk
				  and emt.data_value_fk = dv.DATA_VALUE_ID
		   where emt.entity_mult_type_ID is null
		   and emth.Load_From_EMT_Date = (Select top 1 Load_From_EMT_Date LoadDate from epacube.entity_mult_type_historical order by Load_From_EMT_Date desc)
		   and emth.Load_From_EMT_Date >=  cast(@LastTransmissionDate as date)
	) CPT on rf.data_name_fk = cpt.data_name_fk
	where rf.entity_class_cr_fk = 10104 and rf.data_name_fk = 244900 and rf.value1 = cpt.cust_price_type group by rules_filter_id)
	and prod_filter_fk is not null
	)
	and rf_p.data_name_fk <> 110100

	--All products that have a qualifying change on the specified data_names from the Product updates
	UNION
	select distinct value,  edh.PRODUCT_STRUCTURE_FK, 0 PD
	from synchronizer.event_data_history edh with (nolock)
	inner join epacube.PRODUCT_IDENTIFICATION pid with (nolock) on edh.PRODUCT_STRUCTURE_FK = pid.PRODUCT_STRUCTURE_FK and pid.DATA_NAME_FK = 110100
	inner join epacube.product_association pa with (nolock) on edh.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and pa.data_name_fk = 159200 and pa.ENTITY_STRUCTURE_FK = 12
	inner join epacube.DATA_NAME dn with (nolock) on pid.DATA_NAME_FK = dn.DATA_NAME_ID
	where  edh.update_timestamp > = @LastTransmissionDate ------- Cindy CHANGE as per Jan
	 --where Cast( EVENT_EFFECTIVE_DATE as Date) = Cast(GetDate() as Date	 )
	and EVENT_STATUS_CR_FK in (81,83) and edh.DATA_NAME_FK in (100100287,100100110,100100402,100100403,100100389,100100154,100100410,100100411,110815,100100342,110817,100100272,100100376,
	100100163,100100276,100100374,100100219,100100220,100100221,100100375,100100343,100100390,100100356,100100158,100100341,210831,100100358,100100415,100100161,100100100,
	110102,100100197,110100,110401,100100360,100100392,100100398,100100520,100100416,100100404,100100600)) PL
	cross join ##CustomerWarehouse

	Create Index idx_st on ##sTransProdList(product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk, job_fk, PD)

	Update ##sTransProdList Set Job_FK = @Job_FK

	--Update SH Set PD = 1
	--from ##sTransProdList sh
	--inner join synchronizer.rules_filter rf_p with (nolock) on sh.[product id] = rf_p.VALUE1 and rf_p.entity_class_cr_fk = 10109 and rf_p.data_name_fk = 110100
	--inner join epacube.entity_identification ei with (nolock) on sh.cust_entity_structure_fk = ei.entity_structure_fk and ei.entity_data_name_fk in (144010, 144020)
	--inner join synchronizer.rules_filter rf_c with (nolock) on ei.value = rf_c.value1 and rf_c.ENTITY_CLASS_CR_FK = 10104
	--inner join synchronizer.rules_filter_set rfs with (nolock) on rf_p.rules_filter_id = rfs.prod_filter_fk and rf_c.rules_filter_id = rfs.cust_filter_fk
	--inner join synchronizer.rules r with (nolock) on rfs.rules_fk = r.rules_id
	--where r.result_data_name_fk = 111602
	--and isnull(r.end_date, getdate() + 1) > getdate()
	--and r.EFFECTIVE_DATE <= getdate()
	--and r.host_precedence_level = '1'

	Update SH Set PD = 1
	from ##sTransProdList sh
	inner join synchronizer.rules_filter rf_p with (nolock) on sh.[product id] = rf_p.VALUE1 and rf_p.entity_class_cr_fk = 10109 and rf_p.data_name_fk = 110100
	inner join epacube.entity_identification ei with (nolock) on sh.cust_entity_structure_fk = ei.entity_structure_fk and ei.entity_data_name_fk in (144010, 144020)
	inner join synchronizer.rules_filter rf_c with (nolock) on ei.value = rf_c.value1 and rf_c.ENTITY_CLASS_CR_FK = 10104
	inner join synchronizer.rules_filter_set rfs with (nolock) on rf_p.rules_filter_id = rfs.prod_filter_fk and rf_c.rules_filter_id = rfs.cust_filter_fk
	left join synchronizer.RULES_FILTER rf_o with (nolock) on rfs.ORG_FILTER_FK = rf_o.RULES_FILTER_ID
	inner join synchronizer.rules r with (nolock) on rfs.rules_fk = r.rules_id
	where r.result_data_name_fk = 111602
	and isnull(r.end_date, getdate() + 1) > getdate()
	and r.EFFECTIVE_DATE <= getdate()
	and (rf_o.VALUE1 = sh.DC or (sh.DC <> 'Corp' and rf_o.VALUE1 is null))
	and r.host_precedence_level = '1'

	Update SH Set PD = 1
	from ##sTransProdList sh
	inner join synchronizer.rules_filter rf_p with (nolock) on sh.[product id] = rf_p.VALUE1 and rf_p.entity_class_cr_fk = 10109 and rf_p.data_name_fk = 110100
	inner join synchronizer.rules_filter_set rfs with (nolock) on rf_p.rules_filter_id = rfs.prod_filter_fk 
	inner join synchronizer.rules r with (nolock) on rfs.rules_fk = r.rules_id
	where isnull(rfs.cust_filter_fk, 0) = 0
	and r.result_data_name_fk = 111602
	and isnull(r.end_date, getdate() + 1) > getdate()
	and r.EFFECTIVE_DATE <= getdate()
	and r.host_precedence_level = '7'

	Update SH Set PD = 1
	from ##sTransProdList sh
	inner join epacube.entity_mult_type emt with (nolock) on sh.cust_entity_structure_fk = emt.ENTITY_STRUCTURE_FK and emt.data_name_fk = 244900
	inner join epacube.data_value dv with (nolock) on emt.data_value_fk = dv.data_value_id
	inner join synchronizer.rules_filter rf_c with (nolock) on dv.value = rf_c.value1 and rf_c.ENTITY_CLASS_CR_FK = 10104
	inner join epacube.entity_identification ei with (nolock) on sh.dc = ei.value and ei.entity_data_name_fk = 141000
	inner join epacube.product_category pc with (nolock) on ei.entity_structure_fk = pc.ORG_ENTITY_STRUCTURE_FK and sh.product_structure_fk = pc.PRODUCT_STRUCTURE_FK
	inner join epacube.data_value dv_p with (nolock) on pc.data_value_fk = dv_p.data_value_id
	inner join synchronizer.rules_filter rf_p with (nolock) on dv_p.value = rf_p.VALUE1 and rf_p.entity_class_cr_fk = 10109 and rf_p.data_name_fk <> 110100
	inner join synchronizer.rules_filter_set rfs with (nolock) on rf_c.rules_filter_id = rfs.cust_filter_fk and rf_p.rules_filter_id = rfs.prod_filter_fk
	inner join synchronizer.rules r with (nolock) on rfs.rules_fk = r.rules_id
	where r.result_data_name_fk = 111602
	and isnull(r.end_date, getdate() + 1) > getdate()
	and r.EFFECTIVE_DATE <= getdate()
	and r.host_precedence_level = '4'

	Update SH Set PD = 1
	from synchronizer.rules_filter_set rfs with (nolock)
	inner join synchronizer.rules_filter rf_p with (nolock) on rfs.prod_filter_fk = rf_p.rules_filter_id
	inner join epacube.data_value dv with (nolock) on rf_p.value1 = dv.value and rf_p.data_name_fk = dv.data_name_fk
	inner join epacube.product_category pc with (nolock) on dv.data_value_id = pc.data_value_fk
	inner join ##sTransProdList sh on pc.product_structure_fk = sh.product_structure_fk and pc.org_entity_structure_fk = sh.org_entity_structure_fk 
	inner join synchronizer.rules r with (nolock) on rfs.rules_fk = r.rules_id
	Where rfs.cust_filter_fk is null
	and r.result_data_name_fk = 111602
	and isnull(r.end_date, getdate() + 1) > getdate()
	and r.EFFECTIVE_DATE <= getdate()
	and r.host_precedence_level = '8'

	Select * into ##SH_Calcs from ##sTransProdList where PD = 1
	Create Index idx_sc on ##SH_Calcs(product_structure_fk, org_entity_structure_fk, cust_entity_structure_fk, job_fk)

	set @SP_Current_time = getdate()
	update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id
	Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'RUN CALC JOB ON PRODUCTS', null, null, null, 200, @SP_Current_time, null

	set @job_ex_id = (select job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name = 'RUN CALC JOB ON PRODUCTS')

	--run calc job for all the products in ##SH_Calcs
	Declare @Eff_Date date
	Declare @Result_Type_CR_FK Int
	Declare @COST_CHANGE_PRIOR_IND Int
	Declare @COST_CHANGE_FIRST_IND int

	Set @Eff_Date = Cast(getdate() + 1 as date)
	Set @Result_Type_CR_FK = Case when ISNULL(@Eff_Date, getdate()) > getdate() then 711 else 710 end

	Set @COST_CHANGE_FIRST_IND = Case When @Result_Type_CR_FK = 711 and @Eff_Date is Null then 1 else 0 end
	Set @COST_CHANGE_PRIOR_IND = Case When @Result_Type_CR_FK = 711 and @Eff_Date is not Null then 1 else 0 end

	Insert into epacube.marginmgr.ANALYSIS_JOB
		(Analysis_Job_ID, Analysis_Name, result_type_cr_fk, result_effective_date, rule_type_filter, Inner_Join_Filter, Product_Filter, Organization_Filter, Customer_Filter, RECORD_STATUS_CR_FK)
		Select 
		@Job_FK
		, 'Johnstone STrans Daily Process'
		, @Result_Type_CR_FK
		, @Eff_Date
		, '(313)'
		, 'Inner Join ##SH_Calcs ST on PS.Product_Structure_ID = ST.Product_Structure_FK 
				And ESC.Entity_Structure_ID = ST.Cust_Entity_Structure_FK 
				And ESO.entity_Structure_ID = ST.org_entity_structure_fk'
		, '(Select distinct Product_Structure_FK from ##SH_Calcs)'
		, '(Select distinct org_entity_structure_fk from ##SH_Calcs)'
		, '(Select distinct cust_entity_structure_fk from ##SH_Calcs)'
		, 1

SET @status_desc =  'Job ' + @Job_FK_vc + ' JS sTrans Calc execution started for custom.SP_sTransGeneration - Job ' + @In_JobFK
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

EXEC  [marginmgr].[CALCULATION_PROCESS] @Job_FK

SET @status_desc =  'Job ' + @Job_FK_vc + ' JS sTrans Calc execution finished for custom.SP_sTransGeneration - Job ' + @In_JobFK
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;



	set @SP_Current_time = getdate()
	update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id
	Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'RESULTS TO JS_sTran_History', null, null, null, 200, @SP_Current_time, null

	set @job_ex_id = (select job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name = 'RESULTS TO JS_sTran_History')

SET @status_desc =  'Inserting Data to History table - custom.SP_sTransGeneration - Job ' + @In_JobFK
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

Select top 1 * into ##sTransTable from custom.JS_sTran_History where 1 = 2

--query to get all the data required for the sTrans to be inserted into sTransHistory table
insert into ##sTransTable --custom.JS_sTran_History 
( [Product_Structure_FK],[job_fk], [calc_job_fk],[ORG_ENTITY_STRUCTURE_FK],[cust_entity_structure_fk],[Store],[DC],[Store Transmit Flag],[RESULT],PD_Effective_Date
,[Product Transmit Flag],[Promo Page #],[Product Number],[Changes Flag],[Lot Pricer Flag],[Regular Base from DC],[AOC Vendor],[Selling Price Each],[Promo EU Ea],[Lot 1 Qty],[Lot 1 Sell Price]
,[Lot 2 Qty],[Lot 2 Sell Price],[Product Description],[Standard Pack],[Catalog Page Number],[Lot A QTY],[Lot A Sell Price],[Lot B QTY],[Lot B Sell Price],[CORP Base Price],[CORP DropShipQty]
,[Weight],[Season Code],[Customer Description],[Dept-Cat],[MSDS Indicator],[Memo Field],[Hazardous Material Ship Code],[Discontinued Flag],[Flyer Month Code],[Buyers Code],[Freight Code]
,[Vendor Bar Code 1],[Reg Base from DC Eff Date],[Direct Cost Effective Date],[End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Manufacturer Part Number]
,[Freight Code #2],[Append Date],[QtyBrk#2],[QtyBrk$1],[QtyBrk$2],[Serialized Product Flag],[Certification Required Flag],[Taxable Flag],[End User Regular Price],[List Each],[List Lot 1]
,[List Lot 2],[WEB_NODE_ID],[Web Node Data],[EDI Part No],[Vendor No],[Country of Origin],[Return Flag],[Regular DC Cost],[DC Sell Qty],[Rules_FK], [Basis Calc],[Host Precedence Level], IsPromoFlag, [Corp SSP], [Store SSP])

select --distinct 
pl.product_structure_fk,@SP_Out_Job_FK , pl.job_fk, pl.org_entity_structure_fk, pl.cust_entity_structure_fk, pl.store, pl.DC, pl.[Transmit],ec.result, ec.RESULT_EFFECTIVE_DATE,[Transmit Flag]
, [Promo Page #],[JS Prod ID],case when [Promo Page #] is not null then 'C' else '*' end as [Changes Flag],
Case when cast([Lot A Prc] as decimal(18,5))  > 0.00000  then '*'  else null end as [Lot Pricer Flag]
, [Regular Base Price],[AOC Vend #],[CEU Ea Prc], [Promo End User Each], [Regular End User Qty 1], [CEU 1 Prc], [Regular End User Qty 2], [CEU 2 Prc], [Descr 1],[Vend Std Pk],[Catalog Page]
,[Lot QTY A], [Lot A prc], [Lot QTY B], [Lot B Prc], [CORP Base Price], [CORP DS 1 QTY],[Wt],[Seasonal CD], [USER Desc], [Store PCAT], [MSDS Sht #], [Memo], [z_icsp.u_hazcode], [Discontinued Ind]
, [On Flyers], [dept]
,[Frt Cd 1]
, UPC, [RegBase_EffDate], [CORP Base Price Effective Date], [End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag]
,[Vend Prod],[Frt Cd 2], cast(getDate() as date), FILTER_VALUE1, OPERAND1, OPERAND2,[Serialized Product Flag],[Cert Cd],[Taxable Tool Flag], [REU Ea Prc], [List Each]
, [List Lot Price 1],[List Lot Price 2],[WEB_NODE_ID],[Web Node Desc],[EDI Part No], right([AOC VEND #],Len([AOC VEND #]) - 1),[Country of Origin], [Return Flag], [Regular Base Price], [Round By], rfs.rules_fk,
  case when rao.RULES_ACTION_OPERANDS_ID is not null then ec.result else null end as [Basis Calc],
 --case when rao.RULES_ACTION_OPERANDS_ID is not null then ecr.BASIS_CALC else null end as [Basis Calc],
 r.host_precedence_level, r.Promo_Ind,
 ec_corp.result Corp_Store_Specific_Sell_Price, ec.result Store_Specific_Sell_Price
from ##sTransProdList pl 
inner join custom.V_AllPrices ap on ap.PRODUCT_STRUCTURE_FK = pl.PRODUCT_STRUCTURE_FK and ap.ORG_ENTITY_STRUCTURE_FK = pl.org_entity_structure_fk
left outer join 
(
select Product_STructure_fk, Org_entity_structure_fk ,[Lot Qty A],[Lot Qty B],[Regular End User Qty 1],[Regular End User Qty 2]
		 FROM
(
		select PRODUCT_STRUCTURE_FK as Product_Structure_FK, Org_entity_structure_fk, dn.label as DataName, ATTRIBUTE_EVENT_DATA as DataValue 
		FROM epacube.PRODUCT_ATTRIBUTE pa with (nolock) 
		INNER JOIN epacube.DATA_NAME dn with (nolock) on pa.DATA_NAME_FK = dn.DATA_NAME_ID 
		where DATA_NAME_FK in (100100410,100100411,100100402,100100403)    ---,100100396,100100397  removed oct 1, 2014

		) As P
		PIVOT (
		Max(P.DataValue) FOR DataName IN ([Lot Qty A],[Lot Qty B],[Regular End User Qty 1],[Regular End User Qty 2])
		) 
		As PivotTable
) w_qty on w_qty.PRODUCT_STRUCTURE_FK = pl.PRODUCT_STRUCTURE_FK and w_qty.ORG_ENTITY_STRUCTURE_FK = pl.org_entity_structure_fk

left outer join (select Product_STructure_fk , [JS Prod ID], [UPC],[Vend Prod],[Promo Page #],[AOC Vend #],[REU 1 QTY],
		[REU 2 QTY],[Descr 1],[Vend Std Pk],[Catalog Page],round([Wt],2) as [Wt],[Seasonal CD],[User Desc],[Store PCAT],[MSDS Sht #],[Memo],[z_icsp.u_hazcode],
		[Discontinued Ind],[On Flyers],[Dept]
		--,[z_icsp.u_freight1]
		,[Frt Cd 1]
		,[End User Each Published Flag],
		[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Frt Cd 2],[Serialized Product Flag],[Cert Cd],[Taxable Tool Flag],[WEB_NODE_ID],
		[Web Node Desc],[EDI Part No],[Country of Origin],[Return Flag],[Round By],[Transmit Flag],[CORP Base Price Effective Date],[CORP Base Price],[CORP DS 1 QTY]
		 FROM
(select distinct pid.Product_structure_FK as Product_Structure_FK, dn.short_name as Dataname, value as DataValue 
		from epacube.PRODUCT_IDENTIFICATION pid with (nolock) 
		inner join epacube.DATA_NAME dn with (nolock) on pid.DATA_NAME_FK = dn.DATA_NAME_ID 
		where pid.DATA_NAME_FK = 110100 
		UNION select pd.product_structure_fk, dn.short_name, pd.description from epacube.PRODUCT_DESCRIPTION pd with (nolock)
		inner join epacube.data_name dn with (nolock) on pd.data_name_fk = dn.DATA_NAME_ID
		where pd.DATA_NAME_FK in (110401,100100360)
		UNION select pid.Product_structure_FK, dn.short_name, value 
		from epacube.PRODUCT_IDENTIFICATION pid with (nolock) inner join epacube.DATA_NAME dn with (nolock) on pid.DATA_NAME_FK = dn.DATA_NAME_ID 
		WHERE pid.DATA_NAME_FK in (110111)
		UNION select pin.Product_structure_FK, dn.short_name, value 
		from epacube.PRODUCT_IDENT_NONUNIQUE pin with (nolock) inner join epacube.DATA_NAME dn with (nolock) on pin.DATA_NAME_FK = dn.DATA_NAME_ID 
		WHERE pin.DATA_NAME_FK in (110102,100100197)
		UNION
		SELECT pc.product_structure_Fk, dn.short_name, dv.value from epacube.PRODUCT_CATEGORY pc with (nolock)
		inner join epacube.DATA_VALUE dv with (nolock) on pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
		inner join epacube.data_name dn with (nolock) on pc.data_name_fk = dn.DATA_NAME_ID
		where pc.DATA_NAME_FK in (100100415,100100161,100100100)
		--change to get from new allprices
		UNION select PRODUCT_STRUCTURE_FK, 'CORP Base Price', cast(NET_VALUE3 as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE with (nolock) where DATA_NAME_FK = 100100416 AND ORG_ENTITY_STRUCTURE_FK = 11794--DN100100417
		UNION select PRODUCT_STRUCTURE_FK, 'CORP Base Price Effective Date', cast(cast(effective_date as date) as varchar(1000)) from marginmgr.SHEET_RESULTS_VALUES_FUTURE with (nolock) where DATA_NAME_FK = 100100416 AND ORG_ENTITY_STRUCTURE_FK = 11794--DN100100417
		UNION
		select PRODUCT_STRUCTURE_FK as ProductKey, 'CORP DS 1 QTY', ATTRIBUTE_EVENT_DATA  
		FROM epacube.PRODUCT_ATTRIBUTE pa with (nolock) 
		where DATA_NAME_FK in (100100296) and ORG_ENTITY_STRUCTURE_FK = 11794
		UNION
		SELECT pc.product_structure_fk, 'Web Node Desc', dv.DESCRIPTION from epacube.PRODUCT_CATEGORY pc with (nolock)
		inner join epacube.DATA_VALUE dv with (nolock) on pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
		inner join epacube.data_name dn with (nolock) on pc.data_name_fk = dn.DATA_NAME_ID
		where pc.DATA_NAME_FK in (100100100)
		UNION
		select PRODUCT_STRUCTURE_FK as ProductKey, dn.short_name as DataName, ATTRIBUTE_EVENT_DATA as DataValue 
		FROM epacube.PRODUCT_ATTRIBUTE pa with (nolock)
		INNER JOIN epacube.DATA_NAME dn with (nolock) on pa.DATA_NAME_FK = dn.DATA_NAME_ID 
		where DATA_NAME_FK in (100100287,100100110,100100389,100100154,110815,100100342,110817,100100272,100100376,
		100100163,100100374,100100219,100100220,100100221,100100375,100100343,100100414,100100356,100100158,100100341,210831,100100358)
		UNION
		SELECT PRODUCT_STRUCTURE_FK, 'On Flyers', 
		SUBSTRING( (  SELECT ( ' ' + dv.VALUE)  from epacube.PRODUCT_MULT_TYPE t2 with (nolock)
		inner join epacube.DATA_VALUE dv with (nolock) on t2.DATA_VALUE_FK = dv.DATA_VALUE_ID 
		WHERE t1.PRODUCT_STRUCTURE_FK= t2.PRODUCT_STRUCTURE_FK and t2.DATA_NAME_FK = 100100276
		ORDER BY t1.PRODUCT_STRUCTURE_FK, t2.PRODUCT_STRUCTURE_FK 
		FOR XML PATH('') ), 2, 1000) 
		from epacube.PRODUCT_MULT_TYPE t1 with (nolock)
		inner join epacube.DATA_VALUE dv with (nolock) on t1.DATA_VALUE_FK = dv.DATA_VALUE_ID 
		GROUP BY PRODUCT_STRUCTURE_FK 

		) As P


		PIVOT (
		Max(P.DataValue) FOR DataName IN ([JS Prod ID], [UPC],[Vend Prod],[Promo Page #],[AOC Vend #],[REU 1 QTY],
		[REU 2 QTY],[Descr 1],[Vend Std Pk],[Catalog Page],[Wt],[Seasonal CD],[User Desc],[Store PCAT],[MSDS Sht #],[Memo],[z_icsp.u_hazcode],
		[Discontinued Ind],[On Flyers],[Dept]
		,[Frt Cd 1]
		,[End User Each Published Flag],
		[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Frt Cd 2],[Serialized Product Flag],[Cert Cd],[Taxable Tool Flag],[WEB_NODE_ID],
		[Web Node Desc],[EDI Part No],[Country of Origin],[Return Flag],[Round By],[Transmit Flag],[CORP Base Price],[CORP DS 1 QTY],
		[CORP Base Price Effective Date])
		) 
		As PivotTable)
		pt on pl.product_structure_fk = pt.Product_Structure_FK

left outer join synchronizer.event_calc ec with (nolock) on pl.product_structure_fk = ec.product_structure_fk and pl.org_entity_structure_fk = ec.org_entity_structure_fk 
				and pl.cust_entity_structure_fk = ec.cust_entity_structure_fk and ec.RESULT_DATA_NAME_FK = 111602 and ec.JOB_FK = @Job_FK	
left outer join synchronizer.event_calc ec_corp with (nolock) on pl.product_structure_fk = ec_corp.product_structure_fk and ec_corp.org_entity_structure_fk = 11794
				and pl.cust_entity_structure_fk = ec_corp.cust_entity_structure_fk and ec_corp.RESULT_DATA_NAME_FK = 111602 and ec_corp.JOB_FK = @Job_FK	
						 
left join synchronizer.rules_action ra with (nolock) on ec.rules_fk = ra.rules_fk
left join synchronizer.rules_action_operands rao with (nolock) on ra.rules_action_id = rao.rules_action_fk and rao.operand_filter_dn_fk = 107000
left join synchronizer.rules_filter_set rfs with (nolock) on ec.rules_fk = rfs.rules_fk
left join synchronizer.rules_filter rf_o with (nolock) on rfs.ORG_FILTER_FK = rf_o.rules_filter_id
left join synchronizer.rules r with (nolock) on ec.rules_fk = r.RULES_ID 
left join synchronizer.event_calc_rules ecr with (nolock) on ec.EVENT_ID = ecr.EVENT_FK and ecr.result_rank = 1
where pl.job_fk = @Job_FK and pl.org_entity_structure_fk <> 11794

Create index idx_stt on ##sTransTable([JS_STRAN_ID], [Product_Structure_FK], [ORG_ENTITY_STRUCTURE_FK], [cust_entity_structure_fk])

insert into custom.JS_sTran_History 
( [Product_Structure_FK],[job_fk], [calc_job_fk],[ORG_ENTITY_STRUCTURE_FK],[cust_entity_structure_fk],[Store],[DC],[Store Transmit Flag],[RESULT],PD_Effective_Date
,[Product Transmit Flag],[Promo Page #],[Product Number],[Changes Flag],[Lot Pricer Flag],[Regular Base from DC],[AOC Vendor],[Selling Price Each],[Promo EU Ea],[Lot 1 Qty],[Lot 1 Sell Price]
,[Lot 2 Qty],[Lot 2 Sell Price],[Product Description],[Standard Pack],[Catalog Page Number],[Lot A QTY],[Lot A Sell Price],[Lot B QTY],[Lot B Sell Price],[CORP Base Price],[CORP DropShipQty]
,[Weight],[Season Code],[Customer Description],[Dept-Cat],[MSDS Indicator],[Memo Field],[Hazardous Material Ship Code],[Discontinued Flag],[Flyer Month Code],[Buyers Code],[Freight Code]
,[Vendor Bar Code 1],[Reg Base from DC Eff Date],[Direct Cost Effective Date],[End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Manufacturer Part Number]
,[Freight Code #2],[Append Date],[QtyBrk#2],[QtyBrk$1],[QtyBrk$2],[Serialized Product Flag],[Certification Required Flag],[Taxable Flag],[End User Regular Price],[List Each],[List Lot 1]
,[List Lot 2],[WEB_NODE_ID],[Web Node Data],[EDI Part No],[Vendor No],[Country of Origin],[Return Flag],[Regular DC Cost],[DC Sell Qty],[Rules_FK], [Basis Calc],[Host Precedence Level], IsPromoFlag, [Corp SSP], [Store SSP])
Select STT.[Product_Structure_FK],[job_fk], [calc_job_fk],STT.[ORG_ENTITY_STRUCTURE_FK], STT.[cust_entity_structure_fk],[Store],[DC],[Store Transmit Flag],[RESULT],PD_Effective_Date
,[Product Transmit Flag],[Promo Page #],[Product Number],[Changes Flag],[Lot Pricer Flag],[Regular Base from DC],[AOC Vendor],[Selling Price Each],[Promo EU Ea],[Lot 1 Qty],[Lot 1 Sell Price]
,[Lot 2 Qty],[Lot 2 Sell Price],[Product Description],[Standard Pack],[Catalog Page Number],[Lot A QTY],[Lot A Sell Price],[Lot B QTY],[Lot B Sell Price],[CORP Base Price],[CORP DropShipQty]
,[Weight],[Season Code],[Customer Description],[Dept-Cat],[MSDS Indicator],[Memo Field],[Hazardous Material Ship Code],[Discontinued Flag],[Flyer Month Code],[Buyers Code],[Freight Code]
,[Vendor Bar Code 1],[Reg Base from DC Eff Date],[Direct Cost Effective Date],[End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Manufacturer Part Number]
,[Freight Code #2],[Append Date],[QtyBrk#2],[QtyBrk$1],[QtyBrk$2],[Serialized Product Flag],[Certification Required Flag],[Taxable Flag],[End User Regular Price],[List Each],[List Lot 1]
,[List Lot 2],[WEB_NODE_ID],[Web Node Data],[EDI Part No],[Vendor No],[Country of Origin],[Return Flag],[Regular DC Cost],[DC Sell Qty],[Rules_FK], [Basis Calc],[Host Precedence Level], IsPromoFlag, [Corp SSP], [Store SSP]
from ##sTransTable STT
inner join (Select Max([JS_STRAN_ID]) 'Max_JS_STRAN_ID', [Product_Structure_FK], [ORG_ENTITY_STRUCTURE_FK], [cust_entity_structure_fk] from ##sTransTable group by [Product_Structure_FK], [ORG_ENTITY_STRUCTURE_FK], [cust_entity_structure_fk]) STT2
	on STT.JS_STRAN_ID = STT2.Max_JS_STRAN_ID and STT.Product_Structure_FK = STT2.Product_Structure_FK and STT.ORG_ENTITY_STRUCTURE_FK = STT2.ORG_ENTITY_STRUCTURE_FK and STT.cust_entity_structure_fk = STT2.cust_entity_structure_fk

--Capture any opportunity for Duplicate Records; duplicates were not loaded into table
insert into custom.JS_sTran_History_Dups 
( [JS_STRAN_ID], [Product_Structure_FK],[job_fk], [calc_job_fk],[ORG_ENTITY_STRUCTURE_FK],[cust_entity_structure_fk],[Store],[DC],[Store Transmit Flag],[RESULT],PD_Effective_Date
,[Product Transmit Flag],[Promo Page #],[Product Number],[Changes Flag],[Lot Pricer Flag],[Regular Base from DC],[AOC Vendor],[Selling Price Each],[Promo EU Ea],[Lot 1 Qty],[Lot 1 Sell Price]
,[Lot 2 Qty],[Lot 2 Sell Price],[Product Description],[Standard Pack],[Catalog Page Number],[Lot A QTY],[Lot A Sell Price],[Lot B QTY],[Lot B Sell Price],[CORP Base Price],[CORP DropShipQty]
,[Weight],[Season Code],[Customer Description],[Dept-Cat],[MSDS Indicator],[Memo Field],[Hazardous Material Ship Code],[Discontinued Flag],[Flyer Month Code],[Buyers Code],[Freight Code]
,[Vendor Bar Code 1],[Reg Base from DC Eff Date],[Direct Cost Effective Date],[End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Manufacturer Part Number]
,[Freight Code #2],[Append Date],[QtyBrk#2],[QtyBrk$1],[QtyBrk$2],[Serialized Product Flag],[Certification Required Flag],[Taxable Flag],[End User Regular Price],[List Each],[List Lot 1]
,[List Lot 2],[WEB_NODE_ID],[Web Node Data],[EDI Part No],[Vendor No],[Country of Origin],[Return Flag],[Regular DC Cost],[DC Sell Qty],[Rules_FK], [Basis Calc],[Host Precedence Level], IsPromoFlag, [Corp SSP], [Store SSP])
Select STT.JS_STRAN_ID, STT.[Product_Structure_FK],[job_fk], [calc_job_fk],STT.[ORG_ENTITY_STRUCTURE_FK], STT.[cust_entity_structure_fk],[Store],[DC],[Store Transmit Flag],[RESULT],PD_Effective_Date
,[Product Transmit Flag],[Promo Page #],[Product Number],[Changes Flag],[Lot Pricer Flag],[Regular Base from DC],[AOC Vendor],[Selling Price Each],[Promo EU Ea],[Lot 1 Qty],[Lot 1 Sell Price]
,[Lot 2 Qty],[Lot 2 Sell Price],[Product Description],[Standard Pack],[Catalog Page Number],[Lot A QTY],[Lot A Sell Price],[Lot B QTY],[Lot B Sell Price],[CORP Base Price],[CORP DropShipQty]
,[Weight],[Season Code],[Customer Description],[Dept-Cat],[MSDS Indicator],[Memo Field],[Hazardous Material Ship Code],[Discontinued Flag],[Flyer Month Code],[Buyers Code],[Freight Code]
,[Vendor Bar Code 1],[Reg Base from DC Eff Date],[Direct Cost Effective Date],[End User Each Published Flag],[End User Lot 1 Published Flag],[End User Lot 2 Published Flag],[Manufacturer Part Number]
,[Freight Code #2],[Append Date],[QtyBrk#2],[QtyBrk$1],[QtyBrk$2],[Serialized Product Flag],[Certification Required Flag],[Taxable Flag],[End User Regular Price],[List Each],[List Lot 1]
,[List Lot 2],[WEB_NODE_ID],[Web Node Data],[EDI Part No],[Vendor No],[Country of Origin],[Return Flag],[Regular DC Cost],[DC Sell Qty],[Rules_FK], [Basis Calc],[Host Precedence Level], IsPromoFlag, [Corp SSP], [Store SSP]
from ##sTransTable STT
inner join (Select [Product_Structure_FK], [ORG_ENTITY_STRUCTURE_FK], [cust_entity_structure_fk] from ##sTransTable group by [Product_Structure_FK], [ORG_ENTITY_STRUCTURE_FK], [cust_entity_structure_fk] Having count(*) > 1) STT2
	on STT.Product_Structure_FK = STT2.Product_Structure_FK and STT.ORG_ENTITY_STRUCTURE_FK = STT2.ORG_ENTITY_STRUCTURE_FK and STT.cust_entity_structure_fk = STT2.cust_entity_structure_fk

-----------------------------------------------------------------------------------------------------------------------
--Need to put the job Name to the correct name for next run so we get the correct timestamp
update common.job set name = 'sTrans File Export'
where job_id = @In_JobFK_int
-------------------------------------------------------------------------------------------------------------------------

	set @SP_Current_time = getdate()
	update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id
	Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'GENERATE FILES COMMAND ISSUED', null, null, null, 200, @SP_Current_time, null

	set @job_ex_id = (select job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name =  'GENERATE FILES COMMAND ISSUED')

SET @status_desc =  'Calling sTransFileGenerator Package - custom.SP_sTransGeneration - Job ' + @In_JobFK
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

DECLARE @cmd VARCHAR(4000)

SET @cmd = 'DTEXEC /ISServer \SSISDB\JohnStoneElectric\sTransFileGenerator\sTransFileGenerator.dtsx /SERVER "EPATEST" ' 

DECLARE @returncode int
EXEC @returncode = master..xp_cmdshell @cmd

WAITFOR DELAY '00:00:15'

	set @SP_Current_time = getdate()
	update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id
	Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'PURGE WORK TABLES', null, null, null, 200, @SP_Current_time, null

	set @job_ex_id = (select job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name =  'PURGE WORK TABLES')

SET @status_desc =  'Calling marginmgr.util_purge_margin_tables SP from - custom.SP_sTransGeneration - Job ' + @In_JobFK
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;

-------------------------------------------------------------------------
---added for logging

Insert into dbo.a_event_calc 
([EVENT_ID], [JOB_FK], [RESULT_DATA_NAME_FK], [RESULT_TYPE_CR_FK], [RESULT_EFFECTIVE_DATE], [PRODUCT_STRUCTURE_FK], [ORG_ENTITY_STRUCTURE_FK], [CUST_ENTITY_STRUCTURE_FK], [SUPL_ENTITY_STRUCTURE_FK], [RESULT], [RULES_FK], [RULE_TYPE_CR_FK], [CALC_GROUP_SEQ], [PARITY_DV_FK], [MODEL_SALES_QTY], [VALUE_UOM_CODE_FK], [PRICING_COST_BASIS_AMT], [MARGIN_COST_BASIS_AMT], [ORDER_COGS_AMT], [REBATED_COST_CB_AMT], [REBATE_CB_AMT], [REBATE_BUY_AMT], [SELL_PRICE_CUST_AMT], [MARGIN_AMT], [MARGIN_PCT], [RELATED_EVENT_FK], [RESULT_PARENT_DATA_NAME_FK], [COLUMN_NAME], [BATCH_NO], [EVENT_SOURCE_CR_FK], [EVENT_CONDITION_CR_FK], [RECORD_STATUS_CR_FK], [UPDATE_TIMESTAMP], [UPDATE_USER], [UPDATE_LOG_FK], [EFF_END_DATE])
Select [EVENT_ID], [JOB_FK], [RESULT_DATA_NAME_FK], [RESULT_TYPE_CR_FK], [RESULT_EFFECTIVE_DATE], [PRODUCT_STRUCTURE_FK], [ORG_ENTITY_STRUCTURE_FK], [CUST_ENTITY_STRUCTURE_FK], [SUPL_ENTITY_STRUCTURE_FK], [RESULT], [RULES_FK], [RULE_TYPE_CR_FK], [CALC_GROUP_SEQ], [PARITY_DV_FK], [MODEL_SALES_QTY], [VALUE_UOM_CODE_FK], [PRICING_COST_BASIS_AMT], [MARGIN_COST_BASIS_AMT], [ORDER_COGS_AMT], [REBATED_COST_CB_AMT], [REBATE_CB_AMT], [REBATE_BUY_AMT], [SELL_PRICE_CUST_AMT], [MARGIN_AMT], [MARGIN_PCT], [RELATED_EVENT_FK], [RESULT_PARENT_DATA_NAME_FK], [COLUMN_NAME], [BATCH_NO], [EVENT_SOURCE_CR_FK], [EVENT_CONDITION_CR_FK], [RECORD_STATUS_CR_FK], [UPDATE_TIMESTAMP], [UPDATE_USER], [UPDATE_LOG_FK], [EFF_END_DATE]
from synchronizer.event_calc with (nolock) Where Job_FK = @Job_FK

Insert into dbo.a_event_calc_Rules
([EVENT_RULES_ID], [EVENT_FK], [RULES_FK], [RESULT_DATA_NAME_FK], [RESULT_TYPE_CR_FK], [RESULT_EFFECTIVE_DATE], [RESULT_RANK], [RESULT], [RESULT1], [RESULT2], [RESULT3], [RULES_OPTIMIZATION_INCR_OPERAND], [BASIS_CALC], [BASIS1], [BASIS2], [BASIS3], [NUMBER1], [NUMBER2], [NUMBER3], [OPERATION1], [OPERATION2], [OPERATION3], [BASIS_CALC_NAME], [BASIS1_NAME], [BASIS2_NAME], [BASIS3_NAME], [BASIS_SOURCE1], [BASIS_SOURCE2], [BASIS_SOURCE3], [HOST_RULE_XREF], [RULES_RESULT_TYPE_CR_FK], [RULES_EFFECTIVE_DATE], [PROD_FILTER], [CUST_FILTER], [ORG_FILTER], [SUPL_FILTER], [RULES_CONTRACT_CUSTOMER_FK], [RELATED_RULES_FK], [RULE_PRECEDENCE], [CONTRACT_PRECEDENCE], [SCHED_PRECEDENCE], [STRUC_PRECEDENCE], [STRUC_GRP_PRECEDENCE], [STRUCTURE_GRP_RANK], [STRUCTURE_RANK], [SCHEDULE_RANK], [RESULT_MTX_ACTION_CR_FK], [STRUC_GRP_MTX_ACTION_CR_FK], [STRUC_MTX_ACTION_CR_FK], [SCHED_MTX_ACTION_CR_FK], [CALC_GROUP_SEQ], [RULES_SCHEDULE_FK], [RULES_STRUCTURE_FK], [RULES_STRUCTURE_GROUP_FK], [RULES_ACTION_FK], [RULES_ACTION_OPERATION1_FK], [RULES_ACTION_OPERATION2_FK], [RULES_ACTION_OPERATION3_FK], [RULES_OPTIMIZATION_INCREMENT_FK], [PROD_FILTER_FK], [PROD_RESULT_TYPE_CR_FK], [PROD_EFFECTIVE_DATE], [PROD_EVENT_FK], [PROD_ORG_ENTITY_STRUCTURE_FK], [PROD_CORP_IND], [PROD_ORG_IND], [PROD_FILTER_HIERARCHY], [PROD_FILTER_ORG_PRECEDENCE], [ORG_FILTER_FK], [ORG_RESULT_TYPE_CR_FK], [ORG_EFFECTIVE_DATE], [ORG_EVENT_FK], [CUST_FILTER_FK], [CUST_RESULT_TYPE_CR_FK], [CUST_EFFECTIVE_DATE], [CUST_EVENT_FK], [SUPL_FILTER_FK], [SUPL_RESULT_TYPE_CR_FK], [SUPL_EFFECTIVE_DATE], [SUPL_EVENT_FK], [DISQUALIFIED_IND], [DISQUALIFIED_COMMENT], [RECORD_STATUS_CR_FK], [UPDATE_TIMESTAMP])
Select [EVENT_RULES_ID], [EVENT_FK], [RULES_FK], [RESULT_DATA_NAME_FK], [RESULT_TYPE_CR_FK], [RESULT_EFFECTIVE_DATE], [RESULT_RANK], [RESULT], [RESULT1], [RESULT2], [RESULT3], [RULES_OPTIMIZATION_INCR_OPERAND], [BASIS_CALC], [BASIS1], [BASIS2], [BASIS3], [NUMBER1], [NUMBER2], [NUMBER3], [OPERATION1], [OPERATION2], [OPERATION3], [BASIS_CALC_NAME], [BASIS1_NAME], [BASIS2_NAME], [BASIS3_NAME], [BASIS_SOURCE1], [BASIS_SOURCE2], [BASIS_SOURCE3], [HOST_RULE_XREF], [RULES_RESULT_TYPE_CR_FK], [RULES_EFFECTIVE_DATE], [PROD_FILTER], [CUST_FILTER], [ORG_FILTER], [SUPL_FILTER], [RULES_CONTRACT_CUSTOMER_FK], [RELATED_RULES_FK], [RULE_PRECEDENCE], [CONTRACT_PRECEDENCE], [SCHED_PRECEDENCE], [STRUC_PRECEDENCE], [STRUC_GRP_PRECEDENCE], [STRUCTURE_GRP_RANK], [STRUCTURE_RANK], [SCHEDULE_RANK], [RESULT_MTX_ACTION_CR_FK], [STRUC_GRP_MTX_ACTION_CR_FK], [STRUC_MTX_ACTION_CR_FK], [SCHED_MTX_ACTION_CR_FK], [CALC_GROUP_SEQ], [RULES_SCHEDULE_FK], [RULES_STRUCTURE_FK], [RULES_STRUCTURE_GROUP_FK], [RULES_ACTION_FK], [RULES_ACTION_OPERATION1_FK], [RULES_ACTION_OPERATION2_FK], [RULES_ACTION_OPERATION3_FK], [RULES_OPTIMIZATION_INCREMENT_FK], [PROD_FILTER_FK], [PROD_RESULT_TYPE_CR_FK], [PROD_EFFECTIVE_DATE], [PROD_EVENT_FK], [PROD_ORG_ENTITY_STRUCTURE_FK], [PROD_CORP_IND], [PROD_ORG_IND], [PROD_FILTER_HIERARCHY], [PROD_FILTER_ORG_PRECEDENCE], [ORG_FILTER_FK], [ORG_RESULT_TYPE_CR_FK], [ORG_EFFECTIVE_DATE], [ORG_EVENT_FK], [CUST_FILTER_FK], [CUST_RESULT_TYPE_CR_FK], [CUST_EFFECTIVE_DATE], [CUST_EVENT_FK], [SUPL_FILTER_FK], [SUPL_RESULT_TYPE_CR_FK], [SUPL_EFFECTIVE_DATE], [SUPL_EVENT_FK], [DISQUALIFIED_IND], [DISQUALIFIED_COMMENT], [RECORD_STATUS_CR_FK], [UPDATE_TIMESTAMP] 
from synchronizer.event_calc_rules where event_fk in (select event_id from synchronizer.event_calc where job_fk = @Job_FK)

Insert into dbo.a_event_calc_Rules_basis
Select [EVENT_RULES_BASIS_ID], [EVENT_RULES_FK], [BASIS_POSITION], [BASIS_RANK], [BASIS_VALUE], [BASIS_DN_FK], [BASIS_PRICESHEET_NAME], [BASIS_PRICESHEET_DATE], [BASIS_OPERAND], [RULES_FILTER_LOOKUP_OPERAND_FK], [BASIS_OPERAND_FILTER_DN_FK], [BASIS_OPERAND_FILTER_VALUE], [BASIS_OPERAND_POSITION], [BASIS_OPERAND_FILTER_OPERATOR_CR_FK], [BASIS_ORG_PRECEDENCE], [BASIS_RESULT_TYPE_CR_FK], [BASIS_ACTION_CR_FK], [BASIS_EFFECTIVE_DATE], [BASIS_EVENT_FK], [BASIS_ORG_ENTITY_STRUCTURE_FK], [BASIS_CUST_ENTITY_STRUCTURE_FK], [BASIS_SUPL_ENTITY_STRUCTURE_FK], [RECORD_STATUS_CR_FK], [UPDATE_TIMESTAMP] 
from synchronizer.event_calc_rules_basis where event_rules_fk in (Select event_rules_id from synchronizer.event_calc_rules where event_fk in (select event_id from synchronizer.event_calc where job_fk = @Job_FK))

----------------------------------------------------------------
exec marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, null
----------------------------------------------------------------

IF OBJECT_ID('tempdb..#sTransDups0') IS NOT NULL
Drop Table #sTransDups0

IF OBJECT_ID('tempdb..#sTransDups') IS NOT NULL
Drop Table #sTransDups

IF OBJECT_ID('tempdb..#Dups') IS NOT NULL
Drop Table #Dups

IF OBJECT_ID('tempdb..##SH_Calcs') IS NOT NULL
Drop Table ##SH_Calcs

IF OBJECT_ID('tempdb..##sTransTable') IS NOT NULL
Drop table ##sTransTable

set @SP_Current_time = getdate()
update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id

set @job_ex_id = (select job_execution_id from common.JOB_EXECUTION where JOB_FK = @SP_Out_Job_FK and name = 'START PROCESSING')
update common.JOB_EXECUTION set JOB_STATUS_CR_FK = 201, END_TIMESTAMP = @SP_Current_time where JOB_EXECUTION_id = @job_ex_id

set @SP_End_Time = getdate()
 exec common.job_execution_create  @SP_Out_Job_FK, 'EXPORT COMPLETE', null, null, null,201, @SP_Start_Time, @SP_End_Time;

 SET @status_desc =  'Execution finished for custom.SP_sTransGeneration - Job ' + @In_JobFK
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @SP_Out_Job_FK;


  END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

	  set @SP_End_Time = getdate()
	  Exec common.JOB_EXECUTION_CREATE @SP_Out_Job_FK, 'JOB FAILED', null, null, null, 202, @SP_Current_time, @SP_End_Time


		SELECT 
			@ErrorMessage = 'Execution of custom.SP_sTransGeneration has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @SP_Out_Job_FK
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );

		exec marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, null

   END CATCH;
    
END 

















