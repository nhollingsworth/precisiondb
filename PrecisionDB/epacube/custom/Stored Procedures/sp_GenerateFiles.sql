﻿




CREATE PROCEDURE [custom].[sp_GenerateFiles]
    (
      @Run_sProdTran bit
      
    )
AS


    BEGIN
	declare @ls_queue varchar(max)
	declare @epa_job_id bigint
    DECLARE @v_job_fk     bigint

	DBCC DBREINDEX('[synchronizer].[RULES]', '', 90)
	DBCC DBREINDEX('[synchronizer].[RULES_ACTION]', '', 90)
	DBCC DBREINDEX('[synchronizer].[RULES_FILTER]', '', 90)
	DBCC DBREINDEX('[synchronizer].[RULES_FILTER_SET]', '', 90)

	






	   
    if (@Run_sProdTran = 1)
	begin
	    Exec [common].[JOB_CREATE] Null, 226, 'sProdTrans File Export', Null, Null, Null, Null, @v_job_fk Output
		SET @ls_queue = 'EXEC epacube.custom.sp_sProdTransGeneration ' + CAST ( @v_job_fk AS VARCHAR(16)) 
	end
	else
	begin
	    Exec [common].[JOB_CREATE] Null, 226, 'Launch sTrans File Export', Null, Null, Null, Null, @v_job_fk Output
		SET @ls_queue = 'EXEC epacube.custom.sp_sTransGeneration ' + CAST ( @v_job_fk AS VARCHAR(16)) 
	end

EXEC queue_manager.enqueue
	@service_name = N'TargetEpaService', --  nvarchar(50)
	@cmd_text = @ls_queue, --  nvarchar(4000)
	@epa_job_id = @v_job_fk, --  int
	@cmd_type = N'SQL' --  nvarchar(10)
    END ;
  




