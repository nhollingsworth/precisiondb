﻿









-- Copyright 2006 epaCUBE, Inc.
--
-- Procedure created by Walt Tucholski
--
--
-- 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV       09/11/2014   Initial SQL Version - 
-- CV       01/16/2015   Add event data history table to logic - SUP - 5140

CREATE PROCEDURE [custom].[CLEANUP_FUTURE_VEND_COST] (@v_job_fk  bigint)
AS
BEGIN

        DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @job_timestamp DATETIME

--DECLARE @v_job_fk     bigint

DECLARE @in_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

		

        BEGIN TRY
            SET NOCOUNT ON ;
            SET @l_exec_no = 0
            SET @l_sysdate = GETDATE()
		
						
			
			set @in_job_fk =(select value from epacube.config_params where scope = 'cleanup')
			--set @v_job_fk = (select value from epacube.config_params where scope = 'cleanup')

							
            SET @ls_stmt = 'started execution of custom.CLEANUP_FUTURE_VEND_COST. '
                
            EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                @exec_id = @l_exec_no OUTPUT ;
                
-------------------------------------------------------------------------------
   ---create job
------------------------------------------------------------------------------             
    --              EXEC seq_manager.db_get_next_sequence_value_impl 'common',
    --                'job_seq', @v_job_fk output
   

    --SET @v_job_class_fk  = 100        --- PRODUCT MAINTENANCE
    --SET @v_job_user = 'EPACUBE'
    --SET @v_job_name = 'CLEANUP FUTURE VEND COST'
    --SET @v_job_date = @l_sysdate
    --SET @v_data1   = NULL
    --SET @v_data2   = @v_last_job_date
    
    --EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
				--						   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

    --EXEC common.job_execution_create   @v_job_fk, 'START CLEAN UP',
		  --                               @v_input_rows, @v_output_rows, @v_exception_rows,
				--						 200, @l_sysdate, @l_sysdate  
  

  -----------------------------------------------------------------------------------------
  --first remove values from table for this job
  -------------------------------------------------------------------------------------------

			DELETE from marginmgr.EFFECTIVE_DATES_BY_DATA_NAME where import_job_fk = @in_job_fk

  -----------------------------------------------------------------------------------------------
  --Create Fake Events so the Future Calc will pickup and recalc changes
  --------------------------------------------------------------------------------------------------
  
INSERT INTO [synchronizer].[EVENT_DATA_HISTORY]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_FILENAME]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
    
  select distinct 151, 10109, getdate(),   PRODUCT_STRUCTURE_FK, 100100500, PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK 
  ,97,81,66,71
  , @v_job_fk
  , 'Cleanup future vendor cost', getdate(), 'CLEANUP'
  from synchronizer.EVENT_DATA  with (nolock) 
   where import_job_fk = @in_job_fk

 

  -------------------------------------------------------------------------------------------------
  --Reject all events
  -------------------------------------------------------------------------------------------------
  
 
Update synchronizer.EVENT_DATA 
set EVENT_STATUS_CR_FK = 82 -- reject 
where EVENT_STATUS_CR_FK in (84, 85)
and IMPORT_JOB_FK = @in_job_fk


  -------------------------------------------------------------------------------------------------
  --last remove the data from the future table.
  -------------------------------------------------------------------------------------------------

                delete from marginmgr.SHEET_RESULTS_VALUES_FUTURE 
                 where SHEET_RESULTS_VALUES_FUTURE_ID in (select distinct SHEET_RESULTS_VALUES_FUTURE_ID from marginmgr.SHEET_RESULTS_VALUES_FUTURE F with (nolock)
  inner join synchronizer.EVENT_DATA ed with (nolock) on (f.PRODUCT_STRUCTURE_FK = ed.PRODUCT_STRUCTURE_FK and f.ORG_ENTITY_STRUCTURE_FK = ed.ORG_ENTITY_STRUCTURE_FK
  and ed.IMPORT_JOB_FK = @in_job_fk)
  where 1 = 1
  and f.DATA_NAME_FK not in (100100500, 100100520))


    delete from marginmgr.SHEET_RESULTS_VALUES_FUTURE 
                 where SHEET_RESULTS_VALUES_FUTURE_ID in (select distinct SHEET_RESULTS_VALUES_FUTURE_ID from marginmgr.SHEET_RESULTS_VALUES_FUTURE F with (nolock)
  inner join synchronizer.EVENT_DATA_history ed with (nolock) on (f.PRODUCT_STRUCTURE_FK = ed.PRODUCT_STRUCTURE_FK and f.ORG_ENTITY_STRUCTURE_FK = ed.ORG_ENTITY_STRUCTURE_FK
  and ed.IMPORT_JOB_FK = @in_job_fk)
  where 1 = 1
  and f.DATA_NAME_FK not in (100100500, 100100520))

---------------------------------------------------------------------------------------------------------------------
  
---------------------------------------------------------------------------------------------------------------------
---RUN FUTURE CALC
---------------------------------------------------------------------------------------------------------------------
	
			
EXECUTE [marginmgr].[FUTURE_RESULTS_AND_VALUES] 


--------------------------------------------------------------------------------------------

	EXEC job_execution_create   @v_job_fk, 'COMPLETED CLEAN UP',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @l_sysdate, @l_sysdate  
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

            SET @status_desc = 'finished execution of custom.CLEANUP_FUTURE_VEND_COST'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;

        END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of custom.CLEANUP_FUTURE_VEND_COST has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            EXEC exec_monitor.email_error_sp @l_exec_no,
                'dbexceptions@epacube.com' ;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH
		  
    END










