﻿






-- Copyright 2008
--
-- Procedure Called from Quartz to submit result calc job
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -----------------------------------------------
-- CV        09/12/2014   Revised Original SQL Version to include Queue
--
CREATE PROCEDURE [custom].[CLEANUP_FUTURE_VEND_COST_QUARTZ]
  AS
BEGIN

DECLARE @l_sysdate    datetime
DECLARE @ls_stmt      varchar (1000)
DECLARE @ls_queue     nvarchar (4000)
DECLARE @l_exec_no    bigint
DECLARE @l_rows_processed    BIGINT

DECLARE @v_job_fk     bigint
DECLARE @in_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

DECLARE @status_desc  varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

BEGIN TRY
SET NOCOUNT ON
SET @l_sysdate = getdate ()

SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of custom.Cleanup_Future_Vend_Cost_QUARTZ', 
												@exec_id = @l_exec_no OUTPUT;


set @in_job_fk =(select value from epacube.config_params where scope = 'cleanup')
/**********************************************************************************/
--   CREATE NEW JOB 
/**********************************************************************************/

    EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'job_seq', @v_job_fk output
   

    SET @v_job_class_fk  = 100        --- PRODUCT MAINTENANCE
    SET @v_job_user = 'EPACUBE'
    SET @v_job_name = 'CLEANUP FUTURE VEND COST'
    SET @v_job_date = @l_sysdate
    SET @v_data1   =  @in_job_fk 
    SET @v_data2   = @v_last_job_date
    
    EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
										   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

    EXEC common.job_execution_create   @v_job_fk, 'START CLEAN UP',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  



-------------------------------------------------------------------------------------
--   Call Calc Job on the Queue <<<<<------ Need to ENQUEUE HERE 
-------------------------------------------------------------------------------------

	EXEC custom.CLEANUP_FUTURE_VEND_COST @V_OUT_JOB_FK


--SET @ls_queue = 'EXEC custom.CLEANUP_FUTURE_VEND_COST @v_job_fk = '
--              + CAST (@V_OUT_JOB_FK AS VARCHAR(16)) 
 
         


--INSERT INTO COMMON.T_SQL
--    ( TS,  SQL_TEXT )
--VALUES ( GETDATE(), ISNULL ( @ls_queue, 'NULL:: custom.CLEANUP_FUTURE_VEND_COST'  ) )


--EXEC queue_manager.enqueue
--	@service_name = N'TargetEpaService', --  nvarchar(50)
--	@cmd_text = @ls_queue, --  nvarchar(4000)
--	@epa_job_id = @v_job_fk, --  int
--	@cmd_type = N'SQL' --  nvarchar(10)




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of custom.CLEANUP_FUTURE_VEND_COST_QUARTZ.'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
Exec exec_monitor.print_status_sp  @l_exec_no;


END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of custom.CLEANUP_FUTURE_VEND_COST_QUARTZ. has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END




















