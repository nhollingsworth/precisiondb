﻿
-- =============================================
-- Author:		Gary Stone
-- Create date: April, 2013
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [custom].[Create_Static_Product_Details_Table_SB]	
AS
BEGIN

Declare @Job_FK Bigint

          EXEC [common].[JOB_CREATE] @IN_JOB_FK = NULL,
                 @IN_job_class_fk = 270,
                 @IN_JOB_NAME = 'Create Product Details Table',
                 @IN_DATA1 = NULL,
                 @IN_DATA2 = NULL,
                 @IN_DATA3 = NULL,
                 @IN_JOB_USER = NULL,
                 @OUT_JOB_FK = @Job_FK OUTPUT,
                 @CLIENT_APP = 'SSIS' ;

Declare @ServiceBrokerCall nvarchar(4000)

Set @ServiceBrokerCall = 'epacube.[custom].[Create_Static_Product_Details_Table] ' + Cast(@Job_FK as varchar(64))

	EXEC queue_manager.enqueue
	@service_name = N'TargetEpaService', --  nvarchar(50)
	@cmd_text = @ServiceBrokerCall, --  nvarchar(4000)
	@epa_job_id = @Job_FK, --  int
	@cmd_type = N'SQL' --  nvarchar(10)

END
