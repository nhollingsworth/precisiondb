﻿--A_epaMAUI_Johnstone_MM_PD_Record_Deletes_On_Import

CREATE PROCEDURE [custom].[Johnstone_MM_PD_Record_Deletes_On_Import] @host_Precedence_Level int
AS
BEGIN

	--Declare @host_Precedence_Level Int
	--Set @host_Precedence_Level = 7

	IF object_id('tempdb..#NewPDs') is not null
	drop table #NewPDs;

	IF object_id('tempdb..#Dels') is not null
	drop table #Dels;

	IF object_id('tempdb..#Inacts') is not null
	drop table #Inacts;

	IF object_id('tempdb..#PD7') is not null
	drop table #PD7;
	
	Select [Vendor #], [Start Date] into #NewPDs from [import].[Customer_Johnstone_MM_PD] group by [Vendor #], [Start Date]
	Create index idx_pd on #NewPDs([Vendor #], [Start Date])
	
	Select [Price Sheet #] into #PD7 from [import].[Customer_Johnstone_MM_Promo] group by [Price Sheet #]

	--Eliminate Records Not Yet Sent to SX
		select top 1 Cast(Null as bigint) rules_ID, Cast(Null as bigint) RESULT_DATA_NAME_FK, Cast(Null as bigint) MAUI_Rules_Maintenance_ID into #Dels 
		from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] where 1 = 2

		select top 1 Cast(Null as bigint) rules_ID, Cast(Null as bigint) RESULT_DATA_NAME_FK, Cast(Null as bigint) MAUI_Rules_Maintenance_ID into #Inacts 
		from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] where 1 = 2
	
	If @host_Precedence_Level = 1
	Begin
		Insert into #Dels
		select R.rules_ID, R.RESULT_DATA_NAME_FK, MRM.MAUI_Rules_Maintenance_ID 
		from #NewPDs NP with (nolock)
		Inner Join [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] IMMPR with (nolock) on NP.[Vendor #] = IMMPR.[VendorNo] and NP.[Start Date] = IMMPR.StartDate
		Inner Join dbo.maui_rules_maintenance MRM with (nolock) on IMMPR.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID = MRM.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_FK
		Inner join Synchronizer.rules r with (nolock) on MRM.host_rule_xref = R.host_rule_xref
		where MRM.host_rule_xref like 'New%' and MRM.Transferred_To_ERP = 0 

		Insert into #Dels
		select R.rules_ID, R.RESULT_DATA_NAME_FK, MRM.MAUI_Rules_Maintenance_ID 
		from dbo.maui_rules_maintenance MRM with (nolock)
		Inner Join [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] IMMPR with (nolock)  on MRM.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_FK = IMMPR.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID
		Inner join Synchronizer.rules r with (nolock) on MRM.host_rule_xref = R.host_rule_xref
		where 
		MRM.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_FK not in (Select IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE])
		and MRM.Transferred_To_ERP = 0
		--Or (rule_name like 'New:%' and IMPORT_MASS_MAINTENANCE_PRICE_REBATE_FK is null)

		Insert into #Dels
		Select R.rules_ID, R.RESULT_DATA_NAME_FK, MRM.MAUI_Rules_Maintenance_ID from [import].[Customer_Johnstone_MM_PD] CJMP with (nolock)
		inner join synchronizer.rules_filter rfc with (nolock) on CJMP.[6 Digit Store #] = rfc.value1 and rfc.entity_class_cr_fk = 10104
		inner join synchronizer.rules_filter rfp with (nolock) on CJMP.[Johnstone Stock #] = rfp.value1 and rfp.entity_class_cr_fk = 10109
		inner join synchronizer.rules_filter_set rfs with (nolock) on rfc.rules_filter_id = rfs.cust_filter_fk and rfp.RULES_FILTER_ID = rfs.PROD_FILTER_FK
		inner join synchronizer.rules r with (nolock) on rfs.rules_fk = r.rules_id and r.record_status_cr_fk = 1
		left join dbo.maui_rules_maintenance MRM with (nolock) on r.rules_id = MRM.rules_fk
		left join #Dels D on R.rules_ID = D.rules_ID
		Where 1 = 1
			and cast(r.EFFECTIVE_DATE as Date) = Cast(CJMP.[Start Date] as date)
			and r.[name] like 'New%'
			and D.Rules_ID is null
			and isnull(MRM.transferred_to_erp, 0) = 0

		Insert  into #Inacts
		select R.rules_ID, R.RESULT_DATA_NAME_FK, MRM.MAUI_Rules_Maintenance_ID from #NewPDs NP with (nolock)
		Inner Join [IMPORT].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] IMMPR with (nolock) on NP.[Vendor #] = IMMPR.[VendorNo] and NP.[Start Date] = IMMPR.StartDate
		Inner Join dbo.maui_rules_maintenance MRM with (nolock) on IMMPR.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_ID = MRM.IMPORT_MASS_MAINTENANCE_PRICE_REBATE_FK
		Inner join Synchronizer.rules r with (nolock) on MRM.host_rule_xref = R.host_rule_xref
		where MRM.Transferred_To_ERP = 1

		Insert into #Inacts
		Select R.rules_ID, R.RESULT_DATA_NAME_FK, MRM.MAUI_Rules_Maintenance_ID from [import].[Customer_Johnstone_MM_PD] CJMP with (nolock)
		inner join synchronizer.rules_filter rfc with (nolock) on CJMP.[6 Digit Store #] = rfc.value1 and rfc.entity_class_cr_fk = 10104
		inner join synchronizer.rules_filter rfp with (nolock) on CJMP.[Johnstone Stock #] = rfp.value1 and rfp.entity_class_cr_fk = 10109
		inner join synchronizer.rules_filter_set rfs with (nolock) on rfc.rules_filter_id = rfs.cust_filter_fk and rfp.RULES_FILTER_ID = rfs.PROD_FILTER_FK
		inner join synchronizer.rules r with (nolock) on rfs.rules_fk = r.rules_id and r.record_status_cr_fk = 1
		left join dbo.maui_rules_maintenance MRM with (nolock) on r.rules_id = MRM.rules_fk
		left join #Dels D on R.rules_ID = D.rules_ID
		Where 1 = 1
			and cast(r.EFFECTIVE_DATE as Date) <= Cast(CJMP.[Start Date] as date)
			and D.Rules_ID is null
			and r.RECORD_STATUS_CR_FK = 1

		Delete IMMPR
		from [import].[IMPORT_MASS_MAINTENANCE_PRICE_REBATE] IMMPR
		inner join #NewPDs NPD on IMMPR.VendorNo = NPD.[Vendor #] and Cast(IMMPR.StartDate as Date) = Cast(NPD.[Start Date] as date)

		--Inactivate Records already Sent To SX

		Create index rls_del on #Inacts(Rules_ID, Result_Data_Name_FK) --, MAUI_Rules_Maintenance_ID)

		Update MRM
		Set End_Date = Case when Cast(End_Date as date) >= getdate() then Getdate() - 1 else End_Date End
		, Inactivated_By = 'Vendor Replaced'
		, Unique_Key1 = Replace(Unique_Key1, '_Inactivated', '') + '_Inactivated'
		, Transferred_To_ERP = 4
		, Record_Status_CR_FK = 2
		from dbo.Maui_Rules_Maintenance MRM
		inner join #Inacts Inacts on MRM.rules_fk = Inacts.rules_ID

		Update R
		Set Unique_Key1 = Replace(Unique_Key1, '_Inactivated', '') + '_Inactivated'
		, end_date = cast(getdate() - 1 as date)
		, RECORD_STATUS_CR_FK = 2
		from synchronizer.rules R
		inner join #Inacts Inacts on R.rules_id = Inacts.RULES_ID

	End
	Else
	If @host_Precedence_Level = 7
	Begin
		
		Insert into #Dels
		select R.rules_ID, R.RESULT_DATA_NAME_FK, Null 'MAUI_Rules_Maintenance_ID'--, MRM.MAUI_Rules_Maintenance_ID  
		From Synchronizer.RULES R
		inner join #PD7 PD7 on R.Reference like PD7.[Price Sheet #] + '%'
		where r.Host_Precedence_Level = '7' and r.HOST_RULE_XREF like 'New%'

		Insert into #Inacts
		select R.rules_ID, R.RESULT_DATA_NAME_FK, Null 'MAUI_Rules_Maintenance_ID'--, MRM.MAUI_Rules_Maintenance_ID 
		From Synchronizer.RULES R
		inner join #PD7 PD7 on R.Reference like PD7.[Price Sheet #] + '%'
		where r.Host_Precedence_Level = '7' and r.HOST_RULE_XREF not like 'New%'
		and 1 = 2

	End
	Else
		Goto EOP

		--Drop Table #Dels
		--Select Rules_ID, result_data_name_fk into #Dels from synchronizer.rules where name like 'New%' group by Rules_ID, result_data_name_fk
		
		Create index rls_del on #Dels(Rules_ID, Result_Data_Name_FK) --, MAUI_Rules_Maintenance_ID)

		Delete from Synchronizer.rules_filter_set where rules_fk in (select rules_id from #Dels)
		Delete from synchronizer.rules_filter where RULES_FILTER_ID not in
		(
		select prod_filter_fk rules_filter_fk from synchronizer.RULES_FILTER_SET with (nolock) where prod_filter_fk is not null group by prod_filter_fk Union
		select Org_filter_fk from synchronizer.RULES_FILTER_SET with (nolock) where Org_filter_fk is not null group by Org_filter_fk Union
		select cust_filter_fk from synchronizer.RULES_FILTER_SET with (nolock) where cust_filter_fk is not null group by cust_filter_fk Union
		select supl_filter_fk from synchronizer.RULES_FILTER_SET with (nolock) where supl_filter_fk is not null group by supl_filter_fk
		)

		Delete rao from Synchronizer.rules_action_operands rao inner join synchronizer.rules_action ra with (nolock) on rao.rules_action_fk = ra.rules_action_id inner join #Dels Dels on ra.rules_fk = Dels.rules_id
		Delete from synchronizer.RULES_ACTION_STRFUNCTION where RULES_ACTION_FK in (Select RULES_ACTION_ID from synchronizer.RULES_ACTION where RULES_FK in (select rules_id from #Dels))
		Delete from Synchronizer.rules_action where rules_fk in (select rules_id from #Dels)
		Delete from synchronizer.rules where RULES_ID in (select rules_id from #Dels)

		Delete MRM from dbo.MAUI_Rules_Maintenance MRM 
		inner join #Dels Dels on MRM.Rules_FK = Dels.rules_ID

/*	Use for Type 1 Records Only
--Inactivate Records already Sent To SX

		Create index rls_del on #Inacts(Rules_ID, Result_Data_Name_FK) --, MAUI_Rules_Maintenance_ID)

		Update MRM
		Set End_Date = Case when Cast(End_Date as date) >= getdate() then Getdate() - 1 else End_Date End
		, Inactivated_By = 'Vendor Replaced'
		, Unique_Key1 = Replace(Unique_Key1, '_Inactivated', '') + '_Inactivated'
		, Transferred_To_ERP = 4
		, Record_Status_CR_FK = 2
		from dbo.Maui_Rules_Maintenance MRM
		inner join #Inacts Inacts on MRM.rules_fk = Inacts.rules_ID

		Update R
		Set Unique_Key1 = Replace(Unique_Key1, '_Inactivated', '') + '_Inactivated'
		, end_date = cast(getdate() - 1 as date)
		, RECORD_STATUS_CR_FK = 2
		from synchronizer.rules R
		inner join #Inacts Inacts on R.rules_id = Inacts.RULES_ID
*/
	
EOP:

	IF object_id('tempdb..#NewPDs') is not null
	drop table #NewPDs;

	IF object_id('tempdb..#Dels') is not null
	drop table #Dels;

	IF object_id('tempdb..#Inacts') is not null
	drop table #Inacts;

End
