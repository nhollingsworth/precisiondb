﻿

CREATE PROCEDURE [custom].[GetPropogateDownToDNFK]  
  @Data_Name_FK bigint = Null
AS
/*----------------------------------------------------------------------
------------------------
' PROCEDURE       : GetPropogateDdownToDNFK
'
' Description    : Return Item Classes with lower priority than current
Table variable
'
' Change History :
'
' WHEN        WHO         WHAT
' 4/26/2018   Gary Stone  initial version
'-----------------------------------------------------------------------
-----------------------
'-----------------------------------------------------------------------
---------------------*/
 Select * from (
 --Select Null 'DATA_NAME_FK', Null 'NAME', 'Current Level Only' 'LABEL', Null 'PRECEDENCE' Union
	Select data_name_id 'DATA_NAME_FK', NAME, Replace(LABEL, ' Surr', '') 'LABEL', PRECEDENCE from epacube.epacube.data_name where data_name_id in (501044, 501045, 110103)  --(501043, 501044, 501045, 110103) 
	--AND PRECEDENCE < (SELECT PRECEDENCE FROM EPACUBE.EPACUBE.DATA_NAME WITH (NOLOCK) WHERE DATA_NAME_ID = @DATA_NAME_FK)
	
) A order by isnull(precedence, 1000) DESC

