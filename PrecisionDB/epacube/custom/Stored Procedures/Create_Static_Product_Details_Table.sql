﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV       03/04/2015   grab uom value vs code uc table- 


CREATE PROCEDURE [custom].[Create_Static_Product_Details_Table] @Job_FK bigint
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

       DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(1000)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @job_timestamp DATETIME


DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_count      INT

BEGIN TRY
    SET NOCOUNT ON ;
    SET @l_exec_no = 0
    SET @l_sysdate = GETDATE()
		
						
			
    SET @ls_stmt = 'started creation of Custom.Product_Details_By_Warehouse.'
                
    EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
        @exec_id = @l_exec_no OUTPUT ;


    SET @status_desc = 'Starting Temp Table for OnFlyers Data'
    EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


IF object_id('tempdb..#OnFlyersByMonth') is not null
drop table #OnFlyersByMonth
Create Table #OnFlyersByMonth(Product Varchar(64) Null, OnFlyers varchar(128) Null)

Declare @Product varchar(Max)
Declare @Sel varchar(Max)
Declare @SelOF varchar(Max)
Declare @Product_ID varchar(64)
Declare @OnFlyers varchar(Max)
Declare @SQLcd varchar(Max)
Set @SQLcd = ''

DECLARE Prod Cursor local for
select pmt.product_structure_fk, pi.value
from epacube.epacube.PRODUCT_MULT_TYPE pmt with (nolock) 
inner join epacube.data_value dv with (nolock) on pmt.data_value_fk = dv.DATA_VALUE_ID
inner join epacube.product_identification pi with (nolock) on pmt.product_structure_fk = pi.PRODUCT_STRUCTURE_FK and pi.data_name_fk = 110100
group by pmt.product_structure_fk, pi.value Order by pi.value

Set @Sel = ''
Set @Product = ''
Set @Product_ID = ''
Set @SQLcd = ''

OPEN Prod;
FETCH NEXT FROM Prod INTO @Product, @Product_ID

WHILE @@FETCH_STATUS = 0
	Begin
		Set @Sel = ''
		Set @Sel = @Sel + char(13) + 'Select ''' + @Product_ID + ''', ''' 
		Set @SelOF = ''	
	
			DECLARE OnFlyer Cursor local for
			select dv.value 
			from epacube.epacube.PRODUCT_MULT_TYPE pmt with (nolock) 
			inner join epacube.data_value dv with (nolock) on pmt.data_value_fk = dv.DATA_VALUE_ID
			inner join epacube.product_identification pi with (nolock) on pmt.product_structure_fk = pi.PRODUCT_STRUCTURE_FK and pi.data_name_fk = 110100
			where pmt.product_structure_fk = @Product order by dv.create_timestamp

			--Set @SelOF = ''

			OPEN OnFlyer;
			FETCH NEXT FROM OnFlyer INTO @OnFlyers
	            
			WHILE @@FETCH_STATUS = 0
	
			Begin

			Set @SelOF = @SelOF + @OnFlyers + ', '
			FETCH NEXT FROM OnFlyer INTO @OnFlyers

	
			End
			Close OnFlyer;
			Deallocate OnFlyer;

		FETCH NEXT FROM Prod INTO @Product, @Product_ID

		Set @SelOF = Left(@SelOF, len(@SelOF) - 1) + ''''
		Set @SQLcd = 'Insert into #OnFlyersByMonth ' + @Sel + @SelOF
		exec(@SQLcd)

	End
Close Prod;
Deallocate Prod;

Create index idx_flyers on #OnFlyersByMonth(Product)

    SET @status_desc = 'OnFlyers Temp Table Created'
    EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


IF object_id('Custom.Product_Details_By_Warehouse') is not null
drop table Custom.Product_Details_By_Warehouse

Select getdate() 'View Refresh Date', ps.create_timestamp 'Product_Creation_epaDATE',  pi.value 'Product', ei.value 'WHSE Code', ei_v.value 'Vendor #', pi_vp.value 'Vendor Product', 
 sr_231100.NET_VALUE1 'Regular Replacement Cost', 
 sr_231100.NET_VALUE2 'Regular Standard Cost', 
 sr_100100392.NET_VALUE1 'Current End User Each Price', 
 sr_100100392.NET_VALUE2 'Current End User 1 Price', 
 sr_100100392.NET_VALUE3 'Current End User 2 Price', 
 sr_100100398.NET_VALUE5 'Lot Price A', 
 sr_100100398.NET_VALUE6 'Lot Price B', 
 sr_100100398.NET_VALUE1 'Regular End User Each Price', 
 sr_100100398.NET_VALUE2 'Regular End User 1 Price', 
 sr_100100398.NET_VALUE3 'Regular End User 2 Price', 
 sr_100100404.NET_VALUE4 'List Each', 
 sr_100100404.NET_VALUE3 'List Lot Price 1', 
 sr_100100404.NET_VALUE5 'List Lot Price 2', 
 sr_100100416.NET_VALUE3 'Regular Base Price', 
 sr_100100500.NET_VALUE1 'Promo Repl Cost', 
 sr_100100500.NET_VALUE2 'Promo Std Cost', 
 sr_100100520.NET_VALUE1 'Promo End User Each', 
 sr_100100530.NET_VALUE1 'Current Replacement Cost', 
 sr_100100530.NET_VALUE2 'Current Standard Cost', 
 pv_100100418.NET_VALUE4 'Suggested Contractor Price', 
 pv_100100418.NET_VALUE1 'Vendor Corp Cost', 
 pv_100100418.NET_VALUE3 'Vendor List Cost', 
 pv_100100418.NET_VALUE2 'Vendor Store Cost', 
pas_110811.ATTRIBUTE_NUMBER 'Length', 
pas_110812.ATTRIBUTE_NUMBER 'Width', 
pas_110813.ATTRIBUTE_NUMBER 'Height', 
pas_110815.ATTRIBUTE_NUMBER 'Weight', 
dv_110817.value 'MSDS Sheet Number', 
pas_210831.ATTRIBUTE_NUMBER 'DC Sell QTY', 
dv_211825.value 'WHSE Status', 
pas_291801.ATTRIBUTE_CHAR 'Cross Dock/User1W', 
pas_100100110.ATTRIBUTE_CHAR 'AOC Vendor#', 
pas_100100154.ATTRIBUTE_CHAR 'Catalog Page', 
dv_100100158.value 'Country of Origin', 
pas_100100163.ATTRIBUTE_CHAR 'Discontinued Ind', 
dv_100100219.value 'End User Each Published Flag ', 
dv_100100220.value 'End User Lot 1 Published Flag', 
dv_100100221.value 'End User Lot 2 Published Flag', 
pas_100100229.ATTRIBUTE_CHAR 'Flyer Month', 
pas_100100237.ATTRIBUTE_CHAR 'Last Flyer', 
pas_100100272.ATTRIBUTE_CHAR 'Memo', 
pas_100100280.ATTRIBUTE_CHAR 'Promo Price Sheet #', 
pas_100100281.ATTRIBUTE_CHAR 'Promo Price Sheet Descrip', 
pas_100100287.ATTRIBUTE_CHAR 'Promo Page #', 
pas_100100296.ATTRIBUTE_NUMBER 'Regular Drop Ship 1 Qty', 
dv_100100341.value 'Return Flag', 
dv_100100342.value 'Seasonal CD', 
pas_100100343.ATTRIBUTE_Y_N 'Serialized Product Flag', 
pas_100100356.ATTRIBUTE_Y_N 'Taxable Tool Flag', 
pas_100100358.ATTRIBUTE_Y_N 'Transmit Flag', 
pas_100100373.ATTRIBUTE_NUMBER 'Case Qty', 
pas_100100374.ATTRIBUTE_CHAR 'Freight Code 1', 
pas_100100375.ATTRIBUTE_CHAR 'Freight Code 2', 
pas_100100376.ATTRIBUTE_CHAR 'z_icsp.u_hazcode', 
pas_100100377.ATTRIBUTE_DATE 'Line Discount Begin Date', 
pas_100100378.ATTRIBUTE_DATE 'Line Discount End Date', 
pas_100100379.ATTRIBUTE_NUMBER 'Line Discount Percent', 
pas_100100380.ATTRIBUTE_NUMBER 'Line Discount Qty', 
pas_100100389.ATTRIBUTE_CHAR 'Vendor Std Pk', 
pas_100100402.ATTRIBUTE_NUMBER 'Regular End User 1 Qty', 
pas_100100403.ATTRIBUTE_NUMBER 'Regular End User 2 Qty', 
pas_100100410.ATTRIBUTE_NUMBER 'Lot Qty A', 
pas_100100411.ATTRIBUTE_NUMBER 'Lot Qty B', 
pas_100100412.ATTRIBUTE_NUMBER 'List Lot Qty 1', 
pas_100100413.ATTRIBUTE_NUMBER 'List Lot Qty 2', 
dv_100100414.value 'Certification Code', 
pas_100100435.ATTRIBUTE_CHAR 'Vendor Price Sheet #', 
pas_100100436.ATTRIBUTE_CHAR 'Vendor Price Sheet Descrip', 
pas_100100551.ATTRIBUTE_DATE 'Store DC Cost Effective Date', 
pas_100100552.ATTRIBUTE_DATE 'Corp Cost Effective Date', 
pas_100100553.ATTRIBUTE_DATE 'End User Price Effective Date', 

uc_110603.UOM_CODE 'Stocking UOM', 
uc_211601.UOM_CODE 'Corp Standard Pack', 
uc_211602.UOM_CODE 'Buying Unit', 

dv_210901.value 'SX PCAT', 
dv_210903.value 'Brand Name', 
dv_211901.value 'Price Type', 
dv_100100100.value 'Web Node ID', 
dv_100100161.value 'Dept', 
dv_100100357.value 'Taxonomy Assignment', 
dv_100100415.value 'Store Prod Category', 
dv_100100423.value 'RP Family Group', 
dv_100100424.value 'ST Family Group', 
dv_100100425.value 'BP Family Group', 
dv_100100426.value 'EU Family Group', 
dv_100100427.value 'EU1 Family Group', 
dv_100100428.value 'EU2 Family Group'
, OnF.OnFlyers
into Custom.Product_Details_By_Warehouse
from epacube.product_identification pi with (nolock) 
inner join epacube.product_structure ps with (nolock) on pi.PRODUCT_STRUCTURE_FK = ps.PRODUCT_STRUCTURE_ID and ps.DATA_NAME_FK = 149000
left join epacube.product_identification pi_vp with (nolock) on pi.product_structure_fk = pi_vp.product_structure_fk and pi_vp.data_name_fk = 110111
inner join epacube.product_association pa with (nolock) on pi.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and pa.DATA_NAME_FK = 159100 
inner join epacube.entity_identification ei with (nolock) on pa.org_entity_structure_fk = ei.entity_structure_fk and ei.data_name_fk = 141111 and entity_data_name_fk = 141000
left join epacube.product_association pa_v with (nolock) on pi.product_structure_fk = pa_v.product_structure_fk and pa.org_entity_structure_fk = pa_v.org_entity_structure_fk and pa_v.data_name_fk = 253501
left join epacube.entity_identification ei_v with (nolock) on pa_v.entity_structure_fk = ei_v.entity_structure_fk and ei_v.entity_data_name_fk = 143000 and ei_v.DATA_NAME_FK = 143110

Left Join marginmgr.SHEET_RESULTS_VALUES_FUTURE SR_231100 with (nolock) on pa.product_structure_fk = SR_231100.product_structure_fk and pa.org_entity_structure_fk = SR_231100.org_entity_structure_fk and SR_231100.data_name_fk = 231100
Left Join marginmgr.SHEET_RESULTS_VALUES_FUTURE SR_100100392 with (nolock) on pa.product_structure_fk = SR_100100392.product_structure_fk and pa.org_entity_structure_fk = SR_100100392.org_entity_structure_fk and SR_100100392.data_name_fk = 100100392
Left Join marginmgr.SHEET_RESULTS_VALUES_FUTURE SR_100100398 with (nolock) on pa.product_structure_fk = SR_100100398.product_structure_fk and pa.org_entity_structure_fk = SR_100100398.org_entity_structure_fk and SR_100100398.data_name_fk = 100100398
Left Join marginmgr.SHEET_RESULTS_VALUES_FUTURE SR_100100404 with (nolock) on pa.product_structure_fk = SR_100100404.product_structure_fk and pa.org_entity_structure_fk = SR_100100404.org_entity_structure_fk and SR_100100404.data_name_fk = 100100404
Left Join marginmgr.SHEET_RESULTS_VALUES_FUTURE SR_100100416 with (nolock) on pa.product_structure_fk = SR_100100416.product_structure_fk and pa.org_entity_structure_fk = SR_100100416.org_entity_structure_fk and SR_100100416.data_name_fk = 100100416
Left Join marginmgr.SHEET_RESULTS_VALUES_FUTURE SR_100100500 with (nolock) on pa.product_structure_fk = SR_100100500.product_structure_fk and pa.org_entity_structure_fk = SR_100100500.org_entity_structure_fk and SR_100100500.data_name_fk = 100100500
Left Join marginmgr.SHEET_RESULTS_VALUES_FUTURE SR_100100520 with (nolock) on pa.product_structure_fk = SR_100100520.product_structure_fk and pa.org_entity_structure_fk = SR_100100520.org_entity_structure_fk and SR_100100520.data_name_fk = 100100520
Left Join marginmgr.SHEET_RESULTS_VALUES_FUTURE SR_100100530 with (nolock) on pa.product_structure_fk = SR_100100530.product_structure_fk and pa.org_entity_structure_fk = SR_100100530.org_entity_structure_fk and SR_100100530.data_name_fk = 100100530
Left Join epaCUBE.Product_Values PV_100100418 with (nolock) on pa.product_structure_fk = PV_100100418.product_structure_fk and pa.org_entity_structure_fk = PV_100100418.org_entity_structure_fk and PV_100100418.data_name_fk = 100100418

left join epacube.product_category pc_210901 with (nolock) on pa.product_structure_fk = pc_210901.product_structure_fk and pc_210901.org_entity_structure_fk = 1  and pc_210901.data_name_fk = 210901
left join epacube.data_value dv_210901 with (nolock) on pc_210901.data_value_fk = dv_210901.data_value_id
left join epacube.product_category pc_210903 with (nolock) on pa.product_structure_fk = pc_210903.product_structure_fk and pc_210903.org_entity_structure_fk = 1  and pc_210903.data_name_fk = 210903
left join epacube.data_value dv_210903 with (nolock) on pc_210903.data_value_fk = dv_210903.data_value_id
left join epacube.product_category pc_211901 with (nolock) on pa.product_structure_fk = pc_211901.product_structure_fk and   pa.org_entity_structure_fk = pc_211901.org_entity_structure_fk  and pc_211901.data_name_fk = 211901
left join epacube.data_value dv_211901 with (nolock) on pc_211901.data_value_fk = dv_211901.data_value_id
left join epacube.product_category pc_100100100 with (nolock) on pa.product_structure_fk = pc_100100100.product_structure_fk and pc_100100100.org_entity_structure_fk = 1  and pc_100100100.data_name_fk = 100100100
left join epacube.data_value dv_100100100 with (nolock) on pc_100100100.data_value_fk = dv_100100100.data_value_id
left join epacube.product_category pc_100100161 with (nolock) on pa.product_structure_fk = pc_100100161.product_structure_fk and pc_100100161.org_entity_structure_fk = 1  and pc_100100161.data_name_fk = 100100161
left join epacube.data_value dv_100100161 with (nolock) on pc_100100161.data_value_fk = dv_100100161.data_value_id
left join epacube.product_category pc_100100357 with (nolock) on pa.product_structure_fk = pc_100100357.product_structure_fk and pc_100100357.org_entity_structure_fk = 1  and pc_100100357.data_name_fk = 100100357
left join epacube.data_value dv_100100357 with (nolock) on pc_100100357.data_value_fk = dv_100100357.data_value_id
left join epacube.product_category pc_100100415 with (nolock) on pa.product_structure_fk = pc_100100415.product_structure_fk and pc_100100415.org_entity_structure_fk = 1  and pc_100100415.data_name_fk = 100100415
left join epacube.data_value dv_100100415 with (nolock) on pc_100100415.data_value_fk = dv_100100415.data_value_id
left join epacube.product_category pc_100100423 with (nolock) on pa.product_structure_fk = pc_100100423.product_structure_fk and   pa.org_entity_structure_fk = pc_100100423.org_entity_structure_fk  and pc_100100423.data_name_fk = 100100423
left join epacube.data_value dv_100100423 with (nolock) on pc_100100423.data_value_fk = dv_100100423.data_value_id
left join epacube.product_category pc_100100424 with (nolock) on pa.product_structure_fk = pc_100100424.product_structure_fk and   pa.org_entity_structure_fk = pc_100100424.org_entity_structure_fk  and pc_100100424.data_name_fk = 100100424
left join epacube.data_value dv_100100424 with (nolock) on pc_100100424.data_value_fk = dv_100100424.data_value_id
left join epacube.product_category pc_100100425 with (nolock) on pa.product_structure_fk = pc_100100425.product_structure_fk and   pa.org_entity_structure_fk = pc_100100425.org_entity_structure_fk  and pc_100100425.data_name_fk = 100100425
left join epacube.data_value dv_100100425 with (nolock) on pc_100100425.data_value_fk = dv_100100425.data_value_id
left join epacube.product_category pc_100100426 with (nolock) on pa.product_structure_fk = pc_100100426.product_structure_fk and   pa.org_entity_structure_fk = pc_100100426.org_entity_structure_fk  and pc_100100426.data_name_fk = 100100426
left join epacube.data_value dv_100100426 with (nolock) on pc_100100426.data_value_fk = dv_100100426.data_value_id
left join epacube.product_category pc_100100427 with (nolock) on pa.product_structure_fk = pc_100100427.product_structure_fk and   pa.org_entity_structure_fk = pc_100100427.org_entity_structure_fk  and pc_100100427.data_name_fk = 100100427
left join epacube.data_value dv_100100427 with (nolock) on pc_100100427.data_value_fk = dv_100100427.data_value_id
left join epacube.product_category pc_100100428 with (nolock) on pa.product_structure_fk = pc_100100428.product_structure_fk and   pa.org_entity_structure_fk = pc_100100428.org_entity_structure_fk  and pc_100100428.data_name_fk = 100100428
left join epacube.data_value dv_100100428 with (nolock) on pc_100100428.data_value_fk = dv_100100428.data_value_id

left join epacube.product_Attribute pas_110811 with (nolock) on pa.product_structure_fk = pas_110811.product_structure_fk  and pas_110811.org_entity_structure_fk = 1  and pas_110811.data_name_fk = 110811
left join epacube.data_value dv_110811 with (nolock) on pas_110811.data_value_fk = dv_110811.data_value_id
left join epacube.product_Attribute pas_110812 with (nolock) on pa.product_structure_fk = pas_110812.product_structure_fk  and pas_110812.org_entity_structure_fk = 1  and pas_110812.data_name_fk = 110812
left join epacube.data_value dv_110812 with (nolock) on pas_110812.data_value_fk = dv_110812.data_value_id
left join epacube.product_Attribute pas_110813 with (nolock) on pa.product_structure_fk = pas_110813.product_structure_fk  and pas_110813.org_entity_structure_fk = 1  and pas_110813.data_name_fk = 110813
left join epacube.data_value dv_110813 with (nolock) on pas_110813.data_value_fk = dv_110813.data_value_id
left join epacube.product_Attribute pas_110815 with (nolock) on pa.product_structure_fk = pas_110815.product_structure_fk  and pas_110815.org_entity_structure_fk = 1  and pas_110815.data_name_fk = 110815
left join epacube.data_value dv_110815 with (nolock) on pas_110815.data_value_fk = dv_110815.data_value_id
left join epacube.product_Attribute pas_110817 with (nolock) on pa.product_structure_fk = pas_110817.product_structure_fk  and pas_110817.org_entity_structure_fk = 1  and pas_110817.data_name_fk = 110817
left join epacube.data_value dv_110817 with (nolock) on pas_110817.data_value_fk = dv_110817.data_value_id
left join epacube.product_Attribute pas_210831 with (nolock) on pa.product_structure_fk = pas_210831.product_structure_fk  and pas_210831.org_entity_structure_fk = 1  and pas_210831.data_name_fk = 210831
left join epacube.data_value dv_210831 with (nolock) on pas_210831.data_value_fk = dv_210831.data_value_id
left join epacube.product_Attribute pas_211825 with (nolock) on pa.product_structure_fk = pas_211825.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_211825.org_entity_structure_fk, 1)  and pas_211825.data_name_fk = 211825
left join epacube.data_value dv_211825 with (nolock) on pas_211825.data_value_fk = dv_211825.data_value_id
left join epacube.product_Attribute pas_291801 with (nolock) on pa.product_structure_fk = pas_291801.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_291801.org_entity_structure_fk, 1)  and pas_291801.data_name_fk = 291801
left join epacube.data_value dv_291801 with (nolock) on pas_291801.data_value_fk = dv_291801.data_value_id
left join epacube.product_Attribute pas_100100110 with (nolock) on pa.product_structure_fk = pas_100100110.product_structure_fk  and pas_100100110.org_entity_structure_fk = 1  and pas_100100110.data_name_fk = 100100110
left join epacube.data_value dv_100100110 with (nolock) on pas_100100110.data_value_fk = dv_100100110.data_value_id
left join epacube.product_Attribute pas_100100154 with (nolock) on pa.product_structure_fk = pas_100100154.product_structure_fk  and pas_100100154.org_entity_structure_fk = 1  and pas_100100154.data_name_fk = 100100154
left join epacube.data_value dv_100100154 with (nolock) on pas_100100154.data_value_fk = dv_100100154.data_value_id
left join epacube.product_Attribute pas_100100158 with (nolock) on pa.product_structure_fk = pas_100100158.product_structure_fk  and pas_100100158.org_entity_structure_fk = 1  and pas_100100158.data_name_fk = 100100158
left join epacube.data_value dv_100100158 with (nolock) on pas_100100158.data_value_fk = dv_100100158.data_value_id
left join epacube.product_Attribute pas_100100163 with (nolock) on pa.product_structure_fk = pas_100100163.product_structure_fk  and pas_100100163.org_entity_structure_fk = 1  and pas_100100163.data_name_fk = 100100163
left join epacube.data_value dv_100100163 with (nolock) on pas_100100163.data_value_fk = dv_100100163.data_value_id
left join epacube.product_Attribute pas_100100219 with (nolock) on pa.product_structure_fk = pas_100100219.product_structure_fk  and pas_100100219.org_entity_structure_fk = 1  and pas_100100219.data_name_fk = 100100219
left join epacube.data_value dv_100100219 with (nolock) on pas_100100219.data_value_fk = dv_100100219.data_value_id
left join epacube.product_Attribute pas_100100220 with (nolock) on pa.product_structure_fk = pas_100100220.product_structure_fk  and pas_100100220.org_entity_structure_fk = 1  and pas_100100220.data_name_fk = 100100220
left join epacube.data_value dv_100100220 with (nolock) on pas_100100220.data_value_fk = dv_100100220.data_value_id
left join epacube.product_Attribute pas_100100221 with (nolock) on pa.product_structure_fk = pas_100100221.product_structure_fk  and pas_100100221.org_entity_structure_fk = 1  and pas_100100221.data_name_fk = 100100221
left join epacube.data_value dv_100100221 with (nolock) on pas_100100221.data_value_fk = dv_100100221.data_value_id
left join epacube.product_Attribute pas_100100229 with (nolock) on pa.product_structure_fk = pas_100100229.product_structure_fk  and pas_100100229.org_entity_structure_fk = 1  and pas_100100229.data_name_fk = 100100229
left join epacube.data_value dv_100100229 with (nolock) on pas_100100229.data_value_fk = dv_100100229.data_value_id
left join epacube.product_Attribute pas_100100237 with (nolock) on pa.product_structure_fk = pas_100100237.product_structure_fk  and pas_100100237.org_entity_structure_fk = 1  and pas_100100237.data_name_fk = 100100237
left join epacube.data_value dv_100100237 with (nolock) on pas_100100237.data_value_fk = dv_100100237.data_value_id
left join epacube.product_Attribute pas_100100272 with (nolock) on pa.product_structure_fk = pas_100100272.product_structure_fk  and pas_100100272.org_entity_structure_fk = 1  and pas_100100272.data_name_fk = 100100272
left join epacube.data_value dv_100100272 with (nolock) on pas_100100272.data_value_fk = dv_100100272.data_value_id
left join epacube.product_Attribute pas_100100280 with (nolock) on pa.product_structure_fk = pas_100100280.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100280.org_entity_structure_fk, 1)  and pas_100100280.data_name_fk = 100100280
left join epacube.data_value dv_100100280 with (nolock) on pas_100100280.data_value_fk = dv_100100280.data_value_id
left join epacube.product_Attribute pas_100100281 with (nolock) on pa.product_structure_fk = pas_100100281.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100281.org_entity_structure_fk, 1)  and pas_100100281.data_name_fk = 100100281
left join epacube.data_value dv_100100281 with (nolock) on pas_100100281.data_value_fk = dv_100100281.data_value_id
left join epacube.product_Attribute pas_100100287 with (nolock) on pa.product_structure_fk = pas_100100287.product_structure_fk  and pas_100100287.org_entity_structure_fk = 1  and pas_100100287.data_name_fk = 100100287
left join epacube.data_value dv_100100287 with (nolock) on pas_100100287.data_value_fk = dv_100100287.data_value_id
left join epacube.product_Attribute pas_100100296 with (nolock) on pa.product_structure_fk = pas_100100296.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100296.org_entity_structure_fk, 1)  and pas_100100296.data_name_fk = 100100296
left join epacube.data_value dv_100100296 with (nolock) on pas_100100296.data_value_fk = dv_100100296.data_value_id
left join epacube.product_Attribute pas_100100341 with (nolock) on pa.product_structure_fk = pas_100100341.product_structure_fk  and pas_100100341.org_entity_structure_fk = 1  and pas_100100341.data_name_fk = 100100341
left join epacube.data_value dv_100100341 with (nolock) on pas_100100341.data_value_fk = dv_100100341.data_value_id

left join epacube.product_Attribute pas_100100342 with (nolock) on pa.product_structure_fk = pas_100100342.product_structure_fk  and pas_100100342.org_entity_structure_fk = 1  and pas_100100342.data_name_fk = 100100342
left join epacube.data_value dv_100100342 with (nolock) on pas_100100342.data_value_fk = dv_100100342.data_value_id
left join epacube.product_Attribute pas_100100343 with (nolock) on pa.product_structure_fk = pas_100100343.product_structure_fk  and pas_100100343.org_entity_structure_fk = 1  and pas_100100343.data_name_fk = 100100343
left join epacube.data_value dv_100100343 with (nolock) on pas_100100343.data_value_fk = dv_100100343.data_value_id
left join epacube.product_Attribute pas_100100356 with (nolock) on pa.product_structure_fk = pas_100100356.product_structure_fk  and pas_100100356.org_entity_structure_fk = 1  and pas_100100356.data_name_fk = 100100356
left join epacube.data_value dv_100100356 with (nolock) on pas_100100356.data_value_fk = dv_100100356.data_value_id
left join epacube.product_Attribute pas_100100358 with (nolock) on pa.product_structure_fk = pas_100100358.product_structure_fk  and pas_100100358.org_entity_structure_fk = 1  and pas_100100358.data_name_fk = 100100358
left join epacube.data_value dv_100100358 with (nolock) on pas_100100358.data_value_fk = dv_100100358.data_value_id
left join epacube.product_Attribute pas_100100373 with (nolock) on pa.product_structure_fk = pas_100100373.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100373.org_entity_structure_fk, 1)  and pas_100100373.data_name_fk = 100100373
left join epacube.data_value dv_100100373 with (nolock) on pas_100100373.data_value_fk = dv_100100373.data_value_id
left join epacube.product_Attribute pas_100100374 with (nolock) on pa.product_structure_fk = pas_100100374.product_structure_fk  and pas_100100374.org_entity_structure_fk = 1  and pas_100100374.data_name_fk = 100100374
left join epacube.data_value dv_100100374 with (nolock) on pas_100100374.data_value_fk = dv_100100374.data_value_id
left join epacube.product_Attribute pas_100100375 with (nolock) on pa.product_structure_fk = pas_100100375.product_structure_fk  and pas_100100375.org_entity_structure_fk = 1  and pas_100100375.data_name_fk = 100100375
left join epacube.data_value dv_100100375 with (nolock) on pas_100100375.data_value_fk = dv_100100375.data_value_id
left join epacube.product_Attribute pas_100100376 with (nolock) on pa.product_structure_fk = pas_100100376.product_structure_fk  and pas_100100376.org_entity_structure_fk = 1  and pas_100100376.data_name_fk = 100100376
left join epacube.data_value dv_100100376 with (nolock) on pas_100100376.data_value_fk = dv_100100376.data_value_id
left join epacube.product_Attribute pas_100100377 with (nolock) on pa.product_structure_fk = pas_100100377.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100377.org_entity_structure_fk, 1)  and pas_100100377.data_name_fk = 100100377
left join epacube.data_value dv_100100377 with (nolock) on pas_100100377.data_value_fk = dv_100100377.data_value_id
left join epacube.product_Attribute pas_100100378 with (nolock) on pa.product_structure_fk = pas_100100378.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100378.org_entity_structure_fk, 1)  and pas_100100378.data_name_fk = 100100378
left join epacube.data_value dv_100100378 with (nolock) on pas_100100378.data_value_fk = dv_100100378.data_value_id
left join epacube.product_Attribute pas_100100379 with (nolock) on pa.product_structure_fk = pas_100100379.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100379.org_entity_structure_fk, 1)  and pas_100100379.data_name_fk = 100100379
left join epacube.data_value dv_100100379 with (nolock) on pas_100100379.data_value_fk = dv_100100379.data_value_id
left join epacube.product_Attribute pas_100100380 with (nolock) on pa.product_structure_fk = pas_100100380.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100380.org_entity_structure_fk, 1)  and pas_100100380.data_name_fk = 100100380
left join epacube.data_value dv_100100380 with (nolock) on pas_100100380.data_value_fk = dv_100100380.data_value_id
left join epacube.product_Attribute pas_100100389 with (nolock) on pa.product_structure_fk = pas_100100389.product_structure_fk  and pas_100100389.org_entity_structure_fk = 1  and pas_100100389.data_name_fk = 100100389
left join epacube.data_value dv_100100389 with (nolock) on pas_100100389.data_value_fk = dv_100100389.data_value_id
left join epacube.product_Attribute pas_100100402 with (nolock) on pa.product_structure_fk = pas_100100402.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100402.org_entity_structure_fk, 1)  and pas_100100402.data_name_fk = 100100402
left join epacube.data_value dv_100100402 with (nolock) on pas_100100402.data_value_fk = dv_100100402.data_value_id
left join epacube.product_Attribute pas_100100403 with (nolock) on pa.product_structure_fk = pas_100100403.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100403.org_entity_structure_fk, 1)  and pas_100100403.data_name_fk = 100100403
left join epacube.data_value dv_100100403 with (nolock) on pas_100100403.data_value_fk = dv_100100403.data_value_id
left join epacube.product_Attribute pas_100100410 with (nolock) on pa.product_structure_fk = pas_100100410.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100410.org_entity_structure_fk, 1)  and pas_100100410.data_name_fk = 100100410
left join epacube.data_value dv_100100410 with (nolock) on pas_100100410.data_value_fk = dv_100100410.data_value_id
left join epacube.product_Attribute pas_100100411 with (nolock) on pa.product_structure_fk = pas_100100411.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100411.org_entity_structure_fk, 1)  and pas_100100411.data_name_fk = 100100411
left join epacube.data_value dv_100100411 with (nolock) on pas_100100411.data_value_fk = dv_100100411.data_value_id
left join epacube.product_Attribute pas_100100412 with (nolock) on pa.product_structure_fk = pas_100100412.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100412.org_entity_structure_fk, 1)  and pas_100100412.data_name_fk = 100100412
left join epacube.data_value dv_100100412 with (nolock) on pas_100100412.data_value_fk = dv_100100412.data_value_id
left join epacube.product_Attribute pas_100100413 with (nolock) on pa.product_structure_fk = pas_100100413.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100413.org_entity_structure_fk, 1)  and pas_100100413.data_name_fk = 100100413
left join epacube.data_value dv_100100413 with (nolock) on pas_100100413.data_value_fk = dv_100100413.data_value_id
left join epacube.product_Attribute pas_100100414 with (nolock) on pa.product_structure_fk = pas_100100414.product_structure_fk  and pas_100100414.org_entity_structure_fk = 1  and pas_100100414.data_name_fk = 100100414
left join epacube.data_value dv_100100414 with (nolock) on pas_100100414.data_value_fk = dv_100100414.data_value_id
left join epacube.product_Attribute pas_100100435 with (nolock) on pa.product_structure_fk = pas_100100435.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100435.org_entity_structure_fk, 1)  and pas_100100435.data_name_fk = 100100435
left join epacube.data_value dv_100100435 with (nolock) on pas_100100435.data_value_fk = dv_100100435.data_value_id
left join epacube.product_Attribute pas_100100436 with (nolock) on pa.product_structure_fk = pas_100100436.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100436.org_entity_structure_fk, 1)  and pas_100100436.data_name_fk = 100100436
left join epacube.data_value dv_100100436 with (nolock) on pas_100100436.data_value_fk = dv_100100436.data_value_id

left join epacube.product_Attribute pas_100100551 with (nolock) on pa.product_structure_fk = pas_100100551.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100551.org_entity_structure_fk, 1)  and pas_100100551.data_name_fk = 100100551
left join epacube.data_value dv_100100551 with (nolock) on pas_100100551.data_value_fk = dv_100100551.data_value_id
left join epacube.product_Attribute pas_100100552 with (nolock) on pa.product_structure_fk = pas_100100552.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100552.org_entity_structure_fk, 1)  and pas_100100552.data_name_fk = 100100552
left join epacube.data_value dv_100100552 with (nolock) on pas_100100552.data_value_fk = dv_100100552.data_value_id
left join epacube.product_Attribute pas_100100553 with (nolock) on pa.product_structure_fk = pas_100100553.product_structure_fk  and pa.org_entity_structure_fk = isnull(pas_100100553.org_entity_structure_fk, 1)  and pas_100100553.data_name_fk = 100100553
left join epacube.data_value dv_100100553 with (nolock) on pas_100100553.data_value_fk = dv_100100553.data_value_id

left join epacube.product_uom_class puc_110603 with (nolock) on pa.product_structure_fk = puc_110603.product_structure_fk  and puc_110603.org_entity_structure_fk = 1  and puc_110603.data_name_fk = 110603
left join epacube.UOM_Code uc_110603 with (nolock) on puc_110603.uom_code_fk = uc_110603.uom_code_id
left join epacube.product_uom_class puc_211601 with (nolock) on pa.product_structure_fk = puc_211601.product_structure_fk  and pa.org_entity_structure_fk = isnull(puc_211601.org_entity_structure_fk, 1)  and puc_211601.data_name_fk = 211601
left join epacube.UOM_Code uc_211601 with (nolock) on puc_211601.uom_code_fk = uc_211601.uom_code_id
left join epacube.product_uom_class puc_211602 with (nolock) on pa.product_structure_fk = puc_211602.product_structure_fk  and pa.org_entity_structure_fk = isnull(puc_211602.org_entity_structure_fk, 1)  and puc_211602.data_name_fk = 211602
left join epacube.UOM_Code uc_211602 with (nolock) on puc_211602.uom_code_fk = uc_211602.uom_code_id
left join #OnFlyersByMonth OnF on PI.value = OnF.Product 
Where pi.data_name_fk = 110100

Create Index idx_prods on Custom.Product_Details_By_Warehouse(Product, [Whse Code], [Vendor #], [Vendor Product], OnFlyers)

	Set @v_input_rows = (Select count(*) from Custom.Product_Details_By_Warehouse)
	
	EXEC job_execution_create   @Job_FK, 'Custom.Product_Details_By_Warehouse Created',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @l_sysdate, @l_sysdate 

           SET @status_desc = 'finished execution of custom.CLEANUP_FUTURE_VEND_COST'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;

        END TRY
        BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of Create Custom.Product_Details_By_Warehouse Created has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            EXEC exec_monitor.email_error_sp @l_exec_no,
                'dbexceptions@epacube.com' ;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH

END
