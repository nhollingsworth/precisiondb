﻿CREATE TABLE [custom].[V_FuturePrices] (
    [PRODUCT_STRUCTURE_FK]       BIGINT         NOT NULL,
    [ORG_ENTITY_STRUCTURE_FK]    BIGINT         NOT NULL,
    [Product_ID]                 VARCHAR (128)  NULL,
    [Warehouse]                  VARCHAR (128)  NULL,
    [List_effDate]               DATE           NULL,
    [List_EndDate]               DATE           NULL,
    [List Each]                  VARCHAR (1000) NULL,
    [List Lot Price 1]           VARCHAR (1000) NULL,
    [List Lot Price 2]           VARCHAR (1000) NULL,
    [RegCost_EffDate]            DATE           NULL,
    [RegCost_EndDate]            DATE           NULL,
    [Reg Repl Cost]              VARCHAR (1000) NULL,
    [Reg Std Cost]               VARCHAR (1000) NULL,
    [CEU_EffDate]                DATE           NULL,
    [CEU_EndDate]                DATE           NULL,
    [CEU Ea Prc]                 VARCHAR (1000) NULL,
    [CEU 1 Prc]                  VARCHAR (1000) NULL,
    [CEU 2 Prc]                  VARCHAR (1000) NULL,
    [REU_EffDate]                DATE           NULL,
    [REU_EndDate]                DATE           NULL,
    [Lot A Prc]                  VARCHAR (1000) NULL,
    [Lot B Prc]                  VARCHAR (1000) NULL,
    [REU Ea Prc]                 VARCHAR (1000) NULL,
    [REU 1 Prc]                  VARCHAR (1000) NULL,
    [REU 2 Prc]                  VARCHAR (1000) NULL,
    [RegBase_EffDate]            DATE           NULL,
    [RegBase_EndDate]            DATE           NULL,
    [Regular Base Price]         VARCHAR (1000) NULL,
    [PrEU_EffDate]               DATE           NULL,
    [PrEU_EndDate]               DATE           NULL,
    [Promo End User Each]        VARCHAR (1000) NULL,
    [Curr_EffDate]               DATE           NULL,
    [Curr_EndDate]               DATE           NULL,
    [Current Repl Cost]          VARCHAR (1000) NULL,
    [Current Std Cost]           VARCHAR (1000) NULL,
    [Vend_EffDate]               DATE           NULL,
    [Vend_EndDate]               DATE           NULL,
    [Vendor Corp Cost]           VARCHAR (1000) NULL,
    [Vendor Store Cost]          VARCHAR (1000) NULL,
    [Vendor List Cost]           VARCHAR (1000) NULL,
    [Suggested Contractor Price] VARCHAR (1000) NULL,
    [Promo_EffDate]              DATE           NULL,
    [Promo_EndDate]              DATE           NULL,
    [Promo Repl Cost]            VARCHAR (1000) NULL,
    [Promo Std Cost]             VARCHAR (1000) NULL,
    [DATA_NAME_FK]               INT            NULL,
    [View_Refresh_Timestamp]     DATETIME       NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_vfp]
    ON [custom].[V_FuturePrices]([DATA_NAME_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC, [ORG_ENTITY_STRUCTURE_FK] ASC, [Product_ID] ASC, [Warehouse] ASC, [List_effDate] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

