﻿CREATE TABLE [custom].[V_FuturePricing] (
    [CompanyNumber]                 INT            NOT NULL,
    [Whse Code]                     VARCHAR (128)  NULL,
    [Vendor ID]                     VARCHAR (256)  NULL,
    [Product Number]                VARCHAR (256)  NULL,
    [Product Description]           VARCHAR (256)  NULL,
    [Price Sheet #]                 VARCHAR (MAX)  NULL,
    [Price Sheet Descrip]           VARCHAR (MAX)  NULL,
    [Vendor Corp Cost]              VARCHAR (1000) NULL,
    [Vendor Store Cost]             VARCHAR (1000) NULL,
    [Vendor List Cost]              VARCHAR (1000) NULL,
    [Whse Costs Effective Date]     DATE           NULL,
    [Whse Costs End Date]           DATE           NULL,
    [ST RULE]                       VARCHAR (256)  NULL,
    [Replacement Cost]              VARCHAR (1000) NULL,
    [RP RULE]                       VARCHAR (256)  NULL,
    [Standard Cost]                 VARCHAR (1000) NULL,
    [Store DC Costs Effective Date] DATE           NULL,
    [Store DC Costs End Date]       DATE           NULL,
    [BP RULE]                       VARCHAR (256)  NULL,
    [Base Price]                    VARCHAR (1000) NULL,
    [End User Price Effective Date] DATE           NULL,
    [End User Price End Date]       DATE           NULL,
    [EU RULE]                       VARCHAR (256)  NULL,
    [End User Each Price]           VARCHAR (1000) NULL,
    [EU1 RULE]                      VARCHAR (256)  NULL,
    [End User 1 Price]              VARCHAR (1000) NULL,
    [EU2 RULE]                      VARCHAR (256)  NULL,
    [End User 2 Price]              VARCHAR (1000) NULL,
    [End User A]                    VARCHAR (1000) NULL,
    [End User B]                    VARCHAR (1000) NULL,
    [List Each]                     VARCHAR (1000) NULL,
    [List Lot Price 1]              VARCHAR (1000) NULL,
    [List Lot Price 2]              VARCHAR (1000) NULL,
    [View_Refresh_Timestamp]        DATETIME       NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_vpf]
    ON [custom].[V_FuturePricing]([Whse Code] ASC, [Vendor ID] ASC, [Product Number] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

