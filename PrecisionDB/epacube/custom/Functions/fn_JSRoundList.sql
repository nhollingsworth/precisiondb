﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [custom].[fn_JSRoundList]
(
	-- Add the parameters for the function here
	@list decimal(10,4)
)
RETURNS decimal (15,2)
AS
BEGIN
declare @rawlist decimal(15,2)
declare @tier int
declare @rounded decimal(15,2)
declare @adder decimal(15,2)
declare @finallist decimal(15,2)

set @rawlist = @list

set @tier = case 
			when @rawlist < 1 then 1
			when @rawlist >= 1 and @rawlist < 10 then 2
			when @rawlist >= 10 and @rawlist < 50 then 3
			when @rawlist >= 50.00 and @rawlist < 1000 then 4
			when @rawlist >= 1000 and @rawlist < 10000 then 5
			when @rawlist >= 10000 then 6
			else null end

set @rounded = case
			when @tier = 1 then @rawlist 
			when @tier = 2 then round(@rawlist,1,1)
			when @tier = 3 then round(@rawlist,0,1)
			when @tier = 4 then round(@rawlist,-1,1)
			when @tier = 5 then round(@rawlist,-2,1)
			else round(round(@rawlist,0),-3,1)
			end
set @Adder = case 
			when @tier=1 then 0.00
			when @tier=2 then 
				case 
					when @rawlist-@rounded <= .05 then .05
				else .09 
				end
			when @tier=3 then 
				case 
					when @rawlist-@rounded <= .49 then .49
				else .99 
				end				
			when @tier=4 then 
				case 
					when @rawlist-@rounded <= 5 then 5.00
					when @rawlist-@rounded > 9 then 15.00
				else 9.00 
				end
			when @tier=5 then 
				case 
					when @rawlist-@rounded <= 45.00 then 45.00
					when @rawlist-@rounded > 99.00 then 145.00
				else 99.00 
				end
			when @tier=6 then 
				case 
					when @rawlist-@rounded <= 500 then 500
					when @rawlist-@rounded > 900 then 1500
				else 900.00 
				end				
			else null end

set @finallist = @rounded+@adder


RETURN @finallist

END
