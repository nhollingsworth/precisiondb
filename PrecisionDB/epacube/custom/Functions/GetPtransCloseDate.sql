﻿-- =============================================
-- Author:		Rita Charlesworth
-- Create date: 5/23/2012
-- Description:	Based on run date - passed in so we can test various dates
--				and so we can easily change in the calling program to the
--				prior day, if we ever get to pre-midnight runs -- find
--				the Ptrans Close Date - defined as the fourth to the last
--				business day in the month. Meaning there are three business 
--				days in the month after this day. So if the PtransCloseDate
--				is the 28th, just after midnight on the 29th, the final ptrans
--				for the month will be produced, and that same file will be 
--				sent on the following 2 days, adhering to the blackout
--				period.
--				Code below says - find the last day of the month of Run Date. 
--				Starting from that last day, write each business date of the month
--				to a temp table with ident. A business day is any day of the
--				week other than Saturday or Sunday. In December, if the 31st
--				falls on a Friday, that is not a business day. In May, if this
--				is a Monday and the day is 25 or larger, it is not a business day.  
--				In November, if this is Thursday the 22 - 28, it is not a business day,
--              In November, if this is Friday the 23 - 29, it is not a business day.
--				There's no attempt to identify and exclude New Years, Independence
--				Day, Labor Day or Christmas holidays because these have no bearing
--				on the blackout period. 
--				When done writing to the table, the third row will have the 
--				begin date of the blackout period. Subtract 1 day to get the
--				close date.. 
-- =============================================
Create FUNCTION [custom].[GetPtransCloseDate] 
(
	-- Add the parameters for the function here
	@RunDate smalldatetime
)
RETURNS smalldatetime
AS
BEGIN
	-- Declare the return variable here
DECLARE @CloseDate smalldatetime

-- Derive begin date and end date of current - run - month 
Declare @BeginDate as smalldatetime
Set @BeginDate = convert(smalldatetime, cast(datepart(mm,@RunDate) as varchar(2)) + '/01/' + cast(datepart(yyyy,@RunDate) as varchar(4)))
Declare @EndDate as smalldatetime
Set @EndDate = DateAdd(dd,-1,convert(smalldatetime, cast(datepart(mm,DateAdd(mm,1,@RunDate)) as varchar(2)) + '/01/' + cast(datepart(yyyy,DateAdd(mm,1,@RunDate)) as varchar(4))))
--Select @BeginDate, @EndDate

-- specifically create temp table so we have an ident on each row
DECLARE @RunDays TABLE ([ID] [int] IDENTITY(1,1) NOT NULL, [RunDate] smalldatetime NOT NULL )

-- Counting down from EndDate, write business dates to table. Skip weekends and any Decemeber 31 that falls on a Friday
While @BeginDate < @EndDate
Begin
	If datename(weekday, @EndDate) <> 'Sunday' and datename(weekday, @EndDate) <> 'Saturday'
			and not (datename(weekday, @EndDate) = 'Friday' and datename(month, @EndDate) = 'December' and datepart(dd,@EndDate) = 31 )  
            and not (datename(weekday, @EndDate) = 'Monday' and datename(month, @EndDate) = 'May' and datepart(dd,@EndDate) >= 25 )  
            and not (datename(weekday, @EndDate) = 'Thursday' and datename(month, @EndDate) = 'November' and datepart(dd,@EndDate) between 22 and 28 )  
            and not (datename(weekday, @EndDate) = 'Friday' and datename(month, @EndDate) = 'November' and datepart(dd,@EndDate) between 23 and 29 )  
		Begin
		Insert into @RunDays values (@EndDate)
	End
		set @EndDate = DateAdd(dd,-1,@EndDate) 
End

-- Close date will be 4th row of table
Select @CloseDate = (Select DateAdd(dd,-1,RunDate) from @RunDays where ID = 3)

	-- Return the result of the function
	RETURN @CloseDate

END
