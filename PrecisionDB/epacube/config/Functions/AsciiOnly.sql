﻿--DMU_Update_AsciiOnly

--GHS	Feb 9, 2014	Revised to allow for null data; otherwise was only getting results when every row had data for the column in question

-- =============================================
-- Author:		<Gary Stone>
-- Create date: <August 19, 2013>
-- =============================================
Create FUNCTION [config].[AsciiOnly] 
(
	-- Add the parameters for the function here
	@OrigString varchar(128)
)
RETURNS VARCHAR(128)
AS
BEGIN
Declare @strOldString varchar(128)
Declare @strThisCharacter varchar(128)
Declare @strNewString varchar(128)
Declare @i int
Declare @intLength int

Set @strOldString = @OrigString + ''
Set @intLength = Len(@strOldString)
Set @strNewString = ''
Set @i = 1
While @i <= @intLength
	Begin
	Set @strThisCharacter = Substring(@strOldString, @i, 1)
	Set @strThisCharacter = 
		Case when ascii(@strThisCharacter) between 32 and 122 and @strThisCharacter <> '?' then @strThisCharacter end	

	Set @strNewString = @strNewString + isnull(@strThisCharacter, '')
	Set @i = @i + 1
End
	RETURN @strNewString

END
