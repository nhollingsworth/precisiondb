﻿CREATE TABLE [config].[tblXREF] (
    [TableSource]     VARCHAR (64)   NULL,
    [FieldSourceRoot] VARCHAR (64)   NULL,
    [ValueOriginal]   VARCHAR (128)  NULL,
    [ValueXRef]       NVARCHAR (128) NULL,
    [ColumnSeq]       INT            NULL,
    [tblXREF_ID]      BIGINT         NOT NULL
);

