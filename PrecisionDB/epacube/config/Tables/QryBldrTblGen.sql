﻿CREATE TABLE [config].[QryBldrTblGen] (
    [QuerySource]         NVARCHAR (64)  NOT NULL,
    [TableName]           NVARCHAR (128) NULL,
    [ColumnName]          NVARCHAR (64)  NOT NULL,
    [OrgName]             NVARCHAR (64)  NULL,
    [QueryNameSource]     NVARCHAR (128) NULL,
    [TableNameTarget]     NVARCHAR (128) NULL,
    [WhereClauseEPAcube]  VARCHAR (MAX)  NULL,
    [lngJoinType]         INT            NULL,
    [lngOrgType]          INT            NULL,
    [WS_FK]               INT            NOT NULL,
    [QryBldrGenTblDef_ID] INT            NOT NULL
);

