﻿CREATE TABLE [config].[columns] (
    [Profiles_Data_Source_FK]  INT            NOT NULL,
    [Table_Name]               NVARCHAR (128) NOT NULL,
    [Column_Name]              NVARCHAR (128) NOT NULL,
    [Ordinal_Position]         INT            NULL,
    [Data_Type]                NVARCHAR (64)  NULL,
    [Character_Maximum_Length] INT            NULL,
    [WS_FK]                    INT            NULL,
    [Columns_ID]               INT            NOT NULL
);

