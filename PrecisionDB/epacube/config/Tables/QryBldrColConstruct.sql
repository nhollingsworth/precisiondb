﻿CREATE TABLE [config].[QryBldrColConstruct] (
    [ColType]                NVARCHAR (64)  NULL,
    [ColCode]                NVARCHAR (256) NULL,
    [ColAlias]               NVARCHAR (64)  NOT NULL,
    [QryBldrQueries_FK]      INT            NOT NULL,
    [QryBldrColConstruct_ID] INT            NOT NULL
);

