﻿CREATE TABLE [config].[Data_Source_Names] (
    [Profiles_Data_Source_FK] INT            NOT NULL,
    [FileName]                NVARCHAR (255) NOT NULL,
    [Sel]                     INT            NULL,
    [XMLtblName]              NVARCHAR (50)  NULL,
    [Type]                    INT            NULL,
    [Seq]                     INT            NULL,
    [SS_ID]                   INT            NOT NULL
);

