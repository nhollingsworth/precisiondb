﻿CREATE TABLE [config].[ws] (
    [Profiles_Data_Source_FK] INT            NOT NULL,
    [Name]                    NVARCHAR (255) NOT NULL,
    [Table_Name]              NVARCHAR (255) NULL,
    [WS_Mult_FK]              INT            NULL,
    [WS_ID]                   INT            NOT NULL
);

