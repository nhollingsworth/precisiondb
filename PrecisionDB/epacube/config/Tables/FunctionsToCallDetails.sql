﻿CREATE TABLE [config].[FunctionsToCallDetails] (
    [WS_FK]                     INT            NULL,
    [Table_Name]                NVARCHAR (64)  NULL,
    [Column_Name]               NVARCHAR (64)  NULL,
    [FunctionName]              NVARCHAR (128) NULL,
    [FunctionDescription]       VARCHAR (MAX)  NULL,
    [Timing]                    NVARCHAR (64)  NULL,
    [Action]                    INT            NULL,
    [lngVariable1]              INT            NULL,
    [lngVariable2]              INT            NULL,
    [lngVariable3]              INT            NULL,
    [strVariable1]              NVARCHAR (255) NULL,
    [strVariable2]              NVARCHAR (255) NULL,
    [strVariable3]              NVARCHAR (255) NULL,
    [chkStrip]                  INT            NULL,
    [Separator1]                NVARCHAR (16)  NULL,
    [Separator2]                NVARCHAR (16)  NULL,
    [FunctionCriteria]          NVARCHAR (255) NULL,
    [Sequence]                  INT            NOT NULL,
    [FunctionsToCall_FK]        INT            NULL,
    [FunctionsToCallDetails_ID] INT            NOT NULL
);

