﻿CREATE TABLE [config].[epaCUBE_Tables] (
    [Table_Name]        NVARCHAR (100) NULL,
    [Alias]             NVARCHAR (3)   NULL,
    [Active]            BIT            NULL,
    [DataTypes]         NVARCHAR (50)  NULL,
    [Schema]            NVARCHAR (255) NULL,
    [DN_Defined]        BIT            NULL,
    [qryLNK]            NVARCHAR (255) NULL,
    [JoinSQL]           NTEXT          NULL,
    [epacube_tables_id] INT            NOT NULL
);

