﻿CREATE TABLE [config].[columns_last_import] (
    [Profiles_Data_Source_FK]  INT            NOT NULL,
    [Table_Name]               NVARCHAR (128) NOT NULL,
    [Column_Name]              NVARCHAR (128) NOT NULL,
    [Ordinal_Position]         INT            NULL,
    [Data_Type]                NVARCHAR (64)  NULL,
    [Character_Maximum_Length] INT            NULL,
    [WS_FK]                    INT            NULL,
    [Columns_ID]               INT            NULL,
    [Error_Msg]                VARCHAR (128)  NULL
);

