﻿CREATE TABLE [config].[tblMapsDestinations] (
    [Profiles_Data_Source_FK]     INT            NOT NULL,
    [TableSource]                 NVARCHAR (128) NOT NULL,
    [TableDestination]            NVARCHAR (128) NOT NULL,
    [FieldSource]                 NVARCHAR (128) NULL,
    [ColumnTransformation]        VARCHAR (MAX)  NULL,
    [FieldDestination]            NVARCHAR (128) NOT NULL,
    [TblDestinations_FK]          INT            NOT NULL,
    [DataPosition]                NVARCHAR (64)  NULL,
    [ynDateField]                 INT            NULL,
    [FieldSourceRoot]             NVARCHAR (150) NULL,
    [FieldSourceGroup]            NVARCHAR (128) NULL,
    [ColumnTransformationInitial] NVARCHAR (150) NULL,
    [Seq]                         INT            NULL,
    [DateOfChange]                DATETIME       NULL,
    [WS_FK]                       INT            NULL,
    [LastChangedBy]               NVARCHAR (128) NULL,
    [RequiresGrouping]            INT            NULL,
    [Update_User]                 NVARCHAR (128) NULL,
    [Update_Timestamp]            DATETIME       NULL,
    [tblMapsDestinations_ID]      INT            NOT NULL
);

