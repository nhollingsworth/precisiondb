﻿CREATE TABLE [config].[tbldestinations] (
    [TableDestination]         NVARCHAR (50)  NULL,
    [lnkTblDestinationAlias]   NVARCHAR (50)  NULL,
    [tblDestinations_ID]       INT            NOT NULL,
    [TableDestinationType]     INT            NULL,
    [Import_Package_Name]      NVARCHAR (255) NULL,
    [Import_Package_FK]        BIGINT         NULL,
    [TableSource]              NVARCHAR (100) NULL,
    [TableSourceLcl]           NVARCHAR (128) NULL,
    [WS_FK]                    INT            NULL,
    [Profiles_Data_Source_FK]  INT            NULL,
    [WhereTransformation]      VARCHAR (MAX)  NULL,
    [memWherePT]               VARCHAR (MAX)  NULL,
    [QueryName]                NVARCHAR (100) NULL,
    [exportPathDefault]        NVARCHAR (255) NULL,
    [exportPath]               NVARCHAR (255) NULL,
    [exportFilename]           NVARCHAR (50)  NULL,
    [exportFileExtension]      NVARCHAR (3)   NULL,
    [exportFormat]             NVARCHAR (50)  NULL,
    [exportSpec]               NVARCHAR (50)  NULL,
    [ynDistinct]               INT            NULL,
    [ynIncrementJobID]         INT            NULL,
    [PivotColumnValue]         NVARCHAR (50)  NULL,
    [PivotColumnQualifier]     NVARCHAR (50)  NULL,
    [PivotColumnName]          NVARCHAR (50)  NULL,
    [ynPivotExport]            INT            NULL,
    [ynColumnNamesExport]      INT            NULL,
    [Call_SP_Name]             NVARCHAR (128) NULL,
    [TableDestinationSchema]   VARCHAR (64)   NULL,
    [TableDestinationName]     VARCHAR (64)   NULL,
    [TableDestinationDatabase] VARCHAR (64)   NULL,
    [exportColumnDelimiter]    VARCHAR (16)   NULL,
    [exportTextDelimiter]      VARCHAR (16)   NULL
);

