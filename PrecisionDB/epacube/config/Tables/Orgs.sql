﻿CREATE TABLE [config].[Orgs] (
    [Profiles_Data_Source_FK]    INT            NOT NULL,
    [Org_Identifier]             NVARCHAR (128) NOT NULL,
    [Org_Name]                   NVARCHAR (128) NULL,
    [Entity_Structure_FK]        BIGINT         NOT NULL,
    [Parent_Entity_Structure_FK] BIGINT         NULL,
    [Entity_Data_Name_FK]        BIGINT         NOT NULL,
    [Orgs_ID]                    INT            NOT NULL
);

