﻿CREATE TABLE [config].[QryBldrColumns] (
    [Column_Name_User]    NVARCHAR (64) NULL,
    [Schema]              NVARCHAR (64) NULL,
    [Table_Name]          NVARCHAR (64) NULL,
    [Table_Name_Alias]    NVARCHAR (64) NULL,
    [Column_Name]         NVARCHAR (64) NULL,
    [Data_Type]           NVARCHAR (64) NULL,
    [Data_Name_FK]        NVARCHAR (64) NOT NULL,
    [Reference_FK]        VARCHAR (255) NULL,
    [Org_Ind]             INT           NULL,
    [Seq]                 INT           NULL,
    [Entity_Data_Name_FK] NVARCHAR (64) NOT NULL,
    [QryBldrQueries_FK]   INT           NOT NULL,
    [QryBldrColumns_ID]   INT           NOT NULL
);

