﻿CREATE TABLE [config].[Filters_Child] (
    [Filters_Parent_FK] INT          NOT NULL,
    [ColName]           VARCHAR (96) NOT NULL,
    [ColAction]         VARCHAR (16) NOT NULL,
    [Filters_Child_ID]  INT          NOT NULL
);

