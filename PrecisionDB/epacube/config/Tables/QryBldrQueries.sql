﻿CREATE TABLE [config].[QryBldrQueries] (
    [QuerySource]       NVARCHAR (64)  NULL,
    [QueryName]         NVARCHAR (128) NOT NULL,
    [WhereClause]       VARCHAR (MAX)  NULL,
    [DerivationColumns] VARCHAR (128)  NULL,
    [QryBldrQueries_ID] INT            NOT NULL,
    [WS_FK]             INT            NULL,
    [PivotOrgLabel]     VARCHAR (32)   NULL,
    [PivotValueLabel]   VARCHAR (32)   NULL
);

