﻿CREATE TABLE [config].[Defaults] (
    [Class]       NVARCHAR (64)  NULL,
    [Column]      NVARCHAR (64)  NULL,
    [Seq]         INT            NULL,
    [Name]        NVARCHAR (64)  NULL,
    [Value]       NVARCHAR (64)  NULL,
    [Description] NVARCHAR (MAX) NULL,
    [Active]      INT            NULL,
    [Defaults_ID] INT            NOT NULL,
    [Variable1]   VARCHAR (92)   NULL,
    [Variable2]   VARCHAR (96)   NULL
);

