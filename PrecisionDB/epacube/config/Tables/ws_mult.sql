﻿CREATE TABLE [config].[ws_mult] (
    [profiles_data_source_fk] INT           NOT NULL,
    [WorkSheetName]           NVARCHAR (64) NOT NULL,
    [SpreadsheetName]         NVARCHAR (64) NULL,
    [WorkSheetNameInitial]    NVARCHAR (64) NULL,
    [WS_FK]                   INT           NOT NULL,
    [WS_Mult_ID]              INT           NOT NULL
);

