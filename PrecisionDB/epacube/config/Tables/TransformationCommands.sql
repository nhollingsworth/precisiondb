﻿CREATE TABLE [config].[TransformationCommands] (
    [FunctionName]            NVARCHAR (64) NULL,
    [CommandName]             NVARCHAR (64) NULL,
    [LblParam1]               INT           NULL,
    [Param1]                  NVARCHAR (64) NULL,
    [LblParam2]               INT           NULL,
    [Param2]                  NVARCHAR (64) NULL,
    [Group]                   INT           NULL,
    [Group Disallow]          INT           NULL,
    [HelpText]                VARCHAR (MAX) NULL,
    [Hint]                    VARCHAR (MAX) NULL,
    [HiddenObject]            NVARCHAR (64) NULL,
    [FunctionSpecificCommand] NTEXT         NULL,
    [ShowProcessButton]       INT           NULL,
    [DisplayToUser]           INT           NULL,
    [ExampleText]             VARCHAR (MAX) NULL,
    [ExampleImage]            IMAGE         NULL,
    [StringCommand_Before]    NVARCHAR (64) NULL,
    [StringCommand_After]     NVARCHAR (64) NULL
);

