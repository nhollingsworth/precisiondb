﻿CREATE TABLE [config].[Filters_Parent] (
    [Profiles_Data_Source_FK] BIGINT        NOT NULL,
    [TblDestinations_FK]      BIGINT        NOT NULL,
    [WhereFilter]             VARCHAR (MAX) NULL,
    [Filters_Parent_ID]       INT           NOT NULL
);

