﻿CREATE TABLE [config].[FunctionsToCall] (
    [FunctionName]        NVARCHAR (100) NULL,
    [FunctionDescription] NVARCHAR (MAX) NULL,
    [FunctionsToCall_ID]  INT            NOT NULL,
    [TableNameReq]        INT            NULL,
    [ColumnNameReq]       INT            NULL,
    [OptActionReq]        INT            NULL,
    [Seq]                 INT            NULL,
    [Active]              INT            NULL,
    [ImportTypeRequired]  INT            NULL
);

