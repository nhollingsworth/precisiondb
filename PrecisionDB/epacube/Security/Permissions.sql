﻿GRANT VIEW ANY COLUMN ENCRYPTION KEY DEFINITION TO PUBLIC;


GO
GRANT VIEW ANY COLUMN MASTER KEY DEFINITION TO PUBLIC;


GO
GRANT CONNECT TO [epacube];


GO
GRANT ALTER TO [db_epacube_developers];


GO
GRANT DELETE TO [db_epacube_developers];


GO
GRANT EXECUTE TO [db_epacube_developers];


GO
GRANT INSERT TO [db_epacube_developers];


GO
GRANT SELECT TO [db_epacube_developers];


GO
GRANT SHOWPLAN TO [db_epacube_developers];


GO
GRANT UPDATE TO [db_epacube_developers];


GO
GRANT CONNECT TO [NONPROD-DB\epaCUBE Developers];


GO
GRANT CONNECT TO [cvoutour];

