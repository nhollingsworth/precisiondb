﻿
-- =============================================
-- Author:			Gary Stone
-- Create date:		2018-06-15
-- Description:		Proceses cost changes into scheduled retail price changes - zone
-- Modifications
--	5/5/20			Jira 1873/1873	Remove comment-out from section 'Load alias headers for non-level-gross items...'
--	5/12/2020						Same Jira - Adjusted Retail and New GM% on non level gross alias when same level gross alias exists in same zone.
--  8/18/2020		Jira 1926/IN1043 Because the item with the highest cost is not the item in parity, and the National Brand relationship is to an item not in an alias, the system first calculated the correct differential between the National Brand and the Private Label, but then recalculated the alias price based on the target margin of 85644, and adjusted the price back – which is of course incorrect. Created new case statement in sub-select, line 854.
-- =============================================
CREATE PROCEDURE [precision].[price_maintenance_processing_zone] @MarginChanges int = 0, @Wk int = 0, @ReinstateRescind int = 0
AS

----set @wk to 0 for current week's level gross calculations and insert; 1 to regenerate and replace last week's data, 2 to regenerate and replace 2 weeks ago data, etc.
	--declare @marginchanges int = 0
	--declare @wk int = 0
	--declare @ReinstateRescind int = 0

	delete from epacube.epacube.segments_settings 
	where 1 = 1
	and data_source = 'user update' 
	and (create_user like '%epacube%' or create_user = 'dba')
	and isnull((select comment from epacube.epacube_params where value = 'URM' and name = 'EPACUBE CUSTOMER'), '') <> 'DEV'

	Declare @Qry varchar(Max), @QryID as int
	Set @Qry = 
	'Insert into dbo.pentaho_processing_test_log (proc_name, process_time_start, ProcParameters)
	Select ''precision.price_maintenance_processing_zone'' ''proc_name'', getdate() ''process_time_start'', ''@MarginChanges = ' + cast(@MarginChanges as varchar(8)) + '; @Wk = ' + cast(@Wk as varchar(8)) + '; @ReinstateRescind = ' + cast(@ReinstateRescind as varchar(8)) + ''''

	if object_id('dbo.pentaho_processing_test_log') is not null
	Begin
		exec(@Qry)
		Set @QryID = (select top 1 pentaho_processing_test_log_id from dbo.pentaho_processing_test_log order by pentaho_processing_test_log_id desc)
	End

	if object_id('tempdb..#pbv') is not null
	drop table #pbv

	if object_id('tempdb..#t_prod') is not null
	drop table #t_prod

	if object_id('tempdb..#zones') is not null
	drop table #zones

	if object_id('tempdb..#nz') is not null
	drop table #nz

	if object_id('tempdb..#mc') is not null
	drop table #mc

	if object_id('tempdb..#pmccf') is not null
	drop table #pmccf

	if object_id('tempdb..#MCadjs') is not null
	drop table #MCadjs

	if object_id('tempdb..#item_groups') is not null
	drop table #item_groups

	if object_id('tempdb..#multi') is not null
	drop table #multi

	declare @zone_type varchar(8) = 'h'
	declare @daysout int = 7

	declare @price_change_effective_date date
	set @price_change_effective_date = cast(getdate() + 6 - (@wk * 7) - datepart(dw, getdate()) as date)
	
	declare @datelastproc date
	set @datelastproc = cast(getdate() + 6 - (@wk * 7) - datepart(dw, getdate()) as date)
	set @datelastproc = dateadd(d, -7, @datelastproc)
	
	declare @lgprocdate date
	set @lgprocdate = cast(getdate() + 3 - (@wk * 7) - datepart(dw, getdate()) as date)

	--select @lgprocdate '@lgprocdate', @price_change_effective_date '@price_change_effective_date', @datelastproc '@datelastproc'

		create table #pbv(value numeric(18, 6) null, unit_value numeric(18, 6) null, effective_date date null, pack int null, size_uom varchar(16) null, product_structure_fk bigint null, pricesheet_basis_values_id bigint null, data_name_fk bigint null
		, create_timestamp datetime null, cust_entity_structure_fk bigint null, drank bigint null, pbv_status varchar(128) null, add_to_zone_date date null, change_status int null, cost_change_processed_date date null
		, drank_f bigint null, ind_cost_change_item int null, item varchar(64) null, unique_cost_record_id varchar(64) null, pbv_id bigint identity(1, 1) not null)

		create table #t_prod(item_change_type_cr_fk bigint null, qualified_status_cr_fk bigint null, price_mgmt_type_cr_fk bigint null, pack int null, size_uom varchar(16) null
		, parent_product_structure_fk bigint null, item varchar(64) null, item_description varchar(64) null, product_structure_fk bigint null, entity_structure_fk bigint null, cost_change_effective_date_cur date null
		, cost_change_effective_date date null, case_cost_new numeric(18, 6) null, unit_cost_new numeric(18, 6) null, case_cost_cur numeric(18, 2) null, unit_cost_cur numeric(18, 4) null, price_multiple int null
		, price_multiple_new int null, price_effective_date_cur date null, price_cur numeric(18, 2) null, price_new_initial numeric(18, 2) null, price_new numeric(18, 2) null, tm_pct_cur numeric(18, 4) null
		, urm_target_margin numeric(18, 4) null, gp_new numeric(18, 4) null, pricesheet_basis_values_fk bigint null, pricesheet_basis_values_fk_cur bigint null, pbv_status varchar(128) null, change_status int null
		, cost_change_processed_date date null, ind_cost_change_item int null, ind_requested_item int null, parity_type varchar(16) null, parity_status varchar(8) null, ind_parity_status int null, ind_parity int null
		, parity_parent_product_structure_fk bigint null, parity_child_product_structure_fk bigint null, parity_parent_item varchar(64) null, parity_child_item varchar(64) null, parity_discount_amt numeric(18, 2) null
		, parity_mult int null, parity_discount_pct numeric(18, 4) null, alias_id bigint null, alias_description varchar(64) null, parity_alias_id bigint null, ind_initial_load int null, load_step int null
		, lg_active varchar(8) null, lg_flag varchar(8) null, use_lg_price_points varchar(8) null, minamt numeric(18, 2) null, zone_id varchar(64) null, zone_name varchar(64) null, zone_sort int null, pricing_steps varchar(128) null
		, related_item_alias_id bigint null, unit_cost_new_actual numeric(18, 4), subgroup_id bigint null, subgroup_description varchar(64) null, subgroup_data_value_fk bigint null, item_sort bigint null, item_group_root bigint null, item_group bigint null
		, ind_display_as_reg int null, Digit_DN_FK bigint null, Digit_Adj Numeric(18, 2) Null, price_rounded Numeric(18, 2) Null, ftn_breakdown_parent varchar(8) null, ftn_cost_delta varchar(8) null, stage_for_processing_fk bigint null
		, igai bigint null, qualified_status_step varchar(64) Null, ig_alias_item bigint null, unique_cost_record_id varchar(64) null, t_prod_id bigint identity(1, 1) not null)

		create table #zones(entity_structure_fk bigint not null, zone_type varchar(8) null, zone_id varchar(16) not null, zone_name varchar(64) null, daysout int not null, lg_active varchar(8))

		insert into #zones
		(entity_structure_fk, zone_type, zone_id, zone_name, daysout, lg_active)
		select zone_segment_fk, zone_type, zone_id, zone_name, daysout, lg_active from (
		select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) dnl, dv.value 'zone_type', eiz.value 'zone_id', eizn.value 'zone_name'
		, ss.zone_segment_fk, cast(ss.attribute_event_data as numeric(18, 0)) 'daysout', ss.effective_date, lga.lg_active
		, dense_rank()over(partition by ss.zone_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) 'drank'
		from epacube.segments_settings ss with (nolock)
		inner join epacube.entity_attribute ea with (nolock) on ss.zone_segment_fk = ea.entity_structure_fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
		inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
		inner join epacube.entity_identification eiz with (nolock) on ss.zone_segment_fk = eiz.entity_structure_fk and eiz.entity_data_name_fk = 151000 and eiz.data_name_fk = 151110
		left join epacube.entity_ident_nonunique eizn with (nolock) on eiz.entity_structure_fk = eizn.entity_structure_fk and eizn.data_name_fk = 140200
		left join
			(	select * from (
				select ss.attribute_y_n 'lg_active', ss.zone_segment_fk, ss.segments_settings_id
				, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) drank
				from epacube.segments_settings ss with (nolock) 
				where ss.data_name_fk = 144862
				and ss.prod_segment_fk is null
				and ss.zone_segment_fk is not null 
				and cust_entity_structure_fk is null
				and ss.effective_date <= @price_change_effective_date
				and ss.record_status_cr_fk = 1
				) a where drank = 1
			) lga on ss.zone_segment_fk = lga.zone_segment_fk
		where 1 = 1
		and cust_entity_structure_fk is null
		and ss.data_name_fk = 144868
		and ss.effective_date <= @price_change_effective_date
		and ss.record_status_cr_fk = 1
		) a where drank = 1
		and zone_type = @zone_type

		Create Table #MC(product_structure_fk bigint null, entity_structure_fk bigint null, stage_for_processing_fk bigint null, new_tm Numeric(18, 4), MC_ID bigint identity(1, 1) Not Null)
		
		Insert into #MC
		Select a.product_structure_fk, a.entity_structure_fk, a.stage_for_processing_fk, a.new_tm
		from (
			Select P.product_structure_fk, p.entity_structure_fk, P.stage_for_processing_id 'stage_for_processing_fk', p.value 'new_tm'
			, dense_rank()over(partition by p.product_structure_fk, p.entity_structure_fk, p.data_name_fk order by p.create_timestamp desc) DRank
			from [precision].stage_for_processing P
			inner join epacube.segments_product sp with (nolock) on p.product_structure_fk = sp.PRODUCT_STRUCTURE_FK and sp.data_name_fk = 110103
			inner join epacube.product_association pa with (nolock) on sp.product_structure_fk = pa.PRODUCT_STRUCTURE_FK and pa.DATA_NAME_FK = 159450 and p.entity_structure_fk = pa.ENTITY_STRUCTURE_FK and pa.record_status_cr_fk = 1
			inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
			inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
			where processing_Type = 'Level Gross MC' 
			and (
					(P.Processed_Timestamp is null and @MarginChanges = 1)
					or @MarginChanges = 0
				)
			and P.price_change_effective_date = @price_change_effective_date
			and P.data_name_fk = 502046
			and dv.value = @zone_type
		) a where DRank = 1

		Alter table #MC Add Primary Key([MC_ID])
		create index idx_01 on #MC(Product_structure_fk, entity_structure_fk)
		create index idx_02 on #MC(stage_for_processing_fk)
----
If @MarginChanges = 0
Begin
--identify items that are new to the zone
		select pa.product_structure_fk, cast(min(pa.create_timestamp) as date) 'add_to_zone_date'
		into #nz
		from epacube.product_association pa with (nolock) 
		inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_fk = ea.entity_structure_fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
		inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
		inner join #zones z on pa.entity_structure_fk = z.entity_structure_fk
		where pa.data_name_fk = 159450 
			and pa.record_status_cr_fk = 1
			and @marginchanges = 0
		group by pa.product_structure_fk
		having min(pa.create_timestamp) >= cast(getdate() + 4 - 7 - (@wk * 7) - datepart(dw, getdate()) as date)

		create index idx_01 on #nz(product_structure_fk, add_to_zone_date)

--stage most current [cost changes to processing] date
		select * 
		into #pmccf
		from (
				Select 
				a.pricesheet_basis_values_id
				, a.cost_change_Effective_date 'effective_date'
				, a.product_structure_FK
				, a.zone_entity_structure_fk
				, a.data_name_fk
				, a.unit_cost_new
				, pi.value 'item'
				, a.daysout
				, a.price_change_effective_date 'cost_change_processed_date'
				, a.unique_cost_record_id
				from	(
							Select prc.price_change_effective_date
							, prc.product_structure_FK
							, prc.cost_change_Effective_date
							, prc.unit_cost_new
							, pbv.Pricesheet_Basis_Values_ID
							, pbv.unique_cost_record_id
							, pbv.Zone_Entity_Structure_FK
							, prc.DAYSOUT
							, pbv.data_name_fk
							, dense_rank()over(partition by prc.product_structure_fk, prc.DAYSOUT order by prc.price_change_effective_date desc, prc.cost_change_effective_date desc, pbv.pricesheet_basis_values_id desc) 'DRank'
							from marginmgr.PRICESHEET_RETAIL_CHANGES prc
							inner join marginmgr.PRICESHEET_BASIS_VALUES pbv on	prc.unique_cost_record_id = pbv.unique_cost_record_id
							where 1 = 1
							and pbv.RESCIND_TIMESTAMP is null
							and pbv.RECORD_STATUS_CR_FK = 1
							and prc.unit_cost_cur <> prc.unit_cost_new
							and prc.daysout = @daysout
							and prc.price_change_effective_date <= @datelastproc
							and prc.unique_cost_record_id not in (
																		select prcA.unique_cost_record_id
																		from marginmgr.pricesheet_retail_changes  prcA
																		inner join marginmgr.PRICESHEET_BASIS_VALUES pbv on prcA.unique_cost_record_id = pbv.unique_cost_record_id
																		where prcA.PRICE_CHANGE_EFFECTIVE_DATE = dateadd(d, -7, @price_change_effective_date)
																		and prcA.COST_CHANGE_REASON like 'new to zone%' 
																		and prcA.cost_change_effective_date > dateadd(d, -7, @price_change_effective_date)
																		and isnull(pbv.Change_Status, 0) = -1
																		group by prcA.unique_cost_record_id
																)
							group by prc.price_change_effective_date
							, prc.product_structure_FK
							, prc.cost_change_Effective_date
							, prc.unit_cost_new
							, pbv.Pricesheet_Basis_Values_ID
							, pbv.unique_cost_record_id
							, pbv.Zone_Entity_Structure_FK
							, prc.DAYSOUT
							, pbv.data_name_fk
						) A
						inner join epacube.PRODUCT_IDENTIFICATION pi with (nolock) on a.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK and pi.data_name_fk = 110100
					where DRank = 1
				) Z
		order by cost_change_processed_date desc

		create index idx_01 on #pmccf(product_structure_fk, zone_entity_structure_fk, data_name_fk)

--find all items with cost changes to process
		insert into #pbv
		(value, unit_value, effective_date, pack, product_structure_fk, pricesheet_basis_values_id, data_name_fk, create_timestamp, cust_entity_structure_fk, drank, pbv_status, add_to_zone_date, change_status, cost_change_processed_date, unique_cost_record_id, drank_f, ind_cost_change_item)
		select c.*, 1 'ind_cost_change_item' 
		from (
				select *,  dense_rank()over(partition by product_structure_fk order by 
					case when pbv_status like 'new to zone%' then 10
						when pbv_status = 'future' and isnull(change_status, 0) = 0 then 30
						when pbv_status = 'current' then 40
						end) drank_f
					from (
						select * from (
										select pbv.value, pbv.unit_value, pbv.effective_date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, pbv.create_timestamp
										, case when isnull(patt.attribute_event_data, 'w') <> 'w' then pbv.cust_entity_structure_fk end 'cust_entity_structure_fk' 
										, dense_rank()over(partition by pbv.data_name_fk, pbv.product_structure_fk order by pbv.effective_date desc, pbv.pricesheet_basis_values_id desc) drank
										, 'current' 'pbv_status', null 'add_to_zone_date', pbv.change_status, pmccf.cost_change_processed_date, pbv.unique_cost_record_id
										from marginmgr.pricesheet_basis_values pbv with (nolock) 
										left join epacube.product_attribute patt with (nolock) on pbv.product_structure_fk = patt.product_structure_fk and patt.data_name_fk = 500000
										left join #pmccf pmccf on pbv.product_structure_fk = pmccf.product_structure_fk
																												and pbv.data_name_fk = pmccf.data_name_fk
																												and pbv.zone_entity_structure_fk = pmccf.zone_entity_structure_fk 
										where pbv.data_name_fk = 503201 
										and cust_entity_structure_fk is null
										and (
												(cast(pbv.effective_date as date) <= dateadd(d, 1, @price_change_effective_date) and isnull(pbv.Change_Status, 0) = -1)
												or
												(cast(pbv.effective_date as date) <= @price_change_effective_date and isnull(pbv.Change_Status, 0) = 0)
											)
										and cast(pbv.effective_date as date) > dateadd(d, -7, @lgprocdate)
										and (
											(@ReinstateRescind = 0 and cast(pbv.create_timestamp as date) <= @lgprocdate)
											or
											(@ReinstateRescind = 1 and cast(pbv.create_timestamp as date) <= @price_change_effective_date)
											)
										and pbv.record_status_cr_fk = 1
										and pbv.effective_date > isnull(pmccf.effective_date, '2019-01-01')
										and @marginchanges = 0
											) a where drank = 1
				union
						select * from (
										select pbv.value, pbv.unit_value, pbv.effective_date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, pbv.create_timestamp
										, case when isnull(patt.attribute_event_data, 'w') <> 'w' then pbv.cust_entity_structure_fk end 'cust_entity_structure_fk' 
										, dense_rank()over(partition by pbv.data_name_fk, pbv.product_structure_fk order by pbv.effective_date asc, pbv.pricesheet_basis_values_id desc) drank
										, 'future' 'pbv_status', null 'add_to_zone_date', pbv.change_status, pmccf.cost_change_processed_date, pbv.unique_cost_record_id
										from marginmgr.pricesheet_basis_values pbv with (nolock) 
										left join epacube.product_attribute patt with (nolock) on pbv.product_structure_fk = patt.product_structure_fk and patt.data_name_fk = 500000
										left join #pmccf pmccf on pbv.product_structure_fk = pmccf.product_structure_fk
																												and pbv.data_name_fk = pmccf.data_name_fk
																												and pbv.zone_entity_structure_fk = pmccf.zone_entity_structure_fk 
										where pbv.data_name_fk = 503201 
										and cust_entity_structure_fk is null
										and isnull(pbv.change_status, 0) = 0
										and cast(pbv.effective_date as date) between dateadd(d, 1, @price_change_effective_date) and dateadd(d, 7, @price_change_effective_date)
										and (
											(@ReinstateRescind = 0 and cast(pbv.create_timestamp as date) <= @lgprocdate)
											or
											(@ReinstateRescind = 1 and cast(pbv.create_timestamp as date) <= @price_change_effective_date)
											)
										
										and pbv.record_status_cr_fk = 1
										and pbv.effective_date > isnull(pmccf.effective_date, '2019-01-01')
										and pbv.effective_date > isnull(pmccf.cost_change_processed_date, '2019-01-01')
										and @marginchanges = 0
									) a where drank = 1
				union
						select * from (
										select pbv.value, pbv.unit_value, pbv.effective_date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, pbv.create_timestamp
										, case when isnull(patt.attribute_event_data, 'w') <> 'w' then pbv.cust_entity_structure_fk end 'cust_entity_structure_fk' 
										, dense_rank()over(partition by pbv.data_name_fk, pbv.product_structure_fk 
											order by datediff(d, first_added_to_any_h_zone, lg_process_date) desc, pbv.effective_date desc, pbv.pricesheet_basis_values_id desc
												) drank
										, 'new to zone ' + cast(first_added_to_any_h_zone as varchar(64)) 'pbv_status', add_to_zone_date, pbv.change_status, pmccf.cost_change_processed_date, pbv.unique_cost_record_id
										from epacube.marginmgr.pricesheet_basis_values pbv with (nolock) 
										left join epacube.epacube.product_attribute patt with (nolock) on pbv.product_structure_fk = patt.product_structure_fk and patt.data_name_fk = 500000
										left join #pmccf pmccf on pbv.product_structure_fk = pmccf.product_structure_fk
															and pbv.data_name_fk = pmccf.data_name_fk
															and pbv.zone_entity_structure_fk = pmccf.zone_entity_structure_fk 
										inner join #nz nz on pbv.product_structure_fk = nz.product_structure_fk
										inner join 
											(
												select
												item
												, first_added_to_any_h_zone
												, [lg_cutoff]
												, case when [lg_cutoff] >= first_added_to_any_h_zone then [lg_cutoff] else dateadd(d, 7, [lg_cutoff]) end 'lg_process_date'
												, effective_date 'cost_change_effective_date'
												, dateadd(d, 3, case when [lg_cutoff] >= first_added_to_any_h_zone then [lg_cutoff] else dateadd(d, 7, [lg_cutoff]) end) 'first_lg_opportunity'
												, [case cost]
												, [unit cost]
												, created
												, product_structure_fk
												from (
												select 
												item
												, first_added_to_any_h_zone
												, cast(cast(dateadd(d, 3, first_added_to_any_h_zone) as datetime) - datepart(dw, first_added_to_any_h_zone) as date) 'lg_cutoff'
												, effective_date
												, lg_date
												, value 'case cost'
												, unit_value 'unit cost'
												, created
												, product_structure_fk
												from (
												select pi.value 'item', pbv.effective_date
												, cast((
												select min(pa.create_timestamp) from epacube.epacube.product_association pa
												inner join epacube.epacube.entity_attribute ea with (nolock) on ea.entity_structure_fk = pa.entity_structure_fk and ea.data_name_fk = 502060
												inner join epacube.epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id
												where pa.data_name_fk = 159450 and pa.product_structure_fk = pbv.product_structure_fk
												and dv.value = 'h'
												) as date) 'first_added_to_any_h_zone'
												, pmccf.cost_change_processed_date 'lg_date', pbv.value, pbv.unit_value, cast(pbv.create_timestamp as date) 'created', pbv.product_structure_fk 
												from epacube.marginmgr.pricesheet_basis_values pbv
												inner join epacube.epacube.product_identification pi with (nolock) on pbv.product_structure_fk = pi.product_structure_fk and pi.data_name_fk = 110100
												inner join #nz nz on pbv.product_structure_fk = nz.product_structure_fk
												left join #pmccf pmccf on pbv.product_structure_fk = pmccf.product_structure_fk
																												and pbv.data_name_fk = pmccf.data_name_fk
																												and pbv.zone_entity_structure_fk = pmccf.zone_entity_structure_fk 
												where pbv.data_name_fk = 503201
												and pbv.effective_date <= dateadd(d, 7, @price_change_effective_date)
												and 
													(
														isnull(pbv.Change_Status, 0) = 0
														or
														(isnull(pbv.Change_Status, 0) = -1 and pbv.effective_date <=  @price_change_effective_date)
													)												
												and (
													(@ReinstateRescind = 0 and cast(pbv.create_timestamp as date) <= @lgprocdate)
													or
													(@ReinstateRescind = 1 and cast(pbv.create_timestamp as date) <= @price_change_effective_date)
													)
												) a
												) b
											) zoneadds on nz.product_structure_fk = zoneadds.product_structure_fk and pbv.effective_date = zoneadds.cost_change_effective_date
										where pbv.data_name_fk = 503201 
										and cust_entity_structure_fk is null
										and first_added_to_any_h_zone >= dateadd(d, -6, lg_process_date)
										and first_added_to_any_h_zone <= @lgprocdate
										and pbv.record_status_cr_fk = 1
										and @marginchanges = 0
									) a where drank = 1
				) b
			) c 
			where 1 = 1
				and c.drank_f = 1
End
Else
If @MarginChanges = 1
Begin

		If (Select count(*) from #MC) = 0
		goto eof

		Insert into #pbv
		(
		value, unit_value, Effective_Date, product_structure_fk, pricesheet_basis_values_id, data_name_fk, create_timestamp
			, Cust_Entity_Structure_FK, DRank, PBV_Status,Add_To_Zone_Date, Change_Status, unique_cost_record_id
		)
		Select * from (
			Select distinct pbv.value, pbv.unit_value, pbv.Effective_Date--, pbv.pack
			, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, pbv.create_timestamp
			, case when isnull(patt.attribute_event_data, 'W') <> 'W' then pbv.cust_entity_structure_fk end 'Cust_Entity_Structure_FK' 
			, dense_rank()over(partition by pbv.data_name_fk, pbv.product_structure_fk 
				order by pbv.effective_date desc
				, pbv.pricesheet_basis_values_id
				) DRank
			, 'Current' 'PBV_Status', Null 'Add_To_Zone_Date', pbv.Change_Status, pbv.unique_cost_record_id
			from marginmgr.pricesheet_basis_values pbv with (nolock)
			inner join #MC MC on pbv.Product_Structure_FK = MC.product_structure_fk 
			left join epacube.product_attribute patt with (nolock) on pbv.product_structure_fk = patt.product_structure_fk and patt.data_name_fk = 500000
			where pbv.data_name_fk = 503201 
			and cust_entity_structure_fk is null
			and 
				PBV.Effective_Date <= Case isnull(change_status, 0) when 0 then dateadd(d, 7, @Price_Change_Effective_Date) when -1 then @Price_Change_Effective_Date end
			and pbv.record_status_cr_fk = 1
				) C where drank = 1

End

If @ReinstateRescind = 1
Begin
	If (select count(*) from marginmgr.PRICESHEET_RETAIL_CHANGES 
		where PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date 
		and IND_MARGIN_CHANGES = @marginchanges
		and isnull(IND_RESCIND, 0) <> 0) = 0
	goto eof
	else
	--begin
		Delete from #pbv where 1 = 1
		and product_structure_fk NOT in 
			(
			Select distinct product_structure_fk
				from epacube.marginmgr.pricesheet_retail_changes prc 
				where 1 = 1 
				and PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
				and IND_MARGIN_CHANGES = @marginchanges
				and isnull(IND_RESCIND, 0) <> 0
				and ITEM_CHANGE_TYPE_CR_FK <> 656
				and isnull(IND_COST_CHANGE_ITEM, 0) = 1
			Union
			Select distinct product_structure_fk
				from epacube.marginmgr.pricesheet_retail_changes prc 
				where 1 = 1
				and PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
				and IND_MARGIN_CHANGES = @marginchanges
				and alias_id in
					(Select distinct alias_id
					from epacube.marginmgr.pricesheet_retail_changes prc 
					where 1 = 1
					and PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
					and IND_MARGIN_CHANGES = @marginchanges
					and isnull(IND_RESCIND, 0) <> 0
					)
			Union
			Select distinct product_structure_fk
				from epacube.marginmgr.pricesheet_retail_changes prc 
				where 1 = 1 
				and PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
				and IND_MARGIN_CHANGES = @marginchanges
				and isnull(IND_RESCIND, 0) = 0
				and isnull(IND_COST_CHANGE_ITEM, 0) = 1
				and product_structure_fk in 
					(
						Select distinct product_structure_fk
						from epacube.marginmgr.pricesheet_retail_changes prc 
						where 1 = 1 
						and PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
						and IND_MARGIN_CHANGES = @marginchanges
						and isnull(IND_RESCIND, 0) <> 0
						and ITEM_CHANGE_TYPE_CR_FK <> 656
					)
				and isnull(IND_COST_CHANGE_ITEM, 0) = 1
				and ITEM_CHANGE_TYPE_CR_FK <> 656
			)

			Insert into #pbv
			(value, unit_value, effective_date, pack, size_uom, product_structure_fk, pricesheet_basis_values_id, data_name_fk, create_timestamp, change_status, ind_cost_change_item, unique_cost_record_id)
			select value, unit_value, effective_date, pack, size_uom, product_structure_fk, pricesheet_basis_values_id, data_name_fk, create_timestamp, change_status, ind_cost_change_item, unique_cost_record_id
			from marginmgr.pricesheet_basis_values_icc where product_structure_fk not in (select product_structure_fk from #pbv)

End
		update pbv
		set item = pi.value
		from #pbv pbv
		inner join epacube.product_identification pi on pbv.product_structure_fk = pi.product_structure_fk and pi.data_name_fk = 110100

		If @MarginChanges = 1
		Begin
			exec [precision].[pricing_calcs_common_tmp_tbls] Null, @price_change_effective_date
			delete from #t_prod where isnull(lg_flag, '') = 'N'
		End
		else
		Begin
			exec [precision].[pricing_calcs_common_tmp_tbls] @lgprocdate, @price_change_effective_date
		End

	update tp
	set URM_Target_Margin = mc.new_tm
	, stage_for_processing_fk = mc.stage_for_processing_fk
	from #t_prod tp
	inner join #mc mc on tp.product_structure_fk = mc.product_structure_fk and tp.entity_structure_fk = mc.entity_structure_fk

	update tp
	set subgroup_id = dv.value
	, subgroup_description = dv.description
	, subgroup_data_value_fk = dv.data_value_id
	from #t_prod tp
	inner join epacube.segments_product sp with (nolock) on tp.product_structure_fk = sp.product_structure_fk and sp.data_name_fk = 501045
	inner join epacube.data_value dv with (nolock) on sp.prod_segment_fk = dv.data_value_id 

	Update tp
	Set case_cost_cur = case_cost_new
	, unit_cost_cur = unit_cost_new
	, cost_change_effective_date_cur = cost_change_effective_date
	, pricesheet_basis_values_fk_cur = pricesheet_basis_values_fk
	from #t_prod tp
	where tp.cost_change_effective_date <= dateadd(d, -7, @LGprocDate)

	update tp
	set case_cost_new = pbv1.value
	, unit_cost_new = pbv1.unit_value
	from #t_prod tp
	left join
		(select * from (
		select pbv.product_structure_fk, pbv.value, pbv.unit_value, dense_rank()over(partition by product_structure_fk order by effective_date desc) 'drank'
		from marginmgr.pricesheet_basis_values pbv with (nolock)
			where pbv.effective_date <= @price_change_effective_date 
			and pbv.data_name_fk = 503201 
			and pbv.cust_entity_structure_fk is null 
			and pbv.zone_entity_structure_fk is not null
			and pbv.record_status_cr_fk = 1
			and pbv.product_structure_fk in (select product_structure_fk from #t_prod where unit_cost_new is null and product_structure_fk is not null)
			) a
		where drank = 1) pbv1 on tp.product_structure_fk = pbv1.product_structure_fk
	where tp.unit_cost_new is null

--	store actual unit cost for all items
	update tp
	set unit_cost_new_actual = unit_cost_new
	, item_sort = left(item, len(item) - 1)
	from #t_prod tp

--	align unit_cost_new to be the same max amount for all items in the same zone/alias
	update tp
	set unit_cost_new = tp0.ucn_new
	from #t_prod tp
	inner join
				(
				select alias_id, entity_structure_fk, max(unit_cost_new) 'ucn_new'
				from #t_prod
				where alias_id is not null
				group by alias_id, entity_structure_fk
				) tp0 on tp.alias_id = tp0.alias_id and tp.entity_structure_fk = tp0.entity_structure_fk

	update tp
	set price_new_initial = case when ind_cost_change_item = 0 and isnull(@marginchanges, 0) = 0 then price_cur 
		else round(cast((isnull(tp.[unit_cost_new], tp.[unit_cost_cur]) * price_multiple_new) / (1 - tp.urm_target_margin) as numeric(18, 6)), 2) end
	, item_group = isnull(tp.subgroup_id, 0) + 1000000
	from #t_prod tp
	where 1 = 1

--	1.	Initial Prices Set	Set pricing of all items according to price point and rounding requirements
	update tp
	set price_new = case	when use_lg_price_points = 'n' then price_new_initial
							when ind_cost_change_item = 0 and isnull(@marginchanges, 0) = 0 then price_cur 
							else [precision].[getretailprices]	(tp.entity_structure_fk, tp.product_structure_fk, tp.price_new_initial, @price_change_effective_date) end
	, pricing_steps = '1:' + 
		cast(case	when use_lg_price_points = 'n' then price_new_initial
							when ind_cost_change_item = 0 then price_cur 
							else [precision].[getretailprices]	(tp.entity_structure_fk, tp.product_structure_fk, tp.price_new_initial, @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tp
	where 1 = 1
	and tp.lg_active = 'y'
	and tp.lg_flag = 'y'

--	Store how initial level pricing was accomplished
	update tp
	set Digit_DN_FK = Case right(Cast(price_new_initial as Numeric(18, 2)), 1)
	when 0 then 144897
		when 1 then 144898
		when 2 then 144899
		when 3 then 144901
		when 4 then 144902
		when 5 then 144903
		when 6 then 144904
		when 7 then 144905
		when 8 then 144906
		when 9 then 144907 end
	from #t_prod tp

	update tp
	set Digit_Adj = aa.add_amt
	from #t_prod tp
	inner join
		(Select * from 
					(
						Select distinct cast(ss.attribute_number as Numeric(18, 2)) 'add_amt', tp0.digit_dn_fk, sp.product_structure_fk, ss.zone_segment_fk, precedence
						, dense_rank()over(partition by sp.product_structure_fk, ss.zone_segment_fk order by isnull(precedence, 99), effective_date desc) DRank
						from epacube.epacube.segments_settings ss
						inner join epacube.segments_product sp with (nolock) on ((ss.prod_segment_fk = sp.PROD_SEGMENT_FK and ss.PROD_SEGMENT_DATA_NAME_FK = sp.DATA_NAME_FK) or ss.prod_segment_fk is null)
						inner join #t_prod tp0 on ss.ZONE_SEGMENT_FK = tp0.entity_structure_fk and sp.PRODUCT_STRUCTURE_FK = tp0.product_structure_fk
						where 1 = 1
							and ss.data_name_fk = tp0.Digit_DN_FK 
							and ss.Effective_Date <= @lgprocdate
							and (ss.reinstate_inheritance_date > @lgprocdate or ss.reinstate_inheritance_date is null)
					) a where DRank = 1
		) aa on tp.product_structure_fk = aa.product_structure_fk and tp.entity_structure_fk = aa.zone_segment_fk and aa.digit_dn_fk = tp.digit_dn_fk

	update tp
	set price_rounded = rnd.price_rounded
	from #t_prod tp
	inner join
		(	Select * 
			from (
					Select ss.ATTRIBUTE_NUMBER 'price_rounded', sp.product_structure_fk, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK, SS.Effective_Date, SS.PRECEDENCE
					, ss.LOWER_LIMIT, ss.UPPER_LIMIT
					, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
					from epacube.epacube.segments_settings ss with (nolock) 
					inner join epacube.epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
					inner join #t_prod tp on sp.product_structure_fk = tp.product_structure_fk and ss.ZONE_SEGMENT_FK = tp.entity_structure_fk
					where ss.data_name_fk = 144891
					and ss.RECORD_STATUS_CR_FK = 1
					and tp.price_new_initial + isnull(tp.Digit_Adj, 0) between ss.LOWER_LIMIT and ss.UPPER_LIMIT
				) a where DRank = 1
		) rnd on tp.product_structure_fk = rnd.PRODUCT_STRUCTURE_FK and tp.entity_structure_fk = rnd.ZONE_SEGMENT_FK

	update tp1
	set related_item_alias_id = tp2.alias_id
	from #t_prod tp1
	inner join #t_prod tp2 on tp1.entity_structure_fk = tp2.entity_structure_fk
	and tp1.product_structure_fk = tp2.parity_parent_product_structure_fk
	and tp1.alias_id is null
	and tp2.alias_id is not null

----price all parity relationships
--	10.	National Item has Cost Change	Set PL to discount from National
	update tpp
	set price_new = case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, related_item_alias_id = case when tpp.alias_id is null and tpc.alias_id is not null then tpc.alias_id else null end
	, pricing_steps = tpp.pricing_steps + '10:' +
		cast(case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.product_structure_fk = tpc.parity_parent_product_structure_fk
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1
	and tpc.ind_cost_change_item = 1
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'

--	15.	PL has cost and price increase or cost increase with current price < parity differential 	Set Ntl to parity differential of PL
	update tpc
	set price_new = case	when tpc.use_lg_price_points = 'n' then  round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, related_item_alias_id = case when tpc.alias_id is null and tpp.alias_id is not null then tpp.alias_id else null end
	, pricing_steps = tpc.pricing_steps + '15:'
		+ cast(case	when tpc.use_lg_price_points = 'n' then  round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.product_structure_fk = tpc.parity_parent_product_structure_fk
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and (tpp.price_new > tpp.price_cur
		OR
			(
			tpp.price_new = tpp.price_cur and
				case	when tpc.use_lg_price_points = 'n' then  round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
								else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end > tpc.price_new
			)
		)

	and isnull(tpp.change_status, 0) = 0
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'

--	20.	Above step causes National Price Decline	Set National New Price to Current
	update tpc
	set price_new = tpc.price_cur
	, pricing_steps = tpc.pricing_steps + '20:' + cast(tpc.price_cur as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.product_structure_fk = tpc.parity_parent_product_structure_fk
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and tpp.price_new > tpp.price_cur
	and tpc.price_new < tpc.price_cur
	and isnull(tpp.change_status, 0) = 0
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'

--	22. Reprice Private Label to parity differential of National Brand when steps 15 and 20 are invoked.
	update tpp
	set price_new = case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, related_item_alias_id = case when tpp.alias_id is null and tpc.alias_id is not null then tpc.alias_id else null end
	, pricing_steps = tpp.pricing_steps + '22:' +
		cast(case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.product_structure_fk = tpc.parity_parent_product_structure_fk
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1
	and tpc.pricing_steps like '%~15:%~20:%'
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'

--	25.	PL Only has cost decline or (increase with no price advance)	Set National New Price to Current
	update tpc
	set price_new = tpc.price_cur
	, pricing_steps = tpc.pricing_steps + '25:' + cast(tpc.price_cur as varchar(16)) + '~'
	, related_item_alias_id = case when tpc.alias_id is null and tpp.alias_id is not null then tpp.alias_id else null end
	from #t_prod tpp
	inner join #t_prod tpc on tpp.product_structure_fk = tpc.parity_parent_product_structure_fk
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and (isnull(tpp.change_status, 0) = -1 or tpp.price_new <= tpp.price_cur)
--	do not want to reduce national price increase when it is due (from step 15)
	and tpc.price_new < tpc.price_cur
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'

--	30.	PL Only has cost decline or (increase with no price advance)	Set PL to discount from National provice
----	Set all the 30 segments to only make the PL Price Change if it is an increase
	update tpp
	set price_new = case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, pricing_steps = tpp.pricing_steps + '30:'
		+ Cast(case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	, related_item_alias_id = case when tpp.alias_id is null and tpc.alias_id is not null then tpc.alias_id else null end
	from #t_prod tpp
	inner join #t_prod tpc on tpp.product_structure_fk = tpc.parity_parent_product_structure_fk
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and (isnull(tpp.change_status, 0) = -1 or 
											(	tpp.price_new <= tpp.price_cur
												and case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
												else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
												> tpp.price_new
											
											)
		)
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'

	--35.	Items in Alias have different prices	Set all items in an alias to the max item price of any item in the alias with a cost change
	update tpc
	set price_new = tpp.price_new
	, pricing_steps = tpc.pricing_steps + '35:' + cast(tpp.price_new as varchar(16)) + '~'
	from #t_prod tpc
	inner join
		(
			Select * from (
							select tpp1.*, Dense_Rank()over(partition by tpp1.alias_id, tpp1.entity_structure_fk order by tpp1.price_new desc, tpp1.t_prod_id) DRank
							from  #t_prod tpp1
							where 1 = 1
								and tpp1.ind_cost_change_item = 1
						) a where drank = 1
		) tpp on tpc.alias_id = tpp.alias_id
				and tpc.entity_structure_fk = tpp.entity_structure_fk
	where 1 = 1
	and tpp.price_new > tpc.price_new
	and tpc.pricing_steps not like '%~30:%'

--	36. Private Label items not in parity with National Current Price and initial calculation of National Price is declining or remaining unchanged - Set new PL price to parity differential of National Current price where not already done by step 15
	Update tp
	set price_new = a.corrected_pl_price_new
	, pricing_steps = tp.pricing_steps + '36:' + cast(a.corrected_PL_price_new as varchar(16)) + '~'
	from #t_prod tp
	inner join (
			select p.t_prod_id, p.alias_id 'PL Alias', c.alias_id 'Natl Alias', p.item, p.zone_id, p.price_new
			, case when p.use_lg_price_points = 'n' then  round(cast(c.price_cur * (1 - p.parity_discount_pct) as numeric(18, 6)), 2)
			else [precision].[getretailprices]	(p.entity_structure_fk, p.product_structure_fk, round(cast(c.price_cur * (1 - p.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end 'corrected_PL_price_new'
			, c.pricing_steps 'National_Pricing_Steps'
			from #t_prod p
			inner join #t_prod c on p.alias_id = c.parity_alias_id and p.zone_id = c.zone_id
			where p.parity_status = 'P'
			and c.parity_status = 'c'
			and c.price_new <= c.price_cur
			and c.pricing_steps not like '%~15:%'
			and (	c.parity_parent_product_structure_fk is not null
					or c.parity_child_product_structure_fk is not null
				)
			) A on tp.t_prod_id = a.t_prod_id
	where a.price_new <> a.corrected_pl_price_new

--	37. Private Label items not in parity with National New Price and initial calculation of National Price is increasing - Set new PL price to parity differential of National New price where not already done by step 15
	Update tp
	set price_new = a.corrected_pl_price_new
	, pricing_steps = tp.pricing_steps + '37:' + cast(a.corrected_PL_price_new as varchar(16)) + '~'
	from #t_prod tp
	inner join (
			select p.t_prod_id, p.alias_id 'PL Alias', c.alias_id 'Natl Alias', p.item, p.zone_id, p.price_new
			, case when p.use_lg_price_points = 'n' then  round(cast(c.price_new * (1 - p.parity_discount_pct) as numeric(18, 6)), 2)
			else [precision].[getretailprices]	(p.entity_structure_fk, p.product_structure_fk, round(cast(c.price_new * (1 - p.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end 'corrected_PL_price_new'
			, c.pricing_steps 'National_Pricing_Steps'
			from #t_prod p
			inner join #t_prod c on p.alias_id = c.parity_alias_id and p.zone_id = c.zone_id
			where p.parity_status = 'P'
			and c.parity_status = 'c'
			and c.price_new > c.price_cur
			and c.pricing_steps not like '%~15:%'
			and (	c.parity_parent_product_structure_fk is not null
					or c.parity_child_product_structure_fk is not null
				)
			) A on tp.t_prod_id = a.t_prod_id
	where a.price_new <> a.corrected_pl_price_new

--	38. Set all items within the alias to the same price_new value, by zone, when not a PL item already adjusted to parity differential
	Update tp
	set price_new = tp1.price_new
	, pricing_steps = tp.pricing_steps + '38:' + cast(tp1.price_new as varchar(16)) + '~'
	from #t_prod tp
	inner join
		(select * from (select alias_id, zone_id, price_new, dense_rank()over(partition by alias_id, zone_id order by price_new desc, t_prod_id) DRank from #t_prod where alias_id is not null) a where DRank = 1) tp1
		on tp.alias_id = tp1.alias_id and tp.zone_id = tp1.zone_id
	where tp.price_new <> tp1.price_new
	and tp.pricing_steps not like '%~30:%'

--	40.	Create Alias Header Records	Using Max TM from any item in Alias - 656
	insert into #t_prod
	(
	item_change_type_cr_fk
	, price_mgmt_type_cr_fk, item, alias_description, product_structure_fk, entity_structure_fk, cost_change_effective_date, case_cost_new, unit_cost_new, case_cost_cur, unit_cost_cur
	, price_multiple, price_effective_date_cur, price_cur, price_new, urm_target_margin, pricesheet_basis_values_fk, pbv_status, change_status, cost_change_processed_date, ind_cost_change_item, parity_status, ind_parity_status
	, ind_parity, parity_parent_product_structure_fk, parity_child_product_structure_fk, parity_parent_item, parity_child_item, parity_discount_amt, parity_mult, parity_discount_pct, alias_id, parity_alias_id, ind_initial_load, load_step
	, lg_active, lg_flag, use_lg_price_points, minamt, zone_id, zone_name, zone_sort, pricing_steps, subgroup_id, subgroup_description, subgroup_data_value_fk, item_group, item_sort, price_new_initial
	, Digit_DN_FK, Digit_Adj, price_rounded, pack, size_uom, unique_cost_record_id
	)
	select
	656 'item_change_type_cr_fk'
	, 650 'price_mgmt_type_cr_fk', item, alias_description, product_structure_fk, entity_structure_fk, cost_change_effective_date
	, case_cost_new, unit_cost_new, case_cost_cur, unit_cost_cur
	, price_multiple, price_effective_date_cur, price_cur
	,  case when abs(price_new1 - price_cur) >= isnull(minamt, 0) then price_new1 else price_cur end 'price_new'
	, tm_max 'urm_target_margin', pricesheet_basis_values_fk, pbv_status, change_status, cost_change_processed_date, ind_cost_change_item
	, parity_status, ind_parity_status
	, ind_parity, parity_parent_product_structure_fk, parity_child_product_structure_fk, parity_parent_item, parity_child_item, parity_discount_amt, parity_mult, parity_discount_pct, alias_id, parity_alias_id, ind_initial_load, load_step
	, lg_active, lg_flag, use_lg_price_points, minamt, zone_id, zone_name, zone_sort, pricing_steps
	, subgroup_id, subgroup_description, subgroup_data_value_fk, item_group, item_sort
	, round(cast(isnull([unit_cost_new], [unit_cost_cur]) / (1 - tm_max) as numeric(18, 6)), 2) 'price_new_initial'
	, Digit_DN_FK, Digit_Adj, price_rounded, pack, size_uom, unique_cost_record_id
	from (
		select * 
--		, case when ind_parity_status <> '00' then price_new
		
		, case when cast((select max(isnull(ind_parity_status, 0)) from #t_prod where isnull(alias_id, 0) = a.alias_id and entity_structure_fk = a.entity_structure_fk) as int) <> 0 then price_new
		
				when use_lg_price_points = 'n' then  round(cast([unit_cost_new] / (1 - tm_max) as numeric(18, 6)), 2)
								else [precision].[getretailprices]	(entity_structure_fk, product_structure_fk, round(cast([unit_cost_new] / (1 - tm_max) as numeric(18, 6)), 2), @price_change_effective_date) end 'price_new1'
		from (
		select 
		item_change_type_cr_fk, qualified_status_cr_fk, price_mgmt_type_cr_fk, item, alias_description, product_structure_fk, entity_structure_fk, cost_change_effective_date
		, isnull(case_cost_new, case_cost_cur) 'case_cost_new'
		, isnull(unit_cost_new, unit_cost_cur) 'unit_cost_new'
		, max(case_cost_cur) 'case_cost_cur'
		, max(unit_cost_cur) 'unit_cost_cur'
		, price_multiple, price_effective_date_cur
		, (select max(isnull(price_cur, 0)) from #t_prod tpa where alias_id = tp.alias_id and zone_id = tp.zone_id) 'price_cur'
		, price_new
		, max(urm_target_margin) 'tm_max'
		, pricesheet_basis_values_fk, pbv_status, change_status, cost_change_processed_date, ind_cost_change_item, parity_status, ind_parity_status
		, ind_parity, parity_parent_product_structure_fk, parity_child_product_structure_fk, parity_parent_item, parity_child_item, parity_discount_amt, parity_mult, parity_discount_pct, alias_id, parity_alias_id, ind_initial_load, load_step
		, lg_active, lg_flag, use_lg_price_points, minamt, zone_id, zone_name, zone_sort, pricing_steps + '40:' 'pricing_steps', subgroup_id, subgroup_description, subgroup_data_value_fk, item_group, alias_id 'item_sort'
		, Digit_DN_FK, Digit_Adj, price_rounded, pack, size_uom, unique_cost_record_id
		, dense_rank()over(partition by alias_id, entity_structure_fk 
			order by isnull(unit_cost_new_actual, unit_cost_cur) desc, ind_cost_change_item desc, case when ind_parity_status = 0 then 0 else 1 end desc, cast(left(item, len(item) - 1) as bigint), t_prod_id desc) 'drank'
		from #t_prod tp
		where alias_id is not null
		group by	item_change_type_cr_fk, qualified_status_cr_fk, price_mgmt_type_cr_fk, item, alias_description, product_structure_fk, entity_structure_fk, cost_change_effective_date
		, isnull(case_cost_new, case_cost_cur), isnull(unit_cost_new, unit_cost_cur), unit_cost_cur, unit_cost_new_actual, case_cost_new, price_multiple, price_effective_date_cur--, price_cur
		, price_new
		, pricesheet_basis_values_fk, pbv_status, change_status, cost_change_processed_date, ind_cost_change_item, parity_status, ind_parity_status
		, ind_parity, parity_parent_product_structure_fk, parity_child_product_structure_fk, parity_parent_item, parity_child_item, parity_discount_amt, parity_mult
		, parity_discount_pct, alias_id, parity_alias_id, ind_initial_load, load_step, lg_active, lg_flag, use_lg_price_points, minamt, zone_id, zone_name, zone_sort, pricing_steps
		, subgroup_id, subgroup_description, subgroup_data_value_fk, item_group, item_sort, Digit_DN_FK, Digit_Adj, price_rounded, pack, size_uom, unique_cost_record_id, t_prod_id
		) a where drank = 1
	) b

--set subgroup id to be consistent across all header alias records when alias spans more than 1 subgroup id
	update tp
	set subgroup_id = adj.subgroup_id
	, subgroup_description = adj.subgroup_description
	, subgroup_data_value_fk = adj.subgroup_data_value_fk
	from #t_prod tp
	inner join (
					Select * from (
					select alias_id, subgroup_id, subgroup_description, subgroup_data_value_fk
					, dense_rank()over(partition by alias_id order by subgroup_id) DRank
					from #t_prod tp
					where item_change_type_cr_fk = 656
					) A where DRank = 1
				) adj on tp.alias_id = adj.alias_id

	Update tp
	set parity_status = tp1.parity_status
	, ind_parity_status = tp1.ind_parity_status
	from #t_prod tp
	inner join
		(select alias_id, zone_id, parity_status, ind_parity_status from #t_prod where alias_id is not null and parity_status is not null group by alias_id, zone_id, parity_status, ind_parity_status) tp1
			on tp.alias_id = tp1.alias_id
			and tp.zone_id = tp1.zone_id

	Update tp
	set parity_discount_pct = parity_discount_pct1
	from #t_prod tp
	inner join
			(	select alias_id, parity_discount_pct1
				from (
						select isnull(tp1.alias_id, tp2.alias_id) 'alias_id'
						, case when tp1.alias_id is not null and tp1.parity_status = 'P' then tp1.parity_discount_pct else tp2.parity_discount_pct end 'parity_discount_pct1'
						from #t_prod tp1
							inner join #t_prod tp2 on tp1.product_structure_fk = isnull(tp2.parity_parent_product_structure_fk, tp2.parity_child_product_structure_fk)
													and tp1.entity_structure_fk = tp2.entity_structure_fk
													and (tp1.related_item_alias_id is not null or tp2.related_item_alias_id is not null)
					) a where parity_discount_pct1 is not null
					group by alias_id, parity_discount_pct1
			) rltd on tp.alias_id = rltd.alias_id
	where isnull(tp.item_change_type_cr_fk, 0) = 656
	and tp.parity_discount_pct is null

	Update tp
	set pricing_steps = pricing_steps + cast(price_new as varchar(16)) + '~'
	from #t_prod tp
	where item_change_type_cr_fk = 656

	Select distinct item_group 'item_group_root', item, item_sort, alias_id, parity_alias_id, ind_parity_status, parity_status
	, cast(case	when parity_status = 'P' then isnull(isnull(related_item_alias_id, alias_id), item_sort)
			when parity_status = 'C' then isnull(isnull(isnull(isnull(related_item_alias_id, parity_alias_id), case when alias_id is null and parity_alias_id is null and related_item_alias_id is null then left(parity_parent_item, Len(parity_parent_item) - 1) else alias_id end), alias_id), item_sort)
			when alias_id is not null then alias_id
			else item_sort end as float) 'alias_item'
	, related_item_alias_id, t_prod_id 't_prod_fk', cast(null as bigint) 'item_group'
	into #item_groups
	from #t_prod 
	where 1 = 1

	update ig
	set alias_item = alias_item + 0.01
	from #item_groups ig
	where alias_id is null and parity_alias_id is null and item_sort in (select alias_id from #t_prod group by alias_id)

	Update IG
	Set alias_item = adj.alias_item
	from #item_groups IG
	inner join
		(select parity_alias_id, alias_item from #item_groups group by parity_alias_id, alias_item) adj
		on IG.related_item_alias_id = adj.parity_alias_id

	Update IG
	set alias_item = adj.alias_item
	from #item_groups ig
	inner join
		(select alias_id, parity_alias_id, alias_item from #item_groups where parity_status = 'P' and parity_alias_id in (select distinct alias_id from #item_groups where parity_alias_id is null and alias_id in (select parity_alias_id from #item_groups where parity_alias_id is not null and parity_status = 'P' group by parity_alias_id)))
		adj on ig.alias_id = adj.parity_alias_id

	Update IG
	Set item_group_root = adj.item_group_root * 1000
	from #item_groups IG
	inner join
		(select alias_item, min(item_group_root) item_group_root from #item_groups group by alias_item) adj on ig.alias_item = adj.alias_item

update IG
set alias_item = adj.alias_id
from #item_groups IG
inner join
	(select distinct tp1.item, tp2.alias_id, tp2.parity_alias_id, tp1.related_item_alias_id, tp1.parity_status, tp1.product_structure_fk
		from #t_prod tp1
		inner join #t_prod tp2 on tp1.parity_parent_product_structure_fk = tp2.product_structure_fk
		where 1 = 1
		and tp1.parity_status = 'C'
		and tp1.alias_id is null
		and tp1.parity_alias_id is null
		and (tp2.alias_id is not null or tp2.parity_alias_id is not null)
		and tp2.parity_status = 'P'
	) adj on ig.item = adj.item

	Update IG
	Set item_group = IG.item_group_root + isnull(adj.DRank, 0)
	from #item_groups IG
	inner join
				(select t_prod_fk, dense_rank()over(partition by item_group_root order by alias_item, ind_parity_status, case when related_item_alias_id is null then 1 else 0 end, case when alias_id is null then 0 else 1 end) 'DRank'
				from #item_groups) adj on ig.t_prod_fk = adj.t_prod_fk

	update tp
	set item_group = ig.item_group
	, item_group_root = ig.item_group_root
	, ig_alias_item = ig.alias_item
	, igai = cast(cast(ig.item_group_root as varchar(24)) + cast(ig.alias_item as varchar(16)) as bigint)
	from #t_prod tp
	inner join #item_groups ig on tp.t_prod_id = ig.t_prod_fk

----	National Item not in alias but in parity to PL Item in Alias
--	110.	National Item in parity to item in alias, but not in an alias itself, has Cost Change	Set PL to discount from National
	Update tpp
	set price_new = case when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
					else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, pricing_steps = tpp.pricing_steps + '110:'
		+ cast(case when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
					else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.entity_structure_fk = tpc.entity_structure_fk
						and tpp.alias_id = tpc.related_item_alias_id
						and tpp.item_change_type_cr_fk = 656
	where 1 = 1
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'
	and tpc.ind_cost_change_item = 1
			
--	115.	PL Only has cost and price increase 	Set Ntl to parity differential of PL
	update tpc
	set price_new = case when tpc.use_lg_price_points = 'n' then  round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
					else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, pricing_steps = tpc.pricing_steps + '115:'
		+ Cast(case when tpc.use_lg_price_points = 'n' then  round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
					else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.entity_structure_fk = tpc.entity_structure_fk
						and tpp.alias_id = tpc.related_item_alias_id
						and tpp.item_change_type_cr_fk = 656
	where 1 = 1
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and isnull(tpc.change_status, 0) = 0
	and tpp.price_new > tpp.price_cur

--	120.	Above step causes National Price Decline	Set National New Price to Current
	update tpc
	set price_new = tpc.price_cur
	, pricing_steps = tpc.pricing_steps + '120:'
		+ Cast(tpc.price_cur as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.entity_structure_fk = tpc.entity_structure_fk
						and tpp.alias_id = tpc.related_item_alias_id
						and tpp.item_change_type_cr_fk = 656
	where 1 = 1
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and isnull(tpc.change_status, 0) = 0
	and tpp.price_new > tpp.price_cur
	and tpc.price_new < tpc.price_cur

--	125.	PL Only has cost decline or (increase with no price advance)	Set National New Price to Current
	Update tpc
	set price_new = tpc.price_cur
	, pricing_steps = tpc.pricing_steps + '125:' + cast(tpc.price_cur as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.entity_structure_fk = tpc.entity_structure_fk
						and tpp.alias_id = tpc.related_item_alias_id
						and tpp.item_change_type_cr_fk = 656
	where 1 = 1
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and (isnull(tpp.change_status, 0) = -1 or tpp.price_new <= tpp.price_cur)

--	130.	PL Only has cost decline or (increase with no price advance)	Set PL to discount from National
	Update tpp
	set price_new = case when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
					else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, pricing_steps = tpp.pricing_steps + '130:'
		+ Cast(case when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
					else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.entity_structure_fk = tpc.entity_structure_fk
						and tpp.alias_id = tpc.related_item_alias_id
						and tpp.item_change_type_cr_fk = 656
	where 1 = 1
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
--	and (isnull(tpp.change_status, 0) = -1 or tpp.price_new <= tpp.price_cur)
	and (isnull(tpp.change_status, 0) = -1 or 
											(	tpp.price_new <= tpp.price_cur
												and case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
												else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
												> tpp.price_new
											
											)
		)

----	PL item not in alias but in parity to National Item in Alias
--	210.	National Item has Cost Change	Set PL to discount from National
	Update tpp
	set price_new = case when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
					else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, pricing_steps = tpp.pricing_steps + '210:'
		+ Cast(case when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
					else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.entity_structure_fk = tpc.entity_structure_fk
						and tpc.alias_id = tpp.related_item_alias_id
						and tpc.item_change_type_cr_fk = 656
	where 1 = 1
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'
	and tpc.ind_cost_change_item = 1
			
--	215.	PL Only has cost and price increase 	Set Ntl to parity differential of PL
	update tpc
	set price_new = case when tpc.use_lg_price_points = 'n' then  round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
					else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, pricing_steps = tpc.pricing_steps + '215:'
		+ Cast(case when tpc.use_lg_price_points = 'n' then  round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
					else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.entity_structure_fk = tpc.entity_structure_fk
						and tpc.alias_id = tpp.related_item_alias_id
						and tpc.item_change_type_cr_fk = 656
	where 1 = 1
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and isnull(tpc.change_status, 0) = 0
	and tpp.price_new > tpp.price_cur

--	220.	Above step causes National Price Decline	Set National New Price to Current
	update tpc
	set price_new = tpc.price_cur
	, pricing_steps = tpc.pricing_steps + '220:'
		+ Cast(tpc.price_cur as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.entity_structure_fk = tpc.entity_structure_fk
						and tpc.alias_id = tpp.related_item_alias_id
						and tpc.item_change_type_cr_fk = 656
	where 1 = 1
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and isnull(tpc.change_status, 0) = 0
	and tpp.price_new > tpp.price_cur
	and tpc.price_new < tpc.price_cur

--	225.	PL Only has cost decline or (increase with no price advance)	Set National New Price to Current
	Update tpc
	set price_new = tpc.price_cur
	, pricing_steps = tpc.pricing_steps + '225:' + cast(tpc.price_cur as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.entity_structure_fk = tpc.entity_structure_fk
						and tpc.alias_id = tpp.related_item_alias_id
						and tpc.item_change_type_cr_fk = 656
	where 1 = 1
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and (isnull(tpp.change_status, 0) = -1 or tpp.price_new <= tpp.price_cur)
	and tpc.price_new < tpc.price_cur

--	230.	PL Only has cost decline or (increase with no price advance)	Set PL to discount from National
	Update tpp
	set price_new = case when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
					else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, pricing_steps = tpp.pricing_steps + '230:'
		+ Cast(case when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
					else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.entity_structure_fk = tpc.entity_structure_fk
						and tpc.alias_id = tpp.related_item_alias_id
						and tpc.item_change_type_cr_fk = 656
	where 1 = 1
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	--and (isnull(tpp.change_status, 0) = -1 or tpp.price_new <= tpp.price_cur)
	and (isnull(tpp.change_status, 0) = -1 or 
											(	tpp.price_new <= tpp.price_cur
												and case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
												else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
												> tpp.price_new
											)
		)

--	310.	National Item has cost change	Set PL to discount from National
	update tpp
	set price_new = case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, pricing_steps = tpp.pricing_steps + '310:'
				+ cast(case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.alias_id = tpc.parity_alias_id
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1
	and tpp.item_change_type_cr_fk = 656
	and tpc.item_change_type_cr_fk = 656
	and tpc.ind_cost_change_item = 1
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'

--	312.	No Item in either alias has a cost change	Set PL to discount from National
	update tpp
	set price_new = case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, pricing_steps = tpp.pricing_steps + '312:'
				+ cast(case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.alias_id = tpc.parity_alias_id
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1
	and tpp.item_change_type_cr_fk = 656
	and tpc.item_change_type_cr_fk = 656
	and isnull(tpp.unit_cost_cur, tpp.unit_cost_new) = tpp.unit_cost_new
	and isnull(tpc.unit_cost_cur, tpc.unit_cost_new) = tpc.unit_cost_new
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'

--	315.	PL has cost and price increase	Set Ntl to parity differential of PL where PL has not already been adjusted to parity differential of National item in steps 36 or 37
	update tpc
	set price_new = case	when tpc.use_lg_price_points = 'n' then  round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, pricing_steps = tpc.pricing_steps + '315:'
					+ cast(case	when tpc.use_lg_price_points = 'n' then  round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpp.price_new / (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.alias_id = tpc.parity_alias_id
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1
	and tpp.item_change_type_cr_fk = 656
	and tpc.item_change_type_cr_fk = 656
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and tpp.price_new > tpp.price_cur
	and isnull(tpp.change_status, 0) = 0
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'
	and tpp.pricing_steps not like '%~22:%'
	and tpp.pricing_steps not like '%~30:%'
	and tpp.pricing_steps not like '%~35:%'
	and tpp.pricing_steps not like '%~36:%'
	and tpp.pricing_steps not like '%~37:%'

--	320.	Above step causes National Price Decline	Set National New Price to Current
	update tpc
	set price_new = tpc.price_cur
	, pricing_steps = tpc.pricing_steps + '320:'
					+ cast(tpc.price_cur as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.alias_id = tpc.parity_alias_id
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1
	and tpp.item_change_type_cr_fk = 656
	and tpc.item_change_type_cr_fk = 656
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and tpp.price_new > tpp.price_cur
	and tpc.price_new < tpc.price_cur
	and tpc.parity_status = 'C'
	and isnull(tpp.change_status, 0) = 0
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'

--	325.	PL Only has cost decline or (increase with no price advance)	Set National New Price to Current
	update tpc
	set price_new = tpc.price_cur
	, pricing_steps = tpc.pricing_steps + '325:'
				+ cast(tpc.price_cur as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.alias_id = tpc.parity_alias_id
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1
	and tpp.item_change_type_cr_fk = 656
	and tpc.item_change_type_cr_fk = 656
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and (isnull(tpp.change_status, 0) = -1 or tpp.price_new <= tpp.price_cur)
	and tpc.price_new < tpc.price_cur
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'

--	330.	PL Only has cost decline or (increase with no price advance)	Set PL to discount from National
	update tpp
	set price_new = case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
	, pricing_steps = tpp.pricing_steps + '330:'
				+ cast(case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
							else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_new * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end as varchar(16)) + '~'
	from #t_prod tpp
	inner join #t_prod tpc on tpp.alias_id = tpc.parity_alias_id
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1
	and tpp.item_change_type_cr_fk = 656
	and tpc.item_change_type_cr_fk = 656
	and tpp.ind_cost_change_item = 1
	and tpc.ind_cost_change_item = 0
	and (isnull(tpp.change_status, 0) = -1 or 
											(	tpp.price_new <= tpp.price_cur
												and case	when tpp.use_lg_price_points = 'n' then  round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2)
												else [precision].[getretailprices]	(tpp.entity_structure_fk, tpp.product_structure_fk, round(cast(tpc.price_cur * (1 - tpp.parity_discount_pct) as numeric(18, 6)), 2), @price_change_effective_date) end
												> tpp.price_new
											
											)
		)
	and tpp.parity_status = 'P'
	and tpc.parity_status = 'C'

--	Alias items not in header to be updated to appropriate header values, denoting the pricing steps involved
	update tp657
	set item_change_type_cr_fk = 657
	, price_mgmt_type_cr_fk = tp656.price_mgmt_type_cr_fk
	, price_new = tp656.price_new
	, pricing_steps = tp656.pricing_steps + '_657:' + cast(tp656.price_new as varchar(16)) + '~'
	from #t_prod tp656
	inner join #t_prod tp657 on tp656.alias_id = tp657.alias_id
							and tp656.entity_structure_fk = tp657.entity_structure_fk
	where 1 = 1
	and tp656.item_change_type_cr_fk = 656 
	and tp657.item_change_type_cr_fk  is null

	---update status of items without alias in parity to items with alias
	update tpc
	set price_mgmt_type_cr_fk = isnull(tpp.price_mgmt_type_cr_fk, tpc.price_mgmt_type_cr_fk)
	, item_change_type_cr_fk = 657
			from #t_prod tpp
			inner join #t_prod tpc on tpp.entity_structure_fk = tpc.entity_structure_fk
								and tpp.alias_id = tpc.related_item_alias_id
								and tpp.item_change_type_cr_fk = 656

--	set appropiately as qualified vs non-qualified changes
	update tpp
	set item_change_type_cr_fk =	case	when tpp.item_change_type_cr_fk is not null then tpp.item_change_type_cr_fk else 655 end
	, price_mgmt_type_cr_fk =	case	when tpp.lg_active = 'n' and tpp.lg_flag = 'n' and tpc.lg_active = 'n' and tpc.lg_flag = 'n' then 651 else 650 end
	from #t_prod tpp
	inner join #t_prod tpc on tpp.product_structure_fk = tpc.parity_parent_product_structure_fk
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1

	update tpc
	set item_change_type_cr_fk =	case	when tpc.item_change_type_cr_fk <> 656 and tpp.item_change_type_cr_fk <> 656 then 655 else tpc.item_change_type_cr_fk end
	, price_mgmt_type_cr_fk =		case	when tpp.lg_active = 'n' and tpp.lg_flag = 'n' and tpc.lg_active = 'n' and tpc.lg_flag = 'n' then 651 else 650 end
	from #t_prod tpp
	inner join #t_prod tpc on tpp.product_structure_fk = tpc.parity_parent_product_structure_fk
							and tpp.entity_structure_fk = tpc.entity_structure_fk
	where 1 = 1

--	set all status indicators on items not in alias, not in parity
	update tp
	set item_change_type_cr_fk =	655
	, price_mgmt_type_cr_fk =	case	when tp.lg_active = 'n' and tp.lg_flag = 'n' then 651 else 650 end
	from #t_prod tp
	where item_change_type_cr_fk is null

	update tp
	set item_change_type_cr_fk = 657
	from #t_prod tp
	where 1 = 1
	and alias_id is not null
	and ITEM_CHANGE_TYPE_CR_FK = 655

	update tp
	set ftn_breakdown_parent = 'brkdwn'
	from #t_prod tp
	inner join epacube.product_bom pb on tp.product_structure_fk = pb.product_structure_fk and pb.data_name_fk = 500213
	where 1 = 1

	update tp
	set ftn_breakdown_parent = 'brkdwn'
	from #t_prod tp
	inner join
	(
		select tp1.t_prod_id
		from #t_prod tp1 
		inner join #t_prod tp2 on tp1.item_change_type_cr_fk in (656, 662) and tp2.item_change_type_cr_fk in (657)
			and tp1.alias_id = tp2.alias_id and tp1.entity_structure_fk = tp2.entity_structure_fk
		inner join epacube.product_bom pb on tp2.product_structure_fk = pb.product_structure_fk and pb.data_name_fk = 500213
		where 1 = 1
		and tp2.ftn_breakdown_parent is not null
		group by 
		tp1.t_prod_id
	) cds on tp.t_prod_id = cds.t_prod_id

	update tp
	set ftn_cost_delta = 'mcv'
	from #t_prod tp
	where alias_id in
		(	select alias_id
			from #t_prod
			where alias_id is not null
			and cast(unit_cost_new as numeric(18, 2)) <> cast(isnull(unit_cost_new_actual, unit_cost_new) as numeric(18, 2))
			group by alias_id
		)

	Update tp
	set unit_cost_new = cast(isnull(unit_cost_new_actual, unit_cost_new) as numeric(18, 4))
	from #t_prod tp

	update tp1
	set ind_parity = 1
	, parity_child_item = tp2.item
	from #t_prod tp1
	inner join #t_prod tp2 on tp1.entity_structure_fk = tp2.entity_structure_fk
							and tp1.alias_id = tp2.related_item_alias_id
	where tp1.item_change_type_cr_fk = 656
			and tp2.item_change_type_cr_fk <> 656

	update tp1
	set ind_parity = 1
	, parity_child_item = tp2.item
	from #t_prod tp1
	inner join #t_prod tp2 on tp1.entity_structure_fk = tp2.entity_structure_fk
							and tp1.alias_id = tp2.related_item_alias_id
	where tp2.item_change_type_cr_fk = 656
			and tp1.item_change_type_cr_fk <> 656

	Update tp
	set ind_parity = 1
	from #t_prod tp
	where item_change_type_cr_fk = 656
	and parity_alias_id is not null
	and ind_parity <> 1

	update tp
	set ind_parity = 1
	, parity_child_item = case when tp.parity_child_item is null then adj.item else tp.parity_child_item end
	, parity_child_product_structure_fk = case when parity_child_product_structure_fk is null then adj.product_structure_fk else parity_child_product_structure_fk end
	from #t_prod tp
	inner join
		(select distinct tp1.item, tp2.alias_id, tp2.parity_alias_id, tp1.related_item_alias_id, tp1.parity_status, tp1.product_structure_fk, tp1.ind_parity
			from #t_prod tp1
			inner join #t_prod tp2 on tp1.parity_parent_product_structure_fk = tp2.product_structure_fk
			where 1 = 1
			and tp1.parity_status = 'C'
			and tp1.alias_id is null
			and tp1.parity_alias_id is null
			and (tp2.alias_id is not null or tp2.parity_alias_id is not null)
			and tp2.parity_status = 'P'
		) adj on tp.alias_id = adj.alias_id
	where tp.item_change_type_cr_fk in (656, 662)

	update tpp3
	set related_item_alias_id = tpp1.alias_id
	from #t_prod tpp1
	inner join #t_prod tpp2 on tpp1.alias_id = tpp2.alias_id
							and tpp1.entity_structure_fk = tpp2.entity_structure_fk
							and tpp1.item_change_type_cr_fk = 656
							and tpp2.item_change_type_cr_fk <> 656
	inner join #t_prod tpp3 on tpp2.product_structure_fk = tpp3.parity_parent_product_structure_fk
							and tpp2.entity_structure_fk = tpp3.entity_structure_fk
							and tpp3.alias_id is null and tpp3.related_item_alias_id is null

------------------------Qualified Status Start

	update tp1
	Set qualified_status_cr_fk = case when abs(tp1.price_new - tp1.price_cur) > = isnull(tp1.minamt, 0) and price_new <> tp1.price_cur then 653 else 654 end
	from #t_prod tp1

	Update tp1
	Set qualified_status_cr_fk = case when abs(tp1.price_new - tp1.price_cur) > = isnull(tp1.minamt, 0) and price_new <> tp1.price_cur then 653 else 654 end
	from #t_prod tp1
	inner join (select item_group_root, entity_structure_fk from #t_prod where ind_parity_status = 0 group by item_group_root, entity_structure_fk) qs on tp1.item_group_root = qs.item_group_root
	where ind_parity_status = 0
	and item_change_type_cr_fk in (656, 662)

	Update tp2
	Set qualified_status_cr_fk = tp1.qualified_status_cr_fk
	from #t_prod tp1
	inner join (select item_group_root, entity_structure_fk from #t_prod where ind_parity_status = 0 group by item_group_root, entity_structure_fk) qs on tp1.item_group_root = qs.item_group_root
	inner join #t_prod tp2 on tp1.alias_id = tp2.alias_id 
		and tp1.entity_structure_fk = tp2.entity_structure_fk 
		and tp1.item_change_type_cr_fk in (656, 662)
		and tp2.item_change_type_cr_fk not in (656, 662) 
	where tp1.ind_parity_status = 0
	and tp2.ind_parity_status = 0

	Update tp1
	Set qualified_status_cr_fk = 653
	from #t_prod tp1
	inner join (select item_group_root, ig_alias_item, entity_structure_fk from #t_prod where qualified_status_cr_fk = 653 and ind_parity_status <> 0 group by item_group_root, ig_alias_item, entity_structure_fk) qs 
		on tp1.item_group_root = qs.item_group_root 
		and tp1.ig_alias_item = qs.ig_alias_item
		and tp1.entity_structure_fk = qs.entity_structure_fk
	where ind_parity_status <> 0

	update tp1
	set qualified_status_cr_fk = 653
	from #t_prod tp1
	inner join #t_prod tp2 on tp1.product_structure_fk = tp2.parity_parent_product_structure_fk and tp1.entity_structure_fk = tp2.entity_structure_fk
	where 1 = 1
	and (tp1.qualified_status_cr_fk = 653 or tp2.qualified_status_cr_fk = 653)
	and tp1.qualified_status_cr_fk <> tp2.qualified_status_cr_fk
	and tp1.related_item_alias_id is null
	and tp2.related_item_alias_id is null
	and tp1.alias_id is null
	and tp2.alias_id is null

	update tp2
	set qualified_status_cr_fk = 653
	from #t_prod tp1
	inner join #t_prod tp2 on tp1.product_structure_fk = tp2.parity_parent_product_structure_fk and tp1.entity_structure_fk = tp2.entity_structure_fk
	where 1 = 1
	and (tp1.qualified_status_cr_fk = 653 or tp2.qualified_status_cr_fk = 653)
	and tp1.qualified_status_cr_fk <> tp2.qualified_status_cr_fk
	and tp1.related_item_alias_id is null
	and tp2.related_item_alias_id is null
	and tp1.alias_id is null
	and tp2.alias_id is null

------------------------Qualified Status End

--	normalize sequencing of items when multiple aliases / related item alias id's are in parity with each other
	select 
	*, dense_rank()over(partition by std_alias_id order by item_group) 'IG_Rank'
	 , dense_rank()over(partition by std_alias_id order by ind_parity_status, isnull(alias_id, 0)) 'Parity_Rank'
	, cast(null as bigint) 'item_group_revised'
	into #multi
	from (
			Select distinct alias_id 'Std_Alias_ID', Alias_ID, null product_structure_fk, null 'item', item_group, ind_parity_status 
			from #t_prod 
			where 1 = 1
			and alias_id in (select parity_alias_id from #t_prod)
			and ind_parity_status = 1
			and item_change_type_cr_fk = 656
		Union
			Select distinct parity_alias_id 'Std_Alias_ID', alias_id 'Alias_ID', null product_structure_fk, null 'item', item_group, ind_parity_status 
			from #t_prod
			where 1 = 1
			and parity_alias_id in (select alias_id from #t_prod)
			and ind_parity_status = 2
			and item_change_type_cr_fk = 656
		Union
			Select distinct related_item_alias_id 'Std_Alias_ID', null 'alias_id', product_structure_fk, item, item_group, ind_parity_status 
			from #t_prod
			where 1 = 1
			and related_item_alias_id is not null
		union
			select tp2.alias_id 'Std_Alias_ID', tp1.alias_id, tp1.product_structure_fk, tp1.item, tp1.item_group, tp1.ind_parity_status 
			from #t_prod tp1
			inner join #t_prod tp2 on tp1.related_item_alias_id = tp2.alias_id
									and tp1.entity_structure_fk = tp2.entity_structure_fk
			where 1 = 1
			and tp2.item_change_type_cr_fk = 656						
		union
			select tp1.alias_id 'Std_Alias_ID', tp2.alias_id, tp2.product_structure_fk, tp2.item, tp2.item_group, tp2.ind_parity_status 
			from #t_prod tp1
			inner join #t_prod tp2 on tp2.related_item_alias_id = tp1.alias_id
									and tp1.entity_structure_fk = tp2.entity_structure_fk
			where 1 = 1
			and tp1.item_change_type_cr_fk = 656
		union
			select tp2.parity_alias_id 'Std_Alias_ID', tp1.alias_id, tp1.product_structure_fk, tp1.item, tp1.item_group, tp1.ind_parity_status 
			from #t_prod tp1
			inner join #t_prod tp2 on tp1.related_item_alias_id = tp2.alias_id
									and tp1.entity_structure_fk = tp2.entity_structure_fk
			where 1 = 1
			and tp2.item_change_type_cr_fk = 656						
		union
			select tp1.parity_alias_id 'Std_Alias_ID', tp2.alias_id, tp2.product_structure_fk, tp2.item, tp2.item_group, tp2.ind_parity_status 
			from #t_prod tp1
			inner join #t_prod tp2 on tp2.related_item_alias_id = tp1.alias_id
									and tp1.entity_structure_fk = tp2.entity_structure_fk
			where 1 = 1
			and tp1.item_change_type_cr_fk = 656
			) a
		order by Std_Alias_ID, ind_parity_status, isnull(alias_id, 0)

		delete from #Multi where std_alias_id in (select std_alias_id from #multi group by std_alias_id having count(*) < 3) or Std_Alias_ID is null

--	End of normalizing item groups due to multiple parity groupings

	update tp
	set parity_type = 'PL'
	from #t_prod tp
	where parity_status is not null

	Declare @HasMarginChanges int
	Set @HasMarginChanges = (select count(*) from marginmgr.PRICESHEET_RETAIL_CHANGES where isnull(IND_MARGIN_CHANGES, 0) = 1 and PRICE_CHANGE_EFFECTIVE_DATE = @PRICE_CHANGE_EFFECTIVE_DATE)

	If @MarginChanges = 1
	Begin
----	Set New Price and GM to Current for zones without margin changes when other zones had margin changes for the item(s) in question		
		update tp1
		set price_new = price_cur
		from #t_prod tp1
		left join
			(
				select tp.ig_alias_item, tp.entity_structure_fk, mc.mc_id 
				from #t_prod tp
				inner join #MC mc on tp.product_structure_fk = mc.product_structure_fk and tp.entity_structure_fk = mc.entity_structure_fk
			) a on tp1.ig_alias_item = a.ig_alias_item and tp1.entity_structure_fk = a.entity_structure_fk
		where a.mc_id is null

----	Set new and cur target margins to current target margin for zones without margin changes when other zones had margin changes for the item(s) in question (added 1/6/2020)
		Update P
		Set urm_target_margin = sz_502046.attribute_number
		, tm_pct_cur = sz_502046.attribute_number
		from #t_prod P
		inner join 
			(select * from (
							Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
							, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
							from epacube.segments_settings ss with (nolock) 
							inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
							inner join #t_prod p on sp.product_structure_fk = p.product_structure_fk and ss.zone_segment_fk = p.entity_structure_fk
							where ss.data_name_fk = 502046
							and ss.Effective_Date <= @LGprocDate
							and (ss.reinstate_inheritance_date > @LGprocDate or ss.reinstate_inheritance_date is null)
							and ss.RECORD_STATUS_CR_FK = 1
							 and cast(SS.CREATE_TIMESTAMP as date) <= @LGprocDate
							) a where Drank = 1
			) sz_502046 on p.Product_structure_FK = sz_502046.product_structure_fk and p.entity_structure_fk = sz_502046.zone_segment_fk
		left join
			(
				select tp.ig_alias_item, tp.entity_structure_fk, mc.mc_id 
				from #t_prod tp
				inner join #MC mc on tp.product_structure_fk = mc.product_structure_fk and tp.entity_structure_fk = mc.entity_structure_fk
			) a on p.ig_alias_item = a.ig_alias_item and p.entity_structure_fk = a.entity_structure_fk
		where a.mc_id is null
		and sz_502046.attribute_number is not null

		Update tp
		set ind_cost_change_item = case when cast(unit_cost_cur as numeric(4)) = cast(unit_cost_new as numeric(4)) and isnull(ind_requested_item, 0) = 0 then 0 else 1 end
		from #t_prod tp
		where @marginchanges = 1
	
		Update SFP
		Set Processed_timestamp = getdate()
		from [precision].stage_for_processing SFP
		inner join #t_prod tp
			on sfp.stage_for_processing_id = tp.stage_for_processing_fk	
		where sfp.price_change_effective_date = @PRICE_CHANGE_EFFECTIVE_DATE
		and @marginchanges = 1
	End

	update tpp3
	set related_item_alias_id = tpa1.alias_id
	from #t_prod tpa1
	inner join #t_prod tpa2 on tpa1.alias_id = tpa2.alias_id
							and tpa1.entity_structure_fk = tpa2.entity_structure_fk
							and tpa1.item_change_type_cr_fk = 656
							and tpa2.item_change_type_cr_fk <> 656
	inner join #t_prod tpp3 on tpa2.product_structure_fk = tpp3.parity_parent_product_structure_fk
							and tpa2.entity_structure_fk = tpp3.entity_structure_fk
							and tpp3.alias_id is null and tpp3.related_item_alias_id is null

	Update tp
	set price_mgmt_type_cr_fk = 651
	, qualified_status_cr_fk = 654
	, price_new = null
	, qualified_status_step = qualified_status_step + ', 130'
	from #t_prod tp
	where lg_flag = 'N'
	and @marginchanges = 0

	update tp
	set parity_alias_id = als.parity_alias_id
	, parity_status = als.parity_status
	, ind_parity_status = als.ind_parity_status
	, ind_parity = 1
	from #t_prod tp
	inner join (select alias_id, parity_alias_id, parity_status, ind_parity_status from #t_prod where parity_alias_id is not null group by alias_id, parity_alias_id, parity_status, ind_parity_status) als on tp.alias_id = als.alias_id
	where tp.parity_alias_id is null
	and tp.item_change_type_cr_fk = 656

--Set all items to Regular Display/Report
	update tp1
	set ind_display_as_reg = 1
	from #t_prod tp1

--	Move to Alias Display/Report
	update tp1
	set ind_display_as_reg = 0
	from #t_prod tp1
	where igai in 
		(select igai from #t_prod where isnull(ind_cost_change_item, 0) = 1 and alias_id is not null group by igai)

	If @HasMarginChanges > 0 or (@ReinstateRescind = 1 and @MarginChanges = 0)
	Begin

	select 'Step 1' 'Insert_Step'
	, tp.*, prc.PRICESHEET_RETAIL_CHANGES_ID
	into #MCadjs
		from #t_prod tp
		inner join marginmgr.PRICESHEET_RETAIL_CHANGES prc on ((tp.product_structure_fk = prc.PRODUCT_STRUCTURE_FK and tp.item_change_type_cr_fk = prc.ITEM_CHANGE_TYPE_CR_FK)
																or (tp.alias_id = prc.alias_id and tp.item_change_type_cr_fk = 656 and prc.ITEM_CHANGE_TYPE_CR_FK = 656))
															and tp.entity_structure_fk = prc.ZONE_ENTITY_STRUCTURE_FK
															and prc.PRICE_CHANGE_EFFECTIVE_DATE = @PRICE_CHANGE_EFFECTIVE_DATE
		where 1 = 1
		and 
		(
			isnull(prc.IND_MARGIN_CHANGES, 0) = 1	
			or
			(@ReinstateRescind = 1 and prc.unit_cost_new = (select max(unit_cost_new) from #t_prod where isnull(ind_rescind, 0) <> 1 and (item = tp.item or alias_id = tp.alias_id))
			--Below line excludes immediate cost changes already accounted for
			and prc.unit_cost_new <> isnull((select max(unit_cost_new) from #t_prod where isnull(ind_rescind, 0) = 2 and (item = tp.item or alias_id = tp.alias_id)), 0))
			or
			(@ReinstateRescind = 1 and tp.Alias_id in (select alias_id from #t_prod tp1 where (select count(*) from #t_prod where isnull(IND_COST_CHANGE_ITEM, 0) = 1 and alias_id = tp1.alias_id) = 0))
			or
			--Below line captures immediate cost changes not already accounted for.
			(@ReinstateRescind = 1 and prc.product_structure_fk in (select product_structure_fk from marginmgr.pricesheet_basis_values_icc))
		)

----	Get any missing updated alias header records
		Insert into #MCadjs
		select 'Step 2' 'Insert_Step', tp.*, prc.PRICESHEET_RETAIL_CHANGES_ID
		from #t_prod tp
		inner join marginmgr.PRICESHEET_RETAIL_CHANGES prc on ((tp.product_structure_fk = prc.PRODUCT_STRUCTURE_FK and tp.item_change_type_cr_fk = prc.ITEM_CHANGE_TYPE_CR_FK)
																or (tp.alias_id = prc.alias_id and tp.item_change_type_cr_fk = 656 and prc.ITEM_CHANGE_TYPE_CR_FK = 656))
															and tp.entity_structure_fk = prc.ZONE_ENTITY_STRUCTURE_FK
															and prc.PRICE_CHANGE_EFFECTIVE_DATE = @PRICE_CHANGE_EFFECTIVE_DATE
		where 1 = 1
		and @ReinstateRescind = 1
		and tp.item_change_type_cr_fk in (656, 662)
		and 
			(
				(tp.alias_id in (select alias_id from #MCadjs where isnull(alias_id, 0) not in (select alias_id from #MCadjs where item_change_type_cr_fk in (656, 662))))
				or
				(tp.alias_id in (select tp.alias_id from #t_prod tp where unit_cost_new > 
									(select max(unit_cost_new) from marginmgr.PRICESHEET_RETAIL_CHANGES 
										where price_change_effective_date = @price_change_effective_date 
											and alias_id = tp.alias_id 
											and isnull(ind_rescind, 0) = 0 
											and ITEM_CHANGE_TYPE_CR_FK = 656
									)
								)
				)
			)

		create index idx_01 on #MCadjs(PRICESHEET_RETAIL_CHANGES_ID)
		create index idx_02 on #MCadjs(t_prod_id)

		if @ReinstateRescind = 1
			Begin
				delete from #MCadjs
				where PRICESHEET_RETAIL_CHANGES_ID not in
				(	select mca.PRICESHEET_RETAIL_CHANGES_ID
					from (select product_structure_fk from marginmgr.pricesheet_basis_values_unique union select product_structure_fk from marginmgr.pricesheet_basis_values_icc) pbvu
					inner join #MCadjs mca on pbvu.product_structure_fk = mca.product_structure_fk
					left join epacube.product_mult_type pmt on mca.product_structure_fk = pmt.PRODUCT_STRUCTURE_FK and mca.entity_structure_fk = pmt.ENTITY_STRUCTURE_FK and pmt.DATA_NAME_FK = 500019
					where pmt.PRODUCT_MULT_TYPE_ID is null
					group by mca.PRICESHEET_RETAIL_CHANGES_ID
					Union
					select mca.PRICESHEET_RETAIL_CHANGES_ID
					from (select product_structure_fk from marginmgr.pricesheet_basis_values_unique union select product_structure_fk from marginmgr.pricesheet_basis_values_icc) pbvu
					inner join epacube.product_mult_type pmt1 on pbvu.product_structure_fk = pmt1.PRODUCT_STRUCTURE_FK and pmt1.data_name_fk = 500019
					inner join epacube.product_mult_type pmt on pmt1.DATA_VALUE_FK = pmt.DATA_VALUE_FK and pmt1.ENTITY_STRUCTURE_FK = pmt.ENTITY_STRUCTURE_FK and pmt1.DATA_NAME_FK = pmt.DATA_NAME_FK
					inner join #MCadjs mca on pmt.product_structure_fk = mca.product_structure_fk and pmt.ENTITY_STRUCTURE_FK = mca.entity_structure_fk
					group by mca.PRICESHEET_RETAIL_CHANGES_ID
				)
			End

		Update prc
		Set 
		QUALIFIED_STATUS_CR_FK = p.QUALIFIED_STATUS_CR_FK
		, case_cost_cur = isnull(P.Case_Cost_Cur, P.Case_Cost_New)
		, unit_cost_cur = isnull(P.Unit_Cost_Cur, P.Unit_Cost_new)
		, price_cur = p.price_cur
		, cur_gm_pct = Cast(Case when isnull(p.unit_Cost_Cur, p.Unit_Cost_new) = 0 then Null else (p.Price_Cur - (isnull(p.Unit_Cost_Cur, p.Unit_Cost_new) * isnull(p.price_multiple, 1))) / (p.Price_Cur) end as Numeric(18, 4))
		, price_effective_date_cur = p.Price_Effective_Date_Cur
		, case_cost_new = p.Case_Cost_New
		, unit_cost_new = isnull(P.Unit_Cost_New, P.Unit_Cost_Cur)
		, COST_CHANGE = Case when isnull(P.unit_cost_cur, 0) = isnull(P.Unit_cost_new, 0) or isnull(P.unit_cost_cur, 0) = 0 or isnull(P.Unit_cost_new, 0) = 0 then Null else P.unit_cost_new - P.unit_cost_Cur end
		, price_change = Case when isnull(P.price_cur, 0) = isnull(P.price_new, 0) or isnull(P.price_cur, 0) = 0 or isnull(P.price_new, 0) = 0 then Null 
			else cast((p.price_new / isnull(p.PRICE_MULTIPLE_NEW, 1)) as numeric(18, 4)) - cast((p.price_cur / isnull(p.price_multiple, 1)) as numeric(18, 4)) end
			--else P.price_new - P.price_cur end
		, price_new = p.Price_New
		, NEW_GM_PCT = Cast(Case when isnull(p.unit_Cost_Cur, 0) = 0 then Null else (p.Price_new - (isnull(p.Unit_Cost_new, p.Unit_Cost_Cur)) * isnull(p.price_multiple_new, p.price_multiple)) / (p.Price_New) end as Numeric(18, 4))
		, ind_Margin_Tol = Case when Cast(Case when isnull(p.unit_Cost_Cur, 0) = 0 then Null else (p.Price_new - isnull(p.Unit_Cost_new, p.Unit_Cost_Cur)) / (p.Price_New) end as Numeric(18, 4)) - Cast(Case when isnull(p.unit_Cost_Cur, 0) = 0 then Null else (p.Price_Cur - p.Unit_Cost_Cur) / (p.Price_Cur) end as Numeric(18, 4))
			> mt.margin_tol then 1 
			when Cast(Case when isnull(p.unit_Cost_Cur, 0) = 0 then Null else (p.Price_new - isnull(p.Unit_Cost_new, p.Unit_Cost_Cur)) / (p.Price_New) end as Numeric(18, 4)) - Cast(Case when isnull(p.unit_Cost_Cur, 0) = 0 then Null else (p.Price_Cur - p.Unit_Cost_Cur) / (p.Price_Cur) end as Numeric(18, 4))
			< mt.margin_tol * -1 then -1 else Null end
		, COST_CHANGE_EFFECTIVE_DATE = p.Cost_Change_Effective_Date
		, price_initial = p.price_new_initial
		, Digit_DN_FK = P.Digit_DN_FK
		, price_multiple_new = p.price_multiple
		, srp_adjusted = Null
		, GM_ADJUSTED = null
		, TM_ADJUSTED = null
		, HOLD_CUR_PRICE = Null
		, reviewed = null
		, Update_Timestamp = getdate()
		, ind_cost_change_item = p.ind_Cost_Change_Item
		, ind_parity = p.ind_Parity
		, ind_display_as_reg = p.ind_display_as_reg
		, ftn_cost_delta = p.ftn_cost_delta
		--, ind_rescind = case when isnull(prc.IND_RESCIND, 0) = 1 then 2 else prc.IND_RESCIND end
		, cost_change_reason = p.pbv_status
		, update_user_price = Null
		, UPDATE_USER_SET_TM = Null
		, UPDATE_USER_HOLD = null
		, UPDATE_USER_REVIEWED = Null
		, PRICESHEET_BASIS_VALUES_FK = isnull(p.Pricesheet_basis_Values_fk, p.Pricesheet_Basis_Values_FK_Cur)
		, pricing_steps = p.Pricing_Steps
		, comments_rescind = prc.comments_rescind + '; ' + 'New cost in place'
		, unique_cost_record_id = p.unique_cost_record_id
		from marginmgr.pricesheet_retail_changes prc
		inner join #MCadjs p on prc.PRICESHEET_RETAIL_CHANGES_ID = p.PRICESHEET_RETAIL_CHANGES_ID
		cross Join (Select cast(ep.value as numeric(18, 2)) 'margin_tol' from epacube.epacube.epacube_params ep Where ep.application_scope_fk = 5013 and ep.[name] = 'MARGIN CHANGE TOLERENCE') mt

--		Reset to original values where cost change rescinded
		Update PRC
		Set ind_rescind = 1
		, Hold_Cur_Price = 1
		, Reviewed = 1
		, SRP_ADJUSTED = prc.price_cur
		, GM_Adjusted = prc.cur_gm_pct
		, TM_Adjusted = Null
		, Comments_Rescind = isnull(prc.Comments_Rescind, '') + 'Step6_p, '
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC
		where PRICE_CHANGE_EFFECTIVE_DATE = @price_change_effective_date
		and ITEM_CHANGE_TYPE_CR_FK in (656, 662)
		and unit_cost_cur = unit_cost_new
		and alias_id in 
			(select alias_id 
			from marginmgr.PRICESHEET_RETAIL_CHANGES PRC
			where PRICE_CHANGE_EFFECTIVE_DATE = @price_change_effective_date 
			and isnull(IND_RESCIND, 0) = 1)

--		Remove green highlighting from alias (items only) that had no cost change; impacted only due to its parity relationship
		Update PRC
		Set ind_rescind = Null
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC
		where 1 = 1
		and price_change_effective_date = @price_change_effective_date
			and ITEM_CHANGE_TYPE_CR_FK not in (656, 662)
			and alias_id is not null
			and ind_rescind = 2
			and @ReinstateRescind = 1
			and alias_id not in (
									Select alias_id
									from marginmgr.PRICESHEET_RETAIL_CHANGES
									where price_change_effective_date = @price_change_effective_date
									and alias_id is not null
									and (isnull(ind_rescind, 0) = 0 or isnull(IND_COST_CHANGE_ITEM, 0) = 1)
									and ITEM_CHANGE_TYPE_CR_FK not in (656, 662)
								)

--		Remove green background where the rescind had no impact on the pricing.
		Update PRC1
		set IND_RESCIND = Null
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC1
		inner join
				(
					select tp.alias_id
					from #t_prod tp
					inner join marginmgr.PRICESHEET_RETAIL_CHANGES prc on tp.product_structure_fk = prc.PRODUCT_STRUCTURE_FK 
																		and tp.item_change_type_cr_fk = prc.ITEM_CHANGE_TYPE_CR_FK
																		and tp.entity_structure_fk = prc.ZONE_ENTITY_STRUCTURE_FK
																		and prc.PRICE_CHANGE_EFFECTIVE_DATE = @PRICE_CHANGE_EFFECTIVE_DATE
																		and prc.ind_rescind = 2
																		and prc.unit_cost_cur = prc.unit_cost_new
			--															and prc.IND_COST_CHANGE_ITEM = 0
					where 1 = 1
					and prc.unit_cost_new = tp.unit_cost_new
					and tp.alias_id in (select alias_id from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICE_CHANGE_EFFECTIVE_DATE = @PRICE_CHANGE_EFFECTIVE_DATE and isnull(IND_RESCIND, 0) = 1 group by alias_id)
					group by tp.alias_id
				) PRC2 on prc1.alias_id = prc2.alias_id
		where prc1.PRICE_CHANGE_EFFECTIVE_DATE = @price_change_effective_date
		and isnull(prc1.IND_RESCIND, 0) <> 1


		delete tp
		from #t_prod tp
		inner join #MCadjs mc on tp.t_prod_id = mc.t_prod_id

		If @ReinstateRescind = 1
		goto eof

	End

----Load alias headers for non-level-gross items when both types exist in the same alias
	insert into #t_prod (
	[item_change_type_cr_fk], [qualified_status_cr_fk]
	, [price_mgmt_type_cr_fk]
	, [pack], [size_uom]
	--, [parent_product_structure_fk]
	--, [item], [item_description], [product_structure_fk]
	, [entity_structure_fk]--, [cost_change_effective_date_cur]
	, [cost_change_effective_date], [case_cost_new], [unit_cost_new], [case_cost_cur], [unit_cost_cur], [price_multiple], [price_multiple_new]
	--, [price_effective_date_cur]
	, [price_cur], [tm_pct_cur], [urm_target_margin], [pricesheet_basis_values_fk]
	--, [pricesheet_basis_values_fk_cur]
	, [pbv_status], [change_status], [cost_change_processed_date], [ind_cost_change_item], [ind_requested_item], [parity_type], [parity_status], [ind_parity_status], [ind_parity], [parity_parent_product_structure_fk], [parity_child_product_structure_fk], [parity_parent_item], [parity_child_item], [parity_discount_amt], [parity_mult], [parity_discount_pct], [alias_id], [alias_description], [parity_alias_id], [ind_initial_load], [load_step]
	, [lg_active], [lg_flag], [use_lg_price_points]
	, [minamt], [zone_id], [zone_name]
	--, [zone_sort], [pricing_steps]
	, [related_item_alias_id], [unit_cost_new_actual], [subgroup_id], [subgroup_description], [subgroup_data_value_fk], [item_sort], [item_group_root], [item_group], [ind_display_as_reg]
	--, [Digit_DN_FK], [Digit_Adj], [price_rounded]
	, [ftn_breakdown_parent], [ftn_cost_delta], [stage_for_processing_fk], [igai], [qualified_status_step], [ig_alias_item], [unique_cost_record_id]
	)
	Select distinct
	case @marginchanges when 0 then 656 else 662 end 'item_change_type_cr_fk', 654 'qualified_status_cr_fk'
	, 651 'price_mgmt_type_cr_fk'
	, [pack], [size_uom]
	--, [parent_product_structure_fk]
	--, [item], [item_description], [product_structure_fk]
	, [entity_structure_fk]--, [cost_change_effective_date_cur]
	, [cost_change_effective_date], [case_cost_new], [unit_cost_new], [case_cost_cur], [unit_cost_cur], [price_multiple], [price_multiple_new]
	--, [price_effective_date_cur]
	, [price_cur], [tm_pct_cur], [urm_target_margin], [pricesheet_basis_values_fk]
	--, [pricesheet_basis_values_fk_cur]
	, 'nlg mixed status' 'pbv_status', [change_status], [cost_change_processed_date], [ind_cost_change_item], [ind_requested_item], [parity_type], [parity_status], [ind_parity_status], [ind_parity], [parity_parent_product_structure_fk], [parity_child_product_structure_fk], [parity_parent_item], [parity_child_item], [parity_discount_amt], [parity_mult], [parity_discount_pct], [alias_id], [alias_description], [parity_alias_id], [ind_initial_load], [load_step]
	, null 'lg_active', 'N' 'lg_flag', 'N' 'use_lg_price_points'
	, [minamt], [zone_id], [zone_name]
	--, [zone_sort], [pricing_steps]
	, [related_item_alias_id], [unit_cost_new_actual], [subgroup_id], [subgroup_description], [subgroup_data_value_fk]
	, alias_id 'item_sort'
	, [item_group_root], [item_group], [ind_display_as_reg]
	--, [Digit_DN_FK], [Digit_Adj], [price_rounded]
	, [ftn_breakdown_parent], [ftn_cost_delta], [stage_for_processing_fk], [igai], [qualified_status_step], [ig_alias_item], [unique_cost_record_id]
	from #t_prod tp
	where alias_id is not null
	and lg_flag = 'N'
	and alias_id not in (select alias_id from #t_prod where item_change_type_cr_fk in (656, 662) and lg_flag = 'N' and entity_structure_fk = tp.entity_structure_fk)
	and (select count(*) from #t_prod where item_change_type_cr_fk not in (656, 662) and lg_flag = 'N' and entity_structure_fk = tp.entity_structure_fk and alias_id = tp.alias_id) > 0

--Set New Price and recalculated Margin of non-level gross alias to that of level gross alias when both exist in the same alias in the same zone

	Update tpN
	set Price_New = tpY.Price_New
	, gp_new =	Cast(Case when isnull(tpN.unit_Cost_New, 0) = 0 or isnull(tpY.Price_new, 0) = 0 then Null 
				else (tpY.Price_new - (isnull(tpN.Unit_Cost_new, tpN.Unit_Cost_Cur)) * isnull(tpY.price_multiple_new, tpY.price_multiple)) / (tpY.Price_New) end as Numeric(18, 4))
	from #t_prod tpN
	inner join #t_prod tpY on tpN.alias_id = tpY.alias_id and tpN.ENTITY_STRUCTURE_FK = tpY.ENTITY_STRUCTURE_FK
	where 1 = 1
	and tpN.price_mgmt_type_cr_fk = 651
	and tpY.price_mgmt_type_cr_fk = 650
	and tpY.ITEM_CHANGE_TYPE_CR_FK = 656

----Reset subgroup_id at the item level only to items in an alias	
	Update tp
	set subgroup_id = dv.value
	, SUBGROUP_DESCRIPTION = dv.DESCRIPTION
	, SUBGROUP_DATA_VALUE_FK = dv.DATA_VALUE_ID
	from #t_prod tp
	inner join epacube.segments_product sp on tp.PRODUCT_STRUCTURE_FK = sp.PRODUCT_STRUCTURE_FK and sp.DATA_NAME_FK = 501045
	inner join epacube.data_value dv on sp.PROD_SEGMENT_FK = dv.DATA_VALUE_ID
	where 1 = 1
	and tp.ITEM_CHANGE_TYPE_CR_FK not in (656, 662)
	and tp.subgroup_data_value_fk <> sp.PROD_SEGMENT_FK

----Undo any price changes where the Level Gross Activation Flag is set to 'N'
	Update tp
	Set price_new = price_cur
	, price_mgmt_type_cr_fk = 651
	, qualified_status_cr_fk = 654
	from #t_prod tp
	where zone_id in (select distinct zone_id from #t_prod where lg_active = 'N')

----insure ui selection (alias/regular) is correct for parity relationships between multiple aliases
	update tp
	set ind_display_as_reg = (select top 1 ind_display_as_reg from #t_prod where entity_structure_fk = tp.entity_structure_fk and product_structure_fk = isnull(tp.parity_parent_product_structure_fk, tp.parity_child_product_structure_fk) and alias_id is not null)
	from #t_prod tp
	where 1 = 1
	and tp.parity_alias_id is null
	and (select top 1 alias_id from #t_prod where entity_structure_fk = tp.entity_structure_fk and product_structure_fk = isnull(tp.parity_parent_product_structure_fk, tp.parity_child_product_structure_fk) and alias_id is not null) is not null
	and item_change_type_cr_fk in (656, 662)

--	Insure all alias children have the same setting.
	update tp2
	set ind_display_as_reg = tp1.ind_display_as_reg
	from #t_prod tp1
	inner join #t_prod tp2 on tp1.alias_id = tp2.alias_id and tp1.entity_structure_fk = tp2.entity_structure_fk and tp1.item_change_type_cr_fk in (656, 662) and tp2.item_change_type_cr_fk not in (656, 662)
	where tp1.ind_display_as_reg <> tp2.ind_display_as_reg

----Clear then load Level Gross Table - marginmgr.pricesheet_retail_changes
	delete from marginmgr.pricesheet_retail_changes where PRICE_CHANGE_EFFECTIVE_DATE = @price_change_effective_date and IND_MARGIN_CHANGES = @MarginChanges
				--and @MarginChanges = 0 
				and @ReinstateRescind = 0

	Insert into marginmgr.pricesheet_retail_changes
	([PRICE_MGMT_TYPE_CR_FK]
	, [ITEM_CHANGE_TYPE_CR_FK]
	, [QUALIFIED_STATUS_CR_FK]
	, [PRICESHEET_BASIS_VALUES_FK]
	, pricesheet_basis_values_fk_cur
	, [ZONE]
	, [ZONE_NAME]
	, Zone_Sort
	, ITEM
	, ALIAS_ID
	, [ALIAS_DATA_VALUE_FK]
	, [ITEM_DESCRIPTION]
	, ALIAS_DESCRIPTION
	, cost_change_effective_date_cur
	, [STORE_PACK]
	, size_uom
	, case_cost_cur
	, unit_cost_cur
	, [PRICE_MULTIPLE_CUR]
	, PRICE_MULTIPLE_NEW
	, [PRICE_CUR]
	, [CUR_GM_PCT]
	, [PRICE_EFFECTIVE_DATE_CUR]
	, case_cost_new
	, unit_cost_new
	, [COST_CHANGE]
	, [PRICE_CHANGE]
	, [PRICE_NEW]
	, [NEW_GM_PCT]
	, [ind_Margin_Tol]
	, [COST_CHANGE_EFFECTIVE_DATE]
	, [TARGET_MARGIN]
	, lg_activation
	, LEVEL_GROSS
	, [MINAMT]
	, [DAYSOUT]
	, [PRODUCT_STRUCTURE_FK]
	, [ZONE_ENTITY_STRUCTURE_FK]
	, [SUBGROUP_ID]
	, [SUBGROUP_DESCRIPTION]
	, [SUBGROUP_DATA_VALUE_FK]
	, [USE_LG_PRICE_POINTS]
	, price_initial
	, [DIGIT_ADJ]
	, price_rounded
	, [PRICE_CHANGE_EFFECTIVE_DATE]
	, [CREATE_TIMESTAMP]
	, [PARITY_DISCOUNT_AMT]
	, [PARITY_MULT]
	, [PARITY_DISCOUNT_PCT]
	, [IND_COST_CHANGE_ITEM]
	, [INSERT_STEP]
	, [COST_CHANGE_REASON]
	, change_status
	, ind_margin_changes
	, [STAGE_FOR_PROCESSING_FK]
	, related_item_alias_id
	, ftn_breakdown_parent
	, ftn_cost_delta
	, ind_display_as_reg
	, parity_alias_id
	, ind_parity
	, ind_parity_status
	, parity_status
	, parity_parent_product_structure_fk
	, parity_child_product_structure_fk
	, parity_parent_item
	, parity_child_item
	, pricing_steps
	, previous_lg_date
	, item_group
	, item_sort
	, entity_structure_fk
	, entity_data_name_fk
	, qualified_status_step
	, unique_cost_record_id
	, item_group_alias_item)
select
	price_mgmt_type_cr_fk
	, item_change_type_cr_fk
	, qualified_status_cr_fk
	, Pricesheet_Basis_Values_FK
	, pricesheet_basis_values_fk_cur
	, Zone_id 'ZONE'
	, Zone_Name 'ZONE_NAME'
	, Zone_Sort
	, item 'ITEM'
	, alias_id 'ALIAS_ID'
	, alias_id 'ALIAS_DATA_VALUE_FK'
	, Item_Description 'ITEM_DESCRIPTION'
	, alias_description 'ALIAS_DESCRIPTION'
	, cost_change_effective_date_cur
	, pack 'STORE_PACK'
	, size_uom
	, case_cost_cur
	, unit_cost_cur
	, price_multiple 'PRICE_MULTIPLE_CUR'
	, price_multiple_new 'PRICE_MULTIPLE_NEW'
	, PRICE_CUR
	, Cast(Case when isnull(unit_Cost_Cur, Unit_Cost_new) = 0 or isnull(price_cur, 0) = 0 then Null else (Price_Cur - (isnull(Unit_Cost_Cur, Unit_Cost_new)) * isnull(price_multiple, 1)) / Price_Cur end as Numeric(18, 4)) 'CUR_GM_PCT'
	, PRICE_EFFECTIVE_DATE_CUR
	, case_cost_new
	, unit_cost_new
	, Case when isnull(tp.unit_cost_cur, 0) = isnull(tp.Unit_cost_new, 0) or isnull(tp.unit_cost_cur, 0) = 0 or isnull(tp.Unit_cost_new, 0) = 0 then Null else tp.unit_cost_new - tp.unit_cost_Cur end 'COST_CHANGE'
	, Case when isnull(tp.price_cur, 0) = isnull(tp.price_new, 0) or isnull(tp.price_cur, 0) = 0 or isnull(tp.price_new, 0) = 0 then Null 
		else cast((tp.price_new / isnull(tp.PRICE_MULTIPLE_NEW, 1)) as numeric(18, 4)) - cast((tp.price_cur / isnull(tp.PRICE_MULTIPLE, 1)) as numeric(18, 4)) end 'PRICE_CHANGE'
	, price_new 'PRICE_NEW'
	, Cast(Case	when price_mgmt_type_cr_fk = 651 and gp_new is not null then gp_new
				when price_mgmt_type_cr_fk = 651 and isnull(unit_Cost_New, 0) <> 0 and isnull(price_cur, 0) <> 0 then 
					(price_cur - (unit_cost_new * isnull(price_multiple, 1))) / price_cur
				when isnull(isnull(unit_cost_new, unit_Cost_Cur), 0) = 0 then Null 
				else 
				(Price_new - (isnull(Unit_Cost_new, Unit_Cost_Cur) * isnull(tp.PRICE_MULTIPLE_NEW, 1) )) / Price_New end as Numeric(18, 4)) 'NEW_GM_PCT'

	, Case when isnull(price_cur, 0) = 0 then null else 
		Case	when Cast(Case when isnull(unit_Cost_Cur, 0) = 0 then Null 
				else (Price_new - isnull(Unit_Cost_new, Unit_Cost_Cur)) / (Price_New) end as Numeric(18, 4)) 
					- Cast(Case when isnull(unit_Cost_Cur, 0) = 0 then Null else (Price_Cur - Unit_Cost_Cur) / (Price_Cur) end as Numeric(18, 4)) > mt.margin_tol then 1
					  			when Cast(Case when isnull(unit_Cost_Cur, 0) = 0 then Null else (Price_new - isnull(Unit_Cost_new, Unit_Cost_Cur)) / (Price_New) end as Numeric(18, 4)) 
								- Cast(Case when isnull(unit_Cost_Cur, 0) = 0 then Null else (Price_Cur - Unit_Cost_Cur) / (Price_Cur) end as Numeric(18, 4)) 	< mt.margin_tol * -1 then -1 else Null 
		end 
	  end 'ind_Margin_Tol'
	, cost_change_effective_date 'COST_CHANGE_EFFECTIVE_DATE'
	, urm_target_margin 'TARGET_MARGIN'
	, lg_active 'lg_activation'
	, lg_flag 'LEVEL_GROSS'
	, MINAMT
	, (select top 1 daysout from #zones) 'DAYSOUT'
	, PRODUCT_STRUCTURE_FK
	, entity_structure_fk 'ZONE_ENTITY_STRUCTURE_FK'
	, subgroup_id 'SUBGROUP_ID'
	, subgroup_description 'SUBGROUP_DESCRIPTION'
	, subgroup_data_value_fk 'SUBGROUP_DATA_VALUE_FK'
	, use_lg_price_points 'USE_LG_PRICE_POINTS'
	, price_new_initial 'price_initial'
	, DIGIT_ADJ
	, price_rounded
	, @price_change_effective_date 'PRICE_CHANGE_EFFECTIVE_DATE'
	, getdate() 'CREATE_TIMESTAMP'
	, parity_discount_amt 'PARITY_DISCOUNT_AMT'
	, parity_mult 'PARITY_MULT'
	, parity_discount_pct 'PARITY_DISCOUNT_PCT'
	, ind_cost_change_item 'IND_COST_CHANGE_ITEM'
	, load_step 'INSERT_STEP'
	, pbv_status 'COST_CHANGE_REASON'
	, change_status
	, @marginChanges 'ind_margin_changes'
	, case when @marginchanges = 0 then null else tp.stage_for_processing_fk end 'STAGE_FOR_PROCESSING_FK'
	, related_item_alias_id
	, ftn_breakdown_parent
	, ftn_cost_delta
	, ind_display_as_reg
	, parity_alias_id
	, ind_parity
	, ind_parity_status
	, parity_status
	, parity_parent_product_structure_fk
	, parity_child_product_structure_fk
	, parity_parent_item
	, parity_child_item
	, pricing_steps
	, cost_change_processed_date 'previous_lg_date'
	, item_group
	, item_sort
	, entity_structure_fk
	, 151000 'entity_data_name_fk'
	, qualified_status_step
	, unique_cost_record_id
	, igai
	from #t_prod tp
	cross Join (Select cast(ep.value as numeric(18, 2)) 'margin_tol' from epacube.epacube.epacube_params ep Where ep.application_scope_fk = 5013 and ep.[name] = 'MARGIN CHANGE TOLERENCE') mt

	if @marginchanges = 0
		exec [precision_comps].[level_gross_audit_items] @price_Change_Effective_Date

eof:
	if object_id('dbo.pentaho_processing_test_log') is not null
	Begin
		Update pptl
		Set process_time_end = getdate()
		from dbo.pentaho_processing_test_log pptl where pentaho_processing_test_log_id = @QryID
	End

	If @marginchanges = 0 and @ReinstateRescind = 0
		ALTER INDEX ALL ON marginmgr.pricesheet_retail_changes
		REBUILD WITH (FILLFACTOR = 80, SORT_IN_TEMPDB = ON, STATISTICS_NORECOMPUTE = ON)
