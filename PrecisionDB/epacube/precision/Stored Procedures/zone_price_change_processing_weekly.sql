﻿-- =============================================
-- Author:		<Gary Stone>
-- Create date: <2018-07-24>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [precision].[zone_price_change_processing_weekly] @Price_Change_Effective_date as date = Null
AS
	SET NOCOUNT ON;

	--Declare @Price_Change_Effective_date as date
	--Set @Price_Change_Effective_date = '2019-06-07'

	Set @Price_Change_Effective_date = Case when isnull(@Price_Change_Effective_date, '') = '' then CAST(getdate() + 6 - datepart(dw, getdate()) AS DATE) else @Price_Change_Effective_date end

	If @Price_Change_Effective_date <> cast(getdate() as date)
	goto eof

	Delete from marginmgr.pricesheet_retails where EFFECTIVE_DATE = @Price_Change_Effective_date and Data_Source = 'LEVEL GROSS PROCESSING' and data_name_fk = 503301

	If Object_ID('tempdb..#prc') is not null
	drop table #prc

	Declare @CL bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'CL')
	Declare @CS bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'CS')
	Declare @CZ bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'CZ')
	Declare @DP bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'DP')
	Declare @FT bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'FT')
	Declare @GA bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'GA')
	Declare @GD bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'GD')
	Declare @GL bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'GL')
	Declare @GM bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'GM')
	Declare @GP bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'GP')
	Declare @GR bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'GR')
	Declare @ND bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'ND')
	Declare @NL bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'NL')
	Declare @NW bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'NW')
	Declare @RR bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'RR')
	Declare @TO bigint = (Select data_value_id from epacube.data_value with (nolock) where data_name_fk = 503321 and value = 'TO')

	Select 
	prc.*
	into #prc
	from marginmgr.PRICESHEET_RETAIL_CHANGES PRC with (nolock)
	inner join epacube.entity_identification eiz with (nolock) on prc.ZONE_ENTITY_STRUCTURE_FK = eiz.entity_structure_fk and eiz.entity_data_name_fk = 151000 and eiz.data_name_fk = 151110
	left join marginmgr.pricesheet_retails pr with (nolock) on prc.pricesheet_retail_changes_id = pr.pricesheet_retail_changes_fk and prc.PRICE_CHANGE_EFFECTIVE_DATE = pr.EFFECTIVE_DATE
															and pr.data_source = 'LEVEL GROSS PROCESSING'
	where 1 = 1
	and prc.PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_date
	and pr.pricesheet_retails_id is null

	update prcc
	set SRP_ADJUSTED = prcp.SRP_ADJUSTED
	, TM_ADJUSTED = prcp.TM_ADJUSTED
	, GM_ADJUSTED = prcp.GM_ADJUSTED
	, PRICE_MULTIPLE_NEW = prcp.PRICE_MULTIPLE_NEW
	, HOLD_CUR_PRICE = prcp.HOLD_CUR_PRICE
	, REVIEWED = prcp.REVIEWED
	from #prc prcp
	inner join #prc prcc on prcp.ITEM_CHANGE_TYPE_CR_FK in (656, 662)
						and prcc.ITEM_CHANGE_TYPE_CR_FK = 657
						and isnull(prcp.IND_MARGIN_CHANGES, 0) = isnull(prcc.IND_MARGIN_CHANGES, 0)
						and prcp.ZONE_ENTITY_STRUCTURE_FK = prcc.ZONE_ENTITY_STRUCTURE_FK
						and prcp.ALIAS_ID = prcc.ALIAS_ID

----Insert all types of changes, including alias parity, except alias non-parity 
	Insert into marginmgr.pricesheet_retails
	(Price_Mgmt_Type_CR_FK, Item_Change_Type_CR_FK, Qualified_Status_CR_FK, Parity_PL_Parent_Structure_FK, Parity_SIZE_Parent_Structure_FK, Alias_Data_Value_FK, Target_Margin, Use_LG_Price_Points, Data_Name_FK
	, Product_Structure_FK, Zone_Entity_Structure_FK, Price_Multiple, UNIT_COST, Price, GM_PCT, Effective_Date, Price_Type, Retail_Source_Data_Name_FK, Retail_Source_FK, Retail_Source, Price_Change_Reason_DV_FK
	, Price_Change_Reason
	, Pricesheet_Retail_Changes_FK, Create_User, [Data_Source], PRICESHEET_BASIS_VALUES_FK)
	Select 
	Price_Mgmt_Type_CR_FK, Item_Change_Type_CR_FK, Qualified_Status_CR_FK, Parity_PL_Parent_Structure_FK, Parity_SIZE_Parent_Structure_FK, Alias_Data_Value_FK
	, Target_Margin
	, isnull(Use_LG_Price_Points, 'Y') 'Use_LG_Price_Points'
	, Data_Name_FK
	, Product_Structure_FK, Zone_Entity_Structure_FK
	, isnull(Price_Multiple, 1) 'Price_Multiple'
	, UNIT_COST, Price
	, cast((price - unit_cost) / price as numeric(18, 4)) 'GM_PCT'
	, isnull(Effective_Date, @Price_Change_Effective_date) 'Effective_Date'
	, Price_Type, Retail_Source_Data_Name_FK, Retail_Source_FK, Retail_Source, Price_Change_Reason_DV_FK
	, (select [value] + ': ' + [description] from epacube.data_value dv with (nolock) where data_name_fk = 503321 and data_value_id = A.Price_Change_Reason_DV_FK) 'Price_Change_Reason'
	, Pricesheet_Retail_Changes_FK, Create_User, [Data_Source], PRICESHEET_BASIS_VALUES_FK
	from (
	Select 
	prc.Price_Mgmt_Type_CR_FK, prc.Item_Change_Type_CR_FK, prc.Qualified_Status_CR_FK, prc.Parity_PL_Parent_Structure_FK, prc.Parity_SIZE_Parent_Structure_FK
	, NULL 'Alias_Data_Value_FK'
	, isnull(PRC.TM_Adjusted,prc.Target_Margin) 'Target_Margin'
	, prc.Use_LG_Price_Points
	, 503301 'Data_Name_FK'
	, PRC.PRODUCT_STRUCTURE_FK
	, PRC.ZONE_ENTITY_STRUCTURE_FK 
	, isnull(PRC.PRICE_MULTIPLE_NEW, PRC.PRICE_MULTIPLE_CUR) 'Price_Multiple'
	, case when isnull(prc.unit_cost_new, 0) = 0 then prc.unit_cost_cur else prc.unit_cost_new end 'Unit_Cost'
	, isnull(PRC.SRP_Adjusted, PRC.Price_New) 'Price'
	, isnull(PRC.GM_Adjusted , PRC.New_GM_Pct) 'GM_pct'
	, PRC.PRICE_CHANGE_EFFECTIVE_DATE 'Effective_Date'
	, 1 'PRICE_TYPE'
	, 151110 'RETAIL_SOURCE_DATA_NAME_FK'
	, PRC.ZONE_ENTITY_STRUCTURE_FK 'RETAIL_SOURCE_FK'
	, prc.[zone] 'RETAIL_SOURCE'
	, Case	when prc.PARITY_PL_PARENT_STRUCTURE_FK is not null or prc.PARITY_SIZE_PARENT_STRUCTURE_FK is not null then @CZ
			when prc.Level_Gross = 'Y' then
				Case	When IsNull(prc.cost_change, 0) > 0 then @GA
						When IsNull(prc.cost_change, 0) = 0 then @GM
						When IsNull(prc.cost_change, 0) < 0 then @GD end
			when isnull(PRC.SRP_Adjusted, PRC.Price_New) <> 0 and isnull(PRC.Price_Cur, 0) = 0 then @NW
			when isnull(PRC.SRP_Adjusted, PRC.Price_New) < isnull(PRC.Price_Cur, 0) then @RR
		end 'Price_Change_Reason_DV_FK'
	, prc.Pricesheet_Retail_Changes_ID 'Pricesheet_Retail_Changes_FK'
	, isnull(ISNULL(ISNULL(ISNULL(PRC.UPDATE_USER_PRICE, PRC.UPDATE_USER_SET_TM), UPDATE_USER_HOLD), UPDATE_USER_REVIEWED), 'Level Gross Auto-Approved') 'CREATE_USER'
	, 'LEVEL GROSS PROCESSING' 'DATA_SOURCE'
	, PRC.PRICESHEET_BASIS_VALUES_FK
	from #prc prc
	where 1 = 1
		and (
				isnull(prc.SRP_ADJUSTED, prc.price_new) <> isnull(prc.price_cur, 0)
				or
				TM_ADJUSTED is not null
			)
		and prc.PRODUCT_STRUCTURE_FK is not null
	) A

----Move adjusted Target Margins to Segments_Settings
	Insert into epacube.segments_settings
	(Org_Entity_Structure_FK, ZONE_ENTITY_STRUCTURE_FK, Data_Name_FK, ATTRIBUTE_NUMBER, ATTRIBUTE_EVENT_DATA, Precedence, RECORD_STATUS_CR_FK
	, create_user, Update_User, prod_segment_fk, zone_segment_fk
	, Product_Structure_FK, Effective_Date, Entity_Data_Name_FK, PROD_SEGMENT_DATA_NAME_FK, DATA_SOURCE)
	Select 
	1 'Org_Entity_Structure_FK', prc.ZONE_ENTITY_STRUCTURE_FK, 502046 'Data_Name_FK', prc.TM_ADJUSTED 'Attribute_Number', prc.TM_ADJUSTED 'Attribute_Event_Data', 5 'Precedence', 1 'Record_Status_CR_FK'
	, prc.Update_User_Set_TM 'Create_User', 'Level Gross Adjustment' 'Update_User', prc.PRODUCT_STRUCTURE_FK, prc.ZONE_ENTITY_STRUCTURE_FK, prc.PRODUCT_STRUCTURE_FK, prc.PRICE_CHANGE_EFFECTIVE_DATE 'Effective_Date', 151000 'Entity_Data_Name_FK', 110103 'prod_segment_data_name_fk'
	, 'LEVEL GROSS PROCESSING' 'DATA_SOURCE'
	from #prc prc with (nolock)
	left join epacube.segments_settings ss with (nolock) on prc.ZONE_ENTITY_STRUCTURE_FK = ss.ZONE_SEGMENT_FK and prc.product_structure_fk = ss.PRODUCT_STRUCTURE_FK and ss.data_name_fk = 502046
														and prc.PRICE_CHANGE_EFFECTIVE_DATE = ss.Effective_Date
	where 1 = 1
    and prc.PRODUCT_STRUCTURE_FK is not null
	and prc.TM_ADJUSTED is not null 
	and prc.TM_ADJUSTED <> isnull(prc.TARGET_MARGIN, 0)
	and ss.segments_settings_id is null

eof:
