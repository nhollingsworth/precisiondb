﻿
-- =============================================
-- Author:		Gary Stone
-- Create date: July 29, 2019
-- Description:	Common proc to calculate pricing
--	Modifications
--	IN: 885 URM-1881 5/12/2020	Change in current cost to include today's date when not a level gross calculation
-- =============================================

CREATE PROCEDURE [precision].[Pricing_Calcs_Common_Tmp_Tbls] @LGprocDate date = Null, @effective_date date = Null

AS
	SET NOCOUNT ON;

	Set @effective_date = isnull(@effective_date, cast(getdate() as date))
	Declare @Yesterday date = Case when @LGprocDate is null then cast(getdate() - 1 as date) else Null end
	Declare @LG_Source int = Case when @LGprocDate is null then 0 else 1 end	-- 0 = General; 1 = Level Gross
	Set @LGprocDate = Case when @LGprocDate is null then @effective_date else @LGprocDate end

/*	*** the following create table code is required for each procedure calling this one
	if object_id('tempdb..#pbv') is not null
	drop table #pbv

	if object_id('tempdb..#t_prod') is not null
	drop table #t_prod

	If object_id('tempdb..#zones') is not null
	drop table #zones

		create table #pbv(value numeric(18, 6) null, unit_value numeric(18, 6) null, effective_date date null, pack int null, size_uom varchar(16) null, product_structure_fk bigint null, pricesheet_basis_values_id bigint null, data_name_fk bigint null
		, create_timestamp datetime null, cust_entity_structure_fk bigint null, drank bigint null, pbv_status varchar(128) null, add_to_zone_date date null, change_status int null, cost_change_processed_date date null
		, drank_f bigint null, ind_cost_change_item int null, item varchar(64) null, unique_cost_record_id varchar(64) null, pbv_id bigint identity(1, 1) not null)

		create table #t_prod(item_change_type_cr_fk bigint null, qualified_status_cr_fk bigint null, price_mgmt_type_cr_fk bigint null, pack int null, size_uom varchar(16) null
		, parent_product_structure_fk bigint null, item varchar(64) null, item_description varchar(64) null, product_structure_fk bigint null, entity_structure_fk bigint null, cost_change_effective_date_cur date null
		, cost_change_effective_date date null, case_cost_new numeric(18, 6) null, unit_cost_new numeric(18, 6) null, case_cost_cur numeric(18, 2) null, unit_cost_cur numeric(18, 4) null, price_multiple int null
		, price_multiple_new int null, price_effective_date_cur date null, price_cur numeric(18, 2) null, price_new_initial numeric(18, 2) null, price_new numeric(18, 2) null, tm_pct_cur numeric(18, 4) null
		, urm_target_margin numeric(18, 4) null, gp_new numeric(18, 4) null, pricesheet_basis_values_fk bigint null, pricesheet_basis_values_fk_cur bigint null, pbv_status varchar(128) null, change_status int null
		, cost_change_processed_date date null, ind_cost_change_item int null, ind_requested_item int null, parity_type varchar(16) null, parity_status varchar(8) null, ind_parity_status int null, ind_parity int null
		, parity_parent_product_structure_fk bigint null, parity_child_product_structure_fk bigint null, parity_parent_item varchar(64) null, parity_child_item varchar(64) null, parity_discount_amt numeric(18, 2) null
		, parity_mult int null, parity_discount_pct numeric(18, 4) null, alias_id bigint null, alias_description varchar(64) null, parity_alias_id bigint null, ind_initial_load int null, load_step int null
		, lg_active varchar(8) null, lg_flag varchar(8) null, use_lg_price_points varchar(8) null, minamt numeric(18, 2) null, zone_id varchar(64) null, zone_name varchar(64) null, zone_sort int null, pricing_steps varchar(128) null
		, related_item_alias_id bigint null, unit_cost_new_actual numeric(18, 4), subgroup_id bigint null, subgroup_description varchar(64) null, subgroup_data_value_fk bigint null, item_sort bigint null, item_group bigint null
		, ind_display_as_reg int null, Digit_DN_FK bigint null, Digit_Adj Numeric(18, 2) Null, price_rounded Numeric(18, 2) Null, ftn_breakdown_parent varchar(8) null, ftn_cost_delta varchar(8) null, stage_for_processing_fk bigint null
		, qualified_status_step varchar(64) Null, unique_cost_record_id varchar(64) null, t_prod_id bigint identity(1, 1) not null)

		CREATE NONCLUSTERED INDEX idx_ps_es on #t_prod([product_structure_fk],[entity_structure_fk])

		CREATE NONCLUSTERED INDEX idx_alias_include1 on #t_prod([alias_id])
		INCLUDE ([product_structure_fk],[entity_structure_fk],[Parity_Status],[t_prod_id])

		CREATE NONCLUSTERED INDEX idx_alias_include2 on #t_prod([alias_id])
		INCLUDE ([entity_structure_fk],[Parity_Status],[parity_child_product_structure_fk],[t_prod_id])

		create table #zones(entity_structure_fk bigint not null, zone_type varchar(8) null, zone_id varchar(16) not null, zone_name varchar(64) null, daysout int not null, LG_Active varchar(8))
		create index idx_es on #zones(entity_structure_fk)

*/
	create index idx_01 on #pbv(product_structure_fk)

	CREATE NONCLUSTERED INDEX idx_es_ps
	on #t_prod([entity_structure_fk], [product_structure_fk])

	CREATE NONCLUSTERED INDEX idx_alias_include1 
	on #t_prod([alias_id])
	INCLUDE ([product_structure_fk],[entity_structure_fk],[Parity_Status],[t_prod_id])

	create index idx_es_ulgpp 
	on #t_prod([entity_structure_fk],[USE_LG_PRICE_POINTS])

	CREATE NONCLUSTERED INDEX idx_alias_include2 
	on #t_prod([alias_id])
	INCLUDE ([entity_structure_fk],[Parity_Status],[parity_child_product_structure_fk],[t_prod_id])

	create index idx_es on #zones(entity_structure_fk)

	CREATE NONCLUSTERED INDEX ids_es_mamt
	ON #t_prod ([entity_structure_fk],[MINAMT])

	CREATE NONCLUSTERED INDEX idx_alias_incl1
	on #t_prod ([alias_id])
	INCLUDE ([entity_structure_fk],[Parity_Status],[ind_parity_status],[parity_child_product_structure_fk],[PARITY_DISCOUNT_AMT],[PARITY_MULT],[PARITY_DISCOUNT_PCT],[t_prod_id])

	CREATE NONCLUSTERED INDEX idx_alias_incl2
	on #t_prod ([alias_id])
	INCLUDE ([entity_structure_fk],[Parity_Status],[PARITY_DISCOUNT_AMT],[PARITY_MULT],[PARITY_DISCOUNT_PCT],[parity_alias_id],[t_prod_id])

--1. load parent parity items with cost changes
		insert into #t_prod
		(parent_product_structure_fk, product_structure_fk, entity_structure_fk, cost_change_effective_date, Case_Cost_New, Unit_Cost_New, Pricesheet_Basis_Values_FK, PBV_Status
		, Change_Status, cost_change_processed_date, ind_cost_change_item, ind_Initial_Load, load_step, ind_requested_item, unique_cost_record_id)
		select pa_159903.product_structure_fk 'parent_product_structure_fk', pa_159903.product_structure_fk, pa_159903.entity_structure_fk
		, pbv.effective_date 'cost_change_effective_date', pbv.value 'Case_Cost_New', pbv.unit_value 'Unit_Cost_New', pbv.Pricesheet_Basis_Values_ID 'Pricesheet_Basis_Values_FK', pbv.PBV_Status, pbv.Change_Status, pbv.cost_change_processed_date
		, 1 'ind_cost_change_item', 1 'ind_Initial_Load', 1 'load_step', 1 'ind_requested_item', pbv.unique_cost_record_id
		FROM epacube.product_association pa_159450P with (nolock)
			inner join epacube.product_association pa_159903 with (nolock)
				on pa_159450P.PRODUCT_STRUCTURE_FK = pa_159903.PRODUCT_STRUCTURE_FK 
				and pa_159450P.ENTITY_STRUCTURE_FK = pa_159903.ENTITY_STRUCTURE_FK 
				and pa_159450P.data_name_fk = 159450 
				and pa_159450P.RECORD_STATUS_CR_FK = 1
				and pa_159903.DATA_NAME_FK = 159903
				and cast(pa_159903.CREATE_TIMESTAMP as date) <= @LGprocDate
			inner join epacube.product_association pa_159450C with (nolock) on pa_159903.ENTITY_STRUCTURE_FK = pa_159450C.entity_structure_fk and pa_159903.child_product_structure_fk = pa_159450C.product_structure_fk and pa_159450C.DATA_NAME_FK = 159450
																			and cast(pa_159450C.CREATE_TIMESTAMP as date) <= @LGprocDate
			inner join #zones z on pa_159450P.ENTITY_STRUCTURE_FK = z.entity_structure_fk
			inner join #PBV PBV on pa_159450P.PRODUCT_STRUCTURE_FK = PBV.product_structure_fk
			--left join epacube.product_attribute pa_500012 with (nolock) on pa_159903.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
			--left join epacube.product_attribute pa_500012c with (nolock) on pa_159903.child_product_structure_fk = pa_500012c.PRODUCT_STRUCTURE_FK and pa_500012c.data_name_fk = 500012
		where 1 = 1
		--and pa_500012.product_attribute_id is null
		--and pa_500012c.product_attribute_id is null

--2. load child parity items with cost changes
		Insert into #t_prod
		(parent_product_structure_fk, product_structure_fk, entity_structure_fk, cost_change_effective_date, Case_Cost_New, Unit_Cost_New, Pricesheet_Basis_Values_FK, PBV_Status
		, Change_Status, cost_change_processed_date, ind_cost_change_item, ind_Initial_Load, load_step, ind_requested_item, unique_cost_record_id)
		select pa_159903.product_structure_fk 'parent_product_structure_fk', pa_159903.child_product_structure_fk, pa_159903.entity_structure_fk 
		, pbv.effective_date 'cost_change_effective_date', pbv.value 'Case_Cost_New', pbv.unit_value 'Unit_Cost_New', pbv.Pricesheet_Basis_Values_ID 'Pricesheet_Basis_Values_FK', pbv.PBV_Status, pbv.Change_Status, pbv.cost_change_processed_date
		, 1 'ind_cost_change_item', 1 'ind_Initial_Load', 2 'load_step', 1 'ind_requested_item', pbv.unique_cost_record_id
		FROM epacube.product_association pa_159450P with (nolock)
			inner join epacube.product_association pa_159903 with (nolock)
				on pa_159450P.PRODUCT_STRUCTURE_FK = pa_159903.CHILD_PRODUCT_STRUCTURE_FK 
				and pa_159450P.ENTITY_STRUCTURE_FK = pa_159903.ENTITY_STRUCTURE_FK 
				and pa_159450P.data_name_fk = 159450 
				and pa_159450P.RECORD_STATUS_CR_FK = 1
				and cast(pa_159450P.CREATE_TIMESTAMP as date) <= @LGprocDate
				and pa_159903.DATA_NAME_FK = 159903
				and cast(pa_159903.CREATE_TIMESTAMP as date) <= @LGprocDate
			inner join epacube.product_association pa_159450C with (nolock) on pa_159903.ENTITY_STRUCTURE_FK = pa_159450C.entity_structure_fk and pa_159903.product_structure_fk = pa_159450C.product_structure_fk and pa_159450C.DATA_NAME_FK = 159450
																			and pa_159450C.RECORD_STATUS_CR_FK = 1 and cast(pa_159450C.CREATE_TIMESTAMP as date) <= @LGprocDate
			inner join #zones z on pa_159450P.ENTITY_STRUCTURE_FK = z.entity_structure_fk
			inner join #PBV PBV on pa_159903.child_product_structure_fk = PBV.product_structure_fk
			--left join epacube.product_attribute pa_500012 with (nolock) on pa_159903.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
			--left join epacube.product_attribute pa_500012c with (nolock) on pa_159903.child_product_structure_fk = pa_500012c.PRODUCT_STRUCTURE_FK and pa_500012c.data_name_fk = 500012
		where 1 = 1
		--and pa_500012.product_attribute_id is null
		--and pa_500012c.product_attribute_id is null

--3. load remaining items with cost changes that have no parity

		Insert into #t_prod
		(parent_product_structure_fk, product_structure_fk, entity_structure_fk, cost_change_effective_date, Case_Cost_New, Unit_Cost_New, Pricesheet_Basis_Values_FK, PBV_Status
		, Change_Status, cost_change_processed_date, ind_cost_change_item, ind_Initial_Load, load_step, ind_requested_item, unique_cost_record_id)
		select pa_159450P.product_structure_fk 'parent_product_structure_fk', pa_159450P.product_structure_fk, pa_159450P.entity_structure_fk
		, pbv.effective_date 'cost_change_effective_date', pbv.value 'Case_Cost_New', pbv.unit_value 'Unit_Cost_New', pbv.Pricesheet_Basis_Values_ID 'Pricesheet_Basis_Values_FK', pbv.PBV_Status, pbv.Change_Status, pbv.cost_change_processed_date
		, 1 'ind_cost_change_item', 1 'ind_Initial_Load', 3 'load_step', 1 'ind_requested_item', pbv.unique_cost_record_id
		FROM epacube.product_association pa_159450P with (nolock)
		inner join #PBV PBV on pa_159450P.product_structure_fk = PBV.product_structure_fk and pa_159450P.DATA_NAME_FK = 159450 and cast(pa_159450P.CREATE_TIMESTAMP as date) <= @LGprocDate and pa_159450P.RECORD_STATUS_CR_FK = 1
		inner join #zones z on pa_159450P.ENTITY_STRUCTURE_FK = z.entity_structure_fk
		left join #t_prod t_prod on pbv.Product_Structure_FK = t_prod.product_structure_fk and pa_159450P.ENTITY_STRUCTURE_FK = t_prod.ENTITY_STRUCTURE_FK
		--left join epacube.product_attribute pa_500012 with (nolock) on pa_159450P.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
		where t_prod.t_prod_id is null
		--and pa_500012.product_attribute_id is null

--4. load alias items to those already loaded

		Insert into #t_prod
		(product_structure_fk, entity_structure_fk, ind_cost_change_item, ind_Initial_Load, load_step, PBV_Status, ind_requested_item)
		select distinct pmt.product_structure_fk, pmt.entity_structure_fk
		, 0 'ind_cost_change_item', 0 'ind_Initial_Load', 4 'load_step', 'aliases to items with cost changes' 'PBV_Status', 0 'ind_requested_item'
		from #t_prod t_prod
		inner join epacube.product_mult_type pmt with (nolock) on t_prod.ENTITY_STRUCTURE_FK = pmt.ENTITY_STRUCTURE_FK
																and pmt.data_name_fk = 500019
																and cast(pmt.CREATE_TIMESTAMP as date) <= @LGprocDate
																and pmt.data_value_fk = (select data_value_fk from epacube.product_mult_type pmt1 with (nolock)
																							where product_structure_fk = t_prod.product_structure_fk
																							and ENTITY_STRUCTURE_FK = t_prod.ENTITY_STRUCTURE_FK
																							and cast(CREATE_TIMESTAMP as date) <= @LGprocDate
																							and data_name_fk = 500019)
		inner join epacube.product_association pa_159450 with (nolock) on pmt.ENTITY_STRUCTURE_FK = pa_159450.entity_structure_fk and pmt.product_structure_fk = pa_159450.product_structure_fk and pa_159450.DATA_NAME_FK = 159450
																		and cast(pa_159450.CREATE_TIMESTAMP as date) <= @LGprocDate
																		and pa_159450.RECORD_STATUS_CR_FK = 1
		left join #t_prod t_prod1 on pmt.ENTITY_STRUCTURE_FK = t_prod1.ENTITY_STRUCTURE_FK
									and pmt.PRODUCT_STRUCTURE_FK = t_prod1.PRODUCT_STRUCTURE_FK
		where t_prod1.t_prod_id is null
		and pmt.PRODUCT_STRUCTURE_FK not in (select product_structure_fk from epacube.product_attribute where data_name_fk = 500015)

--5. load child items with parity to alias items loaded without cost changes
		Insert into #t_prod
		(parent_product_structure_fk, product_structure_fk, entity_structure_fk, ind_cost_change_item, ind_Initial_Load, load_step, pbv_status, ind_requested_item)
		select pa_159903.product_structure_fk 'parent_product_structure_fk', pa_159903.child_product_structure_fk 'product_structure_fk', pa_159903.entity_structure_fk
		, 0 'ind_cost_change_item', 0 'ind_Initial_Load', 5 'load_step', 'parity children to aliases loaded without cost changes' 'pbv_status', 0 'ind_requested_item'
		FROM #t_prod t_prod
			inner join epacube.product_association pa_159903 with (nolock)
				on t_prod.PRODUCT_STRUCTURE_FK = pa_159903.PRODUCT_STRUCTURE_FK 
				and t_prod.ENTITY_STRUCTURE_FK = pa_159903.ENTITY_STRUCTURE_FK 
				and pa_159903.DATA_NAME_FK = 159903
				and cast(pa_159903.CREATE_TIMESTAMP as date) <= @LGprocDate
			inner join #zones z on pa_159903.ENTITY_STRUCTURE_FK = z.entity_structure_fk
			inner join epacube.product_association pa_159450 with (nolock) on pa_159903.ENTITY_STRUCTURE_FK = pa_159450.entity_structure_fk and pa_159903.child_product_structure_fk = pa_159450.product_structure_fk and pa_159450.DATA_NAME_FK = 159450
																			and cast(pa_159450.CREATE_TIMESTAMP as date) <= @LGprocDate
																			and pa_159450.RECORD_STATUS_CR_FK = 1
			--left join epacube.product_attribute pa_500012 with (nolock) on pa_159903.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
			--left join epacube.product_attribute pa_500012c with (nolock) on pa_159903.child_product_structure_fk = pa_500012c.PRODUCT_STRUCTURE_FK and pa_500012c.data_name_fk = 500012
			left join #t_prod t_prod1 on pa_159903.CHILD_PRODUCT_STRUCTURE_FK = t_prod1.product_structure_fk
		where t_prod1.t_prod_id is null
		--and pa_500012.product_attribute_id is null
		--and pa_500012c.product_attribute_id is null

--6. load parent items with parity to alias items loaded without cost changes
		Insert into #t_prod
		(parent_product_structure_fk, product_structure_fk, entity_structure_fk, ind_cost_change_item, ind_Initial_Load, load_step, PBV_Status, ind_requested_item)
		select pa_159903.product_structure_fk 'parent_product_structure_fk', pa_159903.product_structure_fk, pa_159903.entity_structure_fk
		, 0 'ind_cost_change_item', 0 'ind_Initial_Load', 6 'load_step', 'parity parents to aliases loaded without cost changes' 'pbv_status', 0 'ind_requested_item'
		FROM #t_prod t_prod
			inner join epacube.product_association pa_159903 with (nolock)
				on t_prod.PRODUCT_STRUCTURE_FK = pa_159903.CHILD_PRODUCT_STRUCTURE_FK 
				and t_prod.ENTITY_STRUCTURE_FK = pa_159903.ENTITY_STRUCTURE_FK 
				and pa_159903.DATA_NAME_FK = 159903
				and cast(pa_159903.CREATE_TIMESTAMP as date) <= @LGprocDate
			inner join #zones z on pa_159903.ENTITY_STRUCTURE_FK = z.entity_structure_fk
			inner join epacube.product_association pa_159450P on pa_159903.PRODUCT_STRUCTURE_FK = pa_159450P.PRODUCT_STRUCTURE_FK and pa_159903.ENTITY_STRUCTURE_FK = pa_159450P.ENTITY_STRUCTURE_FK and pa_159450P.data_name_fk = 159450
															and pa_159450P.RECORD_STATUS_CR_FK = 1 and cast(pa_159450P.CREATE_TIMESTAMP as date) <= @LGprocDate
			inner join epacube.product_association pa_159450C on pa_159903.CHILD_PRODUCT_STRUCTURE_FK = pa_159450C.PRODUCT_STRUCTURE_FK and pa_159903.ENTITY_STRUCTURE_FK = pa_159450C.ENTITY_STRUCTURE_FK and pa_159450C.data_name_fk = 159450
															and pa_159450C.RECORD_STATUS_CR_FK = 1 and cast(pa_159450C.CREATE_TIMESTAMP as date) <= @LGprocDate
			--left join epacube.product_attribute pa_500012 with (nolock) on pa_159903.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
			--left join epacube.product_attribute pa_500012c with (nolock) on pa_159903.child_product_structure_fk = pa_500012c.PRODUCT_STRUCTURE_FK and pa_500012c.data_name_fk = 500012
			left join #t_prod t_prod1 on pa_159903.PRODUCT_STRUCTURE_FK = t_prod1.product_structure_fk
		where t_prod1.t_prod_id is null
		--and pa_500012.product_attribute_id is null
		--and pa_500012c.product_attribute_id is null

--7. load alias items to those parity items just loaded without cost changes
		Insert into #t_prod
		(product_structure_fk, entity_structure_fk, ind_cost_change_item, ind_Initial_Load, load_step, PBV_Status, ind_requested_item)
		select distinct pmt.product_structure_fk, pmt.entity_structure_fk
		, 0 'ind_cost_change_item', 0 'ind_Initial_Load', 7 'load_step', 'aliases to items loaded in parity to other aliases' 'pbv_status', 0 'ind_requested_item'
		from #t_prod t_prod
		inner join epacube.product_mult_type pmt with (nolock) on t_prod.ENTITY_STRUCTURE_FK = pmt.ENTITY_STRUCTURE_FK
																and pmt.data_name_fk = 500019
																and cast(pmt.CREATE_TIMESTAMP as date) <= @LGprocDate
																and pmt.data_value_fk = (select data_value_fk from epacube.product_mult_type pmt1 with (nolock)
																							where product_structure_fk = t_prod.product_structure_fk
																							and ENTITY_STRUCTURE_FK = t_prod.ENTITY_STRUCTURE_FK
																							and cast(CREATE_TIMESTAMP as date) <= @LGprocDate
																							and data_name_fk = 500019)
		inner join epacube.product_association pa_159450 on pmt.PRODUCT_STRUCTURE_FK = pa_159450.PRODUCT_STRUCTURE_FK and pmt.ENTITY_STRUCTURE_FK = pa_159450.ENTITY_STRUCTURE_FK and pa_159450.data_name_fk = 159450
														and pa_159450.RECORD_STATUS_CR_FK = 1 and cast(pa_159450.CREATE_TIMESTAMP as date) <= @LGprocDate
		left join #t_prod t_prod1 on pmt.ENTITY_STRUCTURE_FK = t_prod1.ENTITY_STRUCTURE_FK
									and pmt.PRODUCT_STRUCTURE_FK = t_prod1.PRODUCT_STRUCTURE_FK
		where t_prod1.t_prod_id is null
		and pmt.PRODUCT_STRUCTURE_FK not in (select product_structure_fk from epacube.product_attribute where data_name_fk = 500015)



--8. Again, load child items with parity to alias items loaded without cost changes
		Insert into #t_prod
		(parent_product_structure_fk, product_structure_fk, entity_structure_fk, ind_cost_change_item, ind_Initial_Load, load_step, pbv_status, ind_requested_item)
		select pa_159903.product_structure_fk 'parent_product_structure_fk', pa_159903.child_product_structure_fk 'product_structure_fk', pa_159903.entity_structure_fk
		, 0 'ind_cost_change_item', 0 'ind_Initial_Load', 8 'load_step', 'parity children to aliases loaded without cost changes' 'pbv_status', 0 'ind_requested_item'
		FROM #t_prod t_prod
			inner join epacube.product_association pa_159903 with (nolock)
				on t_prod.PRODUCT_STRUCTURE_FK = pa_159903.PRODUCT_STRUCTURE_FK 
				and t_prod.ENTITY_STRUCTURE_FK = pa_159903.ENTITY_STRUCTURE_FK 
				and pa_159903.DATA_NAME_FK = 159903
				and cast(pa_159903.CREATE_TIMESTAMP as date) <= @LGprocDate
			inner join #zones z on pa_159903.ENTITY_STRUCTURE_FK = z.entity_structure_fk
			inner join epacube.product_association pa_159450 with (nolock) on pa_159903.ENTITY_STRUCTURE_FK = pa_159450.entity_structure_fk and pa_159903.child_product_structure_fk = pa_159450.product_structure_fk and pa_159450.DATA_NAME_FK = 159450
																and pa_159450.RECORD_STATUS_CR_FK = 1 and cast(pa_159450.CREATE_TIMESTAMP as date) <= @LGprocDate
			--left join epacube.product_attribute pa_500012 with (nolock) on pa_159903.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
			--left join epacube.product_attribute pa_500012c with (nolock) on pa_159903.child_product_structure_fk = pa_500012c.PRODUCT_STRUCTURE_FK and pa_500012c.data_name_fk = 500012
			left join #t_prod t_prod1 on pa_159903.CHILD_PRODUCT_STRUCTURE_FK = t_prod1.product_structure_fk
		where t_prod1.t_prod_id is null
		--and pa_500012.product_attribute_id is null
		--and pa_500012c.product_attribute_id is null

--9. load parent items with parity to alias items loaded without cost changes
--		Check to see if this one is ever used
		Insert into #t_prod
		(parent_product_structure_fk, product_structure_fk, entity_structure_fk, ind_cost_change_item, ind_Initial_Load, load_step, PBV_Status, ind_requested_item)
		select pa_159903.product_structure_fk 'parent_product_structure_fk', pa_159903.product_structure_fk, pa_159903.entity_structure_fk
		, 0 'ind_cost_change_item', 0 'ind_Initial_Load', 9 'load_step', 'parity parents to aliases loaded without cost changes' 'pbv_status', 0 'ind_requested_item'
		FROM #t_prod t_prod
			inner join epacube.product_association pa_159903 with (nolock)
				on t_prod.PRODUCT_STRUCTURE_FK = pa_159903.CHILD_PRODUCT_STRUCTURE_FK 
				and t_prod.ENTITY_STRUCTURE_FK = pa_159903.ENTITY_STRUCTURE_FK 
				and pa_159903.DATA_NAME_FK = 159903
				and cast(pa_159903.CREATE_TIMESTAMP as date) <= @LGprocDate
			inner join #zones z on pa_159903.ENTITY_STRUCTURE_FK = z.entity_structure_fk
			inner join epacube.product_association pa_159450P on pa_159903.PRODUCT_STRUCTURE_FK = pa_159450P.PRODUCT_STRUCTURE_FK and pa_159903.ENTITY_STRUCTURE_FK = pa_159450P.ENTITY_STRUCTURE_FK and pa_159450P.data_name_fk = 159450
																and pa_159450P.RECORD_STATUS_CR_FK = 1 and cast(pa_159450P.CREATE_TIMESTAMP as date) <= @LGprocDate
			inner join epacube.product_association pa_159450C on pa_159903.CHILD_PRODUCT_STRUCTURE_FK = pa_159450C.PRODUCT_STRUCTURE_FK and pa_159903.ENTITY_STRUCTURE_FK = pa_159450C.ENTITY_STRUCTURE_FK and pa_159450C.data_name_fk = 159450
																and pa_159450C.RECORD_STATUS_CR_FK = 1 and cast(pa_159450C.CREATE_TIMESTAMP as date) <= @LGprocDate
			--left join epacube.product_attribute pa_500012 with (nolock) on pa_159903.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
			--left join epacube.product_attribute pa_500012c with (nolock) on pa_159903.child_product_structure_fk = pa_500012c.PRODUCT_STRUCTURE_FK and pa_500012c.data_name_fk = 500012
			left join #t_prod t_prod1 on pa_159903.PRODUCT_STRUCTURE_FK = t_prod1.product_structure_fk
		where t_prod1.t_prod_id is null
		--and pa_500012.product_attribute_id is null
		--and pa_500012c.product_attribute_id is null

--10. Again, load alias items to those parity items just loaded without cost changes
		Insert into #t_prod
		(product_structure_fk, entity_structure_fk, ind_cost_change_item, ind_Initial_Load, load_step, PBV_Status, ind_requested_item)
		select distinct pmt.product_structure_fk, pmt.entity_structure_fk
		, 0 'ind_cost_change_item', 0 'ind_Initial_Load', 10 'load_step', 'aliases to items loaded in parity to other aliases' 'pbv_status', 0 'ind_requested_item'
		from #t_prod t_prod
		inner join epacube.product_mult_type pmt with (nolock) on t_prod.ENTITY_STRUCTURE_FK = pmt.ENTITY_STRUCTURE_FK
																and pmt.data_name_fk = 500019
																and cast(pmt.CREATE_TIMESTAMP as date) <= @LGprocDate
																and pmt.data_value_fk = (select data_value_fk from epacube.product_mult_type pmt1 with (nolock)
																							where product_structure_fk = t_prod.product_structure_fk
																							and ENTITY_STRUCTURE_FK = t_prod.ENTITY_STRUCTURE_FK
																							and cast(CREATE_TIMESTAMP as date) <= @LGprocDate
																							and data_name_fk = 500019)
		inner join epacube.product_association pa_159450 on pmt.PRODUCT_STRUCTURE_FK = pa_159450.PRODUCT_STRUCTURE_FK and pmt.ENTITY_STRUCTURE_FK = pa_159450.ENTITY_STRUCTURE_FK and pa_159450.data_name_fk = 159450
		left join #t_prod t_prod1 on pmt.ENTITY_STRUCTURE_FK = t_prod1.ENTITY_STRUCTURE_FK
									and pmt.PRODUCT_STRUCTURE_FK = t_prod1.PRODUCT_STRUCTURE_FK
		where t_prod1.t_prod_id is null
		and pmt.PRODUCT_STRUCTURE_FK not in (select product_structure_fk from epacube.product_attribute where data_name_fk = 500015)

--	Delete items not authorized to the zone
		Delete tp from #t_prod tp
		left join epacube.product_association pa with (nolock) on tp.product_structure_fk = pa.product_structure_fk and tp.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK and pa.data_name_fk = 159450
		where isnull(pa.RECORD_STATUS_CR_FK, 2) <> 1
------

		update t_prod
		set item = pi.value
		from #t_prod t_prod
		inner join epacube.PRODUCT_IDENTIFICATION pi with (nolock) on t_prod.product_structure_fk = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110100

		update t_prod
		set alias_id = dv.value
		, alias_description = dv.DESCRIPTION
		from #t_prod t_prod
		inner join epacube.product_mult_type pmt with (nolock) on t_prod.product_structure_fk = pmt.PRODUCT_STRUCTURE_FK and t_prod.entity_structure_fk = pmt.ENTITY_STRUCTURE_FK and pmt.DATA_NAME_FK = 500019
																and cast(pmt.CREATE_TIMESTAMP as date) <= @LGprocDate
		inner join epacube.data_value dv with (nolock) on pmt.DATA_VALUE_FK = dv.DATA_VALUE_ID

		update t_prod
		Set parity_status = 'P'
		, ind_parity_status = 1
		, ind_parity = '1'
		, parity_child_product_structure_fk = pa_159903.CHILD_PRODUCT_STRUCTURE_FK
		from #t_prod t_prod
		inner join epacube.PRODUCT_ASSOCIATION pa_159903 on t_prod.product_structure_fk = pa_159903.PRODUCT_STRUCTURE_FK
													and t_prod.entity_structure_fk = pa_159903.ENTITY_STRUCTURE_FK
													and cast(pa_159903.CREATE_TIMESTAMP as date) <= @LGprocDate
		where pa_159903.DATA_NAME_FK = 159903

		update t_prod
		Set parity_status = 'C'
		, ind_parity_status = 2
		, ind_parity = '1'
		, parity_parent_product_structure_fk = pa_159903.PRODUCT_STRUCTURE_FK
		from #t_prod t_prod
		inner join epacube.PRODUCT_ASSOCIATION pa_159903 on t_prod.product_structure_fk = pa_159903.CHILD_PRODUCT_STRUCTURE_FK
													and t_prod.entity_structure_fk = pa_159903.ENTITY_STRUCTURE_FK
													and cast(pa_159903.CREATE_TIMESTAMP as date) <= @LGprocDate
		where pa_159903.DATA_NAME_FK = 159903

		update t_prod
		Set parity_status = null
		, ind_parity_status = 0
		, ind_parity = '0'
		from #t_prod t_prod
		where parity_status is null

		Update tp
		set PBV_Status = Case when @LG_Source = 0 then Null else PBV_Status end
		, ind_cost_change_item = Case when @LG_Source = 0 then 0 else ind_cost_change_item end
		from #t_prod tp

		update tp
		set pack = ps.pack
		, size_uom = ps.size_uom
		from #t_prod tp
		inner join (
					select pk.pack 'pack'
					, cast(cast(case when uc.uom_code = 'rwc' then uoc.quantity_multiple / 16 when uc.uom_code = 'rwl' then 1 else uoc.quantity_multiple end as numeric(18, 2)) as varchar(16)) + ' ' + uc.uom_code 'size_uom'
					, uc.uom_code 'uom'
					, uoc.product_structure_fk
					from epacube.epacube.product_uom_class uoc
					inner join epacube.epacube.uom_code uc on uoc.uom_code_fk = uc.uom_code_id and uoc.data_name_fk = 500025
					inner join 
								(
									select product_structure_fk, pack from (		
												select product_structure_fk, pack
												, dense_rank()over(partition by product_structure_fk, data_name_fk order by effective_date desc) pk_drank
												from epacube.product_uom_pack pup
												where 1 = 1
												and RECORD_STATUS_CR_FK = 1
												and product_structure_fk in (select PRODUCT_STRUCTURE_FK from #t_prod group by product_structure_fk)
												and effective_date <= @effective_date
									) pk1 where pk_drank = 1
								) pk on uoc.product_structure_fk = pk.product_structure_fk
					) ps on tp.product_structure_fk = ps.product_structure_fk
		where 1 = 1

	Update P
	Set PARITY_DISCOUNT_AMT = sz_500210.Parity_Discount_Amt 
	from #t_prod p
	inner join
		(
		Select
		ss.Product_structure_fk, ss.zone_segment_fk, cast(ss.attribute_number as numeric(18, 4)) 'Parity_Discount_Amt'
		, Dense_rank()over(partition by ss.Product_structure_fk, ss.zone_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank
		from epacube.segments_settings ss
		--inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.PRODUCT_ASSOCIATION_FK = pa.PRODUCT_ASSOCIATION_ID and cast(pa.CREATE_TIMESTAMP as date) <= @LGprocDate
		inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.PRODUCT_STRUCTURE_FK = pa.product_structure_fk 
																and ss.ZONE_SEGMENT_FK = pa.ENTITY_STRUCTURE_FK
																and pa.data_name_fk = 159903
																and cast(pa.CREATE_TIMESTAMP as date) <= @LGprocDate
		where ss.data_name_fk = 500210 
		and ss.attribute_number <> 0 
		and ss.RECORD_STATUS_CR_FK = 1
		and pa.record_status_cr_fk = 1
		and isnull(ss.Effective_Date, @effective_date) <= @effective_date
		and (ss.reinstate_inheritance_date > isnull(ss.Effective_Date, dateadd(d, -1, @effective_date)) or ss.reinstate_inheritance_date is null)
		) sz_500210 on p.PRODUCT_STRUCTURE_FK = sz_500210.product_structure_fk and p.ENTITY_STRUCTURE_FK = sz_500210.zone_segment_fk
		Where DRank = 1

	Update P
	Set Parity_Mult = sz_500212.Parity_Mult 
	from #t_prod P
	inner join
		(
		Select
		ss.Product_structure_fk, ss.zone_segment_fk, cast(ss.attribute_number as numeric(18, 4)) 'Parity_Mult'
		, Dense_rank()over(partition by ss.Product_structure_fk, ss.zone_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank
		from epacube.segments_settings ss 
		--inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.PRODUCT_ASSOCIATION_FK = pa.PRODUCT_ASSOCIATION_ID and cast(pa.CREATE_TIMESTAMP as date) <= @LGprocDate
		inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.PRODUCT_STRUCTURE_FK = pa.product_structure_fk 
																and ss.ZONE_SEGMENT_FK = pa.ENTITY_STRUCTURE_FK
																and pa.data_name_fk = 159903
																and cast(pa.CREATE_TIMESTAMP as date) <= @LGprocDate
		where ss.data_name_fk = 500212 
		and ss.attribute_number <> 0
		and ss.RECORD_STATUS_CR_FK = 1
		and pa.record_status_cr_fk = 1
		and isnull(ss.Effective_Date, @effective_date) <= @effective_date
		and (ss.reinstate_inheritance_date > isnull(ss.Effective_Date, dateadd(d, -1, @effective_date)) or ss.reinstate_inheritance_date is null)
		) sz_500212 on P.PRODUCT_STRUCTURE_FK = sz_500212.product_structure_fk and P.ENTITY_STRUCTURE_FK = sz_500212.zone_segment_fk
		Where DRank = 1

	Update p
	Set PARITY_DISCOUNT_PCT = sz_500211.Parity_Discount_Pct 
	from #t_prod P
	inner join
		(
		Select
		ss.Product_structure_fk, ss.zone_segment_fk, cast(ss.attribute_number as numeric(18, 4)) 'Parity_Discount_Pct'
		, Dense_rank()over(partition by ss.Product_structure_fk, ss.zone_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank
		from epacube.segments_settings ss
		--inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.PRODUCT_ASSOCIATION_FK = pa.PRODUCT_ASSOCIATION_ID and cast(pa.CREATE_TIMESTAMP as date) <= @LGprocDate
		inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.PRODUCT_STRUCTURE_FK = pa.product_structure_fk 
																and ss.ZONE_SEGMENT_FK = pa.ENTITY_STRUCTURE_FK
																and pa.data_name_fk = 159903
																and cast(pa.CREATE_TIMESTAMP as date) <= @LGprocDate
		where ss.data_name_fk = 500211 
		and ss.attribute_number <> 0
		and ss.RECORD_STATUS_CR_FK = 1
		and pa.record_status_cr_fk = 1
		and isnull(ss.Effective_Date, @effective_date) <= @effective_date
		and (ss.reinstate_inheritance_date > isnull(ss.Effective_Date, dateadd(d, -1, @effective_date)) or ss.reinstate_inheritance_date is null)
		) sz_500211 on p.PRODUCT_STRUCTURE_FK = sz_500211.product_structure_fk and p.ENTITY_STRUCTURE_FK = sz_500211.zone_segment_fk
		where DRank = 1

	Update P
	Set urm_target_margin = sz_502046.attribute_number --case when urm_target_margin is null then sz_502046.attribute_number else urm_target_margin end
	, tm_pct_cur = sz_502046.attribute_number
	from #t_prod P
	inner join 
		(select * from (
						Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
						, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
						from epacube.segments_settings ss with (nolock) 
						inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
						inner join #t_prod p on sp.product_structure_fk = p.product_structure_fk and ss.zone_segment_fk = p.entity_structure_fk
						where ss.data_name_fk = 502046
						and ss.Effective_Date <= @LGprocDate
						and (ss.reinstate_inheritance_date > @LGprocDate or ss.reinstate_inheritance_date is null)
						and ss.RECORD_STATUS_CR_FK = 1
						 and cast(SS.CREATE_TIMESTAMP as date) <= @LGprocDate
						) a where Drank = 1
		) sz_502046 on p.Product_structure_FK = sz_502046.product_structure_fk and p.entity_structure_fk = sz_502046.zone_segment_fk

--	not for use in getting current target margins for vendor cost changes in level gross results
	Update P
	Set tm_pct_cur = sz_502046.attribute_number
	from #t_prod P
	inner join 
		(select * from (
						Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
						, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
						from epacube.segments_settings ss with (nolock) 
						inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
						inner join #t_prod p on sp.product_structure_fk = p.product_structure_fk and ss.zone_segment_fk = p.entity_structure_fk
						where ss.data_name_fk = 502046
						and ss.Effective_Date <= @Yesterday
						and (ss.reinstate_inheritance_date > @Yesterday or ss.reinstate_inheritance_date is null)
						and ss.RECORD_STATUS_CR_FK = 1
						 and cast(SS.CREATE_TIMESTAMP as date) <= @Yesterday
						) a where Drank = 1
		) sz_502046 on p.Product_structure_FK = sz_502046.product_structure_fk and p.entity_structure_fk = sz_502046.zone_segment_fk
		where @Yesterday is not null

	If @LG_Source = 1
		Begin
			If Object_id('tempdb..#LGflags') is not null
			drop table #LGflags

			If Object_id('tempdb..#LGflags_upd') is not null
			drop table #LGflags_upd

			 Update P
			 Set lG_ACTIVE = isnull(sz_144862.ATTRIBUTE_Y_N, 'N')
			 from #t_prod P
				inner join 
					(select * from (
									Select ss.ATTRIBUTE_Y_N, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID
									, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank
									from epacube.segments_settings ss with (nolock) 
									where ss.data_name_fk = 144862
									and ss.zone_segment_fk is not null 
									and CUST_ENTITY_STRUCTURE_FK is null
									and ss.RECORD_STATUS_CR_FK = 1
									and ss.Effective_Date <= @effective_date
									 and cast(ss.CREATE_TIMESTAMP as date) <= @LGprocDate
									) a where Drank = 1
					) sz_144862 on sz_144862.zone_segment_fk = P.ENTITY_STRUCTURE_FK

			select * 
			into #LGflags
			from	(							
						Select distinct ss.ATTRIBUTE_Y_N 'lg_flag', sp.product_structure_fk, ss.zone_segment_fk
						, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk, ss.precedence order by ss.effective_date desc, ss.segments_settings_id desc) DRank
						from epacube.segments_settings ss with (nolock) 
						inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
						inner join #t_prod zc on sp.product_structure_fk = zc.PRODUCT_STRUCTURE_FK and ss.zone_segment_fk = zc.ENTITY_STRUCTURE_FK
						where ss.data_name_fk = 500015
						and ss.RECORD_STATUS_CR_FK = 1
						and ss.Effective_Date <= @effective_date
						and (ss.reinstate_inheritance_date > @EFFECTIVE_DATE or ss.reinstate_inheritance_date is null)
						and cast(ss.CREATE_TIMESTAMP as date) <= @LGprocDate
					) a where Drank = 1

			Create index idx_01 on #LGflags(zone_segment_fk, product_structure_fk, lg_flag)

			Select * 
			into #LGflags_upd
			from (
					select * 
					, DENSE_RANK()over(partition by product_structure_fk, zone_segment_fk order by lg_flag) DRank_lgf
					from #LGflags
				) a where DRank_lgf = 1

			create index idx_01 on 	#LGflags_upd(zone_segment_fk, product_structure_fk)
			 
			Update P
			Set LG_FLAG = lgf.lg_flag
			from #t_prod P
  			inner join #LGflags_upd lgf on lgf.zone_segment_fk = P.ENTITY_STRUCTURE_FK and lgf.PRODUCT_STRUCTURE_FK = p.product_structure_fk

		End

	Update P
	Set Price_Multiple = CurPrice.price_multiple
	, Price_Multiple_New = case @LG_Source when 0 then 1 else CurPrice.price_multiple end
	, Price_Cur = CurPrice.Price
	, Price_Effective_Date_Cur = CurPrice.Effective_Date
	from #t_prod P
	inner join 
		(Select pr.zone_entity_structure_FK, pr.product_structure_fk, pr.price_multiple, pr.Price, pr.Effective_Date
		, Dense_Rank()over(partition by pr.zone_entity_structure_FK, pr.product_structure_fk order by pr.effective_date desc, pr.Price) DRank_Eff
		from marginmgr.PRICESHEET_RETAILS PR 
		inner join #t_prod P on pr.product_structure_fk = P.Product_structure_FK and pr.zone_entity_structure_FK = P.Entity_Structure_FK
		where 1 = 1
		and pr.zone_entity_structure_FK is not null 
		and pr.effective_date < case when @LG_Source = 1 then  @effective_date else dateadd(d, 1, @effective_date) end
		group by pr.zone_entity_structure_FK, pr.product_structure_fk, pr.price_multiple, pr.Price, pr.Effective_Date
		) CurPrice on P.PRODUCT_STRUCTURE_FK = CurPrice.Product_Structure_FK and P.Entity_Structure_FK = CurPrice.zone_entity_structure_FK
	where 1 = 1
		and isnull(CurPrice.DRank_Eff, 1) = 1

	Update P
	Set [Use_LG_Price_Points] = isnull(PP.UsePPT, 'N')
	from #t_prod P
	inner join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, sp.PRODUCT_STRUCTURE_FK, ss.ZONE_SEGMENT_FK, ss.attribute_event_data 'UsePPT', ss.precedence, ss.effective_date, SEGMENTS_SETTINGS_ID
						, Dense_Rank()over(partition by ss.data_name_fk, ss.ZONE_SEGMENT_FK, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) 'DRank_PPTs'
						from epacube.segments_settings ss with (nolock)
						inner join epacube.segments_product sp with (nolock) on sp.PROD_SEGMENT_FK = ss.PROD_SEGMENT_FK
						inner join #t_prod p on sp.product_structure_fk = p.product_structure_fk and ss.ZONE_SEGMENT_FK = p.Entity_Structure_FK
						where 1 = 1
						and ss.ZONE_SEGMENT_FK is not null
						and ss.prod_segment_fk is not null
						and ss.cust_entity_structure_fk is null
						and ss.data_name_fk = 144876
						and ss.effective_date <= @effective_date
						and (ss.reinstate_inheritance_date > @EFFECTIVE_DATE or ss.reinstate_inheritance_date is null)
						 and cast(ss.CREATE_TIMESTAMP as date) <= @LGprocDate
						and ss.RECORD_STATUS_CR_FK = 1
						and ss.precedence is not null
					) a where DRank_PPTs = 1 
		) PP on P.ENTITY_STRUCTURE_FK = PP.ZONE_SEGMENT_FK and P.product_structure_fk = PP.PRODUCT_STRUCTURE_FK

	Update P
	Set [Use_LG_Price_Points] = isnull(PP.UsePPT, 'N')
	from #t_prod P
	inner join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, ss.ZONE_SEGMENT_FK, ss.attribute_event_data 'UsePPT', ss.effective_date, SEGMENTS_SETTINGS_ID
						, Dense_Rank()over(partition by ss.ZONE_SEGMENT_FK order by ss.effective_date desc, ss.segments_settings_id desc) 'DRank'
						from epacube.segments_settings ss with (nolock)
						where 1 = 1
						and ss.ZONE_SEGMENT_FK is not null
						and ss.prod_segment_fk is null
						and ss.cust_entity_structure_fk is null
						and ss.data_name_fk = 144876
						and ss.effective_date <= @effective_date
						and cast(ss.CREATE_TIMESTAMP as date) <= @LGprocDate
						and ss.RECORD_STATUS_CR_FK = 1
						) A where DRank = 1

		) PP on P.ENTITY_STRUCTURE_FK = PP.ZONE_SEGMENT_FK
	where 1 = 1
		and P.[Use_LG_Price_Points] is null

--get MinAmt at Zone Item Class level
	Update P
	Set MinAmt = isnull(PP.MinAmt, 0)
	from #t_prod P
	inner join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, sp.PRODUCT_STRUCTURE_FK, ss.ZONE_SEGMENT_FK, ss.attribute_number 'MinAmt', ss.precedence, ss.effective_date, SEGMENTS_SETTINGS_ID
						, Dense_Rank()over(partition by ss.data_name_fk, ss.ZONE_SEGMENT_FK, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) 'DRank_PPTs'
						from epacube.segments_settings ss with (nolock)
						inner join epacube.segments_product sp with (nolock) on sp.PROD_SEGMENT_FK = ss.PROD_SEGMENT_FK
						inner join #t_prod p on sp.product_structure_fk = p.product_structure_fk and ss.ZONE_SEGMENT_FK = p.entity_structure_fk
						where 1 = 1
						and ss.ZONE_SEGMENT_FK is not null
						and ss.prod_segment_fk is not null
						and ss.cust_entity_structure_fk is null
						and ss.data_name_fk = 144908
						and ss.effective_date <= @effective_date
						and cast(ss.CREATE_TIMESTAMP as date) <= @LGprocDate
						and (ss.reinstate_inheritance_date > @EFFECTIVE_DATE or ss.reinstate_inheritance_date is null)
						and ss.RECORD_STATUS_CR_FK = 1
						and ss.precedence is not null
					) a where DRank_PPTs = 1 
		) PP on P.ENTITY_STRUCTURE_FK = PP.ZONE_SEGMENT_FK and P.product_structure_fk = PP.PRODUCT_STRUCTURE_FK

	--get MinAmt at Zone level
	Update P
	Set MinAmt = isnull(PP.MinAmt, 0)
	from #t_prod P
	left join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, ss.ZONE_SEGMENT_FK, ss.attribute_number 'MinAmt', ss.effective_date, SEGMENTS_SETTINGS_ID
						, Dense_Rank()over(partition by ss.ZONE_SEGMENT_FK order by ss.effective_date desc, ss.segments_settings_id desc) 'DRank'
						from epacube.segments_settings ss with (nolock)
						where 1 = 1
						and ss.ZONE_SEGMENT_FK is not null
						and ss.prod_segment_fk is null
						and ss.cust_entity_structure_fk is null
						and ss.data_name_fk = 144908
						and cast(ss.CREATE_TIMESTAMP as date) <= @LGprocDate
						and ss.effective_date <= @effective_date
						and ss.RECORD_STATUS_CR_FK = 1
						) A where DRank = 1
		) PP on P.ENTITY_STRUCTURE_FK = PP.ZONE_SEGMENT_FK
	where 1 = 1
		and P.MinAmt is null

Update P
	Set MinAmt = 0
	from #t_prod P
	where 1 = 1
		and P.MinAmt is null

	Update P
	Set Case_Cost_Cur = Pbv_c.value
	, Unit_Cost_Cur = Pbv_c.unit_value
	, case_cost_new = case @LG_Source when 0 then Pbv_c.value else case_cost_new end
	, unit_cost_new = case @LG_Source when 0 then Pbv_c.unit_value else unit_cost_new end
	, pricesheet_basis_values_FK_cur = pbv_c.pricesheet_basis_values_id
	, cost_change_effective_date_cur = pbv_c.Effective_Date
	from #t_prod P
		inner join 
			(Select * from (
							Select pbv.value, pbv.unit_value, pbv.Effective_Date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, p.entity_structure_fk
							, dense_rank()over(partition by pbv.data_name_fk, p.entity_structure_fk, pbv.product_structure_fk order by pbv.effective_date desc, pbv.pricesheet_basis_values_id desc) DRank
							from marginmgr.pricesheet_basis_values pbv with (nolock)
							inner join #t_prod P on pbv.product_structure_fk = P.product_structure_fk 
							where pbv.data_name_fk = 503201 
							and pbv.cust_entity_structure_fk is null
							and PBV.Effective_Date < case when @LG_Source = 1 then  @effective_date else dateadd(d, 1, @effective_date) end
							and pbv.record_status_cr_fk = 1
							and cast(pbv.CREATE_TIMESTAMP as date) <= @LGprocDate
							) a where DRank = 1
			) Pbv_c on P.[Product_Structure_FK] = Pbv_c.Product_Structure_FK and P.entity_structure_fk = pbv_c.entity_structure_fk

	If @LG_Source = 1
	Begin
		Update P
		Set Case_Cost_Cur = Pbv_c.value
		, Unit_Cost_Cur = Cast(Pbv_c.value / P.Pack as Numeric(18, 6)) --Pbv_c.unit_value
		, pricesheet_basis_values_fk_cur = pbv_c.pricesheet_basis_values_id
		, cost_change_effective_date_cur = pbv_c.Effective_Date
		from #t_prod P
			inner join 
				(Select * from (
								Select pbv.value, pbv.unit_value, pbv.Effective_Date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, p.entity_structure_fk
								, dense_rank()over(partition by pbv.data_name_fk, p.entity_structure_fk, pbv.product_structure_fk order by pbv.effective_date desc, pbv.pricesheet_basis_values_id desc) DRank
								from marginmgr.pricesheet_basis_values pbv with (nolock)
								inner join #t_prod P on pbv.product_structure_fk = P.product_structure_fk 
								where pbv.data_name_fk = 503201 
								and pbv.cust_entity_structure_fk is null
								and PBV.Effective_Date < cast(p.cost_change_effective_date as date)
								and cast(pbv.CREATE_TIMESTAMP as date) <= @LGprocDate
								and pbv.record_status_cr_fk = 1
								) a where DRank = 1
				) Pbv_c on P.[Product_Structure_FK] = Pbv_c.Product_Structure_FK and P.entity_structure_fk = pbv_c.entity_structure_fk
	End



		update tp0
		set parity_alias_id = tp.parity_alias_id
		, parity_status = tp.parity_status
		, ind_parity_status = tp.ind_parity_status
		, PARITY_DISCOUNT_AMT = tp.PARITY_DISCOUNT_AMT
		, PARITY_MULT = tp.PARITY_MULT
		, PARITY_DISCOUNT_PCT = tp.PARITY_DISCOUNT_PCT
		from #t_prod tp0
		inner join 
			(select parents.product_structure_fk, parents.entity_structure_fk, parents.alias_id, children.alias_id 'parity_alias_id', parents.Parity_Status, parents.ind_parity_status, parents.PARITY_DISCOUNT_AMT, parents.PARITY_MULT, parents.PARITY_DISCOUNT_PCT
			from
				(
					select product_structure_fk, entity_structure_fk, parity_child_product_structure_fk , alias_id, parity_alias_id, Parity_Status, ind_parity_status, PARITY_DISCOUNT_AMT, PARITY_MULT, PARITY_DISCOUNT_PCT from (
					select dense_rank()over(partition by alias_id, entity_structure_fk order by t_prod_id) DRank
					, *
					from #t_prod tp1
					where tp1.alias_id is not null and isnull(tp1.parity_status, '') = 'p'
					) a where DRAnk = 1
				) parents
				inner join
				(
					select product_structure_fk, entity_structure_fk, parity_child_product_structure_fk , alias_id, parity_alias_id, Parity_Status, ind_parity_status, PARITY_DISCOUNT_AMT, PARITY_MULT, PARITY_DISCOUNT_PCT from (
					select dense_rank()over(partition by alias_id, entity_structure_fk order by t_prod_id) DRank
					, *
					from #t_prod tp1
					where tp1.alias_id is not null and isnull(tp1.parity_status, '') = 'C'
					) a where DRAnk = 1
				) children on parents.parity_child_product_structure_fk = children.product_structure_fk
						and parents.entity_structure_fk = children.entity_structure_fk
			) tp on tp0.alias_id = tp.alias_id
				and tp0.entity_structure_fk = tp.entity_structure_fk

		update tp0
		set parity_alias_id = tp.parity_alias_id
		, Parity_Status = tp.Parity_Status
		, ind_parity_status = tp.ind_parity_status
		, PARITY_DISCOUNT_AMT = tp.PARITY_DISCOUNT_AMT
		, PARITY_MULT = tp.PARITY_MULT
		, PARITY_DISCOUNT_PCT = tp.PARITY_DISCOUNT_PCT
		from #t_prod tp0
		inner join 
			(select parents.product_structure_fk, parents.entity_structure_fk, parents.alias_id 'parity_alias_id', children.alias_id, children.Parity_Status, children.ind_parity_status, parents.PARITY_DISCOUNT_AMT, parents.PARITY_MULT, parents.PARITY_DISCOUNT_PCT
			from
				(
					select product_structure_fk, entity_structure_fk, parity_child_product_structure_fk , alias_id, parity_alias_id, Parity_Status, ind_parity_status, PARITY_DISCOUNT_AMT, PARITY_MULT, PARITY_DISCOUNT_PCT from (
					select dense_rank()over(partition by alias_id, entity_structure_fk order by t_prod_id) DRank
					, *
					from #t_prod tp1
					where tp1.alias_id is not null and isnull(tp1.parity_status, '') = 'p'
					) a where DRAnk = 1
				) parents
				inner join
				(
					select product_structure_fk, entity_structure_fk, parity_child_product_structure_fk , alias_id, parity_alias_id, Parity_Status, ind_parity_status, PARITY_DISCOUNT_AMT, PARITY_MULT, PARITY_DISCOUNT_PCT from (
					select dense_rank()over(partition by alias_id, entity_structure_fk order by t_prod_id) DRank
					, *
					from #t_prod tp1
					where tp1.alias_id is not null and isnull(tp1.parity_status, '') = 'C'
					) a where DRAnk = 1
				) children on parents.parity_alias_id = children.alias_id
						and parents.entity_structure_fk = children.entity_structure_fk
			) tp on tp0.alias_id = tp.alias_id
				and tp0.entity_structure_fk = tp.entity_structure_fk

----Applying parity variables to children where children are in an alias and parent is not, and parent may not be related to all children
	If @LG_Source = 0
		update tp0
		set parity_alias_id = tp.parity_alias_id
		, Parity_Status = tp.Parity_Status
		, ind_parity_status = tp.ind_parity_status
		, PARITY_DISCOUNT_AMT = tp.PARITY_DISCOUNT_AMT
		, PARITY_MULT = tp.PARITY_MULT
		, PARITY_DISCOUNT_PCT = tp.PARITY_DISCOUNT_PCT
		from #t_prod tp0
		inner join 
			(select parents.product_structure_fk, parents.entity_structure_fk, parents.alias_id 'parity_alias_id', children.alias_id, children.Parity_Status, children.ind_parity_status, parents.PARITY_DISCOUNT_AMT, parents.PARITY_MULT, parents.PARITY_DISCOUNT_PCT
			from
				(
					select product_structure_fk, entity_structure_fk, parity_child_product_structure_fk , alias_id, parity_alias_id, Parity_Status, ind_parity_status, PARITY_DISCOUNT_AMT, PARITY_MULT, PARITY_DISCOUNT_PCT from (
					select dense_rank()over(partition by tp2.alias_id, tp2.entity_structure_fk order by tp2.t_prod_id) DRank
					, tp1.product_structure_fk, tp1.entity_structure_fk, tp2.product_structure_fk 'parity_child_product_structure_fk', tp2.alias_id, tp1.parity_alias_id, tp1.Parity_Status
					, tp1.ind_parity_status, tp1.PARITY_DISCOUNT_AMT, tp1.PARITY_MULT, tp1.PARITY_DISCOUNT_PCT
					from #t_prod tp1
					inner join #t_prod tp2 on tp1.entity_structure_fk = tp2.entity_structure_fk and tp1.product_structure_fk = tp2.parity_parent_product_structure_fk
					where tp2.alias_id is not null and isnull(tp1.parity_status, '') = 'p'
					) a where DRAnk = 1
				) parents
				inner join
				(
					select product_structure_fk, entity_structure_fk, parity_child_product_structure_fk , alias_id, parity_alias_id, Parity_Status, ind_parity_status, PARITY_DISCOUNT_AMT, PARITY_MULT, PARITY_DISCOUNT_PCT from (
					select dense_rank()over(partition by alias_id, entity_structure_fk order by t_prod_id) DRank
					, *
					from #t_prod tp1
					where tp1.alias_id is not null and isnull(tp1.parity_status, '') = 'C'
					) a where DRAnk = 1
				) children on parents.alias_id = children.alias_id
						and parents.entity_structure_fk = children.entity_structure_fk
			) tp on tp0.alias_id = tp.alias_id
				and tp0.entity_structure_fk = tp.entity_structure_fk
		else
		If @LG_Source = 1
		update tp0
		Set PARITY_DISCOUNT_AMT = tp.PARITY_DISCOUNT_AMT
		, PARITY_MULT = tp.PARITY_MULT
		, PARITY_DISCOUNT_PCT = tp.PARITY_DISCOUNT_PCT
		from #t_prod tp0
		inner join 
			(select parents.product_structure_fk, parents.entity_structure_fk, parents.alias_id 'parity_alias_id', children.alias_id, children.Parity_Status, children.ind_parity_status, parents.PARITY_DISCOUNT_AMT, parents.PARITY_MULT, parents.PARITY_DISCOUNT_PCT
			from
				(
					select product_structure_fk, entity_structure_fk, parity_child_product_structure_fk , alias_id, parity_alias_id, Parity_Status, ind_parity_status, PARITY_DISCOUNT_AMT, PARITY_MULT, PARITY_DISCOUNT_PCT from (
					select dense_rank()over(partition by tp2.alias_id, tp2.entity_structure_fk order by tp2.t_prod_id) DRank
					, tp1.product_structure_fk, tp1.entity_structure_fk, tp2.product_structure_fk 'parity_child_product_structure_fk', tp2.alias_id, tp1.parity_alias_id, tp1.Parity_Status
					, tp1.ind_parity_status, tp1.PARITY_DISCOUNT_AMT, tp1.PARITY_MULT, tp1.PARITY_DISCOUNT_PCT
					from #t_prod tp1
					inner join #t_prod tp2 on tp1.entity_structure_fk = tp2.entity_structure_fk and tp1.product_structure_fk = tp2.parity_parent_product_structure_fk
					where tp2.alias_id is not null and isnull(tp1.parity_status, '') = 'p'
					) a where DRAnk = 1
				) parents
				inner join
				(
					select product_structure_fk, entity_structure_fk, parity_child_product_structure_fk , alias_id, parity_alias_id, Parity_Status, ind_parity_status, PARITY_DISCOUNT_AMT, PARITY_MULT, PARITY_DISCOUNT_PCT from (
					select dense_rank()over(partition by alias_id, entity_structure_fk order by t_prod_id) DRank
					, *
					from #t_prod tp1
					where tp1.alias_id is not null and isnull(tp1.parity_status, '') = 'C'
					) a where DRAnk = 1
				) children on parents.alias_id = children.alias_id
						and parents.entity_structure_fk = children.entity_structure_fk
			) tp on tp0.alias_id = tp.alias_id
				and tp0.entity_structure_fk = tp.entity_structure_fk

		update tp2
		set parity_discount_pct = tp1.parity_discount_pct
		from #t_prod tp1
		inner join #t_prod tp2 on tp1.product_structure_fk = tp2.parity_parent_product_structure_fk
							and tp1.entity_structure_fk = tp2.entity_structure_fk
		where tp2.parity_discount_pct is null

		Update tp
		set PARITY_PARENT_ITEM = pi.value
		from #t_prod tp
		inner join epacube.product_identification pi with (nolock) on tp.parity_parent_product_structure_fk = pi.product_structure_fk and pi.data_name_fk = 110100

		Update tp
		set PARITY_CHILD_ITEM = pi.value
		from #t_prod tp
		inner join epacube.product_identification pi with (nolock) on tp.parity_child_product_structure_fk = pi.product_structure_fk and pi.data_name_fk = 110100

		Update t_prod
		set zone_id = tp0.zone_id
		, zone_name = tp0.zone_name
		, zone_sort = tp0.zone_sort
		from #t_prod t_prod
		inner join 
			(select tp.entity_structure_fk, tp.product_structure_fk, ei.value 'Zone_id', einz.value 'Zone_name'
			, cast(right('0' + cast(rank()over(order by left(einz.value, charindex(' ', einz.value) - 1)) as varchar(64)) , 2)
						+ case when left(einz.value, 3) = 'URM' then right('0' + ltrim(rtrim(right(einz.value, 2))), 2) else right('0' + cast(rank()over(order by einz.value desc) as varchar(64)), 2) end as bigint) 'zone_sort'

			from #t_prod tp
			inner join epacube.entity_identification ei with (nolock) on tp.entity_structure_fk = ei.ENTITY_STRUCTURE_FK 
																	and ei.ENTITY_DATA_NAME_FK = 151000 
																	and ei.DATA_NAME_FK = 151110
			left join epacube.ENTITY_IDENT_NONUNIQUE einz with (nolock) on ei.entity_structure_fk = einz.ENTITY_STRUCTURE_FK
																	and ei.ENTITY_DATA_NAME_FK = einz.ENTITY_DATA_NAME_FK
																	and einz.DATA_NAME_FK = 140200
		) tp0 on t_prod.entity_structure_fk = tp0.entity_structure_fk and t_prod.product_structure_fk = tp0.product_structure_fk

		Update tp
		set item_description = pd.description
		from #t_prod tp
		inner join epacube.PRODUCT_DESCRIPTION pd with (nolock) on tp.product_structure_fk = pd.PRODUCT_STRUCTURE_FK and pd.DATA_NAME_FK = 110401

		update tp
		set unit_cost_cur = cast(tp.case_cost_cur / ps.pack / ps.units as numeric(18, 6))
		from #t_prod tp
		inner join (
					select pk.pack 'pack'
					, cast(cast(case when uc.uom_code = 'rwc' then uoc.quantity_multiple / 16 when uc.uom_code = 'rwl' then 1 else uoc.quantity_multiple end as numeric(18, 3)) as varchar(16)) 'units'
					, uc.uom_code 'uom'
					, uoc.product_structure_fk
					from epacube.epacube.product_uom_class uoc
					inner join epacube.epacube.uom_code uc on uoc.uom_code_fk = uc.uom_code_id and uoc.data_name_fk = 500025
					inner join 
								(
									select pk1.product_structure_fk, pk1.pack 
									from (		
												select pup.product_structure_fk, pup.pack
												, dense_rank()over(partition by pup.product_structure_fk, pup.data_name_fk order by pup.effective_date desc) pk_drank
												from epacube.product_uom_pack pup
												inner join 
													(select tp1.product_structure_fk, tp1.cost_change_effective_date_cur from #t_prod tp1 where tp1.size_uom like '%RWC%' group by tp1.product_structure_fk, tp1.cost_change_effective_date_cur) tprods
													on pup.product_structure_fk = tprods.product_structure_fk
												where 1 = 1
												and pup.RECORD_STATUS_CR_FK = 1
												and pup.effective_date <= tprods.cost_change_effective_date_cur
										) pk1 where pk_drank = 1
								) pk on uoc.product_structure_fk = pk.product_structure_fk
					) ps on tp.product_structure_fk = ps.product_structure_fk
		where 1 = 1
		and ps.UOM  = 'RWC'

		update tp
		set unit_cost_new = cast(tp.case_cost_new / ps.pack/ ps.units as numeric(18, 6))
		from #t_prod tp
		inner join (
					select pk.pack 'pack'
					, cast(cast(case when uc.uom_code = 'rwc' then uoc.quantity_multiple / 16 when uc.uom_code = 'rwl' then 1 else uoc.quantity_multiple end as numeric(18, 3)) as varchar(16)) 'units'
					, uc.uom_code 'uom'
					, uoc.product_structure_fk
					from epacube.epacube.product_uom_class uoc
					inner join epacube.epacube.uom_code uc on uoc.uom_code_fk = uc.uom_code_id and uoc.data_name_fk = 500025
					inner join 
								(
									select pk1.product_structure_fk, pk1.pack 
									from (		
												select pup.product_structure_fk, pup.pack
												, dense_rank()over(partition by pup.product_structure_fk, pup.data_name_fk order by pup.effective_date desc) pk_drank
												from epacube.product_uom_pack pup
												inner join 
													(select tp1.product_structure_fk, tp1.cost_change_effective_date from #t_prod tp1 where tp1.size_uom like '%RWC%' group by tp1.product_structure_fk, tp1.cost_change_effective_date) tprods
													on pup.product_structure_fk = tprods.product_structure_fk
												where 1 = 1
												and pup.RECORD_STATUS_CR_FK = 1
												and pup.effective_date <= tprods.cost_change_effective_date
										) pk1 where pk_drank = 1
								) pk on uoc.product_structure_fk = pk.product_structure_fk
					) ps on tp.product_structure_fk = ps.product_structure_fk
		where 1 = 1
		and ps.UOM  = 'RWC'

		Update tp
		set ind_parity_status = 1
		from #t_prod tp
		where alias_id = (select top 1 alias_id from #t_prod where parity_status = 'P')

		Update tp
		set ind_parity_status = 2
		from #t_prod tp
		where alias_id = (select top 1 alias_id from #t_prod where parity_status = 'C')
