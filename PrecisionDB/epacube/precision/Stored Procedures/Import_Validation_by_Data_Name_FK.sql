﻿CREATE PROCEDURE [precision].[Import_Validation_by_Data_Name_FK]
as

--Batch 1 should refer to a fullly loaded database with all required data names in place

Declare @Batch1 int = 1 --isnull((Select top 1 Batch from [PRECISION].[IMPORT_VALIDATION_BY_DATA_NAME] where Batch < (Select top 1 Batch from [PRECISION].[IMPORT_VALIDATION_BY_DATA_NAME] order by batch desc)), 1)
Declare @Batch2 int = (Select top 1 Batch from [PRECISION].[IMPORT_VALIDATION_BY_DATA_NAME] order by batch desc)

Select P1.Table_Name, P1.Query_Date 'Older_Batch_Date', P1.import_timestamp, P1.data_name_fk, P1.DNL, case When isnull(P2.Batch, 0) = 0 then 'Not Present This Import' else Null end 'Import_Status'
, P2.Import_Timestamp 'New_Batch_Date', P1.Batch 'Older_Batch', P1.Records 'Older Batch Records', P2.BATCH 'New_Batch', P2.Records 'New Batch Records'
, Case when p1.records not between p2.records * 0.75 and p2.records / 0.75 then '*' else null end 'Large_Delta'
from [PRECISION].[IMPORT_VALIDATION_BY_DATA_NAME] P1
left join [PRECISION].[IMPORT_VALIDATION_BY_DATA_NAME] P2
       on P1.table_name = P2.table_name
       and P1.data_name_fk = P2.Data_Name_fk
Where 1 = 1
and P2.Batch = @Batch2
and P1.Batch = @Batch1
--and cast(P2.Import_Timestamp as date) = cast(getdate() as date)



