﻿

---- =============================================
---- Author:		Gary Stone
---- Create date: 2018-06-15
---- Description:	Proceses cost changes into scheduled retail price changes - zone
---- =============================================
CREATE PROCEDURE [precision].[RETAIL_PRICE_ITEMS_SUBGROUP_ZONE]
	@zone_segment_fk varchar(max) = ''
	, @price_change_effective_date date = null
	, @subgroup_data_value_fk bigint = null
	, @user varchar(96) = ''
WITH RECOMPILE

AS

	SET NOCOUNT ON;

	--Declare @zone_segment_fk varchar(Max) = '113246,113237,113247,113230,113231,113232,113233,113234,113239'
	--Declare @price_change_effective_date date = '2020-01-17'
	--Declare @subgroup_data_value_fk bigint = 1014720270 --(select data_value_id from epacube.data_value where data_name_fk = 501045 and value = '310012') --1014719916

	if object_id('tempdb..#Zones') is not null
	begin
		drop table #Zones
	end

	create table #Zones(zone_entity_structure_fk bigint primary key)

	insert into #Zones
	select listitem 
	from utilities.SplitString(@zone_segment_fk, ',')

	if @price_change_effective_date is null
	begin	
		set @price_change_effective_date = cast(getdate() + 6 - datepart(dw, getdate()) as date)
	end

	if object_id('tempdb..#Price_Retail_Changes') is not null
	begin
		drop table #Price_Retail_Changes
	end

	select srp_adjusted
		, price_new
		, level_gross
		, product_structure_fk
		, zone_entity_structure_fk
		, unit_cost_cur
		, unit_cost_new
		, price_cur
	into #Price_Retail_Changes
	from (
		select prc1.srp_adjusted
			, prc1.price_new
			, prc1.level_gross
			, prc1.product_structure_fk
			, prc1.zone_entity_structure_fk
			, round(prc1.unit_cost_cur, 2) unit_cost_cur
			, round(prc1.unit_cost_new, 2) unit_cost_new
			, prc1.price_cur
			, dense_rank()over(partition by prc1.product_structure_fk, prc1.zone_entity_structure_fk order by prc1.level_gross, isnull(prc1.srp_adjusted, 0) desc, isnull(prc1.price_new, 0) desc, prc1.pricesheet_retail_changes_id) drank
		from marginmgr.pricesheet_retail_changes prc1
			inner join #Zones z 
				on prc1.zone_entity_structure_fk = z.zone_entity_Structure_FK
		where prc1.SubGroup_Data_Value_FK = @subgroup_data_value_fk
			and prc1.price_change_effective_date = @price_change_effective_date) d 
	where drank = 1
	
	create clustered index pk_price_retail_changes 
	on #Price_Retail_Changes (product_structure_fk, zone_entity_structure_fk)

	if object_id('tempdb..#Current_Price') is not null
	begin
		drop table #Current_Price
	end

	select Price
		, zone_entity_structure_fk
		, product_structure_fk
		, effective_date
	into #Current_Price
	from (
		select cp.price
			, cp.zone_entity_structure_fk
			, cp.product_structure_fk
			, cp.effective_date
			, DENSE_RANK()over(partition by cp.zone_entity_structure_fk, cp.product_structure_fk order by cp.effective_date desc, cp.create_timestamp desc, cp.pricesheet_retails_id desc) drank
		from marginmgr.PRICESHEET_RETAILS cp
			inner join #zones cpz 
				on cp.zone_entity_structure_fk = cpz.zone_entity_structure_fk
			inner join epacube.segments_product sp
				on cp.product_structure_fk = sp.product_structure_fk 
					and sp.data_name_fk = 501045 
					and sp.prod_segment_fk = @subgroup_data_value_fk
		where effective_date <= @price_change_effective_date) a 
	where drank = 1

	create clustered index pk_current_price 
	on #Current_Price (zone_entity_structure_fk, product_structure_fk)

	if object_id('tempdb..#Segments_Zones_500015') is not null
	begin
		drop table #Segments_Zones_500015
	end

	select ATTRIBUTE_Y_N
		, product_structure_fk
		, zone_segment_fk
	into #Segments_Zones_500015
	from (
		select distinct ss.ATTRIBUTE_Y_N
			, sp.product_structure_fk
			, ss.zone_segment_fk
			, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.attribute_y_n, ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) drank
		from epacube.segments_settings ss
			inner join epacube.segments_product sp
				on ss.prod_segment_fk = sp.prod_segment_fk 
					and ss.prod_segment_data_name_fk = sp.data_name_fk
			inner join #Zones z 
				on ss.zone_segment_fk = z.zone_entity_structure_fk
			inner join epacube.segments_product sp_501045
				on sp.product_structure_fk = sp_501045.product_structure_fk 
					and sp_501045.data_name_fk = 501045 
					and sp_501045.prod_segment_fk = @subgroup_data_value_fk
		where ss.data_name_fk = 500015
			and ss.record_status_cr_fk = 1) a 
	where drank = 1
	order by product_structure_fk, zone_segment_fk

	create clustered index pk_segments_zones_500015 
	on #Segments_Zones_500015 (product_structure_fk, zone_segment_fk)

	if object_id('tempdb..#Pack') is not null
	begin
		drop table #Pack
	end

	select product_structure_fk
		, pack
	into #Pack
	from (		
		select product_structure_fk
			, pack
			, dense_rank()over(partition by product_structure_fk, data_name_fk order by effective_date desc) pk_drank
		from epacube.product_uom_pack pup
		where record_status_cr_fk = 1
			and product_structure_fk in (
				select product_structure_fk
				from epacube.segments_product 
				where prod_segment_fk = @subgroup_data_value_fk 
				group by product_structure_fk)
			and effective_date <= @price_change_effective_date) a
	where pk_drank = 1

	create clustered index pk_pack 
	on #Pack (product_structure_fk)

	if object_id('tempdb..#Product_Structure') is not null
	begin
		drop table #Product_Structure
	end

	select pk.pack
		, cast(case when uc.uom_code = 'rwc' then uoc.quantity_multiple / 16 when uc.uom_code = 'rwl' then 1 else uoc.quantity_multiple end as numeric(18, 2)) size
		, uc.uom_code uom
		, uoc.product_structure_fk
	into #Product_Structure
	from epacube.product_uom_class uoc
		inner join epacube.uom_code uc 
			on uoc.uom_code_fk = uc.uom_code_id 
				and uoc.data_name_fk = 500025
				inner join #Pack pk 
					on uoc.product_structure_fk = pk.product_structure_fk

	create clustered index pk_product_structure 
	on #Product_Structure (product_structure_fk)

	declare @current_date as date = getdate()

	select pi.product_structure_fk
		, dv_501045.data_value_id
		, cast(cast(dv_501045.value as varchar(16)) + ': ' + cast(dv_501045.[description] as varchar(64)) as varchar(82)) SubGroup
		, cast(eiz.value as int) Zone
		, pi.value item
		, pd.description
		, cast(left(pi.value, Len(pi.value) - 1) as bigint) item_sort
		, dv_alias.value 'alias_id'
		, ps.pack
		, ps.size
		, ps.uom
		, isnull(prc.unit_cost_cur, (
			select top 1 Cast(unit_value as numeric(18, 2))
			from marginmgr.pricesheet_basis_values pbv
			where pbv.data_name_fk = 503201 
				and cust_entity_structure_fk is null
				and pbv.product_structure_fk = pi.product_structure_fk
				and pbv.effective_date < @current_date
				and pbv.record_status_cr_fk = 1
				and pbv.unit_value is not null
			order by pbv.effective_date desc)) Cur_Cost
		, isnull(prc.price_cur, cur_price.price) Cur_Price
		, cast(case when isnull(isnull(prc.price_cur, cur_price.price), 0) = 0 then null else 
			(isnull(prc.price_cur, cur_price.price) - 
				isnull(prc.unit_cost_cur, (
					select top 1 Cast(unit_value as numeric(18, 2))
					from marginmgr.pricesheet_basis_values pbv
					where pbv.data_name_fk = 503201 
						and cust_entity_structure_fk is null
						and pbv.product_structure_fk = pi.product_structure_fk
						and pbv.effective_date < @current_date
						and pbv.record_status_cr_fk = 1
						and pbv.unit_value is not null
					order by pbv.effective_date desc))) / isnull(prc.price_cur, cur_price.price) end as numeric(18, 4)) gm_pct
		, isnull(prc.unit_cost_new, (
			select top 1 Cast(unit_value as numeric(18, 2))
			from marginmgr.pricesheet_basis_values pbv
			where pbv.data_name_fk = 503201 
				and cust_entity_structure_fk is null
				and pbv.product_structure_fk = pi.product_structure_fk
				and pbv.effective_date >= @current_date
				and pbv.record_status_cr_fk = 1
				and pbv.unit_value is not null
			order by pbv.effective_date desc)) New_Cost
		, isnull(prc.srp_adjusted, prc.price_new) New_Price
		, sz_500015.attribute_y_n Level_Gross
		, null tm_pct
		, case when pa_500012.product_attribute_id is not null then '{"renderer":{"color":"red","text-decoration":"underline"}}' else null end ind_discontinued
	from epacube.product_identification pi
		inner join epacube.segments_product sp
			on pi.product_structure_fk = sp.product_structure_fk 
				and pi.data_name_fk = 110100 and sp.data_name_fk = 501045
		inner join epacube.data_value dv_501045
			on sp.PROD_SEGMENT_FK = dv_501045.data_value_id 
				and sp.data_name_fk = dv_501045.data_name_fk
		inner join epacube.product_association pa_159450
			on sp.product_structure_fk = pa_159450.product_structure_fk
				and pa_159450.data_name_fk = 159450
		inner join #Zones z 
			on pa_159450.entity_structure_fk = z.zone_entity_structure_fk
		inner join epacube.entity_identification eiz
			on pa_159450.entity_structure_fk = eiz.entity_structure_fk
				and eiz.entity_data_name_fk = 151000 and eiz.data_name_fk = 151110
		left join epacube.product_description pd
			on pi.product_structure_fk = pd.product_structure_fk 
				and pd.data_name_fk = 110401
		left join epacube.product_attribute pa_500012
			on pi.product_structure_fk = pa_500012.product_structure_fk
				and pa_500012.data_name_fk = 500012
		left join #Price_Retail_Changes prc 
			on pi.product_structure_fk = prc.product_structure_fk
				and eiz.entity_structure_fk = prc.zone_entity_structure_fk
		left join #Current_Price Cur_Price 
			on z.zone_entity_structure_fk = Cur_Price.zone_entity_structure_fk
				and pi.product_structure_fk = Cur_Price.product_structure_fk
  		left join #Segments_Zones_500015 sz_500015 
			on pi.product_structure_fk = sz_500015.product_structure_fk 
				and sz_500015.zone_segment_fk = z.zone_entity_structure_fk
		inner join #Product_Structure ps
			on pi.product_structure_fk = ps.product_structure_fk
		left join epacube.PRODUCT_MULT_TYPE pmt on pi.product_structure_fk = pmt.product_structure_fk and eiz.ENTITY_STRUCTURE_FK = pmt.ENTITY_STRUCTURE_FK and pmt.DATA_NAME_FK = 500019
		left join epacube.data_value dv_alias on pmt.DATA_VALUE_FK = dv_alias.DATA_VALUE_ID
	where dv_501045.data_value_id = @subgroup_data_value_fk
	order by Zone desc, item_sort
