﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [precision].[export_preparation_zone_changes_daily] @price_change_effective_date date = null
AS
	SET NOCOUNT ON;

	If isnull(@price_change_effective_date, '') = ''
	Set @price_change_effective_date = cast(getdate() as date)

	If object_id('tempdb..#TMs') is not null
	drop table #TMs

	If object_id('tempdb..#Data_To_Load') is not null
	drop table #Data_To_Load

/*
--Update Price and/or Target Margin that missed yesterday's cutoff dates to today's effective date, and export
		update r
		set effective_date = @price_change_effective_date
		, process_source = isnull(r.process_source, '') + '-MISSED YESTERDAY CUTOFF'
		from marginmgr.pricesheet_retails r
			inner join epacube.PRODUCT_IDENTIFICATION pi with (nolock) on pi.product_structure_fk = r.product_structure_fk and pi.data_name_fk = 110103
			inner join epacube.entity_identification eiz with (nolock) on eiz.entity_structure_fk = r.ZONE_ENTITY_STRUCTURE_FK and eiz.data_name_fk = 151110
			inner join epacube.entity_attribute ea with (nolock) on ea.entity_structure_fk = eiz.entity_structure_fk and ea.data_name_fk = 502060
			left join EXPORT.RETAIL_CHANGE_HISTORY rch with (nolock) on r.PRICESHEET_RETAILS_ID = rch.TABLE_ID and rch.process = 'Price Changes' and rch.TABLE_NAME = 'MARGINMGR.PRICESHEET_RETAILS'
			where 1 = 1
			and r.data_name_fk = 503301
			and isnull(r.price, 0) <> 0
			and cast(r.effective_date as date) = dateadd(d, -1, @price_change_effective_date)
			and rch.RETAIL_CHANGE_HISTORY_ID is null
			and ea.attribute_event_data in ('H') 
			and r.data_source not like 'urm%'
			and r.data_source <> 'DAILY UPDATE'
			and r.PROCESS_SOURCE not like '%-MISSED YESTERDAY CUTOFF%'

		update ss
		set effective_date = @price_change_effective_date
		, process_source = isnull(ss.PROCESS_SOURCE, '') + '-MISSED YESTERDAY CUTOFF'
		from epacube.segments_settings ss with (nolock) 
			inner join epacube.entity_identification eiz with (nolock) on ss.ZONE_SEGMENT_FK = eiz.ENTITY_STRUCTURE_FK and eiz.ENTITY_DATA_NAME_FK = 151000
			inner join epacube.entity_attribute ea with (nolock) on ea.entity_structure_fk = eiz.entity_structure_fk and ea.data_name_fk = 502060
			Left Join EXPORT.RETAIL_CHANGE_HISTORY rch with (nolock) on ss.SEGMENTS_SETTINGS_ID = rch.table_id and rch.[table_name] = 'EPACUBE.SEGMENTS_SETTINGS' and rch.process = 'Margin Changes'
			where ss.data_name_fk = 502046
			and ss.RECORD_STATUS_CR_FK = 1
			and ss.ATTRIBUTE_NUMBER is not null
			and ss.Effective_Date = dateadd(d, -1, @price_change_effective_date)
			and ea.attribute_event_data in ('H') 
			and ss.DATA_SOURCE in ('USER UPDATE', 'LEVEL GROSS PROCESSING')
			and ss.data_source not like 'urm%'
			and ss.data_source <> 'DAILY UPDATE'
			and ss.PROCESS_SOURCE not like '%-MISSED YESTERDAY CUTOFF%'
			and rch.RETAIL_CHANGE_HISTORY_ID is null
*/
	
	Create table #Data_To_Load(Table_Name varchar(64) Null, Table_ID bigint null, Process varchar(16) Null, Customer_ID int null, zone_id varchar(64) Null, Zone_Type varchar(16) null, item_surrogate varchar(64) null
	, Effective_Date date null, Price_Multiple int null, price numeric(18, 2) null, Item_Level varchar(2) null, item_class varchar(64) null, Level_Gross_Flag varchar(1) null, Target_Margin Numeric(18, 4) null
	, Price_Type int null, product_structure_fk bigint null, zone_entity_structure_fk bigint null, DRank_Exp bigint null, update_timestamp datetime null, data_value_fk bigint null, Data_To_Load_ID bigint identity(1, 1) not null)

----Collect Price Changes
	Insert into #Data_To_Load
	(Table_Name, Table_ID, Process, Customer_ID, zone_id, Zone_Type, item_surrogate, Effective_Date, Price_Multiple, price, Item_Level, item_class, Level_Gross_Flag, Target_Margin, Price_Type, product_structure_fk, zone_entity_structure_fk
	, DRank_Exp, update_timestamp)

	Select TABLE_NAME, Table_ID, Process, Customer_ID, Zone_ID, Zone_Type, Item_Surrogate, Price_Eff_Date
	, case item_level when 60 then isnull(Price_Multiple, 1) else Price_Multiple end 'Price_Multiple'
	, Price, Item_level, Item_class, Level_Gross_flag, Target_Margin
	, Case when item_level = 60 then isnull(Price_Type, 1) else Price_Type end 'Price_Type'
	, product_structure_fk, zone_entity_structure_fk
	, Dense_Rank()over(partition by zone_id, item_class, item_level order by case table_name when 'MARGINMGR.PRICESHEET_RETAILS' then 1 else 2 end) DRank_Exp
	, update_timestamp
	from (
			Select 'MARGINMGR.PRICESHEET_RETAILS' 'TABLE_NAME', PRICESHEET_RETAILS_ID 'Table_ID', 'Price Changes' 'Process', Customer_ID, Zone_ID, Zone_Type, Item_Surrogate, Price_Eff_Date, Price_Multiple, Price, Item_level, Item_class
			, Level_Gross_flag, Target_Margin, Price_Type, Data_Source, product_structure_fk, zone_entity_structure_fk, update_timestamp
			from 
			(
			select distinct NULL CUSTOMER_ID, eiz.value ZONE_ID, ea.attribute_event_data ZONE_TYPE, pi.value ITEM_SURROGATE, r.Effective_Date Price_eff_date, isnull(r.PRICE_MULTIPLE, 1) 'PRICE_MULTIPLE'
			, r.Price, '60' ITEM_LEVEL, pi.value ITEM_CLASS
			, case r.PRICE_MGMT_TYPE_CR_FK when 650 then 'Y' else 'N' end 'Level_Gross_Flag'
			, cast(r.TARGET_MARGIN as numeric(18, 4)) TARGET_MARGIN, R.Price_Type
			, Dense_Rank()over(partition by eiz.value, pi.value, r.effective_date order by pricesheet_retail_changes_fk desc) DRank 
			, pricesheet_retails_id
			, r.data_source
			, r.product_structure_fk, r.zone_entity_structure_fk
			, (select top 1 update_timestamp from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = r.PRICESHEET_RETAIL_CHANGES_FK)  'update_timestamp'
			from marginmgr.pricesheet_retails r with (nolock)
			inner join epacube.PRODUCT_IDENTIFICATION pi with (nolock) on pi.product_structure_fk = r.product_structure_fk and pi.data_name_fk = 110103
			inner join epacube.entity_identification eiz with (nolock) on eiz.entity_structure_fk = r.ZONE_ENTITY_STRUCTURE_FK and eiz.data_name_fk = 151110
			inner join epacube.entity_attribute ea with (nolock) on ea.entity_structure_fk = eiz.entity_structure_fk and ea.data_name_fk = 502060
			where 1 = 1
			and r.data_name_fk = 503301
			and isnull(r.price, 0) <> 0
			and cast(r.effective_date as date) = @price_change_effective_date
			and ea.attribute_event_data in ('H') 
			and r.data_source not like 'urm%'
			) A where DRank = 1
	) B

	create index idx_01 on #Data_To_Load(product_structure_fk, zone_entity_structure_fk)

	Update DTL
	Set Target_Margin = cast(sz_502046.ATTRIBUTE_NUMBER as Numeric(18, 4))
	from #Data_To_Load DTL
	inner join 
		(select * from (
					Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
					, dense_rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
					from epacube.segments_settings ss with (nolock) 
					inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
					inner join #Data_To_Load p on sp.product_structure_fk = p.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = p.zone_entity_structure_fk
					and (ss.reinstate_inheritance_date > @price_change_effective_date or ss.reinstate_inheritance_date is null)
					where ss.data_name_fk = 502046
					and ss.Effective_Date <= cast(getdate() as date)
					and ss.RECORD_STATUS_CR_FK = 1
					) a where Drank = 1
		) sz_502046 on DTL.Product_structure_FK = sz_502046.product_structure_fk and DTL.zone_entity_structure_fk = sz_502046.ZONE_ENTITY_STRUCTURE_FK
	--where DTL.Target_Margin is null

----Collect Margin Changes not already in Price Changes OR Margin Change TM created after Price Change TM created
	Select 'EPACUBE.SEGMENTS_SETTINGS' 'TABLE_NAME', SEGMENTS_SETTINGS_ID 'Table_ID', 'Margin Changes' 'Process', NULL CUSTOMER_ID, ZONE_ID, ZONE_TYPE, ITEM_SURROGATE, Effective_date, Null 'PRICE_MULTIPLE', Null 'Price'
	, ITEM_LEVEL, ITEM_CLASS
	, a.LG_Flag 'Level_Gross_Flag'
	, TARGET_MARGIN
	, Null 'Price_Type'
	, Data_Source
	, product_structure_fk
	, zone_segment_fk 'zone_entity_structure_fk'
	, data_value_fk
	into #TMs
	from (
			Select ss.ATTRIBUTE_NUMBER 'TARGET_MARGIN', eiz.value 'ZONE_ID', ea.attribute_event_data 'ZONE_TYPE'
			--, Cast(cast(ss.effective_date as datetime) + 6 - DATEPART(DW, ss.effective_date) as date) 'Price_Eff_Date'
			, ss.effective_date
			, isnull(dv.display_seq, 60) 'ITEM_LEVEL', isnull(dv.value, pi.value) 'ITEM_CLASS', pi.value 'ITEM_SURROGATE', dv.data_value_id, eiz.ENTITY_STRUCTURE_FK 'zone_segment_fk', ss.PROD_SEGMENT_DATA_NAME_FK
			, dense_rank()over(partition by ss.prod_segment_data_name_fk, ss.Prod_Segment_FK, ss.zone_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank
			, ss.segments_settings_id, pi.product_structure_fk, ss.CREATE_TIMESTAMP, ss.data_value_fk, ss.UPDATE_TIMESTAMP
			, (select ATTRIBUTE_Y_N from (
				Select distinct ss2.ATTRIBUTE_Y_N, ss2.PROD_SEGMENT_FK, ss2.ZONE_SEGMENT_FK
							, dense_rank()over(partition by ss2.ZONE_SEGMENT_FK, sp2.product_structure_fk order by ss2.precedence desc, ss2.attribute_y_n, ss2.effective_date desc, ss2.segments_settings_id desc) DRank
				from epacube.segments_settings ss1
				inner join epacube.segments_product sp with (nolock) on ss1.PROD_SEGMENT_DATA_NAME_FK = sp.data_name_fk and ss1.PROD_SEGMENT_FK = sp.PROD_SEGMENT_FK
				inner join epacube.segments_product sp2 with (nolock) on sp.product_structure_fk = sp2.product_structure_fk
				inner join epacube.segments_settings ss2 with (nolock) on sp2.prod_segment_fk = ss2.prod_segment_fk and ss2.PROD_SEGMENT_DATA_NAME_FK = sp2.data_name_fk and ss2.data_name_fk = 500015
																		and ss1.ZONE_SEGMENT_FK = ss2.ZONE_SEGMENT_FK
				where sp.PROD_SEGMENT_FK = isnull(dv.DATA_VALUE_ID, pi.PRODUCT_STRUCTURE_FK) and ss1.ZONE_SEGMENT_FK = ss.ZONE_SEGMENT_FK
				) a where Drank = 1) 'LG_Flag'
			, ss.data_source
			from epacube.segments_settings ss with (nolock) 
			inner join epacube.entity_identification eiz with (nolock) on ss.ZONE_SEGMENT_FK = eiz.ENTITY_STRUCTURE_FK and eiz.ENTITY_DATA_NAME_FK = 151000
			inner join epacube.entity_attribute ea with (nolock) on ea.entity_structure_fk = eiz.entity_structure_fk and ea.data_name_fk = 502060
			left join epacube.data_value dv with (nolock) on ss.PROD_SEGMENT_FK = dv.data_value_id
			left join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
			left join epacube.product_identification pi with (nolock) on ss.PROD_SEGMENT_DATA_NAME_FK = pi.data_name_fk and ss.PROD_SEGMENT_FK = pi.PRODUCT_STRUCTURE_FK 
			Left Join EXPORT.RETAIL_CHANGE_HISTORY rch with (nolock) on ss.SEGMENTS_SETTINGS_ID = rch.table_id and rch.[table_name] = 'EPACUBE.SEGMENTS_SETTINGS' and rch.process = 'Margin Changes'
			left join #Data_To_Load x on ss.PRODUCT_STRUCTURE_FK = x.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = x.ZONE_ENTITY_STRUCTURE_FK
			left join #Data_To_Load xTM on ss.PRODUCT_STRUCTURE_FK = xTM.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = xTM.ZONE_ENTITY_STRUCTURE_FK
			where ss.data_name_fk = 502046
			and ss.RECORD_STATUS_CR_FK = 1
			and ss.ATTRIBUTE_NUMBER is not null
			and ss.Effective_Date = @price_change_effective_date
			and (ss.reinstate_inheritance_date > @price_change_effective_date or ss.reinstate_inheritance_date is null)
			and ea.attribute_event_data in ('H') 
			and 
				(
					x.table_id is null
					or
					(isnull(ss.UPDATE_TIMESTAMP, ss.create_timestamp) is not null
						and xTM.update_timestamp is not null
						and cast(isnull(ss.UPDATE_TIMESTAMP, ss.create_timestamp) as date) > cast(xTM.update_timestamp as date)
					)
				)
			and ss.DATA_SOURCE in ('USER UPDATE', 'LEVEL GROSS PROCESSING')
			and ss.data_source not like 'urm%'
		) a where Drank = 1

		create index idx_01 on #TMs(product_structure_fk, zone_entity_structure_fk)


	Insert into #Data_To_Load
	([Table_Name], Table_ID, Process, [Zone_ID], Zone_Type, Item_Surrogate, Effective_Date, Price_Multiple, Price, Item_Level, Item_Class, Level_Gross_Flag, Target_Margin, Price_Type, product_structure_fk, zone_entity_structure_fk, data_value_fk)
	Select TMS.[Table_Name], TMS.Table_ID, TMS.Process, TMS.[Zone_ID], TMS.Zone_Type, TMS.Item_Surrogate, TMS.Effective_date, TMS.Price_Multiple, TMS.Price, TMS.Item_Level, TMS.Item_Class, TMS.Level_Gross_Flag, TMS.Target_Margin, TMS.Price_Type
	, TMS.product_structure_fk, TMS.zone_entity_structure_fk, TMS.data_value_fk
	from #TMs TMS
	left join #Data_To_Load DTL on DTL.PRODUCT_STRUCTURE_FK = tms.PRODUCT_STRUCTURE_FK and DTL.ZONE_ENTITY_STRUCTURE_FK = tms.zone_entity_structure_fk
		where 1 = 1
		and DTL.Data_To_Load_ID is null

----Collect item level records to send back to RIMS
	Insert into #Data_To_Load
	([Table_Name], Table_ID, Process, [Zone_ID], Zone_Type, Item_Surrogate, Effective_Date, Price_Multiple, Price, Item_Level, Item_Class, Level_Gross_Flag, Target_Margin, Price_Type, product_structure_fk, zone_entity_structure_fk)
	Select
	'PRECISION.STAGE_FOR_PROCESSING' 'Table_Name', sfp.STAGE_FOR_PROCESSING_ID 'table_id', sfp.Processing_Type
	, ei.value 'zone_id', dv.value 'zone_type', pi.value 'item_surrogate', @price_change_effective_date 'Price_Effective_Date'
	, Null 'price_multiple', Null 'Price', 60 'item_level', pi.value 'item_class'
	, sz_500015.ATTRIBUTE_Y_N 'LG_Flag'
	, sfp.value 'target_margin', Null 'price_type', sfp.product_structure_fk, sfp.entity_structure_fk
	from precision.Stage_For_Processing sfp with (nolock)
	inner join epacube.entity_identification ei with (nolock) on sfp.entity_structure_fk = ei.ENTITY_STRUCTURE_FK and ei.ENTITY_DATA_NAME_FK = 151000
	inner join epacube.ENTITY_ATTRIBUTE ea with (nolock) on ei.ENTITY_STRUCTURE_FK = ea.ENTITY_STRUCTURE_FK and ea.DATA_NAME_FK = 502060
	inner join epacube.data_value dv with (nolock) on ea.DATA_VALUE_FK = dv.data_value_id
	inner join epacube.product_identification pi with (nolock) on sfp.product_structure_fk = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110103
	left join
		(select * 
			from (
					Select ss.ATTRIBUTE_Y_N, sp.product_structure_fk, ss.zone_segment_fk entity_structure_fk
					, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.attribute_y_n, ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
					from epacube.segments_settings ss with (nolock) 
					inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
					inner join precision.Stage_For_Processing sfp1 on sp.product_structure_fk = sfp1.PRODUCT_STRUCTURE_FK and ss.zone_segment_fk = sfp1.ENTITY_STRUCTURE_FK and sfp1.tm_change_effective_date = @price_change_effective_date
					where ss.data_name_fk = 500015
					and ss.RECORD_STATUS_CR_FK = 1
					and ss.Effective_Date <= @price_change_effective_date
					) a 
			where Drank = 1
		) sz_500015 on sfp.product_structure_fk = sz_500015.product_structure_fk and sfp.entity_structure_fk = sz_500015.entity_structure_fk
	left join #data_to_load dtl on sfp.product_structure_fk = dtl.product_structure_fk and sfp.entity_structure_fk = dtl.zone_entity_structure_fk
	where 1 = 1
	and sfp.tm_change_effective_date = @price_change_effective_date
	and dtl.Data_To_Load_ID is null

----Load data for export
	Insert into EXPORT.RETAIL_CHANGE_HISTORY
	([Table_Name], Table_ID, Process, [Zone], Zone_Type, Item, Effective_Date, Price_Multiple, Price, Item_Level, Item_Class, Level_Gross_Flag, Target_Margin, Price_Type, product_structure_fk, zone_entity_structure_fk, data_value_fk)
	Select
	dtl.[Table_Name], dtl.Table_ID, dtl.Process, dtl.ZONE_ID, dtl.ZONE_TYPE
	, item_surrogate
	, dtl.Effective_Date, dtl.Price_Multiple, dtl.Price, dtl.Item_Level, dtl.Item_Class
	, dtl.Level_Gross_Flag, dtl.Target_Margin, dtl.Price_Type, dtl.product_structure_fk, dtl.zone_entity_structure_fk, dtl.DATA_VALUE_FK
	from #Data_To_Load dtl
	inner join precision.entities_live el on dtl.ZONE_ENTITY_STRUCTURE_FK = el.entity_structure_fk and isnull(el.export_record_status_cr_fk, 0) = 1
	where 1 = 1

