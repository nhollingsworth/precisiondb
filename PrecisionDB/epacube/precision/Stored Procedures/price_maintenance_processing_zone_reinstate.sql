﻿-- =============================================
-- Author:		Gary Stone
-- Create date: 2018-06-15
-- Description:	Proceses cost changes into scheduled retail price changes - zone
-- =============================================
CREATE PROCEDURE [precision].[price_maintenance_processing_zone_reinstate] @MarginChanges int = 0, @Wk int = 0
AS

	--Get rid of any potential test data inadvertently input by epacube testing
	delete from epacube.epacube.segments_settings where data_source = 'user update' and (create_user like '%epacube%' or create_user = 'dba')

----	Set @Wk to 0 for current week's Level Gross Calculations and Insert; 1 to regenerate and replace last week's data, 2 to regenerate and replace 2 weeks ago data, etc.
--	Declare @Wk int = 0
--	Declare @MarginChanges int = 0

--Declare @Qry varchar(Max), @QryID as int
--Set @Qry = 
--'Insert into dbo.pentaho_processing_test_log (proc_name, process_time_start, ProcParameters)
--Select ''precision.price_maintenance_processing_zone'' ''proc_name'', getdate() ''process_time_start'', ''@MarginChanges = ' + cast(@MarginChanges as varchar(8)) + '; @Wk = ' + cast(@Wk as varchar(8)) + ''''

--if object_id('dbo.pentaho_processing_test_log') is not null
--Begin
--	exec(@Qry)
--	Set @QryID = (select top 1 pentaho_processing_test_log_id from dbo.pentaho_processing_test_log order by pentaho_processing_test_log_id desc)
--End

	Declare @Price_Change_Effective_Date date
	Set @Price_Change_Effective_Date = Cast(GETDATE() + 6 - (@WK * 7) - DATEPART(DW, GETDATE()) as date)
	
	Declare @DateLastProc Date
	Set @DateLastProc = Cast(GETDATE() + 6 - (@WK * 7) - DATEPART(DW, GETDATE()) as date)
	Set @DateLastProc = Dateadd(d, -7, @DateLastProc)
	
	Declare @LGprocDate date
	Set @LGprocDate = Cast(GETDATE() + 3 - (@WK * 7) - DATEPART(DW, GETDATE()) as date)

	if object_id('tempdb..#PBV') is not null
	drop table #PBV	

	if object_id('tempdb..#NZ') is not null
	drop table #NZ

	if object_id('tempdb..#MC') is not null
	drop table #MC

	if object_id('tempdb..#LG_hist') is not null
	drop table #LG_hist

	If Object_ID('tempdb..#Aliases') is not null
	drop table #Aliases

	If Object_ID('tempdb..#Products') is not null
	drop table #Products

	If Object_ID('tempdb..#Aliases_Adj') is not null
	drop table #Aliases_Adj

	If Object_ID('tempdb..#Products_Adj') is not null
	drop table #Products_Adj

	If Object_ID('tempdb..#Aliases_Mult') is not null
	drop table #Aliases_Mult

	If object_id('tempdb..#pmccf') is not null
	drop table #pmccf

--Stage most current [cost changes to processing] date
		Select * 
		into #pmccf
		from (
		select 
		Dense_Rank()over(partition by product_structure_fk, zone_entity_structure_fk, data_name_fk order by cost_change_processed_date desc, effective_date desc, pricesheet_basis_values_id desc) 'DRank'
		, * from precision.price_maintenance_cost_change_flags
		where 1 = 1
		and pre_notify_days = 7
		and cost_change_processed_date <= @DateLastProc
		) a where DRank = 1
		order by cost_change_processed_date desc

		Select pa.product_structure_fk, Cast(min(pa.create_timestamp) as date) 'Add_To_Zone_Date'
		into #NZ
		from epacube.product_association pa with (nolock) 
		inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
		inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
		where pa.data_name_fk = 159450 
			and pa.record_status_cr_fk = 1
			and dv.value in ('H') --('H') --('R', 'H')
		group by pa.PRODUCT_STRUCTURE_FK
		having min(pa.create_timestamp) >= Cast(GETDATE() + 4 - 7 - (@wk * 7) - DATEPART(DW, GETDATE()) as date)

		Create index idx_01 on #NZ(Product_Structure_FK, Add_To_Zone_Date)

	Select C.*, 1 'IND_COST_CHANGE_ITEM' 
		into #PBV 
		from (
				Select *,  dense_rank()over(partition by product_structure_fk order by 
					case when pbv_status like 'New To Zone%' then 10
						--when pbv_status = 'New To Zone Backward' then 20 
						when pbv_status = 'Future' and isnull(change_status, 0) = 0 then 30
						when pbv_status = 'Current' then 40
						--when pbv_status = 'Margin_Change' then 10 
						end) DRank_f
					from (
						Select * from (
										Select pbv.value, pbv.unit_value, pbv.Effective_Date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, pbv.create_timestamp
										, case when isnull(patt.attribute_event_data, 'W') <> 'W' then pbv.cust_entity_structure_fk end 'Cust_Entity_Structure_FK' 
										, dense_rank()over(partition by pbv.data_name_fk, pbv.product_structure_fk order by pbv.effective_date desc, pbv.pricesheet_basis_values_id) DRank
										, 'Current' 'PBV_Status', Null 'Add_To_Zone_Date', pbv.Change_Status, pmccf.Cost_Change_Processed_Date
										from marginmgr.pricesheet_basis_values pbv with (nolock) 
										left join epacube.product_attribute patt with (nolock) on pbv.product_structure_fk = patt.product_structure_fk and patt.data_name_fk = 500000
										left join #pmccf pmccf on pbv.product_structure_fk = pmccf.product_structure_fk
																												and pbv.data_name_fk = pmccf.data_name_fk
																												and pbv.Zone_Entity_Structure_FK = pmccf.zone_entity_structure_fk 
										where pbv.data_name_fk = 503201 
										and cust_entity_structure_fk is null
										--and isnull(pbv.Change_Status, 0) = 0
										and 
											PBV.Effective_Date between Cast(GETDATE() + 4 - 7 - (@wk * 7) - DATEPART(DW, GETDATE()) as date) and Cast(GETDATE() + 6 - (@wk * 7) - DATEPART(DW, GETDATE()) as date)
											and PBV.Create_Timestamp < Cast(GETDATE() + 4 - (@wk * 7) - DATEPART(DW, GETDATE()) as date)
										and pbv.record_status_cr_fk = 1
										and pbv.effective_date > isnull(pmccf.effective_date, '2019-01-01')
										and @MarginChanges = 0
											) a where drank = 1
				Union
						Select * from (
										Select pbv.value, pbv.unit_value, pbv.Effective_Date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, pbv.create_timestamp
										, case when isnull(patt.attribute_event_data, 'W') <> 'W' then pbv.cust_entity_structure_fk end 'Cust_Entity_Structure_FK' 
										, dense_rank()over(partition by pbv.data_name_fk, pbv.product_structure_fk order by pbv.effective_date asc, pbv.pricesheet_basis_values_id) DRank
										, 'Future' 'PBV_Status', Null 'Add_To_Zone_Date', pbv.Change_Status, pmccf.Cost_Change_Processed_Date
										from marginmgr.pricesheet_basis_values pbv with (nolock) 
										left join epacube.product_attribute patt with (nolock) on pbv.product_structure_fk = patt.product_structure_fk and patt.data_name_fk = 500000
										left join #pmccf pmccf on pbv.product_structure_fk = pmccf.product_structure_fk
																												and pbv.data_name_fk = pmccf.data_name_fk
																												and pbv.Zone_Entity_Structure_FK = pmccf.zone_entity_structure_fk 
										where pbv.data_name_fk = 503201 
										and cust_entity_structure_fk is null
										and isnull(pbv.Change_Status, 0) = 0
										and 
											PBV.Effective_Date between Cast(GETDATE() + 7 - (@wk * 7) - DATEPART(DW, GETDATE()) as date) and Cast(GETDATE() + 13 - (@wk * 7) - DATEPART(DW, GETDATE()) as date)
											and PBV.Create_Timestamp < Cast(GETDATE() + 4 - (@wk * 7) - DATEPART(DW, GETDATE()) as date)
										and pbv.record_status_cr_fk = 1
										and pbv.effective_date > isnull(pmccf.effective_date, '2019-01-01')
										and @MarginChanges = 0
									) a where DRank = 1
				Union
						Select * from (
										Select pbv.value, pbv.unit_value, pbv.Effective_Date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, pbv.create_timestamp
										, case when isnull(patt.attribute_event_data, 'W') <> 'W' then pbv.cust_entity_structure_fk end 'Cust_Entity_Structure_FK' 
										, dense_rank()over(partition by pbv.data_name_fk, pbv.product_structure_fk 
											order by datediff(d, first_added_to_any_H_zone, LG_Process_Date) desc, pbv.effective_date desc, pbv.pricesheet_basis_values_id
												) DRank
										, 'New To Zone ' + Cast(first_added_to_any_H_zone as varchar(64)) 'PBV_Status', Add_To_Zone_Date, pbv.Change_Status, pmccf.Cost_Change_Processed_Date
										from epacube.marginmgr.pricesheet_basis_values pbv with (nolock) 
										left join epacube.epacube.product_attribute patt with (nolock) on pbv.product_structure_fk = patt.product_structure_fk and patt.data_name_fk = 500000
										left join #pmccf pmccf on pbv.product_structure_fk = pmccf.product_structure_fk
															and pbv.data_name_fk = pmccf.data_name_fk
															and pbv.Zone_Entity_Structure_FK = pmccf.zone_entity_structure_fk 
										inner join #NZ NZ on pbv.product_structure_fk = nz.PRODUCT_STRUCTURE_FK
										inner join 
											(
												Select
												Item
												, first_added_to_any_H_zone
												, [LG_Cutoff]
												, case when [LG_Cutoff] >= first_added_to_any_H_zone then [LG_Cutoff] else dateadd(d, 7, [LG_Cutoff]) end 'LG_Process_Date'
												, Effective_Date 'Cost_Change_Effective_Date'
												, dateadd(d, 3, case when [LG_Cutoff] >= first_added_to_any_H_zone then [LG_Cutoff] else dateadd(d, 7, [LG_Cutoff]) end) 'First_LG_Opportunity'
												, [Case Cost]
												, [Unit Cost]
												, Created
												, Product_Structure_FK
												from (
												Select 
												item
												, first_added_to_any_H_zone
												, cast(cast(dateadd(d, 3, first_added_to_any_H_zone) as datetime) - datepart(dw, first_added_to_any_H_zone) as date) 'LG_Cutoff'
												, Effective_Date
												, LG_Date
												, value 'Case Cost'
												, unit_value 'Unit Cost'
												, Created
												, Product_Structure_FK
												from (

												select pi.value 'Item', pbv.effective_date
												, cast((
												select min(pa.CREATE_TIMESTAMP) from epacube.epacube.product_association pa
												inner join epacube.epacube.entity_attribute ea with (nolock) on ea.entity_structure_fk = pa.ENTITY_STRUCTURE_FK and ea.data_name_fk = 502060
												inner join epacube.epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id
												where pa.DATA_NAME_FK = 159450 and pa.product_structure_fk = pbv.product_structure_fk
												and dv.value = 'H'
												) as date) 'first_added_to_any_H_zone'
												, pmccf.cost_change_processed_date 'LG_Date', pbv.value, pbv.unit_value, cast(pbv.create_timestamp as date) 'Created', pbv.Product_Structure_FK 
												from epacube.marginmgr.pricesheet_basis_values pbv
												inner join epacube.epacube.product_identification pi with (nolock) on pbv.Product_Structure_FK = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110100
												inner join #NZ nz on pbv.Product_Structure_FK = nz.product_structure_fk
												left join #pmccf pmccf on pbv.product_structure_fk = pmccf.product_structure_fk
																												and pbv.data_name_fk = pmccf.data_name_fk
																												and pbv.Zone_Entity_Structure_FK = pmccf.zone_entity_structure_fk 
												where pbv.data_name_fk = 503201 --and pbv.product_structure_fk in (363946)
												) a
												) b
											) ZoneAdds on nz.product_structure_fk = ZoneAdds.product_structure_fk and pbv.effective_date = ZoneAdds.cost_change_effective_date
										where pbv.data_name_fk = 503201 
										and cust_entity_structure_fk is null
										and first_added_to_any_H_zone > dateadd(d, -6, lg_process_date)
										and pbv.record_status_cr_fk = 1
										and @MarginChanges = 0
									) a where DRank = 1
				) B
			) C 
			where 1 = 1
				and C.DRank_f = 1
				and C.Product_Structure_FK in 
					(Select distinct product_structure_fk from epacube.marginmgr.pricesheet_retail_changes prc 
						where PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
						and isnull(IND_RESCIND, 0) = 1)

	If (select count(*) from #pbv) = 0
	goto eof

			create index idx_01 on #pbv(product_structure_fk)

			Alter table #pbv add item varchar(64) null

			Update pbv
			set item = pi.value
			from #pbv pbv
			inner join epacube.product_identification pi on pbv.Product_Structure_FK = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110100


	If object_id('tempdb..#products') is not null
	drop table #products

	Create table #Products(Insert_Step int not null, [Item] varchar(16) Null, Product_structure_FK bigint null, Parity_PL_Parent_Structure_FK bigint null, PARITY_ALIAS_PL_PARENT_STRUCTURE_FK bigint Null, Parity_Alias_Data_Value_FK varchar(64) null--, Alias_Parity_Alias_PL_Parent_Structure_FK bigint null
	, Alias_Data_Value_FK varchar(64) null, ind_Cost_Change_Item int null, Cost_Change_Status int null
	, Parity_Child int null, ind_Parity int Null, Price_Cur money null, Price_New money null, Unit_Cost_Cur money null, Unit_Cost_New money null, TMpct Numeric(18, 4) Null, Level_Gross varchar(1) Null
	, Item_Change_Type_CR_FK int null, Qualified_Status_CR_FK int null, Price_Mgmt_Type_CR_FK int null, Parity_SIZE_Parent_Structure_FK bigint null, Zone_Entity_Structure_FK bigint null, Item_Description Varchar(64) Null
	, Case_Cost_New money null, Cost_Change_Effective_Date_New date null, Pack int null, Pricesheet_basis_Values_ID_New bigint null, PARITY_DISCOUNT_AMT money null
	, PARITY_DISCOUNT_PCT Numeric(18, 2) Null, Parity_Mult Int null, Parity_Type Varchar(64) Null, Reason_For_Item Varchar(64) Null, Price_Multiple_Cur int null, Price_Effective_Date_Cur date null, [Use_LG_Price_Points] varchar(1) Null
	, MinAmt Numeric(18, 2) Null, DaysOut int null, Case_Cost_Cur money null, Cost_Change_Effective_Date_Cur date null, [ind_cost_delta] int null, Pricesheet_Basis_Values_ID_Cur bigint Null, Reason_For_Inclusion varchar(64) null
	, Subgroup_Data_Value_FK bigint null, Subgroup varchar(64) Null, Subgroup_Description varchar(64) Null, Notification_Date date Null, [Pricing_Update_Steps] varchar(Max) Null, Price_Type_Step int null, Qualified_Step int null
	, Price_Init Numeric(18, 2) Null, Digit_DN_FK bigint null, Price_Adj_Amt Numeric(18, 2) Null, Price_Set_Step int null, Final_Pricing_Step varchar(max) null
	)

	alter table #Products add [Products_ID] bigint identity(1, 1) not null

	create index idx_01 on #products(zone_entity_structure_fk, product_structure_fk)
	create unique index idx_00 on #products([Products_ID])

--Add Private Label item in parity with National items having a cost change - No PL Aliases Involved
	Insert into #products
	(Item_Change_Type_CR_FK, Product_structure_FK, Zone_Entity_Structure_FK, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion)

	Select case isnull(@MarginChanges, 0) when 0 then 655 else 661 end 'Item_Change_Type_CR_FK', pa.product_structure_fk 'product_structure_fk'
		, pa.entity_structure_fk 'zone_entity_structure_fk'
		, (Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk) IND_COST_CHANGE_ITEM
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.value else null end 'Case_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.Unit_value else null end 'Unit_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.Effective_Date else Null end 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'pack'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.Pricesheet_Basis_Values_ID else Null end 'Pricesheet_Basis_Values_ID_New'
		, 1 'ind_parity'
		, 10 'Insert_Step', 'PL for National Cost Change' 'Parity_Type', 'PL Parity without Alias' 'Reason_For_Item', PBV.PBV_Status 'Reason_For_Inclusion'
	from #pbv pbv 
	inner join epacube.product_association pa with (nolock) on pbv.product_structure_fk = pa.child_product_structure_fk and pa.data_name_fk in (159903) and pa.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	left join epacube.product_mult_type pmt with (nolock) on pa.product_structure_fk = pmt.product_structure_fk and pa.entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019
	inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	inner join epacube.product_association paP with (nolock) on pbv.product_structure_fk = paP.product_structure_fk and paP.entity_structure_fk = pa.entity_structure_fk and paP.data_name_fk = 159450 and paP.record_status_cr_fk = 1
	inner join epacube.product_association pap2 with (nolock) on pa.PRODUCT_STRUCTURE_FK = pap2.product_structure_fk and pa.ENTITY_STRUCTURE_FK = pap2.ENTITY_STRUCTURE_FK and pap2.data_name_fk = 159450 and pap2.RECORD_STATUS_CR_FK = 1
	left join epacube.product_attribute pa_500001 with (nolock) on pbv.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	where dv.value in ('H') --('H') --('R', 'H')
	AND pmt.product_mult_type_id is null
	and pa.product_structure_fk not in (select product_structure_fk from epacube.product_mult_type where data_name_fk = 500019 and entity_structure_fk = pa.entity_structure_fk)

	--add items in alias to parity item
	Insert into #products
	(Item_Change_Type_CR_FK, Product_structure_FK, Zone_Entity_Structure_FK, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion)
	Select case isnull(@MarginChanges, 0) when 0 then 655 else 661 end 'Item_Change_Type_CR_FK', pa.product_structure_fk 'product_structure_fk'
		, pa.entity_structure_fk 'zone_entity_structure_fk'
		, (Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk) IND_COST_CHANGE_ITEM
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.value else null end 'Case_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.Unit_value else null end 'Unit_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.Effective_Date else Null end 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'pack'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.Pricesheet_Basis_Values_ID else Null end 'Pricesheet_Basis_Values_ID_New'
		, 1 'ind_parity'
		, 12 'Insert_Step', 'PL for National Cost Change' 'Parity_Type', 'PL Parity without Alias' 'Reason_For_Item', PBV.PBV_Status 'Reason_For_Inclusion'
	from #pbv pbv 
	inner join epacube.product_association pa with (nolock) on pbv.product_structure_fk = pa.child_product_structure_fk and pa.data_name_fk in (159903) and pa.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	left join epacube.product_mult_type pmt with (nolock) on pa.child_product_structure_fk = pmt.product_structure_fk and pa.entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019
	inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	inner join epacube.product_association paP with (nolock) on pbv.product_structure_fk = paP.product_structure_fk and paP.entity_structure_fk = pa.entity_structure_fk and paP.data_name_fk = 159450 and paP.record_status_cr_fk = 1
	inner join epacube.product_association pap2 with (nolock) on pa.PRODUCT_STRUCTURE_FK = pap2.product_structure_fk and pa.ENTITY_STRUCTURE_FK = pap2.ENTITY_STRUCTURE_FK and pap2.data_name_fk = 159450 and pap2.RECORD_STATUS_CR_FK = 1
	left join epacube.product_attribute pa_500001 with (nolock) on pbv.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	where dv.value in ('H') --('H') --('R', 'H')
	AND pmt.product_mult_type_id is not null
	and pbv.product_structure_fk not in (select product_structure_fk from epacube.product_mult_type where data_name_fk = 500019 and entity_structure_fk = pa.entity_structure_fk)

--Add the National Brand Items with the cost change - Alias may or may not be Involved
	Insert into #products
	(Item_Change_Type_CR_FK, Product_structure_FK, Parity_PL_Parent_Structure_FK, Parity_Alias_Data_Value_FK, Parity_Child, Zone_Entity_Structure_FK, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion)
	Select distinct case isnull(@MarginChanges, 0) when 0 then 655 else 661 end 'Item_Change_Type_CR_FK', pa.child_product_structure_fk 'product_structure_fk'
		, pa.product_structure_fk 'parity_pl_parent_structure_fk'
		, pmt.data_value_fk 'Parity_Alias_Data_Value_FK'
		, 1 'parity_child', pa.entity_structure_fk 'zone_entity_structure_fk'
		, (Select 1 from #PBV where product_structure_fk = pa.child_product_structure_fk) 'IND_COST_CHANGE_ITEM'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv.value else null end 'Case_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv.Unit_value else null end 'Unit_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv.Effective_Date else Null end 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'pack'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv.Pricesheet_Basis_Values_ID else Null end 'Pricesheet_Basis_Values_ID_New'
		, 1 'ind_parity'
		, 20 'Insert_Step', 'National Cost Change' 'Parity_Type', 'Parity with or without Alias' 'Reason_For_Item', PBV.PBV_Status 'Reason_For_Inclusion'
	from #pbv pbv 
	inner join epacube.product_association pa with (nolock) on pbv.product_structure_fk = pa.child_product_structure_fk and pa.data_name_fk in (159903) and pa.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	left join epacube.product_mult_type pmt with (nolock) on Pbv.product_structure_fk = pmt.product_structure_fk and pa.entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019
	inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.product_association paP with (nolock) on pbv.product_structure_fk = paP.product_structure_fk and paP.entity_structure_fk = pa.entity_structure_fk and paP.data_name_fk = 159450 and paP.record_status_cr_fk = 1
	inner join epacube.product_association paC with (nolock) on pa.child_product_structure_fk = paC.product_structure_fk and paC.entity_structure_fk = pa.entity_structure_fk and paC.data_name_fk = 159450 and paC.record_status_cr_fk = 1
	inner join epacube.product_association pap2 with (nolock) on pa.PRODUCT_STRUCTURE_FK = pap2.product_structure_fk and pa.ENTITY_STRUCTURE_FK = pap2.ENTITY_STRUCTURE_FK and pap2.data_name_fk = 159450 and pap2.RECORD_STATUS_CR_FK = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	left join epacube.product_attribute pa_500001 with (nolock) on pbv.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Products P1 on pa.child_product_structure_fk = p1.product_structure_fk and pa.entity_structure_fk = p1.zone_entity_structure_fk
	--left join #LG_hist lga on pa.child_product_structure_fk = lga.product_structure_fk and pa.ENTITY_STRUCTURE_FK = lga.zone_entity_structure_fk
	--			and pbv.effective_date = lga.new_cost_effective_date --and lga.effective_date = Cast(GETDATE() - 1 - (@WK * 7) - DATEPART(DW, GETDATE()) as date)
	Where 1 = 1
	and dv.value in ('H') --('H') --('R', 'H')
	and p1.products_id is null
	and pbv.product_structure_fk not in (select product_structure_fk from epacube.product_mult_type where data_name_fk = 500019 and entity_structure_fk = pa.entity_structure_fk)

	Insert into #products
	(Item_Change_Type_CR_FK, Product_structure_FK, Parity_Alias_PL_Parent_Structure_FK, Parity_Alias_Data_Value_FK, Parity_Child, Zone_Entity_Structure_FK
	, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion)
	Select case isnull(@MarginChanges, 0) when 0 then 655 else 661 end 'Item_Change_Type_CR_FK', pmt.product_structure_fk
	, P.Product_structure_FK 'Parity_Alias_PL_Parent_Structure_FK'
	, pmt.data_value_fk 'Parity_Alias_Data_Value_FK'
	, 1 Parity_Child
	, p.zone_entity_structure_fk 
	, (Select 1 from #PBV where product_structure_fk = pmt.PRODUCT_STRUCTURE_FK) 'IND_COST_CHANGE_ITEM'
	, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.PRODUCT_STRUCTURE_FK), 0) when 1 then pbv.value else null end 'Case_Cost_New'
	, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.PRODUCT_STRUCTURE_FK), 0) when 1 then pbv.Unit_value else null end 'Unit_Cost_New'
	, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.PRODUCT_STRUCTURE_FK), 0) when 1 then pbv.Effective_Date else Null end 'Cost_Change_Effective_Date_New'
	, pa_500001.attribute_event_data 'pack'
	, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.PRODUCT_STRUCTURE_FK), 0) when 1 then pbv.Pricesheet_Basis_Values_ID else Null end 'Pricesheet_Basis_Values_ID_New'
	, 1 'ind_parity'
	, 25 'Insert_Step', 'National Cost Change' 'Parity_Type', 'Aliases to Natl Parity Items' 'Reason_For_Item', 'National Item in Parity has Cost Change' 'Reason_For_Inclusion'
	from #Products P
	inner join epacube.product_mult_type pmt with (nolock) 
		on p.parity_alias_data_value_fk = pmt.data_value_fk
		and p.product_structure_fk <> pmt.PRODUCT_STRUCTURE_FK 
		and p.Zone_Entity_Structure_FK = pmt.ENTITY_STRUCTURE_FK 
		and pmt.DATA_NAME_FK = 500019
	inner join epacube.product_association pa_159450 with (nolock) 
		on pmt.product_structure_fk = pa_159450.product_structure_fk 
		and pmt.ENTITY_STRUCTURE_FK = pa_159450.entity_structure_fk 
		and pa_159450.data_name_fk = 159450
		 and pa_159450.record_status_cr_fk = 1
	left join #pbv pbv on pmt.PRODUCT_STRUCTURE_FK = pbv.Product_Structure_FK
	left join epacube.product_attribute pa_500001 with (nolock) on pmt.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	where p.parity_alias_data_value_fk is not null

----??? MayNeed to add code for aliases to National Brands that have a cost change and are in parity with a private label item

--Add private label parity items having a cost change - No aliases involved
	Insert into #products
	(Item_Change_Type_CR_FK, Product_structure_FK, Zone_Entity_Structure_FK, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion)
	Select case isnull(@MarginChanges, 0) when 0 then 655 else 661 end 'Item_Change_Type_CR_FK', pa.product_structure_fk 'product_structure_fk'
		, pa.entity_structure_fk 'zone_entity_structure_fk'
		, (Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk) IND_COST_CHANGE_ITEM
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.value else null end 'Case_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.Unit_value else null end 'Unit_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.Effective_Date else Null end 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'pack'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.Pricesheet_Basis_Values_ID else Null end 'Pricesheet_Basis_Values_ID_New'
		, 1 'ind_parity'
		, 30 'Insert_Step', 'Private Label Cost Change' 'Parity_Type', 'Parity without Alias' 'Reason_For_Item', PBV.PBV_Status 'Reason_For_Inclusion'
	from #pbv pbv 
	inner join epacube.product_association pa with (nolock) on pbv.product_structure_fk = pa.product_structure_fk and pa.data_name_fk in (159903) and pa.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	inner join epacube.product_association paP with (nolock) on pa.product_structure_fk = paP.product_structure_fk and paP.entity_structure_fk = pa.entity_structure_fk and paP.data_name_fk = 159450 and paP.record_status_cr_fk = 1
	left join epacube.product_mult_type pmt with (nolock) on pa.product_structure_fk = pmt.product_structure_fk and pa.entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019
	inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	left join epacube.product_attribute pa_500001 with (nolock) on pbv.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Products P1 on pa.product_structure_fk = p1.product_structure_fk and pa.entity_structure_fk = p1.zone_entity_structure_fk
	where dv.value in ('H') --('H') --('R', 'H')
	and p1.products_id is null
	and pmt.product_mult_type_id is null

--Add National Brand items in parity with Private Label items with cost changes - National Brand Alias identified
	Insert into #products
	(Item_Change_Type_CR_FK, Product_structure_FK, Parity_PL_Parent_Structure_FK, Parity_Child, Zone_Entity_Structure_FK, alias_data_value_fk, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion)
	Select case isnull(@MarginChanges, 0) when 0 then 655 else 661 end 'Item_Change_Type_CR_FK', pa.child_product_structure_fk 'product_structure_fk'
		, pa.product_structure_fk 'Parity_PL_Parent_Structure_FK'
		, 1 'Parity_Child'
		, pa.entity_structure_fk 'zone_entity_structure_fk'
		, pmt.data_value_fk 'alias_data_value_fk'
		, (Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk) IND_COST_CHANGE_ITEM
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv_c.value else null end 'Case_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv_c.Unit_value else null end 'Unit_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv_c.Effective_Date else Null end 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'pack'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv_c.Pricesheet_Basis_Values_ID else Null end 'Pricesheet_Basis_Values_ID_New'
		, 1 'ind_parity'
		, 40 'Insert_Step', 'National Items with PL Cost Changes' 'Parity_Type', 'Child_Parity with or without Alias' 'Reason_For_Item', pbv_c.PBV_Status 'Reason_For_Inclusion'
	from #pbv pbv 
	inner join epacube.product_association pa with (nolock) on pbv.product_structure_fk = pa.product_structure_fk and pa.data_name_fk in (159903) and pa.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	left join #pbv pbv_c on pa.CHILD_PRODUCT_STRUCTURE_FK = pbv_c.Product_Structure_FK
	inner join epacube.product_association paP with (nolock) on pbv.product_structure_fk = paP.product_structure_fk and paP.entity_structure_fk = pa.entity_structure_fk and paP.data_name_fk = 159450 and paP.record_status_cr_fk = 1
	left join epacube.product_mult_type pmtP with (nolock) on pa.product_structure_fk = pmtP.product_structure_fk and pa.entity_structure_fk = pmtP.entity_structure_fk and pmtP.data_name_fk = 500019
	inner join epacube.product_association paC with (nolock) on pa.child_product_structure_fk = paC.product_structure_fk and paC.entity_structure_fk = pa.entity_structure_fk and paC.data_name_fk = 159450 and paC.record_status_cr_fk = 1
	left join epacube.product_mult_type pmt with (nolock) on pa.child_product_structure_fk = pmt.product_structure_fk and pa.entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019
	inner join epacube.product_association pap2 with (nolock) on pa.PRODUCT_STRUCTURE_FK = pap2.product_structure_fk and pa.ENTITY_STRUCTURE_FK = pap2.ENTITY_STRUCTURE_FK and pap2.data_name_fk = 159450 and pap2.RECORD_STATUS_CR_FK = 1
	inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	left join epacube.product_attribute pa_500001 with (nolock) on pbv.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Products P1 on pa.child_product_structure_fk = p1.product_structure_fk and pa.entity_structure_fk = p1.zone_entity_structure_fk
	where 1 = 1
	and dv.value in ('H') --('H') --('R', 'H')
	and p1.products_id is null
	and pmtP.product_mult_type_id is null

--Add items that are in alias to National Brand parity items without cost changes
	insert into #Products
	(Item_Change_Type_CR_FK, Product_structure_FK, Zone_Entity_Structure_FK, ind_Cost_Change_Item, Alias_Data_Value_FK, Parity_Alias_PL_Parent_Structure_FK, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New
	, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion)
	Select case isnull(@MarginChanges, 0) when 0 then 655 else 661 end 'Item_Change_Type_CR_FK', pmt.product_structure_fk, p.zone_entity_structure_fk
		, (Select 1 from #PBV where product_structure_fk = pmt.product_structure_fk) 'IND_COST_CHANGE_ITEM'
		, pmt.data_value_fk 'Alias_data_value_fk'
		, P.product_structure_fk 'parity_alias_PL_Parent_Structure_fk'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.product_structure_fk), 0) when 1 then p.Case_Cost_New else null end 'Case_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.product_structure_fk), 0) when 1 then p.Unit_Cost_New else null end 'Unit_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.product_structure_fk), 0) when 1 then p.Cost_Change_Effective_Date_New else Null end 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'Pack'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.product_structure_fk), 0) when 1 then p.Pricesheet_Basis_Values_ID_New else Null end 'Pricesheet_Basis_Values_ID_New'
		, P.ind_parity
		, 50 'Insert_Step',  'Parity_Type'
		, 'Alias due to National Parity Cost Change' 'Reason_For_Item', p.Reason_For_Inclusion
	from #Products P
	inner join epacube.product_mult_type pmt with (nolock) on p.Alias_data_value_fk = pmt.data_value_fk and p.zone_entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019 --and p.product_structure_fk <> pmt.product_structure_fk
	inner join epacube.product_association paPa with (nolock) on pmt.product_structure_fk = paPa.product_structure_fk and paPa.entity_structure_fk = pmt.entity_structure_fk and paPa.data_name_fk = 159450 and paPa.record_status_cr_fk = 1
	inner join epacube.entity_attribute ea with (nolock) on p.zone_entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	left join epacube.product_attribute pa_500001 with (nolock) on pmt.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Products P1 on pmt.product_structure_fk = p1.product_structure_fk and pmt.entity_structure_fk = p1.zone_entity_structure_fk
	where 1 = 1
	and dv.value in ('H') --('H') --('R', 'H')
	and isnull(p.IND_COST_CHANGE_ITEM, 0) = 0
	and p1.products_id is null

--Add Regular items not involved in Parity or Alias
	Insert into #products
	(Item_Change_Type_CR_FK, Product_structure_FK, Zone_Entity_Structure_FK, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion)
	Select case isnull(@MarginChanges, 0) when 0 then 655 else 661 end 'Item_Change_Type_CR_FK', pbv.product_structure_fk 'product_structure_fk'
		, pa.entity_structure_fk 'zone_entity_structure_fk'
		, 1 IND_COST_CHANGE_ITEM
		, pbv.value 'Case_Cost_New'
		, pbv.unit_value 'Unit_Cost_New'
		, pbv.Effective_Date 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'pack'
		, pbv.Pricesheet_Basis_Values_ID 'Pricesheet_Basis_Values_ID_New'
		, 60 'Insert_Step', 'None' 'Parity_Type', 'Regular Item without Parity Without Alias' 'Reason_For_Item', PBV.PBV_Status 'Reason_For_Inclusion'
	from #pbv pbv 
	inner join epacube.product_association pa with (nolock) on pbv.product_structure_fk = pa.product_structure_fk and pa.data_name_fk in (159450) and pa.record_status_cr_fk = 1
	left join epacube.product_association pa_159903 with (nolock) on pa.product_structure_fk = pa_159903.product_structure_fk and pa.entity_structure_fk = pa_159903.entity_structure_fk and pa_159903.data_name_fk = 159903 and pa_159903.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	left join epacube.product_mult_type pmt with (nolock) on pa.product_structure_fk = pmt.product_structure_fk and pa.entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019
	inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	inner join epacube.product_association paP with (nolock) on pbv.product_structure_fk = paP.product_structure_fk and paP.entity_structure_fk = pa.entity_structure_fk and paP.data_name_fk = 159450 and paP.record_status_cr_fk = 1
	left join epacube.product_attribute pa_500001 with (nolock) on pbv.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Products P1 on pa.product_structure_fk = p1.product_structure_fk and pa.entity_structure_fk = p1.zone_entity_structure_fk
	where dv.value in ('H') --('H') --('R', 'H')
	and pa_159903.product_association_id is null
	and pmt.product_mult_type_id is null
	and p1.products_id is null

--Begin Updates to establish new prices, level gross and qualified statuses	

	Update P
	Set PARITY_DISCOUNT_AMT = sz_500210.Parity_Discount_Amt 
	from #products p
	inner join
		(
		Select
		ss.Product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK, cast(ss.attribute_number as numeric(18, 4)) 'Parity_Discount_Amt'
		, Dense_rank()over(partition by ss.Product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) DRank
		from epacube.segments_settings ss
		inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.PRODUCT_ASSOCIATION_FK = pa.PRODUCT_ASSOCIATION_ID
		where ss.data_name_fk = 500210 
		and ss.attribute_number <> 0 
		and ss.RECORD_STATUS_CR_FK = 1
		and pa.record_status_cr_fk = 1
		) sz_500210 on p.PRODUCT_STRUCTURE_FK = sz_500210.product_structure_fk and p.ZONE_ENTITY_STRUCTURE_FK = sz_500210.ZONE_ENTITY_STRUCTURE_FK
		Where DRank = 1

	Update P
	Set Parity_Mult = sz_500212.Parity_Mult 
	from #products P
	inner join
		(
		Select
		ss.Product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK, cast(ss.attribute_number as numeric(18, 4)) 'Parity_Mult'
		, Dense_rank()over(partition by ss.Product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) DRank
		from epacube.segments_settings ss 
		inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.PRODUCT_ASSOCIATION_FK = pa.PRODUCT_ASSOCIATION_ID
		where ss.data_name_fk = 500212 
		and ss.attribute_number <> 0
		and ss.RECORD_STATUS_CR_FK = 1
		and pa.record_status_cr_fk = 1
		) sz_500212 on P.PRODUCT_STRUCTURE_FK = sz_500212.product_structure_fk and P.ZONE_ENTITY_STRUCTURE_FK = sz_500212.ZONE_ENTITY_STRUCTURE_FK
		Where DRank = 1

	Update p
	Set PARITY_DISCOUNT_PCT = sz_500211.Parity_Discount_Pct 
	from #products P
	inner join
		(
		Select
		ss.Product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK, cast(ss.attribute_number as numeric(18, 4)) 'Parity_Discount_Pct'
		, Dense_rank()over(partition by ss.Product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) DRank
		from epacube.segments_settings ss
		inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.PRODUCT_ASSOCIATION_FK = pa.PRODUCT_ASSOCIATION_ID
		where ss.data_name_fk = 500211 
		and ss.attribute_number <> 0
		and ss.RECORD_STATUS_CR_FK = 1
		and pa.record_status_cr_fk = 1
		) sz_500211 on p.PRODUCT_STRUCTURE_FK = sz_500211.product_structure_fk and p.ZONE_ENTITY_STRUCTURE_FK = sz_500211.ZONE_ENTITY_STRUCTURE_FK
		where DRank = 1

	Update P
	Set TMpct = sz_502046.attribute_number
	from #Products P
	inner join 
		(select * from (
						Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
						, dense_rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
						from epacube.segments_settings ss with (nolock) 
						inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
						inner join #products p on sp.product_structure_fk = p.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = p.zone_entity_structure_fk
						where ss.data_name_fk = 502046
						and ss.Effective_Date <= @LGprocDate
						and ss.RECORD_STATUS_CR_FK = 1
						) a where Drank = 1
		) sz_502046 on p.Product_structure_FK = sz_502046.product_structure_fk and p.zone_entity_structure_fk = sz_502046.ZONE_ENTITY_STRUCTURE_FK

	 Update P
	 Set Level_Gross = Case isnull(sz_144862.ATTRIBUTE_Y_N, 'N') when 'Y' then sz_500015.ATTRIBUTE_Y_N else 'N' end
	 from #Products P
  		left join 
			(select * from (
							
							Select distinct ss.ATTRIBUTE_Y_N, sp.product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK
							, dense_rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK, sp.product_structure_fk order by ss.attribute_y_n, ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
							from epacube.segments_settings ss with (nolock) 
							inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
							inner join #Products zc on sp.product_structure_fk = zc.PRODUCT_STRUCTURE_FK and ss.ZONE_ENTITY_STRUCTURE_FK = zc.ZONE_ENTITY_STRUCTURE_FK
							where ss.data_name_fk = 500015
							and ss.RECORD_STATUS_CR_FK = 1
							and isnull(zc.ind_cost_change_item, 0) = 1
							) a where Drank = 1
			) sz_500015 on P.product_structure_fk = sz_500015.product_structure_fk and sz_500015.ZONE_ENTITY_STRUCTURE_FK = p.ZONE_ENTITY_STRUCTURE_FK
		left join 
			(select * from (
							Select ss.ATTRIBUTE_Y_N, ss.ZONE_ENTITY_STRUCTURE_FK, ss.SEGMENTS_SETTINGS_ID
							, dense_rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) DRank
							from epacube.segments_settings ss with (nolock) 
							where ss.data_name_fk = 144862
							and ss.ZONE_ENTITY_STRUCTURE_FK is not null 
							and CUST_ENTITY_STRUCTURE_FK is null
							and ss.RECORD_STATUS_CR_FK = 1
							) a where Drank = 1
			) sz_144862 on sz_144862.ZONE_ENTITY_STRUCTURE_FK = P.ZONE_ENTITY_STRUCTURE_FK
			where isnull(P.ind_cost_change_item, 0) = 1

	Update P
	Set Price_Multiple_Cur = CurPrice.price_multiple
	, Price_Cur = CurPrice.Price
	, Price_Effective_Date_Cur = CurPrice.Effective_Date
	from #Products P
	inner join 
		(Select
		pr.zone_entity_structure_FK, pr.product_structure_fk, pr.price_multiple, pr.Price, pr.Effective_Date
		, Dense_Rank()over(partition by pr.zone_entity_structure_FK, pr.product_structure_fk order by pr.effective_date desc, pr.Price) DRank_Eff
		from marginmgr.PRICESHEET_RETAILS PR 
		inner join #Products P on pr.product_structure_fk = P.Product_structure_FK and pr.zone_entity_structure_FK = P.Zone_Entity_Structure_FK
		where pr.zone_entity_structure_FK is not null and pr.effective_date < Cast(GETDATE() + 6 - (@wk * 7) - DATEPART(DW, GETDATE()) as date)
		group by pr.zone_entity_structure_FK, pr.product_structure_fk, pr.price_multiple, pr.Price, pr.Effective_Date
		) CurPrice on P.PRODUCT_STRUCTURE_FK = CurPrice.Product_Structure_FK and P.Zone_Entity_Structure_FK = CurPrice.zone_entity_structure_FK
	where 1 = 1
		and isnull(CurPrice.DRank_Eff, 1) = 1

	Update P
	Set [Use_LG_Price_Points] = isnull(PP.UsePPT, 'N')
	from #Products P
	inner join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, sp.PRODUCT_STRUCTURE_FK, ss.ZONE_ENTITY_STRUCTURE_FK, ss.attribute_event_data 'UsePPT', ss.precedence, ss.effective_date, SEGMENTS_SETTINGS_ID
						, Dense_Rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) 'DRank_PPTs'
						from epacube.segments_settings ss with (nolock)
						inner join epacube.segments_product sp with (nolock) on sp.PROD_SEGMENT_FK = ss.PROD_SEGMENT_FK
						inner join #products p on sp.product_structure_fk = p.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = p.Zone_Entity_Structure_FK
						where 1 = 1
						and ss.ZONE_ENTITY_STRUCTURE_FK is not null
						and ss.prod_segment_fk is not null
						and ss.cust_entity_structure_fk is null
						and ss.data_name_fk = 144876
						and isnull(p.ind_Cost_Change_Item, 0) = 1
						and ss.effective_date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + 7, getdate() - (@wk * 7)) as date)
						and ss.RECORD_STATUS_CR_FK = 1
						and ss.precedence is not null
					) a where DRank_PPTs = 1 
		) PP on P.ZONE_ENTITY_STRUCTURE_FK = PP.ZONE_ENTITY_STRUCTURE_FK and P.product_structure_fk = PP.PRODUCT_STRUCTURE_FK
	where P.ind_Cost_Change_Item = 1

	Update P
	Set [Use_LG_Price_Points] = isnull(PP.UsePPT, 'N')
	from #Products P
	inner join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, ss.ZONE_ENTITY_STRUCTURE_FK, ss.attribute_event_data 'UsePPT', ss.effective_date, SEGMENTS_SETTINGS_ID
						, Dense_Rank()over(partition by ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) 'DRank'
						from epacube.segments_settings ss with (nolock)
						where 1 = 1
						and ss.ZONE_ENTITY_STRUCTURE_FK is not null
						and ss.prod_segment_fk is null
						and ss.cust_entity_structure_fk is null
						and ss.data_name_fk = 144876
						and ss.effective_date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + 7, getdate() - (@WK * 7)) as date)
						and ss.RECORD_STATUS_CR_FK = 1
						) A where DRank = 1

		) PP on P.ZONE_ENTITY_STRUCTURE_FK = PP.ZONE_ENTITY_STRUCTURE_FK
	where 1 = 1
		and isnull(P.ind_Cost_Change_Item, 0) = 1
		and P.[Use_LG_Price_Points] is null

--get MinAmt at Zone Item Class level
	Update P
	Set MinAmt = isnull(PP.MinAmt, 0)
	from #Products P
	inner join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, sp.PRODUCT_STRUCTURE_FK, ss.ZONE_ENTITY_STRUCTURE_FK, ss.attribute_number 'MinAmt', ss.precedence, ss.effective_date, SEGMENTS_SETTINGS_ID
						, Dense_Rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) 'DRank_PPTs'
						from epacube.segments_settings ss with (nolock)
						inner join epacube.segments_product sp with (nolock) on sp.PROD_SEGMENT_FK = ss.PROD_SEGMENT_FK
						inner join #products p on sp.product_structure_fk = p.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = p.Zone_Entity_Structure_FK
						where 1 = 1
						and ss.ZONE_ENTITY_STRUCTURE_FK is not null
						and ss.prod_segment_fk is not null
						and ss.cust_entity_structure_fk is null
						and ss.data_name_fk = 144908
						and isnull(p.ind_Cost_Change_Item, 0) = 1
						and ss.effective_date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + 7, getdate() - (@wk * 7)) as date)
						and ss.RECORD_STATUS_CR_FK = 1
						and ss.precedence is not null
					) a where DRank_PPTs = 1 
		) PP on P.ZONE_ENTITY_STRUCTURE_FK = PP.ZONE_ENTITY_STRUCTURE_FK and P.product_structure_fk = PP.PRODUCT_STRUCTURE_FK
	where P.ind_Cost_Change_Item = 1

	--get MinAmt at Zone level
	Update P
	Set MinAmt = isnull(PP.MinAmt, 0)
	from #Products P
	inner join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, ss.ZONE_ENTITY_STRUCTURE_FK, ss.attribute_number 'MinAmt', ss.effective_date, SEGMENTS_SETTINGS_ID
						, Dense_Rank()over(partition by ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) 'DRank'
						from epacube.segments_settings ss with (nolock)
						where 1 = 1
						and ss.ZONE_ENTITY_STRUCTURE_FK is not null
						and ss.prod_segment_fk is null
						and ss.cust_entity_structure_fk is null
						and ss.data_name_fk = 144908
						and ss.effective_date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + 7, getdate() - (@WK * 7)) as date)
						and ss.RECORD_STATUS_CR_FK = 1
						) A where DRank = 1

		) PP on P.ZONE_ENTITY_STRUCTURE_FK = PP.ZONE_ENTITY_STRUCTURE_FK
	where 1 = 1
		and isnull(P.ind_Cost_Change_Item, 0) = 1
		and P.MinAmt is null

--Get PreNotify Days (days out)
	Update P
	Set DaysOut = isnull(Daysout.daysout, 7)
	from #Products P
	left join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, ss.ZONE_ENTITY_STRUCTURE_FK, cast(ss.attribute_event_data as Numeric(18, 0)) 'DaysOut', ss.effective_date
						, Dense_Rank()over(partition by ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) 'DRank'
						from epacube.segments_settings ss with (nolock)
						where 1 = 1
						and cust_entity_structure_fk is null
						and ss.data_name_fk = 144868
						and ss.effective_date <= Cast(GETDATE() + 6 - (@WK * 7) - DATEPART(DW, GETDATE()) as date)
						and ss.RECORD_STATUS_CR_FK = 1
						) a where DRank = 1
		) DaysOut on DaysOut.ZONE_ENTITY_STRUCTURE_FK = P.ZONE_ENTITY_STRUCTURE_FK

	Update P
	Set Case_Cost_Cur = Pbv_c.value
	, Unit_Cost_Cur = Pbv_c.unit_value
	, pricesheet_basis_values_id_cur = pbv_c.pricesheet_basis_values_id
	from #Products P
		inner join 
			(Select * from (
							Select pbv.value, pbv.unit_value, pbv.Effective_Date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, p.zone_entity_structure_fk
							, dense_rank()over(partition by pbv.data_name_fk, p.zone_entity_structure_fk, pbv.product_structure_fk order by pbv.effective_date desc, pbv.pricesheet_basis_values_id desc) DRank
							from marginmgr.pricesheet_basis_values pbv with (nolock)
							inner join #Products P on pbv.product_structure_fk = P.product_structure_fk 
							where pbv.data_name_fk = 503201 
							and pbv.cust_entity_structure_fk is null
							and PBV.Effective_Date < cast(isnull(p.cost_change_effective_date_new, getdate() - (@WK * 7)) as date)
							and pbv.record_status_cr_fk = 1
							) a where DRank = 1
			) Pbv_c on P.[Product_Structure_FK] = Pbv_c.Product_Structure_FK and P.zone_entity_structure_fk = pbv_c.zone_entity_structure_fk

----Begin logging Pricing Steps increments of 10

--Calculate New Prices on all products with a cost change		Duration: 30 Seconds
	Update P
	--Set PRICE_Init = round(Cast(isnull(p.[unit_cost_new], P.[unit_cost_cur]) / (1 - P.TMpct) as Numeric(18, 6)) + 0.0049, 2)
	Set PRICE_Init = round(Cast(isnull(p.[unit_cost_new], P.[unit_cost_cur]) / (1 - P.TMpct) as Numeric(18, 6)), 2)
	from #Products P
	Where P.level_gross = 'Y'
	and isnull(P.ind_cost_change_item, 0) = 1

	Update P
	Set Digit_DN_FK = Case Cast(Right(PRICE_Init, 1) as int)	
															when 0 then 144897
															when 1 then 144898
															when 2 then 144899
															when 3 then 144901
															when 4 then 144902
															when 5 then 144903
															when 6 then 144904
															when 7 then 144905
															when 8 then 144906
															when 9 then 144907 end
	from #Products P
	Where P.level_gross = 'Y'
	and isnull(P.USE_LG_PRICE_POINTS, 'N') = 'Y'
	and isnull(P.ind_cost_change_item, 0) = 1

	Update P
	Set Price_Adj_Amt = adj.attribute_number
	from #Products P
	inner join 
		(Select attribute_number, product_structure_fk, ZONE_ENTITY_STRUCTURE_FK, data_name_fk 
			from (
					Select ss.attribute_number, sp.product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK, ss.data_name_fk, ss.precedence
					, Dense_Rank()over(partition by ss.ZONE_ENTITY_STRUCTURE_FK, sp.product_structure_fk, ss.data_name_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
					from epacube.epacube.segments_settings ss
					inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.PROD_SEGMENT_FK and ss.PROD_SEGMENT_DATA_NAME_FK = sp.DATA_NAME_FK
					inner join #Products a on sp.PRODUCT_STRUCTURE_FK = a.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = a.Zone_Entity_Structure_FK
					where 1 = 1
					and a.Digit_DN_FK = ss.data_name_fk
				) B where DRank = 1
		) Adj on P.product_structure_fk = adj.product_structure_fk and P.zone_entity_structure_fk = Adj.ZONE_ENTITY_STRUCTURE_FK and P.Digit_DN_FK= Adj.data_name_fk

	Update P
	Set Price_Adj_Amt = adj.attribute_number
	from #Products P
	inner join 
		(
			Select ss.attribute_number, ss.ZONE_ENTITY_STRUCTURE_FK, ss.data_name_fk 
			from epacube.epacube.segments_settings ss
			where 1 = 1 
			and ss.prod_segment_fk is null
			and ss.ZONE_ENTITY_STRUCTURE_FK is not null
			and ss.CUST_ENTITY_STRUCTURE_FK is null
			and ss.data_name_fk between 144897 and 144907
			and ss.data_name_fk <> 144900
		) Adj on P.zone_entity_structure_fk = Adj.ZONE_ENTITY_STRUCTURE_FK and P.Digit_DN_FK = Adj.DATA_NAME_FK
	where P.Price_Adj_Amt is null


Update P
Set Price_New = isnull(adj.attribute_number, P.PRICE_Init + isnull(P.Price_Adj_Amt, 0))
, Pricing_Update_Steps = isnull(Pricing_Update_Steps, '') + '10, '
from #Products P
left join 
			(
				select * 
				from (
						Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.zone_entity_structure_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
						, ss.lower_limit, ss.upper_limit, ss.Effective_Date, ss.UPDATE_TIMESTAMP
						, dense_rank()over(partition by ss.data_name_fk, ss.zone_entity_structure_fk, sp.product_structure_fk, ss.lower_limit order by ss.precedence, ss.effective_date desc, ss.update_timestamp desc) DRank
						from epacube.segments_settings ss with (nolock) 
						inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
						inner join #Products a on sp.PRODUCT_STRUCTURE_FK = a.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = a.Zone_Entity_Structure_FK
						left join epacube.data_value dv with (nolock) on sp.PROD_SEGMENT_FK = dv.data_value_id
						left join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
						where ss.data_name_fk = 144891
						and ss.CUST_ENTITY_STRUCTURE_FK is null
						and ss.RECORD_STATUS_CR_FK = 1
						and a.PRICE_Init + isnull(a.Price_Adj_Amt, 0) between ss.lower_limit and ss.upper_limit
						and ss.Effective_Date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + a.DaysOut, getdate() - (@WK * 7)) as date)
					) B where Drank = 1
			) adj on p.product_structure_fk = adj.product_structure_fk and p.zone_entity_structure_fk = adj.ZONE_ENTITY_STRUCTURE_FK
where isnull(P.level_gross, 'N') = 'Y'

--Set New Price to Current Price where a new price could not be calculated
	Update P
		Set Price_New = Case when isnull(level_gross, 'N') = 'N' then Null else Price_Cur end
		, Qualified_Status_CR_FK = 654
	, Pricing_Update_Steps = isnull(Pricing_Update_Steps, '') + '20, '
	, Qualified_Step = 20
	from #Products P
	where 1 = 1
	and (
			(ind_cost_change_item = 1 and isnull(level_gross, 'N') = 'Y' and price_new is null)
			Or
			isnull(level_gross, 'N') = 'N'
			Or
			Abs(Price_New - Price_Cur) < isnull(MinAmt, 0)
		)

--Set Private Label pricing where National Brand Cost Changes
	Update P1
		Set Price_New =	Cast(
			Case	when p2.price_new = p2.price_cur then p1.price_cur
					when (isnull(P1.USE_LG_PRICE_POINTS, 'N') = 'N' and isnull(P2.USE_LG_PRICE_POINTS, 'N') = 'N') then 
						Case when isnull(p1.PARITY_DISCOUNT_PCT, 0) > 0 then
																			P2.price_new * (1 - p1.parity_discount_pct)
							when isnull(p1.PARITY_DISCOUNT_AMT, 0) > 0 then
																			P2.price_new - p1.PARITY_DISCOUNT_AMT
						End 
					else
					[precision].[getretailpricezone]	(P1.zone_entity_structure_FK, p1.product_structure_fk
														,	Case	
																	when isnull(p1.PARITY_DISCOUNT_PCT, 0) > 0 then
																		P2.price_new * (1 - p1.parity_discount_pct)

																	when isnull(p1.PARITY_DISCOUNT_AMT, 0) > 0 then
																		P2.price_new - p1.PARITY_DISCOUNT_AMT
															End 
															, 7
															, @wk
														) 
				end As Numeric(18, 2))
		, Price_Mgmt_type_cr_fk = 650
		, Level_Gross = P2.Level_Gross 
		, Pricing_Update_Steps = isnull(P1.Pricing_Update_Steps, '') + '30, '
		, Qualified_Step = 30
		, Qualified_Status_CR_FK = P2.Qualified_Status_CR_FK
		, Price_Type_Step = 30
	from #Products P1
	inner join #Products P2 on P2.Parity_PL_Parent_Structure_FK = P1.Product_Structure_FK and P1.zone_entity_structure_fk = P2.zone_entity_structure_fk
	where isnull(P2.ind_cost_change_item, 0) = 1
	and (p1.Qualified_Status_CR_FK is null or p1.price_new is null)
	and (isnull(P1.Level_Gross, 'N') = 'Y' or isnull(P2.Level_Gross, 'N') = 'Y')

	
----ADJUST INDICATORS ON NATIONAL BRANDS WITH COST CHANGES IN PARITY WITH PRIVATE LABEL

		--Update Indicators where National Brand Parity product has cost changes
			Update P1
			Set Qualified_Status_CR_FK = Case when P1.price_cur = P1.price_new or Abs(P1.Price_New - P1.Price_cur) < isnull(P1.MinAmt, 0) then 654 else 653 end
			, Pricing_Update_Steps = isnull(P1.Pricing_Update_Steps, '') + '40, '
			, Qualified_Step = 40
			from #Products P1
			inner join #Products P2 on P2.Parity_PL_Parent_Structure_FK = P1.Product_Structure_FK and P1.zone_entity_structure_fk = P2.zone_entity_structure_fk
			where isnull(P2.ind_cost_change_item, 0) = 1
			and (isnull(P1.Level_Gross, 'N') = 'Y' or isnull(P2.Level_Gross, 'N') = 'Y')

			Update P2
			Set Qualified_Status_CR_FK = Case when P1.price_cur = P1.price_new or Abs(P1.Price_New - P1.Price_cur) < isnull(P1.MinAmt, 0) then 654 else 653 end
			, Pricing_Update_Steps = isnull(P2.Pricing_Update_Steps, '') + '50, '
			, Qualified_Step = 50
			from #Products P1
			inner join #Products P2 on P2.Parity_PL_Parent_Structure_FK = P1.Product_Structure_FK and P1.zone_entity_structure_fk = P2.zone_entity_structure_fk
			where isnull(P2.ind_cost_change_item, 0) = 1
			and (isnull(P1.Level_Gross, 'N') = 'Y' or isnull(P2.Level_Gross, 'N') = 'Y')

--Set New Price to Current Price, status to unqualified on Private Label Parity Products where cost or price has declined and National Brand cost has not changed
--and new price would be less than the defined differential between item and parity item.

	Update P1
	Set price_new = P1.price_cur
	, Qualified_Status_CR_FK = 654
	, Price_Mgmt_Type_CR_FK = 650
	, Pricing_Update_Steps = isnull(P1.Pricing_Update_Steps, '') + '60, '
	, Qualified_Step = 60
	, Price_Type_Step = 60
	from #Products P1
	inner join #Products P2 on P2.Parity_PL_Parent_Structure_FK = P1.Product_Structure_FK and P1.zone_entity_structure_fk = P2.zone_entity_structure_fk
	where isnull(P1.ind_cost_change_item, 0) = 1
	and 
		(
			isnull(P1.unit_cost_new, p1.unit_cost_cur) <= p1.unit_cost_cur
			or
			isnull(P1.price_new, p1.price_cur) <= p1.price_cur
		)
	and (isnull(P1.Level_Gross, 'N') = 'Y' or isnull(P2.Level_Gross, 'N') = 'Y')
	and (
			(isnull(p1.parity_discount_pct, 0) > 0 and p1.price_new < isnull(p2.price_new, p2.price_cur) * (1-P1.parity_discount_pct))
			or
			(isnull(p1.parity_discount_amt, 0) > 0 and p1.price_new < isnull(p2.price_new, p2.price_cur) - P1.parity_discount_amt)
		)

-- New Price to Current Price, status to unqualified on National Brand products in parity to Private Label products with cost or price declines where National has no changes
	Update P2
	Set price_new = P2.price_cur
	, Qualified_Status_CR_FK = 654
	, Price_Mgmt_Type_CR_FK = 650
	, Pricing_Update_Steps = isnull(P2.Pricing_Update_Steps, '') + '70, '
	, Qualified_Step = 70
	, Price_Type_Step = 70
	from #Products P1
	inner join #Products P2 on P2.Parity_PL_Parent_Structure_FK = P1.Product_Structure_FK and P1.zone_entity_structure_fk = P2.zone_entity_structure_fk
	where isnull(P1.ind_cost_change_item, 0) = 1
	and 
		(
			isnull(P1.unit_cost_new, p1.unit_cost_cur) <= p1.unit_cost_cur
			or
			isnull(P1.price_new, p1.price_cur) <= p1.price_cur
		)
	and (isnull(P1.Level_Gross, 'N') = 'Y' or isnull(P2.Level_Gross, 'N') = 'Y')

--Set National Brand pricing where Private Label Cost Changes and National Brand Cost does not change
	Update P2
		Set Price_New =	Cast(
			Case	when p1.price_new = p1.price_cur then p2.price_cur
					when isnull(P1.USE_LG_PRICE_POINTS, 'N') = 'N' then 
						Case when isnull(p1.PARITY_DISCOUNT_PCT, 0) > 0 then
																			P1.price_new / (1 - p1.parity_discount_pct)
							when isnull(p1.PARITY_DISCOUNT_AMT, 0) > 0 then
																			P1.price_new + p1.PARITY_DISCOUNT_AMT
						End 
					else
					[precision].[getretailpricezone]	(P1.zone_entity_structure_FK, p1.product_structure_fk
														,	Case	
																	when isnull(p1.PARITY_DISCOUNT_PCT, 0) > 0 then
																		P1.price_new / (1 - p1.parity_discount_pct)

																	when isnull(p1.PARITY_DISCOUNT_AMT, 0) > 0 then
																		P1.price_new + p1.PARITY_DISCOUNT_AMT
															End 
															, 7
															, @wk
														) 
				end As Numeric(18, 2))
		, Price_Mgmt_type_cr_fk = 650 
		, Qualified_Status_CR_FK = Case when p1.price_new = p1.price_cur or Abs(P1.Price_New - P1.Price_cur) < isnull(P1.MinAmt, 0) then 654 else 653 end
		, Pricing_Update_Steps = isnull(P2.Pricing_Update_Steps, '') + '80, '
		, Qualified_Step = 80
		, Price_Type_Step = 80
	from #Products P1
	inner join #Products P2 on P2.Parity_PL_Parent_Structure_FK = P1.Product_Structure_FK and P1.zone_entity_structure_fk = P2.zone_entity_structure_fk
	inner join #pbv pbv on p1.product_structure_fk = pbv.Product_Structure_FK
	where isnull(P1.ind_cost_change_item, 0) = 1
	and isnull(P2.ind_cost_change_item, 0) = 0
	and isnull(pbv.change_status, 0) = 0
	and (isnull(P1.Level_Gross, 'N') = 'Y' or isnull(P2.Level_Gross, 'N') = 'Y')


--Set Private Label pricing where PL Cost Changes, National Brand does not, and PL Cost or Price declines
	Update P1
		Set Price_New =	Cast(
			Case	--when p2.price_new = p2.price_cur then p1.price_cur
					when (isnull(P1.USE_LG_PRICE_POINTS, 'N') = 'N' and isnull(P2.USE_LG_PRICE_POINTS, 'N') = 'N') then 
						Case when isnull(p1.PARITY_DISCOUNT_PCT, 0) > 0 then
																			P2.price_new * (1 - p1.parity_discount_pct)
							when isnull(p1.PARITY_DISCOUNT_AMT, 0) > 0 then
																			P2.price_new - p1.PARITY_DISCOUNT_AMT
						End 
					else
					[precision].[getretailpricezone]	(P1.zone_entity_structure_FK, p1.product_structure_fk
														,	Case	
																	when isnull(p1.PARITY_DISCOUNT_PCT, 0) > 0 then
																		P2.price_new * (1 - p1.parity_discount_pct)

																	when isnull(p1.PARITY_DISCOUNT_AMT, 0) > 0 then
																		P2.price_new - p1.PARITY_DISCOUNT_AMT
															End 
															, 7
															, @wk
														) 
				end As Numeric(18, 2))
		, Price_Mgmt_type_cr_fk = 650
		, Level_Gross = P2.Level_Gross 
		, Pricing_Update_Steps = isnull(P1.Pricing_Update_Steps, '') + '85, '
		, Qualified_Step = 30
		, Qualified_Status_CR_FK = P2.Qualified_Status_CR_FK
		, Price_Type_Step = 30
	from #Products P1
	inner join #Products P2 on P2.Parity_PL_Parent_Structure_FK = P1.Product_Structure_FK and P1.zone_entity_structure_fk = P2.zone_entity_structure_fk
	where isnull(P2.ind_cost_change_item, 0) = 0 and isnull(P1.ind_cost_change_item, 0) = 1
	and (isnull(p1.cost_change_status, 0) = -1
		or [precision].[getretailpricezone]	(P1.zone_entity_structure_FK, p1.product_structure_fk
														,	Case	
																	when isnull(p1.PARITY_DISCOUNT_PCT, 0) > 0 then
																		P2.price_new * (1 - p1.parity_discount_pct)

																	when isnull(p1.PARITY_DISCOUNT_AMT, 0) > 0 then
																		P2.price_new - p1.PARITY_DISCOUNT_AMT
															End 
															, 7
															, @wk
														) 
		> Case when isnull(p1.PARITY_DISCOUNT_PCT, 0) > 0 then
																			P2.price_new * (1 - p1.parity_discount_pct)
							when isnull(p1.PARITY_DISCOUNT_AMT, 0) > 0 then
																			P2.price_new - p1.PARITY_DISCOUNT_AMT
						End 
		)
--	and (p1.Qualified_Status_CR_FK is null or p1.price_new is null)
	and (isnull(P1.Level_Gross, 'N') = 'Y' or isnull(P2.Level_Gross, 'N') = 'Y')



----ADJUST INDICATORS ON PRIVATE LABEL ITEMS WITH COST CHANGES IN PARITY WITH NATIONAL BRANDS
	--Set Statuses on Private Label parity items with cost changes
		Update P1
			Set Qualified_Status_CR_FK = Case when p1.price_cur = p1.price_new or Abs(P1.Price_New - P1.Price_cur) < isnull(P1.MinAmt, 0) then 654 else 653 end
			, Price_Mgmt_type_cr_fk = 650 
			--, Pricing_Update_Steps = isnull(P1.Pricing_Update_Steps, '') + '90, '
			, Qualified_Step = 90
			, Price_Type_Step = 90
		from #Products P1
		inner join #Products P2 on P2.Parity_PL_Parent_Structure_FK = P1.Product_Structure_FK and P1.zone_entity_structure_fk = P2.zone_entity_structure_fk
		where isnull(P1.ind_cost_change_item, 0) = 1
		and (P1.Qualified_Status_CR_FK is null or P1.Price_Mgmt_Type_CR_FK is null)
		and (isnull(P1.Level_Gross, 'N') = 'Y' or isnull(P2.Level_Gross, 'N') = 'Y')

		Update P2
			Set Qualified_Status_CR_FK = Case when isnull(P1.price_new, p1.price_cur) = p1.price_cur or Abs(P1.Price_New - P1.Price_cur) < isnull(P1.MinAmt, 0) then 654 else 653 end
			, Price_Mgmt_type_cr_fk = 650 
			, Pricing_Update_Steps = isnull(P1.Pricing_Update_Steps, '') + '100, '
			, Qualified_Step = 100
			, Price_Type_Step = 100
		from #Products P1
		inner join #Products P2 on P2.Parity_PL_Parent_Structure_FK = P1.Product_Structure_FK and P1.zone_entity_structure_fk = P2.zone_entity_structure_fk
		where isnull(P1.ind_cost_change_item, 0) = 1
		and (P2.Qualified_Status_CR_FK is null or P2.Price_Mgmt_Type_CR_FK is null)
		and (isnull(P1.Level_Gross, 'N') = 'Y' or isnull(P2.Level_Gross, 'N') = 'Y')

		Update P2
			Set Qualified_Status_CR_FK = Case when isnull(P1.price_new, p1.price_cur) = p1.price_cur or Abs(P1.Price_New - P1.Price_cur) < isnull(P1.MinAmt, 0) then 654 else 653 end
			, Price_Mgmt_type_cr_fk = 650 
			, Pricing_Update_Steps = isnull(P1.Pricing_Update_Steps, '') + '100, '
			, Qualified_Step = 101
			, Price_Type_Step = 101
		from #Products P1
		inner join #Products P2 on P2.Parity_PL_Parent_Structure_FK = P1.Product_Structure_FK and P1.zone_entity_structure_fk = P2.zone_entity_structure_fk
		where isnull(P1.ind_cost_change_item, 0) = 1
--		and (P2.Qualified_Status_CR_FK is null or P2.Price_Mgmt_Type_CR_FK is null)
		and (isnull(P2.Qualified_Status_CR_FK, 0) <> P1.Qualified_Status_CR_FK or P2.Price_Mgmt_Type_CR_FK is null)
		and (isnull(P1.Level_Gross, 'N') = 'Y' or isnull(P2.Level_Gross, 'N') = 'Y')

--Create Alias Header Records

Insert into #Products
(
Item_Change_Type_CR_FK, Zone_Entity_Structure_FK, Alias_Data_Value_FK, Insert_Step, Level_Gross, Price_Mgmt_Type_CR_FK
)
Select case isnull(@MarginChanges, 0) when 0 then 656 else 662 end 'Item_Change_Type_CR_FK', P.zone_entity_structure_fk, P.Alias_data_value_FK
, 1000 'Insert_Step', Max(isnull(Level_Gross, 'N')), Case Max(isnull(Level_Gross, 'N')) when 'N' then 651 else 650 end 'Price_Mgmt_Type_CR_FK'
from #Products P 
where P.alias_data_value_FK is not null 
group by P.alias_data_value_FK, P.zone_entity_structure_fk

--Set header values on alias header records where at least one item in the alias had a cost change

Update PA
Set Price_New = pi.price_new
, price_cur = pi.price_cur
, TMpct = pi.TMpct
, unit_cost_cur = pi.unit_cost_cur
, unit_cost_new = pi.unit_cost_new
, Case_Cost_Cur = pi.Case_Cost_Cur
, Case_Cost_New = pi.Case_Cost_New
, Level_Gross = isnull(pi.level_gross, 'N')
, Pricing_Update_Steps = isnull(pa.Pricing_Update_Steps, '') + '110, '
, ind_parity = Case when pi.ind_parity <> 0 then pi.ind_parity end
from #Products Pa
inner join
	(
	Select 
	pa.alias_data_value_fk, pa.zone_entity_structure_fk
	, Max(isnull(pi.price_new, pi.price_cur)) Price_New, Max(pi.price_cur) price_cur, Max(pi.TMpct) TMpct, Max(pi.unit_cost_cur) unit_cost_cur
	, Max(isnull(pi.unit_cost_new, pi.unit_cost_cur)) unit_cost_new
	, Max(pi.Case_Cost_Cur) Case_Cost_Cur, Max(isnull(pi.Case_Cost_New, pi.Case_Cost_Cur)) Case_Cost_New, Max(isnull(pi.level_gross, 'N')) 'Level_Gross'--, Min(pi.qualified_status_cr_fk) 'qualified_status_cr_fk'
	, Max(isnull(pi.ind_parity, 0)) ind_parity
	from #Products Pa
	inner join #Products Pi on Pa.alias_data_value_fk = pi.alias_data_value_fk and Pa.zone_entity_structure_fk = Pi.zone_entity_structure_fk
	where Pa.Item_Change_Type_CR_FK in (656, 662)
	and isnull(pi.Item_Change_Type_CR_FK, 0) not in (656, 662)
	group by pa.alias_data_value_fk, pa.zone_entity_structure_fk
	) pi on pa.alias_data_value_fk = pi.alias_data_value_fk and pa.zone_entity_structure_fk = pi.zone_entity_structure_fk and Pa.Item_Change_Type_CR_FK = 656

--Set header values on alias header records where NO items in the alias had a cost change
Update PA
Set Price_New = pi.price_new
, price_cur = pi.price_cur
, TMpct = pi.TMpct
, unit_cost_cur = pi.unit_cost_cur
, unit_cost_new = pi.unit_cost_new
, Case_Cost_Cur = pi.Case_Cost_Cur
, Case_Cost_New = pi.Case_Cost_New
, Level_Gross = isnull(pi.level_gross, 'N')
, ind_parity = pi.ind_parity
, Pricing_Update_Steps = isnull(pa.Pricing_Update_Steps, '') + '120'
from #Products Pa
inner join
	(
	Select 
	pa.alias_data_value_fk, pa.zone_entity_structure_fk, Max(pi.price_new) Price_New, Max(pi.price_cur) price_cur, Max(pi.TMpct) TMpct, Max(pi.unit_cost_cur) unit_cost_cur, Max(pi.unit_cost_new) unit_cost_new, Max(pi.Case_Cost_Cur) Case_Cost_Cur, Max(pi.Case_Cost_New) Case_Cost_New, Max(isnull(pi.level_gross, 'N')) 'Level_Gross'
	, Max(isnull(pi.ind_parity, 0)) ind_parity
	from #Products Pa
	inner join #Products Pi on Pa.alias_data_value_fk = pi.alias_data_value_fk and Pa.zone_entity_structure_fk = Pi.zone_entity_structure_fk
	where Pa.Item_Change_Type_CR_FK in (656, 662)
	and isnull(pi.Item_Change_Type_CR_FK, 0) not in (656, 662)
	and pa.alias_data_value_fk not in (Select alias_data_value_fk from #products where Alias_Data_Value_fk = Pa.alias_data_value_fk and zone_entity_structure_fk = pa.zone_entity_structure_fk and isnull(ind_cost_change_item, 0) = 1)
	and Pa.Level_Gross = 'Y'
	group by pa.alias_data_value_fk, pa.zone_entity_structure_fk
	) pi on pa.alias_data_value_fk = pi.alias_data_value_fk and pa.zone_entity_structure_fk = pi.zone_entity_structure_fk and Pa.Item_Change_Type_CR_FK in (656, 662)

----Update Alias_Children to Parent Pricing aValues
	Update pi
	Set Price_New = pa.price_new
	, price_cur = pa.price_cur
	, TMpct = pa.TMpct
	, ind_cost_delta = Case When pi.unit_cost_new <> pa.unit_cost_new then 2 else Null end
	, Level_Gross = pa.level_gross
	, Pricing_Update_Steps = isnull(pi.Pricing_Update_Steps, '') + '130'
		from #Products Pa
		inner join #Products Pi on Pa.alias_data_value_fk = pi.alias_data_value_fk and Pa.zone_entity_structure_fk = Pi.zone_entity_structure_fk
		where Pa.Item_Change_Type_CR_FK in (656, 662)
		and isnull(pi.Item_Change_Type_CR_FK, 0) not in (656, 662)
		and pa.level_gross = 'Y'

	Update P2
	Set Price_Mgmt_Type_CR_FK = Case	when isnull(P1.ind_cost_change_item, 0) = 1 and isnull(P1.Level_Gross, 'N') = 'N' then 651
										when isnull(P2.ind_cost_change_item, 0) = 1 and isnull(P2.Level_Gross, 'N') = 'N' then 651
										when isnull(P3.ind_cost_change_item, 0) = 1 and isnull(P3.Level_Gross, 'N') = 'N' then 651 else 650 end
	, Qualified_Status_CR_FK = Case		when isnull(P1.ind_cost_change_item, 0) = 1 and (isnull(P1.Level_Gross, 'N') = 'N' or isnull(P1.Price_New, P1.Price_Cur) = P1.Price_Cur) then 654
										when isnull(P2.ind_cost_change_item, 0) = 1 and (isnull(P2.Level_Gross, 'N') = 'N' or isnull(P2.Price_New, P2.Price_Cur) = P2.Price_Cur) then 654
										when isnull(P3.ind_cost_change_item, 0) = 1 and (isnull(P3.Level_Gross, 'N') = 'N' or isnull(P3.Price_New, P3.Price_Cur) = P1.Price_Cur) then 654 else 653 end
	, Level_Gross =		Case			when isnull(P1.ind_cost_change_item, 0) = 1 then isnull(P1.Level_Gross, 'N')
										when isnull(P2.ind_cost_change_item, 0) = 1 then isnull(P2.Level_Gross, 'N')
										when isnull(P3.ind_cost_change_item, 0) = 1 then isnull(P3.Level_Gross, 'N') end
	, Price_Type_Step = '140'
	, Qualified_Step = '140'
	from #products P1
	inner join #Products P2 on p2.parity_pl_parent_structure_fk = P1.product_structure_fk and P2.zone_entity_structure_fk = p1.zone_entity_structure_fk
	inner join #products p3 on p3.parity_alias_pl_parent_structure_fk = P2.product_structure_fk and P3.zone_entity_structure_fk = p1.zone_entity_structure_fk

	Update P3
	Set Price_Mgmt_Type_CR_FK = Case	when isnull(P1.ind_cost_change_item, 0) = 1 and isnull(P1.Level_Gross, 'N') = 'N' then 651
										when isnull(P2.ind_cost_change_item, 0) = 1 and isnull(P2.Level_Gross, 'N') = 'N' then 651
										when isnull(P3.ind_cost_change_item, 0) = 1 and isnull(P3.Level_Gross, 'N') = 'N' then 651 else 650 end
	, Qualified_Status_CR_FK = Case		when isnull(P1.ind_cost_change_item, 0) = 1 and (isnull(P1.Level_Gross, 'N') = 'N' or isnull(P1.Price_New, P1.Price_Cur) = P1.Price_Cur) then 654
										when isnull(P2.ind_cost_change_item, 0) = 1 and (isnull(P2.Level_Gross, 'N') = 'N' or isnull(P2.Price_New, P2.Price_Cur) = P2.Price_Cur) then 654
										when isnull(P3.ind_cost_change_item, 0) = 1 and (isnull(P3.Level_Gross, 'N') = 'N' or isnull(P3.Price_New, P3.Price_Cur) = P1.Price_Cur) then 654 else 653 end
	, Level_Gross =		Case			when isnull(P1.ind_cost_change_item, 0) = 1 then isnull(P1.Level_Gross, 'N')
										when isnull(P2.ind_cost_change_item, 0) = 1 then isnull(P2.Level_Gross, 'N')
										when isnull(P3.ind_cost_change_item, 0) = 1 then isnull(P3.Level_Gross, 'N') end

	, Price_New =						Case when P3.Price_New is null then P2.Price_New else P3.Price_New end
	, Pricing_Update_Steps = P3.Pricing_Update_Steps + '150, '
	, Price_Type_Step = 150
	, Qualified_Step = 150
	from #products P1
	inner join #Products P2 on p2.parity_pl_parent_structure_fk = P1.product_structure_fk and P2.zone_entity_structure_fk = p1.zone_entity_structure_fk
	inner join #products p3 on p3.parity_alias_pl_parent_structure_fk = P2.product_structure_fk and P3.zone_entity_structure_fk = p1.zone_entity_structure_fk

--Remove records with cost increases beyond zone limits
	Delete from #Products where isnull(cost_change_effective_date_new, getdate() - (@Wk * 7)) > Cast(GETDATE() + 6 - (@Wk * 7) + isnull(daysout, 7) - DATEPART(DW, GETDATE()) as date) and unit_cost_new > unit_cost_cur

--Remove Alias Headers from regular items
	Delete P from #Products P 
	where 1 = 1
	and item_change_type_cr_fk in (656, 662)
	and Alias_Data_Value_FK in (select alias_data_value_fk from #Products where item_change_type_cr_fk = case isnull(@MarginChanges, 0) when 0 then 655 else 661 end and zone_entity_structure_fk = P.zone_entity_structure_fk)

	Update Tbl
	Set Subgroup_Data_Value_FK = dv.data_value_id
	, Subgroup = dv.[value]
	, Subgroup_Description = dv.[description]
	, Notification_Date = cast(pbv.create_timestamp as date)
	from #Products Tbl
	left join epacube.segments_product sp with (nolock) on tbl.product_structure_fk = sp.product_structure_fk and sp.data_name_fk = 501045
	left join epacube.data_value dv with (nolock) on sp.prod_segment_fk = dv.data_value_id
	left join #pbv pbv on tbl.product_structure_fk = pbv.product_structure_fk

	Update P
	Set Item = pi.value
	, item_description = pd.[description]
	from #Products P
	inner join epacube.product_identification pi with (nolock) on p.product_structure_fk = pi.product_structure_fk and pi.data_name_fk = 110100
	left join epacube.product_description pd with (nolock) on P.product_structure_fk = pd.product_structure_fk and pd.data_name_fk = 110401

	 Update P
	 Set Level_Gross = Case isnull(sz_144862.ATTRIBUTE_Y_N, 'N') when 'Y' then sz_500015.ATTRIBUTE_Y_N else 'N' end
	 from #Products P
  		left join 
			(select * from (
							
							Select distinct ss.ATTRIBUTE_Y_N, sp.product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK
							, dense_rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK, sp.product_structure_fk order by ss.attribute_y_n, ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
							from epacube.segments_settings ss with (nolock) 
							inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
							inner join #Products zc on sp.product_structure_fk = zc.PRODUCT_STRUCTURE_FK and ss.ZONE_ENTITY_STRUCTURE_FK = zc.ZONE_ENTITY_STRUCTURE_FK
							where ss.data_name_fk = 500015
							and ss.RECORD_STATUS_CR_FK = 1
							) a where Drank = 1
			) sz_500015 on P.product_structure_fk = sz_500015.product_structure_fk and sz_500015.ZONE_ENTITY_STRUCTURE_FK = p.ZONE_ENTITY_STRUCTURE_FK
		left join 
			(select * from (
							Select ss.ATTRIBUTE_Y_N, ss.ZONE_ENTITY_STRUCTURE_FK, ss.SEGMENTS_SETTINGS_ID
							, dense_rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) DRank
							from epacube.segments_settings ss with (nolock) 
							where ss.data_name_fk = 144862
							and ss.ZONE_ENTITY_STRUCTURE_FK is not null 
							and CUST_ENTITY_STRUCTURE_FK is null
							and ss.RECORD_STATUS_CR_FK = 1
							) a where Drank = 1
			) sz_144862 on sz_144862.ZONE_ENTITY_STRUCTURE_FK = P.ZONE_ENTITY_STRUCTURE_FK

--Fully reset statuses on regular items
	Update P
	Set Price_Mgmt_Type_CR_FK = Case	when isnull(Level_Gross, 'N') = 'N' then 651 else 650 end
	, Qualified_Status_CR_FK = Case		when isnull(Level_Gross, 'N') = 'N' then 654
										when isnull(price_new, price_cur) = isnull(price_cur, 0) then 654 --or isnull(price_cur, 0) = 0 then 654
										else 653 end
	, Price_Type_Step = 200
	, Qualified_Step = 200
	from #products P
	where P.item_change_type_cr_fk = case isnull(@MarginChanges, 0) when 0 then 655 else 661 end 

	Update P1
	Set Price_Mgmt_Type_CR_FK = Case	when isnull(p1.Level_Gross, 'N') = 'N' and isnull(p2.Level_Gross, 'N') = 'N' and isnull(p3.Level_Gross, 'N') = 'N' then 651 else 650 end
	, Qualified_Status_CR_FK = Case		when isnull(P1.Level_Gross, 'N') = 'N' and isnull(P2.Level_Gross, 'N') = 'N' and isnull(P3.Level_Gross, 'N') = 'N' then 654
										when isnull(p1.price_new, p1.price_cur) = isnull(p1.price_cur, 0)
											and isnull(p2.price_new, p2.price_cur) = isnull(p2.price_cur, 0)
											and isnull(p3.price_new, p3.price_cur) = isnull(p3.price_cur, 0)
										 then 654 else 653 end
	, Price_Type_Step = 201
	, Qualified_Step = 201
	from #products p1
	inner join #products p2 on p2.parity_pl_parent_structure_fk = p1.product_structure_fk and p1.Zone_Entity_Structure_FK = p2.Zone_Entity_Structure_FK
	inner join #products p3 on P3.parity_alias_pl_Parent_Structure_FK = p2.Product_structure_FK and p1.Zone_Entity_Structure_FK = p3.Zone_Entity_Structure_FK and p3.product_structure_fk <> p2.product_structure_fk

	Update P2
	Set Price_Mgmt_Type_CR_FK = Case	when isnull(p1.Level_Gross, 'N') = 'N' and isnull(p2.Level_Gross, 'N') = 'N' and isnull(p3.Level_Gross, 'N') = 'N' then 651 else 650 end
	, Qualified_Status_CR_FK = Case		when isnull(P1.Level_Gross, 'N') = 'N' and isnull(P2.Level_Gross, 'N') = 'N' and isnull(P3.Level_Gross, 'N') = 'N' then 654
										when isnull(p1.price_new, p1.price_cur) = isnull(p1.price_cur, 0)
											and isnull(p2.price_new, p2.price_cur) = isnull(p2.price_cur, 0)
											and isnull(p3.price_new, p3.price_cur) = isnull(p3.price_cur, 0)
										 then 654 else 653 end
	, Price_Type_Step = 202
	, Qualified_Step = 202
	from #products p1
	inner join #products p2 on p2.parity_pl_parent_structure_fk = p1.product_structure_fk and p1.Zone_Entity_Structure_FK = p2.Zone_Entity_Structure_FK
	inner join #products p3 on P3.parity_alias_pl_Parent_Structure_FK = p2.Product_structure_FK and p1.Zone_Entity_Structure_FK = p3.Zone_Entity_Structure_FK and p3.product_structure_fk <> p2.product_structure_fk
	
	Update P3
	Set Price_Mgmt_Type_CR_FK = Case	when isnull(p1.Level_Gross, 'N') = 'N' and isnull(p2.Level_Gross, 'N') = 'N' and isnull(p3.Level_Gross, 'N') = 'N' then 651 else 650 end
	, Qualified_Status_CR_FK = Case		when isnull(P1.Level_Gross, 'N') = 'N' and isnull(P2.Level_Gross, 'N') = 'N' and isnull(P3.Level_Gross, 'N') = 'N' then 654
										when isnull(p1.price_new, p1.price_cur) = isnull(p1.price_cur, 0)
											and isnull(p2.price_new, p2.price_cur) = isnull(p2.price_cur, 0)
											and isnull(p3.price_new, p3.price_cur) = isnull(p3.price_cur, 0)
										 then 654 else 653 end
	, Price_Type_Step = 203
	, Qualified_Step = 203
	from #products p1
	inner join #products p2 on p2.parity_pl_parent_structure_fk = p1.product_structure_fk and p1.Zone_Entity_Structure_FK = p2.Zone_Entity_Structure_FK
	inner join #products p3 on P3.parity_alias_pl_Parent_Structure_FK = p2.Product_structure_FK and p1.Zone_Entity_Structure_FK = p3.Zone_Entity_Structure_FK and p3.product_structure_fk <> p2.product_structure_fk

	Update P1
	Set Price_Mgmt_Type_CR_FK = Case	when isnull(p1.Level_Gross, 'N') = 'N' and isnull(p2.Level_Gross, 'N') = 'N' then 651 else 650 end
	, Qualified_Status_CR_FK = Case		when isnull(P1.Level_Gross, 'N') = 'N' and isnull(P2.Level_Gross, 'N') = 'N' then 654
										when isnull(p1.price_new, p1.price_cur) = isnull(p1.price_cur, 0)
											and isnull(p2.price_new, p2.price_cur) = isnull(p2.price_cur, 0)
										 then 654 else 653 end
	, Price_Type_Step = 211
	, Qualified_Step = 211
	from #products p1
	inner join #products p2 on p2.parity_pl_parent_structure_fk = p1.product_structure_fk and p1.Zone_Entity_Structure_FK = p2.Zone_Entity_Structure_FK
	left join #products p3 on P3.parity_alias_pl_Parent_Structure_FK = p2.Product_structure_FK and p1.Zone_Entity_Structure_FK = p3.Zone_Entity_Structure_FK
	where p3.product_structure_fk is null

	Update P2
	Set Price_Mgmt_Type_CR_FK = Case	when isnull(p1.Level_Gross, 'N') = 'N' and isnull(p2.Level_Gross, 'N') = 'N' then 651 else 650 end
	, Qualified_Status_CR_FK = Case		when isnull(P1.Level_Gross, 'N') = 'N' and isnull(P2.Level_Gross, 'N') = 'N' then 654
										when isnull(p1.price_new, p1.price_cur) = isnull(p1.price_cur, 0)
											and isnull(p2.price_new, p2.price_cur) = isnull(p2.price_cur, 0)
										 then 654 else 653 end
	, Price_Type_Step = 212
	, Qualified_Step = 212
	from #products p1
	inner join #products p2 on p2.parity_pl_parent_structure_fk = p1.product_structure_fk and p1.Zone_Entity_Structure_FK = p2.Zone_Entity_Structure_FK
	left join #products p3 on P3.parity_alias_pl_Parent_Structure_FK = p2.Product_structure_FK and p1.Zone_Entity_Structure_FK = p3.Zone_Entity_Structure_FK
	where p3.product_structure_fk is null

-----------------------------------------------------------------------------------------------------------

--Declare @MarginChanges int = 0	
--Declare @Wk int = 0

	If Object_ID('tempdb..#Aliases_Adj') is not null
	drop table #Aliases_Adj

	If Object_ID('tempdb..#Products_Adj') is not null
	drop table #Products_Adj

	If object_id('tempdb..#Aliases') is not null
	drop table #Aliases

	Create table #Aliases(Insert_Step int not null, [Item] varchar(16) Null, Product_structure_FK bigint null, Parity_PL_Parent_Structure_FK bigint null, PARITY_ALIAS_PL_PARENT_STRUCTURE_FK bigint Null, Parity_Alias_Data_Value_FK varchar(64) null--, Alias_Parity_Alias_PL_Parent_Structure_FK bigint null
	, Alias_Data_Value_FK varchar(64) null, ind_Cost_Change_Item int null, Cost_Change_Status int null
	, Parity_Child int null, ind_Parity int Null, Price_Cur money null, Price_New money null, Unit_Cost_Cur money null, Unit_Cost_New money null, TMpct Numeric(18, 4) Null, Level_Gross varchar(1) Null
	, Item_Change_Type_CR_FK int null, Qualified_Status_CR_FK int null, Price_Mgmt_Type_CR_FK int null, Parity_SIZE_Parent_Structure_FK bigint null, Zone_Entity_Structure_FK bigint null, Item_Description Varchar(64) Null
	, Case_Cost_New money null, Cost_Change_Effective_Date_New date null, Pack int null, Pricesheet_basis_Values_ID_New bigint null, PARITY_DISCOUNT_AMT money null
	, PARITY_DISCOUNT_PCT Numeric(18, 2) Null, Parity_Mult Int null, Parity_Type Varchar(64) Null, Reason_For_Item Varchar(64) Null, Price_Multiple_Cur int null, Price_Effective_Date_Cur date null, [Use_LG_Price_Points] varchar(1) Null
	, MinAmt Numeric(18, 2) Null, DaysOut int null, Case_Cost_Cur money null, Cost_Change_Effective_Date_Cur date null, [ind_cost_delta] int null, Pricesheet_Basis_Values_ID_Cur bigint Null, Reason_For_Inclusion varchar(64) null
	, Subgroup_Data_Value_FK bigint null, Subgroup varchar(64) Null, Subgroup_Description varchar(64) Null, Notification_Date date Null, [Pricing_Update_Steps] varchar(Max) Null, Price_Type_Step int null, Qualified_Step int null
	, Price_Init Numeric(18, 2) Null, Digit_DN_FK bigint null, Price_Adj_Amt Numeric(18, 2) Null, Price_Set_Step int null, Final_Pricing_Step varchar(max) null
	)

	alter table #Aliases add [Aliases_ID] bigint identity(1, 1) not null

	create index idx_01 on #Aliases(zone_entity_structure_fk, product_structure_fk)
	create unique index idx_00 on #Aliases([Aliases_ID])

--Add Private Label item in parity with Alias to Private Label (Private Label item also in an alias)
	Insert into #Aliases
	(Item_Change_Type_CR_FK, Product_structure_FK, Zone_Entity_Structure_FK, Alias_Data_Value_FK, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion, Cost_Change_Status)
	Select 657 'Item_Change_Type_CR_FK', pa.product_structure_fk 'product_structure_fk'
		, pa.entity_structure_fk 'zone_entity_structure_fk'
		, pmt.data_value_fk 'Alias_Data_Value_FK'
		, (Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk) 'IND_COST_CHANGE_ITEM'
		, (Select top 1 value from #PBV where product_structure_fk = pa.product_structure_fk) 'Case_Cost_New'
		, (Select top 1 unit_value from #PBV where product_structure_fk = pa.product_structure_fk) 'Unit_Cost_New'
		, (Select top 1 effective_date from #PBV where product_structure_fk = pa.product_structure_fk) 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'pack'
		, (Select top 1 Pricesheet_Basis_Values_ID from #PBV where product_structure_fk = pa.product_structure_fk) 'Pricesheet_Basis_Values_ID_New'
		, 1 'ind_parity'
		, 10 'Insert_Step', 'PL for National Cost Change' 'Parity_Type', 'PL in Alias' 'Reason_For_Item', PBV.PBV_Status 'Reason_For_Inclusion'
		, (Select top 1 change_status from #PBV where product_structure_fk = pa.product_structure_fk) 'Cost_Change_Status'
	from #pbv pbv 
	inner join epacube.product_association pa with (nolock) on pbv.product_structure_fk = pa.child_product_structure_fk and pa.data_name_fk in (159903) and pa.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	inner join epacube.product_mult_type pmt with (nolock) on pa.product_structure_fk = pmt.product_structure_fk and pa.entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019
	inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	inner join epacube.product_association paP with (nolock) on pbv.product_structure_fk = paP.product_structure_fk and paP.entity_structure_fk = pa.entity_structure_fk and paP.data_name_fk = 159450 and paP.record_status_cr_fk = 1
	left join epacube.product_attribute pa_500001 with (nolock) on pbv.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Aliases P1 on pa.product_structure_fk = p1.product_structure_fk and pa.entity_structure_fk = p1.zone_entity_structure_fk
	left join #Products prods on pa.product_structure_fk = prods.product_structure_fk  and pa.entity_structure_fk = prods.zone_entity_structure_fk
	where dv.value in ('H') --('H') --('R', 'H')
	and P1.Aliases_ID is null
	and prods.Products_ID is null

--Add the National Brand Items with the cost change - No Aliases Involved
	Insert into #Aliases
	(Item_Change_Type_CR_FK, Product_structure_FK, Parity_PL_Parent_Structure_FK, Parity_Child, Zone_Entity_Structure_FK, Alias_Data_Value_FK, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion, Cost_Change_Status)
	Select 657 'Item_Change_Type_CR_FK', pa.child_product_structure_fk 'product_structure_fk'
		, pa.product_structure_fk 'parity_pl_parent_structure_fk', 1 'parity_child', pa.entity_structure_fk 'zone_entity_structure_fk', pmt.data_value_fk 'Alias_Data_Value_FK'
		, (Select 1 from #PBV where product_structure_fk = pa.child_product_structure_fk) 'IND_COST_CHANGE_ITEM'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv.value else null end 'Case_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv.Unit_value else null end 'Unit_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv.Effective_Date else Null end 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'pack'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv.Pricesheet_Basis_Values_ID else Null end 'Pricesheet_Basis_Values_ID_New'
		, 1 'ind_parity'
		, 20 'Insert_Step', 'National Cost Change' 'Parity_Type', 'Parity without Alias' 'Reason_For_Item', PBV.PBV_Status 'Reason_For_Inclusion'
		, (Select top 1 change_status from #PBV where product_structure_fk = pa.child_product_structure_fk) 'Cost_Change_Status'
	from #pbv pbv 
	inner join epacube.product_association pa with (nolock) on pbv.product_structure_fk = pa.child_product_structure_fk and pa.data_name_fk in (159903) and pa.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	left join epacube.product_mult_type pmt with (nolock) on Pbv.product_structure_fk = pmt.product_structure_fk and pa.entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019
	inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.product_association paP with (nolock) on pbv.product_structure_fk = paP.product_structure_fk and paP.entity_structure_fk = pa.entity_structure_fk and paP.data_name_fk = 159450 and paP.record_status_cr_fk = 1
	inner join epacube.product_association paC with (nolock) on pa.child_product_structure_fk = paC.product_structure_fk and paC.entity_structure_fk = pa.entity_structure_fk and paC.data_name_fk = 159450 and paC.record_status_cr_fk = 1
	inner join epacube.product_association pap2 with (nolock) on pa.PRODUCT_STRUCTURE_FK = pap2.product_structure_fk and pa.ENTITY_STRUCTURE_FK = pap2.ENTITY_STRUCTURE_FK and pap2.data_name_fk = 159450 and pap2.RECORD_STATUS_CR_FK = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	left join epacube.product_attribute pa_500001 with (nolock) on pbv.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Aliases P1 on pa.child_product_structure_fk = p1.product_structure_fk and pa.entity_structure_fk = p1.zone_entity_structure_fk
	left join #Products prods on pa.child_product_structure_fk = prods.product_structure_fk  and pa.entity_structure_fk = prods.zone_entity_structure_fk
	Where 1 = 1
	and dv.value in ('H') --('H') --('R', 'H')
	and p1.Aliases_ID is null
	and prods.Products_ID is null

--Add private label parity items having a cost change - With Alias
	Insert into #Aliases
	(Item_Change_Type_CR_FK, Product_structure_FK, Zone_Entity_Structure_FK, Alias_Data_Value_FK, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion, Cost_Change_Status)
	Select 657 'Item_Change_Type_CR_FK', pa.product_structure_fk 'product_structure_fk'
		, pa.entity_structure_fk 'zone_entity_structure_fk'
		, pmt.data_value_fk 'Alias_Data_Value_FK'
		, (Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk) 'IND_COST_CHANGE_ITEM'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.value else null end 'Case_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.Unit_value else null end 'Unit_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.Effective_Date else Null end 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'pack'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.product_structure_fk), 0) when 1 then pbv.Pricesheet_Basis_Values_ID else Null end 'Pricesheet_Basis_Values_ID_New'
		, 1 'ind_parity'
		, 30 'Insert_Step', 'Private Label Cost Change' 'Parity_Type', 'Parity without Alias' 'Reason_For_Item', PBV.PBV_Status 'Reason_For_Inclusion'
		, (Select top 1 change_status from #PBV where product_structure_fk = pa.product_structure_fk) 'IND_COST_CHANGE_ITEM'
	from #pbv pbv 
	inner join epacube.product_association pa with (nolock) on pbv.product_structure_fk = pa.product_structure_fk and pa.data_name_fk in (159903) and pa.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	inner join epacube.product_association paP with (nolock) on pbv.product_structure_fk = paP.product_structure_fk and paP.entity_structure_fk = pa.entity_structure_fk and paP.data_name_fk = 159450 and paP.record_status_cr_fk = 1
	inner join epacube.product_mult_type pmt with (nolock) on pa.product_structure_fk = pmt.product_structure_fk and pa.entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019
	inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	left join epacube.product_attribute pa_500001 with (nolock) on pbv.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Aliases P1 on pa.product_structure_fk = p1.product_structure_fk and pa.entity_structure_fk = p1.zone_entity_structure_fk
	left join #Products prods on pa.product_structure_fk = prods.product_structure_fk  and pa.entity_structure_fk = prods.zone_entity_structure_fk
	where dv.value in ('H') --('H') --('R', 'H')
	and p1.Aliases_ID is null
	and prods.Products_ID is null

--Add National Brand items in parity with Private Label items with cost changes - National Brand Alias identified
	Insert into #Aliases
	(Item_Change_Type_CR_FK, Product_structure_FK, Parity_PL_Parent_Structure_FK, Parity_Child, Zone_Entity_Structure_FK, alias_data_value_fk, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion, Cost_Change_Status)
	Select 657 'Item_Change_Type_CR_FK', pa.child_product_structure_fk 'product_structure_fk'
		, pa.product_structure_fk 'Parity_PL_Parent_Structure_FK'
		, 1 'Parity_Child'
		, pa.entity_structure_fk 'zone_entity_structure_fk'
		, pmt.data_value_fk 'alias_data_value_fk'
		, (Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk) 'IND_COST_CHANGE_ITEM'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv.value else null end 'Case_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv.Unit_value else null end 'Unit_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv.Effective_Date else Null end 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'pack'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa.child_product_structure_fk), 0) when 1 then pbv.Pricesheet_Basis_Values_ID else Null end 'Pricesheet_Basis_Values_ID_New'
		, 1 'ind_parity'
		, 40 'Insert_Step', 'National Items with PL Cost Changes' 'Parity_Type', 'Child_Parity with or without Alias' 'Reason_For_Item', PBV.PBV_Status 'Reason_For_Inclusion'
		, (Select top 1 change_status from #PBV where product_structure_fk = pa.child_product_structure_fk) 'cost_change_status'
	from #pbv pbv 
	inner join epacube.product_association pa with (nolock) on pbv.product_structure_fk = pa.product_structure_fk and pa.data_name_fk in (159903) and pa.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	inner join epacube.product_association paP with (nolock) on pbv.product_structure_fk = paP.product_structure_fk and paP.entity_structure_fk = pa.entity_structure_fk and paP.data_name_fk = 159450 and paP.record_status_cr_fk = 1
	inner join epacube.product_association paC with (nolock) on pa.child_product_structure_fk = paC.product_structure_fk and paC.entity_structure_fk = pa.entity_structure_fk and paC.data_name_fk = 159450 and paC.record_status_cr_fk = 1
	inner join epacube.product_association pap2 with (nolock) on pa.PRODUCT_STRUCTURE_FK = pap2.product_structure_fk and pa.ENTITY_STRUCTURE_FK = pap2.ENTITY_STRUCTURE_FK and pap2.data_name_fk = 159450 and pap2.RECORD_STATUS_CR_FK = 1
	left join epacube.product_mult_type pmt with (nolock) on pa.child_product_structure_fk = pmt.product_structure_fk and pa.entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019
	inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	left join epacube.product_attribute pa_500001 with (nolock) on pbv.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Aliases P1 on pa.child_product_structure_fk = p1.product_structure_fk and pa.entity_structure_fk = p1.zone_entity_structure_fk
	left join #Products prods on pa.product_structure_fk = prods.product_structure_fk  and pa.entity_structure_fk = prods.zone_entity_structure_fk
	where 1 = 1
	and dv.value in ('H') --('R', 'H')
	and p1.Aliases_ID is null
	and prods.Products_ID is null

--Add items with cost changes that are in an Alias not involved in Parity
	Insert into #Aliases
	(Item_Change_Type_CR_FK, Product_structure_FK, Zone_Entity_Structure_FK, alias_data_value_fk, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion, Cost_Change_Status)
	Select 657 'Item_Change_Type_CR_FK', pbv.product_structure_fk 'product_structure_fk'
		, pa.entity_structure_fk 'zone_entity_structure_fk'
		, pmt.data_value_fk 'Alias_Data_Value_FK'
		, 1 IND_COST_CHANGE_ITEM
		, pbv.value 'Case_Cost_New'
		, pbv.unit_value 'Unit_Cost_New'
		, pbv.Effective_Date 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'pack'
		, pbv.Pricesheet_Basis_Values_ID 'Pricesheet_Basis_Values_ID_New'
		, 60 'Insert_Step', 'None' 'Parity_Type', 'Regular Item without Parity Without Alias' 'Reason_For_Item', PBV.PBV_Status 'Reason_For_Inclusion'
		, pbv.Change_Status
	from #pbv pbv 
	inner join epacube.product_association pa with (nolock) on pbv.product_structure_fk = pa.product_structure_fk and pa.data_name_fk in (159450) and pa.record_status_cr_fk = 1
	left join epacube.product_association pa_159903 with (nolock) on pa.product_structure_fk = pa_159903.product_structure_fk and pa.entity_structure_fk = pa_159903.entity_structure_fk and pa_159903.data_name_fk = 159903 and pa_159903.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	inner join epacube.product_mult_type pmt with (nolock) on pa.product_structure_fk = pmt.product_structure_fk and pa.entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019
	inner join epacube.entity_attribute ea with (nolock) on pa.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	left join epacube.product_attribute pa_500001 with (nolock) on pbv.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Aliases P1 on pa.product_structure_fk = p1.product_structure_fk and pa.entity_structure_fk = p1.zone_entity_structure_fk
	left join #Products prods on pa.product_structure_fk = prods.product_structure_fk  and pa.entity_structure_fk = prods.zone_entity_structure_fk
	where dv.value in ('H') --('R', 'H')
	and pa_159903.product_association_id is null
	and prods.products_id is null
	and P1.aliases_id is null

	--Add items to above aliases without cost changes, identifying whether or not parity exists
	insert into #Aliases
	(Item_Change_Type_CR_FK, Product_structure_FK, Zone_Entity_Structure_FK, Alias_Data_Value_FK, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack
	, Pricesheet_Basis_Values_ID_New, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion, Cost_Change_Status)
	Select distinct 657 'Item_Change_Type_CR_FK', pmt.product_structure_fk, p.zone_entity_structure_fk
		, pmt.data_value_fk 'Alias_data_value_fk'
		, (Select 1 from #PBV where product_structure_fk = pmt.product_structure_fk) 'IND_COST_CHANGE_ITEM'
		, (select top 1 value from #PBV pbv1 where pbv1.product_structure_fk = pmt.product_structure_fk order by pbv1.Pricesheet_basis_Values_ID desc) 'Case_Cost_New'
		, (select top 1 unit_value from #PBV pbv1 where pbv1.product_structure_fk = pmt.product_structure_fk order by pbv1.Pricesheet_basis_Values_ID desc) 'Unit_Cost_New'
		, (select top 1 effective_date from #PBV pbv1 where pbv1.product_structure_fk = pmt.product_structure_fk order by pbv1.Pricesheet_basis_Values_ID desc) 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'Pack'
		, (select top 1 Pricesheet_basis_Values_ID from #PBV pbv1 where pbv1.product_structure_fk = pmt.product_structure_fk order by pbv1.Pricesheet_basis_Values_ID desc) 'Pricesheet_Basis_Values_ID_New'
		, P.ind_parity
		, Case isnull(pa_159903.product_association_id, 0) when 0 then 70 else 80 end 'Insert_Step'
		, Case isnull(pa_159903.product_association_id, 0) when 0 then 'None' else 'Item in Alias-No Cost Change' end 'Parity_Type'
		, Case isnull(pa_159903.product_association_id, 0) when 0 then 'Alias to item with cost change - No Parity' else 'Alias to item with cost change - With Parity' end 'Reason_For_Item'
		, 'Alias Relationship' 'Reason_For_Inclusion'
		, (Select top 1 change_status from #PBV where product_structure_fk = pmt.product_structure_fk) 'cost_change_status'
	from #Aliases P
	inner join epacube.product_mult_type pmt with (nolock) on p.Alias_data_value_fk = pmt.data_value_fk and p.zone_entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019 --and p.product_structure_fk <> pmt.product_structure_fk
	left join epacube.product_association pa_159903 with (nolock) on pmt.product_structure_fk = pa_159903.product_structure_fk and pmt.entity_structure_fk = pa_159903.ENTITY_STRUCTURE_FK and pa_159903.data_name_fk in (159903) and pa_159903.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	inner join epacube.product_association pa with (nolock) on pmt.product_structure_fk = pa.product_structure_fk and pa.entity_structure_fk = pmt.entity_structure_fk and pa.data_name_fk = 159450 and pa.record_status_cr_fk = 1
	inner join epacube.entity_attribute ea with (nolock) on p.zone_entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	left join epacube.product_attribute pa_500001 with (nolock) on pmt.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Aliases P1 on pa.product_structure_fk = p1.product_structure_fk and pmt.entity_structure_fk = p1.zone_entity_structure_fk
	left join #Products prods on pa.product_structure_fk = prods.product_structure_fk  and pa.entity_structure_fk = prods.zone_entity_structure_fk
	where 1 = 1
	and dv.value in ('H') --('R', 'H')
	and p1.Aliases_ID is null
	and prods.Products_ID is null
	--and isnull(p.IND_COST_CHANGE_ITEM, 0) = 0

--Add National Brand items in parity with Private Label - both without cost changes, PL in alias with another item having a cost change - National Brand Alias identified
	Insert into #Aliases
	(Item_Change_Type_CR_FK, Product_structure_FK, Parity_PL_Parent_Structure_FK, Parity_Child, Zone_Entity_Structure_FK, alias_data_value_fk, ind_Cost_Change_Item, Case_Cost_New, Unit_Cost_New
	, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion, Cost_Change_Status)
	Select 657 'Item_Change_Type_CR_FK', pa_159903.child_product_structure_fk 'product_structure_fk'
		, pa_159903.product_structure_fk 'Parity_PL_Parent_Structure_FK'
		, 1 'Parity_Child'
		, pa_159903.entity_structure_fk 'zone_entity_structure_fk'
		, pmt.data_value_fk 'alias_data_value_fk'
		, (Select top 1 1 from #PBV where product_structure_fk = pa_159903.child_product_structure_fk) 'IND_COST_CHANGE_ITEM'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa_159903.child_product_structure_fk), 0) when 1 then p.Case_Cost_New else null end 'Case_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa_159903.child_product_structure_fk), 0) when 1 then p.Unit_Cost_New else null end 'Unit_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pa_159903.child_product_structure_fk), 0) when 1 then p.Cost_Change_Effective_Date_New else Null end 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'pack'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.product_structure_fk), 0) when 1 then p.Pricesheet_Basis_Values_ID_New else Null end 'Pricesheet_Basis_Values_ID_New'
		, 1 'ind_parity'
		, 90 'Insert_Step', 'National Items in parity with an Alias - no Cost Change' 'Parity_Type', 'Child_Parity within Alias' 'Reason_For_Item', 'Alias Relationship' 'Reason_For_Inclusion'
		, (Select top 1 Change_Status from #PBV where product_structure_fk = pa_159903.child_product_structure_fk) 'Cost_Change_Status'
	from #Aliases p 
	inner join epacube.product_association pa_159903 with (nolock) on p.product_structure_fk = pa_159903.product_structure_fk and p.Zone_Entity_Structure_FK = pa_159903.ENTITY_STRUCTURE_FK and pa_159903.data_name_fk in (159903) and pa_159903.create_timestamp < (CAST(getdate() + 4 - (@Wk * 7) - datepart(dw, getdate()) AS DATE))
	inner join epacube.product_association paP with (nolock) on p.product_structure_fk = paP.product_structure_fk and paP.entity_structure_fk = pa_159903.entity_structure_fk and paP.data_name_fk = 159450 and paP.record_status_cr_fk = 1
	inner join epacube.product_association paC with (nolock) on pa_159903.child_product_structure_fk = paC.product_structure_fk and paC.entity_structure_fk = pa_159903.entity_structure_fk and paC.data_name_fk = 159450 and paC.record_status_cr_fk = 1
	inner join epacube.product_association pap2 with (nolock) on pa_159903.PRODUCT_STRUCTURE_FK = pap2.product_structure_fk and pa_159903.ENTITY_STRUCTURE_FK = pap2.ENTITY_STRUCTURE_FK and pap2.data_name_fk = 159450 and pap2.RECORD_STATUS_CR_FK = 1
	left join epacube.product_mult_type pmt with (nolock) on pa_159903.child_product_structure_fk = pmt.product_structure_fk and pa_159903.entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019
	inner join epacube.entity_attribute ea with (nolock) on pa_159903.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	left join epacube.product_attribute pa_500001 with (nolock) on p.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Aliases P1 on pa_159903.child_product_structure_fk = p1.product_structure_fk and pa_159903.entity_structure_fk = p1.zone_entity_structure_fk
	left join #Products prods on pa_159903.product_structure_fk = prods.product_structure_fk  and pa_159903.entity_structure_fk = prods.zone_entity_structure_fk
	where 1 = 1
	and dv.value in ('H') --('R', 'H')
	and p1.Aliases_ID is null
	and prods.Products_ID is null

--Add items that are in alias to National Brand parity items without cost changes
	insert into #Aliases
	(Item_Change_Type_CR_FK, Product_structure_FK, Zone_Entity_Structure_FK, ind_Cost_Change_Item, Alias_Data_Value_FK, parity_alias_pl_Parent_Structure_FK, Case_Cost_New, Unit_Cost_New, Cost_Change_Effective_Date_New, Pack, Pricesheet_Basis_Values_ID_New
	, ind_parity, Insert_Step, Parity_Type, Reason_For_Item, Reason_For_Inclusion, Cost_Change_Status)
	Select distinct 657 'Item_Change_Type_CR_FK', pmt.product_structure_fk, p.zone_entity_structure_fk
		, (Select 1 from #PBV where product_structure_fk = pmt.product_structure_fk) 'IND_COST_CHANGE_ITEM'
		, pmt.data_value_fk 'Alias_data_value_fk'
		, P.product_structure_fk 'parity_alias_pl_Parent_Structure_FK'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.product_structure_fk), 0) when 1 then p.Case_Cost_New else null end 'Case_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.product_structure_fk), 0) when 1 then p.Unit_Cost_New else null end 'Unit_Cost_New'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.product_structure_fk), 0) when 1 then p.Cost_Change_Effective_Date_New else Null end 'Cost_Change_Effective_Date_New'
		, pa_500001.attribute_event_data 'Pack'
		, case isnull((Select top 1 1 from #PBV where product_structure_fk = pmt.product_structure_fk), 0) when 1 then p.Pricesheet_Basis_Values_ID_New else Null end 'Pricesheet_Basis_Values_ID_New'
		, P.ind_parity
		, 95 'Insert_Step', 'National Brand' 'Parity_Type'
		, 'Alias due to National Parity Cost Change' 'Reason_For_Item', p.Reason_For_Inclusion
		, (Select top 1 change_status from #PBV where product_structure_fk = pmt.product_structure_fk)
	from #Aliases P
	inner join epacube.product_mult_type pmt with (nolock) on p.Alias_data_value_fk = pmt.data_value_fk and p.zone_entity_structure_fk = pmt.entity_structure_fk and pmt.data_name_fk = 500019 --and p.product_structure_fk <> pmt.product_structure_fk
	inner join epacube.product_association paPa with (nolock) on pmt.product_structure_fk = paPa.product_structure_fk and paPa.entity_structure_fk = pmt.entity_structure_fk and paPa.data_name_fk = 159450 and paPa.record_status_cr_fk = 1
	inner join epacube.entity_attribute ea with (nolock) on p.zone_entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
	inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
	left join epacube.product_attribute pa_500001 with (nolock) on pmt.product_structure_fk = pa_500001.product_structure_fk and pa_500001.data_name_fk = 500001
	left join #Aliases P1 on pmt.product_structure_fk = p1.product_structure_fk and pmt.entity_structure_fk = p1.zone_entity_structure_fk
	where 1 = 1 
	and p.Parity_PL_Parent_Structure_FK is not null
	and dv.value in ('H') --('R', 'H')
	and isnull(p.IND_COST_CHANGE_ITEM, 0) = 0
	and p1.Aliases_ID is null


Update A
Set case_cost_new = pbv.[value]
, unit_cost_new = pbv.unit_value
, Cost_Change_Effective_Date_New = pbv.Effective_Date
, Pricesheet_Basis_Values_ID_New = pbv.Pricesheet_Basis_Values_ID
from #Aliases a
inner join #pbv pbv on a.Product_structure_FK = pbv.Product_Structure_FK

--Begin Updates to establish new prices, level gross and qualified statuses	

	Update P
	Set PARITY_DISCOUNT_AMT = sz_500210.Parity_Discount_Amt 
	from #Aliases p
	inner join
		(
		Select
		ss.Product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK, cast(ss.attribute_number as numeric(18, 4)) 'Parity_Discount_Amt'
		, Dense_rank()over(partition by ss.Product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) DRank
		from epacube.segments_settings ss
		inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.PRODUCT_ASSOCIATION_FK = pa.PRODUCT_ASSOCIATION_ID
		where ss.data_name_fk = 500210 
		and ss.attribute_number <> 0
		and pa.record_status_cr_fk = 1
		and ss.RECORD_STATUS_CR_FK = 1
		) sz_500210 on p.PRODUCT_STRUCTURE_FK = sz_500210.product_structure_fk and p.ZONE_ENTITY_STRUCTURE_FK = sz_500210.ZONE_ENTITY_STRUCTURE_FK
		Where DRank = 1

	Update P
	Set Parity_Mult = sz_500212.Parity_Mult 
	from #Aliases P
	inner join
		(
		Select
		ss.Product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK, cast(ss.attribute_number as numeric(18, 4)) 'Parity_Mult'
		, Dense_rank()over(partition by ss.Product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) DRank
		from epacube.segments_settings ss 
		inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.PRODUCT_ASSOCIATION_FK = pa.PRODUCT_ASSOCIATION_ID
		where ss.data_name_fk = 500212 
		and ss.attribute_number <> 0
		and pa.record_status_cr_fk = 1
		and ss.RECORD_STATUS_CR_FK = 1
		) sz_500212 on P.PRODUCT_STRUCTURE_FK = sz_500212.product_structure_fk and P.ZONE_ENTITY_STRUCTURE_FK = sz_500212.ZONE_ENTITY_STRUCTURE_FK
		Where DRank = 1

	Update p
	Set PARITY_DISCOUNT_PCT = sz_500211.Parity_Discount_Pct 
	from #Aliases P
	inner join
		(
		Select
		ss.Product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK, cast(ss.attribute_number as numeric(18, 4)) 'Parity_Discount_Pct'
		, Dense_rank()over(partition by ss.Product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) DRank
		from epacube.segments_settings ss
		inner join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.PRODUCT_ASSOCIATION_FK = pa.PRODUCT_ASSOCIATION_ID
		where ss.data_name_fk = 500211 
		and ss.attribute_number <> 0
		and pa.record_status_cr_fk = 1
		and ss.RECORD_STATUS_CR_FK = 1
		) sz_500211 on p.PRODUCT_STRUCTURE_FK = sz_500211.product_structure_fk and p.ZONE_ENTITY_STRUCTURE_FK = sz_500211.ZONE_ENTITY_STRUCTURE_FK
		where DRank = 1

	Update P
	Set TMpct = sz_502046.attribute_number
	from #Aliases P
	inner join 
		(select * from (
						Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
						, dense_rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
						from epacube.segments_settings ss with (nolock) 
						inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
						inner join #Aliases p on sp.product_structure_fk = p.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = p.zone_entity_structure_fk
						where ss.data_name_fk = 502046
						and ss.Effective_Date <= @LGprocDate
						and ss.RECORD_STATUS_CR_FK = 1
						) a where Drank = 1
		) sz_502046 on p.Product_structure_FK = sz_502046.product_structure_fk and p.zone_entity_structure_fk = sz_502046.ZONE_ENTITY_STRUCTURE_FK

	 Update P
	 Set Level_Gross = Case isnull(sz_144862.ATTRIBUTE_Y_N, 'N') when 'Y' then sz_500015.ATTRIBUTE_Y_N else 'N' end
	 from #Aliases P
  		left join 
			(select * from (
							Select distinct ss.ATTRIBUTE_Y_N, sp.product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK
							, dense_rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK, sp.product_structure_fk order by ss.attribute_y_n, ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
							from epacube.segments_settings ss with (nolock) 
							inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
							inner join #Aliases zc on sp.product_structure_fk = zc.PRODUCT_STRUCTURE_FK and ss.ZONE_ENTITY_STRUCTURE_FK = zc.ZONE_ENTITY_STRUCTURE_FK
							where ss.data_name_fk = 500015
							and ss.RECORD_STATUS_CR_FK = 1
							and isnull(zc.ind_cost_change_item, 0) = 1
							) a where Drank = 1
			) sz_500015 on P.product_structure_fk = sz_500015.product_structure_fk and sz_500015.ZONE_ENTITY_STRUCTURE_FK = p.ZONE_ENTITY_STRUCTURE_FK
		left join 
			(select * from (
							Select ss.ATTRIBUTE_Y_N, ss.ZONE_ENTITY_STRUCTURE_FK, ss.SEGMENTS_SETTINGS_ID
							, dense_rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) DRank
							from epacube.segments_settings ss with (nolock) 
							where ss.data_name_fk = 144862
							and ss.ZONE_ENTITY_STRUCTURE_FK is not null 
							and CUST_ENTITY_STRUCTURE_FK is null
							and ss.RECORD_STATUS_CR_FK = 1
							) a where Drank = 1
			) sz_144862 on sz_144862.ZONE_ENTITY_STRUCTURE_FK = P.ZONE_ENTITY_STRUCTURE_FK

	Update P
	Set Price_Multiple_Cur = CurPrice.price_multiple
	, Price_Cur = CurPrice.Price
	, Price_Effective_Date_Cur = CurPrice.Effective_Date
	from #Aliases P
	inner join 
		(Select
		pr.zone_entity_structure_FK, pr.product_structure_fk, pr.price_multiple, pr.Price, pr.Effective_Date
		, Dense_Rank()over(partition by pr.ZONE_ENTITY_STRUCTURE_FK, pr.product_structure_fk order by pr.effective_date desc, pr.Price) DRank_Eff
		from marginmgr.PRICESHEET_RETAILS PR 
		inner join #Aliases P on pr.product_structure_fk = P.Product_structure_FK and pr.zone_entity_structure_FK = P.Zone_Entity_Structure_FK
		where pr.zone_entity_structure_FK is not null and pr.effective_date < Cast(GETDATE() + 6 - (@wk * 7) - DATEPART(DW, GETDATE()) as date)
		group by pr.zone_entity_structure_FK, pr.product_structure_fk, pr.price_multiple, pr.Price, pr.Effective_Date
		) CurPrice on P.PRODUCT_STRUCTURE_FK = CurPrice.Product_Structure_FK and P.Zone_Entity_Structure_FK = CurPrice.zone_entity_structure_FK
	where 1 = 1
		and isnull(CurPrice.DRank_Eff, 1) = 1

	Update P
	Set [Use_LG_Price_Points] = isnull(PP.UsePPT, 'N')
	from #Aliases P
	inner join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, sp.PRODUCT_STRUCTURE_FK, ss.ZONE_ENTITY_STRUCTURE_FK, ss.attribute_event_data 'UsePPT', ss.precedence, ss.effective_date, SEGMENTS_SETTINGS_ID
						, Dense_Rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) 'DRank_PPTs'
						from epacube.segments_settings ss with (nolock)
						inner join epacube.segments_product sp with (nolock) on sp.PROD_SEGMENT_FK = ss.PROD_SEGMENT_FK
						inner join #Aliases p on sp.product_structure_fk = p.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = p.Zone_Entity_Structure_FK
						where 1 = 1
						and ss.ZONE_ENTITY_STRUCTURE_FK is not null
						and ss.prod_segment_fk is not null
						and ss.cust_entity_structure_fk is null
						and ss.data_name_fk = 144876
						and isnull(p.ind_Cost_Change_Item, 0) = 1
						and ss.effective_date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + 7, getdate() - (@Wk * 7)) as date)
						and ss.RECORD_STATUS_CR_FK = 1
						and ss.precedence is not null
					) a where DRank_PPTs = 1 
		) PP on P.ZONE_ENTITY_STRUCTURE_FK = PP.ZONE_ENTITY_STRUCTURE_FK and P.product_structure_fk = PP.PRODUCT_STRUCTURE_FK
	where P.ind_Cost_Change_Item = 1

	Update P
	Set [Use_LG_Price_Points] = isnull(PP.UsePPT, 'N')
	from #Aliases P
	inner join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, ss.ZONE_ENTITY_STRUCTURE_FK, ss.attribute_event_data 'UsePPT', ss.effective_date, SEGMENTS_SETTINGS_ID
						, Dense_Rank()over(partition by ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) 'DRank'
						from epacube.segments_settings ss with (nolock)
						where 1 = 1
						and ss.ZONE_ENTITY_STRUCTURE_FK is not null
						and ss.prod_segment_fk is null
						and ss.cust_entity_structure_fk is null
						and ss.data_name_fk = 144876
						and ss.effective_date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + 7, getdate() - (@Wk * 7)) as date)
						and ss.RECORD_STATUS_CR_FK = 1
						) A where DRank = 1

		) PP on P.ZONE_ENTITY_STRUCTURE_FK = PP.ZONE_ENTITY_STRUCTURE_FK
	where 1 = 1
		and isnull(P.ind_Cost_Change_Item, 0) = 1
		and P.[Use_LG_Price_Points] is null

--get MinAmt at Zone Item Class level
	Update P
	Set MinAmt = isnull(PP.MinAmt, 0)
	from #Aliases P
	inner join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, sp.PRODUCT_STRUCTURE_FK, ss.ZONE_ENTITY_STRUCTURE_FK, ss.attribute_number 'MinAmt', ss.precedence, ss.effective_date, SEGMENTS_SETTINGS_ID
						, Dense_Rank()over(partition by ss.data_name_fk, ss.ZONE_ENTITY_STRUCTURE_FK, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) 'DRank_PPTs'
						from epacube.segments_settings ss with (nolock)
						inner join epacube.segments_product sp with (nolock) on sp.PROD_SEGMENT_FK = ss.PROD_SEGMENT_FK
						inner join #Aliases p on sp.product_structure_fk = p.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = p.Zone_Entity_Structure_FK
						where 1 = 1
						and ss.ZONE_ENTITY_STRUCTURE_FK is not null
						and ss.prod_segment_fk is not null
						and ss.cust_entity_structure_fk is null
						and ss.data_name_fk = 144908
						and isnull(p.ind_Cost_Change_Item, 0) = 1
						and ss.effective_date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + 7, getdate() - (@wk * 7)) as date)
						and ss.RECORD_STATUS_CR_FK = 1
						and ss.precedence is not null
					) a where DRank_PPTs = 1 
		) PP on P.ZONE_ENTITY_STRUCTURE_FK = PP.ZONE_ENTITY_STRUCTURE_FK and P.product_structure_fk = PP.PRODUCT_STRUCTURE_FK
	where P.ind_Cost_Change_Item = 1

	--get MinAmt at Zone level
	Update P
	Set MinAmt = isnull(PP.MinAmt, 0)
	from #Aliases P
	inner join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, ss.ZONE_ENTITY_STRUCTURE_FK, ss.attribute_number 'MinAmt', ss.effective_date, SEGMENTS_SETTINGS_ID
						, Dense_Rank()over(partition by ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) 'DRank'
						from epacube.segments_settings ss with (nolock)
						where 1 = 1
						and ss.ZONE_ENTITY_STRUCTURE_FK is not null
						and ss.prod_segment_fk is null
						and ss.cust_entity_structure_fk is null
						and ss.data_name_fk = 144908
						and ss.effective_date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + 7, getdate() - (@WK * 7)) as date)
						and ss.RECORD_STATUS_CR_FK = 1
						) A where DRank = 1

		) PP on P.ZONE_ENTITY_STRUCTURE_FK = PP.ZONE_ENTITY_STRUCTURE_FK
	where 1 = 1
		and isnull(P.ind_Cost_Change_Item, 0) = 1
		and P.MinAmt is null

	Update P
	Set DaysOut = isnull(Daysout.daysout, 7)
	from #Aliases P
	left join
		(Select * from (
						select ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
						, ss.ZONE_ENTITY_STRUCTURE_FK, cast(ss.attribute_event_data as Numeric(18, 0)) 'DaysOut', ss.effective_date
						, Dense_Rank()over(partition by ss.ZONE_ENTITY_STRUCTURE_FK order by ss.effective_date desc, ss.segments_settings_id desc) 'DRank'
						from epacube.segments_settings ss with (nolock)
						where 1 = 1
						and cust_entity_structure_fk is null
						and ss.data_name_fk = 144868
						and ss.effective_date <= Cast(GETDATE() + 6 - (@Wk * 7) - DATEPART(DW, GETDATE()) as date)
						and ss.RECORD_STATUS_CR_FK = 1
						) a where DRank = 1
		) DaysOut on DaysOut.ZONE_ENTITY_STRUCTURE_FK = P.ZONE_ENTITY_STRUCTURE_FK

	Update P
	Set Case_Cost_Cur = Pbv_c.value
	, Unit_Cost_Cur = Pbv_c.unit_value
	, pricesheet_basis_values_id_cur = pbv_c.pricesheet_basis_values_id
	from #Aliases P
		inner join 
			(Select * from (
							Select pbv.value, pbv.unit_value, pbv.Effective_Date, pbv.pack, pbv.product_structure_fk, pbv.pricesheet_basis_values_id, pbv.data_name_fk, p.zone_entity_structure_fk
							, dense_rank()over(partition by pbv.data_name_fk, p.zone_entity_structure_fk, pbv.product_structure_fk order by pbv.effective_date desc, pbv.pricesheet_basis_values_id desc) DRank
							from marginmgr.pricesheet_basis_values pbv with (nolock)
							inner join #Aliases P on pbv.product_structure_fk = P.product_structure_fk 
							where pbv.data_name_fk = 503201 
							and pbv.cust_entity_structure_fk is null
							and PBV.Effective_Date < cast(isnull(p.cost_change_effective_date_new, getdate() - (@Wk * 7)) as date)
							and pbv.record_status_cr_fk = 1
							) a where DRank = 1
			) Pbv_c on P.[Product_Structure_FK] = Pbv_c.Product_Structure_FK and P.zone_entity_structure_fk = pbv_c.zone_entity_structure_fk

----Begin logging Pricing Steps increments of 10

--------Calculate New Prices on all products with a cost change		Duration: 30 Seconds
	Update P
--	Set PRICE_Init = round(Cast(isnull(p.[unit_cost_new], P.[unit_cost_cur]) / (1 - P.TMpct) as Numeric(18, 6)) + 0.0049, 2)
	Set PRICE_Init = round(Cast(isnull(p.[unit_cost_new], P.[unit_cost_cur]) / (1 - P.TMpct) as Numeric(18, 6)), 2)
	from #Aliases P
	Where P.level_gross = 'Y'
	and isnull(P.ind_cost_change_item, 0) = 1

	Update P
	Set Digit_DN_FK = Case Cast(Right(PRICE_Init, 1) as int)	
															when 0 then 144897
															when 1 then 144898
															when 2 then 144899
															when 3 then 144901
															when 4 then 144902
															when 5 then 144903
															when 6 then 144904
															when 7 then 144905
															when 8 then 144906
															when 9 then 144907 end
	from #Aliases P
	Where P.level_gross = 'Y'
	and isnull(P.USE_LG_PRICE_POINTS, 'N') = 'Y'
	and isnull(P.ind_cost_change_item, 0) = 1

	Update P
	Set Price_Adj_Amt = adj.attribute_number
	from #Aliases P
	inner join 
		(Select attribute_number, product_structure_fk, ZONE_ENTITY_STRUCTURE_FK, data_name_fk 
			from (
					Select ss.attribute_number, sp.product_structure_fk, ss.ZONE_ENTITY_STRUCTURE_FK, ss.data_name_fk, ss.precedence
					, Dense_Rank()over(partition by ss.ZONE_ENTITY_STRUCTURE_FK, sp.product_structure_fk, ss.data_name_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
					from epacube.epacube.segments_settings ss
					inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.PROD_SEGMENT_FK and ss.PROD_SEGMENT_DATA_NAME_FK = sp.DATA_NAME_FK
					inner join #Aliases a on sp.PRODUCT_STRUCTURE_FK = a.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = a.Zone_Entity_Structure_FK
					where 1 = 1
					and ss.ZONE_ENTITY_STRUCTURE_FK is not null
					and ss.CUST_ENTITY_STRUCTURE_FK is null
					and a.Digit_DN_FK = ss.data_name_fk
				) B where DRank = 1
		) Adj on P.product_structure_fk = adj.product_structure_fk and P.zone_entity_structure_fk = Adj.ZONE_ENTITY_STRUCTURE_FK and P.Digit_DN_FK= Adj.data_name_fk

	Update P
	Set Price_Adj_Amt = adj.attribute_number
	from #Aliases P
	inner join 
		(
			Select ss.attribute_number, ss.ZONE_ENTITY_STRUCTURE_FK, ss.data_name_fk 
			from epacube.epacube.segments_settings ss
			where 1 = 1 
			and ss.prod_segment_fk is null
			and ss.ZONE_ENTITY_STRUCTURE_FK is not null
			and ss.CUST_ENTITY_STRUCTURE_FK is null
			and ss.data_name_fk between 144897 and 144907
			and ss.data_name_fk <> 144900
		) Adj on P.zone_entity_structure_fk = Adj.ZONE_ENTITY_STRUCTURE_FK and P.Digit_DN_FK = Adj.DATA_NAME_FK
	where P.Price_Adj_Amt is null

--Declare @WK int = 0
--Set initial prices on all records with cost changes
Update P
Set Price_New = isnull(adj.attribute_number, P.PRICE_Init + isnull(P.Price_Adj_Amt, 0))
, Pricing_Update_Steps = isnull(Pricing_Update_Steps, '') + 'A10-1-' + Cast(isnull(adj.attribute_number, P.PRICE_Init + isnull(P.Price_Adj_Amt, 0)) as varchar(16)) + ', '
from #Aliases P
left join 
			(
				select * 
				from (
						Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.zone_entity_structure_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
						, ss.lower_limit, ss.upper_limit, ss.Effective_Date, ss.UPDATE_TIMESTAMP
						, dense_rank()over(partition by ss.data_name_fk, ss.zone_entity_structure_fk, sp.product_structure_fk, ss.lower_limit order by ss.precedence, ss.effective_date desc, ss.update_timestamp desc) DRank
						from epacube.segments_settings ss with (nolock) 
						inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
						inner join #Aliases a on sp.PRODUCT_STRUCTURE_FK = a.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = a.Zone_Entity_Structure_FK
						left join epacube.data_value dv with (nolock) on sp.PROD_SEGMENT_FK = dv.data_value_id
						left join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
						where ss.data_name_fk = 144891
						and ss.CUST_ENTITY_STRUCTURE_FK is null
						and ss.RECORD_STATUS_CR_FK = 1
						and a.PRICE_Init + isnull(a.Price_Adj_Amt, 0) between ss.lower_limit and ss.upper_limit
						and ss.Effective_Date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + a.DaysOut, getdate() - (@WK * 7)) as date)
					) B where Drank = 1
			) adj on p.product_structure_fk = adj.product_structure_fk and p.zone_entity_structure_fk = adj.ZONE_ENTITY_STRUCTURE_FK
where isnull(P.level_gross, 'N') = 'Y'

Insert into #Aliases
(
Item_Change_Type_CR_FK, Zone_Entity_Structure_FK, Alias_Data_Value_FK, DaysOut, Insert_Step, Level_Gross, Product_structure_FK, MinAmt, Price_Mgmt_Type_CR_FK
)
Select Case isnull(@MarginChanges, 0) when 0 then 656 else 662 end 'Item_Change_Type_CR_FK', P.zone_entity_structure_fk, P.Alias_data_value_FK, isnull(Max(P.DaysOut), 7) 'DaysOut'
, 1000 'Insert_Step', Max(isnull(Level_Gross, 'N')) 'Level_Gross', Max(Product_structure_FK) 'Product_structure_FK', max(minamt) MinAmt
, Case Max(isnull(Level_Gross, 'N')) when 'N' then 651 else 650 end 'Price_Mgmt_Type_CR_FK'
from #Aliases P 
where P.alias_data_value_FK is not null 
and p.alias_data_value_fk not in (select alias_data_value_fk from #aliases where Item_Change_Type_CR_FK in (656, 662))
group by P.alias_data_value_FK, P.zone_entity_structure_fk

--Set header values on alias header records where at least one item in the alias had a cost change
Update PA
Set Price_New = --isnull(pi.price_new, pi.price_cur)
	Cast([precision].[getretailpricezone]	(prc.zone_entity_structure_FK--, pi.Product_structure_FK
		, prc.product_structure_fk
		, cast(prc.unit_cost / (1 - pi.TMpct) as numeric(18, 4)), prc.daysout, @wk) as Numeric(18, 2))
, price_cur = pi.price_cur
, TMpct = pi.TMpct
, unit_cost_cur = pi.unit_cost_cur
, unit_cost_new = pi.unit_cost_new
, Case_Cost_Cur = pi.Case_Cost_Cur
, Case_Cost_New = pi.Case_Cost_New
, Level_Gross = isnull(pi.level_gross, 'N')
, use_lg_price_points = pi.use_lg_price_points
, Pricing_Update_Steps = isnull(Pricing_Update_Steps, '') + 'A10-2-' + cast(isnull(pi.price_new, pi.price_cur) as varchar(16)) + ', '
, ind_parity = Case when pi.ind_parity <> 0 then pi.ind_parity end
, reason_for_inclusion = pi.reason_for_inclusion
, cost_change_status = pi.cost_change_status
, ind_cost_change_item = pi.ind_cost_change_item
from #Aliases Pa
inner join
	(
	select (select top 1 reason_for_inclusion from #aliases where alias_data_value_fk = a.alias_data_value_fk and zone_entity_structure_fk = a.zone_entity_structure_fk 
	and unit_cost_new = a.unit_cost_new) 'reason_for_inclusion' 
		, * from (
		Select 
		pa.alias_data_value_fk, pa.zone_entity_structure_fk
		, Max(isnull(pi.price_new, pi.price_cur)) Price_New --, Max(pi.price_new) Price_New
		, Max(pi.price_cur) price_cur, Max(pi.TMpct) TMpct, Max(pi.unit_cost_cur) unit_cost_cur
		, Max(isnull(pi.unit_cost_new, pi.unit_cost_cur)) unit_cost_new  --, Max(pi.unit_cost_new) unit_cost_new
		, Max(pi.Case_Cost_Cur) Case_Cost_Cur, Max(isnull(pi.Case_Cost_New, pi.Case_Cost_Cur)) Case_Cost_New, Max(isnull(pi.level_gross, 'N')) 'Level_Gross'--, Min(pi.qualified_status_cr_fk) 'qualified_status_cr_fk'
		, Max(isnull(pi.ind_parity, 0)) ind_parity, Max(isnull(pi.ind_cost_change_item, 0)) 'ind_cost_change_item'
		, max(isnull(pi.use_lg_price_points, 'N')) 'use_lg_price_points'
		, max(isnull(pi.PARITY_DISCOUNT_PCT, 0)) 'PARITY_DISCOUNT_PCT', max(isnull(pi.PARITY_DISCOUNT_AMT, 0)) 'PARITY_DISCOUNT_AMT', min(isnull(pi.cost_change_status, 0)) cost_change_status
		, max(pi.daysout) daysout
		from #Aliases Pa
		inner join #Aliases Pi on Pa.alias_data_value_fk = pi.alias_data_value_fk and Pa.zone_entity_structure_fk = Pi.zone_entity_structure_fk
		where Pa.Item_Change_Type_CR_FK in (656, 662)
		and isnull(pi.Item_Change_Type_CR_FK, 0) not in (656, 662)
		group by pa.alias_data_value_fk, pa.zone_entity_structure_fk
		) a
	) pi on pa.alias_data_value_fk = pi.alias_data_value_fk and pa.zone_entity_structure_fk = pi.zone_entity_structure_fk and Pa.Item_Change_Type_CR_FK in (656, 662)
inner join (
		Select * from (Select alias_data_value_fk, zone_entity_structure_fk, product_structure_fk, isnull(unit_cost_new, unit_cost_cur) 'unit_cost'
		, dense_rank()over(partition by alias_data_value_fk, zone_entity_structure_fk order by isnull(unit_cost_new, unit_cost_cur) desc) 'DRank'
		, daysout
		from #aliases where item_change_type_cr_fk = 657) b where drank = 1
		) Prc on pa.Alias_Data_Value_FK = prc.Alias_Data_Value_FK and pa.Zone_Entity_Structure_FK = prc.Zone_Entity_Structure_FK

	Update A
	Set parity_discount_pct = B.parity_discount_pct
	, parity_discount_amt = B.parity_discount_amt
	, parity_mult = B.parity_mult
	, Parity_Alias_Data_Value_FK = B.Parity_Alias_Data_Value_FK
	from #Aliases A
	inner join
		(
			Select * from (
				select a1.alias_data_value_fk, a2.alias_data_value_fk 'Parity_Alias_Data_Value_FK', a1.zone_entity_structure_fk, a1.parity_discount_pct, a1.parity_discount_amt, isnull(a1.parity_mult, 1) 'parity_mult'
				, dense_rank()over(partition by a1.alias_data_value_fk, a1.zone_entity_structure_fk order by a1.parity_discount_pct desc, a1.parity_discount_amt desc) 'DRank'
				from #aliases a1
				inner join #aliases a2 on a2.parity_pl_parent_structure_fk = a1.product_structure_fk and a2.Zone_Entity_Structure_FK = a1.Zone_Entity_Structure_FK
				where a1.Item_Change_Type_CR_FK not in (656, 662)
				) AA where DRank = 1
		) B on A.alias_data_value_fk = B.alias_data_value_fk and A.zone_entity_structure_fk = B.zone_entity_structure_fk
	where a.Item_Change_Type_CR_FK in (656, 662)

	create index idx_100 on #aliases(Item_Change_Type_CR_FK, zone_entity_structure_fk, Alias_Data_Value_FK, Parity_alias_data_value_fk, product_structure_fk, PARITY_ALIAS_PL_PARENT_STRUCTURE_FK)

	-------------------------------------
----	----	Address aliases with parity and items in alias to that parity
--PL item decreases; national no change OR PL - no change; national change
	Update al
	Set price_new = Cast(case	when a.level_gross = 'N' then null
								when a.level_gross = 'Y' and a.use_lg_price_points = 'N' then a.price_new_raw
								else [precision].[getretailpricezone]	(a.zone_entity_structure_FK, a.Product_structure_FK, a.price_new_raw, a.daysout, @wk) end as Numeric(18, 2))
	, level_gross = a.level_gross
	, use_lg_price_points = a.use_lg_price_points
	, final_pricing_step = 0
	from #Aliases al
	inner join 
			(
				Select distinct
				Case	when isnull(a0.parity_discount_amt, 0) > 0 then a2.price_new - a0.PARITY_DISCOUNT_AMT 
						when isnull(a0.parity_discount_pct, 0) > 0 then a2.price_new * (1 - a0.parity_discount_pct) end 'price_new_raw'
				, case	when isnull(a0.use_lg_price_points, 'N') = 'N' and isnull(a2.use_lg_price_points, 'N') = 'N' then 'N' else 'Y' end 'use_lg_price_points'
				, case	when isnull(a0.level_gross, 'N') = 'N' and isnull(a2.level_gross, 'N') = 'N' then 'N' else 'Y' end 'level_gross'
				, a0.Product_structure_FK
				, a0.Zone_Entity_Structure_FK
				, a0.daysout
				, a0.aliases_id
				from #Aliases A0
				inner join #Aliases A1 on A0.Alias_Data_Value_FK = A1.Alias_Data_Value_FK and A0.zone_entity_structure_fk = A1.zone_entity_structure_fk and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
				inner join #Aliases A2 on A2.alias_data_value_fk = A0.Parity_alias_data_value_fk and A0.Zone_Entity_Structure_FK = A2.zone_entity_structure_fk and a2.Item_Change_Type_CR_FK in (656, 662)
				inner join #aliases A3 on A2.alias_data_value_fk = a3.alias_data_value_fk and a2.Zone_Entity_Structure_FK = a3.Zone_Entity_Structure_FK and a3.Item_Change_Type_CR_FK = 657
				inner join #aliases A4 on A3.product_structure_fk = a4.PARITY_ALIAS_PL_PARENT_STRUCTURE_FK and a3.Zone_Entity_Structure_FK = a4.Zone_Entity_Structure_FK and a4.Item_Change_Type_CR_FK = 657
				where (a0.ind_cost_change_item = 1 and a0.cost_change_status = -1 and a2.ind_cost_change_item = 0) or (a0.ind_cost_change_item = 0 and a2.ind_cost_change_item = 1)
			) a on al.aliases_id = a.aliases_id


--PL item increases; national no change
	Update al
	Set price_new = Cast(case	when a.level_gross = 'N' then null
								when a.level_gross = 'Y' and a.use_lg_price_points = 'N' then a.price_new_raw
								else [precision].[getretailpricezone]	(a.zone_entity_structure_FK, a.Product_structure_FK, a.price_new_raw, a.daysout, @wk) end as Numeric(18, 2))
	, level_gross = a.level_gross
	, use_lg_price_points = a.use_lg_price_points
	, final_pricing_step = 2
	from #Aliases al
	inner join 
			(
				Select distinct
				Case	when isnull(a0.parity_discount_amt, 0) > 0 then a0.price_new + a0.PARITY_DISCOUNT_AMT 
						when isnull(a0.parity_discount_pct, 0) > 0 then a0.price_new / (1 - a0.parity_discount_pct) end 'price_new_raw'
				, case	when isnull(a0.use_lg_price_points, 'N') = 'N' and isnull(a2.use_lg_price_points, 'N') = 'N' then 'N' else 'Y' end 'use_lg_price_points'
				, case	when isnull(a0.level_gross, 'N') = 'N' and isnull(a2.level_gross, 'N') = 'N' then 'N' else 'Y' end 'level_gross'
				, a2.Product_structure_FK
				, a2.Zone_Entity_Structure_FK
				, a2.daysout
				, a2.aliases_id
				from #Aliases A0
				inner join #Aliases A1 on A0.Alias_Data_Value_FK = A1.Alias_Data_Value_FK and A0.zone_entity_structure_fk = A1.zone_entity_structure_fk and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
				inner join #Aliases A2 on A2.alias_data_value_fk = A0.Parity_alias_data_value_fk and A0.Zone_Entity_Structure_FK = A2.zone_entity_structure_fk and a2.Item_Change_Type_CR_FK in (656, 662)
				inner join #aliases A3 on A2.alias_data_value_fk = a3.alias_data_value_fk and a2.Zone_Entity_Structure_FK = a3.Zone_Entity_Structure_FK and a3.Item_Change_Type_CR_FK = 657
				--inner join #aliases A4 on A3.product_structure_fk = a4.PARITY_ALIAS_PL_PARENT_STRUCTURE_FK and a3.Zone_Entity_Structure_FK = a4.Zone_Entity_Structure_FK and a4.Item_Change_Type_CR_FK = 657
				where a0.ind_cost_change_item = 1 and a0.cost_change_status = 0 and a2.ind_cost_change_item = 0 and a0.price_cur <> a0.price_new
			) a on al.aliases_id = a.aliases_id

			Update A0
			Set qualified_status_cr_fk = upd.qualified_status_cr_fk
			, Price_Mgmt_Type_CR_FK = upd.Price_Mgmt_Type_CR_FK
			, qualified_step = 0
			from #Aliases A0
			inner join #Aliases A2 on A2.alias_data_value_fk = A0.Parity_alias_data_value_fk and A0.Zone_Entity_Structure_FK = A2.zone_entity_structure_fk and a0.Item_Change_Type_CR_FK in (656, 662) and a2.Item_Change_Type_CR_FK in (656, 662)
			inner join
				(
					select distinct a0.Alias_Data_Value_FK, a0.Parity_Alias_Data_Value_FK, case	when a0.price_new <> a0.price_cur then 653 when a2.price_new <> a2.price_cur then 653 else 654 end 'qualified_status_cr_fk'
					, case when a0.Level_Gross = 'Y' or a2.Level_Gross = 'Y' then 650 else 651 end 'Price_Mgmt_Type_CR_FK', a0.Zone_Entity_Structure_FK
					, a0.aliases_id 'a0_aliases_id', a2.aliases_id 'a2_aliases_id'
					from #Aliases A0
					inner join #Aliases A1 on A0.Alias_Data_Value_FK = A1.Alias_Data_Value_FK and A0.zone_entity_structure_fk = A1.zone_entity_structure_fk and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
					inner join #Aliases A2 on A2.alias_data_value_fk = A0.Parity_alias_data_value_fk and A0.Zone_Entity_Structure_FK = A2.zone_entity_structure_fk and a2.Item_Change_Type_CR_FK in (656, 662)
					inner join #aliases A3 on A2.alias_data_value_fk = a3.alias_data_value_fk and a2.Zone_Entity_Structure_FK = a3.Zone_Entity_Structure_FK and a3.Item_Change_Type_CR_FK = 657
					--inner join #aliases A4 on A3.product_structure_fk = a4.PARITY_ALIAS_PL_PARENT_STRUCTURE_FK and a3.Zone_Entity_Structure_FK = a4.Zone_Entity_Structure_FK and a4.Item_Change_Type_CR_FK = 657
				) Upd on a0.aliases_id = Upd.a0_aliases_id and a2.aliases_id = Upd.a2_aliases_id

			Update A2
			Set qualified_status_cr_fk = upd.qualified_status_cr_fk
			, Price_Mgmt_Type_CR_FK = upd.Price_Mgmt_Type_CR_FK
			, qualified_step = 2
			from #Aliases A0
			inner join #Aliases A2 on A2.alias_data_value_fk = A0.Parity_alias_data_value_fk and A0.Zone_Entity_Structure_FK = A2.zone_entity_structure_fk and a0.Item_Change_Type_CR_FK in (656, 662) and a2.Item_Change_Type_CR_FK in (656, 662)
			inner join
				(
					select distinct a0.Alias_Data_Value_FK, a0.Parity_Alias_Data_Value_FK, case	when a0.price_new <> a0.price_cur then 653 when a2.price_new <> a2.price_cur then 653 else 654 end 'qualified_status_cr_fk'
					, case when a0.Level_Gross = 'Y' or a2.Level_Gross = 'Y' then 650 else 651 end 'Price_Mgmt_Type_CR_FK', a0.Zone_Entity_Structure_FK
					, a0.aliases_id 'a0_aliases_id', a2.aliases_id 'a2_aliases_id'
					from #Aliases A0
					inner join #Aliases A1 on A0.Alias_Data_Value_FK = A1.Alias_Data_Value_FK and A0.zone_entity_structure_fk = A1.zone_entity_structure_fk and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
					inner join #Aliases A2 on A2.alias_data_value_fk = A0.Parity_alias_data_value_fk and A0.Zone_Entity_Structure_FK = A2.zone_entity_structure_fk and a2.Item_Change_Type_CR_FK in (656, 662)
					inner join #aliases A3 on A2.alias_data_value_fk = a3.alias_data_value_fk and a2.Zone_Entity_Structure_FK = a3.Zone_Entity_Structure_FK and a3.Item_Change_Type_CR_FK = 657
					--inner join #aliases A4 on A3.product_structure_fk = a4.PARITY_ALIAS_PL_PARENT_STRUCTURE_FK and a3.Zone_Entity_Structure_FK = a4.Zone_Entity_Structure_FK and a4.Item_Change_Type_CR_FK = 657
				) Upd on a0.alias_data_value_fk = Upd.alias_data_value_fk and a2.aliases_id = Upd.a2_aliases_id

		Update A0
		set Qualified_Status_CR_FK = case	when a0.price_new <> a0.price_cur then 653
											when a2.price_new <> a2.price_cur then 653 else 654 end
		, Price_Mgmt_Type_CR_FK = case when a0.Level_Gross = 'Y' or a2.Level_Gross = 'Y' then 650 else 651 end
		, Qualified_step = 0
		from #Aliases A0
		inner join #Aliases A1 on A0.Alias_Data_Value_FK = A1.Alias_Data_Value_FK and A0.zone_entity_structure_fk = A1.zone_entity_structure_fk and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
		inner join #Aliases A2 on A2.alias_data_value_fk = A0.Parity_alias_data_value_fk and A0.Zone_Entity_Structure_FK = A2.zone_entity_structure_fk and a2.Item_Change_Type_CR_FK in (656, 662)
		inner join #aliases A3 on A2.alias_data_value_fk = a3.alias_data_value_fk and a2.Zone_Entity_Structure_FK = a3.Zone_Entity_Structure_FK and a3.Item_Change_Type_CR_FK = 657
		--inner join #aliases A4 on A3.product_structure_fk = a4.PARITY_ALIAS_PL_PARENT_STRUCTURE_FK and a3.Zone_Entity_Structure_FK = a4.Zone_Entity_Structure_FK and a4.Item_Change_Type_CR_FK = 657

		Update A2
		set Qualified_Status_CR_FK = case	when Cast(a0.price_new as Numeric(18, 2)) <> Cast(a0.price_cur as Numeric(18, 2)) then 653
											when Cast(a2.price_new as Numeric(18, 2)) <> Cast(a2.price_cur as Numeric(18, 2)) then 653 else 654 end 
		, Price_Mgmt_Type_CR_FK = case when a0.Level_Gross = 'Y' or a2.Level_Gross = 'Y' then 650 else 651 end
		, Qualified_step = 22
		from #Aliases A0
		inner join #Aliases A1 on A0.Alias_Data_Value_FK = A1.Alias_Data_Value_FK and A0.zone_entity_structure_fk = A1.zone_entity_structure_fk and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
		inner join #Aliases A2 on A2.alias_data_value_fk = A0.Parity_alias_data_value_fk and A0.Zone_Entity_Structure_FK = A2.zone_entity_structure_fk and a2.Item_Change_Type_CR_FK in (656, 662)
		inner join #aliases A3 on A2.alias_data_value_fk = a3.alias_data_value_fk and a2.Zone_Entity_Structure_FK = a3.Zone_Entity_Structure_FK and a3.Item_Change_Type_CR_FK = 657
		--inner join #aliases A4 on A3.product_structure_fk = a4.PARITY_ALIAS_PL_PARENT_STRUCTURE_FK and a3.Zone_Entity_Structure_FK = a4.Zone_Entity_Structure_FK and a4.Item_Change_Type_CR_FK = 657

--Apply changes to alias children

		Update A1
		Set price_new = a0.price_new
		, level_gross = a0.level_gross
		, use_lg_price_points = a0.use_lg_price_points
		, Qualified_Status_CR_FK = a0.Qualified_Status_CR_FK
		, Price_Mgmt_Type_CR_FK = a0.Price_Mgmt_Type_CR_FK
		, Qualified_Step = 1
		, final_pricing_step = 1
		from #Aliases A0
		inner join #Aliases A1 on A0.Alias_Data_Value_FK = A1.Alias_Data_Value_FK and A0.zone_entity_structure_fk = A1.zone_entity_structure_fk and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
		inner join #Aliases A2 on A2.alias_data_value_fk = A0.Parity_alias_data_value_fk and A0.Zone_Entity_Structure_FK = A2.zone_entity_structure_fk and a2.Item_Change_Type_CR_FK in (656, 662)
		inner join #aliases A3 on A2.alias_data_value_fk = a3.alias_data_value_fk and a2.Zone_Entity_Structure_FK = a3.Zone_Entity_Structure_FK and a3.Item_Change_Type_CR_FK = 657
		--inner join #aliases A4 on A3.product_structure_fk = a4.PARITY_ALIAS_PL_PARENT_STRUCTURE_FK and a3.Zone_Entity_Structure_FK = a4.Zone_Entity_Structure_FK and a4.Item_Change_Type_CR_FK = 657

--Apply changes to parity_alias_children

		Update A3
		Set price_new = a2.price_new
		, level_gross = a2.level_gross
		, use_lg_price_points = a2.use_lg_price_points
		, Qualified_Status_CR_FK = case when a0.price_new <> a0.price_cur or a2.price_new <> a2.price_cur then 653 else 654 end
		, Price_Mgmt_Type_CR_FK =  case when a0.Level_Gross = 'Y' or a2.Level_Gross = 'Y' then 650 else 651 end
		, Qualified_Step = 3
		, final_pricing_step = 3
		from #Aliases A0
		inner join #Aliases A1 on A0.Alias_Data_Value_FK = A1.Alias_Data_Value_FK and A0.zone_entity_structure_fk = A1.zone_entity_structure_fk and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
		inner join #Aliases A2 on A2.alias_data_value_fk = A0.Parity_alias_data_value_fk and A0.Zone_Entity_Structure_FK = A2.zone_entity_structure_fk and a2.Item_Change_Type_CR_FK in (656, 662)
		inner join #aliases A3 on A2.alias_data_value_fk = a3.alias_data_value_fk and a2.Zone_Entity_Structure_FK = a3.Zone_Entity_Structure_FK and a3.Item_Change_Type_CR_FK = 657
		--inner join #aliases A4 on A3.product_structure_fk = a4.PARITY_ALIAS_PL_PARENT_STRUCTURE_FK and a3.Zone_Entity_Structure_FK = a4.Zone_Entity_Structure_FK and a4.Item_Change_Type_CR_FK = 657

--Apply changes to items in alias with parity items above
		Update A4
		Set price_new = a3.price_new
		, level_gross = a3.level_gross
		, use_lg_price_points = a3.use_lg_price_points
		, Qualified_Status_CR_FK = a3.Qualified_Status_CR_FK
		, Price_Mgmt_Type_CR_FK = a3.Price_Mgmt_Type_CR_FK
		, Qualified_Step = 4
		, final_pricing_step = 4
		from #Aliases A0
		inner join #Aliases A1 on A0.Alias_Data_Value_FK = A1.Alias_Data_Value_FK and A0.zone_entity_structure_fk = A1.zone_entity_structure_fk and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
		inner join #Aliases A2 on A2.alias_data_value_fk = A0.Parity_alias_data_value_fk and A0.Zone_Entity_Structure_FK = A2.zone_entity_structure_fk and a2.Item_Change_Type_CR_FK in (656, 662)
		inner join #aliases A3 on A2.alias_data_value_fk = a3.alias_data_value_fk and a2.Zone_Entity_Structure_FK = a3.Zone_Entity_Structure_FK and a3.Item_Change_Type_CR_FK = 657
		inner join #aliases A4 on A3.product_structure_fk = a4.PARITY_ALIAS_PL_PARENT_STRUCTURE_FK and a3.Zone_Entity_Structure_FK = a4.Zone_Entity_Structure_FK and a4.Item_Change_Type_CR_FK = 657

--declare @wk int = 0

----	----	Address aliases with parity items that are not in any alias - parity declines or national changes
	Update al
	Set price_new = Cast(case	when a.level_gross = 'N' then null
								when a.level_gross = 'Y' and a.use_lg_price_points = 'N' then a.price_new_raw
								else [precision].[getretailpricezone]	(a.zone_entity_structure_FK, a.Product_structure_FK, a.price_new_raw, a.daysout, @wk) end as Numeric(18, 2))
	, level_gross = a.level_gross
	, use_lg_price_points = a.use_lg_price_points
	, final_pricing_step = 5
	from #Aliases al
	inner join 
			(
				Select distinct
				a0.alias_data_value_fk,
				Case	when isnull(a0.parity_discount_amt, 0) > 0 then isnull(a2.price_new, a2.price_cur) - a0.PARITY_DISCOUNT_AMT 
						when isnull(a0.parity_discount_pct, 0) > 0 then isnull(a2.price_new, a2.price_cur) * (1 - a0.parity_discount_pct) end 'price_new_raw'
				, case	when isnull(a0.use_lg_price_points, 'N') = 'N' and isnull(a2.use_lg_price_points, 'N') = 'N' then 'N' else 'Y' end 'use_lg_price_points'
				, case	when isnull(a0.level_gross, 'N') = 'N' and isnull(a2.level_gross, 'N') = 'N' then 'N' else 'Y' end 'level_gross'
				, a0.Product_structure_FK
				, a0.Zone_Entity_Structure_FK
				, a0.daysout
				, a0.final_pricing_step '0_final_pricing_step'
				, a2.final_pricing_step '2_final_pricing_step'
				, a0.ind_Cost_Change_Item '0_cost_change_ind'
				, a0.Cost_Change_Status '0_cost_change_status'
				, a2.ind_Cost_Change_Item '2_cost_change_ind'
				, a2.Cost_Change_Status '2_cost_change_status'
				, a0.aliases_id
				from #aliases A0
				inner join #aliases A1 on A0.Zone_Entity_Structure_FK = A1.Zone_Entity_Structure_FK and a0.Alias_Data_Value_FK = a1.Alias_Data_Value_FK and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
				inner join #aliases A2 on A1.Product_structure_FK = a2.Parity_PL_Parent_Structure_FK and a1.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK and a1.Item_Change_Type_CR_FK = a2.Item_Change_Type_CR_FK 
				left join #aliases A3 on A3.parity_alias_data_value_fk = A2.alias_data_value_fk and A3.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK
				where 1 = 1
				--and a3.aliases_id is null
				and ((isnull(a0.ind_cost_change_item, 0) = 1 and isnull(a0.cost_change_status, 0) = -1 and isnull(a2.ind_cost_change_item, 0) = 0) 
					or (isnull(a0.ind_cost_change_item, 0) = 0 and isnull(a2.ind_cost_change_item, 0) = 1))
				--and isnull(a0.Parity_Alias_Data_Value_FK, '0') = '0'
			) a on al.aliases_id = a.aliases_id

----	----	Apply above results to alias children
	Update a1
	Set price_new = a0.price_new
	, level_gross = a0.level_gross
	, use_lg_price_points = a0.use_lg_price_points
	, final_pricing_step = 6
	from #aliases A0
	inner join #aliases A1 on A0.Zone_Entity_Structure_FK = A1.Zone_Entity_Structure_FK and a0.Alias_Data_Value_FK = a1.Alias_Data_Value_FK and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
	inner join #aliases A2 on A1.Product_structure_FK = a2.Parity_PL_Parent_Structure_FK and a1.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK and a1.Item_Change_Type_CR_FK = a2.Item_Change_Type_CR_FK 
	left join #aliases A3 on A3.parity_alias_data_value_fk = A2.alias_data_value_fk and A3.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK
	where 1 = 1
	and a3.aliases_id is null
	and ((isnull(a0.ind_cost_change_item, 0) = 1 and isnull(a0.cost_change_status, 0) = -1 and isnull(a2.ind_cost_change_item, 0) = 0) 
		or (isnull(a0.ind_cost_change_item, 0) = 0 and isnull(a2.ind_cost_change_item, 0) = 1))

----	----	Address aliases with parity items that are not in any alias - parity declines and national no changes

	Update al
	Set price_new = Cast(case	when a.level_gross = 'N' then null
								when a.level_gross = 'Y' and a.use_lg_price_points = 'N' then a.price_new_raw
								else [precision].[getretailpricezone]	(a.zone_entity_structure_FK, a.Product_structure_FK, a.price_new_raw, a.daysout, @wk) end as Numeric(18, 2))
	, level_gross = a.level_gross
	, use_lg_price_points = a.use_lg_price_points
	, final_pricing_step = 7
	from #Aliases al
	inner join 
			(
				Select distinct
				Case	when isnull(a0.parity_discount_amt, 0) > 0 then a0.price_new + a0.PARITY_DISCOUNT_AMT 
						when isnull(a0.parity_discount_pct, 0) > 0 then a0.price_new / (1 - a0.parity_discount_pct) end 'price_new_raw'
				, case	when isnull(a0.use_lg_price_points, 'N') = 'N' and isnull(a2.use_lg_price_points, 'N') = 'N' then 'N' else 'Y' end 'use_lg_price_points'
				, case	when isnull(a0.level_gross, 'N') = 'N' and isnull(a2.level_gross, 'N') = 'N' then 'N' else 'Y' end 'level_gross'
				, a2.Product_structure_FK
				, a2.Zone_Entity_Structure_FK
				, a2.daysout
				, a2.aliases_id
				from #aliases A0
				inner join #aliases A1 on A0.Zone_Entity_Structure_FK = A1.Zone_Entity_Structure_FK and a0.Alias_Data_Value_FK = a1.Alias_Data_Value_FK and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
				inner join #aliases A2 on A1.Product_structure_FK = a2.Parity_PL_Parent_Structure_FK and a1.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK and a1.Item_Change_Type_CR_FK = a2.Item_Change_Type_CR_FK 
				left join #aliases A3 on A3.parity_alias_data_value_fk = A2.alias_data_value_fk and A3.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK
				where 1 = 1
				and a0.ind_cost_change_item = 1 and a0.cost_change_status = 0 and a2.ind_cost_change_item = 0 and a0.price_cur <> a0.price_new--) or (a2.price_new is null))
				and a3.aliases_id is null
			) a on al.aliases_id = a.aliases_id

		Update A
		Set Price_New = Price_Cur
		, qualified_status_cr_fk = 654
		, price_mgmt_type_cr_fk = case when isnull(level_gross, 'N') = 'Y' then 650 else 651 end
		, Qualified_Step = 8
		, final_pricing_step = 8
		from #Aliases A
		where parity_pl_parent_structure_fk is not null
		and price_new is null
		and final_pricing_step is null

		Update A0
		set Qualified_Status_CR_FK = case when a0.price_new <> a0.price_cur or a1.price_new <> a1.price_cur or a2.price_new <> a2.price_cur then 653 else 654 end
		, Price_Mgmt_Type_CR_FK = case when a0.Level_Gross = 'Y' or a1.Level_Gross = 'Y' or a2.Level_Gross = 'Y' then 650 else 651 end
		, Qualified_Step = 80
		from #aliases A0
		inner join #aliases A1 on A0.Zone_Entity_Structure_FK = A1.Zone_Entity_Structure_FK and a0.Alias_Data_Value_FK = a1.Alias_Data_Value_FK and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
		inner join #aliases A2 on A1.Product_structure_FK = a2.Parity_PL_Parent_Structure_FK and a1.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK and a1.Item_Change_Type_CR_FK = a2.Item_Change_Type_CR_FK 
		left join #aliases A3 on A3.parity_alias_data_value_fk = A2.alias_data_value_fk and A3.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK
		where 1 = 1
		and a3.aliases_id is null

		Update A1
		set Qualified_Status_CR_FK = case when a0.price_new <> a0.price_cur or a1.price_new <> a1.price_cur or a2.price_new <> a2.price_cur then 653 else 654 end
		, Price_Mgmt_Type_CR_FK = case when a0.Level_Gross = 'Y' or a1.Level_Gross = 'Y' or a2.Level_Gross = 'Y' then 650 else 651 end
		, Qualified_Step = 81
		from #aliases A0
		inner join #aliases A1 on A0.Zone_Entity_Structure_FK = A1.Zone_Entity_Structure_FK and a0.Alias_Data_Value_FK = a1.Alias_Data_Value_FK and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
		inner join #aliases A2 on A1.Product_structure_FK = a2.Parity_PL_Parent_Structure_FK and a1.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK and a1.Item_Change_Type_CR_FK = a2.Item_Change_Type_CR_FK 
		left join #aliases A3 on A3.parity_alias_data_value_fk = A2.alias_data_value_fk and A3.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK
		where 1 = 1
		and a3.aliases_id is null

		Update A2
		set Qualified_Status_CR_FK = case when a0.price_new <> a0.price_cur or a1.price_new <> a1.price_cur or a2.price_new <> a2.price_cur then 653 else 654 end
		, Price_Mgmt_Type_CR_FK = case when a0.Level_Gross = 'Y' or a1.Level_Gross = 'Y' or a2.Level_Gross = 'Y' then 650 else 651 end
		, Qualified_Step = 82
		from #aliases A0
		inner join #aliases A1 on A0.Zone_Entity_Structure_FK = A1.Zone_Entity_Structure_FK and a0.Alias_Data_Value_FK = a1.Alias_Data_Value_FK and a0.Item_Change_Type_CR_FK in (656, 662) and a1.Item_Change_Type_CR_FK = 657
		inner join #aliases A2 on A1.Product_structure_FK = a2.Parity_PL_Parent_Structure_FK and a1.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK and a1.Item_Change_Type_CR_FK = a2.Item_Change_Type_CR_FK 
		left join #aliases A3 on A3.parity_alias_data_value_fk = A2.alias_data_value_fk and A3.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK
		where 1 = 1
		and a3.aliases_id is null

	Update a
	Set qualified_status_cr_fk = case when price_cur = price_new then 654 else 653 end 
	, Price_Mgmt_Type_CR_FK = case when isnull(level_gross, 'N') = 'Y' then 650 else 651 end
	, Qualified_Step = 9
	--, final_pricing_step = 9
	from #aliases a 
	where Item_Change_Type_CR_FK in (656, 662) --and Final_Pricing_Step is null

	Update A1
	Set price_new = A0.price_new
	, qualified_status_cr_fk = A0.qualified_status_cr_fk
	, Price_Mgmt_Type_CR_FK = A0.Price_Mgmt_Type_CR_FK
	, Qualified_Step = 10
	, final_pricing_step = 10
	from #aliases A0
	inner join #Aliases A1 on A0.Zone_Entity_Structure_FK = A1.Zone_Entity_Structure_FK and A0.Alias_Data_Value_FK = a1.Alias_Data_Value_FK
	where	A0.Item_Change_Type_CR_FK in (656, 662)
			and a1.Item_Change_Type_CR_FK = 657 
			and a1.final_pricing_step is null

	Update A0
	Set Qualified_Status_CR_FK = Case when A0.Qualified_Status_CR_FK = 653 or A1.qualified_status_cr_fk = 653 then 653 else 654 end
	, Qualified_Step = 11
	from #Aliases A0
	inner join #Aliases A1 on A0.Zone_Entity_Structure_FK = A1.zone_entity_structure_fk and A0.insert_step = 1000 and A0.parity_alias_data_value_fk = A1.alias_data_value_fk
	where A0.qualified_status_cr_fk <> A1.qualified_status_cr_fk

	Update A1
	Set Qualified_Status_CR_FK = Case when A0.Qualified_Status_CR_FK = 653 or A1.qualified_status_cr_fk = 653 then 653 else 654 end
	, Qualified_Step = 12
	from #Aliases A0
	inner join #Aliases A1 on A0.Zone_Entity_Structure_FK = A1.zone_entity_structure_fk and A0.insert_step = 1000 and A0.parity_alias_data_value_fk = A1.alias_data_value_fk
	where A0.qualified_status_cr_fk <> A1.qualified_status_cr_fk

	update a2
	Set a2.Price_mgmt_type_cr_fk = a1.Price_mgmt_type_cr_fk
	, a2.item_change_type_cr_fk = a1.item_change_type_cr_fk
	, a2.qualified_status_cr_fk = a1.qualified_status_cr_fk
	, a2.level_gross = a1.level_gross
	from #aliases a1
	inner join #aliases a2 on isnull(a1.parity_alias_data_value_fk, 0) = a2.alias_data_value_fk and a1.zone_entity_structure_fk = a2.zone_entity_structure_fk
	where a1.item_change_type_cr_fk = 656
	and a2.item_change_type_cr_fk = 656

------SET PRICES END-----

--Remove records with cost increases beyond zone limits
Delete 
from #Aliases 
where isnull(cost_change_effective_date_new, getdate() - (@Wk * 7)) > Cast(GETDATE() + 6 - (@Wk * 7) + isnull(daysout, 7) - DATEPART(DW, GETDATE()) as date) 
and unit_cost_new > unit_cost_cur

----Remove Alias Headers from regular items
--Delete P 
--from #Aliases P 
--where 1 = 1
--and item_change_type_cr_fk = 656
--and Alias_Data_Value_FK in (select alias_data_value_fk from #Aliases where item_change_type_cr_fk = 657 and zone_entity_structure_fk = P.zone_entity_structure_fk and parity_alias_pl_parent_structure_fk is not null)

----Remove Alias header records where alias is also in parity with an item in another alias
--Delete from #aliases where Item_Change_Type_CR_FK in (656, 662) and alias_data_value_fk in 
--(
--	Select a2.alias_data_value_fk
--	from #aliases a1
--	inner join #aliases a2 on a1.Zone_Entity_Structure_FK = a2.Zone_Entity_Structure_FK and a2.parity_pl_parent_structure_fk = a1.Product_structure_FK
--	where a2.alias_data_value_fk <> a1.alias_data_value_fk
--	group by a2.alias_data_value_fk
--)

Update Tbl
Set Subgroup_Data_Value_FK = dv.data_value_id
, Subgroup = dv.[value]
, Subgroup_Description = dv.[description]
, Notification_Date = cast(pbv.create_timestamp as date)
from #Aliases Tbl
left join epacube.segments_product sp with (nolock) on tbl.product_structure_fk = sp.product_structure_fk and sp.data_name_fk = 501045
left join epacube.data_value dv with (nolock) on sp.prod_segment_fk = dv.data_value_id
left join #pbv pbv on tbl.product_structure_fk = pbv.product_structure_fk

Update P
Set Subgroup_Data_Value_FK = (select top 1 Subgroup_Data_Value_FK from #Aliases where item_change_type_cr_fk = 657 and alias_data_value_fk = P.alias_data_value_fk and zone_entity_structure_fk = P.zone_entity_structure_fk order by isnull(ind_cost_change_item, 0) desc)
, Subgroup = (select top 1 Subgroup from #Aliases where item_change_type_cr_fk = 657 and alias_data_value_fk = P.alias_data_value_fk and zone_entity_structure_fk = P.zone_entity_structure_fk order by isnull(ind_cost_change_item, 0) desc)
, Subgroup_Description = (select top 1 Subgroup_Description from #Aliases where item_change_type_cr_fk = 657 and alias_data_value_fk = P.alias_data_value_fk and zone_entity_structure_fk = P.zone_entity_structure_fk order by isnull(ind_cost_change_item, 0) desc)
, Notification_Date = (select top 1 Notification_Date from #Aliases where item_change_type_cr_fk = 657 and alias_data_value_fk = P.alias_data_value_fk and zone_entity_structure_fk = P.zone_entity_structure_fk order by isnull(ind_cost_change_item, 0) desc)
from #Aliases P where item_change_type_cr_fk in (656, 662)

Update P
Set Item = pi.value
, item_description = pd.[description]
from #Aliases P
inner join epacube.product_identification pi with (nolock) on p.product_structure_fk = pi.product_structure_fk and pi.data_name_fk = 110100
left join epacube.product_description pd with (nolock) on P.product_structure_fk = pd.product_structure_fk and pd.data_name_fk = 110401

Update P1
Set ind_Parity = Null
from #Aliases P1
where P1.Item_Change_Type_CR_FK = 657
and P1.product_structure_fk not in (select parity_pl_parent_structure_fk from #Aliases where Item_Change_Type_CR_FK = 657 and parity_pl_parent_structure_fk is not null and zone_entity_structure_fk = P1.zone_entity_structure_fk)

Update P1
set ind_parity = 1
from #Aliases P1
inner join #Aliases P2 on p2.Parity_PL_Parent_Structure_FK = p1.product_structure_fk and p1.Zone_Entity_Structure_FK = p2.Zone_Entity_Structure_FK
--where p1.ind_parity is null

--UPdate P1
--Set ind_parity = 1
----, parity_child = 1
--from #Aliases P1
--where Parity_PL_Parent_Structure_FK is not null or PARITY_ALIAS_PL_PARENT_STRUCTURE_FK is not null or Parity_Alias_Data_Value_FK is not null

UPdate P1
Set ind_parity = 1
--, parity_child = 1
from #Aliases P1
where Parity_PL_Parent_Structure_FK is not null --or PARITY_ALIAS_PL_PARENT_STRUCTURE_FK is not null or Parity_Alias_Data_Value_FK is not null

alter table #Products add stage_for_processing_fk bigint null
alter table #aliases add stage_for_processing_fk bigint null

Update A
set Product_Structure_fk = Null
, Item = Null
from #Aliases A where item_change_type_cr_fk in (656, 662)

Update P
Set Alias_Data_Value_FK = Parity_Alias_Data_Value_fk
, Parity_Alias_Data_Value_fk = Null
from #Products P where Parity_Alias_Data_Value_fk is not null and Alias_Data_Value_FK is null

Insert into #Aliases
(Insert_Step, Alias_Data_Value_FK, ind_parity, price_cur, Price_New, Unit_Cost_Cur, Unit_Cost_New, TMpct
, Level_Gross, Item_Change_Type_CR_FK, Qualified_Status_CR_FK, Price_Mgmt_Type_CR_FK, Zone_Entity_Structure_FK
, Item_Description, Subgroup_Data_Value_FK, subgroup, Subgroup_Description, DaysOut)
Select 
Insert_Step, Alias_Data_Value_FK, ind_parity, price_cur, Price_New, Unit_Cost_Cur, Unit_Cost_New, TMpct
, Level_Gross, Item_Change_Type_CR_FK, Qualified_Status_CR_FK, Price_Mgmt_Type_CR_FK, Zone_Entity_Structure_FK
, Item_Description, Subgroup_Data_Value_FK, subgroup, Subgroup_Description, DaysOut
from (
select
1002 'Insert_Step', P.alias_data_value_fk, 1 'ind_parity', Max(P.price_cur) 'price_cur', max(P.Price_New) 'price_new', max(P.Unit_Cost_Cur) 'unit_cost_cur'
, max(P.Unit_cost_new) 'Unit_cost_new', max(P.tmpct) 'tmpct'
, P.level_gross, Case isnull(@MarginChanges, 0) when 0 then 656 else 662 end 'item_change_type_cr_fk'
, P.qualified_status_cr_fk, P.Price_Mgmt_Type_CR_FK, P.Zone_Entity_Structure_FK, 'In Parity to ALIAS ' + A.Alias_Data_Value_FK 'Item_Description', P.Subgroup_Data_Value_FK, P.Subgroup
, P.Subgroup_Description, P.DaysOut
, Dense_Rank()over(partition by p.alias_data_value_fk order by p.subgroup_data_value_fk) DRank
from #Aliases P 
inner join #Aliases A on P.Zone_Entity_Structure_FK = A.Zone_Entity_Structure_FK and A.Parity_Alias_Data_Value_FK = P.Alias_Data_Value_FK and a.insert_step = 1002
where P.Alias_Data_Value_FK is not null
--and (select count(*) from epacube.epacube.product_mult_type where Entity_Structure_FK = P.zone_entity_structure_fk and data_value_fk = P.Alias_Data_Value_FK and RECORD_STATUS_CR_FK = 1) > 1
and P.alias_data_value_fk not in (select alias_data_value_fk from #Aliases 
	where item_change_type_cr_fk = Case isnull(@MarginChanges, 0) when 0 then 656 else 662 end
	and zone_entity_structure_fk = P.zone_entity_structure_fk)
group by
P.alias_data_value_fk, P.level_gross
, P.qualified_status_cr_fk, P.Price_Mgmt_Type_CR_FK, P.Zone_Entity_Structure_FK, 'In Parity to ALIAS ' + A.Alias_Data_Value_FK, P.Subgroup_Data_Value_FK, P.Subgroup
, P.Subgroup_Description, P.DaysOut
) B where DRank = 1

Insert into #Aliases
(Insert_Step, Alias_Data_Value_FK, ind_parity, price_cur, Price_New, Unit_Cost_Cur, Unit_Cost_New, TMpct, Level_Gross, Item_Change_Type_CR_FK, Qualified_Status_CR_FK, Price_Mgmt_Type_CR_FK, Zone_Entity_Structure_FK
, Item_Description, Subgroup_Data_Value_FK, subgroup, Subgroup_Description, DaysOut)
select  
1001 'Insert_Step', P.alias_data_value_fk, 1 'ind_parity', P.price_cur, P.Price_New, P.Unit_Cost_Cur, P.Unit_cost_new, P.tmpct, P.level_gross
, Case isnull(@MarginChanges, 0) when 0 then 656 else 662 end 'item_change_type_cr_fk'
, P.qualified_status_cr_fk, P.Price_Mgmt_Type_CR_FK, P.Zone_Entity_Structure_FK, 'Alias/Parity to REGULAR ITEM ' + pi.value 'Item_Description', P.Subgroup_Data_Value_FK, P.Subgroup
, P.Subgroup_Description, P.DaysOut
from #Products P 
inner join epacube.epacube.product_identification pi with (nolock) on p.Parity_PL_Parent_Structure_FK = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110100
left join #aliases a on p.alias_data_value_fk = a.alias_data_value_fk and p.zone_entity_structure_fk = a.zone_entity_structure_fk
where P.Alias_Data_Value_FK is not null
and a.alias_data_value_fk is null
and (select count(*) from epacube.epacube.product_mult_type where Entity_Structure_FK = P.zone_entity_structure_fk and data_value_fk = P.Alias_Data_Value_FK and RECORD_STATUS_CR_FK = 1) > 1

Update prc
Set QUALIFIED_STATUS_CR_FK = p.QUALIFIED_STATUS_CR_FK
, CUR_CASE_COST = isnull(P.Case_Cost_Cur, P.Case_Cost_New)
, cur_cost = isnull(P.Unit_Cost_Cur, Unit_Cost_new)
, price_cur = p.price_cur
, cur_gm_pct = Cast(Case when isnull(p.unit_Cost_Cur, p.Unit_Cost_new) = 0 then Null else (p.Price_Cur - isnull(p.Unit_Cost_Cur, p.Unit_Cost_new)) / (p.Price_Cur) end as Numeric(18, 4))
, price_effective_date_cur = p.Price_Effective_Date_Cur
, new_case_cost = p.Case_Cost_New
, new_cost = isnull(P.Unit_Cost_New, P.Unit_Cost_Cur)
, COST_CHANGE = Case when isnull(P.unit_cost_cur, 0) = isnull(P.Unit_cost_new, 0) or isnull(P.unit_cost_cur, 0) = 0 or isnull(P.Unit_cost_new, 0) = 0 then Null else P.unit_cost_new - P.unit_cost_Cur end
, price_change = Case when isnull(P.price_cur, 0) = isnull(P.price_new, 0) or isnull(P.price_cur, 0) = 0 or isnull(P.price_new, 0) = 0 then Null else P.price_new - P.price_cur end
, price_new = p.Price_New
, NEW_GM_PCT = Cast(Case when isnull(p.unit_Cost_Cur, 0) = 0 then Null else (p.Price_new - isnull(p.Unit_Cost_new, p.Unit_Cost_Cur)) / (p.Price_New) end as Numeric(18, 4))
, ind_Margin_Tol = Case when Cast(Case when isnull(p.unit_Cost_Cur, 0) = 0 then Null else (p.Price_new - isnull(p.Unit_Cost_new, p.Unit_Cost_Cur)) / (p.Price_New) end as Numeric(18, 4)) - Cast(Case when isnull(p.unit_Cost_Cur, 0) = 0 then Null else (p.Price_Cur - p.Unit_Cost_Cur) / (p.Price_Cur) end as Numeric(18, 4))
	> mt.margin_tol then 1 
	when Cast(Case when isnull(p.unit_Cost_Cur, 0) = 0 then Null else (p.Price_new - isnull(p.Unit_Cost_new, p.Unit_Cost_Cur)) / (p.Price_New) end as Numeric(18, 4)) - Cast(Case when isnull(p.unit_Cost_Cur, 0) = 0 then Null else (p.Price_Cur - p.Unit_Cost_Cur) / (p.Price_Cur) end as Numeric(18, 4))
	< mt.margin_tol * -1 then -1 else Null end
, COST_CHANGE_EFFECTIVE_DATE = p.Cost_Change_Effective_Date_New
, NOTIFICATION_DATE = p.Notification_Date
, srp_initial = p.Price_Init
, Digit_DN_FK = P.Digit_DN_FK
, price_multiple_new = p.price_multiple_cur
, srp_adjusted = Null
, GM_ADJUSTED = null
, TM_ADJUSTED = null
, HOLD_CUR_PRICE = Null
, reviewed = null
, Update_Timestamp = getdate()
, ind_cost_change_item = p.ind_Cost_Change_Item
, ind_parity = p.ind_Parity
, ind_rescind = 2
, cost_change_reason = p.REASON_FOR_INCLUSION
, update_user_price = Null
, UPDATE_USER_SET_TM = Null
, UPDATE_USER_HOLD = null
, UPDATE_USER_REVIEWED = Null
, PRICESHEET_BASIS_VALUES_FK = isnull(p.Pricesheet_basis_Values_ID_New, p.Pricesheet_Basis_Values_ID_Cur)
, update_steps = p.Pricing_Update_Steps
, comments_rescind = prc.comments_rescind + '; ' + 'New cost in place'
from marginmgr.pricesheet_retail_changes prc
inner join
		(
			select * from (
						select Dense_Rank()over(partition by product_structure_fk, zone_entity_structure_fk order by [source], reason_for_inclusion, qualified_status_cr_fk, products_id) DRank 
						, *
							from (
								select 'products' 'source', * from #products
								union
								select 'aliases' 'source', * from #aliases
							) a
					where 1 = 1
			) b where drank = 1
		) P on prc.insert_step = p.insert_step and isnull(prc.product_structure_fk, prc.alias_id) = isnull(p.product_structure_fk, p.Alias_Data_Value_FK) and prc.ZONE_ENTITY_STRUCTURE_FK = p.Zone_Entity_Structure_FK
inner join epacube.epacube.entity_identification eiz with (nolock) on P.zone_entity_structure_fk = eiz.entity_structure_fk and eiz.entity_data_name_fk = 151000
left join epacube.epacube.entity_ident_nonunique einz with (nolock) on P.zone_entity_structure_fk = einz.entity_structure_fk and einz.entity_data_name_fk = 151000 and einz.data_name_fk = 151112
left join epacube.epacube.data_value dv_alias on Case when P.alias_data_value_fk like '%-%' then left(P.alias_data_value_fk, charindex('-', P.alias_data_value_fk) -1) else P.alias_data_value_fk end = cast(dv_alias.data_value_id as varchar(64))
cross Join (Select cast(ep.value as numeric(18, 2)) 'margin_tol' from epacube.epacube.epacube_params ep Where ep.application_scope_fk = 5013 and ep.[name] = 'MARGIN CHANGE TOLERENCE') mt
where prc.PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
and prc.IND_RESCIND = 1

If Object_ID('tempdb..#Competitors') is not null
drop table #Competitors

Create table #Competitors(product_structure_fk bigint not null)

Insert into #Competitors
Select cv.product_structure_fk from [epacube].[COMPETITOR_VALUES] CV with (nolock) 
where cv.PRODUCT_STRUCTURE_FK is not null group by cv.product_structure_fk

Create index idx_01 on #Competitors(product_structure_fk)

update prc
set product_structure_fk = comps.product_structure_fk
from marginmgr.PRICESHEET_RETAIL_CHANGES prc
inner join (
				Select prc2.PRODUCT_STRUCTURE_FK, prc1.alias_id, prc2.item, prc1.PRICESHEET_RETAIL_CHANGES_id
				, Dense_Rank()over(partition by prc1.alias_id, prc1.zone_entity_structure_fk order by cast(left(prc2.item, len(prc2.item)-1) as bigint)) DRank
				from marginmgr.PRICESHEET_RETAIL_CHANGES prc1 with (nolock)
				inner join marginmgr.PRICESHEET_RETAIL_CHANGES prc2 with (nolock) on prc1.PRICE_CHANGE_EFFECTIVE_DATE = prc2.PRICE_CHANGE_EFFECTIVE_DATE
																					and prc1.ZONE_ENTITY_STRUCTURE_FK = prc2.ZONE_ENTITY_STRUCTURE_FK
																					and prc1.alias_id = prc2.alias_id
																					and prc1.ind_margin_changes = prc2.ind_margin_changes
				inner join #Competitors c with (nolock) on prc2.PRODUCT_STRUCTURE_FK = c.product_structure_fk
				where prc1.PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date
				and prc1.IND_MARGIN_CHANGES = @MarginChanges
				and prc1.ITEM_CHANGE_TYPE_CR_FK in (656, 662)
				and prc2.item_change_type_cr_fk not in (656, 662)
			) comps on prc.PRICESHEET_RETAIL_CHANGES_ID = comps.PRICESHEET_RETAIL_CHANGES_ID

--if object_id('dbo.pentaho_processing_test_log') is not null
--Begin
--	Update pptl
--	Set process_time_end = getdate()
--	from dbo.pentaho_processing_test_log pptl where pentaho_processing_test_log_id = @QryID
--End

eof:
