﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [precision].[weekly_rims_epa_level_gross_comparison] @Z_FK bigint = 0, @zone bigint = 0, @wks int = 0 
AS
	SET NOCOUNT ON;
	
--Declare @Z_FK bigint = 0, @zone bigint = 30011, @wks int = 0

Declare @Eff_Date date
Declare @CostChgProcDate date
Set @Eff_Date = Cast(GETDATE() + 6 - (@Wks * 7) - DATEPART(DW, GETDATE()) as date)

Set @Z_FK = Case when @zone = 0 then @Z_FK else (select entity_structure_fk from epacube.epacube.entity_identification where entity_data_name_fk = 151000 and value = cast(@zone as varchar(64))) end

Select 'Aliases'  'Data Type'

Select 'RIMS epa Price mismatch' 'Reason', a.* from (
Select
'RIMS' 'Source'
, ZONE_NUM
, Alias 'Alias_ID'
, Qual_Flg
, item
, isnull(replace(paritem, '0', ''), '') + isnull(paritmtyp, '') 'Parity_Item'
, (select top 1 effective_date from imports.urm_level_gross_audit where item = lga.item and zone_entity_structure_fk = lga.zone_entity_structure_fk and effective_date < lga.effective_date order by effective_date desc) 'Last RIMS'
, (select top 1 cast(rescind_timestamp as date) 'rescinded' from epacube.marginmgr.pricesheet_basis_values where product_structure_fk = lga.product_structure_fk and effective_date = New_Cost_Effective_Date order by effective_date desc) 'Rescinded'
, Null 'Cost_Change_Reason'
, New_Cost_Effective_Date
, (Select top 1 Cost_Change_Processed_Date from epacube.marginmgr.PRICESHEET_BASIS_VALUES where data_name_fk = 503201 and Product_Structure_FK = lga.PRODUCT_STRUCTURE_FK and isnull(Cost_Change_Processed_Date, getdate()) < @Eff_Date order by effective_date desc) 'Last_Cost_Change_Processed_Date'

, Null 'Use_PPTs'
, Null 'price_initial'
, Null 'price_rounded'
, Null 'SRP_Digit'
, Null 'Digit_Adj'
, New_prc 'New Sell Price'
, price 'price_cur'
, TM_PCT
, PACK
, unit_cost_cur
, unit_cost_new
, NEWGMPCT 'NEW_GM_PCT'
, uom_code
, prc_mult
, lga.product_structure_fk
, URM_Level_Gross_Audit_ID 'Foreign_Key'
, dense_rank()over(partition by alias, zone_entity_structure_fk order by New_prc desc, Case when unit_cost_new = unit_cost_cur then 0 else unit_cost_new end desc, product_structure_fk) 'DRank'
, Cast(null as int) 'insert_step'
, cast(null as varchar(128)) 'Pricing_Steps'
from imports.urm_level_gross_audit lga with (nolock) 
where 1 = 1
and Alias is not null
and zone_entity_structure_fk = @Z_FK
and Effective_Date = @Eff_Date
and cast(lga.alias as varchar(64)) not in (select cast(alias_id as varchar(64)) from epacube.marginmgr.pricesheet_retail_changes where price_change_effective_date = lga.effective_date and zone_entity_structure_fk = @Z_FK and insert_step in ('1001', '1002'))

Union

Select 
'Precision' 'Source'
, Zone
, prc_656.Alias_id
, Case prc_656.qualified_status_cr_fk when 653 then 'Y' when 654 then 'N' end 'Qual_Flg'
, ln_itm.item
, (Select top 1 item from epacube.marginmgr.pricesheet_retail_changes with (nolock) where price_change_Effective_Date = ln_itm.price_change_Effective_Date and parity_pl_parent_structure_fk = ln_itm.product_structure_fk and isnull(ind_cost_change_item, 0) = 1 order by price_new desc) 'Parity_Item'
, (Select top 1 effective_date from imports.urm_level_gross_audit lga with (nolock) where item = ln_itm.item and zone_entity_structure_fk = ln_itm.zone_entity_structure_fk and effective_date < @Eff_Date order by effective_date desc) 'Last RIMS'
, (select top 1 cast(rescind_timestamp as date) 'rescinded' from epacube.marginmgr.pricesheet_basis_values where product_structure_fk = ln_itm.product_structure_fk and effective_date = ln_itm.cost_change_effective_date) 'Rescinded'
, ln_itm.Cost_Change_Reason
, cast(ln_itm.cost_change_effective_date as varchar(16)) 'cost_change_effective_date'
, (Select top 1 Cost_Change_Processed_Date from epacube.marginmgr.PRICESHEET_BASIS_VALUES where data_name_fk = 503201 and Product_Structure_FK = ln_itm.PRODUCT_STRUCTURE_FK and isnull(Cost_Change_Processed_Date, getdate()) < @Eff_Date order by effective_date desc) 'Last_Cost_Change_Processed_Date'

, ln_itm.Use_LG_Price_Points 'Use_PPTs'
, ln_itm.price_initial
, ln_itm.price_rounded
, ln_itm.SRP_Digit
, ln_itm.Digit_Adj
, ln_itm.price_new 'New Sell Price'
, ln_itm.price_cur
, Cast(prc_656.Target_Margin * 100 as Numeric(18, 2)) 'Target_Margin'
, ln_itm.STORE_PACK
, ln_itm.unit_cost_cur
, ln_itm.unit_cost_new
, Cast(prc_656.NEW_GM_PCT as Numeric(18, 2)) 'New_GM_Pct'
, Null 'UOM_Code' 
, prc_656.PRICE_MULTIPLE_CUR
, ln_itm.product_structure_fk
, ln_itm.PRICESHEET_RETAIL_CHANGES_ID 'Foreign_Key'
, 1 'DRank'
, ln_itm.insert_step
, ln_itm.Pricing_Steps
from epacube.marginmgr.pricesheet_retail_changes prc_656
inner join
	(Select * from 
		(Select prc_657.zone_entity_structure_fk, prc_657.Alias_id, prc_657.Use_LG_Price_Points, prc_657.price_initial, prc_657.price_rounded, prc_657.SRP_Digit, prc_657.Digit_Adj
		, prc_657.cost_change_effective_date, prc_657.unit_cost_cur, prc_657.unit_cost_new, prc_657.price_cur, prc_657.price_new, item, STORE_PACK, pricesheet_retail_changes_id
		, dense_rank()over(partition by prc_657.Alias_id, prc_657.zone_entity_structure_fk order by prc_657.price_new desc, prc_657.unit_cost_new desc, prc_657.product_structure_fk) 'DRank'
		, prc_657.product_structure_fk, prc_657.insert_step, prc_657.Pricing_Steps, prc_657.Cost_Change_Reason, prc_657.price_change_Effective_Date
		from epacube.marginmgr.pricesheet_retail_changes prc_657
		where prc_657.ZONE_ENTITY_STRUCTURE_FK = @Z_FK 
			and prc_657.ITEM_CHANGE_TYPE_CR_FK = 657
			and prc_657.ind_cost_change_item = 1
			and prc_657.price_change_effective_date = @Eff_Date 
		) a where DRank = 1
	) ln_itm on prc_656.Alias_id = ln_itm.Alias_id and prc_656.ZONE_ENTITY_STRUCTURE_FK = ln_itm.ZONE_ENTITY_STRUCTURE_FK

where prc_656.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date
and prc_656.ZONE_ENTITY_STRUCTURE_FK = @Z_FK 
and prc_656.ITEM_CHANGE_TYPE_CR_FK = 656
and prc_656.Alias_id is not null
and prc_656.Alias_id not in (select Alias_id from epacube.marginmgr.pricesheet_retail_changes where price_change_effective_date = prc_656.price_change_effective_date and zone_entity_structure_fk = @Z_FK and insert_step in (1001, 1002))
) A 
where a.drank = 1 
and a.Alias_id in
(
Select a.Alias_id from (
Select
lga.Alias 'Alias_Id'
, lga.zone_entity_structure_fk
, lga.New_prc 'New_Sell_Price'
, dense_rank()over(partition by lga.Alias, lga.zone_entity_structure_fk order by lga.New_prc desc, lga.unit_cost_new desc, lga.URM_Level_Gross_Audit_ID) 'DRank'
from imports.urm_level_gross_audit lga with (nolock) 
where 1 = 1
and lga.zone_entity_structure_fk = @Z_FK
and lga.Effective_Date = @Eff_Date
and lga.Alias is not null
) a 
inner join
	(
		Select
		prc_656.Alias_id
		, prc_656.ZONE_ENTITY_STRUCTURE_FK
		, prc_656.PRICE_NEW 'New_Sell_Price'
		from epacube.marginmgr.pricesheet_retail_changes prc_656
		where 1 = 1
		and prc_656.ZONE_ENTITY_STRUCTURE_FK = @Z_FK
		and prc_656.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date 
		and prc_656.Alias_id is not null
		and prc_656.ITEM_CHANGE_TYPE_CR_FK = 656
	) b on a.Alias_id = b.Alias_id
			and a.zone_entity_structure_fk = b.ZONE_ENTITY_STRUCTURE_FK
where a.DRank = 1
and isnull(b.New_Sell_Price, 0) <> a.New_Sell_Price
)
order by a.Alias_id, a.source desc

Select 'In RIMS not in Precision' 'Reason', a.* from (
Select
'RIMS' 'Source'
, ZONE_NUM
, Alias 'Alias_ID'
, Qual_Flg
, item
, isnull(replace(paritem, '0', ''), '') + isnull(paritmtyp, '') 'Parity_Item'
, (select top 1 effective_date from imports.urm_level_gross_audit where item = lga.item and zone_entity_structure_fk = lga.zone_entity_structure_fk and effective_date < lga.effective_date order by effective_date desc) 'Last RIMS'
, (select top 1 cast(rescind_timestamp as date) 'rescinded' from epacube.marginmgr.pricesheet_basis_values where product_structure_fk = lga.product_structure_fk and effective_date = New_Cost_Effective_Date order by effective_date desc) 'Rescinded'
, Null 'Cost_Change_Reason'
, New_Cost_Effective_Date
, (Select top 1 Cost_Change_Processed_Date from epacube.marginmgr.PRICESHEET_BASIS_VALUES where data_name_fk = 503201 and Product_Structure_FK = lga.PRODUCT_STRUCTURE_FK and isnull(Cost_Change_Processed_Date, getdate()) < @Eff_Date order by effective_date desc) 'Last_Cost_Change_Processed_Date'

, Null 'Use_PPTs'
, Null 'price_initial'
, Null 'price_rounded'
, Null 'SRP_Digit'
, Null 'Digit_Adj'
, New_prc 'New Sell Price'
, price 'price_cur'
, TM_PCT
, PACK
, unit_cost_cur
, unit_cost_new
, NEWGMPCT 'NEW_GM_PCT'
, uom_code
, prc_mult
, lga.product_structure_fk
, URM_Level_Gross_Audit_ID 'Foreign_Key'
, dense_rank()over(partition by alias, zone_entity_structure_fk order by New_prc desc, Case when unit_cost_new = unit_cost_cur then 0 else unit_cost_new end desc, product_structure_fk) 'DRank'
, Cast(null as int) 'insert_step'
, cast(null as varchar(128)) 'Pricing_Steps'
from imports.urm_level_gross_audit lga with (nolock) 
where 1 = 1
and Alias is not null
and zone_entity_structure_fk = @Z_FK
and Effective_Date = @Eff_Date

Union

Select 
'Precision' 'Source'
, Zone
, prc_656.Alias_id
, Case prc_656.qualified_status_cr_fk when 653 then 'Y' when 654 then 'N' end 'Qual_Flg'
, ln_itm.item
, (Select top 1 item from epacube.marginmgr.pricesheet_retail_changes with (nolock) where price_change_Effective_Date = ln_itm.price_change_Effective_Date and parity_pl_parent_structure_fk = ln_itm.product_structure_fk and isnull(ind_cost_change_item, 0) = 1 order by price_new desc) 'Parity_Item'
, (Select top 1 effective_date from imports.urm_level_gross_audit lga with (nolock) where item = ln_itm.item and zone_entity_structure_fk = ln_itm.zone_entity_structure_fk and effective_date < @Eff_Date order by effective_date desc) 'Last RIMS'
, (select top 1 cast(rescind_timestamp as date) 'rescinded' from epacube.marginmgr.pricesheet_basis_values where product_structure_fk = ln_itm.product_structure_fk and effective_date = ln_itm.cost_change_effective_date) 'Rescinded'
, ln_itm.Cost_Change_Reason
, cast(ln_itm.cost_change_effective_date as varchar(16)) 'cost_change_effective_date'
, (Select top 1 Cost_Change_Processed_Date from epacube.marginmgr.PRICESHEET_BASIS_VALUES where data_name_fk = 503201 and Product_Structure_FK = ln_itm.PRODUCT_STRUCTURE_FK and isnull(Cost_Change_Processed_Date, getdate()) < @Eff_Date order by effective_date desc) 'Last_Cost_Change_Processed_Date'

, ln_itm.Use_LG_Price_Points 'Use_PPTs'
, ln_itm.price_initial
, ln_itm.price_rounded
, ln_itm.SRP_Digit
, ln_itm.Digit_Adj
, ln_itm.price_new 'New Sell Price'
, ln_itm.price_cur
, Cast(prc_656.Target_Margin * 100 as Numeric(18, 2)) 'Target_Margin'
, ln_itm.STORE_PACK
, ln_itm.unit_cost_cur
, ln_itm.unit_cost_new
, Cast(prc_656.NEW_GM_PCT as Numeric(18, 2)) 'New_GM_Pct'
, Null 'UOM_Code' 
, prc_656.PRICE_MULTIPLE_CUR
, ln_itm.product_structure_fk
, ln_itm.PRICESHEET_RETAIL_CHANGES_ID 'Foreign_Key'
, 1 'DRank'
, ln_itm.insert_step
, ln_itm.Pricing_Steps
from epacube.marginmgr.pricesheet_retail_changes prc_656
inner join
	(Select * from 
		(Select prc_657.zone_entity_structure_fk, prc_657.Alias_id, prc_657.Use_LG_Price_Points, prc_657.price_initial, prc_657.price_rounded, prc_657.SRP_Digit, prc_657.Digit_Adj
		, prc_657.cost_change_effective_date, prc_657.unit_cost_cur, prc_657.unit_cost_new, prc_657.price_cur, prc_657.price_new, item, STORE_PACK, pricesheet_retail_changes_id
		, dense_rank()over(partition by prc_657.Alias_id, prc_657.zone_entity_structure_fk order by prc_657.price_new desc, prc_657.unit_cost_new desc, prc_657.product_structure_fk) 'DRank'
		, prc_657.product_structure_fk, prc_657.insert_step, prc_657.Pricing_Steps, prc_657.Cost_Change_Reason, prc_657.price_change_Effective_Date
		from epacube.marginmgr.pricesheet_retail_changes prc_657
		where prc_657.ZONE_ENTITY_STRUCTURE_FK = @Z_FK 
			and prc_657.ITEM_CHANGE_TYPE_CR_FK = 657
			and prc_657.ind_cost_change_item = 1
			and prc_657.price_change_effective_date = @Eff_Date 
		) a where DRank = 1
	) ln_itm on prc_656.Alias_id = ln_itm.Alias_id and prc_656.ZONE_ENTITY_STRUCTURE_FK = ln_itm.ZONE_ENTITY_STRUCTURE_FK

where prc_656.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date
and prc_656.ZONE_ENTITY_STRUCTURE_FK = @Z_FK 
and prc_656.ITEM_CHANGE_TYPE_CR_FK = 656
and prc_656.Alias_id is not null
) A 
where a.drank = 1 
and a.Alias_id in
(
Select a.Alias_id from (
Select
lga.Alias 'Alias_Id'
, lga.zone_entity_structure_fk
, lga.New_prc 'New_Sell_Price'
, dense_rank()over(partition by lga.Alias, lga.zone_entity_structure_fk order by lga.New_prc desc, lga.unit_cost_new desc, lga.URM_Level_Gross_Audit_ID) 'DRank'
from imports.urm_level_gross_audit lga with (nolock) 
where 1 = 1
and lga.zone_entity_structure_fk = @Z_FK
and lga.Effective_Date = @Eff_Date
and lga.Alias is not null
) a 
Left join
	(
		Select
		prc_656.Alias_id
		, prc_656.ZONE_ENTITY_STRUCTURE_FK
		, prc_656.PRICE_NEW 'New_Sell_Price'
		, prc_656.PRICESHEET_RETAIL_CHANGES_ID
		from epacube.marginmgr.pricesheet_retail_changes prc_656
		where 1 = 1
		and prc_656.ZONE_ENTITY_STRUCTURE_FK = @Z_FK
		and prc_656.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date 
		and prc_656.Alias_id is not null
		and prc_656.ITEM_CHANGE_TYPE_CR_FK = 656
	) b on a.Alias_id = b.Alias_id
			and a.zone_entity_structure_fk = b.ZONE_ENTITY_STRUCTURE_FK
where a.DRank = 1
and b.PRICESHEET_RETAIL_CHANGES_ID is null
)
order by a.Alias_id, a.source desc

Select 'In Precision not in RIMS' 'Reason', a.* from (
--Select
--'RIMS' 'Source'
--, ZONE_NUM
--, Alias
--, Qual_Flg
--, item
--, isnull(replace(paritem, '0', ''), '') + isnull(paritmtyp, '') 'Parity_Item'
--, (select top 1 effective_date from imports.urm_level_gross_audit where item = lga.item and zone_entity_structure_fk = lga.zone_entity_structure_fk and effective_date < lga.effective_date order by effective_date desc) 'Last RIMS'
--, (select top 1 cast(rescind_timestamp as date) 'rescinded' from epacube.marginmgr.pricesheet_basis_values where product_structure_fk = lga.product_structure_fk and effective_date = New_Cost_Effective_Date order by effective_date desc) 'Rescinded'
--, Null 'Cost_Change_Reason'
--, New_Cost_Effective_Date
--, (Select top 1 Cost_Change_Processed_Date from epacube.marginmgr.PRICESHEET_BASIS_VALUES where data_name_fk = 503201 and Product_Structure_FK = lga.PRODUCT_STRUCTURE_FK and isnull(Cost_Change_Processed_Date, getdate()) < @Eff_Date order by effective_date desc) 'Last_Cost_Change_Processed_Date'

--, Null 'Use_PPTs'
--, Null 'price_initial'
--, Null 'price_rounded'
--, Null 'SRP_Digit'
--, Null 'Digit_Adj'
--, New_prc 'New Sell Price'
--, price 'price_cur'
--, TM_PCT
--, PACK
--, unit_cost_cur
--, unit_cost_new
--, NEWGMPCT 'NEW_GM_PCT'
--, uom_code
--, prc_mult
--, lga.product_structure_fk
--, URM_Level_Gross_Audit_ID 'Foreign_Key'
--, dense_rank()over(partition by alias, zone_entity_structure_fk order by New_prc desc, Case when unit_cost_new = unit_cost_cur then 0 else unit_cost_new end desc, product_structure_fk) 'DRank'
--, Cast(null as int) 'insert_step'
--, cast(null as varchar(128)) 'Pricing_Steps'
--from imports.urm_level_gross_audit lga with (nolock) 
--where 1 = 1
--and Alias is not null
--and zone_entity_structure_fk = @Z_FK
--and Effective_Date = @Eff_Date

--Union

Select 
Null 'Source'
, Zone
, prc_656.Alias_id
, Case prc_656.qualified_status_cr_fk when 653 then 'Y' when 654 then 'N' end 'Qual_Flg'
, ln_itm.item
, (Select top 1 item from epacube.marginmgr.pricesheet_retail_changes with (nolock) where price_change_Effective_Date = ln_itm.price_change_Effective_Date and parity_pl_parent_structure_fk = ln_itm.product_structure_fk and isnull(ind_cost_change_item, 0) = 1 order by price_new desc) 'Parity_Item'
, (Select top 1 effective_date from imports.urm_level_gross_audit lga with (nolock) where item = ln_itm.item and zone_entity_structure_fk = ln_itm.zone_entity_structure_fk and effective_date < @Eff_Date order by effective_date desc) 'Last RIMS'
, (select top 1 cast(rescind_timestamp as date) 'rescinded' from epacube.marginmgr.pricesheet_basis_values where product_structure_fk = ln_itm.product_structure_fk and effective_date = ln_itm.cost_change_effective_date) 'Rescinded'
, ln_itm.Cost_Change_Reason
, cast(ln_itm.cost_change_effective_date as varchar(16)) 'cost_change_effective_date'
, (Select top 1 Cost_Change_Processed_Date from epacube.marginmgr.PRICESHEET_BASIS_VALUES where data_name_fk = 503201 and Product_Structure_FK = ln_itm.PRODUCT_STRUCTURE_FK and isnull(Cost_Change_Processed_Date, getdate()) < @Eff_Date order by effective_date desc) 'Last_Cost_Change_Processed_Date'

, ln_itm.Use_LG_Price_Points 'Use_PPTs'
, ln_itm.price_initial
, ln_itm.price_rounded
, ln_itm.SRP_Digit
, ln_itm.Digit_Adj
, ln_itm.price_new 'New Sell Price'
, ln_itm.price_cur
, Cast(prc_656.Target_Margin * 100 as Numeric(18, 2)) 'Target_Margin'
, ln_itm.STORE_PACK
, ln_itm.unit_cost_cur
, ln_itm.unit_cost_new
, Cast(prc_656.NEW_GM_PCT as Numeric(18, 2)) 'New_GM_Pct'
, Null 'UOM_Code' 
, prc_656.PRICE_MULTIPLE_CUR
, ln_itm.product_structure_fk
, ln_itm.PRICESHEET_RETAIL_CHANGES_ID 'Foreign_Key'
, 1 'DRank'
, ln_itm.insert_step
, ln_itm.Pricing_Steps
from epacube.marginmgr.pricesheet_retail_changes prc_656
inner join
	(Select * from 
		(Select prc_657.zone_entity_structure_fk, prc_657.Alias_id, prc_657.Use_LG_Price_Points, prc_657.price_initial, prc_657.price_rounded, prc_657.SRP_Digit, prc_657.Digit_Adj
		, prc_657.cost_change_effective_date, prc_657.unit_cost_cur, prc_657.unit_cost_new, prc_657.price_cur, prc_657.price_new, item, STORE_PACK, pricesheet_retail_changes_id
		, dense_rank()over(partition by prc_657.Alias_id, prc_657.zone_entity_structure_fk order by prc_657.price_new desc, prc_657.unit_cost_new desc, prc_657.product_structure_fk) 'DRank'
		, prc_657.product_structure_fk, prc_657.insert_step, prc_657.Pricing_Steps, prc_657.Cost_Change_Reason, prc_657.price_change_Effective_Date
		from epacube.marginmgr.pricesheet_retail_changes prc_657
		where prc_657.ZONE_ENTITY_STRUCTURE_FK = @Z_FK 
			and prc_657.ITEM_CHANGE_TYPE_CR_FK = 657
			and prc_657.ind_cost_change_item = 1
			and prc_657.price_change_effective_date = @Eff_Date 
		) a where DRank = 1
	) ln_itm on prc_656.Alias_id = ln_itm.Alias_id and prc_656.ZONE_ENTITY_STRUCTURE_FK = ln_itm.ZONE_ENTITY_STRUCTURE_FK
where prc_656.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date
and prc_656.ZONE_ENTITY_STRUCTURE_FK = @Z_FK 
and prc_656.ITEM_CHANGE_TYPE_CR_FK = 656
and prc_656.Alias_id is not null
and prc_656.Alias_id not in (select Alias_id from epacube.marginmgr.pricesheet_retail_changes where price_change_effective_date = prc_656.price_change_effective_date and zone_entity_structure_fk = @Z_FK and insert_step in (1001, 1002))
) A 
where a.drank = 1 
and a.Alias_id in
(
Select a.Alias_id from (
		Select
		prc_656.Alias_id
		, prc_656.ZONE_ENTITY_STRUCTURE_FK
		, prc_656.PRICE_NEW 'New_Sell_Price'
		, prc_656.PRICESHEET_RETAIL_CHANGES_ID
		from epacube.marginmgr.pricesheet_retail_changes prc_656
		where 1 = 1
		and prc_656.ZONE_ENTITY_STRUCTURE_FK = @Z_FK
		and prc_656.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date 
		and prc_656.Alias_id is not null
		and prc_656.ITEM_CHANGE_TYPE_CR_FK = 656
) a 
Left join
	(
		Select
		lga.Alias 'Alias_Id'
		, lga.zone_entity_structure_fk
		, lga.New_prc 'New_Sell_Price'
		, dense_rank()over(partition by lga.Alias, lga.zone_entity_structure_fk order by lga.New_prc desc, lga.unit_cost_new desc, lga.URM_Level_Gross_Audit_ID) 'DRank'
		from imports.urm_level_gross_audit lga with (nolock) 
		where 1 = 1
		and lga.zone_entity_structure_fk = @Z_FK
		and lga.Effective_Date = @Eff_Date
		and lga.Alias is not null

	) b on a.Alias_id = b.Alias_id
			and a.zone_entity_structure_fk = b.ZONE_ENTITY_STRUCTURE_FK
where b.DRank is null
)
order by a.Alias_id

Select 'Regular Items' 'Data Type'

Select 'RIMS epa Price mismatch' 'Reason', a.* from (
Select
'RIMS' 'Source'
, ZONE_NUM
, Qual_Flg
, item
, isnull(replace(paritem, '0', ''), '') + isnull(paritmtyp, '') 'Parity_Item'
, (select top 1 effective_date from imports.urm_level_gross_audit where item = lga.item and zone_entity_structure_fk = lga.zone_entity_structure_fk and effective_date < @Eff_Date order by effective_date desc) 'Last RIMS'
, (select top 1 cast(rescind_timestamp as date) 'rescinded' from epacube.marginmgr.pricesheet_basis_values where product_structure_fk = lga.product_structure_fk and effective_date = New_Cost_Effective_Date order by effective_date desc) 'Rescinded'
, Null 'Cost_Change_Reason'
, New_Cost_Effective_Date
, (Select top 1 Cost_Change_Processed_Date from epacube.marginmgr.PRICESHEET_BASIS_VALUES where data_name_fk = 503201 and Product_Structure_FK = lga.PRODUCT_STRUCTURE_FK and isnull(Cost_Change_Processed_Date, getdate()) < @Eff_Date order by effective_date desc) 'Last_Cost_Change_Processed_Date'
, Null 'Use_PPTs'
, Null 'price_initial'
, Null 'price_rounded'
, Null 'SRP_Digit'
, Null 'Digit_Adj'
, New_prc 'New Sell Price'
, price 'price_cur'
, TM_PCT
, PACK
, unit_cost_cur
, unit_cost_new
, NEWGMPCT 'NEW_GM_PCT'
, uom_code
, prc_mult
, lga.product_structure_fk
, URM_Level_Gross_Audit_ID 'Foreign_Key'
, dense_rank()over(partition by Alias, zone_entity_structure_fk order by New_prc desc, Case when unit_cost_new = unit_cost_cur then 0 else unit_cost_new end desc, product_structure_fk) 'DRank'
, Cast(null as int) 'insert_step'
, cast(null as varchar(128)) 'Pricing_Steps'
from imports.urm_level_gross_audit lga with (nolock) 
where 1 = 1
and Alias is null
and zone_entity_structure_fk = @Z_FK
and Effective_Date = @Eff_Date

Union

Select 
'Precision' 'Source'
, Zone
, Case prc_655.qualified_status_cr_fk when 653 then 'Y' when 654 then 'N' end 'Qual_Flg'
, prc_655.item

, (Select top 1 item from epacube.marginmgr.pricesheet_retail_changes with (nolock) where price_change_Effective_Date = prc_655.price_change_Effective_Date and parity_pl_parent_structure_fk = prc_655.product_structure_fk and isnull(ind_cost_change_item, 0) = 1 order by price_new desc) 'Parity_Item'
, (Select top 1 effective_date from imports.urm_level_gross_audit lga with (nolock) where item = prc_655.item and zone_entity_structure_fk = prc_655.zone_entity_structure_fk and effective_date < @Eff_Date order by effective_date desc) 'Last RIMS'
, (select top 1 cast(rescind_timestamp as date) 'rescinded' from epacube.marginmgr.pricesheet_basis_values where product_structure_fk = prc_655.product_structure_fk and effective_date = prc_655.cost_change_effective_date) 'Rescinded'
, prc_655.Cost_Change_Reason
, cast(prc_655.cost_change_effective_date as varchar(16)) 'cost_change_effective_date'
, (Select top 1 Cost_Change_Processed_Date from epacube.marginmgr.PRICESHEET_BASIS_VALUES where data_name_fk = 503201 and Product_Structure_FK = prc_655.PRODUCT_STRUCTURE_FK and isnull(Cost_Change_Processed_Date, getdate()) < @Eff_Date order by effective_date desc) 'Last_Cost_Change_Processed_Date'

, prc_655.Use_LG_Price_Points 'Use_PPTs'
, prc_655.price_initial
, prc_655.price_rounded
, prc_655.SRP_Digit
, prc_655.Digit_Adj
, prc_655.price_new 'New Sell Price'
, prc_655.price_cur
, Cast(prc_655.Target_Margin * 100 as Numeric(18, 2)) 'Target_Margin'
, prc_655.STORE_PACK
, prc_655.unit_cost_cur
, prc_655.unit_cost_new
, Cast(prc_655.NEW_GM_PCT as Numeric(18, 2)) 'New_GM_Pct'
, Null 'UOM_Code' 
, prc_655.PRICE_MULTIPLE_CUR
, prc_655.product_structure_fk
, prc_655.PRICESHEET_RETAIL_CHANGES_ID 'Foreign_Key'
, 1 'DRank'
, prc_655.insert_step
, prc_655.Pricing_Steps
from epacube.marginmgr.pricesheet_retail_changes prc_655
where prc_655.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date
and prc_655.ZONE_ENTITY_STRUCTURE_FK = @Z_FK 
and prc_655.ITEM_CHANGE_TYPE_CR_FK = 655
and prc_655.parity_pl_parent_structure_fk is null
) A 
where a.Item in
	(
	Select a.item from (
	Select
	lga.item
	, lga.zone_entity_structure_fk
	, lga.New_prc 'New_Sell_Price'
	from imports.urm_level_gross_audit lga with (nolock) 
	where 1 = 1
	and lga.zone_entity_structure_fk = @Z_FK
	and lga.Effective_Date = @Eff_Date
	and lga.Alias is null
	) a 
	inner join
		(
			Select
			prc_655.item
			, prc_655.ZONE_ENTITY_STRUCTURE_FK
			, prc_655.PRICE_NEW 'New_Sell_Price'
			from epacube.marginmgr.pricesheet_retail_changes prc_655
			where 1 = 1
			and prc_655.ZONE_ENTITY_STRUCTURE_FK = @Z_FK
			and prc_655.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date 
			and prc_655.Alias_id is null
			and prc_655.ITEM_CHANGE_TYPE_CR_FK = 655
		) b on a.item = b.item
				and a.zone_entity_structure_fk = b.ZONE_ENTITY_STRUCTURE_FK
	where cast(isnull(b.New_Sell_Price, 0) as numeric(18, 2)) <> cast(a.New_Sell_Price as numeric(18, 2))
	)
order by a.item, a.source desc

Select 'In RIMS not in Precision' 'Reason', a.* from (
Select
'RIMS' 'Source'
, ZONE_NUM
, Qual_Flg
, item
, isnull(replace(paritem, '0', ''), '') + isnull(paritmtyp, '') 'Parity_Item'

, (select top 1 effective_date from imports.urm_level_gross_audit where item = lga.item and zone_entity_structure_fk = lga.zone_entity_structure_fk and effective_date < @Eff_Date order by effective_date desc) 'Last RIMS'
, (select top 1 cast(rescind_timestamp as date) 'rescinded' from epacube.marginmgr.pricesheet_basis_values where product_structure_fk = lga.product_structure_fk and effective_date = New_Cost_Effective_Date order by effective_date desc) 'Rescinded'
, Null 'Cost_Change_Reason'
, New_Cost_Effective_Date
, (Select top 1 Cost_Change_Processed_Date from epacube.marginmgr.PRICESHEET_BASIS_VALUES where data_name_fk = 503201 and Product_Structure_FK = lga.PRODUCT_STRUCTURE_FK and isnull(Cost_Change_Processed_Date, getdate()) < @Eff_Date order by effective_date desc) 'Last_Cost_Change_Processed_Date'

, Null 'Use_PPTs'
, Null 'price_initial'
, Null 'price_rounded'
, Null 'SRP_Digit'
, Null 'Digit_Adj'
, New_prc 'New Sell Price'
, price 'price_cur'
, TM_PCT
, PACK
, unit_cost_cur
, unit_cost_new
, NEWGMPCT 'NEW_GM_PCT'
, uom_code
, prc_mult
, lga.product_structure_fk
, URM_Level_Gross_Audit_ID 'Foreign_Key'
, dense_rank()over(partition by Alias, zone_entity_structure_fk order by New_prc desc, Case when unit_cost_new = unit_cost_cur then 0 else unit_cost_new end desc, product_structure_fk) 'DRank'
, Cast(null as int) 'insert_step'
, cast(null as varchar(128)) 'Pricing_Steps'
from imports.urm_level_gross_audit lga with (nolock) 
where 1 = 1
and Alias is null
and zone_entity_structure_fk = @Z_FK
and Effective_Date = @Eff_Date

Union

Select 
'Precision' 'Source'
, Zone
, Case prc_655.qualified_status_cr_fk when 653 then 'Y' when 654 then 'N' end 'Qual_Flg'
, prc_655.item

, (Select top 1 item from epacube.marginmgr.pricesheet_retail_changes with (nolock) where price_change_Effective_Date = prc_655.price_change_Effective_Date and parity_pl_parent_structure_fk = prc_655.product_structure_fk and isnull(ind_cost_change_item, 0) = 1 order by price_new desc) 'Parity_Item'
, (Select top 1 effective_date from imports.urm_level_gross_audit lga with (nolock) where item = prc_655.item and zone_entity_structure_fk = prc_655.zone_entity_structure_fk and effective_date < @Eff_Date order by effective_date desc) 'Last RIMS'
, (select top 1 cast(rescind_timestamp as date) 'rescinded' from epacube.marginmgr.pricesheet_basis_values where product_structure_fk = prc_655.product_structure_fk and effective_date = prc_655.cost_change_effective_date) 'Rescinded'
, prc_655.Cost_Change_Reason
, cast(prc_655.cost_change_effective_date as varchar(16)) 'cost_change_effective_date'
, (Select top 1 Cost_Change_Processed_Date from epacube.marginmgr.PRICESHEET_BASIS_VALUES where data_name_fk = 503201 and Product_Structure_FK = prc_655.PRODUCT_STRUCTURE_FK and isnull(Cost_Change_Processed_Date, getdate()) < @Eff_Date order by effective_date desc) 'Last_Cost_Change_Processed_Date'

, prc_655.Use_LG_Price_Points 'Use_PPTs'
, prc_655.price_initial
, prc_655.price_rounded
, prc_655.SRP_Digit
, prc_655.Digit_Adj
, prc_655.price_new 'New Sell Price'
, prc_655.price_cur
, Cast(prc_655.Target_Margin * 100 as Numeric(18, 2)) 'Target_Margin'
, prc_655.STORE_PACK

, prc_655.unit_cost_cur
, prc_655.unit_cost_new
, Cast(prc_655.NEW_GM_PCT as Numeric(18, 2)) 'New_GM_Pct'
, Null 'UOM_Code' 
, prc_655.PRICE_MULTIPLE_CUR
, prc_655.product_structure_fk
, prc_655.PRICESHEET_RETAIL_CHANGES_ID 'Foreign_Key'
, 1 'DRank'
, prc_655.insert_step
, prc_655.Pricing_Steps
from epacube.marginmgr.pricesheet_retail_changes prc_655
where prc_655.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date
and prc_655.ZONE_ENTITY_STRUCTURE_FK = @Z_FK 
and prc_655.ITEM_CHANGE_TYPE_CR_FK = 655
and prc_655.parity_pl_parent_structure_fk is null
) A 
where a.Item in
	(
	Select a.item from (
	Select
	lga.item
	, lga.zone_entity_structure_fk
	, lga.New_prc 'New_Sell_Price'
	from imports.urm_level_gross_audit lga with (nolock) 
	where 1 = 1
	and lga.zone_entity_structure_fk = @Z_FK
	and lga.Effective_Date = @Eff_Date
	and lga.Alias is null
	) a 
	left join
		(
			Select
			prc_655.item
			, prc_655.ZONE_ENTITY_STRUCTURE_FK
			, prc_655.PRICE_NEW 'New_Sell_Price'
			from epacube.marginmgr.pricesheet_retail_changes prc_655
			where 1 = 1
			and prc_655.ZONE_ENTITY_STRUCTURE_FK = @Z_FK
			and prc_655.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date 
			and prc_655.Alias_id is null
			and prc_655.ITEM_CHANGE_TYPE_CR_FK = 655
		) b on a.item = b.item
				and a.zone_entity_structure_fk = b.ZONE_ENTITY_STRUCTURE_FK
	where b.item is null
	)
order by a.item, a.source desc

Select 'In Precision not in RIMS' 'Reason', a.* from (
--Select
--'RIMS' 'Source'
--, ZONE_NUM
--, Qual_Flg
--, item
--, Null 'Parity_Item'
--, Null 'Use_PPTs'
--, Null 'price_initial'
--, Null 'price_rounded'
--, Null 'SRP_Digit'
--, Null 'Digit_Adj'
--, New_prc 'New Sell Price'
--, price 'price_cur'
--, TM_PCT
--, PACK
--, Cast(New_Cost_Effective_Date as varchar(16)) 'New_Cost_Effective_Date'
--, Null 'Cost_Change_Reason'
--, Null 'Last_Cost_Change_Processed_Date'
--, unit_cost_cur
--, unit_cost_new
--, NEWGMPCT 'NEW_GM_PCT'
--, uom_code
--, prc_mult
--, paritem
--, paritmtyp
--, rescinded
--, lga.product_structure_fk
--, URM_Level_Gross_Audit_ID 'Foreign_Key'
--, dense_rank()over(partition by Alias_id, zone_entity_structure_fk order by New_prc desc, Case when unit_cost_new = unit_cost_cur then 0 else unit_cost_new end desc, product_structure_fk) 'DRank'
--, Cast(null as int) 'insert_step'
--, cast(null as varchar(128)) 'Pricing_Steps'
--from imports.urm_level_gross_audit lga with (nolock) 
--where 1 = 1
--and Alias_id is null
--and zone_entity_structure_fk = @Z_FK
--and Effective_Date = @Eff_Date

--Union

Select 
Null 'Source'
, Zone
, Case prc_655.qualified_status_cr_fk when 653 then 'Y' when 654 then 'N' end 'Qual_Flg'
, prc_655.item

, (Select top 1 item from epacube.marginmgr.pricesheet_retail_changes with (nolock) where price_change_Effective_Date = prc_655.price_change_Effective_Date and parity_pl_parent_structure_fk = prc_655.product_structure_fk and isnull(ind_cost_change_item, 0) = 1 order by price_new desc) 'Parity_Item'
, (Select top 1 effective_date from imports.urm_level_gross_audit lga with (nolock) where item = prc_655.item and zone_entity_structure_fk = prc_655.zone_entity_structure_fk and effective_date < @Eff_Date order by effective_date desc) 'Last RIMS'
, (select top 1 cast(rescind_timestamp as date) 'rescinded' from epacube.marginmgr.pricesheet_basis_values where product_structure_fk = prc_655.product_structure_fk and effective_date = prc_655.cost_change_effective_date) 'Rescinded'
, prc_655.Cost_Change_Reason
, cast(prc_655.cost_change_effective_date as varchar(16)) 'cost_change_effective_date'
, (Select top 1 Cost_Change_Processed_Date from epacube.marginmgr.PRICESHEET_BASIS_VALUES where data_name_fk = 503201 and Product_Structure_FK = prc_655.PRODUCT_STRUCTURE_FK and isnull(Cost_Change_Processed_Date, getdate()) < @Eff_Date order by effective_date desc) 'Last_Cost_Change_Processed_Date'

, prc_655.Use_LG_Price_Points 'Use_PPTs'
, prc_655.price_initial
, prc_655.price_rounded
, prc_655.SRP_Digit
, prc_655.Digit_Adj
, prc_655.price_new 'New Sell Price'
, prc_655.price_cur
, Cast(prc_655.Target_Margin * 100 as Numeric(18, 2)) 'Target_Margin'
, prc_655.STORE_PACK
, prc_655.unit_cost_cur
, prc_655.unit_cost_new
, Cast(prc_655.NEW_GM_PCT as Numeric(18, 2)) 'New_GM_Pct'
, Null 'UOM_Code' 
, prc_655.PRICE_MULTIPLE_CUR
, prc_655.product_structure_fk
, prc_655.PRICESHEET_RETAIL_CHANGES_ID 'Foreign_Key'
, 1 'DRank'
, prc_655.insert_step
, prc_655.Pricing_Steps
from epacube.marginmgr.pricesheet_retail_changes prc_655
where prc_655.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date
and prc_655.ZONE_ENTITY_STRUCTURE_FK = @Z_FK 
and prc_655.ITEM_CHANGE_TYPE_CR_FK = 655
and prc_655.parity_pl_parent_structure_fk is null
) A 
where a.item in
	(
		Select a1.item from (
			Select
			prc_655.item
			, prc_655.ZONE_ENTITY_STRUCTURE_FK
			, prc_655.PRICE_NEW 'New_Sell_Price'
			from epacube.marginmgr.pricesheet_retail_changes prc_655
			where 1 = 1
			and prc_655.ZONE_ENTITY_STRUCTURE_FK = @Z_FK
			and prc_655.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date 
			and prc_655.Alias_id is null
			and prc_655.ITEM_CHANGE_TYPE_CR_FK = 655
	) a1 
	left join
		(
			Select
			lga.item
			, lga.zone_entity_structure_fk
			, lga.New_prc 'New_Sell_Price'
			from imports.urm_level_gross_audit lga with (nolock) 
			where 1 = 1
			and lga.zone_entity_structure_fk = @Z_FK
			and lga.Effective_Date = @Eff_Date
			--and lga.Alias_id is null
		) b1 on a1.item = b1.ITEM
				--and a1.zone_entity_structure_fk = b1.ZONE_ENTITY_STRUCTURE_FK
	where b1.ITEM is null
	)
and a.item not in
	(
		Select PL_Item from 	(
			Select
				prc_655.item 'PL_Item'
				, prc_655a.item 'National_Item'
				, prc_655a.ZONE_ENTITY_STRUCTURE_FK
				, prc_655a.PRICE_NEW 'New_Sell_Price'
				from epacube.marginmgr.pricesheet_retail_changes prc_655
				left join epacube.marginmgr.pricesheet_retail_changes prc_655a
					on prc_655a.zone_entity_structure_fk = prc_655.zone_entity_structure_fk
					and prc_655a.price_change_effective_date = prc_655.price_change_effective_date
					and prc_655a.item_change_type_cr_fk = prc_655.item_change_type_cr_fk
					and prc_655a.parity_pl_parent_structure_fk = prc_655.product_structure_fk
				where 1 = 1
				and prc_655.ZONE_ENTITY_STRUCTURE_FK = @Z_FK
				and prc_655.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date 
				and prc_655.Alias_id is null
				and prc_655a.Alias_id is null
				and prc_655.ITEM_CHANGE_TYPE_CR_FK = 655
		) a2 
			inner join
			(
				Select
				lga.item
				, lga.zone_entity_structure_fk
				, lga.New_prc 'New_Sell_Price'
				from imports.urm_level_gross_audit lga with (nolock) 
				where 1 = 1
				and lga.zone_entity_structure_fk = @Z_FK
				and lga.Effective_Date = @Eff_Date
				--and lga.Alias_id is null
			) b2 on (a2.PL_Item = b2.ITEM or a2.National_Item = b2.item)
					and a2.zone_entity_structure_fk = b2.ZONE_ENTITY_STRUCTURE_FK
	)
and a.item not in
(
		Select lga.item from imports.urm_level_gross_audit lga with (nolock) 
			where 1 = 1
			and lga.zone_entity_structure_fk = @Z_FK
			and lga.Effective_Date = @Eff_Date
)
and a.item not in
(
		Select isnull(lga.paritem, 0) + isnull(paritmtyp, 'w') from imports.urm_level_gross_audit lga with (nolock) 
			where 1 = 1
			and lga.zone_entity_structure_fk = @Z_FK
			and lga.Effective_Date = @Eff_Date
)

order by a.item, a.source desc
	


		--Select PL_Item from 	(
		--	Select
		--		prc_655.item 'PL_Item'
		--		, prc_655a.item 'National_Item'
		--		, prc_655a.ZONE_ENTITY_STRUCTURE_FK
		--		, prc_655a.PRICE_NEW 'New_Sell_Price'
		--		from epacube.marginmgr.pricesheet_retail_changes prc_655
		--		left join epacube.marginmgr.pricesheet_retail_changes prc_655a
		--			on prc_655a.zone_entity_structure_fk = prc_655.zone_entity_structure_fk
		--			and prc_655a.price_change_effective_date = prc_655.price_change_effective_date
		--			and prc_655a.item_change_type_cr_fk = prc_655.item_change_type_cr_fk
		--			and prc_655a.parity_pl_parent_structure_fk = prc_655.product_structure_fk
		--		where 1 = 1
		--		and prc_655.ZONE_ENTITY_STRUCTURE_FK = @Z_FK
		--		and prc_655.PRICE_CHANGE_EFFECTIVE_DATE = @Eff_Date 
		--		and prc_655.alias is null
		--		and prc_655a.alias is null
		--		and prc_655.ITEM_CHANGE_TYPE_CR_FK = 655
		--) a2 
		--	inner join
		--	(
		--		Select
		--		lga.item
		--		, lga.zone_entity_structure_fk
		--		, lga.New_prc 'New_Sell_Price'
		--		from imports.urm_level_gross_audit lga with (nolock) 
		--		where 1 = 1
		--		and lga.zone_entity_structure_fk = @Z_FK
		--		and lga.Effective_Date = @Eff_Date
		--		--and lga.alias is null
		--	) b2 on (a2.PL_Item = b1.ITEM or a2.National_Item = b2.item)
		--			and a2.zone_entity_structure_fk = b2.ZONE_ENTITY_STRUCTURE_FK
