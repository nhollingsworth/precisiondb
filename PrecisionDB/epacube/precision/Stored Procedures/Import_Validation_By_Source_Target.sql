﻿CREATE PROCEDURE [precision].[Import_Validation_By_Source_Target]
as
Select * from (
Select A.[Procedure_name], A.source_table, A.target_table, A.Type_Of_Data, Cast(getdate() as date) 'Today''s Date'
, Case when CUR.DAILY_IMPORT_LOG_ID is null then 'Did Not Run' else null end 'Today''s Status', Cur.RECORDS_LOADED, cur.Create_timestamp 
		 from (
					Select [Procedure_name], source_table, target_table, Type_Of_Data, Create_Timestamp from (
					Select [Procedure_name], Type_Of_Data, source_table, target_table, CREATE_TIMESTAMP
					, dense_rank()over(partition by [Procedure_name], Type_Of_Data order by cast(create_timestamp as date), daily_import_log_id) DRank
					from [PRECISION].[DAILY_IMPORT_LOG]
					where cast(CREATE_TIMESTAMP as date) > '2018-09-17'
					) A where DRank = 1 
				) A
		left join (
					Select *
					from [PRECISION].[DAILY_IMPORT_LOG]
					where cast(create_timestamp as date) = cast(getdate() as date)
				) Cur on A.Procedure_name = Cur.Procedure_Name and A.TYPE_OF_DATA = Cur.TYPE_OF_DATA
			) z order by isnull([today's Status], '') desc, [Procedure_name], CREATE_TIMESTAMP

select 'urm_level_gross_zone' 'Table', 'Effective_date' 'Data_Type', max(epacube.[URM_DATE_CONVERSION](eff_date)) 'Actual_Value', cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date) 'Expected_Date', max(import_timestamp) 'import_timestamp' from imports.urm_level_gross_zone with (nolock)
Union
select 'urm_level_gross_audit' 'Table', 'Effective_date' 'Data_Type', max(effective_date) 'Actual_Value', cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date) 'Expected_Date', max(import_timestamp) 'import_timestamp' from imports.urm_level_gross_audit with (nolock)
Union
select 'pricesheet_basis_values' 'Table', 'cost_change_processed_date' 'Data_Type', max(cost_change_processed_date) 'Actual_Value', cast(GETDATE() -1 - DATEPART(DW, GETDATE()) as date) 'Expected_Date', max(import_timestamp) 'import_timestamp' from epacube.marginmgr.PRICESHEET_BASIS_VALUES where cost_change_processed_date is not null

