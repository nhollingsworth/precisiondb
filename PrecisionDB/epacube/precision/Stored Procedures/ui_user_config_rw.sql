﻿-- ==============================================
-- Author:		Gary Stone
-- Create date: 4/3/2019
-- Description:	Provide user configurations to UI
-- ==============================================
CREATE PROCEDURE [precision].[ui_user_config_rw]
@username varchar(64) = null
, @ui_name varchar(128) = null
, @field_name varchar(64) = null
, @value varchar(64) = null
, @Label varchar(64) = null
, @checkbox int = 0
, @action int = 0

--@Action; 0 = Read, 1 = Write
AS
	SET NOCOUNT ON;

	--Declare @username varchar(64) = 'gstone@epacube_com'
	--Declare @ui_name varchar(128) = 'pricing-zone-settings'
	--Declare @field_name varchar(64) = null
	--Declare @value varchar(64) = null
	--Declare @Label varchar(64) = null
	--Declare @checkbox int = 0
	--Declare @action int = 0

Set @username = replace(@username, '.', '_')

If @action = 0
	Begin
		Select 
		uuc.ui_name
		, uuc.field_name
		, uuc.[value]
		, uuc.label
		, uuc.exclusion
		, case when uuc.date_value is not null or ds.data_set_id is not null then cast(getdate() as date) else Null end 'date_value'
		--, case when uf.UI_Filter_ID is not null then cast(getdate() as date) else Null end 'date_value'
		, case when uuc.date_value is not null or ds.data_set_id is not null then 1 else 0 end 'ind_date'
		, 1 'record_status_cr_fk'
		from precision.ui_user_config uuc with (nolock)
		left join epacube.ui_filter uf with (nolock) on uuc.field_name = cast(uf.data_name_fk as varchar(64))
		left join epacube.data_name dn with (nolock) on uuc.field_name = cast(dn.data_name_id as varchar(16))
		left join epacube.data_set ds with (nolock) on dn.DATA_SET_FK = ds.DATA_SET_ID and ds.DATA_TYPE_CR_FK = 312 
		where 1 = 1
		and uuc.username = @username
		and uuc.ui_name = @ui_name
		and uuc.field_name <> 'overwrite'
	End
	Else
	Begin
		If @checkbox = 1 and @value = 0
			delete from  precision.ui_user_config where 1 = 1 and username = @username and ui_name = @ui_name and field_name = @field_name
		Else
		If (select count(*) from  precision.ui_user_config with (nolock) where 1 = 1 and username = @username and ui_name = @ui_name and field_name = @field_name) <> 0
			Update uucr
			Set [value] = @value
			, Label = @Label
			, update_timestamp = getdate()
			from precision.ui_user_config uucr where 1 = 1 and username = @username and ui_name = @ui_name and field_name = @field_name
		Else
			Insert into  precision.ui_user_config
			(username, ui_name, field_name, [value], Label, create_timestamp)
			values(@username, @ui_name, @field_name, @value, @Label, getdate())
	End
