﻿CREATE TABLE [precision].[help_messages] (
    [class]            VARCHAR (64) NOT NULL,
    [value]            VARCHAR (16) NOT NULL,
    [criteria]         VARCHAR (16) NULL,
    [msg]              VARCHAR (64) NULL,
    [help_messages_id] INT          IDENTITY (1, 1) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_01]
    ON [precision].[help_messages]([class] ASC, [value] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

