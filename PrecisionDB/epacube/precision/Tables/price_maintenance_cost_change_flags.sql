﻿CREATE TABLE [precision].[price_maintenance_cost_change_flags] (
    [pricesheet_basis_values_id]             BIGINT          NOT NULL,
    [effective_date]                         DATE            NULL,
    [product_structure_fk]                   BIGINT          NULL,
    [zone_entity_structure_fk]               BIGINT          NULL,
    [data_name_fk]                           BIGINT          NOT NULL,
    [value]                                  NUMERIC (18, 6) NULL,
    [item]                                   VARCHAR (128)   NULL,
    [cost_change_processed_date]             DATE            NULL,
    [pre_notify_days]                        INT             NULL,
    [price_maintenance_cost_change_flags_id] BIGINT          IDENTITY (1, 1) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_01]
    ON [precision].[price_maintenance_cost_change_flags]([product_structure_fk] ASC, [cost_change_processed_date] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_pnd]
    ON [precision].[price_maintenance_cost_change_flags]([pricesheet_basis_values_id] ASC, [price_maintenance_cost_change_flags_id] ASC, [pre_notify_days] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_pbv_pnd]
    ON [precision].[price_maintenance_cost_change_flags]([price_maintenance_cost_change_flags_id] ASC, [pricesheet_basis_values_id] ASC, [pre_notify_days] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

