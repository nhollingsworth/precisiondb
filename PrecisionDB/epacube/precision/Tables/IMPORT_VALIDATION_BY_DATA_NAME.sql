﻿CREATE TABLE [precision].[IMPORT_VALIDATION_BY_DATA_NAME] (
    [BATCH]                             INT          NULL,
    [Table_Name]                        VARCHAR (41) NOT NULL,
    [Query_Date]                        DATETIME     NOT NULL,
    [data_name_fk]                      BIGINT       NULL,
    [DNL]                               VARCHAR (64) NULL,
    [Import_Timestamp]                  DATETIME     NULL,
    [Records]                           INT          NULL,
    [IMPORT_VALIDATION_BY_DATA_NAME_ID] BIGINT       NOT NULL
);

