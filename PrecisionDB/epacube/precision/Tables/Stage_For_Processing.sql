﻿CREATE TABLE [precision].[STAGE_FOR_PROCESSING] (
    [Entity_Data_Name_FK]         BIGINT       NULL,
    [Processing_Type]             VARCHAR (64) NOT NULL,
    [product_structure_fk]        BIGINT       NOT NULL,
    [entity_structure_fk]         BIGINT       NOT NULL,
    [Data_Name_FK]                BIGINT       NULL,
    [value]                       VARCHAR (64) NOT NULL,
    [username]                    VARCHAR (64) NOT NULL,
    [Create_timestamp]            DATETIME     CONSTRAINT [DF_Stage_For_Processing_Create_timestamp] DEFAULT (getdate()) NULL,
    [Processed_timestamp]         DATETIME     NULL,
    [STAGE_FOR_PROCESSING_ID]     BIGINT       IDENTITY (1, 1) NOT NULL,
    [price_change_effective_date] DATE         NULL,
    [Prod_Segment_Data_Name_FK]   BIGINT       NULL,
    [Prod_Segment_FK]             BIGINT       NULL,
    [Prop_To_Data_Name_FK]        BIGINT       NULL,
    [Immediate]                   INT          NULL,
    [OverWrite]                   INT          NULL,
    [tm_change_effective_date]    DATE         NULL,
    CONSTRAINT [PK_Stage_For_Processing] PRIMARY KEY CLUSTERED ([STAGE_FOR_PROCESSING_ID] ASC)
);




GO
CREATE NONCLUSTERED INDEX [idx_01]
    ON [precision].[Stage_For_Processing]([product_structure_fk] ASC, [entity_structure_fk] ASC, [Entity_Data_Name_FK] ASC, [Processing_Type] ASC, [Data_Name_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_pt_ptme_incl_edn_ps_es_dn]
    ON [precision].[STAGE_FOR_PROCESSING]([Processing_Type] ASC, [Processed_timestamp] ASC)
    INCLUDE([Entity_Data_Name_FK], [product_structure_fk], [entity_structure_fk], [Data_Name_FK]) WITH (FILLFACTOR = 80);



