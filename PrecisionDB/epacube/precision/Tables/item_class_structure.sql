﻿CREATE TABLE [precision].[item_class_structure] (
    [Prod_Segment_FK3] BIGINT        NOT NULL,
    [DV3_Desc]         VARCHAR (256) NULL,
    [data_name_fk3]    INT           NULL,
    [IC_ID3]           INT           NULL,
    [Prod_Segment_FK4] BIGINT        NOT NULL,
    [DV4_Desc]         VARCHAR (256) NULL,
    [data_name_fk4]    INT           NULL,
    [IC_ID4]           INT           NULL,
    [Prod_Segment_FK5] BIGINT        NOT NULL,
    [DV5_Desc]         VARCHAR (256) NULL,
    [data_name_fk5]    INT           NULL,
    [IC_ID5]           INT           NULL,
    [Zone_Segment_FK]  BIGINT        NOT NULL,
    [Zone]             VARCHAR (128) NULL,
    [Zone_Name]        VARCHAR (128) NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_03]
    ON [precision].[item_class_structure]([Prod_Segment_FK3] ASC, [data_name_fk3] ASC, [Zone_Segment_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_04]
    ON [precision].[item_class_structure]([Prod_Segment_FK4] ASC, [data_name_fk4] ASC, [Zone_Segment_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_05]
    ON [precision].[item_class_structure]([Prod_Segment_FK5] ASC, [data_name_fk5] ASC, [Zone_Segment_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

