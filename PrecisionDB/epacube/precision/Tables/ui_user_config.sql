﻿CREATE TABLE [precision].[UI_USER_CONFIG] (
    [username]         VARCHAR (64)  NULL,
    [ui_name]          VARCHAR (128) NULL,
    [field_name]       VARCHAR (64)  NULL,
    [value]            VARCHAR (MAX) NULL,
    [label]            VARCHAR (64)  NULL,
    [Exclusion]        VARCHAR (8)   NULL,
    [date_value]       DATE          NULL,
    [ui_config]        VARCHAR (MAX) NULL,
    [create_timestamp] DATETIME      NULL,
    [update_timestamp] DATETIME      NULL
);



