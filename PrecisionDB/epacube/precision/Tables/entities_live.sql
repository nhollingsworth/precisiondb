﻿CREATE TABLE [precision].[ENTITIES_LIVE] (
    [entity_value]               VARCHAR (64) NOT NULL,
    [entity_structure_fk]        BIGINT       NOT NULL,
    [entity_data_name_fk]        BIGINT       NOT NULL,
    [data_name_fk]               BIGINT       NOT NULL,
    [entities_live_id]           BIGINT       IDENTITY (1, 1) NOT NULL,
    [import_record_status_cr_fk] INT          NULL,
    [import_go_live_date]        DATE         NULL,
    [export_record_status_cr_fk] INT          NULL,
    [export_go_live_date]        DATE         NULL
);




GO
CREATE NONCLUSTERED INDEX [idx_01]
    ON [precision].[entities_live]([entity_structure_fk] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

