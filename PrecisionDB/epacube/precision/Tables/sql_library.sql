﻿CREATE TABLE [precision].[sql_library] (
    [sql_library_id] INT           IDENTITY (1, 1) NOT NULL,
    [sql_name]       VARCHAR (64)  NOT NULL,
    [sql_code]       VARCHAR (MAX) NOT NULL
);

