﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [precision].[getretailpricezone]
(
@Zone_Entity_Structure_FK bigint
, @Product_Structure_FK bigint
, @Price_Initial Numeric(18, 2)
, @DaysOut Int = 7
, @WK int = 0
)
RETURNS Numeric(18, 2)
AS
BEGIN

	--Declare @Zone_Entity_Structure_FK bigint = 113232
	--Declare @Product_Structure_FK bigint = 301135
	--Declare @Price_Initial Numeric(18, 2) = 2.86
	--Declare @DaysOut Int = 7
	--Declare @WK int

	Set @WK = isnull(@WK, 0)

	Declare @SRP_Digit Int
	Declare @Digit_DN_FK bigint
	Declare @Digit_Adj Numeric(18, 2)

	Declare @Price_Calc Numeric(18, 2)
--	Declare @Price_Type_Entity_Data_Value_FK bigint

--	Set @Price_Type_Entity_Data_Value_FK = (select entity_data_value_id from epacube.entity_data_value where data_name_fk = 502031 and value = 1 and DATA_VALUE_FK = 
--		(select data_value_id from epacube.data_value where data_name_fk = 502041 and value = '10'))

	Set @Price_Calc = @Price_Initial
	
	Set @SRP_Digit = right(@Price_Calc, 1)

	Set @Digit_DN_FK = Case @SRP_Digit	when 0 then 144897
										when 1 then 144898
										when 2 then 144899
										when 3 then 144901
										when 4 then 144902
										when 5 then 144903
										when 6 then 144904
										when 7 then 144905
										when 8 then 144906
										when 9 then 144907 end
--Add Amount to Adjust by Zone Item Class

	Set @Digit_Adj = isnull(
						(Select top 1 attribute_number from (
							Select ss.prod_segment_fk, ss.zone_segment_fk, precedence, attribute_number
							from epacube.epacube.segments_settings ss
							inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.PROD_SEGMENT_FK and ss.PROD_SEGMENT_DATA_NAME_FK = sp.DATA_NAME_FK
							where 1 = 1
								and ss.data_name_fk = @Digit_DN_FK 
								and ss.zone_segment_fk = @Zone_Entity_Structure_FK 
								and ss.prod_segment_fk is not null
								and sp.product_structure_fk = @Product_Structure_FK
							Union
							Select Null 'Product_structure_fk', ss.zone_segment_fk, precedence, attribute_number
							from epacube.epacube.segments_settings ss
							where 1 = 1 
								and prod_segment_fk is null
								and ZONE_SEGMENT_FK = @Zone_Entity_Structure_FK
								and data_name_fk = @Digit_DN_FK
							) a order by isnull(precedence, 99)
						), 0)
/*
	Set @Digit_Adj = isnull((Select ss1.DigitAdj
	from epacube.epacube.segments_product SP with (nolock) 
	inner join
		(
			Select * from (
			select distinct ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL, ss.precedence
			, ss.ZONE_SEGMENT_FK, ss.prod_segment_fk, ss.attribute_Number 'DigitAdj', ss.effective_date, SEGMENTS_SETTINGS_ID
			--, Dense_Rank()over(partition by ss.data_name_fk, ss.ZONE_SEGMENT_FK, ss.prod_segment_fk order by ss.precedence, ss.effective_date desc, ss.update_timestamp desc) 'DRank'
			, Dense_Rank()over(partition by ss.data_name_fk, ss.ZONE_SEGMENT_FK order by ss.precedence, ss.effective_date desc, ss.update_timestamp desc) 'DRank'
			from epacube.segments_settings ss with (nolock)
			inner join epacube.data_name dn with (nolock) on ss.data_name_fk = dn.data_name_id
			where 1 = 1
			and ss.ZONE_ENTITY_STRUCTURE_FK = @Zone_Entity_Structure_FK
			and ss.cust_entity_structure_fk is null
			and ss.prod_segment_fk is not null
			and ss.DATA_NAME_FK = @Digit_DN_FK
			and dn.parent_data_name_fk = 144893
			--and ss.effective_date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + @DaysOut, getdate() - (@WK * 7)) as date)
			and ss.RECORD_STATUS_CR_FK = 1
			) a where DRank = 1
		) ss1  on 
			1 = 1
			and	SP.PROD_SEGMENT_FK = ss1.PROD_SEGMENT_FK
			and sp.product_structure_fk = @Product_Structure_FK
		), 0)

Select @Digit_DN_FK, @Digit_Adj

	If @Digit_Adj = 0
	Set @Digit_Adj = 
		isnull((Select DigitAdj
								From
									(
										select * from (
										select distinct ss.data_name_fk, epacube.getdatanamelabel(ss.data_name_fk) DNL
										, ss.ZONE_SEGMENT_FK, ss.attribute_Number 'DigitAdj', ss.effective_date, SEGMENTS_SETTINGS_ID, isnull(ss.precedence, 99) 'precedence', ss.PROD_SEGMENT_FK
										, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk order by isnull(ss.precedence, 99), ss.effective_date desc, ss.segments_settings_id desc) DRank

										from epacube.epacube.segments_settings ss with (nolock) 
										inner join epacube.epacube.data_name dn with (nolock) on ss.data_name_fk = dn.data_name_id
										inner join epacube.epacube.entity_identification eiz with (nolock) on ss.zone_segment_fk = eiz.entity_structure_fk and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.DATA_NAME_FK = 151110
										left join epacube.epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
										left join epacube.epacube.product_identification pi with (nolock) on sp.product_structure_fk = pi.product_structure_fk and pi.data_name_fk = 110100
										where 1 = 1
										and ss.RECORD_STATUS_CR_FK = 1
										and ss.ZONE_SEGMENT_FK = @Zone_Entity_Structure_FK
										and ss.cust_entity_structure_fk is null
										and (sp.product_structure_fk = @Product_Structure_FK or ss.PROD_SEGMENT_FK is null)
										and dn.parent_data_name_fk = 144893
										and ss.DATA_NAME_FK = @Digit_DN_FK
										and ss.effective_date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + @DaysOut, getdate() - (@WK * 7)) as date)
										and ss.RECORD_STATUS_CR_FK = 1
										) a where Drank = 1
										) B
										), 0)

*/
	--Select @Digit_Adj
	
	Set @Price_Calc = @Price_Calc + @Digit_Adj

	Set @Price_Calc = isnull((
							select DISTINCT 
							sz_144891.attribute_number 'Rounded_SRP'
							from 
								(
									select * from (
									Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.zone_entity_structure_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
									, ss.lower_limit, ss.upper_limit, ss.Effective_Date, ss.UPDATE_TIMESTAMP
									, dense_rank()over(partition by ss.data_name_fk, ss.zone_entity_structure_fk, sp.product_structure_fk, ss.lower_limit order by ss.precedence, ss.effective_date desc, ss.update_timestamp desc) DRank
									from epacube.segments_settings ss with (nolock) 
									inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
									left join epacube.data_value dv with (nolock) on sp.PROD_SEGMENT_FK = dv.data_value_id
									left join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
									where ss.data_name_fk = 144891
									and ss.CUST_ENTITY_STRUCTURE_FK is null
									and ss.RECORD_STATUS_CR_FK = 1
									and @Price_Calc between ss.lower_limit and ss.upper_limit
									and ss.Effective_Date <= Cast(dateadd(day, 6 - DATEPART(DW, GETDATE()) + @daysout, getdate() - (@WK * 7)) as date)
									and sp.PRODUCT_STRUCTURE_FK = @Product_Structure_FK
									and ss.ZONE_ENTITY_STRUCTURE_FK = @ZONE_ENTITY_STRUCTURE_FK
									--and ss.price_type_entity_data_value_fk = @Price_Type_Entity_Data_Value_FK
									) a where Drank = 1
								) sz_144891
							), @Price_Calc)

	--Select @Price_Initial, @Price_Calc


	RETURN @Price_Calc

END
