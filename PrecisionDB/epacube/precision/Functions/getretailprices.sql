﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [precision].[getretailprices]
(
@Zone_Entity_Structure_FK bigint
, @Product_Structure_FK bigint
, @Price_Initial Numeric(18, 2)
, @effective_date date
--, @DaysOut Int = 7
--, @WK int = 0
)
RETURNS Numeric(18, 2)
AS
BEGIN

	--Declare @Zone_Entity_Structure_FK bigint = 113232
	--Declare @Product_Structure_FK bigint = 301135
	--Declare @Price_Initial Numeric(18, 2) = 2.86
	--Declare @effective_date date

	--set @effective_date = cast(getdate() as date)

	Declare @SRP_Digit Int
	Declare @Digit_DN_FK bigint
	Declare @Digit_Adj Numeric(18, 2)

	Declare @Price_Calc Numeric(18, 2)

	Set @Price_Calc = @Price_Initial
	
	Set @Price_Calc = isnull((
							select DISTINCT 
							sz_144891.attribute_number 'Rounded_SRP'
							from 
								(
									select * from (
									Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.zone_entity_structure_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
									, ss.lower_limit, ss.upper_limit, ss.Effective_Date, ss.UPDATE_TIMESTAMP
									, dense_rank()over(partition by ss.data_name_fk, ss.zone_entity_structure_fk, sp.product_structure_fk, ss.lower_limit order by ss.precedence, ss.effective_date desc, ss.update_timestamp desc) DRank
									from epacube.segments_settings ss with (nolock) 
									inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
									left join epacube.data_value dv with (nolock) on sp.PROD_SEGMENT_FK = dv.data_value_id
									left join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
									where ss.data_name_fk = 144891
									and ss.CUST_ENTITY_STRUCTURE_FK is null
									and ss.RECORD_STATUS_CR_FK = 1
									and @Price_Calc between ss.lower_limit and ss.upper_limit
									and ss.Effective_Date <= @effective_date
									and (ss.reinstate_inheritance_date > isnull(ss.Effective_Date, dateadd(d, -1, @effective_date)) or ss.reinstate_inheritance_date is null)
									and sp.PRODUCT_STRUCTURE_FK = @Product_Structure_FK
									and ss.ZONE_ENTITY_STRUCTURE_FK = @ZONE_ENTITY_STRUCTURE_FK
									) a where Drank = 1
								) sz_144891
							), @Price_Calc)

	Set @SRP_Digit = right(@Price_Calc, 1)

	Set @Digit_DN_FK = Case @SRP_Digit	when 0 then 144897
										when 1 then 144898
										when 2 then 144899
										when 3 then 144901
										when 4 then 144902
										when 5 then 144903
										when 6 then 144904
										when 7 then 144905
										when 8 then 144906
										when 9 then 144907 end
--Add Amount to Adjust by Zone Item Class

	Set @Digit_Adj = isnull(
						(Select top 1 attribute_number from (
							Select ss.prod_segment_fk, ss.zone_segment_fk, precedence, attribute_number, ss.effective_date
							from epacube.segments_settings ss
							inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.PROD_SEGMENT_FK and ss.PROD_SEGMENT_DATA_NAME_FK = sp.DATA_NAME_FK
							where 1 = 1
								and ss.data_name_fk = @Digit_DN_FK 
								and ss.zone_segment_fk = @Zone_Entity_Structure_FK 
								and ss.prod_segment_fk is not null
								and sp.product_structure_fk = @Product_Structure_FK
								and ss.Effective_Date <= @effective_date
								and (ss.reinstate_inheritance_date > isnull(ss.Effective_Date, dateadd(d, -1, @effective_date)) or ss.reinstate_inheritance_date is null)
							Union
							Select Null 'Product_structure_fk', ss.zone_segment_fk, precedence, attribute_number, ss.effective_date
							from epacube.segments_settings ss
							where 1 = 1 
								and prod_segment_fk is null
								and ZONE_SEGMENT_FK = @Zone_Entity_Structure_FK
								and ss.Effective_Date <= @effective_date
								and data_name_fk = @Digit_DN_FK
							) a order by isnull(precedence, 99), effective_date desc
						), 0)
	
	Set @Price_Calc = @Price_Calc + @Digit_Adj

	--Set @Price_Calc = isnull((
	--						select DISTINCT 
	--						sz_144891.attribute_number 'Rounded_SRP'
	--						from 
	--							(
	--								select * from (
	--								Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.zone_entity_structure_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
	--								, ss.lower_limit, ss.upper_limit, ss.Effective_Date, ss.UPDATE_TIMESTAMP
	--								, dense_rank()over(partition by ss.data_name_fk, ss.zone_entity_structure_fk, sp.product_structure_fk, ss.lower_limit order by ss.precedence, ss.effective_date desc, ss.update_timestamp desc) DRank
	--								from epacube.segments_settings ss with (nolock) 
	--								inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
	--								left join epacube.data_value dv with (nolock) on sp.PROD_SEGMENT_FK = dv.data_value_id
	--								left join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
	--								where ss.data_name_fk = 144891
	--								and ss.CUST_ENTITY_STRUCTURE_FK is null
	--								and ss.RECORD_STATUS_CR_FK = 1
	--								and @Price_Calc between ss.lower_limit and ss.upper_limit
	--								and ss.Effective_Date <= @effective_date
	--								and (ss.reinstate_inheritance_date > isnull(ss.Effective_Date, dateadd(d, -1, @effective_date)) or ss.reinstate_inheritance_date is null)
	--								and sp.PRODUCT_STRUCTURE_FK = @Product_Structure_FK
	--								and ss.ZONE_ENTITY_STRUCTURE_FK = @ZONE_ENTITY_STRUCTURE_FK
	--								) a where Drank = 1
	--							) sz_144891
	--						), @Price_Calc)

	RETURN @Price_Calc

END
