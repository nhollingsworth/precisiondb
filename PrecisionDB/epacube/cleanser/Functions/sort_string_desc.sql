﻿

-- =============================================
-- Author:		Yuri Nogin
-- Create date: 9/30/2006
-- Description:	Sorts all tokens in text in desc order
-- =============================================

CREATE FUNCTION [cleanser].[sort_string_desc] (
      @text               varchar(8000)
   )
 RETURNS varchar(8000)
AS
    BEGIN
			--process two biggest (length) tokens in step 1 excluding the numeric ones. sort tokens by length in desc order 
			DECLARE cur_tokens cursor local for SELECT * FROM utilities.SplitString (@text,' ')
												ORDER BY ListItem desc;
         
            		
            -- Declare all local variables here
            DECLARE @sToken           varchar(8000),
                    @l_match_string   varchar(8000);

            
		    --initialize the variables
		    select @l_match_string = '';
		    
            --open the cursor 
			OPEN cur_tokens;
			FETCH NEXT FROM cur_tokens Into @sToken;
            

            WHILE @@FETCH_STATUS = 0
                  BEGIN -- loop through all tokens
						                   
                          
						 set @l_match_string =  @l_match_string + @sToken + ' ';
                       --get next token here        
						  FETCH NEXT FROM cur_tokens Into @sToken;
                  END --end of the loop to go through all tokens      
         
      --close the cursor 
  	  CLOSE cur_tokens;
	  DEALLOCATE cur_tokens;  
     
   

            

      RETURN Rtrim(@l_match_string)
   END











