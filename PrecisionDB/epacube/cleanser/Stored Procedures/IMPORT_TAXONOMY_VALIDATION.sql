﻿



-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: For DUPLICATE DATA CHECK
--			
--			
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        05/06/2014   Initial SQL Version
-- CV        02/10/2015   Added data name to product id look up to tighten up code


CREATE PROCEDURE [cleanser].[IMPORT_TAXONOMY_VALIDATION] 
       @IN_JOB_FK                BIGINT,
	   @v_import_package_fk      bigint 
      
AS
BEGIN



DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint

DECLARE @v_filename_count INT
DECLARE @epa_job_id			 INT


DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

DECLARE @V_TREE_id        bigint



BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.IMPORT_TAXONOMY_VALIDATION', 
                                                @epa_job_id = @in_job_fk, @epa_batch_no = @in_job_FK,
												@exec_id = @l_exec_no OUTPUT;
												
SET @l_sysdate = GETDATE()


SET @V_TREE_id = (select TAXONOMY_TREE_ID from epacube.TAXONOMY_TREE 
where name = (select distinct DATA_POSITION20 from import.IMPORT_RECORD_DATA 
where job_Fk =   @IN_JOB_FK
))
 
/*
 -- ---------------------------------------------------------------------------------------------------
 --     Used for imports that are of Taxonomy type.  Need to validate tree and nodes and 
        data names checked in data cleanser validation from name value table.
 -- ---------------------------------------------------------------------------------------------------
 --*/
 ---make sure we do not auto insert values that are considered restricted
 
update epacube.DATA_NAME
set INSERT_MISSING_VALUES = NULL 
where DATA_NAME_ID in (
select distinct DATA_NAME_FK from epacube.TAXONOMY_NODE_ATTRIBUTE
where RESTRICTED_DATA_VALUES_IND = 1)

 ----------------------------------------------------------------------------------------------
 ---need to remove null values from import record name value and import record for product
 --- or this causes bogus errors
 -----------------------------------------------------------------------------------------------

  Delete from import.IMPORT_RECORD_DATA where DATA_POSITION2 ='NULL' and JOB_FK =  @IN_JOB_FK
  Delete from import.IMPORT_RECORD where ENTITY_ID_VALUE ='NULL' and JOB_FK = @IN_JOB_FK
 
 ---No need to try and validate NULL DATA

  Delete from import.import_record_name_value where DATA_NAME = 'NULL' and JOB_FK = @IN_JOB_FK

 -------------------------------------------------------------------------------------------------
 --Please do not create new products here so need to remove those entries. or flag to not import
 -------------------------------------------------------------------------------------------------
update ird 
set DO_NOT_IMPORT_IND = 1
from import.import_Record_data ird where 1= 1
and ird.data_position2 not in (
select value from epacube.PRODUCT_IDENTIFICATION where DATA_NAME_FK =( select IDENT_DATA_NAME_FK
 from import.IMPORT_MAP_METADATA where SOURCE_FIELD_NAME = 'IMPORT PRODUCT ID' and IMPORT_PACKAGE_FK = @v_import_package_fk)
 )
and ird.job_fk = @in_job_fk

----------LOG FOR USERS

 INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IRD.JOB_FK
            ,'IMPORT TAXONOMY PRODUCT ATTRIBUTES'
            ,IRD.IMPORT_RECORD_DATA_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'INVALID PRODUCT - NOT IN EPACUBE'
           ,IRD.DATA_POSITION20
           ,ird.DATA_POSITION2
           ,NULL
           ,NULL
           ,GETDATE ()
FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
where 1= 1
and ird.data_position2 not in (
select value from epacube.PRODUCT_IDENTIFICATION where DATA_NAME_FK =( select IDENT_DATA_NAME_FK
 from import.IMPORT_MAP_METADATA where SOURCE_FIELD_NAME = 'IMPORT PRODUCT ID' and IMPORT_PACKAGE_FK = @v_import_package_fk)
 )
and ird.job_fk = @in_job_fk
   


------------------------------------------------------------------------------------------------------
 ---CHECK IF TREE EXIST
 --------------------------------------------------------------------------------------------------------
   
   INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IRD.JOB_FK
            ,'IMPORT TAXONOMY PRODUCT ATTRIBUTES'
            ,IRD.IMPORT_RECORD_DATA_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING TAXONOMY TREE'
           ,IRD.DATA_POSITION20
           ,ird.DATA_POSITION2
           ,NULL
           ,NULL
           ,GETDATE ()
FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
WHERE 1=1
and JOB_FK = @IN_JOB_FK 
AND  DATA_POSITION20  <> (select NAME from epacube.TAXONOMY_TREE WITH (nolock) where TAXONOMY_TREE_ID = @V_TREE_id )
   

 update ird
 set DO_NOT_IMPORT_IND = 1
  FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
WHERE JOB_FK = @IN_JOB_FK 
AND  DATA_POSITION20  <> (select NAME from epacube.TAXONOMY_TREE WITH (nolock) where TAXONOMY_TREE_ID = @V_TREE_id )


----------------------------------------------------------------------------------------
----CHECK IF TREE NODE EXIST
-----------------------------------------------------------------------------------------
--CHECK NODE ID FIRST


INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT distinct
            IRD.JOB_FK
            ,'IMPORT TAXONOMY PRODUCT ATTRIBUTES'
            ,IRD.IMPORT_RECORD_DATA_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING TAXONOMY NODE ID - FOR THIS TREE'
           ,IRD.DATA_POSITION7
		   ,IRD.DATA_POSITION3
		   ,ird.DATA_POSITION1
           ,IRD.DATA_POSITION2
           ,GETDATE ()
FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
WHERE JOB_FK = @IN_JOB_FK 
AND  DATA_POSITION7 not in (SELECT
 tn.TAXONOMY_NODE_ID
 from epacube.taxonomy_tree tt
  inner join epacube.TAXONOMY_NODE tn
   on (tt.TAXONOMY_TREE_ID = tn.TAXONOMY_TREE_FK)
left join epacube.TAXONOMY_NODE_ATTRIBUTE tna
  inner join epacube.data_name dn
 on (dn.DATA_NAME_ID = tna.DATA_NAME_FK)
inner join epacube.data_set ds
 on (ds.DATA_SET_ID = dn.DATA_SET_FK)
 on (tn.TAXONOMY_NODE_ID = tna.TAXONOMY_NODE_FK)
 inner join epacube.data_value nodevalue
 on (nodevalue.DATA_VALUE_ID = tn.TAX_NODE_DV_FK)
  and tt.NAME =(select NAME from epacube.TAXONOMY_TREE WITH (nolock) where TAXONOMY_TREE_ID =   @V_TREE_id))


  
update ird 
set DO_NOT_IMPORT_IND =  1 
FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
WHERE JOB_FK = @IN_JOB_FK 
AND  DATA_POSITION7 not in (SELECT
 tn.TAXONOMY_NODE_ID
 from epacube.taxonomy_tree tt
  inner join epacube.TAXONOMY_NODE tn
   on (tt.TAXONOMY_TREE_ID = tn.TAXONOMY_TREE_FK)
left join epacube.TAXONOMY_NODE_ATTRIBUTE tna
  inner join epacube.data_name dn
 on (dn.DATA_NAME_ID = tna.DATA_NAME_FK)
inner join epacube.data_set ds
 on (ds.DATA_SET_ID = dn.DATA_SET_FK)
 on (tn.TAXONOMY_NODE_ID = tna.TAXONOMY_NODE_FK)
 inner join epacube.data_value nodevalue
 on (nodevalue.DATA_VALUE_ID = tn.TAX_NODE_DV_FK)
 and tt.NAME =(select NAME from epacube.TAXONOMY_TREE WITH (nolock) where TAXONOMY_TREE_ID = @V_TREE_id))

 -----CHECK NODE NAME
   
   INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT distinct
            IRD.JOB_FK
            ,'IMPORT TAXONOMY PRODUCT ATTRIBUTES'
            ,IRD.IMPORT_RECORD_DATA_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING TAXONOMY NODE VALUE - FOR THIS TREE'
           ,IRD.DATA_POSITION3
		   ,ird.DATA_POSITION1
           ,IRD.DATA_POSITION2
           ,ird.DATA_POSITION7
           ,GETDATE ()
FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
WHERE JOB_FK = @IN_JOB_FK 
AND  DATA_POSITION3 not in (SELECT
 nodevalue.value Tax_node 
 from epacube.taxonomy_tree tt
  inner join epacube.TAXONOMY_NODE tn
   on (tt.TAXONOMY_TREE_ID = tn.TAXONOMY_TREE_FK)
left join epacube.TAXONOMY_NODE_ATTRIBUTE tna
  inner join epacube.data_name dn
 on (dn.DATA_NAME_ID = tna.DATA_NAME_FK)
inner join epacube.data_set ds
 on (ds.DATA_SET_ID = dn.DATA_SET_FK)
 on (tn.TAXONOMY_NODE_ID = tna.TAXONOMY_NODE_FK)
 inner join epacube.data_value nodevalue
 on (nodevalue.DATA_VALUE_ID = tn.TAX_NODE_DV_FK)
  and tt.NAME =(select NAME from epacube.TAXONOMY_TREE WITH (nolock) where TAXONOMY_TREE_ID = @V_TREE_id))



update ird 
set DO_NOT_IMPORT_IND =  1 
FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
WHERE JOB_FK = @IN_JOB_FK 
AND  DATA_POSITION3 not in (SELECT
 nodevalue.value Tax_node 
 from epacube.taxonomy_tree tt
  inner join epacube.TAXONOMY_NODE tn
   on (tt.TAXONOMY_TREE_ID = tn.TAXONOMY_TREE_FK)
left join epacube.TAXONOMY_NODE_ATTRIBUTE tna
  inner join epacube.data_name dn
 on (dn.DATA_NAME_ID = tna.DATA_NAME_FK)
inner join epacube.data_set ds
 on (ds.DATA_SET_ID = dn.DATA_SET_FK)
 on (tn.TAXONOMY_NODE_ID = tna.TAXONOMY_NODE_FK)
 inner join epacube.data_value nodevalue
 on (nodevalue.DATA_VALUE_ID = tn.TAX_NODE_DV_FK)
 and tt.NAME =(select NAME from epacube.TAXONOMY_TREE WITH (nolock) where TAXONOMY_TREE_ID = @V_TREE_id))



-----------------------------------------------------------------------------------
 ----CHECKING THE REMOVE ADD FLAG AT NODE LEVEL

 --if NULL just remove the product catgory so we don't do anything with it.
 -- move it off for reference to postion21
 ------------------------------------------------------------------------------------
 update import.import_record_data
 set data_position21 = data_position3 
 where job_fk = @in_job_Fk
 ----------------------------------------------------------------------------------
 ----set to NULL if there is no action on the remove add flag
update ird 
set data_position3 = NULL
from import.import_Record_data ird where 1= 1
and  isNULL(ird.data_position26,'NULL') = 'NULL'
and ird.job_fk = @in_job_fk

--------------- remove if not an Add or Delete.. we are not allowing any updates here.
update ird 
set data_position3 = NULL
from import.import_Record_data ird where 1= 1
and ird.data_position26 not in ('A','R')
and ird.job_fk = @in_job_fk

------------------------------------------------------------------------------------------
--CREATE INACTIVATION EVENT FOR TAX NODE
--------------------------------------------------------------------------------------------

 INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
		   ,ORG_ENTITY_STRUCTURE_FK
           ,[NEW_DATA]
           ,[CURRENT_DATA]
           ,[DATA_VALUE_FK]         
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[TABLE_ID_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
		   ,PARENT_STRUCTURE_FK)
   ( SELECT distinct
	150
	,10109
	,getdate()
	,pc.PRODUCT_STRUCTURE_FK
	,(select data_name_fk from import.IMPORT_MAP_METADATA where SOURCE_FIELD_NAME = 'TAX NODE' and IMPORT_PACKAGE_FK = ird.IMPORT_PACKAGE_FK)
	,pc.PRODUCT_STRUCTURE_FK
	,1
	,'-999999999' --new data
	,dv.value
	,dv.DATA_VALUE_ID
	,97
	,87
	,66
	,70
	,53
	,ird.job_fk
	,ird.EFFECTIVE_DATE
	,ird.IMPORT_PACKAGE_FK
	,ird.IMPORT_FILENAME
	 ,pc.PRODUCT_CATEGORY_ID 
	 ,2
	 ,getdate()
	 ,'REMOVE PROD FROM NODE'
	 ,ird.DATA_POSITION25
	 from import.IMPORT_RECORD_DATA  IRD WITH (nolock)
 inner join epacube.TAXONOMY_NODE TN  WITH (nolock)
 on (IRD.DATA_POSITION25 = tn.TAXONOMY_NODE_ID)
 inner join epacube.data_value dv WITH (nolock)
 on (tn.TAX_NODE_DV_FK = dv.DATA_VALUE_ID)
 inner join epacube.PRODUCT_IDENTIFICATION pi WITH (nolock)
 on (pi.value = ird.DATA_POSITION2 
 and pi.DATA_NAME_FK = (select IDENT_DATA_NAME_FK
 from import.IMPORT_MAP_METADATA where SOURCE_FIELD_NAME = 'IMPORT PRODUCT ID' and IMPORT_PACKAGE_FK = @v_import_package_fk))
 inner join epacube.PRODUCT_CATEGORY pc WITH (nolock)
 on (pc.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
 and pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
 and pc.TAXONOMY_NODE_FK = tn.TAXONOMY_NODE_ID)
 where 1 = 1
 and ird.DO_NOT_IMPORT_IND is NULL
 and  ird.job_fk = @IN_JOB_FK
 and DATA_POSITION26 = 'R')

---------------------------------------------------------
 update ird 
set data_position3 = NULL
from import.import_Record_data ird where 1= 1
and  ird.data_position26 = 'R'
and ird.job_fk = @in_job_fk


   
-----------------------------------------------------------------------------------------------
---CREATE EVENTS FOR ADDING A PRODUCT TO A TAX NODE
---------------------------------------------------------------------------------------------

----check to see if Product aready exist on NODE
----commented out because second time around marked all my changes as do not import

--update ird
--set DO_NOT_IMPORT_IND = 1
-- from import.IMPORT_RECORD_DATA  IRD WITH (nolock)
-- inner join epacube.TAXONOMY_NODE TN  WITH (nolock)
-- on (IRD.DATA_POSITION25 = tn.TAXONOMY_NODE_ID) 
-- inner join epacube.data_value dv WITH (nolock)
-- on (tn.TAX_NODE_DV_FK = dv.DATA_VALUE_ID)
-- inner join epacube.PRODUCT_IDENTIFICATION pi WITH (nolock)
-- on (pi.value = ird.DATA_POSITION2)
--inner join epacube.PRODUCT_CATEGORY pc
-- on (Pc.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
-- and pc.DATA_VALUE_FK = tn.TAX_NODE_DV_FK)
--  where 1 = 1
--  and ird.DO_NOT_IMPORT_IND is NULL
-- and  ird.job_fk =  @IN_JOB_FK
-- and DATA_POSITION26  = 'A'





 INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
		   ,ORG_ENTITY_STRUCTURE_FK
           ,[NEW_DATA]
           ,[CURRENT_DATA]
           ,[DATA_VALUE_FK]         
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[TABLE_ID_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER]
		   ,PARENT_STRUCTURE_FK)
   ( SELECT distinct 
	150
	,10109
	,getdate()
	,pi.PRODUCT_STRUCTURE_FK
	,(select data_name_fk from import.IMPORT_MAP_METADATA where SOURCE_FIELD_NAME = 'TAX NODE' and IMPORT_PACKAGE_FK = ird.IMPORT_PACKAGE_FK)
	,pi.PRODUCT_STRUCTURE_FK
	,1
	,ird.data_Position3--new data
	,dv.value
	,dv.DATA_VALUE_ID
	,97
	,87
	,66
	,70
	,51
	,ird.job_fk
	,ird.EFFECTIVE_DATE
	,ird.IMPORT_PACKAGE_FK
	,ird.IMPORT_FILENAME
	 ,NULL 
	 ,1
	 ,getdate()
	 ,'ADD PROD TO NODE'
	  ,ird.data_position25
	 from import.IMPORT_RECORD_DATA  IRD WITH (nolock)
 inner join epacube.TAXONOMY_NODE TN  WITH (nolock)
 on (IRD.DATA_POSITION25 = tn.TAXONOMY_NODE_ID) 
 inner join epacube.data_value dv WITH (nolock)
 on (tn.TAX_NODE_DV_FK = dv.DATA_VALUE_ID)
 inner join epacube.PRODUCT_IDENTIFICATION pi WITH (nolock)
 on (pi.value = ird.DATA_POSITION2
 and pi.DATA_NAME_FK = (select IDENT_DATA_NAME_FK
 from import.IMPORT_MAP_METADATA where SOURCE_FIELD_NAME = 'IMPORT PRODUCT ID' and IMPORT_PACKAGE_FK = @v_import_package_fk))
  where 1 = 1
  and ird.DO_NOT_IMPORT_IND is NULL
 and  ird.job_fk = @IN_JOB_FK
 and DATA_POSITION26 = 'A')

--------------------------------------------------------
 update ird 
set data_position3 = NULL
from import.import_Record_data ird where 1= 1
and  ird.data_position26 = 'A'
and ird.job_fk = @in_job_fk

-------------------------------------------------------------------------------------------
 ----create events to remove product tax attribute
 --------------------------------------------------------------------------------------------


  INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
		   ,ORG_ENTITY_STRUCTURE_FK
           ,[NEW_DATA]
           ,[CURRENT_DATA]
           ,[DATA_VALUE_FK]         
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[TABLE_ID_FK]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
   ( SELECT Distinct
	153
	,10109
	,getdate()
	,pc.PRODUCT_STRUCTURE_FK
	,tna.DATA_NAME_FK
	,pc.PRODUCT_STRUCTURE_FK
	,1
	,'-999999999' --new data
	,pta.ATTRIBUTE_EVENT_DATA
	,pta.DATA_VALUE_FK
	,97
	,87
	,66
	,70
	,53
	,ird.job_fk
	,ird.EFFECTIVE_DATE
	,ird.IMPORT_PACKAGE_FK
	,ird.IMPORT_FILENAME
	 ,pta.PRODUCT_TAX_ATTRIBUTE_ID
	 ,2
	 ,getdate()
	 ,'REMOVE PROD TAX ATTR'
from import.IMPORT_RECORD_DATA  IRD WITH (nolock)
 inner join epacube.TAXONOMY_NODE TN WITH (nolock)
 on (IRD.DATA_POSITION25 = tn.TAXONOMY_NODE_ID)
 inner join epacube.data_value dv
 on (tn.TAX_NODE_DV_FK = dv.DATA_VALUE_ID)
 inner join epacube.PRODUCT_IDENTIFICATION pi WITH (nolock)
 on (pi.value = ird.DATA_POSITION2
 and pi.DATA_NAME_FK = (select IDENT_DATA_NAME_FK
 from import.IMPORT_MAP_METADATA where SOURCE_FIELD_NAME = 'IMPORT PRODUCT ID' and IMPORT_PACKAGE_FK = @v_import_package_fk))
 inner join epacube.PRODUCT_CATEGORY pc WITH (nolock)
 on (pc.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
 and pc.DATA_VALUE_FK = dv.DATA_VALUE_ID)
 inner join epacube.TAXONOMY_NODE_ATTRIBUTE tna WITH (nolock)
 on (tna.TAXONOMY_NODE_FK = ird.DATA_POSITION25)
 inner join epacube.PRODUCT_TAX_ATTRIBUTE pta WITH (nolock)
 on (pta.DATA_NAME_FK = tna.DATA_NAME_FK
 and pta.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK)
 where 1 = 1
 and ird.DO_NOT_IMPORT_IND is NULL
 and  ird.job_fk = @IN_JOB_FK
 and ird.DATA_POSITION26 = 'R')





-------------------------------------------------------------------------------------
----CHECK IF ATTRIBUTE IS ON THE NODE OR NOT
-------------------------------------------------------------------------------------

 update import.IMPORT_RECORD_NAME_VALUE
 set do_not_import_ind = 1 
 where  JOB_FK = @IN_JOB_FK
 AND IMPORT_RECORD_NAME_VALUE_ID not in (select irnv.IMPORT_RECORD_NAME_VALUE_ID
FROM  import.IMPORT_RECORD_NAME_VALUE IRNV WITH (nolock)
inner join epacube.data_name dn
on (dn.name = irnv.DATA_NAME)
inner join epacube.TAXONOMY_NODE_ATTRIBUTE tna WITH (nolock)
on (tna.DATA_NAME_FK = dn.DATA_NAME_ID)
inner join import.IMPORT_RECORD_DATA ird WITH (nolock)
 on (ird.JOB_FK = irnv.JOB_FK
 and ird.RECORD_NO = irnv.RECORD_NO)
 inner join epacube.TAXONOMY_NODE tn WITH (nolock)
on (tn.TAXONOMY_NODE_ID = tna.TAXONOMY_NODE_FK
and ird.DATA_POSITION25 = tn.TAXONOMY_NODE_ID)
where irnv.JOB_FK =@IN_JOB_FK)


INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
		   ,ERROR_DATA5
           ,[CREATE_TIMESTAMP])
SELECT
           distinct IRD.JOB_FK
            ,'IMPORT TAXONOMY PRODUCT ATTRIBUTES'
            ,IRnv.IMPORT_RECORD_name_value_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'ATTRIBUTE NOT ON NODE'
           ,ird.DATA_POSITION2
		   ,ird.DATA_POSITION21
		   ,ird.DATA_POSITION25
		   ,IRnV.DATA_name
           ,IRnv.DATA_VALUE
		   ,GETDATE ()
 FROM  import.IMPORT_RECORD_NAME_VALUE IRNV WITH (nolock)
inner join import.IMPORT_RECORD_DATA ird
on (ird.RECORD_NO = irnv.RECORD_NO)
where irnv.JOB_FK = @IN_JOB_FK
 and irnv.DO_NOT_IMPORT_IND = 1

-------------------------------------------------------------------


delete from synchronizer.EVENT_DATA where NEW_DATA = CURRENT_DATA
and IMPORT_JOB_FK =  @IN_JOB_FK




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.IMPORT_TAXONOMY_VALIDATION'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk,  @epa_batch_no = @in_job_FK
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.IMPORT_TAXONOMY_VALIDATION ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END





































































































































































































