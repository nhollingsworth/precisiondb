﻿
CREATE PROCEDURE [cleanser].[VALIDATE_NEW_ENTITY] (
            @IN_JOB_FK                         BIGINT     )
   AS
BEGIN

DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @v_sysdate			 DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint;          
     DECLARE @v_exception_rows   bigint;               

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);
	 DECLARE @epa_job_id         bigint;

-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        08/18/2010  Added to validate newly added Entities for Reprocessing.
	
/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   
-------------------------------------------------------------------------------------------------
--   FLOW:
--
--	 
-------------------------------------------------------------------------------------------------
*/

--------------------------------------------------------------------------------
---Check entity identification table
--------------------------------------------------------------------------------
   
   DECLARE 
              @vm$entity_structure_fk        int, 
              @vm$STG_RECORD_ENTITY_FK		 int,
              @vm$STG_RECORD_FK				 int	
			 	
			 
			 
			                                      

      DECLARE  cur_v_import cursor local FOR
						 SELECT  EI.ENTITY_STRUCTURE_FK, srei.stg_record_entity_Fk, srei.stg_record_fk
						    from epacube.ENTITY_IDENTIFICATION  EI
                          inner join stage.STG_RECORD_ENTITY_IDENT srei
                          on (srei.ENTITY_ID_DATA_NAME_FK = EI.DATA_NAME_FK
                          AND srei.ENTITY_DATA_NAME_FK = EI.ENTITY_DATA_NAME_FK
                          AND srei.ENTITY_ID_VALUE = EI.VALUE
                          and srei.JOB_FK =  @IN_JOB_FK
                          and srei.ENTITY_STRUCTURE_FK is null)
                          
        OPEN cur_v_import;
			  
        FETCH NEXT FROM cur_v_import INTO 
                                         @vm$entity_structure_fk,
                                         @vm$STG_RECORD_ENTITY_FK,
                                         @vm$STG_RECORD_FK                                         

         WHILE @@FETCH_STATUS = 0 
         BEGIN
            SET @status_desc =  'Cleanser.Validate_entity: Entity structure fk - '
                                + Cast (@vm$entity_structure_fk as varchar)
                                

            Exec  exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
                          
                          
                          update stage.STG_RECORD_ENTITY_IDENT
                          set ENTITY_STRUCTURE_FK =  @vm$entity_structure_fk
                          Where ENTITY_STRUCTURE_FK is null
                          AND STG_RECORD_ENTITY_FK = @vm$STG_RECORD_ENTITY_FK
                          AND  job_fk =  @IN_JOB_FK;
   
   update stage.STG_RECORD_ENTITY
   SET ENTITY_STRUCTURE_FK =  @vm$entity_structure_fk
   WHERE STG_RECORD_ENTITY_ID = @vm$STG_RECORD_ENTITY_FK
   AND JOB_FK = @IN_JOB_FK
   AND ENTITY_STRUCTURE_FK IS NULL
   
   ----update stage.STG_RECORD_IDENT
   ----SET ID_ENTITY_STRUCTURE_FK =  @vm$entity_structure_fk
   ----WHERE STG_RECORD_FK = @vm$STG_RECORD_FK
   ----AND JOB_FK = @IN_JOB_FK
   ----AND ID_ENTITY_STRUCTURE_FK IS NULL

     update sri
   SET ID_ENTITY_STRUCTURE_FK =  @vm$entity_structure_fk
   from stage.STG_RECORD_IDENT sri
   inner join epacube.data_name dn on dn.DATA_NAME_ID = sri.ID_DATA_NAME_FK
   and dn.ENTITY_DATA_NAME_FK is not NULL
   WHERE sri.STG_RECORD_FK = @vm$STG_RECORD_FK
   AND sri.JOB_FK = @IN_JOB_FK
   AND sri.ID_ENTITY_STRUCTURE_FK IS NULL


   
   
        FETCH NEXT FROM cur_v_import INTO                                           
                          @vm$entity_structure_fk ,
                          @vm$STG_RECORD_ENTITY_FK,
                          @vm$STG_RECORD_FK                                       


     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 
   


--------------------------------------------------------------------------------
-- Also look at entity ident mult table to get new entity
--mainly seller id.
--------------------------------------------------------------------------------
			 
			 
			                                      

      DECLARE  cur_v_import cursor local FOR
						  SELECT  EI.ENTITY_STRUCTURE_FK, srei.stg_record_entity_Fk, srei.stg_record_fk
						    from epacube.ENTITY_IDENT_MULT  EI
                          inner join stage.STG_RECORD_ENTITY_IDENT srei
                          on (srei.ENTITY_ID_DATA_NAME_FK = EI.DATA_NAME_FK
                          --AND srei.ENTITY_DATA_NAME_FK = EI.ENTITY_DATA_NAME_FK
                          AND srei.ENTITY_ID_VALUE = EI.VALUE
                          and srei.JOB_FK =  @in_JOB_FK
                          and srei.ENTITY_STRUCTURE_FK is null)
                          
        OPEN cur_v_import;
			  
        FETCH NEXT FROM cur_v_import INTO 
                                         @vm$entity_structure_fk,
                                         @vm$STG_RECORD_ENTITY_FK,
                                         @vm$STG_RECORD_FK                                         

         WHILE @@FETCH_STATUS = 0 
         BEGIN
            SET @status_desc =  'Cleanser.Validate_entity: Entity structure fk - '
                                + Cast (@vm$entity_structure_fk as varchar)
                                

            Exec  exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
                          
                          
                          update stage.STG_RECORD_ENTITY_IDENT
                          set ENTITY_STRUCTURE_FK =  @vm$entity_structure_fk
                          Where ENTITY_STRUCTURE_FK is null
                          AND STG_RECORD_ENTITY_FK = @vm$STG_RECORD_ENTITY_FK
                          AND  job_fk =  @IN_JOB_FK;
   
   update stage.STG_RECORD_ENTITY
   SET ENTITY_STRUCTURE_FK =  @vm$entity_structure_fk
   WHERE STG_RECORD_ENTITY_ID = @vm$STG_RECORD_ENTITY_FK
   AND JOB_FK = @IN_JOB_FK
   AND ENTITY_STRUCTURE_FK IS NULL
   
   --update stage.STG_RECORD_IDENT
   --SET ID_ENTITY_STRUCTURE_FK =  @vm$entity_structure_fk
   --WHERE STG_RECORD_FK = @vm$STG_RECORD_FK
   --AND JOB_FK = @IN_JOB_FK
   --AND ID_ENTITY_STRUCTURE_FK IS NULL

   
     update sri
   SET ID_ENTITY_STRUCTURE_FK =  @vm$entity_structure_fk
   from stage.STG_RECORD_IDENT sri
   inner join epacube.data_name dn on dn.DATA_NAME_ID = sri.ID_DATA_NAME_FK
   and dn.ENTITY_DATA_NAME_FK is not NULL
   WHERE sri.STG_RECORD_FK = @vm$STG_RECORD_FK
   AND sri.JOB_FK = @IN_JOB_FK
   AND sri.ID_ENTITY_STRUCTURE_FK IS NULL
   
   
        FETCH NEXT FROM cur_v_import INTO                                           
                          @vm$entity_structure_fk ,
                          @vm$STG_RECORD_ENTITY_FK,
                          @vm$STG_RECORD_FK                                       


     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 
   
END
