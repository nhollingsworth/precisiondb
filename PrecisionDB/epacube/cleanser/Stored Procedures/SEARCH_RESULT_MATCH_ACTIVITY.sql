﻿










-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        11/10/2008   Intial Version 
-- 

CREATE PROCEDURE [cleanser].[SEARCH_RESULT_MATCH_ACTIVITY] (
       @IN_JOB_FK               BIGINT 
      ,@IN_BATCH_NO				BIGINT 
      ,@IN_SEARCH_ACTIVITY_FK   INT      
)       
   AS
BEGIN
      DECLARE @l_exec_no   bigint,
              @l_stmt      varchar(8000),
              @ls_exec     varchar(8000),
              @l_sysdate   datetime

		DECLARE @status_desc				varchar (max)
		DECLARE @ErrorMessage				nvarchar(4000)
		DECLARE @ErrorSeverity				int
		DECLARE @ErrorState					int
   
        DECLARE @v_count                    INT
        DECLARE @V_ENTITY_CLASS_CR_FK       INT


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.SEARCH_RESULT_MATCH_ACTIVITY', 
                                                @epa_job_id = @in_job_fk,  @epa_batch_no = @in_batch_no,
												@exec_id = @l_exec_no OUTPUT;

     SET @l_sysdate = GETDATE()



---------------------------------------------------------------------------------------
----   PERFORM DATA CLEANSER IDENTIFICATION MATCHING
---------------------------------------------------------------------------------------


      EXEC cleanser.SEARCH_RESULT_MATCH_IDENT
                @IN_JOB_FK, @IN_BATCH_NO , @IN_SEARCH_ACTIVITY_FK

----	  set @v_sysdate = getdate();
----      set @v_input_rows = @v_output_rows;

----      SELECT @v_output_rows = COUNT(1)
----        FROM stage.STG_ENTITY_STRUCTURE WITH (NOLOCK)
----       WHERE import_job_fk = @in_job_fk
----	   AND   ACTIVITY_ACTION_CR_FK <> 140
----
----	  set @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @l_sysdate = getdate()
----      exec job_execution_create  @in_job_fk, 'ENTITY MATCHING',
----                                @v_input_rows, @v_output_rows, @v_exception_rows,
----							 201, @v_sysdate, @l_sysdate  ;



---------------------------------------------------------------------------------------
----   PERFORM DATA CLEANSER STRING MATCHING
---------------------------------------------------------------------------------------

------>>>>>>>>>>>>>  ADD BACK WHEN WE PUT CLEANSER INTO RELEASE

----      EXEC cleanser.SEARCH_RESULT_MATCH_TASK
----                @IN_BATCH_NO , @IN_SEARCH_ACTIVITY_FK, @IN_JOB_FK

----	  set @v_sysdate = getdate();
----      set @v_input_rows = @v_output_rows;

----      SELECT @v_output_rows = COUNT(1)
----        FROM stage.STG_ENTITY_STRUCTURE WITH (NOLOCK)
----       WHERE import_job_fk = @in_job_fk
----	   AND   ACTIVITY_ACTION_CR_FK <> 140
----
----	  set @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @l_sysdate = getdate()
----      exec job_execution_create  @in_job_fk, 'ENTITY MATCHING',
----                                @v_input_rows, @v_output_rows, @v_exception_rows,
----							 201, @v_sysdate, @l_sysdate  ;


	


------------------------------------------------------------------------------------------
--   Establish BEST records for Cleanser UI
------------------------------------------------------------------------------------------


    EXEC cleanser.SEARCH_RESULT_MATCH_BEST  @IN_JOB_FK, @IN_BATCH_NO



------------------------------------------------------------------------------------------------------------
--   CALL cleanser.DATA_CLEANSER_VALIDATION
--------------------------------------------------------------------------------------------------------------
	 	
    EXEC cleanser.DATA_CLEANSER_VALIDATION
                @IN_JOB_FK, @IN_BATCH_NO

----	  set @v_sysdate = getdate();
----      set @v_input_rows = @v_output_rows;

----      SELECT @v_output_rows = COUNT(1)
----        FROM stage.STG_ENTITY_STRUCTURE WITH (NOLOCK)
----       WHERE import_job_fk = @in_job_fk
----	   AND   ACTIVITY_ACTION_CR_FK <> 140
----
----	  set @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @l_sysdate = getdate()
----      exec job_execution_create  @in_job_fk, 'ENTITY MATCHING',
----                                @v_input_rows, @v_output_rows, @v_exception_rows,
----							 201, @v_sysdate, @l_sysdate  ;
	


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.SEARCH_RESULT_MATCH_ACTIVITY'

Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk,  @epa_batch_no = @in_batch_no;
Exec exec_monitor.print_status_sp @exec_id = @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.SEARCH_RESULT_MATCH_ACTIVITY has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END































