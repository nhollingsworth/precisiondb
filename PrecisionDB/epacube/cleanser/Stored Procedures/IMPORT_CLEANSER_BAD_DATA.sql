﻿











-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: For DUPLICATE DATA CHECK
--			
--			
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        06/02/2010   Initial SQL Version
-- CV	     08/23/2010   tightened down my update statement
-- CV		 11/16/2010   Added More validation for "." 
-- CV		 02/25/2011   Added to no check bad data if do not import ind = 0
-- CV		 03/20/2018   Check for Scientific Notation


CREATE PROCEDURE [cleanser].[IMPORT_CLEANSER_BAD_DATA] 
       @IN_JOB_FK                BIGINT 
      
AS
BEGIN



DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint
DECLARE @v_import_package_fk   int  
DECLARE @v_filename_count INT
DECLARE @epa_job_id			 INT


DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int




BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.IMPORT_CLEANSER_BAD_DATA', 
                                                @epa_job_id = @in_job_fk, @epa_batch_no = @in_job_FK,
												@exec_id = @l_exec_no OUTPUT;
												
SET @l_sysdate = GETDATE()


SET @v_import_package_fk = (select distinct import_package_fk 
							from import.import_record_data WITH (NOLOCK)
							where  job_fk = @in_job_fk)



 
/*
 -- ------------------------------------------------------------------------------------
 --      Loop thru Import Data Names that are either NUMERIC or DATE 
 -- -----------------------------------------------------------------------------------
 --*/


      DECLARE 
              @vm$data_name_fk            varchar(16), 
              @vm$data_name               varchar(64), 
              @vm$data_value_column       varchar(64),
              @vm$import_column_name      varchar(64),               
              @vm$data_validation         varchar(16)
                            

      DECLARE  cur_v_import cursor local for            
               SELECT DISTINCT
					isnull ( cast(vm.data_name_fk as varchar(16)), 'NULL' ) AS data_name_fk,
                    dn.NAME,
                    CASE ISNULL ( dn.trim_ind, 0 )
                    WHEN 1 THEN ( 'UPPER(RTRIM(LTRIM( ID.' + vm.import_column_name + ' )))' ) 
                    ELSE vm.import_column_name 
                    END data_value_column,  
				    vm.import_column_name,
				    CASE ds.data_type_cr_fk
				    WHEN 131 THEN ' ISNUMERIC '
				    WHEN 132 THEN ' ISDATE '
				    ELSE ' ISNUMERIC '
				    END 
			  FROM import.v_import vm
			  INNER JOIN epacube.data_name dn WITH (NOLOCK)
			    ON ( dn.data_name_id = vm.data_name_fk )
			  INNER JOIN epacube.data_set ds WITH (NOLOCK)
			    ON ( ds.data_set_id = dn.data_set_fk )
			    INNER JOIN import.IMPORT_MAP_METADATA imp
			    ON (imp.IMPORT_PACKAGE_FK = vm.IMPORT_PACKAGE_FK
			    AND imp.DATA_NAME_FK = dn.DATA_NAME_ID)
			  INNER JOIN cleanser.SEARCH_JOB_DATA_NAME SADN
			    ON ( sadn.SEARCH_JOB_FK =  @in_job_fk   )
			  WHERE 1 = 1
			  AND   vm.import_package_fk = @v_import_package_fk
			  AND   vm.import_column_name IS NOT NULL 
			  AND   DS.DATA_TYPE_CR_FK IN ( 131)  -- 132 date revist
      
        OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO 
										  @vm$data_name_fk,
										  @vm$data_name,
										  @vm$data_value_column,
										  @vm$import_column_name,
										  @vm$data_validation
                                          

         WHILE @@FETCH_STATUS = 0 
         BEGIN
            SET @status_desc =  'cleanser.IMPORT_CLEANSER_BAD_DATA - '
                                + @vm$data_name
                                + ' '
                                + @vm$import_column_name

            Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;


            SET @ls_exec =
					'INSERT INTO [stage].[STG_RECORD_ERRORS]
							   ([STG_RECORD_FK]
								,STG_RECORD_IDENT_FK
							   ,[JOB_FK]
							   ,[ERROR_NAME]
							   ,[ERROR_DATA1]
							   ,[ERROR_DATA2]
							   ,[ERROR_DATA3]
							   ,[EVENT_CONDITION_CR_FK]
							   ,[BATCH_NO]
							   ,[UPDATE_TIMESTAMP])
					SELECT 
							   SSR.STG_RECORD_ID
							  ,NULL  
							  ,IRD.JOB_FK'
				   +', ''BAD DATA IN IMPORT:: '''
				   + ', '''
				   + @vm$data_name 					   
				   + ''', '					   
				   + ''' SHOULD BE DATA TYPE '''			  
				   + ', '''
				   + @vm$data_validation 					   
				   + ''' '					   
				   + '        ,98    
							  ,SSR.BATCH_NO
							  ,GETDATE()
						FROM IMPORT.IMPORT_RECORD_DATA IRD WITH (NOLOCK)
						INNER JOIN STAGE.STG_RECORD SSR WITH (NOLOCK)
						ON (SSR.RECORD_NO = IRD.RECORD_NO
						AND SSR.JOB_FK = IRD.JOB_FK)
													
						WHERE 1 = 1
						AND ISNULL(IRD.DO_NOT_IMPORT_IND,0) = 0 
						AND   IRD.JOB_FK = '
                   + cast(isnull (@in_job_fk, -999) as varchar(20))						
				   + '	AND   '
                   + @vm$data_validation
				   + '( ISNULL('
				   + @vm$import_column_name
				   + ',1) ) <> 1 '					
					
 INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), ISNULL(@ls_exec,'CRAPPY CODE') )

         exec sp_executesql 	@ls_exec;

-----add to validate if . or not

   SET @ls_exec =
					'INSERT INTO [stage].[STG_RECORD_ERRORS]
							   ([STG_RECORD_FK]
								,STG_RECORD_IDENT_FK
							   ,[JOB_FK]
							   ,[ERROR_NAME]
							   ,[ERROR_DATA1]
							   ,[ERROR_DATA2]
							   ,[ERROR_DATA3]
							   ,[EVENT_CONDITION_CR_FK]
							   ,[BATCH_NO]
							   ,[UPDATE_TIMESTAMP])
					SELECT 
							   SSR.STG_RECORD_ID
							  ,NULL  
							  ,IRD.JOB_FK'
				   +', ''BAD DATA IN IMPORT:: '''
				   + ', '''
				   + @vm$data_name 					   
				   + ''', '					   
				   + ''' SHOULD BE DATA TYPE '''			  
				   + ', '''
				   + @vm$data_validation 					   
				   + ''' '					   
				   + '        ,98    
							  ,SSR.BATCH_NO
							  ,GETDATE()
						FROM IMPORT.IMPORT_RECORD_DATA IRD WITH (NOLOCK)
						INNER JOIN STAGE.STG_RECORD SSR WITH (NOLOCK)
						ON (SSR.RECORD_NO = IRD.RECORD_NO
						AND SSR.JOB_FK = IRD.JOB_FK)
													
						WHERE 1 = 1
                        AND ISNULL(IRD.DO_NOT_IMPORT_IND,0) = 0
						 
						AND   IRD.JOB_FK = '
                   + cast(isnull (@in_job_fk, -999) as varchar(20))						
				   + '	AND   '
                   + @vm$import_column_name
				   +  ' = ''.'' '
				   
				 
				   
				      INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), ISNULL(@ls_exec,'CRAPPY CODE') )					

				   exec sp_executesql 	@ls_exec;
					
-----add to validate if , or not

   SET @ls_exec =
					'INSERT INTO [stage].[STG_RECORD_ERRORS]
							   ([STG_RECORD_FK]
								,STG_RECORD_IDENT_FK
							   ,[JOB_FK]
							   ,[ERROR_NAME]
							   ,[ERROR_DATA1]
							   ,[ERROR_DATA2]
							   ,[ERROR_DATA3]
							   ,[EVENT_CONDITION_CR_FK]
							   ,[BATCH_NO]
							   ,[UPDATE_TIMESTAMP])
					SELECT 
							   SSR.STG_RECORD_ID
							  ,NULL  
							  ,IRD.JOB_FK'
				   +', ''BAD DATA IN IMPORT:: '''
				   + ', '''
				   + @vm$data_name 					   
				   + ''', '					   
				   + ''' SHOULD BE DATA TYPE '''			  
				   + ', '''
				   + @vm$data_validation 					   
				   + ''' '					   
				   + '        ,98    
							  ,SSR.BATCH_NO
							  ,GETDATE()
						FROM IMPORT.IMPORT_RECORD_DATA IRD WITH (NOLOCK)
						INNER JOIN STAGE.STG_RECORD SSR WITH (NOLOCK)
						ON (SSR.RECORD_NO = IRD.RECORD_NO
						AND SSR.JOB_FK = IRD.JOB_FK)
													
						WHERE 1 = 1
                        AND ISNULL(IRD.DO_NOT_IMPORT_IND,0) = 0
						 
						AND   IRD.JOB_FK = '
                   + cast(isnull (@in_job_fk, -999) as varchar(20))						
				   + '	AND   '
                   + @vm$import_column_name
				   +  ' like ''%,%'' '				
				   
				      INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), ISNULL(@ls_exec,'CRAPPY CODE') )	
					
					exec sp_executesql 	@ls_exec;

-----add to check for scientific notation
---------------------------------------------------------------------------

   SET @ls_exec =
					'INSERT INTO [stage].[STG_RECORD_ERRORS]
							   ([STG_RECORD_FK]
								,STG_RECORD_IDENT_FK
							   ,[JOB_FK]
							   ,[ERROR_NAME]
							   ,[ERROR_DATA1]
							   ,[ERROR_DATA2]
							   ,[ERROR_DATA3]
							   ,[EVENT_CONDITION_CR_FK]
							   ,[BATCH_NO]
							   ,[UPDATE_TIMESTAMP])
					SELECT 
							   SSR.STG_RECORD_ID
							  ,NULL  
							  ,IRD.JOB_FK'
				   +', ''BAD DATA IN IMPORT:: '''
				   + ', '''
				   + @vm$data_name 					   
				   + ''', '					   
				   + ''' SHOULD BE DATA TYPE '''			  
				   + ', '''
				   + @vm$data_validation 					   
				   + ''' '					   
				   + '        ,98    
							  ,SSR.BATCH_NO
							  ,GETDATE()
						FROM IMPORT.IMPORT_RECORD_DATA IRD WITH (NOLOCK)
						INNER JOIN STAGE.STG_RECORD SSR WITH (NOLOCK)
						ON (SSR.RECORD_NO = IRD.RECORD_NO
						AND SSR.JOB_FK = IRD.JOB_FK)
													
						WHERE 1 = 1
                        AND ISNULL(IRD.DO_NOT_IMPORT_IND,0) = 0
						 
						AND   IRD.JOB_FK = '
                   + cast(isnull (@in_job_fk, -999) as varchar(20))						
				   + '	AND   '
                   + @vm$import_column_name
				   +  ' like ''%E%'' '				
				   


				      INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), ISNULL(@ls_exec,'CRAPPY CODE') )	
					
					exec sp_executesql 	@ls_exec;

 SET @ls_exec =
					'INSERT INTO [stage].[STG_RECORD_ERRORS]
							   ([STG_RECORD_FK]
								,STG_RECORD_IDENT_FK
							   ,[JOB_FK]
							   ,[ERROR_NAME]
							   ,[ERROR_DATA1]
							   ,[ERROR_DATA2]
							   ,[ERROR_DATA3]
							   ,[EVENT_CONDITION_CR_FK]
							   ,[BATCH_NO]
							   ,[UPDATE_TIMESTAMP])
					SELECT 
							   SSR.STG_RECORD_ID
							  ,NULL  
							  ,IRD.JOB_FK'
				   +', ''BAD DATA IN IMPORT:: '''
				   + ', '''
				   + @vm$data_name 					   
				   + ''', '					   
				   + ''' SHOULD BE DATA TYPE '''			  
				   + ', '''
				   + @vm$data_validation 					   
				   + ''' '					   
				   + '        ,98    
							  ,SSR.BATCH_NO
							  ,GETDATE()
						FROM IMPORT.IMPORT_RECORD_DATA IRD WITH (NOLOCK)
						INNER JOIN STAGE.STG_RECORD SSR WITH (NOLOCK)
						ON (SSR.RECORD_NO = IRD.RECORD_NO
						AND SSR.JOB_FK = IRD.JOB_FK)
													
						WHERE 1 = 1
                        AND ISNULL(IRD.DO_NOT_IMPORT_IND,0) = 0
						 
						AND   IRD.JOB_FK = '
                   + cast(isnull (@in_job_fk, -999) as varchar(20))						
				   + '	AND   '
                   + @vm$import_column_name
				   +  ' like ''%$%'' '		

                        
--AND   IRD.DO_NOT_IMPORT_IND = 1 removed as don't see thisbeing set
--           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), ISNULL(@ls_exec,'CRAPPY CODE') )

         exec sp_executesql 	@ls_exec;

            

        FETCH NEXT FROM cur_v_import INTO 
										  @vm$data_name_fk,
										  @vm$data_name,
										  @vm$data_value_column,
										  @vm$import_column_name,
										  @vm$data_validation


     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 
     

-------------------------------------------------------------------------
--NEED TO UPDATE DO NOT IMPORT RECORD DATA IND
------------------------------------------------------------------------
 UPDATE IMPORT.IMPORT_RECORD_DATA
			SET DO_NOT_IMPORT_IND = 1
			from stage.STG_RECORD_ERRORS
			WHERE IMPORT.IMPORT_RECORD_DATA.RECORD_NO in 
			(SELECT RECORD_NO from stage.STG_RECORD 
			where STG_RECORD_ID in (select STG_RECORD_FK
			 from stage.STG_RECORD_ERRORS
			where 1 = 1
			AND ERROR_NAME = 'BAD DATA IN IMPORT:: '
			AND JOB_FK = @IN_JOB_FK))
			AND IMPORT.IMPORT_RECORD_DATA.JOB_FK = @in_job_fk
			

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.IMPORT_CLEANSER_BAD_DATA'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk,  @epa_batch_no = @in_job_FK
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.IMPORT_CLEANSER_BAD_DATA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END





































































































































































































