﻿










-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: For Cleansing Bad Data in Excel Spreadsheet for rule import.
--			
--			
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        06/02/2010   Initial SQL Version
-- CV        09/06/2011   Write errors to the job error table instead 
--                        off rules error table.
-- CV        12/20/2011   Added check for duplicate key1
-- KS        05/30/2012   Restrict precedence error checking not if rule type (312, 313)
-- CV	     02/15/2013   Added Check to validate rules action operation name.
-- CV        02/26/2013   Tighten down check and add error insert.


CREATE PROCEDURE [cleanser].[IMPORT_RULES_CLEANSER] 
       @IN_JOB_FK         BIGINT 
       
AS
BEGIN



DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint
DECLARE @v_import_package_fk   int  
DECLARE @V_UNIQUE1   varchar(25) 
DECLARE @V_UNIQUE2   varchar(25) 
DECLARE @V_UNIQUE3   varchar(25) 
DECLARE @v_filename_count INT
DECLARE @epa_job_id			 INT


DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int




BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.IMPORT_RULES_CLEANSER', 
                                                @epa_job_id = @in_job_fk, @epa_batch_no = @IN_JOB_FK,
												@exec_id = @l_exec_no OUTPUT;
												
SET @l_sysdate = GETDATE()



 -- ------------------------------------------------------------------------------------
 --			CHECK FOR SPACES - TRIM ALL FIELDS
 -- -----------------------------------------------------------------------------------


update import.IMPORT_RULES
   Set 
	   RULES_STRUCTURE_NAME = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RULES_STRUCTURE_NAME)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RULES_STRUCTURE_NAME)) )
		                  END )
	  ,RULES_SCHEDULE_NAME = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RULES_SCHEDULE_NAME)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RULES_SCHEDULE_NAME)) )
		                  END )
      ,RULE_NAME = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RULE_NAME)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RULE_NAME)) )
		                  END )
      ,SHORT_NAME = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SHORT_NAME)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SHORT_NAME)) )
		                  END )
      ,RESULT_DATA_NAME= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RESULT_DATA_NAME)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RESULT_DATA_NAME)) )
		                  END )
      ,RESULT_DATA_NAME2= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RESULT_DATA_NAME2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RESULT_DATA_NAME2)) )
		                  END )
      ,EFFECTIVE_DATE= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(EFFECTIVE_DATE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(EFFECTIVE_DATE)) )
		                  END )
      ,END_DATE= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(END_DATE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(END_DATE)) )
		                  END )
      ,TRIGGER_EVENT_TYPE= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(TRIGGER_EVENT_TYPE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(TRIGGER_EVENT_TYPE)) )
		                  END )
      ,TRIGGER_EVENT_DN= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(TRIGGER_EVENT_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(TRIGGER_EVENT_DN)) )
		                  END )
      ,TRIGGER_OPERATOR= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(TRIGGER_OPERATOR)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(TRIGGER_OPERATOR)) )
		                  END )
      ,TRIGGER_EVENT_VALUE1= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(TRIGGER_EVENT_VALUE1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(TRIGGER_EVENT_VALUE1)) )
		                  END )
      ,TRIGGER_EVENT_VALUE2= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(TRIGGER_EVENT_VALUE2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(TRIGGER_EVENT_VALUE2)) )
		                  END )
      ,TRIGGER_IMPORT_PACKAGE= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(TRIGGER_IMPORT_PACKAGE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(TRIGGER_IMPORT_PACKAGE)) )
		                  END )
      ,TRIGGER_EVENT_SOURCE= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(TRIGGER_EVENT_SOURCE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(TRIGGER_EVENT_SOURCE)) )
		                  END )
      ,TRIGGER_EVENT_ACTION= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(TRIGGER_EVENT_ACTION)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(TRIGGER_EVENT_ACTION)) )
		                  END )
      ,APPLY_TO_ORG_CHILDREN_IND= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(APPLY_TO_ORG_CHILDREN_IND)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(APPLY_TO_ORG_CHILDREN_IND)) )
		                  END )
      ,PROD_FILTER_DN= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(PROD_FILTER_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(PROD_FILTER_DN)) )
		                  END )
      ,PROD_FILTER_VALUE1= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(PROD_FILTER_VALUE1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(PROD_FILTER_VALUE1)) )
		                  END )
      ,PROD_FILTER_VALUE2= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(PROD_FILTER_VALUE2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(PROD_FILTER_VALUE2)) )
		                  END )
      ,PROD_FILTER_OPERATOR= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(PROD_FILTER_OPERATOR)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(PROD_FILTER_OPERATOR)) )
		                  END )
      ,PROD_FILTER_ASSOC_DN= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(PROD_FILTER_ASSOC_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(PROD_FILTER_ASSOC_DN)) )
		                  END )
      ,ORG_FILTER_DN= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(ORG_FILTER_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(ORG_FILTER_DN)) )
		                  END )
      ,ORG_FILTER_ENTITY_DN= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(ORG_FILTER_ENTITY_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(ORG_FILTER_ENTITY_DN)) )
		                  END )
      ,ORG_FILTER_VALUE1= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(ORG_FILTER_VALUE1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(ORG_FILTER_VALUE1)) )
		                  END )
      ,ORG_FILTER_VALUE2= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(ORG_FILTER_VALUE2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(ORG_FILTER_VALUE2)) )
		                  END )
      ,ORG_FILTER_OPERATOR= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(ORG_FILTER_OPERATOR)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(ORG_FILTER_OPERATOR)) )
		                  END )
      ,ORG_FILTER_ASSOC_DN= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(ORG_FILTER_ASSOC_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(ORG_FILTER_ASSOC_DN)) )
		                  END )
      ,CUST_FILTER_DN= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(CUST_FILTER_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(CUST_FILTER_DN)) )
		                  END )
      ,CUST_FILTER_ENTITY_DN= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(CUST_FILTER_ENTITY_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(CUST_FILTER_ENTITY_DN)) )
		                  END )
      ,CUST_FILTER_VALUE1= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(CUST_FILTER_VALUE1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(CUST_FILTER_VALUE1)) )
		                  END )
      ,CUST_FILTER_VALUE2= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(CUST_FILTER_VALUE2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(CUST_FILTER_VALUE2)) )
		                  END )
      ,CUST_FILTER_OPERATOR= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(CUST_FILTER_OPERATOR)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(CUST_FILTER_OPERATOR)) )
		                  END )
      ,CUST_FILTER_ASSOC_DN= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(CUST_FILTER_ASSOC_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(CUST_FILTER_ASSOC_DN)) )
		                  END )
      ,SUPL_FILTER_DN= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUPL_FILTER_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUPL_FILTER_DN)) )
		                  END )
      ,SUPL_FILTER_ENTITY_DN= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUPL_FILTER_ENTITY_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUPL_FILTER_ENTITY_DN)) )
		                  END )
      ,SUPL_FILTER_VALUE1= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUPL_FILTER_VALUE1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUPL_FILTER_VALUE1)) )
		                  END )
      ,SUPL_FILTER_VALUE2= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUPL_FILTER_VALUE2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUPL_FILTER_VALUE2)) )
		                  END )
      ,SUPL_FILTER_OPERATOR= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUPL_FILTER_OPERATOR)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUPL_FILTER_OPERATOR)) )
		                  END )
      ,SUPL_FILTER_ASSOC_DN= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUPL_FILTER_ASSOC_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUPL_FILTER_ASSOC_DN)) )
		                  END )
      ,RULE_PRECEDENCE= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RULE_PRECEDENCE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RULE_PRECEDENCE)) )
		                  END )
      ,HOST_PRECEDENCE_LEVEL_DESCRIPTION = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(HOST_PRECEDENCE_LEVEL_DESCRIPTION)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(HOST_PRECEDENCE_LEVEL_DESCRIPTION)) )
		                  END )
      ,HOST_RULE_XREF= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(HOST_RULE_XREF)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(HOST_RULE_XREF)) )
		                  END )
      ,REFERENCE= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REFERENCE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REFERENCE)) )
		                  END )
      ,CONTRACT_NO= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(CONTRACT_NO)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(CONTRACT_NO)) )
		                  END )
      ,RULES_CONTRACT_IND= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RULES_CONTRACT_IND)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RULES_CONTRACT_IND)) )
		                  END )
      ,GROUP_NAME= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(GROUP_NAME)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(GROUP_NAME)) )
		                  END )
      ,DISPLAY_SEQ= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(DISPLAY_SEQ)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(DISPLAY_SEQ)) )
		                  END )
      ,RULE_RESULT_TYPE= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RULE_RESULT_TYPE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RULE_RESULT_TYPE)) )
		                  END )
      ,RULE_STATUS= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RULE_STATUS)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RULE_STATUS)) )
		                  END )
      ,SEED_RULE_IND= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SEED_RULE_IND)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SEED_RULE_IND)) )
		                  END )
      ,RULE_TYPE= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RULE_TYPE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RULE_TYPE)) )
		                  END )
      ,UNIQUE_KEY1= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(UNIQUE_KEY1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(UNIQUE_KEY1)) )
		                  END )
      ,RULES_FK= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RULES_FK)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RULES_FK)) )
		                  END )
      ,EVENT_ACTION_CR_FK= ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(EVENT_ACTION_CR_FK)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(EVENT_ACTION_CR_FK)) )
		                  END )
        WHERE JOB_FK = @IN_JOB_FK 

 ------------------------------------------------------------------------------       



 ------------------------------------------------------------------------------       
        
        
  
 ------------------------------------------------------------------------------       
        
        
  UPDATE IMPORT.IMPORT_RULES_DATA
  SET RULE_NAME = LTRIM(RTRIM(RULE_NAME))
      ,RULES_ACTION_OPERATION1  = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RULES_ACTION_OPERATION1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RULES_ACTION_OPERATION1)) )
		                  END )
      ,RULES_ACTION_OPERATION2  = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RULES_ACTION_OPERATION2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RULES_ACTION_OPERATION2)) )
		                  END )
      ,RULES_ACTION_OPERATION3  = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(RULES_ACTION_OPERATION3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(RULES_ACTION_OPERATION3)) )
		                  END )
      ,COMPARE_OPERATOR = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(COMPARE_OPERATOR)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(COMPARE_OPERATOR)) )
		                  END )
      ,BASIS_CALC_DN = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS_CALC_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS_CALC_DN)) )
		                  END )
      ,BASIS1_DN = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS1_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS1_DN)) )
		                  END )
      ,BASIS2_DN = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS2_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS2_DN)) )
		                  END )
      ,BASIS3_DN = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS3_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS3_DN)) )
		                  END )
      ,BASIS1_NUMBER = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS1_NUMBER)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS1_NUMBER)) )
		                  END )
      ,BASIS2_NUMBER = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS2_NUMBER)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS2_NUMBER)) )
		                  END )
      ,BASIS3_NUMBER = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS3_NUMBER)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS3_NUMBER)) )
		                  END )
      ,BASIS1_VALUE = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS1_VALUE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS1_VALUE)) )
		                  END )
      ,BASIS2_VALUE = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS2_VALUE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS2_VALUE)) )
		                  END )
      ,BASIS3_VALUE = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS3_VALUE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS3_VALUE)) )
		                  END )
      ,PREFIX1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(PREFIX1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(PREFIX1)) )
		                  END )
      ,PREFIX2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(PREFIX2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(PREFIX2)) )
		                  END )
      ,PREFIX3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(PREFIX3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(PREFIX3)) )
		                  END )
      ,BASIS_CALC_PRICESHEET_NAME = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS_CALC_PRICESHEET_NAME)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS_CALC_PRICESHEET_NAME)) )
		                  END )
      ,BASIS1_PRICESHEET_NAME = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS1_PRICESHEET_NAME)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS1_PRICESHEET_NAME)) )
		                  END )
      ,BASIS2_PRICESHEET_NAME = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS2_PRICESHEET_NAME)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS2_PRICESHEET_NAME)) )
		                  END )
      ,BASIS3_PRICESHEET_NAME = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS3_PRICESHEET_NAME)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS3_PRICESHEET_NAME)) )
		                  END )
      ,BASIS_USE_CORP_ONLY_IND = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(BASIS_USE_CORP_ONLY_IND)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(BASIS_USE_CORP_ONLY_IND)) )
		                  END )
      ,ROUNDING_METHOD = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(ROUNDING_METHOD)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(ROUNDING_METHOD)) )
		                  END )
      ,ROUNDING_TO_VALUE = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(ROUNDING_TO_VALUE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(ROUNDING_TO_VALUE)) )
		                  END )
      ,ROUNDING_ADDER_VALUE = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(ROUNDING_ADDER_VALUE)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(ROUNDING_ADDER_VALUE)) )
		                  END )
      ,OPERAND_FILTER1_DN = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_FILTER1_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_FILTER1_DN)) )
		                  END )
      ,OPERAND_FILTER1_OPERATOR = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_FILTER1_OPERATOR)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_FILTER1_OPERATOR)) )
		                  END )
      ,OPERAND_1_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_1_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_1_1)) )
		                  END )
      ,OPERAND_1_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_1_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_1_2)) )
		                  END )
      ,OPERAND_1_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_1_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_1_3)) )
		                  END )
      ,OPERAND_1_4 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_1_4)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_1_4)) )
		                  END )
      ,OPERAND_1_5 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_1_5)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_1_5)) )
		                  END )
      ,OPERAND_1_6 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_1_6)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_1_6)) )
		                  END )
      ,OPERAND_1_7 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_1_7)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_1_7)) )
		                  END )
      ,OPERAND_1_8 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_1_8)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_1_8)) )
		                  END )
      ,OPERAND_1_9 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_1_9)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_1_9)) )
		                  END )
      ,OPERAND_1_10 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_1_10)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_1_10)) )
		                  END )
      ,FILTER_VALUE_1_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_1_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_1_1)) )
		                  END )
      ,FILTER_VALUE_1_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_1_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_1_2)) )
		                  END )
      ,FILTER_VALUE_1_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_1_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_1_3)) )
		                  END )
      ,FILTER_VALUE_1_4 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_1_4)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_1_4)) )
		                  END )
      ,FILTER_VALUE_1_5 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_1_5)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_1_5)) )
		                  END )
      ,FILTER_VALUE_1_6 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_1_6)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_1_6)) )
		                  END )
      ,FILTER_VALUE_1_7 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_1_7)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_1_7)) )
		                  END )
      ,FILTER_VALUE_1_8 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_1_8)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_1_8)) )
		                  END )
      ,FILTER_VALUE_1_9 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_1_9)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_1_9)) )
		                  END )
      ,FILTER_VALUE_1_10 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_1_10)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_1_10)) )
		                  END )
      ,OPERAND_FILTER2_DN = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_FILTER2_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_FILTER2_DN)) )
		                  END )
      ,OPERAND_FILTER2_OPERATOR = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_FILTER2_OPERATOR)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_FILTER2_OPERATOR)) )
		                  END )		                  		                  
      ,OPERAND_2_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_2_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_2_1)) )
		                  END )
      ,OPERAND_2_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_2_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_2_2)) )
		                  END )
      ,OPERAND_2_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_2_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_2_3)) )
		                  END )
      ,OPERAND_2_4 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_2_4)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_2_4)) )
		                  END )
      ,OPERAND_2_5 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_2_5)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_2_5)) )
		                  END )
      ,OPERAND_2_6 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_2_6)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_2_6)) )
		                  END )
      ,OPERAND_2_7 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_2_7)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_2_7)) )
		                  END )
      ,OPERAND_2_8 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_2_8)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_2_8)) )
		                  END )
      ,OPERAND_2_9 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_2_9)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_2_9)) )
		                  END )
      ,OPERAND_2_10 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_2_10)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_2_10)) )
		                  END )
      ,FILTER_VALUE_2_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_2_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_2_1)) )
		                  END )
      ,FILTER_VALUE_2_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_2_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_2_2)) )
		                  END )
      ,FILTER_VALUE_2_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_2_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_2_3)) )
		                  END )
      ,FILTER_VALUE_2_4 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_2_4)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_2_4)) )
		                  END )
      ,FILTER_VALUE_2_5 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_2_5)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_2_5)) )
		                  END )
      ,FILTER_VALUE_2_6 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_2_6)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_2_6)) )
		                  END )
      ,FILTER_VALUE_2_7 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_2_7)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_2_7)) )
		                  END )
      ,FILTER_VALUE_2_8 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_2_8)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_2_8)) )
		                  END )
      ,FILTER_VALUE_2_9 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_2_9)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_2_9)) )
		                  END )
      ,FILTER_VALUE_2_10 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_2_10)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_2_10)) )
		                  END )
      ,OPERAND_FILTER3_DN = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_FILTER3_DN)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_FILTER3_DN)) )
		                  END )
      ,OPERAND_FILTER3_OPERATOR = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_FILTER3_OPERATOR)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_FILTER3_OPERATOR)) )
		                  END )
		                  
      ,OPERAND_3_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_3_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_3_1)) )
		                  END )
      ,OPERAND_3_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_3_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_3_2)) )
		                  END )
      ,OPERAND_3_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_3_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_3_3)) )
		                  END )
      ,OPERAND_3_4 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_3_4)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_3_4)) )
		                  END )
      ,OPERAND_3_5 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_3_5)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_3_5)) )
		                  END )
      ,OPERAND_3_6 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_3_6)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_3_6)) )
		                  END )
      ,OPERAND_3_7 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_3_7)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_3_7)) )
		                  END )
      ,OPERAND_3_8 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_3_8)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_3_8)) )
		                  END )
      ,OPERAND_3_9 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_3_9)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_3_9)) )
		                  END )
      ,OPERAND_3_10 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(OPERAND_3_10)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(OPERAND_3_10)) )
		                  END )
      ,FILTER_VALUE_3_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_3_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_3_1)) )
		                  END )
      ,FILTER_VALUE_3_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_3_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_3_2)) )
		                  END )
      ,FILTER_VALUE_3_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_3_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_3_3)) )
		                  END )
      ,FILTER_VALUE_3_4 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_3_4)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_3_4)) )
		                  END )
      ,FILTER_VALUE_3_5 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_3_5)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_3_5)) )
		                  END )
      ,FILTER_VALUE_3_6 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_3_6)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_3_6)) )
		                  END )
      ,FILTER_VALUE_3_7 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_3_7)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_3_7)) )
		                  END )
      ,FILTER_VALUE_3_8 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_3_8)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_3_8)) )
		                  END )
      ,FILTER_VALUE_3_9 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_3_9)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_3_9)) )
		                  END )
      ,FILTER_VALUE_3_10 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(FILTER_VALUE_3_10)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(FILTER_VALUE_3_10)) )
		                  END )
      ,EVENT_PRIORITY = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(EVENT_PRIORITY)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(EVENT_PRIORITY)) )
		                  END )
      ,EVENT_STATUS = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(EVENT_STATUS)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(EVENT_STATUS)) )
		                  END )
      ,EVENT_CONDITION = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(EVENT_CONDITION)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(EVENT_CONDITION)) )
		                  END )
      ,ERROR_NAME = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(ERROR_NAME)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(ERROR_NAME)) )
		                  END )
      ,SUBSTR_START_POS_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUBSTR_START_POS_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUBSTR_START_POS_1)) )
		                  END )
      ,SUBSTR_LENGTH_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUBSTR_LENGTH_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUBSTR_LENGTH_1)) )
		                  END )
      ,SUBSTR_WHOLE_TOKEN_IND_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUBSTR_WHOLE_TOKEN_IND_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUBSTR_WHOLE_TOKEN_IND_1)) )
		                  END )
      ,TRUNC_FIRST_TOKEN_IND_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(TRUNC_FIRST_TOKEN_IND_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(TRUNC_FIRST_TOKEN_IND_1)) )
		                  END )
      ,REMOVE_SPEC_CHARS_IND_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REMOVE_SPEC_CHARS_IND_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REMOVE_SPEC_CHARS_IND_1)) )
		                  END )
      ,SPECIAL_CHARS_SET_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SPECIAL_CHARS_SET_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SPECIAL_CHARS_SET_1)) )
		                  END )
      ,REMOVE_SPACES_IND_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REMOVE_SPACES_IND_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REMOVE_SPACES_IND_1)) )
		                  END )
      ,REPLACING_SPACE_CHAR_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACING_SPACE_CHAR_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACING_SPACE_CHAR_1)) )
		                  END )
      ,REPLACE_CHAR_1  = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACE_CHAR_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACE_CHAR_1)) )
		                  END )
      ,REPLACING_CHAR_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACING_CHAR_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACING_CHAR_1)) )
		                  END )
      ,REPLACE_STRING_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACE_STRING_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACE_STRING_1)) )
		                  END )
      ,REPLACING_STRING_1 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACING_STRING_1)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACING_STRING_1)) )
		                  END )
      ,SUBSTR_START_POS_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUBSTR_START_POS_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUBSTR_START_POS_2)) )
		                  END )
      ,SUBSTR_LENGTH_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUBSTR_LENGTH_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUBSTR_LENGTH_2)) )
		                  END )
      ,SUBSTR_WHOLE_TOKEN_IND_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUBSTR_WHOLE_TOKEN_IND_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUBSTR_WHOLE_TOKEN_IND_2)) )
		                  END )
      ,TRUNC_FIRST_TOKEN_IND_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(TRUNC_FIRST_TOKEN_IND_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(TRUNC_FIRST_TOKEN_IND_2)) )
		                  END )
      ,REMOVE_SPEC_CHARS_IND_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REMOVE_SPEC_CHARS_IND_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REMOVE_SPEC_CHARS_IND_2)) )
		                  END )
      ,SPECIAL_CHARS_SET_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SPECIAL_CHARS_SET_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SPECIAL_CHARS_SET_2)) )
		                  END )
      ,REMOVE_SPACES_IND_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REMOVE_SPACES_IND_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REMOVE_SPACES_IND_2)) )
		                  END )
      ,REPLACING_SPACE_CHAR_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACING_SPACE_CHAR_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACING_SPACE_CHAR_2)) )
		                  END )
      ,REPLACE_CHAR_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACE_CHAR_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACE_CHAR_2)) )
		                  END )
      ,REPLACING_CHAR_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACING_CHAR_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACING_CHAR_2)) )
		                  END )
      ,REPLACE_STRING_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACE_STRING_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACE_STRING_2)) )
		                  END )
      ,REPLACING_STRING_2 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACING_STRING_2)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACING_STRING_2)) )
		                  END )
      ,SUBSTR_START_POS_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUBSTR_START_POS_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUBSTR_START_POS_3)) )
		                  END )
      ,SUBSTR_LENGTH_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUBSTR_LENGTH_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUBSTR_LENGTH_3)) )
		                  END )
      ,SUBSTR_WHOLE_TOKEN_IND_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SUBSTR_WHOLE_TOKEN_IND_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SUBSTR_WHOLE_TOKEN_IND_3)) )
		                  END )
      ,TRUNC_FIRST_TOKEN_IND_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(TRUNC_FIRST_TOKEN_IND_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(TRUNC_FIRST_TOKEN_IND_3)) )
		                  END )
      ,REMOVE_SPEC_CHARS_IND_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REMOVE_SPEC_CHARS_IND_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REMOVE_SPEC_CHARS_IND_3)) )
		                  END )
      ,SPECIAL_CHARS_SET_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(SPECIAL_CHARS_SET_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(SPECIAL_CHARS_SET_3)) )
		                  END )
      ,REMOVE_SPACES_IND_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REMOVE_SPACES_IND_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REMOVE_SPACES_IND_3)) )
		                  END )
      ,REPLACING_SPACE_CHAR_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACING_SPACE_CHAR_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACING_SPACE_CHAR_3)) )
		                  END )
      ,REPLACE_CHAR_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACE_CHAR_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACE_CHAR_3)) )
		                  END )
      ,REPLACING_CHAR_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACING_CHAR_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACING_CHAR_3)) )
		                  END )
      ,REPLACE_STRING_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACE_STRING_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACE_STRING_3)) )
		                  END )
      ,REPLACING_STRING_3 = ( CASE WHEN ( REPLACE ( LTRIM(RTRIM(REPLACING_STRING_3)), 'NULL', '' ) = '' ) THEN NULL
		                  ELSE ( LTRIM(RTRIM(REPLACING_STRING_3)) )
		                  END )
       WHERE JOB_FK = @IN_JOB_FK 
        


        
------------------------------------------------------------------------------------------
--   IMPORT_RULES
--		INSERT  ERROR MESSAGE FOR BAD DATA LATER... FOR NOW JUST SET ERROR IND
------------------------------------------------------------------------------------------

UPDATE import.IMPORT_RULES
SET ERROR_ON_IMPORT_IND = NULL                          
   ,RULES_FK = NULL
   ,EVENT_ACTION_CR_FK = NULL 
WHERE JOB_FK = @IN_JOB_FK 



--- NAME REQUIRED
UPDATE import.IMPORT_RULES
SET ERROR_ON_IMPORT_IND = 1
WHERE JOB_FK = @IN_JOB_FK 
AND   RULE_NAME IS NULL 



INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING RULE NAME'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
WHERE JOB_FK = @IN_JOB_FK 
AND   RULE_NAME IS NULL 


--- RECORD_NO REQUIRED
UPDATE import.IMPORT_RULES
SET ERROR_ON_IMPORT_IND = 1
WHERE JOB_FK = @IN_JOB_FK 
AND   RECORD_NO IS NULL 




INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING RECORD NUMBER'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
WHERE JOB_FK = @IN_JOB_FK 
AND   RECORD_NO IS NULL 



---- NAME NOT UNIQUE
UPDATE import.IMPORT_RULES
SET ERROR_ON_IMPORT_IND = 1
WHERE JOB_FK = @IN_JOB_FK 
AND   RULE_NAME IN (
		SELECT RULE_NAME
		FROM import.IMPORT_RULES
		WHERE RULE_NAME IS NOT NULL 
		AND JOB_FK = @in_job_fk       
		GROUP BY RULE_NAME
		HAVING COUNT(*) > 1   ) 



INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'DUPLICATE RULE NAMES IN IMPORT'
           ,IR.RULE_NAME
           ,IR.HOST_RULE_XREF
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
WHERE JOB_FK = @IN_JOB_FK 
AND   RULE_NAME IN (
		SELECT RULE_NAME
		FROM import.IMPORT_RULES
		WHERE RULE_NAME IS NOT NULL 
		and JOB_FK = @IN_JOB_FK  
		GROUP BY RULE_NAME
		HAVING COUNT(*) > 1   ) 

--drop temp table if it exists
IF object_id('tempdb..#TS_RULE_HOST_XREF_DUPS') is not null
   drop table #TS_RULE_HOST_XREF_DUPS
   
   
-- create temp table
CREATE TABLE #TS_RULE_HOST_XREF_DUPS(
    RESULT_DATA_NAME         varchar(64)  NULL,
    HOST_RULE_XREF           varchar(128)  NULL,
	 )


INSERT INTO #TS_RULE_HOST_XREF_DUPS
 (  RESULT_DATA_NAME
   ,HOST_RULE_XREF )
SELECT 
       RESULT_DATA_NAME
      ,HOST_RULE_XREF
FROM import.IMPORT_RULES IR
WHERE JOB_FK = @IN_JOB_FK 	
AND   HOST_RULE_XREF IS NOT NULL 
GROUP BY RESULT_DATA_NAME, HOST_RULE_XREF 
HAVING COUNT(*) > 1
 


--- HOST RULE XREF NOT UNIQUE
UPDATE IR
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
INNER JOIN #TS_RULE_HOST_XREF_DUPS TSRHX
ON    TSRHX.RESULT_DATA_NAME = IR.RESULT_DATA_NAME
AND   TSRHX.HOST_RULE_XREF   = IR.HOST_RULE_XREF
WHERE IR.JOB_FK = @IN_JOB_FK 	  

INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'DUPLICATE HOST RULE XREFS IN IMPORT'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
INNER JOIN #TS_RULE_HOST_XREF_DUPS TSRHX
ON    TSRHX.RESULT_DATA_NAME = IR.RESULT_DATA_NAME
AND   TSRHX.HOST_RULE_XREF   = IR.HOST_RULE_XREF
WHERE IR.JOB_FK = @IN_JOB_FK 	  



--drop temp table if it exists
IF object_id('tempdb..#TS_RULE_HOST_XREF_DUPS') is not null
   drop table #TS_RULE_HOST_XREF_DUPS
   


--- RESULT DATA NAME REQUIRED
UPDATE import.IMPORT_RULES
SET ERROR_ON_IMPORT_IND = 1
WHERE JOB_FK = @IN_JOB_FK 
AND   RESULT_DATA_NAME IS NULL    
AND   RULES_STRUCTURE_NAME  NOT IN  (  --- AND NOT AN AUTO SYNC RULE
        SELECT RS.NAME
        FROM synchronizer.RULES_STRUCTURE RS
        WHERE RS.RULE_TYPE_CR_FK NOT IN ( 308, 309 )   )
        
        
        
INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING RESULT DATA NAME'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
WHERE JOB_FK = @IN_JOB_FK 
AND   RESULT_DATA_NAME IS NULL    
AND   RULES_STRUCTURE_NAME  NOT IN  (  --- AND NOT AN AUTO SYNC RULE
        SELECT RS.NAME
        FROM synchronizer.RULES_STRUCTURE RS
        WHERE RS.RULE_TYPE_CR_FK NOT IN ( 308, 309 )   ) 




--- RESULT DATA NAME NOT VALID OR INACTIVE
UPDATE import.IMPORT_RULES 
SET ERROR_ON_IMPORT_IND = 1
WHERE JOB_FK = @IN_JOB_FK 
AND   RESULT_DATA_NAME NOT IN (
        SELECT DN.NAME
        FROM EPACUBE.DATA_NAME DN 
        WHERE DN.RECORD_STATUS_CR_FK = 1 )
        
        INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'RESULT DATA NAME INVALID OR INACTIVE'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
WHERE JOB_FK = @IN_JOB_FK 
AND   RESULT_DATA_NAME NOT IN (
        SELECT DN.NAME
        FROM EPACUBE.DATA_NAME DN 
        WHERE DN.RECORD_STATUS_CR_FK = 1 )


--- STRUCTURE REQURED
UPDATE import.IMPORT_RULES
SET ERROR_ON_IMPORT_IND = 1
WHERE JOB_FK = @IN_JOB_FK 
AND   RULES_STRUCTURE_NAME NOT IN (
            SELECT NAME
            FROM synchronizer.RULES_STRUCTURE )
            
            INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING RULES STRUCTURE'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
WHERE JOB_FK = @IN_JOB_FK 
AND   RULES_STRUCTURE_NAME NOT IN (
            SELECT NAME
            FROM synchronizer.RULES_STRUCTURE )



--- SCHEDULE REQURED
UPDATE IR 
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
INNER JOIN synchronizer.RULES_STRUCTURE RS
 ON ( RS.NAME = IR.RULES_STRUCTURE_NAME )
WHERE JOB_FK = @IN_JOB_FK 
AND   RULES_SCHEDULE_NAME NOT IN (
            SELECT RSCHED.NAME
            FROM synchronizer.RULES_SCHEDULE RSCHED
            WHERE RSCHED.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID ) 




            INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING RULES SCHEDULE'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
           FROM import.IMPORT_RULES IR
INNER JOIN synchronizer.RULES_STRUCTURE RS
 ON ( RS.NAME = IR.RULES_STRUCTURE_NAME )
WHERE JOB_FK = @IN_JOB_FK 
AND   RULES_SCHEDULE_NAME NOT IN (
            SELECT RSCHED.NAME
            FROM synchronizer.RULES_SCHEDULE RSCHED
            WHERE RSCHED.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID ) 
        


--- RULE PRECEDENCE IS REQUIRED
UPDATE import.IMPORT_RULES
SET ERROR_ON_IMPORT_IND = 1
WHERE JOB_FK = @IN_JOB_FK 
AND   RULE_PRECEDENCE IS NULL 
AND   RULES_STRUCTURE_NAME NOT IN (
            SELECT RS.NAME
            FROM synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
            INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
              ON  RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
              AND RH.RULE_TYPE_CR_FK IN ( 312, 313) 
              AND RH.RECORD_STATUS_CR_FK = 1
              )




            INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING RULES PRECEDENCE'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
          FROM import.IMPORT_RULES IR
WHERE JOB_FK = @IN_JOB_FK 
AND   RULE_PRECEDENCE IS NULL 
AND   RULES_STRUCTURE_NAME NOT IN (
            SELECT RS.NAME
            FROM synchronizer.RULES_STRUCTURE RS WITH (NOLOCK) 
            INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
              ON  RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK
              AND RH.RULE_TYPE_CR_FK IN ( 312, 313) 
              AND RH.RECORD_STATUS_CR_FK = 1
              )



---  DATA TYPE CHECKS 

UPDATE import.IMPORT_RULES 
SET ERROR_ON_IMPORT_IND = 1
WHERE JOB_FK = @IN_JOB_FK 
AND (
       ISDATE ( ISNULL ( EFFECTIVE_DATE, '01/01/2001' ) ) = 0 
    OR ISDATE ( ISNULL ( END_DATE, '01/01/2001' )  ) = 0 
    )
    
    
    

            INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'BAD DATA IN DATE FIELD'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
         FROM import.IMPORT_RULES IR
WHERE JOB_FK = @IN_JOB_FK 
AND (
       ISDATE ( ISNULL ( EFFECTIVE_DATE, '01/01/2001' ) ) = 0 
    OR ISDATE ( ISNULL ( END_DATE, '01/01/2001' )  ) = 0 
    )





UPDATE import.IMPORT_RULES 
SET ERROR_ON_IMPORT_IND = 1
WHERE JOB_FK = @IN_JOB_FK 
AND (
      ISNUMERIC ( ISNULL ( APPLY_TO_ORG_CHILDREN_IND, 0 ) )= 0 
    )
			 
			 
			  INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'BAD DATA IN RULE NUMERIC FIELD'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
           FROM import.IMPORT_RULES IR
WHERE JOB_FK = @IN_JOB_FK 
AND (
      ISNUMERIC ( ISNULL ( APPLY_TO_ORG_CHILDREN_IND, 0 ) )= 0 
    )
			 
			 

			 
 -- ------------------------------------------------------------------------------------
 --			CHECK FOR DUPLICATE RULES WITHIN SAME IMPORT JOB --- add later
 -- -----------------------------------------------------------------------------------

/*

RESULT DATA NAME, PROD, ORG, SUPL, CUST FILTERS ... EFFECTIVE DATE... DRANKING ???

*/



------------------------------------------------------------------------------------------
--   IMPORT_RULES_DATA
--		MISSING FILTER DATA AND VALIDATIONS OF ENTITY
------------------------------------------------------------------------------------------


UPDATE IR
SET SUPL_FILTER_ENTITY_DN = ( SELECT DNX.NAME FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.DATA_NAME_ID = DN.ENTITY_DATA_NAME_FK )
FROM import.IMPORT_RULES IR
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK) ON (DN.NAME = SUPL_FILTER_ASSOC_DN)
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   SUPL_FILTER_ENTITY_DN IS NULL
AND   SUPL_FILTER_DN IN ( SELECT DNX.NAME
                          FROM epacube.DATA_NAME DNX WITH (NOLOCK)
                          INNER JOIN epacube.DATA_SET DSX WITH (NOLOCK) ON DSX.DATA_SET_ID = DNX.DATA_SET_FK 
                          WHERE DSX.ENTITY_CLASS_CR_FK = 10103 -- VENDOR 
                          AND   DSX.TABLE_NAME = 'ENTITY_IDENTIFICATION' )


UPDATE IR
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   SUPL_FILTER_ASSOC_DN IS NULL
AND   SUPL_FILTER_DN IN ( SELECT DNX.NAME
                          FROM epacube.DATA_NAME DNX WITH (NOLOCK)
                          INNER JOIN epacube.DATA_SET DSX WITH (NOLOCK) ON DSX.DATA_SET_ID = DNX.DATA_SET_FK 
                          WHERE DSX.ENTITY_CLASS_CR_FK = 10103 -- VENDOR 
                          AND   DSX.TABLE_NAME = 'ENTITY_IDENTIFICATION' )


 INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING SUPL ASSOC REQUIRED WITH IDENTIFIER'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
WHERE JOB_FK = @IN_JOB_FK 
AND   SUPL_FILTER_ASSOC_DN IS NULL
AND   SUPL_FILTER_DN IN ( SELECT DNX.NAME
                          FROM epacube.DATA_NAME DNX WITH (NOLOCK)
                          INNER JOIN epacube.DATA_SET DSX WITH (NOLOCK) ON DSX.DATA_SET_ID = DNX.DATA_SET_FK 
                          WHERE DSX.ENTITY_CLASS_CR_FK = 10103 -- VENDOR 
                          AND   DSX.TABLE_NAME = 'ENTITY_IDENTIFICATION' )



UPDATE IR
SET ORG_FILTER_ENTITY_DN = ( SELECT DNX.NAME FROM EPACUBE.DATA_NAME DNX WITH (NOLOCK) WHERE DNX.DATA_NAME_ID = DN.ORG_DATA_NAME_FK )
FROM import.IMPORT_RULES IR 
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK) ON (DN.NAME = ORG_FILTER_ASSOC_DN)
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   ORG_FILTER_ENTITY_DN IS NULL
AND   ORG_FILTER_DN IN ( SELECT DNX.NAME
                          FROM epacube.DATA_NAME DNX WITH (NOLOCK)
                          INNER JOIN epacube.DATA_SET DSX WITH (NOLOCK) ON DSX.DATA_SET_ID = DNX.DATA_SET_FK 
                          WHERE DSX.ENTITY_CLASS_CR_FK = 10101 -- ORGANIZATION
                          AND   DSX.TABLE_NAME = 'ENTITY_IDENTIFICATION' )


                                  
------------------------------------------------------------------------------------------
--   IMPORT_RULES_DATA
--		INSERT  ERROR MESSAGE FOR BAD DATA LATER... FOR NOW JUST SET ERROR IND
------------------------------------------------------------------------------------------



--- BASIS CALC DATA NAME NOT VALID OR INACTIVE
UPDATE IR
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IRD.BASIS_CALC_DN NOT IN (
        SELECT DN.NAME
        FROM EPACUBE.DATA_NAME DN 
        WHERE DN.RECORD_STATUS_CR_FK = 1 )


INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'BASIS CALC DATA NAME INVALID OR INACTIVE'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IRD.BASIS_CALC_DN NOT IN (
        SELECT DN.NAME
        FROM EPACUBE.DATA_NAME DN 
        WHERE DN.RECORD_STATUS_CR_FK = 1 )
AND    IRD.BASIS_CALC_DN  <>  'REBATED COST NET AMT'         



--- BASIS1 DATA NAME NOT VALID OR INACTIVE
UPDATE IR
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IRD.BASIS1_DN NOT IN (
        SELECT DN.NAME
        FROM EPACUBE.DATA_NAME DN 
        WHERE DN.RECORD_STATUS_CR_FK = 1 )


INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'BASIS1 DATA NAME INVALID OR INACTIVE'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
           FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IRD.BASIS1_DN NOT IN (
        SELECT DN.NAME
        FROM EPACUBE.DATA_NAME DN 
        WHERE DN.RECORD_STATUS_CR_FK = 1 )



--- BASIS2 DATA NAME NOT VALID OR INACTIVE
UPDATE IR
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IRD.BASIS2_DN NOT IN (
        SELECT DN.NAME
        FROM EPACUBE.DATA_NAME DN 
        WHERE DN.RECORD_STATUS_CR_FK = 1 )
        
        INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'BASIS2 DATA NAME INVALID OR INACTIVE'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IRD.BASIS2_DN NOT IN (
        SELECT DN.NAME
        FROM EPACUBE.DATA_NAME DN 
        WHERE DN.RECORD_STATUS_CR_FK = 1 )



--- BASIS3 DATA NAME NOT VALID OR INACTIVE
UPDATE IR
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IRD.BASIS3_DN NOT IN (
        SELECT DN.NAME
        FROM EPACUBE.DATA_NAME DN 
        WHERE DN.RECORD_STATUS_CR_FK = 1 )
        
        INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'BASIS3 DATA NAME INVALID OR INACTIVE'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
           FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IRD.BASIS3_DN NOT IN (
        SELECT DN.NAME
        FROM EPACUBE.DATA_NAME DN 
        WHERE DN.RECORD_STATUS_CR_FK = 1 )





------    IF ANY BASIS IS SUPL SPECIFIC THEN NEED SUPL_ASSOC


UPDATE IR
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IR.SUPL_FILTER_ASSOC_DN IS NULL
AND   ( 
        ( BASIS1_DN IN (  SELECT DNX.NAME
                          FROM epacube.DATA_NAME DNX WITH (NOLOCK)
                          INNER JOIN epacube.DATA_SET DSX WITH (NOLOCK) ON DSX.DATA_SET_ID = DNX.DATA_SET_FK 
                          WHERE DSX.ENTITY_STRUCTURE_EC_CR_FK = 10103 ) ) -- VENDOR 
       OR
        ( BASIS2_DN IN (  SELECT DNX.NAME
                          FROM epacube.DATA_NAME DNX WITH (NOLOCK)
                          INNER JOIN epacube.DATA_SET DSX WITH (NOLOCK) ON DSX.DATA_SET_ID = DNX.DATA_SET_FK 
                          WHERE DSX.ENTITY_STRUCTURE_EC_CR_FK = 10103 ) ) -- VENDOR 
       OR
        ( BASIS3_DN IN (  SELECT DNX.NAME
                          FROM epacube.DATA_NAME DNX WITH (NOLOCK)
                          INNER JOIN epacube.DATA_SET DSX WITH (NOLOCK) ON DSX.DATA_SET_ID = DNX.DATA_SET_FK 
                          WHERE DSX.ENTITY_STRUCTURE_EC_CR_FK = 10103  ) ) -- VENDOR 
       )
       
       INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING SUPL ASSOC REQUIRED WITH SUPPLIER BASIS'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
       FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IR.SUPL_FILTER_ASSOC_DN IS NULL
AND   ( 
        ( BASIS1_DN IN (  SELECT DNX.NAME
                          FROM epacube.DATA_NAME DNX WITH (NOLOCK)
                          INNER JOIN epacube.DATA_SET DSX WITH (NOLOCK) ON DSX.DATA_SET_ID = DNX.DATA_SET_FK 
                          WHERE DSX.ENTITY_STRUCTURE_EC_CR_FK = 10103 ) ) -- VENDOR 
       OR
        ( BASIS2_DN IN (  SELECT DNX.NAME
                          FROM epacube.DATA_NAME DNX WITH (NOLOCK)
                          INNER JOIN epacube.DATA_SET DSX WITH (NOLOCK) ON DSX.DATA_SET_ID = DNX.DATA_SET_FK 
                          WHERE DSX.ENTITY_STRUCTURE_EC_CR_FK = 10103 ) ) -- VENDOR 
       OR
        ( BASIS3_DN IN (  SELECT DNX.NAME
                          FROM epacube.DATA_NAME DNX WITH (NOLOCK)
                          INNER JOIN epacube.DATA_SET DSX WITH (NOLOCK) ON DSX.DATA_SET_ID = DNX.DATA_SET_FK 
                          WHERE DSX.ENTITY_STRUCTURE_EC_CR_FK = 10103  ) ) -- VENDOR 
       )




--- OPERATION1 REQUIRED
UPDATE IR
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IRD.RULES_ACTION_OPERATION1 IS NULL 


INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING OPERATION1'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IRD.RULES_ACTION_OPERATION1 IS NULL 

------------------------------------
--- OPERATION1 IS VALID NAME
-------------------------------------
UPDATE IR
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IRD.RULES_ACTION_OPERATION1 NOT IN 
(SELECT distinct Name from synchronizer.RULES_ACTION_OPERATION) 


INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'INVALID OPERATION1 NAME'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND   IRD.RULES_ACTION_OPERATION1 NOT IN 
(SELECT distinct Name from synchronizer.RULES_ACTION_OPERATION) 


--- REQUIRED DATA BY OPERATION LATER.. IN MORE DETAIL
----  ????
		
---- STRIP ANY COMMAS FROM NUMBERS 

UPDATE IRD
SET BASIS1_NUMBER = REPLACE ( BASIS1_NUMBER,',','' )
   ,BASIS2_NUMBER = REPLACE ( BASIS2_NUMBER,',','' )
   ,BASIS3_NUMBER = REPLACE ( BASIS3_NUMBER,',','' )
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK =  @IN_JOB_FK 


---  DATA TYPE CHECKS 

UPDATE IR
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND (
      ISNUMERIC ( ISNULL ( BASIS1_NUMBER, 0 ) )= 0 
   OR ISNUMERIC ( ISNULL ( BASIS2_NUMBER, 0 ) )= 0 
   OR ISNUMERIC ( ISNULL ( BASIS3_NUMBER, 0 ) )= 0 
   OR ISNUMERIC ( ISNULL ( ROUNDING_TO_VALUE, 0 ) )= 0 
   OR ISNUMERIC ( ISNULL ( ROUNDING_ADDER_VALUE, 0 ) )= 0 
    )


INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'BAD DATA IN BASIS NUMBER OR ROUNDING FIELD'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND (
      ISNUMERIC ( ISNULL ( BASIS1_NUMBER, 0 ) )= 0 
   OR ISNUMERIC ( ISNULL ( BASIS2_NUMBER, 0 ) )= 0 
   OR ISNUMERIC ( ISNULL ( BASIS3_NUMBER, 0 ) )= 0 
   OR ISNUMERIC ( ISNULL ( ROUNDING_TO_VALUE, 0 ) )= 0 
   OR ISNUMERIC ( ISNULL ( ROUNDING_ADDER_VALUE, 0 ) )= 0 
    )




UPDATE IR
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND (
      ISNUMERIC ( ISNULL ( OPERAND_1_1, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_2, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_3, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_4, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_5, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_7, 0 )  )= 0    
   OR ISNUMERIC ( ISNULL ( OPERAND_1_8, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_9, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_10, 0 ) )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_1, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_2, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_3, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_4, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_5, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_7, 0 )  )= 0    
   OR ISNUMERIC ( ISNULL ( OPERAND_2_8, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_9, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_10, 0 ) )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_1, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_2, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_3, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_4, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_5, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_7, 0 )  )= 0    
   OR ISNUMERIC ( ISNULL ( OPERAND_3_8, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_9, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_10, 0 ) )= 0 
    )
    
    
    INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'BAD DATA IN OPERAND FIELD'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
           FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND (
      ISNUMERIC ( ISNULL ( OPERAND_1_1, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_2, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_3, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_4, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_5, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_7, 0 )  )= 0    
   OR ISNUMERIC ( ISNULL ( OPERAND_1_8, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_9, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_1_10, 0 ) )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_1, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_2, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_3, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_4, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_5, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_7, 0 )  )= 0    
   OR ISNUMERIC ( ISNULL ( OPERAND_2_8, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_9, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_2_10, 0 ) )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_1, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_2, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_3, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_4, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_5, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_7, 0 )  )= 0    
   OR ISNUMERIC ( ISNULL ( OPERAND_3_8, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_9, 0 )  )= 0 
   OR ISNUMERIC ( ISNULL ( OPERAND_3_10, 0 ) )= 0 
    )





---  DATA TYPE CHECKS 

UPDATE IR
SET ERROR_ON_IMPORT_IND = 1
FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND (
      ISNUMERIC (  ISNULL ( SUBSTR_START_POS_1, 0 ) )= 0 
   OR ISNUMERIC (  ISNULL ( SUBSTR_LENGTH_1, 0 ) )= 0 
   OR ISNUMERIC (  ISNULL ( SUBSTR_START_POS_2, 0 ) )= 0 
   OR ISNUMERIC (  ISNULL ( SUBSTR_LENGTH_2, 0 ) )= 0 
   OR ISNUMERIC (  ISNULL ( SUBSTR_START_POS_3, 0 ) )= 0 
   OR ISNUMERIC (  ISNULL ( SUBSTR_LENGTH_3, 0 ) )= 0 
    )

 INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'BAD DATA IN SUBSTRING FIELD'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.PROD_FILTER_DN
           ,GETDATE ()
           FROM import.IMPORT_RULES IR
INNER JOIN import.IMPORT_RULES_DATA IRD
 ON ( IR.JOB_FK = IRD.JOB_FK
 AND  IR.RECORD_NO = IRD.RECORD_NO )
WHERE IR.JOB_FK = @IN_JOB_FK 
AND (
      ISNUMERIC (  ISNULL ( SUBSTR_START_POS_1, 0 ) )= 0 
   OR ISNUMERIC (  ISNULL ( SUBSTR_LENGTH_1, 0 ) )= 0 
   OR ISNUMERIC (  ISNULL ( SUBSTR_START_POS_2, 0 ) )= 0 
   OR ISNUMERIC (  ISNULL ( SUBSTR_LENGTH_2, 0 ) )= 0 
   OR ISNUMERIC (  ISNULL ( SUBSTR_START_POS_3, 0 ) )= 0 
   OR ISNUMERIC (  ISNULL ( SUBSTR_LENGTH_3, 0 ) )= 0 
    )

-------------------------------------------------------------------------------------
--Check for duplicate unique key 1 in import data
-------------------------------------------------------------------------------------

update import.IMPORT_RULES
set ERROR_ON_IMPORT_IND = 1
where UNIQUE_KEY1 in (
select a.UNIQUE_KEY1 from (

select UNIQUE_KEY1, COUNT(1) how
      from import.import_rules ir with (nolock)
      where ir.UNIQUE_KEY1 is not NULL
      group by  UNIQUE_KEY1 having COUNT(1) > 1) a)
 


INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IR.JOB_FK
            ,'IMPORT RULES'
            ,IR.IMPORT_RULES_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'DUPLICATE UNIQUE KEY1 IN IMPORT DATA'
           ,IR.RULE_NAME
           ,IR.RULES_SCHEDULE_NAME
           ,IR.RESULT_DATA_NAME
           ,IR.UNIQUE_KEY1
           ,GETDATE ()
           FROM import.IMPORT_RULES IR

WHERE IR.JOB_FK = @IN_JOB_FK AND UNIQUE_KEY1 in (
select a.UNIQUE_KEY1 from (
select UNIQUE_KEY1, COUNT(1) how
      from import.import_rules ir with (nolock)
       where ir.UNIQUE_KEY1 is not NULL
      group by  UNIQUE_KEY1 having COUNT(1) > 1) a
)






------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.IMPORT_RULES_CLEANSER'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk,  @epa_batch_no = @in_job_Fk
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.IMPORT_RULES_CLEANSER has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END





















