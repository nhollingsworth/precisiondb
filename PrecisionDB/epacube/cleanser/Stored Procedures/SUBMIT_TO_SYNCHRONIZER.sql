﻿












-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: For Stg Records submitted to Synhcronizer
--			Create Events, Derived Events, 
--			Perform Event Rules, Validations, and Posting
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/28/2008   Initial SQL Version
-- CV        06/10/2009   Added logging at end
-- CV        08/31/2009   Enhance logging of start and end date
-- RG		 10/10/2009	  Setting Cleansed_Indicator on Discard items (EPA-2595)
-- CV        11/11/2009   Added tighter where clause when updating cleansed indicator. EPA - 2659
-- KS        02/28/2010   Added alternate Import Path to allow minimal Event Creation for performance
-- CV        04/16/2010   Epa - 2869 UPC Length check for new products error check
-- KS        02/13/2011   KS FIXED PERFORMANCE FROM EPA-2595 FROM ABOVE
-- KS        12/23/2011   Correction to Cindy's NCE PI Primary Id change see below
-- CV        05/15/2012   do not compare value in product ident insert 
-- CV        04/01/2013   Moved call for Derivation and Default rules here.
-- CV        06/04/2013   Added to call execute cednetcataglog for records from cleanser.
-- CV        09/20/2013   Added tighter control on running cednetcatalog if and 
-- CV        04/09/2014   Add new procedure call to use table to look up any special procedures to run.


CREATE PROCEDURE [cleanser].[SUBMIT_TO_SYNCHRONIZER] 
       @IN_JOB_FK BIGINT
      ,@IN_BATCH_NO BIGINT
      ,@IN_UPDATE_USER VARCHAR(64) = NULL
AS
BEGIN



DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          BIGINT
DECLARE @v_count             bigint
DECLARE @v_search_activity	 INT
DECLARE @v_entity_class_cr_fk INT 
DECLARE @epa_job_id			  INT
DECLARE @DiscardCodeRefId	  INT
DECLARE @V_import_package_fk  int

DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @V_START_TIMESTAMP    datetime
DECLARE @V_END_TIMESTAMP      datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int


set @DiscardCodeRefId = epacube.getCodeRefId('ACTIVITY_ACTION','DISCARD')




BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;

EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.SUBMIT_TO_SYNCHRONIZER', 
												@epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no,
												@exec_id = @l_exec_no OUTPUT




UPDATE STAGE.STG_RECORD
SET BATCH_NO = @IN_BATCH_NO
WHERE STG_RECORD_ID IN (
  SELECT BEST_SEARCH_FK
  FROM cleanser.SEARCH_JOB_RESULTS_BEST WITH (NOLOCK)
  WHERE SEARCH_JOB_FK = @IN_JOB_FK
  AND   BATCH_NO = @IN_BATCH_NO  )



SET @v_entity_class_cr_fk = ( SELECT TOP 1 entity_class_cr_fk
                              FROM stage.STG_RECORD srec WITH (NOLOCK)
                              WHERE srec.batch_no = @in_batch_no )
                              


------------------------------------------------------------------------------------------
--   Insert Matched Rows into Stage.STG_RECORD_STRUCTURE
------------------------------------------------------------------------------------------

INSERT INTO [stage].[STG_RECORD_STRUCTURE]
           ([STG_RECORD_FK]
           ,[JOB_FK]
           ,[SEARCH_STRUCTURE_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
SELECT 
       [TO_SEARCH_FK]
      ,[SEARCH_JOB_FK]
      ,[FROM_SEARCH_FK]
      ,getdate()
      ,getdate()
      ,@IN_UPDATE_USER
  FROM [cleanser].[SEARCH_JOB_FROM_TO_MATCH] SJFTM WITH (NOLOCK)
  INNER JOIN STAGE.STG_RECORD SREC WITH (NOLOCK)
    ON ( SREC.STG_RECORD_ID = SJFTM.TO_SEARCH_FK )
  WHERE SREC.JOB_FK = @IN_JOB_FK
  AND   SREC.BATCH_NO = @IN_BATCH_NO
  AND   SREC.EVENT_CONDITION_CR_FK <> 99
  AND   SREC.ACTIVITY_ACTION_CR_FK IN ( 147, 144 )   --- FUTURE MERGE ( 145 ), INACTIVATE ( 143 )
   

------------------------------------------------------------------------------------------
--   Update Matched Products Set Import Job and Date in epaCUBE
------------------------------------------------------------------------------------------

IF @v_entity_class_cr_fk = (select code_ref_id from epacube.code_ref 
							where code_type = 'ENTITY_CLASS'
							and code = 'PRODUCT')
BEGIN
		
SET @v_count = (  SELECT COUNT(1)
				  FROM [stage].[STG_RECORD] SREC  WITH (NOLOCK)
				  WHERE SREC.JOB_FK = @IN_JOB_FK
				  AND   SREC.BATCH_NO = @IN_BATCH_NO
				  AND   SREC.EVENT_CONDITION_CR_FK <> 99      
				  AND   SREC.ACTIVITY_ACTION_CR_FK = 144
				  AND   NOT EXISTS
					   ( SELECT 1
						 FROM EPACUBE.PRODUCT_STRUCTURE PS WITH (NOLOCK)
						 WHERE PS.JOB_FK = SREC.JOB_FK
						 AND   PS.IMPORT_FILENAME = SREC.IMPORT_FILENAME
						 AND   PS.RECORD_NO = SREC.RECORD_NO 
						 AND   PS.DATA_NAME_FK = SREC.ENTITY_DATA_NAME_FK ) )

IF @v_count > 0	
	EXEC cleanser.NEW_PRODUCT_STRUCTURE    @IN_JOB_FK, @IN_BATCH_NO


    UPDATE epacube.product_structure
    SET last_import_filename = srec.import_filename
       ,last_import_timestamp = getdate()
       ,last_import_job_fk    = srec.job_fk
    FROM stage.STG_RECORD srec WITH (NOLOCK)
    INNER JOIN stage.STG_RECORD_STRUCTURE srecs WITH (NOLOCK)
      ON ( srecs.STG_RECORD_FK = srec.STG_RECORD_ID )
    WHERE srec.batch_no = @in_batch_no
    AND   srecs.search_structure_fk = epacube.product_structure.product_structure_id

END   -- entity class = product
ELSE 
BEGIN

	EXEC cleanser.NEW_ENTITY_STRUCTURE    @IN_JOB_FK, @IN_BATCH_NO

    UPDATE epacube.entity_structure
    SET last_import_filename = srec.import_filename
       ,last_import_timestamp = getdate()
       ,last_import_job_fk    = srec.job_fk
    FROM stage.STG_RECORD srec WITH (NOLOCK)
    INNER JOIN stage.STG_RECORD_STRUCTURE srecs WITH (NOLOCK)
      ON ( srecs.STG_RECORD_FK = srec.STG_RECORD_ID )
    WHERE srec.batch_no = @in_batch_no
    AND   srecs.search_structure_fk = epacube.entity_structure.entity_structure_id

END 


-------------------------------------------------------------------------------------
--   Update STG_RECORD rows as Synchronized
-------------------------------------------------------------------------------------

     UPDATE cleanser.SEARCH_JOB_RESULTS_BEST
     SET CLEANSED_INDICATOR = 1  -- SYNCHRONIZED
     WHERE 1 = 1	
     AND SEARCH_JOB_FK  = @IN_JOB_FK
     AND BATCH_NO = @IN_BATCH_NO 
     AND ACTIVITY_ACTION_CR_FK IN ( 144, 147 )
     AND FROM_SEARCH_FK IN (
				SELECT SRECS.SEARCH_STRUCTURE_FK
				FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
				INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS  WITH (NOLOCK)  
				ON ( SRECS.STG_RECORD_FK = SREC.STG_RECORD_ID )
				WHERE SREC.BATCH_NO = @IN_BATCH_NO
				AND   SREC.EVENT_CONDITION_CR_FK <> 99
                AND   SREC.ACTIVITY_ACTION_CR_FK IN ( 144, 147 )   --- FUTURE MERGE ( 145 ), INACTIVATE ( 143 )				
			  )	


-----------CINDY ADDED TO SET THE CLEANSED INDICATOR FOR "NEW" PRODUCTS

--------		UPDATE cleanser.SEARCH_JOB_RESULTS_BEST
--------		SET CLEANSED_INDICATOR = 1  
--------		WHERE 1 = 1
--------		AND SEARCH_JOB_FK = @IN_JOB_FK	
--------		AND BATCH_NO = @IN_BATCH_NO
--------        AND ACTIVITY_ACTION_CR_FK = 144  -- ADD	
--------		AND BEST_SEARCH_FK not in (select stg_record_fk 
--------							from stage.STG_RECORD_ERRORS
--------							where job_fk = @IN_JOB_FK)  ---epa 2869

------ KS 02/13/2011
------
------		CINDY REPLACED SQL ABOVE WITH BELOW.... 
------          THE PERFORMANCE WAS HORRIBLE.. NEVER FINISHED AFTER 24 HOURS ON THIS SINGLE STATEMENT
------			VERY IMPORTANT.. WHEN DOING LARGE SETS AVOID NOT IN !!!  
------

UPDATE cleanser.SEARCH_JOB_RESULTS_BEST
SET CLEANSED_INDICATOR = 1
WHERE SEARCH_JOB_RESULTS_BEST_ID IN (
		SELECT SJRB.SEARCH_JOB_RESULTS_BEST_ID
		FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH(NOLOCK)
		LEFT JOIN stage.STG_RECORD_ERRORS SRECE WITH (NOLOCK)
		 ON ( SJRB.BEST_SEARCH_FK = SRECE.STG_RECORD_FK )
		WHERE 1 = 1
		AND   SRECE.STG_RECORD_ERRORS_ID IS NULL
		AND   SJRB.SEARCH_JOB_FK = @IN_JOB_FK	
		AND   SJRB.BATCH_NO = @IN_BATCH_NO
		AND   SJRB.ACTIVITY_ACTION_CR_FK = 144  -- ADD	
)





-- RG EPA-2595 setting items flagged Discard to cleansed
		UPDATE cleanser.SEARCH_JOB_RESULTS_BEST
		SET CLEANSED_INDICATOR = 1  
		WHERE 1 = 1 
		AND SEARCH_JOB_FK = @IN_JOB_FK	
		AND BATCH_NO = @IN_BATCH_NO
        AND ACTIVITY_ACTION_CR_FK = @DiscardCodeRefId 

-----------------------------------------------------------------------------------
--   Check for Path of Event Logging
-------------------------------------------------------------------------------------

---- TEMPORARY....   SET ICSP, ICSW AND ICSC JOBS TO EVENT_LOGGING_PARTIAL
----
----				 AFTER TESTING COMPLETE... ALL JOBS WILL WORK THIS WAY


--SET @V_import_package_fk  = ( SELECT TOP 1 ISNULL ( SREC.IMPORT_PACKAGE_FK, 0 )
--							  FROM STAGE.STG_RECORD SREC WITH (NOLOCK) 
--							  WHERE SREC.JOB_FK = @IN_JOB_FK
--							  AND   SREC.BATCH_NO = @IN_BATCH_NO  )
--
--
--IF @V_import_package_fk IN ( 241000, 242000  )  --- CUSTOMER IMPORTS... USE OLD PATH FOR NOW
--BEGIN  
--
-------------------------------------------------------------------------------------
----   Call Event Factory(s)
---------------------------------------------------------------------------------------
--		SET @V_START_TIMESTAMP = GETDATE()
--     
--	 EXEC synchronizer.EVENT_FACTORY @in_batch_no
--
--       SELECT @v_input_rows = COUNT(1)
--        FROM import.import_Record WITH (NOLOCK)
--       WHERE job_fk = @in_job_fk;
--
--      SELECT @v_output_rows = COUNT(1)
--        FROM stage.STG_Record WITH (NOLOCK)
--       WHERE job_fk = @in_job_fk;
--
--	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
--      SET @v_end_timestamp = getdate()
--      EXEC job_execution_create  @in_job_fk, 'CREATING EVENTS',
--		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
--										 201, @v_start_timestamp, @v_end_timestamp;
----	  SET @v_sysdate = GETDATE();
--


-------------------------------------------------------------------------------------
--   Perform EVENT POSTING
--			Validate and Post 
-------------------------------------------------------------------------------------
--    SET @V_START_TIMESTAMP = getdate()
--
--    EXEC SYNCHRONIZER.EVENT_POSTING  @in_batch_no
--
--
--END   -- @V_import_package_fk IN ( 241000, 242000  )  
--ELSE
--BEGIN



----------------------------------------------------------------------------------------
--NEW PRIMARY ID insert
--
-- KS ADDED CORRECTIONS TO THE FOLLOWING LOGIC BELOW.... 12/23/2011
--     ISSUES WITH THE LOGIC BELOW 
--       (1) THIS CHECK SHOULD BE FOR CHANGES ONLY... NOT ALL
--       (2) INSERT SHOULD BE LIMITED TO PRIMARY DATA NAMES HOST IMPORT ONLY
--       (3) ONLY IF NO PRIMARY MATCHING IS ACTIVE 
--  ADD Paramater to only use if needed
   ---- Insert missing Primary identifier if ADD_UNIQUE_IDENT_ON_CHANGE = 1
----------------------------------------------------------------------------------------
DECLARE @v_insert_product_identification int

set @v_insert_product_identification = (select value from epacube.epacube_params where name = 'ADD_UNIQUE_IDENT_ON_CHANGE'
and value = 1)

if @v_insert_product_identification = 1
begin

INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
,STG_RECORD_IDENT_FK
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT 
           SREC.STG_RECORD_ID
,SRECI.STG_RECORD_IDENT_ID
          ,SREC.JOB_FK
          ,'Different value already exists for this data name'          
           ,SRECI.ID_data_Name_FK
          ,pi.VALUE
          ,SRECI.ID_value
          ,NULL
          ,98    --EVENT_CONDITION_CR_FK
          ,16335
          ,GETDATE()
FROM STAGE.STG_RECORD SREC WITH (NOLOCK) 
              INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
                    ON (  SRECS.JOB_FK          = SREC.JOB_FK
                      AND SRECS.stg_RECORD_fK       = SREC.STG_RECORD_ID )      
            INNER JOIN epacube.PRODUCT_STRUCTURE  PS WITH (NOLOCK)
                ON ( PS.PRODUCT_STRUCTURE_ID = SRECS.SEARCH_STRUCTURE_FK ) ---<< MAKE SURE A PS RECORD EXISTS    
                 INNER JOIN epacube.PRODUCT_IDENTIFICATION PI
                on (pi.PRODUCT_STRUCTURE_FK = ps.PRODUCT_STRUCTURE_ID)
              INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
                  ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID
                  and pi.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
          AND ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL')
              INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
              ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )              
              INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
                 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
              WHERE SREC.JOB_FK = @IN_JOB_FK
              AND   SREC.BATCH_NO = @IN_BATCH_NO
              AND   DS.TABLE_NAME = 'PRODUCT_IDENTIFICATION'
              AND   SREC.ACTIVITY_ACTION_CR_FK = 147          ----<<<<  DO NOT INCLUDE ADD(S)
              AND   SREC.EVENT_ACTION_CR_FK = 52              ----<<<<  CHANGES ONLY
              ---- KS 12/23/2011  ADDED DATA NAME PRIMARY IMPORT ID LOOKUP HOST IMPORT ONLY
              AND   DN.DATA_NAME_ID IN  (       SELECT IMM.IDENT_DATA_NAME_FK
                                                            FROM import.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
                                                            INNER JOIN import.IMPORT_PACKAGE IP WITH (NOLOCK)
                                                            ON ( IP.IMPORT_PACKAGE_ID = IMM.IMPORT_PACKAGE_FK 
                                                             AND  IP.ENTITY_CLASS_CR_FK = 10109  ---<<< PRODUCTS ONLY
                                                            AND  IP.EVENT_SOURCE_CR_FK = 70 )   ---<<<  HOST IMPORT
                                                            WHERE   IMM.IMPORT_PACKAGE_FK = SREC.IMPORT_PACKAGE_FK 
                                                            AND     IMM.COLUMN_NAME = SREC.ENTITY_ID_COLUMN_NAME )                      
             
            AND  EXISTS
                  ( SELECT 1   --- Primary Identiifier Does not already exist 
                    FROM EPACUBE.PRODUCT_IDENTIFICATION PI WITH (NOLOCK)
                    WHERE PI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
                     AND   PI.VALUE <> SRECI.ID_VALUE 
                    and PI.PRODUCT_STRUCTURE_fk = SRECS.SEARCH_STRUCTURE_FK      
                  )
            -----  KS ADDED ONLY IF NO PRIMARY MATCHING IS ACTIVE 
              --AND   (
              --    ( SELECT COUNT(1)
              --      FROM cleanser.SEARCH_ACTIVITY_IDENT_MATCH_RULE WITH (NOLOCK)
              --      WHERE RECORD_STATUS_CR_FK = 1
              --      AND   SEARCH_ACTIVITY_FK = 1
              --      AND   MATCH_TYPE_CR_FK = 171  )  
              --      = 0 ) 
                -------------------------------    
                    


INSERT INTO [epacube].[PRODUCT_IDENTIFICATION]
			   ([PRODUCT_STRUCTURE_FK]
			   ,[DATA_NAME_FK]
			   ,[VALUE]
			   ,[ORG_ENTITY_STRUCTURE_FK]			   			   			   
			   ,[ENTITY_STRUCTURE_FK]	
			   ,[JOB_FK]		   
			   )
	SELECT DISTINCT
			    SRECS.SEARCH_STRUCTURE_FK
			   ,SRECI.ID_DATA_NAME_FK
			   ,SRECI.ID_VALUE
			   ,SRECI.ORG_ENTITY_STRUCTURE_FK			   			   
			   ,SRECI.ID_ENTITY_STRUCTURE_FK
			   ,SREC.JOB_FK			   			   
		  FROM STAGE.STG_RECORD SREC WITH (NOLOCK) 
		  INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
			  ON (  SRECS.JOB_FK          = SREC.JOB_FK
			    AND SRECS.stg_RECORD_fK       = SREC.STG_RECORD_ID )	
	      INNER JOIN epacube.PRODUCT_STRUCTURE  PS WITH (NOLOCK)
	          ON ( PS.PRODUCT_STRUCTURE_ID = SRECS.SEARCH_STRUCTURE_FK ) ---<< MAKE SURE A PS RECORD EXISTS	  
		  INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
			 ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID
          AND ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL')
		  INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
              ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )		  
		  INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
		     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		  WHERE SREC.JOB_FK = @IN_JOB_FK
		  AND   SREC.BATCH_NO = @IN_BATCH_NO
		  AND   DS.TABLE_NAME = 'PRODUCT_IDENTIFICATION'
		  AND   SREC.ACTIVITY_ACTION_CR_FK = 147          ----<<<<  DO NOT INCLUDE ADD(S)
		  AND   SREC.EVENT_ACTION_CR_FK = 52              ----<<<<  CHANGES ONLY
		  ---- KS 12/23/2011  ADDED DATA NAME PRIMARY IMPORT ID LOOKUP HOST IMPORT ONLY
		  AND   DN.DATA_NAME_ID IN  ( 	SELECT IMM.IDENT_DATA_NAME_FK
										FROM import.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
										INNER JOIN import.IMPORT_PACKAGE IP WITH (NOLOCK)
										 ON ( IP.IMPORT_PACKAGE_ID = IMM.IMPORT_PACKAGE_FK 
										 AND  IP.ENTITY_CLASS_CR_FK = 10109  ---<<< PRODUCTS ONLY
										 --AND  IP.EVENT_SOURCE_CR_FK = 79 )   ---<<<  HOST IMPORT 
										 AND  IP.EVENT_SOURCE_CR_FK = 70 )   ---<<<  IMPORT

										WHERE   IMM.IMPORT_PACKAGE_FK = SREC.IMPORT_PACKAGE_FK 
										AND     IMM.COLUMN_NAME = SREC.ENTITY_ID_COLUMN_NAME )		  		  
		  AND   NOT EXISTS
	            ( SELECT 1 
	              FROM STAGE.STG_RECORD_ERRORS SRE WITH (NOLOCK)
	              WHERE SRE.STG_RECORD_IDENT_FK =  SRECI.STG_RECORD_IDENT_ID)
	      AND   NOT EXISTS
	            ( SELECT 1   --- Primary Identiifier Does not already exist 
	              FROM EPACUBE.PRODUCT_IDENTIFICATION PI WITH (NOLOCK)
	              WHERE PI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
	              --AND   PI.VALUE = SRECI.ID_VALUE     
				    --AND   PI.VALUE <> SRECI.ID_VALUE 
                    and PI.PRODUCT_STRUCTURE_fk = SRECS.SEARCH_STRUCTURE_FK 
	            )
	      -----  KS ADDED ONLY IF NO PRIMARY MATCHING IS ACTIVE 
		  --AND   (
	   --         ( SELECT COUNT(1)
	   --           FROM cleanser.SEARCH_ACTIVITY_IDENT_MATCH_RULE WITH (NOLOCK)
	   --           WHERE RECORD_STATUS_CR_FK = 1
	   --           AND   SEARCH_ACTIVITY_FK = 1
	   --           AND   MATCH_TYPE_CR_FK = 171  )  
	   --           = 0 ) 


end

-------------------------------------------------------------------------------------------
--------   Perform EVENT POSTING IMPORT
--------			Validate and Post 
-------------------------------------------------------------------------------------------


SET @V_START_TIMESTAMP = getdate()

EXEC SYNCHRONIZER.EVENT_POSTING_IMPORT  @in_batch_no


SELECT @v_input_rows = COUNT(1)
FROM import.import_Record WITH (NOLOCK)
WHERE job_fk = @in_job_fk;

SELECT @v_output_rows = COUNT(1)
FROM stage.STG_Record WITH (NOLOCK)
WHERE job_fk = @in_job_fk;

SET @v_exception_rows = @v_input_rows - @v_output_rows;
SET @V_END_TIMESTAMP = getdate()
EXEC common.job_execution_create  @in_job_fk, 'EVENT POSTING IMPORT COMPLETE',
                                 @v_input_rows, @v_output_rows, @v_exception_rows,
								 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

-----------------------------------------------------------------------
---added so derivations would work correctly
-----------------------------------------------------------------------
			EXEC synchronizer.EVENT_POST_DERIVATION @in_batch_no

			EXEC synchronizer.EVENT_POSTING @in_batch_no

-------------------------------------------------------------------------
---added to clean up entity change
------------------------------------------------------------------------
----URM --- don't want to run this
----EXECUTE [epacube].[UTIL_CLEANUP_AFTER_ENTITY_CHANGE] 

-------------------------------------------------------------------------
---Adding a call so Table can be used and special procedures called after import is complete
---------------------------------------------------------------------------
DECLARE @Run_special int

SET @Run_special =(
select count(1) from epacube.util_Special_sql where record_status_Cr_fk = 1 
and IMPORT_PACKAGE_FK  = (select top 1 import_package_fk from 
import.import_record_data where job_fk = @IN_JOB_FK))

if @Run_special > 0
BEGIN

EXEC	[epacube].[EXECUTE_UTIL_SPECIAL_SQL]
		@IN_JOB_FK 

END




---added so after cleanser this can happen.  need to remove from ssis package.
--declare @customer_name varchar(50)

--set @customer_name = (select value from epacube.EPACUBE_PARAMS where name = 'EPACUBE CUSTOMER')

--IF   @customer_name = 'CED' AND  @IN_JOB_FK in (select distinct  job_fk from import.IMPORT_RECORD_DATA where IMPORT_PACKAGE_FK = 802000)

-- BEGIN

--EXECUTE  [epacube].[PAXSON_CREATE_CEDNETCATALOG] 
--   @IN_JOB_FK

--  END

--END

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

	  SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'IMPORT COMPLETE2',
		                                 NULL, NULL, NULL,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


SET @status_desc = 'finished execution of cleanser.SUBMIT_TO_SYNCHRONIZER'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id;
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.SUBMIT_TO_SYNCHRONIZER has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


       EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END










