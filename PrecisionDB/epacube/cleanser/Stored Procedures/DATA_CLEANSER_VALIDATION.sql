﻿















-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: For Stg Records submitted to Synhcronizer
--			Create any missing entities associated with them
--			Insert all Identifications also associated with them
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/28/2008   Initial SQL Version
-- CV        09/01/2009   Attempt to remove ORG DATA NAME FK
-- CV        09/21/2009   Added to update activity action for add new products Epa 2561
-- CV        12/09/2009   Added stg_record_ident_fk to error table. epa 2724
-- KS        02/25/2010   Moved Identification column size validation here instead of New Product
-- CV        04/16/2010   Epa - 2869 needed to add yellow condition to also make unresolved.
-- CV        05/14/2010   EPA - needed to check idents to make sure entity is not required.
-- RG		 06/01/2010	  EPA-2731 Updating multiple imports matching to single epacube error message 
-- KS		 06/02/2010	  Changed Error Messages for Required Entity and Warehouse for an Attribute
--							to be ONLY one per stg_record instead of for EVERY attribute in metadata
-- KS		 07/29/2010	  Added check for Bad Data.. ISNUMERIC, ISDATE
-- CV        09/10/2010   Added validation for VPN jira 3055
-- CV	     05/27/2011   Add call to Import NULLING data
-- CV        07/20/2011   Do not check for warehouse/terrirory if required enity creation
-- CV        09/27/2011   added to not update activity action cr fk when = 141




CREATE PROCEDURE [cleanser].[DATA_CLEANSER_VALIDATION] 
       @IN_JOB_FK         BIGINT ,
       @IN_BATCH_NO       BIGINT
AS
BEGIN



DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint
DECLARE @v_search_activity	 INT
DECLARE @v_import_package_fk BIGINT 
DECLARE @epa_job_id			 INT
DECLARE @v_filename_count INT

DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int




BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.DATA_CLEANSER_VALIDATION', 
                                                @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no,
												@exec_id = @l_exec_no OUTPUT;
												
SET @l_sysdate = GETDATE()

SET @v_import_package_fk = ( SELECT TOP 1 IMPORT_PACKAGE_FK
                             FROM STAGE.STG_RECORD 
                             WHERE JOB_FK = @IN_JOB_FK  )


 -- ------------------------------------------------------------------------------------
 --			CHECK FOR DUPLICATE UNIQUE IDENTS WITHIN SAME IMPORT JOB
 --			IF THEY EXIST SET STG_RECORD TO RED 
 --			NOTE:  THIS VALIDATION HAS TO CHECK ENTIRE IMPORT JOB
 -- -----------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_BATCH_DUPS') is not null
	   drop table #TS_BATCH_DUPS
	   
	   
	-- create temp table
	CREATE TABLE #TS_BATCH_DUPS(
	    DATA_NAME_FK             INT NULL,
	    ID_ORG_VALUE             varchar(128)  NULL,
	    ID_ENTITY_VALUE          varchar(128)  NULL,
		ID_VALUE		         varchar(128)  NULL
		 )


    INSERT INTO #TS_BATCH_DUPS
     (  DATA_NAME_FK
       ,ID_ORG_VALUE
       ,ID_ENTITY_VALUE
       ,ID_VALUE )
    SELECT 
           SRECI.ID_DATA_NAME_FK 
          ,SRECI.ID_ORG_VALUE
          ,SRECI.ID_ENTITY_VALUE
          ,SRECI.ID_VALUE              
	FROM stage.STG_RECORD SREC WITH (NOLOCK)
	INNER JOIN stage.STG_RECORD_IDENT SRECI WITH (NOLOCK)
	   ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID 
		AND (ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL'
		AND  SRECI.ID_VALUE <> '-999999999'))
	INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
	   ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
	INNER JOIN epacube.DATA_SET DS WITH (NOLOCK)
	   ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )      
	WHERE SREC.JOB_FK = @IN_JOB_FK
	AND   DS.TABLE_NAME IN ( 'PRODUCT_IDENTIFICATION', 'PRODUCT_IDENT_MULT', 'ENTITY_IDENTIFICATION', 'ENTITY_IDENT_MULT' )
    GROUP BY SRECI.ID_DATA_NAME_FK, SRECI.ID_ORG_VALUE, SRECI.ID_ENTITY_VALUE, SRECI.ID_VALUE
    HAVING COUNT(*) > 1
              

--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------


	CREATE NONCLUSTERED INDEX IDX_TSBD_DNF_OESF_ESF_IND ON #TS_BATCH_DUPS
	(   DATA_NAME_FK	ASC
       ,ID_ORG_VALUE ASC
       ,ID_ENTITY_VALUE   ASC
       ,ID_VALUE  ASC )
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


 -- ------------------------------------------------------------------------------------
 --			INSERT ERROR MESSAGE
 -- -----------------------------------------------------------------------------------

INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
,STG_RECORD_IDENT_FK
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT
           A.STG_RECORD_ID
,A.STG_RECORD_IDENT_ID
          ,A.JOB_FK
          ,'Duplicate Unique Identifiers in same import job'
          ,A.ID_DATA_NAME_FK
          ,A.ID_VALUE
          ,CASE ISNULL ( A.ID_ENTITY_DATA_NAME_FK, 0 )
           WHEN 0 THEN 
           ( SELECT LABEL FROM EPACUBE.DATA_NAME
             WHERE DATA_NAME_ID = A.ID_ORG_DATA_NAME_FK )          
           ELSE
           ( SELECT LABEL FROM EPACUBE.DATA_NAME
             WHERE DATA_NAME_ID = A.ID_ENTITY_DATA_NAME_FK )
           END
          ,CASE ISNULL ( A.ID_ENTITY_VALUE, 'NULL DATA' )
           WHEN 'NULL DATA'  THEN A.ID_ORG_VALUE                
           ELSE A.ID_ENTITY_VALUE
           END
          ,99    --EVENT_CONDITION_CR_FK
          ,@IN_BATCH_NO
          ,GETDATE()
FROM (
SELECT 
           SREC.STG_RECORD_ID
,SRECI.STG_RECORD_IDENT_ID
          ,SREC.JOB_FK
          ,SRECI.ID_DATA_NAME_FK
          ,SRECI.ID_VALUE
          ,SRECI.ID_ENTITY_DATA_NAME_FK  
          ,SRECI.ID_ENTITY_VALUE         
          ,SRECI.ID_ENTITY_STRUCTURE_FK 
          ,SRECI.ID_ORG_DATA_NAME_FK  
          ,SRECI.ID_ORG_VALUE                          
          ,SRECI.ORG_ENTITY_STRUCTURE_FK           
FROM stage.STG_RECORD SREC WITH (NOLOCK)
INNER JOIN stage.STG_RECORD_IDENT SRECI WITH (NOLOCK)
   ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID )
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
   ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
INNER JOIN epacube.DATA_SET DS WITH (NOLOCK)
   ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )      
INNER JOIN #TS_BATCH_DUPS TSBD
  ON ( TSBD.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK 
  AND  ISNULL ( TSBD.ID_ORG_VALUE, -999 ) = ISNULL ( SRECI.ID_ORG_VALUE, -999 )
  AND  ISNULL ( TSBD.ID_ENTITY_VALUE, -999 ) = ISNULL(SRECI.ID_ENTITY_VALUE,-999) 
  AND  TSBD.ID_VALUE = SRECI.ID_VALUE )   
WHERE  SREC.JOB_FK = @IN_JOB_FK
AND   DS.TABLE_NAME IN ( 'PRODUCT_IDENTIFICATION', 'PRODUCT_IDENT_MULT', 'ENTITY_IDENTIFICATION', 'ENTITY_IDENT_MULT' )
) A 


--drop temp table if it exists
	IF object_id('tempdb..#TS_BATCH_DUPS') is not null
	   drop table #TS_BATCH_DUPS


 -- ------------------------------------------------------------------------------------
 --			CHECK FOR DIFFERENT UNIQUE IDENTS WITHIN SAME RECORD
 --			IF THEY EXIST SET STG_RECORD TO RED 
--			IE: VPN  JIRA - 3055
 -- -----------------------------------------------------------------------------------

	   
	-- create temp table
	CREATE TABLE #TS_BATCH_DUPS2(
	    DATA_NAME_FK             INT NULL,
	    ID_ORG_VALUE             varchar(128)  NULL,
	    ID_ENTITY_VALUE          varchar(128)  NULL,
		STG_RECORD_FK		      BIGINT  NULL
		 )


    INSERT INTO #TS_BATCH_DUPS2
     (  DATA_NAME_FK
       ,ID_ORG_VALUE
       ,ID_ENTITY_VALUE
       ,STG_RECORD_FK )
    SELECT 
           SRECI.ID_DATA_NAME_FK 
          ,SRECI.ID_ORG_VALUE
          ,SRECI.ID_ENTITY_VALUE
          ,SRECI.STG_RECORD_FK             
	FROM stage.STG_RECORD SREC WITH (NOLOCK)
	INNER JOIN stage.STG_RECORD_IDENT SRECI WITH (NOLOCK)
	   ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID 
		AND (ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL'
		OR SRECI.ID_VALUE <> '-999999999'))
	INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
	   ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
	INNER JOIN epacube.DATA_SET DS WITH (NOLOCK)
	   ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )      
	WHERE SREC.JOB_FK = @IN_JOB_FK
	AND   DS.TABLE_NAME IN ( 'PRODUCT_IDENTIFICATION')
    GROUP BY SRECI.ID_DATA_NAME_FK, SRECI.ID_ORG_VALUE, SRECI.ID_ENTITY_VALUE, SRECI.STG_RECORD_FK
    HAVING COUNT(*) > 1
              

--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------


	CREATE NONCLUSTERED INDEX IDX_TSBD_DNF_OESF_ESF_IND ON #TS_BATCH_DUPS2
	(   DATA_NAME_FK	ASC
       ,ID_ORG_VALUE ASC
       ,ID_ENTITY_VALUE   ASC
       ,STG_RECORD_FK  ASC )
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


 -- ------------------------------------------------------------------------------------
 --			INSERT ERROR MESSAGE
 -- -----------------------------------------------------------------------------------

INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
			,STG_RECORD_IDENT_FK
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
           
           SELECT 
  A.STG_RECORD_ID
 ,A.STG_RECORD_IDENT_ID
 ,A.JOB_FK
  ,'Different Product Identifiers Imported for Same Product/Vendor'
            ,A.ID_DATA_NAME_FK
            ,CASE ISNULL ( A.ID_ENTITY_DATA_NAME_FK, 0 )
           WHEN 0 THEN 
           ( SELECT LABEL FROM EPACUBE.DATA_NAME
             WHERE DATA_NAME_ID = A.ID_ORG_DATA_NAME_FK )          
           ELSE
           ( SELECT LABEL FROM EPACUBE.DATA_NAME
             WHERE DATA_NAME_ID = A.ID_ENTITY_DATA_NAME_FK )
           END
          ,A.ID_VALUE
           ,NULL
          ,99
          ,@IN_BATCH_NO
          ,GETDATE()
           
  
FROM (
SELECT 
           SREC.STG_RECORD_ID
		  ,SRECI.STG_RECORD_IDENT_ID
          ,SREC.JOB_FK
          ,SRECI.ID_DATA_NAME_FK
          ,SRECI.ID_VALUE
          ,SRECI.ID_ENTITY_DATA_NAME_FK  
          ,SRECI.ID_ENTITY_VALUE         
          ,SRECI.ID_ENTITY_STRUCTURE_FK 
          ,SRECI.ID_ORG_DATA_NAME_FK  
          ,SRECI.ID_ORG_VALUE                          
          ,SRECI.ORG_ENTITY_STRUCTURE_FK           
FROM stage.STG_RECORD SREC WITH (NOLOCK)
INNER JOIN stage.STG_RECORD_IDENT SRECI WITH (NOLOCK)
   ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID )
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
   ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
INNER JOIN epacube.DATA_SET DS WITH (NOLOCK)
   ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )      
INNER JOIN #TS_BATCH_DUPS2 TSBD
  ON ( TSBD.STG_RECORD_FK = SREC.STG_RECORD_ID)
  ----AND  TSBD.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK 
  ----AND  ISNULL ( TSBD.ID_ORG_VALUE, -999 ) = ISNULL ( SRECI.ID_ORG_VALUE, -999 )
  ----AND  ISNULL ( TSBD.ID_ENTITY_VALUE, -999 ) = ISNULL(SRECI.ID_ENTITY_VALUE,-999) 
  ----AND  TSBD.ID_VALUE <> SRECI.ID_VALUE )   
WHERE  SREC.JOB_FK = @IN_JOB_FK
AND   DS.TABLE_NAME IN ( 'PRODUCT_IDENTIFICATION' )
) A 


--drop temp table if it exists
	IF object_id('tempdb..#TS_BATCH_DUPS2') is not null
	   drop table #TS_BATCH_DUPS2



 -- ------------------------------------------------------------------------------------
 --			CHECK FOR MORE THAN ONE IDENT PER PRODUCT WITHIN SAME IMPORT JOB
 --			IF THEY EXIST SET STG_RECORD TO RED 
 --			NOTE:  THIS VALIDATION HAS TO CHECK ENTIRE IMPORT JOB
 -- -----------------------------------------------------------------------------------


INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
           ,STG_RECORD_IDENT_FK
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT
           A.STG_RECORD_ID
          ,A.STG_RECORD_IDENT_ID
          ,A.JOB_FK
          ,'There is more than one Identifier per Product in same import job'
          ,A.ID_DATA_NAME_FK
          ,A.ID_VALUE
          ,CASE ISNULL ( A.ID_ENTITY_DATA_NAME_FK, 0 )
           WHEN 0 THEN 
           ( SELECT LABEL FROM EPACUBE.DATA_NAME
             WHERE DATA_NAME_ID = A.ID_ORG_DATA_NAME_FK )          
           ELSE
           ( SELECT LABEL FROM EPACUBE.DATA_NAME
             WHERE DATA_NAME_ID = A.ID_ENTITY_DATA_NAME_FK )
           END
          ,CASE ISNULL ( A.ID_ENTITY_VALUE, 'NULL DATA' )
           WHEN 'NULL DATA'  THEN A.ID_ORG_VALUE                
           ELSE A.ID_ENTITY_VALUE
           END
          ,99    --EVENT_CONDITION_CR_FK
          ,@IN_BATCH_NO
          ,GETDATE()
FROM (
SELECT 
           SREC.STG_RECORD_ID
          ,SRECI.STG_RECORD_IDENT_ID
          ,SREC.JOB_FK
          ,SRECI.ID_DATA_NAME_FK
          ,SRECI.ID_VALUE
          ,SRECI.ID_ENTITY_DATA_NAME_FK  
          ,SRECI.ID_ENTITY_VALUE         
          ,SRECI.ID_ENTITY_STRUCTURE_FK 
          ,SRECI.ID_ORG_DATA_NAME_FK  
          ,SRECI.ID_ORG_VALUE                          
          ,SRECI.ORG_ENTITY_STRUCTURE_FK           
FROM stage.STG_RECORD SREC WITH (NOLOCK)
INNER JOIN stage.STG_RECORD_IDENT SRECI WITH (NOLOCK)
   ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID )
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
   ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
INNER JOIN epacube.DATA_SET DS WITH (NOLOCK)
   ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )      
WHERE  SREC.STG_RECORD_ID IN (
				    SELECT SRECX.STG_RECORD_ID
					FROM stage.STG_RECORD SRECX WITH (NOLOCK)
					INNER JOIN stage.STG_RECORD_IDENT SRECIX WITH (NOLOCK)
					   ON ( SRECIX.STG_RECORD_FK = SRECX.STG_RECORD_ID 
						AND ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL')
					INNER JOIN epacube.DATA_NAME DNX WITH (NOLOCK)
					   ON ( DNX.DATA_NAME_ID = SRECIX.ID_DATA_NAME_FK )
					INNER JOIN epacube.DATA_SET DSX WITH (NOLOCK)
					   ON ( DSX.DATA_SET_ID = DNX.DATA_SET_FK )      
					WHERE SRECX.JOB_FK = @IN_JOB_FK
					AND   DSX.TABLE_NAME IN ( 'PRODUCT_IDENTIFICATION', 'PRODUCT_IDENT_NONUNIQUE' )
					GROUP BY SRECX.STG_RECORD_ID, SRECIX.ID_DATA_NAME_FK,  SRECIX.ID_ORG_VALUE, SRECIX.ID_ENTITY_VALUE
					HAVING COUNT(*) > 1
				     ) 
) A 




 -- ************************************************************************************
 --
 --     ASSOCIATED AND ATTRIBUTE ENTITY VALIDATES   ( STG_RECORD_ENTITY TABLE )
 --
 -- ************************************************************************************



 -- ------------------------------------------------------------------------------------
 --       IF a Stg Record Entity was Auto Inserted.. Log Message
 -- -----------------------------------------------------------------------------------
	
			   INSERT INTO stage.STG_RECORD_ERRORS
                        (job_fk,
						 batch_no,
						 STG_RECORD_fk,
                         error_name,
						 data_name_fk, error_data1 ,
                         error_data2 ,error_data3,
                         event_condition_cr_fk )
               SELECT  DISTINCT
                       SREC.JOB_FK
					  ,@in_batch_no
			   		  ,SREC.STG_RECORD_ID
					  ,'Entity was auto inserted during Import Process' error_name
					  ,SRECE.ENTITY_DATA_NAME_FK
					  ,( SELECT DN.LABEL
					     FROM EPACUBE.DATA_NAME DN WITH (NOLOCK)
					     WHERE DATA_NAME_ID = SRECEI.ENTITY_ID_DATA_NAME_FK )
                      ,SRECEI.ENTITY_ID_VALUE
					  ,null error_data3
                      ,97
			   FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
			   INNER JOIN STAGE.STG_RECORD_ENTITY SRECE WITH (NOLOCK)
			     ON ( SRECE.STG_RECORD_FK = SREC.STG_RECORD_ID )
			   INNER JOIN stage.STG_RECORD_ENTITY_IDENT SRECEI WITH (NOLOCK)
			     ON ( SRECEI.STG_RECORD_ENTITY_FK = SRECE.STG_RECORD_ENTITY_ID 
			     AND  SRECEI.COLUMN_NAME = SRECE.COLUMN_NAME )			     
               WHERE SREC.BATCH_NO = @IN_BATCH_NO
               AND   SRECE.ACTIVITY_ACTION_CR_FK = 144  -- NEW



------------------------------------------------------------------------------------------
--   ORG MISSING WITH EVENT DATA DEPENDENCY
------------------------------------------------------------------------------------------

INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT 
           SREC.STG_RECORD_ID
          ,SREC.JOB_FK
          ,'Warehouse is UNKNOWN and is REQUIRED for Attributes on Import Row '          
          ,SRECE.ENTITY_DATA_NAME_FK
          ,( SELECT LABEL FROM EPACUBE.DATA_NAME WITH (NOLOCK)
             WHERE DATA_NAME_ID = SRECEI.ENTITY_ID_DATA_NAME_FK )
          ,SRECEI.ENTITY_ID_VALUE
          ,NULL             
          ,99    --EVENT_CONDITION_CR_FK
          ,@IN_BATCH_NO
          ,GETDATE()
FROM stage.STG_RECORD SREC WITH (NOLOCK)
INNER JOIN stage.STG_RECORD_ENTITY SRECE WITH (NOLOCK)
   ON ( SRECE.STG_RECORD_FK = SREC.STG_RECORD_ID
   AND  SRECE.ENTITY_DATA_NAME_FK = ( SELECT DNX.DATA_NAME_ID FROM EPACUBE.DATA_NAME DNX WHERE NAME = 'WAREHOUSE'  ) )           
INNER JOIN stage.STG_RECORD_ENTITY_IDENT SRECEI WITH (NOLOCK)
   ON ( SRECEI.STG_RECORD_ENTITY_FK = SRECE.STG_RECORD_ENTITY_ID 
   AND  SRECEI.COLUMN_NAME = SRECE.COLUMN_NAME )
WHERE SREC.STG_RECORD_ID IN 
(   SELECT SJRB.BEST_SEARCH_FK
    FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
    WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
    AND   SJRB.BATCH_NO = @IN_BATCH_NO
    AND   SJRB.BEST_SEARCH_FK IS NOT NULL )
---
AND   SRECE.ENTITY_STRUCTURE_FK IS NULL
--- KS ADDED THIS LOGIC INSTEAD OF THE INNER JOIN ABOVE
AND   SREC.ENTITY_DATA_NAME_FK not in ( 141000, 141111)   ---  add to not check when creating warehouse
AND   SREC.IMPORT_PACKAGE_FK IN (
           SELECT DISTINCT IMPORT_PACKAGE_FK
           FROM import.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
           INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
             ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK )
           INNER JOIN epacube.DATA_SET DS WITH (NOLOCK)
             ON ( DS.DATA_SET_ID = DN.DATA_SET_FK 
             AND  DS.ORG_IND = 1 )
           WHERE IMM.IMPORT_PACKAGE_FK = @v_import_package_fk
           )





------------------------------------------------------------------------------------------
--   ENTITIES MISSING WITH EVENT DATA DEPENDENCY
------------------------------------------------------------------------------------------

INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT 
           SREC.STG_RECORD_ID
          ,SREC.JOB_FK
          ,'Entity is UNKNOWN and is REQUIRED for Attribute '          
          ,SRECE.ENTITY_DATA_NAME_FK
          ,( SELECT LABEL FROM EPACUBE.DATA_NAME WITH (NOLOCK)
             WHERE DATA_NAME_ID = SRECEI.ENTITY_ID_DATA_NAME_FK )
          ,SRECEI.ENTITY_ID_VALUE             
          ,( SELECT LABEL FROM EPACUBE.DATA_NAME WITH (NOLOCK)
             WHERE DATA_NAME_ID = IMM.DATA_NAME_FK )                    
          ,99    --EVENT_CONDITION_CR_FK
          ,@IN_BATCH_NO
          ,GETDATE()
FROM stage.STG_RECORD SREC WITH (NOLOCK)
INNER JOIN stage.STG_RECORD_ENTITY SRECE WITH (NOLOCK)
   ON ( SRECE.STG_RECORD_FK = SREC.STG_RECORD_ID )
INNER JOIN stage.STG_RECORD_ENTITY_IDENT SRECEI WITH (NOLOCK)
   ON ( SRECEI.STG_RECORD_ENTITY_FK = SRECE.STG_RECORD_ENTITY_ID 
   AND  SRECEI.COLUMN_NAME = SRECE.COLUMN_NAME )
INNER JOIN import.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
   ON ( IMM.IMPORT_PACKAGE_FK = SREC.IMPORT_PACKAGE_FK
   AND  IMM.RECORD_STATUS_CR_FK = 1)
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
   ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK 
   AND  DN.ENTITY_DATA_NAME_FK = SRECE.ENTITY_DATA_NAME_FK )      
WHERE SREC.STG_RECORD_ID IN 
(   SELECT SJRB.BEST_SEARCH_FK
    FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
    WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
    AND   SJRB.BATCH_NO = @IN_BATCH_NO
    AND   SJRB.BEST_SEARCH_FK IS NOT NULL )
---
AND   SRECE.ENTITY_STRUCTURE_FK IS NULL

------------------------------------------------------------------------------------------
--  TAXONOMY CHECK  PRODUCT NOT ASSIGNED TO NODE MISSING CAN NOT IMPORT
-------------------------------------------------------------------------------------------

--INSERT INTO [stage].[STG_RECORD_ERRORS]
--           ([STG_RECORD_FK]
--           ,[JOB_FK]
--           ,[ERROR_NAME]
--           ,[DATA_NAME_FK]
--           ,[ERROR_DATA1]
--           ,[ERROR_DATA2]
--           ,[ERROR_DATA3]
--           ,[EVENT_CONDITION_CR_FK]
--           ,[BATCH_NO]
--           ,[UPDATE_TIMESTAMP])
--SELECT 
--           SREC.STG_RECORD_ID
--          ,SREC.JOB_FK
--          ,'Data Name is UNKNOWN and is REQUIRED for Attribute '          
--,NULL
--,IRNV.DATA_NAME              
--          ,DATA_VALUE                 
--          ,NULL
--          ,99    --EVENT_CONDITION_CR_FK
--          ,@IN_BATCH_NO
--          ,GETDATE()
--FROM stage.STG_RECORD SREC WITH (NOLOCK)
--INNER JOIN import.IMPORT_RECORD_NAME_VALUE IRNV WITH (NOLOCK)
--   ON ( IRNV.JOB_FK = SREC.JOB_FK
--   AND  IRNV.RECORD_NO = SREC.RECORD_NO )
--LEFT JOIN epacube.DATA_NAME DN WITH (NOLOCK)
--   ON ( DN.NAME = IRNV.DATA_NAME 
--   AND  DN.RECORD_STATUS_CR_FK = 1 )
--WHERE SREC.STG_RECORD_ID IN 
--(   SELECT SJRB.BEST_SEARCH_FK
--    FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
--    WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
--    AND   SJRB.BATCH_NO = @IN_BATCH_NO
--    AND   SJRB.BEST_SEARCH_FK IS NOT NULL )
-----
--AND   DN.DATA_NAME_ID IS NULL


------------------------------------------------------------------------------------------
--   DATA NAME MISSING CAN NOT IMPORT
------------------------------------------------------------------------------------------

INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT 
           SREC.STG_RECORD_ID
          ,SREC.JOB_FK
          ,'Data Name is UNKNOWN and is REQUIRED for Attribute '          
,NULL
,IRNV.DATA_NAME              
          ,DATA_VALUE                 
          ,NULL
          ,99    --EVENT_CONDITION_CR_FK
          ,@IN_BATCH_NO
          ,GETDATE()
FROM stage.STG_RECORD SREC WITH (NOLOCK)
INNER JOIN import.IMPORT_RECORD_NAME_VALUE IRNV WITH (NOLOCK)
   ON ( IRNV.JOB_FK = SREC.JOB_FK
   AND  IRNV.RECORD_NO = SREC.RECORD_NO )
LEFT JOIN epacube.DATA_NAME DN WITH (NOLOCK)
   ON ( DN.NAME = IRNV.DATA_NAME 
   AND  DN.RECORD_STATUS_CR_FK = 1 )
WHERE SREC.STG_RECORD_ID IN 
(   SELECT SJRB.BEST_SEARCH_FK
    FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
    WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
    AND   SJRB.BATCH_NO = @IN_BATCH_NO
    AND   SJRB.BEST_SEARCH_FK IS NOT NULL )
---
AND   DN.DATA_NAME_ID IS NULL




------------------------------------------------------------------------------------------
--   ORG or ENTITY STRUCTURE MISSING FOR IDENTIFIER
------------------------------------------------------------------------------------------

INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
,STG_RECORD_IDENT_FK
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT 
           SREC.STG_RECORD_ID
,SRECI.STG_RECORD_IDENT_ID
          ,SREC.JOB_FK
          ,'Warehouse is UNKNOWN and is REQUIRED for Identifier '          
           ,NULL
          ,DN.LABEL             
          ,SRECI.ID_VALUE                
          ,NULL
          ,99    --EVENT_CONDITION_CR_FK
          ,@IN_BATCH_NO
          ,GETDATE()
FROM stage.STG_RECORD SREC WITH (NOLOCK)
INNER JOIN stage.STG_RECORD_IDENT SRECI WITH (NOLOCK)
   ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID )
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
   ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
WHERE SREC.STG_RECORD_ID IN 
(   SELECT SJRB.BEST_SEARCH_FK
    FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
    WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
    AND   SJRB.BATCH_NO = @IN_BATCH_NO
    AND   SJRB.BEST_SEARCH_FK IS NOT NULL )
---
AND   SRECI.ORG_ENTITY_STRUCTURE_FK IS NULL
AND   SREC.ENTITY_DATA_NAME_FK not in (141000,141111, 311030)    ---  add to not check when creating warehouse

-----Check other idents to see if entity is required.

INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT 
           SREC.STG_RECORD_ID
          ,SREC.JOB_FK
          ,'Entity is UNKNOWN and is REQUIRED for Identifier '          
          ,SRECI.ID_ENTITY_DATA_NAME_FK
          ,( SELECT LABEL FROM EPACUBE.DATA_NAME WITH (NOLOCK)
             WHERE DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
          ,SRECI.ID_VALUE             
          ,( SELECT LABEL FROM EPACUBE.DATA_NAME WITH (NOLOCK)
             WHERE DATA_NAME_ID = IMM.DATA_NAME_FK )                    
          ,99    --EVENT_CONDITION_CR_FK
          ,@IN_BATCH_NO
          ,GETDATE()
FROM stage.STG_RECORD SREC WITH (NOLOCK)
   INNER JOIN import.IMPORT_MAP_METADATA IMM WITH (NOLOCK)
   ON ( IMM.IMPORT_PACKAGE_FK = SREC.IMPORT_PACKAGE_FK
   AND  IMM.RECORD_STATUS_CR_FK = 1)
INNER JOIN stage.stg_record_ident SRECI WITH (NOLOCK)
ON (SREC.STG_RECORD_ID = SRECI.STG_RECORD_FK
AND SREC.JOB_FK = SRECI.JOB_FK)
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
   ON ( DN.DATA_NAME_ID = IMM.DATA_NAME_FK
AND  DN.ENTITY_DATA_NAME_FK is not null )
--  
AND   ISNULL(SRECI.ID_ENTITY_DATA_NAME_FK,-999) <> -999
AND   SRECI.ID_ENTITY_STRUCTURE_FK IS NULL
AND   SREC.JOB_FK = @IN_JOB_FK
AND   SREC.BATCH_NO = @IN_BATCH_NO



------------------------------------------------------------------------------------------
--   INSERT ERROR THAT MULTIPLE STAGE RECORDS MATCHED TO A SINGLE EPACUBE RECORD
------------------------------------------------------------------------------------------

INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT 
           SREC.STG_RECORD_ID
          ,SREC.JOB_FK
          ,'Multiple Import Records matching to a single epaCUBE record'          
           ,NULL
          ,( SELECT DN.LABEL FROM EPACUBE.DATA_NAME DN  WITH (NOLOCK)
             WHERE DN.DATA_NAME_ID = ( SELECT CAST ( EEP.VALUE AS INT )
                                                          FROM EPACUBE.EPACUBE_PARAMS eep WITH (NOLOCK)
                                                          WHERE APPLICATION_SCOPE_FK = 100
                                                          AND   EEP.NAME = 'PRODUCT' ) )
          ,( SELECT PI.VALUE FROM EPACUBE.PRODUCT_IDENTIFICATION PI WITH (NOLOCK)
             WHERE PI.PRODUCT_STRUCTURE_FK = SJFTM.FROM_SEARCH_FK
             AND   PI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
                                                           FROM EPACUBE.EPACUBE_PARAMS eep WITH (NOLOCK)
                                                           WHERE APPLICATION_SCOPE_FK = 100
                                                           AND   EEP.NAME = 'PRODUCT' ) )
          ,NULL
          ,99    --EVENT_CONDITION_CR_FK
          ,@IN_BATCH_NO
          ,GETDATE()
FROM stage.STG_RECORD SREC WITH (NOLOCK)
INNER JOIN cleanser.SEARCH_JOB_FROM_TO_MATCH SJFTM WITH (NOLOCK)
  ON ( SJFTM.TO_SEARCH_FK = SREC.STG_RECORD_ID )
WHERE SREC.STG_RECORD_ID IN 
(   SELECT SJRB.BEST_SEARCH_FK
    FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
    WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
    AND   SJRB.BATCH_NO = @IN_BATCH_NO
    AND   SJRB.BEST_SEARCH_FK IS NOT NULL )
---
AND   SJFTM.FROM_SEARCH_FK IN (
			SELECT A.FROM_SEARCH_FK
			FROM (
			SELECT SRECX.JOB_FK, SRECX.STG_RECORD_ID, SJFTMX.FROM_SEARCH_FK
			FROM STAGE.STG_RECORD SRECX WITH (NOLOCK)
			INNER JOIN [cleanser].[SEARCH_JOB_FROM_TO_MATCH] SJFTMX WITH (NOLOCK)
							   ON (SJFTMX.TO_SEARCH_FK = SRECX.STG_RECORD_ID)
			WHERE SRECX.BATCH_NO = @IN_BATCH_NO
			GROUP BY SRECX.JOB_FK, SRECX.STG_RECORD_ID, SJFTMX.FROM_SEARCH_FK
			) A
			GROUP BY A.FROM_SEARCH_FK
			HAVING COUNT(*) > 1		   
       )


------------------------------------------------------------------------------------------
--   INSERT WARNING THAT STAGE RECORD MATCHED TO MULTIPLE EPACUBE RECORDS
------------------------------------------------------------------------------------------

INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT 
           SREC.STG_RECORD_ID
          ,SREC.JOB_FK
          ,'Multiple epaCUBE Records matching to a single Import record' + ' - ' + isnull((select label
          from epacube.data_name where name = ENTITY_ID_DATA_NAME),ENTITY_ID_DATA_NAME)          
           ,NULL
          ,ENTITY_ID_DATA_NAME
          ,SREC.ENTITY_ID_VALUE
          ,NULL
          ,98    --EVENT_CONDITION_CR_FK
          ,@IN_BATCH_NO
          ,GETDATE()
FROM stage.STG_RECORD SREC WITH (NOLOCK)
WHERE SREC.STG_RECORD_ID IN 
(   SELECT SJRB.BEST_SEARCH_FK
    FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
    WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
    AND   SJRB.BATCH_NO = @IN_BATCH_NO
    AND   SJRB.BEST_SEARCH_FK IS NOT NULL )
---
AND   SREC.STG_RECORD_ID IN (
			SELECT A.STG_RECORD_ID
			FROM (
			SELECT SRECX.JOB_FK, SRECX.STG_RECORD_ID, SJFTM.FROM_SEARCH_FK
			FROM STAGE.STG_RECORD SRECX WITH (NOLOCK)
			INNER JOIN [cleanser].[SEARCH_JOB_FROM_TO_MATCH] SJFTM WITH (NOLOCK)
							   ON (SJFTM.TO_SEARCH_FK = SRECX.STG_RECORD_ID)
		    WHERE SRECX.BATCH_NO = @IN_BATCH_NO
			GROUP BY  SRECX.JOB_FK, SRECX.STG_RECORD_ID, SJFTM.FROM_SEARCH_FK
			) A
			GROUP BY A.STG_RECORD_ID
			HAVING COUNT(*) > 1		   
       )



------------------------------------------------------------------------------------------
--   INSERT ERROR PRIMARY PRODUCT IDENTIFICATION DO NOT MATCH
------------------------------------------------------------------------------------------

INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
,STG_RECORD_IDENT_FK
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT 
           SREC.STG_RECORD_ID
,SRECI.STG_RECORD_IDENT_ID
          ,SREC.JOB_FK
          ,'Primary Product Identification does not Match'          
           ,NULL
          ,( SELECT DN.LABEL FROM EPACUBE.DATA_NAME DN WITH (NOLOCK) WHERE DN.DATA_NAME_ID = ( SELECT CAST ( EEP.VALUE AS INT )
                                                                                FROM EPACUBE.EPACUBE_PARAMS eep WITH (NOLOCK)
                                                                                WHERE APPLICATION_SCOPE_FK = 100
                                                                                AND   EEP.NAME = 'PRODUCT' ) )
          ,( SELECT PI.VALUE FROM EPACUBE.PRODUCT_IDENTIFICATION PI WITH (NOLOCK)
             WHERE PI.PRODUCT_STRUCTURE_FK = SJRB.FROM_SEARCH_FK
             AND   PI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
                                                                                FROM EPACUBE.EPACUBE_PARAMS eep WITH (NOLOCK)
                                                                                WHERE APPLICATION_SCOPE_FK = 100
                                                                                AND   EEP.NAME = 'PRODUCT' ) )
          ,NULL
          ,98    --EVENT_CONDITION_CR_FK
          ,@IN_BATCH_NO
          ,GETDATE()
FROM stage.STG_RECORD SREC WITH (NOLOCK)
INNER JOIN cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
  ON ( SJRB.BEST_SEARCH_FK = SREC.STG_RECORD_ID )
INNER JOIN stage.STG_RECORD_IDENT SRECI
    ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID
    AND  SRECI.ID_DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
                                    FROM EPACUBE.EPACUBE_PARAMS eep WITH (NOLOCK)
                                    WHERE APPLICATION_SCOPE_FK = 100
                                    AND   EEP.NAME = 'PRODUCT' ) )
INNER JOIN EPACUBE.PRODUCT_IDENTIFICATION PI WITH (NOLOCK)
    ON ( PI.PRODUCT_STRUCTURE_FK = SJRB.FROM_SEARCH_FK
    AND  PI.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
                                    FROM EPACUBE.EPACUBE_PARAMS eep WITH (NOLOCK)
                                    WHERE APPLICATION_SCOPE_FK = 100
                                    AND   EEP.NAME = 'PRODUCT' ) )
WHERE 1 = 1
AND  PI.VALUE <> SRECI.ID_VALUE
AND  SJRB.SEARCH_JOB_FK = @IN_JOB_FK
AND   SJRB.BATCH_NO = @IN_BATCH_NO



------------------------------------------------------------------------------------------
--   VALIDATE THAT SIZE OF IDENTIFIER IS CORRECT DURING CLEANSER..
--		IF NOT THEN WILL NOT BE AUTO INSERTED AS A PRE-POST VALUE
------------------------------------------------------------------------------------------


INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
			,[STG_RECORD_IDENT_FK]
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])

		(	SELECT 
SRECI.STG_RECORD_FK
			 ,SRECI.STG_RECORD_IDENT_ID --EVENT FK
			,SREC.JOB_FK
			,CASE ISNULL ( DN.MAX_OR_EQ, 'MAX' ) 
			 WHEN 'MAX' THEN 'Length has exceeded maximum' 
			 ELSE 'Length is not equal to' 
			 END  --- error name  
			,SRECI.ID_DATA_NAME_FK 
			,DN.LENGTH  --ERROR1
			,'Current Length '
			,LEN ( SRECI.ID_VALUE )
			,98 -- WARNING THAT IT WILL NOT BE IMPORTED... CONDITION
			,SREC.BATCH_NO
			,@l_sysdate
			 FROM STAGE.STG_RECORD SREC WITH (NOLOCK) 
		  INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
			 ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID
		AND ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL')
		  INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
		     ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK
		AND   dn.LENGTH IS NOT NULL 
		AND SRECI.ID_VALUE <> '-999999999'
			AND   (
			        (     dn.max_or_eq = 'EQ' 
			        AND  LEN ( SRECI.ID_VALUE ) <> DN.LENGTH )
			      OR
				    (     LEN ( SRECI.ID_VALUE ) > DN.LENGTH )			
				  ) )
		  INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
		     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		  WHERE SREC.JOB_FK = @IN_JOB_FK
		  AND   SREC.BATCH_NO = @IN_BATCH_NO
		  AND   DS.TABLE_NAME = 'PRODUCT_IDENTIFICATION'
       )


/*

---- ks comment out for now...
------------------------------------------------------------------------------------------
--   INSERT MESSAGE FOR THE TYPE OF MATCHING PERFORMED
------------------------------------------------------------------------------------------


INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT 
           SREC.STG_RECORD_ID
          ,SREC.JOB_FK
          ,'Matching Method:: '          
          ,( SELECT ID_DATA_NAME_FK
             FROM cleanser.SEARCH_JOB_RESULTS_IDENT WITH (NOLOCK)
             WHERE SEARCH_JOB_RESULTS_IDENT_ID = SJRB.SEARCH_JOB_RESULTS_IDENT_FK ) 
          ,( SELECT code FROM epacube.code_ref WITH (NOLOCK)
             WHERE  code_ref_id = SAIMR.MATCH_TYPE_CR_FK )
          ,' Confidence:: '
          ,CAST ( ISNULL ( SJRB.BEST_CONFIDENCE, 0 ) AS VARCHAR(8) )
          ,SAIMR.EVENT_CONDITION_CR_FK
          ,@IN_BATCH_NO
          ,GETDATE()
FROM stage.STG_RECORD SREC WITH (NOLOCK)
INNER JOIN [cleanser].[SEARCH_JOB_RESULTS_BEST] SJRB WITH (NOLOCK)
				   ON (SJRB.BEST_SEARCH_FK = SREC.STG_RECORD_ID)
INNER JOIN cleanser.SEARCH_ACTIVITY_IDENT_MATCH_RULE SAIMR WITH (NOLOCK)
                   ON (SAIMR.SEARCH_ACTIVITY_IDENT_MATCH_RULE_ID = SJRB.SEARCH_ACTIVITY_IDENT_MATCH_RULE_FK )
WHERE SREC.STG_RECORD_ID IN 
(   SELECT SJRB.BEST_SEARCH_FK
    FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
    WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
    AND   SJRB.BATCH_NO = @IN_BATCH_NO
    AND   SJRB.BEST_SEARCH_FK IS NOT NULL )
  

*/


------------------------------------------------------------------------------------------
--   INSERT YELLOW ERROR MESSAGE FOR DUPLICATE DATA FROM THE SAME FILE
------------------------------------------------------------------------------------------


SET @v_filename_count = (SELECT ISNULL (
                              ( select count(1)
								from (
								SELECT distinct IRD.IMPORT_FILENAME 
								from import.IMPORT_RECORD_DATA IRD
								where  IRD.job_fk = @IN_JOB_FK
								group by IRD.IMPORT_FILENAME) a )
                               , 1 )
)

IF @v_filename_count = 1 

BEGIN


EXECUTE  [cleanser].[IMPORT_CLEANSER_VALIDATION] 
   @IN_JOB_FK
  ,@IN_BATCH_NO


END


------------------------------------------------------------------------------------------
--   CHECK FOR BAD DATA
------------------------------------------------------------------------------------------

EXEC  [cleanser].[IMPORT_CLEANSER_BAD_DATA]    @IN_JOB_FK
  
  
------------------------------------------------------------------------------------------
--   CHECK FOR REMOVING DATA
------------------------------------------------------------------------------------------

EXEC  [cleanser].[IMPORT_NULLING_DATA]    @IN_JOB_FK
  

/* -------------------------------------------------------------------------------------------------- */
/*    Set Condition in order or RED, YELLOW then GREEN */
/* ------------------------------------------------------------------------------------------------- */

UPDATE stage.STG_RECORD
SET event_condition_cr_fk =
	 (SELECT top 1 srece.event_condition_cr_fk
		FROM [stage].[STG_RECORD_ERRORS] SRECE WITH (NOLOCK)
		WHERE srece.batch_no = @in_batch_no
		AND   srece.stg_record_fk = stage.STG_RECORD.stg_record_id
		ORDER BY srece.event_condition_cr_fk desc, srece.stg_record_errors_id asc )
WHERE stg_record_id IN 
(SELECT ee.stg_record_fk
	FROM stage.stg_record_errors ee WITH (NOLOCK)
	INNER JOIN cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
	  ON ( SJRB.BEST_SEARCH_FK = EE.STG_RECORD_FK )
    WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
    AND   SJRB.BATCH_NO = @IN_BATCH_NO
    AND   SJRB.BEST_SEARCH_FK IS NOT NULL )



------------------------------------------------------------------------------------------
--  UPDATE ACTIVITY ACTION AND EVENT ACTION 
---
---  ONLY PLACE GETTING UPDATED IN DATA CLEANSER PROCESS FLOW
------------------------------------------------------------------------------------------


UPDATE STAGE.STG_RECORD
SET ACTIVITY_ACTION_CR_FK = CASE ( SELECT COUNT(1) 
                                   FROM cleanser.SEARCH_JOB_FROM_TO_MATCH SJFTM WITH (NOLOCK)
                                   WHERE SJFTM.TO_SEARCH_FK = STAGE.STG_RECORD.STG_RECORD_ID )   -- COUNT OF MATCHES
                            WHEN 0 THEN  -- NO MATCHES ( ADD NEW OR DISCARD BASED ON IMPORT PACKAGE )
										( select ip.ACTIVITY_ACTION_CR_FK 
										  from import.import_package ip with (nolock)
										  where ip.import_package_id = STAGE.STG_RECORD.import_package_fk )  
                            WHEN 1 THEN  -- SINGLE MATCH
										CASE ( STAGE.STG_RECORD.EVENT_CONDITION_CR_FK )
										WHEN 99 THEN 140  -- UNRESOLVED
										WHEN 98 THEN 
												CASE WHEN ( @IN_BATCH_NO = STAGE.STG_RECORD.IMPORT_BATCH_NO )
													 THEN 140 -- UNRESOLVED
													 ELSE 147 -- REVIEWED ( MULTIPLE MATCHES ALLOWED BUT MUST BE MANUALLY REVIEWED )
												 END
										ELSE 147  -- REVIEWED 
										END  
                            ELSE -- MORE THAN ONE MATCHING  
										CASE ( STAGE.STG_RECORD.EVENT_CONDITION_CR_FK )
										WHEN 99 THEN 140  -- UNRESOLVED
										WHEN 98 THEN 
												CASE WHEN ( @IN_BATCH_NO = STAGE.STG_RECORD.IMPORT_BATCH_NO )
													 THEN 140 -- UNRESOLVED
													 ELSE 147 -- REVIEWED ( MULTIPLE MATCHES ALLOWED BUT MUST BE MANUALLY REVIEWED )
												 END
										ELSE 147  -- REVIEWED 
										END  
							END  
--
WHERE STG_RECORD_ID IN 
(   SELECT SJRB.BEST_SEARCH_FK
    FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
    WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
    AND   SJRB.BATCH_NO = @IN_BATCH_NO
    AND   SJRB.BEST_SEARCH_FK IS NOT NULL )

---added because if event is add new then nothing in the cleanser.SEARCH_JOB_FROM_TO_MATCH table
UPDATE STAGE.STG_RECORD
SET ACTIVITY_ACTION_CR_FK = 140
WHERE BATCH_NO = @IN_BATCH_NO
AND   EVENT_CONDITION_CR_FK in (98, 99) -- added yellow epa 2869
AND ACTIVITY_ACTION_CR_FK <> 141

--------------------------------------

UPDATE STAGE.STG_RECORD
SET EVENT_ACTION_CR_FK = CASE ( STAGE.STG_RECORD.ACTIVITY_ACTION_CR_FK )
                         WHEN 140 THEN NULL
                         WHEN 141 THEN NULL
                         WHEN 143 THEN 53   -- INACTIVATE
                         WHEN 144 THEN 54   -- ADD NEW
                         WHEN 146 THEN NULL
                         WHEN 147 THEN 52   -- CHANGE
                         ELSE NULL
                         END
--
WHERE STG_RECORD_ID IN 
(   SELECT SJRB.BEST_SEARCH_FK
    FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
    WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
    AND   SJRB.BATCH_NO = @IN_BATCH_NO
    AND   SJRB.BEST_SEARCH_FK IS NOT NULL )



UPDATE cleanser.SEARCH_JOB_RESULTS_BEST
SET ACTIVITY_ACTION_CR_FK = SREC.ACTIVITY_ACTION_CR_FK
FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
WHERE SREC.STG_RECORD_ID = BEST_SEARCH_FK
AND   SREC.JOB_FK = @IN_JOB_FK
AND   SREC.BATCH_NO = @IN_BATCH_NO



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.DATA_CLEANSER_VALIDATION'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk,  @epa_batch_no = @in_batch_no
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.DATA_CLEANSER_VALIDATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END

























































































































































































































