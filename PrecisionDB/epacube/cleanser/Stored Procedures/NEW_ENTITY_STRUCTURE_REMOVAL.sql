﻿







-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: For Stg Records submitted to Synhcronizer
--			Create any missing entities associated with them
--			Insert all Identifications also associated with them
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/28/2008   Initial SQL Version


CREATE PROCEDURE [cleanser].[NEW_ENTITY_STRUCTURE_REMOVAL] 
       @IN_JOB_FK         BIGINT            
      ,@IN_BATCH_NO       BIGINT       
AS
BEGIN


/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   Before a Stg Entity can leave cleanser it must have an ENTITY_STRUCTURE_fk assigned
--   and its UNIQUE identifiers established.  This is to avoid the next import from creating
--	 a duplicate of the same entity prior to this entities events being approved.
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--	 All Import Columns referencing Entities MUST be setup in the IMPORT_MAP_METADATA table
--	 with a data_name of data_set ENTITY_STRUCTURE with its REQUIRED Identification Data Name
--   and its option Data Association Data Name
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   STAGE.LOAD_ENTITIES
--   STAGE.LOAD_ENTITY_ENTITY_NAMES
--   Data inserted into Stage.STG_RECORD
--   cleanser.SEARCH_ENTITY_RESULT_MATCH_IDENT
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--   New "ROW" Entities Must be established first in order to create all needed ENTITY_STRUCTURE_fk's 
--	 To ensure the entity hierarchy is maintained properly; Insert Level 1's first then others
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   Next Create any missing "ASSOCIATION" Entities known by their UNIQUE identifiers 
-------------------------------------------------------------------------------------------------
--   PROCESS STEP3: 
--   
-- 
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint
DECLARE @v_search_activity	 int

DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int




BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.NEW_ENTITY_STRUCTURE_REMOVAL', 
                                                @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no,
												@exec_id = @l_exec_no OUTPUT;


SET @l_sysdate = GETDATE()

 -- ------------------------------------------------------------------------------------
 --			PLACE PRODUCTS TO BE DELETED IN A TEMP TABLE
 --			CHECK FOR PRODUCT MATCHED IN ANOTHER IMPORT JOB
 -- -----------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_BATCH_REMOVE') is not null
	   drop table #TS_BATCH_REMOVE
	   
	   
	-- create temp table
	CREATE TABLE #TS_BATCH_REMOVE(
	    SEARCH_STRUCTURE_FK      BIGINT
		 )


    INSERT INTO #TS_BATCH_REMOVE (
	    SEARCH_STRUCTURE_FK     
		 )
    SELECT 
        SRECS.SEARCH_STRUCTURE_FK 
    FROM STAGE.STG_RECORD SREC
	INNER JOIN EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
	  ON ( ES.JOB_FK          = SREC.JOB_FK
	  AND  ES.IMPORT_FILENAME = SREC.IMPORT_FILENAME
	  AND  ES.RECORD_NO       = SREC.RECORD_NO
	  AND  ES.DATA_NAME_FK    = SREC.ENTITY_DATA_NAME_FK )    
    INNER JOIN STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
       ON ( SREC.STG_RECORD_ID = SRECS.STG_RECORD_FK )
    LEFT JOIN STAGE.STG_RECORD_STRUCTURE SRECSX   WITH (NOLOCK)      --- MAKE SURE NO OTHER IMPORTS REFERENCING THAT PRODUCT
       ON ( SRECSX.SEARCH_STRUCTURE_FK = ES.ENTITY_STRUCTURE_ID 
       AND  SRECSX.STG_RECORD_FK IN (  SELECT STG_RECORD_ID 
                                       FROM STAGE.STG_RECORD WITH (NOLOCK)
                                       WHERE JOB_FK <> SREC.JOB_FK
                                       AND   ENTITY_CLASS_CR_FK = SREC.ENTITY_CLASS_CR_FK ) )
    WHERE SREC.JOB_FK = @IN_JOB_FK
    AND   SREC.BATCH_NO = @IN_BATCH_NO 
    AND   SRECSX.STG_RECORD_STRUCTURE_ID IS NULL
    

--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------


	CREATE CLUSTERED INDEX IDX_TSBR_SSF ON #TS_BATCH_REMOVE
	(   SEARCH_STRUCTURE_FK	ASC )
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)
    


--------------------------------------------------------------------------------------------
--  DELETE FROM PRODUCT TABLES ONLY IF NOT USED IN ANOTHER IMPORT
--------------------------------------------------------------------------------------------



    DELETE
    FROM [epacube].[ENTITY_IDENTIFICATION] 
    WHERE ENTITY_STRUCTURE_FK IN 
     ( SELECT SEARCH_STRUCTURE_FK
       FROM #TS_BATCH_REMOVE )
       

    DELETE
    FROM [epacube].[ENTITY_MULT_IDENT] 
    WHERE ENTITY_STRUCTURE_FK IN 
     ( SELECT SEARCH_STRUCTURE_FK
       FROM #TS_BATCH_REMOVE )


    DELETE
    FROM [epacube].[ENTITY_NONUNIQUE_IDENT]
    WHERE ENTITY_STRUCTURE_FK IN 
     ( SELECT SEARCH_STRUCTURE_FK
       FROM #TS_BATCH_REMOVE )
       

    DELETE
    FROM [epacube].[ENTITY_STRUCTURE]
    WHERE ENTITY_STRUCTURE_ID IN 
     ( SELECT SEARCH_STRUCTURE_FK
       FROM #TS_BATCH_REMOVE )



--drop temp table if it exists
	IF object_id('tempdb..#TS_BATCH_REMOVE') is not null
	   drop table #TS_BATCH_REMOVE





------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.NEW_ENTITY_STRUCTURE_REMOVAL'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.NEW_ENTITY_STRUCTURE_REMOVAL has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END






































































































































































