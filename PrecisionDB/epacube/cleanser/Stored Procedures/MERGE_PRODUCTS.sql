﻿




CREATE PROCEDURE [cleanser].[MERGE_PRODUCTS] (
      @in_search_job_fk      int,
      @in_from_search_fk     int,
      @in_to_search_fk       int,
      @in_update_user        VARCHAR(64)
   )
   AS
BEGIN

    UPDATE CLEANSER.SEARCH_RESULTS_BEST
    SET ACTIVITY_ACTION_CR_FK = 145
       ,PARENT_FROM_PRODUCT_FK = @in_from_search_fk
       ,UPDATE_TIMESTAMP = GETDATE()
       ,UPDATE_USER = @in_update_user
    WHERE SEARCH_JOB_FK = @in_search_job_fk 
    AND   FROM_SEARCH_FK IN (
			  SELECT FROM_SEARCH_FK
			  FROM  CLEANSER.FROM_TO_MATCH WITH (NOLOCK)
			  WHERE TO_SEARCH_FK = @in_to_search_fk )
    

END





