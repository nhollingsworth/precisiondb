﻿





CREATE PROCEDURE [cleanser].[REMOVE_MATCH_SCISSOR] (
      @in_job_fk            BIGINT, 
      @in_batch_no          BIGINT,
      @in_update_user       VARCHAR(64)
   )
   AS
BEGIN


  UPDATE STAGE.STG_RECORD
  SET BATCH_NO = @IN_BATCH_NO
  WHERE STG_RECORD_ID IN (
			SELECT BEST_SEARCH_FK
            FROM cleanser.SEARCH_JOB_RESULTS_BEST WITH (NOLOCK)
			WHERE  search_job_fk = @IN_JOB_FK
		    AND    batch_no = @in_batch_no   )

---- RESOLVER ACTIVITIES ( 0, 1, 2 )			

   UPDATE cleanser.SEARCH_JOB_RESULTS_BEST 
   SET from_search_fk = NULL
      ,ACTIVITY_ACTION_CR_FK = 140    -- RESET TO UNRESOLVED
   WHERE  search_job_fk = @IN_JOB_FK
   AND    batch_no = @in_batch_no
   AND    search_activity_fk BETWEEN 0 AND 2     

   delete from cleanser.search_job_from_to_match 
   where TO_SEARCH_FK IN (        
			SELECT BEST_SEARCH_FK
            FROM cleanser.SEARCH_JOB_RESULTS_BEST WITH (NOLOCK)
			WHERE  search_job_fk = @IN_JOB_FK
		    AND    batch_no = @in_batch_no   
			AND    search_activity_fk BETWEEN 0 AND 2  )


---- CLEANSER ACTIVITIES 

   delete from cleanser.search_job_from_to_match 
   where TO_SEARCH_FK IN (        
			SELECT BEST_SEARCH_FK
            FROM cleanser.SEARCH_JOB_RESULTS_BEST WITH (NOLOCK)
			WHERE  search_job_fk = @IN_JOB_FK
		    AND    batch_no = @in_batch_no   
            AND    search_activity_fk NOT BETWEEN 0 AND 2 )
       
       
	


END;












