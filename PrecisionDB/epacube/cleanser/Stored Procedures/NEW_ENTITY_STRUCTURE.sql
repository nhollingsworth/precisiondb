﻿















-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: For Stg Records submitted to Synhcronizer
--			Create any missing entities associated with them
--			Insert all Identifications also associated with them
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/28/2008   Initial SQL Version
-- CV        12/17/2009   Added select for parent entity stucture
-- CV        05/03/2010   Commented Epacube_search
-- CV        10/05/2013   Add entity data that is not the primary ident in package
-- CV        04/28/2017   Added insert to segments customer table

CREATE PROCEDURE [cleanser].[NEW_ENTITY_STRUCTURE] 
       @IN_JOB_FK         BIGINT
      ,@IN_BATCH_NO       BIGINT       
AS
BEGIN


/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   Before a Stg Entity can leave cleanser it must have an entity_structure_fk assigned
--   and its UNIQUE identifiers established.  This is to avoid the next import from creating
--	 a duplicate of the same entity prior to this entities events being approved.
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--	 All Import Columns referencing Entities MUST be setup in the IMPORT_MAP_METADATA table
--	 with a data_name of data_set ENTITY_STRUCTURE with its REQUIRED Identification Data Name
--   and its option Data Association Data Name
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   STAGE.LOAD_ENTITIES
--   STAGE.LOAD_ENTITY_ENTITY_NAMES
--   Data inserted into Stage.STG_RECORD
--   cleanser.SEARCH_ENTITY_RESULT_MATCH_IDENT
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--   New "ROW" Entities Must be established first in order to create all needed entity_structure_fk's 
--	 To ensure the entity hierarchy is maintained properly; Insert Level 1's first then others
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   Next Create any missing "ASSOCIATION" Entities known by their UNIQUE identifiers 
-------------------------------------------------------------------------------------------------
--   PROCESS STEP3: 
--   
-- 
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint
DECLARE @v_search_activity	 int

DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int




BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.NEW_ENTITY_STRUCTURE', 
                                                @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no,
												@exec_id = @l_exec_no OUTPUT;


SET @l_sysdate = GETDATE()

------------------------------------------------------------------------------------------
--   NEW ENTITY EVENTS --- >>>>  CREATE ENTITY_STRUCTURE ( LEVEL 1 FIRST )
------------------------------------------------------------------------------------------

            SET @status_desc =  'cleanser.NEW_ENTITY_STRUCTURE: inserting - '
                                + 'ENTITY_STRUCTURE'
                                + ' '
                                + 'ALL LEVELS'

            Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;



	INSERT INTO epacube.ENTITY_STRUCTURE
           (ENTITY_STATUS_CR_FK
           ,DATA_NAME_FK
           ,LEVEL_SEQ
           ,PARENT_ENTITY_STRUCTURE_FK
           ,ENTITY_CLASS_CR_FK
           ,IMPORT_FILENAME
           ,RECORD_NO
,JOB_FK   ---CINDY ADDED
           ,RECORD_STATUS_CR_FK
           ,CREATE_TIMESTAMP
           ,UPDATE_TIMESTAMP
           )
	SELECT 
           ( SELECT CODE_REF_ID
             FROM EPACUBE.CODE_REF  WITH (NOLOCK)
             WHERE CODE_TYPE = 'ENTITY_STATUS'
             AND   CODE = SREC.ENTITY_STATUS )
		  ,SREC.ENTITY_DATA_NAME_FK
		  ,SREC.LEVEL_SEQ
          ,SREC.PARENT_STRUCTURE_FK ---CINDY 2/26
--,(select ei.entity_structure_fk from epacube.entity_identification ei with (nolock)
--			where ei.data_name_fk = srec.parent_id_data_name_Fk
--			and ei.entity_data_name_fk = srec.parent_data_name_fk
--			and ei.value = srec.parent_id_value)   --- [PARENT_ENTITY_STRUCTURE_FK]
		  ,SREC.ENTITY_CLASS_CR_FK
		  ,SREC.IMPORT_FILENAME
		  ,SREC.RECORD_NO
,SREC.JOB_FK
          ,1  -- RECORD STATUS
          ,@L_SYSDATE
          ,@L_SYSDATE
	  FROM [stage].[STG_RECORD] SREC  WITH (NOLOCK)
      WHERE SREC.JOB_FK = @IN_JOB_FK
      AND   SREC.BATCH_NO = @IN_BATCH_NO
	  AND   SREC.EVENT_CONDITION_CR_FK <> 99      
      AND   SREC.ACTIVITY_ACTION_CR_FK = 144		
--      AND   SREC.LEVEL_SEQ = 1      
      AND   NOT EXISTS
           ( SELECT 1
             FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
             WHERE ES.JOB_FK = SREC.JOB_FK
             AND   ES.IMPORT_FILENAME = SREC.IMPORT_FILENAME
             AND   ES.RECORD_NO = SREC.RECORD_NO 
             AND   ES.DATA_NAME_FK = SREC.ENTITY_DATA_NAME_FK )





------------------------------------------------------------------------------------------
--   NEW ENTITY ALL LEVELS  --- >>>>  UPDATE STG_RECORD WITH NEW EVENT_STRUCTURE
------------------------------------------------------------------------------------------


            SET @status_desc =  'cleanser.NEW_ENTITY_STRUCTURE: updating - '
                                + 'ENTITY_STRUCTURE_FK'
                                + ' '
                                + 'ALL LEVELS'

            Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


		INSERT INTO STAGE.STG_RECORD_STRUCTURE
		(  STG_RECORD_FK
		  ,JOB_FK
		  ,SEARCH_STRUCTURE_FK
		)
		SELECT 
		   SREC.STG_RECORD_ID
		  ,SREC.JOB_FK
		  ,ES.ENTITY_STRUCTURE_ID
		FROM stage.STG_RECORD SREC WITH (NOLOCK)
		INNER JOIN EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
		  ON ( ES.JOB_FK          = SREC.JOB_FK
		  AND  ES.IMPORT_FILENAME = SREC.IMPORT_FILENAME
		  AND  ES.RECORD_NO       = SREC.RECORD_NO
		  AND  ES.DATA_NAME_FK    = SREC.ENTITY_DATA_NAME_FK )
		WHERE SREC.JOB_FK = @IN_JOB_FK
		AND   SREC.BATCH_NO = @IN_BATCH_NO
		AND   SREC.EVENT_CONDITION_CR_FK <> 99		
        AND   SREC.ACTIVITY_ACTION_CR_FK = 144				
--		AND   SREC.LEVEL_SEQ = 1
		AND   NOT EXISTS 
		       ( SELECT 1
		         FROM STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
		         WHERE SRECS.STG_RECORD_FK = SREC.STG_RECORD_ID
		         AND   SRECS.SEARCH_STRUCTURE_FK = ES.ENTITY_STRUCTURE_ID )

-----------------------------------------------------------------------------------------------------
--- insert entity if insert missing is flagged and entity is new
-----------------------------------------------------------------------------------------------------
--	INSERT INTO epacube.ENTITY_STRUCTURE
--           (ENTITY_STATUS_CR_FK
--           ,DATA_NAME_FK
--           ,LEVEL_SEQ
--           ,PARENT_ENTITY_STRUCTURE_FK
--           ,ENTITY_CLASS_CR_FK
--           ,IMPORT_FILENAME
--           ,RECORD_NO
--,JOB_FK   ---CINDY ADDED
--           ,RECORD_STATUS_CR_FK
--           ,CREATE_TIMESTAMP
--           ,UPDATE_TIMESTAMP
--           )
--	SELECT 
--           ( SELECT CODE_REF_ID
--             FROM EPACUBE.CODE_REF  WITH (NOLOCK)
--             WHERE CODE_TYPE = 'ENTITY_STATUS'
--             AND   CODE = SREC.ENTITY_STATUS )
--		  ,SREC.ENTITY_DATA_NAME_FK
--		  ,SREC.LEVEL_SEQ
--          ,SREC.PARENT_STRUCTURE_FK ---CINDY 2/26
----,(select ei.entity_structure_fk from epacube.entity_identification ei with (nolock)
----			where ei.data_name_fk = srec.parent_id_data_name_Fk
----			and ei.entity_data_name_fk = srec.parent_data_name_fk
----			and ei.value = srec.parent_id_value)   --- [PARENT_ENTITY_STRUCTURE_FK]
--		  ,SREC.ENTITY_CLASS_CR_FK
--		  ,SREC.IMPORT_FILENAME
--		  ,SREC.RECORD_NO
--,SREC.JOB_FK
--          ,1  -- RECORD STATUS
--          ,@L_SYSDATE
--          ,@L_SYSDATE
--	  FROM [stage].[STG_RECORD] SREC  WITH (NOLOCK)
--	  inner join epacube.stage.STG_RECORD_ENTITY_IDENT SREI WITH (NOLOCK)
--	  on (SREC.STG_RECORD_ID = SREI.STG_RECORD_FK)
--      WHERE SREC.JOB_FK = @IN_JOB_FK
--      AND   SREC.BATCH_NO = @IN_BATCH_NO
--	  AND   SREC.EVENT_CONDITION_CR_FK <> 99      
--      AND   SREC.ACTIVITY_ACTION_CR_FK = 144		
----      AND   SREC.LEVEL_SEQ = 1      
--      AND   NOT EXISTS
--           ( SELECT 1
--             FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
--             WHERE ES.JOB_FK = SREC.JOB_FK
--             AND   ES.IMPORT_FILENAME = SREC.IMPORT_FILENAME
--             AND   ES.RECORD_NO = SREC.RECORD_NO 
--             AND   ES.DATA_NAME_FK = SREC.ENTITY_DATA_NAME_FK )

			
--select * from stage.STG_RECORD_ENTITY_IDENT


------	
------
------------------------------------------------------------------------------------------------
--------   NEW ENTITY EVENTS --- >>>>  CREATE EVENT_STRUCTURE ( ALL LEVELS > 1 )
--------      IMPORTANT !!!!!!
--------		GO AHEAD AND INSERT IF THE PARENT IS UNKNOWN
--------		FLAG WITH ERROR ( CONDITION = RED ) AND LEAVE IN CLEANSER
------------------------------------------------------------------------------------------------
------
------
------            SET @status_desc =  'cleanser.NEW_ENTITY_STRUCTURE: inserting - '
------                                + 'ENTITY_STRUCTURE'
------                                + ' '
------                                + 'LEVEL > 1'
------
------            Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
------
------
------	INSERT INTO epacube.ENTITY_STRUCTURE
------           (ENTITY_STATUS_CR_FK
------           ,DATA_NAME_FK
------           ,LEVEL_SEQ
------           ,PARENT_ENTITY_STRUCTURE_FK
------           ,ENTITY_CLASS_CR_FK
------           ,IMPORT_FILENAME
------           ,RECORD_NO
------           ,RECORD_STATUS_CR_FK
------           ,CREATE_TIMESTAMP
------           ,UPDATE_TIMESTAMP
------           )
------	SELECT 
------           ( SELECT CODE_REF_ID
------             FROM EPACUBE.CODE_REF  WITH (NOLOCK)
------             WHERE CODE_TYPE = 'ENTITY_STATUS'
------             AND   CODE = SREC.ENTITY_STATUS )
------		  ,[ENTITY_DATA_NAME_FK]
------		  ,[LEVEL_SEQ]
------		  ---- FIRST SEE IF EXISTS IN EPACUBE
------		  ---- THEN SEE IF PARENT CREATED AS PART OF IMPORT 
------          ---- ELSE NULL 
------		  ,ISNULL ( 
------				( SELECT EI.ENTITY_STRUCTURE_FK
------				  FROM EPACUBE.ENTITY_IDENTIFICATION EI
------				  WHERE EI.ENTITY_DATA_NAME_FK = SREC.PARENT_DATA_NAME_FK
------				  AND   EI.DATA_NAME_FK = SREC.PARENT_ID_DATA_NAME_FK
------				  AND   EI.VALUE = SREC.PARENT_ID_VALUE
------				 )
------				, 
------				( SELECT SRECP.SEARCH_STRUCTURE_FK
------				  FROM STAGE.STG_RECORD_IDENT SRECIP WITH (NOLOCK)
------				  INNER JOIN STAGE.STG_RECORD SRECP WITH (NOLOCK)
------					ON ( SRECP.STG_RECORD_ID = SRECIP.STG_RECORD_FK 
------					AND  SRECP.ENTITY_DATA_NAME_FK = SREC.PARENT_DATA_NAME_FK )
------				  WHERE SRECIP.ENTITY_DATA_NAME_FK = SREC.PARENT_DATA_NAME_FK
------				  AND   SRECIP.ID_DATA_NAME_FK = SREC.PARENT_ID_DATA_NAME_FK
------				  AND   SRECIP.ID_VALUE = SREC.PARENT_ID_VALUE
------				 )
------			)
------		  ,[ENTITY_CLASS_CR_FK]
------		  ,[IMPORT_FILENAME]
------		  ,[RECORD_NO]
------          ,1  -- RECORD STATUS
------          ,@L_SYSDATE
------          ,@L_SYSDATE
------	  FROM [stage].[STG_RECORD] SREC WITH (NOLOCK)
------      WHERE BATCH_NO = @IN_BATCH_NO
------      AND   EVENT_ACTION_CR_FK = 54 
------      AND   EVENT_CONDITION_CR_FK IN ( 97, 98 )      
------      AND   SEARCH_STRUCTURE_FK IS NULL
------      AND   LEVEL_SEQ > 1 
------
------
------
------------------------------------------------------------------------------------------------
--------   NEW ENTITY EVENTS --- >>>>  UPDATE STG_RECORD WITH NEW EVENT_STRUCTURE
------------------------------------------------------------------------------------------------
------
------
------            SET @status_desc =  'cleanser.NEW_ENTITY_STRUCTURE: updating - '
------                                + 'ENTITY_STRUCTURE_FK'
------                                + ' '
------                                + 'LEVEL > 1'
------
------            Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
------
------
------
------	  UPDATE  stage.STG_RECORD
------      SET SEARCH_STRUCTURE_FK = 
------					( SELECT ENTITY_STRUCTURE_ID
------                      FROM EPACUBE.ENTITY_STRUCTURE
------                      WHERE IMPORT_FILENAME = stage.STG_RECORD.IMPORT_FILENAME
------                      AND   RECORD_NO = stage.STG_RECORD.RECORD_NO 
------                      AND   DATA_NAME_FK = stage.STG_RECORD.ENTITY_DATA_NAME_FK )                      
------         ,PARENT_STRUCTURE_FK =
------					( SELECT PARENT_ENTITY_STRUCTURE_FK
------                      FROM EPACUBE.ENTITY_STRUCTURE
------                      WHERE IMPORT_FILENAME = stage.STG_RECORD.IMPORT_FILENAME
------                      AND   RECORD_NO = stage.STG_RECORD.RECORD_NO 
------                      AND   DATA_NAME_FK = stage.STG_RECORD.PARENT_DATA_NAME_FK )                      
------      WHERE BATCH_NO = @IN_BATCH_NO
------      AND   EVENT_ACTION_CR_FK = 54 
------      AND   EVENT_CONDITION_CR_FK IN ( 97, 98 )      
------      AND   SEARCH_STRUCTURE_FK IS NULL
------      AND   LEVEL_SEQ > 1
------
------


------------------------------------------------------------------------------------------
--   FOR ALL NEW ENTITIES --- >>>>  INSERT ENTITY UNIQUE IDENTIFICATIONS
------------------------------------------------------------------------------------------
		

	INSERT INTO [epacube].[ENTITY_IDENTIFICATION]
			   ([ENTITY_STRUCTURE_FK]
			   ,[ENTITY_DATA_NAME_FK]
			   ,[DATA_NAME_FK]
			   ,[VALUE]
			   )
	SELECT 
			    ES.ENTITY_STRUCTURE_ID
			   ,SRECI.ENTITY_DATA_NAME_FK
			   ,SRECI.ID_DATA_NAME_FK
			   ,SRECI.ID_VALUE
		  FROM STAGE.STG_RECORD SREC WITH (NOLOCK) 
		  INNER JOIN EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
			  ON ( ES.JOB_FK          = SREC.JOB_FK
			  AND  ES.IMPORT_FILENAME = SREC.IMPORT_FILENAME
			  AND  ES.RECORD_NO       = SREC.RECORD_NO
			  AND  ES.DATA_NAME_FK    = SREC.ENTITY_DATA_NAME_FK )		  
		  INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
			 ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID 
			 AND  SRECI.ENTITY_DATA_NAME_FK = SREC.ENTITY_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
		     ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
		     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		  WHERE SREC.JOB_FK = @IN_JOB_FK
		  AND   SREC.BATCH_NO = @IN_BATCH_NO
		  AND   DS.TABLE_NAME = 'ENTITY_IDENTIFICATION'
	      AND   NOT EXISTS
	            ( SELECT 1 
	              FROM EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
	              WHERE EI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
	              AND   EI.ENTITY_DATA_NAME_FK = SRECI.ENTITY_DATA_NAME_FK
	              AND   EI.VALUE = SRECI.ID_VALUE
	            )




------------------------------------------------------------------------------------------
--   FOR ALL NEW ENTITIES --- >>>>  INSERT ENTITY UNIQUE IDENT MULT
------------------------------------------------------------------------------------------
		

	INSERT INTO [epacube].[ENTITY_IDENT_MULT]
			   ([ENTITY_STRUCTURE_FK]
			   ,[ENTITY_DATA_NAME_FK]
			   ,[DATA_NAME_FK]
			   ,[VALUE]
			   )
	SELECT
			    ES.ENTITY_STRUCTURE_ID
			   ,SRECI.ENTITY_DATA_NAME_FK
			   ,SRECI.ID_DATA_NAME_FK
			   ,SRECI.ID_VALUE
		  FROM STAGE.STG_RECORD SREC WITH (NOLOCK) 
		  INNER JOIN EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
			  ON ( ES.JOB_FK          = SREC.JOB_FK
			  AND  ES.IMPORT_FILENAME = SREC.IMPORT_FILENAME
			  AND  ES.RECORD_NO       = SREC.RECORD_NO
			  AND  ES.DATA_NAME_FK    = SREC.ENTITY_DATA_NAME_FK )		  
		  INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
			 ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID 
			 AND  SRECI.ENTITY_DATA_NAME_FK = SREC.ENTITY_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
		     ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
		     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		  WHERE SREC.JOB_FK = @IN_JOB_FK
		  AND   SREC.BATCH_NO = @IN_BATCH_NO
		  AND   DS.TABLE_NAME = 'ENTITY_IDENT_MULT'
	      AND   NOT EXISTS
	            ( SELECT 1 
	              FROM EPACUBE.ENTITY_IDENT_MULT EI WITH (NOLOCK)
	              WHERE EI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
	              AND   EI.VALUE = SRECI.ID_VALUE
	            )
	


------------------------------------------------------------------------------------------
--   FOR ALL NEW ENTITIES --- >>>>  INSERT ENTITY UNIQUE IDENT MULT
------------------------------------------------------------------------------------------
		

	INSERT INTO [epacube].[ENTITY_IDENT_NONUNIQUE]
			   ([ENTITY_STRUCTURE_FK]
			   ,[ENTITY_DATA_NAME_FK]
			   ,[DATA_NAME_FK]
			   ,[VALUE]
			   )
	SELECT
			    ES.ENTITY_STRUCTURE_ID
			   ,SRECI.ENTITY_DATA_NAME_FK
			   ,SRECI.ID_DATA_NAME_FK
			   ,SRECI.ID_VALUE
		  FROM STAGE.STG_RECORD SREC WITH (NOLOCK) 
		  INNER JOIN EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
			  ON ( ES.JOB_FK          = SREC.JOB_FK
			  AND  ES.IMPORT_FILENAME = SREC.IMPORT_FILENAME
			  AND  ES.RECORD_NO       = SREC.RECORD_NO
			  AND  ES.DATA_NAME_FK    = SREC.ENTITY_DATA_NAME_FK )		  
		  INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
			 ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID 
			 AND  SRECI.ENTITY_DATA_NAME_FK = SREC.ENTITY_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
		     ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
		     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		  WHERE SREC.JOB_FK = @IN_JOB_FK
		  AND   SREC.BATCH_NO = @IN_BATCH_NO
		  AND   DS.TABLE_NAME = 'ENTITY_IDENT_NONUNIQUE'
	      AND   NOT EXISTS
	            ( SELECT 1 
	              FROM EPACUBE.ENTITY_IDENT_NONUNIQUE EI WITH (NOLOCK)
	              WHERE EI.ENTITY_STRUCTURE_FK = ES.ENTITY_STRUCTURE_ID
	              AND   EI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
	              AND   EI.VALUE = SRECI.ID_VALUE
	            )
	

------------------------------------------------------------------------------
-----add insert to new segements customer table for new structure
-------------------------------------------------------------------------------

INSERT INTO [epacube].[SEGMENTS_CUSTOMER]
           ([CUST_ENTITY_STRUCTURE_FK]
           ,[ENTITY_DATA_NAME_FK]
           ,[DATA_NAME_FK]
           ,[CUST_SEGMENT_FK]
           ,[CUSTOMER_PRECEDENCE]          
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[CREATE_USER]
         )
		 SELECT 
			    ES.ENTITY_STRUCTURE_ID
			   ,SRECI.ENTITY_DATA_NAME_FK
			   ,SRECI.ID_DATA_NAME_FK
			   ,ES.ENTITY_STRUCTURE_ID
			   ,10
			   ,1
			   ,getdate()
			   ,'IMPORT'
		  FROM STAGE.STG_RECORD SREC WITH (NOLOCK) 
		  INNER JOIN EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
			  ON ( ES.JOB_FK          = SREC.JOB_FK
			  AND  ES.IMPORT_FILENAME = SREC.IMPORT_FILENAME
			  AND  ES.RECORD_NO       = SREC.RECORD_NO
			  AND  ES.DATA_NAME_FK    = SREC.ENTITY_DATA_NAME_FK )		  
		  INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
			 ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID 
			 AND  SRECI.ENTITY_DATA_NAME_FK = SREC.ENTITY_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
		     ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
		     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		  WHERE SREC.JOB_FK = @IN_JOB_FK
		  AND   SREC.BATCH_NO = @IN_BATCH_NO
		  AND  SRECI.ID_DATA_NAME_FK = 144111
	      AND   NOT EXISTS
	            ( SELECT 1 
	              FROM EPACUBE.SEGMENTS_CUSTOMER EI WITH (NOLOCK)
	              WHERE EI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
	              AND   EI.ENTITY_DATA_NAME_FK = SRECI.ENTITY_DATA_NAME_FK
	              AND   EI.CUST_SEGMENT_FK = ES.ENTITY_STRUCTURE_ID
	            )




---- -- ------------------------------------------------------------------------------------
---- --       IF a Stg Record is a child level seq > 1 and no parent assigned 
---- --			Then flag as RED Condition and log an error.
---- -- -----------------------------------------------------------------------------------
---- --*/
----			   
----			   
----			   INSERT INTO stage.STG_RECORD_ERRORS
----                        (job_fk,
----						 batch_no,
----						 STG_RECORD_fk,
----                         error_name,
----						 data_name_fk, error_data1 ,
----                         error_data2 ,error_data3,
----                         event_condition_cr_fk )
----               SELECT  DISTINCT
----                       SREC.JOB_FK
----					  ,@in_batch_no
----			   		  ,SREC.STG_RECORD_id
----					  ,'No Parent Entity is assigned to this Staged Entity' error_name
----					  ,dn.name
----                      ,SREC.ENTITY_ID_VALUE
----					  ,null error_data2
----					  ,null error_data3
----                      ,99
----			   FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
----			   INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = SREC.ENTITY_DATA_NAME_FK )
----               WHERE SREC.BATCH_NO = @IN_BATCH_NO
----				AND   LEVEL_SEQ > 1
----				AND   PARENT_STRUCTURE_FK IS NULL 
----
----
----				UPDATE stage.STG_RECORD
----				SET EVENT_CONDITION_CR_FK = 99
----				   ,EVENT_ACTION_CR_FK = NULL
----				WHERE BATCH_NO = @IN_BATCH_NO
----				AND   LEVEL_SEQ > 1
----				AND   PARENT_STRUCTURE_FK IS NULL 




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.NEW_ENTITY_STRUCTURE'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.NEW_ENTITY_STRUCTURE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END











































































































































































