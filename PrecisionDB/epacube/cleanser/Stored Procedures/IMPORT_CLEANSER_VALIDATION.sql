﻿










-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: For DUPLICATE DATA CHECK
--			
--			
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        06/02/2010   Initial SQL Version
-- CV        09/09/2010   Tightened down where clause because of exclusion logic


CREATE PROCEDURE [cleanser].[IMPORT_CLEANSER_VALIDATION] 
       @IN_JOB_FK         BIGINT ,
       @IN_BATCH_NO       BIGINT
AS
BEGIN



DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint
DECLARE @v_import_package_fk   int  
DECLARE @V_UNIQUE1   varchar(25) 
DECLARE @V_UNIQUE2   varchar(25) 
DECLARE @V_UNIQUE3   varchar(25) 
DECLARE @v_filename_count INT
DECLARE @epa_job_id			 INT


DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int




BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.IMPORT_CLEANSER_VALIDATION', 
                                                @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no,
												@exec_id = @l_exec_no OUTPUT;
												
SET @l_sysdate = GETDATE()


SET @v_import_package_fk = (select distinct import_package_fk 
							from import.import_record_data WITH (NOLOCK)
							where  job_fk = @in_job_fk)

--LOOKUP WHAT MAKES THE ROW UNIQUE BY PACKAGE

SET @V_UNIQUE1 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 1),-999)

SET @V_UNIQUE2 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 2),-999)

SET @V_UNIQUE3 = ISNULL((select column_name from import.import_map_metadata
where import_package_fk = @v_import_package_fk
and unique_seq_ind = 3), -999)


 -- ------------------------------------------------------------------------------------
 --			CHECK FOR DUPLICATE UNIQUE RECORDS WITHIN SAME IMPORT JOB
 --			IF THEY EXIST SET STG_RECORD TO RED 
 --			NOTE:  THIS VALIDATION HAS TO CHECK ENTIRE IMPORT JOB
 -- -----------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--   INSERT YELLOW ERROR MESSAGE FOR DUPLICATE DATA FROM THE SAME FILE
------------------------------------------------------------------------------------------


INSERT INTO [stage].[STG_RECORD_ERRORS]
           ([STG_RECORD_FK]
			,STG_RECORD_IDENT_FK
           ,[JOB_FK]
           ,[ERROR_NAME]
           ,[DATA_NAME_FK]
           ,[ERROR_DATA1]
           ,[ERROR_DATA2]
           ,[ERROR_DATA3]
           ,[EVENT_CONDITION_CR_FK]
           ,[BATCH_NO]
           ,[UPDATE_TIMESTAMP])
SELECT 
           SREC.STG_RECORD_ID
		  ,SRECI.STG_RECORD_IDENT_ID
          ,SREC.JOB_FK
          ,'DUPLICATE ROW OF DATA WITHIN SAME FILE '          
           ,NULL
          ,NULL --DN.LABEL             
          ,SRECI.ID_VALUE                
          ,NULL
          ,98    --EVENT_CONDITION_CR_FK
          ,@IN_BATCH_NO
          ,GETDATE()

FROM IMPORT.IMPORT_RECORD_DATA IRD WITH (NOLOCK)
	INNER JOIN STAGE.STG_RECORD SREC
ON (SREC.RECORD_NO = IRD.RECORD_NO
AND SREC.BATCH_NO = @IN_BATCH_NO
AND SREC.JOB_FK = @IN_JOB_FK)
INNER JOIN STAGE.STG_RECORD_IDENT SRECI
ON (SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID)
WHERE IRD.DO_NOT_IMPORT_IND = 1 
AND IRD.SEQ_ID IS NULL 
 AND IRD.JOB_FK = @IN_JOB_FK





------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.IMPORT_CLEANSER_VALIDATION'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk,  @epa_batch_no = @in_batch_no
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.IMPORT_CLEANSER_VALIDATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END




































































































































































































