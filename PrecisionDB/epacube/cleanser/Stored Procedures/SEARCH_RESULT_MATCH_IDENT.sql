﻿














-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/20/2008   Intital Version
-- CV	     09/30/2009   Only match on active rules 
-- CV        09/17/2013   entity ident mult commented out code

CREATE PROCEDURE [cleanser].[SEARCH_RESULT_MATCH_IDENT] 
       @IN_JOB_FK                     bigint
      ,@IN_BATCH_NO                   bigint 
      ,@IN_SEARCH_ACTIVITY_FK         int

   AS
BEGIN



/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This procedure is called from both LOAD_ENTITIES and RESOLVER_REPROCESS_ENTITY
--
--   Its purpose is to attempt to match ONLY the Import Entity Data Name with its 
--   epaCUBE Entity Data Name utilizing the available Imported Identifiers 
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   Resolver Activity is ALWAYS SEEDED to be 1 for Products and 2 for Entities
--	 For Resolver Activities ONLY the job_fk is a parameter;  for cleasner activies
--	 it resides on the Search Activity itself.. ( will want to change that in the future )
--
--   In Cleanser a Display direction is setup for each Activity (see @v_from_data_set & @v_to_data_set) 
--   This direction determines how the data is displayed in the UI and also drives how this
--   procedure MUST insert the matching results into the tables for PROPER display.
--   The default direction currently is (FROM) EPACUBE --> (TO) STAGE to accomodate the UI.
--		The UI ONLY WORKS in one direction right now.  (FROM) EPACUBE --> (TO) STAGE.
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Every Entity Structure's Stg_Entity_Identification has been logged and only the rows
--   whose data name is a UNIQUE Entity Identifier have been matched at this time.
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--   Remove any previously inserted [cleanser].[SEARCH_RESULTS_IDENT] by Job_ID 
--		for all UNMATCHED Entity_Structure_FK's
--   If any Stg_Entity_Identification have a NULL entity_structure_fk and its data name is
--   a UNIQUE Entity Identifier then try again to update the entity_structure_fk
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   Insert any missing STG_Entity_Identification Rows Unique and Non Unique matching
--   into [cleanser].[SEARCH_RESULTS_IDENT] for the Stg_Entity_Structure's Entity Data Name Only.
-------------------------------------------------------------------------------------------------
--   PROCESS STEP3: 
--   If for each Stg_Entity_Structure there is ONLY 1 matching entity_structure_fk in 
--   [cleanser].[SEARCH_RESULTS_IDENT] then mark as MATCHED;  if No entity_structure_fk's 
--	 found at all then mark with the ACTIVITY_ACTION_CR_FK default in the package
-------------------------------------------------------------------------------------------------
--   PROCESS STEP4: 
--   Update Stg Entity Structure with the appropriate entity_structure_fk; stage status 
--   and event action
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   Currently ONLY writing the RESOLVER approach and utilizing the current UI flow
--	 which is (FROM) EPACUBE --> (TO) STAGE becasue that is the ONLY direction the UI works with.
--   When we determine we need to allow for more flexibiltiy we will address it then.
--
--   AWAITING DECENT REQUIREMENTS FROM JAN AND COMPANY.... 
--		DO NOT BELIEVE THAT WE SHOULD AUTO MATCH ON NONUNIQE AT ALL DURING RESOLVER.
--
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/




      DECLARE @l_exec_no   bigint,
              @l_stmt      varchar(8000),
              @ls_exec     varchar(8000),
              @l_sysdate   datetime

		DECLARE @v_count			        INT
		DECLARE @l_rows_processed           INT

		DECLARE @status_desc				varchar (max)
		DECLARE @ErrorMessage				nvarchar(4000)
		DECLARE @ErrorSeverity				int
		DECLARE @ErrorState					int
   

SET NOCOUNT ON;

BEGIN TRY
      SET @l_sysdate = GETDATE()
      SET @l_exec_no = 0;
      SET @status_desc = 
       'started execution of cleanser.SEARCH_JOB_RESULT_MATCH_IDENT' 
       + ' BATCH: '  + CAST (@IN_BATCH_NO AS VARCHAR(16))
       
      EXEC exec_monitor.start_monitor_sp @exec_desc = @status_desc, 
                                                 @epa_job_id = @in_job_fk,  @epa_batch_no = @in_batch_no,
                                                 @exec_id = @l_exec_no OUTPUT;


/*
 -- ------------------------------------------------------------------------------------
 --   Insert search_job_results_ident from the stg_record_ident table
 -- -----------------------------------------------------------------------------------
 --*/

SET @V_COUNT = ( SELECT COUNT(1)
                 FROM STAGE.STG_RECORD WITH (NOLOCK)
                 WHERE BATCH_NO = @IN_BATCH_NO
                 AND   ENTITY_CLASS_CR_FK = (select code_ref_id from epacube.code_ref  WITH (NOLOCK)
											where code_type = 'ENTITY_CLASS'
											and code = 'PRODUCT') )  -- PRODUCTS

       SET @l_rows_processed = @V_COUNT
	   
	   BEGIN
			 SET @status_desc = 'total COUNT PRODUCTS:: '
                                + ' '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END

               
IF @V_COUNT > 0               
BEGIN


/*
 -- ------------------------------------------------------------------------------------
 --   Product Primary Unique Matches -- not org or entity specific
 -- -----------------------------------------------------------------------------------
 --*/

	
INSERT INTO [cleanser].[SEARCH_JOB_RESULTS_IDENT]
           ([SEARCH_JOB_FK]
           ,[BATCH_NO]
           ,[SEARCH_ACTIVITY_FK]
           ,[FROM_SEARCH_FK]
           ,[MATCH_TYPE_CR_FK]           
           ,[TO_SEARCH_FK]
           ,[ID_DATA_NAME_FK]
           ,[ID_VALUE]
           ,[DERIVED_ID_IND]                      
           ,[STG_RECORD_IDENT_FK]           
          )
     SELECT
            A.JOB_FK
           ,@IN_BATCH_NO
           ,@IN_SEARCH_ACTIVITY_FK
           ,A.PRODUCT_STRUCTURE_FK
           ,171   -- PRIMARY
           ,A.STG_RECORD_FK
           ,A.DATA_NAME_FK
		   ,A.VALUE
		   ,CASE ISNULL ( A.EVENT_SOURCE_CR_FK, 0 )
		    WHEN 0 THEN NULL
		    WHEN 72 THEN 1
		    ELSE NULL
		    END 		   
		   ,A.STG_RECORD_IDENT_ID
    FROM (
		SELECT 
		    SREC.JOB_FK
           ,PI.PRODUCT_STRUCTURE_FK
           ,SRECI.STG_RECORD_FK
           ,PI.DATA_NAME_FK
           ,PI.VALUE
		   ,SRECI.EVENT_SOURCE_CR_FK        
		   ,SRECI.STG_RECORD_IDENT_ID           		   
        FROM stage.STG_RECORD SREC WITH (NOLOCK)
        INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
           ON ( SREC.STG_RECORD_ID = SRECI.STG_RECORD_FK
			AND ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL' )
        INNER JOIN EPACUBE.PRODUCT_IDENTIFICATION PI WITH (NOLOCK)
           ON ( PI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
           AND  PI.VALUE        = SRECI.ID_VALUE )
        WHERE SREC.BATCH_NO = @IN_BATCH_NO
        AND   SRECI.ID_DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
                                               FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK)
                                               WHERE EEP.APPLICATION_SCOPE_FK = 100 
                                               AND   EEP.NAME = ( SELECT CODE
                                                                  FROM EPACUBE.CODE_REF WITH (NOLOCK)
                                                                  WHERE CODE_REF_ID = SRECI.ENTITY_CLASS_CR_FK ) )
    ) A

       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			 SET @status_desc = 'total rows PRIMARY MATCH:: '
                                + ' '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END


/*
 -- ------------------------------------------------------------------------------------
 --   Product Unique Matches  -- Include Entity Entity_Structure  and ORG structure
 --		That are not already Primary Matches
 -- -----------------------------------------------------------------------------------
 --*/


	
INSERT INTO [cleanser].[SEARCH_JOB_RESULTS_IDENT]
           ([SEARCH_JOB_FK]
           ,[BATCH_NO]
           ,[SEARCH_ACTIVITY_FK]
           ,[FROM_SEARCH_FK]
           ,[MATCH_TYPE_CR_FK]           
           ,[TO_SEARCH_FK]
           ,[ID_DATA_NAME_FK]
           ,[ID_VALUE]
           ,[DERIVED_ID_IND]           
           ,[STG_RECORD_IDENT_FK]                      
          )
     SELECT
            A.JOB_FK
           ,@IN_BATCH_NO
           ,@IN_SEARCH_ACTIVITY_FK
           ,A.PRODUCT_STRUCTURE_FK
           ,172   -- UNIQUE
           ,A.STG_RECORD_FK
           ,A.DATA_NAME_FK
		   ,A.VALUE
		   ,CASE ISNULL ( A.EVENT_SOURCE_CR_FK, 0 )
		    WHEN 0 THEN NULL
		    WHEN 72 THEN 1
		    ELSE NULL
		    END 		   
		   ,A.STG_RECORD_IDENT_ID		   
    FROM (
		SELECT 
		    SREC.JOB_FK
           ,PI.PRODUCT_STRUCTURE_FK
           ,SRECI.STG_RECORD_FK
           ,PI.DATA_NAME_FK
           ,PI.VALUE
		   ,SRECI.EVENT_SOURCE_CR_FK                   
           ,SRECI.STG_RECORD_IDENT_ID           
        FROM stage.STG_RECORD SREC WITH (NOLOCK)
        INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
           ON ( SREC.STG_RECORD_ID = SRECI.STG_RECORD_FK )
        INNER JOIN EPACUBE.PRODUCT_IDENTIFICATION PI WITH (NOLOCK)
           ON ( PI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
           AND  PI.VALUE        = SRECI.ID_VALUE
           AND  PI.ORG_ENTITY_STRUCTURE_FK = SRECI.ORG_ENTITY_STRUCTURE_FK                        
           AND  ISNULL ( PI.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK , 0 )
           )
----        LEFT JOIN cleanser.SEARCH_JOB_RESULTS_IDENT SJRI WITH (NOLOCK)
----           ON ( SJRI.STG_RECORD_IDENT_FK = SRECI.STG_RECORD_IDENT_ID 
----           AND  SJRI.FROM_SEARCH_FK = PI.PRODUCT_STRUCTURE_FK )
        WHERE SREC.BATCH_NO = @IN_BATCH_NO        
----        AND   SJRI.SEARCH_JOB_RESULTS_IDENT_ID IS NULL 
    UNION ALL
		SELECT 
		    SREC.JOB_FK
           ,PI.PRODUCT_STRUCTURE_FK
           ,SRECI.STG_RECORD_FK
           ,PI.DATA_NAME_FK
           ,PI.VALUE
		   ,SRECI.EVENT_SOURCE_CR_FK                   
           ,SRECI.STG_RECORD_IDENT_ID                      
        FROM stage.STG_RECORD SREC WITH (NOLOCK)
        INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
           ON ( SREC.STG_RECORD_ID = SRECI.STG_RECORD_FK
			AND ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL' )
        INNER JOIN EPACUBE.PRODUCT_IDENT_MULT PI WITH (NOLOCK)
           ON ( PI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
           AND  PI.VALUE        = SRECI.ID_VALUE 
           AND  PI.ORG_ENTITY_STRUCTURE_FK = SRECI.ORG_ENTITY_STRUCTURE_FK 
           AND  ISNULL ( PI.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK , 0 )
           )           
----        LEFT JOIN cleanser.SEARCH_JOB_RESULTS_IDENT SJRI WITH (NOLOCK)
----           ON ( SJRI.STG_RECORD_IDENT_FK = SRECI.STG_RECORD_IDENT_ID 
----           AND  SJRI.FROM_SEARCH_FK = PI.PRODUCT_STRUCTURE_FK )
        WHERE SREC.BATCH_NO = @IN_BATCH_NO
----        AND   SJRI.SEARCH_JOB_RESULTS_IDENT_ID IS NULL 
    ) A

       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			 SET @status_desc = 'total rows UNIQUE MATCH:: '
                                + ' '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END



INSERT INTO [cleanser].[SEARCH_JOB_RESULTS_IDENT]
           ([SEARCH_JOB_FK]
           ,[BATCH_NO]
           ,[SEARCH_ACTIVITY_FK]
           ,[FROM_SEARCH_FK]
           ,[MATCH_TYPE_CR_FK]           
           ,[TO_SEARCH_FK]
           ,[ID_DATA_NAME_FK]
           ,[ID_VALUE]
           ,[DERIVED_ID_IND]           
           ,[STG_RECORD_IDENT_FK]                      
          )          
     SELECT
            A.JOB_FK
           ,@IN_BATCH_NO
           ,@IN_SEARCH_ACTIVITY_FK
           ,A.PRODUCT_STRUCTURE_FK
           ,173   -- STRIPPED UNIQUE
           ,A.STG_RECORD_FK
           ,A.DATA_NAME_FK
		   ,A.STRIPPED_ID_VALUE
		   ,CASE ISNULL ( A.EVENT_SOURCE_CR_FK, 0 )
		    WHEN 0 THEN NULL
		    WHEN 72 THEN 1
		    ELSE NULL
		    END 
		   ,A.STG_RECORD_IDENT_ID		   
    FROM (
		SELECT 
		    SREC.JOB_FK
           ,PI.PRODUCT_STRUCTURE_FK
           ,SRECI.STG_RECORD_FK
           ,PI.DATA_NAME_FK
           ,SRECI.STRIPPED_ID_VALUE
		   ,SRECI.EVENT_SOURCE_CR_FK                   
           ,SRECI.STG_RECORD_IDENT_ID           
        FROM stage.STG_RECORD SREC WITH (NOLOCK)
        INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
           ON ( SREC.STG_RECORD_ID = SRECI.STG_RECORD_FK 
			AND ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL' )
        INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
           ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
        INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
           ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )           
        INNER JOIN EPACUBE.PRODUCT_IDENT_MULT PI WITH (NOLOCK)
           ON ( PI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
           AND  PI.VALUE        = SRECI.ID_VALUE 
           AND  PI.ORG_ENTITY_STRUCTURE_FK = SRECI.ORG_ENTITY_STRUCTURE_FK 
           AND  ISNULL ( PI.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK , 0 )
           )
        WHERE DS.TABLE_NAME = 'PRODUCT_IDENT_MULT'
        AND   SREC.BATCH_NO = @IN_BATCH_NO
    ) A


       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			 SET @status_desc = 'total rows MULT IDENT MATCH:: '
                                + ' '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END



 -- ------------------------------------------------------------------------------------
 --   Product Unique Matches where Entity is Different
 --											Org is Assumed 1 for now and is not included
 --			That are not already Unique Matches
 -- -----------------------------------------------------------------------------------
 --*/


	
INSERT INTO [cleanser].[SEARCH_JOB_RESULTS_IDENT]
           ([SEARCH_JOB_FK]
           ,[BATCH_NO]
           ,[SEARCH_ACTIVITY_FK]
           ,[FROM_SEARCH_FK]
           ,[MATCH_TYPE_CR_FK]           
           ,[TO_SEARCH_FK]
           ,[ID_DATA_NAME_FK]
           ,[ID_VALUE]
           ,[ID_ENTITY_DATA_NAME_FK]
           ,[TO_ID_ENTITY_STRUCTURE_FK]                      
           ,[FROM_ID_ENTITY_STRUCTURE_FK]        
           ,[DERIVED_ID_IND]                                               
           ,[STG_RECORD_IDENT_FK]                      
          )
     SELECT
            A.JOB_FK
           ,@IN_BATCH_NO
           ,@IN_SEARCH_ACTIVITY_FK
           ,A.PRODUCT_STRUCTURE_FK
           ,175   -- DIFF ENT UNIQUE
           ,A.STG_RECORD_FK
           ,A.DATA_NAME_FK
		   ,A.VALUE
		   ,A.ID_ENTITY_DATA_NAME_FK
		   ,A.TO_ID_ENTITY_STRUCTURE_FK	  
		   ,A.FROM_ID_ENTITY_STRUCTURE_FK 	
		   ,CASE ISNULL ( A.EVENT_SOURCE_CR_FK, 0 )
		    WHEN 0 THEN NULL
		    WHEN 72 THEN 1
		    ELSE NULL
		    END 		   	   
		   ,A.STG_RECORD_IDENT_ID	
    FROM (
		SELECT 
		    SREC.JOB_FK
           ,PI.PRODUCT_STRUCTURE_FK
           ,SRECI.STG_RECORD_FK
           ,PI.DATA_NAME_FK
           ,PI.VALUE
           ,SRECI.ID_ENTITY_DATA_NAME_FK
           ,SRECI.ID_ENTITY_STRUCTURE_FK AS TO_ID_ENTITY_STRUCTURE_FK
           ,PI.ENTITY_STRUCTURE_FK AS FROM_ID_ENTITY_STRUCTURE_FK 
		   ,SRECI.EVENT_SOURCE_CR_FK                   
           ,SRECI.STG_RECORD_IDENT_ID           
        FROM stage.STG_RECORD SREC WITH (NOLOCK)
        INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
           ON ( SREC.STG_RECORD_ID = SRECI.STG_RECORD_FK 
			AND ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL' )
        INNER JOIN EPACUBE.PRODUCT_IDENTIFICATION PI WITH (NOLOCK)
           ON ( PI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
           AND  PI.VALUE        = SRECI.ID_VALUE 
           AND  PI.ORG_ENTITY_STRUCTURE_FK = SRECI.ORG_ENTITY_STRUCTURE_FK 
           AND  ISNULL ( PI.ENTITY_STRUCTURE_FK, 0 ) <> ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK , 0 )
           )
           INNER JOIN EPACUBE.DATA_NAME DN
on (DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK)
INNER JOIN EPACUBE.DATA_SET DS
on (ds.data_Set_Id = dn.data_set_fk 
and ds.entity_structure_ec_cr_fk is NULL) 
        WHERE SREC.BATCH_NO = @IN_BATCH_NO
    UNION ALL
		SELECT 
            SREC.JOB_FK
           ,PI.PRODUCT_STRUCTURE_FK
           ,SRECI.STG_RECORD_FK
           ,PI.DATA_NAME_FK
           ,PI.VALUE
           ,SRECI.ID_ENTITY_DATA_NAME_FK
           ,SRECI.ID_ENTITY_STRUCTURE_FK AS TO_ID_ENTITY_STRUCTURE_FK   
           ,PI.ENTITY_STRUCTURE_FK AS FROM_ID_ENTITY_STRUCTURE_FK 
		   ,SRECI.EVENT_SOURCE_CR_FK                                      
           ,SRECI.STG_RECORD_IDENT_ID                      
        FROM stage.STG_RECORD SREC WITH (NOLOCK)
        INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
           ON ( SREC.STG_RECORD_ID = SRECI.STG_RECORD_FK )
        INNER JOIN EPACUBE.PRODUCT_IDENT_MULT PI WITH (NOLOCK)
           ON ( PI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
           AND  PI.VALUE        = SRECI.ID_VALUE 
           AND  PI.ORG_ENTITY_STRUCTURE_FK = SRECI.ORG_ENTITY_STRUCTURE_FK 
           AND  ISNULL ( PI.ENTITY_STRUCTURE_FK, 0 ) <> ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK , 0 )
           )   
INNER JOIN EPACUBE.DATA_NAME DN
on (DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK)
INNER JOIN EPACUBE.DATA_SET DS
on (ds.data_Set_Id = dn.data_set_fk 
and ds.entity_structure_ec_cr_fk is NULL)        
        WHERE SREC.BATCH_NO = @IN_BATCH_NO
    ) A


       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			 SET @status_desc = 'total rows ENTITY MIS MATCH:: '
                                + ' '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END


/*
 -- ------------------------------------------------------------------------------------
 --   Product NON-Unique Matches   -- Include Entity Entity_Structure  
 --									Org is Assumed 1 for now and is not included
 -- -----------------------------------------------------------------------------------
 --*/


	
INSERT INTO [cleanser].[SEARCH_JOB_RESULTS_IDENT]
           ([SEARCH_JOB_FK]
           ,[BATCH_NO]
           ,[SEARCH_ACTIVITY_FK]
           ,[FROM_SEARCH_FK]
           ,[MATCH_TYPE_CR_FK]           
           ,[TO_SEARCH_FK]
           ,[ID_DATA_NAME_FK]
           ,[ID_VALUE]
           ,[DERIVED_ID_IND]                      
           ,[STG_RECORD_IDENT_FK]                      
          )
     SELECT
            A.JOB_FK
           ,@IN_BATCH_NO
           ,@IN_SEARCH_ACTIVITY_FK
           ,A.PRODUCT_STRUCTURE_FK
           ,176   -- NONUNIQUE
           ,A.STG_RECORD_FK
           ,A.DATA_NAME_FK
		   ,A.VALUE
		   ,CASE ISNULL ( A.EVENT_SOURCE_CR_FK, 0 )
		    WHEN 0 THEN NULL
		    WHEN 72 THEN 1
		    ELSE NULL
		    END 		   
		   ,A.STG_RECORD_IDENT_ID		   
    FROM (
		SELECT 
		    SREC.JOB_FK
           ,PI.PRODUCT_STRUCTURE_FK
           ,SRECI.STG_RECORD_FK
           ,PI.DATA_NAME_FK
           ,PI.VALUE
		   ,SRECI.EVENT_SOURCE_CR_FK                   
           ,SRECI.STG_RECORD_IDENT_ID           
        FROM stage.STG_RECORD SREC WITH (NOLOCK)
        INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
           ON ( SREC.STG_RECORD_ID = SRECI.STG_RECORD_FK 
			AND ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL' )
        INNER JOIN EPACUBE.PRODUCT_IDENT_NONUNIQUE PI WITH (NOLOCK)
           ON ( PI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
           AND  PI.VALUE        = SRECI.ID_VALUE 
           AND  PI.ORG_ENTITY_STRUCTURE_FK = SRECI.ORG_ENTITY_STRUCTURE_FK 
           AND  ISNULL ( PI.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK , 0 ))
           INNER JOIN cleanser.SEARCH_ACTIVITY_IDENT_MATCH_RULE SAIMR WITH (NOLOCK)
ON ( SAIMR.MATCH_TYPE_CR_FK in (174, 176) -- NONUNIQUE TYPES
AND SAIMR.RECORD_STATUS_CR_FK = 1) ---CV ADDED
        WHERE SREC.BATCH_NO = @IN_BATCH_NO
    ) A

	
       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			 SET @status_desc = 'total rows NONUNIQUE MATCH:: '
                                + ' '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END

END
ELSE
BEGIN



/*
 -- ------------------------------------------------------------------------------------
 --   ENTITY Primary Unique Matches
 -- -----------------------------------------------------------------------------------
 --*/

	
INSERT INTO [cleanser].[SEARCH_JOB_RESULTS_IDENT]
           ([SEARCH_JOB_FK]
           ,[BATCH_NO]
           ,[SEARCH_ACTIVITY_FK]
           ,[FROM_SEARCH_FK]
           ,[MATCH_TYPE_CR_FK]           
           ,[TO_SEARCH_FK]
           ,[ID_DATA_NAME_FK]
           ,[ID_VALUE]
           ,[DERIVED_ID_IND]                      
           ,[STG_RECORD_IDENT_FK]           
          )
     SELECT
            A.JOB_FK
           ,@IN_BATCH_NO
           ,@IN_SEARCH_ACTIVITY_FK
           ,A.ENTITY_STRUCTURE_FK
           ,171   -- PRIMARY
           ,A.STG_RECORD_FK
           ,A.DATA_NAME_FK
		   ,A.VALUE	
		   ,CASE ISNULL ( A.EVENT_SOURCE_CR_FK, 0 )
		    WHEN 0 THEN NULL
		    WHEN 72 THEN 1
		    ELSE NULL
		    END 		   	   
		   ,A.STG_RECORD_IDENT_ID
    FROM (
		SELECT 
		    SREC.JOB_FK
           ,EI.ENTITY_STRUCTURE_FK
           ,SRECI.STG_RECORD_FK
           ,EI.DATA_NAME_FK
           ,EI.VALUE
		   ,SRECI.EVENT_SOURCE_CR_FK                   
		   ,SRECI.STG_RECORD_IDENT_ID           
        FROM stage.STG_RECORD SREC WITH (NOLOCK)
        INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
           ON ( SREC.STG_RECORD_ID = SRECI.STG_RECORD_FK 
			AND ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL' )
        INNER JOIN EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
           ON ( EI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
           AND  EI.VALUE        = SRECI.ID_VALUE 
           AND EI.ENTITY_DATA_NAME_FK = SRECI.ENTITY_DATA_NAME_FK)   -----ECL CUSTOMER
        WHERE SREC.BATCH_NO = @IN_BATCH_NO
        AND   SRECI.ID_DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
                                               FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK)
                                               WHERE EEP.APPLICATION_SCOPE_FK = 100
                                               AND   EEP.NAME = ( SELECT CODE
                                                                  FROM EPACUBE.CODE_REF
                                                                  WHERE CODE_REF_ID = SRECI.ENTITY_CLASS_CR_FK ) )
    ) A

       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			 SET @status_desc = 'total rows ENTITY PRIMARY MATCH:: '
                                + ' '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END


/*
 -- ------------------------------------------------------------------------------------
 --   ENTITY Unique Matches
 -- -----------------------------------------------------------------------------------
 --*/


	
INSERT INTO [cleanser].[SEARCH_JOB_RESULTS_IDENT]
           ([SEARCH_JOB_FK]
           ,[BATCH_NO]
           ,[SEARCH_ACTIVITY_FK]
           ,[FROM_SEARCH_FK]
           ,[MATCH_TYPE_CR_FK]           
           ,[TO_SEARCH_FK]
           ,[ID_DATA_NAME_FK]
           ,[ID_VALUE]
           ,[DERIVED_ID_IND]                      
           ,[STG_RECORD_IDENT_FK]                      
          )
     SELECT
            A.JOB_FK
           ,@IN_BATCH_NO
           ,@IN_SEARCH_ACTIVITY_FK
           ,A.ENTITY_STRUCTURE_FK
           ,172   -- UNIQUE
           ,A.STG_RECORD_FK
           ,A.DATA_NAME_FK
		   ,A.VALUE
		   ,CASE ISNULL ( A.EVENT_SOURCE_CR_FK, 0 )
		    WHEN 0 THEN NULL
		    WHEN 72 THEN 1
		    ELSE NULL
		    END 		   
		   ,A.STG_RECORD_IDENT_ID		   
    FROM (
		SELECT 
		    SREC.JOB_FK
           ,EI.ENTITY_STRUCTURE_FK
           ,SRECI.STG_RECORD_FK
           ,EI.DATA_NAME_FK
           ,EI.VALUE
		   ,SRECI.EVENT_SOURCE_CR_FK                   
           ,SRECI.STG_RECORD_IDENT_ID           
        FROM stage.STG_RECORD SREC WITH (NOLOCK)
        INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
           ON ( SREC.STG_RECORD_ID = SRECI.STG_RECORD_FK 
			AND ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL' )
        INNER JOIN EPACUBE.ENTITY_IDENTIFICATION EI WITH (NOLOCK)
           ON ( EI.ENTITY_DATA_NAME_FK = SRECI.ENTITY_DATA_NAME_FK
           AND  EI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
           AND  EI.VALUE        = SRECI.ID_VALUE            
           )
        WHERE SREC.BATCH_NO = @IN_BATCH_NO
  ----  UNION ALL  ---CV check here
		----SELECT 
  ----          SREC.JOB_FK
  ----         ,EI.ENTITY_STRUCTURE_FK
  ----         ,SRECI.STG_RECORD_FK
  ----         ,EI.DATA_NAME_FK
  ----         ,EI.VALUE
		----   ,SRECI.EVENT_SOURCE_CR_FK                   
  ----         ,SRECI.STG_RECORD_IDENT_ID                      
  ----      FROM stage.STG_RECORD SREC WITH (NOLOCK)
  ----      INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
  ----         ON ( SREC.STG_RECORD_ID = SRECI.STG_RECORD_FK )
  ----      INNER JOIN EPACUBE.ENTITY_IDENT_MULT EI WITH (NOLOCK)
  ----         ON ( EI.ENTITY_DATA_NAME_FK = SRECI.ENTITY_DATA_NAME_FK
  ----         AND  EI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
  ----         AND  EI.VALUE        = SRECI.ID_VALUE            
  ----         )
  ----      WHERE SREC.BATCH_NO = @IN_BATCH_NO
    ) A


       SET @l_rows_processed = @@ROWCOUNT
	   
	   IF @l_rows_processed > 0
	   BEGIN
			 SET @status_desc = 'total rows ENTITY NON MATCH:: '
                                + ' '			 
								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
       END

           
END            




 -- ------------------------------------------------------------------------------------
 --       Create Temp Tables for Matching Algorithm
 -- -----------------------------------------------------------------------------------


--drop temp tables if exists
IF  EXISTS (SELECT * FROM tempdb.sys.objects WHERE NAME = '#TS_IDENT_MATCH' AND type in (N'U'))
   DROP TABLE dbo.#TS_IDENT_MATCH

-- create temporary tables and indexes


CREATE TABLE #TS_IDENT_MATCH  (
	[TS_IDENT_MATCH_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SEARCH_JOB_RESULTS_IDENT_FK] [bigint],	
	[STG_RECORD_FK] [bigint],
	[SEARCH_STRUCTURE_FK] [bigint],
	[MATCH_TYPE_CR_FK] [int],		
	[UNIQUE_MISMATCH_CNT] [int],	
	[NONUNIQUE_MISMATCH_CNT] [int],	
    [SEARCH_ACTIVITY_IDENT_MATCH_RULE_FK] [int]
)


CREATE NONCLUSTERED INDEX IDX_TSEM_SES ON #TS_IDENT_MATCH
(STG_RECORD_FK ASC )
INCLUDE ( [SEARCH_ACTIVITY_IDENT_MATCH_RULE_FK], [TS_IDENT_MATCH_ID] )
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


CREATE NONCLUSTERED INDEX IDX_TSEM_SES_MR_DR ON #TS_IDENT_MATCH
(STG_RECORD_FK ASC,
 SEARCH_ACTIVITY_IDENT_MATCH_RULE_FK ASC
)
INCLUDE ( [SEARCH_STRUCTURE_FK], [TS_IDENT_MATCH_ID] )
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

CREATE CLUSTERED INDEX IDX_TSEM_TSEMID ON #TS_IDENT_MATCH
(TS_IDENT_MATCH_ID ASC)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



------ need to include data set lookup for entity requiremnts with idents.


INSERT INTO #TS_IDENT_MATCH
  (  SEARCH_JOB_RESULTS_IDENT_FK
    ,STG_RECORD_FK
    ,SEARCH_STRUCTURE_FK  
	,MATCH_TYPE_CR_FK
	,UNIQUE_MISMATCH_CNT
	,NONUNIQUE_MISMATCH_CNT 
    ,SEARCH_ACTIVITY_IDENT_MATCH_RULE_FK
   )
---
SELECT	
    B.SEARCH_JOB_RESULTS_IDENT_FK
   ,B.STG_RECORD_FK
   ,B.SEARCH_STRUCTURE_FK
   ,B.MATCH_TYPE_CR_FK
   ,B.UNIQUE_MISMATCH_CNT
   ,B.NONUNIQUE_MISMATCH_CNT
   ,ISNULL (
    ( SELECT SEARCH_ACTIVITY_IDENT_MATCH_RULE_ID
      FROM cleanser.SEARCH_ACTIVITY_IDENT_MATCH_RULE SAIMR 
      WHERE SAIMR.SEARCH_ACTIVITY_FK = @IN_SEARCH_ACTIVITY_FK
AND SAIMR.RECORD_STATUS_CR_FK = 1  --CV added
      AND   SAIMR.MATCH_TYPE_CR_FK = B.MATCH_TYPE_CR_FK
      AND  (   
               (    SAIMR.UNIQUE_MISMATCH_CNT = B.UNIQUE_MISMATCH_CNT  )
               OR
               (    SAIMR.UNIQUE_MISMATCH_CNT = 1 
                AND B.UNIQUE_MISMATCH_CNT >= 1  )
           )
     )
    ,
    ( SELECT SEARCH_ACTIVITY_IDENT_MATCH_RULE_ID
      FROM cleanser.SEARCH_ACTIVITY_IDENT_MATCH_RULE SAIMR 
      WHERE SAIMR.SEARCH_ACTIVITY_FK = @IN_SEARCH_ACTIVITY_FK
AND SAIMR.RECORD_STATUS_CR_FK = 1  ---CV added
      AND   SAIMR.MATCH_TYPE_CR_FK = B.MATCH_TYPE_CR_FK
      AND   SAIMR.UNIQUE_MISMATCH_CNT = 99999 
      AND   SAIMR.NONUNIQUE_MISMATCH_CNT = 99999 
     )
    ) AS SEARCH_ACTIVITY_IDENT_MATCH_RULE_FK
-----
FROM (
SELECT  A.SEARCH_JOB_RESULTS_IDENT_FK
       ,A.STG_RECORD_FK
       ,A.SEARCH_STRUCTURE_FK
       ,A.MATCH_TYPE_CR_FK
---   TOTAL COUNT OF DISTINCT DIFF ENTITY WHICH HAVE UNIQUE MATCHES TO THE STG_RECORD_FK
       ,( SELECT COUNT( DISTINCT SJRI.FROM_SEARCH_FK )
          FROM cleanser.SEARCH_JOB_RESULTS_IDENT SJRI  WITH (NOLOCK)
		  INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = SJRI.ID_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
          WHERE SJRI.TO_SEARCH_FK = A.STG_RECORD_FK 
          AND   DS.TABLE_NAME IN ( 'PRODUCT_IDENTIFICATION', 'PRODUCT_IDENT_MULT', 'ENTITY_IDENTIFICATION', 'ENTITY_IDENT_MULT' )
          AND   SJRI.FROM_SEARCH_FK <> A.SEARCH_STRUCTURE_FK          
         ) AS UNIQUE_MISMATCH_CNT
---   TOTAL COUNT OF DISTINCT DIFF ENTITY WHICH HAVE NON-UNIQUE MATCHES TO THE STG_RECORD_FK
       ,( SELECT COUNT( DISTINCT SJRI.FROM_SEARCH_FK )
          FROM cleanser.SEARCH_JOB_RESULTS_IDENT SJRI  WITH (NOLOCK)
		  INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = SJRI.ID_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
          WHERE SJRI.TO_SEARCH_FK = A.STG_RECORD_FK 
          AND   DS.TABLE_NAME IN ( 'PRODUCT_IDENT_NONUNIQUE', 'ENTITY_IDENT_NONUNIQUE'  )
          AND   SJRI.FROM_SEARCH_FK <> A.SEARCH_STRUCTURE_FK
         ) AS NONUNIQUE_MISMATCH_CNT
------
FROM (
SELECT  SJRI.SEARCH_JOB_RESULTS_IDENT_ID AS SEARCH_JOB_RESULTS_IDENT_FK
       ,SREC.STG_RECORD_ID AS STG_RECORD_FK 
       ,SJRI.FROM_SEARCH_FK AS SEARCH_STRUCTURE_FK
       ,SJRI.MATCH_TYPE_CR_FK
FROM STAGE.STG_RECORD SREC WITH (NOLOCK) 
INNER JOIN [cleanser].[SEARCH_JOB_RESULTS_IDENT] SJRI WITH (NOLOCK)
   ON (SJRI.TO_SEARCH_FK = SREC.STG_RECORD_ID
   AND SJRI.FROM_SEARCH_FK IS NOT NULL ) 
WHERE SREC.BATCH_NO = @IN_BATCH_NO
) A 
) B




UPDATE cleanser.SEARCH_JOB_RESULTS_IDENT
SET CONFIDENCE = ( SELECT CONFIDENCE
                   FROM cleanser.SEARCH_ACTIVITY_IDENT_MATCH_RULE
                   WHERE SEARCH_ACTIVITY_IDENT_MATCH_RULE_ID = TSIM.SEARCH_ACTIVITY_IDENT_MATCH_RULE_FK )
   ,UNIQUE_MISMATCH_CNT    = TSIM.UNIQUE_MISMATCH_CNT
   ,NONUNIQUE_MISMATCH_CNT = TSIM.NONUNIQUE_MISMATCH_CNT
   ,SEARCH_ACTIVITY_IDENT_MATCH_RULE_FK = TSIM.SEARCH_ACTIVITY_IDENT_MATCH_RULE_FK
FROM #TS_IDENT_MATCH  TSIM
WHERE TSIM.SEARCH_JOB_RESULTS_IDENT_FK = cleanser.SEARCH_JOB_RESULTS_IDENT.SEARCH_JOB_RESULTS_IDENT_ID
   
   

 -- ------------------------------------------------------------------------------------
 --      PICK TOP MATCH FOUND  ( only ONE per TO_SEARCH_FK / FROM_SEARCH_FK )
 --         INSERT INTO cleanser.SEARCH_JOB_FROM_TO_MATCH
 --			DRANK by Confidence 
 -- -----------------------------------------------------------------------------------

   

INSERT INTO [cleanser].[SEARCH_JOB_FROM_TO_MATCH]
           ([SEARCH_JOB_FK]
           ,[FROM_SEARCH_FK]
           ,[TO_SEARCH_FK]
           ,[SEARCH_JOB_RESULTS_IDENT_FK]           
           ,[CONFIDENCE]
           ,[UPDATE_USER]
           ,[UPDATE_TIMESTAMP])
SELECT 
            SJRI.SEARCH_JOB_FK
           ,SJRI.FROM_SEARCH_FK
           ,SJRI.TO_SEARCH_FK
           ,SJRI.SEARCH_JOB_RESULTS_IDENT_ID
           ,SJRI.CONFIDENCE
           ,'EPACUBE'
           ,GETDATE()
FROM  cleanser.SEARCH_JOB_RESULTS_IDENT SJRI WITH (NOLOCK)       
WHERE SEARCH_JOB_RESULTS_IDENT_ID IN
   ( SELECT 
     A.SEARCH_JOB_RESULTS_IDENT_ID
     FROM ( 
		SELECT
		SEARCH_JOB_RESULTS_IDENT_ID,
		DENSE_RANK() OVER ( PARTITION BY TO_SEARCH_FK, FROM_SEARCH_FK
							ORDER BY CONFIDENCE DESC, SEARCH_JOB_RESULTS_IDENT_ID DESC ) AS DRANK
		FROM cleanser.SEARCH_JOB_RESULTS_IDENT WITH (NOLOCK)       
		WHERE BATCH_NO = @IN_BATCH_NO
		--and ID_DATA_NAME_FK <> 110111   ---testing.....CINDY
	) A
	WHERE A.DRANK = 1
	)




--drop temp tables if exists
IF  EXISTS (SELECT * FROM tempdb.sys.objects WHERE NAME = '#TS_IDENT_MATCH' AND type in (N'U'))
   DROP TABLE dbo.#TS_IDENT_MATCH




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------
--
SET @status_desc = 'finished execution of cleanser.SEARCH_JOB_RESULT_MATCH_IDENT'
                         + '; Search Activity:' + cast(@IN_SEARCH_ACTIVITY_FK as varchar(20))
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no;
Exec exec_monitor.print_status_sp @exec_id = @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.SEARCH_JOB_RESULT_MATCH_IDENT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END



































































