﻿













-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: For Stg Records submitted to Synhcronizer
--			Create any missing entities associated with them
--			Insert all Identifications also associated with them
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/28/2008   Initial SQL Version
-- CV		 06/10/2009   Added Validation for Product Identification
-- CV        12/09/2009   Added new column to stage error table
--							added stg_record_ident_fk for identifiation errors
-- KS        02/25/2010   Corrected Problem with Pre-Post; 
--							and Moved Validation to Data_Cleanser_Validation for flow and maintenance of code
-- CV		 03/19/2010     Change default status from Setup to Active
-- CV        05/03/2010   Commented epacube search
-- CV        06/10/2011   Do not add -999999999 data

CREATE PROCEDURE [cleanser].[NEW_PRODUCT_STRUCTURE]       
         @IN_JOB_FK         BIGINT
        ,@IN_BATCH_NO       BIGINT
AS
BEGIN


DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint
DECLARE @v_search_activity	 int

DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int




BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.NEW_PRODUCT_STRUCTURE', 
                                                @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no,
												@exec_id = @l_exec_no OUTPUT;


SET @l_sysdate = GETDATE()

------------------------------------------------------------------------------------------
--   NEW ENTITY EVENTS --- >>>>  CREATE PRODUCT_STRUCTURE ( LEVEL 1 FIRST )
------------------------------------------------------------------------------------------

            SET @status_desc =  'cleanser.NEW_PRODUCT_STRUCTURE: inserting - '
                                + 'PRODUCT_STRUCTURE'

            Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;



	INSERT INTO epacube.PRODUCT_STRUCTURE
           (
            DATA_NAME_FK
		   ,PRODUCT_STATUS_CR_FK           
           ,JOB_FK
           ,IMPORT_FILENAME
           ,RECORD_NO
           ,RECORD_STATUS_CR_FK
           ,CREATE_TIMESTAMP
           ,UPDATE_TIMESTAMP
           )
	SELECT 
		   SREC.ENTITY_DATA_NAME_FK	
          ,ISNULL (
           ( SELECT CODE_REF_ID
             FROM EPACUBE.CODE_REF  WITH (NOLOCK)
             WHERE CODE_TYPE = 'PRODUCT_STATUS'
             AND   CODE = SREC.ENTITY_STATUS )
           , 100 )
          ,SREC.JOB_FK
		  ,SREC.IMPORT_FILENAME
		  ,SREC.RECORD_NO
          ,1  -- RECORD STATUS
          ,@L_SYSDATE
          ,@L_SYSDATE
	  FROM [stage].[STG_RECORD] SREC  WITH (NOLOCK)
      WHERE SREC.JOB_FK = @IN_JOB_FK
      AND   SREC.BATCH_NO = @IN_BATCH_NO
	  AND   SREC.EVENT_CONDITION_CR_FK <> 99      
      AND   SREC.ACTIVITY_ACTION_CR_FK = 144
      AND   NOT EXISTS
           ( SELECT 1
             FROM EPACUBE.PRODUCT_STRUCTURE PS WITH (NOLOCK)
             WHERE PS.JOB_FK = SREC.JOB_FK
             AND   PS.IMPORT_FILENAME = SREC.IMPORT_FILENAME
             AND   PS.RECORD_NO = SREC.RECORD_NO 
             AND   PS.DATA_NAME_FK = SREC.ENTITY_DATA_NAME_FK )




------------------------------------------------------------------------------------------
--   NEW ENTITY LEVEL 1 --- >>>>  UPDATE STG_RECORD WITH NEW EVENT_STRUCTURE
------------------------------------------------------------------------------------------


            SET @status_desc =  'cleanser.NEW_PRODUCT_STRUCTURE: updating - '
                                + 'PRODUCT_STRUCTURE_FK'

            Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


		INSERT INTO STAGE.STG_RECORD_STRUCTURE
		(  STG_RECORD_FK
		  ,JOB_FK
		  ,SEARCH_STRUCTURE_FK
		)
		SELECT 
		   SREC.STG_RECORD_ID
		  ,SREC.JOB_FK
		  ,PS.PRODUCT_STRUCTURE_ID
		FROM stage.STG_RECORD SREC WITH (NOLOCK)
		INNER JOIN EPACUBE.PRODUCT_STRUCTURE PS WITH (NOLOCK)
		  ON ( PS.JOB_FK          = SREC.JOB_FK
		  AND  PS.IMPORT_FILENAME = SREC.IMPORT_FILENAME
		  AND  PS.RECORD_NO       = SREC.RECORD_NO
		  AND  PS.DATA_NAME_FK    = SREC.ENTITY_DATA_NAME_FK )
		WHERE SREC.JOB_FK = @IN_JOB_FK
		AND   SREC.BATCH_NO = @IN_BATCH_NO
		AND   SREC.EVENT_CONDITION_CR_FK <> 99		
        AND   SREC.ACTIVITY_ACTION_CR_FK = 144		
		AND   NOT EXISTS 
		       ( SELECT 1
		         FROM STAGE.STG_RECORD_STRUCTURE SRECS WITH (NOLOCK)
		         WHERE SRECS.STG_RECORD_FK = SREC.STG_RECORD_ID
		         AND   SRECS.SEARCH_STRUCTURE_FK = PS.PRODUCT_STRUCTURE_ID )



------------------------------------------------------------------------------------------
--   FOR ALL NEW ENTITIES --- >>>>  INSERT ENTITY UNIQUE IDENTIFICATIONS
------------------------------------------------------------------------------------------
	

	INSERT INTO [epacube].[PRODUCT_IDENTIFICATION]
			   ([PRODUCT_STRUCTURE_FK]
			   ,[DATA_NAME_FK]
			   ,[VALUE]
			   ,[ORG_ENTITY_STRUCTURE_FK]			   			   			   
			   ,[ENTITY_STRUCTURE_FK]	
			   ,[JOB_FK]		   
			   )
	SELECT DISTINCT
			    PS.PRODUCT_STRUCTURE_ID
			   ,SRECI.ID_DATA_NAME_FK
			   ,SRECI.ID_VALUE
			   ,SRECI.ORG_ENTITY_STRUCTURE_FK			   			   
			   ,SRECI.ID_ENTITY_STRUCTURE_FK
			   ,SREC.JOB_FK			   			   
		  FROM STAGE.STG_RECORD SREC WITH (NOLOCK) 
		  INNER JOIN EPACUBE.PRODUCT_STRUCTURE PS WITH (NOLOCK)
			  ON ( PS.JOB_FK          = SREC.JOB_FK
			  AND  PS.IMPORT_FILENAME = SREC.IMPORT_FILENAME
			  AND  PS.RECORD_NO       = SREC.RECORD_NO
			  AND  PS.DATA_NAME_FK    = SREC.ENTITY_DATA_NAME_FK )		  
		  INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
			 ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID
          AND (ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL'
          AND SRECI.ID_VALUE <> '-999999999'))
		  INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
		     ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
		     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		  WHERE SREC.JOB_FK = @IN_JOB_FK
		  AND   SREC.BATCH_NO = @IN_BATCH_NO
		  AND   DS.TABLE_NAME = 'PRODUCT_IDENTIFICATION'
		AND   NOT EXISTS
	            ( SELECT 1 
	              FROM STAGE.STG_RECORD_ERRORS SRE WITH (NOLOCK)
	              WHERE SRE.STG_RECORD_IDENT_FK =  SRECI.STG_RECORD_IDENT_ID)
	      AND   NOT EXISTS
	            ( SELECT 1 
	              FROM EPACUBE.PRODUCT_IDENTIFICATION PI WITH (NOLOCK)
	              WHERE PI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
	              AND   PI.VALUE = SRECI.ID_VALUE
	              AND   PI.ORG_ENTITY_STRUCTURE_FK = SRECI.ORG_ENTITY_STRUCTURE_FK
	              AND   ISNULL ( PI.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK, 0 )
	            )


------------------------------------------------------------------------------------------
--   FOR ALL NEW ENTITIES --- >>>>  INSERT ENTITY UNIQUE IDENT MULT
--
--    BECAUSE MULT IDENT LEAVE AS NOT EXISTS
------------------------------------------------------------------------------------------
		

	INSERT INTO [epacube].[PRODUCT_IDENT_MULT]
			   ([PRODUCT_STRUCTURE_FK]
			   ,[DATA_NAME_FK]
			   ,[VALUE]
			   ,[ORG_ENTITY_STRUCTURE_FK]			   			   			   
			   ,[ENTITY_STRUCTURE_FK]			   
			   )
	SELECT
			    PS.PRODUCT_STRUCTURE_ID
			   ,SRECI.ID_DATA_NAME_FK
			   ,SRECI.ID_VALUE
			   ,SRECI.ORG_ENTITY_STRUCTURE_FK			   			   
			   ,SRECI.ID_ENTITY_STRUCTURE_FK			   			   
		  FROM STAGE.STG_RECORD SREC WITH (NOLOCK) 
		  INNER JOIN EPACUBE.PRODUCT_STRUCTURE PS WITH (NOLOCK)
			  ON ( PS.JOB_FK          = SREC.JOB_FK
			  AND  PS.IMPORT_FILENAME = SREC.IMPORT_FILENAME
			  AND  PS.RECORD_NO       = SREC.RECORD_NO
			  AND  PS.DATA_NAME_FK    = SREC.ENTITY_DATA_NAME_FK )		  
		  INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
			 ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID 
AND (ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL'
          AND SRECI.ID_VALUE <> '-999999999'))
		  INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
		     ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
		     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		  WHERE SREC.JOB_FK = @IN_JOB_FK
		  AND   SREC.BATCH_NO = @IN_BATCH_NO
		  AND   DS.TABLE_NAME = 'PRODUCT_IDENT_MULT'
		AND   NOT EXISTS
	            ( SELECT 1 
	              FROM STAGE.STG_RECORD_ERRORS SRE WITH (NOLOCK)
	              WHERE SRE.STG_RECORD_IDENT_FK =  SRECI.STG_RECORD_IDENT_ID)		  
	      AND   NOT EXISTS
	            ( SELECT 1 
	              FROM EPACUBE.PRODUCT_IDENT_MULT PI WITH (NOLOCK)
	              WHERE PI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
	              AND   PI.VALUE = SRECI.ID_VALUE
	              AND   PI.ORG_ENTITY_STRUCTURE_FK = SRECI.ORG_ENTITY_STRUCTURE_FK
	              AND   ISNULL ( PI.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK, 0 )
	            )
	


------------------------------------------------------------------------------------------
--   FOR ALL NEW ENTITIES --- >>>>  INSERT ENTITY UNIQUE IDENT MULT
--
--    BECAUSE NON UNIQUE LEAVE AS NOT EXISTS BUT CHECK INCLUDES DN/PROD/ORG/ES
------------------------------------------------------------------------------------------
		

	INSERT INTO [epacube].[PRODUCT_IDENT_NONUNIQUE]
			   ([PRODUCT_STRUCTURE_FK]
			   ,[DATA_NAME_FK]
			   ,[VALUE]
			   ,[ORG_ENTITY_STRUCTURE_FK]			   			   			   
			   ,[ENTITY_STRUCTURE_FK]			   
			   )
	SELECT
			    PS.PRODUCT_STRUCTURE_ID
			   ,SRECI.ID_DATA_NAME_FK
			   ,SRECI.ID_VALUE
			   ,SRECI.ORG_ENTITY_STRUCTURE_FK			   			   
			   ,SRECI.ID_ENTITY_STRUCTURE_FK			   			   
		  FROM STAGE.STG_RECORD SREC WITH (NOLOCK) 
		  INNER JOIN EPACUBE.PRODUCT_STRUCTURE PS WITH (NOLOCK)
			  ON ( PS.JOB_FK          = SREC.JOB_FK
			  AND  PS.IMPORT_FILENAME = SREC.IMPORT_FILENAME
			  AND  PS.RECORD_NO       = SREC.RECORD_NO
			  AND  PS.DATA_NAME_FK    = SREC.ENTITY_DATA_NAME_FK )		  
		  INNER JOIN STAGE.STG_RECORD_IDENT SRECI WITH (NOLOCK)
			 ON ( SRECI.STG_RECORD_FK = SREC.STG_RECORD_ID 
AND (ISNULL (SRECI.ID_VALUE, 'NULL') <> 'NULL'
          AND SRECI.ID_VALUE <> '-999999999'))
		  INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
		     ON ( DN.DATA_NAME_ID = SRECI.ID_DATA_NAME_FK )
		  INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
		     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
		  WHERE SREC.JOB_FK = @IN_JOB_FK
		  AND   SREC.BATCH_NO = @IN_BATCH_NO
		  AND   DS.TABLE_NAME = 'PRODUCT_IDENT_NONUNIQUE'
		AND   NOT EXISTS
	            ( SELECT 1 
	              FROM STAGE.STG_RECORD_ERRORS SRE WITH (NOLOCK)
	              WHERE SRE.STG_RECORD_IDENT_FK =  SRECI.STG_RECORD_IDENT_ID)		  
	      AND   NOT EXISTS
	            ( SELECT 1 
	              FROM EPACUBE.PRODUCT_IDENT_NONUNIQUE PI WITH (NOLOCK)
	              WHERE PI.DATA_NAME_FK = SRECI.ID_DATA_NAME_FK
	              AND   PI.PRODUCT_STRUCTURE_FK = PS.PRODUCT_STRUCTURE_ID
	              AND   PI.ORG_ENTITY_STRUCTURE_FK = SRECI.ORG_ENTITY_STRUCTURE_FK
	              AND   ISNULL ( PI.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( SRECI.ID_ENTITY_STRUCTURE_FK, 0 )
	            )
	


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.NEW_PRODUCT_STRUCTURE'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.NEW_PRODUCT_STRUCTURE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END








