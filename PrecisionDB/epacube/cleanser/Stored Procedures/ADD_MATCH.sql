﻿










CREATE PROCEDURE [cleanser].[ADD_MATCH] (
      @in_search_job_fk     int,
      @in_from_search_fk int,
      @in_to_search_fk int,
      @in_update_user varchar(64)
   )
   AS

DECLARE  @V_SEARCH_ACTIVITY_FK  INT
        ,@V_BATCH_NO            BIGINT

SET @V_SEARCH_ACTIVITY_FK = (SELECT SEARCH_ACTIVITY_FK
FROM CLEANSER.SEARCH_JOB WITH (NOLOCK)
WHERE SEARCH_JOB_ID = @in_search_job_fk)

IF @V_SEARCH_ACTIVITY_FK IN ( 1, 2 )
  -- if resolver do this 
BEGIN

			IF (select count(1) from cleanser.search_job_from_to_match WITH (NOLOCK)
		 where search_job_fk=@in_search_job_fk
		   and from_search_fk=@in_from_search_fk
		   and to_search_fk=@in_to_search_fk) = 0 
	BEGIN 

	 insert into cleanser.search_job_from_to_match 
					(search_job_fk
					,from_search_fk
					,to_search_fk
					,update_user) 
		values 
					(@in_search_job_fk
					,@in_to_search_fk
					,@in_from_search_fk
					,@in_update_user) 


	EXEC seq_manager.db_get_next_sequence_value_impl 'common','batch_seq', @v_batch_no output

	  update  cleanser.SEARCH_JOB_RESULTS_BEST
	  set FROM_SEARCH_FK = @in_to_search_fk
		  ,BATCH_NO       = @V_BATCH_NO
	  where search_job_fk = @in_search_job_fk
	  AND BEST_SEARCH_FK = @in_from_search_fk
		
      UPDATE STAGE.STG_RECORD
      SET BATCH_NO = @V_BATCH_NO
      WHERE JOB_FK = @in_search_job_fk
	  AND STG_RECORD_ID = @in_from_search_fk
		
    EXEC cleanser.DATA_CLEANSER_VALIDATION  @in_search_job_fk, @V_BATCH_NO

END

 ELSE --Cleanser

BEGIN
	IF (select count(1) from cleanser.search_job_from_to_match WITH (NOLOCK)
		 where search_job_fk=@in_search_job_fk
		   and from_search_fk=@in_from_search_fk
		   and to_search_fk=@in_to_search_fk) = 0 
	BEGIN 
	 insert into cleanser.search_job_from_to_match (search_job_fk,from_search_fk,to_search_fk,update_user) 
			values (@in_search_job_fk,@in_from_search_fk,@in_to_search_fk,@in_update_user) 
			
---- should it update BEST for non resolver and FROM for resolver ??



	END;

END;
END









