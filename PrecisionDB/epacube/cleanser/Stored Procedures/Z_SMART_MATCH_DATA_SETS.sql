﻿







-- Modified by YN on 09/30/2006 - Initial version
-- Modified by YN on 11/14/2006 to change to Kathi's tables (SEARCH_STRING and SEARCH_RESULTS_TASK)
-- Modified by YN on 11/24/2006 to add temp tables to improve performance and restartability
-- Modified by YN on 12/02/2006 to change the reverse matching to have all rows to go over 
-- Modified by YN on 04/08/2007 to add exception handling (try - catch)
-- Modified by YN on 04/10/2007 to retrieve top N by confidence
-- Modified by YN on 05/15/2007 changed the order by (in insert / select) to use confidence instead of FTS rank
-- Modified by YN on 08/05/2007 changed procedure to use the new working tables instead of the permanent ones
-- Modified by YN on 08/05/2007 added threshold parameters for direct and reverse matching
-- Modified by YN on 08/07/2007 removed NOT Exist Clause in the where clause to populate searhc_tab tables
-- Modified by YN on 08/08/2007 added status comments for checking the time to populate temp and Work tables



CREATE PROCEDURE [cleanser].[Z_SMART_MATCH_DATA_SETS] (
      @search_activity_fk         int,
      @search_task_fk             int,
      @task_seq                   int,
      @from_data_set_id           bigint,
      @to_data_set_id             bigint,
      @exec_no                    bigint,
      @total_threads              smallint = 1,
      @this_thread                smallint = 1,
      @top_n_rows                 smallint = 50,
      @reverse                    smallint = 0,
      @direct_matching_threshold  int = 0,
      @reverse_matching_threshold int = 50
   )
   AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    BEGIN TRY
    -- Declare all local variables here
      DECLARE @l_from_product_fk   bigint,
              @l_from_search_string_fk bigint,
			  @l_from_smart_search_value varchar(8000),
              @l_exec_no   bigint,
              @status_desc varchar(8000),
              @l_cnt    int;
      Select @status_desc = 
       'started execution of cleanser.smart_match_data_sets' 
       + '; From Data Set:' + cast(@from_data_set_id as varchar(20))
       + '; To Data Set:' + cast(@to_data_set_id as varchar(20))
       + '; Thread: ' + cast(@this_thread as varchar(20)) + ' Of ' + cast(@total_threads as varchar(20))
       + '; With Direction: ' + (case when @reverse = 0 then 'direct' else 'reverse' end); 
												
      EXEC exec_monitor.Report_Status_sp @exec_id = @exec_no,@status_desc = @status_desc;
    --lets create temp table to store all  rows to be processed
    if object_id('tempdb..#search_tab') > 0
       drop table #search_tab;

    
		create table  #search_tab (search_string_id bigint,
								   product_fk bigint,
                                   smart_search_string varchar(max))      
		   
    --populate temp table
    Select @status_desc = 'started populateion of the temp driver table search_tab';
    EXEC exec_monitor.Report_Status_sp @exec_id = @exec_no,@status_desc = @status_desc;
	if @reverse = 0
       insert into #search_tab
       SELECT a.search_string_id, a.product_fk, a.smart_search_string
                        FROM epacube.cleanser.SEARCH_STRING_WORK a
                        WHERE ((a.search_string_id % @total_threads) + 1 =
                                                                   @this_thread) 
                        AND a.search_set_fk = @from_data_set_id;
                            
    else
	   insert into #search_tab
       SELECT a.search_string_id, a.product_fk, a.smart_search_string
                        FROM epacube.cleanser.SEARCH_STRING_WORK a
                        WHERE ((a.search_string_id % @total_threads) + 1 =
                                                                   @this_thread) 
                        AND a.search_set_fk = @to_data_set_id;

    

            --declare main cursor to loop through the FROM data set rows 
   		    DECLARE cur_from_data_set cursor local  for 
                        SELECT a.search_string_id, a.product_fk,  a.smart_search_string
                        FROM #search_tab a;

            Select @status_desc = 'finished populateion of the temp driver table search_tab';
            EXEC exec_monitor.Report_Status_sp @exec_id = @exec_no,@status_desc = @status_desc;
            OPEN cur_from_data_set;
			FETCH NEXT FROM cur_from_data_set Into @l_from_search_string_fk,@l_from_product_fk,@l_from_smart_search_value;
            Select @status_desc = 'started populateion of the SEARCH_RESULTS_TASK_WORK table for thread No: ' + cast(@this_thread as varchar(20))
            + ' with direction - ' + case when @reverse = 0 then 'DIRECT' else 'REVERSE' end;
            EXEC exec_monitor.Report_Status_sp @exec_id = @exec_no,@status_desc = @status_desc;
            WHILE @@FETCH_STATUS = 0
            BEGIN -- loop through all tokens
						
				 IF @l_from_smart_search_value > ' '
                       if @reverse = 0
						   INSERT INTO epacube.cleanser.SEARCH_RESULTS_TASK_WORK
									(  Search_Activity_FK
									  ,Search_Task_FK
                                      ,Task_Seq
                                      ,From_PRODUCT_FK
                                      ,From_Search_String_FK
									  ,Confidence
                                      ,To_PRODUCT_FK
									  ,To_Search_String_FK
									)
						     SELECT top(@top_n_rows)
                                  @search_activity_fk as search_activity_fk,
								  @search_task_fk as search_task_fk,
                                  @task_seq as task_seq, 
								  @l_from_product_fk as from_product_fk, 
                                  @l_from_search_string_fk as from_search_string_fk,
								  master.dbo.fn_dist_lev(a.smart_search_string,@l_from_smart_search_value) * 100 as Confidence,
                                  a.product_fk as To_PRODUCT_FK,
								  a.search_string_id as To_Search_String_FK
							 FROM epacube.cleanser.SEARCH_STRING_WORK a inner join freetexttable (epacube.cleanser.SEARCH_STRING_WORK, smart_search_string,
										@l_from_smart_search_value) f on (a.search_string_id = f.[key] and a.search_set_fk = @to_data_set_id)
							WHERE a.search_set_fk = @to_data_set_id
                            ORDER BY (master.dbo.fn_dist_lev(a.smart_search_string,@l_from_smart_search_value) * 100) desc;
                       else
						   INSERT INTO epacube.cleanser.SEARCH_RESULTS_TASK_WORK
									(  Search_Activity_FK
									  ,Search_Task_FK
                                      ,Task_Seq
                                      ,From_PRODUCT_FK
                                      ,From_Search_String_FK
									  ,Confidence
                                      ,To_PRODUCT_FK
									  ,To_Search_String_FK
                                      ,Reverse_From_Search_string_FK
									)
                           SELECT top(@top_n_rows)
                                  @search_activity_fk as search_activity_fk,
								  @search_task_fk as search_task_fk,
                                  @task_seq as task_seq, 
								  a.product_fk as from_product_fk,
                                  a.search_string_id as From_Search_String_FK,
								  master.dbo.fn_dist_lev(a.smart_search_string,@l_from_smart_search_value) * 100 as Confidence,
                                  @l_from_product_fk as To_PRODUCT_FK, 
								  @l_from_search_string_fk as To_Search_String_FK,
								  @l_from_search_string_fk as Reverse_From_Search_string_FK
							 FROM cleanser.SEARCH_STRING_WORK a inner join freetexttable (epacube.cleanser.SEARCH_STRING_WORK, smart_search_string,
 									   @l_from_smart_search_value) f on (a.search_string_id = f.[key] and a.search_set_fk = @from_data_set_id)
							WHERE a.search_set_fk = @from_data_set_id
                            ORDER BY (master.dbo.fn_dist_lev(a.smart_search_string,@l_from_smart_search_value) * 100) desc;

					if @@trancount > 0
					   COMMIT;
            FETCH NEXT FROM cur_from_data_set Into @l_from_search_string_fk,@l_from_product_fk,@l_from_smart_search_value;
      END --loop
      CLOSE cur_from_data_set;
      DEALLOCATE cur_from_data_set;
	  if object_id('tempdb..#search_tab') > 0
         drop table #search_tab;
      Select @status_desc = 'finished populateion of the SEARCH_RESULTS_TASK_WORK table for thread No: ' + cast(@this_thread as varchar(20))
            + ' with direction - ' + case when @reverse = 0 then 'DIRECT' else 'REVERSE' end;
      EXEC exec_monitor.Report_Status_sp @exec_id = @exec_no,@status_desc = @status_desc;
   END TRY
   BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
   END;