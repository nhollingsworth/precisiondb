﻿











-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: For NULLING DATA FROM IMPORT PACKAGE
--			
--			
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        05/26/2011   Initial SQL Version
--
--

CREATE PROCEDURE [cleanser].[IMPORT_NULLING_DATA] 
       @IN_JOB_FK                BIGINT 
      
AS
BEGIN



DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint
DECLARE @v_import_package_fk   int  
DECLARE @v_NULLING_IND   int 
DECLARE @v_filename_count INT
DECLARE @epa_job_id			 INT


DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int




BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.IMPORT_NULLING_DATA', 
                                                @epa_job_id = @in_job_fk, @epa_batch_no = @in_job_FK,
												@exec_id = @l_exec_no OUTPUT;
												
SET @l_sysdate = GETDATE()


SET @v_import_package_fk = (select distinct import_package_fk 
							from import.import_record_data WITH (NOLOCK)
							where  job_fk = @in_job_fk)

Set @v_NULLING_IND = (select CHECK_FOR_NULLS
						from import.IMPORT_PACKAGE  WITH (NOLOCK)
						where IMPORT_PACKAGE_ID = @v_import_package_fk) 

 
/*
 -- ------------------------------------------------------------------------------------
 --      Loop thru Import Data Names that are either NUMERIC or DATE 
 -- -----------------------------------------------------------------------------------
 --*/

IF @v_NULLING_IND = 1 

BEGIN

      DECLARE 
              @vm$data_name_fk            varchar(16), 
              @vm$data_name               varchar(64), 
              @vm$data_value_column       varchar(64),
              @vm$import_column_name      varchar(64)              
     
                            

      DECLARE  cur_v_import cursor local for            
               SELECT DISTINCT
					isnull ( cast(vm.data_name_fk as varchar(16)), 'NULL' ) AS data_name_fk,
                    dn.NAME,
                    CASE ISNULL ( dn.trim_ind, 0 )
                    WHEN 1 THEN ( 'UPPER(RTRIM(LTRIM( ID.' + vm.import_column_name + ' )))' ) 
                    ELSE vm.import_column_name 
                    END data_value_column,  
				    vm.import_column_name
			  FROM import.v_import vm
			  INNER JOIN epacube.data_name dn WITH (NOLOCK)
			    ON ( dn.data_name_id = vm.data_name_fk )
			  INNER JOIN epacube.data_set ds WITH (NOLOCK)
			    ON ( ds.data_set_id = dn.data_set_fk )
			    INNER JOIN import.IMPORT_MAP_METADATA imp
			    ON (imp.IMPORT_PACKAGE_FK = vm.IMPORT_PACKAGE_FK
			    AND imp.DATA_NAME_FK = dn.DATA_NAME_ID
			    AND imp.CHECK_FOR_NULL  = 1)
			  INNER JOIN cleanser.SEARCH_JOB_DATA_NAME SADN
			    ON ( sadn.SEARCH_JOB_FK = @in_job_fk   )
			  WHERE 1 = 1
			  AND   vm.import_package_fk = @v_import_package_fk
			  AND   vm.import_column_name IS NOT NULL 
			 
      
        OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO 
										  @vm$data_name_fk,
										  @vm$data_name,
										  @vm$data_value_column,
										  @vm$import_column_name
										 
                                          

         WHILE @@FETCH_STATUS = 0 
         BEGIN
            SET @status_desc =  'cleanser.IMPORT_NULLING_DATA - '
                                + @vm$data_name
                                + ' '
                                + @vm$import_column_name

            Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;


            SET @ls_exec =
					'UPDATE IMPORT.IMPORT_RECORD_DATA
					SET '  + @vm$import_column_name
					+ '= ''-999999999'' 
						
						WHERE 1 = 1
						AND ISNULL('
						+ @vm$import_column_name + ',''NULL'') = ''NULL'' 
						AND JOB_FK = '
                   + cast(isnull (@in_job_fk, -999) as varchar(20))						
				   				
					
 INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), ISNULL(@ls_exec,'CRAPPY CODE') )

         exec sp_executesql 	@ls_exec;

            

        FETCH NEXT FROM cur_v_import INTO 
										  @vm$data_name_fk,
										  @vm$data_name,
										  @vm$data_value_column,
										  @vm$import_column_name


     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 
     


END			

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.IMPORT_NULLING_DATA'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk,  @epa_batch_no = @in_job_FK
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.IMPORT_NULLING_DATA has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END





































































































































































































