﻿



-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: For DUPLICATE DATA CHECK
--			
--			
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        01/18/2016   Initial SQL Version
 


CREATE PROCEDURE [cleanser].[IMPORT_TAXONOMY_DN_ATTRIBUTE_EVENTS] 
       @IN_JOB_FK                BIGINT,
	   @v_import_package_fk      bigint 
      
AS
BEGIN



DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint

DECLARE @v_filename_count INT
DECLARE @epa_job_id			 INT


DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

DECLARE @V_TREE_id        bigint



BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.IMPORT_TAXONOMY_DN_ATTRIBUTE_EVENTS', 
                                                @epa_job_id = @in_job_fk, @epa_batch_no = @in_job_FK,
												@exec_id = @l_exec_no OUTPUT;
												
SET @l_sysdate = GETDATE()


SET @V_TREE_id = (select TAXONOMY_TREE_ID from epacube.TAXONOMY_TREE 
where name = (select distinct DATA_POSITION1 from import.IMPORT_RECORD_DATA 
where job_Fk =   @IN_JOB_FK
))
 
 
 
------------------------------------------------------------------------------------------------------
 ---CHECK IF TREE EXIST
 --------------------------------------------------------------------------------------------------------
   
   INSERT INTO Common.Job_ERRORS
           ([JOB_FK]
           ,TABLE_NAME
           ,TABLE_PK
           ,[EVENT_CONDITION_CR_FK]
           ,[ERROR_NAME]
           ,ERROR_DATA1
           ,ERROR_DATA2
           ,ERROR_DATA3
           ,ERROR_DATA4
           ,[CREATE_TIMESTAMP])
SELECT
            IRD.JOB_FK
            ,'IMPORT TAXONOMY PRODUCT ATTRIBUTES'
            ,IRD.IMPORT_RECORD_DATA_ID
           ,99  -- <EVENT_CONDITION_CR_FK, int,>
           ,'MISSING TAXONOMY TREE'
           ,IRD.DATA_POSITION1
           ,ird.DATA_POSITION2
           ,NULL
           ,NULL
           ,GETDATE ()
FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
WHERE 1=1
and JOB_FK = @IN_JOB_FK 
AND  DATA_POSITION1  <> (select NAME from epacube.TAXONOMY_TREE WITH (nolock) where TAXONOMY_TREE_ID = @V_TREE_id )
   

 update ird
 set DO_NOT_IMPORT_IND = 1
  FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
WHERE JOB_FK = @IN_JOB_FK 
AND  DATA_POSITION1  <> (select NAME from epacube.TAXONOMY_TREE WITH (nolock) where TAXONOMY_TREE_ID = @V_TREE_id )


----------------------------------------------------------------------------------------
----CHECK IF TREE NODE EXIST
-----------------------------------------------------------------------------------------
--CHECK NODE ID FIRST


--INSERT INTO Common.Job_ERRORS
--           ([JOB_FK]
--           ,TABLE_NAME
--           ,TABLE_PK
--           ,[EVENT_CONDITION_CR_FK]
--           ,[ERROR_NAME]
--           ,ERROR_DATA1
--           ,ERROR_DATA2
--           ,ERROR_DATA3
--           ,ERROR_DATA4
--           ,[CREATE_TIMESTAMP])
--SELECT distinct
--            IRD.JOB_FK
--            ,'IMPORT TAXONOMY PRODUCT ATTRIBUTES'
--            ,IRD.IMPORT_RECORD_DATA_ID
--           ,99  -- <EVENT_CONDITION_CR_FK, int,>
--           ,'MISSING TAXONOMY NODE ID - FOR THIS TREE'
--           ,IRD.DATA_POSITION1
--		   ,IRD.DATA_POSITION2
--		   ,ird.DATA_POSITION4
--           ,NULL
--           ,GETDATE ()
--FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
--WHERE JOB_FK = @IN_JOB_FK 
--AND  DATA_POSITION4 not in (SELECT
-- tn.TAXONOMY_NODE_ID
-- from epacube.taxonomy_tree tt
--  inner join epacube.TAXONOMY_NODE tn
--   on (tt.TAXONOMY_TREE_ID = tn.TAXONOMY_TREE_FK)
--left join epacube.TAXONOMY_NODE_ATTRIBUTE tna
--  inner join epacube.data_name dn
-- on (dn.DATA_NAME_ID = tna.DATA_NAME_FK)
--inner join epacube.data_set ds
-- on (ds.DATA_SET_ID = dn.DATA_SET_FK)
-- on (tn.TAXONOMY_NODE_ID = tna.TAXONOMY_NODE_FK)
-- inner join epacube.data_value nodevalue
-- on (nodevalue.DATA_VALUE_ID = tn.TAX_NODE_DV_FK)
--  and tt.NAME =(select NAME from epacube.TAXONOMY_TREE WITH (nolock) where TAXONOMY_TREE_ID =   @V_TREE_id))


  
--update ird 
--set DO_NOT_IMPORT_IND =  1 
--FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
--WHERE JOB_FK = @IN_JOB_FK 
--AND  DATA_POSITION4 not in (SELECT
-- tn.TAXONOMY_NODE_ID
-- from epacube.taxonomy_tree tt
--  inner join epacube.TAXONOMY_NODE tn
--   on (tt.TAXONOMY_TREE_ID = tn.TAXONOMY_TREE_FK)
--left join epacube.TAXONOMY_NODE_ATTRIBUTE tna
--  inner join epacube.data_name dn
-- on (dn.DATA_NAME_ID = tna.DATA_NAME_FK)
--inner join epacube.data_set ds
-- on (ds.DATA_SET_ID = dn.DATA_SET_FK)
-- on (tn.TAXONOMY_NODE_ID = tna.TAXONOMY_NODE_FK)
-- inner join epacube.data_value nodevalue
-- on (nodevalue.DATA_VALUE_ID = tn.TAX_NODE_DV_FK)
-- and tt.NAME =(select NAME from epacube.TAXONOMY_TREE WITH (nolock) where TAXONOMY_TREE_ID = @V_TREE_id))

 -----CHECK NODE NAME
   
--   INSERT INTO Common.Job_ERRORS
--           ([JOB_FK]
--           ,TABLE_NAME
--           ,TABLE_PK
--           ,[EVENT_CONDITION_CR_FK]
--           ,[ERROR_NAME]
--           ,ERROR_DATA1
--           ,ERROR_DATA2
--           ,ERROR_DATA3
--           ,ERROR_DATA4
--           ,[CREATE_TIMESTAMP])
--SELECT distinct
--            IRD.JOB_FK
--            ,'IMPORT TAXONOMY PRODUCT ATTRIBUTES'
--            ,IRD.IMPORT_RECORD_DATA_ID
--           ,99  -- <EVENT_CONDITION_CR_FK, int,>
--           ,'MISSING TAXONOMY NODE VALUE - FOR THIS TREE'
--           ,IRD.DATA_POSITION1
--		   ,ird.DATA_POSITION2
--           ,IRD.DATA_POSITION4
--           ,NULL
--           ,GETDATE ()
--FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
--WHERE JOB_FK = @IN_JOB_FK 
--AND  DATA_POSITION2 not in (SELECT
-- nodevalue.value Tax_node 
-- from epacube.taxonomy_tree tt
--  inner join epacube.TAXONOMY_NODE tn
--   on (tt.TAXONOMY_TREE_ID = tn.TAXONOMY_TREE_FK)
--left join epacube.TAXONOMY_NODE_ATTRIBUTE tna
--  inner join epacube.data_name dn
-- on (dn.DATA_NAME_ID = tna.DATA_NAME_FK)
--inner join epacube.data_set ds
-- on (ds.DATA_SET_ID = dn.DATA_SET_FK)
-- on (tn.TAXONOMY_NODE_ID = tna.TAXONOMY_NODE_FK)
-- inner join epacube.data_value nodevalue
-- on (nodevalue.DATA_VALUE_ID = tn.TAX_NODE_DV_FK)
--  and tt.NAME =(select NAME from epacube.TAXONOMY_TREE WITH (nolock) where TAXONOMY_TREE_ID = @V_TREE_id))



--update ird 
--set DO_NOT_IMPORT_IND =  1 
--FROM import.IMPORT_RECORD_DATA IRD WITH (nolock)
--WHERE JOB_FK = @IN_JOB_FK 
--AND  DATA_POSITION2 not in (SELECT
-- nodevalue.value Tax_node 
-- from epacube.taxonomy_tree tt
--  inner join epacube.TAXONOMY_NODE tn
--   on (tt.TAXONOMY_TREE_ID = tn.TAXONOMY_TREE_FK)
--left join epacube.TAXONOMY_NODE_ATTRIBUTE tna
--  inner join epacube.data_name dn
-- on (dn.DATA_NAME_ID = tna.DATA_NAME_FK)
--inner join epacube.data_set ds
-- on (ds.DATA_SET_ID = dn.DATA_SET_FK)
-- on (tn.TAXONOMY_NODE_ID = tna.TAXONOMY_NODE_FK)
-- inner join epacube.data_value nodevalue
-- on (nodevalue.DATA_VALUE_ID = tn.TAX_NODE_DV_FK)
-- and tt.NAME =(select NAME from epacube.TAXONOMY_TREE WITH (nolock) where TAXONOMY_TREE_ID = @V_TREE_id))


/*
 -- ---------------------------------------------------------------------------------------------------
 --     Create fake event for missing data names.
 ----   user descides to approve or reject events.
 User takes action by rejecting events.  and then running the Add to tax tree 
 if approve - create the data name and add to the tax tree node
 if reject - no worries.
 
 -- ---------------------------------------------------------------------------------------------------
 --*/

----create a "blank " data value entry
--select * from epacube.DATA_VALUE where VALUE = ' ' 



  INSERT INTO [synchronizer].[EVENT_DATA]
           ([EVENT_TYPE_CR_FK]
           ,[EVENT_ENTITY_CLASS_CR_FK]
           ,[EVENT_EFFECTIVE_DATE]
           ,[EPACUBE_ID]
           ,PARENT_STRUCTURE_FK
           ,[DATA_NAME_FK]
           ,[PRODUCT_STRUCTURE_FK]
		   ,ORG_ENTITY_STRUCTURE_FK
           ,[NEW_DATA]
           ,[CURRENT_DATA]
           ,[EVENT_CONDITION_CR_FK]
           ,[EVENT_STATUS_CR_FK]
           ,[EVENT_PRIORITY_CR_FK]
           ,[EVENT_SOURCE_CR_FK]
           ,[EVENT_ACTION_CR_FK]
           ,[IMPORT_JOB_FK]
           ,[IMPORT_DATE]
           ,[IMPORT_PACKAGE_FK]
           ,[IMPORT_FILENAME]
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_USER])
   ( SELECT Distinct
	153
	,10109
	,getdate()
	,0
	,ird.DATA_POSITION4   --- node_id -parent structure fk
	,100001  --- fixed value tna.DATA_NAME_FK
	,0
	,1
	,ird.DATA_POSITION3 --new data
	,'ADD TO NODE - ' + ird.DATA_POSITION2
	,97
	,80
	,61
	,70
	,51
	,ird.job_fk
	,ird.EFFECTIVE_DATE
	,ird.IMPORT_PACKAGE_FK
	,ird.IMPORT_FILENAME
	 ,1
	 ,getdate()
	 ,'Add new data name'
from import.IMPORT_RECORD_DATA  IRD WITH (nolock)
left join epacube.DATA_NAME dn on dn.NAME = ird.DATA_POSITION3
where 1=1
--and dn.DATA_NAME_ID is NULL
and ird.do_not_import_ind is NULL
  and  ird.job_fk =  @IN_JOB_FK
 )  


--select * from epacube.data_value where data_name_fk in (
--select data_name_id  from epacube.data_name where data_name_id > 1000000008)

--delete from epacube.taxonomy_node_attribute where data_name_fk in (
--select data_name_id  from epacube.data_name where data_name_id > 1000000008)

--delete from epacube.data_value where data_name_fk in (
--select data_name_id  from epacube.data_name where data_name_id > 1000000008)

--delete  from epacube.data_name where data_name_id > 1000000008

--update  
-- import.import_map_metadata  
-- set data_name_fk = NULL
-- where data_name_fk 
-- in (select data_name_id  from epacube.data_name where data_name_id > 1000000008)



--select_name * from import.import_map_metadata where import_package_fk = 193000
--delete from synchronizer.EVENT_DATA where IMPORT_JOB_FK = 7257 

--select * from synchronizer.EVENT_DATA where IMPORT_JOB_FK = 7257 
 


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.IMPORT_TAXONOMY_DN_ATTRIBUTE_EVENTS'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk,  @epa_batch_no = @in_job_FK
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.IMPORT_TAXONOMY_DN_ATTRIBUTE_EVENTS' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END
