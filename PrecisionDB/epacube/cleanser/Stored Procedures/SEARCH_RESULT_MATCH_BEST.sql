﻿
















--Build the Search ID Results for Cleanser


-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/20/2006   Inital Verions
-- KS        11/10/2008	  Combined Resolver and Cleanser
-- 

CREATE PROCEDURE [cleanser].[SEARCH_RESULT_MATCH_BEST] 
       @IN_JOB_FK         BIGINT,
       @IN_BATCH_NO       BIGINT

   AS
BEGIN
      DECLARE @l_exec_no   bigint,
              @l_stmt      varchar(8000),
              @ls_exec     varchar(8000),
              @l_sysdate   datetime

		DECLARE @status_desc				varchar (max)
		DECLARE @ErrorMessage				nvarchar(4000)
		DECLARE @ErrorSeverity				int
		DECLARE @ErrorState					int
 

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.SEARCH_RESULT_MATCH_BEST', 
                                                @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no,      
												@exec_id = @l_exec_no OUTPUT;

     SET @l_sysdate = GETDATE()


 
 
DELETE 
FROM CLEANSER.SEARCH_JOB_RESULTS_BEST
WHERE BATCH_NO = @IN_BATCH_NO 

 
   

/*
 -- ------------------------------------------------------------------------------------
 --       Insert Rows for the Activity of RESOLVER
 --			Then Set Best and ID Match to the Top FROM_TO Match
 --			IF NO MATCHES Then leave NULL
 -- -----------------------------------------------------------------------------------
 --*/

INSERT INTO 
CLEANSER.SEARCH_JOB_RESULTS_BEST
( 
SEARCH_JOB_FK,
BATCH_NO,
SEARCH_ACTIVITY_FK,
BEST_SEARCH_FK,               -- STG_RECORD_ID
ACTIVITY_ACTION_CR_FK,        -- DEFAULT TO UNRESOLVED
FROM_SEARCH_FK
)
SELECT 
 SREC.JOB_FK
,@IN_BATCH_NO
,SREC.SEARCH_ACTIVITY_FK
,SREC.STG_RECORD_ID
,140
,( SELECT TOP 1 SJFTM.FROM_SEARCH_FK
   FROM cleanser.SEARCH_JOB_FROM_TO_MATCH SJFTM WITH (NOLOCK)
   WHERE SJFTM.TO_SEARCH_FK = SREC.STG_RECORD_ID
   ORDER BY SJFTM.CONFIDENCE DESC, SJFTM.SEARCH_JOB_FROM_TO_MATCH_ID DESC )
FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
WHERE SREC.BATCH_NO = @IN_BATCH_NO 
AND   SREC.SEARCH_ACTIVITY_FK IN ( 1, 2 )


UPDATE cleanser.SEARCH_JOB_RESULTS_BEST
SET  BEST_CONFIDENCE = SJFTM.CONFIDENCE
    ,SEARCH_ACTIVITY_IDENT_MATCH_RULE_FK = SJRI.SEARCH_ACTIVITY_IDENT_MATCH_RULE_FK
    ,SEARCH_ID_DNA = SJRI.MATCH_DNA
    ,SEARCH_ID_FK  = SJRI.TO_SEARCH_FK
    ,SEARCH_ID_CONFIDENCE = SJRI.CONFIDENCE
    ,SEARCH_JOB_RESULTS_IDENT_FK = SJRI.SEARCH_JOB_RESULTS_IDENT_ID
---------------------------------------------------------------
FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
INNER JOIN cleanser.SEARCH_JOB_FROM_TO_MATCH SJFTM WITH (NOLOCK)
 ON ( SJFTM.TO_SEARCH_FK = SREC.STG_RECORD_ID )
LEFT JOIN cleanser.SEARCH_JOB_RESULTS_IDENT SJRI WITH (NOLOCK)
 ON ( SJRI.SEARCH_JOB_RESULTS_IDENT_ID = SJFTM.SEARCH_JOB_RESULTS_IDENT_FK )
WHERE SREC.BATCH_NO = @IN_BATCH_NO 
AND   SREC.SEARCH_ACTIVITY_FK IN ( 1, 2 )
---
AND   cleanser.SEARCH_JOB_RESULTS_BEST.BEST_SEARCH_FK = SJFTM.TO_SEARCH_FK
AND   cleanser.SEARCH_JOB_RESULTS_BEST.FROM_SEARCH_FK = SJFTM.FROM_SEARCH_FK


   

/*
 -- ------------------------------------------------------------------------------------
 --       Insert BEST Matching Rows for the Activity of DATA CLEANSER
 --       Review V3.2 when ready to do this.
 -- -----------------------------------------------------------------------------------
 --*/




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.SEARCH_RESULT_MATCH_BEST'

Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no
Exec exec_monitor.print_status_sp @exec_id = @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.SEARCH_RESULT_MATCH_BEST has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END


































