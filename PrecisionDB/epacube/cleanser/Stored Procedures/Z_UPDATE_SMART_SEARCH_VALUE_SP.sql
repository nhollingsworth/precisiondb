﻿

-- =============================================
-- Author:		Yuri Nogin
-- Create date: 10.16.2006
-- Description:	Updates smart_search_value column in SEARCH_STRING table
-- =============================================
CREATE PROCEDURE [cleanser].[Z_UPDATE_SMART_SEARCH_VALUE_SP] 
	@sg_data_set_id bigint,
	@smart_search_value varchar(max)
AS
BEGIN
	SET NOCOUNT ON;
    Update cleanser.SEARCH_STRING
    Set    smart_search_string = @smart_search_value
    Where  search_string_id = @sg_data_set_id;
END