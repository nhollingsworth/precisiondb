﻿
-- Copyright 2007
--
-- Procedure created by Someone
--
--
-- Purpose: To Take the Best Match in Resolver
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        05/13/2008   5.0 - Added Matched logic for Resolver
-- CV        09/25/2009   EPA-2566 to fix subquery error.



CREATE PROCEDURE [cleanser].[MATCH_TO_BEST] (
      @in_job_fk     bigint,
      @in_batch_no          BIGINT,
      @in_update_user       VARCHAR(64)
   )
   AS
DECLARE @V_SEARCH_ACTIVITY_FK     BIGINT
DECLARE @IN_ACTIVITY_ACTION_CR_FK BIGINT


BEGIN

SET @V_SEARCH_ACTIVITY_FK = (SELECT SEARCH_ACTIVITY_FK
FROM CLEANSER.SEARCH_JOB
WHERE SEARCH_JOB_ID = @IN_JOB_FK)

IF @V_SEARCH_ACTIVITY_FK IN ( 1, 2 )
BEGIN
       ---  THE BEST ID SEARCH IS THE BEST_SEARCH_FK FOR RESOLVER

--epa2566
DELETE from cleanser.search_job_from_to_match
WHERE search_job_from_to_match_id NOT IN (
---
select sjfm.search_job_from_to_match_id --, sjrb.Best_search_fk , sjrb.best_confidence
from cleanser.SEARCH_JOB_RESULTS_BEST sjrb WITH (NOLOCK)
inner join cleanser.search_job_from_to_match sjfm WITH (NOLOCK)
on (sjfm.to_search_fk = sjrb.best_search_fk
and sjfm.confidence = sjrb.best_confidence)
WHERE sjrb.SEARCH_JOB_FK = @IN_JOB_FK
AND   sjrb.BATCH_NO = @IN_BATCH_NO)
---
AND to_search_fk in (
---
select  sjrb.Best_search_fk 
from cleanser.SEARCH_JOB_RESULTS_BEST sjrb WITH (NOLOCK)
inner join cleanser.search_job_from_to_match sjfm WITH (NOLOCK)
on (sjfm.to_search_fk = sjrb.best_search_fk
and sjfm.confidence = sjrb.best_confidence)
WHERE sjrb.SEARCH_JOB_FK = @IN_JOB_FK
AND   sjrb.BATCH_NO =  @IN_BATCH_NO)


----Delete from cleanser.search_job_from_to_match
----WHERE To_search_fk = (select Best_search_fk
----from cleanser.SEARCH_JOB_RESULTS_BEST 
----WHERE SEARCH_JOB_FK = @IN_JOB_FK
----AND   BATCH_NO = @IN_BATCH_NO)

SET  @IN_ACTIVITY_ACTION_CR_FK  = '147' --REVIEWED

EXEC cleanser.ACTIVITY_ACTION  @IN_ACTIVITY_ACTION_CR_FK , @IN_JOB_FK, @IN_BATCH_NO, @IN_UPDATE_USER 

--UPDATE cleanser.SEARCH_JOB_RESULTS_BEST 
 --  SET  FROM_SEARCH_FK =  FROM_SEARCH_FK 
   --    ,BEST_CONFIDENCE = SEARCH_ID_CONFIDENCE
--WHERE SEARCH_JOB_FK = @IN_JOB_FK
--AND   BATCH_NO = @IN_BATCH_NO



END
ELSE

BEGIN
UPDATE cleanser.SEARCH_JOB_RESULTS_BEST 
   SET BEST_SEARCH_FK = 
					(ISNULL (
						 ( SELECT TOP 1 SJRI.TO_SEARCH_FK
						   FROM CLEANSER.SEARCH_JOB_RESULTS_IDENT SJRI WITH (NOLOCK)
						   WHERE SJRI.SEARCH_JOB_FK = cleanser.SEARCH_JOB_RESULTS_BEST.SEARCH_JOB_FK
						   AND   SJRI.FROM_SEARCH_FK = cleanser.SEARCH_JOB_RESULTS_BEST.FROM_SEARCH_FK 
						   ORDER BY CONFIDENCE DESC  
						 ) ,
						 ( SELECT TOP 1 SRTSK.TO_SEARCH_FK
						   FROM CLEANSER.SEARCH_JOB_RESULTS_TASK SRTSK WITH (NOLOCK)
						   WHERE SRTSK.SEARCH_JOB_FK = cleanser.SEARCH_JOB_RESULTS_BEST.SEARCH_JOB_FK
						   AND   SRTSK.FROM_SEARCH_FK = cleanser.SEARCH_JOB_RESULTS_BEST.FROM_SEARCH_FK 
						   ORDER BY CONFIDENCE DESC 
						 ) 
					) )
       ,BEST_CONFIDENCE = 
					(ISNULL (
						 ( SELECT TOP 1 SJRI.CONFIDENCE
						   FROM CLEANSER.SEARCH_JOB_RESULTS_IDENT SJRI WITH (NOLOCK)
						   WHERE SJRI.SEARCH_JOB_FK = cleanser.SEARCH_JOB_RESULTS_BEST.SEARCH_JOB_FK
						   AND   SJRI.FROM_SEARCH_FK = cleanser.SEARCH_JOB_RESULTS_BEST.FROM_SEARCH_FK 
						   ORDER BY CONFIDENCE DESC 
						  ),
						 ( SELECT TOP 1 SRTSK.CONFIDENCE
						   FROM CLEANSER.SEARCH_JOB_RESULTS_TASK SRTSK WITH (NOLOCK)
						   WHERE SRTSK.SEARCH_JOB_FK = cleanser.SEARCH_JOB_RESULTS_BEST.SEARCH_JOB_FK
						   AND   SRTSK.FROM_SEARCH_FK = cleanser.SEARCH_JOB_RESULTS_BEST.FROM_SEARCH_FK 
						   ORDER BY CONFIDENCE DESC
						  ) 
					 ) )
WHERE SEARCH_JOB_FK = @IN_JOB_FK
AND   BATCH_NO = @IN_BATCH_NO


END


INSERT INTO cleanser.search_job_from_to_match (
       [SEARCH_JOB_FK]
      ,[FROM_SEARCH_FK]
      ,[TO_SEARCH_FK]
      ,[CONFIDENCE]
      ,[UPDATE_USER]
      ,[UPDATE_TIMESTAMP]  )
SELECT 
       [SEARCH_JOB_FK]
      ,[FROM_SEARCH_FK]
      ,[BEST_SEARCH_FK]
      ,[BEST_CONFIDENCE]
      ,@IN_UPDATE_USER
      ,GETDATE()
FROM cleanser.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
AND   SJRB.BATCH_NO = @IN_BATCH_NO
AND   NOT EXISTS  (
         SELECT 1 FROM cleanser.search_job_from_to_match  WITH (NOLOCK)
         WHERE FROM_SEARCH_FK = SJRB.FROM_SEARCH_FK
         AND   TO_SEARCH_FK   = SJRB.BEST_SEARCH_FK )


	
END;
