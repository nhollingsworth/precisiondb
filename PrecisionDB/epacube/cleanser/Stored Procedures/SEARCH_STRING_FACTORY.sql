﻿









-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/20/2006   Build the Search Strings for Cleanser
-- 

CREATE PROCEDURE [cleanser].[SEARCH_STRING_FACTORY] (
      @in_search_set_fk      int,
      @in_data_source        varchar(8),
      @in_job_fk             bigint,
      @IN_EXEC_NO            bigint = NULL
   )
   AS
BEGIN
      DECLARE @l_exec_no   bigint,
              @l_stmt      varchar(8000),
              @ls_exec     varchar(8000)

		DECLARE @status_desc				varchar (max)
		DECLARE @ErrorMessage				nvarchar(4000)
		DECLARE @ErrorSeverity				int
		DECLARE @ErrorState					int
   

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      SET @status_desc = 
       'started execution of cleanser.SEARCH_STRING_FACTORY' 
												
      If IsNull(@IN_EXEC_NO,0) = 0
         EXEC exec_monitor.start_monitor_sp @exec_desc = @status_desc, @exec_id = @l_exec_no OUTPUT;
      Else
         Begin
             SET @l_exec_no = @IN_EXEC_NO;
             Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
         End;


/*
 -- ------------------------------------------------------------------------------------
 --       Initialize the Result table with a row for every Product in Set
 -- -----------------------------------------------------------------------------------
 --*/

	DELETE FROM
	cleanser.search_string
	WHERE search_set_fk = @in_search_set_fk


      
   IF @in_data_source = 'STAGE'            -- IMPORT
   
	   INSERT INTO cleanser.search_string
	   (
		 search_set_fk,
		 product_fk,
		 search_string
	   )
	   SELECT
		 @in_search_set_fk,
		 sps.stg_product_structure_id,
		 null
	   FROM stage.stg_product_structure sps WITH (NOLOCK)
	   WHERE sps.import_job_fk = @in_job_fk 
       AND NOT EXISTS (
           SELECT 1
		   FROM cleanser.search_string sstr WITH (NOLOCK)
           INNER JOIN stage.stg_product_structure sps  WITH (NOLOCK)
                 ON ( sps.stg_product_structure_id = sstr.product_fk )
	       WHERE sps.import_job_fk = @in_job_fk 
		   AND   sstr.search_set_fk = @in_search_set_fk
           )


   IF @in_data_source = 'EPACUBE'           -- EPACUBE

	   INSERT INTO cleanser.search_string
	   (
		 search_set_fk,
		 product_fk,
		 search_string
	   )
	   SELECT
		 @in_search_set_fk,
		 ps.product_structure_id,
		 null
	   FROM epacube.product_structure ps WITH (NOLOCK)
	   INNER JOIN common.job_list_product jlp WITH (NOLOCK)
			   ON ( jlp.product_structure_fk = ps.product_structure_id )
	   WHERE jlp.job_fk = @in_job_fk 
       AND NOT EXISTS (
           SELECT 1
		   FROM cleanser.search_string sstr WITH (NOLOCK)
           INNER JOIN epacube.product_structure ps  WITH (NOLOCK)
                 ON ( ps.product_structure_id = sstr.product_fk )
		   INNER JOIN common.job_list_product jlp WITH (NOLOCK)
				   ON ( jlp.product_structure_fk = ps.product_structure_id )
		   WHERE jlp.job_fk = @in_job_fk 
		   AND sstr.search_set_fk = @in_search_set_fk
           )


/*
 -- ------------------------------------------------------------------------------------
 --        Loop through Set Data Name(s) to build Search Strings
 --              Utilize Product Filter for epaCUBE side  ( Event Source = 74 ) 
 --              Utilize Job_Fk on Import side  ( Event Source = 70 )
 --              Concatenate with separator ( NULL = space )
 -- -----------------------------------------------------------------------------------
 --*/



      DECLARE 
        @STRING_DATA$DATA_NAME_FK int,
        @STRING_DATA$DATA_NAME_SEQ int,
        @STRING_DATA$TABLE_NAME varchar(64),
        @STRING_DATA$DATA_TYPE_CR_FK int


      DECLARE 
        DB_IMPLICIT_CURSOR_FOR_STRING_DATA CURSOR LOCAL 
         FOR 
          SELECT DISTINCT 
              SSDN.DATA_NAME_FK, 
              SSDN.DATA_NAME_SEQ,
              DS.TABLE_NAME,
              DS.DATA_TYPE_CR_FK
            FROM CLEANSER.SEARCH_SET_DATA_NAME SSDN WITH (NOLOCK)
            INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON ( DN.DATA_NAME_ID = SSDN.DATA_NAME_FK )
            INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
            WHERE SSDN.SEARCH_SET_FK = @in_search_set_fk
          ORDER BY SSDN.DATA_NAME_SEQ
      

      OPEN DB_IMPLICIT_CURSOR_FOR_STRING_DATA

      FETCH NEXT FROM DB_IMPLICIT_CURSOR_FOR_STRING_DATA
        INTO 
        @STRING_DATA$DATA_NAME_FK,
        @STRING_DATA$DATA_NAME_SEQ,
        @STRING_DATA$TABLE_NAME,
        @STRING_DATA$DATA_TYPE_CR_FK 
    
      WHILE  NOT(@@FETCH_STATUS = -1)
      BEGIN
    
-- Concatenate to existing value  ( Import Stage Product Data )

          IF @in_data_source = 'STAGE'
               UPDATE cleanser.search_string
               SET search_string  = 
                   ( CASE isnull ( search_string, 'NULL' )
                     WHEN 'NULL' THEN
                        ( SELECT spd.data_value
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN stage.stg_product_data spd WITH (NOLOCK)
                                  ON ( spd.stg_product_structure_fk = sstr.product_fk 
                                  AND  spd.data_name_fk = @STRING_DATA$DATA_NAME_FK )
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id )
                    ELSE
                        ( search_string + ' ' +
                        ( SELECT spd.data_value
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN stage.stg_product_data spd WITH (NOLOCK)
                                  ON ( spd.stg_product_structure_fk = sstr.product_fk 
                                  AND  spd.data_name_fk = @STRING_DATA$DATA_NAME_FK )
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id )
                        ) 
                    END 
                  )
                WHERE search_set_fk = @in_search_set_fk
                AND   @STRING_DATA$TABLE_NAME <> 'ENTITY_IDENTIFICATION'


-- Concatenate to existing value  ( Import Entity Identification )


          IF @in_data_source = 'STAGE'
               UPDATE cleanser.search_string
               SET search_string  = 
                   ( CASE isnull ( search_string, 'NULL' )
                     WHEN 'NULL' THEN
                        ( SELECT ei.value
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN stage.stg_product_structure sps WITH (NOLOCK)
                                  ON ( sps.stg_product_structure_id = sstr.product_fk )
                          INNER JOIN epacube.epacube_params eep WITH (NOLOCK)
                                  ON ( eep.name = 'MANUFACTURER'
										AND eep.APPLICATION_SCOPE_FK = 100 )
                          INNER JOIN epacube.entity_identification ei WITH (NOLOCK)
                                  ON ( ei.data_name_fk = CAST ( eep.value as int )
                                  AND  ei.entity_structure_fk = sps.mfr_entity_structure_fk )
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id )
                     ELSE
                        ( search_string + ' ' +
                        ( SELECT ei.value
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN stage.stg_product_structure sps WITH (NOLOCK)
                                  ON ( sps.stg_product_structure_id = sstr.product_fk )
                          INNER JOIN epacube.epacube_params eep WITH (NOLOCK)
                                  ON ( eep.name = 'MANUFACTURER'
										AND eep.APPLICATION_SCOPE_FK = 100 )
                          INNER JOIN epacube.entity_identification ei WITH (NOLOCK)
                                  ON ( ei.data_name_fk = CAST ( eep.value as int )
                                  AND  ei.entity_structure_fk = sps.mfr_entity_structure_fk )
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id )
                        ) 
                      END
                   )
                WHERE search_set_fk = @in_search_set_fk
                AND   @STRING_DATA$TABLE_NAME = 'ENTITY_IDENTIFICATION'


-- Concatenate to existing value  ( Epacube Entity Identification )


          IF @in_data_source = 'EPACUBE'
               UPDATE cleanser.search_string
               SET search_string  = 
                   ( CASE isnull ( search_string, 'NULL' )
                     WHEN 'NULL' THEN
                        ( SELECT ei.value
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN epacube.product_structure ps WITH (NOLOCK)
                                  ON ( ps.product_structure_id = sstr.product_fk )
                          INNER JOIN epacube.epacube_params eep WITH (NOLOCK)
                                  ON ( eep.name = 'MANUFACTURER'
										AND eep.APPLICATION_SCOPE_FK = 100 )
                          INNER JOIN epacube.entity_identification ei WITH (NOLOCK)
                                  ON ( ei.data_name_fk = CAST ( eep.value as int )
                                  AND  ei.entity_structure_fk = ps.mfr_entity_structure_fk )
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id ) 
                    ELSE
                        ( search_string + ' ' +
                        ( SELECT ei.value
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN epacube.product_structure ps WITH (NOLOCK)
                                  ON ( ps.product_structure_id = sstr.product_fk )
                          INNER JOIN epacube.epacube_params eep WITH (NOLOCK)
                                  ON ( eep.name = 'MANUFACTURER'
										AND eep.APPLICATION_SCOPE_FK = 100 )
                          INNER JOIN epacube.entity_identification ei WITH (NOLOCK)
                                  ON ( ei.data_name_fk = CAST ( eep.value as int )
                                  AND  ei.entity_structure_fk = ps.mfr_entity_structure_fk )
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id ) 
                        ) 
                     END
                   )
                WHERE search_set_fk = @in_search_set_fk
                AND   @STRING_DATA$TABLE_NAME = 'ENTITY_IDENTIFICATION'


-- Concatenate to existing value  ( Product_Identification ) 

          IF @in_data_source = 'EPACUBE'
               UPDATE cleanser.search_string
               SET search_string  = 
                   ( CASE isnull ( search_string, 'NULL' )
                     WHEN 'NULL' THEN
                        ( SELECT pi.value
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN epacube.product_identification pi WITH (NOLOCK)
                                  ON ( pi.product_structure_fk = sstr.product_fk 
                                  AND  pi.data_name_fk = @STRING_DATA$DATA_NAME_FK )
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id ) 
                     ELSE
                        ( search_string + ' ' +
                        ( SELECT pi.value
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN epacube.product_identification pi  WITH (NOLOCK)
                                  ON ( pi.product_structure_fk = sstr.product_fk 
                                  AND  pi.data_name_fk = @STRING_DATA$DATA_NAME_FK )
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id ) 
                        )
                      END
                     )
                WHERE search_set_fk = @in_search_set_fk
                AND   @STRING_DATA$TABLE_NAME = 'PRODUCT_IDENTIFICATION'


-- Concatenate to existing value  ( Product_Description ) 

          IF @in_data_source = 'EPACUBE'
               UPDATE cleanser.search_string
               SET search_string  = 
                   ( CASE isnull ( search_string, 'NULL' )
                     WHEN 'NULL' THEN
                        ( SELECT pd.description
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN epacube.product_description pd WITH (NOLOCK)
                                  ON ( pd.product_structure_fk = sstr.product_fk 
                                  AND  pd.data_name_fk = @STRING_DATA$DATA_NAME_FK )
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id ) 
                     ELSE
                        ( search_string + ' ' +
                        ( SELECT pd.description
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN epacube.product_description pd WITH (NOLOCK)
                                  ON ( pd.product_structure_fk = sstr.product_fk 
                                  AND  pd.data_name_fk = @STRING_DATA$DATA_NAME_FK )
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id ) 
                        ) 
                     END
                    )
                WHERE search_set_fk = @in_search_set_fk
                AND   @STRING_DATA$TABLE_NAME = 'PRODUCT_DESCRIPTION'


-- Concatenate to existing value  ( Product_Category 134 ) 

          IF @in_data_source = 'EPACUBE'
               UPDATE cleanser.search_string
               SET search_string  = 
                   ( CASE isnull ( search_string, 'NULL' )
                     WHEN 'NULL' THEN
                        ( SELECT dv.value
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN epacube.product_category pc WITH (NOLOCK)
                                  ON ( pc.product_structure_fk = sstr.product_fk 
                                  AND  pc.data_name_fk = @STRING_DATA$DATA_NAME_FK )
                          INNER JOIN epacube.data_value dv WITH (NOLOCK)
                                  ON ( dv.data_value_id = pc.data_value_fk ) 
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id ) 
                     ELSE
                        ( search_string + ' ' +
                        ( SELECT dv.value
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN epacube.product_category pc WITH (NOLOCK)
                                  ON ( pc.product_structure_fk = sstr.product_fk 
                                  AND  pc.data_name_fk = @STRING_DATA$DATA_NAME_FK )
                          INNER JOIN epacube.data_value dv WITH (NOLOCK)
                                  ON ( dv.data_value_id = pc.data_value_fk ) 
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id ) 
                        ) 
                     END
                    )
                WHERE search_set_fk = @in_search_set_fk
                AND   @STRING_DATA$TABLE_NAME = 'PRODUCT_CATEGORY'
                AND   @STRING_DATA$DATA_TYPE_CR_FK = 134


-- Concatenate to existing value  ( Product_Category 135 ) 

          IF @in_data_source = 'EPACUBE'
               UPDATE cleanser.search_string
               SET search_string  = 
                   ( CASE isnull ( search_string, 'NULL' )
                     WHEN 'NULL' THEN
                        ( SELECT edv.value
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN epacube.product_category pc WITH (NOLOCK)
                                  ON ( pc.product_structure_fk = sstr.product_fk 
                                  AND  pc.data_name_fk = @STRING_DATA$DATA_NAME_FK )
                          INNER JOIN epacube.entity_data_value edv WITH (NOLOCK)
                                  ON ( edv.entity_data_value_id = pc.entity_data_value_fk ) 
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id ) 
                     ELSE
                        ( search_string + ' ' +
                        ( SELECT edv.value
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN epacube.product_category pc WITH (NOLOCK)
                                  ON ( pc.product_structure_fk = sstr.product_fk 
                                  AND  pc.data_name_fk = @STRING_DATA$DATA_NAME_FK )
                          INNER JOIN epacube.entity_data_value edv WITH (NOLOCK)
                                  ON ( edv.entity_data_value_id = pc.entity_data_value_fk ) 
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id ) 
                        ) 
                     END
                   )
                WHERE search_set_fk = @in_search_set_fk
                AND   @STRING_DATA$TABLE_NAME = 'PRODUCT_CATEGORY'
                AND   @STRING_DATA$DATA_TYPE_CR_FK = 135



-- Concatenate to existing value  ( Product_Attribute ) 

          IF @in_data_source = 'EPACUBE'
               UPDATE cleanser.search_string
               SET search_string  = 
                   ( CASE isnull ( search_string, 'NULL' )
                     WHEN 'NULL' THEN
                        ( SELECT pa.attribute_event_data
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN epacube.product_attribute pa WITH (NOLOCK)
                                  ON ( pa.product_structure_fk = sstr.product_fk 
                                  AND  pa.data_name_fk = @STRING_DATA$DATA_NAME_FK )
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id ) 
                     ELSE
                        ( search_string + ' ' +
                        ( SELECT pa.attribute_event_data
                          FROM cleanser.search_string sstr WITH (NOLOCK)
                          INNER JOIN epacube.product_attribute pa WITH (NOLOCK)
                                  ON ( pa.product_structure_fk = sstr.product_fk 
                                  AND  pa.data_name_fk = @STRING_DATA$DATA_NAME_FK )
                          WHERE sstr.search_string_id = cleanser.search_string.search_string_id ) 
                        ) 
                     END
                    )
                WHERE search_set_fk = @in_search_set_fk
                AND   @STRING_DATA$TABLE_NAME = 'PRODUCT_ATTRIBUTE'




      FETCH NEXT FROM DB_IMPLICIT_CURSOR_FOR_STRING_DATA
        INTO 
        @STRING_DATA$DATA_NAME_FK,
        @STRING_DATA$DATA_NAME_SEQ,
        @STRING_DATA$TABLE_NAME,
        @STRING_DATA$DATA_TYPE_CR_FK 

    END
        /*  Data Name Loop */



   DELETE 
   FROM CLEANSER.SEARCH_STRING
   WHERE search_set_fk = @in_search_set_fk
   AND   search_string IS NULL

   UPDATE CLEANSER.SEARCH_STRING
   SET SEARCH_STRING = 
          (   UPPER
                   (LTRIM
                       (RTRIM (utilities.TRANSLATE (search_string,
                                          '`~!@#$%^&*():;?/<>,."-_{}[]+=|\''',
                                          '                                 '
                                         )
                              )
                       )
                   ) 
           )
   WHERE search_set_fk = @in_search_set_fk
   AND   search_string IS NOT NULL

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------
--
SET @status_desc = 'finished execution of cleanser.SEARCH_STRING_FACTORY'
                         + '; Search Set:' + cast(@in_search_set_fk as varchar(20))
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
Exec exec_monitor.print_status_sp @exec_id = @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.SEARCH_STRING_FACTORY has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END



















