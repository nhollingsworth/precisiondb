﻿


CREATE PROCEDURE [cleanser].[ACTIVITY_ACTION] (
      @IN_ACTIVITY_ACTION_CR_FK          INT,
      @IN_JOB_FK                         BIGINT,      
      @IN_BATCH_NO                       BIGINT,
      @IN_UPDATE_USER                    VARCHAR(64)
   )
   AS
BEGIN

DECLARE  @V_SEARCH_ACTIIVTY_FK int,
@L_SYSDATE DATETIME,
@UnresolvedStatus int,
@DiscardStatus int,
@InactivateStatus int,
@AddStatus int,
@MergeStatus int,
@HoldStatus int,
@ReviewedStatus int,
@ChangeAction int,
@AddNewAction int,
@GreenCondition int

-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        12/17/2009  EPA-2695. Fixing Cleansed_indicator sets; added Mod History
-- CV	     04/26/2010  EPA-2941 Not marking Cleansed indicator because of errors in 
---                      STG error table.
-- CV        08/18/2010  Added to remove errors if product marked as Reviewed.
-- CV        01/18/2011  added to remove error to include batch no
-- CV        03/18/2013  Add Update cleanser ind to review section
-- CV        07/30/2014  Add Update cleanser ind to new section
-- CV		 03/24/2015  Can't mark cleansed ind or will not display on UI screen
	
/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   
-------------------------------------------------------------------------------------------------
--   FLOW:
--
--	 ACTION REVIEWED ( 147 ) DOES NOT APPLY TO RESOLVER
--   ACTION MERGED ( 145 ) DOES NOT APPLY TO RESOLVER
--	 IF CURRENT ACTION IS ADD NEW AND CHANGING IT THEN NEED TO DELETE STRUCTURE_FK IN EPACUBE
--   MUST RUN CLEANSER VALIDATION
--   THEN RUN INDIVIDUAL ACTIONS
--
--	 ADD		WILL CALL NEW_PRODUCT_STRUCTURE OR NEW_ENTITY_STRUCTURE
--	 
-------------------------------------------------------------------------------------------------
*/

SET @V_SEARCH_ACTIIVTY_FK = ( SELECT SEARCH_ACTIVITY_FK
                              FROM cleanser.SEARCH_JOB WITH (NOLOCK)
                              WHERE SEARCH_JOB_ID = @IN_JOB_FK )

SET @UnresolvedStatus = epacube.getCodeRefId('ACTIVITY_ACTION','UNRESOLVED')
SET @DiscardStatus = epacube.getCodeRefId('ACTIVITY_ACTION','DISCARD')
SET @InactivateStatus = epacube.getCodeRefId('ACTIVITY_ACTION','INACTIVATE')
SET @AddStatus = epacube.getCodeRefId('ACTIVITY_ACTION','ADD')
SET @MergeStatus = epacube.getCodeRefId('ACTIVITY_ACTION','MERGE')
SET @HoldStatus = epacube.getCodeRefId('ACTIVITY_ACTION','HOLD')
SET @ReviewedStatus = epacube.getCodeRefId('ACTIVITY_ACTION','REVIEWED')
SET @ChangeAction = epacube.getCodeRefId('EVENT_ACTION','CHANGE')
SET @AddNewAction = epacube.getCodeRefId('EVENT_ACTION','ADD NEW')
SET @GreenCondition = epacube.getCodeRefId('EVENT_CONDITION','GREEN')


IF @V_SEARCH_ACTIIVTY_FK IN ( 1, 2 )  --- RESOLVER

BEGIN

	IF @IN_ACTIVITY_ACTION_CR_FK IN (@MergeStatus)  --- DO NOTHING

	BEGIN 
        SET @L_SYSDATE = GETDATE()  
    END

ELSE

---- IF ADD AS NEW THEN REMOVE MATCHES 
IF @IN_ACTIVITY_ACTION_CR_FK IN ( @AddStatus)  --- ADD NEW
	BEGIN 

--EPA 2941 
DELETE FROM STAGE.STG_RECORD_ERRORS 
where job_fk = @in_Job_FK
AND STG_RECORD_FK in (select STG_RECORD_ID from stage.STG_RECORD SREC with (nolock)
INNER JOIN CLEANSER.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
		ON (SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK
		AND SREC.JOB_FK = SJRB.SEARCH_JOB_FK )
        WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
        AND   SJRB.BATCH_NO = @IN_BATCH_NO )

 --- added epa 3133
        
		EXEC cleanser.REMOVE_MATCH_SCISSOR  @IN_JOB_FK, @IN_BATCH_NO, @IN_UPDATE_USER 

 
   
---- IF DISCARD CLEAR THE MATCHES REMOVE THE EPACUBE PRODUCT FROM_SEARCH_FK SET ACTIVITY

---- IF HOLD JUST SET ACTIVITY 

-------------------------------------------------------------------------------------------------
--   UPDATE STG RECORDS TO SAME BATCH as the BEST RECORDS
-------------------------------------------------------------------------------------------------

                 

  UPDATE STAGE.STG_RECORD
  SET BATCH_NO = @IN_BATCH_NO
     ,ACTIVITY_ACTION_CR_FK = @IN_ACTIVITY_ACTION_CR_FK
  WHERE STG_RECORD_ID IN (
		SELECT STG_RECORD_ID
		FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
		INNER JOIN CLEANSER.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
		ON (SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK
		AND SREC.JOB_FK = SJRB.SEARCH_JOB_FK )
        WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
        AND   SJRB.BATCH_NO = @IN_BATCH_NO )

			UPDATE STAGE.STG_RECORD
			  SET EVENT_ACTION_CR_FK = @AddNewAction
				,EVENT_CONDITION_CR_FK = @GreenCondition	  --CV added
			  WHERE JOB_FK = @IN_JOB_FK
			  AND   BATCH_NO = @IN_BATCH_NO 
			  

-------------------------------------------------------------------------------------------------
--   NEW, UNRESOLVED
--   Set Action and Clear out BEST data; WHEN TO Clear out Matches
-------------------------------------------------------------------------------------------------

  --UPDATE cleanser.SEARCH_JOB_RESULTS_BEST
  --SET ACTIVITY_ACTION_CR_FK = @IN_ACTIVITY_ACTION_CR_FK
  --,CLEANSED_INDICATOR = 1 --CV added
  --WHERE BATCH_NO = @IN_BATCH_NO
  
     
   EXEC [cleanser].[VALIDATE_NEW_ENTITY] 
   @IN_JOB_FK
   


END  

IF @IN_ACTIVITY_ACTION_CR_FK IN ( @ReviewedStatus)  --- REVIEWED
	BEGIN 
          

DELETE FROM STAGE.STG_RECORD_ERRORS 
where job_fk = @in_Job_FK
AND STG_RECORD_FK in (select STG_RECORD_ID from stage.STG_RECORD SREC with (nolock)
INNER JOIN CLEANSER.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
		ON (SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK
		AND SREC.JOB_FK = SJRB.SEARCH_JOB_FK )
        WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
        AND   SJRB.BATCH_NO = @IN_BATCH_NO )

          
UPDATE STAGE.STG_RECORD
  SET BATCH_NO = @IN_BATCH_NO
		,ACTIVITY_ACTION_CR_FK = @IN_ACTIVITY_ACTION_CR_FK
		,EVENT_ACTION_CR_FK = @ChangeAction 
		,EVENT_CONDITION_CR_FK = @GreenCondition	  --CV added
	  WHERE STG_RECORD_ID IN (
		SELECT STG_RECORD_ID
		FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
		INNER JOIN CLEANSER.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
		ON (SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK
		AND SREC.JOB_FK = SJRB.SEARCH_JOB_FK )
        WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
        AND   SJRB.BATCH_NO = @IN_BATCH_NO )


		--commented out because will not display if cleansed = 1
----UPDATE cleanser.SEARCH_JOB_RESULTS_BEST
----  SET ACTIVITY_ACTION_CR_FK = @IN_ACTIVITY_ACTION_CR_FK
----  ,CLEANSED_INDICATOR = 1 --CV added
----  WHERE BATCH_NO = @IN_BATCH_NO
   
    
   EXEC [cleanser].[VALIDATE_NEW_ENTITY] 
   @IN_JOB_FK
   
 
   
   
   

END

 ELSE 

UPDATE STAGE.STG_RECORD
  SET BATCH_NO = @IN_BATCH_NO
      ,ACTIVITY_ACTION_CR_FK = @IN_ACTIVITY_ACTION_CR_FK
	  WHERE STG_RECORD_ID IN (
		SELECT STG_RECORD_ID
		FROM STAGE.STG_RECORD SREC WITH (NOLOCK)
		INNER JOIN CLEANSER.SEARCH_JOB_RESULTS_BEST SJRB WITH (NOLOCK)
		ON (SREC.STG_RECORD_ID = SJRB.BEST_SEARCH_FK
		AND SREC.JOB_FK = SJRB.SEARCH_JOB_FK )
        WHERE SJRB.SEARCH_JOB_FK = @IN_JOB_FK
        AND   SJRB.BATCH_NO = @IN_BATCH_NO )

UPDATE cleanser.SEARCH_JOB_RESULTS_BEST
	SET ACTIVITY_ACTION_CR_FK = @IN_ACTIVITY_ACTION_CR_FK
		WHERE BATCH_NO = @IN_BATCH_NO

UPDATE cleanser.SEARCH_JOB_RESULTS_BEST
	SET CLEANSED_INDICATOR = 1 --CV added
  WHERE BATCH_NO = @IN_BATCH_NO
	and ACTIVITY_ACTION_CR_FK in (@DiscardStatus, @InactivateStatus)
  
END
--- ELSE
--- BEGIN
---cleanser starts here

END


