﻿









-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: For Stg Records submitted to Synhcronizer
--			Create any missing entities associated with them
--			Insert all Identifications also associated with them
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/28/2008   Initial SQL Version


CREATE PROCEDURE [cleanser].[REPROCESS_MATCHING] 
       @IN_JOB_FK         BIGINT
      ,@IN_BATCH_NO       BIGINT      
      ,@IN_UPDATE_USER    VARCHAR (64) 
AS
BEGIN


/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   Before a Stg Entity can leave cleanser it must have an entity_structure_fk assigned
--   and its UNIQUE identifiers established.  This is to avoid the next import from creating
--	 a duplicate of the same entity prior to this entities events being approved.
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--	 All Import Columns referencing Entities MUST be setup in the IMPORT_MAP_METADATA table
--	 with a data_name of data_set ENTITY_STRUCTURE with its REQUIRED Identification Data Name
--   and its option Data Association Data Name
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   STAGE.LOAD_ENTITIES
--   STAGE.LOAD_ENTITY_ENTITY_NAMES
--   Data inserted into Stage.STG_RECORD
--   cleanser.SEARCH_ENTITY_RESULT_MATCH_IDENT
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--   New "ROW" Entities Must be established first in order to create all needed entity_structure_fk's 
--	 To ensure the entity hierarchy is maintained properly; Insert Level 1's first then others
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   Next Create any missing "ASSOCIATION" Entities known by their UNIQUE identifiers 
-------------------------------------------------------------------------------------------------
--   PROCESS STEP3: 
--   
-- 
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

DECLARE @l_sysdate           datetime
DECLARE @ls_stmt             VARCHAR (1000)
DECLARE @ls_exec             nvarchar (max)
DECLARE @l_exec_no           bigint
DECLARE @l_rows_processed    bigint
DECLARE @v_batch_no          bigint

DECLARE @v_input_rows        int
DECLARE @v_output_rows       int
DECLARE @v_exception_rows    int
DECLARE @v_sysdate           datetime

DECLARE @status_desc		 varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

DECLARE @v_count                    INT
DECLARE @v_search_activity_fk		INT
DECLARE @v_entity_class_fk		    INT



BEGIN TRY
SET NOCOUNT ON;
SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of cleanser.REPROCESS_MATCHING', 
                                                @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no,
												@exec_id = @l_exec_no OUTPUT;


SET @l_sysdate = GETDATE()

SET @v_search_activity_fk = ( SELECT SEARCH_ACTIVITY_FK
                              FROM CLEANSER.SEARCH_JOB WITH (NOLOCK)
                              WHERE SEARCH_JOB_ID = @IN_JOB_FK )


SET @v_entity_class_fk    = ( SELECT TOP 1 ENTITY_CLASS_CR_FK
                              FROM STAGE.STG_RECORD WITH (NOLOCK)
                              WHERE JOB_FK = @IN_JOB_FK )

 -- ------------------------------------------------------------------------------------
 --      IF BATCH IS NULL THEN ASSIGN ALL UNCLEANSED ROWS THAT BELONG TO THE JOB
 -- -----------------------------------------------------------------------------------

 IF @IN_BATCH_NO IS NULL
 BEGIN
 
 	EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'batch_seq', @IN_BATCH_NO output

    UPDATE stage.stg_record
    SET BATCH_NO = @IN_BATCH_NO
    WHERE STG_RECORD_ID IN (
             SELECT BEST_SEARCH_FK
             FROM cleanser.SEARCH_JOB_RESULTS_BEST WITH (NOLOCK)
             WHERE SEARCH_JOB_FK= @IN_JOB_FK
             AND   CLEANSED_INDICATOR <> 1 )
	
 END


 -- ------------------------------------------------------------------------------------
 --      Reset the Row to UNRESOLVED and GREEN
 -- -----------------------------------------------------------------------------------
  UPDATE stage.stg_record
    SET BATCH_NO = @IN_BATCH_NO
    WHERE STG_RECORD_ID IN (
             SELECT BEST_SEARCH_FK
             FROM cleanser.SEARCH_JOB_RESULTS_BEST WITH (NOLOCK)
             WHERE SEARCH_JOB_FK= @IN_JOB_FK
             AND   BATCH_NO = @IN_BATCH_NO )


	UPDATE stage.stg_record 
	set    event_action_cr_fk     = NULL 
          ,event_condition_cr_fk  = 97  -- Green
		  ,ACTIVITY_ACTION_CR_FK  = 140 -- UNRESOLVED	  
		  ,update_timestamp       = @L_SYSDATE
		  ,UPDATE_USER            = @IN_UPDATE_USER
	WHERE batch_no = @in_batch_no


 -- ------------------------------------------------------------------------------------
 --      Deleting all existing Matching data if NOT the import batch
 -- -----------------------------------------------------------------------------------



    DELETE 
    FROM stage.stg_record_errors
    WHERE stg_record_fk IN  
           ( SELECT stg_record_id
             FROM stage.stg_record WITH (NOLOCK)
             WHERE batch_no = @in_batch_no )


--- always driven by to_search_fk as import data
	DELETE
	FROM cleanser.SEARCH_JOB_FROM_TO_MATCH
	WHERE to_search_fk IN 
           ( SELECT stg_record_id
             FROM stage.stg_record WITH (NOLOCK)
             WHERE batch_no = @in_batch_no )

	DELETE 
	FROM cleanser.SEARCH_JOB_RESULTS_IDENT
	WHERE to_search_fk IN 
           ( SELECT stg_record_id
             FROM stage.stg_record WITH (NOLOCK)
             WHERE batch_no = @in_batch_no )

	DELETE 
	FROM cleanser.SEARCH_JOB_RESULTS_TASK
	WHERE to_search_fk IN 
           ( SELECT stg_record_id
             FROM stage.stg_record WITH (NOLOCK)
             WHERE batch_no = @in_batch_no )

	DELETE 
	FROM cleanser.SEARCH_JOB_RESULTS_BEST
	WHERE best_search_fk IN 
           ( SELECT stg_record_id
             FROM stage.stg_record WITH (NOLOCK)
             WHERE batch_no = @in_batch_no )




---------------------------------------------------------------------------------------
----   PERFORM DATA CLEANSER MATCHING
---------------------------------------------------------------------------------------


      EXEC cleanser.SEARCH_RESULT_MATCH_ACTIVITY
                @IN_JOB_FK, @IN_BATCH_NO , @V_SEARCH_ACTIVITY_FK

----	  set @v_sysdate = getdate();
----      set @v_input_rows = @v_output_rows;

----      SELECT @v_output_rows = COUNT(1)
----        FROM stage.STG_ENTITY_STRUCTURE WITH (NOLOCK)
----       WHERE import_job_fk = @in_job_fk
----	   AND   ACTIVITY_ACTION_CR_FK <> 140
----
----	  set @v_exception_rows = @v_input_rows - @v_output_rows;
----      SET @l_sysdate = getdate()
----      exec job_execution_create  @in_job_fk, 'ENTITY MATCHING',
----                                @v_input_rows, @v_output_rows, @v_exception_rows,
----							 201, @v_sysdate, @l_sysdate  ;




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of cleanser.REPROCESS_MATCHING'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_job_id = @in_job_fk, @epa_batch_no = @in_batch_no
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of cleanser.REPROCESS_MATCHING has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH
		  
END









































































































































































