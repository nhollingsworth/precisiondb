﻿



CREATE PROCEDURE [cleanser].[SEARCH_RESULT_MATCH_TASK] (
      
      @search_task_fk                  int,
      @a_number_of_threads             smallint = 4,
      @split_flag                      smallint = 1,
      @top_n_rows                      smallint = 50,
      @IN_EXEC_NO                      bigint = NULL,
      @IN_DIRECT_MATCHING_THRESHOLD    int  = 0,
      @IN_REVERSE_MATCHING_THRESHOLD   int  = 50
       
   )
   AS
BEGIN
      DECLARE @l_exec_no   bigint,
              @ls_step_i   varchar(10),
              @status_desc varchar(max),
              @l_stmt      varchar(8000),
              @ls_exec     varchar(8000),
              @ls_cmd_exec varchar(8000),
              @l_job_queue varchar(8000),
              @search_activity_fk    int,
              @task_seq              int,
              @a_from_data_set_fk    bigint,
              @a_to_data_set_fk      bigint,
              @sExec       nvarchar(4000);
      
DECLARE @l_cleanser_path   varchar(300); 
DECLARE @l_server_name     varchar(300); 

SET NOCOUNT ON;

BEGIN TRY
	  SELECT @l_server_name = cast(SERVERPROPERTY ( 'SERVERNAME' ) as varchar(300))
      SET @l_cleanser_path = 'c:\SmartTextCleanser\'
      SET @l_job_queue = '';
      SET @l_exec_no = 0;
      SET @status_desc = 
       'started execution of cleanser.SEARCH_RESULT_MATCH_TASK' 
       + '; From Data Set:' + cast(@a_from_data_set_fk as varchar(20))
       + '; To Data Set:' + cast(@a_to_data_set_fk as varchar(20)); 
												
      If IsNull(@IN_EXEC_NO,0) = 0
         EXEC exec_monitor.start_monitor_sp @exec_desc = @status_desc, @exec_id = @l_exec_no OUTPUT;
      Else
         Begin
             SET @l_exec_no = @IN_EXEC_NO;
             Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
         End;

      -- select values
      select top 1 @search_activity_fk = SEARCH_ACTIVITY_FK, @task_seq = TASK_SEQ,
                   @a_from_data_set_fk = FROM_SEARCH_SET_FK, @a_to_data_set_fk = TO_SEARCH_SET_FK
      from epacube.cleanser.SEARCH_TASK WITH (NOLOCK)
      where @search_task_fk = SEARCH_TASK_ID;

      --lets split the tokens in datasets first
      -- 1 from data set
      if @split_flag > 0 
      BEGIN     
              --DROP FULL TEXT INDEX FIRST
              SET @ls_exec = 'drop fulltext index on epacube.cleanser.SEARCH_STRING_WORK'
			  Exec exec_manager.Exec_Immediate_sp @exec_id = @l_exec_no, 
														 @statement = @ls_exec;
              --put back the step to rebuild the Full text catalog
              -- done by YN 8/9/2007
              ALTER FULLTEXT CATALOG Cleanser_FTC rebuild;

			  --new step by YN 4/2/2007 to use CLR function to clean strings
              update cleanser.SEARCH_STRING
			  set SMART_SEARCH_STRING = utilities.fn_CleanString(search_string,' ','asc')
              where SEARCH_SET_fK in (@a_from_data_set_fk,@a_to_data_set_fk);
			  
              --split tokens with SmartCleanserConsole external application
		      SET @ls_cmd_exec = @l_cleanser_path + 'smartcleanserconsole.exe /Server=' + @l_server_name + ' /User=epacube /Password=epacube /SetID='+ cast(@a_from_data_set_fk as varchar(20))+' /DictFile=' + @l_cleanser_path + 'cleanser.dict /Database=epacube /DictExceptionsFile=' + @l_cleanser_path + 'dict_excep' + cast(@a_from_data_set_fk as varchar(20)) +'.txt /LogFile=' + @l_cleanser_path + 'smart_log' + cast(@a_from_data_set_fk as varchar(20)) + '.txt /PKColumn=search_string_id /Table=cleanser.SEARCH_STRING /ExecNo=' + cast(@l_exec_no as varchar(20))

			  Exec exec_manager.exec_post_cmd_sp @exec_id = @l_exec_no, 
														@statement = @ls_cmd_exec,
														@JobQueue = @l_job_queue OUTPUT;
			  --Print 'JobQueue: ' + isnull(@l_job_queue,' ');
			  SET @status_desc =  'Executing Smart Text Cleanser for ' + cast(@a_from_data_set_fk as varchar(20)) + ' set_id';
			  Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;  

			  -- 2 to data set	
			  SET @ls_cmd_exec = @l_cleanser_path + 'smartcleanserconsole.exe /Server=' + @l_server_name + ' /User=epacube /Password=epacube /SetID='+ cast(@a_to_data_set_fk as varchar(20))+' /DictFile=' + @l_cleanser_path + 'cleanser.dict /Database=epacube /DictExceptionsFile=' + @l_cleanser_path + 'dict_excep' + cast(@a_to_data_set_fk as varchar(20)) +'.txt /LogFile=' + @l_cleanser_path + 'smart_log' + cast(@a_to_data_set_fk as varchar(20)) + '.txt /PKColumn=search_string_id /Table=cleanser.SEARCH_STRING /ExecNo=' + cast(@l_exec_no as varchar(20))
			  Exec exec_manager.exec_post_cmd_sp @exec_id = @l_exec_no, 
														@statement = @ls_cmd_exec,
														@JobQueue = @l_job_queue OUTPUT;
			  --Print 'JobQueue: ' + isnull(@l_job_queue,' ');
			  SET @status_desc =  'Executing Smart Text Cleanser for ' + cast(@a_to_data_set_fk as varchar(20)) + ' set_id';
			  Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;  

			  --wait for SmartCleanserConsole jobs to finish

			  Exec exec_manager.Wait_For_All_Procs_To_Finish_sp @exec_id = @l_exec_no, 
																	   @JobQueue = @l_job_queue;

             /************************************************************************************************/
              -- NEW STEP TO COPY DATA INTO THE WORK TABLE (TRUNCATE FIRST) ADDED BY YURI NOGIN ON 8/5/2007
             /************************************************************************************************/
              SET @ls_exec = 'truncate table epacube.cleanser.SEARCH_STRING_WORK'
			  Exec exec_manager.Exec_Immediate_sp @exec_id = @l_exec_no, 
														 @statement = @ls_exec; 


              SET @ls_exec = 'truncate table epacube.cleanser.SEARCH_RESULTS_TASK_WORK'
			  Exec exec_manager.Exec_Immediate_sp @exec_id = @l_exec_no, 
														 @statement = @ls_exec;

              INSERT INTO epacube.cleanser.SEARCH_STRING_WORK
              (SEARCH_STRING_ID, SEARCH_SET_FK, PRODUCT_FK, SEARCH_STRING, SORTED_SEARCH_STRING, SMART_SEARCH_STRING, UPDATE_TIMESTAMP)
              SELECT  SEARCH_STRING_ID, SEARCH_SET_FK, PRODUCT_FK, SEARCH_STRING, SORTED_SEARCH_STRING, SMART_SEARCH_STRING, UPDATE_TIMESTAMP
              FROM epacube.cleanser.SEARCH_STRING WITH (NOLOCK)
              WHERE SEARCH_SET_FK in (@a_from_data_set_fk,@a_to_data_set_fk);


             /************************************************************************************************/
              -- END OF NEW MODIFICATIONS BY YURI NOGIN ON 8/5/2007
             /************************************************************************************************/
			  
              --RESTORE FULL TEXT INDEX FIRST
              SET @ls_exec = 'create fulltext index on epacube.cleanser.SEARCH_STRING_WORK (smart_search_string) key index PK_SSTRW on Cleanser_FTC with change_tracking off'
			  Exec exec_manager.Exec_Immediate_sp @exec_id = @l_exec_no, 
														 @statement = @ls_exec;
              ALTER FULLTEXT CATALOG [Cleanser_FTC] REORGANIZE;
			 

      END --split tokens

           
      BEGIN -- Start Full Text Matching Process
      DECLARE @i smallint;
      
      

      select @i = 1,@l_job_queue = ''; 
            WHILE @i <= @a_number_of_threads
            BEGIN -- thread loop
               set @l_stmt =
                           'exec epacube.cleanser.smart_match_data_sets '
                           + cast(@search_activity_fk as varchar(20))
                           + ','
                           + cast(@search_task_fk as varchar(20))
                           + ','
                           + cast(@task_seq as varchar(20))
                           + ',' 
                           + cast(@a_from_data_set_fk as varchar(20))
                           + ','
                           + cast(@a_to_data_set_fk as varchar(20))
                           + ','
                           + cast(@l_exec_no as varchar(20))
                           + ','
                           + cast(@a_number_of_threads as varchar(5))
                           + ','
                           + cast(@i as varchar(5))
                           + ','
                           + cast(@top_n_rows as varchar(5))
						   + ','
                           + '1'
                           + ','
						   + cast(IsNull(@IN_DIRECT_MATCHING_THRESHOLD,0) as varchar(20))
						   + ','
						   + cast(IsNull(@IN_REVERSE_MATCHING_THRESHOLD,0) as varchar(20));
                --Print @l_stmt;    
                Exec exec_manager.exec_post_sp @exec_id = @l_exec_no, 
                                                      @statement = @l_stmt,
                                                      @JobQueue = @l_job_queue OUTPUT;
                --Print 'JobQueue: ' + isnull(@l_job_queue,' ');
                SET @status_desc =  'Executing: ' + @l_stmt;
                Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;  
                Set @i = @i + 1;                                  
            END; --thread loop

            Exec exec_manager.Wait_For_All_Procs_To_Finish_sp @exec_id = @l_exec_no, 
                                                                     @JobQueue = @l_job_queue;
           
            SET @status_desc = 'all threads of cleanser.smart_match_data_sets for '
                               + cast(@a_from_data_set_fk as varchar(20)) + ' - ' + cast(@a_to_data_set_fk as varchar(20))+
                               ' reverse matching have finished';
            Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


            
            select @i = 1,@l_job_queue = ''; 
            WHILE @i <= @a_number_of_threads
            BEGIN -- thread loop
               set @l_stmt =
                           'exec epacube.cleanser.smart_match_data_sets '
                           + cast(@search_activity_fk as varchar(20))
                           + ','
                           + cast(@search_task_fk as varchar(20))
                           + ','
                           + cast(@task_seq as varchar(20))
                           + ',' 
                           + cast(@a_from_data_set_fk as varchar(20))
                           + ','
                           + cast(@a_to_data_set_fk as varchar(20))
                           + ','
                           + cast(@l_exec_no as varchar(20))
                           + ','
                           + cast(@a_number_of_threads as varchar(5))
                           + ','
                           + cast(@i as varchar(5))
                           + ','
                           + cast(@top_n_rows as varchar(5))
						   + ','
                           + '0'
                           + ','
						   + cast(IsNull(@IN_DIRECT_MATCHING_THRESHOLD,0) as varchar(20))
						   + ','
						   + cast(IsNull(@IN_REVERSE_MATCHING_THRESHOLD,0) as varchar(20));
                --Print @l_stmt;    
                Exec exec_manager.exec_post_sp @exec_id = @l_exec_no, 
                                                      @statement = @l_stmt,
                                                      @JobQueue = @l_job_queue OUTPUT;
                --Print 'JobQueue: ' + isnull(@l_job_queue,' ');
                SET @status_desc =  'Executing: ' + @l_stmt;
                Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;  
                Set @i = @i + 1;                                  
            END; --thread loop

            Exec exec_manager.Wait_For_All_Procs_To_Finish_sp @exec_id = @l_exec_no, 
                                                                     @JobQueue = @l_job_queue;
           
            SET @status_desc = 'all threads of cleanser.smart_match_data_sets for '
                               + cast(@a_from_data_set_fk as varchar(20)) + ' - ' + cast(@a_to_data_set_fk as varchar(20)) +
                               ' direct matching have finished';
            Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
     
            


            /**********************************************************************************************************/
             -- NEW STEP TO COPY DATA FROM THE WORK TABLE INTO THE PERMANENT ONE ADDED BY YURI NOGIN ON 8/5/2007
            /**********************************************************************************************************/
--             DELETE FROM epacube.cleanser.SEARCH_RESULTS_TASK - THESE TWO LINES REMARKED OUT BY GARY STONE
--             WHERE SEARCH_ACTIVITY_FK = @search_activity_fk; - THESE TWO LINES REMARKED OUT BY GARY STONE
    
--             INSERT INTO epacube.cleanser.SEARCH_RESULTS_TASK
--             (SEARCH_ACTIVITY_FK, SEARCH_TASK_FK, TASK_SEQ, FROM_PRODUCT_FK, FROM_SEARCH_STRING_FK, MATCH_MFR, CONFIDENCE, TO_PRODUCT_FK, TO_SEARCH_STRING_FK, UPDATE_TIMESTAMP)
--          
--             SELECT SEARCH_ACTIVITY_FK, SEARCH_TASK_FK, TASK_SEQ, FROM_PRODUCT_FK, FROM_SEARCH_STRING_FK, MATCH_MFR, CONFIDENCE, TO_PRODUCT_FK, TO_SEARCH_STRING_FK, UPDATE_TIMESTAMP
--             FROM epacube.cleanser.SEARCH_RESULTS_TASK_WORK
--             WHERE SEARCH_ACTIVITY_FK = @search_activity_fk and   REVERSE_FROM_SEARCH_STRING_FK is null and CONFIDENCE >= @IN_DIRECT_MATCHING_THRESHOLD
--             UNION
--             SELECT SEARCH_ACTIVITY_FK, SEARCH_TASK_FK, TASK_SEQ, FROM_PRODUCT_FK, FROM_SEARCH_STRING_FK, MATCH_MFR, CONFIDENCE, TO_PRODUCT_FK, TO_SEARCH_STRING_FK, UPDATE_TIMESTAMP
--             FROM epacube.cleanser.SEARCH_RESULTS_TASK_WORK
--             WHERE SEARCH_ACTIVITY_FK = @search_activity_fk and   REVERSE_FROM_SEARCH_STRING_FK >= 0 and CONFIDENCE >= @IN_REVERSE_MATCHING_THRESHOLD

--	Grouping to code added by GHS on 8/6/2007

             INSERT INTO epacube.cleanser.SEARCH_RESULTS_TASK
             (SEARCH_ACTIVITY_FK, SEARCH_TASK_FK, TASK_SEQ, FROM_PRODUCT_FK, FROM_SEARCH_STRING_FK, MATCH_MFR, CONFIDENCE, TO_PRODUCT_FK, TO_SEARCH_STRING_FK, REVERSE_FROM_SEARCH_STRING_FK, UPDATE_TIMESTAMP)

			 Select DS1.SEARCH_ACTIVITY_FK, DS1.SEARCH_TASK_FK, DS1.TASK_SEQ, DS1.FROM_PRODUCT_FK, DS1.FROM_SEARCH_STRING_FK, DS1.MATCH_MFR, DS1.CONFIDENCE, DS1.TO_PRODUCT_FK, DS1.TO_SEARCH_STRING_FK, MIN(DS1.REVERSE_FROM_SEARCH_STRING_FK), MIN(DS1.UPDATE_TIMESTAMP)
             FROM (
			 SELECT SEARCH_ACTIVITY_FK, SEARCH_TASK_FK, TASK_SEQ, FROM_PRODUCT_FK, FROM_SEARCH_STRING_FK, MATCH_MFR, CONFIDENCE, TO_PRODUCT_FK, TO_SEARCH_STRING_FK, REVERSE_FROM_SEARCH_STRING_FK, UPDATE_TIMESTAMP
             FROM epacube.cleanser.SEARCH_RESULTS_TASK_WORK
             WHERE SEARCH_ACTIVITY_FK = @search_activity_fk and   REVERSE_FROM_SEARCH_STRING_FK is null and CONFIDENCE >= @IN_DIRECT_MATCHING_THRESHOLD
             UNION
             SELECT SEARCH_ACTIVITY_FK, SEARCH_TASK_FK, TASK_SEQ, FROM_PRODUCT_FK, FROM_SEARCH_STRING_FK, MATCH_MFR, CONFIDENCE, TO_PRODUCT_FK, TO_SEARCH_STRING_FK, REVERSE_FROM_SEARCH_STRING_FK, UPDATE_TIMESTAMP
             FROM epacube.cleanser.SEARCH_RESULTS_TASK_WORK
             WHERE SEARCH_ACTIVITY_FK = @search_activity_fk and   REVERSE_FROM_SEARCH_STRING_FK >= 0 and CONFIDENCE >= @IN_REVERSE_MATCHING_THRESHOLD             
			 ) DS1
			 GROUP BY DS1.SEARCH_ACTIVITY_FK, DS1.SEARCH_TASK_FK, DS1.TASK_SEQ, DS1.FROM_PRODUCT_FK, DS1.FROM_SEARCH_STRING_FK, DS1.MATCH_MFR, DS1.CONFIDENCE, DS1.TO_PRODUCT_FK, DS1.TO_SEARCH_STRING_FK

            /**********************************************************************************************************/
             -- END OF NEW MODIFICATIONS BY YURI NOGIN ON 8/5/2007
            /**********************************************************************************************************/
      END; -- End Full Text Matching Process 

      
      SET @status_desc = 'finished load of cleanser.SEARCH_RESULTS_TASK table'
                         + ' From Data Set:' + cast(@a_from_data_set_fk as varchar(20))
                         + ' To Data Set:' + cast(@a_to_data_set_fk as varchar(20)); 
      Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
      --Exec exec_monitor.print_status_sp @exec_id = @l_exec_no;

   END TRY
   BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
   END;



