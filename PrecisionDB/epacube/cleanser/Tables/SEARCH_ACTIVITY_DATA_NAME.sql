﻿CREATE TABLE [cleanser].[SEARCH_ACTIVITY_DATA_NAME] (
    [SEARCH_ACTIVITY_DATA_NAME_ID] INT IDENTITY (1000, 1) NOT NULL,
    [SEARCH_ACTIVITY_FK]           INT NOT NULL,
    [DATA_NAME_FK]                 INT NOT NULL,
    [DATA_NAME_SEQ]                INT NOT NULL,
    CONSTRAINT [PK_SDDN] PRIMARY KEY CLUSTERED ([SEARCH_ACTIVITY_DATA_NAME_ID] ASC),
    CONSTRAINT [FK_SACT_SACTF_SDDN] FOREIGN KEY ([SEARCH_ACTIVITY_FK]) REFERENCES [cleanser].[SEARCH_ACTIVITY] ([SEARCH_ACTIVITY_ID]),
    CONSTRAINT [FK_SADN_DNF_DN] FOREIGN KEY ([DATA_NAME_FK]) REFERENCES [epacube].[DATA_NAME] ([DATA_NAME_ID])
);

