﻿CREATE TABLE [cleanser].[SEARCH_ACTIVITY] (
    [SEARCH_ACTIVITY_ID]         INT            IDENTITY (1000, 1) NOT NULL,
    [NAME]                       VARCHAR (32)   NOT NULL,
    [SEARCH_FROM]                VARCHAR (8)    NULL,
    [SEARCH_TO]                  VARCHAR (8)    NULL,
    [FROM_DATA_SOURCE_NAME]      VARCHAR (32)   NULL,
    [FROM_JOB_FK]                BIGINT         NULL,
    [FROM_FILTER_FK]             INT            NULL,
    [TO_DATA_SOURCE_NAME]        VARCHAR (32)   NULL,
    [TO_JOB_FK]                  BIGINT         NULL,
    [TO_FILTER_FK]               INT            NULL,
    [THREAD_COUNT]               INT            CONSTRAINT [DF_SEARCH_ACTIVITY_THREAD_COUNT] DEFAULT ((1)) NULL,
    [TOP_N_ROWS]                 INT            NULL,
    [IDENT_ONLY_SEARCH_IND]      SMALLINT       NULL,
    [ALLOW_MULT_MATCH_IND]       SMALLINT       NULL,
    [DATA_NAME_ASSOC_FK]         INT            NULL,
    [DNA_CONFIDENCE_WEIGHT]      NUMERIC (7, 3) NULL,
    [ID_CONFIDENCE_WEIGHT]       NUMERIC (7, 3) NULL,
    [SEARCH1_CONFIDENCE_WEIGHT]  NUMERIC (7, 3) NULL,
    [SEARCH2_CONFINDENCE_WEIGHT] NUMERIC (7, 3) NULL,
    [SEARCH3_CONFIDENCE_WEIGHT]  NUMERIC (7, 3) NULL,
    CONSTRAINT [PK_SACT] PRIMARY KEY CLUSTERED ([SEARCH_ACTIVITY_ID] ASC)
);

