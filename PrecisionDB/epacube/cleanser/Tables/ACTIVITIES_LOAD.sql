﻿CREATE TABLE [cleanser].[ACTIVITIES_LOAD] (
    [Search_Activity_ID]           INT           IDENTITY (1000, 1) NOT NULL,
    [Activity_Name]                VARCHAR (32)  NULL,
    [Activity_Type]                VARCHAR (16)  NULL,
    [Activity_Pass]                INT           NULL,
    [Activity_Record_Count_Parent] INT           NULL,
    [Activity_Record_Count_Child]  INT           NULL,
    [Activity_Status]              VARCHAR (32)  NULL,
    [Parent_Job_FK]                BIGINT        NULL,
    [Child_Job_FK]                 BIGINT        NULL,
    [Child_Filter]                 VARCHAR (MAX) NULL,
    [Total_Text_Matches]           INT           NULL,
    [Data_Source_Parent]           VARCHAR (32)  NULL,
    [Data_Source_Child]            VARCHAR (32)  NULL,
    [StartTime]                    DATETIME      NULL,
    [EndTime]                      DATETIME      NULL,
    [Duration_Processing]          VARCHAR (32)  NULL,
    [Import_Package_FK]            BIGINT        NULL,
    [Activity_Subset]              VARCHAR (MAX) NULL,
    CONSTRAINT [PK_ACTIVITIES_LOAD] PRIMARY KEY CLUSTERED ([Search_Activity_ID] ASC)
);

