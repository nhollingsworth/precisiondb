﻿CREATE TABLE [cleanser].[SEARCH_JOB] (
    [SEARCH_JOB_ID]              BIGINT         NOT NULL,
    [SEARCH_ACTIVITY_FK]         BIGINT         NULL,
    [NAME]                       VARCHAR (32)   NOT NULL,
    [IMPORT_BATCH_NO]            BIGINT         NULL,
    [ENTITY_CLASS_CR_FK]         INT            NULL,
    [FROM_DATA_SOURCE_NAME]      VARCHAR (32)   NULL,
    [FROM_JOB_FK]                BIGINT         NULL,
    [FROM_FILTER_FK]             INT            NULL,
    [TO_DATA_SOURCE_NAME]        VARCHAR (32)   NULL,
    [TO_JOB_FK]                  BIGINT         NULL,
    [TO_FILTER_FK]               INT            NULL,
    [THREAD_COUNT]               INT            CONSTRAINT [DF_SEARCH_ACT_JOB_THREAD_COUNT] DEFAULT ((1)) NULL,
    [TOP_N_ROWS]                 INT            NULL,
    [ASSOC_DATA_NAME_FK]         INT            NULL,
    [DNA_CONFIDENCE_WEIGHT]      NUMERIC (7, 3) NULL,
    [ID_CONFIDENCE_WEIGHT]       NUMERIC (7, 3) NULL,
    [SEARCH1_CONFIDENCE_WEIGHT]  NUMERIC (7, 3) NULL,
    [SEARCH2_CONFINDENCE_WEIGHT] NUMERIC (7, 3) NULL,
    [SEARCH3_CONFIDENCE_WEIGHT]  NUMERIC (7, 3) NULL,
    [CREATE_TIMESTAMP]           DATETIME       CONSTRAINT [DF_SEARCH_JOB_CREATE_TIMESTAMP] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SACT_JOB] PRIMARY KEY CLUSTERED ([SEARCH_JOB_ID] ASC)
);

