﻿CREATE TABLE [cleanser].[SEARCH_JOB_RESULTS_TASK_WORK] (
    [SEARCH_JOB_RESULTS_TASK_ID]    BIGINT         IDENTITY (1, 1) NOT NULL,
    [SEARCH_JOB_FK]                 BIGINT         NULL,
    [SEARCH_ACTIVITY_FK]            INT            NOT NULL,
    [ENTITY_CLASS_CR_FK]            INT            NULL,
    [ENTITY_DATA_NAME_FK]           INT            NULL,
    [SEARCH_TASK_FK]                INT            NOT NULL,
    [TASK_SEQ]                      INT            NULL,
    [FROM_SEARCH_FK]                BIGINT         NOT NULL,
    [FROM_SEARCH_STRING_FK]         BIGINT         NOT NULL,
    [MATCH_DNA]                     SMALLINT       NULL,
    [CONFIDENCE]                    NUMERIC (7, 3) NULL,
    [TO_SEARCH_FK]                  BIGINT         NOT NULL,
    [TO_SEARCH_STRING_FK]           BIGINT         NOT NULL,
    [REVERSE_FROM_SEARCH_STRING_FK] BIGINT         NULL,
    [UPDATE_TIMESTAMP]              DATETIME       CONSTRAINT [DF_SJRTW_UPDATE_TIMESTAMP] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SJRTW] PRIMARY KEY CLUSTERED ([SEARCH_JOB_RESULTS_TASK_ID] ASC)
);

