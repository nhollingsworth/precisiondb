﻿
CREATE VIEW [cleanser].[V_SEARCH_BEST_ES]
AS
SELECT
        A.SEARCH_JOB_RESULTS_BEST_ID,
        A.SEARCH_JOB_FK, 
	    A.FROM_SEARCH_FK, 
	    A.BEST_SEARCH_FK,  
	    A.STG_RECORD_ID,      
	    A.CLEANSED_INDICATOR,  
	    A.EVENT_CONDITION_CR_FK,	           
	    A.ACTIVITY_ACTION_CR_FK, 
	    A.ACTIVITY_ACTION, 
        A.PRODUCT_STATUS_CR_FK,
	    A.PRODUCT_STATUS, 
	    A.FROM_PRODUCT, 	    
        A.FROM_MATCHES, 
        A.CHILD_ITEMS,   
	    A.PARENT_FROM_SEARCH_FK,         
        A.BEST_PRODUCT,
	    A.BEST_CONFIDENCE, 
        A.BEST_MATCHES, 
	    A.IMPORT_JOB, 
        A.TASKID,            
	    A.UPDATE_TIMESTAMP, 
	    A.SEARCH_ID_CONFIDENCE, 
	    A.SEARCH1_CONFIDENCE, 
	    A.SEARCH2_CONFIDENCE, 
	    A.SEARCH3_CONFIDENCE,
		A.ERROR
FROM (
SELECT   SRB.SEARCH_JOB_RESULTS_BEST_ID
        ,SRB.SEARCH_JOB_FK 
        ,SRB.FROM_SEARCH_FK
        ,SRB.BEST_SEARCH_FK
        ,SRB.BEST_SEARCH_FK AS STG_RECORD_ID         
        ,SRB.CLEANSED_INDICATOR 
        ,SREC.EVENT_CONDITION_CR_FK
        ,SRB.ACTIVITY_ACTION_CR_FK          
	    ,(SELECT CODE FROM epacube.CODE_REF 
          WHERE  CODE_REF_ID = SRB.ACTIVITY_ACTION_CR_FK ) AS ACTIVITY_ACTION
        , CASE ISNULL(PS.PRODUCT_STATUS_CR_FK , 0) 
				WHEN 0 
				THEN 104 
				ELSE PS.PRODUCT_STATUS_CR_FK END AS PRODUCT_STATUS_CR_FK      
		,CASE ISNULL((SELECT CODE FROM epacube.CODE_REF
						WHERE CODE_REF_ID = PS.PRODUCT_STATUS_CR_FK ), 'NULL')
				WHEN 'NULL'
				THEN 'SETUP'
				ELSE (SELECT CODE FROM epacube.CODE_REF
						 WHERE CODE_REF_ID = PS.PRODUCT_STATUS_CR_FK )
				END  AS product_status
        , (SELECT PI.VALUE FROM epacube.PRODUCT_IDENTIFICATION AS PI 
              INNER JOIN epacube.EPACUBE_PARAMS AS EEP ON (EEP.application_scope_fk = 100
                                                       AND PI.DATA_NAME_FK = CAST(EEP.value AS INT) AND EEP.name = 'PRODUCT' )
              WHERE PI.PRODUCT_STRUCTURE_FK = SRB.from_search_fk ) AS FROM_PRODUCT
        ,(SELECT COUNT(1) FROM cleanser.search_job_from_to_match AS SJFTM
          WHERE  SJFTM.SEARCH_JOB_FK = SRB.SEARCH_JOB_FK 
          AND    SJFTM.from_search_fk = SRB.FROM_SEARCH_FK ) AS FROM_MATCHES
        ,(SELECT COUNT(1) FROM cleanser.SEARCH_JOB_RESULTS_BEST AS srbm
          WHERE  SRBM.SEARCH_JOB_FK = SRB.SEARCH_JOB_FK 
          AND    SRBM.PARENT_from_search_fk = SRB.from_search_fk ) AS CHILD_ITEMS  
        ,SRB.PARENT_FROM_SEARCH_FK        
--------        ,SREC.ENTITY_ID_VALUE AS BEST_PRODUCT
        ,isnull(isnull((select top 1 DATA_VALUE from stage.stg_record_data 

                                          where data_name_fk=(select top 1 data_name_fk from cleanser.search_job_data_name sjdn                                                                    

                                                                        where sjdn.search_job_fk = srb.search_job_fk

                                                                        order by data_name_seq) and stg_record_fk = srb.best_search_fk ),                                   

                                          (SELECT top 1 SPD.DATA_VALUE FROM stage.STG_RECORD_DATA 

                                          AS SPD INNER JOIN epacube.EPACUBE_PARAMS AS EEP ON SPD.DATA_NAME_FK = 

                                          CAST(EEP.value AS INT) AND EEP.name = 'PRODUCT' AND 

                                          EEP.application_scope_fk = 100 where spd.stg_record_fk = srb.best_search_fk )),                           

                        isnull((select top 1 DATA_VALUE from stage.stg_record_data where data_name_fk=(select CAST(EEP.value AS INT) from epacube.epacube_params eep where EEP.name = 'DESCRIPTION' AND 

                                          EEP.application_scope_fk = 100) and stg_record_fk=srb.BEST_SEARCH_FK ),

                                 (SELECT TOP 1 SPD.DATA_VALUE FROM stage.STG_RECORD_DATA AS SPD 

                                    INNER JOIN epacube.EPACUBE_PARAMS AS EEP ON SPD.DATA_NAME_FK = CAST(EEP.value AS INT) AND EEP.name = 'PRODUCT' AND 

                            EEP.application_scope_fk = 100

                            WHERE      (SPD.stg_record_fk = SRB.BEST_SEARCH_FK)))
          ) AS BEST_PRODUCT
        ,SRB.BEST_CONFIDENCE      
		,(select top 1 error_name
		  from stage.stg_record_errors srece
		  where stg_record_fk = srec.stg_record_id and srece.job_fk = srec.job_fk
		  order by event_action_cr_fk desc) ERROR 
        ,(SELECT COUNT(1) FROM cleanser.search_job_from_to_match AS SJFTM
          WHERE  SJFTM.SEARCH_JOB_FK = SRB.SEARCH_JOB_FK 
          AND    SJFTM.to_search_fk = SRB.BEST_SEARCH_FK ) AS BEST_MATCHES
        ,(SELECT TO_JOB_FK FROM cleanser.SEARCH_JOB AS SJ
          WHERE  SJ.SEARCH_JOB_ID = SRB.SEARCH_JOB_FK) AS IMPORT_JOB       
       ,(select top(1) search_task_id from cleanser.search_task with (NOLOCK) 
         where search_job_fk = srb.search_job_fk order by task_seq) TASKID                                      
        ,SRB.UPDATE_TIMESTAMP, SRB.SEARCH_ID_CONFIDENCE, SRB.SEARCH1_CONFIDENCE
        ,SRB.SEARCH2_CONFIDENCE, SRB.SEARCH3_CONFIDENCE
FROM cleanser.SEARCH_JOB_RESULTS_BEST AS SRB WITH (NOLOCK) 
LEFT JOIN epacube.PRODUCT_STRUCTURE AS PS WITH (NOLOCK) ON ( SRB.from_search_fk = PS.PRODUCT_STRUCTURE_ID )
LEFT JOIN STAGE.STG_RECORD AS SREC WITH (NOLOCK) ON ( SREC.STG_RECORD_ID = SRB.BEST_SEARCH_FK )
) A

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[13] 4[14] 2[54] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = 0
      End
      Begin Tables = 
         Begin Table = "SRB"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PS"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 241
               Right = 275
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sps"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 361
               Right = 274
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'cleanser', @level1type = N'VIEW', @level1name = N'V_SEARCH_BEST_ES';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'cleanser', @level1type = N'VIEW', @level1name = N'V_SEARCH_BEST_ES';

