﻿ 

--A_MAUI_CRT_ASSIGNMENTS_BY_CUSTOMER

-- =============================================
-- Author:		<Gary Stone>
-- Create date: <September 28, 2016>
-- =============================================
CREATE PROCEDURE [Special].[CRT_ASSIGNMENTS_BY_CUSTOMER]

AS
BEGIN

	SET NOCOUNT ON;

----*** Include this code with all Tables loading into Special schema

	Declare @Job_FK varchar(16)

	If (select count(*) from INFORMATION_SCHEMA.TABLES where table_schema = 'special' and TABLE_NAME = 'Customer_Rebate_Type_Assignments') = 0
		Set @Job_FK = isnull((Select MAX(JOB_FK) from cpl.aCPL_Profiles where intType = 10), 0) + 1
	Else 
		Set @Job_FK = (Select top 1 Job_FK from Special.epaCUBE_Rules_Complete)

----*** -------------------------------------------------------------

If (Select COUNT(*) from cpl.aCPL_Profiles where intType = 10 and strProfile_Name = 'Customer_Rebate_Type_Assignments') = 0
Insert into cpl.aCPL_Profiles
(Job_FK, strProfile_Name, Create_User, Pricing_Effective_Date, intType)
Select @Job_FK, 'Customer_Rebate_Type_Assignments', 'System_Generated', CAST(getdate() as DATE), 10

Update CPL
Set Pricing_Effective_Date = CAST(getdate() as DATE)
from cpl.aCPL_Profiles CPL where intType = 10 and strProfile_Name = 'Customer_Rebate_Type_Assignments'

If OBJECT_ID('Special.Customer_Rebate_Type_Assignments') is not null
drop table Special.Customer_Rebate_Type_Assignments

Select 
Region '1 Region', [Region Name] '2 Region Name', Whse '3 Whse', [Whse Name] '4 Whse Name', [Bill-To ID] '5 Bill-To ID', [Ship-To ID] '6 Ship-To ID'
, [Customer Rebate Type] '7 Customer Rebate Type'
, GETDATE() '8 Create_Date'
, @Job_FK '9 Job_FK' 
into Special.Customer_Rebate_Type_Assignments
from (
Select eir.value 'Region'
, einr.VALUE 'Region Name'
, eio.value 'Whse'
, eino.VALUE 'Whse Name'
, eic.VALUE Customer_ID
, Case when eic.VALUE like '%-%' then Left(eic.VALUE, CHARINDEX('-', eic.VALUE) -1) else eic.VALUE end 'Bill-To ID'
, Case when eic.VALUE like '%-%' then right(eic.VALUE, Len(eic.VALUE) - CHARINDEX('-', eic.VALUE)) else Null end 'Ship-To ID'
, dv.VALUE 'Customer Rebate Type'
from epacube.epacube.ENTITY_IDENTIFICATION eir with (nolock)
left join epacube.epacube.ENTITY_IDENT_NONUNIQUE einr with (nolock) on eir.ENTITY_STRUCTURE_FK = einr.ENTITY_STRUCTURE_FK and einr.DATA_NAME_FK = 141112
inner join epacube.epacube.ENTITY_STRUCTURE es with (nolock) on es.PARENT_ENTITY_STRUCTURE_FK = eir.ENTITY_STRUCTURE_FK
inner join epacube.epacube.ENTITY_IDENTIFICATION eio with (nolock) on es.ENTITY_STRUCTURE_ID = eio.entity_structure_fk
left join epacube.epacube.ENTITY_IDENT_NONUNIQUE eino with (nolock) on eio.ENTITY_STRUCTURE_FK = eino.ENTITY_STRUCTURE_FK and eino.DATA_NAME_FK = 141112
left join epacube.epacube.ENTITY_ATTRIBUTE ea with (nolock) on eio.VALUE = ea.ATTRIBUTE_EVENT_DATA and ea.DATA_NAME_FK = 144900
left join epacube.epacube.ENTITY_IDENTIFICATION eic with (nolock) on ea.ENTITY_STRUCTURE_FK = eic.ENTITY_STRUCTURE_FK
left join epacube.epacube.entity_mult_type emt with (nolock) on eic.ENTITY_STRUCTURE_FK = emt.ENTITY_STRUCTURE_FK and emt.DATA_NAME_FK = 244901
left join epacube.epacube.DATA_VALUE dv with (nolock) on emt.DATA_VALUE_FK = dv.DATA_VALUE_ID
where eir.ENTITY_DATA_NAME_FK = 141030
) A
where [Bill-to ID] is not null
Order by Region, Whse, Customer_ID, [Customer Rebate Type]

Create index idx_cpts on Special.Customer_Rebate_Type_Assignments([1 Region], [3 Whse], [5 Bill-to ID], [6 Ship-to ID], [7 Customer Rebate Type])
END
