﻿--A_MAUI_GROUP_ASSIGNMENTS_BY_CUSTOMER


-- =============================================
-- Author:		Gary Stone
-- Create date: April 12, 2016
-- Description:	Listing of GROUP ASSIGNMENTS BY CUSTOMER

-- =============================================
CREATE PROCEDURE [Special].[GROUP_ASSIGNMENTS_BY_CUSTOMER]

AS
BEGIN

	SET NOCOUNT ON;

----*** Include this code with all Tables loading into Special schema

	Declare @Job_FK varchar(16)

	If (select count(*) from INFORMATION_SCHEMA.TABLES where table_schema = 'special' and TABLE_NAME = 'Customer_Group_Assignments') = 0
		Set @Job_FK = isnull((Select MAX(JOB_FK) from cpl.aCPL_Profiles where intType = 10), 0) + 1
	Else 
		Set @Job_FK = (Select top 1 Job_FK from Special.Customer_Group_Assignments)

----*** -------------------------------------------------------------

	If (Select COUNT(*) from cpl.aCPL_Profiles where intType = 10 and strProfile_Name = 'Customer_Group_Assignments') = 0
	Insert into cpl.aCPL_Profiles
	(Job_FK, strProfile_Name, Create_User, Pricing_Effective_Date, intType)
	Select @Job_FK, 'Customer_Group_Assignments', 'System_Generated', CAST(getdate() as DATE), 10

	Update CPL
	Set Pricing_Effective_Date = CAST(getdate() as DATE)
	from cpl.aCPL_Profiles CPL where intType = 10 and strProfile_Name = 'Customer_Group_Assignments'

	If OBJECT_ID('Special.Customer_Group_Assignments') is not null
	drop table Special.Customer_Group_Assignments

		Create Table Special.Customer_Group_Assignments
		(
		[01 Region] Varchar(128) Null
		, [02 Region Name] Varchar(128) Null
		, [03 Home Branch] Varchar(128) Null
		, [04 Home Branch Name] Varchar(128) Null
		, [05 Bill-To ID] Varchar(128) Null
		, [06 Ship-To ID] Varchar(128) Null
		, [07 Value] Varchar(128) Null
		, [08 Data_Name] Varchar(128) Null
		, [09 Data_Name_ID] Bigint Null
		, [10 Import Date] date Null
		, [Job_FK] bigint Null
		)

		Create index idx_cpts on Special.Customer_Group_Assignments([01 Region], [03 Home Branch], [05 Bill-to ID], [06 Ship-to ID], [07 Value], [08 Data_Name])

		Declare @DN_FK as bigint, @NM as varchar(64)

		DECLARE EMT CURSOR FOR 
		Select distinct data_name_fk from epacube.epacube.entity_mult_type emt inner join epacube.epacube.data_name dn on emt.data_name_fk = dn.data_name_id

		OPEN EMT 

			FETCH NEXT FROM EMT INTO @DN_FK
			WHILE @@FETCH_STATUS = 0 
			BEGIN 
				
			Insert into Special.Customer_Group_Assignments
			
			Select 
			Region, [Region Name], Whse, [Whse Name], [Bill-To ID], [Ship-To ID], Customer_Group_Name, Data_Name, Data_Name_ID, GETDATE(), @Job_FK
			from (
			Select eir.value 'Region'
			, einr.VALUE 'Region Name'
			, eio.value 'Whse'
			, Upper(eino.VALUE) 'Whse Name'
			, eic.VALUE Customer_ID
			, Case when eic.VALUE like '%-%' then Left(eic.VALUE, CHARINDEX('-', eic.VALUE) -1) else eic.VALUE end 'Bill-To ID'
			, Case when eic.VALUE like '%-%' then right(eic.VALUE, Len(eic.VALUE) - CHARINDEX('-', eic.VALUE)) else Null end 'Ship-To ID'
			, Upper(dv.VALUE) 'Customer_Group_Name'
			, Upper(DN.Name) 'Data_Name'
			, DN.Data_Name_ID
			from epacube.epacube.ENTITY_STRUCTURE es with (nolock)
			inner join epacube.epacube.ENTITY_IDENTIFICATION eio with (nolock) on es.ENTITY_STRUCTURE_ID = eio.entity_structure_fk and es.data_name_fk = eio.entity_data_name_fk and es.DATA_NAME_FK = 141000 and eio.data_name_fk = 141111
			left join epacube.epacube.ENTITY_IDENTIFICATION eir with (nolock) on es.PARENT_ENTITY_STRUCTURE_FK = eir.ENTITY_STRUCTURE_FK and eir.ENTITY_DATA_NAME_FK = 141030 and es.data_name_fk = 141000
			left join epacube.epacube.ENTITY_IDENT_NONUNIQUE einr with (nolock) on eir.ENTITY_STRUCTURE_FK = einr.ENTITY_STRUCTURE_FK and einr.DATA_NAME_FK = 141112
			left join epacube.epacube.ENTITY_IDENT_NONUNIQUE eino with (nolock) on eio.ENTITY_STRUCTURE_FK = eino.ENTITY_STRUCTURE_FK and eino.DATA_NAME_FK = 141112
			left join epacube.epacube.ENTITY_ATTRIBUTE ea with (nolock) on eio.VALUE = ea.ATTRIBUTE_EVENT_DATA and ea.DATA_NAME_FK = 144900
			left join epacube.epacube.ENTITY_IDENTIFICATION eic with (nolock) on ea.ENTITY_STRUCTURE_FK = eic.ENTITY_STRUCTURE_FK
			left join epacube.epacube.entity_mult_type emt with (nolock) on eic.ENTITY_STRUCTURE_FK = emt.ENTITY_STRUCTURE_FK and emt.DATA_NAME_FK = @DN_FK
			left join epacube.epacube.DATA_VALUE dv with (nolock) on emt.DATA_VALUE_FK = dv.DATA_VALUE_ID
			left join epacube.epacube.data_name dn with (nolock) on emt.data_name_fk = dn.data_name_id
			where dv.VALUE is not null
			) A
			Order by Region, Whse, Customer_ID, Customer_Group_Name

			FETCH NEXT FROM EMT INTO @DN_FK
			END 

		CLOSE EMT 
		DEALLOCATE EMT

END
