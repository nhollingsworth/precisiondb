﻿CREATE TABLE [common].[db_sequence_currvals] (
    [spid]          INT           NOT NULL,
    [schema_name]   NVARCHAR (30) NOT NULL,
    [sequence_name] NVARCHAR (30) NOT NULL,
    [currval]       NUMERIC (38)  NOT NULL
);

