﻿CREATE TABLE [common].[TESTING_LOGS] (
    [TESTING_LOGS_ID]          BIGINT          IDENTITY (1000, 1) NOT NULL,
    [SCENARIO_NAME]            VARCHAR (64)    NULL,
    [JOB_FK]                   BIGINT          NULL,
    [BATCH_NO]                 BIGINT          NULL,
    [EVENT_FK]                 BIGINT          NULL,
    [RULES_FK]                 BIGINT          NULL,
    [DATA_NAME_FK]             INT             NULL,
    [EVENT_CONDITION_CR_FK]    INT             NULL,
    [SCENARIO_DESCRIPTION]     VARCHAR (256)   NULL,
    [ACTUAL_NUMBER_OR_COUNT]   NUMERIC (18, 6) NULL,
    [ACTUAL_DATA]              VARCHAR (256)   NULL,
    [EXPECTED_RESULT]          VARCHAR (256)   NULL,
    [EXPECTED_NUMBER_OR_COUNT] NUMERIC (18, 6) NULL,
    [EXPECTED_DATA]            VARCHAR (256)   NULL,
    [DATA1]                    VARCHAR (64)    NULL,
    [DATA2]                    VARCHAR (64)    NULL,
    [DATA3]                    VARCHAR (64)    NULL,
    [UPDATE_TIMESTAMP]         DATETIME        NULL,
    [UPDATE_LOG_FK]            BIGINT          NULL,
    [LOG_TIMESTAMP]            DATETIME        CONSTRAINT [DF_TLOG_UPDAT__546C6DB3] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_TESTING_LOGS] PRIMARY KEY CLUSTERED ([TESTING_LOGS_ID] ASC)
);

