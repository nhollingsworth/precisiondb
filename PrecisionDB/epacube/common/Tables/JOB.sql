﻿CREATE TABLE [common].[JOB] (
    [JOB_ID]                 BIGINT         NOT NULL,
    [JOB_CLASS_FK]           INT            NULL,
    [NAME]                   VARCHAR (64)   NULL,
    [DATA1]                  VARCHAR (256)  NULL,
    [DATA2]                  VARCHAR (256)  NULL,
    [DATA3]                  VARCHAR (256)  NULL,
    [DATA4]                  VARCHAR (256)  NULL,
    [DATA5]                  VARCHAR (256)  NULL,
    [CONFIG_FK]              BIGINT         NULL,
    [FILTER_FK]              INT            NULL,
    [IMPORT_PACKAGE_FK]      BIGINT         NULL,
    [BINARY_JOB_ID]          BINARY (50)    NULL,
    [EXEC_STMT]              NVARCHAR (MAX) NULL,
    [JOB_CREATE_TIMESTAMP]   DATETIME       CONSTRAINT [DF_JOB_JOB_TIMESTAMP] DEFAULT (getdate()) NULL,
    [JOB_COMPLETE_TIMESTAMP] DATETIME       NULL,
    [JOB_USER]               VARCHAR (64)   NULL,
    CONSTRAINT [PK_JOB] PRIMARY KEY CLUSTERED ([JOB_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [J_20100602_PURGE_IMPORT_DATA]
    ON [common].[JOB]([JOB_ID] ASC, [JOB_COMPLETE_TIMESTAMP] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

