﻿CREATE TABLE [common].[T_SQL] (
    [TS]       DATETIME       CONSTRAINT [DF_COMMON.T_SQL_TS] DEFAULT (getdate()) NULL,
    [SQL_TEXT] NVARCHAR (MAX) NULL
);

