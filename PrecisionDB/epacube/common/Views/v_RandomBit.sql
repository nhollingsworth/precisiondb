﻿CREATE VIEW common.v_RandomBit
AS
SELECT CASE WHEN CONVERT(CHAR(1), CONVERT(VARCHAR(36), NewId())) > '7' 
            THEN 1 
            ELSE 0 
            END AS RandomBit

