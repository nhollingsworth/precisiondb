﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 02/09/2010
-- Description:	Rounding Method Round UP
--
-- This method always rounds the number up to the next highest number. 
-- For example, 3.1 rounded up is 4 and -3.1 rounded up is -3. 
-- In SQL Server, the easiest way to achieve ‘RoundUp’ is to use the ceiling function. 
-- Unfortunately, the Ceiling function only works with whole numbers. 
-- There’s no built-in function that RoundsUp where you can specify the number of digits. 
-- This is no problem, we can just build our own.
--
-- =============================================
CREATE FUNCTION [common].[RoundUp](@Val DECIMAL(18,6), @Digits INT)
RETURNS DECIMAL(18,6)
AS
BEGIN
    RETURN CASE WHEN ABS(@Val - ROUND(@Val, @Digits, 1)) * POWER(10, @Digits+1) = 5 
                THEN CEILING(@Val * POWER(10,@Digits))/POWER(10, @Digits)
                ELSE ROUND(@Val, @Digits)
                END
END


