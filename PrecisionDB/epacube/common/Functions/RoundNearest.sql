﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 02/09/2010
-- Description:	Rounding Method Round Down
--
-- This method uses the SQL ROUND function for rounding up
--	Note:  The 0 parameter is optional but when specified will do nearest rounding
--	       The 1 in the last parameter would truncate the result a simplified round down 
--
-- =============================================
CREATE FUNCTION [common].[RoundNearest](@Val DECIMAL(18,6), @Digits INT)
RETURNS DECIMAL(18,6)
AS
BEGIN
    RETURN ( SELECT ROUND ( @Val, @Digits, 0 ) ) -- NEAREST  NOTE: ,1 would truncate with round 

END



