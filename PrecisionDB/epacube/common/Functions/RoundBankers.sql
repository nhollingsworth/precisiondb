﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 02/09/2010
-- Description:	Rounding Method Bankers Rounding
--
-- This is known by many different names 
-- (Bankers Rounding, unbiased rounding, Gaussian rounding, and statisticians rounding). 
-- With this method, special exceptions are made if the last digit to remove is exactly 5. 
-- If it is, the number is rounded to the next even number.
-- Example, 4.15 Bankers Rounded to 1 digits is 4.2;  4.25 Bankers Rounded to 1 digits is 4.2
--
-- This occurs because everything to the right of the number of digits (5) is exactly 5. 
-- Had it been 51, the number would have been rounded up. 
-- Less than 50, and the number is rounded down. 
-- Since it is exactly 50, we need to examine the digit to the left of it (the 1st decimal place). 
-- If it’s an even number, the rounded value is truncated. 
-- If it’s an odd number (like the 1 in 4.15), then it’s rounded to the next even number (to become 4.2). 

--
-- =============================================
CREATE FUNCTION [common].[RoundBankers](@Val DECIMAL(18,6), @Digits INT)
RETURNS DECIMAL(18,6)
AS
BEGIN
    RETURN CASE WHEN ABS(@Val - ROUND(@Val, @Digits, 1)) * POWER(10, @Digits+1) = 5 
                THEN ROUND(@Val, @Digits, CASE WHEN CONVERT(INT, ROUND(ABS(@Val) * POWER(10,@Digits), 0, 1)) % 2 = 1 THEN 0 ELSE 1 END)
                ELSE ROUND(@Val, @Digits)
                END
END




