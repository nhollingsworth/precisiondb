﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 02/09/2010
-- Description:	Rounding Method Round Down
--
-- This method is similar to rounding up, except it is rounded to the nearest smaller number. 
-- For example, 3.1 rounded down is 3, -3.1 rounded down is -4. 
-- SQL Server has a floor function that rounds down for up. 
-- Just like the ceiling function, the floor function does not accommodate the number of digits (it only works on whole numbers). 
-- Another function to the rescue.
--
-- =============================================
CREATE FUNCTION [common].[RoundDown](@Val DECIMAL(18,6), @Digits INT)
RETURNS DECIMAL(18,6)
AS
BEGIN
    RETURN CASE WHEN ABS(@Val - ROUND(@Val, @Digits, 1)) * POWER(10, @Digits+1) = 5 
                THEN FLOOR(@Val * POWER(10,@Digits))/POWER(10, @Digits)
                ELSE ROUND(@Val, @Digits)
                END
END



