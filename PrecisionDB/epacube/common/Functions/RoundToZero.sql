﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 02/09/2010
-- Description:	Rounding Method Round To Zero
--
-- In this method, 3.5 rounds down to 3, but -3.5 rounds up to -3. 
-- Implementing Round Towards Zero is relatively simple in SQL Server too. 
-- There is an optional 3rd parameter to the Round function that simply truncates the number to the number of decimal places. 
-- Unfortunately, this does not check for the edge case where the last digit is 5 followed by zeros. 
-- 4.15 should round to 4.1;  4.15000001 should round to 4.2
--
-- Using the 3rd argument of the round function will blindly truncate the trailing numbers, 
-- so both would be rounded to 4.1. This is not what we want, so we’ll need to write another function.
--
-- =============================================
CREATE FUNCTION [common].[RoundToZero](@Val DECIMAL(18,6), @Digits INT)
RETURNS DECIMAL(18,6)
AS
BEGIN
    RETURN CASE WHEN ABS(@Val - ROUND(@Val, @Digits, 1)) * POWER(10, @Digits+1) = 5 
                THEN ROUND(@Val, @Digits, 1)
                ELSE ROUND(@Val, @Digits)
                END
END




