﻿

-- =============================================
-- Author:		Kathi Scott
-- Create date: 02/09/2010
-- Description:	Rounding Method Round Nearest to Value
--
-- Rounding to the nearest penny, nickel, dime, quarter, or dollar 
-- Utilizing the SQL Round with the Round To Value specified ( ie .01, .05, .10, .25 and 1 for dollar )
-- Different function are required for RoundUpToValue or RoundDownToValue
--
-- =============================================
CREATE FUNCTION [common].[RoundNearestToValue](@Val DECIMAL(18,6), @RoundToValue DECIMAL(18,6))
RETURNS DECIMAL(18,6)
AS
BEGIN
    RETURN ( SELECT ROUND ( @Val / @RoundToValue , 0 ) * @RoundToValue )
END





