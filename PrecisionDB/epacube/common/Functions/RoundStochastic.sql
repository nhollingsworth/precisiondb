﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 02/09/2010
-- Description:	Rounding Method Bankers Rounding
--
-- This is similar to 'RoundBankers'. 
-- When the remaining digits is a 5 followed by zeros, we randomly round down or up. 
-- Implementing this is a bit more difficult because SQL Server doesn't like random numbers in user defined function. 
-- To work-around this limitation, we can create a view that returns a random number and call that view from within the function. 
-- First, the view:  common.v_RandomBit is created
--
-- =============================================
CREATE FUNCTION [common].[RoundStochastic](@Val DECIMAL(18,6), @Digits INT)
RETURNS DECIMAL(18,6)
AS
BEGIN
    RETURN CASE WHEN ABS(@Val - ROUND(@Val, @Digits, 1)) * POWER(10, @Digits+1) = 5 
                THEN ROUND(@Val, @Digits, CONVERT(INT, (SELECT  RandomBit FROM vw_RandomBit)))
                ELSE ROUND(@Val, @Digits)
                END
END




