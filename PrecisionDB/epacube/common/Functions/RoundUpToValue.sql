﻿
-- =============================================
-- Author:		Kathi Scott
-- Create date: 02/09/2010
-- Description:	Rounding Method Round Up to Value
--
-- Rounding Down to the penny, nickel, dime, quarter, or dollar 
-- The Round To Value specified ( ie .01, .05, .10, .25 and 1 for dollar )
-- Use the Floor function to separate decimal from dollars
-- Next determine the multiplier of the RoundtoValue that the current Decimal is
-- Add to the Dollar portion the this multiplier times the RoundToValue
--
-- =============================================
CREATE FUNCTION [common].[RoundUpToValue](@Val DECIMAL(18,6), @RoundToValue DECIMAL(18,6))
RETURNS DECIMAL(18,6)
AS
BEGIN
    DECLARE @v_decimals AS DECIMAL(18,6)
    
    SET @v_decimals = ( SELECT @Val - FLOOR(@Val) )
    
    RETURN CASE @v_decimals
           WHEN 0 THEN @Val
           ELSE ( SELECT FLOOR ( @Val ) + ( @RoundToValue * ( ( SELECT  FLOOR ( ( @Val - FLOOR(@Val) ) / @RoundToValue ) ) + 1 ) ) )
           END 
END




