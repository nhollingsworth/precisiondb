﻿



CREATE PROCEDURE [common].[JOB_EXECUTION_CREATE]
      (
        @IN_JOB_FK bigint
      , @IN_JOB_NAME VARCHAR(max) = null
      , @IN_INPUT_ROWS bigint = null
      , @IN_OUTPUT_ROWS bigint = null
      , @IN_EXCEPTION_ROWS bigint = null
      , @IN_JOB_STATUS_CR_FK int = null
      , @IN_START_TIMESTAMP DATETIME --= GETDATE
      , @IN_END_TIMESTAMP DATETIME --= GETDATE
      )
AS --Copyright 2005, epaCUBE, Inc.
--$Header: /sdi/development/db/update/1.06.06.03/proc_sheet_calc_insert_results.sql,v 1.1 2005/05/11 18:43:34 cnoe Exp $
      BEGIN
            DECLARE @l_sysdate DATETIME
                  , @ls_stmt VARCHAR(1000)
                  , @l_exec_no BIGINT
                  , @v_job_exec_fk BIGINT ;

            SET @l_sysdate = getdate() ;


-------------------------------------------------------------------------------------
--   Insert JOB info for NOW into JOB Table... Trigger will set Job_ID if NULL
---   what YURI put in here is not working....
---
-------------------------------------------------------------------------------------

/*/
            SET @v_job_exec_fk = 0 ;
            EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                 'job_exec_seq', @v_job_exec_fk OUTPUT
                 
            WHILE (EXISTS (SELECT 1 FROM COMMON.JOB_EXECUTION WHERE JOB_EXECUTION_ID = @v_job_exec_fk)) 
            BEGIN
            EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                 'job_exec_seq', @v_job_exec_fk OUTPUT
            END     
            --SELECT @v_calc_job_fk = MAX(JOB_EXECUTION_ID) + 1 FROM JOB_EXECUTION 


            INSERT  INTO JOB_EXECUTION
                    (
                      JOB_EXECUTION_ID
                    , JOB_FK
                    , NAME
                    , INPUT_ROWS
                    , OUTPUT_ROWS
                    , EXCEPTION_ROWS
                    , JOB_STATUS_CR_FK
                    , START_TIMESTAMP
                    , END_TIMESTAMP
			   
                    )
            VALUES  (
                      @v_job_exec_fk
                    , @in_job_fk
                    , @in_job_name
                    , @in_input_rows
                    , @in_output_rows
                    , @in_exception_rows
                    , @in_job_status_cr_fk
                    , ISNULL(@in_start_timestamp, @l_sysdate)
                    , ISNULL(@in_end_timestamp, @l_sysdate)
                    ) ;

*/
                 

            INSERT  INTO COMMON.JOB_EXECUTION
                    (
                      JOB_FK
                    , NAME
                    , INPUT_ROWS
                    , OUTPUT_ROWS
                    , EXCEPTION_ROWS
                    , JOB_STATUS_CR_FK
                    , START_TIMESTAMP
                    , END_TIMESTAMP
			   
                    )
            VALUES  (
                      @in_job_fk
                    , @in_job_name
                    , @in_input_rows
                    , @in_output_rows
                    , @in_exception_rows
                    , @in_job_status_cr_fk
                    , ISNULL(@in_start_timestamp, @l_sysdate)
                    , ISNULL(@in_end_timestamp, @l_sysdate)
                    ) ;


      END




