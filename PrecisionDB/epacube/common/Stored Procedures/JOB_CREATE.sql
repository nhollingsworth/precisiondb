﻿

CREATE PROCEDURE [common].[JOB_CREATE]
    (
      @IN_JOB_FK BIGINT = NULL,
      @IN_job_class_fk INT = NULL,
      @IN_JOB_NAME VARCHAR(MAX) = 'UNKNOWN',
      @IN_DATA1 VARCHAR(MAX) = NULL,
      @IN_DATA2 VARCHAR(MAX) = NULL,
      @IN_DATA3 VARCHAR(MAX) = NULL,
      @IN_JOB_USER VARCHAR(MAX) = SUSER_NAME,
      @OUT_JOB_FK BIGINT OUTPUT,
      @CLIENT_APP VARCHAR(500) = 'OTHER'
      
    )
AS --Copyright 2005, epaCUBE, Inc.
  --$Header: /sdi/development/db/update/1.06.06.03/proc_sheet_calc_insert_results.sql,v 1.1 2005/05/11 18:43:34 cnoe Exp $
    BEGIN
        DECLARE @l_sysdate DATETIME,
            @ls_stmt VARCHAR(1000),
            @l_exec_no BIGINT ;
    
        SET @l_sysdate = GETDATE() ;
    
    
    -------------------------------------------------------------------------------------
    --   Insert JOB info into JOB Table... 
    -------------------------------------------------------------------------------------
    
        IF ISNULL(@IN_JOB_FK,0) = 0 
            EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                'job_seq', @in_job_fk OUTPUT


--            IF @CLIENT_APP <> 'OTHER' AND
--        IF NOT EXISTS ( SELECT  1
--                        FROM    common.job
--                        WHERE   job_id = @in_job_fk ) 
--            EXEC seq_manager.db_get_next_sequence_value_impl 'common',
--                'job_seq', @in_job_fk OUTPUT
                                                
                                         
        IF NOT EXISTS ( SELECT  1
                        FROM    common.job
                        WHERE   job_id = @in_job_fk ) 
            INSERT  INTO common.job
                    (
                      job_id,
                      job_class_fk,
                      name,
                      data1,
                      data2,
                      data3,
                      job_user,
                      job_create_timestamp
                        
                    )
            VALUES  (
                      @in_job_fk,
                      @in_job_class_fk,
                      @in_job_name,
                      @in_data1,
                      @in_data2,
                      @in_data3,
                      @in_job_user,
                      @l_sysdate
                        
                    ) ;
        ELSE 
            IF EXISTS ( SELECT  1
                        FROM    common.job
                        WHERE   job_id = @in_job_fk ) 
                UPDATE  common.job
                SET     job_class_fk = @in_job_class_fk,
                        NAME = @in_job_name,
                        data1 = @in_data1,
                        data2 = @in_data2,
                        data3 = @in_data3,
                        job_user = @in_job_user
                WHERE   job_id = @in_job_fk ;
        SET @OUT_JOB_FK = @in_job_fk ;
        
        IF @@TRANCOUNT > 0 
            COMMIT ;
    
    END ;
  

