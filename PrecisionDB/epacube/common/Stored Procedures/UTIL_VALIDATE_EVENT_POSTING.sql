﻿














-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To validate event data did post by batch no
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        02/24/2010  Initial version


CREATE PROCEDURE [common].[UTIL_VALIDATE_EVENT_POSTING] 
          @in_import_job_fk int 
         
AS



     DECLARE @ls_exec            nvarchar (max)
     DECLARE @l_exec_no          BIGINT
     DECLARE @ls_sql_stmt        nvarchar (max)
     DECLARE @ls_sql_stmt2       nvarchar (max)     

     DECLARE @ls_sql_perf1       nvarchar (max)     
     DECLARE @ls_sql_perf2       nvarchar (max)     

     DECLARE @ls_sql_pk          varchar (64)
     DECLARE @l_rows_processed   bigint
     DECLARE @l_sysdate          datetime
		
DECLARE @v_count bigint,
        @v_batch_no bigint,
        @v_job_class_fk  int,
        @v_job_name varchar(64),
        @v_data1 varchar(128),
        @v_data2 varchar(128),
        @v_data3 varchar(128),
        @v_job_user varchar(64),
        @v_out_job_fk bigint,
        @v_input_rows bigint,
        @v_output_rows bigint,
        @v_output_total bigint,
        @v_exception_rows bigint,
        @v_job_date datetime,
        @v_sysdate datetime

	DECLARE @l_stmt_no           bigint
    DECLARE @ls_stmt             varchar(1000)
	DECLARE @status_desc		 varchar (max)
	DECLARE @ErrorMessage		 nvarchar(4000)
	DECLARE @ErrorSeverity		 int
	DECLARE @ErrorState			 int


   SET NOCOUNT ON;

   BEGIN TRY
      SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of SYNCHRONIZER.UTIL_VALIDATE_EVENT_POSTING. '

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;

      SET @l_sysdate   = getdate()

            

            SET @status_desc =  'synchronizer.VALIDATE_EVENT_POSTING '
                                
                                
            Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

--------------------------------------------


-------------------------------------------------
--PRODUCT EVENTS THAT ARE MARKED POSTED - BUT DID'NT REALLY POST
-------------------------------------------------


INSERT INTO [common].[TESTING_LOGS]
           ([SCENARIO_NAME]
           ,[JOB_FK]
           ,[BATCH_NO]
           ,[EVENT_FK]
           ,[RULES_FK]
           ,[DATA_NAME_FK]           
           ,[EVENT_CONDITION_CR_FK]
           ,[SCENARIO_DESCRIPTION]
           ,[ACTUAL_NUMBER_OR_COUNT]
           ,[ACTUAL_DATA]
           ,[EXPECTED_RESULT]
           ,[EXPECTED_NUMBER_OR_COUNT]
           ,[EXPECTED_DATA]
           ,[DATA1]
           ,[DATA2]
           ,[DATA3]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK]
           )
SELECT 
           'NO POST'
           ,A.IMPORT_JOB_FK
           ,A.BATCH_NO
           ,NULL  --EVENT_FK, bigint,>
           ,NULL  --<RULES_FK, bigint,>
           ,A.DATA_NAME_FK
           ,NULL  --<EVENT_CONDITION_CR_FK, int,>
           ,'EVENT APPROVED BUT NOT POSTED - PRODUCT'
           ,A.CNT   --<ACTUAL_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<ACTUAL_DATA, varchar(256),>
           ,NULL  --<EXPECTED_RESULT, varchar(256),>
           ,NULL  --<EXPECTED_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<EXPECTED_DATA, varchar(256),>
           ,NULL  --<DATA1, varchar(64),>
           ,NULL  --<DATA2, varchar(64),>
           ,NULL  --<DATA3, varchar(64),>
           ,NULL  --<UPDATE_TIMESTAMP, datetime,>
           ,NULL  --<UPDATE_LOG_FK, bigint,>
FROM (
SELECT ED.IMPORT_JOB_FK
      ,ED.BATCH_NO
      ,ED.DATA_NAME_FK
      ,COUNT(1) CNT
FROM synchronizer.EVENT_DATA ED
	WHERE ED.event_id NOT IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD WITH (NOLOCK)
				 ON ( VCPD.DATA_NAME_FK = tsed.DATA_NAME_FK
				 AND  ISNULL ( VCPD.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( tsed.PRODUCT_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPD.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ORG_ENTITY_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPD.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ENTITY_STRUCTURE_FK, 0 )  
				 AND  ISNULL ( VCPD.UPDATE_LOG_FK, 0 ) = ISNULL ( tsed.EVENT_ID, 0 ) )
			    WHERE tsed.event_status_cr_fk = 81
				AND   tsed.event_type_cr_fk IN ( 150, 153, 154, 155, 156 )
				AND tsed.IMPORT_JOB_FK = @IN_IMPORT_JOB_FK
				)
AND import_job_fk = @in_import_job_fk
AND event_status_cr_fk = 81
AND event_type_cr_fk IN ( 150, 153, 154, 155, 156 )
GROUP BY IMPORT_JOB_FK, BATCH_NO, DATA_NAME_FK
) A


----PRE-POST PRODUCT EVENTS

INSERT INTO [common].[TESTING_LOGS]
           ([SCENARIO_NAME]
           ,[JOB_FK]
           ,[BATCH_NO]
           ,[EVENT_FK]
           ,[RULES_FK]
           ,[DATA_NAME_FK]           
           ,[EVENT_CONDITION_CR_FK]
           ,[SCENARIO_DESCRIPTION]
           ,[ACTUAL_NUMBER_OR_COUNT]
           ,[ACTUAL_DATA]
           ,[EXPECTED_RESULT]
           ,[EXPECTED_NUMBER_OR_COUNT]
           ,[EXPECTED_DATA]
           ,[DATA1]
           ,[DATA2]
           ,[DATA3]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK]
           )
SELECT 
           'NO POST'
           ,A.IMPORT_JOB_FK
           ,A.BATCH_NO
           ,NULL  --EVENT_FK, bigint,>
           ,NULL  --<RULES_FK, bigint,>
           ,A.DATA_NAME_FK
           ,NULL  --<EVENT_CONDITION_CR_FK, int,>
           ,'EVENT PRE-APPROVED BUT NOT POSTED - PRODUCT'
           ,A.CNT   --<ACTUAL_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<ACTUAL_DATA, varchar(256),>
           ,NULL  --<EXPECTED_RESULT, varchar(256),>
           ,NULL  --<EXPECTED_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<EXPECTED_DATA, varchar(256),>
           ,NULL  --<DATA1, varchar(64),>
           ,NULL  --<DATA2, varchar(64),>
           ,NULL  --<DATA3, varchar(64),>
           ,NULL  --<UPDATE_TIMESTAMP, datetime,>
           ,NULL  --<UPDATE_LOG_FK, bigint,>
FROM (
SELECT ED.IMPORT_JOB_FK
      ,ED.BATCH_NO
      ,ED.DATA_NAME_FK
      ,COUNT(1) CNT
FROM synchronizer.EVENT_DATA ED
	WHERE ED.event_id NOT IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD WITH (NOLOCK)
				 ON ( VCPD.DATA_NAME_FK = tsed.DATA_NAME_FK
				 AND  VCPD.CURRENT_DATA = tsed.NEW_DATA 
				 AND  ISNULL ( VCPD.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( tsed.PRODUCT_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPD.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ORG_ENTITY_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPD.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ENTITY_STRUCTURE_FK, 0 )  
				 )
			    WHERE tsed.event_status_cr_fk = 83
                AND   tsed.event_type_cr_fk IN ( 150, 153, 154, 155, 156 )
				AND   tsed.IMPORT_JOB_FK = @IN_IMPORT_JOB_FK
				)
AND import_job_fk = @in_import_job_fk
AND event_status_cr_fk = 83
AND Data_Name_FK <> 149000
AND event_type_cr_fk IN ( 150, 153, 154, 155, 156 )
GROUP BY IMPORT_JOB_FK, BATCH_NO, DATA_NAME_FK
) A




-------------------------------------------------
--PRODUCT EVENTS NOT MARKED APPROVED - BUT DID REALLY POST
-------------------------------------------------
 
INSERT INTO [common].[TESTING_LOGS]
           ([SCENARIO_NAME]
           ,[JOB_FK]
           ,[BATCH_NO]
           ,[EVENT_FK]
           ,[RULES_FK]
           ,[DATA_NAME_FK]           
           ,[EVENT_CONDITION_CR_FK]
           ,[SCENARIO_DESCRIPTION]
           ,[ACTUAL_NUMBER_OR_COUNT]
           ,[ACTUAL_DATA]
           ,[EXPECTED_RESULT]
           ,[EXPECTED_NUMBER_OR_COUNT]
           ,[EXPECTED_DATA]
           ,[DATA1]
           ,[DATA2]
           ,[DATA3]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK]
           )
SELECT 
           'POSTED BUT NOT MARKED APPROVE'
           ,A.IMPORT_JOB_FK
           ,A.BATCH_NO
           ,NULL  --EVENT_FK, bigint,>
           ,NULL  --<RULES_FK, bigint,>
           ,A.DATA_NAME_FK
           ,NULL  --<EVENT_CONDITION_CR_FK, int,>
           ,'EVENT DATA POSTED BUT EVENT NOT MARKED APPROVED - PRODUCT'
           ,A.CNT   --<ACTUAL_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<ACTUAL_DATA, varchar(256),>
           ,NULL  --<EXPECTED_RESULT, varchar(256),>
           ,NULL  --<EXPECTED_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<EXPECTED_DATA, varchar(256),>
           ,NULL  --<DATA1, varchar(64),>
           ,NULL  --<DATA2, varchar(64),>
           ,NULL  --<DATA3, varchar(64),>
           ,NULL  --<UPDATE_TIMESTAMP, datetime,>
           ,NULL  --<UPDATE_LOG_FK, bigint,>
FROM (
SELECT ED.IMPORT_JOB_FK
      ,ED.BATCH_NO
      ,ED.DATA_NAME_FK
      ,COUNT(1) CNT
FROM synchronizer.EVENT_DATA ED
	WHERE ED.event_id IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_PRODUCT_DATA VCPD WITH (NOLOCK)
				 ON ( VCPD.DATA_NAME_FK = tsed.DATA_NAME_FK
				 AND  ISNULL ( VCPD.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( tsed.PRODUCT_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPD.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ORG_ENTITY_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPD.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ENTITY_STRUCTURE_FK, 0 )  
				 AND  ISNULL ( VCPD.UPDATE_LOG_FK, 0 ) = ISNULL ( tsed.EVENT_ID, 0 ) )
			    WHERE tsed.event_status_cr_fk IN (80, 88, 86, 87)
				AND   tsed.event_type_cr_fk IN ( 150, 153, 154, 155, 156 )
				AND tsed.IMPORT_JOB_FK = @IN_IMPORT_JOB_FK
				)
AND import_job_fk = @in_import_job_fk
AND event_status_cr_fk IN (80, 88, 86, 87)
AND event_type_cr_fk IN ( 150, 153, 154, 155, 156 )
GROUP BY IMPORT_JOB_FK, BATCH_NO, DATA_NAME_FK
) A


----	 
----       SET @l_rows_processed = @@ROWCOUNT
----	   
----	   IF @l_rows_processed > 0
----	   BEGIN
----			 SET @status_desc = 'total rows Events APPROVED-BUT NOT POSTED:: '
----                                + ' '			 
----								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
----			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
----       END


---------------------------------------------------------
--PROD VALUES THAT ARE MARKED POSTED - BUT DIDN'T REALLY POST - Values
---------------------------------------------------------
 
  			  	  

INSERT INTO [common].[TESTING_LOGS]
           ([SCENARIO_NAME]
           ,[JOB_FK]
           ,[BATCH_NO]
           ,[EVENT_FK]
           ,[RULES_FK]
           ,[DATA_NAME_FK]           
           ,[EVENT_CONDITION_CR_FK]
           ,[SCENARIO_DESCRIPTION]
           ,[ACTUAL_NUMBER_OR_COUNT]
           ,[ACTUAL_DATA]
           ,[EXPECTED_RESULT]
           ,[EXPECTED_NUMBER_OR_COUNT]
           ,[EXPECTED_DATA]
           ,[DATA1]
           ,[DATA2]
           ,[DATA3]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK]
           )
SELECT 
           'NO POST'
           ,A.IMPORT_JOB_FK
           ,A.BATCH_NO
           ,NULL  --EVENT_FK, bigint,>
           ,NULL  --<RULES_FK, bigint,>
           ,A.DATA_NAME_FK
           ,NULL  --<EVENT_CONDITION_CR_FK, int,>
           ,'EVENT APPROVED BUT NOT POSTED - VALUES'
           ,A.CNT   --<ACTUAL_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<ACTUAL_DATA, varchar(256),>
           ,NULL  --<EXPECTED_RESULT, varchar(256),>
           ,NULL  --<EXPECTED_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<EXPECTED_DATA, varchar(256),>
           ,NULL  --<DATA1, varchar(64),>
           ,NULL  --<DATA2, varchar(64),>
           ,NULL  --<DATA3, varchar(64),>
           ,NULL  --<UPDATE_TIMESTAMP, datetime,>
           ,NULL  --<UPDATE_LOG_FK, bigint,>
FROM (
SELECT ED.IMPORT_JOB_FK
      ,ED.BATCH_NO
      ,ED.DATA_NAME_FK
      ,COUNT(1) CNT
FROM synchronizer.EVENT_DATA ED
	WHERE ED.event_id NOT IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_PRODUCT_VALUES VCPV WITH (NOLOCK)
				 ON ( VCPV.DATA_NAME_FK = tsed.DATA_NAME_FK
				 AND  ISNULL ( VCPV.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( tsed.PRODUCT_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPV.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ORG_ENTITY_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCPV.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ENTITY_STRUCTURE_FK, 0 )  
				 AND  ISNULL ( VCPV.UPDATE_LOG_FK, 0 ) = ISNULL ( tsed.EVENT_ID, 0 ) )
			   WHERE tsed.event_status_cr_fk = 81
 AND   tsed.event_type_cr_fk IN ( 151 )
				AND tsed.IMPORT_JOB_FK = @IN_IMPORT_JOB_FK
				)
AND import_job_fk = @in_import_job_fk
AND event_status_cr_fk = 81	
AND event_type_cr_fk IN ( 151 )
GROUP BY IMPORT_JOB_FK, BATCH_NO, DATA_NAME_FK
) A
    
	 
----       SET @l_rows_processed = @@ROWCOUNT
----	   
----	   IF @l_rows_processed > 0
----	   BEGIN
----			 SET @status_desc = 'total rows Product Value Events APPROVED-BUT NOT POSTED:: '
----                                + ' '			 
----								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
----			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
----       END


---------------------------------------------------------
--PROD RESULTS THAT ARE MARKED POSTED - BUT DIDN'T REALLY POST - RESULTS
---------------------------------------------------------
 

INSERT INTO [common].[TESTING_LOGS]
           ([SCENARIO_NAME]
           ,[JOB_FK]
           ,[BATCH_NO]
           ,[EVENT_FK]
           ,[RULES_FK]
           ,[DATA_NAME_FK]           
           ,[EVENT_CONDITION_CR_FK]
           ,[SCENARIO_DESCRIPTION]
           ,[ACTUAL_NUMBER_OR_COUNT]
           ,[ACTUAL_DATA]
           ,[EXPECTED_RESULT]
           ,[EXPECTED_NUMBER_OR_COUNT]
           ,[EXPECTED_DATA]
           ,[DATA1]
           ,[DATA2]
           ,[DATA3]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK]
           )
SELECT 
           'NO POST'
           ,A.IMPORT_JOB_FK
           ,A.BATCH_NO
           ,NULL  --EVENT_FK, bigint,>
           ,NULL  --<RULES_FK, bigint,>
           ,A.DATA_NAME_FK
           ,NULL  --<EVENT_CONDITION_CR_FK, int,>
           ,'EVENT APPROVED BUT NOT POSTED - RESULTS'
           ,A.CNT   --<ACTUAL_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<ACTUAL_DATA, varchar(256),>
           ,NULL  --<EXPECTED_RESULT, varchar(256),>
           ,NULL  --<EXPECTED_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<EXPECTED_DATA, varchar(256),>
           ,NULL  --<DATA1, varchar(64),>
           ,NULL  --<DATA2, varchar(64),>
           ,NULL  --<DATA3, varchar(64),>
           ,NULL  --<UPDATE_TIMESTAMP, datetime,>
           ,NULL  --<UPDATE_LOG_FK, bigint,>
FROM (
SELECT ED.IMPORT_JOB_FK
      ,ED.BATCH_NO
      ,ED.DATA_NAME_FK
      ,COUNT(1) CNT
FROM synchronizer.EVENT_DATA ED
	WHERE ED.event_id NOT IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_SHEET_RESULTS VCSR WITH (NOLOCK)
				 ON ( VCSR.DATA_NAME_FK = tsed.DATA_NAME_FK
				 AND  ISNULL ( VCSR.PRODUCT_STRUCTURE_FK, 0 ) = ISNULL ( tsed.PRODUCT_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCSR.ORG_ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ORG_ENTITY_STRUCTURE_FK, 0 )
				 AND  ISNULL ( VCSR.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( tsed.ENTITY_STRUCTURE_FK, 0 )  
				 AND  ISNULL ( VCSR.UPDATE_LOG_FK, 0 ) = ISNULL ( tsed.EVENT_ID, 0 ) )			    
			    WHERE tsed.event_status_cr_fk =81
 AND   tsed.event_type_cr_fk IN ( 152 )
				AND tsed.IMPORT_JOB_FK = @IN_IMPORT_JOB_FK
				)
AND import_job_fk = @in_import_job_fk
AND event_status_cr_fk = 81
AND event_type_cr_fk IN ( 152 )
GROUP BY IMPORT_JOB_FK, BATCH_NO, DATA_NAME_FK
) A

	 
----       SET @l_rows_processed = @@ROWCOUNT
----	   
----	   IF @l_rows_processed > 0
----	   BEGIN
----			 SET @status_desc = 'total rows Sheet Result Events APPROVED-BUT NOT POSTED:: '
----                                + ' '			 
----								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
----			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
----       END

---------------------------------------------------------
--ENTITY EVENTS THAT ARE MARKED POSTED - BUT DIDN'T REALLY POST - ENTITIES
---------------------------------------------------------
 

INSERT INTO [common].[TESTING_LOGS]
           ([SCENARIO_NAME]
           ,[JOB_FK]
           ,[BATCH_NO]
           ,[EVENT_FK]
           ,[RULES_FK]
           ,[DATA_NAME_FK]           
           ,[EVENT_CONDITION_CR_FK]
           ,[SCENARIO_DESCRIPTION]
           ,[ACTUAL_NUMBER_OR_COUNT]
           ,[ACTUAL_DATA]
           ,[EXPECTED_RESULT]
           ,[EXPECTED_NUMBER_OR_COUNT]
           ,[EXPECTED_DATA]
           ,[DATA1]
           ,[DATA2]
           ,[DATA3]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK]
           )
SELECT 
           'NO POST'
           ,A.IMPORT_JOB_FK
           ,A.BATCH_NO
           ,NULL  --EVENT_FK, bigint,>
           ,NULL  --<RULES_FK, bigint,>
           ,A.DATA_NAME_FK
           ,NULL  --<EVENT_CONDITION_CR_FK, int,>
           ,'EVENT APPROVED BUT NOT POSTED - ENTITIES'
           ,A.CNT   --<ACTUAL_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<ACTUAL_DATA, varchar(256),>
           ,NULL  --<EXPECTED_RESULT, varchar(256),>
           ,NULL  --<EXPECTED_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<EXPECTED_DATA, varchar(256),>
           ,NULL  --<DATA1, varchar(64),>
           ,NULL  --<DATA2, varchar(64),>
           ,NULL  --<DATA3, varchar(64),>
           ,NULL  --<UPDATE_TIMESTAMP, datetime,>
           ,NULL  --<UPDATE_LOG_FK, bigint,>
FROM (
SELECT ED.IMPORT_JOB_FK
      ,ED.BATCH_NO
      ,ED.DATA_NAME_FK
      ,COUNT(1) CNT
FROM synchronizer.EVENT_DATA ED
	WHERE ED.event_id NOT IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_ENTITY_DATA VCED WITH (NOLOCK)
				 ON ( VCED.DATA_NAME_FK = TSED.DATA_NAME_FK
			AND  ISNULL ( VCED.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.EPACUBE_ID, 0 )  			 
			AND  ISNULL ( VCED.UPDATE_LOG_FK, 0 ) = ISNULL ( TSED.EVENT_ID, 0 ) )	
	        AND   tsed.event_type_cr_fk IN ( 154, 160 )
			AND tsed.event_status_cr_fk = 81
				AND tsed.IMPORT_JOB_FK = @IN_IMPORT_JOB_FK
				)
AND import_job_fk = @in_import_job_fk
AND event_status_cr_fk = 81		
AND event_type_cr_fk IN ( 154, 160 )    
GROUP BY IMPORT_JOB_FK, BATCH_NO, DATA_NAME_FK
) A

	 
----       SET @l_rows_processed = @@ROWCOUNT
----	   
----	   IF @l_rows_processed > 0
----	   BEGIN
----			 SET @status_desc = 'total rows Entity Events APPROVED-BUT NOT POSTED:: '
----                                + ' '			 
----								+ CAST(isnull(@l_rows_processed,0) AS VARCHAR(30))
----			 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
----       END

---------------------------------------------------------
--ENTITY EVENTS THAT ARE POSTED - BUT NOT MARKED AS POSTED - ENTITIES
---------------------------------------------------------
 

INSERT INTO [common].[TESTING_LOGS]
           ([SCENARIO_NAME]
           ,[JOB_FK]
           ,[BATCH_NO]
           ,[EVENT_FK]
           ,[RULES_FK]
           ,[DATA_NAME_FK]           
           ,[EVENT_CONDITION_CR_FK]
           ,[SCENARIO_DESCRIPTION]
           ,[ACTUAL_NUMBER_OR_COUNT]
           ,[ACTUAL_DATA]
           ,[EXPECTED_RESULT]
           ,[EXPECTED_NUMBER_OR_COUNT]
           ,[EXPECTED_DATA]
           ,[DATA1]
           ,[DATA2]
           ,[DATA3]
           ,[UPDATE_TIMESTAMP]
           ,[UPDATE_LOG_FK]
           )
SELECT 
           'POSTED BUT NOT MARKED APPROVE'
           ,A.IMPORT_JOB_FK
           ,A.BATCH_NO
           ,NULL  --EVENT_FK, bigint,>
           ,NULL  --<RULES_FK, bigint,>
           ,A.DATA_NAME_FK
           ,NULL  --<EVENT_CONDITION_CR_FK, int,>
           ,'EVENT DATA POSTED BUT EVENT NOT MARKED APPROVED - ENTITIES'
           ,A.CNT   --<ACTUAL_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<ACTUAL_DATA, varchar(256),>
           ,NULL  --<EXPECTED_RESULT, varchar(256),>
           ,NULL  --<EXPECTED_NUMBER_OR_COUNT, numeric,>
           ,NULL  --<EXPECTED_DATA, varchar(256),>
           ,NULL  --<DATA1, varchar(64),>
           ,NULL  --<DATA2, varchar(64),>
           ,NULL  --<DATA3, varchar(64),>
           ,NULL  --<UPDATE_TIMESTAMP, datetime,>
           ,NULL  --<UPDATE_LOG_FK, bigint,>
FROM (
SELECT ED.IMPORT_JOB_FK
      ,ED.BATCH_NO
      ,ED.DATA_NAME_FK
      ,COUNT(1) CNT
FROM synchronizer.EVENT_DATA ED
	WHERE ED.event_id IN (
				SELECT tsed.EVENT_ID
				FROM synchronizer.EVENT_DATA  tsed
				INNER JOIN epacube.V_CURRENT_ENTITY_DATA VCED WITH (NOLOCK)
				 ON ( VCED.DATA_NAME_FK = TSED.DATA_NAME_FK
			AND  ISNULL ( VCED.ENTITY_STRUCTURE_FK, 0 ) = ISNULL ( TSED.EPACUBE_ID, 0 )  			 
			AND  ISNULL ( VCED.UPDATE_LOG_FK, 0 ) = ISNULL ( TSED.EVENT_ID, 0 ) )	
	        AND   tsed.event_type_cr_fk IN ( 154, 160 )
			AND tsed.event_status_cr_fk IN (80, 88, 86, 87)
				AND tsed.IMPORT_JOB_FK = @IN_IMPORT_JOB_FK
				)
AND import_job_fk = @in_import_job_fk
AND event_status_cr_fk IN (80, 88, 86, 87) 
AND event_type_cr_fk IN ( 154, 160 )   
GROUP BY IMPORT_JOB_FK, BATCH_NO, DATA_NAME_FK
) A





------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of SYNCHRONIZER.UTIL_VALIDATE_EVENT_POSTING_DATA'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of SYNCHRONIZER.SYNCHRONIZER.UTIL_VALIDATE_EVENT_POSTING has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH













