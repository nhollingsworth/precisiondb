﻿CREATE CONTRACT [http://epacube.com/EPAContract]
    AUTHORIZATION [dbo]
    ([http://epacube.com/RequestMessage] SENT BY INITIATOR, [http://epacube.com/ResponseMessage] SENT BY TARGET);

