﻿CREATE QUEUE [queue_manager].[TargetEpaQueue]
    WITH POISON_MESSAGE_HANDLING(STATUS = OFF), ACTIVATION (STATUS = ON, PROCEDURE_NAME = [queue_manager].[ProcessRequestMessages], MAX_QUEUE_READERS = 1, EXECUTE AS N'dbo');

