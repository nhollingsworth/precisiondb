﻿CREATE QUEUE [queue_manager].[InitiatorQueue]
    WITH POISON_MESSAGE_HANDLING(STATUS = OFF), ACTIVATION (STATUS = ON, PROCEDURE_NAME = [queue_manager].[ProcessRequestMessages], MAX_QUEUE_READERS = 5, EXECUTE AS N'dbo');

