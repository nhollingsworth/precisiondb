﻿CREATE TABLE [marginmgr].[PRICESHEET_RETAILS_SPECIAL] (
    [product_structure_fk]          BIGINT          NULL,
    [cust_entity_structure_fk]      BIGINT          NULL,
    [Effective_Date]                DATE            NULL,
    [End_Date]                      DATE            NULL,
    [prc_mult]                      INT             NULL,
    [SRP]                           NUMERIC (18, 2) NULL,
    [Reason_Change_Code]            VARCHAR (16)    NULL,
    [Price_Change_Reason_DV_FK]     BIGINT          NULL,
    [Reason]                        VARCHAR (256)   NULL,
    [Rules_Sales_Programs_FK]       BIGINT          NULL,
    [PRICESHEET_RETAILS_SPECIAL_ID] BIGINT          IDENTITY (1000, 1) NOT NULL,
    [data_name_fk]                  BIGINT          NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_01]
    ON [marginmgr].[PRICESHEET_RETAILS_SPECIAL]([cust_entity_structure_fk] ASC, [product_structure_fk] ASC, [Rules_Sales_Programs_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_o2]
    ON [marginmgr].[PRICESHEET_RETAILS_SPECIAL]([Rules_Sales_Programs_FK] ASC, [product_structure_fk] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

