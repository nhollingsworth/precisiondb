﻿CREATE TABLE [marginmgr].[PRICESHEET_RETAILS_WEEKLY_AUDIT] (
    [PRICESHEET_RETAILS_WEEKLY_AUDIT_ID]        BIGINT          IDENTITY (1000, 1) NOT NULL,
    [PRICESHEET_BASIS_VALUES_FK]                BIGINT          NULL,
    [DATA_NAME_FK]                              BIGINT          NULL,
    [PRICE_MGMT_TYPE_CR_FK]                     INT             NULL,
    [ITEM_CHANGE_TYPE_CR_FK]                    INT             NULL,
    [QUALIFIED_STATUS_CR_FK]                    INT             NULL,
    [PARITY_PL_PARENT_STRUCTURE_FK]             BIGINT          NULL,
    [PARITY_SIZE_PARENT_STRUCTURE_FK]           BIGINT          NULL,
    [ALIAS_DATA_VALUE_FK]                       BIGINT          NULL,
    [TARGET_MARGIN]                             NUMERIC (18, 4) NULL,
    [PRODUCT_STRUCTURE_FK]                      BIGINT          NULL,
    [CUST_ENTITY_STRUCTURE_FK]                  BIGINT          NULL,
    [ZONE_SEGMENT_FK]                           BIGINT          NULL,
    [PRICE_MULTIPLE]                            INT             NULL,
    [UNIT_COST]                                 MONEY           NULL,
    [PRICE]                                     MONEY           NULL,
    [GM_PCT]                                    NUMERIC (18, 4) NULL,
    [EFFECTIVE_DATE]                            DATE            NULL,
    [END_DATE]                                  DATE            NULL,
    [PRICE_TYPE]                                INT             NULL,
    [PROGRAM_TYPE]                              VARCHAR (64)    NULL,
    [USE_LG_PRICE_POINTS]                       VARCHAR (8)     NULL,
    [RETAIL_SOURCE_DATA_NAME_FK]                BIGINT          NULL,
    [RETAIL_SOURCE_FK]                          BIGINT          NULL,
    [RETAIL_SOURCE]                             VARCHAR (16)    NULL,
    [PRICE_CHANGE_REASON_DV_FK]                 BIGINT          NULL,
    [PRICE_CHANGE_REASON]                       VARCHAR (64)    NULL,
    [CREATE_TIMESTAMP]                          DATETIME        NULL,
    [CREATE_USER]                               VARCHAR (96)    NULL,
    [UPDATE_TIMESTAMP]                          DATE            NULL,
    [UPDATE_USER]                               VARCHAR (96)    NULL,
    [DATA_SOURCE]                               VARCHAR (96)    NULL,
    [PRICESHEET_RETAIL_CHANGES_FK]              BIGINT          NULL,
    [ALIAS_PARITY_ALIAS_PL_PARENT_STRUCTURE_FK] BIGINT          NULL,
    [PARITY_ALIAS_PL_PARENT_STRUCTURE_FK]       BIGINT          NULL,
    [IMPORT_TIMESTAMP]                          DATETIME        CONSTRAINT [DF_PRICESHEET_RETAILS_WEEKLY_AUDIT_CREATE_TIMESTAMP] DEFAULT (getdate()) NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_cust_01]
    ON [marginmgr].[PRICESHEET_RETAILS_WEEKLY_AUDIT]([DATA_NAME_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC, [CUST_ENTITY_STRUCTURE_FK] ASC, [EFFECTIVE_DATE] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_eff_zs_ps]
    ON [marginmgr].[PRICESHEET_RETAILS_WEEKLY_AUDIT]([ZONE_SEGMENT_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC, [EFFECTIVE_DATE] ASC, [CREATE_TIMESTAMP] ASC, [PRICESHEET_RETAILS_WEEKLY_AUDIT_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_zone_01]
    ON [marginmgr].[PRICESHEET_RETAILS_WEEKLY_AUDIT]([DATA_NAME_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC, [ZONE_SEGMENT_FK] ASC, [EFFECTIVE_DATE] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

