﻿CREATE TABLE [marginmgr].[PRICESHEET_RETAIL_CHANGES_USER_TRACE] (
    [PROCEDURE_NAME]                          VARCHAR (96)    NOT NULL,
    [DATA_STREAM]                             VARCHAR (16)    NULL,
    [PRICESHEET_RETAIL_CHANGES_FK]            BIGINT          NOT NULL,
    [CREATE_USER]                             VARCHAR (96)    NULL,
    [CREATE_TIMESTAMP]                        DATETIME        CONSTRAINT [DF_PRICESHEET_RETAIL_CHANGES_USER_TRACE_CREATE_TIMESTAMP] DEFAULT (getdate()) NULL,
    [GP_New]                                  NUMERIC (18, 4) NULL,
    [Price_New]                               NUMERIC (18, 4) NULL,
    [TM_Adjusted]                             NUMERIC (18, 4) NULL,
    [Price_Multiple_New]                      INT             NULL,
    [PPT]                                     INT             NULL,
    [AutoCalcTM]                              INT             NULL,
    [FLAG]                                    VARCHAR (16)    NULL,
    [VALUE]                                   INT             NULL,
    [SRP_ADJUSTED_OUT]                        NUMERIC (18, 2) NULL,
    [GM_ADJUSTED_OUT]                         NUMERIC (18, 4) NULL,
    [TM_ADJUSTED_OUT]                         NUMERIC (18, 4) NULL,
    [PRICE_MULTIPLE_NEW_OUT]                  INT             NULL,
    [HOLD_CUR_PRICE_OUT]                      INT             NULL,
    [REVIEWED_OUT]                            INT             NULL,
    [USER_MSG_OUT]                            VARCHAR (128)   NULL,
    [PRICESHEET_RETAIL_CHANGES_USER_TRACE_ID] BIGINT          IDENTITY (1, 1) NOT NULL
);



