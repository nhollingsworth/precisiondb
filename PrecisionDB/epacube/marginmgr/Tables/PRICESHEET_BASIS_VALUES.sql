﻿CREATE TABLE [marginmgr].[PRICESHEET_BASIS_VALUES] (
    [Pricesheet_Basis_Values_ID]        BIGINT          IDENTITY (1, 1) NOT NULL,
    [PriceSheetID]                      VARCHAR (64)    NULL,
    [Effective_Date]                    DATE            NULL,
    [End_Date]                          DATE            NULL,
    [Product_Structure_FK]              BIGINT          NULL,
    [Org_Entity_Structure_FK]           BIGINT          NULL,
    [Cust_Entity_Structure_FK]          BIGINT          NULL,
    [Vendor_Entity_Structure_FK]        BIGINT          NULL,
    [Zone_Type_CR_FK]                   INT             NULL,
    [Zone_Entity_Structure_FK]          BIGINT          NULL,
    [Data_Name_FK]                      BIGINT          NOT NULL,
    [Basis_Position]                    INT             NULL,
    [Value]                             NUMERIC (18, 6) NULL,
    [UOM_CODE_FK]                       BIGINT          NULL,
    [PACK]                              INT             NULL,
    [SPC_UOM_FK]                        BIGINT          NULL,
    [Calculated_Value]                  SMALLINT        NULL,
    [Promo]                             SMALLINT        NULL,
    [CO_ENTITY_STRUCTURE_FK]            BIGINT          NULL,
    [RECORD_STATUS_CR_FK]               SMALLINT        NULL,
    [JOB_FK]                            BIGINT          NULL,
    [UPDATE_USER]                       VARCHAR (64)    NULL,
    [CREATE_TIMESTAMP]                  DATETIME        NULL,
    [UPDATE_TIMESTAMP]                  DATETIME        NULL,
    [CREATE_USER]                       VARCHAR (64)    NULL,
    [Comp_Entity_Structure_FK]          BIGINT          NULL,
    [Comp_Segment_FK]                   BIGINT          NULL,
    [UNIT_VALUE]                        NUMERIC (18, 6) NULL,
    [Rescind]                           INT             NULL,
    [RESCIND_TIMESTAMP]                 DATETIME        NULL,
    [Change_Status]                     INT             NULL,
    [IMPORT_TIMESTAMP]                  DATETIME        CONSTRAINT [DF_PRICESHEET_BASIS_VALUES_IMPORT_TIMESTAMP] DEFAULT (getdate()) NULL,
    [Cost_Change_Processed_Date]        DATE            NULL,
    [comment]                           VARCHAR (256)   NULL,
    [PRICESHEET_BASIS_VALUES_UNIQUE_FK] BIGINT          NULL,
    [unique_cost_record_id]             VARCHAR (64)    NULL
);




GO
CREATE CLUSTERED INDEX [idx_pbv_ED_PSFK_CESFFK_DNFK]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([Effective_Date] ASC, [Product_Structure_FK] ASC, [Cust_Entity_Structure_FK] ASC, [Data_Name_FK] ASC) WITH (FILLFACTOR = 80);


GO
CREATE NONCLUSTERED INDEX [idx_pbv_518]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([Data_Name_FK] ASC, [Product_Structure_FK] ASC, [Zone_Entity_Structure_FK] ASC, [Effective_Date] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_pbv_508]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([Data_Name_FK] ASC, [Product_Structure_FK] ASC, [Vendor_Entity_Structure_FK] ASC, [Cust_Entity_Structure_FK] ASC, [Effective_Date] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [inx_table_id]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([Pricesheet_Basis_Values_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_tst01]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([Cust_Entity_Structure_FK] ASC, [Data_Name_FK] ASC, [RECORD_STATUS_CR_FK] ASC, [Effective_Date] ASC)
    INCLUDE([Pricesheet_Basis_Values_ID], [Product_Structure_FK], [Value], [UPDATE_TIMESTAMP], [UNIT_VALUE]) WITH (FILLFACTOR = 80);




GO
CREATE NONCLUSTERED INDEX [idx_]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([Data_Name_FK] ASC, [Effective_Date] ASC, [Product_Structure_FK] ASC, [Pricesheet_Basis_Values_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_job]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([JOB_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_ces_dn_rs_effdt_cdt_incl]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([Cust_Entity_Structure_FK] ASC, [Data_Name_FK] ASC, [RECORD_STATUS_CR_FK] ASC, [Effective_Date] ASC, [CREATE_TIMESTAMP] ASC)
    INCLUDE([Pricesheet_Basis_Values_ID], [Product_Structure_FK], [Value], [UNIT_VALUE]) WITH (FILLFACTOR = 80);




GO
CREATE NONCLUSTERED INDEX [idx_rt_incl_eff_ps]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([RESCIND_TIMESTAMP] ASC)
    INCLUDE([Effective_Date], [Product_Structure_FK]) WITH (FILLFACTOR = 80);




GO
CREATE NONCLUSTERED INDEX [idx_ces_rs_rt_zes_incl_others]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([Cust_Entity_Structure_FK] ASC, [RECORD_STATUS_CR_FK] ASC, [RESCIND_TIMESTAMP] ASC, [Zone_Entity_Structure_FK] ASC)
    INCLUDE([Pricesheet_Basis_Values_ID], [UNIT_VALUE], [unique_cost_record_id]) WITH (FILLFACTOR = 80);




GO
CREATE NONCLUSTERED INDEX [idx_ces_dn_rs_eff_zes_incl_others]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([Cust_Entity_Structure_FK] ASC, [Data_Name_FK] ASC, [RECORD_STATUS_CR_FK] ASC, [Effective_Date] ASC, [Zone_Entity_Structure_FK] ASC)
    INCLUDE([Product_Structure_FK], [Value], [UNIT_VALUE]) WITH (FILLFACTOR = 80);




GO
CREATE NONCLUSTERED INDEX [idx_ces_rs_rt_zes_incl_others2]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([Cust_Entity_Structure_FK] ASC, [RECORD_STATUS_CR_FK] ASC, [RESCIND_TIMESTAMP] ASC, [Zone_Entity_Structure_FK] ASC)
    INCLUDE([Pricesheet_Basis_Values_ID], [UNIT_VALUE], [unique_cost_record_id]) WITH (FILLFACTOR = 80);




GO
CREATE NONCLUSTERED INDEX [idx_01]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([Product_Structure_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_ucr]
    ON [marginmgr].[PRICESHEET_BASIS_VALUES]([unique_cost_record_id] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

