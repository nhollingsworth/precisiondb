﻿CREATE TABLE [marginmgr].[pricesheet_basis_values_unique] (
    [Data_Name_FK]                             BIGINT          NOT NULL,
    [Zone_Entity_Structure_FK]                 BIGINT          NULL,
    [Item_Surrogate]                           VARCHAR (64)    NOT NULL,
    [Product_Structure_FK]                     BIGINT          NULL,
    [sur_tme]                                  BIGINT          NOT NULL,
    [unique_cost_record_id]                    VARCHAR (64)    NOT NULL,
    [Effective_Date]                           DATE            NOT NULL,
    [Effective_Date_Raw]                       BIGINT          NOT NULL,
    [Member_Price]                             NUMERIC (18, 4) NOT NULL,
    [rescind]                                  INT             NULL,
    [rescind_timestamp]                        DATETIME        NULL,
    [Update_Timestamp]                         DATETIME        NULL,
    [Create_Timestamp]                         DATETIME        NOT NULL,
    [DTE_CRT]                                  INT             NULL,
    [TME_CRT]                                  INT             NULL,
    [USR_CRT]                                  VARCHAR (64)    NULL,
    [DTE_UPD]                                  INT             NULL,
    [TME_UPD]                                  INT             NULL,
    [USR_UPD]                                  VARCHAR (64)    NULL,
    [Job_FK]                                   BIGINT          NULL,
    [Import_Timestamp]                         DATETIME        NULL,
    [Apply_Rescind_Flag]                       INT             NULL,
    [Apply_Rescind_timestamp]                  DATETIME        NULL,
    [Apply_Rescind_Pricesheet_Basis_Values_FK] BIGINT          NULL,
    [pricesheet_basis_values_unique_id]        BIGINT          IDENTITY (1, 1) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_01]
    ON [marginmgr].[pricesheet_basis_values_unique]([Item_Surrogate] ASC, [Effective_Date] ASC, [sur_tme] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

