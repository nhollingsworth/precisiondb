﻿CREATE TABLE [marginmgr].[pricesheet_basis_values_icc] (
    [value]                      NUMERIC (18, 6) NULL,
    [unit_value]                 NUMERIC (18, 6) NULL,
    [effective_date]             DATE            NULL,
    [pack]                       INT             NULL,
    [size_uom]                   VARCHAR (16)    NULL,
    [product_structure_fk]       BIGINT          NULL,
    [pricesheet_basis_values_id] BIGINT          NULL,
    [data_name_fk]               BIGINT          NULL,
    [create_timestamp]           DATETIME        NULL,
    [cust_entity_structure_fk]   BIGINT          NULL,
    [drank]                      BIGINT          NULL,
    [pbv_status]                 VARCHAR (128)   NULL,
    [add_to_zone_date]           DATE            NULL,
    [change_status]              INT             NULL,
    [cost_change_processed_date] DATE            NULL,
    [drank_f]                    BIGINT          NULL,
    [ind_cost_change_item]       INT             NULL,
    [item]                       VARCHAR (64)    NULL,
    [unique_cost_record_id]      VARCHAR (64)    NULL,
    [pbv_id]                     BIGINT          IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_pricesheet_basis_values_icc] PRIMARY KEY CLUSTERED ([pbv_id] ASC) WITH (FILLFACTOR = 80)
);

