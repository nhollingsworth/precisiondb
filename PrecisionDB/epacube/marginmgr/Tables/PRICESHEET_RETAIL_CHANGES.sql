﻿CREATE TABLE [marginmgr].[PRICESHEET_RETAIL_CHANGES] (
    [PRICE_MGMT_TYPE_CR_FK]                 BIGINT          NULL,
    [PRICE_MGMT_TYPE]                       VARCHAR (24)    NULL,
    [ITEM_CHANGE_TYPE_CR_FK]                INT             NOT NULL,
    [ITEM_CHANGE_TYPE]                      VARCHAR (32)    NULL,
    [PARITY_CHILD]                          INT             NULL,
    [QUALIFIED_STATUS_CR_FK]                INT             NULL,
    [QUALIFIED_STATUS]                      VARCHAR (64)    NULL,
    [PRICESHEET_BASIS_VALUES_FK]            BIGINT          NULL,
    [ZONE]                                  VARCHAR (128)   NULL,
    [ZONE_NAME]                             VARCHAR (128)   NULL,
    [ITEM_DESCRIPTION]                      VARCHAR (256)   NULL,
    [ALIAS_ID]                              VARCHAR (16)    NULL,
    [cost_change_effective_date_cur]        DATE            NULL,
    [STORE_PACK]                            INT             NULL,
    [case_cost_cur]                         NUMERIC (18, 6) NULL,
    [unit_cost_cur]                         NUMERIC (18, 6) NULL,
    [PRICE_MULTIPLE_CUR]                    INT             NULL,
    [PRICE_CUR]                             MONEY           NULL,
    [CUR_GM_PCT]                            NUMERIC (18, 4) NULL,
    [PRICE_EFFECTIVE_DATE_CUR]              DATE            NULL,
    [case_cost_new]                         NUMERIC (18, 6) NULL,
    [unit_cost_new]                         NUMERIC (18, 6) NULL,
    [COST_CHANGE]                           NUMERIC (19, 6) NULL,
    [PRICE_CHANGE]                          NUMERIC (21, 4) NULL,
    [PRICE_NEW]                             NUMERIC (18, 2) NULL,
    [NEW_GM_PCT]                            NUMERIC (18, 4) NULL,
    [ind_Margin_Tol]                        INT             NULL,
    [ftn_Cost_Delta]                        VARCHAR (64)    NULL,
    [COST_CHANGE_EFFECTIVE_DATE]            DATE            NULL,
    [TARGET_MARGIN]                         NUMERIC (18, 6) NULL,
    [TM_SOURCE]                             VARCHAR (8000)  NULL,
    [LEVEL_GROSS]                           VARCHAR (8)     NULL,
    [MINAMT]                                MONEY           NULL,
    [DAYSOUT]                               VARCHAR (256)   NOT NULL,
    [PRODUCT_STRUCTURE_FK]                  BIGINT          NULL,
    [ZONE_ENTITY_STRUCTURE_FK]              BIGINT          NOT NULL,
    [PROD_SEGMENT_DATA_NAME_FK]             BIGINT          NULL,
    [PROD_SEGMENT_FK]                       BIGINT          NULL,
    [NOTIFICATION_DATE]                     DATE            NULL,
    [PROD_SEGMENT]                          VARCHAR (514)   NULL,
    [SUBGROUP_DESCRIPTION]                  VARCHAR (256)   NULL,
    [PARITY_PL_PARENT_STRUCTURE_FK]         BIGINT          NULL,
    [PARITY_SIZE_PARENT_STRUCTURE_FK]       BIGINT          NULL,
    [SEGMENTS_SETTINGS_FK_144891]           BIGINT          NULL,
    [SEGMENTS_SETTINGS_FK_PARENT_144893]    BIGINT          NULL,
    [USE_LG_PRICE_POINTS]                   VARCHAR (8)     NULL,
    [SEGMENTS_SETTINGS_FK_144876]           BIGINT          NULL,
    [price_initial]                         NUMERIC (18, 2) NULL,
    [price_rounded]                         NUMERIC (18, 2) NULL,
    [SRP_DIGIT]                             INT             NULL,
    [DIGIT_DN_FK]                           NUMERIC (18, 2) NULL,
    [DIGIT_ADJ]                             NUMERIC (18, 2) NULL,
    [DIGIT_ADJ_DATA_LEVEL]                  VARCHAR (64)    NULL,
    [PRICE_MULTIPLE_NEW]                    INT             NULL,
    [SRP_ADJUSTED]                          MONEY           NULL,
    [GM_ADJUSTED]                           NUMERIC (18, 4) NULL,
    [TM_ADJUSTED]                           NUMERIC (18, 4) NULL,
    [HOLD_CUR_PRICE]                        INT             NULL,
    [REVIEWED]                              INT             NULL,
    [PRICE_CHANGE_EFFECTIVE_DATE]           DATE            NULL,
    [UPDATE_TIMESTAMP]                      DATETIME        NULL,
    [CREATE_TIMESTAMP]                      DATETIME        NOT NULL,
    [UPDATE_USER_PRICE]                     VARCHAR (64)    NULL,
    [UPDATE_USER_SET_TM]                    VARCHAR (64)    NULL,
    [UPDATE_USER_HOLD]                      VARCHAR (64)    NULL,
    [UPDATE_USER_REVIEWED]                  VARCHAR (64)    NULL,
    [PRICESHEET_RETAIL_CHANGES_ID]          BIGINT          IDENTITY (1, 1) NOT NULL,
    [PARITY_DISCOUNT_AMT]                   NUMERIC (18, 4) NULL,
    [PARITY_MULT]                           INT             NULL,
    [PARITY_DISCOUNT_PCT]                   NUMERIC (18, 4) NULL,
    [SUBGROUP_DATA_VALUE_FK]                BIGINT          NULL,
    [IND_COST_CHANGE_ITEM]                  INT             NULL,
    [STAGE_FOR_PROCESSING_FK]               BIGINT          NULL,
    [IND_MARGIN_CHANGES]                    INT             NULL,
    [IND_PARITY]                            INT             NULL,
    [IND_RESCIND]                           INT             NULL,
    [PARITY_ALIAS_PL_PARENT_STRUCTURE_FK]   BIGINT          NULL,
    [INSERT_STEP]                           VARCHAR (64)    NULL,
    [Pricing_Steps]                         VARCHAR (MAX)   NULL,
    [COST_CHANGE_REASON]                    VARCHAR (64)    NULL,
    [PARITY_ALIAS_SIZE_PARENT_STRUCTURE_FK] BIGINT          NULL,
    [SIZE_UOM]                              VARCHAR (16)    NULL,
    [ftn_Breakdown_Parent]                  VARCHAR (64)    NULL,
    [Breakdown_Parent_Product_Structure_FK] BIGINT          NULL,
    [ALIAS_DESCRIPTION]                     VARCHAR (256)   NULL,
    [SUBGROUP_ID]                           BIGINT          NULL,
    [ALIAS_DATA_VALUE_FK]                   BIGINT          NULL,
    [ITEM]                                  VARCHAR (64)    NULL,
    [ITEM_GROUP]                            BIGINT          NULL,
    [item_sort_report]                      INT             NULL,
    [parity_alias_id]                       BIGINT          NULL,
    [comments]                              VARCHAR (256)   NULL,
    [comments_rescind]                      VARCHAR (128)   NULL,
    [PARITY_STATUS]                         VARCHAR (8)     NULL,
    [ITEM_GROUP_NEW]                        BIGINT          NULL,
    [ITEM_SORT_NEW]                         INT             NULL,
    [ALIAS_ALIGN_ID]                        BIGINT          NULL,
    [MULTIPLE_PARITIES]                     VARCHAR (128)   NULL,
    [ind_discontinued]                      VARCHAR (128)   NULL,
    [PARITY_PL_CHILD_ITEM]                  VARCHAR (64)    NULL,
    [ALIAS_ID_CHILD]                        VARCHAR (64)    NULL,
    [PARITY_PL_PARENT_ITEM]                 VARCHAR (64)    NULL,
    [ITEM_SORT]                             BIGINT          NULL,
    [ind_display_as_reg]                    INT             NULL,
    [zone_sort]                             INT             NULL,
    [lg_activation]                         VARCHAR (8)     NULL,
    [parity_type]                           VARCHAR (8)     NULL,
    [ind_parity_status]                     VARCHAR (8)     NULL,
    [parity_parent_product_structure_fk]    BIGINT          NULL,
    [parity_child_product_structure_fk]     BIGINT          NULL,
    [parity_parent_item]                    VARCHAR (64)    NULL,
    [parity_child_item]                     VARCHAR (64)    NULL,
    [pricesheet_basis_values_fk_cur]        BIGINT          NULL,
    [related_item_alias_id]                 BIGINT          NULL,
    [entity_structure_fk]                   BIGINT          NULL,
    [entity_data_name_fk]                   BIGINT          NULL,
    [change_status]                         INT             NULL,
    [previous_lg_date]                      DATE            NULL,
    [unique_cost_record_id]                 VARCHAR (64)    NULL,
    [qualified_status_step]                 VARCHAR (64)    NULL,
    [item_group_alias_item]                 BIGINT          NULL,
    CONSTRAINT [PK_PRICESHEET_RETAIL_CHANGES] PRIMARY KEY CLUSTERED ([PRICESHEET_RETAIL_CHANGES_ID] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON)
);




GO
CREATE NONCLUSTERED INDEX [idx_reg]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ZONE_ENTITY_STRUCTURE_FK] ASC, [PRICE_MGMT_TYPE_CR_FK] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC, [QUALIFIED_STATUS_CR_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC, [PARITY_CHILD] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_reg_parity]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ZONE_ENTITY_STRUCTURE_FK] ASC, [PRICE_MGMT_TYPE_CR_FK] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC, [QUALIFIED_STATUS_CR_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC, [PARITY_PL_PARENT_STRUCTURE_FK] ASC, [PARITY_SIZE_PARENT_STRUCTURE_FK] ASC, [PARITY_CHILD] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_alias]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ZONE_ENTITY_STRUCTURE_FK] ASC, [PRICE_MGMT_TYPE_CR_FK] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC, [QUALIFIED_STATUS_CR_FK] ASC, [ALIAS_ID] ASC, [PARITY_CHILD] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_alias_prod]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ZONE_ENTITY_STRUCTURE_FK] ASC, [PRICE_MGMT_TYPE_CR_FK] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC, [QUALIFIED_STATUS_CR_FK] ASC, [ALIAS_ID] ASC, [PRODUCT_STRUCTURE_FK] ASC, [PARITY_CHILD] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_alias_prod_parity]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ZONE_ENTITY_STRUCTURE_FK] ASC, [PRICE_MGMT_TYPE_CR_FK] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC, [QUALIFIED_STATUS_CR_FK] ASC, [ALIAS_ID] ASC, [PRODUCT_STRUCTURE_FK] ASC, [PARITY_PL_PARENT_STRUCTURE_FK] ASC, [PARITY_SIZE_PARENT_STRUCTURE_FK] ASC, [PARITY_CHILD] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_par01]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC, [ZONE_ENTITY_STRUCTURE_FK] ASC, [PRODUCT_STRUCTURE_FK] ASC, [PARITY_PL_PARENT_STRUCTURE_FK] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_mc01]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([STAGE_FOR_PROCESSING_FK] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_alias_prc_lookup]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([unit_cost_new] ASC, [ZONE_ENTITY_STRUCTURE_FK] ASC, [PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC)
    INCLUDE([ALIAS_ID], [PRODUCT_STRUCTURE_FK]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_pceff_ig_si]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ITEM_GROUP] ASC, [SUBGROUP_ID] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_pm_ic_q_pceff_alias]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_MGMT_TYPE_CR_FK] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC, [QUALIFIED_STATUS_CR_FK] ASC, [PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ALIAS_ID] ASC)
    INCLUDE([PRICE_MGMT_TYPE], [ITEM_CHANGE_TYPE], [QUALIFIED_STATUS], [ITEM_DESCRIPTION], [STORE_PACK], [ftn_Cost_Delta], [ZONE_ENTITY_STRUCTURE_FK], [SUBGROUP_DESCRIPTION], [PARITY_PL_PARENT_STRUCTURE_FK], [SUBGROUP_DATA_VALUE_FK], [IND_MARGIN_CHANGES], [PARITY_ALIAS_PL_PARENT_STRUCTURE_FK], [SIZE_UOM], [ftn_Breakdown_Parent], [SUBGROUP_ID], [ITEM], [ITEM_GROUP]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_status_PCeff]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_MGMT_TYPE_CR_FK] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC, [QUALIFIED_STATUS_CR_FK] ASC, [PRICE_CHANGE_EFFECTIVE_DATE] ASC)
    INCLUDE([ALIAS_ID], [COST_CHANGE], [PRODUCT_STRUCTURE_FK], [PARITY_PL_PARENT_STRUCTURE_FK], [IND_MARGIN_CHANGES]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_PCeff_ICT]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC)
    INCLUDE([PRICE_MGMT_TYPE_CR_FK], [QUALIFIED_STATUS_CR_FK], [ITEM_DESCRIPTION], [ALIAS_ID], [STORE_PACK], [case_cost_cur], [unit_cost_cur], [case_cost_new], [unit_cost_new], [COST_CHANGE], [ftn_Cost_Delta], [PRODUCT_STRUCTURE_FK], [ZONE_ENTITY_STRUCTURE_FK], [PARITY_PL_PARENT_STRUCTURE_FK], [IND_MARGIN_CHANGES], [IND_PARITY], [SIZE_UOM], [ftn_Breakdown_Parent], [SUBGROUP_ID], [ITEM], [ITEM_GROUP]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_PCeff_ICT2]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC)
    INCLUDE([ZONE], [ZONE_NAME], [case_cost_cur], [unit_cost_cur], [PRICE_MULTIPLE_CUR], [PRICE_CUR], [CUR_GM_PCT], [case_cost_new], [unit_cost_new], [COST_CHANGE], [PRICE_CHANGE], [PRICE_NEW], [NEW_GM_PCT], [ind_Margin_Tol], [ftn_Cost_Delta], [TARGET_MARGIN], [TM_SOURCE], [PRODUCT_STRUCTURE_FK], [ZONE_ENTITY_STRUCTURE_FK], [SRP_ADJUSTED], [GM_ADJUSTED], [TM_ADJUSTED], [HOLD_CUR_PRICE], [REVIEWED], [UPDATE_USER_PRICE], [UPDATE_USER_SET_TM], [UPDATE_USER_HOLD], [UPDATE_USER_REVIEWED], [PRICESHEET_RETAIL_CHANGES_ID], [IND_MARGIN_CHANGES], [IND_PARITY], [IND_RESCIND]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_ICT_PCeff_PAfk]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([ITEM_CHANGE_TYPE_CR_FK] ASC, [PRICE_CHANGE_EFFECTIVE_DATE] ASC, [parity_alias_id] ASC)
    INCLUDE([PRICE_MGMT_TYPE_CR_FK], [QUALIFIED_STATUS_CR_FK], [ALIAS_ID], [ftn_Cost_Delta], [PRODUCT_STRUCTURE_FK], [ZONE_ENTITY_STRUCTURE_FK], [SUBGROUP_DESCRIPTION], [PARITY_PL_PARENT_STRUCTURE_FK], [SUBGROUP_DATA_VALUE_FK], [IND_MARGIN_CHANGES], [IND_RESCIND], [ftn_Breakdown_Parent], [SUBGROUP_ID], [ITEM_GROUP]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_PS_PCeff]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRODUCT_STRUCTURE_FK] ASC, [PRICE_CHANGE_EFFECTIVE_DATE] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_PCeff_ict_CCeff]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC, [COST_CHANGE_EFFECTIVE_DATE] ASC)
    INCLUDE([PRICESHEET_BASIS_VALUES_FK], [case_cost_new], [PRODUCT_STRUCTURE_FK], [IND_COST_CHANGE_ITEM], [IND_RESCIND], [ITEM]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_PCeff_ict_incl]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC)
    INCLUDE([PRICE_MGMT_TYPE_CR_FK], [QUALIFIED_STATUS_CR_FK], [ITEM_DESCRIPTION], [ALIAS_ID], [STORE_PACK], [case_cost_cur], [unit_cost_cur], [case_cost_new], [unit_cost_new], [COST_CHANGE], [ftn_Cost_Delta], [PRODUCT_STRUCTURE_FK], [ZONE_ENTITY_STRUCTURE_FK], [PARITY_PL_PARENT_STRUCTURE_FK], [IND_MARGIN_CHANGES], [IND_PARITY], [SIZE_UOM], [ftn_Breakdown_Parent], [ITEM], [ITEM_GROUP_NEW], [ITEM_SORT_NEW], [ALIAS_ALIGN_ID]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_ict_aid_zes_pceff_incl_imc]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([ITEM_CHANGE_TYPE_CR_FK] ASC, [ALIAS_ID] ASC, [ZONE_ENTITY_STRUCTURE_FK] ASC, [PRICE_CHANGE_EFFECTIVE_DATE] ASC)
    INCLUDE([PRICESHEET_RETAIL_CHANGES_ID], [IND_MARGIN_CHANGES]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_ict_aid_zes_pceff_imc_incl_id]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([ITEM_CHANGE_TYPE_CR_FK] ASC, [ALIAS_ID] ASC, [ZONE_ENTITY_STRUCTURE_FK] ASC, [PRICE_CHANGE_EFFECTIVE_DATE] ASC, [IND_MARGIN_CHANGES] ASC)
    INCLUDE([PRICESHEET_RETAIL_CHANGES_ID]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_z_pceff_itm]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([ZONE] ASC, [PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ITEM] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_PCeff_ICT_Incl_Others]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC)
    INCLUDE([QUALIFIED_STATUS_CR_FK], [ALIAS_ID], [PRICE_CUR], [PRICE_NEW], [ZONE_ENTITY_STRUCTURE_FK], [qualified_status_step]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_zes_pceff_cstchg_itm_alias]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([ZONE_ENTITY_STRUCTURE_FK] ASC, [PRICE_CHANGE_EFFECTIVE_DATE] ASC, [IND_COST_CHANGE_ITEM] ASC, [ITEM] ASC, [ALIAS_ID] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_PCeff_dsply_incl_others]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ind_display_as_reg] ASC)
    INCLUDE([ALIAS_ID], [ZONE_ENTITY_STRUCTURE_FK], [ITEM], [related_item_alias_id]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_ict_pceff_par_alias_incl_others]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([ITEM_CHANGE_TYPE_CR_FK] ASC, [PRICE_CHANGE_EFFECTIVE_DATE] ASC, [PARITY_STATUS] ASC, [ALIAS_ID] ASC)
    INCLUDE([QUALIFIED_STATUS_CR_FK], [ZONE_ENTITY_STRUCTURE_FK], [ITEM_GROUP], [ind_display_as_reg]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_pceff_cstchg_incl_als_zes]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [IND_COST_CHANGE_ITEM] ASC)
    INCLUDE([ALIAS_ID], [ZONE_ENTITY_STRUCTURE_FK]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_pceff_als_incl_ig_dsply]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ALIAS_ID] ASC)
    INCLUDE([ITEM_GROUP], [ind_display_as_reg]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_PCeff_incl_others]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC)
    INCLUDE([unit_cost_cur], [unit_cost_new], [COST_CHANGE_EFFECTIVE_DATE], [DAYSOUT], [PRODUCT_STRUCTURE_FK]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_prc_PCeff_imc_incl_prcid]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [IND_MARGIN_CHANGES] ASC)
    INCLUDE([PRICESHEET_RETAIL_CHANGES_ID]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_PCeff_incl_others2]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC)
    INCLUDE([unit_cost_cur], [unit_cost_new], [COST_CHANGE_EFFECTIVE_DATE], [DAYSOUT], [PRODUCT_STRUCTURE_FK]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_zes_it_incl_others]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([ZONE_ENTITY_STRUCTURE_FK] ASC, [ITEM] ASC)
    INCLUDE([QUALIFIED_STATUS_CR_FK], [ZONE], [ALIAS_ID], [case_cost_cur], [unit_cost_cur], [PRICE_CUR], [case_cost_new], [unit_cost_new], [PRICE_NEW], [LEVEL_GROSS], [COST_CHANGE_REASON], [ITEM_GROUP], [parity_alias_id], [ind_display_as_reg], [parity_child_item]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_PCeff_incl_others3]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC)
    INCLUDE([case_cost_cur], [unit_cost_cur], [PRICE_CUR], [case_cost_new], [unit_cost_new], [PRICE_NEW], [LEVEL_GROSS]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_PCeff_cc_als_incl_ces_it]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC, [IND_COST_CHANGE_ITEM] ASC, [ALIAS_ID] ASC)
    INCLUDE([ZONE_ENTITY_STRUCTURE_FK], [ITEM]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_PCeff_incl_csts_do_ps_ucr]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC)
    INCLUDE([unit_cost_cur], [unit_cost_new], [COST_CHANGE_EFFECTIVE_DATE], [DAYSOUT], [PRODUCT_STRUCTURE_FK], [unique_cost_record_id]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_ucr]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([unique_cost_record_id] ASC) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);




GO
CREATE NONCLUSTERED INDEX [idx_PCeff_incl]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([PRICE_CHANGE_EFFECTIVE_DATE] ASC)
    INCLUDE([PRICE_MGMT_TYPE_CR_FK], [ITEM_CHANGE_TYPE_CR_FK], [QUALIFIED_STATUS_CR_FK], [ALIAS_ID], [PRODUCT_STRUCTURE_FK]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);


GO
CREATE NONCLUSTERED INDEX [idx_aid_pceff_ict_itm_incl]
    ON [marginmgr].[PRICESHEET_RETAIL_CHANGES]([ALIAS_ID] ASC, [PRICE_CHANGE_EFFECTIVE_DATE] ASC, [ITEM_CHANGE_TYPE_CR_FK] ASC, [ITEM] ASC)
    INCLUDE([ZONE], [unit_cost_cur], [PRICE_CUR], [case_cost_new], [unit_cost_new], [PRICE_NEW], [COST_CHANGE_EFFECTIVE_DATE], [TARGET_MARGIN], [PRODUCT_STRUCTURE_FK], [price_initial], [price_rounded], [DIGIT_ADJ], [IND_COST_CHANGE_ITEM], [Pricing_Steps], [COST_CHANGE_REASON], [parity_alias_id], [zone_sort], [parity_parent_item], [parity_child_item], [related_item_alias_id], [entity_structure_fk], [change_status]) WITH (FILLFACTOR = 80, STATISTICS_NORECOMPUTE = ON);

