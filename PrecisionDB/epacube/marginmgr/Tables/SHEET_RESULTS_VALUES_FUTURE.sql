﻿CREATE TABLE [marginmgr].[SHEET_RESULTS_VALUES_FUTURE] (
    [SHEET_RESULTS_VALUES_FUTURE_ID] BIGINT          IDENTITY (1000, 1) NOT NULL,
    [DATA_NAME_FK]                   INT             NULL,
    [PRODUCT_STRUCTURE_FK]           BIGINT          NULL,
    [ORG_ENTITY_STRUCTURE_FK]        BIGINT          NULL,
    [ENTITY_CLASS_CR_FK]             INT             NULL,
    [ENTITY_DATA_NAME_FK]            INT             NULL,
    [ENTITY_STRUCTURE_FK]            BIGINT          NULL,
    [EFFECTIVE_DATE]                 DATE            NULL,
    [END_DATE]                       DATE            NULL,
    [NET_VALUE1]                     NUMERIC (18, 6) NULL,
    [NET_VALUE2]                     NUMERIC (18, 6) NULL,
    [NET_VALUE3]                     NUMERIC (18, 6) NULL,
    [NET_VALUE4]                     NUMERIC (18, 6) NULL,
    [NET_VALUE5]                     NUMERIC (18, 6) NULL,
    [NET_VALUE6]                     NUMERIC (18, 6) NULL,
    [NET_VALUE7]                     NUMERIC (18, 6) NULL,
    [NET_VALUE8]                     NUMERIC (18, 6) NULL,
    [NET_VALUE9]                     NUMERIC (18, 6) NULL,
    [NET_VALUE10]                    NUMERIC (18, 6) NULL,
    [PREV_VALUE1]                    NUMERIC (18, 6) NULL,
    [PREV_VALUE2]                    NUMERIC (18, 6) NULL,
    [PREV_VALUE3]                    NUMERIC (18, 6) NULL,
    [PREV_VALUE4]                    NUMERIC (18, 6) NULL,
    [PREV_VALUE5]                    NUMERIC (18, 6) NULL,
    [PREV_VALUE6]                    NUMERIC (18, 6) NULL,
    [PREV_VALUE7]                    NUMERIC (18, 6) NULL,
    [PREV_VALUE8]                    NUMERIC (18, 6) NULL,
    [PREV_VALUE9]                    NUMERIC (18, 6) NULL,
    [PREV_VALUE10]                   NUMERIC (18, 6) NULL,
    [PERCENT_CHANGE1]                NUMERIC (18, 6) NULL,
    [PERCENT_CHANGE2]                NUMERIC (18, 6) NULL,
    [PERCENT_CHANGE3]                NUMERIC (18, 6) NULL,
    [PERCENT_CHANGE4]                NUMERIC (18, 6) NULL,
    [PERCENT_CHANGE5]                NUMERIC (18, 6) NULL,
    [PERCENT_CHANGE6]                NUMERIC (18, 6) NULL,
    [PERCENT_CHANGE7]                NUMERIC (18, 6) NULL,
    [PERCENT_CHANGE8]                NUMERIC (18, 6) NULL,
    [PERCENT_CHANGE9]                NUMERIC (18, 6) NULL,
    [PERCENT_CHANGE10]               NUMERIC (18, 6) NULL,
    [VALUE_UOM_CODE_FK]              INT             NULL,
    [ASSOC_UOM_CODE_FK]              INT             NULL,
    [UOM_CONVERSION_FACTOR]          NUMERIC (18, 6) NULL,
    [VALUE_EFFECTIVE_DATE]           DATETIME        NULL,
    [PREV_PRODUCT_UOM_CLASS_FK]      BIGINT          NULL,
    [PREV_EFFECTIVE_DATE]            DATETIME        NULL,
    [PREV_END_DATE]                  DATETIME        NULL,
    [PREV_UPDATE_TIMESTAMP]          DATETIME        NULL,
    [RECORD_STATUS_CR_FK]            INT             CONSTRAINT [DF_SHEET_RES_Val_Fut_REC_STAT] DEFAULT ((1)) NULL,
    [CREATE_TIMESTAMP]               DATETIME        CONSTRAINT [DF_SHEET_RES_Val_Fut_CREAT] DEFAULT (getdate()) NULL,
    [UPDATE_TIMESTAMP]               DATETIME        CONSTRAINT [DF_SHEET_RES_Val_Fut_UPDAT] DEFAULT (getdate()) NULL,
    [UPDATE_LOG_FK]                  BIGINT          NULL,
    [UPDATE_USER]                    VARCHAR (50)    NULL,
    [PRICESHEET_NAME]                VARCHAR (50)    NULL,
    [EVENT_DATA_EVENT_ID]            BIGINT          NULL,
    [INITIAL_EVENT_EFFECTIVE_DATE]   DATE            NULL,
    [CALC_JOB_FK]                    BIGINT          NULL
);


GO
CREATE CLUSTERED INDEX [IDX_PS_OES_EFFDATE]
    ON [marginmgr].[SHEET_RESULTS_VALUES_FUTURE]([PRODUCT_STRUCTURE_FK] ASC, [ORG_ENTITY_STRUCTURE_FK] ASC, [DATA_NAME_FK] ASC);

