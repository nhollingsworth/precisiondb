﻿
-- Copyright 2009
--
-- Function created by Kathi Scott
--
--
-- Purpose: Purpose of this Function is to retrieve Analysis Margin Results based on job
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/03/2009  Initial SQL Version
--

CREATE FUNCTION [marginmgr].[Get_Analysis_Margin_Results]

( @JobFK BIGINT, @Scenario_Seq SMALLINT,
  @ProdFilterDN INT ,  @ProdFilterValue VARCHAR(64),
  @WhseFilterDN INT , @WhseFilterValue VARCHAR(64) ,  
  @CustFilterDN INT , @CustFilterValue VARCHAR(64) ,    
  @SuplFilterDN INT , @SuplFilterValue VARCHAR(64) ,      
  @SaleFilterValue VARCHAR(64),   @TerrFilterValue VARCHAR(64) , 
  @PrcMgrFilterValue VARCHAR(64),  @BuyFilterValue VARCHAR(64) ,  
  @CustSegFilterValue VARCHAR(64), @ProdSeqFilterValue VARCHAR(64), @ProdCustFilterValue VARCHAR(64) ,  
  @ProdSuplFilterValue VARCHAR(64), @StrgFilterValue VARCHAR(64), @RuleFilterValue VARCHAR(64), @MatrixFilterValue VARCHAR(64),   
  ---- Analysis Filters
  @StockItemsInd SMALLINT , @NonStockItemsInd SMALLINT ,  
  @PrcOvrInd SMALLINT,      @RbtCBInd SMALLINT, 
  @LastSellDate DATETIME , 
  @TotSalesHitsMin INT ,  @TotSalesHitsMax INT ,
  @TotSalesQtyMin INT ,  @TotSalesQtyMax INT ,
  @TotSalesDollarsMin INT ,  @TotSalesDollarsMax INT ,
  @Margin_Net_PctMin INT,    @Margin_Net_PctMax  INT,
  @Condition INT, @CurrencyFK INT, @UserLogin VARCHAR(64)
   )
RETURNS @MarginResults TABLE 
(
    -- Columns returned by the function
    Job_fk					BIGINT 
   ,Scenario				VARCHAR (32)
   ,Prod_ID_Value			VARCHAR(32)
   ,Whse_ID_Value			VARCHAR(32)
   ,Cust_ID_Value			VARCHAR(32) 
------   
   ,Margin_Amt				NUMERIC (8,2)
   ,Margin_Pct				NUMERIC (8,2)
   ,Margin_Net_Amt			NUMERIC (8,2)
   ,Margin_Net_Pct			NUMERIC (8,2)
   ,Margin_Net_Net_Amt		NUMERIC (8,2)
   ,Margin_Net_Net_Pct		NUMERIC (8,2)
------   
   ,Sell_Price_Net_Amt		NUMERIC (8,2)
   ,Rebated_Cost_CB_Amt		NUMERIC (8,2)
   ,Rebated_Cost_Net_Amt	NUMERIC (8,2)
------
   ,Description				VARCHAR (32)
   ,Supl_ID_Value			VARCHAR(32)    
   ,UOM_Code				VARCHAR(8)       
------
   ,Margin_Cost_Basis		VARCHAR (32)   
   ,Margin_Cost_Basis_Amt	NUMERIC (8,2)
   ,Rebate_CB_Amt			NUMERIC (8,2)
   ,Rebate_Buy_Amt			NUMERIC (8,2)      
   ,Rebate_Cust_Amt			NUMERIC (8,2)    
   ,Sell_Price_Cust_Amt		NUMERIC (8,2)           
   ,Sell_Price_Disc_Amt		NUMERIC (8,2)           
   ,Sell_Price_Optimized_Amt NUMERIC (8,2)           
   ,Sell_Price_Floor_Amt	NUMERIC (8,2)              
   ,Sell_Price_Ceiling_Amt  NUMERIC (8,2)   
------  
   ,Sale_ID_Value			VARCHAR(64)
   ,Terr_ID_Value			VARCHAR(64)
   ,PrcMgr_ID_Value			VARCHAR(64)
   ,Buy_ID_Value			VARCHAR(64)  
   ,CustSeg_ID_Value		VARCHAR(64)
   ,ProdSeq_ID_Value		VARCHAR(64)
   ,ProdCust_ID_Value		VARCHAR(64)
   ,ProdSupl_ID_Value		VARCHAR(64)
   ,Strg_ID_Value			VARCHAR(64)
   ,Rule_ID_Value			VARCHAR(64) 
   ,Matrix_ID_Value			VARCHAR(64)     
------ 
   ,Condition				INT
   ,Effective_Date			DATETIME     
   ,Scenario_fk				bigint     
)
AS
-- Returns the product_structure_fk, Org_entity_Structure, and Entity_structure_fk
BEGIN

IF @JobFK IS NULL
SET @JobFK = 99          ---- leave for now but not sure how I want to handle

 INSERT into @MarginResults (
    Job_fk			
   ,Scenario		
   ,Prod_ID_Value	
   ,Whse_ID_Value	
   ,Cust_ID_Value	
------   
   ,Margin_Amt		
   ,Margin_Pct		
   ,Margin_Net_Amt	
   ,Margin_Net_Pct	
   ,Margin_Net_Net_Amt	
   ,Margin_Net_Net_Pct	
------   
   ,Sell_Price_Net_Amt	
   ,Rebated_Cost_CB_Amt	
   ,Rebated_Cost_Net_Amt   
------
--   ,Description
   ,Supl_ID_Value	
   ,UOM_Code		
------   
   ,Margin_Cost_Basis	
   ,Margin_Cost_Basis_Amt	
   ,Rebate_CB_Amt			
   ,Rebate_Buy_Amt			
   ,Rebate_Cust_Amt			
   ,Sell_Price_Cust_Amt		
   ,Sell_Price_Disc_Amt		
   ,Sell_Price_Optimized_Amt
   ,Sell_Price_Floor_Amt	
   ,Sell_Price_Ceiling_Amt  
-----
   ,Sale_ID_Value		
   ,Terr_ID_Value	
   ,PrcMgr_ID_Value	
   ,Buy_ID_Value		
   ,CustSeg_ID_Value	
   ,ProdSeq_ID_Value	
   ,ProdCust_ID_Value	
   ,ProdSupl_ID_Value	
   ,Strg_ID_Value		
   ,Rule_ID_Value		
   ,Matrix_ID_Value	
-----
   ,Condition				
   ,Effective_Date			
   ,Scenario_fk		
)		
------------------------------------------------------
SELECT 
    Job_fk			
   ,Scenario_Seq	
   ,( SELECT epacube.getProductIdent ( Product_Structure_FK ) )  AS Prod_ID_Value	
   ,( SELECT epacube.getEntityIdent ( Org_Entity_Structure_FK ) ) AS Whse_ID_Value	
   ,( SELECT epacube.getEntityIdent ( Cust_Entity_Structure_FK ) ) AS Cust_ID_Value	
------   
   ,CAST ( Margin_Amt AS NUMERIC (8,2) )
   ,CAST ( Margin_Pct AS NUMERIC (8,2) )
   ,CAST ( Margin_Net_Amt AS NUMERIC (8,2) )
   ,CAST ( Margin_Net_Pct AS NUMERIC (8,2) )
   ,CAST ( Margin_Net_Net_Amt AS NUMERIC (8,2) )
   ,CAST ( Margin_Net_Net_Pct AS NUMERIC (8,2) )
------   
   ,CAST ( Sell_Price_Net_Amt AS NUMERIC (8,2) )
   ,CAST ( Rebated_Cost_CB_Amt AS NUMERIC (8,2) )
   ,CAST ( Rebated_Cost_Net_Amt   AS NUMERIC (8,2) )
------
--   ,( SELECT search6 FROM epacube.epacube_search WHERE epacube_id = Product_Structure_FK ) AS Description
   ,( SELECT epacube.getEntityIdent ( Supl_Entity_Structure_FK ) ) AS Supl_ID_Value	
   ,( SELECT uom_code FROM epacube.uom_code WHERE uom_code_id = Unit_UOM_Code_FK ) AS uom_code
------   
   ,( SELECT short_name FROM epacube.data_name WHERE data_name_id = Margin_Cost_Basis_DN_FK ) AS margin_cost_basis 
   ,CAST ( Margin_Cost_Basis_Amt AS NUMERIC (8,2) )
   ,CAST ( Rebate_CB_Amt AS NUMERIC (8,2) )
   ,CAST ( Rebate_Buy_Amt AS NUMERIC (8,2) )
   ,CAST ( Rebate_Cust_Amt AS NUMERIC (8,2) )
   ,CAST ( Sell_Price_Cust_Amt AS NUMERIC (8,2) )
   ,CAST ( Sell_Price_Disc_Amt AS NUMERIC (8,2) )
   ,CAST ( Sell_Price_Optimized_Amt AS NUMERIC (8,2) )
   ,CAST ( Sell_Price_Floor_Amt AS NUMERIC (8,2) )
   ,CAST ( Sell_Price_Ceiling_Amt AS NUMERIC (8,2) )
----- 
   ,( SELECT epacube.getEntityIdent ( Sales_Entity_Structure_FK ) ) AS Sales_ID_Value	
   ,( SELECT epacube.getEntityIdent ( Terr_Entity_Structure_FK ) ) AS Terr_ID_Value	
   ,( SELECT epacube.getEntityIdent ( PrcMgr_Entity_Structure_FK ) ) AS PrcMgr_ID_Value	
   ,( SELECT epacube.getEntityIdent ( Buyer_Entity_Structure_FK ) ) AS Buy_ID_Value	
   ,NULL AS CustSeg_ID_Value	
   ,NULL AS ProdSeq_ID_Value	
   ,NULL AS ProdCust_ID_Value	
   ,NULL AS ProdSupl_ID_Value	
   ,NULL AS Strg_ID_Value		
   ,NULL AS Rule_ID_Value		
   ,NULL AS Matrix_ID_Value	
----- 
   ,97 AS Condition				
   ,Effective_Date			
   ,Scenario_fk				
----------
FROM marginmgr.V_MARGIN_RESULTS_ALL VMRA
WHERE 1 =1 
AND   VMRA.job_fk = @JobFK
AND   (   @Scenario_Seq IS NULL
      OR  @Scenario_Seq = VMRA.SCENARIO_SEQ )
--------
----   PRODUCT FILTER
AND   (   @ProdFilterDN IS NULL
      OR  VMRA.PRODUCT_STRUCTURE_FK IN (  SELECT VCPD.PRODUCT_STRUCTURE_FK
                                        FROM epacube.V_CURRENT_PRODUCT_DATA VCPD
                                        WHERE VCPD.DATA_NAME_FK = @ProdFilterDN
                                        AND   VCPD.CURRENT_DATA = @ProdFilterValue )
      )
----   WHSE FILTER
AND   (   @WhseFilterDN IS NULL
      OR  VMRA.ORG_ENTITY_STRUCTURE_FK IN (  SELECT EI.ENTITY_STRUCTURE_FK
                                           FROM epacube.ENTITY_IDENTIFICATION EI
                                           WHERE EI.DATA_NAME_FK = @WhseFilterDN
                                           AND   EI.VALUE = @WhseFilterValue )
      )
----   CUST FILTER
AND   (   @CustFilterDN IS NULL
      OR  VMRA.CUST_ENTITY_STRUCTURE_FK IN (  SELECT VCED.ENTITY_STRUCTURE_FK
                                            FROM epacube.V_CURRENT_PRODUCT_DATA VCED
                                            WHERE VCED.DATA_NAME_FK = @CustFilterDN
                                            AND   VCED.CURRENT_DATA = @CustFilterValue )
      )
--------


    
    RETURN
END;
