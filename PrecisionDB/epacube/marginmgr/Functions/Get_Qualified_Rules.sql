﻿

-- Copyright 2009
--
-- Function created by Kathi Scott
--
--
-- Purpose: Purpose of this Function is to retrieve Qualified Rules for a product, org, cust, supl
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        10/23/2009  Initial SQL Version

CREATE FUNCTION [marginmgr].[Get_Qualified_Rules]
( @RULE_TYPE_CR_FK int = NULL, 
  @RESULT_DATA_NAME_FK int = NULL, 
  @PRODUCT_STRUCTURE_FK bigint = NULL, 
  @ORG_ENTITY_STRUCTURE_FK bigint = NULL, 
  @CUST_ENTITY_STRUCTURE_FK bigint = NULL, 
  @SUPL_ENTITY_STRUCTURE_FK bigint = NULL
   )
RETURNS @QualifiedRules TABLE 
(
    -- Columns returned by the function
    Rules_Fk BIGINT 
)
AS
-- Returns the product_structure_fk, Org_entity_Structure, and Entity_structure_fk
--	 for all products updated since the last export
BEGIN


 INSERT into @QualifiedRules (
    Rules_fk
  )
------------------------------------------------------
SELECT R.RULES_ID
FROM SYNCHRONIZER.RULES_STRUCTURE RS WITH (NOLOCK)
INNER JOIN SYNCHRONIZER.RULES R WITH (NOLOCK)
 ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID )
INNER JOIN SYNCHRONIZER.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( R.RULES_ID = RFS.RULES_FK )
WHERE 1 = 1
AND  R.RECORD_STATUS_CR_FK = 1 
AND (   @RULE_TYPE_CR_FK IS NULL
    OR  RS.RULE_TYPE_CR_FK = @RULE_TYPE_CR_FK )
AND (   @RESULT_DATA_NAME_FK IS NULL
    OR  R.RESULT_DATA_NAME_FK = @RESULT_DATA_NAME_FK )
----- FILTERS
AND (   @PRODUCT_STRUCTURE_FK  IS NULL
    OR  (  RFS.PROD_FILTER_FK IS NULL
		OR RFS.PROD_FILTER_FK IN (
                   SELECT RULES_FILTER_FK
                   FROM synchronizer.V_RULE_FILTERS_PROD VRFP
				   WHERE PRODUCT_STRUCTURE_FK = @PRODUCT_STRUCTURE_FK   ) ) )
AND (   @ORG_ENTITY_STRUCTURE_FK  IS NULL
    OR  (  RFS.ORG_FILTER_FK IS NULL
		OR RFS.ORG_FILTER_FK IN (
                   SELECT RULES_FILTER_FK
                   FROM synchronizer.V_RULE_FILTERS_ENTITY VRFO
				   WHERE ENTITY_STRUCTURE_FK = @ORG_ENTITY_STRUCTURE_FK   ) ) )
AND (   @CUST_ENTITY_STRUCTURE_FK  IS NULL
    OR  (  RFS.CUST_FILTER_FK IS NULL
		OR RFS.CUST_FILTER_FK IN (
                   SELECT RULES_FILTER_FK
                   FROM synchronizer.V_RULE_FILTERS_ENTITY VRFC
				   WHERE ENTITY_STRUCTURE_FK = @CUST_ENTITY_STRUCTURE_FK   ) ) )
AND (   @SUPL_ENTITY_STRUCTURE_FK  IS NULL
    OR  (  RFS.SUPL_FILTER_FK IS NULL
		OR RFS.SUPL_FILTER_FK IN (
                   SELECT RULES_FILTER_FK
                   FROM synchronizer.V_RULE_FILTERS_ENTITY VRFS
				   WHERE ENTITY_STRUCTURE_FK = @SUPL_ENTITY_STRUCTURE_FK   ) ) )

    
    RETURN
END;

