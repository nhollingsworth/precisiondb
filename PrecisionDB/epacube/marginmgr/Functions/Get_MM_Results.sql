﻿

-- Copyright 2009
--
-- Function created by Kathi Scott
--
--
-- Purpose: Purpose of this Function is to retrieve Analysis Results based on Focus and Filter
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/03/2009  Initial SQL Version
--

CREATE FUNCTION [marginmgr].[Get_MM_Results]
( 
  @PrdFocusDN INT, @WhseFocusDN INT, @CustFocusDN INT, @SaleFocusDN INT,
  @PrdFilterDN INT, @PrdFilterValue VARCHAR(256),
  @WhseFilterDN INT, @WhseFilterValue VARCHAR(256),  
  @CustFilterDN INT, @CustFilterValue VARCHAR(256),    
  @SaleFilterDN INT, @SaleFilterValue VARCHAR(256),      
  @StockItems SMALLINT, @NonStockItems SMALLINT,
  @MtxInd SMALLINT, @OvrInd SMALLINT, @SpaInd SMALLINT,
  @RbtSpaInd SMALLINT, @RbtBuyInd SMALLINT, @RbtSellInd SMALLINT, 
  @OvrTranInd SMALLINT, @LastSellDate_Begin DATETIME, @LastSellDate_End DATETIME,
  @SalesTransNum_Begin INT, @SalesTransNum_End INT
  
   )
RETURNS @MMResults TABLE 
(
    -- Columns returned by the function
    Prd_Focus_DN_FK BIGINT NOT NULL
   ,Prd_Focus_Name VARCHAR(32)
   ,Prd_Focus_Value VARCHAR(32)
   ,Whse_Focus_DN_FK BIGINT NOT NULL
   ,Whse_Focus_Name VARCHAR(32)
   ,Whse_Focus_Value VARCHAR(32)
   ,Cust_Focus_DN_FK BIGINT NOT NULL
   ,Cust_Focus_Name VARCHAR(32)
   ,Cust_Focus_Value VARCHAR(32)   
   ,Margin_Min NUMERIC (8,2)
   ,Margin_Max NUMERIC (8,2)     
   ,Margin_Delta  NUMERIC (9,2)
   ,Margin_Pct_Min NUMERIC (9,2)
   ,Margin_Pct_Max NUMERIC (9,2)
   ,Margin_Pct_Delta  NUMERIC (9,2)      
   ,Price_Min NUMERIC (8,2)
   ,Price_Max NUMERIC (8,2)
   ,Price_Delta NUMERIC (9,2)
   ,COGS_Min NUMERIC (8,2)
   ,COGS_Max NUMERIC (8,2)
   ,COGS_Delta NUMERIC (9,2)   
   ,Repl_Cost_Min NUMERIC (8,2)
   ,Repl_Cost_Max NUMERIC (8,2)   
   ,Margin_Cost_Min NUMERIC (8,2)
   ,Margin_Cost_Max NUMERIC (8,2)      
   ,Rebate_Amt_Spa_Min NUMERIC (8,2)
   ,Rebate_Amt_Spa_Max NUMERIC (8,2)
   ,Rebate_Amt_Buy_Min NUMERIC (8,2)
   ,Rebate_Amt_Buy_Max NUMERIC (8,2)
   ,Rebate_Amt_Sell_Min NUMERIC (8,2)
   ,Rebate_Amt_Sell_Max NUMERIC (8,2)   
   ,Sales_Qty_Min NUMERIC (8,2)
   ,Sales_Qty_Max NUMERIC (8,2)
   ,Prd_Cnt INT 
   ,Whse_Cnt INT
   ,Cust_Cnt INT   
   ,Last_Sell_Date DATETIME 
)
AS
-- Returns the product_structure_fk, Org_entity_Structure, and Entity_structure_fk
--	 for all products updated since the last export
BEGIN

IF   @PrdFocusDN IS NULL
 SET   @PrdFocusDN = 149099   --- all products

IF   @WhseFocusDN IS NULL
 SET   @WhseFocusDN = 141099   --- all WHSES

IF   @CustFocusDN IS NULL
 SET   @CustFocusDN = 144099   --- all CUSTOMERS


 INSERT into @MMResults (
    Prd_Focus_DN_FK 
   ,Prd_Focus_Name 
   ,Prd_Focus_Value 
   ,Whse_Focus_DN_FK 
   ,Whse_Focus_Name 
   ,Whse_Focus_Value 
   ,Cust_Focus_DN_FK 
   ,Cust_Focus_Name 
   ,Cust_Focus_Value    
   ,Margin_Min 
   ,Margin_Max 
   ,Margin_Delta
   ,Margin_Pct_Min
   ,Margin_Pct_Max 
   ,Margin_Pct_Delta
   ,Price_Min
   ,Price_Max
   ,Price_Delta
   ,COGS_Min 
   ,COGS_Max 
   ,COGS_Delta
   ,Repl_Cost_Min 
   ,Repl_Cost_Max    
   ,Margin_Cost_Min 
   ,Margin_Cost_Max       
   ,Rebate_Amt_Spa_Min 
   ,Rebate_Amt_Spa_Max
   ,Rebate_Amt_Buy_Min 
   ,Rebate_Amt_Buy_Max 
   ,Rebate_Amt_Sell_Min 
   ,Rebate_Amt_Sell_Max   
   ,Sales_Qty_Min
   ,Sales_Qty_Max
   ,Prd_Cnt 
   ,Whse_Cnt
   ,Cust_Cnt   
   ,Last_Sell_Date
  )
------------------------------------------------------
SELECT
	 B.PRD_FOCUS_DN_FK
	,B.PRD_FOCUS_NAME
	,B.PRD_FOCUS_VALUE
	,B.WHSE_FOCUS_DN_FK
	,B.WHSE_FOCUS_NAME
	,B.WHSE_FOCUS_VALUE
	,B.CUST_FOCUS_DN_FK
	,B.CUST_FOCUS_NAME
	,B.CUST_FOCUS_VALUE	
    ,B.Margin_Min 
    ,CASE WHEN B.Margin_Max = B.Margin_Min 
     THEN NULL
     ELSE B.Margin_Max
     END 
    ,CASE WHEN B.Margin_Min  = 0
     THEN NULL 
     ELSE ( B.Margin_Max - B.Margin_Min ) / B.Margin_Min                 
     END 
    ,B.Margin_Pct_Min
    ,CASE WHEN B.Margin_Pct_Max = B.Margin_Pct_Min 
     THEN NULL
     ELSE B.Margin_Pct_Max
     END 
    ,CASE WHEN B.Margin_Pct_Min  = 0
     THEN NULL 
     ELSE ( B.Margin_Pct_Max - B.Margin_Pct_Min ) / B.Margin_Pct_Min                 
     END      
    ,B.Price_Min
    ,CASE WHEN B.Price_Max = B.Price_Min 
     THEN NULL
     ELSE B.Price_Max
     END 
    ,CASE WHEN B.Price_Min = 0
     THEN 0 
     ELSE ( B.Price_Max - B.Price_Min ) / B.Price_Min                      
     END 
    ,B.COGS_Min 
    ,CASE WHEN B.COGS_Max = B.COGS_Min 
     THEN NULL
     ELSE B.COGS_Max
     END 
    ,CASE WHEN B.COGS_Min = 0
     THEN 0 
     ELSE ( B.COGS_Max - B.COGS_Min ) / B.COGS_Min                      
     END      
    ,B.Repl_Cost_Min 
    ,CASE WHEN B.Repl_Cost_Max = B.Repl_Cost_Min 
     THEN NULL
     ELSE B.Repl_Cost_Max
     END      
    ,B.Margin_Cost_Min 
    ,CASE WHEN B.Margin_Cost_Max = B.Margin_Cost_Min 
     THEN NULL
     ELSE B.Margin_Cost_Max
     END           
    ,B.Rebate_Amt_Spa_Min 
    ,CASE WHEN B.Rebate_Amt_Spa_Max = B.Rebate_Amt_Spa_Min 
     THEN NULL
     ELSE B.Rebate_Amt_Spa_Max
     END 
    ,B.Rebate_Amt_Buy_Min 
    ,CASE WHEN B.Rebate_Amt_Buy_Max = B.Rebate_Amt_Buy_Min 
     THEN NULL
     ELSE B.Rebate_Amt_Buy_Max
     END 
    ,B.Rebate_Amt_Sell_Min 
    ,CASE WHEN B.Rebate_Amt_Sell_Max = B.Rebate_Amt_Sell_Min 
     THEN NULL
     ELSE B.Rebate_Amt_Sell_Max
     END      
   ,B.Sales_Qty_Min
   ,CASE WHEN B.Sales_Qty_Max = B.Sales_Qty_Min
    THEN NULL 
    ELSE B.Sales_Qty_Max
    END      
   ,B.Prd_Cnt 
   ,B.Whse_Cnt 
   ,B.Cust_Cnt      
   ,B.Last_Sell_Date
----------------------------------
FROM (
SELECT
     @PrdFocusDN AS PRD_FOCUS_DN_FK
	,A.PRD_FOCUS_NAME
	,A.PRD_FOCUS_VALUE     
	,@WhseFocusDN AS WHSE_FOCUS_DN_FK
	,A.WHSE_FOCUS_NAME
	,A.WHSE_FOCUS_VALUE     
	,@CustFocusDN AS CUST_FOCUS_DN_FK
	,A.CUST_FOCUS_NAME
	,A.CUST_FOCUS_VALUE     	
	,Cast(min(A.Margin) as Numeric(8,2)) AS margin_min
	,Cast(max(A.Margin) as Numeric(8,2)) AS margin_max
	,Cast(min(A.Margin_Pct) as Numeric(8,2)) AS margin_pct_min	
	,Cast(max(A.Margin_Pct) as Numeric(8,2)) AS margin_pct_max
	,Cast(min(A.Price) as Numeric(8,2)) AS price_min
	,Cast(max(A.Price) as Numeric(8,2)) AS price_max
	,Cast(min(A.COGS) as Numeric(8,2)) AS COGS_min
	,Cast(max(A.COGS) as Numeric(8,2)) AS COGS_max
	,Cast(min(A.Repl_Cost) as Numeric(8,2)) AS Repl_Cost_min
	,Cast(max(A.Repl_Cost) as Numeric(8,2)) AS Repl_Cost_max
	,Cast(min(A.Margin_Cost) as Numeric(8,2)) AS Margin_Cost_min
	,Cast(max(A.Margin_Cost) as Numeric(8,2)) AS Margin_Cost_max	
	,Cast(min(A.Rebate_Amt_Spa) as Numeric(8,2)) AS Rebate_Amt_Spa_Min	
	,Cast(max(A.Rebate_Amt_Spa) as Numeric(8,2)) AS Rebate_Amt_Spa_Max
	,Cast(min(A.Rebate_Amt_Buy) as Numeric(8,2)) AS Rebate_Amt_Buy_Min	
	,Cast(max(A.Rebate_Amt_Buy) as Numeric(8,2)) AS Rebate_Amt_Buy_Max
	,Cast(min(A.Rebate_Amt_Sell) as Numeric(8,2)) AS Rebate_Amt_Sell_Min	
	,Cast(max(A.Rebate_Amt_Sell) as Numeric(8,2)) AS Rebate_Amt_Sell_Max	
	,MIN ( a.sales_qty ) AS sales_qty_min		
	,MAX ( a.sales_qty ) AS sales_qty_max
	,COUNT  ( DISTINCT a.product_structure_fk )     AS Prd_Cnt 
	,COUNT  ( DISTINCT a.org_entity_structure_fk )  AS Whse_Cnt
	,COUNT  ( DISTINCT a.cust_entity_structure_fk ) AS Cust_Cnt	
	,MAX ( last_sell_date ) AS last_sell_date
FROM (
---------------------------
SELECT 
       MMR.PRODUCT_STRUCTURE_FK
      ,MMR.ORG_ENTITY_STRUCTURE_FK
      ,MMR.CUST_ENTITY_STRUCTURE_FK
	  ,dnprd.SHORT_NAME AS PRD_FOCUS_NAME
	  ,CASE @PrdFocusDN
	   WHEN 149099
	   THEN NULL
	   ELSE ( SELECT vcpd.current_data FROM epacube.V_CURRENT_PRODUCT_DATA vcpd
			     WHERE vcpd.data_name_fk = @PrdFocusDN
			     AND   vcpd.product_structure_fk = MMR.product_structure_fk
				 AND   (   dsprd.org_ind IS NULL
						OR vcpd.org_entity_structure_fk = MMR.org_entity_structure_fk ) ) 
		END PRD_FOCUS_VALUE
	  ,dnwhse.SHORT_NAME AS WHSE_FOCUS_NAME
	  ,CASE @WhseFocusDN
	   WHEN 141099
	   THEN NULL
	   ELSE ( SELECT ei.value FROM epacube.entity_identification ei
				WHERE  ei.data_name_fk = @WhseFocusDN
				AND    ei.entity_structure_fk = MMR.org_entity_structure_fk )  
		END WHSE_FOCUS_VALUE
	  ,dncust.SHORT_NAME AS CUST_FOCUS_NAME
	  ,CASE @CustFocusDN
	   WHEN 144099
	   THEN NULL
	   ELSE  ( SELECT vced.current_data FROM epacube.V_CURRENT_ENTITY_DATA vced
				WHERE  vced.data_name_fk = @CustFocusDN
				AND    vced.entity_structure_fk = MMR.cust_entity_structure_fk )  
		END CUST_FOCUS_VALUE	   
      ,[MARGIN]
      ,[MARGIN_PERCENT] AS MARGIN_PCT
      ,[PRICE]
      ,[COGS]
      ,[REPL_COST]
      ,[Margin_Cost]      
      ,[REBATE_AMT_SPA]
      ,[REBATE_AMT_BUY]
      ,[REBATE_AMT_SELL_SIDE]  AS rebate_amt_sell    
      ,[SALES_TOTAL_QTY] AS SALES_QTY
      ,[LAST_SELL_DATE]      
  FROM [epacube].[marginmgr].[MARGIN_RESULTS] MMR WITH (NOLOCK)
INNER JOIN epacube.data_name dnprd WITH (NOLOCK) ON ( dnprd.data_name_id = @PrdFocusDN )
INNER JOIN epacube.data_set dsprd WITH (NOLOCK) ON ( dsprd.data_set_id = dnprd.data_set_fk )
INNER JOIN epacube.data_name dnWhse WITH (NOLOCK) ON ( dnwhse.data_name_id = @WhseFocusDN )
INNER JOIN epacube.data_name dnCust WITH (NOLOCK) ON ( dncust.data_name_id = @CustFocusDN )
---------  inner join for price, cost,.... blah blah..
WHERE 1 = 1 
AND   (     @LastSellDate_Begin IS NULL 
       OR  (    @LastSellDate_End IS NULL 
            AND LAST_SELL_DATE > @LastSellDate_Begin )
       OR  (  LAST_SELL_DATE BETWEEN @LastSellDate_Begin AND @LastSellDate_End  )
      )
----
----   PRODUCT FILTER
AND   (   @PrdFilterDN IS NULL
      OR  MMR.PRODUCT_STRUCTURE_FK IN (  SELECT VCPD.PRODUCT_STRUCTURE_FK
                                        FROM epacube.V_CURRENT_PRODUCT_DATA VCPD
                                        WHERE VCPD.DATA_NAME_FK = @PrdFilterDN
                                        AND   VCPD.CURRENT_DATA = @PrdFilterValue )
      )
----   WHSE FILTER
AND   (   @WhseFilterDN IS NULL
      OR  MMR.ORG_ENTITY_STRUCTURE_FK IN (  SELECT EI.ENTITY_STRUCTURE_FK
                                           FROM epacube.ENTITY_IDENTIFICATION EI
                                           WHERE EI.DATA_NAME_FK = @WhseFilterDN
                                           AND   EI.VALUE = @WhseFilterValue )
      )
----   CUST FILTER
AND   (   @CustFilterDN IS NULL
      OR  MMR.CUST_ENTITY_STRUCTURE_FK IN (  SELECT VCED.ENTITY_STRUCTURE_FK
                                            FROM epacube.V_CURRENT_PRODUCT_DATA VCED
                                            WHERE VCED.DATA_NAME_FK = @CustFilterDN
                                            AND   VCED.CURRENT_DATA = @CustFilterValue )
      )
----
) A
GROUP BY 	 A.PRD_FOCUS_NAME, A.PRD_FOCUS_VALUE,
             A.WHSE_FOCUS_NAME, A.WHSE_FOCUS_VALUE,
             A.CUST_FOCUS_NAME, A.CUST_FOCUS_VALUE

) B





---- and grouping

    
    RETURN
END;

