﻿
-- Copyright 2009
--
-- Function created by Kathi Scott
--
--
-- Purpose: Purpose of this Function is to retrieve Analysis Results based on Focus and Filter
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        12/29/2009  Initial SQL Version

CREATE FUNCTION [marginmgr].[Get_Analysis_Results]
( @JobFK BIGINT, @Scenario_Seq SMALLINT , @Margin_Type INT,
  @ProdFocusDN INT , @WhseFocusDN INT , @CustFocusDN INT , @SuplFocusDN INT,
  @SalesFocusIND SMALLINT , @TerrFocusIND SMALLINT , @PrcMgrFocusIND SMALLINT, @BuyFocusIND SMALLINT, 
  @CustSegFocusIND SMALLINT , @ProdSeqFocusIND SMALLINT , @ProdCustFocusIND SMALLINT ,  
  @ProdSuplFocusIND SMALLINT , @StrgFocusIND SMALLINT , @RuleFocusIND SMALLINT ,  @MatrixFocusIND SMALLINT ,   
  @ProdFilterDN INT ,  @ProdFilterValue VARCHAR(64),
  @WhseFilterDN INT , @WhseFilterValue VARCHAR(64) ,  
  @CustFilterDN INT , @CustFilterValue VARCHAR(64) ,    
  @SuplFilterDN INT , @SuplFilterValue VARCHAR(64) ,      
  @SaleFilterValue VARCHAR(64),   @TerrFilterValue VARCHAR(64) , 
  @PrcMgrFilterValue VARCHAR(64),  @BuyFilterValue VARCHAR(64) ,  
  @CustSegFilterValue VARCHAR(128) , @ProdSeqFilterValue VARCHAR(128) , @ProdCustFilterValue VARCHAR(128) ,  
  @ProdSuplFilterValue VARCHAR(128) , @StrgFilterValue VARCHAR(128) , @RuleFilterValue VARCHAR(128) , @MatrixFilterValue VARCHAR(128) ,   
  ---- Analysis Filters
  @StockItemsInd SMALLINT , @NonStockItemsInd SMALLINT ,  
  @PrcOvrInd SMALLINT,      @RbtCBInd SMALLINT, 
  @LastSellDate DATETIME , 
  @TotSalesHitsMin INT ,  @TotSalesHitsMax INT ,
  @TotSalesQtyMin INT ,  @TotSalesQtyMax INT ,
  @TotSalesDollarsMin INT ,  @TotSalesDollarsMax INT ,
  @Margin_Net_PctMin INT,    @Margin_Net_PctMax  INT,
  @Condition INT, @CurrencyFK INT, @UserLogin VARCHAR(64)
   )
RETURNS @AnalysisResults TABLE 
(
    -- Columns returned by the function
    Job_fk BIGINT 
   ,Scenario_Seq SMALLINT
   ,Scenario VARCHAR (32)
   ,Prod_Focus_DN_FK BIGINT NOT NULL
   ,Prod_Focus_Name VARCHAR(32)
   ,Prod_Focus_Value VARCHAR(32)
   ,Whse_Focus_DN_FK BIGINT NOT NULL
   ,Whse_Focus_Name VARCHAR(32)
   ,Whse_Focus_Value VARCHAR(32)
   ,Cust_Focus_DN_FK BIGINT NOT NULL
   ,Cust_Focus_Name VARCHAR(32)
   ,Cust_Focus_Value VARCHAR(32) 
   ,Supl_Focus_DN_FK BIGINT NULL   
   ,Supl_Focus_Name VARCHAR(32)
   ,Supl_Focus_Value VARCHAR(32)
   ,Sales_Focus_Value VARCHAR(32)     
   ,Terr_Focus_Value VARCHAR(32) 
   ,PrcMgr_Focus_Value VARCHAR(32) 
   ,Buyer_Focus_Value VARCHAR(32)       
   ,CustSeg_Focus_Value VARCHAR(32)
   ,ProdSeg_Focus_Value VARCHAR(32)  
   ,ProdCust_Focus_Value VARCHAR(32)  
   ,ProdSupl_Focus_Value VARCHAR(32)  
   ,Strategy_Focus_Value VARCHAR(32)
   ,Rule_Focus_Value VARCHAR(32)   
   ,Matrix_Focus_Value VARCHAR(32)      
------   
   ,Ovr_Qty   NUMERIC (8,0)   
   ,Ovr_Pct   NUMERIC (6,2)
   ,Ovr_Dollars NUMERIC (8,0)     
   ,Sales_Qty NUMERIC (8,0)
   ,Sales_Dollars NUMERIC (18,2)
   ,Sales_Dollars_Base NUMERIC (18,2)   
   ,Margin_Dollars NUMERIC (18,2)
   ,Margin_Dollars_Base NUMERIC (18,2)   
   ,Margin_Pct NUMERIC (6,2)
   ,Margin_Pct_Base NUMERIC (6,2)
   ,Neg_Impact NUMERIC (18,2)
   ,Pos_Impact NUMERIC (18,2)
   ,Margin_Impact NUMERIC (18,2)
   ,Pct_Impact NUMERIC (6,2)
   ,Pct_Cost NUMERIC (6,2)
   ,Pct_Price NUMERIC (6,2)
   ,Pct_Rebate NUMERIC (6,2)
   ,Prod_Cnt INT 
   ,Whse_Cnt INT
   ,Cust_Cnt INT
   ,Rebate_CB_Qty NUMERIC (8,0)                
   ,Condition INT
   ,Effective_Date DATETIME     
   ,Scenario_fk bigint     
)
AS
-- Returns the product_structure_fk, Org_entity_Structure, and Entity_structure_fk
--	 for all products updated since the last export
BEGIN

IF   @ProdFocusDN IS NULL
 SET   @ProdFocusDN = 149099   --- all products

IF   @WhseFocusDN IS NULL
 SET   @WhseFocusDN = 141099   --- all WHSES

IF   @CustFocusDN IS NULL
 SET   @CustFocusDN = 144099   --- all CUSTOMERS

IF   @SuplFocusDN IS NULL
 SET   @SuplFocusDN = 143099   --- all SUPPLIERS



 INSERT into @AnalysisResults (
    Job_fk
   ,Scenario_Seq    
   ,Scenario
   ,Prod_Focus_DN_FK 
   ,Prod_Focus_Name 
   ,Prod_Focus_Value 
   ,Whse_Focus_DN_FK 
   ,Whse_Focus_Name 
   ,Whse_Focus_Value 
   ,Cust_Focus_DN_FK 
   ,Cust_Focus_Name 
   ,Cust_Focus_Value 
   ,Supl_Focus_DN_FK 
   ,Supl_Focus_Name 
   ,Supl_Focus_Value 
   ,Sales_Focus_Value       
   ,Terr_Focus_Value 
   ,PrcMgr_Focus_Value 
   ,Buyer_Focus_Value 
   ,CustSeg_Focus_Value 
   ,ProdSeg_Focus_Value 
   ,ProdCust_Focus_Value          
   ,ProdSupl_Focus_Value             
   ,Strategy_Focus_Value
   ,Rule_Focus_Value 
   ,Matrix_Focus_Value    
   ,Ovr_Qty   
   ,Ovr_Pct   
   ,Ovr_Dollars 
   ,Sales_Qty 
   ,Sales_Dollars 
   ,Sales_Dollars_Base
   ,Margin_Dollars 
   ,Margin_Dollars_Base
   ,Margin_Pct 
   ,Margin_Pct_Base
   ,Neg_Impact 
   ,Pos_Impact 
   ,Margin_Impact 
   ,Pct_Impact 
   ,Pct_Cost 
   ,Pct_Price 
   ,Pct_Rebate    
   ,Prod_Cnt 
   ,Whse_Cnt 
   ,Cust_Cnt 
   ,Rebate_CB_Qty         
   ,Condition
   ,Effective_Date   
   ,Scenario_fk
  )
------------------------------------------------------
SELECT
	 B.JOB_FK
    ,B.SCENARIO_SEQ	 
	,B.SCENARIO 
	,B.Prod_FOCUS_DN_FK
	,B.Prod_FOCUS_NAME
	,B.Prod_FOCUS_VALUE
	,B.WHSE_FOCUS_DN_FK
	,B.WHSE_FOCUS_NAME
	,B.WHSE_FOCUS_VALUE
	,B.CUST_FOCUS_DN_FK
	,B.CUST_FOCUS_NAME
	,B.CUST_FOCUS_VALUE	
	,B.SUPL_FOCUS_DN_FK
	,B.SUPL_FOCUS_NAME
	,B.SUPL_FOCUS_VALUE
    ,B.Sales_Focus_Value 		
    ,B.Terr_Focus_Value 
    ,B.PrcMgr_Focus_Value 
    ,B.Buyer_Focus_Value 
    ,B.CustSeg_Focus_Value 
    ,B.ProdSeg_Focus_Value 
    ,B.ProdCust_Focus_Value          
    ,B.ProdSupl_Focus_Value             
    ,B.Strategy_Focus_Value
    ,B.Rule_Focus_Value 
    ,B.Matrix_Focus_Value     
-----
    ,B.Sales_Ovr_Qty
    ,B.Sales_Ovr_Pct
--------------,Case
--------------	When Max(OverRide_Type) = 1 then
--------------	Cast(Sum(Override_Qty) / Sum(Qty) as Numeric(8,4)) Else
--------------	Cast(Sum(Transactions_OverRide) / Sum(Transactions_Total) as Numeric(8,4)) end 'OverRide_Pct'		
    ,B.Sales_Ovr_Dollars	
	,B.Sales_Qty
    ,B.Sales_Dollars
    ,CASE WHEN B.Sales_Dollars <> B.Sales_Dollars_Base
     THEN B.Sales_Dollars_Base
     ELSE NULL 
     END as Sales_Dollars_Base
    ,B.Margin_Dollars
    ,CASE WHEN B.Margin_Dollars <> B.Margin_Dollars_Base
     THEN B.Margin_Dollars_Base
     ELSE NULL 
     END as Margin_Dollars_Base
    ,CASE WHEN B.Sales_Dollars = 0
     THEN 0
     ELSE ( B.Margin_Dollars / B.Sales_Dollars )
     END AS Margin_Pct
    ,CASE WHEN B.Sales_Dollars_Base = 0
     THEN 0
     ELSE ( B.Margin_Dollars_Base / B.Sales_Dollars_Base )
     END AS Margin_Pct_Base     
	,CASE WHEN ISNULL ( Margin_Impact_Dollars, 0 ) < 0
	 THEN Margin_Impact_Dollars
	 ELSE NULL
	 END AS Neg_Impact
	,CASE WHEN ISNULL ( Margin_Impact_Dollars, 0 ) > 0
	 THEN Margin_Impact_Dollars
	 ELSE NULL
	 END AS Pos_Impact
	,B.Margin_Impact_Dollars
	,Case When B.Sales_Dollars > 0 
	 THEN CASE WHEN ISNULL ( B.Margin_Dollars, 0 ) = 0
	      THEN 0
	      ELSE (B.Margin_Impact_Dollars / B.Margin_Dollars )
	      END 
	 ELSE 0 
	 END AS Pct_Impact
	,Case B.REPL_COST_CUR
	 When 0 then 0
	 ELSE ( (B.REPL_COST_New - B.REPL_COST_CUR) / (B.REPL_COST_CUR) )
	 END Pct_Cost
	,Case B.PRICE_CUR
	 When 0 then 0
	 ELSE ( (B.PRICE_New - B.PRICE_CUR) / (B.PRICE_CUR) )
	 END Pct_Price
	,Case B.RBT_AMT_CUR
	 When 0 then 0
	 ELSE ( (B.RBT_AMT_New - B.RBT_AMT_CUR) / (B.RBT_AMT_CUR) )
	 END Pct_Rebate	 
   ,B.Prod_Cnt 
   ,B.Whse_Cnt 
   ,B.Cust_Cnt 
   ,B.Rebate_CB_Qty         
   ,B.Condition
   ,B.Effective_Date   
   ,B.Scenario_fk
----------------------------------
FROM (
SELECT
	 A.JOB_FK
	,A.SCENARIO_SEQ	 
	,A.SCENARIO
	,@ProdFocusDN AS Prod_FOCUS_DN_FK
	,A.Prod_FOCUS_NAME
	,A.Prod_FOCUS_VALUE     
	,@WhseFocusDN AS WHSE_FOCUS_DN_FK
	,A.WHSE_FOCUS_NAME
	,A.WHSE_FOCUS_VALUE     
	,@CustFocusDN AS CUST_FOCUS_DN_FK
	,A.CUST_FOCUS_NAME
	,A.CUST_FOCUS_VALUE   
	,@SuplFocusDN AS SUPL_FOCUS_DN_FK
	,A.SUPL_FOCUS_NAME
	,A.SUPL_FOCUS_VALUE   
    ,A.Sales_Focus_Value 		
    ,A.Terr_Focus_Value 
    ,A.PrcMgr_Focus_Value 
    ,A.Buyer_Focus_Value 
    ,A.CustSeg_Focus_Value 
    ,A.ProdSeg_Focus_Value 
    ,A.ProdCust_Focus_Value          
    ,A.ProdSupl_Focus_Value             
    ,NULL AS STRATEGY_FOCUS_VALUE  --A.Strategy_Focus_Value
    ,NULL AS  RULE_FOCUS_VALUE     --A.Rule_Focus_Value 
    ,NULL AS MATRIX_FOCUS_VALUE  --- A.Matrix_Focus_Value     
-----	
	,Cast(Sum(A.Sales_Qty) as Numeric(8,0)) AS Sales_Qty	 	
    ,Cast(Sum(A.Sales_Dollars) as Numeric(18,2)) as Sales_Dollars
    ,Cast(Sum(A.Sales_Dollars_Base) as Numeric(18,2)) as Sales_Dollars_Base
    ,Cast(Sum(A.Margin_Dollars) as Numeric(18,2)) as Margin_Dollars
    ,Cast(Sum(A.Margin_Dollars_Base) as Numeric(18,2)) as Margin_Dollars_Base
	,Cast(Sum( ( A.Margin_Dollars_Base - A.Margin_Dollars) ) as numeric(18,2)) Margin_Impact_DOLLARS
	,Cast(Sum(A.REPL_COST_NEW) as numeric(18,2)) REPL_COST_NEW
	,Cast(Sum(A.REPL_COST_CUR) as numeric(18,2)) REPL_COST_CUR	
	,Cast(Sum(A.PRICE_NEW) as numeric(18,2)) PRICE_NEW
	,Cast(Sum(A.PRICE_CUR) as numeric(18,2)) PRICE_CUR	
	,Cast(Sum(A.REBATE_NEW) as numeric(18,2)) RBT_AMT_NEW
	,Cast(Sum(A.REBATE_CUR) as numeric(18,2)) RBT_AMT_CUR	
----	,Cast(Sum(A.Margin_Dollars_Cur) as Numeric(18,2)) as Margin_Dollars_Cur
	,Cast(Sum(A.Sales_Ovr_Qty) as Numeric(8,0)) AS Sales_Ovr_Qty
	,Cast(Sum(A.Sales_Ovr_Qty) / Sum(A.Sales_Qty) as Numeric(6,2))  AS Sales_Ovr_Pct
	,Cast(Sum(A.Sales_Ovr_Dollars) as Numeric(8,0)) Sales_Ovr_Dollars
----
	,COUNT  ( DISTINCT a.product_structure_fk )     AS Prod_Cnt 
	,COUNT  ( DISTINCT a.org_entity_structure_fk )  AS Whse_Cnt
	,COUNT  ( DISTINCT a.cust_entity_structure_fk ) AS Cust_Cnt
   ,Cast(Sum(A.Rebate_CB_Qty) as Numeric(8,0)) AS Rebate_CB_Qty
   ,MAX ( 97 )  AS Condition   --- fix later   
   ,A.Effective_Date
   ,A.Scenario_fk
FROM (
---------------------------
SELECT 
       AR.JOB_FK
      ,AR.SCENARIO_SEQ       
      ,AR.SCENARIO
      ,AR.PRODUCT_STRUCTURE_FK
      ,AR.ORG_ENTITY_STRUCTURE_FK
      ,AR.CUST_ENTITY_STRUCTURE_FK
      ,AR.SUPL_ENTITY_STRUCTURE_FK      
	  ,dnProd.SHORT_NAME AS Prod_FOCUS_NAME
	  ,CASE @ProdFocusDN
	   WHEN 149099
	   THEN NULL
	   ELSE ( SELECT vcpd.current_data FROM epacube.V_CURRENT_PRODUCT_DATA vcpd
			     WHERE vcpd.data_name_fk = @ProdFocusDN
			     AND   vcpd.product_structure_fk = AR.product_structure_fk
				 AND   (   dsProd.org_ind IS NULL
						OR vcpd.org_entity_structure_fk = ar.org_entity_structure_fk ) ) 
		END Prod_FOCUS_VALUE
	  ,dnwhse.SHORT_NAME AS WHSE_FOCUS_NAME
	  ,CASE @WhseFocusDN
	   WHEN 141099
	   THEN NULL
	   ELSE ( SELECT ei.value FROM epacube.entity_identification ei
				WHERE  ei.data_name_fk = @WhseFocusDN
				AND    ei.entity_structure_fk = ar.org_entity_structure_fk )  
		END WHSE_FOCUS_VALUE
	  ,dncust.SHORT_NAME AS CUST_FOCUS_NAME
	  ,CASE @CustFocusDN
	   WHEN 144099
	   THEN NULL
	   ELSE  ( SELECT vced.current_data FROM epacube.V_CURRENT_ENTITY_DATA vced
				WHERE  vced.data_name_fk = @CustFocusDN
				AND    vced.entity_structure_fk = ar.cust_entity_structure_fk )  
		END CUST_FOCUS_VALUE
	  ,dnsupl.SHORT_NAME AS SUPL_FOCUS_NAME			
	  ,CASE @SuplFocusDN
	   WHEN 143099
	   THEN NULL
	   ELSE  ( SELECT vced.current_data FROM epacube.V_CURRENT_ENTITY_DATA vced
				WHERE  vced.data_name_fk = @SuplFocusDN
				AND    vced.entity_structure_fk = ar.supl_entity_structure_fk )  
		END SUPL_FOCUS_VALUE	
--------
-----	 @SalesFocusIND SMALLINT,  @TerrFocusIND SMALLINT 
-----    @PrcMgrFocusIND SMALLINT, @BuyFocusIND SMALLINT, 
-----    @etc.. , @StrgFocusIND SMALLINT , @RuleFocusIND SMALLINT ,  
    ,CASE ISNULL ( @SalesFocusIND, 0 )
     WHEN 0 THEN 'N/A' 
     ELSE 'LOOKUP FUTURE' 
     END  AS Sales_Focus_Value 
    ,CASE ISNULL ( @TerrFocusIND, 0 )
     WHEN 0 THEN 'N/A' 
     ELSE 'LOOKUP FUTURE' 
     END  AS Terr_Focus_Value 
    ,CASE ISNULL ( @PrcMgrFocusIND, 0 )
     WHEN 0 THEN 'N/A' 
     ELSE 'LOOKUP FUTURE' 
     END  AS PrcMgr_Focus_Value 
    ,CASE ISNULL ( @BuyFocusIND, 0 )
     WHEN 0 THEN 'N/A' 
     ELSE 'LOOKUP FUTURE' 
     END  AS Buyer_Focus_Value 
    ,CASE ISNULL (  @CustSegFocusIND, 0 )
     WHEN 0 THEN 'N/A' 
     ELSE 'LOOKUP FUTURE'
     END  AS CustSeg_Focus_Value   
    ,CASE ISNULL (  @ProdSeqFocusIND, 0 )
     WHEN 0 THEN 'N/A' 
     ELSE 'LOOKUP FUTURE'
     END  AS ProdSeg_Focus_Value   
    ,CASE ISNULL (  @ProdCustFocusIND, 0 )
     WHEN 0 THEN 'N/A' 
     ELSE 'LOOKUP FUTURE'
     END  AS ProdCust_Focus_Value   
    ,CASE ISNULL (  @ProdSuplFocusIND, 0 )
     WHEN 0 THEN 'N/A' 
     ELSE 'LOOKUP FUTURE'
     END  AS ProdSupl_Focus_Value   
----    ,CASE ISNULL ( @StrgFocusIND, 0 )
----     WHEN 0 THEN 'N/A' 
----     ELSE ISNULL (
----             ( SELECT SHORT_NAME FROM marginmgr.PRICE_STRATEGY WHERE PRICE_STRATEGY_ID = AR.PRICE_STRATEGY_FK )
----             ,'N/A' )
----     END  AS Strategy_Focus_Value      
----    ,CASE ISNULL ( @RuleFocusIND, 0 )
----     WHEN 0 THEN 'N/A' 
----     ELSE ISNULL (
----             ( SELECT NAME FROM synchronizer.RULES_STRUCTURE WHERE RULES_STRUCTURE_ID = AR.PRICE_RULES_STRUCTURE_FK )
----             ,'N/A' )
----     END  AS Rule_Focus_Value 		
----      ,AR.Host_Matrix_Type AS Matrix_Focus_Value 
---------
      , AR.SALES_QTY 
      , AR.BASE_SALES_QTY  AS SALES_QTY_BASE
      ,( AR.SALES_QTY * AR.SELL_PRICE_NET_AMT ) AS SALES_DOLLARS 
      ,( AR.BASE_SALES_QTY * AR.SELL_PRICE_NET_AMT ) AS SALES_DOLLARS_BASE       
      , CASE ( ISNULL ( @Margin_Type, 742 ) )
        WHEN 741 THEN ( AR.SALES_QTY * AR.MARGIN_AMT )
        WHEN 742 THEN ( AR.SALES_QTY * AR.MARGIN_NET_AMT )
        WHEN 743 THEN ( AR.SALES_QTY * AR.MARGIN_NET_NET_AMT )
        ELSE ( AR.SALES_QTY * AR.MARGIN_NET_AMT )
        END AS MARGIN_DOLLARS
       ,CASE ( ISNULL ( @Margin_Type, 742 ) )
        WHEN 741 THEN ( AR.BASE_SALES_QTY * AR.BASE_MARGIN_AMT )
        WHEN 742 THEN ( AR.BASE_SALES_QTY * AR.BASE_MARGIN_NET_AMT )
        WHEN 743 THEN ( AR.BASE_SALES_QTY * AR.BASE_MARGIN_NET_NET_AMT )
        ELSE ( AR.SALES_QTY * AR.MARGIN_NET_AMT )
        END AS MARGIN_DOLLARS_BASE 
----      ,AR.MARGIN_PERCENT 
----      ,AR.MARGIN_PERCENT_BASE 
----      ,AR.NEGATIVE_IMPACT_DOLLARS 
----      ,AR.POSITIVE_IMPACT_DOLLARS 
----      ,AR.MARGIN_IMPACT_DOLLARS 
----      ,AR.MARGIN_IMPACT_PERCENT 
      ,( AR.SALES_QTY * AR.REPL_COST_AMT )  AS REPL_COST_DOLLARS 
      ,( AR.SALES_QTY *  AR.REBATE_CB_AMT ) AS REBATE_CB_DOLLARS
      ,( AR.SALES_QTY *  AR.REBATE_BUY_AMT ) AS REBATE_BUY_DOLLARS 
      ,( AR.SALES_QTY *  AR.REBATE_CUST_AMT ) AS REBATE_CUST_DOLLARS 
      ,( AR.SALES_OVR_QTY *  AR.SELL_PRICE_NET_AMT ) AS SALES_OVR_DOLLARS             
      ,AR.SALES_OVR_QTY  
      ,AR.REBATE_CB_QTY 
      ,AR.REBATE_BUY_QTY       
      ,AR.REBATE_CUST_QTY       
      ,AR.REPL_COST_AMT AS REPL_COST_NEW
      ,AR.BASE_REPL_COST_AMT AS REPL_COST_CUR
      ,AR.REBATE_CB_AMT AS REBATE_NEW
      ,AR.BASE_REBATE_CB_AMT AS REBATE_CUR
      ,AR.SELL_PRICE_NET_AMT AS PRICE_NEW
      ,AR.BASE_SELL_PRICE_AMT AS PRICE_CUR
	  ,AR.EFFECTIVE_DATE
	  ,AR.SCENARIO_FK
FROM marginmgr.V_MARGIN_DETAILS AR 
LEFT JOIN epacube.data_name dnProd WITH (NOLOCK) ON ( dnProd.data_name_id = @ProdFocusDN )
LEFT JOIN epacube.data_set dsProd WITH (NOLOCK) ON ( dsProd.data_set_id = dnProd.data_set_fk )
LEFT JOIN epacube.data_name dnWhse WITH (NOLOCK) ON ( dnwhse.data_name_id = @WhseFocusDN )
LEFT JOIN epacube.data_name dnCust WITH (NOLOCK) ON ( dncust.data_name_id = @CustFocusDN )
LEFT JOIN epacube.data_name dnSupl WITH (NOLOCK) ON ( dnsupl.data_name_id = @SuplFocusDN )
--------------------	  
----FROM marginmgr.V_MARGIN_RESULTS_BLENDED AR 
----LEFT JOIN epacube.data_name dnProd WITH (NOLOCK) ON ( dnProd.data_name_id = @ProdFocusDN )
----LEFT JOIN epacube.data_set dsProd WITH (NOLOCK) ON ( dsProd.data_set_id = dnProd.data_set_fk )
----LEFT JOIN epacube.data_name dnWhse WITH (NOLOCK) ON ( dnwhse.data_name_id = @WhseFocusDN )
----LEFT JOIN epacube.data_name dnCust WITH (NOLOCK) ON ( dncust.data_name_id = @CustFocusDN )
----LEFT JOIN epacube.data_name dnSupl WITH (NOLOCK) ON ( dnsupl.data_name_id = @SuplFocusDN )
---------  
WHERE job_fk = @JobFK
AND   (   @Scenario_Seq IS NULL
      OR  @Scenario_Seq = AR.scenario_seq )
----
----   PRODUCT FILTER
AND   (   @ProdFilterDN IS NULL
      OR  AR.PRODUCT_STRUCTURE_FK IN (  SELECT VCPD.PRODUCT_STRUCTURE_FK
                                        FROM epacube.V_CURRENT_PRODUCT_DATA VCPD
                                        WHERE VCPD.DATA_NAME_FK = @ProdFilterDN
                                        AND   VCPD.CURRENT_DATA = @ProdFilterValue )
      )
----   WHSE FILTER
AND   (   @WhseFilterDN IS NULL
      OR  AR.ORG_ENTITY_STRUCTURE_FK IN (  SELECT EI.ENTITY_STRUCTURE_FK
                                           FROM epacube.ENTITY_IDENTIFICATION EI
                                           WHERE EI.DATA_NAME_FK = @WhseFilterDN
                                           AND   EI.VALUE = @WhseFilterValue )
      )
AND   (   @CustFilterDN IS NULL
      OR  AR.CUST_ENTITY_STRUCTURE_FK IN (  SELECT EI.ENTITY_STRUCTURE_FK
                                            FROM epacube.ENTITY_IDENTIFICATION EI
                                            WHERE EI.DATA_NAME_FK = @CustFilterDN
                                            AND   EI.VALUE = @CustFilterValue )
      )
      
--------   CUST FILTER
----AND   (   @CustFilterDN IS NULL
----      OR  AR.CUST_ENTITY_STRUCTURE_FK IN (  SELECT VCED.ENTITY_STRUCTURE_FK
----                                            FROM epacube.V_CURRENT_PRODUCT_DATA VCED
----                                            WHERE VCED.DATA_NAME_FK = @CustFilterDN
----                                            AND   VCED.CURRENT_DATA = @CustFilterValue )
----      )
----
) A
GROUP BY 	 A.JOB_FK, A.SCENARIO_SEQ, A.SCENARIO, 
             A.Prod_FOCUS_NAME, A.Prod_FOCUS_VALUE,
             A.WHSE_FOCUS_NAME, A.WHSE_FOCUS_VALUE,
             A.CUST_FOCUS_NAME, A.CUST_FOCUS_VALUE,
             A.SUPL_FOCUS_NAME, A.SUPL_FOCUS_VALUE,
             A.Sales_Focus_Value, A.Terr_Focus_Value, 
             A.PrcMgr_Focus_Value, A.Buyer_Focus_Value, 
             A.CustSeg_Focus_Value, A.ProdSeg_Focus_Value,
             A.ProdCust_Focus_Value, A.ProdSupl_Focus_Value,
----             A.Strategy_Focus_Value, A.Rule_Focus_Value, A.Matrix_Focus_Value
             A.Effective_Date, A.scenario_fk             
) B





---- CHANGES 

---------
----      ,AR.SALES_QTY 		  
----      ,AR.SALES_DOLLARS 
----      ,AR.SALES_DOLLARS_BASE 
----      ,( AR.MARGIN_DOLLARS  - ISNULL ( ABLI.BOTTOMLINE_DOLLARS, 0 ) )   AS MARGIN_DOLLARS    
----      ,AR.MARGIN_DOLLARS_BASE 
----      ,( ( AR.MARGIN_DOLLARS  - ISNULL ( ABLI.BOTTOMLINE_DOLLARS, 0 ) )  /  AR.SALES_DOLLARS  )  AS  MARGIN_PERCENT 
----      ,AR.MARGIN_PERCENT_BASE 
----      ,( AR.NEGATIVE_IMPACT_DOLLARS  + ISNULL ( ABLI.BOTTOMLINE_DOLLARS, 0 ) )   AS NEGATIVE_IMPACT_DOLLARS 
----      ,( AR.POSITIVE_IMPACT_DOLLARS  - ISNULL ( ABLI.BOTTOMLINE_DOLLARS, 0 ) ) AS POSITIVE_IMPACT_DOLLARS 
----      ,( AR.MARGIN_IMPACT_DOLLARS  - ISNULL ( ABLI.BOTTOMLINE_DOLLARS, 0 ) )   AS  MARGIN_IMPACT_DOLLARS 
----      , ( ( AR.MARGIN_IMPACT_DOLLARS  - ISNULL ( ABLI.BOTTOMLINE_DOLLARS, 0 ) ) /  AR.SALES_DOLLARS  )   MARGIN_IMPACT_PERCENT 
----      ,0 AS Bottomline_Approved_Dollars 
----      ,ISNULL ( ABLI.BOTTOMLINE_DOLLARS, 0 ) AS Bottomline_Pending_Dollars       
----      ,AR.REPL_COST_DOLLARS 




/*

,Cast(Sum(B.Price_New) as Numeric(18,2)) 'Sales Dollars'
,Cast(Sum(B.Price_New) - Sum(B.REPL_COST_New) as Numeric(18,2)) 'Margin Dollars'
--,Cast((Sum(B.Price_New) - Sum(B.REPL_COST_New)) / Sum(B.Price_New) as Numeric(8,5))'Margin Pct'
,Case sum(B.Price_New)
	When 0 then Cast(0 as numeric(8,3))
	else cast((Sum(B.Price_New) - Sum(B.REPL_COST_New)) / Sum(B.Price_New) as numeric(8,3)) end 'Margin Pct'
,Cast(Sum(B.Neg_Impact) as numeric(18,2)) Neg_Impact
,Cast(Sum(B.Pos_Impact) as numeric(18,2)) Pos_Impact
,Cast(Sum(B.Neg_Impact) + Sum(B.Pos_Impact) as numeric(18,2)) 'Margin Impact'
,Cast(Case 
	When Sum(B.Price_New) > 0 and (Sum(B.price_cur) - sum(B.REPL_COST_cur)) <> 0 then
	Sum(B.[Margin Impact]) / (Sum(B.price_cur) - sum(B.REPL_COST_cur))
	else 
	0 end as Numeric(8,5)) 'Pct Impact'
--,Cast((Sum(B.REPL_COST_New) - Sum(B.REPL_COST_CUR)) / Sum(B.REPL_COST_CUR) as numeric(8,5)) 'Pct Cost Change'
,Case Sum(B.REPL_COST_CUR)
	When 0 then 0
	else Cast((Sum(B.REPL_COST_New) - Sum(B.REPL_COST_CUR)) / Sum(B.REPL_COST_CUR) as numeric(8,5)) end 'Pct Cost Change'
--,Cast((Sum(B.Price_New) - Sum(B.Price_Cur)) / Sum(B.Price_Cur) as numeric(8,5)) 'Pct Price Change'
,Case Sum(B.Price_Cur)
	When 0 then 0
	else Cast((Sum(B.Price_New) - Sum(B.Price_Cur)) / Sum(B.Price_Cur) as numeric(8,5)) end 'Pct Price Change'
--
,Cast(Sum(B.Price_New_Proj) as Numeric(18,2)) 'Sales_Dollars_Proj'
,Cast(Sum(B.Price_New_Proj - B.REPL_COST_New) as Numeric(18,2)) 'Margin_Dollars_Proj'
--,Cast(Sum(B.Price_New_Proj - B.REPL_COST_New)/ Sum(B.Price_New_Proj) as Numeric(18,2)) 'Margin_Pct_Proj'
,Case Sum(B.Price_New_Proj)
	When 0 then 0
	else
	Cast(Sum(B.Price_New_Proj - B.REPL_COST_New)/ Sum(B.Price_New_Proj) as Numeric(18,2)) end 'Margin_Pct_Proj'
,Cast(Sum(B.Price_New_Proj - B.Price_New_Proj_Basis + REPL_COST_New_Basis - REPL_COST_New) as numeric(18,2)) 'Margin_Impact_Proj'
--
,Case Cast(Sum(B.Price_New_Proj_Basis - REPL_COST_New_Basis) as numeric(18,2))
	When 0 then 0 else
	Cast(	(Sum(B.Price_New_Proj - B.REPL_COST_New) -  Sum(B.Price_New_Proj_Basis - REPL_COST_New_Basis)) / 
	Sum(B.Price_New_Proj_Basis - REPL_COST_New_Basis) as numeric(18,2)) End
	'Pct_Impact_Proj'

--,Cast(Sum(REPL_COST_NEW - REPL_COST_NEW_Basis) / Sum(REPL_COST_NEW_Basis) as numeric(18,2)) 'Pct_Cost_Change'
,Case Sum(REPL_COST_NEW_Basis)
	When 0 then 0
	else Cast(Sum(REPL_COST_NEW - REPL_COST_NEW_Basis) / Sum(REPL_COST_NEW_Basis) as numeric(18,2)) end 'Pct_Cost_Change'
--,Cast(Sum(B.Price_New_Proj - B.Price_New_Proj_Basis) / Sum(B.Price_New_Proj_Basis) as numeric(8,5)) 'Pct_Price_Change'
,Case Sum(B.Price_New_Proj_Basis)
	When 0 then 0
	else Cast(Sum(B.Price_New_Proj - B.Price_New_Proj_Basis) / Sum(B.Price_New_Proj_Basis) as numeric(8,5)) end 'Pct_Price_Change'
--
,Cast(Sum(
	Case when (B.Price_New_Proj - B.REPL_COST_New) < (B.Price_Cur_Proj - B.REPL_COST_Cur)
		then ((B.Price_New_Proj - B.REPL_COST_New) - (B.Price_Cur_Proj - B.REPL_COST_Cur))
		Else 0 End	
	+

	Case When Price_Adjustment < 0
	then 
	((B.Price_New_Proj - B.REPL_COST_New) - (B.Price_New_Proj_Basis - B.REPL_COST_New))
	else 0 end +
	Case When REPL_COST_Adjustment > 0 Then
		REPL_COST_New_Basis - REPL_COST_New
		Else 0 End) as numeric(18,2)) 'Neg_Impact_Proj'
--
,Cast(Sum(

	Case when (B.Price_New_Proj - B.REPL_COST_New) > (B.Price_Cur_Proj - B.REPL_COST_Cur)
		then ((B.Price_New_Proj - B.REPL_COST_New) - (B.Price_Cur_Proj - B.REPL_COST_Cur))
		Else 0 End	
	+
	Case When Price_Adjustment > 0 then
	((B.Price_New_Proj - B.REPL_COST_New) - (B.Price_New_Proj_Basis - B.REPL_COST_New))
	else 0 end + 
	Case When REPL_COST_Adjustment <= 0 Then
		REPL_COST_New_Basis - REPL_COST_New
	else 0
	end) as numeric(18,2)) 'Pos_Impact_Proj'

,Cast(Sum(REPL_COST_NEW) as numeric(18,2)) REPL_COST_New
,Cast(Sum(REPL_COST_Actual) as numeric(18,2)) REPL_COST_Actual
,Cast(Sum(REPL_COST_NEW_Basis) as numeric(18,2)) REPL_COST_NEW_Basis
,Cast(Sum(Price_Actual) as numeric(18,2)) Sales_Dollars_Actual
,Cast(Sum(Price_Actual - REPL_COST_Actual) as numeric(18,2)) Margin_Dollars_Actual
--,Cast(Sum(Price_Actual - REPL_COST_Actual) / Sum(Price_Actual) as numeric(18,2)) Margin_Pct_Actual
,Case Sum(Price_Actual)
	When 0 then 0
	else Cast(Sum(Price_Actual - REPL_COST_Actual) / Sum(Price_Actual) as numeric(18,2)) end Margin_Pct_Actual

,Case isnull(Sum(Price_Actual),0)
	When 0 then 0
	else Cast(Sum(Price_Actual - REPL_COST_Actual) / Sum(Price_Actual) as numeric(18,2))
	end Margin_Pct_Actual


From 
(
Select

Sum(Qty * Qty_Adjustment) Qty
,Sum((Neg_Impact + REPL_COST_Adjustment) * Qty_Adjustment) Neg_Impact
,Sum((Pos_Impact + Price_Adjustment) * Qty_Adjustment) Pos_Impact
,Sum((Neg_Impact + REPL_COST_Adjustment + Pos_Impact + Price_Adjustment ) * Qty_Adjustment) 'Margin Impact'
,Sum((Price_New + Price_Adjustment) * Qty_Adjustment) Price_New
,Sum((price_cur) * Qty_Adjustment) price_cur
,sum((REPL_COST_new - REPL_COST_Adjustment) * Qty_Adjustment) REPL_COST_new
,sum((REPL_COST_cur) * Qty_Adjustment) REPL_COST_cur
,Sum((Override_Qty) * Qty_Adjustment) OverRide_Qty
,Sum(Transactions_Total) Transactions_Total
,Sum(Transactions_OverRide) Transactions_OverRide
,Sum(REPL_COST_Cur_Actual) REPL_COST_Actual
,Sum(Price_Cur_Actual) Price_Actual
, 1 'OverRide_Type'
,Sum((Price_New_Proj + Price_Adjustment) * Qty_Adjustment) Price_New_Proj
,Sum((Price_Cur_Proj + Price_Adjustment) * Qty_Adjustment) Price_Cur_Proj
,Sum(Price_New_Proj * Qty_Adjustment) Price_New_Proj_Basis
,sum((REPL_COST_new) * Qty_Adjustment) REPL_COST_new_Basis
,Max(Price_Adjustment) Price_Adjustment
,Max(REPL_COST_Adjustment) REPL_COST_Adjustment
--,Case 
--	
--	(Select top 1 Cost_Calc_Method from dbo.MMDB_Parameters_Jobs where Calc_Job_FK_i = 12412.01 and What_IF = 0)
--	When 'Multiply By' Then (Select top 1 Cost_Calc_Value from dbo.MMDB_Parameters_Jobs where Calc_Job_FK_i = 12412.01 and What_IF = 0)
--	Else 1 / (Select top 1 Cost_Calc_Value from dbo.MMDB_Parameters_Jobs where Calc_Job_FK_i = 12412.01 and What_IF = 0)
--End REPL_COST_Factor 
--,Case 
--	
--	(Select top 1 Price_Calc_Method from dbo.MMDB_Parameters_Jobs where Calc_Job_FK_i = 12412.01 and What_IF = 0)
--	When 'Multiply By' Then (Select top 1 Price_Calc_Value from dbo.MMDB_Parameters_Jobs where Calc_Job_FK_i = 12412.01 and What_IF = 0)
--	Else 1 / (Select top 1 Price_Calc_Value from dbo.MMDB_Parameters_Jobs where Calc_Job_FK_i = 12412.01 and What_IF = 0)
--End Price_Factor 


from
(
Select

isnull(Case 
	
	(Select top 1 Cost_Calc_Method from dbo.MMDB_Parameters_Jobs where Calc_Job_FK_ipj = 12412.01 and What_IF = 0)
	When 'Multiply By' Then REPL_COST_NEW - (REPL_COST_New * (Select top 1 Cost_Calc_Value from dbo.MMDB_Parameters_Jobs where Calc_Job_FK_ipj = 12412.01 and What_IF = 0))
	Else REPL_COST_New - (REPL_COST_New / (Select top 1 Cost_Calc_Value from dbo.MMDB_Parameters_Jobs where Calc_Job_FK_ipj = 12412.01 and What_IF = 0))
End,0) REPL_COST_Adjustment 

,isnull(Case 
	
	(Select top 1 Price_Calc_Method from dbo.MMDB_Parameters_Jobs where Calc_Job_FK_ipj = 12412.01 and What_IF = 0)
	When 'Multiply By' Then (Price_New_Proj * (Select top 1 Price_Calc_Value from dbo.MMDB_Parameters_Jobs where Calc_Job_FK_ipj = 12412.01 and What_IF = 0)) - Price_New_Proj
	Else (Price_New_Proj / (Select top 1 Price_Calc_Value from dbo.MMDB_Parameters_Jobs where Calc_Job_FK_ipj = 12412.01 and What_IF = 0)) - Price_New_Proj
End,0) Price_Adjustment

,isnull((Select top 1 Qty_Calc_Value from dbo.MMDB_Parameters_Jobs where calc_Job_FK_ipj =12412.01 and What_IF = 0),1) Qty_Adjustment

,(Qty) Qty
,(Neg_Impact) Neg_Impact
,(Pos_Impact) Pos_Impact
,(Price_New) Price_New
,(price_cur) price_cur
,(REPL_COST_new) REPL_COST_new
,(REPL_COST_cur) REPL_COST_cur
,Last_Sell_Price_Hist
,Last_REPL_COST_Hist
,REPL_COST_Cur_Actual
,Price_Cur_Actual
,(Override_Qty) OverRide_Qty
,OverridePct_Qty
,Transactions_OverRide
,Transactions_Total
,Result
,Calc_job_fk_i
,MMCA_FK
,Reference_Price_New
,Price_New_Proj
,Price_Cur_Proj


from dbo.MMDB_CALCULATOR MMDB

Where result = 'price' and Calc_job_fk_i =12412.01 and isnull(REPL_COST_new,0) > 0

) A



) B




        

*/
    
    RETURN
END;
