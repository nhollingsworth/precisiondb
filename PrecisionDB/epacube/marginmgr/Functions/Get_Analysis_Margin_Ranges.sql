﻿
-- Copyright 2009
--
-- Function created by Kathi Scott
--
--
-- Purpose: Purpose of this Function is to retrieve Analysis Margin Results based on Focus and Filter
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/03/2009  Initial SQL Version
--

CREATE FUNCTION [marginmgr].[Get_Analysis_Margin_Ranges]

( @JobFK BIGINT, @Scenario_FK SMALLINT ,
  @PrdFocusDN INT , @WhseFocusDN INT , @CustFocusDN INT , @SaleFocusDN INT ,
  @TerrFocusIND SMALLINT , @PrcMgrFocusIND SMALLINT, @BuyFocusIND SMALLINT, 
  @BLFocusIND SMALLINT , @StrgFocusIND SMALLINT , @RuleFocusIND SMALLINT ,   
  @PrdFilterDN INT , @PrdFilterValue VARCHAR(64),
  @WhseFilterDN INT , @WhseFilterValue VARCHAR(64) ,  
  @CustFilterDN INT , @CustFilterValue VARCHAR(64) ,    
  @SaleFilterDN INT , @SaleFilterValue VARCHAR(64) ,  
  @TerrFilterValue VARCHAR(64) ,  @PrcMgrFilterValue VARCHAR(64) ,  @BuyFilterValue VARCHAR(64) ,  
  @BLFilterValue VARCHAR(64) , @StrgFilterValue VARCHAR(64) ,  @RuleFilteValue VARCHAR(64) ,            
  ---- Analysis Filters
  @BottomlinePendingInd SMALLINT , @BottomlineApprovedInd SMALLINT ,
  @StockItemsInd SMALLINT , @NonStockItemsInd SMALLINT ,  
  @PrcOvrInd SMALLINT , @PrcMissInd SMALLINT , @PrcMtxInd SMALLINT ,
  @PrcConInd SMALLINT , @PrcSPAInd SMALLINT , @PromoInd SMALLINT ,  
  @RbtSPAInd SMALLINT , @RbtBuyInd SMALLINT , @RbtSellInd SMALLINT , 
  @LastSellDate DATETIME , 
  @TotSalesHitsMin INT ,  @TotSalesHitsMax INT ,
  @TotSalesQtyMin INT ,  @TotSalesQtyMax INT ,
  @TotSalesDollarsMin INT ,  @TotSalesDollarsMax INT ,
  @CurrencyFK INT  
   )
RETURNS @MMResults TABLE 
(
    -- Columns returned by the function
    Job_fk BIGINT 
   ,Scenario VARCHAR (32)
   ,Prd_Focus_DN_FK BIGINT NOT NULL
   ,Prd_Focus_Name VARCHAR(32)
   ,Prd_Focus_Value VARCHAR(32)
   ,Whse_Focus_DN_FK BIGINT NOT NULL
   ,Whse_Focus_Name VARCHAR(32)
   ,Whse_Focus_Value VARCHAR(32)
   ,Cust_Focus_DN_FK BIGINT NOT NULL
   ,Cust_Focus_Name VARCHAR(32)
   ,Cust_Focus_Value VARCHAR(32) 
   ,Sales_Focus_DN_FK BIGINT NOT NULL   
   ,Sales_Focus_Name VARCHAR(32)
   ,Sales_Focus_Value VARCHAR(32) 
   ,Terr_Focus_Value VARCHAR(32) 
   ,PrcMgr_Focus_Value VARCHAR(32) 
   ,Buyer_Focus_Value VARCHAR(32)       
   ,Bottomline_Focus_Value VARCHAR(32)
   ,Strategy_Focus_Value VARCHAR(32)
   ,Rule_Focus_Value VARCHAR(32)   
------   
   ,Margin_Min NUMERIC (8,2)
   ,Margin_Max NUMERIC (8,2)     
   ,Margin_Delta  NUMERIC (9,2)
   ,Margin_Mean  NUMERIC (9,2)   
   ,Margin_Median NUMERIC (8,2)
   ,Margin_Mode NUMERIC (8,2)     
   ,Margin_stdev  NUMERIC (9,2)
   ,Margin_var  NUMERIC (9,2)      
-----    
   ,Margin_Pct_Min NUMERIC (9,2)
   ,Margin_Pct_Max NUMERIC (9,2)
   ,Margin_Pct_Delta  NUMERIC (9,2)      
   ,Margin_Pct_Mean  NUMERIC (9,2)     
   ,Margin_Pct_Median NUMERIC (9,2)
   ,Margin_Pct_Mode NUMERIC (9,2)
   ,Margin_Pct_stdev  NUMERIC (9,2)      
   ,Margin_Pct_var  NUMERIC (9,2)        
-----        
   ,Price_Min NUMERIC (8,2)
   ,Price_Max NUMERIC (8,2)
   ,Price_Delta NUMERIC (9,2)
   ,Price_Mean NUMERIC (9,2)   
   ,Price_Median NUMERIC (8,2)
   ,Price_Mode NUMERIC (8,2)
   ,Price_stdev NUMERIC (9,2)
   ,Price_var NUMERIC (9,2)      
-----    
   ,COGS_Min NUMERIC (8,2)
   ,COGS_Max NUMERIC (8,2)
   ,COGS_Delta NUMERIC (9,2)   
   ,COGS_Mean NUMERIC (9,2)    
   ,COGS_Median NUMERIC (8,2)
   ,COGS_Mode NUMERIC (8,2)
   ,COGS_Stdev NUMERIC (9,2)   
   ,COGS_var NUMERIC (9,2)       
-----      
   ,Sales_Qty_Min NUMERIC (8,2)
   ,Sales_Qty_Max NUMERIC (8,2)
   ,Sales_Qty_Delta NUMERIC (8,2)
   ,Sales_Qty_Mean NUMERIC (8,2)   
   ,Sales_Qty_Median NUMERIC (8,2)
   ,Sales_Qty_Mode NUMERIC (8,2)
   ,Sales_Qty_Stdev NUMERIC (8,2)
   ,Sales_Qty_var NUMERIC (8,2)      
----- 
   ,Sales_Dollars_Min NUMERIC (8,2)
   ,Sales_Dollars_Max NUMERIC (8,2)
   ,Sales_Dollars_Delta NUMERIC (8,2)
   ,Sales_Dollars_Mean NUMERIC (8,2) 
   ,Sales_Dollars_Median NUMERIC (8,2)
   ,Sales_Dollars_Mode NUMERIC (8,2)
   ,Sales_Dollars_stdev NUMERIC (8,2)
   ,Sales_Dollars_var NUMERIC (8,2)    
-----      
   ,Sales_Transactions_Min NUMERIC (8,2)
   ,Sales_Transactions_Max NUMERIC (8,2)
   ,Sales_Transactions_Delta NUMERIC (8,2)
   ,Sales_Transactions_Mean NUMERIC (8,2)   
   ,Sales_Transactions_Median NUMERIC (8,2)
   ,Sales_Transactions_Mode NUMERIC (8,2)
   ,Sales_Transactions_stdev NUMERIC (8,2)
   ,Sales_Transactions_var NUMERIC (8,2)      
-----    
   ,Repl_Cost_Min NUMERIC (8,2)
   ,Repl_Cost_Max NUMERIC (8,2)   
   ,Repl_Cost_Delta NUMERIC (8,2)   
   ,Repl_Cost_Mean NUMERIC (8,2)  
   ,Repl_Cost_Median NUMERIC (8,2)
   ,Repl_Cost_Mode NUMERIC (8,2)   
   ,Repl_Cost_stdev NUMERIC (8,2)   
   ,Repl_Cost_var NUMERIC (8,2)     
-----           
   ,Margin_Cost_Min NUMERIC (8,2)
   ,Margin_Cost_Max NUMERIC (8,2)      
   ,Margin_Cost_Delta NUMERIC (8,2)   
   ,Margin_Cost_Mean NUMERIC (8,2)     
   ,Margin_Cost_Median NUMERIC (8,2)
   ,Margin_Cost_Mode NUMERIC (8,2)      
   ,Margin_Cost_stdev NUMERIC (8,2)   
   ,Margin_Cost_var NUMERIC (8,2)        
-----           
   ,Rebate_Amt_Spa_Min NUMERIC (8,2)
   ,Rebate_Amt_Spa_Max NUMERIC (8,2)
   ,Rebate_Amt_Spa_Delta NUMERIC (8,2)
   ,Rebate_Amt_Spa_Mean NUMERIC (8,2)     
   ,Rebate_Amt_Spa_Median NUMERIC (8,2)
   ,Rebate_Amt_Spa_Mode NUMERIC (8,2)
   ,Rebate_Amt_Spa_stdev NUMERIC (8,2)
   ,Rebate_Amt_Spa_var NUMERIC (8,2)        
-----        
   ,Rebate_Amt_Buy_Min NUMERIC (8,2)
   ,Rebate_Amt_Buy_Max NUMERIC (8,2)
   ,Rebate_Amt_Buy_Delta NUMERIC (8,2)
   ,Rebate_Amt_Buy_Mean NUMERIC (8,2)   
   ,Rebate_Amt_Buy_Median NUMERIC (8,2)
   ,Rebate_Amt_Buy_Mode NUMERIC (8,2)
   ,Rebate_Amt_Buy_stdev NUMERIC (8,2)
   ,Rebate_Amt_Buy_var NUMERIC (8,2)      
-----             
   ,Rebate_Amt_Sell_Min NUMERIC (8,2)
   ,Rebate_Amt_Sell_Max NUMERIC (8,2)   
   ,Rebate_Amt_Sell_Delta NUMERIC (8,2)
   ,Rebate_Amt_Sell_Mean NUMERIC (8,2)            
   ,Rebate_Amt_Sell_Median NUMERIC (8,2)
   ,Rebate_Amt_Sell_Mode NUMERIC (8,2)   
   ,Rebate_Amt_Sell_stdev NUMERIC (8,2)
   ,Rebate_Amt_Sell_var NUMERIC (8,2)               
----- 
   ,Prd_Cnt INT 
   ,Whse_Cnt INT
   ,Cust_Cnt INT   
)
AS
-- Returns the product_structure_fk, Org_entity_Structure, and Entity_structure_fk
--	 for all products updated since the last export
BEGIN

IF   @PrdFocusDN IS NULL
 SET   @PrdFocusDN = 149099   --- all products

IF   @WhseFocusDN IS NULL
 SET   @WhseFocusDN = 141099   --- all WHSES

IF   @CustFocusDN IS NULL
 SET   @CustFocusDN = 144099   --- all CUSTOMERS


 INSERT into @MMResults (
    Job_fk
   ,Scenario
   ,Prd_Focus_DN_FK 
   ,Prd_Focus_Name 
   ,Prd_Focus_Value 
   ,Whse_Focus_DN_FK 
   ,Whse_Focus_Name 
   ,Whse_Focus_Value 
   ,Cust_Focus_DN_FK 
   ,Cust_Focus_Name 
   ,Cust_Focus_Value 
   ,Sales_Focus_DN_FK 
   ,Sales_Focus_Name 
   ,Sales_Focus_Value    
   ,Terr_Focus_Value 
   ,PrcMgr_Focus_Value 
   ,Buyer_Focus_Value 
   ,Bottomline_Focus_Value 
   ,Strategy_Focus_Value
   ,Rule_Focus_Value 
-----   
   ,Margin_Min 
   ,Margin_Max      
   ,Margin_Delta
   ,Margin_Mean
   ,Margin_Median
   ,Margin_Mode
   ,Margin_Stdev
   ,Margin_Var  
-----   
   ,Margin_Pct_Min
   ,Margin_Pct_Max
   ,Margin_Pct_Delta
   ,Margin_Pct_Mean
   ,Margin_Pct_Median
   ,Margin_Pct_Mode
   ,Margin_Pct_Stdev
   ,Margin_Pct_Var 
-----   
   ,Price_Min 
   ,Price_Max 
   ,Price_Delta
   ,Price_Mean 
   ,Price_Median
   ,Price_Mode
   ,Price_Stdev
   ,Price_Var
-----   
   ,COGS_Min 
   ,COGS_Max 
   ,COGS_Delta 
   ,COGS_Mean
   ,COGS_Median
   ,COGS_Mode
   ,COGS_Stdev
   ,COGS_var
-----   
   ,Sales_Qty_Min 
   ,Sales_Qty_Max 
   ,Sales_Qty_Delta 
   ,Sales_Qty_Mean  
   ,Sales_Qty_Median
   ,Sales_Qty_Mode
   ,Sales_Qty_Stdev
   ,Sales_Qty_Var  
-----    
   ,Sales_Dollars_Min 
   ,Sales_Dollars_Max 
   ,Sales_Dollars_Delta 
   ,Sales_Dollars_Mean  
   ,Sales_Dollars_Median
   ,Sales_Dollars_Mode
   ,Sales_Dollars_Stdev
   ,Sales_Dollars_Var  
-----                
   ,Sales_Transactions_Min 
   ,Sales_Transactions_Max 
   ,Sales_Transactions_Delta 
   ,Sales_Transactions_Mean  
   ,Sales_Transactions_Median
   ,Sales_Transactions_Mode
   ,Sales_Transactions_Stdev
   ,Sales_Transactions_Var  
-----                
   ,Repl_Cost_Min 
   ,Repl_Cost_Max    
   ,Repl_Cost_Delta    
   ,Repl_Cost_Mean  
   ,Repl_Cost_Median 
   ,Repl_Cost_Mode
   ,Repl_Cost_Stdev  
   ,Repl_Cost_Var  
-----                
   ,Margin_Cost_Min 
   ,Margin_Cost_Max       
   ,Margin_Cost_Delta    
   ,Margin_Cost_Mean             
   ,Margin_Cost_Median
   ,Margin_Cost_Mode
   ,Margin_Cost_Stdev 
   ,Margin_Cost_Var
-----   
   ,Rebate_Amt_Spa_Min 
   ,Rebate_Amt_Spa_Max 
   ,Rebate_Amt_Spa_Delta 
   ,Rebate_Amt_Spa_Mean          
   ,Rebate_Amt_Spa_Median        
   ,Rebate_Amt_Spa_Mode     
   ,Rebate_Amt_Spa_Stdev     
   ,Rebate_Amt_Spa_Var   
-----                     
   ,Rebate_Amt_Buy_Min 
   ,Rebate_Amt_Buy_Max 
   ,Rebate_Amt_Buy_Delta 
   ,Rebate_Amt_Buy_Mean
   ,Rebate_Amt_Buy_Median
   ,Rebate_Amt_Buy_Mode
   ,Rebate_Amt_Buy_Stdev
   ,Rebate_Amt_Buy_Var
-----   
   ,Rebate_Amt_Sell_Min 
   ,Rebate_Amt_Sell_Max    
   ,Rebate_Amt_Sell_Delta 
   ,Rebate_Amt_Sell_Mean
   ,Rebate_Amt_Sell_Median
   ,Rebate_Amt_Sell_Mode
   ,Rebate_Amt_Sell_Stdev
   ,Rebate_Amt_Sell_Var   
-----             
   ,Prd_Cnt 
   ,Whse_Cnt
   ,Cust_Cnt
  )
------------------------------------------------------
SELECT
	 B.JOB_FK
	,B.SCENARIO 
	,B.PRD_FOCUS_DN_FK
	,B.PRD_FOCUS_NAME
	,B.PRD_FOCUS_VALUE
	,B.WHSE_FOCUS_DN_FK
	,B.WHSE_FOCUS_NAME
	,B.WHSE_FOCUS_VALUE
	,B.CUST_FOCUS_DN_FK
	,B.CUST_FOCUS_NAME
	,B.CUST_FOCUS_VALUE	
	,B.SALES_FOCUS_DN_FK
	,B.SALES_FOCUS_NAME
	,B.SALES_FOCUS_VALUE	
    ,B.Terr_Focus_Value 
    ,B.PrcMgr_Focus_Value 
    ,B.Buyer_Focus_Value 
    ,B.Bottomline_Focus_Value 
    ,B.Strategy_Focus_Value
    ,B.Rule_Focus_Value 
-----
   ,B.Margin_Min 
   ,B.Margin_Max      
   ,CASE WHEN ISNULL ( b.margin_min, 0 ) = 0 
    THEN CASE WHEN ISNULL ( b.margin_max, 0 ) = 0 
		 THEN 0
		 ELSE ( ( B.margin_max - b.margin_min ) / b.margin_max )
		 END 
	ELSE 100
    END AS Margin_Delta
   ,B.Margin_Mean
   ,B.Margin_Median
   ,B.Margin_Mode
   ,B.Margin_Stdev   
   ,B.Margin_Var  
-----   
   ,B.Margin_Pct_Min
   ,B.Margin_Pct_Max
   ,CASE WHEN ISNULL ( b.Margin_Pct_min, 0 ) = 0 
    THEN CASE WHEN ISNULL ( b.Margin_Pct_max, 0 ) = 0 
		 THEN 0
		 ELSE ( ( B.Margin_Pct_max - b.Margin_Pct_min ) / b.Margin_Pct_max ) 
		 END 
	ELSE 100
    END AS Margin_Pct_Delta
   ,B.Margin_Pct_Mean
   ,B.Margin_Pct_Median
   ,B.Margin_Pct_Mode
   ,B.Margin_Pct_Stdev
   ,B.Margin_Pct_Var 
-----   
   ,B.Price_Min 
   ,B.Price_Max 
   ,CASE WHEN ISNULL ( b.Price_min, 0 ) = 0 
    THEN CASE WHEN ISNULL ( b.Price_max, 0 ) = 0 
		 THEN 0
		 ELSE ( ( B.Price_max - b.Price_min ) / b.Price_max ) 
		 END 
	ELSE 100
    END AS Price_Delta
   ,B.Price_Mean 
   ,B.Price_Median
   ,B.Price_Mode
   ,B.Price_Stdev
   ,B.Price_Var
-----   
   ,B.COGS_Min 
   ,B.COGS_Max 
   ,CASE WHEN ISNULL ( b.COGS_min, 0 ) = 0 
    THEN CASE WHEN ISNULL ( b.COGS_max, 0 ) = 0 
		 THEN 0
		 ELSE ( ( B.COGS_max - b.COGS_min ) / b.COGS_max ) 
		 END 
	ELSE 100
    END AS COGS_Delta
   ,B.COGS_Mean
   ,B.COGS_Median
   ,B.COGS_Mode
   ,B.COGS_Stdev
   ,B.COGS_var
-----   
   ,B.Sales_Qty_Min 
   ,B.Sales_Qty_Max 
   ,CASE WHEN ISNULL ( b.Sales_Qty_min, 0 ) = 0 
    THEN CASE WHEN ISNULL ( b.Sales_Qty_max, 0 ) = 0 
		 THEN 0
		 ELSE ( ( B.Sales_Qty_max - b.Sales_Qty_min ) / b.Sales_Qty_max ) 
		 END 
	ELSE 100
    END AS Sales_Qty_Delta
   ,B.Sales_Qty_Mean  
   ,B.Sales_Qty_Median
   ,B.Sales_Qty_Mode
   ,B.Sales_Qty_Stdev
   ,B.Sales_Qty_Var    
-----     
   ,B.Sales_Dollars_Min 
   ,B.Sales_Dollars_Max 
   ,CASE WHEN ISNULL ( b.Sales_Dollars_min, 0 ) = 0 
    THEN CASE WHEN ISNULL ( b.Sales_Dollars_max, 0 ) = 0 
		 THEN 0
		 ELSE ( ( B.Sales_Dollars_max - b.Sales_Dollars_min ) / b.Sales_Dollars_max ) 
		 END 
	ELSE 100
    END AS Sales_Dollars_Delta
   ,B.Sales_Dollars_Mean  
   ,B.Sales_Dollars_Median
   ,B.Sales_Dollars_Mode
   ,B.Sales_Dollars_Stdev
   ,B.Sales_Dollars_Var   
-----         
   ,B.Sales_Transactions_Min 
   ,B.Sales_Transactions_Max 
   ,CASE WHEN ISNULL ( b.Sales_Transactions_min, 0 ) = 0 
    THEN CASE WHEN ISNULL ( b.Sales_Transactions_max, 0 ) = 0 
		 THEN 0
		 ELSE ( ( B.Sales_Transactions_max - b.Sales_Transactions_min ) / b.Sales_Transactions_max ) 
		 END 
	ELSE 100
    END AS Sales_Transactions_Delta
   ,B.Sales_Transactions_Mean  
   ,B.Sales_Transactions_Median
   ,B.Sales_Transactions_Mode
   ,B.Sales_Transactions_Stdev
   ,B.Sales_Transactions_Var   
-----            
   ,B.Repl_Cost_Min 
   ,B.Repl_Cost_Max    
   ,CASE WHEN ISNULL ( b.Repl_Cost_min, 0 ) = 0 
    THEN CASE WHEN ISNULL ( b.Repl_Cost_max, 0 ) = 0 
		 THEN 0
		 ELSE ( ( B.Repl_Cost_max - b.Repl_Cost_min ) / b.Repl_Cost_max ) 
		 END 
	ELSE 100
    END AS Repl_Cost_Delta
   ,B.Repl_Cost_Mean  
   ,B.Repl_Cost_Median 
   ,B.Repl_Cost_Mode
   ,B.Repl_Cost_Stdev  
   ,B.Repl_Cost_Var    
-----              
   ,B.Margin_Cost_Min 
   ,B.Margin_Cost_Max       
   ,CASE WHEN ISNULL ( b.Margin_Cost_min, 0 ) = 0 
    THEN CASE WHEN ISNULL ( b.Margin_Cost_max, 0 ) = 0 
		 THEN 0
		 ELSE ( ( B.Margin_Cost_max - b.Margin_Cost_min ) / b.Margin_Cost_max ) 
		 END 
	ELSE 100
    END AS Margin_Cost_Delta
   ,B.Margin_Cost_Mean             
   ,B.Margin_Cost_Median
   ,B.Margin_Cost_Mode
   ,B.Margin_Cost_Stdev 
   ,B.Margin_Cost_Var
-----   
   ,B.Rebate_Amt_Spa_Min 
   ,B.Rebate_Amt_Spa_Max 
   ,CASE WHEN ISNULL ( b.Rebate_Amt_SPA_min, 0 ) = 0 
    THEN CASE WHEN ISNULL ( b.Rebate_Amt_SPA_max, 0 ) = 0 
		 THEN 0
		 ELSE ( ( B.Rebate_Amt_SPA_max - b.Rebate_Amt_SPA_min ) / b.Rebate_Amt_SPA_max ) 
		 END 
	ELSE 100
    END AS Rebate_Amt_SPA_Delta
   ,B.Rebate_Amt_Spa_Mean          
   ,B.Rebate_Amt_Spa_Median        
   ,B.Rebate_Amt_Spa_Mode     
   ,B.Rebate_Amt_Spa_Stdev     
   ,B.Rebate_Amt_Spa_Var 
-----                       
   ,B.Rebate_Amt_Buy_Min 
   ,B.Rebate_Amt_Buy_Max 
   ,CASE WHEN ISNULL ( b.Rebate_Amt_Buy_min, 0 ) = 0 
    THEN CASE WHEN ISNULL ( b.Rebate_Amt_Buy_max, 0 ) = 0 
		 THEN 0
		 ELSE ( ( B.Rebate_Amt_Buy_max - b.Rebate_Amt_Buy_min ) / b.Rebate_Amt_Buy_max ) 
		 END 
	ELSE 100
    END AS Rebate_Amt_Buy_Delta
   ,B.Rebate_Amt_Buy_Mean
   ,B.Rebate_Amt_Buy_Median
   ,B.Rebate_Amt_Buy_Mode
   ,B.Rebate_Amt_Buy_Stdev
   ,B.Rebate_Amt_Buy_Var
-----   
   ,B.Rebate_Amt_Sell_Min 
   ,B.Rebate_Amt_Sell_Max   
   ,CASE WHEN ISNULL ( b.Rebate_Amt_Sell_min, 0 ) = 0 
    THEN CASE WHEN ISNULL ( b.Rebate_Amt_Sell_max, 0 ) = 0 
		 THEN 0
		 ELSE ( ( B.Rebate_Amt_Sell_max - b.Rebate_Amt_Sell_min ) / b.Rebate_Amt_Sell_max ) 
		 END 
	ELSE 100
    END AS Rebate_Amt_Sell_Delta
   ,B.Rebate_Amt_Sell_Mean
   ,B.Rebate_Amt_Sell_Median
   ,B.Rebate_Amt_Sell_Mode
   ,B.Rebate_Amt_Sell_Stdev
   ,B.Rebate_Amt_Sell_Var  
-----              
   ,B.Prd_Cnt 
   ,B.Whse_Cnt
   ,B.Cust_Cnt
----------------------------------
FROM (
SELECT
	 A.JOB_FK
	,A.SCENARIO
	,@PrdFocusDN AS PRD_FOCUS_DN_FK
	,A.PRD_FOCUS_NAME
	,A.PRD_FOCUS_VALUE     
	,@WhseFocusDN AS WHSE_FOCUS_DN_FK
	,A.WHSE_FOCUS_NAME
	,A.WHSE_FOCUS_VALUE     
	,@CustFocusDN AS CUST_FOCUS_DN_FK
	,A.CUST_FOCUS_NAME
	,A.CUST_FOCUS_VALUE   
	,@SaleFocusDN AS SALES_FOCUS_DN_FK
	,A.SALES_FOCUS_NAME
	,A.SALES_FOCUS_VALUE   
    ,A.Terr_Focus_Value 
    ,A.PrcMgr_Focus_Value 
    ,A.Buyer_Focus_Value 
    ,A.Bottomline_Focus_Value 
    ,A.Strategy_Focus_Value
    ,A.Rule_Focus_Value 
-----	
	,Cast(min(A.Margin) as Numeric(8,2)) AS margin_min
	,Cast(max(A.Margin) as Numeric(8,2)) AS margin_max
	,Cast(avg(A.Margin) as Numeric(8,2)) AS margin_mean
	,Cast(avg(A.Margin) as Numeric(8,2)) AS margin_median
	,Cast(avg(A.Margin) as Numeric(8,2)) AS margin_mode
	,Cast(stdev(A.Margin) as Numeric(8,2)) AS margin_stdev
	,Cast(var(A.Margin) as Numeric(8,2)) AS margin_var		
-----
	,Cast(min(A.Margin_Pct) as Numeric(8,2)) AS margin_pct_min	
	,Cast(max(A.Margin_Pct) as Numeric(8,2)) AS margin_pct_max
	,Cast(avg(A.Margin_Pct) as Numeric(8,2)) AS margin_pct_mean
	,Cast(avg(A.Margin_Pct) as Numeric(8,2)) AS margin_pct_median
	,Cast(avg(A.Margin_Pct) as Numeric(8,2)) AS margin_pct_mode	
	,Cast(stdev(A.Margin_Pct) as Numeric(8,2)) AS margin_pct_stdev	
	,Cast(var(A.Margin_Pct) as Numeric(8,2)) AS margin_pct_var
-----			
	,Cast(min(A.Price) as Numeric(8,2)) AS price_min
	,Cast(max(A.Price) as Numeric(8,2)) AS price_max
	,Cast(avg(A.Price) as Numeric(8,2)) AS price_mean	
	,Cast(avg(A.Price) as Numeric(8,2)) AS price_median	
	,Cast(avg(A.Price) as Numeric(8,2)) AS price_mode
	,Cast(stdev(A.Price) as Numeric(8,2)) AS price_stdev
	,Cast(var(A.Price) as Numeric(8,2)) AS price_var			
-----
	,Cast(min(A.COGS) as Numeric(8,2)) AS COGS_min
	,Cast(max(A.COGS) as Numeric(8,2)) AS COGS_max
	,Cast(avg(A.COGS) as Numeric(8,2)) AS COGS_mean
	,Cast(avg(A.COGS) as Numeric(8,2)) AS COGS_median
	,Cast(avg(A.COGS) as Numeric(8,2)) AS COGS_mode
	,Cast(stdev(A.COGS) as Numeric(8,2)) AS COGS_stdev
	,Cast(var(A.COGS) as Numeric(8,2)) AS COGS_var		
-----	
	,Cast(min(A.Sales_Qty) as Numeric(8,2)) AS sales_qty_min
	,Cast(max(A.Sales_Qty) as Numeric(8,2)) AS sales_qty_max
	,Cast(avg(A.Sales_Qty) as Numeric(8,2)) AS sales_qty_mean
	,Cast(avg(A.Sales_Qty) as Numeric(8,2)) AS sales_qty_median
	,Cast(avg(A.Sales_Qty) as Numeric(8,2)) AS sales_qty_mode
	,Cast(stdev(A.Sales_Qty) as Numeric(8,2)) AS sales_qty_stdev	
	,Cast(var(A.Sales_Qty) as Numeric(8,2)) AS sales_qty_var	
-----	
	,Cast(min(A.Sales_Dollars) as Numeric(8,2)) AS sales_Dollars_min
	,Cast(max(A.Sales_Dollars) as Numeric(8,2)) AS sales_Dollars_max
	,Cast(avg(A.Sales_Dollars) as Numeric(8,2)) AS sales_Dollars_mean
	,Cast(avg(A.Sales_Dollars) as Numeric(8,2)) AS sales_Dollars_median
	,Cast(avg(A.Sales_Dollars) as Numeric(8,2)) AS sales_Dollars_mode
	,Cast(stdev(A.Sales_Dollars) as Numeric(8,2)) AS sales_Dollars_stdev	
	,Cast(var(A.Sales_Dollars) as Numeric(8,2)) AS sales_Dollars_var					
-----	
	,Cast(min(A.Sales_Transaction_Cnt) as Numeric(8,2)) AS sales_Transactions_min
	,Cast(max(A.Sales_Transaction_Cnt) as Numeric(8,2)) AS sales_Transactions_max
	,Cast(avg(A.Sales_Transaction_Cnt) as Numeric(8,2)) AS sales_Transactions_mean
	,Cast(avg(A.Sales_Transaction_Cnt) as Numeric(8,2)) AS sales_Transactions_median
	,Cast(avg(A.Sales_Transaction_Cnt) as Numeric(8,2)) AS sales_Transactions_mode
	,Cast(stdev(A.Sales_Transaction_Cnt) as Numeric(8,2)) AS sales_Transactions_stdev	
	,Cast(var(A.Sales_Transaction_Cnt) as Numeric(8,2)) AS sales_Transactions_var						
-----
	,Cast(min(A.Repl_Cost) as Numeric(8,2)) AS Repl_Cost_min
	,Cast(max(A.Repl_Cost) as Numeric(8,2)) AS Repl_Cost_max
	,Cast(avg(A.Repl_Cost) as Numeric(8,2)) AS Repl_Cost_mean
	,Cast(avg(A.Repl_Cost) as Numeric(8,2)) AS Repl_Cost_median
	,Cast(avg(A.Repl_Cost) as Numeric(8,2)) AS Repl_Cost_mode	
	,Cast(stdev(A.Repl_Cost) as Numeric(8,2)) AS Repl_Cost_stdev
	,Cast(var(A.Repl_Cost) as Numeric(8,2)) AS Repl_Cost_var		
-----		
	,Cast(min(A.Margin_Cost) as Numeric(8,2)) AS Margin_Cost_min
	,Cast(max(A.Margin_Cost) as Numeric(8,2)) AS Margin_Cost_max	
	,Cast(avg(A.Margin_Cost) as Numeric(8,2)) AS Margin_Cost_mean		
	,Cast(avg(A.Margin_Cost) as Numeric(8,2)) AS Margin_Cost_median
	,Cast(avg(A.Margin_Cost) as Numeric(8,2)) AS Margin_Cost_mode
	,Cast(stdev(A.Margin_Cost) as Numeric(8,2)) AS Margin_Cost_stdev
	,Cast(var(A.Margin_Cost) as Numeric(8,2)) AS Margin_Cost_var		
-----	
	,Cast(min(A.Rebate_Amt_Spa) as Numeric(8,2)) AS Rebate_Amt_Spa_Min	
	,Cast(max(A.Rebate_Amt_Spa) as Numeric(8,2)) AS Rebate_Amt_Spa_Max
	,Cast(avg(A.Rebate_Amt_Spa) as Numeric(8,2)) AS Rebate_Amt_Spa_mean
	,Cast(avg(A.Rebate_Amt_Spa) as Numeric(8,2)) AS Rebate_Amt_Spa_median
	,Cast(avg(A.Rebate_Amt_Spa) as Numeric(8,2)) AS Rebate_Amt_Spa_mode		
	,Cast(stdev(A.Rebate_Amt_Spa) as Numeric(8,2)) AS Rebate_Amt_Spa_stdev
	,Cast(var(A.Rebate_Amt_Spa) as Numeric(8,2)) AS Rebate_Amt_Spa_var		
-----	
	,Cast(min(A.Rebate_Amt_Buy) as Numeric(8,2)) AS Rebate_Amt_Buy_Min	
	,Cast(max(A.Rebate_Amt_Buy) as Numeric(8,2)) AS Rebate_Amt_Buy_Max
	,Cast(avg(A.Rebate_Amt_Buy) as Numeric(8,2)) AS Rebate_Amt_Buy_Mean
	,Cast(avg(A.Rebate_Amt_Buy) as Numeric(8,2)) AS Rebate_Amt_Buy_Median		
	,Cast(avg(A.Rebate_Amt_Buy) as Numeric(8,2)) AS Rebate_Amt_Buy_mode	
	,Cast(stdev(A.Rebate_Amt_Buy) as Numeric(8,2)) AS Rebate_Amt_Buy_stdev	
	,Cast(var(A.Rebate_Amt_Buy) as Numeric(8,2)) AS Rebate_Amt_Buy_var		
-----	
	,Cast(min(A.Rebate_Amt_Sell) as Numeric(8,2)) AS Rebate_Amt_Sell_Min	
	,Cast(max(A.Rebate_Amt_Sell) as Numeric(8,2)) AS Rebate_Amt_Sell_Max	
	,Cast(avg(A.Rebate_Amt_Sell) as Numeric(8,2)) AS Rebate_Amt_Sell_Mean
	,Cast(avg(A.Rebate_Amt_Sell) as Numeric(8,2)) AS Rebate_Amt_Sell_Median		
	,Cast(avg(A.Rebate_Amt_Sell) as Numeric(8,2)) AS Rebate_Amt_Sell_Mode
	,Cast(stdev(A.Rebate_Amt_Sell) as Numeric(8,2)) AS Rebate_Amt_Sell_stdev
	,Cast(var(A.Rebate_Amt_Sell) as Numeric(8,2)) AS Rebate_Amt_Sell_var		
	,COUNT  ( DISTINCT a.product_structure_fk )     AS Prd_Cnt 
	,COUNT  ( DISTINCT a.org_entity_structure_fk )  AS Whse_Cnt
	,COUNT  ( DISTINCT a.cust_entity_structure_fk ) AS Cust_Cnt
FROM (
---------------------------
SELECT 
       AR.JOB_FK
      ,( SELECT SHORT_NAME FROM marginmgr.ANALYSIS_SCENARIO WITH (NOLOCK)
	     WHERE ANALYSIS_SCENARIO_ID = AR.ANALYSIS_SCENARIO_FK ) AS SCENARIO
      ,AR.PRODUCT_STRUCTURE_FK
      ,AR.ORG_ENTITY_STRUCTURE_FK
      ,AR.CUST_ENTITY_STRUCTURE_FK
	  ,dnprd.SHORT_NAME AS PRD_FOCUS_NAME
	  ,CASE @PrdFocusDN
	   WHEN 149099
	   THEN NULL
	   ELSE ( SELECT vcpd.current_data FROM epacube.V_CURRENT_PRODUCT_DATA vcpd
			     WHERE vcpd.data_name_fk = @PrdFocusDN
			     AND   vcpd.product_structure_fk = AR.product_structure_fk
				 AND   (   dsprd.org_ind IS NULL
						OR vcpd.org_entity_structure_fk = ar.org_entity_structure_fk ) ) 
		END PRD_FOCUS_VALUE
	  ,dnwhse.SHORT_NAME AS WHSE_FOCUS_NAME
	  ,CASE @WhseFocusDN
	   WHEN 141099
	   THEN NULL
	   ELSE ( SELECT ei.value FROM epacube.entity_identification ei
				WHERE  ei.data_name_fk = @WhseFocusDN
				AND    ei.entity_structure_fk = ar.org_entity_structure_fk )  
		END WHSE_FOCUS_VALUE
	  ,dncust.SHORT_NAME AS CUST_FOCUS_NAME
	  ,CASE @CustFocusDN
	   WHEN 144099
	   THEN NULL
	   ELSE  ( SELECT vced.current_data FROM epacube.V_CURRENT_ENTITY_DATA vced
				WHERE  vced.data_name_fk = @CustFocusDN
				AND    vced.entity_structure_fk = ar.cust_entity_structure_fk )  
		END CUST_FOCUS_VALUE
	  ,dnsales.SHORT_NAME AS SALES_FOCUS_NAME			
	  ,CASE @SaleFocusDN
	   WHEN 144099
	   THEN NULL
	   ELSE  ( SELECT vced.current_data FROM epacube.V_CURRENT_ENTITY_DATA vced
				WHERE  vced.data_name_fk = @SaleFocusDN
				AND    vced.entity_structure_fk = ar.sales_entity_structure_fk )  
		END SALES_FOCUS_VALUE	
--------
    ,'N/A' AS Terr_Focus_Value 
    ,'N/A' AS PrcMgr_Focus_Value 
    ,'N/A' AS Buyer_Focus_Value 
    ,'N/A' AS Bottomline_Focus_Value 
    ,'N/A' AS Strategy_Focus_Value
    ,'N/A' AS Rule_Focus_Value 		
---------
      ,[UNIT_MARGIN] AS margin 
      ,[MARGIN_PERCENT] AS margin_pct 
      ,[UNIT_PRICE] AS price 
      ,[UNIT_COGS] AS cogs 
      ,[SALES_QTY] 
      ,[SALES_DOLLARS] 
      ,[SALES_TRANSACTION_CNT] 
      ,[UNIT_REPL_COST] AS repl_cost 
      ,[UNIT_Margin_Cost] AS Margin_Cost 
      ,[UNIT_REBATE_AMT_SPA] AS rebate_amt_spa 
      ,[UNIT_REBATE_AMT_BUY] AS rebate_amt_buy
      ,[UNIT_REBATE_AMT_SELL]  AS rebate_amt_sell    
----------
FROM marginmgr.ANALYSIS_RESULTS AR WITH (NOLOCK)
LEFT JOIN epacube.data_name dnprd WITH (NOLOCK) ON ( dnprd.data_name_id = @PrdFocusDN )
LEFT JOIN epacube.data_set dsprd WITH (NOLOCK) ON ( dsprd.data_set_id = dnprd.data_set_fk )
LEFT JOIN epacube.data_name dnWhse WITH (NOLOCK) ON ( dnwhse.data_name_id = @WhseFocusDN )
LEFT JOIN epacube.data_name dnCust WITH (NOLOCK) ON ( dncust.data_name_id = @CustFocusDN )
LEFT JOIN epacube.data_name dnSales WITH (NOLOCK) ON ( dncust.data_name_id = @SaleFocusDN )
---------  inner join for price, cost,.... blah blah..
WHERE job_fk = @JobFK
AND   (   @Scenario_fk IS NULL
      OR  @Scenario_fk = AR.analysis_scenario_fk )
----
----   PRODUCT FILTER
AND   (   @PrdFilterDN IS NULL
      OR  AR.PRODUCT_STRUCTURE_FK IN (  SELECT VCPD.PRODUCT_STRUCTURE_FK
                                        FROM epacube.V_CURRENT_PRODUCT_DATA VCPD
                                        WHERE VCPD.DATA_NAME_FK = @PrdFilterDN
                                        AND   VCPD.CURRENT_DATA = @PrdFilterValue )
      )
----   WHSE FILTER
AND   (   @WhseFilterDN IS NULL
      OR  AR.ORG_ENTITY_STRUCTURE_FK IN (  SELECT EI.ENTITY_STRUCTURE_FK
                                           FROM epacube.ENTITY_IDENTIFICATION EI
                                           WHERE EI.DATA_NAME_FK = @WhseFilterDN
                                           AND   EI.VALUE = @WhseFilterValue )
      )
----   CUST FILTER
AND   (   @CustFilterDN IS NULL
      OR  AR.CUST_ENTITY_STRUCTURE_FK IN (  SELECT VCED.ENTITY_STRUCTURE_FK
                                            FROM epacube.V_CURRENT_PRODUCT_DATA VCED
                                            WHERE VCED.DATA_NAME_FK = @CustFilterDN
                                            AND   VCED.CURRENT_DATA = @CustFilterValue )
      )
----
) A
GROUP BY 	 A.JOB_FK, A.SCENARIO, 
             A.PRD_FOCUS_NAME, A.PRD_FOCUS_VALUE,
             A.WHSE_FOCUS_NAME, A.WHSE_FOCUS_VALUE,
             A.CUST_FOCUS_NAME, A.CUST_FOCUS_VALUE,
             A.SALES_FOCUS_NAME, A.SALES_FOCUS_VALUE,
             A.Terr_Focus_Value, A.PrcMgr_Focus_Value,
             A.Buyer_Focus_Value, A.Bottomline_Focus_Value,
             A.Strategy_Focus_Value, A.Rule_Focus_Value 
             
) B


  
  
  
  
  
  
  
  
  
  
  
/*  
------------------------------------------------------
SELECT
	 B.PRD_FOCUS_DN_FK
	,B.PRD_FOCUS_NAME
	,B.PRD_FOCUS_VALUE
	,B.WHSE_FOCUS_DN_FK
	,B.WHSE_FOCUS_NAME
	,B.WHSE_FOCUS_VALUE
	,B.CUST_FOCUS_DN_FK
	,B.CUST_FOCUS_NAME
	,B.CUST_FOCUS_VALUE	
    ,B.Margin_Min 
    ,CASE WHEN B.Margin_Max = B.Margin_Min 
     THEN NULL
     ELSE B.Margin_Max
     END 
    ,CASE WHEN B.Margin_Min  = 0
     THEN NULL 
     ELSE ( B.Margin_Max - B.Margin_Min ) / B.Margin_Min                 
     END 
    ,B.Margin_Pct_Min
    ,CASE WHEN B.Margin_Pct_Max = B.Margin_Pct_Min 
     THEN NULL
     ELSE B.Margin_Pct_Max
     END 
    ,CASE WHEN B.Margin_Pct_Min  = 0
     THEN NULL 
     ELSE ( B.Margin_Pct_Max - B.Margin_Pct_Min ) / B.Margin_Pct_Min                 
     END      
    ,B.Price_Min
    ,CASE WHEN B.Price_Max = B.Price_Min 
     THEN NULL
     ELSE B.Price_Max
     END 
    ,CASE WHEN B.Price_Min = 0
     THEN 0 
     ELSE ( B.Price_Max - B.Price_Min ) / B.Price_Min                      
     END 
    ,B.COGS_Min 
    ,CASE WHEN B.COGS_Max = B.COGS_Min 
     THEN NULL
     ELSE B.COGS_Max
     END 
    ,CASE WHEN B.COGS_Min = 0
     THEN 0 
     ELSE ( B.COGS_Max - B.COGS_Min ) / B.COGS_Min                      
     END      
    ,B.Repl_Cost_Min 
    ,CASE WHEN B.Repl_Cost_Max = B.Repl_Cost_Min 
     THEN NULL
     ELSE B.Repl_Cost_Max
     END      
    ,B.Margin_Cost_Min 
    ,CASE WHEN B.Margin_Cost_Max = B.Margin_Cost_Min 
     THEN NULL
     ELSE B.Margin_Cost_Max
     END           
    ,B.Rebate_Amt_Spa_Min 
    ,CASE WHEN B.Rebate_Amt_Spa_Max = B.Rebate_Amt_Spa_Min 
     THEN NULL
     ELSE B.Rebate_Amt_Spa_Max
     END 
    ,B.Rebate_Amt_Buy_Min 
    ,CASE WHEN B.Rebate_Amt_Buy_Max = B.Rebate_Amt_Buy_Min 
     THEN NULL
     ELSE B.Rebate_Amt_Buy_Max
     END 
    ,B.Rebate_Amt_Sell_Min 
    ,CASE WHEN B.Rebate_Amt_Sell_Max = B.Rebate_Amt_Sell_Min 
     THEN NULL
     ELSE B.Rebate_Amt_Sell_Max
     END      
   ,B.Sales_Qty_Min
   ,CASE WHEN B.Sales_Qty_Max = B.Sales_Qty_Min
    THEN NULL 
    ELSE B.Sales_Qty_Max
    END      
   ,B.Prd_Cnt 
   ,B.Whse_Cnt 
   ,B.Cust_Cnt      
   ,B.Last_Sell_Date
----------------------------------
FROM (
SELECT
     @PrdFocusDN AS PRD_FOCUS_DN_FK
	,A.PRD_FOCUS_NAME
	,A.PRD_FOCUS_VALUE     
	,@WhseFocusDN AS WHSE_FOCUS_DN_FK
	,A.WHSE_FOCUS_NAME
	,A.WHSE_FOCUS_VALUE     
	,@CustFocusDN AS CUST_FOCUS_DN_FK
	,A.CUST_FOCUS_NAME
	,A.CUST_FOCUS_VALUE     	
	,Cast(min(A.Margin) as Numeric(8,2)) AS margin_min
	,Cast(max(A.Margin) as Numeric(8,2)) AS margin_max
	,Cast(min(A.Margin_Pct) as Numeric(8,2)) AS margin_pct_min	
	,Cast(max(A.Margin_Pct) as Numeric(8,2)) AS margin_pct_max
	,Cast(min(A.Price) as Numeric(8,2)) AS price_min
	,Cast(max(A.Price) as Numeric(8,2)) AS price_max
	,Cast(min(A.COGS) as Numeric(8,2)) AS COGS_min
	,Cast(max(A.COGS) as Numeric(8,2)) AS COGS_max
	,Cast(min(A.Repl_Cost) as Numeric(8,2)) AS Repl_Cost_min
	,Cast(max(A.Repl_Cost) as Numeric(8,2)) AS Repl_Cost_max
	,Cast(min(A.Margin_Cost) as Numeric(8,2)) AS Margin_Cost_min
	,Cast(max(A.Margin_Cost) as Numeric(8,2)) AS Margin_Cost_max	
	,Cast(min(A.Rebate_Amt_Spa) as Numeric(8,2)) AS Rebate_Amt_Spa_Min	
	,Cast(max(A.Rebate_Amt_Spa) as Numeric(8,2)) AS Rebate_Amt_Spa_Max
	,Cast(min(A.Rebate_Amt_Buy) as Numeric(8,2)) AS Rebate_Amt_Buy_Min	
	,Cast(max(A.Rebate_Amt_Buy) as Numeric(8,2)) AS Rebate_Amt_Buy_Max
	,Cast(min(A.Rebate_Amt_Sell) as Numeric(8,2)) AS Rebate_Amt_Sell_Min	
	,Cast(max(A.Rebate_Amt_Sell) as Numeric(8,2)) AS Rebate_Amt_Sell_Max	
	,MIN ( a.sales_qty ) AS sales_qty_min		
	,MAX ( a.sales_qty ) AS sales_qty_max
	,COUNT  ( DISTINCT a.product_structure_fk )     AS Prd_Cnt 
	,COUNT  ( DISTINCT a.org_entity_structure_fk )  AS Whse_Cnt
	,COUNT  ( DISTINCT a.cust_entity_structure_fk ) AS Cust_Cnt	
	,MAX ( last_sell_date ) AS last_sell_date
FROM (
---------------------------
SELECT 
       MMR.PRODUCT_STRUCTURE_FK
      ,MMR.ORG_ENTITY_STRUCTURE_FK
      ,MMR.CUST_ENTITY_STRUCTURE_FK
	  ,dnprd.SHORT_NAME AS PRD_FOCUS_NAME
	  ,CASE @PrdFocusDN
	   WHEN 149099
	   THEN NULL
	   ELSE ( SELECT vcpd.current_data FROM epacube.V_CURRENT_PRODUCT_DATA vcpd
			     WHERE vcpd.data_name_fk = @PrdFocusDN
			     AND   vcpd.product_structure_fk = MMR.product_structure_fk
				 AND   (   dsprd.org_ind IS NULL
						OR vcpd.org_entity_structure_fk = MMR.org_entity_structure_fk ) ) 
		END PRD_FOCUS_VALUE
	  ,dnwhse.SHORT_NAME AS WHSE_FOCUS_NAME
	  ,CASE @WhseFocusDN
	   WHEN 141099
	   THEN NULL
	   ELSE ( SELECT ei.value FROM epacube.entity_identification ei
				WHERE  ei.data_name_fk = @WhseFocusDN
				AND    ei.entity_structure_fk = MMR.org_entity_structure_fk )  
		END WHSE_FOCUS_VALUE
	  ,dncust.SHORT_NAME AS CUST_FOCUS_NAME
	  ,CASE @CustFocusDN
	   WHEN 144099
	   THEN NULL
	   ELSE  ( SELECT vced.current_data FROM epacube.V_CURRENT_ENTITY_DATA vced
				WHERE  vced.data_name_fk = @CustFocusDN
				AND    vced.entity_structure_fk = MMR.cust_entity_structure_fk )  
		END CUST_FOCUS_VALUE	   
      ,[MARGIN]
      ,[MARGIN_PERCENT] AS MARGIN_PCT
      ,[PRICE]
      ,[COGS]
      ,[REPL_COST]
      ,[Margin_Cost]      
      ,[REBATE_AMT_SPA]
      ,[REBATE_AMT_BUY]
      ,[REBATE_AMT_SELL_SIDE]  AS rebate_amt_sell    
      ,[SALES_TOTAL_QTY] AS SALES_QTY
      ,[LAST_SELL_DATE]      
  FROM [epacube].[marginmgr].[MARGIN_RESULTS] MMR WITH (NOLOCK)
INNER JOIN epacube.data_name dnprd WITH (NOLOCK) ON ( dnprd.data_name_id = @PrdFocusDN )
INNER JOIN epacube.data_set dsprd WITH (NOLOCK) ON ( dsprd.data_set_id = dnprd.data_set_fk )
INNER JOIN epacube.data_name dnWhse WITH (NOLOCK) ON ( dnwhse.data_name_id = @WhseFocusDN )
INNER JOIN epacube.data_name dnCust WITH (NOLOCK) ON ( dncust.data_name_id = @CustFocusDN )
---------  inner join for price, cost,.... blah blah..
WHERE 1 = 1 
AND   (     @LastSellDate_Begin IS NULL 
       OR  (    @LastSellDate_End IS NULL 
            AND LAST_SELL_DATE > @LastSellDate_Begin )
       OR  (  LAST_SELL_DATE BETWEEN @LastSellDate_Begin AND @LastSellDate_End  )
      )
----
----   PRODUCT FILTER
AND   (   @PrdFilterDN IS NULL
      OR  MMR.PRODUCT_STRUCTURE_FK IN (  SELECT VCPD.PRODUCT_STRUCTURE_FK
                                        FROM epacube.V_CURRENT_PRODUCT_DATA VCPD
                                        WHERE VCPD.DATA_NAME_FK = @PrdFilterDN
                                        AND   VCPD.CURRENT_DATA = @PrdFilterValue )
      )
----   WHSE FILTER
AND   (   @WhseFilterDN IS NULL
      OR  MMR.ORG_ENTITY_STRUCTURE_FK IN (  SELECT EI.ENTITY_STRUCTURE_FK
                                           FROM epacube.ENTITY_IDENTIFICATION EI
                                           WHERE EI.DATA_NAME_FK = @WhseFilterDN
                                           AND   EI.VALUE = @WhseFilterValue )
      )
----   CUST FILTER
AND   (   @CustFilterDN IS NULL
      OR  MMR.CUST_ENTITY_STRUCTURE_FK IN (  SELECT VCED.ENTITY_STRUCTURE_FK
                                            FROM epacube.V_CURRENT_PRODUCT_DATA VCED
                                            WHERE VCED.DATA_NAME_FK = @CustFilterDN
                                            AND   VCED.CURRENT_DATA = @CustFilterValue )
      )
----
) A
GROUP BY 	 A.PRD_FOCUS_NAME, A.PRD_FOCUS_VALUE,
             A.WHSE_FOCUS_NAME, A.WHSE_FOCUS_VALUE,
             A.CUST_FOCUS_NAME, A.CUST_FOCUS_VALUE

) B

---- and grouping


*/


    
    RETURN
END;
