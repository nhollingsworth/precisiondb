﻿--/****** Object:  StoredProcedure [marginmgr].[RETAIL_PRICE_MAINTENANCE_ZONE_FILTER]    Script Date: 6/9/2018 4:01:00 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		<Author,,Name>
---- Create date: <Create Date,,>
---- Description:	<Description,,>
---- =============================================
CREATE PROCEDURE [marginmgr].[RETAIL_PRICE_MAINTENANCE_ZONE_FILTER] 
	@FILTER_TYPE VARCHAR(64) = 'PRICE_CHANGE_EFFECTIVE_DATE'
	, @PRICE_CHANGE_EFFECTIVE_DATE DATE = NULL
	, @User varchar(64) = Null
AS

--If object_id('dbo.User_Called') is not null
--drop table dbo.User_Called

--Select @FILTER_TYPE 'Filter_Type', @PRICE_CHANGE_EFFECTIVE_DATE 'PRICE_CHANGE_EFFECTIVE_DATE', @user 'User_Called' into dbo.User_Called

/*
OPTIONS"
@FILTER_TYPE = 'ZONE', 'ZONESETTINGS', 'PRICE_CHANGE_EFFECTIVE_DATE', 'COUNTS', 'PRICE_MGMT_TYPE', 'QUALIFIED_STATUS', 'ITEM_CHANGE_TYPE'

*/

--Declare @FILTER_TYPE VARCHAR(64) = 'ZONESETTINGS'
--Declare @PRICE_CHANGE_EFFECTIVE_DATE DATE = NULL
--Declare @User varchar(64) = 'gstone@epacube.com'

	SET NOCOUNT ON;

--Set @user = Case when isnull(@user, '') = '' then 'gstone@epacube.com' else @user end

IF @PRICE_CHANGE_EFFECTIVE_DATE IS NULL
	SET @PRICE_CHANGE_EFFECTIVE_DATE = CAST(getdate() + 6 - datepart(dw, getdate()) AS DATE)

IF @FILTER_TYPE = 'ZONE'
Begin
		Select ZONE_ENTITY_STRUCTURE_FK, ZONE, ZONE_NAME, count(*) 'CHANGES' 
		from MARGINMGR.PRICESHEET_RETAIL_CHANGES prc WITH (NOLOCK) 
		inner join [epacube].[getEntitiesByPermission](@User, 151110) A1 on prc.ZONE_ENTITY_STRUCTURE_FK = a1.entity_structure_fk
		where 1 = 1
		and Cast(PRICE_CHANGE_EFFECTIVE_DATE as date) = @PRICE_CHANGE_EFFECTIVE_DATE
		group by ZONE_ENTITY_STRUCTURE_FK, ZONE, ZONE_NAME 
		--order by case isnumeric(right(zone_name, Charindex(' ',Reverse(zone_name)))) when 1 then cast(reverse(right(zone_name, Charindex(' ',Reverse(zone_name)))) as int) else zone end desc
		Order by 
		Case isnumeric(reverse(ltrim(rtrim(left(Reverse(zone_name), Charindex(' ',Reverse(zone_name))))))) when 1 then cast(reverse(ltrim(rtrim(left(Reverse(zone_name)
		, Charindex(' ',Reverse(zone_name)))))) as int) + 50 else Rank()over(order by zone_name desc) end desc

End
ELSE
IF @FILTER_TYPE = 'ZONESETTINGS'
		Select ei.entity_structure_fk 'ZONE_ENTITY_STRUCTURE_FK', ei.value 'ZONE', ein.value 'ZONE_NAME', NULL 'CHANGES' 
		from epacube.entity_identification ei with (nolock)
		inner join epacube.entity_ident_nonunique ein with (nolock) on ei.ENTITY_STRUCTURE_FK = ein.ENTITY_STRUCTURE_FK
		inner join epacube.entity_attribute ea with (nolock) on ei.entity_structure_fk = ea.ENTITY_STRUCTURE_FK and ea.data_name_fk = 502060
		inner join epacube.data_value dv with (nolock) on ea.DATA_VALUE_FK = dv.DATA_VALUE_ID
		inner join [epacube].[getEntitiesByPermission](@User, 151110) A1 on ei.ENTITY_STRUCTURE_FK = a1.entity_structure_fk
		--inner join [epacube].[getZonesByPermission](@User) A1 on ei.ENTITY_STRUCTURE_FK = a1.entity_structure_fk
		where 1 = 1
				and ei.entity_data_name_fk = 151000 
				and ein.data_name_fk = 140200
		Order by Case isnumeric(reverse(ltrim(rtrim(left(Reverse(ein.value), Charindex(' ',Reverse(ein.value))))))) when 1 then cast(reverse(ltrim(rtrim(left(Reverse(ein.value)
		, Charindex(' ',Reverse(ein.value)))))) as int) + 50 else Rank()over(order by ein.value desc) end desc
ELSE
IF @FILTER_TYPE = 'PRICE_CHANGE_EFFECTIVE_DATE'
	Select Cast(Price_Change_Effective_Date as Date) 'PRICE_CHANGE_EFFECTIVE_DATE' from MARGINMGR.PRICESHEET_RETAIL_CHANGES Group by Price_Change_Effective_Date
	Order by Cast(Price_Change_Effective_Date as Date) desc
	--Order by Case 
	--when datepart(ww, getdate()) = datepart(ww, Price_Change_Effective_Date) then 1
	--when Price_Change_Effective_Date > getdate() then 2 else 3 end
	--, Price_Change_Effective_Date Desc
ELSE
IF @FILTER_TYPE = 'COUNTS'
	Select 
	PRC.PRICE_MGMT_TYPE_CR_FK
	, CR1.CODE 'PRICE PROCESS'
	, PRC.ITEM_CHANGE_TYPE_CR_FK
	, CR2.CODE 'ITEMS'
	, COUNT(*) 'NOT REVIEWED'
	from MARGINMGR.PRICESHEET_RETAIL_CHANGES PRC
	INNER JOIN EPACUBE.CODE_REF CR1 WITH (NOLOCK) ON PRC.PRICE_MGMT_TYPE_CR_FK = CR1.CODE_REF_ID
	INNER JOIN EPACUBE.CODE_REF CR2 WITH (NOLOCK) ON PRC.ITEM_CHANGE_TYPE_CR_FK = CR2.CODE_REF_ID
	WHERE PRC.ITEM_CHANGE_TYPE_CR_FK NOT IN (660, 657)
	AND ISNULL(PRC.REVIEWED, 0) <> 1
	and Cast(PRICE_CHANGE_EFFECTIVE_DATE as date) = @PRICE_CHANGE_EFFECTIVE_DATE
	GROUP BY 
	PRC.PRICE_MGMT_TYPE_CR_FK
	, CR1.CODE
	, PRC.ITEM_CHANGE_TYPE_CR_FK
	, CR2.CODE
	ORDER BY PRC.PRICE_MGMT_TYPE_CR_FK
	, ITEM_CHANGE_TYPE_CR_FK

ELSE
IF @FILTER_TYPE IN ('PRICE_MGMT_TYPE', 'QUALIFIED_STATUS', 'ITEM_CHANGE_TYPE')
SELECT * FROM (
SELECT '0' 'CODE_REF_ID', 'ALL' 'CODE' UNION
SELECT CODE_REF_ID, CODE FROM EPACUBE.CODE_REF WHERE CODE_TYPE = @FILTER_TYPE AND CODE_REF_ID NOT IN (657, 660)) A
ORDER BY CASE WHEN CODE = 'ALL' THEN 1 ELSE 2 END, CODE DESC

