﻿






-- Copyright 2008
--
-- Procedure Called from Quartz to submit result calc job
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -----------------------------------------------
-- KS        09/17/2010   Revised Original SQL Version to include Queue
--
CREATE PROCEDURE [marginmgr].[RUN_CALC_RESULTS_SINCE_LAST_JOB_QUARTZ]
  AS
BEGIN

DECLARE @l_sysdate    datetime
DECLARE @ls_stmt      varchar (1000)
DECLARE @ls_queue     nvarchar (4000)
DECLARE @l_exec_no    bigint
DECLARE @l_rows_processed    BIGINT

DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

DECLARE @status_desc  varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

BEGIN TRY
SET NOCOUNT ON
SET @l_sysdate = getdate ()

SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of marginmgr.RUN_CALC_RESULTS_SINCE_LAST_JOB_QUARTZ', 
												@exec_id = @l_exec_no OUTPUT;


/**********************************************************************************/
--   CREATE NEW JOB 
/**********************************************************************************/



	SET @v_last_job_fk =  ( SELECT MAX ( JOB_ID )
							 FROM COMMON.JOB 
							 WHERE JOB_CLASS_FK = 400        --- RESULT CALCULATIONS
							 AND   NAME = 'RESULTS SINCE LAST JOB'  )

						 
	SET @v_last_job_date = 	ISNULL (
							( SELECT JOB_CREATE_TIMESTAMP
							  FROM COMMON.JOB 
							  WHERE JOB_ID = @v_last_job_fk )
                            ,'01/01/2000' )


      	   
    EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'job_seq', @v_job_fk output
    
    SET @v_job_class_fk  = 400        --- RESULT CALCULATIONS
    SET @v_job_user = 'EPACUBE'
    SET @v_job_name = 'RESULTS SINCE LAST JOB'
    SET @v_job_date = @l_sysdate
    SET @v_data1   = 'LAST JOB:: '
                     + CAST ( ISNULL ( @v_last_job_fk, -999 )  AS VARCHAR(32) )
                     + ' - '
                     + CAST ( ISNULL ( @v_last_job_date, '01/01/2000' ) AS VARCHAR(64) )
    SET @v_data2   = @v_last_job_date
    
    EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
										   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

    EXEC common.job_execution_create   @v_job_fk, 'Queue Calculation Job:: RESULTS SINCE LAST JOB ',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  



-------------------------------------------------------------------------------------
--   Call Calc Job on the Queue <<<<<------ Need to ENQUEUE HERE 
-------------------------------------------------------------------------------------


	--CINDY TESTING
----SET @ls_queue = 'EXEC Epacube.stage.STG_TO_EVENTS @in_job_fk = ' 
----              + CAST ( @v_job_fk AS VARCHAR(16)) 

	
SET @ls_queue = 'EXEC Epacube.marginmgr.RUN_CALC_RESULTS_SINCE_LAST_JOB @in_analysis_job_fk = ' 
              + CAST ( @v_job_fk AS VARCHAR(16)) 
			  + ',@v_last_job_fk  = ' 
			  + CAST ( @v_last_job_fk AS VARCHAR(16)) 
			  ----+ ',@v_last_job_date = ' 
			  ----+ CAST ( @v_last_job_date  AS VARCHAR(25))


INSERT INTO COMMON.T_SQL
    ( TS,  SQL_TEXT )
VALUES ( GETDATE(), ISNULL ( @ls_queue, 'NULL:: marginmgr.RUN_CALC_RESULTS_SINCE_LAST_JOB'  ) )


EXEC queue_manager.enqueue
	@service_name = N'TargetEpaService', --  nvarchar(50)
	@cmd_text = @ls_queue, --  nvarchar(4000)
	@epa_job_id = @v_job_fk, --  int
	@cmd_type = N'SQL' --  nvarchar(10)




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of marginmgr.RUN_CALC_RESULTS_SINCE_LAST_JOB_QUARTZ'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;


END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of marginmgr.RUN_CALC_RESULTS_SINCE_LAST_JOB_QUARTZ has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

  EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END




















