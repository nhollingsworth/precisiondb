﻿-- =============================================
-- Author:		Gary Stone
-- Create date: May 4, 2018
-- Description:	Update Zone_Price_Flag_Settings
-- =============================================
CREATE PROCEDURE [marginmgr].[Retail_Range_Setting_Zone]
	@Pricing_Date Date = getdate
	, @Zone_Entity_Structure_FK bigint = Null
	, @Prod_Segment_FK bigint = Null
	--, @Price_Type Varchar(16) = 'Regular'

AS
	SET NOCOUNT ON;

	--Declare @Pricing_Date Date = getdate()
	--, @Zone_Entity_Structure_FK bigint = 113246
	--, @Prod_Segment_FK bigint = 1014719248

--Set @Price_Type = Case @Price_Type when 'Regular' then 1 else 2 end

Select
Segments_Settings_ID
--, Zone_Entity_Structure_FK
--, [Zone]
--, [Zone_Name]
--, Prod_Segment_FK
--, Prod_Segment
--, Data_Name_FK
--, DESCRIPTION]
--, Effective_Date
, [Price_Type]
, [Round_To_Value]
, Lower_Limit
, Upper_Limit

--, ss.Attribute_Event_Data
--, ss.Precedence
--, ss.PRICE_TYPE_entity_data_value_fk
--, ss.Entity_Data_Name_FK

from (
Select
ss.Segments_Settings_ID
--ss.Zone_Entity_Structure_FK
--, eiz.value 'Zone'
--, einz.value 'Zone_Name'
--, ss.Prod_Segment_FK
--, dv.[description] Prod_Segment
--, ss.Data_Name_FK
--, edv.[DESCRIPTION]
, @Pricing_Date Effective_Date
, ss.Attribute_Number 'Round_To_Value'
, ss.Lower_Limit
, ss.Upper_Limit
, edv.value 'Price_Type'
--, ss.Attribute_Event_Data
--, ss.Precedence
--, ss.PRICE_TYPE_entity_data_value_fk
--, ss.Entity_Data_Name_FK
, Dense_Rank()over(partition by ss.zone_entity_structure_FK, ss.prod_segment_FK, ss.Attribute_Number, ss.PRICE_TYPE_entity_data_value_fk order by ss.effective_date desc, ss.update_timestamp desc) DRank_Eff
from epacube.epacube.segments_settings ss with (nolock)
inner join epacube.epacube.data_value dv with (nolock) on ss.prod_data_value_fk = dv.data_value_id
inner join epacube.epacube.entity_identification eiz with (nolock) on ss.ZONE_ENTITY_STRUCTURE_FK = eiz.ENTITY_STRUCTURE_FK and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.DATA_NAME_FK = 151110
inner join epacube.epacube.ENTITY_IDENT_NONUNIQUE einz with (nolock) on eiz.ENTITY_STRUCTURE_FK = einz.ENTITY_STRUCTURE_FK and eiz.entity_data_name_fk = einz.entity_data_name_fk and einz.data_name_fk = 140200
inner join epacube.epacube.entity_data_value edv with (nolock) on ss.PRICE_TYPE_entity_data_value_fk = edv.entity_data_value_ID
inner join epacube.epacube.data_value dv_pt with (nolock) on edv.DATA_VALUE_FK = dv_pt.DATA_VALUE_ID and dv_pt.data_name_fk = 502041 and dv_pt.value = '10'
where ss.data_name_fk = 144891
and ss.Effective_Date <= @Pricing_Date
and ss.ZONE_ENTITY_STRUCTURE_FK = @Zone_Entity_Structure_FK
and ss.PROD_SEGMENT_FK = @Prod_Segment_FK
and ss.cust_entity_structure_fk is null
and ss.RECORD_STATUS_CR_FK = 1
--and edv.value = @Price_Type
) A where DRank_Eff = 1
Order by Round_To_Value
