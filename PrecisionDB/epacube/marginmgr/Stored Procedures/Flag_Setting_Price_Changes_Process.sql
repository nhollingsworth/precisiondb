﻿-- =============================================
-- Author:		Gary Stone
-- Create date: April 9, 2018
-- Description:	Get Retail Price, Rounded and with Price Points
-- =============================================
CREATE PROCEDURE [marginmgr].[Flag_Setting_Price_Changes_Process]
	@PRICESHEET_RETAIL_CHANGES_ID bigint = Null
	, @FLAG VARCHAR(16)
	, @VALUE INT
	, @USER VARCHAR(96)
	, @AutocalculateTM int = 0
AS

	SET NOCOUNT ON;

/*
--OPTIONS FOR FLAG
--	SET_AS_TM
--	HOLD_CUR_PRICE
--	REVIEWED

	Declare @PRICESHEET_RETAIL_CHANGES_ID bigint = 10641856
	Declare @FLAG VARCHAR(16) = 'Reviewed' --'TM'
	Declare @VALUE INT = 0
	Declare @AutocalculateTM int = 0
	Declare @USER VARCHAR(96) = 'gstone@epacube.com'
*/

	Insert into marginmgr.PRICESHEET_RETAIL_CHANGES_USER_TRACE
	(
	[PROCEDURE_NAME]
	, DATA_STREAM
	, PRICESHEET_RETAIL_CHANGES_FK
	, CREATE_USER
	, FLAG
	, [VALUE]
	)
	Values
	(
		'marginmgr.Flag_Setting_Price_Changes'
		, 'In'
		, @PRICESHEET_RETAIL_CHANGES_ID
		, @User
		, @FLAG
		, @VALUE
	)

If (select price_change_effective_date from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID) < Cast(getdate() as date)
Begin
	Select PRICESHEET_RETAIL_CHANGES_ID, HOLD_CUR_PRICE 'HOLD_CUR_PRICE', SRP_Adjusted 'SRP_ADJUSTED', GM_Adjusted 'GM_ADJUSTED', TM_ADJUSTED 'TM_ADJUSTED', Reviewed 'REVIEWED'
	, case isnull(PRC.Price_Multiple_New, 1) when 1 then null else PRC.Price_Multiple_New end 'PRICE_MULTIPLE_NEW', 'Prior weeks''s work cannot be changed.' 'USER_MSG'
	from marginmgr.PRICESHEET_RETAIL_CHANGES PRC where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID

	goto eof
End

IF @fLAG = 'HOLD_CUR_PRICE'
	Begin
		Update PRC
		Set HOLD_CUR_PRICE = @VALUE
		, UPDATE_USER_HOLD = @USER
		, Update_timestamp = getdate()
		, Price_Multiple_New = Case @VALUE when 1 then Price_Multiple_Cur else Null end
		, SRP_Adjusted = Case @VALUE when 1 then PRICE_CUR else Null end
		, GM_Adjusted = Case @VALUE when 1 then cast((price_cur - (unit_cost_new * Price_Multiple_Cur)) / price_cur as Numeric(18, 4)) else Null end
		--, GM_Adjusted = Case @VALUE when 1 then CUR_GM_PCT else Null end
		, TM_Adjusted = Case	when @VALUE = 1 and @AutocalculateTM = 0 then prc.TARGET_MARGIN 
								when @Value = 1 and @AutocalculateTM = 1 then cast((price_cur - (unit_cost_new * Price_Multiple_Cur)) / price_cur as Numeric(18, 4))
						else Null end
		, Reviewed = 1
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID

	Select PRICESHEET_RETAIL_CHANGES_ID, @VALUE 'HOLD_CUR_PRICE', SRP_Adjusted 'SRP_ADJUSTED', GM_Adjusted 'GM_ADJUSTED', case when @value = 0 then Null when @AutocalculateTM = 0 then TM_Adjusted else cast((price_cur - (unit_cost_new * Price_Multiple_Cur)) / price_cur as Numeric(18, 4)) end 'TM_ADJUSTED', 1 'REVIEWED'
	, case isnull(PRC.Price_Multiple_New, 1) when 1 then null else PRC.Price_Multiple_New end 'PRICE_MULTIPLE_NEW', Null 'USER_MSG'
	from marginmgr.PRICESHEET_RETAIL_CHANGES PRC where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID

	End

ELSE

IF @fLAG = 'REVIEWED' and @VALUE = 1
	Begin
		Update PRC
		Set REVIEWED = @VALUE
		, UPDATE_USER_REVIEWED = @USER
		, Update_timestamp = getdate()
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID
	
		Select PRC.PRICESHEET_RETAIL_CHANGES_ID, Null 'HOLD_CUR_PRICE', Null 'SRP_ADJUSTED', Null 'GM_ADJUSTED', Null 'TM_ADJUSTED', 1 'REVIEWED'
		, case isnull(PRC.Price_Multiple_New, 1) when 1 then null else PRC.Price_Multiple_New end 'PRICE_MULTIPLE_NEW', Null 'USER_MSG'
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID
		--Select @PRICESHEET_RETAIL_CHANGES_ID 'PRICESHEET_RETAIL_CHANGES_ID', 1 'REVIEWED' 
	
	End
Else
IF @fLAG = 'REVIEWED' and isnull(@VALUE, 0) <> 1
	Begin
		Update PRC
		Set Reviewed = Null
		, UPDATE_USER_HOLD = Null
		, Update_timestamp = getdate()
		, Price_Multiple_New = Price_Multiple_Cur
		, SRP_Adjusted = Null
		, GM_Adjusted = Null
		, TM_Adjusted = Null
		, HOLD_CUR_PRICE = Null
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID
	
		Select PRICESHEET_RETAIL_CHANGES_ID, Null 'HOLD_CUR_PRICE', Null 'SRP_ADJUSTED', Null 'GM_ADJUSTED', Null 'TM_ADJUSTED', @VALUE 'REVIEWED'
		, case isnull(PRC.Price_Multiple_New, 1) when 1 then null else PRC.Price_Multiple_New end 'PRICE_MULTIPLE_NEW', Null 'USER_MSG'
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID
	End
eof:

	Insert into marginmgr.PRICESHEET_RETAIL_CHANGES_USER_TRACE
	(
	[PROCEDURE_NAME]
	, DATA_STREAM
	, PRICESHEET_RETAIL_CHANGES_FK
	, CREATE_USER
	, FLAG
	, SRP_ADJUSTED_OUT
	, GM_ADJUSTED_OUT
	, TM_ADJUSTED_OUT
	, PRICE_MULTIPLE_NEW_OUT
	, HOLD_CUR_PRICE_OUT
	, REVIEWED_OUT
	)
	Select
	'marginmgr.Flag_Setting_Price_Changes'
		, 'Out'
		, @PRICESHEET_RETAIL_CHANGES_ID
		, @User
		, @FLAG
		, SRP_ADJUSTED 
		, GM_ADJUSTED
		, TM_ADJUSTED
		, PRICE_MULTIPLE_NEW
		, HOLD_CUR_PRICE
		, REVIEWED
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC 
		where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID