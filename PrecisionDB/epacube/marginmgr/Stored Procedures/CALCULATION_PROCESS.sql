﻿--A_epaMAUI_CALCULATION_PROCESS


-- Copyright 2012
--
-- Procedure to combine all the desparate calculation processes
--
--    Assumes that the Analysis Job and All of its parameters have been established prior to executing
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -----------------------------------------------
-- KS        05/01/2012  Initial Version
-- GHS		 09/22/2013  Restructured Delete Statements for performance starting Line 129
--
CREATE PROCEDURE [marginmgr].[CALCULATION_PROCESS]
                 ( @in_analysis_job_fk BIGINT, @in_test_filter_only SMALLINT = NULL  )
AS
BEGIN

DECLARE @l_sysdate    datetime
DECLARE @ls_stmt      varchar (1000)
DECLARE @ls_queue     nvarchar (4000)
DECLARE @l_exec_no    bigint
DECLARE @l_rows_processed    BIGINT

DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @V_END_TIMESTAMP DATETIME
DECLARE @v_count      INT
DECLARE @v_common_job_fk BIGINT

DECLARE @status_desc  varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

BEGIN TRY
SET NOCOUNT ON
SET @l_sysdate = getdate ()

SET @l_exec_no = 0;
SET @ls_stmt = 'started execution of MARGINMGR.CALCULATION_PROCESS' 
               + ' JOB:: ' + CAST ( ISNULL (@in_analysis_job_fk, 0 ) AS VARCHAR(16) )

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
										@exec_id = @l_exec_no OUTPUT;


------------------------------------------------------------------------------------------
---  Throw and Error if no Analysis_Job Data exists for the @in_analysis_job_fk  
------------------------------------------------------------------------------------------

   IF ( ISNULL ( (SELECT analysis_job_id FROM marginmgr.ANALYSIS_JOB WITH (NOLOCK)
				  WHERE ANALYSIS_JOB_ID = @in_analysis_job_fk ), 0 ) = 0 )
   BEGIN 
   
	SET @status_desc = 'MARGINMGR.CALCULATION_PROCESS:: No Analysis Job exists for Processing '
					 + CAST ( ISNULL ( @in_analysis_job_fk, 0 ) AS VARCHAR (16) )
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
	EXEC exec_monitor.print_status_sp  @l_exec_no;

   END
   ELSE
   BEGIN


-------------------------------------------------------------------------------------
--  ERROR IF NOT COMMON.JOB ENTRY EXISTS... UNLESS ONLY TEST JOB or TESTING FILTERS
-------------------------------------------------------------------------------------

   SET @v_common_job_fk = ISNULL ( (SELECT job_id FROM common.job WITH (NOLOCK)
									  WHERE  JOB_ID = @in_analysis_job_fk ), 0 )

   IF  ( 
          ( @v_common_job_fk = 0 )
		   AND ( ( ( SELECT ISNULL ( test_job_ind, 0 ) FROM marginmgr.ANALYSIS_JOB WITH (NOLOCK) 
				   WHERE  ANALYSIS_JOB_ID = @in_analysis_job_fk )  <> 1 ) )
       AND 
          ( @v_common_job_fk = 0 )        
           AND ( ISNULL ( @in_test_filter_only, 0 ) <> 1 )
       ) 
   BEGIN 
   
	SET @status_desc = 'MARGINMGR.CALCULATION_PROCESS:: No Common Job exists for Processing '
					 + CAST ( ISNULL ( @in_analysis_job_fk, 0 ) AS VARCHAR (16) )
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
	EXEC exec_monitor.print_status_sp  @l_exec_no;

   END
   ELSE
   BEGIN  

    IF @v_common_job_fk > 0
	UPDATE marginmgr.ANALYSIS_JOB
	SET JOB_DATE =  CONVERT ( VARCHAR, ( SELECT ISNULL ( JOB_CREATE_TIMESTAMP, GETDATE () )
										 FROM common.JOB WITH (NOLOCK)
										 WHERE JOB_ID = @in_analysis_job_fk ), 112 )
	WHERE analysis_job_id = @in_analysis_job_fk

-------------------------------------------------------------------------------------
--  DELETE PREVIOUS JOB ROWS
-------------------------------------------------------------------------------------

DELETE
FROM [marginmgr].[ANALYSIS_JOB_COST_CHANGE]
WHERE ANALYSIS_JOB_FK = @in_analysis_job_fk

--GHS 9/22/2013  Restructured Delete Statements for performance

DELETE ECRB
FROM [synchronizer].[EVENT_CALC_RULES_BASIS] ECRB
Inner Join [synchronizer].[EVENT_CALC_RULES] ECR WITH (NOLOCK) on ECRB.EVENT_RULES_FK = ecr.EVENT_RULES_ID
		INNER JOIN [synchronizer].[EVENT_CALC] EC  WITH (NOLOCK) ON ECR.EVENT_FK = EC.EVENT_ID
WHERE ec.JOB_FK = @in_analysis_job_fk 

Delete ECR
FROM [synchronizer].[EVENT_CALC_RULES] ECR
inner join [synchronizer].[EVENT_CALC] EC WITH (NOLOCK) on ECR.event_FK = ec.EVENT_ID
WHERE JOB_FK = @in_analysis_job_fk

DELETE ECerr
FROM [synchronizer].[EVENT_CALC_ERRORS] ECerr
inner join synchronizer.EVENT_CALC EC with (nolock) on ECerr.EVENT_FK = ec.EVENT_ID
WHERE JOB_FK = @in_analysis_job_fk

DELETE 
FROM [synchronizer].[EVENT_CALC]
WHERE JOB_FK = @in_analysis_job_fk 



------------------------------------------------------------------------------------------
---  Select Rows to Calculate
------------------------------------------------------------------------------------------


	EXEC marginmgr.CALCULATION_PROCESS_SELECT
	  @in_analysis_job_fk   WITH RECOMPILE 



-------------------------------------------------------------------------------------
--   Check for Duplicate Events   (  Mark as Record_Status_Cr_fk = 2 )
-------------------------------------------------------------------------------------

	--UPDATE synchronizer.EVENT_CALC
	--SET RECORD_STATUS_CR_FK = 2
	--WHERE EVENT_ID IN (
	--SELECT A.EVENT_ID
	--FROM (
	--SELECT
	--	EC.EVENT_ID,
	--	( DENSE_RANK() OVER ( PARTITION BY EC.RESULT_DATA_NAME_FK, EC.PRODUCT_STRUCTURE_FK
	--	                                  ,EC.ORG_ENTITY_STRUCTURE_FK, EC.CUST_ENTITY_STRUCTURE_FK
	--					ORDER BY EC.RESULT_EFFECTIVE_DATE DESC, EC.EVENT_ID DESC ) ) AS DRANK
	--FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
	--WHERE EC.JOB_FK = @in_analysis_job_fk 
	--AND   EC.RECORD_STATUS_CR_FK = 1
	--) A 
	--WHERE DRANK > 1   ---- ANY DUPLICATES
	--)


	UPDATE EC
	SET RECORD_STATUS_CR_FK = 2
	from synchronizer.EVENT_CALC EC
	inner join
		(
		Select * from (
						SELECT
						EC.EVENT_ID,
						( DENSE_RANK() OVER ( PARTITION BY EC.RESULT_DATA_NAME_FK, EC.PRODUCT_STRUCTURE_FK
														  ,EC.ORG_ENTITY_STRUCTURE_FK, EC.CUST_ENTITY_STRUCTURE_FK
										ORDER BY EC.RESULT_EFFECTIVE_DATE DESC, EC.EVENT_ID DESC ) ) AS DRANK
						FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
						WHERE EC.JOB_FK = @in_analysis_job_fk 
						AND   EC.RECORD_STATUS_CR_FK = 1
						) A 
		) Dups on EC.event_ID = Dups.EVENT_ID
	WHERE Dups.DRANK > 1   ---- ANY DUPLICATES


-------------------------------------------------------------------------------------
--  Delete Event_Calc Rows where Record_Status_Cr_fk = 2
-------------------------------------------------------------------------------------

DELETE 
FROM synchronizer.EVENT_CALC
WHERE JOB_FK = @in_analysis_job_fk 
AND   RECORD_STATUS_CR_FK = 2  




--------------------------------------------------------------------------------------------
--   Check if any products qualified
--------------------------------------------------------------------------------------------

SET @l_rows_processed = ( SELECT COUNT(1) FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
                          WHERE EC.JOB_FK = @in_analysis_job_fk )
	   
IF @l_rows_processed = 0
BEGIN
	SET @status_desc = 'No Result Calculations to be Processed Job:: '
	                          + cast ( @in_analysis_job_fk as varchar (16) ) 
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
END
ELSE

IF ISNULL ( @in_test_filter_only, 0 ) = 0
BEGIN

------------------------------------------------------------------------------------------
---  Call the Calculation Engine for the entire job
------------------------------------------------------------------------------------------


	EXEC marginmgr.CALCULATION_ENGINE  @in_analysis_job_fk
	  


END   ----   CALCULATION PROCESS  

------------------------------------------------------------------------------------------
---  LOG THE JOB AS COMPLETE.... IF common.JOB exists
------------------------------------------------------------------------------------------

IF @v_common_job_fk <> 0
BEGIN
     SET @V_END_TIMESTAMP = getdate()
     SET @v_input_rows =	(select AJ.EVENT_CALC_COUNT FROM marginmgr.ANALYSIS_JOB AJ  WITH (NOLOCK)
                              where ANALYSIS_JOB_ID =  @in_analysis_job_fk )
     exec common.job_execution_create  @in_analysis_job_fk, 'CALCULATION COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;
END  --- LOG JOB COMPLETE


END   ----   MISSING COMMON JOB 

END   ----   MISSING ANALYSIS JOB

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of MARGINMGR.CALCULATION_PROCESS: Job:: '
                                                 + cast ( @in_analysis_job_fk as varchar (16) ) 
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;


END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of MARGINMGR.CALCULATION_PROCESS has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
