﻿--A_epaMAUI_CALCULATION_ENGINE

-- Copyright 2009
--
-- Procedure to test Margin Manager.. Analysis Job
--
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -----------------------------------------------
-- KS        11/30/2009
-- GHS		 02/02/2014	 Added Buy Side Rebates to calculations - ~ line 170
-- GHS		 05/03/2014  Restructured Calc_Group_Seq ordering for Loop

CREATE PROCEDURE [marginmgr].[CALCULATION_ENGINE]
                 ( @in_analysis_job_fk BIGINT  )
AS
BEGIN

DECLARE @l_sysdate    datetime
DECLARE @ls_stmt      varchar (1000)
DECLARE @ls_queue     nvarchar (4000)
DECLARE @l_exec_no    bigint
DECLARE @l_rows_processed    BIGINT

DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

DECLARE @status_desc  varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

BEGIN TRY
SET NOCOUNT ON
SET @l_sysdate = getdate ()

SET @l_exec_no = 0;
EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of MARGINMGR.CALCULATION_ENGINE', 
												@exec_id = @l_exec_no OUTPUT;




------------------------------------------------------------------------------------------
---  Call Routines to Quailify Rules
------------------------------------------------------------------------------------------


	EXEC synchronizer.EVENT_POST_QUALIFY_RULES_CALC
	  @in_analysis_job_fk    WITH RECOMPILE 




------------------------------------------------------------------------------------------
---  LOOP THROUGH RULES BY CALC GROUP SEQ
------------------------------------------------------------------------------------------

      DECLARE   @vm$result_data_name_fk      INT
			   ,@vm$calc_group_seq			 INT


      DECLARE  cur_v_result_group cursor local for
			 SELECT 
			        A.result_data_name_fk,
			        A.calc_group_seq
			  FROM (
				SELECT distinct
				       ec.result_data_name_fk
                      ,ISNULL ( ec.calc_group_seq, 999 ) AS calc_group_seq	
					  , isnull( rh.calc_group_seq, 999 ) as CG_Order
				FROM  synchronizer.EVENT_CALC ec with (nolock)
				inner join synchronizer.rules_hierarchy rh with (nolock) on ec.RESULT_DATA_NAME_FK = rh.RESULT_DATA_NAME_FK and rh.RECORD_STATUS_CR_FK = 1
				WHERE  JOB_FK = @in_analysis_job_fk
				GROUP BY ec.result_data_name_fk, rh.calc_group_seq, ec.calc_group_seq
              ) A
			  ORDER BY  A.CG_Order ASC, A.result_data_name_fk ASC

                    
    OPEN cur_v_result_group;
    FETCH NEXT FROM cur_v_result_group INTO  @vm$result_data_name_fk
											,@vm$calc_group_seq	
												


------------------------------------------------------------------------------------------
---  LOOP THROUGH EVENTS BY RESULT DATA NAME
------------------------------------------------------------------------------------------


         WHILE @@FETCH_STATUS = 0 
         BEGIN
                

		SET @v_count = 
			( SELECT COUNT(1)
			  FROM  synchronizer.EVENT_CALC
				WHERE  JOB_FK = @in_analysis_job_fk
				AND    result_data_name_fk = @vm$result_data_name_fk 
				AND    calc_group_seq      = @vm$calc_group_seq  )

	                      
		SET @status_desc =  'MARGINMGR.CALCULATION_ENGINE: Perform Calculation: '
							+ ' JOB: ' 
							+ CAST ( @in_analysis_job_fk AS VARCHAR(16) )                        
							+ ' CALC GROUP SEQ: ' 
							+ CAST ( @vm$calc_group_seq AS VARCHAR(16) )
							+ ' RESULT: ' 
							+ ( ISNULL ( ( SELECT name FROM epacube.data_name WITH (NOLOCK) WHERE data_name_id = @vm$result_data_name_fk ), 'X' ) )
							+ ' COUNT: '  
							+ CAST (@V_COUNT AS VARCHAR(16) )                              
		EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

------------------------------------------------------------------------------------------
---  Call Routines to Find Basis; Perform Calcs and Choose Result
--		Table EVENT_CALC_RULES should have all its rows; ranked and result
--		updated in the EVENT_CALC table
------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------
--   Find Basis Values for the Qualified Rules
------------------------------------------------------------------------------------------


	EXEC marginmgr.FIND_BASIS_VALUES  @in_analysis_job_fk, 
	@vm$result_data_name_fk, @vm$calc_group_seq    WITH RECOMPILE 

------------------------------------------------------------------------------------------
--   Perform Calculation for every Qualified Rule
------------------------------------------------------------------------------------------

	EXEC marginmgr.PERFORM_CALCULATION   @in_analysis_job_fk, 
	@vm$result_data_name_fk, @vm$calc_group_seq    WITH RECOMPILE 

------------------------------------------------------------------------------------------
--   Choose the correct Rule based on Matrix Actions
------------------------------------------------------------------------------------------

	EXEC marginmgr.PERFORM_MATRIX_ACTION   @in_analysis_job_fk, 
	@vm$result_data_name_fk, @vm$calc_group_seq    WITH RECOMPILE 


------------------------------------------------------------------------------------------
---  Update Associated EVENT_CALC with Rebate Costs and COGS 
------------------------------------------------------------------------------------------

IF @vm$result_data_name_fk IN ( 111501, 111502, 111401, 111602 )  -- Rebate Amt, Rebated Cost or Pricing
	EXEC marginmgr.PERFORM_MARGIN_RESULTS_UPDATE   @in_analysis_job_fk, @vm$result_data_name_fk    WITH RECOMPILE

------------------------------------------------------------------------------------------
---  GET NEXT Warehouse and Result Grouping 
------------------------------------------------------------------------------------------

    FETCH NEXT FROM cur_v_result_group INTO  @vm$result_data_name_fk
											,@vm$calc_group_seq	
											


     END --cur_v_result_group  LOOP
     CLOSE cur_v_result_group
	 DEALLOCATE cur_v_result_group; 
     


------------------------------------------------------------------------------------------
--   last Step is to update the Margin_Amt and Margin_Pct
------------------------------------------------------------------------------------------


UPDATE synchronizer.EVENT_CALC
SET    MARGIN_AMT     = ( SELL_PRICE_CUST_AMT - ORDER_COGS_AMT )
	  ,MARGIN_PCT     =  CASE ISNULL ( SELL_PRICE_CUST_AMT, 0 )
	                     WHEN 0 THEN 0
	                     ELSE (  ( SELL_PRICE_CUST_AMT - ORDER_COGS_AMT ) / ISNULL ( SELL_PRICE_CUST_AMT, 1  ) )
	                     END 
WHERE 1 = 1
AND   synchronizer.EVENT_CALC.JOB_FK = @in_analysis_job_fk



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of MARGINMGR.CALCULATION_ENGINE'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;


END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of MARGINMGR.CALCULATION_ENGINE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
