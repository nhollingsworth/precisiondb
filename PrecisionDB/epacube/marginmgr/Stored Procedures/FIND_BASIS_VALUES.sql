﻿

--A_epaMAUI_FIND_BASIS_VALUES


---- Copyright 2008
----
---- Procedure created by Kathi Scott
----
----
---- Purpose: To determine all qualified rules for Event_Data records in Job
----				Validation and Calculations Rules assigned to Events
----				Derivation Rules will be created with NEW DATA = @
----
---- MODIFICATION HISTORY
---- Person     Date        Comments
---- --------- ----------  ------------------------------------------
---- KS        11/30/2009   Initial SQL Version
---- KS		   08/21/2011   V515 Operand Lookup: QTY Break and CPT Specific Operand lookups utilizing RULES_FILTER_LOOKUP_OPERAND                                        
----                        V515 Basis Value Historical Pricesheet(s) lookups for all Basis DNs
---- KS        09/27/2011   Tune Future Basis lookup with Event_Data Table
---- KS        10/27/2011   New Requirement if RULES_FILTER_LOOKUP_OPERAND OPERAND_FILTER_VALUE = 0 
----                        Then lookup in Category table instead of RULES_ACTION_OPERANDS
---- KS        05/04/2012   Removed the MARGIN_RESULTS table in V516; Values now on EVENT_CALC row
---- KS        05/23/2012   Addition of JASCO Qty Breaks by OPERANDS_FILTER_DN_FK Lookup ( EPA-3328 )
---- KS        05/27/2012   Loop through ECR Update for performance improvement 
---- CV        06/25/2012   Added to dense rank..  PK asc removed the top 1 at update at bottom
---- KS        09/20/2012   Somehow in the Cluster of updating MM code from multiple Customer environments 
----                             during V516 release wrapup the code for dense rank on Pricesheet was stepped on
----                             such that date took precedence over the Org precedence. (should be Org then Date)
---- KS        10/11/2012   EPA-3410 add BASIS_ACTION_CR_FK to determine if basis value is selected from FIRST precedence
----                          or by EFF_DATE ( See Ranking Below )
-----CV         09/10/2013   Added for PV and SR that the record status cr fk = 1
---- GHS	    9/21/2013 for Performance lines 797 and 860
---- GHS		10/8/2013	Set isnull(EC.MODEL_SALES_QTY, 0) whereever Model Qty in use
---- GHS		6/29/2014	Added logging to every routine inserting records into Synchronizer.Event_Calc_Rules_Basis
---- GHS		6/30/2014	Added performance steps to routine around line 924
---- GHS		5/27/2015	Restructured


CREATE PROCEDURE [marginmgr].[FIND_BASIS_VALUES] 
    ( @in_analysis_job_fk   BIGINT, @in_result_data_name_fk INT, @in_calc_group_seq INT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of marginmgr.FIND_BASIS_VALUES.' 
               + ' JOB:: ' + CAST ( ISNULL (@in_analysis_job_fk, 0 ) AS VARCHAR(16) )
               + ' RESULT:: ' + CAST ( ISNULL (@IN_RESULT_DATA_NAME_FK, 0 ) AS VARCHAR(16) )
               + ' CALC SEQ:: ' + CAST ( ISNULL (@IN_CALC_GROUP_SEQ, 0 ) AS VARCHAR(16) )

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;





------------------------------------------------------------------------------------------
--   CREATE  #TS_ORG_XREF
--		This table used for performance to group by warehouse/result for processing
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_ORG_XREF') is not null
	   drop table #TS_ORG_XREF;
	   
	   
	-- create temp table
	CREATE TABLE #TS_ORG_XREF(
	    TS_ORG_XREF_ID    BIGINT IDENTITY(1000,1) NOT NULL,
		ORG_ENTITY_STRUCTURE_FK             BIGINT NULL,	    	    
		XREF_ORG_ENTITY_STRUCTURE_FK        BIGINT NULL,
		 )


------------------------------------------------------------------------------------------
---  temporary solution... will work for everyone but multilevel orgs.. ie Region/Division
------------------------------------------------------------------------------------------

                
		INSERT INTO #TS_ORG_XREF(
			ORG_ENTITY_STRUCTURE_FK    	    	    
		   ,XREF_ORG_ENTITY_STRUCTURE_FK )
		SELECT 
			ES.ENTITY_STRUCTURE_ID
		   ,ES.ENTITY_STRUCTURE_ID 
		FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
		WHERE ES.ENTITY_CLASS_CR_FK = 10101 -- ORG

--- THROW IN THE PARENTS...
		INSERT INTO #TS_ORG_XREF(
			ORG_ENTITY_STRUCTURE_FK    	    	    
		   ,XREF_ORG_ENTITY_STRUCTURE_FK )
		SELECT 
			ES.ENTITY_STRUCTURE_ID
		   ,ES.PARENT_ENTITY_STRUCTURE_FK
		FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
		WHERE ES.ENTITY_CLASS_CR_FK = 10101 -- ORG
		AND   ES.PARENT_ENTITY_STRUCTURE_FK IS NOT NULL 		


--- THROW IN THE ROOTS...
		INSERT INTO #TS_ORG_XREF(
			ORG_ENTITY_STRUCTURE_FK    	    	    
		   ,XREF_ORG_ENTITY_STRUCTURE_FK )
		SELECT 
			ES.ENTITY_STRUCTURE_ID
		   ,1
		FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
		WHERE ES.ENTITY_CLASS_CR_FK = 10101 -- ORG


--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSOX_TSOXID ON #TS_ORG_XREF 
	(TS_ORG_XREF_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSOX_OESF_IXOESF] ON #TS_ORG_XREF 
	(ORG_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( XREF_ORG_ENTITY_STRUCTURE_FK )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSOX_XOESF_IOESF] ON #TS_ORG_XREF 
	(XREF_ORG_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( ORG_ENTITY_STRUCTURE_FK )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



---------------------------------------------------
-- Temp Table for all Qualified Rules
---------------------------------------------------


--drop temp tables
IF object_id('tempdb..#TS_RULES_BASIS') is not null
   drop table #TS_RULES_BASIS

CREATE TABLE #TS_RULES_BASIS(
	[TS_RULES_BASIS_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[RULES_FK] [bigint] NULL,		
	[BASIS_POSITION] [SMALLINT] NULL,
	[BASIS_DN_FK] [INT]  NULL,
	[BASIS_PRICESHEET_NAME] [VARCHAR] (128) NULL,
	[BASIS_PRICESHEET_DATE] [DATETIME]  NULL,	
    [RESULT_EFFECTIVE_DATE] [DATETIME]  NULL,	
	[BASIS_USE_CORP_ONLY_IND] [SMALLINT]  NULL 	
)



---------------------------------------------------
-- Insert into #TS_RULES_BASIS  -- BASIS CALC
---------------------------------------------------

INSERT INTO #TS_RULES_BASIS
	( RULES_FK
	 ,BASIS_POSITION
	 ,BASIS_DN_FK 
	 ,BASIS_PRICESHEET_NAME
	 ,BASIS_PRICESHEET_DATE
	 ,RESULT_EFFECTIVE_DATE
	 ,BASIS_USE_CORP_ONLY_IND )
SELECT DISTINCT
      ECR.RULES_FK 
     ,0
     ,RA.BASIS_CALC_DN_FK
     ,RA.BASIS_CALC_PRICESHEET_NAME
     ,RA.BASIS_CALC_PRICESHEET_DATE
     ,EC.RESULT_EFFECTIVE_DATE
     ,RA.BASIS_USE_CORP_ONLY_IND
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ( ECR.EVENT_FK = EC.EVENT_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)
  ON ( R.RULES_ID = ECR.RULES_FK )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON  ( RA.RULES_FK = R.RULES_ID 
  AND   RA.BASIS_CALC_DN_FK IS NOT NULL )
WHERE 1 = 1
AND   EC.JOB_FK = @in_analysis_job_fk  
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1



---------------------------------------------------
-- Insert into #TS_RULES_BASIS  -- BASIS1
---------------------------------------------------

INSERT INTO #TS_RULES_BASIS
	( RULES_FK
	 ,BASIS_POSITION
	 ,BASIS_DN_FK 
	 ,BASIS_PRICESHEET_NAME
	 ,BASIS_PRICESHEET_DATE
	 ,RESULT_EFFECTIVE_DATE
	 ,BASIS_USE_CORP_ONLY_IND )
SELECT DISTINCT
      ECR.RULES_FK 
     ,1
     ,RA.BASIS1_DN_FK
     ,RA.BASIS1_PRICESHEET_NAME
     ,RA.BASIS1_PRICESHEET_DATE   
     ,EC.RESULT_EFFECTIVE_DATE
     ,RA.BASIS_USE_CORP_ONLY_IND
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ( ECR.EVENT_FK = EC.EVENT_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)
  ON ( R.RULES_ID = ECR.RULES_FK )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON  ( RA.RULES_FK = R.RULES_ID 
  AND   RA.BASIS1_DN_FK IS NOT NULL )
WHERE 1 = 1
AND   EC.JOB_FK = @in_analysis_job_fk  
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1

---------------------------------------------------
-- Insert into #TS_RULES_BASIS  -- BASIS2
---------------------------------------------------

INSERT INTO #TS_RULES_BASIS
	( RULES_FK
	 ,BASIS_POSITION
	 ,BASIS_DN_FK 
	 ,BASIS_PRICESHEET_NAME
	 ,BASIS_PRICESHEET_DATE	 
	 ,RESULT_EFFECTIVE_DATE
	 ,BASIS_USE_CORP_ONLY_IND )
SELECT DISTINCT
      ECR.RULES_FK 
     ,2
     ,RA.BASIS2_DN_FK
     ,RA.BASIS2_PRICESHEET_NAME
     ,RA.BASIS2_PRICESHEET_DATE
     ,EC.RESULT_EFFECTIVE_DATE
     ,RA.BASIS_USE_CORP_ONLY_IND
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ( ECR.EVENT_FK = EC.EVENT_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)
  ON ( R.RULES_ID = ECR.RULES_FK )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON  ( RA.RULES_FK = R.RULES_ID
  AND   RA.BASIS2_DN_FK IS NOT NULL )
WHERE 1 = 1
AND   EC.JOB_FK = @in_analysis_job_fk   
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1


---------------------------------------------------
-- Insert into #TS_RULES_BASIS  -- BASIS3
---------------------------------------------------

INSERT INTO #TS_RULES_BASIS
	( RULES_FK
	 ,BASIS_POSITION
	 ,BASIS_DN_FK 
	 ,BASIS_PRICESHEET_NAME
	 ,BASIS_PRICESHEET_DATE	 	 
	 ,RESULT_EFFECTIVE_DATE
	 ,BASIS_USE_CORP_ONLY_IND )
SELECT DISTINCT
      ECR.RULES_FK 
     ,3
     ,RA.BASIS3_DN_FK
     ,RA.BASIS3_PRICESHEET_NAME
     ,RA.BASIS3_PRICESHEET_DATE
     ,EC.RESULT_EFFECTIVE_DATE
     ,RA.BASIS_USE_CORP_ONLY_IND
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ( ECR.EVENT_FK = EC.EVENT_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)
  ON ( R.RULES_ID = ECR.RULES_FK )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON  ( RA.RULES_FK = R.RULES_ID
  AND   RA.BASIS3_DN_FK IS NOT NULL )
WHERE 1 = 1
AND   EC.JOB_FK = @in_analysis_job_fk   
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1


---------------------------------------------------
-- create all indexes for  #TS_RULES_BASIS
---------------------------------------------------

CREATE NONCLUSTERED INDEX [IDX_TSRB_RF_BP]
ON #TS_RULES_BASIS
(
	[RULES_FK] ASC
   ,[BASIS_POSITION] ASC
)
INCLUDE ( [TS_RULES_BASIS_ID])

CREATE NONCLUSTERED INDEX [IDX_TSRB_DN_RF]
ON #TS_RULES_BASIS
(
	[BASIS_DN_FK] ASC
)
INCLUDE ( [RULES_FK])

CREATE CLUSTERED INDEX [IDX_TSRB_TSRBI] 
ON #TS_RULES_BASIS (
	[TS_RULES_BASIS_ID] ASC
)



----------------------------------------------------------------------------------------------------------------
---    FUTURE EVENT   VALUES AND RESULTS  
----------------------------------------------------------------------------------------------------------------


--drop temp tables
IF object_id('tempdb..#TS_EVENT_BASIS') is not null
   drop table #TS_EVENT_BASIS

CREATE TABLE #TS_EVENT_BASIS(
	[TS_EVENT_BASIS_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[EVENT_FK] [bigint] NULL,		
	[EVENT_DATA_EVENT_FK] [INT]  NULL,
)


---------------------------------------------------
-- create all indexes for  #TS_EVENT_BASIS
---------------------------------------------------

CREATE NONCLUSTERED INDEX [IDX_TSEB_EF]
ON #TS_EVENT_BASIS
(
	[EVENT_FK] ASC
)
INCLUDE ( [TS_EVENT_BASIS_ID])

CREATE NONCLUSTERED INDEX [IDX_TSEB_DN_RF]
ON #TS_EVENT_BASIS
(
	[EVENT_DATA_EVENT_FK] ASC
)
INCLUDE ( [TS_EVENT_BASIS_ID] )


CREATE CLUSTERED INDEX [IDX_TSEB_TSRBI] 
ON #TS_EVENT_BASIS (
	[TS_EVENT_BASIS_ID] ASC
)



------------------------------------------------------------------------------------------
--   CREATE  #TS_PRICESHEET
------------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_PRICESHEET') is not null
	   drop table #TS_PRICESHEET;
	   
	   
	-- create temp table
	CREATE TABLE #TS_PRICESHEET(
	    TS_PRICESHEET_ID        BIGINT IDENTITY(1000,1) NOT NULL,
	    TS_RULES_BASIS_FK       BIGINT NULL,
	    EVENT_RULES_FK          BIGINT NULL,
		EVENT_FK			    BIGINT NULL,	    	    
		RULES_FK			    BIGINT NULL,
		RESULT_EFFECTIVE_DATE   DATETIME NULL,
		PRICESHEET_FK           BIGINT NULL,
		BASIS_PRICESHEET_NAME   VARCHAR(64) NULL,
		BASIS_PRICESHEET_DATE   DATETIME NULL,
		BASIS_VALUE             NUMERIC (18,6) NULL,
		EFFECTIVE_DATE          DATETIME,
		ORG_ENTITY_STRUCTURE_FK BIGINT NULL,
		ENTITY_STRUCTURE_FK     BIGINT NULL,
		DRANK                   INT
		 )


CREATE NONCLUSTERED INDEX [IDX_TSPSHT_RBFK_ID]
ON #TS_PRICESHEET
(
	[TS_RULES_BASIS_FK] ASC
)
INCLUDE ( [TS_PRICESHEET_ID] )


CREATE CLUSTERED INDEX [IDX_TSPSHT_ID] 
ON #TS_PRICESHEET (
	[TS_PRICESHEET_ID] ASC
)



---------------------------------------------------
-- Temp Table for all Operands Basis Values
---------------------------------------------------


--drop temp tables
IF object_id('tempdb..#TS_RULES_BASIS_OPERANDS') is not null
   drop table #TS_RULES_BASIS_OPERANDS

CREATE TABLE #TS_RULES_BASIS_OPERANDS(
	[TS_RULES_BASIS_OPERAND_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[EVENT_FK] [bigint] NULL,		
	[EVENT_RULES_FK] [bigint] NULL,			
	[RULES_FK] [bigint] NULL,			
	[RULES_ACTION_FK] [bigint] NULL,				
	[BASIS_POSITION] [SMALLINT] NULL,
	[RULES_FILTER_LOOKUP_OPERAND_FK] [BIGINT] NULL,
	[OPERAND_FILTER_DN_FK] [INT]  NULL,
	[OPERAND_FILTER_OPERATOR_CR_FK] [INT]  NULL,	
	[OPERAND_FILTER_VALUE]  [VARCHAR] (32) NULL,
	[FILTER_ENTITY_STRUCTURE_FK] [BIGINT] NULL
)


--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSRBO_TSRBOD ON #TS_RULES_BASIS_OPERANDS
	(TS_RULES_BASIS_OPERAND_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSRBO_] ON #TS_RULES_BASIS_OPERANDS 
    ( RULES_ACTION_FK ASC,
      BASIS_POSITION ASC,
      OPERAND_FILTER_DN_FK ASC,
      OPERAND_FILTER_OPERATOR_CR_FK ASC,
      OPERAND_FILTER_VALUE ASC 
      )	
	INCLUDE ( RULES_FILTER_LOOKUP_OPERAND_FK )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSRBO_OFDN] ON #TS_RULES_BASIS_OPERANDS
	(OPERAND_FILTER_DN_FK ASC
	)
	INCLUDE ( TS_RULES_BASIS_OPERAND_ID )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)




--drop temp tables
IF object_id('tempdb..#TS_RULES_FILTER_LOOKUP_OPERANDS') is not null
   drop table #TS_RULES_FILTER_LOOKUP_OPERANDS

CREATE TABLE #TS_RULES_FILTER_LOOKUP_OPERANDS(
	[TS_RULES_FILTER_LOOKUP_OPERAND_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[EVENT_RULES_FK] [bigint] NULL,			
	[RULES_ACTION_FK] [bigint] NULL,				
	[BASIS_POSITION] [SMALLINT] NULL,
	[RULES_FILTER_LOOKUP_OPERAND_FK] [BIGINT] NULL,
	[OPERAND_FILTER_DN_FK] [INT]  NULL,
	[OPERAND_FILTER_OPERATOR_CR_FK] [INT]  NULL,	
	[OPERAND_FILTER_VALUE]  [VARCHAR] (32) NULL
)


-----------------------------------------------------------------------------------------
-- INSERT into synchronizer.EVENT_CALC_RULES_BASIS  --- CURRENT PRODUCT VALUES
-----------------------------------------------------------------------------------------


INSERT INTO [synchronizer].[EVENT_CALC_RULES_BASIS]
           ([EVENT_RULES_FK]
           ,[BASIS_POSITION]
           ,[BASIS_VALUE]                                        
           ,[BASIS_DN_FK]
           ,[BASIS_PRICESHEET_NAME]
           ,[BASIS_PRICESHEET_DATE]                     
           ,[BASIS_ORG_PRECEDENCE]                     
           ,[BASIS_RESULT_TYPE_CR_FK]           
           ,[BASIS_ACTION_CR_FK]                      
           ,[BASIS_EFFECTIVE_DATE]
           ,[BASIS_EVENT_FK]                
           ,[BASIS_ORG_ENTITY_STRUCTURE_FK]
           ,[BASIS_CUST_ENTITY_STRUCTURE_FK]
           ,[BASIS_SUPL_ENTITY_STRUCTURE_FK] 
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP])
SELECT
            ECR.EVENT_RULES_ID
           ,TSRB.BASIS_POSITION
           ,CASE DS.COLUMN_NAME 
            WHEN 'NET_VALUE1' THEN PV.NET_VALUE1
            WHEN 'NET_VALUE2' THEN PV.NET_VALUE2
            WHEN 'NET_VALUE3' THEN PV.NET_VALUE3
            WHEN 'NET_VALUE4' THEN PV.NET_VALUE4
            WHEN 'NET_VALUE5' THEN PV.NET_VALUE5
            WHEN 'NET_VALUE6' THEN PV.NET_VALUE6
            END AS BASIS_VALUE           
           ,TSRB.BASIS_DN_FK
           ,NULL  --- PRICESHEET_NAME
           ,NULL  --- PRICESHEET_DATE   
           ,( SELECT ECT.PRECEDENCE FROM EPACUBE.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
              WHERE ECT.RECORD_STATUS_CR_FK = 1
              AND   ECT.DATA_NAME_FK = ( SELECT ES.DATA_NAME_FK FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
                                         WHERE ES.ENTITY_STRUCTURE_ID = PV.ORG_ENTITY_STRUCTURE_FK ) 
            ) AS BASIS_ORG_PRECEDENCE                                                   
           ,710    -- BASIS_RESULT_TYPE_CR_FK         
           ,ISNULL ( ( SELECT RH.BASIS_ACTION_CR_FK
					  FROM synchronizer.RULES_HIERARCHY RH 
					  INNER JOIN synchronizer.RULES_STRUCTURE RS ON (RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK)
					  WHERE RS.RULES_STRUCTURE_ID = ECR.RULES_STRUCTURE_FK )                                 
                   ,780 )
           ,ISNULL ( PV.EFFECTIVE_DATE, PV.UPDATE_TIMESTAMP ) ---PV.EFFECTIVE_DATE                       
           ,NULL  -- BASIS_EVENT_FK             
           ,PV.ORG_ENTITY_STRUCTURE_FK
           ,CASE DS.ENTITY_STRUCTURE_EC_CR_FK
            WHEN 10104 THEN PV.ENTITY_STRUCTURE_FK
            ELSE NULL
            END
           ,CASE DS.ENTITY_STRUCTURE_EC_CR_FK
            WHEN 10103 THEN PV.ENTITY_STRUCTURE_FK
            ELSE NULL
            END            
           ,1     -- RECORD_STATUS_CR_FK
           ,GETDATE ()  --UPDATE_TIMESTAMP
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ( ECR.EVENT_FK = EC.EVENT_ID )
INNER JOIN #TS_RULES_BASIS TSRB
  ON ( TSRB.RULES_FK = ECR.RULES_FK )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.DATA_NAME_ID = TSRB.BASIS_DN_FK )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )  
INNER JOIN epacube.PRODUCT_VALUES PV WITH (NOLOCK)
 ON ( PV.DATA_NAME_FK = DN.PARENT_DATA_NAME_FK  -- PARENT OF BASIS VALUE
 AND PV.RECORD_STATUS_CR_FK = 1                      -----added so end dated values will not calc.
 AND  ISNULL ( PV.PRODUCT_STRUCTURE_FK, -999 ) = ISNULL ( EC.PRODUCT_STRUCTURE_FK, -999 )
    --- BASIS VALUES ARE ORG TO ORG;  CORP TO ORG   OR ORG TO CORP 
 AND  ( ISNULL ( PV.ORG_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( EC.ORG_ENTITY_STRUCTURE_FK, -999 )
      OR PV.ORG_ENTITY_STRUCTURE_FK IN ( SELECT TOX.XREF_ORG_ENTITY_STRUCTURE_FK
										   FROM  #TS_ORG_XREF TOX
										   WHERE TOX.ORG_ENTITY_STRUCTURE_FK = EC.ORG_ENTITY_STRUCTURE_FK )
	  )
    --- BASIS PARENT ONLY IND = 1 THEN DO NOT ALLOW WHSE LEVEL BASIS VALUES
 AND  (       ISNULL ( TSRB.BASIS_USE_CORP_ONLY_IND, 0 ) = 0
       OR   ( ISNULL ( TSRB.BASIS_USE_CORP_ONLY_IND, 0 ) = 1 
         AND  ISNULL ( PV.ORG_ENTITY_STRUCTURE_FK, -999 ) <> ISNULL ( EC.ORG_ENTITY_STRUCTURE_FK, -999 ) )
      )
    --- ONLY CHECKING SUPL ENTITY RIGHT NOW  	  	  
 AND  ( ISNULL ( PV.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( EC.SUPL_ENTITY_STRUCTURE_FK, -999 )  
      OR
        PV.ENTITY_STRUCTURE_FK IS NULL 
      OR
        EC.SUPL_ENTITY_STRUCTURE_FK IS NULL )
    ) 
WHERE 1 = 1
AND   EC.JOB_FK = @in_analysis_job_fk   
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1 
------------- ADDED SHEET AND DATE LOGIC
AND   DS.EVENT_TYPE_CR_FK IN ( 151 )

SET @status_desc = 'Ln 690 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


IF object_id('tempdb..#ec_summ') is not null
	drop table #ec_summ;

SELECT
            ECR.EVENT_RULES_ID
           ,TSRB.BASIS_POSITION
		   , EC.PRODUCT_STRUCTURE_FK
		   , EC.ORG_ENTITY_STRUCTURE_FK
		   , TSRB.BASIS_USE_CORP_ONLY_IND
		   , DS.COLUMN_NAME
		   , DS.ENTITY_STRUCTURE_EC_CR_FK
		   , EC.SUPL_ENTITY_STRUCTURE_FK
		   , DN.PARENT_DATA_NAME_FK
		   , ECR.RULES_STRUCTURE_FK
           ,TSRB.BASIS_DN_FK
           ,710   BASIS_RESULT_TYPE_CR_FK            
           ,ISNULL ( ( SELECT RH.BASIS_ACTION_CR_FK
					  FROM synchronizer.RULES_HIERARCHY RH 
					  INNER JOIN synchronizer.RULES_STRUCTURE RS ON (RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK)
					  WHERE RS.RULES_STRUCTURE_ID = ECR.RULES_STRUCTURE_FK )                                 
                   ,780 ) BASIS_ACTION_CR_FK
           ,1     RECORD_STATUS_CR_FK
           ,GETDATE ()  UPDATE_TIMESTAMP
into #ec_summ
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ( ECR.EVENT_FK = EC.EVENT_ID )
INNER JOIN #TS_RULES_BASIS TSRB
  ON ( TSRB.RULES_FK = ECR.RULES_FK )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.DATA_NAME_ID = TSRB.BASIS_DN_FK )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )  
WHERE 1 = 1
AND   EC.JOB_FK = @in_analysis_job_fk  
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1
AND   DS.EVENT_TYPE_CR_FK IN ( 152 )

Create index idx_ecs on #ec_summ(PARENT_DATA_NAME_FK, PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, BASIS_USE_CORP_ONLY_IND, SUPL_ENTITY_STRUCTURE_FK, RULES_STRUCTURE_FK)

INSERT INTO [synchronizer].[EVENT_CALC_RULES_BASIS]
           ([EVENT_RULES_FK]
           ,[BASIS_POSITION]
           ,[BASIS_VALUE]                                        
           ,[BASIS_DN_FK]
           ,[BASIS_ORG_PRECEDENCE]    
           ,[BASIS_RESULT_TYPE_CR_FK]     
           ,[BASIS_ACTION_CR_FK]      
           ,[BASIS_EFFECTIVE_DATE]
           ,[BASIS_EVENT_FK]                      
           ,[BASIS_ORG_ENTITY_STRUCTURE_FK]
           ,[BASIS_CUST_ENTITY_STRUCTURE_FK]
           ,[BASIS_SUPL_ENTITY_STRUCTURE_FK] 
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP])
SELECT
            EVENT_RULES_ID		--[EVENT_RULES_FK]
           ,BASIS_POSITION		--[BASIS_POSITION]
           ,CASE ECs.COLUMN_NAME 
            WHEN 'NET_VALUE1' THEN SR.NET_VALUE1
            WHEN 'NET_VALUE2' THEN SR.NET_VALUE2
            WHEN 'NET_VALUE3' THEN SR.NET_VALUE3
            WHEN 'NET_VALUE4' THEN SR.NET_VALUE4
            WHEN 'NET_VALUE5' THEN SR.NET_VALUE5
            WHEN 'NET_VALUE6' THEN SR.NET_VALUE6
            END										--[BASIS_VALUE]     
           ,BASIS_DN_FK								
           ,( SELECT ECT.PRECEDENCE FROM EPACUBE.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
              WHERE ECT.RECORD_STATUS_CR_FK = 1
              AND   ECT.DATA_NAME_FK = ( SELECT ES.DATA_NAME_FK FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
                                         WHERE ES.ENTITY_STRUCTURE_ID = SR.ORG_ENTITY_STRUCTURE_FK ) 
            ) AS BASIS_ORG_PRECEDENCE                                                   
           ,710   -- BASIS_RESULT_TYPE_CR_FK            
           ,ISNULL ( ( SELECT RH.BASIS_ACTION_CR_FK
					  FROM synchronizer.RULES_HIERARCHY RH 
					  INNER JOIN synchronizer.RULES_STRUCTURE RS ON (RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK)
					  WHERE RS.RULES_STRUCTURE_ID = ECs.RULES_STRUCTURE_FK )                                 
                   ,780 )									--[BASIS_ACTION_CR_FK]
           ,ISNULL ( SR.EFFECTIVE_DATE, SR.UPDATE_TIMESTAMP ) --[BASIS_EFFECTIVE_DATE]                   
           ,NULL  -- BASIS_EVENT_FK                                   
           ,SR.ORG_ENTITY_STRUCTURE_FK			--[BASIS_ORG_ENTITY_STRUCTURE_FK]
           ,CASE ECs.ENTITY_STRUCTURE_EC_CR_FK	
            WHEN 10104 THEN SR.ENTITY_STRUCTURE_FK
            ELSE NULL
            END									--[BASIS_CUST_ENTITY_STRUCTURE_FK]
           ,CASE ECs.ENTITY_STRUCTURE_EC_CR_FK
            WHEN 10103 THEN SR.ENTITY_STRUCTURE_FK
            ELSE NULL
            END									--[BASIS_SUPL_ENTITY_STRUCTURE_FK]
           ,1     -- RECORD_STATUS_CR_FK
           ,GETDATE ()  --UPDATE_TIMESTAMP
From #EC_Summ ECs
INNER JOIN marginmgr.SHEET_RESULTS_VALUES_FUTURE SR WITH (NOLOCK)
 ON ( SR.DATA_NAME_FK = ECs.PARENT_DATA_NAME_FK  -- PARENT OF BASIS VALUE
 AND SR.RECORD_STATUS_CR_FK = 1                      -----added so end dated values will not calc.
 AND  ISNULL ( SR.PRODUCT_STRUCTURE_FK, -999 ) = ISNULL ( ECs.PRODUCT_STRUCTURE_FK, -999 )
    --- BASIS VALUES ARE ORG TO ORG;  CORP TO ORG   OR ORG TO CORP 
 AND  ( ISNULL ( SR.ORG_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ECs.ORG_ENTITY_STRUCTURE_FK, -999 )
      OR SR.ORG_ENTITY_STRUCTURE_FK IN ( SELECT TOX.XREF_ORG_ENTITY_STRUCTURE_FK
										   FROM  #TS_ORG_XREF TOX
										   WHERE TOX.ORG_ENTITY_STRUCTURE_FK = ECs.ORG_ENTITY_STRUCTURE_FK )
	  )
    --- BASIS PARENT ONLY IND = 1 THEN DO NOT ALLOW WHSE LEVEL BASIS VALUES
 AND  (       ISNULL ( ECs.BASIS_USE_CORP_ONLY_IND, 0 ) = 0
       OR   ( ISNULL ( ECs.BASIS_USE_CORP_ONLY_IND, 0 ) = 1 
         AND  ISNULL ( SR.ORG_ENTITY_STRUCTURE_FK, -999 ) <> ISNULL ( ECs.ORG_ENTITY_STRUCTURE_FK, -999 ) )
      )
    --- ONLY CHECKING SUPL ENTITY RIGHT NOW  	  	  
 AND  ( ISNULL ( SR.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( ECs.SUPL_ENTITY_STRUCTURE_FK, -999 )  
      OR
        SR.ENTITY_STRUCTURE_FK IS NULL 
      OR
        ECs.SUPL_ENTITY_STRUCTURE_FK IS NULL )
    ) 

IF object_id('tempdb..#ec_summ') is not null
	drop table #ec_summ;
	
SET @status_desc = 'Ln 924 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

-----------------------------------------------------------------------------------------
-- INSERT into synchronizer.EVENT_CALC_RULES_BASIS 
--	 SHEET RESULTS calculated in the SAME CALC JOB
-----------------------------------------------------------------------------------------

--Added by GHS 9/21/2013 for Performance

IF object_id('tempdb..#rdnfk') is null
Begin
	SELECT ECX.RESULT_DATA_NAME_FK
		into #RDNFK
		FROM synchronizer.EVENT_CALC ECX WITH (NOLOCK)
		WHERE ECX.JOB_FK = @in_analysis_job_fk
		Group by ECX.RESULT_DATA_NAME_FK

	Create index idx_rdn on #RDNFK(RESULT_DATA_NAME_FK)

SET @status_desc = 'Ln 819 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

End

INSERT INTO [synchronizer].[EVENT_CALC_RULES_BASIS]
           ([EVENT_RULES_FK]
           ,[BASIS_POSITION]
           ,[BASIS_VALUE]                                        
           ,[BASIS_DN_FK]
           ,[BASIS_ORG_PRECEDENCE]    
           ,[BASIS_RESULT_TYPE_CR_FK]  
           ,[BASIS_ACTION_CR_FK]                                          
           ,[BASIS_EFFECTIVE_DATE]
           ,[BASIS_EVENT_FK]                      
           ,[BASIS_ORG_ENTITY_STRUCTURE_FK]
           ,[BASIS_CUST_ENTITY_STRUCTURE_FK]
           ,[BASIS_SUPL_ENTITY_STRUCTURE_FK] 
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP])
SELECT
            ECR.EVENT_RULES_ID
           ,TSRB.BASIS_POSITION
           ,ECBASIS.RESULT  AS BASIS_VALUE
           ,TSRB.BASIS_DN_FK
           ,( SELECT ECT.PRECEDENCE FROM EPACUBE.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
              WHERE ECT.RECORD_STATUS_CR_FK = 1
              AND   ECT.DATA_NAME_FK = ( SELECT ES.DATA_NAME_FK FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
                                         WHERE ES.ENTITY_STRUCTURE_ID = EC.ORG_ENTITY_STRUCTURE_FK ) 
            ) AS BASIS_ORG_PRECEDENCE                                                              
           ,ECBASIS.RESULT_TYPE_CR_FK   -- BASIS_RESULT_TYPE_CR_FK            
           ,ISNULL ( ( SELECT RH.BASIS_ACTION_CR_FK
					  FROM synchronizer.RULES_HIERARCHY RH 
					  INNER JOIN synchronizer.RULES_STRUCTURE RS ON (RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK)
					  WHERE RS.RULES_STRUCTURE_ID = ECR.RULES_STRUCTURE_FK )                                 
                   ,780 )
           ,ECBASIS.RESULT_EFFECTIVE_DATE                      
           ,ECBASIS.EVENT_ID  -- BASIS_EVENT_FK                                  
           ,ECBASIS.ORG_ENTITY_STRUCTURE_FK
           ,ECBASIS.CUST_ENTITY_STRUCTURE_FK
           ,ECBASIS.SUPL_ENTITY_STRUCTURE_FK           
           ,1     -- RECORD_STATUS_CR_FK
           ,GETDATE ()  --UPDATE_TIMESTAMP
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ( ECR.EVENT_FK = EC.EVENT_ID )
INNER JOIN #TS_RULES_BASIS TSRB
  ON ( TSRB.RULES_FK = ECR.RULES_FK )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.DATA_NAME_ID = TSRB.BASIS_DN_FK )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )  
INNER JOIN synchronizer.EVENT_CALC ECBASIS WITH (NOLOCK)
 ON ( ECBASIS.JOB_FK = EC.JOB_FK
 AND  ECBASIS.RESULT_DATA_NAME_FK = TSRB.BASIS_DN_FK  
 AND  ECBASIS.PRODUCT_STRUCTURE_FK = EC.PRODUCT_STRUCTURE_FK
 AND  ECBASIS.ORG_ENTITY_STRUCTURE_FK = EC.ORG_ENTITY_STRUCTURE_FK
 AND  ISNULL ( ECBASIS.CUST_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( EC.CUST_ENTITY_STRUCTURE_FK, -999 )   
 AND  ISNULL ( ECBASIS.SUPL_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( EC.SUPL_ENTITY_STRUCTURE_FK, -999 )  
    )
INNER JOIN #RDNFK RDNFK ON DN.DATA_NAME_ID = rdnfk.RESULT_DATA_NAME_FK
WHERE 1 = 1
AND   EC.JOB_FK = @in_analysis_job_fk
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1
AND   DS.EVENT_TYPE_CR_FK IN ( 152 )    

SET @status_desc = 'Ln 1014 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;



-----------------------------------------------------------------------------------------
-- INSERT into synchronizer.EVENT_CALC_RULES_BASIS  --	 SAME JOB MARGIN RESULTS 
--     Value should be on the same row;  Otherwise NOT SURE...SHOULD BE THERE WHEN NEEDED
-----------------------------------------------------------------------------------------



INSERT INTO [synchronizer].[EVENT_CALC_RULES_BASIS]
           ([EVENT_RULES_FK]
           ,[BASIS_POSITION]
           ,[BASIS_VALUE]                                        
           ,[BASIS_DN_FK]
           ,[BASIS_ORG_PRECEDENCE]    
           ,[BASIS_RESULT_TYPE_CR_FK]    
           ,[BASIS_ACTION_CR_FK]                                        
           ,[BASIS_EFFECTIVE_DATE]
           ,[BASIS_EVENT_FK]                      
           ,[BASIS_ORG_ENTITY_STRUCTURE_FK]
           ,[BASIS_CUST_ENTITY_STRUCTURE_FK]
           ,[BASIS_SUPL_ENTITY_STRUCTURE_FK] 
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP])
SELECT
            ECR.EVENT_RULES_ID
           ,TSRB.BASIS_POSITION
           ,CASE TSRB.BASIS_DN_FK
            WHEN 111602 THEN EC.SELL_PRICE_CUST_AMT
            WHEN 111501 THEN EC.REBATE_CB_AMT
            WHEN 111202 THEN EC.ORDER_COGS_AMT
            ELSE NULL
            END AS BASIS_VALUE
           ,TSRB.BASIS_DN_FK
           ,( SELECT ECT.PRECEDENCE FROM EPACUBE.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
              WHERE ECT.RECORD_STATUS_CR_FK = 1
              AND   ECT.DATA_NAME_FK = ( SELECT ES.DATA_NAME_FK FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
                                         WHERE ES.ENTITY_STRUCTURE_ID = EC.ORG_ENTITY_STRUCTURE_FK ) 
            ) AS BASIS_ORG_PRECEDENCE                                                   
           ,EC.RESULT_TYPE_CR_FK   --- RESULT TYPE IS SAME AS THE EVENT_CALC ROW        
           ,ISNULL ( ( SELECT RH.BASIS_ACTION_CR_FK
					  FROM synchronizer.RULES_HIERARCHY RH 
					  INNER JOIN synchronizer.RULES_STRUCTURE RS ON (RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK)
					  WHERE RS.RULES_STRUCTURE_ID = ECR.RULES_STRUCTURE_FK )                                 
                   ,780 )
           ,EC.RESULT_EFFECTIVE_DATE                      
           ,EC.EVENT_ID  -- BASIS_EVENT_FK                                              
           ,EC.ORG_ENTITY_STRUCTURE_FK
           ,EC.CUST_ENTITY_STRUCTURE_FK
           ,EC.SUPL_ENTITY_STRUCTURE_FK           
           ,1     -- RECORD_STATUS_CR_FK
           ,GETDATE ()  --UPDATE_TIMESTAMP
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ( ECR.EVENT_FK = EC.EVENT_ID )
INNER JOIN #TS_RULES_BASIS TSRB
  ON ( TSRB.RULES_FK = ECR.RULES_FK )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.DATA_NAME_ID = TSRB.BASIS_DN_FK )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )  
WHERE 1 = 1
AND   EC.JOB_FK = @in_analysis_job_fk   
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 	
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1
AND   DS.EVENT_TYPE_CR_FK IN ( 158 )   -- MARGIN RESULTS
 
SET @status_desc = 'Ln 1089 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

----------------------------------------------------------------------------------------------
-- INSERT into synchronizer.EVENT_CALC_RULES_BASIS  --	 FUTURE EVENTS ( VALUES AND RESULTS )
----------------------------------------------------------------------------------------------

TRUNCATE TABLE #TS_EVENT_BASIS


INSERT INTO  #TS_EVENT_BASIS( 
    EVENT_FK,
    EVENT_DATA_EVENT_FK )
SELECT EC.EVENT_ID AS EVENT_FK
     , ED.EVENT_ID AS EVENT_DATA_FK
FROM #TS_RULES_BASIS TSRB
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)ON DN.DATA_NAME_ID = TSRB.BASIS_DN_FK
INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) ON DS.DATA_SET_ID = DN.DATA_SET_FK  
INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
 ON ( ED.DATA_NAME_FK = DN.PARENT_DATA_NAME_FK )
INNER JOIN synchronizer.EVENT_CALC EC WITH (NOLOCK) 
 ON ( EC.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
 AND  EC.ORG_ENTITY_STRUCTURE_FK IN ( SELECT XREF_ORG_ENTITY_STRUCTURE_FK
										   FROM  #TS_ORG_XREF 
										   WHERE ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK )
	  )  
WHERE 1 = 1
AND   (ED.EVENT_STATUS_CR_FK IN (80, 84)
	Or
	((select value from epacube.EPACUBE_PARAMS where name = 'EPACUBE CUSTOMER') = 'Johnstone' and ED.Event_Status_CR_FK = 85))

AND   ED.EVENT_EFFECTIVE_DATE <= TSRB.RESULT_EFFECTIVE_DATE
AND   EC.JOB_FK = @in_analysis_job_fk
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
--------------------
AND   EC.RESULT_TYPE_CR_FK = 711   --- FUTURE 
AND   DS.EVENT_TYPE_CR_FK IN ( 151, 152 )   -- PRODUCT VALUES OR RESULTS
---------------   ONLY IF VALID VALUE
AND ( 
		( DS.COLUMN_NAME = 'NET_VALUE1' AND ISNULL ( ED.NET_VALUE1_NEW, 0 ) > 0 )       
	 OR
		( DS.COLUMN_NAME = 'NET_VALUE2' AND ISNULL ( ED.NET_VALUE2_NEW, 0 ) > 0 )       
	 OR
		( DS.COLUMN_NAME = 'NET_VALUE3' AND ISNULL ( ED.NET_VALUE3_NEW, 0 ) > 0 )       
	 OR
		( DS.COLUMN_NAME = 'NET_VALUE4' AND ISNULL ( ED.NET_VALUE4_NEW, 0 ) > 0 )       
	 OR
		( DS.COLUMN_NAME = 'NET_VALUE5' AND ISNULL ( ED.NET_VALUE5_NEW, 0 ) > 0 )       
	 OR
		( DS.COLUMN_NAME = 'NET_VALUE6' AND ISNULL ( ED.NET_VALUE6_NEW, 0 ) > 0 )       
	)     

SET @status_desc = 'Ln 1149 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


INSERT INTO [synchronizer].[EVENT_CALC_RULES_BASIS]
           ([EVENT_RULES_FK]
           ,[BASIS_POSITION]
           ,[BASIS_VALUE]                                        
           ,[BASIS_DN_FK]
           ,[BASIS_ORG_PRECEDENCE]    
           ,[BASIS_RESULT_TYPE_CR_FK]    
           ,[BASIS_ACTION_CR_FK]                                        
           ,[BASIS_EFFECTIVE_DATE]
           ,[BASIS_EVENT_FK]                      
           ,[BASIS_ORG_ENTITY_STRUCTURE_FK]
           ,[BASIS_CUST_ENTITY_STRUCTURE_FK]
           ,[BASIS_SUPL_ENTITY_STRUCTURE_FK] 
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP])
SELECT
            ECR.EVENT_RULES_ID
           ,TSRB.BASIS_POSITION
           ,CASE DS.COLUMN_NAME
            WHEN 'NET_VALUE1' THEN ED.NET_VALUE1_NEW
            WHEN 'NET_VALUE2' THEN ED.NET_VALUE2_NEW
            WHEN 'NET_VALUE3' THEN ED.NET_VALUE3_NEW
            WHEN 'NET_VALUE4' THEN ED.NET_VALUE4_NEW
            WHEN 'NET_VALUE5' THEN ED.NET_VALUE5_NEW
            WHEN 'NET_VALUE6' THEN ED.NET_VALUE6_NEW            
            END  AS BASIS_VALUE           
           ,TSRB.BASIS_DN_FK           
           ,( SELECT ECT.PRECEDENCE FROM EPACUBE.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
              WHERE ECT.RECORD_STATUS_CR_FK = 1
              AND   ECT.DATA_NAME_FK = ( SELECT ES.DATA_NAME_FK FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
                                         WHERE ES.ENTITY_STRUCTURE_ID = ED.ORG_ENTITY_STRUCTURE_FK ) 
            ) AS BASIS_ORG_PRECEDENCE                                                              
           ,711   -- BASIS_RESULT_TYPE_CR_FK     --- FUTURE       
           ,ISNULL ( ( SELECT RH.BASIS_ACTION_CR_FK
					  FROM synchronizer.RULES_HIERARCHY RH 
					  INNER JOIN synchronizer.RULES_STRUCTURE RS ON (RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK)
					  WHERE RS.RULES_STRUCTURE_ID = ECR.RULES_STRUCTURE_FK )                                 
                   ,780 )
           ,ED.EVENT_EFFECTIVE_DATE                      
           ,ED.EVENT_ID  -- BASIS_EVENT_FK                                   
           ,ED.ORG_ENTITY_STRUCTURE_FK
           ,NULL  -- CUST_ENTITY_STRUCTURE_FK
           ,NULL  -- SUPL_ENTITY_STRUCTURE_FK           
           ,1     -- RECORD_STATUS_CR_FK
           ,GETDATE ()  --UPDATE_TIMESTAMP
FROM #TS_EVENT_BASIS TSEB
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK) 
  ON ( ECR.EVENT_FK = TSEB.EVENT_FK )
INNER JOIN #TS_RULES_BASIS TSRB
  ON ( TSRB.RULES_FK = ECR.RULES_FK )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.DATA_NAME_ID = TSRB.BASIS_DN_FK )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )  
INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
 ON ( ED.DATA_NAME_FK = DN.PARENT_DATA_NAME_FK   --- USE PARENT
 AND  ED.EVENT_ID = TSEB.EVENT_DATA_EVENT_FK 
    --- BASIS PARENT ONLY IND = 1 THEN DO NOT ALLOW WHSE LEVEL BASIS VALUES
 AND  (       ISNULL ( TSRB.BASIS_USE_CORP_ONLY_IND, 0 ) = 0
       OR   ( ISNULL ( TSRB.BASIS_USE_CORP_ONLY_IND, 0 ) = 1 
         AND  ISNULL ( ED.ORG_ENTITY_STRUCTURE_FK, -999 ) = 1 )
      ) 	  
    )
WHERE 1 = 1
AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1


TRUNCATE TABLE #TS_EVENT_BASIS    ---- TRUNCATE AFTER TO KEEP DBTEMP LOW

SET @status_desc = 'Ln 1222 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


-----------------------------------------------------------------------------------------
-- INSERT into synchronizer.EVENT_CALC_RULES_BASIS  --- PRICESHEET
---  CURRENT, FUTURE AND HISTORICAL SCENARIOS
-----------------------------------------------------------------------------------------


TRUNCATE TABLE #TS_PRICESHEET

INSERT INTO #TS_PRICESHEET (
    TS_RULES_BASIS_FK,
    EVENT_RULES_FK,    
	EVENT_FK,
	RULES_FK,
	RESULT_EFFECTIVE_DATE,
	PRICESHEET_FK,
	BASIS_PRICESHEET_NAME,
	BASIS_PRICESHEET_DATE,
	BASIS_VALUE,
	EFFECTIVE_DATE,
	ORG_ENTITY_STRUCTURE_FK,
	ENTITY_STRUCTURE_FK,
	DRANK
   )
SELECT A.TS_RULES_BASIS_FK
      ,A.EVENT_RULES_FK
      ,A.EVENT_FK
      ,A.RULES_FK
      ,A.RESULT_EFFECTIVE_DATE
      ,A.PRICESHEET_FK 
      ,A.BASIS_PRICESHEET_NAME
      ,A.BASIS_PRICESHEET_DATE
      ,A.BASIS_VALUE
      ,A.EFFECTIVE_DATE
      ,A.ORG_ENTITY_STRUCTURE_FK
      ,A.ENTITY_STRUCTURE_FK
      ,A.DRANK
FROM (
SELECT TSRB.TS_RULES_BASIS_ID AS TS_RULES_BASIS_FK  
     , ECR.EVENT_RULES_ID AS EVENT_RULES_FK
     , ECR.EVENT_FK, ECR.RULES_FK
     , EC.RESULT_EFFECTIVE_DATE 
     , PSHT.PRICESHEET_ID AS PRICESHEET_FK
     , PSHT.PRICESHEET_NAME AS BASIS_PRICESHEET_NAME
     , PSHT.EFFECTIVE_DATE  AS BASIS_PRICESHEET_DATE   --- WILL ALWAYS EXIST 
     , CASE DS.COLUMN_NAME
       WHEN 'NET_VALUE1' THEN  ISNULL ( PSHT.NET_VALUE1, 0 ) 
       WHEN 'NET_VALUE2' THEN  ISNULL ( PSHT.NET_VALUE2, 0 ) 
       WHEN 'NET_VALUE3' THEN  ISNULL ( PSHT.NET_VALUE3, 0 ) 
       WHEN 'NET_VALUE4' THEN  ISNULL ( PSHT.NET_VALUE4, 0 )
       WHEN 'NET_VALUE5' THEN  ISNULL ( PSHT.NET_VALUE5, 0 )
       WHEN 'NET_VALUE6' THEN  ISNULL ( PSHT.NET_VALUE6, 0 ) 
       END  AS BASIS_VALUE
     , PSHT.EFFECTIVE_DATE
     , PSHT.ORG_ENTITY_STRUCTURE_FK
     , PSHT.ENTITY_STRUCTURE_FK
     , ( DENSE_RANK() OVER ( PARTITION BY ECR.EVENT_FK, ECR.RULES_FK
								ORDER BY  ECT.PRECEDENCE ASC, PSHT.EFFECTIVE_DATE DESC, PSHT.PRICESHEET_ID DESC )      
         ) AS DRANK
---------------    
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ( ECR.EVENT_FK = EC.EVENT_ID )
INNER JOIN #TS_RULES_BASIS TSRB
  ON ( TSRB.RULES_FK = ECR.RULES_FK )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.DATA_NAME_ID = TSRB.BASIS_DN_FK )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )  
INNER JOIN marginmgr.PRICESHEET PSHT WITH (NOLOCK)
 ON ( PSHT.RECORD_STATUS_CR_FK = 1
 AND  ISNULL ( PSHT.PRODUCT_STRUCTURE_FK, -999 ) = ISNULL ( EC.PRODUCT_STRUCTURE_FK, -999 ) 
 AND  PSHT.DATA_NAME_FK = DN.PARENT_DATA_NAME_FK ) -- PARENT OF BASIS VALUE 
    --- BASIS VALUES ARE ORG TO ORG;  CORP TO ORG   OR ORG TO CORP 
INNER JOIN  #TS_ORG_XREF TOX
ON (     TOX.ORG_ENTITY_STRUCTURE_FK = EC.ORG_ENTITY_STRUCTURE_FK 
    AND  TOX.XREF_ORG_ENTITY_STRUCTURE_FK = PSHT.ORG_ENTITY_STRUCTURE_FK  )
INNER JOIN epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
ON (  ECT.RECORD_STATUS_CR_FK = 1
AND   ECT.ENTITY_CLASS_CR_FK = 10101
AND   ECT.DATA_NAME_FK = (  SELECT ES.DATA_NAME_FK 
							FROM epacube.ENTITY_STRUCTURE ES WITH (NOLOCK)
							WHERE ES.ENTITY_STRUCTURE_ID = PSHT.ORG_ENTITY_STRUCTURE_FK )    )
WHERE 1 = 1
AND   EC.JOB_FK = @in_analysis_job_fk  
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1
AND   DS.EVENT_TYPE_CR_FK IN ( 159 )
---------------
AND   (
        (    TSRB.BASIS_PRICESHEET_NAME IS NULL
         AND TSRB.BASIS_PRICESHEET_DATE  IS NULL  
         AND TSRB.BASIS_PRICESHEET_DATE  IS NULL 
         AND ISNULL ( PSHT.EFFECTIVE_DATE, EC.RESULT_EFFECTIVE_DATE ) <= TSRB.RESULT_EFFECTIVE_DATE
         )         
       OR
        (    TSRB.BASIS_PRICESHEET_DATE IS NULL
        AND  TSRB.BASIS_PRICESHEET_NAME IS NOT NULL
        AND  PSHT.PRICESHEET_NAME = TSRB.BASIS_PRICESHEET_NAME )  -- DO NOT CHECK DATE -- SXE HISTORICAL
       OR
        (    TSRB.BASIS_PRICESHEET_DATE IS NOT NULL
        AND  ISNULL ( PSHT.EFFECTIVE_DATE, EC.RESULT_EFFECTIVE_DATE ) <= TSRB.BASIS_PRICESHEET_DATE 
        AND ISNULL ( PSHT.EFFECTIVE_DATE, EC.RESULT_EFFECTIVE_DATE ) <= TSRB.RESULT_EFFECTIVE_DATE
        )
      )
-------------
AND ( 
		( DS.COLUMN_NAME = 'NET_VALUE1' AND ISNULL ( PSHT.NET_VALUE1, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE2' AND ISNULL ( PSHT.NET_VALUE2, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE3' AND ISNULL ( PSHT.NET_VALUE3, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE4' AND ISNULL ( PSHT.NET_VALUE4, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE5' AND ISNULL ( PSHT.NET_VALUE5, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE6' AND ISNULL ( PSHT.NET_VALUE6, 0 ) > 0 )       
    )                       
    ) A
WHERE A.DRANK = 1

SET @status_desc = 'Ln 1352 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


INSERT INTO [synchronizer].[EVENT_CALC_RULES_BASIS]
           ([EVENT_RULES_FK]
           ,[BASIS_POSITION]
           ,[BASIS_VALUE]                                        
           ,[BASIS_DN_FK]
           ,[BASIS_PRICESHEET_NAME]               
           ,[BASIS_PRICESHEET_DATE]                          
           ,[BASIS_ORG_PRECEDENCE]    
           ,[BASIS_RESULT_TYPE_CR_FK]  
           ,[BASIS_ACTION_CR_FK]                                          
           ,[BASIS_EFFECTIVE_DATE]
           ,[BASIS_EVENT_FK]                      
           ,[BASIS_ORG_ENTITY_STRUCTURE_FK]
           ,[BASIS_CUST_ENTITY_STRUCTURE_FK]
           ,[BASIS_SUPL_ENTITY_STRUCTURE_FK] 
           ,[RECORD_STATUS_CR_FK]
           ,[UPDATE_TIMESTAMP])
SELECT
            TSPSHT.EVENT_RULES_FK
           ,TSRB.BASIS_POSITION
           ,TSPSHT.BASIS_VALUE           
           ,TSRB.BASIS_DN_FK
           ,TSPSHT.BASIS_PRICESHEET_NAME
           ,TSPSHT.BASIS_PRICESHEET_DATE
           ,ISNULL (
           ( SELECT ECT.PRECEDENCE FROM EPACUBE.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
              WHERE ECT.RECORD_STATUS_CR_FK = 1
              AND   ECT.DATA_NAME_FK = ( SELECT ES.DATA_NAME_FK FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
                                         WHERE ES.ENTITY_STRUCTURE_ID = TSPSHT.ORG_ENTITY_STRUCTURE_FK ) )
            ,5  --- FOR NOW FOR TERRRITORY TO WORK
            ) AS BASIS_ORG_PRECEDENCE                                                   
           ,CASE WHEN ( ISNULL ( TSPSHT.EFFECTIVE_DATE, GETDATE() ) > TSPSHT.RESULT_EFFECTIVE_DATE )
            THEN 711  --- FUTURE
            ELSE 710
            END   -- BASIS_RESULT_TYPE_CR_FK     
           ,ISNULL ( ( SELECT RH.BASIS_ACTION_CR_FK
					  FROM synchronizer.RULES_HIERARCHY RH 
					  INNER JOIN synchronizer.RULES_STRUCTURE RS ON (RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK)
					  INNER JOIN synchronizer.RULES R ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID )
					  WHERE R.RULES_ID = TSPSHT.RULES_FK )       
					, 780 )                          
           ,TSPSHT.EFFECTIVE_DATE  ---PSHT.EFFECTIVE_DATE                   
           ,NULL  -- BASIS_EVENT_FK                                   
           ,TSPSHT.ORG_ENTITY_STRUCTURE_FK
           ,CASE DS.ENTITY_STRUCTURE_EC_CR_FK
            WHEN 10104 THEN TSPSHT.ENTITY_STRUCTURE_FK
            ELSE NULL
            END
           ,CASE DS.ENTITY_STRUCTURE_EC_CR_FK
            WHEN 10103 THEN TSPSHT.ENTITY_STRUCTURE_FK
            ELSE NULL
            END            
           ,1     -- RECORD_STATUS_CR_FK
           ,GETDATE ()  --UPDATE_TIMESTAMP
---------------    
FROM #TS_PRICESHEET TSPSHT
INNER JOIN #TS_RULES_BASIS TSRB
  ON ( TSRB.TS_RULES_BASIS_ID = TSPSHT.TS_RULES_BASIS_FK )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.DATA_NAME_ID = TSRB.BASIS_DN_FK )
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )  
WHERE 1 = 1

SET @status_desc = 'Ln 1420 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;



TRUNCATE TABLE #TS_PRICESHEET    ---- TRUNCATE AFTER TO KEEP DBTEMP LOW


------------------------------------------------------------------------------------------------
-- Perform Ranking of Basis Values where value is NOT NULL AND BASIS_ACTION_CR_FK = 780
------------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_CALC_RULES_BASIS
SET BASIS_RANK = 1
WHERE BASIS_ACTION_CR_FK = 780  --  FIRST
AND   EVENT_RULES_BASIS_ID IN (
	SELECT A.EVENT_RULES_BASIS_ID
	FROM (
			SELECT
			ECRB.EVENT_RULES_BASIS_ID,
			( DENSE_RANK() OVER ( PARTITION BY ECRB.EVENT_RULES_FK, ECRB.BASIS_POSITION
						   ORDER BY ECRB.BASIS_ORG_PRECEDENCE ASC, 
						            ECRB.BASIS_RESULT_TYPE_CR_FK DESC, 									
                                    ECRB.BASIS_EFFECTIVE_DATE DESC,  
                                    ECRB.BASIS_VALUE ASC,
									ECRB.EVENT_RULES_BASIS_ID ASC) )AS BASIS_RANK   
			FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
			INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK) 
			  ON ( ECR.EVENT_FK = EC.EVENT_ID )			
            INNER JOIN synchronizer.EVENT_CALC_RULES_BASIS ECRB WITH (NOLOCK)			
			  ON ( ECR.EVENT_RULES_ID = ECRB.EVENT_RULES_FK )
			WHERE 1 = 1
			AND   EC.JOB_FK = @in_analysis_job_fk 
			AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
			AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
			AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1
            AND   ECRB.BASIS_VALUE IS NOT NULL
            AND   ECRB.BASIS_ACTION_CR_FK = 780  -- FIRST
		) A
		WHERE A.BASIS_RANK = 1
  )

SET @status_desc = 'Ln 1466 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


------------------------------------------------------------------------------------------------
-- Perform Ranking of Basis Values where value is NOT NULL AND BASIS_ACTION_CR_FK = 781
------------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_CALC_RULES_BASIS
SET BASIS_RANK = 1
WHERE BASIS_ACTION_CR_FK = 781  --  EFF_DATE
AND   EVENT_RULES_BASIS_ID IN (
	SELECT A.EVENT_RULES_BASIS_ID
	FROM (
			SELECT
			ECRB.EVENT_RULES_BASIS_ID,
			( DENSE_RANK() OVER ( PARTITION BY ECRB.EVENT_RULES_FK, ECRB.BASIS_POSITION
						   ORDER BY ECRB.BASIS_EFFECTIVE_DATE DESC,  
						            ECRB.BASIS_ORG_PRECEDENCE ASC, 
						            ECRB.BASIS_RESULT_TYPE_CR_FK DESC, 									                                    
                                    ECRB.BASIS_VALUE ASC,
									ECRB.EVENT_RULES_BASIS_ID ASC) )AS BASIS_RANK   
			FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
			INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK) 
			  ON ( ECR.EVENT_FK = EC.EVENT_ID )			
            INNER JOIN synchronizer.EVENT_CALC_RULES_BASIS ECRB WITH (NOLOCK)			
			  ON ( ECR.EVENT_RULES_ID = ECRB.EVENT_RULES_FK )
			WHERE 1 = 1
			AND   EC.JOB_FK = @in_analysis_job_fk 
			AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
			AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
			AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1
            AND   ECRB.BASIS_VALUE IS NOT NULL
            AND   ECRB.BASIS_ACTION_CR_FK = 781  -- EFF_DATE
		) A
		WHERE A.BASIS_RANK = 1
  )

SET @status_desc = 'Ln 1508 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


-----------------------------------------------------------------------------------------
-- OPERANDS VALUES ADDED TO FIND BASIS TO SIMPLIFY CALCULATION
-----------------------------------------------------------------------------------------

---------------------------------------------------
-- Insert into #TS_RULES_BASIS_OPERANDS
---------------------------------------------------

TRUNCATE TABLE #TS_RULES_BASIS_OPERANDS

INSERT INTO #TS_RULES_BASIS_OPERANDS (
	EVENT_FK,
	EVENT_RULES_FK,
	RULES_FK,
	RULES_ACTION_FK,
	BASIS_POSITION,
	RULES_FILTER_LOOKUP_OPERAND_FK,
	OPERAND_FILTER_DN_FK,
	OPERAND_FILTER_OPERATOR_CR_FK,
	OPERAND_FILTER_VALUE,
	FILTER_ENTITY_STRUCTURE_FK
) 
SELECT 
	  A.EVENT_FK
	 ,A.EVENT_RULES_FK
	 ,A.RULES_FK
	 ,A.RULES_ACTION_FK
	 ,A.BASIS_POSITION
	 ,A.RULES_FILTER_LOOKUP_OPERAND_FK
	 ,A.OPERAND_FILTER_DN_FK 
	 ,A.OPERAND_FILTER_OPERATOR_CR_FK
	 ,A.OPERAND_FILTER_VALUE
	 ,A.FILTER_ENTITY_STRUCTURE_FK
FROM (
SELECT 
      ECR.EVENT_FK
     ,ECR.EVENT_RULES_ID AS EVENT_RULES_FK
     ,ECR.RULES_FK 
     ,RAO.RULES_ACTION_FK
     ,RAO.BASIS_POSITION
     ,RAO.RULES_FILTER_LOOKUP_OPERAND_FK
     ,RAO.OPERAND_FILTER_DN_FK 
     ,RAO.OPERAND_FILTER_OPERATOR_CR_FK
     ----- JASCO ---- TEST IF THIS LOGIC WILL WORK FOR FINDING BY EVENT THE AVG_LME VALUE to compare
     -----
     ,CASE ISNULL ( RAO.OPERAND_FILTER_OPERATOR_CR_FK, -999 )   --- was RAO.RULES_FILTER_LOOKUP_OPERAND_FK
      WHEN -999 THEN NULL
      ELSE CASE DSX.ENTITY_CLASS_CR_FK 
           WHEN 10109 THEN 
				( SELECT TOP 1 VCPD.CURRENT_DATA
				  FROM epacube.V_CURRENT_PRODUCT_DATA VCPD
				  WHERE VCPD.DATA_NAME_FK = RAO.OPERAND_FILTER_DN_FK
				  AND   VCPD.PRODUCT_STRUCTURE_FK = EC.PRODUCT_STRUCTURE_FK
				  AND   (   
				          (    ISNULL ( DSX.ORG_IND, -999 ) = 1
				           AND VCPD.ORG_ENTITY_STRUCTURE_FK = EC.ORG_ENTITY_STRUCTURE_FK )
				        OR 
				          (    ISNULL ( DSX.ORG_IND, -999 ) <> 1
				           AND VCPD.ORG_ENTITY_STRUCTURE_FK = 1 )
                        )				        
				  AND   (   
				          (    ISNULL ( DSX.ENTITY_STRUCTURE_EC_CR_FK, -999 ) = 10103 -- SUPPLIER
				           AND ISNULL ( VCPD.ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( EC.SUPL_ENTITY_STRUCTURE_FK, -999 ) )
				        OR 
				          (    ISNULL ( DSX.ENTITY_STRUCTURE_EC_CR_FK, -999 ) <> 10103  ) -- SUPPLIER
                        )				        
                 )
          ELSE
				( SELECT TOP 1 VCED.CURRENT_DATA
				  FROM epacube.V_CURRENT_ENTITY_DATA VCED
				  WHERE VCED.DATA_NAME_FK = RAO.OPERAND_FILTER_DN_FK
				  AND   (
				          (   ISNULL ( DSX.ENTITY_CLASS_CR_FK, -999 )  = 10103  -- SUPPLIER
				          AND VCED.ENTITY_STRUCTURE_FK = EC.SUPL_ENTITY_STRUCTURE_FK )
				        OR
				          (   ISNULL ( DSX.ENTITY_CLASS_CR_FK, -999 )  = 10104  -- CUSTOMER
				          AND VCED.ENTITY_STRUCTURE_FK = EC.CUST_ENTITY_STRUCTURE_FK )
				        )
				 )
	      END
      END  AS OPERAND_FILTER_VALUE
      ---- jasco... need to change this to be driven like above with entity class
      ----
     ,CASE ISNULL ( RAO.OPERAND_FILTER_OPERATOR_CR_FK, -999 )   --- was RAO.RULES_FILTER_LOOKUP_OPERAND_FK
      WHEN -999 THEN NULL
      ELSE EC.CUST_ENTITY_STRUCTURE_FK  -- HARDCODE FOR NOW COME BACK AND FIX WITH FINAL TUNING
      END FILTER_ENTITY_STRUCTURE_FK
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ( ECR.EVENT_FK = EC.EVENT_ID )
INNER JOIN synchronizer.RULES R WITH (NOLOCK)
  ON ( R.RULES_ID = ECR.RULES_FK )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON  ( RA.RULES_FK = R.RULES_ID )
INNER JOIN synchronizer.RULES_ACTION_OPERANDS RAO WITH (NOLOCK)
  ON  ( RAO.RULES_ACTION_FK = RA.RULES_ACTION_ID  )
LEFT JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK) ON DNX.DATA_NAME_ID = RAO.OPERAND_FILTER_DN_FK
LEFT JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK) ON DSX.DATA_SET_ID = DNX.DATA_SET_FK     
WHERE 1 = 1 
AND   EC.JOB_FK = @in_analysis_job_fk  
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1
) A

SET @status_desc = 'Ln 1624 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


-----------------------------------------------------------------------------------------
-- ASSIGN OPERANDS BASED ON THE OPERAND_FILTER_OPERATOR(S) SUPPORTED BY THE VIEW ( 9000, 9019 )
--		OTHER SPECIALIZED OPERAND_FILTER_OPERATOR(S) HAVE INDIVIDUAL SQLS BELOW
-----------------------------------------------------------------------------------------

INSERT INTO [synchronizer].[EVENT_CALC_RULES_BASIS]
           ([EVENT_RULES_FK]
           ,[BASIS_POSITION]
           ,[BASIS_OPERAND]    
           ,[RULES_FILTER_LOOKUP_OPERAND_FK]   
           ,[BASIS_OPERAND_FILTER_DN_FK]
           ,[BASIS_OPERAND_FILTER_VALUE]
           ,[BASIS_OPERAND_POSITION] 
           ,[BASIS_OPERAND_FILTER_OPERATOR_CR_FK]                                 
           ,[BASIS_RESULT_TYPE_CR_FK]           
           ,[BASIS_EFFECTIVE_DATE]
           )
SELECT 
      TSRBO.EVENT_RULES_FK
     ,TSRBO.BASIS_POSITION
     ,VRBO.BASIS_OPERAND
     ,TSRBO.RULES_FILTER_LOOKUP_OPERAND_FK
     ,TSRBO.OPERAND_FILTER_DN_FK
     ,TSRBO.OPERAND_FILTER_VALUE
     ,VRBO.BASIS_OPERAND_POSITION
     ,TSRBO.OPERAND_FILTER_OPERATOR_CR_FK
     ,710  -- CURRENT
     ,VRBO.BASIS_EFFECTIVE_DATE
-------------------      
FROM  #TS_RULES_BASIS_OPERANDS TSRBO
INNER JOIN synchronizer.V_RULES_BASIS_OPERANDS VRBO
  ON  ( VRBO.RULES_ACTION_FK = TSRBO.RULES_ACTION_FK
  AND   VRBO.BASIS_POSITION  = TSRBO.BASIS_POSITION 
  AND   VRBO.OPERAND_FILTER_DN_FK = TSRBO.OPERAND_FILTER_DN_FK 
  AND   VRBO.OPERAND_FILTER_OPERATOR_CR_FK = TSRBO.OPERAND_FILTER_OPERATOR_CR_FK
  AND   VRBO.BASIS_OPERAND_FILTER_VALUE  = TSRBO.OPERAND_FILTER_VALUE )
WHERE 1 = 1
AND   TSRBO.RULES_FILTER_LOOKUP_OPERAND_FK IS NULL --- DO NOT INLCUDE FILTER SPECIFIC OPERAND LOOKUPS

SET @status_desc = 'Ln 1667 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;


-----------------------------------------------------------------------------------------
-- OPERAND_FILTER_OPERATOR IS QTY BREAKS ( 9020 ) COALESCE QTY BRK SALES_QTY ONLY
-----------------------------------------------------------------------------------------


INSERT INTO [synchronizer].[EVENT_CALC_RULES_BASIS]
           ([EVENT_RULES_FK]
           ,[BASIS_POSITION]
           ,[BASIS_OPERAND]    
           ,[RULES_FILTER_LOOKUP_OPERAND_FK]   
           ,[BASIS_OPERAND_FILTER_DN_FK]
           ,[BASIS_OPERAND_FILTER_VALUE]
           ,[BASIS_OPERAND_POSITION]           
           ,[BASIS_OPERAND_FILTER_OPERATOR_CR_FK]                       
           ,[BASIS_RESULT_TYPE_CR_FK]           
           ,[BASIS_EFFECTIVE_DATE]
           )
SELECT 
      TSRBO.EVENT_RULES_FK
     ,TSRBO.BASIS_POSITION
     ,Case   --- GARY'S LOGIC FROM EPA-3226
		When isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value1 as numeric(18, 6)) or Cast(RAO.Filter_Value1 as numeric(18, 6)) = 0 then Cast(RAO.Operand1 as numeric(18, 6))
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value2 as numeric(18, 6)) or Cast(RAO.Filter_Value2 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value3 as numeric(18, 6)) or Cast(RAO.Filter_Value3 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value4 as numeric(18, 6)) or Cast(RAO.Filter_Value4 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand4, RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value5 as numeric(18, 6)) or Cast(RAO.Filter_Value5 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand5, RAO.Operand4, RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value6 as numeric(18, 6)) or Cast(RAO.Filter_Value6 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand6, RAO.Operand5, RAO.Operand4, RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value7 as numeric(18, 6)) or Cast(RAO.Filter_Value7 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand7, RAO.Operand6, RAO.Operand5, RAO.Operand4, RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value8 as numeric(18, 6)) or Cast(RAO.Filter_Value8 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand8, RAO.Operand7, RAO.Operand6, RAO.Operand5, RAO.Operand4, RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		else Cast(Coalesce(RAO.Operand9, RAO.Operand8, RAO.Operand7, RAO.Operand6, RAO.Operand5, RAO.Operand4, RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6)) 
      end as BASIS_OPERAND
     ,TSRBO.RULES_FILTER_LOOKUP_OPERAND_FK
     ,TSRBO.OPERAND_FILTER_DN_FK
     ,isnull(EC.MODEL_SALES_QTY, 0)   
     ,Case   --- GARY'S LOGIC FROM EPA-3226
		When isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value1 as numeric(18, 6)) or Cast(RAO.Filter_Value1 as numeric(18, 6)) = 0 then 1
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value2 as numeric(18, 6)) or Cast(RAO.Filter_Value2 as numeric(18, 6)) = 0 then 2
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value3 as numeric(18, 6)) or Cast(RAO.Filter_Value3 as numeric(18, 6)) = 0 then 3
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value4 as numeric(18, 6)) or Cast(RAO.Filter_Value4 as numeric(18, 6)) = 0 then 4
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value5 as numeric(18, 6)) or Cast(RAO.Filter_Value5 as numeric(18, 6)) = 0 then 5
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value6 as numeric(18, 6)) or Cast(RAO.Filter_Value6 as numeric(18, 6)) = 0 then 6
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value7 as numeric(18, 6)) or Cast(RAO.Filter_Value7 as numeric(18, 6)) = 0 then 7
		when isnull(EC.MODEL_SALES_QTY, 0) < Cast(RAO.Filter_Value8 as numeric(18, 6)) or Cast(RAO.Filter_Value8 as numeric(18, 6)) = 0 then 8
		else 9
      end as BASIS_OPERAND_POSITION
     ,TSRBO.OPERAND_FILTER_OPERATOR_CR_FK
     ,710  -- CURRENT
     ,( SELECT R.EFFECTIVE_DATE FROM synchronizer.RULES R WITH (NOLOCK) WHERE R.RULES_ID = TSRBO.RULES_FK ) AS BASIS_EFFECTIVE_DATE  
-------------------      
FROM  #TS_RULES_BASIS_OPERANDS TSRBO
INNER JOIN synchronizer.EVENT_CALC EC WITH (NOLOCK)
  ON ( EC.EVENT_ID = TSRBO.EVENT_FK )
INNER JOIN synchronizer.RULES_ACTION_OPERANDS RAO WITH (NOLOCK)
  ON ( RAO.RULES_ACTION_FK = TSRBO.RULES_ACTION_FK
  AND  RAO.BASIS_POSITION = TSRBO.BASIS_POSITION
  AND  RAO.OPERAND_FILTER_DN_FK = TSRBO.OPERAND_FILTER_DN_FK 
  AND  RAO.OPERAND_FILTER_OPERATOR_CR_FK = TSRBO.OPERAND_FILTER_OPERATOR_CR_FK )
WHERE 1 = 1
AND   TSRBO.OPERAND_FILTER_OPERATOR_CR_FK = 9020  --- COALESCE QTY BRK
AND   ISNULL ( TSRBO.OPERAND_FILTER_DN_FK, 107000 ) = 107000  ----MODEL_SALES_QTY ASSUMED IF NULL 

SET @status_desc = 'Ln 1732 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;




-----------------------------------------------------------------------------------------
-- OPERAND_FILTER_OPERATOR IS QTY BREAKS ( 9020 ) COALESCE QTY BRK 
--   WITH CONFIGURABLE OPERANDS_FILTER_DN_FK  ( JASCO EPA-3328 )
-----------------------------------------------------------------------------------------


INSERT INTO [synchronizer].[EVENT_CALC_RULES_BASIS]
           ([EVENT_RULES_FK]
           ,[BASIS_POSITION]
           ,[BASIS_OPERAND]    
           ,[RULES_FILTER_LOOKUP_OPERAND_FK]   
           ,[BASIS_OPERAND_FILTER_DN_FK]
           ,[BASIS_OPERAND_FILTER_VALUE]
           ,[BASIS_OPERAND_POSITION]           
           ,[BASIS_OPERAND_FILTER_OPERATOR_CR_FK]                       
           ,[BASIS_RESULT_TYPE_CR_FK]           
           ,[BASIS_EFFECTIVE_DATE]
           )
SELECT 
      TSRBO.EVENT_RULES_FK
     ,TSRBO.BASIS_POSITION
     ,Case   --- GARY'S LOGIC FROM EPA-3226
		When TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value1 as numeric(18, 6)) or Cast(RAO.Filter_Value1 as numeric(18, 6)) = 0 then Cast(RAO.Operand1 as numeric(18, 6))
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value2 as numeric(18, 6)) or Cast(RAO.Filter_Value2 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value3 as numeric(18, 6)) or Cast(RAO.Filter_Value3 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value4 as numeric(18, 6)) or Cast(RAO.Filter_Value4 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand4, RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value5 as numeric(18, 6)) or Cast(RAO.Filter_Value5 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand5, RAO.Operand4, RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value6 as numeric(18, 6)) or Cast(RAO.Filter_Value6 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand6, RAO.Operand5, RAO.Operand4, RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value7 as numeric(18, 6)) or Cast(RAO.Filter_Value7 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand7, RAO.Operand6, RAO.Operand5, RAO.Operand4, RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value8 as numeric(18, 6)) or Cast(RAO.Filter_Value8 as numeric(18, 6)) = 0 then Cast(Coalesce(RAO.Operand8, RAO.Operand7, RAO.Operand6, RAO.Operand5, RAO.Operand4, RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6))
		else Cast(Coalesce(RAO.Operand9, RAO.Operand8, RAO.Operand7, RAO.Operand6, RAO.Operand5, RAO.Operand4, RAO.Operand3, RAO.Operand2, RAO.Operand1) as numeric(18, 6)) 
      end as BASIS_OPERAND
     ,TSRBO.RULES_FILTER_LOOKUP_OPERAND_FK
     ,TSRBO.OPERAND_FILTER_DN_FK
     ,TSRBO.OPERAND_FILTER_VALUE       ---- ASSUMED LOOKED UP PRIOR
     ,Case   --- GARY'S LOGIC FROM EPA-3226
		When TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value1 as numeric(18, 6)) or Cast(RAO.Filter_Value1 as numeric(18, 6)) = 0 then 1
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value2 as numeric(18, 6)) or Cast(RAO.Filter_Value2 as numeric(18, 6)) = 0 then 2
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value3 as numeric(18, 6)) or Cast(RAO.Filter_Value3 as numeric(18, 6)) = 0 then 3
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value4 as numeric(18, 6)) or Cast(RAO.Filter_Value4 as numeric(18, 6)) = 0 then 4
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value5 as numeric(18, 6)) or Cast(RAO.Filter_Value5 as numeric(18, 6)) = 0 then 5
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value6 as numeric(18, 6)) or Cast(RAO.Filter_Value6 as numeric(18, 6)) = 0 then 6
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value7 as numeric(18, 6)) or Cast(RAO.Filter_Value7 as numeric(18, 6)) = 0 then 7
		when TSRBO.OPERAND_FILTER_VALUE < Cast(RAO.Filter_Value8 as numeric(18, 6)) or Cast(RAO.Filter_Value8 as numeric(18, 6)) = 0 then 8
		else 9
      end as BASIS_OPERAND_POSITION
     ,TSRBO.OPERAND_FILTER_OPERATOR_CR_FK
     ,710  -- CURRENT
     ,( SELECT R.EFFECTIVE_DATE FROM synchronizer.RULES R WITH (NOLOCK) WHERE R.RULES_ID = TSRBO.RULES_FK ) AS BASIS_EFFECTIVE_DATE  
-------------------      
FROM  #TS_RULES_BASIS_OPERANDS TSRBO
INNER JOIN synchronizer.EVENT_CALC EC WITH (NOLOCK)
  ON ( EC.EVENT_ID = TSRBO.EVENT_FK )
INNER JOIN synchronizer.RULES_ACTION_OPERANDS RAO WITH (NOLOCK)
  ON ( RAO.RULES_ACTION_FK = TSRBO.RULES_ACTION_FK
  AND  RAO.BASIS_POSITION = TSRBO.BASIS_POSITION
  AND  RAO.OPERAND_FILTER_DN_FK = TSRBO.OPERAND_FILTER_DN_FK 
  AND  RAO.OPERAND_FILTER_OPERATOR_CR_FK = TSRBO.OPERAND_FILTER_OPERATOR_CR_FK )
WHERE 1 = 1
AND   TSRBO.OPERAND_FILTER_OPERATOR_CR_FK = 9020  --- COALESCE QTY BRK
AND   TSRBO.OPERAND_FILTER_DN_FK  <> 107000  ---- Value Not Model Sales QTY and must be looked up

SET @status_desc = 'Ln 1800 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;




-----------------------------------------------------------------------------------------
-- OPERAND_FILTER_OPERATOR IS SPECIFIC RULE FILTER OPERANDS LOOKUP ( OPERATORS 9000, 9019 )
-- UTILIZING [synchronizer].[V_RULES_FILTER_LOOKUP_OPERANDS]
-- Carrier West does not always have Ship Tos... so altered logic below
-----------------------------------------------------------------------------------------

TRUNCATE TABLE #TS_RULES_FILTER_LOOKUP_OPERANDS

INSERT INTO  #TS_RULES_FILTER_LOOKUP_OPERANDS
           ([EVENT_RULES_FK]
           ,[RULES_ACTION_FK]
           ,[BASIS_POSITION]
           ,[RULES_FILTER_LOOKUP_OPERAND_FK]   
           ,[OPERAND_FILTER_DN_FK]
           ,[OPERAND_FILTER_OPERATOR_CR_FK]
           ,[OPERAND_FILTER_VALUE]                                  
           )
SELECT        
       B.EVENT_RULES_FK
      ,B.RULES_ACTION_FK
      ,B.BASIS_POSITION
      ,B.RULES_FILTER_LOOKUP_OPERAND_FK    
      ,B.OPERAND_FILTER_DN_FK
      ,B.OPERAND_FILTER_OPERATOR_CR_FK
      ,B.OPERAND_FILTER_VALUE
------   
FROM (         
SELECT        
       A.EVENT_RULES_FK
      ,A.RULES_ACTION_FK
      ,A.BASIS_POSITION
      ,A.RULES_FILTER_LOOKUP_OPERAND_FK    
      ,A.OPERAND_FILTER_DN_FK
      ,A.OPERAND_FILTER_OPERATOR_CR_FK
      ,A.OPERAND_FILTER_VALUE
      , ( DENSE_RANK() OVER ( PARTITION BY A.EVENT_RULES_FK
								ORDER BY  A.PRECEDENCE ASC,
										  A.TS_RULES_BASIS_OPERAND_ID ASC ) ------LOOK AT ADDING KEY?? PK id
         ) AS DRANK
------   
FROM (         
-------   EVENT SALES HISTORY CUSTOMER ( EITHER A SHIP TO OR BILL TO ) 
SELECT 
TSRBO.TS_RULES_BASIS_OPERAND_ID
      ,TSRBO.EVENT_RULES_FK
      ,TSRBO.RULES_ACTION_FK
      ,TSRBO.BASIS_POSITION
      ,TSRBO.RULES_FILTER_LOOKUP_OPERAND_FK
      ,TSRBO.OPERAND_FILTER_DN_FK
      ,TSRBO.OPERAND_FILTER_OPERATOR_CR_FK
      ,RFLOP.OPERAND_FILTER_VALUE
      ,ECT.PRECEDENCE
FROM  #TS_RULES_BASIS_OPERANDS TSRBO
INNER JOIN epacube.ENTITY_STRUCTURE ES WITH (NOLOCK)
  ON ES.ENTITY_STRUCTURE_ID = TSRBO.FILTER_ENTITY_STRUCTURE_FK
INNER JOIN epacube.ENTITY_IDENTIFICATION  EI WITH (NOLOCK)
  ON  EI.ENTITY_STRUCTURE_FK = ES.ENTITY_STRUCTURE_ID
  AND EI.DATA_NAME_FK = ( SELECT  cast ( eep.value as int ) FROM epacube.epacube_params eep  WITH (NOLOCK)  
							WHERE  eep.name = 'CUSTOMER' and eep.application_scope_fk = 100 )
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( RFS.RULES_FK = TSRBO.RULES_FK )  
INNER JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK) 
  ON ( RF.RULES_FILTER_ID = RFS.CUST_FILTER_FK )  
INNER JOIN synchronizer.RULES_FILTER_LOOKUP_OPERAND RFLO WITH (NOLOCK)
  ON ( RFLO.RULES_FILTER_LOOKUP_OPERAND_ID = TSRBO.RULES_FILTER_LOOKUP_OPERAND_FK  ) 
INNER JOIN synchronizer.RULES_FILTER_LOOKUP_OPERAND_POSITION RFLOP WITH (NOLOCK)
  ON   RFLOP.RULES_FILTER_LOOKUP_OPERAND_FK = RFLO.RULES_FILTER_LOOKUP_OPERAND_ID
  AND  RFLOP.RULES_FILTER_DN_FK = RF.DATA_NAME_FK 
  AND  RFLOP.RULES_FILTER_VALUE = RF.VALUE1 
  AND  RFLOP.OPERAND_FILTER_DN_FK = TSRBO.OPERAND_FILTER_DN_FK   
--- CHECK FOR CUSTOMER SHIP TO OR CUSTOMER BILL TO  SPECIFIC VALUE  
  AND  RFLOP.RULES_FILTER_ENTITY_DN_FK = EI.ENTITY_DATA_NAME_FK
  AND  RFLOP.RULES_FILTER_ENTITY_ID_VALUE =  EI.VALUE
INNER JOIN epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK) 
  ON ( ECT.DATA_NAME_FK = RFLOP.RULES_FILTER_ENTITY_DN_FK
  AND  ECT.RECORD_STATUS_CR_FK = 1 )												
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.DATA_NAME_ID = RFLO.RULES_FILTER_DN_FK )
INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) 
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )   
-----    
WHERE 1 = 1
AND   DS.ENTITY_CLASS_CR_FK = 10104    ---- ADD MORE LATER BUT JUST DOING SXE FOR NOW 
--------------------
----- UNION WITH BILL TO PARENT VALUES IF EXIST 
UNION ALL
SELECT 
TSRBO.TS_RULES_BASIS_OPERAND_ID   ---- ks added 6/28 for union to match from cv addition
      ,TSRBO.EVENT_RULES_FK
      ,TSRBO.RULES_ACTION_FK
      ,TSRBO.BASIS_POSITION
      ,TSRBO.RULES_FILTER_LOOKUP_OPERAND_FK
      ,TSRBO.OPERAND_FILTER_DN_FK
      ,TSRBO.OPERAND_FILTER_OPERATOR_CR_FK
      ,RFLOP.OPERAND_FILTER_VALUE
      ,ECT.PRECEDENCE
FROM  #TS_RULES_BASIS_OPERANDS TSRBO
INNER JOIN epacube.ENTITY_STRUCTURE ES WITH (NOLOCK)
  ON ES.ENTITY_STRUCTURE_ID = TSRBO.FILTER_ENTITY_STRUCTURE_FK
INNER JOIN epacube.ENTITY_IDENTIFICATION  EI WITH (NOLOCK)
  ON  EI.ENTITY_STRUCTURE_FK = ES.PARENT_ENTITY_STRUCTURE_FK                                
  AND EI.ENTITY_DATA_NAME_FK = 144010  --- BILL TO           
  AND EI.DATA_NAME_FK = ( SELECT  cast ( eep.value as int ) FROM epacube.epacube_params eep  WITH (NOLOCK)  
							WHERE  eep.name = 'CUSTOMER' and eep.application_scope_fk = 100 )
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( RFS.RULES_FK = TSRBO.RULES_FK )  
INNER JOIN synchronizer.RULES_FILTER RF WITH (NOLOCK) 
  ON ( RF.RULES_FILTER_ID = RFS.CUST_FILTER_FK )  
INNER JOIN synchronizer.RULES_FILTER_LOOKUP_OPERAND RFLO WITH (NOLOCK)
  ON ( RFLO.RULES_FILTER_LOOKUP_OPERAND_ID = TSRBO.RULES_FILTER_LOOKUP_OPERAND_FK  ) 
INNER JOIN synchronizer.RULES_FILTER_LOOKUP_OPERAND_POSITION RFLOP WITH (NOLOCK)
  ON   RFLOP.RULES_FILTER_LOOKUP_OPERAND_FK = RFLO.RULES_FILTER_LOOKUP_OPERAND_ID
  AND  RFLOP.RULES_FILTER_DN_FK = RF.DATA_NAME_FK 
  AND  RFLOP.RULES_FILTER_VALUE = RF.VALUE1 
  AND  RFLOP.OPERAND_FILTER_DN_FK = TSRBO.OPERAND_FILTER_DN_FK   
--- CHECK FOR CUSTOMER SHIP TO OR CUSTOMER BILL TO  SPECIFIC VALUE  
  AND  RFLOP.RULES_FILTER_ENTITY_DN_FK = EI.ENTITY_DATA_NAME_FK
  AND  RFLOP.RULES_FILTER_ENTITY_ID_VALUE =  EI.VALUE
INNER JOIN epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK) 
  ON ( ECT.DATA_NAME_FK = RFLOP.RULES_FILTER_ENTITY_DN_FK
  AND  ECT.RECORD_STATUS_CR_FK = 1 )												
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.DATA_NAME_ID = RFLO.RULES_FILTER_DN_FK )
INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) 
  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )   
-----    
WHERE 1 = 1
AND   DS.ENTITY_CLASS_CR_FK = 10104    ---- ADD MORE LATER BUT JUST DOING SXE FOR NOW 
) A
) B
WHERE B.DRANK = 1


SET @status_desc = 'Ln 1939 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;



----  Lookup Operand utilizing RULES_FILTER_LOOKUP_OPERANDS operand > 0 

INSERT INTO [synchronizer].[EVENT_CALC_RULES_BASIS]
           ([EVENT_RULES_FK]
           ,[BASIS_POSITION]
           ,[BASIS_OPERAND]    
           ,[RULES_FILTER_LOOKUP_OPERAND_FK]   
           ,[BASIS_OPERAND_FILTER_DN_FK]
           ,[BASIS_OPERAND_FILTER_VALUE]
           ,[BASIS_OPERAND_POSITION]     
           ,[BASIS_OPERAND_FILTER_OPERATOR_CR_FK]                             
           ,[BASIS_RESULT_TYPE_CR_FK]           
           ,[BASIS_EFFECTIVE_DATE]
           )
SELECT 
      TSRBO.EVENT_RULES_FK
     ,TSRBO.BASIS_POSITION
     ,VRBO.BASIS_OPERAND
     ,TSRBO.RULES_FILTER_LOOKUP_OPERAND_FK
     ,TSRFLO.OPERAND_FILTER_DN_FK
     ,TSRFLO.OPERAND_FILTER_VALUE
     ,VRBO.BASIS_OPERAND_POSITION
     ,TSRBO.OPERAND_FILTER_OPERATOR_CR_FK
     ,710  -- CURRENT
     ,VRBO.BASIS_EFFECTIVE_DATE
-------------------      
FROM  #TS_RULES_BASIS_OPERANDS TSRBO
INNER JOIN  #TS_RULES_FILTER_LOOKUP_OPERANDS TSRFLO
  ON ( TSRFLO.EVENT_RULES_FK = TSRBO.EVENT_RULES_FK )
INNER JOIN synchronizer.V_RULES_BASIS_OPERANDS VRBO
  ON  ( VRBO.RULES_ACTION_FK = TSRFLO.RULES_ACTION_FK
  AND   VRBO.BASIS_POSITION  = TSRFLO.BASIS_POSITION                                       ---- KS CHECK DATA  
  AND   VRBO.OPERAND_FILTER_DN_FK = TSRFLO.OPERAND_FILTER_DN_FK 
  AND   VRBO.OPERAND_FILTER_OPERATOR_CR_FK = TSRFLO.OPERAND_FILTER_OPERATOR_CR_FK
  AND   VRBO.BASIS_OPERAND_FILTER_VALUE  = TSRFLO.OPERAND_FILTER_VALUE )
WHERE 1 = 1
AND   TSRBO.RULES_FILTER_LOOKUP_OPERAND_FK IS NOT NULL --- INLCUDE ONLY FILTER SPECIFIC OPERAND LOOKUPS
AND   TSRBO.OPERAND_FILTER_OPERATOR_CR_FK <> 9020 
AND   TSRFLO.OPERAND_FILTER_VALUE > 0


SET @status_desc = 'Ln 1985 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;



----  RULES_FILTER_LOOKUP_OPERANDS operand = 0; Use Category value to lookup in RULES_ACTION_OPERANDS

INSERT INTO [synchronizer].[EVENT_CALC_RULES_BASIS]
           ([EVENT_RULES_FK]
           ,[BASIS_POSITION]
           ,[BASIS_OPERAND]    
           ,[RULES_FILTER_LOOKUP_OPERAND_FK]   
           ,[BASIS_OPERAND_FILTER_DN_FK]
           ,[BASIS_OPERAND_FILTER_VALUE]
           ,[BASIS_OPERAND_POSITION]     
           ,[BASIS_OPERAND_FILTER_OPERATOR_CR_FK]                             
           ,[BASIS_RESULT_TYPE_CR_FK]           
           ,[BASIS_EFFECTIVE_DATE]
           )
SELECT 
      TSRBO.EVENT_RULES_FK
     ,TSRBO.BASIS_POSITION
     ,VRBO.BASIS_OPERAND
     ,TSRBO.RULES_FILTER_LOOKUP_OPERAND_FK
     ,TSRFLO.OPERAND_FILTER_DN_FK
     ,TSRFLO.OPERAND_FILTER_VALUE
     ,VRBO.BASIS_OPERAND_POSITION
     ,TSRBO.OPERAND_FILTER_OPERATOR_CR_FK
     ,710  -- CURRENT
     ,VRBO.BASIS_EFFECTIVE_DATE
-------------------      
FROM  #TS_RULES_BASIS_OPERANDS TSRBO
INNER JOIN  #TS_RULES_FILTER_LOOKUP_OPERANDS TSRFLO
  ON ( TSRFLO.EVENT_RULES_FK = TSRBO.EVENT_RULES_FK )
INNER JOIN synchronizer.V_RULES_BASIS_OPERANDS VRBO
  ON  ( VRBO.RULES_ACTION_FK = TSRBO.RULES_ACTION_FK
  AND   VRBO.BASIS_POSITION  = TSRBO.BASIS_POSITION 
  AND   VRBO.OPERAND_FILTER_DN_FK = TSRBO.OPERAND_FILTER_DN_FK 
  AND   VRBO.OPERAND_FILTER_OPERATOR_CR_FK = TSRBO.OPERAND_FILTER_OPERATOR_CR_FK
  AND   VRBO.BASIS_OPERAND_FILTER_VALUE  = TSRBO.OPERAND_FILTER_VALUE )
WHERE 1 = 1
AND   TSRBO.RULES_FILTER_LOOKUP_OPERAND_FK IS NOT NULL --- INLCUDE ONLY FILTER SPECIFIC OPERAND LOOKUPS
AND   TSRBO.OPERAND_FILTER_OPERATOR_CR_FK <> 9020 
AND   TSRFLO.OPERAND_FILTER_VALUE = 0

SET @status_desc = 'Ln 2030 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;




TRUNCATE TABLE #TS_RULES_BASIS_OPERANDS   --- TRUNCATE VALUES TO KEEP TEMPDP SMALL
TRUNCATE TABLE #TS_RULES_FILTER_LOOKUP_OPERANDS  --- SAME

-----------------------------------------------------------------------------------------
-- ASSIGN BASIS_CALC, BASIS1, BASIS2, AND BASIS3 TO EVENT_CALC_RULE 
--		INCLUDE BASIS_NUMBERS1,3
-----------------------------------------------------------------------------------------



UPDATE ECR
SET BASIS_CALC = ( SELECT   BASIS_VALUE 
                   FROM synchronizer.EVENT_CALC_RULES_BASIS ECRB WITH (NOLOCK)
                   WHERE ECRB.EVENT_RULES_FK = ecr.EVENT_RULES_ID 
                   AND   ECRB.BASIS_POSITION = 0
                   AND   ECRB.BASIS_RANK = 1 )
   ,BASIS1     = ( SELECT  BASIS_VALUE 
				   FROM synchronizer.EVENT_CALC_RULES_BASIS ECRB WITH (NOLOCK)
				   WHERE ECRB.EVENT_RULES_FK = ecr.EVENT_RULES_ID 
				   AND   ECRB.BASIS_POSITION = 1
				   AND   ECRB.BASIS_RANK = 1 )
   ,BASIS2     = ( SELECT  BASIS_VALUE 
				   FROM synchronizer.EVENT_CALC_RULES_BASIS ECRB WITH (NOLOCK)
				   WHERE ECRB.EVENT_RULES_FK = ecr.EVENT_RULES_ID 
				   AND   ECRB.BASIS_POSITION = 2
				   AND   ECRB.BASIS_RANK = 1 )
   ,BASIS3     = ( SELECT  BASIS_VALUE  
				   FROM synchronizer.EVENT_CALC_RULES_BASIS ECRB WITH (NOLOCK)
				   WHERE ECRB.EVENT_RULES_FK = ecr.EVENT_RULES_ID 
				   AND   ECRB.BASIS_POSITION = 3
				   AND   ECRB.BASIS_RANK = 1 )
   ,NUMBER1     =   CASE  BASIS_SOURCE1
					WHEN 'NUMERIC'  THEN ( SELECT BASIS1_NUMBER   
					                       FROM synchronizer.RULES_ACTION RA WITH (NOLOCK)
					                       WHERE RA.RULES_ACTION_ID = ECR.RULES_ACTION_FK )
					WHEN 'OPERANDS' THEN ( SELECT TOP 1 BASIS_OPERAND   --- REMOVE ONCE DATA IS GOOD  
										   FROM synchronizer.EVENT_CALC_RULES_BASIS ECRB WITH (NOLOCK)
										   WHERE ECRB.EVENT_RULES_FK = ecr.EVENT_RULES_ID 
										   AND   ECRB.BASIS_POSITION = 1 
										   AND   ECRB.BASIS_OPERAND IS NOT NULL )
					WHEN 'LOOKUP'   THEN NULL
					ELSE  ( SELECT BASIS1_NUMBER
					                       FROM synchronizer.RULES_ACTION RA WITH (NOLOCK)
					                       WHERE RA.RULES_ACTION_ID = ECR.RULES_ACTION_FK )
					END 
   ,NUMBER2     =   CASE  BASIS_SOURCE2
					WHEN 'NUMERIC'  THEN ( SELECT BASIS2_NUMBER
					                       FROM synchronizer.RULES_ACTION RA WITH (NOLOCK)
					                       WHERE RA.RULES_ACTION_ID = ECR.RULES_ACTION_FK )
					WHEN 'OPERANDS' THEN ( SELECT TOP 1 BASIS_OPERAND   --- REMOVE ONCE DATA IS GOOD 
										   FROM synchronizer.EVENT_CALC_RULES_BASIS ECRB WITH (NOLOCK)
										   WHERE ECRB.EVENT_RULES_FK = ecr.EVENT_RULES_ID 
										   AND   ECRB.BASIS_POSITION = 2
										   AND   ECRB.BASIS_OPERAND IS NOT NULL )
					WHEN 'LOOKUP'   THEN NULL
					ELSE  ( SELECT BASIS2_NUMBER
					                       FROM synchronizer.RULES_ACTION RA WITH (NOLOCK)
					                       WHERE RA.RULES_ACTION_ID = ECR.RULES_ACTION_FK )
					END 
   ,NUMBER3     =   CASE  BASIS_SOURCE3
					WHEN 'NUMERIC'  THEN ( SELECT  BASIS3_NUMBER
					                       FROM synchronizer.RULES_ACTION RA WITH (NOLOCK)
					                       WHERE RA.RULES_ACTION_ID = ECR.RULES_ACTION_FK )
					WHEN 'OPERANDS' THEN ( SELECT TOP 1 BASIS_OPERAND   --- REMOVE ONCE DATA IS GOOD 
										   FROM synchronizer.EVENT_CALC_RULES_BASIS ECRB WITH (NOLOCK)
										   WHERE ECRB.EVENT_RULES_FK = ecr.EVENT_RULES_ID 
										   AND   ECRB.BASIS_POSITION = 3
										   AND   ECRB.BASIS_OPERAND IS NOT NULL )
					WHEN 'LOOKUP'   THEN NULL
					ELSE  ( SELECT BASIS3_NUMBER
					                       FROM synchronizer.RULES_ACTION RA WITH (NOLOCK)
					                       WHERE RA.RULES_ACTION_ID = ECR.RULES_ACTION_FK )
					END 
-----------------------------------------------					                            
from synchronizer.EVENT_CALC_RULES ecr
inner join synchronizer.event_calc ec with (nolock) on ecr.event_fk = ec.event_id
WHERE ISNULL ( ecr.DISQUALIFIED_IND, 0 ) <> 1 
AND   EC.JOB_FK = @in_analysis_job_fk 
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq

SET @status_desc = 'Ln 2124 Find_Basis_Values'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

-----------------------------------------------------------------------------------------
-- Drop all Temporary Tables created in this procedure...
-----------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_ORG_XREF') is not null
	   drop table #TS_ORG_XREF;

--drop temp tables
IF object_id('tempdb..#TS_RULES_BASIS') is not null
   drop table #TS_RULES_BASIS

--drop temp tables
IF object_id('tempdb..#TS_EVENT_BASIS') is not null
   drop table #TS_EVENT_BASIS

--drop temp table if it exists
	IF object_id('tempdb..#TS_PRICESHEET') is not null
	   drop table #TS_PRICESHEET;

--drop temp tables
IF object_id('tempdb..#TS_RULES_BASIS_OPERANDS') is not null
   drop table #TS_RULES_BASIS_OPERANDS

--drop temp tables
IF object_id('tempdb..#TS_RULES_FILTER_LOOKUP_OPERANDS') is not null
   drop table #TS_RULES_FILTER_LOOKUP_OPERANDS

IF object_id('tempdb..#rdnfk') is not null
   drop table #rdnfk
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of marginmgr.FIND_BASIS_VALUES'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of marginmgr.FIND_BASIS_VALUES has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
