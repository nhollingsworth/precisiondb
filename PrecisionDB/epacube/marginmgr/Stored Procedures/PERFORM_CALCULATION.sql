﻿--A_epaMAUI_PERFORM_CALCULATION


-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To determine all qualified rules for Event_Data records in Job
--				Validation and Calculations Rules assigned to Events
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        11/30/2009   Initial SQL Version
-- KS        03/14/2010   Revised Gross Margin Calculation to be lesser of Basis1 or Basis2 Result
-- KS        05/14/2010   Revised Definitions from Roadmap
-- KS        08/27/2011   Added Named Operation 50% Rebate Sharing  EPA-3232
-- KS        08/29/2011   Added Carrier Specific Named Operation 'Carrier ZCurve'  EPA-3237
-- GHS       10/04/2011   Adjusted logic in Rebate Sharing to set result to 0 when charge-back parameters are exceeded ~ Line 224.
-- KS        01/25/2012   JASCO Specific Operation 'Jasco Desired DI-1 Margin'
-- KS        02/10/2012   To support JASCO expanded capabilities of the following Operations to support the use of Data Names
--                            Multiply, Divide, Discount, Percent, Gross Margin, Add, Subtract
--                            In addition backed out Gary's change to GM operation that made it SXE specific 
--                            And created a new Named Operation 'SX Price Gross Margin' to address the specific SXE hardcoded logic
--                        Also changed procedure [import].[Import_To_Production_PDSC] to reflect 'SX Price Gross Margin'
-- KS        05/15/2012   In order to support JASCO NULL Operators and Calculation Engine NULL Operators a Ind was added to 
--                            the RULES_HIERARCHY table to specify Allowed or Not;  If Not then Result will be NULL
-- KS        07/24/2012   NCE requested that we allow the calculation of a negative rebated_cost_cb_amt
--                        Gary's email request below
--							If we need to do this I am OK with doing it where the rebated_cost_cb_amt (111401) is what we’re calculating, but not where rebate_cb_amt and rebate_buy_amt are what we calculate (111501 and 111502). 
--							In the SX world there are many cases where a rebate_cb/buy_amt properly calculate to < 0, but would then never be chosen under any circumstances.
--							I suspect a rebated_cost_cb_amt < 0 is valid as this means the distributor is getting some great incentive for selling the product (probably very rare).
--							But a negative rebate_cb_amt would never be valid, as this would mean the distributor would pay something back to the vendor for every unit sold, and I don’t believe this would ever happen. Not on a charge-back. Perhaps on another mechanism, but not on a chargeback.
-- GS       09/20/2012    Adjusted the logic for the Rebate Sharing named operation starting on line 429 to accommodate Operation1 = Multiplier and to correct calculation when rebate amount should go to 0 (amount to be reduced was incorrectly set to 0, leaving full rebate_cb_amount)
-- GS		09/23/2013	  Changed "IN" clauses to inner joins to improve performance
-- GS		10/10/2013	  Inserted process to delete records causing Divide By Zero failure when calculating Gross Profit Records

CREATE PROCEDURE [marginmgr].[PERFORM_CALCULATION] 
   ( @in_analysis_job_fk BIGINT,  @in_result_data_name_fk  INT, @in_calc_group_seq INT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT
DECLARE  @v_operand_divisor   SMALLINT 

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of marginmgr.PERFORM_CALCULATION.' 
               + ' JOB:: ' + CAST ( ISNULL (@in_analysis_job_fk, 0 ) AS VARCHAR(16) )
               + ' RESULT:: ' + CAST ( ISNULL (@IN_RESULT_DATA_NAME_FK, 0 ) AS VARCHAR(16) )
               + ' CALC GROUP:: ' + CAST ( ISNULL (@in_calc_group_seq, 0 ) AS VARCHAR(16) )
               
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;


SET @v_operand_divisor = ( SELECT ISNULL ( CAST ( EEP.VALUE AS SMALLINT ), 1 )
                          FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK)
                          WHERE EEP.APPLICATION_SCOPE_FK = 100 AND EEP.NAME = 'OPERAND DIVISOR' )

------------------------------------------------------------------------------------------
---  Delete records causing Divide By Zero failure when calculating Gross Profit Records
------------------------------------------------------------------------------------------


delete ecrb
from synchronizer.event_calc_rules_basis ecrb
inner join synchronizer.EVENT_CALC_RULES ECR on ecrb.event_rules_fk = ecr.event_rules_id
inner join synchronizer.event_calc EC with (Nolock) on ecr.event_fk = ec.EVENT_ID
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( ECR.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID )
INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
	ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK )
WHERE 1 = 1
AND   ISNULL ( RH.ALW_NULL_OPS_IND, 0 ) = 0
AND   ECR.RULES_ACTION_OPERATION1_FK IS NOT NULL 		
AND   EC.JOB_FK = @in_analysis_job_fk
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
and OPERATION1 + CAST(Cast(Number1 as int)as varchar(32)) = 'GM100'

Delete ECR
from synchronizer.EVENT_CALC_RULES ECR
inner join synchronizer.event_calc EC with (Nolock) on ecr.event_fk = ec.EVENT_ID
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( ECR.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID )
INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
	ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK )
WHERE 1 = 1
AND   ISNULL ( RH.ALW_NULL_OPS_IND, 0 ) = 0
AND   ECR.RULES_ACTION_OPERATION1_FK IS NOT NULL 		
AND   EC.JOB_FK = @in_analysis_job_fk
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
and OPERATION1 + CAST(Cast(Number1 as int)as varchar(32)) = 'GM100'

------------------------------------------------------------------------------------------
---  CALCULATE RESULT1  --- DO NOT Allow NULL Operators 
------------------------------------------------------------------------------------------



UPDATE ECR
        SET RESULT1 = 
                     CASE OPERATION1
                     WHEN 'NO ACTION'       THEN NULL
                     WHEN '='				THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN NUMBER1
                                                 WHEN 'DATA NAME'	THEN BASIS_CALC
                                                 WHEN 'OPERANDS'	THEN NUMBER1
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END            
                     WHEN '+'				THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN BASIS_CALC + NUMBER1
                                                 WHEN 'DATA NAME'	THEN BASIS_CALC + BASIS1
                                                 WHEN 'OPERANDS'	THEN BASIS_CALC + NUMBER1
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END   
                     WHEN '-'				THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN BASIS_CALC - NUMBER1
                                                 WHEN 'DATA NAME'	THEN BASIS_CALC - BASIS1
                                                 WHEN 'OPERANDS'	THEN BASIS_CALC - NUMBER1
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END   
                     WHEN '*'				THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN BASIS_CALC * ( NUMBER1 / @v_operand_divisor )
                                                 WHEN 'DATA NAME'	THEN BASIS_CALC * ( BASIS1 / @v_operand_divisor )
                                                 WHEN 'OPERANDS'	THEN BASIS_CALC * ( NUMBER1 / @v_operand_divisor )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END  
                     WHEN '/'				THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN BASIS_CALC / ( NUMBER1 / @v_operand_divisor )
                                                 WHEN 'DATA NAME'	THEN BASIS_CALC / ( BASIS1 / @v_operand_divisor )
                                                 WHEN 'OPERANDS'	THEN BASIS_CALC / ( NUMBER1 / @v_operand_divisor )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'DISC'			THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN BASIS_CALC * ( 1 - ( NUMBER1 / @v_operand_divisor )  )
                                                 WHEN 'DATA NAME'	THEN BASIS_CALC * ( 1 - ( BASIS1 / @v_operand_divisor )  )
                                                 WHEN 'OPERANDS'	THEN BASIS_CALC * ( 1 - ( NUMBER1 / @v_operand_divisor )  )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'GM'			    THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN ( BASIS_CALC / ( 1 - ( NUMBER1 / @v_operand_divisor  ) ) )																		  
                                                 WHEN 'DATA NAME'	THEN ( BASIS_CALC / ( 1 - ( BASIS1 / @v_operand_divisor  ) ) )		
                                                 WHEN 'OPERANDS'	THEN ( BASIS_CALC / ( 1 - ( NUMBER1 / @v_operand_divisor  ) ) )		
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                    ---- KS 02/10/2012... why checking BASIS1 GT BASIS_CALC ????  not happy with this "HARDCODED" SXE SPECIFIC 
                    ----                  solution so removing SX-GM to a NAMED OPERATION specific to dealing with it
                    ----                  Verify with Gary and his SXE assumptions.. have to change rules import with this change
                    ----   see old code commented below  AND new 'SX Price Gross Margin' code Below
                    /*
					WHEN 'GM'				-----   THEN BASIS_CALC / ( 1 - ( NUMBER1 / @v_operand_divisor )  )
                                            THEN ( CASE WHEN ( ISNULL ( BASIS_CALC, 0 ) < ISNULL ( BASIS1, 0 ) ) 
														THEN ( ISNULL ( BASIS_CALC, BASIS1 ) / ( 1 - ( NUMBER1 / @v_operand_divisor  ) ) )
													    ELSE ( ISNULL ( BASIS1, BASIS_CALC ) / ( 1 - ( NUMBER1 / @v_operand_divisor  ) ) ) 
												   END )				    
					*/ 
                     WHEN 'PCT'			    THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN ( BASIS_CALC * ( 1 + ( NUMBER1 / @v_operand_divisor )  ) )
                                                 WHEN 'DATA NAME'	THEN ( BASIS_CALC * ( 1 + ( BASIS1 / @v_operand_divisor )  ) )
                                                 WHEN 'OPERANDS'	THEN ( BASIS_CALC * ( 1 + ( NUMBER1 / @v_operand_divisor )  ) )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'NAMED OPERATION'	THEN CASE ( SELECT RAOP.NAME FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
                                                        WHERE RAOP.RULES_ACTION_OPERATION_ID = RULES_ACTION_OPERATION1_FK )
                                                 WHEN 'REBATE DOWN TO FIXED'	  THEN ( BASIS_CALC  -  ( NUMBER1 ) ) 
                                                 WHEN 'REBATE DOWN TO CALCULATED' THEN ( BASIS_CALC - ( BASIS1 * ( NUMBER1 / @v_operand_divisor  ) ) ) 
                                                 WHEN 'GROSS MARGIN GUARANTEE'    THEN ( ( BASIS_CALC / ( 1 - ( NUMBER1 / @v_operand_divisor  ) ) ) 
                                                                                            -  BASIS_CALC  - ( BASIS1 - BASIS_CALC ) )    
                                                 WHEN 'Carrier ZCurve'	  --- See Comments Below on the Variables ( EPA-3237 )                                                     
                                                     THEN ( SELECT Case
															When Basis1 = 0 then 0
															When Basis_Calc / Basis1 < Operand1 Then (Operand2 - Operand3) * Basis1 
															When Basis_Calc / Basis1 > Operand4 then 0
															When Basis_Calc / Basis1 >= Operand1 and Basis_Calc / Basis1 <= Operand6 then
																  Basis1 * (Operand2-(((Basis_Calc / Basis1 - Operand1) * Operand5) + Operand3))
															When Basis_Calc / Basis1 > Operand6 and Basis_Calc / Basis1 <= Operand4 then
																  Basis1 * (Operand2 - (((Basis_Calc / Basis1 - Operand6)* Operand7) + (((Operand6 - Operand1) * Operand5) + Operand3)))
															End 
                                                            FROM synchronizer.RULES_ACTION_OPERANDS RAO 
                                                            WHERE RAO.RULES_ACTION_FK = ecr.RULES_ACTION_FK
                                                            AND   RAO.BASIS_POSITION = 1 ) 
                                                 WHEN 'SX Price Gross Margin(N)'	
													 THEN ( CASE WHEN ( ISNULL ( BASIS_CALC, 0 ) < ISNULL ( BASIS1, 0 ) ) 
																 THEN ( ISNULL ( BASIS_CALC, BASIS1 ) / ( 1 - ( NUMBER1 / @v_operand_divisor  ) ) )
																 ELSE ( ISNULL ( BASIS1, BASIS_CALC ) / ( 1 - ( NUMBER1 / @v_operand_divisor  ) ) ) 
															     END )                                                                    
                                                 WHEN 'SX Price Gross Margin(O)'	
													 THEN ( CASE WHEN ( ISNULL ( BASIS_CALC, 0 ) < ISNULL ( BASIS1, 0 ) ) 
																 THEN ( ISNULL ( BASIS_CALC, BASIS1 ) / ( 1 - ( NUMBER1 / @v_operand_divisor  ) ) )
																 ELSE ( ISNULL ( BASIS1, BASIS_CALC ) / ( 1 - ( NUMBER1 / @v_operand_divisor  ) ) ) 
															     END )                                                                    
                                                 END
                     ELSE NULL  -- No Matching Operation
                     END        
from synchronizer.EVENT_CALC_RULES ECR
inner join synchronizer.event_calc EC with (Nolock) on ecr.event_fk = ec.EVENT_ID
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( ECR.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID )
INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
	ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK )
WHERE 1 = 1
AND   ISNULL ( RH.ALW_NULL_OPS_IND, 0 ) = 0
AND   ECR.RULES_ACTION_OPERATION1_FK IS NOT NULL 		
AND   EC.JOB_FK = @in_analysis_job_fk
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq	


--WHERE EVENT_RULES_ID IN 
--(	SELECT ECR.EVENT_RULES_ID
--	FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
--	INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
--	 ON ( ECR.EVENT_FK = EC.EVENT_ID )
--	INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
--	  ON ( ECR.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID )
--	INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
--	  ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK )
--	WHERE 1 = 1
--	AND   ISNULL ( RH.ALW_NULL_OPS_IND, 0 ) = 0
--	AND   ECR.RULES_ACTION_OPERATION1_FK IS NOT NULL 		
--	AND   EC.JOB_FK = @in_analysis_job_fk
--	AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
--    AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq	
--)



------------------------------------------------------------------------------------------
---  CALCULATE RESULT1  --- Allow NULL Operators 
------------------------------------------------------------------------------------------



UPDATE ECR
        SET RESULT1 = 
                     CASE OPERATION1
                     WHEN 'NO ACTION'       THEN NULL
                     WHEN '='				THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN NUMBER1
                                                 WHEN 'DATA NAME'	THEN BASIS_CALC
                                                 WHEN 'OPERANDS'	THEN NUMBER1
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END            
                     WHEN '+'				THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN BASIS_CALC + ISNULL ( NUMBER1, 0 )
                                                 WHEN 'DATA NAME'	THEN BASIS_CALC + ISNULL ( BASIS1, 0 )
                                                 WHEN 'OPERANDS'	THEN BASIS_CALC + ISNULL ( NUMBER1, 0 )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END   
                     WHEN '-'				THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN BASIS_CALC - ISNULL ( NUMBER1, 0 )
                                                 WHEN 'DATA NAME'	THEN BASIS_CALC - ISNULL ( BASIS1, 0 )
                                                 WHEN 'OPERANDS'	THEN BASIS_CALC - ISNULL ( NUMBER1, 0 )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END   
                     WHEN '*'				THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN BASIS_CALC * 
                                                                                ( CASE ( ISNULL ( NUMBER1, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( NUMBER1 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'DATA NAME'	THEN BASIS_CALC * 
                                                                                ( CASE ( ISNULL ( BASIS1, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( BASIS1 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'OPERANDS'	THEN BASIS_CALC * 
                                                                                ( CASE ( ISNULL ( NUMBER1, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( NUMBER1 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END  
                     WHEN '/'				THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN BASIS_CALC / 
                                                                                ( CASE ( ISNULL ( NUMBER1, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( NUMBER1 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'DATA NAME'	THEN BASIS_CALC / 
                                                                                ( CASE ( ISNULL ( BASIS1, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( BASIS1 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'OPERANDS'	THEN BASIS_CALC / 
                                                                                ( CASE ( ISNULL ( NUMBER1, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( NUMBER1 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'DISC'			THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN BASIS_CALC * ( 1 - ( ISNULL ( NUMBER1, 0 ) / @v_operand_divisor )  )
                                                 WHEN 'DATA NAME'	THEN BASIS_CALC * ( 1 - ( ISNULL ( BASIS1, 0 ) / @v_operand_divisor )  )
                                                 WHEN 'OPERANDS'	THEN BASIS_CALC * ( 1 - ( ISNULL ( NUMBER1, 0 ) / @v_operand_divisor )  )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'GM'			    THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN ( BASIS_CALC / ( 1 - ( ISNULL ( NUMBER1, 0 ) / @v_operand_divisor  ) ) )																		  
                                                 WHEN 'DATA NAME'	THEN ( BASIS_CALC / ( 1 - ( ISNULL ( BASIS1, 0 ) / @v_operand_divisor  ) ) )		
                                                 WHEN 'OPERANDS'	THEN ( BASIS_CALC / ( 1 - ( ISNULL ( NUMBER1, 0 ) / @v_operand_divisor  ) ) )		
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'PCT'			    THEN CASE BASIS_SOURCE1
                                                 WHEN 'NUMERIC'		THEN ( BASIS_CALC * ( 1 + ( ISNULL ( NUMBER1, 0 ) / @v_operand_divisor )  ) )
                                                 WHEN 'DATA NAME'	THEN ( BASIS_CALC * ( 1 + ( ISNULL ( BASIS1, 0 ) / @v_operand_divisor )  ) )
                                                 WHEN 'OPERANDS'	THEN ( BASIS_CALC * ( 1 + ( ISNULL ( NUMBER1, 0 ) / @v_operand_divisor )  ) )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'NAMED OPERATION'	THEN CASE ( SELECT RAOP.NAME FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
                                                        WHERE RAOP.RULES_ACTION_OPERATION_ID = RULES_ACTION_OPERATION1_FK )
                                                 WHEN 'Jasco Desired DI-1 Margin'   --- See Comments Below... what about NULLS ???
                                                     THEN ( SELECT Case
                                                            When ( ISNULL ( BASIS_CALC, 0 ) - ISNULL ( BASIS1, 0 ) ) < ISNULL ( BASIS2, 0 )  
                                                            Then ISNULL ( BASIS2, 0 )   
                                                            Else ( ISNULL ( BASIS_CALC, 0 ) - ISNULL ( BASIS1, 0 ) )    
                                                            END )    
                                                 END
                     ELSE NULL  -- No Matching Operation
                     END        
from synchronizer.EVENT_CALC_RULES ECR
inner join synchronizer.event_calc EC with (Nolock) on ecr.event_fk = ec.EVENT_ID
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( ECR.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID )
INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
	ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK )
WHERE 1 = 1
	AND   ISNULL ( RH.ALW_NULL_OPS_IND, 0 ) = 1
	AND   ECR.RULES_ACTION_OPERATION1_FK IS NOT NULL 		
	AND   EC.JOB_FK = @in_analysis_job_fk
	AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
	AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq


/*
Jasco Desired DI-1 Margin

6 DESIRED DI-1 MARGIN 412303-result
IF (DESIRED DM-1 MARGIN 420601-result - DESIRED DI-1 MARGIN AVG 420602-value IS < DESIRED DI-1 MARGIN NUMERIC FLOOR 420603-value, DESIRED DI-1 MARGIN NUMERIC FLOOR 420603-value, DESIRED DM-1 MARGIN 420601-result- DESIRED DI-1 MARGIN AVG 420602-value)
BASIS_CALC = DESIRED DM-1 MARGIN 420601-result
BASIS1 = DESIRED DI-1 MARGIN AVG 420602-value
BASIS2 = MARGIN NUMERIC FLOOR 420603-value

*/


/*

Carrier ZCurve  EPA-3237

Rules_Action_Operation1_DN = Carrier ZCurve
Basis_Calc_DN = Sell_Price_Cust_AMT
Basis1_DN = SX Base Price {aka Master Trade Price}
Operand1 = BEGSELLMULT2 
Operand2 = AREAMULT 
Operand3 = Cast((BEGSELLMULT2 * ((100 -BEGDGM%SEG2) / 100)) as Numeric(18, 4)) {aka BegBuyMult}
Operand4 = ENDSELLMULT3
Operand5 = Splitseg2
Operand6 = ENDSELLMULT2
Operand7 = SplitSeg3

Case
When Basis1_DN = 0 then 0
When Basis_Calc_DN / Basis1_DN < Operand1 Then (Operand2 - Operand3) * Basis1_DN 
When Basis_Calc_DN / Basis1_DN > Operand4 then 0
When Basis_Calc_DN / Basis1_DN >= Operand1 and Basis_Calc_DN / Basis1_DN <= Operand6 then
      Basis1_DN * (Operand2-(((Basis_Calc_DN/Basis1_DN-Operand1)*Operand5) + Operand3))
When Basis_Calc_DN / Basis1_DN > Operand6 and Basis_Calc_DN / Basis1_DN <= Operand4 then
      Basis1_DN * (Operand2 - (((Basis_Calc_DN/Basis1_DN-Operand6)*Operand7) + (((Operand6-Operand1)*Operand5)+Operand3)))
End Rebate_CB_Amt


*/


------------------------------------------------------------------------------------------
---  CALCULATE RESULT2 DO NOT Allow NULL Operators 
------------------------------------------------------------------------------------------

										

UPDATE ECR
        SET RESULT2 = 
                     CASE OPERATION2
                     WHEN '+'				THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN RESULT1 + NUMBER2
                                                 WHEN 'DATA NAME'	THEN RESULT1 + BASIS2
                                                 WHEN 'OPERANDS'	THEN RESULT1 + NUMBER2
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END   
                     WHEN '-'				THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN RESULT1 - NUMBER2
                                                 WHEN 'DATA NAME'	THEN RESULT1 - BASIS2
                                                 WHEN 'OPERANDS'	THEN RESULT1 - NUMBER2
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END   
                     WHEN '*'				THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN RESULT1 * ( NUMBER2 / @v_operand_divisor )
                                                 WHEN 'DATA NAME'	THEN RESULT1 * ( BASIS2 / @v_operand_divisor )
                                                 WHEN 'OPERANDS'	THEN RESULT1 * ( NUMBER2 / @v_operand_divisor )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END  
                     WHEN '/'				THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN RESULT1 / ( NUMBER2 / @v_operand_divisor )
                                                 WHEN 'DATA NAME'	THEN RESULT1 / ( BASIS2 / @v_operand_divisor )
                                                 WHEN 'OPERANDS'	THEN RESULT1 / ( NUMBER2 / @v_operand_divisor )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'DISC'			THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN RESULT1 * ( 1 - ( NUMBER2 / @v_operand_divisor )  )
                                                 WHEN 'DATA NAME'	THEN RESULT1 * ( 1 - ( BASIS2 / @v_operand_divisor )  )
                                                 WHEN 'OPERANDS'	THEN RESULT1 * ( 1 - ( NUMBER2 / @v_operand_divisor )  )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'GM'			    THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN ( RESULT1 / ( 1 - ( NUMBER2 / @v_operand_divisor  ) ) )																		  
                                                 WHEN 'DATA NAME'	THEN ( RESULT1 / ( 1 - ( BASIS2 / @v_operand_divisor  ) ) )		
                                                 WHEN 'OPERANDS'	THEN ( RESULT1 / ( 1 - ( NUMBER2 / @v_operand_divisor  ) ) )		
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                    ---- KS 02/10/2012... See comment above on the change to 'GM'					
                     WHEN 'PCT'			    THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN ( RESULT1 * ( 1 + ( NUMBER2 / @v_operand_divisor )  ) )
                                                 WHEN 'DATA NAME'	THEN ( RESULT1 * ( 1 + ( BASIS2 / @v_operand_divisor )  ) )
                                                 WHEN 'OPERANDS'	THEN ( RESULT1 * ( 1 + ( NUMBER2 / @v_operand_divisor )  ) )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'NAMED OPERATION'	THEN CASE ( SELECT RAOP.NAME FROM synchronizer.RULES_ACTION_OPERATION RAOP WITH (NOLOCK)
														WHERE RAOP.RULES_ACTION_OPERATION_ID = ecr.RULES_ACTION_OPERATION2_FK )
												 WHEN '50 Percent Rebate Sharing'	  
												 THEN RESULT1 -    --- PRIMARY REBATE OPERATION RESULT
														CASE WHEN OPERATION1 = '*' and BASIS2 is not null THEN 
															CASE	
																WHEN RESULT1 - (((BASIS3 - ( NUMBER2 / 100) * BASIS2)) * 0.5) < 0 THEN RESULT1
																WHEN RESULT1 - (((BASIS3 - ( NUMBER2 / 100) * BASIS2)) * 0.5) >= RESULT1 THEN 0
																ELSE ((BASIS3 - ( NUMBER2 / 100) * BASIS2)) * 0.5
															END
														WHEN OPERATION1 in ('=', '*') and BASIS2 IS NULL THEN
															CASE	
																WHEN NUMBER2 > BASIS3 THEN 0
																WHEN ( BASIS3 - NUMBER2 )  * 0.5 > RESULT1 THEN RESULT1
																ELSE ( BASIS3 - NUMBER2 )  * 0.5
															END
														END
												END 
					END

-------											       
from synchronizer.EVENT_CALC_RULES ECR
inner join synchronizer.event_calc EC with (Nolock) on ecr.event_fk = ec.EVENT_ID
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( ECR.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID )
INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
	ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK )
WHERE 1 = 1
	AND   ISNULL ( RH.ALW_NULL_OPS_IND, 0 ) = 0
	AND   ECR.RULES_ACTION_OPERATION2_FK IS NOT NULL 		
	AND   EC.JOB_FK = @in_analysis_job_fk
	AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
	AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq





------------------------------------------------------------------------------------------
---  CALCULATE RESULT2 Allow  NULL Operators
------------------------------------------------------------------------------------------

										

UPDATE ECR
        SET RESULT2 = 
                     CASE OPERATION2
                     WHEN '+'				THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN RESULT1 + ISNULL ( NUMBER2, 0 )
                                                 WHEN 'DATA NAME'	THEN RESULT1 + ISNULL ( BASIS2, 0 )
                                                 WHEN 'OPERANDS'	THEN RESULT1 + ISNULL ( NUMBER2, 0 )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END   
                     WHEN '-'				THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN RESULT1 - ISNULL ( NUMBER2, 0 )
                                                 WHEN 'DATA NAME'	THEN RESULT1 - ISNULL ( BASIS2, 0 )
                                                 WHEN 'OPERANDS'	THEN RESULT1 - ISNULL ( NUMBER2, 0 )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END   
                     WHEN '*'				THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN RESULT1 * 
                                                                                ( CASE ( ISNULL ( NUMBER2, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( NUMBER2 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'DATA NAME'	THEN RESULT1 * 
                                                                                ( CASE ( ISNULL ( BASIS2, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( BASIS2 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'OPERANDS'	THEN RESULT1 * 
                                                                                ( CASE ( ISNULL ( NUMBER2, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( NUMBER2 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END  
                     WHEN '/'				THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN RESULT1 / 
                                                                                ( CASE ( ISNULL ( NUMBER2, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( NUMBER2 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'DATA NAME'	THEN RESULT1 / 
                                                                                ( CASE ( ISNULL ( BASIS2, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( BASIS2 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'OPERANDS'	THEN RESULT1 / 
                                                                                ( CASE ( ISNULL ( NUMBER2, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( NUMBER2 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'DISC'			THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN RESULT1 * ( 1 - ( ISNULL ( NUMBER2, 0 ) / @v_operand_divisor )  )
                                                 WHEN 'DATA NAME'	THEN RESULT1 * ( 1 - ( ISNULL ( BASIS2, 0 ) / @v_operand_divisor )  )
                                                 WHEN 'OPERANDS'	THEN RESULT1 * ( 1 - ( ISNULL ( NUMBER2, 0 ) / @v_operand_divisor )  )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'GM'			    THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN ( RESULT1 / ( 1 - ( ISNULL ( NUMBER2, 0 ) / @v_operand_divisor  ) ) )																		  
                                                 WHEN 'DATA NAME'	THEN ( RESULT1 / ( 1 - ( ISNULL ( BASIS2, 0 ) / @v_operand_divisor  ) ) )		
                                                 WHEN 'OPERANDS'	THEN ( RESULT1 / ( 1 - ( ISNULL ( NUMBER2, 0 ) / @v_operand_divisor  ) ) )		
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                    ---- KS 02/10/2012... See comment above on the change to 'GM'					
                     WHEN 'PCT'			    THEN CASE BASIS_SOURCE2
                                                 WHEN 'NUMERIC'		THEN ( RESULT1 * ( 1 + ( ISNULL ( NUMBER2, 0 ) / @v_operand_divisor )  ) )
                                                 WHEN 'DATA NAME'	THEN ( RESULT1 * ( 1 + ( ISNULL ( BASIS2, 0 ) / @v_operand_divisor )  ) )
                                                 WHEN 'OPERANDS'	THEN ( RESULT1 * ( 1 + ( ISNULL ( NUMBER2, 0 ) / @v_operand_divisor )  ) )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'NAMED OPERATION'	THEN NULL --- CURRENTLY HAVE NONE DEFINED
					 ELSE NULL
					 END  
-------											       
from synchronizer.EVENT_CALC_RULES ECR
inner join synchronizer.event_calc EC with (Nolock) on ecr.event_fk = ec.EVENT_ID
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( ECR.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID )
INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
	ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK )
WHERE 1 = 1
	AND   ISNULL ( RH.ALW_NULL_OPS_IND, 0 ) = 1
	AND   ECR.RULES_ACTION_OPERATION2_FK IS NOT NULL 		
	AND   EC.JOB_FK = @in_analysis_job_fk
	AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
	AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq





/*

50 Percent Rebate Sharing 

Rules_Action_Operation1_FK is the primary operation
Basis_Calc_DN_FK, BASIS 1_DN_FK along with Basis1_Number utlized according to rule operation.

Rules_Action_Operation2_FK will reference the new operation: "50 Percent Rebate Sharing"
Basis2_DN_FK will be Null for a fixed value Cap Sell; otherwise it will be 231104 to specify List Price
Basis2_Number will be the Cap Sell value (when Basis1_DN_FK is null) OR the number to multiply by the Basis2 in order to get the Cap Sell.
Basis3_DN_FK will be 111602 for the Sell_Price_Cust_Amt

Then, the calculation is:  EPA-3232

Final Rebate_CB_Amt = 
Initial Rebate_CB_Amt - 
Case When Sell_Price_Cust_Amt > Cap Sell and (Sell_Price_Cust_Amt - Cap Sell) * 0.5 < Initial Rebate_CB_Amt 
then (Sell_Price_Cust_Amt - Cap Sell) * 0.5 else 0 end


*/



------------------------------------------------------------------------------------------
---  CALCULATE RESULT3  DO NOT Allow NULL Operators
------------------------------------------------------------------------------------------


UPDATE ECR
        SET RESULT3 = 
                     CASE OPERATION3
                     WHEN '+'				THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN RESULT2 + NUMBER3
                                                 WHEN 'DATA NAME'	THEN RESULT2 + BASIS3
                                                 WHEN 'OPERANDS'	THEN RESULT2 + NUMBER3
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END   
                     WHEN '-'				THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN RESULT2 - NUMBER3
                                                 WHEN 'DATA NAME'	THEN RESULT2 - BASIS3
                                                 WHEN 'OPERANDS'	THEN RESULT2 - NUMBER3
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END   
                     WHEN '*'				THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN RESULT2 * ( NUMBER3 / @v_operand_divisor )
                                                 WHEN 'DATA NAME'	THEN RESULT2 * ( BASIS3 / @v_operand_divisor )
                                                 WHEN 'OPERANDS'	THEN RESULT2 * ( NUMBER3 / @v_operand_divisor )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END  
                     WHEN '/'				THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN RESULT2 / ( NUMBER3 / @v_operand_divisor )
                                                 WHEN 'DATA NAME'	THEN RESULT2 / ( BASIS3 / @v_operand_divisor )
                                                 WHEN 'OPERANDS'	THEN RESULT2 / ( NUMBER3 / @v_operand_divisor )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'DISC'			THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN RESULT2 * ( 1 - ( NUMBER3 / @v_operand_divisor )  )
                                                 WHEN 'DATA NAME'	THEN RESULT2 * ( 1 - ( BASIS3 / @v_operand_divisor )  )
                                                 WHEN 'OPERANDS'	THEN RESULT2 * ( 1 - ( NUMBER3 / @v_operand_divisor )  )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'GM'			    THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN ( RESULT2 / ( 1 - ( NUMBER3 / @v_operand_divisor  ) ) )																		  
                                                 WHEN 'DATA NAME'	THEN ( RESULT2 / ( 1 - ( BASIS3 / @v_operand_divisor  ) ) )		
                                                 WHEN 'OPERANDS'	THEN ( RESULT2 / ( 1 - ( NUMBER3 / @v_operand_divisor  ) ) )		
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                    ---- KS 02/10/2012... See comment above on the change to 'GM'					
                     WHEN 'PCT'			    THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN ( RESULT2 * ( 1 + ( NUMBER3 / @v_operand_divisor )  ) )
                                                 WHEN 'DATA NAME'	THEN ( RESULT2 * ( 1 + ( BASIS3 / @v_operand_divisor )  ) )
                                                 WHEN 'OPERANDS'	THEN ( RESULT2 * ( 1 + ( NUMBER3 / @v_operand_divisor )  ) )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'NAMED OPERATION'	THEN NULL --- CURRENTLY HAVE NONE DEFINED
                     ELSE NULL
                     END        
from synchronizer.EVENT_CALC_RULES ECR
inner join synchronizer.event_calc EC with (Nolock) on ecr.event_fk = ec.EVENT_ID
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( ECR.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID )
INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
	ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK )
WHERE 1 = 1
	AND   ISNULL ( RH.ALW_NULL_OPS_IND, 0 ) = 0
	AND   ECR.RULES_ACTION_OPERATION3_FK IS NOT NULL 		
	AND   EC.JOB_FK = @in_analysis_job_fk
	AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
	AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq





------------------------------------------------------------------------------------------
---  CALCULATE RESULT3  Allow NULL Operators 
------------------------------------------------------------------------------------------


UPDATE ECR 
        SET RESULT3 = 
                     CASE OPERATION3
                     WHEN '+'				THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN RESULT2 + ISNULL ( NUMBER3, 0 )
                                                 WHEN 'DATA NAME'	THEN RESULT2 + ISNULL ( BASIS3, 0 )
                                                 WHEN 'OPERANDS'	THEN RESULT2 + ISNULL ( NUMBER3, 0 )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END   
                     WHEN '-'				THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN RESULT2 - ISNULL ( NUMBER3, 0 )
                                                 WHEN 'DATA NAME'	THEN RESULT2 - ISNULL ( BASIS3, 0 )
                                                 WHEN 'OPERANDS'	THEN RESULT2 - ISNULL ( NUMBER3, 0 )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END   
                     WHEN '*'				THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN RESULT2 * 
                                                                                ( CASE ( ISNULL ( NUMBER3, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( NUMBER3 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'DATA NAME'	THEN RESULT2 * 
                                                                                ( CASE ( ISNULL ( BASIS3, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( BASIS3 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'OPERANDS'	THEN RESULT2 * 
                                                                                ( CASE ( ISNULL ( NUMBER3, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( NUMBER3 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 ELSE  NULL
                                                 END  
                     WHEN '/'				THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN RESULT2 / 
                                                                                ( CASE ( ISNULL ( NUMBER3, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( NUMBER3 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'DATA NAME'	THEN RESULT2 / 
                                                                                ( CASE ( ISNULL ( BASIS3, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( BASIS3 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'OPERANDS'	THEN RESULT2 / 
                                                                                ( CASE ( ISNULL ( NUMBER3, 0 ) )
                                                                                  WHEN 0 THEN 1 
                                                                                  ELSE ( NUMBER3 / @v_operand_divisor )
                                                                                  END
                                                                                )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'DISC'			THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN RESULT2 * ( 1 - ( ISNULL ( NUMBER3, 0 ) / @v_operand_divisor )  )
                                                 WHEN 'DATA NAME'	THEN RESULT2 * ( 1 - ( ISNULL ( BASIS3, 0 ) / @v_operand_divisor )  )
                                                 WHEN 'OPERANDS'	THEN RESULT2 * ( 1 - ( ISNULL ( NUMBER3, 0 ) / @v_operand_divisor )  )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'GM'			    THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN ( RESULT2 / ( 1 - ( ISNULL ( NUMBER3, 0 ) / @v_operand_divisor  ) ) )																		  
                                                 WHEN 'DATA NAME'	THEN ( RESULT2 / ( 1 - ( ISNULL ( BASIS3, 0 ) / @v_operand_divisor  ) ) )		
                                                 WHEN 'OPERANDS'	THEN ( RESULT2 / ( 1 - ( ISNULL ( NUMBER3, 0 ) / @v_operand_divisor  ) ) )		
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                    ---- KS 02/10/2012... See comment above on the change to 'GM'					
                     WHEN 'PCT'			    THEN CASE BASIS_SOURCE3
                                                 WHEN 'NUMERIC'		THEN ( RESULT2 * ( 1 + ( ISNULL ( NUMBER3, 0 ) / @v_operand_divisor )  ) )
                                                 WHEN 'DATA NAME'	THEN ( RESULT2 * ( 1 + ( ISNULL ( BASIS3, 0 ) / @v_operand_divisor )  ) )
                                                 WHEN 'OPERANDS'	THEN ( RESULT2 * ( 1 + ( ISNULL ( NUMBER3, 0 ) / @v_operand_divisor )  ) )
                                                 WHEN 'LOOKUP'		THEN NULL  -- DO LATER
                                                 END 
                     WHEN 'NAMED OPERATION'	THEN NULL --- CURRENTLY HAVE NONE DEFINED
                     ELSE NULL
                     END        
from synchronizer.EVENT_CALC_RULES ECR
inner join synchronizer.event_calc EC with (Nolock) on ecr.event_fk = ec.EVENT_ID
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( ECR.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID )
INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
	ON ( RH.RULES_HIERARCHY_ID = RS.RULES_HIERARCHY_FK )
WHERE 1 = 1
	AND   ISNULL ( RH.ALW_NULL_OPS_IND, 0 ) = 1
	AND   ECR.RULES_ACTION_OPERATION3_FK IS NOT NULL 		
	AND   EC.JOB_FK = @in_analysis_job_fk
	AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
	AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq





------------------------------------------------------------------------------------------
---  CALCULATE FINAL RESULT FOR rebated_cost_cb_amt ONLY
-- KS        07/24/2012   NCE requested that we allow the calculation of a negative rebated_cost_cb_amt
------------------------------------------------------------------------------------------

UPDATE ECR 
        SET RESULT = COALESCE( RESULT3, RESULT2, RESULT1 )
from synchronizer.EVENT_CALC_RULES ECR
inner join synchronizer.event_calc EC with (Nolock) on ecr.event_fk = ec.EVENT_ID
WHERE 1 = 1
	AND   EC.JOB_FK = @in_analysis_job_fk
	AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
	AND   EC.RESULT_DATA_NAME_FK = 111401    --- DO NOT CHECK FOR < 0 FOR rebated_cost_cb_amt
	AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq



------------------------------------------------------------------------------------------
---  CALCULATE FINAL RESULT  WHEN NOT rebated_cost_cb_amt
------------------------------------------------------------------------------------------


UPDATE ECR 
        SET RESULT = CASE WHEN ( COALESCE( RESULT3, RESULT2, RESULT1 )  < 0 )
                          THEN 0
                          ELSE COALESCE( RESULT3, RESULT2, RESULT1 )
                          END 
from synchronizer.EVENT_CALC_RULES ECR
inner join synchronizer.event_calc EC with (Nolock) on ecr.event_fk = ec.EVENT_ID
WHERE 1 = 1
	AND   EC.JOB_FK = @in_analysis_job_fk
	AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
	AND   EC.RESULT_DATA_NAME_FK <> 111401    --- CHECK FOR < 0 WHEN NOT rebated_cost_cb_amt	
	AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq



-----------------------------------------------------------------
--  IF RULES_OPTIMIZATION_MATRIX_FK IS NOT NULL
--  THEN UPDATE RESULT WITH THE INCREMEMT MULTIPLIER IF ONE FOUND
-- CHANGE SO DOES NOT REQUIRE RULES_OPTIMIZATION_FK ON RULES... CHECK RUNTIME.
-----------------------------------------------------------------


IF @in_result_data_name_fk = 111602
BEGIN


---------------------------------------------------
-- Temp Table for all Qualified Rules
---------------------------------------------------


--drop temp tables
IF object_id('tempdb..#TS_RULES_OPTIMIZATION') is not null
   drop table #TS_RULES_OPTIMIZATION

CREATE TABLE #TS_RULES_OPTIMIZATION(
	[TS_RULES_OPTIMIZATION_ID] [bigint] IDENTITY(1000,1) NOT NULL,
	[EVENT_RULES_FK] [bigint] NULL,		
	[PERCENT_VALUE] [NUMERIC](18,6) NULL,		
)

---   TRUNCATE TABLE  #TS_RULES_OPTIMIZATION

INSERT INTO  #TS_RULES_OPTIMIZATION(
	[EVENT_RULES_FK],
	[PERCENT_VALUE]
	)
SELECT 
    ECR.EVENT_RULES_ID
  ,ISNULL (
     ( SELECT ROI.PERCENT_VALUE FROM synchronizer.RULES_OPTIMIZATION_INCREMENT ROI WITH (NOLOCK)
     WHERE  ROI.RULES_OPTIMIZATION_MATRIX_FK = ROM.RULES_OPTIMIZATION_MATRIX_ID
     AND    ROI.INCREMENT_DV_FK =  ECROMI.DATA_VALUE_FK )
     , 1 ) AS PERCENT_VALUE
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ( ECR.EVENT_FK = EC.EVENT_ID )
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
  ON ( RA.RULES_ACTION_ID = ECR.RULES_ACTION_FK )
  ---- HARDCODE FOR ECLIPSE FOR NOW..
INNER JOIN synchronizer.RULES_OPTIMIZATION RO WITH (NOLOCK)
   ON ( RO.RULES_OPTIMIZATION_ID  =  RA.RULES_OPTIMIZATION_FK  )
INNER JOIN synchronizer.RULES_OPTIMIZATION_MATRIX ROM WITH (NOLOCK)
   ON ( ROM.RULES_OPTIMIZATION_FK  =  RO.RULES_OPTIMIZATION_ID  )
INNER JOIN EPACUBE.ENTITY_CATEGORY ECROM WITH (NOLOCK) 
   ON ( ECROM.DATA_NAME_FK  = RO.CUST_DN_FK
   AND  ECROM.DATA_VALUE_FK = ROM.CUST_DV_FK
   AND  ECROM.ENTITY_STRUCTURE_FK = EC.CUST_ENTITY_STRUCTURE_FK )
INNER JOIN EPACUBE.ENTITY_CATEGORY ECROMI WITH (NOLOCK)
   ON (  ECROMI.ENTITY_STRUCTURE_FK = EC.CUST_ENTITY_STRUCTURE_FK
   AND   ECROMI.DATA_NAME_FK = RO.INCREMENT_DN_FK )
WHERE 1 = 1
AND   RA.RULES_OPTIMIZATION_FK IS NOT NULL
AND   EC.JOB_FK = @in_analysis_job_fk
AND   EC.RESULT_DATA_NAME_FK = 111602


UPDATE synchronizer.EVENT_CALC_RULES
SET RESULT = RESULT * TSRO.PERCENT_VALUE
FROM #TS_RULES_OPTIMIZATION TSRO 
WHERE EVENT_RULES_ID = TSRO.EVENT_RULES_FK 


--drop temp tables
IF object_id('tempdb..#TS_RULES_OPTIMIZATION') is not null
   drop table #TS_RULES_OPTIMIZATION


END



------------------------------------------------------------------------------
--  IF RESULT IS REBATE_CB_AMT MAKE SURE ITS REBATED COST <= MARGIN_COST_BASIS
------------------------------------------------------------------------------

IF @in_result_data_name_fk = 111501
BEGIN

UPDATE synchronizer.EVENT_CALC_RULES
SET RESULT = NULL
   ,DISQUALIFIED_COMMENT = 'REBATED CB COST > MARGIN_COST_BASIS_AMT'
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ECR.EVENT_FK = EC.EVENT_ID
WHERE 1 = 1
AND   EC.JOB_FK = @in_analysis_job_fk
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk
AND   ( EC.PRICING_COST_BASIS_AMT - ECR.RESULT ) > ISNULL (EC.MARGIN_COST_BASIS_AMT, 0)

END



------------------------------------------------------------------------------
--  IF RESULT IS REBATED_COST_CB_AMT MAKE SURE LESS THAN MARGIN_COST_BASIS_AMT
--- NOTE ADDED PRICING AND MARGIN COSTS TO EVENT_CALC IN V516
------------------------------------------------------------------------------

IF @in_result_data_name_fk = 111401
BEGIN

UPDATE synchronizer.EVENT_CALC_RULES
SET RESULT = NULL
   ,DISQUALIFIED_COMMENT = 'REBATED CB COST > MARGIN_COST_BASIS_AMT'
FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK)
  ON ECR.EVENT_FK = EC.EVENT_ID
WHERE 1 = 1
AND   EC.JOB_FK = @in_analysis_job_fk
AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk
AND   ECR.RESULT > ISNULL (EC.MARGIN_COST_BASIS_AMT, 0)

END





------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of marginmgr.PERFORM_CALCULATION'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of marginmgr.PERFORM_CALCULATION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
