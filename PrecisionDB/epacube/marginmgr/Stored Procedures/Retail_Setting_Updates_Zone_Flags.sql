﻿
--	Modifications
--	5/6/2020	Gary Stone	URM-1862	Better deal with Reinstate Inheritance so it is not so confusing when the user applies it over a record with the current effective date
--	5/13/2020	Gary Stone	URM-1862	Return user_msg when trying to reinstate inheritance on a field that is already inheriting
CREATE procedure [marginmgr].[Retail_Setting_Updates_Zone_Flags]
	@Pricing_Date Date
	, @Zone_Segment_FK bigint
	, @Data_Name Varchar(64)
	, @Value varchar(16)
	, @Prod_Segment_Data_Name_FK bigint
	, @Prod_Segment_FK bigint
	, @Reinstate_Inheritance int
	, @User varchar(64)
as

Set NoCount On

/*

--marginmgr.Retail_Setting_Updates_Zone '2020-01-16', 113247, 'lg_flag', '0', 110103, 39072, 110103, 0, 0, 0, 'gstone@epacube.com'

--marginmgr.Retail_Setting_Updates_Zone '2020-01-16', 113247, 'lg_flag', '0', 501044, 1014719257, 110103, 0, 0, 0, 'gstone@epacube.com'
--marginmgr.Retail_Setting_Updates_Zone '2020-01-18', 113247, 'lg_flag', '1', 501045, 1014719558, 110103, 0, 0, 0, 'gstone@epacube.com'

--marginmgr.Retail_Setting_Updates_Zone '2020-05-06', 113247, 'lg_flag', '1', 501045, 1014720439, 110103, 0, 0, 1, 'gstone@epacube.com'
--exec marginmgr.Retail_Setting_Updates_Zone '2020-05-14', 113246, 'lg_flag', '0', 501044, 1014719367, 110103, 0, 0, 1, 'gstone@epacube.com'

	Declare @Pricing_Date Date = '2020-05-14'
	Declare @Zone_Segment_FK bigint = 113246
	Declare @Data_Name Varchar(64) = 'lg_flag'
	Declare @Value varchar(16) = '0' --0.56 --0.4321
	Declare @Prod_Segment_Data_Name_FK bigint	= 501044
	Declare @Prod_Segment_FK bigint = 1014719367
	Declare @Reinstate_Inheritance int = 1
	Declare @User varchar(64) = 'gstone@epacube.com'
*/

	Declare @Data_Name_FK bigint
	Set @Data_Name_FK = (select data_name_id from epacube.data_name where name = replace(@Data_Name, 'lg_flag', 'URM_LEVEL_GROSS_FLAG'))
	Declare @val int
	Declare @CurValue int
	Declare @InheritVal int
	Declare @NewVal varchar(8)
	Declare @Precedence int = (select precedence from epacube.data_name where data_name_id = @Prod_Segment_Data_Name_FK)
	Declare @ReturnRecords int = 0


	--select ss.data_name_fk, ss.ATTRIBUTE_Y_N 'InheritVal', ss.effective_date, ss.precedence, ss.segments_settings_id, ss.PROD_SEGMENT_DATA_NAME_FK, reinstate_inheritance_date
	--								from epacube.segments_settings ss
	--								inner join #prodsegments ps on ss.PROD_SEGMENT_FK = ps.prod_segment_fk and ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk
	--								where 1 = 1
	--								and ss.ZONE_SEGMENT_FK = @Zone_Segment_FK
	--								and data_name_fk = @data_name_fk
	--								and ss.effective_date <= @Pricing_Date and ss.precedence > @Precedence
	--								and (ss.reinstate_inheritance_date > @Pricing_Date or ss.reinstate_inheritance_date is null)





	Set @User = Replace(@User, '.', '_')
	Set @val = case when @value = '' then null else cast(@value as int) end
	Set @NewVal = replace(replace(@val, 0, 'N'), 1, 'Y')

if object_id('tempdb..#prodsegments') is not null
drop table #prodsegments

	Create table #prodsegments(prod_segment_fk bigint null, parent_prod_segment_fk bigint null, prod_segment_data_name_fk bigint null, precedence int null, alias_id bigint null)
	create index idx_01 on #prodsegments(prod_segment_fk, prod_segment_data_name_fk)

If @Prod_Segment_FK is not null
Insert into #prodsegments(prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
	Select a.*, dn.precedence 
	from (
	select dv.data_value_id 'prod_segment_fk', dv.parent_data_value_fk, dv.data_name_fk 'prod_segment_data_name_fk' from epacube.data_value dv where data_value_id = (select dv.PARENT_DATA_VALUE_FK from epacube.data_value dv where data_value_id = (select PARENT_DATA_VALUE_FK from epacube.data_value dv where data_value_id = @prod_segment_fk))
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where data_value_id = (select PARENT_DATA_VALUE_FK from epacube.data_value dv where data_value_id = @prod_segment_fk)
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where data_value_id = @prod_segment_fk
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where data_value_id = @prod_segment_fk
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where parent_data_value_fk in (select dv.data_value_id from epacube.data_value dv where data_value_id = @prod_segment_fk)
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where parent_data_value_fk in (select dv.data_value_id from epacube.data_value dv where parent_data_value_fk = (select dv.data_value_id from epacube.data_value dv where data_value_id = @prod_segment_fk))
	) A inner join epacube.data_name dn with (nolock) on a.prod_segment_data_name_fk = dn.DATA_NAME_ID 

	--create index idx_01 on #prodsegments(prod_segment_fk, prod_segment_data_name_fk)

	If isnull(@Prod_Segment_Data_Name_FK, 0) = 110103
	Begin

		insert into #prodsegments(prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
		select @prod_segment_fk, null 'parent_prod_segment_fk', 110103 'prod_segment_data_name_fk', 5 'precedence'

		
		Insert into #prodsegments(prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
		select pmt.product_structure_fk, null 'parent_prod_segment_fk', 110103 'prod_segment_data_name_fk', 5 'precedence'
		from #prodsegments s
		inner join epacube.product_mult_type pmt1 with (nolock) on s.prod_segment_fk = pmt1.PRODUCT_STRUCTURE_FK and pmt1.ENTITY_STRUCTURE_FK = @Zone_Segment_FK and pmt1.data_name_fk = 500019
		inner join epacube.product_mult_type pmt with (nolock) on pmt1.data_value_fk = pmt.data_value_fk and pmt1.ENTITY_STRUCTURE_FK = pmt.ENTITY_STRUCTURE_FK and pmt.DATA_NAME_FK = 500019
			and pmt1.PRODUCT_STRUCTURE_FK <> pmt.PRODUCT_STRUCTURE_FK
		left join epacube.product_attribute pa_500012 with (nolock) on pmt.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
		where pmt.product_structure_fk not in (select prod_segment_fk from #prodsegments)
		and pa_500012.product_attribute_id is null
		group by pmt.product_structure_fk

		insert into #prodsegments 
		(prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
		select distinct
		dv.data_value_id 'prod_segment_fk', dv.PARENT_DATA_VALUE_FK,spp.data_name_fk 'prod_segment-_data_name_fk', dn.precedence
		from #prodsegments ps
		inner join epacube.segments_product sp with (nolock) on ps.prod_segment_fk = sp.product_structure_fk and ps.prod_segment_data_name_fk = sp.DATA_NAME_FK
		inner join epacube.segments_product spp with (nolock) on sp.product_structure_fk = spp.product_structure_fk and spp.data_name_fk not in (110100, 110103)
		inner join epacube.data_value dv with (nolock) on spp.prod_segment_fk = dv.data_value_id
		inner join epacube.data_name dn with (nolock) on spp.DATA_NAME_FK = dn.DATA_NAME_ID

	End

	Set @InheritVal = replace(replace(Isnull(
		(Select InheritVal from (
									Select data_name_fk, InheritVal, effective_date, precedence, dense_rank()over(partition by data_name_fk order by precedence, effective_date desc, segments_settings_id desc) DRank
									from (
									select ss.data_name_fk, ss.ATTRIBUTE_Y_N 'InheritVal', ss.effective_date, ss.precedence, ss.segments_settings_id
									from epacube.segments_settings ss
									inner join #prodsegments ps on ss.PROD_SEGMENT_FK = ps.prod_segment_fk and ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk
									where 1 = 1
									and ss.ZONE_SEGMENT_FK = @Zone_Segment_FK
									and data_name_fk = @data_name_fk
									and ss.effective_date <= @Pricing_Date and ss.precedence > @Precedence
									and (ss.reinstate_inheritance_date > @Pricing_Date or ss.reinstate_inheritance_date is null)
									Union
									select ss.data_name_fk, ss.ATTRIBUTE_Y_N, ss.effective_date, isnull(ss.precedence, 100) 'precedence', ss.segments_settings_id
									from epacube.segments_settings ss
									where 1 = 1
									and PROD_SEGMENT_FK is null
									and PROD_SEGMENT_DATA_NAME_FK is null
									and ss.ZONE_SEGMENT_FK = @Zone_Segment_FK
									and data_name_fk = @data_name_fk
									and effective_date <= @Pricing_Date
									) a 
								) b where DRank = 1)
		, 'N'), 'N', 0), 'Y', 1)

If @Reinstate_Inheritance = 1
Begin
print 'reinstate updated'

	If exists
	(select * from epacube.segments_settings where data_name_fk = 500015 and PROD_SEGMENT_FK = @prod_segment_fk and PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK and effective_date = @Pricing_Date and @Reinstate_Inheritance = 1)
	Begin
		delete from epacube.segments_settings where data_name_fk = 500015 and PROD_SEGMENT_FK = @prod_segment_fk and PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK and effective_date = @Pricing_Date and @Reinstate_Inheritance = 1
	End

	Insert into #prodsegments(prod_segment_fk, prod_segment_data_name_fk, precedence)
	values(@prod_segment_fk, @prod_segment_data_name_fk, @Precedence)

	If @Val = @InheritVal
	Begin
	goto ReturnRecords
		--Delete ss
		--from epacube.segments_settings ss
		--	inner join #prodsegments ps on ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk
		--								and ss.PROD_SEGMENT_FK = ps.prod_segment_fk
		--	where data_name_fk = @Data_Name_FK
		--	and ZONE_SEGMENT_FK = @Zone_Segment_FK
		--	and Effective_Date = @Pricing_Date
		--	and ss.prod_segment_data_name_fk = @Prod_Segment_Data_Name_FK
	End

	If (select count(*) from epacube.segments_settings ss
		where data_name_fk = @Data_Name_FK
		and PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
		and PROD_SEGMENT_FK = @Prod_Segment_FK
		and ZONE_SEGMENT_FK = @Zone_Segment_FK
		and Effective_Date < @Pricing_Date
		and reinstate_inheritance_date = @Pricing_Date
		) = 0
	Begin
		If @Data_Name_FK = 500015
			Update SS
			Set Reinstate_inheritance_date = @Pricing_Date
			from epacube.segments_settings ss
			where data_name_fk = @Data_Name_FK
			and PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
			and PROD_SEGMENT_FK = @Prod_Segment_FK
			and ZONE_SEGMENT_FK = @Zone_Segment_FK
			and Effective_Date < @Pricing_Date
			and reinstate_inheritance_date is null
		Else
			Update SS
			Set Reinstate_inheritance_date = @Pricing_Date
			from epacube.segments_settings ss
			inner join #prodsegments ps on ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk
										and ss.PROD_SEGMENT_FK = ps.prod_segment_fk
			where data_name_fk = @Data_Name_FK
			and ZONE_SEGMENT_FK = @Zone_Segment_FK
			and Effective_Date < @Pricing_Date
			and ss.prod_segment_data_name_fk = @Prod_Segment_Data_Name_FK
			and reinstate_inheritance_date is null
	End
	else
	If @Val <> @InheritVal
	Begin
		If @Data_Name_FK = 500015
			Update SS
			Set Reinstate_inheritance_date = Null
			from epacube.segments_settings ss
			where data_name_fk = @Data_Name_FK
			and PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
			and PROD_SEGMENT_FK = @Prod_Segment_FK
			and ZONE_SEGMENT_FK = @Zone_Segment_FK
			and reinstate_inheritance_date = @Pricing_Date
		Else
			Update SS
			Set Reinstate_inheritance_date = Null
			from epacube.segments_settings ss
			inner join #prodsegments ps on ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk
										and ss.PROD_SEGMENT_FK = ps.prod_segment_fk
			where data_name_fk = @Data_Name_FK
			and ZONE_SEGMENT_FK = @Zone_Segment_FK
			and ss.prod_segment_data_name_fk = @Prod_Segment_Data_Name_FK
			and reinstate_inheritance_date = @Pricing_Date
	End
	goto ReturnRecords
End

If @Prod_Segment_Data_Name_FK is null and @Prod_Segment_FK is null
Begin

----Working at the Zone level

	Delete from epacube.segments_settings where data_name_fk = @data_name_fk and prod_segment_fk is null and prod_segment_data_name_fk is null and zone_segment_fk = @zone_segment_fk and effective_date = @pricing_Date

	Set @CurValue = replace(replace(
							(select top 1 ATTRIBUTE_Y_N from epacube.segments_settings where data_name_fk = @data_name_fk and prod_segment_fk is null 
							and prod_segment_data_name_fk is null and zone_segment_fk = @zone_segment_fk and effective_date <= @pricing_Date order by effective_date desc)
					, 'Y', 1), 'N', 0)

	If @val is not null and @CurValue <> @val
		Insert into epacube.epacube.segments_settings
		(Effective_Date, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, ZONE_SEGMENT_FK, Data_Name_FK, ENTITY_DATA_NAME_FK, Attribute_Y_N, Attribute_Event_Data, Record_Status_CR_FK, Create_User, Precedence, [DATA_SOURCE])
				Select 
				cast(@Pricing_Date as date) 'effective_date'
				, 1 'org_entity_structure_fk'
				, @Zone_Segment_FK 'zone_entity_structure_fk'
				, @Zone_Segment_FK 'zone_segment_fk'
				, dn.data_name_id 'data_name_fk'
				, 151000 'entity_data_name_fk'
				, @NewVal 'attribute_y_n'
				, @NewVal 'attribute_event_data'
				, 1 'record_status_cr_fk'
				, @User 'create_user'
				, (select precedence from epacube.data_name where data_name_id = @Prod_Segment_Data_Name_FK) 'Precedence'
				, 'USER UPDATE' 'DATA_SOURCE'
				from 
				epacube.epacube.data_name dn with (nolock)
				inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.DATA_SET_ID
				left join epacube.epacube.data_value dv with (nolock) on dv.data_value_id = @Prod_Segment_FK
				where 1 = 1
					AND DN.NAME = @Data_Name

	--Return data to UI
		Select pricing_date
		, zone_segment_fk
		, data_name
		, value
		, prod_segment_data_name_fk
		, prod_segment_fk
		, seq
		, sort_value
		from (
				Select @Pricing_Date 'pricing_date'
				, @zone_segment_fk 'zone_segment_fk'
				, ss.DATA_NAME_FK 'data_name'
				, ss.ATTRIBUTE_Y_N 'value'
				, cast(ss.prod_segment_data_name_fk as bigint) 'prod_segment_data_name_fk'
				, cast(ss.prod_segment_fk as bigint) 'prod_segment_fk'
				, '1' 'seq'
				, ss.ATTRIBUTE_Y_N 'sort_value'
				, Dense_Rank()over(partition by ss.data_name_fk order by ss.effective_date desc, segments_settings_id desc) DRank
				from epacube.segments_settings ss with (nolock) 
				where ss.data_name_fk = @data_name_fk
				and ss.Effective_Date <= @Pricing_Date
				and ss.zone_segment_fk = @Zone_Segment_FK
				and ss.PROD_SEGMENT_DATA_NAME_FK is null
				and ss.PROD_SEGMENT_FK is null
				and ss.RECORD_STATUS_CR_FK = 1
			) A where DRank = 1

	goto eof
End
Else
Begin

----Working at the Zone Item Class level
Print 'Deleting current recs'

	Delete ssf
	from epacube.epacube.segments_settings ssf 
	inner join
		(select ss1.SEGMENTS_SETTINGS_ID, ss1.effective_date
		from epacube.epacube.segments_settings ss with (nolock)
		inner join epacube.epacube.PRODUCT_MULT_TYPE pmt1 with (nolock) on ss.prod_segment_fk = pmt1.PRODUCT_STRUCTURE_FK and ss.ZONE_SEGMENT_FK = pmt1.ENTITY_STRUCTURE_FK and pmt1.data_name_fk = 500019
			and pmt1.product_structure_fk = @Prod_Segment_FK
		inner join epacube.epacube.product_mult_type pmt with (nolock) on pmt1.ENTITY_STRUCTURE_FK = pmt.ENTITY_STRUCTURE_FK and pmt1.DATA_VALUE_FK = pmt.DATA_VALUE_FK and pmt.data_name_fk = 500019
		inner join epacube.epacube.segments_settings ss1 with (nolock) on ss1.prod_segment_fk = pmt.PRODUCT_STRUCTURE_FK and ss1.ZONE_SEGMENT_FK = pmt.ENTITY_STRUCTURE_FK and ss1.data_name_fk = ss.data_name_fk
			and pmt.product_structure_fk <> @Prod_Segment_FK and ss.effective_date = ss1.Effective_Date
		where 1 = 1
		and ss.data_name_fk = @data_name_fk
		and ss.ZONE_SEGMENT_FK = @Zone_Segment_FK
		and ss.prod_segment_data_name_fk = @Prod_Segment_Data_Name_FK
		) A
		on ssf.segments_settings_id = a.segments_settings_id
	where ssf.Effective_Date = cast(@Pricing_Date as date)

	Delete ssf 
	from epacube.epacube.segments_settings ssf
	where 1 = 1
	and ssf.data_name_fk = @data_name_fk
	and ssf.prod_segment_fk = @Prod_Segment_FK
	and ssf.PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
	and ssf.ZONE_SEGMENT_FK = @zone_segment_fk
	and ssf.Effective_Date = cast(@Pricing_Date as date)

	Set @CurValue = Replace(Replace((
							Select ATTRIBUTE_EVENT_DATA
							from (
								Select *, Dense_rank()over(partition by data_name_fk order by precedence, effective_date desc, segments_settings_id desc) DRank
								from (
									Select SS.Segments_settings_ID, ss.data_name_fk, SS.ATTRIBUTE_EVENT_DATA, SS.effective_date, SP.PRODUCT_PRECEDENCE 'PRECEDENCE' 
									from epacube.segments_product sp with (nolock)
									inner join epacube.segments_settings ss with (nolock) on sp.prod_segment_fk = ss.prod_segment_FK and sp.DATA_NAME_FK = ss.PROD_SEGMENT_DATA_NAME_FK and ss.data_name_fk = @Data_Name_FK
									where sp.PROD_SEGMENT_FK = @Prod_Segment_FK 
									and sp.data_name_fk = @Prod_Segment_Data_Name_FK
									and ss.zone_segment_fk = @Zone_Segment_FK
									and sp.data_name_fk in (501043, 501044, 501045, 110103)
									and (ss.reinstate_inheritance_date > @Pricing_Date or ss.reinstate_inheritance_date is null)
									and ss.RECORD_STATUS_CR_FK = 1
									and ss.effective_date <= cast(@Pricing_Date as date)
									group by SS.Segments_settings_ID, ss.data_name_fk, SS.ATTRIBUTE_EVENT_DATA, SS.effective_date, SP.PRODUCT_PRECEDENCE
								) A 
							) B where DRank = 1								
					), 'N', 0), 'Y', 1)

	update ps
	set alias_id = dv.value
	from #prodsegments ps
	inner join epacube.product_mult_type pmt on ps.prod_segment_fk = pmt.PRODUCT_STRUCTURE_FK and pmt.ENTITY_STRUCTURE_FK = @Zone_Segment_FK and pmt.DATA_NAME_FK = 500019
	inner join epacube.data_value dv on pmt.data_value_fk = dv.data_value_id


If isnull(@Prod_Segment_Data_Name_FK, 0) <> 110103
and	(
		(@val <> @InheritVal and @CurValue is null)
		or
		(@val <> @InheritVal and @val <> @CurValue and @CurValue is not null)
		or
		(@val = @InheritVal and @val <> @CurValue and @CurValue is not null)
	)
	Begin
	Print 'Load Segments_Settings_1'
		Insert into epacube.epacube.segments_settings
		(Effective_Date, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, ZONE_SEGMENT_FK, Data_Name_FK, PROD_DATA_VALUE_FK, PROD_SEGMENT_DATA_NAME_FK, PROD_SEGMENT_FK, ENTITY_DATA_NAME_FK, Attribute_Y_N, Attribute_Event_Data, Record_Status_CR_FK, Create_User, Precedence, [DATA_SOURCE])
				Select 
				cast(@Pricing_Date as date) 'effective_date'
				, 1 'org_entity_structure_fk'
				, @Zone_Segment_FK 'zone_entity_structure_fk'
				, @Zone_Segment_FK 'zone_segment_fk'
				, @data_name_fk 'data_name_fk'
				, @PROD_SEGMENT_FK 'PROD_DATA_VALUE_FK'
				, @PROD_SEGMENT_DATA_NAME_FK 'PROD_SEGMENT_DATA_NAME_FK'
				, @PROD_SEGMENT_FK 'PROD_SEGMENT_FK'
				, 151000 'entity_data_name_fk'
				, @NewVal 'attribute_y_n'
				, @NewVal 'attribute_event_data'
				, 1 'record_status_cr_fk'
				, @User 'create_user'
				, (select precedence from epacube.data_name where data_name_id = @Prod_Segment_Data_Name_FK) 'Precedence'
				, 'USER UPDATE' 'DATA_SOURCE'
				from epacube.DATA_VALUE
				where data_name_fk = @Prod_Segment_Data_Name_FK and data_value_id = @Prod_Segment_FK
		End
	else 
	If isnull(@Prod_Segment_Data_Name_FK, 0) = 110103 
			and (
					(@data_name_fk = 500015 and @val <> isnull(@CurValue, 1))
				Or
					(@data_name_fk <> 500015 and @val <> @InheritVal and @val <> isnull(@CurValue, 99))
				)					
	Begin
	Print 'Load Segments_Settings_2'
		Insert into epacube.epacube.segments_settings
		(Effective_Date, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, ZONE_SEGMENT_FK, Data_Name_FK, PROD_DATA_VALUE_FK, PROD_SEGMENT_DATA_NAME_FK, PROD_SEGMENT_FK, PRODUCT_STRUCTURE_FK, ENTITY_DATA_NAME_FK, Attribute_Y_N, Attribute_Event_Data, Record_Status_CR_FK, Create_User, Precedence, [DATA_SOURCE])
			Select 
			cast(@Pricing_Date as date) 'effective_date'
			, 1 'org_entity_structure_fk'
			, @Zone_Segment_FK 'zone_entity_structure_fk'
			, @Zone_Segment_FK 'zone_segment_fk'
			, @data_name_fk 'data_name_fk'
			, ps.PROD_SEGMENT_FK 'prod_data_value_fk'
			, ps.PROD_SEGMENT_DATA_NAME_FK
			, ps.PROD_SEGMENT_FK
			, PS.prod_segment_fk 'product_structure_fk'
			, 151000 'entity_data_name_fk'
			, @NewVal 'attribute_y_n'
			, @NewVal 'attribute_event_data'
			, 1 'record_status_cr_fk'
			, @User 'create_user'
			, (select precedence from epacube.data_name where data_name_id = @Prod_Segment_Data_Name_FK) 'Precedence'
			, 'USER UPDATE' 'DATA_SOURCE'
			from #prodsegments ps where precedence = 5
	End

ReturnRecords:
	--Return data to UI

If @Prod_Segment_Data_Name_FK <> 110103
		Select @pricing_date 'pricing_date'
		, @zone_segment_fk 'zone_segment_fk'
		, @data_name 'data_name'
		, case when @Reinstate_Inheritance = 1 then null else @val end 'value'
		, @prod_segment_data_name_fk 'prod_segment_data_name_fk'
		, @prod_segment_fk 'prod_segment_fk'
		, 1 'seq'
		, @val 'sort_value'
		, case when @val = @curvalue and @val <> @inheritval then null
				when @val <> @CurValue and @curvalue is not null and @val = @InheritVal then null else @InheritVal end 'inheritval'
		, case when @Reinstate_Inheritance = 1 and @Val <> @InheritVal then 'Inheritance cannot be reinstated on a value that is already inheriting.' else null end 'USER_MSG'
--		, @InheritVal 'inheritval'
else
		Select @pricing_date 'pricing_date'
		, @zone_segment_fk 'zone_segment_fk'
		, @data_name 'data_name'
		, case when @data_name_fk <> 500015 and @val = @InheritVal and prod_segment_fk <> @Prod_Segment_FK then null else @Val end 'value'
		, @prod_segment_data_name_fk 'prod_segment_data_name_fk'
		, prod_segment_fk 'prod_segment_fk'
		, 1 'seq'
		, @val 'sort_value'
		, case when @data_name_fk = 500015 then null else @InheritVal end 'inheritval'
		, case when @Reinstate_Inheritance = 1 and @Val <> @InheritVal then 'Inheritance cannot be reinstated on a value that is already inheriting.' else null end 'USER_MSG'
		from #prodsegments where precedence = 5
End

eof:
--Select @Val '@Val', @CurValue '@CurValue', @InheritVal '@InheritVal', @Precedence '@Precedence', @data_name_fk '@data_name_fk', @Prod_Segment_Data_Name_FK '@Prod_Segment_Data_Name_FK', @Prod_Segment_FK '@Prod_Segment_FK', @Reinstate_Inheritance '@Reinstate_Inheritance', @ReturnRecords '@ReturnRecords', (select value from epacube.entity_identification where ENTITY_DATA_NAME_FK = 151000 and ENTITY_STRUCTURE_FK = @Zone_Segment_FK) 'zone_id'
