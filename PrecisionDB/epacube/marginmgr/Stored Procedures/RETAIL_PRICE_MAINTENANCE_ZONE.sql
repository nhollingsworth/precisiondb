﻿
-- =============================================
-- Author:		Gary Stone
-- Create date: 2018-04-01
-- Description:	UI and Reporting results for Level Gross Weekly
--	Modifications
--	5/5/2020	Jira 1872/1873	removed unit cost change from item listing line (node 3) in multi-zone option
-- =============================================
CREATE PROCEDURE [marginmgr].[RETAIL_PRICE_MAINTENANCE_ZONE]
 
	@Price_Mgmt_Type_CR_FK int = 650
	, @Item_Change_Type_CR_FK int = 655
	, @Qualified_Status_CR_FK int = 653
	, @Zone_Entity_Structure_FK varchar(Max) = ''
	, @PRICE_CHANGE_EFFECTIVE_DATE varchar(10) = NULL
	, @User Varchar(96) = ''
AS

/*

	Declare @PRICE_CHANGE_EFFECTIVE_DATE DATE = GETDATE() + 6 -  DATEPART(DW, GETDATE())
	Declare @Price_Mgmt_Type_CR_FK int = 650
	Declare @Item_Change_Type_CR_FK int = 656
	Declare @Qualified_Status_CR_FK int = 653
	Declare @Zone_Entity_Structure_FK varchar(Max) =  'all'
	--Declare @User varchar(64) = 'SSRS'
	Declare @User varchar(64) = 'gstone@epacube.com'
	--Declare @Zone_Entity_Structure_FK varchar(Max) = '113231'-- '113246,113237,113247,113230,113231,113232,113233,113234,113239'
	--select * from epacube.entity_identification where value = '40001'
*/

/* ---	NOTE
--	Item_Change_Type_CR_FK in UI results determines how competitive Pricing icon works - whether calling all items in alias or only item selected
	Pricesheet_Retail_Change_ID in results determines editable rows - only rows with values are editable.
	ind_launch_audit determines where Information icons show up. Value determines what pricesheet_retail_changes_id is called as a parameter
*/

	Set NoCount ON

	Declare @Report_Only int
	Declare @ind_display_as_reg int = Null
	Declare @ind_margin_changes int = Null

	Set @Report_Only = Case when isnull(@User, '') = 'SSRS' then 1 else 0 end
	Set @ind_display_as_reg = case when @item_change_type_cr_fk in (656, 662) then 0 else 1 end
	Set @ind_margin_changes = case when @item_change_type_cr_fk in (661, 662) then 1 else 0 end

	IF @PRICE_CHANGE_EFFECTIVE_DATE IS NULL
		SET @PRICE_CHANGE_EFFECTIVE_DATE = CAST(getdate() + 6 - datepart(dw, getdate()) AS DATE)

	If Object_ID('tempdb..#Zones') is not null
	drop table #Zones

	If Object_ID('tempdb..#Competitors') is not null
	drop table #Competitors

	If Object_ID('tempdb..#DisplayData') is not null
	drop table #DisplayData

	Create table #Zones(zone_entity_Structure_FK bigint not null)
	Create table #Competitors(product_structure_fk bigint not null)

	If @Zone_Entity_Structure_FK = 'All'
	Begin
		Insert into #Zones
		SELECT ei.ENTITY_STRUCTURE_FK 'ZONE_ENTITY_STRUCTURE_FK'
		FROM epacube.ENTITY_IDENTIFICATION ei
			INNER JOIN epacube.DATA_NAME dn ON ei.DATA_NAME_FK = dn.DATA_NAME_ID
			inner join epacube.entity_attribute ea with (nolock) on ei.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
			inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
		WHERE dn.NAME = 'ZONE_NUMBER'
		and dv.value = 'H'
	End
	else
	Begin
		Insert into #Zones
		Select listitem from common.utilities.SplitString(@Zone_Entity_Structure_FK, ',')
	End

	Insert into #Competitors
	Select cv.product_structure_fk from [epacube].[epacube].[COMPETITOR_VALUES] CV with (nolock) 
	inner join [epacube].[getEntitiesByPermission](@User, 544111) A1 on cv.COMP_ENTITY_STRUCTURE_FK = a1.entity_structure_fk
	where cv.PRODUCT_STRUCTURE_FK is not null group by cv.product_structure_fk

	Create index idx_01 on #Zones(zone_entity_structure_fk)
	Create index idx_01 on #Competitors(product_structure_fk)

	Declare @ZoneCount int
	Set @ZoneCount = (select count(*) from #Zones)


--		/*
--		----	START	****	Scenarios To Account For	****	----
--		Single or Multiple Zones
--			Regular Items and Margin Changes
--			Alias Parent
--			Parity Size
--			parity privvate Label

--		----	END	****	Scenarios To Account For	****	----
--*/

	Select 
	PRC.PRICESHEET_RETAIL_CHANGES_ID
	, PRC.PRICE_MGMT_TYPE_CR_FK
	, PRC.ITEM_CHANGE_TYPE_CR_FK
	, PRC.QUALIFIED_STATUS_CR_FK
	, PRC.PRODUCT_STRUCTURE_FK
	, PRC.ZONE_ENTITY_STRUCTURE_FK
	, PRC.ZONE 'ZONE'
	, PRC.ZONE_NAME 'ZONE_NAME'
	, PRC.SUBGROUP_ID 'SUBGROUP'
	, PRC.SUBGROUP_DESCRIPTION
	, PRC.SUBGROUP_DATA_VALUE_FK
	, cast(PRC.SUBGROUP_ID as varchar(64)) + ' ' +  PRC.SUBGROUP_DESCRIPTION 'SUBGROUP_SEARCH'
	, prc.item
	, PRC.ITEM_DESCRIPTION 'ITEM_DESCRIPTION'
	, prc.item + ' ' + PRC.ITEM_DESCRIPTION 'ITEM_SEARCH'
	, PRC.Alias_ID
	, prc.ALIAS_DESCRIPTION
	, PRC.Alias_ID + ' ' + PRC.ALIAS_DESCRIPTION 'ALIAS_SEARCH'
	, cast(PRC.STORE_PACK as VARCHAR(16)) 'STORE_PACK'
	, cast(PRC.SIZE_UOM as varchar(64)) 'SIZE_UOM'
	, round(PRC.unit_cost_cur, 4) 'CUR_COST'
	, PRC.case_cost_cur 'CUR_CASE_COST'
	, PRC.case_cost_new 'NEW_CASE_COST'
	, PRC.PRICE_MULTIPLE_CUR 'CMULT'
	, prc.price_cur 'CRETAIL'
	, case when isnull(PRC.PRICE_MULTIPLE_CUR, 1) = 1 then '' else cast(PRC.PRICE_MULTIPLE_CUR as varchar(8)) + '/' end + cast(prc.price_cur as varchar(16)) 'PRICE_CUR'
	, PRC.CUR_GM_PCT
	, PRC.COST_CHANGE
	, cast((prc.price_new / isnull(prc.PRICE_MULTIPLE_NEW, 1)) as numeric(18, 4)) - cast((prc.price_cur / isnull(prc.PRICE_MULTIPLE_CUR, 1)) as numeric(18, 4)) 'PRICE_CHANGE'
	, round(PRC.unit_cost_new, 4) 'NEW_COST'
	, case	when prc.PRICE_MGMT_TYPE_CR_FK = 651 and prc.PRICE_MULTIPLE_CUR = 1 then ''
			when prc.PRICE_MGMT_TYPE_CR_FK = 651 then cast(isnull(PRC.PRICE_MULTIPLE_CUR, 1) as varchar(8)) + '/' 
			--when isnull(PRC.PRICE_MULTIPLE_NEW, 1) = 1 then '' else cast(isnull(PRC.PRICE_MULTIPLE_NEW, 1) as varchar(8)) + '/' end + cast(isnull(PRC.PRICE_NEW, prc.PRICE_CUR) as varchar(16)) 'PRICE_NEW'
			when isnull(PRC.PRICE_MULTIPLE_CUR, 1) = 1 then '' else cast(isnull(PRC.PRICE_MULTIPLE_CUR, 1) as varchar(8)) + '/' end + cast(isnull(PRC.PRICE_NEW, prc.PRICE_CUR) as varchar(16)) 'PRICE_NEW'
	, PRC.NEW_GM_PCT 'NEW_GM_PCT'
	, PRC.TARGET_MARGIN
	, PRC.TM_SOURCE
	, PRC.SRP_ADJUSTED
	, PRC.GM_ADJUSTED
	, PRC.TM_ADJUSTED
	, PRC.HOLD_CUR_PRICE
	, PRC.REVIEWED
	, cast(null as int) 'HOLD_CUR_HDR'
	, cast(null as int) 'REVIEWED_HDR'
	, Case when C.product_structure_fk is not null then 1 else Null end 'CIF'
	, PRC.UPDATE_USER_PRICE
	, PRC.UPDATE_USER_SET_TM
	, PRC.UPDATE_USER_HOLD
	, PRC.UPDATE_USER_REVIEWED
	--. prc.PRICE_MULTIPLE_NEW
	, isnull(prc.PRICE_MULTIPLE_NEW, 1) 'PRICE_MULTIPLE_NEW'
	, prc.ind_Parity
	, prc.ind_Margin_Tol
	, ISNULL(prc.ind_Rescind, 0) 'ind_rescind'
	, 0 'force_rescind_display'
	, prc.ZONE 'zone_number'
	, Case	when prc.multiple_parities is not null then ' Parity to ' + prc.multiple_parities + ' ' 
			when related_item_alias_id is not null then ' Parity to ' + cast(related_item_alias_id as varchar(64)) + ': ' + isnull(parity_parent_item, parity_child_item) --+ ' '
			when parity_alias_id is not null then ' Parity to ' + cast(prc.parity_alias_id as varchar(64)) --+ ' ' 
			when isnull(parity_parent_item, parity_child_item) is not null then ' Parity to ' + isnull(parity_parent_item, parity_child_item) --+ ' '
		else '' end
		+ case when coalesce(Cast(prc.multiple_parities as varchar(8)), Cast(prc.related_item_alias_id as varchar(8)), Cast(prc.parity_alias_id as varchar(8))) is not null
			and coalesce(Cast(prc.ftn_Breakdown_Parent as varchar(8)), Cast(prc.ftn_cost_delta as varchar(8))) is not null then '; ' else '' end --+ ' '
		+ isnull(case when prc.ftn_cost_delta is not null and prc.ftn_Breakdown_Parent is not null then Cast(prc.ftn_Breakdown_Parent as varchar(8)) + '; '+  Cast(prc.ftn_cost_delta as varchar(8))
		else coalesce(Cast(prc.ftn_Breakdown_Parent as varchar(8)), Cast(prc.ftn_cost_delta as varchar(8))) end, '') 
		'item_superscript'
	, prc.multiple_parities
	, prc.parity_alias_id 'parity_alias_fk'
	, prc.ftn_cost_delta
	, prc.ftn_Breakdown_Parent
	, prc.item_sort
	, prc.item_group
	, prc.item_sort_report
	, Cast(ascii(left(PRC.ZONE_NAME, 1)) as varchar(16)) + right('000' + ltrim(rtrim(reverse(left(Reverse(PRC.ZONE_NAME), charindex(' ', Reverse(PRC.ZONE_NAME)) - 1)))), 3) + '1' 'Zone_Sort'
	, prc.ind_display_as_reg
	, prc.parity_status
	, cast(prc.ind_parity_status as varchar(8)) 'ind_parity_status'
	, parity_parent_item
	, parity_child_item
	, case when pa_500012.product_attribute_id is not null then '{"renderer":{"color":"red","text-decoration":"underline"}}' else null end 'ind_discontinued'
	, prc.price_change_effective_date
	, case when prc.COST_CHANGE_REASON like '%new to zone%' then 1 else 0 end 'NTZ'
	, cast(null as Numeric(18, 4)) 'cost_change_grp'
	, prc.related_item_alias_id
	into #DisplayData
	from marginmgr.PRICESHEET_RETAIL_CHANGES prc with (nolock)
	inner join #Zones Z on prc.zone_entity_structure_fk = Z.zone_entity_Structure_FK
	left join epacube.product_attribute pa_500012 with (nolock) on prc.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
	left join #Competitors C on prc.PRODUCT_STRUCTURE_FK = C.product_structure_fk
	where prc.PRICE_CHANGE_EFFECTIVE_DATE = @PRICE_CHANGE_EFFECTIVE_DATE
	and prc.ind_display_as_reg = @ind_display_as_reg
	and (
			(
				(prc.QUALIFIED_STATUS_CR_FK = @QUALIFIED_STATUS_CR_FK or @QUALIFIED_STATUS_CR_FK = 0)
				and (prc.PRICE_MGMT_TYPE_CR_FK = @Price_Mgmt_Type_CR_FK or @Price_Mgmt_Type_CR_FK = 0)
				and (IND_MARGIN_CHANGES = @ind_margin_changes)
				and @Report_Only = 0
			)
		or
			(
				(prc.QUALIFIED_STATUS_CR_FK = @QUALIFIED_STATUS_CR_FK or @ind_margin_changes = 1) --ITEM_CHANGE_TYPE_CR_FK in (661, 662))
				and 
				(prc.PRICE_MGMT_TYPE_CR_FK = @Price_Mgmt_Type_CR_FK or @Price_Mgmt_Type_CR_FK = 0)
				and 
				IND_MARGIN_CHANGES = @ind_margin_changes
				and @Report_Only = 1
			)
		)
	and isnull(prc.[COST_CHANGE_REASON], '') <> case when @Price_Mgmt_Type_CR_FK = 0 then 'nlg mixed status' else 'testing' end
	--and alias_id = 78799
	
	create index idx_01 on #DisplayData(QUALIFIED_STATUS_CR_FK, alias_id, product_structure_fk)
	create index idx_02 on #DisplayData(item_change_type_cr_fk, alias_id)
	create index idx_03 on #DisplayData(item_change_type_cr_fk, product_structure_fk)

	Insert into #DisplayData
	Select 
	PRC.PRICESHEET_RETAIL_CHANGES_ID
	, PRC.PRICE_MGMT_TYPE_CR_FK
	, PRC.ITEM_CHANGE_TYPE_CR_FK
	, PRC.QUALIFIED_STATUS_CR_FK
	, PRC.PRODUCT_STRUCTURE_FK
	, PRC.ZONE_ENTITY_STRUCTURE_FK
	, PRC.ZONE 'ZONE'
	, PRC.ZONE_NAME 'ZONE_NAME'
	, PRC.SUBGROUP_ID 'SUBGROUP'
	, PRC.SUBGROUP_DESCRIPTION
	, PRC.SUBGROUP_DATA_VALUE_FK
	, cast(PRC.SUBGROUP_ID as varchar(64)) + ' ' +  PRC.SUBGROUP_DESCRIPTION 'SUBGROUP_SEARCH'
	, prc.item
	, PRC.ITEM_DESCRIPTION 'ITEM_DESCRIPTION'
	, prc.item + ' ' + PRC.ITEM_DESCRIPTION 'ITEM_SEARCH'
	, PRC.Alias_ID
	, prc.ALIAS_DESCRIPTION
	, PRC.Alias_ID + ' ' + PRC.ALIAS_DESCRIPTION 'ALIAS_SEARCH'
	, cast(PRC.STORE_PACK as VARCHAR(16)) 'STORE_PACK'
	, cast(PRC.SIZE_UOM as varchar(64)) 'SIZE_UOM'
	, round(PRC.unit_cost_cur, 4) 'CUR_COST'
	, PRC.case_cost_cur 'CUR_CASE_COST'
	, PRC.case_cost_new 'NEW_CASE_COST'
	, PRC.PRICE_MULTIPLE_CUR 'CMULT'
	, prc.price_cur 'CRETAIL'
	, case when isnull(PRC.PRICE_MULTIPLE_CUR, 1) = 1 then '' else cast(PRC.PRICE_MULTIPLE_CUR as varchar(8)) + '/' end + cast(prc.price_cur as varchar(16)) 'PRICE_CUR'
	, PRC.CUR_GM_PCT
	, PRC.COST_CHANGE
	, cast((prc.price_new / isnull(prc.PRICE_MULTIPLE_NEW, 1)) as numeric(18, 4)) - cast((prc.price_cur / isnull(prc.PRICE_MULTIPLE_CUR, 1)) as numeric(18, 4)) 'PRICE_CHANGE'
	, round(PRC.unit_cost_new, 4) 'NEW_COST'
	, case	when prc.PRICE_MGMT_TYPE_CR_FK = 651 and prc.PRICE_MULTIPLE_CUR = 1 then ''
			when prc.PRICE_MGMT_TYPE_CR_FK = 651 then cast(isnull(PRC.PRICE_MULTIPLE_CUR, 1) as varchar(8)) + '/' 
			--when isnull(PRC.PRICE_MULTIPLE_NEW, 1) = 1 then '' else cast(isnull(PRC.PRICE_MULTIPLE_NEW, 1) as varchar(8)) + '/' end + cast(isnull(PRC.PRICE_NEW, prc.PRICE_CUR) as varchar(16)) 'PRICE_NEW'
			when isnull(PRC.PRICE_MULTIPLE_CUR, 1) = 1 then '' else cast(isnull(PRC.PRICE_MULTIPLE_CUR, 1) as varchar(8)) + '/' end + cast(isnull(PRC.PRICE_NEW, prc.PRICE_CUR) as varchar(16)) 'PRICE_NEW'
	, PRC.NEW_GM_PCT 'NEW_GM_PCT'
	, PRC.TARGET_MARGIN
	, PRC.TM_SOURCE
	, PRC.SRP_ADJUSTED
	, PRC.GM_ADJUSTED
	, PRC.TM_ADJUSTED
	, PRC.HOLD_CUR_PRICE
	, PRC.REVIEWED
	, cast(null as int) 'HOLD_CUR_HDR'
	, cast(null as int) 'REVIEWED_HDR'
	, Case when C.product_structure_fk is not null then 1 else Null end 'CIF'
	, PRC.UPDATE_USER_PRICE
	, PRC.UPDATE_USER_SET_TM
	, PRC.UPDATE_USER_HOLD
	, PRC.UPDATE_USER_REVIEWED
	--. prc.PRICE_MULTIPLE_NEW
	, isnull(prc.PRICE_MULTIPLE_NEW, 1) 'PRICE_MULTIPLE_NEW'
	, prc.ind_Parity
	, prc.ind_Margin_Tol
	, ISNULL(prc.ind_Rescind, 0) 'ind_rescind'
	, 0 'force_rescind_display'
	, prc.ZONE 'zone_number'
	, Case	when prc.multiple_parities is not null then ' Parity to ' + prc.multiple_parities + ' ' 
			when prc.related_item_alias_id is not null then ' Parity to ' + cast(prc.related_item_alias_id as varchar(64)) + ': ' + isnull(prc.parity_parent_item, prc.parity_child_item) --+ ' '
			when prc.parity_alias_id is not null then ' Parity to ' + cast(prc.parity_alias_id as varchar(64)) --+ ' ' 
			when isnull(prc.parity_parent_item, prc.parity_child_item) is not null then ' Parity to ' + isnull(prc.parity_parent_item, prc.parity_child_item) --+ ' '
		else '' end
		+ case when coalesce(Cast(prc.multiple_parities as varchar(8)), Cast(prc.related_item_alias_id as varchar(8)), Cast(prc.parity_alias_id as varchar(8))) is not null
			and coalesce(Cast(prc.ftn_Breakdown_Parent as varchar(8)), Cast(prc.ftn_cost_delta as varchar(8))) is not null then '; ' else '' end --+ ' '
		+ isnull(case when prc.ftn_cost_delta is not null and prc.ftn_Breakdown_Parent is not null then Cast(prc.ftn_Breakdown_Parent as varchar(8)) + '; '+  Cast(prc.ftn_cost_delta as varchar(8))
		else coalesce(Cast(prc.ftn_Breakdown_Parent as varchar(8)), Cast(prc.ftn_cost_delta as varchar(8))) end, '') 
		'item_superscript'
	, prc.multiple_parities
	, prc.parity_alias_id 'parity_alias_fk'
	, prc.ftn_cost_delta
	, prc.ftn_Breakdown_Parent
	, prc.item_sort
	, prc.item_group
	, prc.item_sort_report
	, Cast(ascii(left(PRC.ZONE_NAME, 1)) as varchar(16)) + right('000' + ltrim(rtrim(reverse(left(Reverse(PRC.ZONE_NAME), charindex(' ', Reverse(PRC.ZONE_NAME)) - 1)))), 3) + '1' 'Zone_Sort'
	, prc.ind_display_as_reg
	, prc.parity_status
	, prc.ind_parity_status
	, prc.parity_parent_item
	, prc.parity_child_item
	, case when pa_500012.product_attribute_id is not null then '{"renderer":{"color":"red","text-decoration":"underline"}}' else null end 'ind_discontinued'
	, prc.price_change_effective_date
	, case when prc.COST_CHANGE_REASON like '%new to zone%' then 1 else 0 end 'NTZ'
	, cast(null as Numeric(18, 4)) 'cost_change_grp'
	, prc.related_item_alias_id
from marginmgr.pricesheet_retail_changes prc
inner join #DisplayData dd on prc.alias_id = dd.alias_id and prc.ZONE_ENTITY_STRUCTURE_FK = dd.ZONE_ENTITY_STRUCTURE_FK and prc.PRICE_MGMT_TYPE_CR_FK <> dd.PRICE_MGMT_TYPE_CR_FK and prc.ITEM_CHANGE_TYPE_CR_FK = dd.ITEM_CHANGE_TYPE_CR_FK
	left join epacube.product_attribute pa_500012 with (nolock) on prc.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
	left join #Competitors C on prc.PRODUCT_STRUCTURE_FK = C.product_structure_fk
where 1 = 1
and prc.PRICE_CHANGE_EFFECTIVE_DATE = dd.PRICE_CHANGE_EFFECTIVE_DATE
and prc.alias_id in 
	(	select prc1.alias_id 
		from marginmgr.pricesheet_retail_changes prc1
		where 1 = 1
		and prc1.PRICE_CHANGE_EFFECTIVE_DATE = prc.PRICE_CHANGE_EFFECTIVE_DATE
		and prc1.ZONE_ENTITY_STRUCTURE_FK = prc.ZONE_ENTITY_STRUCTURE_FK
		and prc1.ITEM_CHANGE_TYPE_CR_FK = 656
		and prc1.PRICE_MGMT_TYPE_CR_FK <> prc.PRICE_MGMT_TYPE_CR_FK
	)
and prc.pricesheet_retail_changes_id not in (select pricesheet_retail_changes_id from #DisplayData)

Update DD
Set zone_name = dd.zone_name + ': LG = ''N'''
from #DisplayData DD
inner join 
	(
	select DDN.pricesheet_retail_changes_id 'PRID'
	from #DisplayData DDN
	inner join #DisplayData DDY on	DDN.alias_id = DDY.alias_ID
								and ddn.ZONE_ENTITY_STRUCTURE_FK = ddy.ZONE_ENTITY_STRUCTURE_FK
								and ddn.ITEM_CHANGE_TYPE_CR_FK = ddy.ITEM_CHANGE_TYPE_CR_FK
								and ddn.PRICE_MGMT_TYPE_CR_FK = 651
								and ddy.PRICE_MGMT_TYPE_CR_FK = 650
	Where DDN.ITEM_CHANGE_TYPE_CR_FK = 656
	) DD1 on DD.PRICESHEET_RETAIL_CHANGES_ID = DD1.PRID

Update DD
Set zone_name = dd.zone_name + ': LG = ''Y'''
from #DisplayData DD
inner join 
	(
	select DDY.pricesheet_retail_changes_id 'PRID'
	from #DisplayData DDN
	inner join #DisplayData DDY on	DDN.alias_id = DDY.alias_ID
								and ddn.ZONE_ENTITY_STRUCTURE_FK = ddy.ZONE_ENTITY_STRUCTURE_FK
								and ddn.ITEM_CHANGE_TYPE_CR_FK = ddy.ITEM_CHANGE_TYPE_CR_FK
								and ddn.PRICE_MGMT_TYPE_CR_FK = 651
								and ddy.PRICE_MGMT_TYPE_CR_FK = 650
	Where DDN.ITEM_CHANGE_TYPE_CR_FK = 656
	) DD1 on DD.PRICESHEET_RETAIL_CHANGES_ID = DD1.PRID

If @Qualified_Status_CR_FK <> 0
	Begin
		UPDATE DD1
		SET REVIEWED_HDR = 1
		from #DisplayData DD1
		where isnull(alias_id, related_item_alias_id) in
						(
							select isnull(alias_id, related_item_alias_id)
							from #DisplayData DD
							where 1 = 1
							and (ITEM_CHANGE_TYPE_CR_FK = 656 or related_item_alias_id is not null)
							and QUALIFIED_STATUS_CR_FK = @Qualified_Status_CR_FK
							group by isnull(alias_id, related_item_alias_id)
							having count(*) = (	select count(*) 
												from #DisplayData where isnull(reviewed, 0) = 1 and isnull(alias_id, related_item_alias_id) = isnull(DD.alias_id, DD.related_item_alias_id) and QUALIFIED_STATUS_CR_FK = @Qualified_Status_CR_FK)
						)

		UPDATE DD1
		SET HOLD_CUR_HDR = 1
		from #DisplayData DD1
		where isnull(alias_id, related_item_alias_id) in
						(
							select isnull(alias_id, related_item_alias_id)
							from #DisplayData DD
							where 1 = 1
							and (ITEM_CHANGE_TYPE_CR_FK = 656 or related_item_alias_id is not null)
							and QUALIFIED_STATUS_CR_FK = @Qualified_Status_CR_FK
							group by isnull(alias_id, related_item_alias_id)
							having count(*) = (select count(*) 
												from #DisplayData where isnull(HOLD_CUR_PRICE, 0) = 1 and isnull(alias_id, related_item_alias_id) = isnull(DD.alias_id, dd.related_item_alias_id) and QUALIFIED_STATUS_CR_FK = @Qualified_Status_CR_FK)
						)

		UPDATE DD1
		SET REVIEWED_HDR = 1
		from #DisplayData DD1
		where product_structure_fk in
						(
							select product_structure_fk
							from #DisplayData DD
							where 1 = 1
							and ITEM_CHANGE_TYPE_CR_FK in (655, 661)
							and QUALIFIED_STATUS_CR_FK = @Qualified_Status_CR_FK
							group by product_structure_fk
							having count(*) = (select count(*) from #DisplayData where isnull(reviewed, 0) = 1 and product_structure_fk = DD.product_structure_fk and QUALIFIED_STATUS_CR_FK = @Qualified_Status_CR_FK)
						)

		UPDATE DD1
		SET HOLD_CUR_HDR = 1
		from #DisplayData DD1
		where product_structure_fk in
						(
							select product_structure_fk
							from #DisplayData DD
							where 1 = 1
							and ITEM_CHANGE_TYPE_CR_FK in (655, 661)
							and QUALIFIED_STATUS_CR_FK = @Qualified_Status_CR_FK
							group by product_structure_fk
							having count(*) = (select count(*) from #DisplayData where isnull(HOLD_CUR_PRICE, 0) = 1 and product_structure_fk = DD.product_structure_fk and QUALIFIED_STATUS_CR_FK = @Qualified_Status_CR_FK)
						)
	End
	else
	Begin
		UPDATE DD1
		SET REVIEWED_HDR = 1
		from #DisplayData DD1
		where isnull(alias_id, related_item_alias_id) in
						(
							select isnull(alias_id, related_item_alias_id)
							from #DisplayData DD
							where 1 = 1
							and (ITEM_CHANGE_TYPE_CR_FK = 656 or related_item_alias_id is not null)
							group by isnull(alias_id, related_item_alias_id)
							having count(*) = (	select count(*) 
												from #DisplayData where isnull(reviewed, 0) = 1 and isnull(alias_id, related_item_alias_id) = isnull(DD.alias_id, DD.related_item_alias_id))
						)

		UPDATE DD1
		SET HOLD_CUR_HDR = 1
		from #DisplayData DD1
		where isnull(alias_id, related_item_alias_id) in
						(
							select isnull(alias_id, related_item_alias_id)
							from #DisplayData DD
							where 1 = 1
							and (ITEM_CHANGE_TYPE_CR_FK = 656 or related_item_alias_id is not null)
							group by isnull(alias_id, related_item_alias_id)
							having count(*) = (select count(*) 
												from #DisplayData where isnull(HOLD_CUR_PRICE, 0) = 1 and isnull(alias_id, related_item_alias_id) = isnull(DD.alias_id, dd.related_item_alias_id))
						)

		UPDATE DD1
		SET REVIEWED_HDR = 1
		from #DisplayData DD1
		where product_structure_fk in
						(
							select product_structure_fk
							from #DisplayData DD
							where 1 = 1
							and ITEM_CHANGE_TYPE_CR_FK in (655, 661)
							group by product_structure_fk
							having count(*) = (select count(*) from #DisplayData where isnull(reviewed, 0) = 1 and product_structure_fk = DD.product_structure_fk)
						)

		UPDATE DD1
		SET HOLD_CUR_HDR = 1
		from #DisplayData DD1
		where product_structure_fk in
						(
							select product_structure_fk
							from #DisplayData DD
							where 1 = 1
							and ITEM_CHANGE_TYPE_CR_FK in (655, 661)
							group by product_structure_fk
							having count(*) = (select count(*) from #DisplayData where isnull(HOLD_CUR_PRICE, 0) = 1 and product_structure_fk = DD.product_structure_fk)
						)
	End
	
	Update DD
	Set size_uom = replace(adj.size_uom, '.00', '')
	from #DisplayData DD
	inner join
			(	select alias_id, left(min_size, charindex(' ', min_size) - 1) + ' - ' + max_size 'size_uom' 
				from (
					select min(size_uom) 'min_size', max(size_uom) 'max_size', alias_id
					from #DisplayData
					group by alias_id
					having min(size_uom) <> max(size_uom)
					) A
			) adj on dd.alias_id = adj.alias_id
	where item_change_type_cr_fk in (656, 662)

	Update DD
	Set store_pack = adj.store_pack
	from #DisplayData DD
	inner join
			(	select alias_id, cast(min_pack as varchar(8)) + ' - ' + cast(max_pack as VARCHAR(8)) 'store_pack' 
				from (
					select min(cast(STORE_PACK as int)) 'min_pack', max(cast(STORE_PACK as int)) 'max_pack', alias_id
					from #DisplayData
					group by alias_id
					having min(STORE_PACK) <> max(STORE_PACK)
					) A
			) adj on dd.alias_id = adj.alias_id
	where item_change_type_cr_fk in (656, 662)

--Set cost_chane status for header records to use when multiple zones are selected
	Update dd
	set cost_change_grp = adj.cost_change
	from #DisplayData dd
	inner join
							(select DD1.alias_id, case when DD1.NTZ = 1 then 0.0001 else DD1.cost_change end 'cost_change'
							from #DisplayData DD1
							where alias_id is not null
							and item_change_type_cr_fk in (656, 662)
							group by DD1.alias_id, case when DD1.NTZ = 1 then 0.0001 else DD1.cost_change end
							) adj on dd.alias_id = adj.alias_id
	where 1 = 1
	and dd.alias_id is not null

	Update dd
	set cost_change_grp = adj.cost_change
	from #DisplayData dd
	inner join
							(select DD1.PRODUCT_STRUCTURE_FK, case when DD1.NTZ = 1 then 0.0001 else DD1.cost_change end 'cost_change'
							from #DisplayData DD1
							where dd1.alias_id is null
							group by DD1.PRODUCT_STRUCTURE_FK, case when DD1.NTZ = 1 then 0.0001 else DD1.cost_change end
							) adj on dd.PRODUCT_STRUCTURE_FK = adj.PRODUCT_STRUCTURE_FK
	where 1 = 1
	and dd.alias_id is null

--Get consistent competitor icons for aliases
	Update dd
	set CIF = 1
	from #DisplayData dd
	where 1 = 1
	and dd.alias_id in (select alias_id from #DisplayData dd where isnull(CIF, 0) = 1)
	and ITEM_CHANGE_TYPE_CR_FK = 656

	update dd
	set item_group = adj.item_group_new
	from #DisplayData dd
	inner join
		(
			select item_group, rank()over(order by item_group) * 10 'item_group_new'
			from #DisplayData 
			where isnull(ind_rescind, 0) <> 0
			or item in (select parity_parent_item from #DisplayData where isnull(ind_rescind, 0) <> 0)
			or item in (select parity_child_item from #DisplayData where isnull(ind_rescind, 0) <> 0)
			group by item_group
		) adj on dd.item_group = adj.item_group



	update dd
	Set Item_Group = adj.item_group + 1
	from #DisplayData dd
	inner join
		(select item_group, alias_id, parity_alias_fk from #DisplayData where isnull(ind_rescind, 0) <> 0 and parity_alias_fk is not null group by item_group, alias_id, parity_alias_fk) adj on dd.alias_id = adj.parity_alias_fk

	Update DD
	set force_rescind_display = isnull((select max(ind_rescind) from #DisplayData where isnull(ind_rescind, 0) <> 0 and item_group = dd.item_group), 0)
	from #DisplayData DD

	Update DD
	set ind_rescind = case when ITEM_CHANGE_TYPE_CR_FK in (656, 662) then 2 else ind_rescind end
	from #DisplayData DD
	where force_rescind_display = 2

	create index idx_05 on #DisplayData(QUALIFIED_STATUS_CR_FK, alias_id, product_structure_fk)

	--update dd
	--set PRICE_MGMT_TYPE_CR_FK = null
	--, QUALIFIED_STATUS_CR_FK = null
	--from #DisplayData dd

	If @ZoneCount = 1
	Begin
	Select * 
	from (
		Select 
		DENSE_RANK()over(partition by Node_Level, Node_Column_Value, node_column_value_parent, case when node_level in (2, 4) then product_structure_fk else null end 
			order by case when node_level in (1, 3) then isnull(new_cost, cost_change) when node_level = 4 then product_structure_fk else 0 end desc, PRICE_MGMT_TYPE_CR_FK, price_new desc, case node_level when 3 then PRICE_MGMT_TYPE_CR_FK else null end) DRank

		, * from (
			Select 
			1 'NODE_LEVEL'
			, cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE'
			, NULL 'NODE_COLUMN_VALUE_PARENT'
			, PRC.Alias_ID 'NODE_SORT'
			, PRICESHEET_RETAIL_CHANGES_ID
			, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
			, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
			, prc.ITEM_CHANGE_TYPE_CR_FK 'COMPETITOR_CALL_TYPE_CR_FK'
			, case when @Report_Only = 1 and @ind_margin_changes = 1 then 0 when @Report_Only = 1 then prc.qualified_status_cr_fk else null end 'QUALIFIED_STATUS_CR_FK'
			, prc.PRODUCT_STRUCTURE_FK
			, prc.ZONE_ENTITY_STRUCTURE_FK
			, prc.ZONE
			, prc.ZONE_NAME
			, PRC.SUBGROUP
			, PRC.SUBGROUP_DESCRIPTION
			, PRC.SUBGROUP_DATA_VALUE_FK
			, PRC.Alias_ID	'ITEM'
			, '' 'ITEM_SEARCH'
			, PRC.Alias_ID + ' ' + PRC.ALIAS_DESCRIPTION 'ALIAS_SEARCH'
			, SUBGROUP_SEARCH
			, PRC.ALIAS_DESCRIPTION 'ITEM_DESCRIPTION'
			, PRC.STORE_PACK
			, PRC.SIZE_UOM
			, PRC.CUR_COST
			, PRC.CUR_CASE_COST
			, PRC.NEW_CASE_COST
			, Case PRC.CMULT when 1 then Null else prc.cmult end 'CMULT'
			, PRC.CRETAIL
			, PRC.PRICE_CUR
			, PRC.CUR_GM_PCT
			, case when isnull(prc.NTZ, '') <> '' then isnull(prc.COST_CHANGE, 0.0001) else prc.COST_CHANGE end 'COST_CHANGE'
			, PRC.PRICE_CHANGE
			, PRC.NEW_COST
			, PRC.PRICE_NEW
			, PRC.NEW_GM_PCT
			, PRC.TARGET_MARGIN
			, PRC.TM_SOURCE
			, PRC.SRP_ADJUSTED
			, PRC.GM_ADJUSTED
			, PRC.TM_ADJUSTED
			, PRC.HOLD_CUR_PRICE
			, PRC.REVIEWED
			, prc.CIF --Null 'CIF'
			, PRC.UPDATE_USER_PRICE
			, PRC.UPDATE_USER_SET_TM
			, PRC.UPDATE_USER_HOLD
			, PRC.UPDATE_USER_REVIEWED
			, case when prc.PRICE_MULTIPLE_NEW = 1 then Null else prc.PRICE_MULTIPLE_NEW end 'PRICE_MULTIPLE_NEW'
			, prc.ind_Parity
			, ind_Margin_Tol
			, prc.force_rescind_display 'ind_Rescind'
			, prc.Zone_Number
			, item_superscript
			, prc.item_sort
			, prc.item_group 'item_group'
			, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort_report'
			, prc.Zone_Sort
			, 1 'ind_bold'
			, 'Alias Header' 'comment'
			, null 'ind_discontinued'
			, prc.price_change_effective_date
			, PRC.PRICESHEET_RETAIL_CHANGES_ID 'ind_launch_audit'
			from #DisplayData prc
			WHERE prc.ITEM_CHANGE_TYPE_CR_FK IN (656, 662)

		UNION

			Select 
			1 'NODE_LEVEL'
			, cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE'
			, NULL 'NODE_COLUMN_VALUE_PARENT'
			, PRC.Alias_ID 'NODE_SORT'
			, PRICESHEET_RETAIL_CHANGES_ID
			, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
			, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
			, prc.ITEM_CHANGE_TYPE_CR_FK 'COMPETITOR_CALL_TYPE_CR_FK'
			, case when @Report_Only = 1 and @ind_margin_changes = 1 then 0 when @Report_Only = 1 then prc.qualified_status_cr_fk else null end 'QUALIFIED_STATUS_CR_FK'
			, PRC.PRODUCT_STRUCTURE_FK
			, prc.ZONE_ENTITY_STRUCTURE_FK
			, prc.ZONE
			, prc.ZONE_NAME
			, PRC.SUBGROUP
			, PRC.SUBGROUP_DESCRIPTION
			, PRC.SUBGROUP_DATA_VALUE_FK
			, PRC.ITEM	'ITEM'
			, PRC.ITEM + ' ' + PRC.ITEM_DESCRIPTION 'ITEM_SEARCH'
			, PRC.Alias_ID + ' ' + PRC.ALIAS_DESCRIPTION 'ALIAS_SEARCH'
			, SUBGROUP_SEARCH
			, PRC.ITEM_DESCRIPTION 'ITEM_DESCRIPTION'
			, PRC.STORE_PACK
			, PRC.SIZE_UOM
			, PRC.CUR_COST
			, PRC.CUR_CASE_COST
			, PRC.NEW_CASE_COST
			, Case PRC.CMULT when 1 then Null else prc.cmult end 'CMULT'
			, PRC.CRETAIL
			, PRC.PRICE_CUR
			, PRC.CUR_GM_PCT
			, case when isnull(prc.NTZ, '') <> '' then isnull(prc.COST_CHANGE, 0.0001) else prc.COST_CHANGE end 'COST_CHANGE'
			, PRC.PRICE_CHANGE
			, PRC.NEW_COST
			, PRC.PRICE_NEW
			, PRC.NEW_GM_PCT
			, PRC.TARGET_MARGIN
			, PRC.TM_SOURCE
			, PRC.SRP_ADJUSTED
			, PRC.GM_ADJUSTED
			, PRC.TM_ADJUSTED
			, prc.HOLD_CUR_PRICE
			, prc.REVIEWED
			, prc.CIF
			, PRC.UPDATE_USER_PRICE
			, PRC.UPDATE_USER_SET_TM
			, PRC.UPDATE_USER_HOLD
			, PRC.UPDATE_USER_REVIEWED
			, case when prc.PRICE_MULTIPLE_NEW = 1 then Null else prc.PRICE_MULTIPLE_NEW end 'PRICE_MULTIPLE_NEW'
			, prc.ind_Parity
			, prc.ind_Margin_Tol
			, prc.ind_Rescind
			, prc.Zone_Number
			, item_superscript
			, prc.item_sort
			, prc.item_group 'item_group'
			, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort_report'
			, 0 'Zone_Sort'
			, 1 'ind_bold'
			, 'Item Headers' 'comment'
			, prc.ind_discontinued
			, prc.price_change_effective_date
			, PRC.PRICESHEET_RETAIL_CHANGES_ID 'ind_launch_audit'
			from #DisplayData prc
			WHERE prc.ITEM_CHANGE_TYPE_CR_FK NOT IN (656, 662)
			and prc.alias_id is null

		Union
		----Get Items listing
			Select distinct
			3 'NODE_LEVEL'
			, cast(PRC.item_group as varchar(16)) + '.03' 'NODE_COLUMN_VALUE'
			, cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE_PARENT'
			, PRC.Alias_ID 'NODE_SORT'
			, Null 'PRICESHEET_RETAIL_CHANGES_ID'
			, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
			, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
			, Null 'COMPETITOR_CALL_TYPE_CR_FK'
			, case when @Report_Only = 1 and @ind_margin_changes = 1 then 0 when @Report_Only = 1 then prc.qualified_status_cr_fk else null end 'QUALIFIED_STATUS_CR_FK'
			, PRC.PRODUCT_STRUCTURE_FK
			, Null 'ZONE_ENTITY_STRUCTURE_FK'
			, Null  'ZONE'
			, Null 'ZONE_NAME'
			, NULL 'SUBGROUP'
			, NULL 'SUBGROUP_DESCRIPTION'
			, NULL 'SUBGROUP_DATA_VALUE_FK'
			, 'Item Listing' 'ITEM'
			, Null 'ITEM_SEARCH'
			, '' 'ALIAS_SEARCH'
			, '' 'SUBGROUP_SEARCH'
			, Null 'ITEM_DESCRIPTION'
			, Null 'STORE_PACK'
			, Null 'SIZE_UOM'
			, Null 'CUR_COST'
			, Null 'CUR_CASE_COST'
			, Null 'New_CASE_COST'
			, Null 'CMULT'
			, Null 'CRETAIL'
			, Null PRICE_CUR
			, Null CUR_GM_PCT
			, case when isnull(prc.NTZ, '') <> '' then isnull(prc.COST_CHANGE, 0.0001) else prc.COST_CHANGE end 'COST_CHANGE'
			, Null PRICE_CHANGE
			, Null 'NEW_COST'
			, Null PRICE_NEW
			, Null NEW_GM_PCT
			, Null TARGET_MARGIN
			, Null TM_SOURCE
			, Null SRP_ADJUSTED
			, Null GM_ADJUSTED
			, Null TM_ADJUSTED
			, Null HOLD_CUR_PRICE
			, Null REVIEWED
			, NULL 'CIF'
			, Null UPDATE_USER_PRICE
			, Null UPDATE_USER_SET_TM
			, Null UPDATE_USER_HOLD
			, Null UPDATE_USER_REVIEWED
			, Null 'PRICE_MULTIPLE_NEW'
			, Null ind_Parity
			, Null 'Margin_Tol'
			, prc.ind_Rescind
			, '' 'Zone_Number'
			, case when isnull(prc.ftn_cost_delta, '') <> '' then ' ' + prc.ftn_cost_delta else '' end
			, prc.item_sort
			, prc.item_group 'item_group'
			, 30 'item_sort_report'
			, Null 'Zone_Sort'
			, 0 'ind_bold'
			, 'Zone Details' 'comment'
			, Null 'ind_discontinued'
			, prc.price_change_effective_date
			, Null 'ind_launch_audit'
			from #DisplayData prc
			WHERE 1 = 1
			and prc.item_change_type_cr_fk in (656, 662)

		Union

		----Get Items 
			Select 
			4 'NODE_LEVEL'
			, cast(PRC.item_group as varchar(16)) + '.04' 'NODE_COLUMN_VALUE'
			, cast(PRC.item_group as varchar(16)) + '.03' 'NODE_COLUMN_VALUE_PARENT'
			, PRC.Alias_ID 'NODE_SORT'
			, Null 'PRICESHEET_RETAIL_CHANGES_ID'
			, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
			, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
			, prc.ITEM_CHANGE_TYPE_CR_FK 'COMPETITOR_CALL_TYPE_CR_FK'
			, case when @Report_Only = 1 and @ind_margin_changes = 1 then 0 when @Report_Only = 1 then prc.qualified_status_cr_fk else null end 'QUALIFIED_STATUS_CR_FK'
			, PRC.PRODUCT_STRUCTURE_FK
			, Null 'ZONE_ENTITY_STRUCTURE_FK'
			, Null  'ZONE'
			, Null 'ZONE_NAME'
			, NULL 'SUBGROUP'
			, NULL 'SUBGROUP_DESCRIPTION'
			, NULL 'SUBGROUP_DATA_VALUE_FK'
			, Item 'ITEM'
			, Item 'ITEM_SEARCH'
			, '' 'ALIAS_SEARCH'
			, '' 'SUBGROUP_SEARCH'
			, PRC.ITEM_DESCRIPTION 'ITEM_DESCRIPTION'
			, STORE_PACK 'STORE_PACK'
			, SIZE_UOM 'SIZE_UOM'
			, prc.CUR_COST
			, prc.CUR_CASE_COST
			, prc.NEW_CASE_COST
			, Null 'CMULT'
			, Null 'CRETAIL'
			, Null PRICE_CUR
			, Null CUR_GM_PCT
			, case when isnull(prc.NTZ, '') <> '' then isnull(prc.COST_CHANGE, 0.0001) else prc.COST_CHANGE end 'COST_CHANGE'
			, Null PRICE_CHANGE
			, prc.NEW_COST
			, Null PRICE_NEW
			, Null NEW_GM_PCT
			, Null TARGET_MARGIN
			, Null TM_SOURCE
			, Null SRP_ADJUSTED
			, Null GM_ADJUSTED
			, Null TM_ADJUSTED
			, Null HOLD_CUR_PRICE
			, Null REVIEWED
			, prc.CIF --Null 'CIF'
			, Null UPDATE_USER_PRICE
			, Null UPDATE_USER_SET_TM
			, Null UPDATE_USER_HOLD
			, Null UPDATE_USER_REVIEWED
			, Null 'PRICE_MULTIPLE_NEW'
			, prc.ind_Parity
			, Null 'Margin_Tol'
			, prc.ind_Rescind
			, '' 'Zone_Number'
			, isnull(Case	when isnull(parity_parent_item, parity_child_item) is not null then ' Parity to ' + isnull(parity_parent_item, parity_child_item) end, '') 'item_superscript'
			, prc.item_sort
			, prc.item_group 'item_group'
			, prc.item_sort 'item_sort_report'
			, Null 'Zone_Sort'
			, 0 'ind_bold'
			, 'Get list of Items' 'comment'
			, prc.ind_discontinued
			, prc.price_change_effective_date
			, PRC.PRICESHEET_RETAIL_CHANGES_ID 'ind_launch_audit'
			from #DisplayData prc
			WHERE 1 = 1
			and prc.item_change_type_cr_fk not in (656, 662)
			and prc.alias_id is not null

			Union

			Select 
			case when alias_id is null then 2 else 5 end 'NODE_LEVEL'
			, cast(PRC.item_group as varchar(16)) + case when alias_id is null then '.00.0' else '.04' end 'NODE_COLUMN_VALUE'
			, cast(PRC.item_group as varchar(16)) + case when prc.alias_id is null then '.00' else '.03' end 'NODE_COLUMN_VALUE_PARENT'
			, ALIAS_ID 'NODE_SORT'
			, NULL 'PRICESHEET_RETAIL_CHANGES_ID'
			, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
			, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
			, Null 'COMPETITOR_CALL_TYPE_CR_FK'
			, case when @Report_Only = 1 and @ind_margin_changes = 1 then 0 when @Report_Only = 1 then prc.qualified_status_cr_fk else null end 'QUALIFIED_STATUS_CR_FK'
			, PB.PRODUCT_STRUCTURE_FK
			, Null ZONE_ENTITY_STRUCTURE_FK
			, Null ZONE
			, Null ZONE_NAME
			, Null 'SUBGROUP'
			, Null SUBGROUP_DESCRIPTION
			, Null SUBGROUP_DATA_VALUE_FK
			, pi.value + ' Qty: ' + Cast(Cast(pb.Qty as int) as varchar(8)) 'ITEM'
			, Null 'ITEM_SEARCH'
			, Null 'ALIAS_SEARCH'
			, Null 'SUBGROUP_SEARCH'
			, ltrim(ltrim(rtrim(Case when isnull(PB.State, '') = '' then '' else 'State: ' + ltrim(rtrim(PB.State)) + ' ' end)) + pd.description) 'item_description'
			, ISNULL(pa_500001.attribute_event_data, '''') 'STORE_PACK'
			, ISNULL(pa_500002.attribute_event_data, '''') 'SIZE_UOM'
			, Null 'CUR_COST'
			, Null 'CUR_CASE_COST'
			, Null 'NEW_CASE_COST'
			, Null 'CMULT'
			, Null 'CRETAIL'
			, Null 'PRICE_CUR'
			, Null 'CUR_GM_PCT'
			, Null 'COST_CHANGE'
			, Null 'PRICE_CHANGE'
			, Null 'NEW_COST'
			, Null 'PRICE_NEW'
			, Null 'NEW_GM_PCT'
			, Null 'TARGET_MARGIN'
			, Null 'TM_SOURCE'
			, Null 'SRP_ADJUSTED'
			, Null 'GM_ADJUSTED'
			, Null 'TM_ADJUSTED'
			, Null 'HOLD_CUR_PRICE'
			, Null 'REVIEWED'
			, Null 'CIF'
			, Null 'UPDATE_USER_PRICE'
			, Null 'UPDATE_USER_SET_TM'
			, Null 'UPDATE_USER_HOLD'
			, Null 'UPDATE_USER_REVIEWED'
			, Null 'PRICE_MULTIPLE_NEW'
			, NULL 'ind_Parity'
			, Null 'ind_Margin_Tol'
			, Null 'ind_Rescind'
			, Null 'zone_number'
			, null 'item_superscript'
			, prc.item_sort + 1 'item_sort'
			, prc.item_group 'item_group'
			, prc.item_sort + 1 'item_sort_report'
			, 0 'Zone_Sort'
			, null 'ind_bold'
			, 'get breakdown items' 'comment'
			, null 'ind_discontinued'
			, prc.price_change_effective_date
			, Null 'ind_launch_audit'
			FROM #DisplayData PRC with (nolock)
			inner join #Zones Z on prc.zone_entity_structure_fk = Z.zone_entity_Structure_FK
			inner join epacube.PRODUCT_BOM PB on prc.product_structure_fk = pb.product_structure_fk and pb.data_name_fk = 500213
			inner join epacube.PRODUCT_IDENTIFICATION pi with (nolock) on pb.child_product_structure_fk = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110100
			inner join epacube.product_description pd with (nolock) on pi.PRODUCT_STRUCTURE_FK = pd.PRODUCT_STRUCTURE_FK and pd.DATA_NAME_FK = 110401
			left join [epacube].[PRODUCT_ATTRIBUTE] pa_500001 with (nolock) on pi.Product_Structure_FK = pa_500001.PRODUCT_STRUCTURE_FK and pa_500001.data_name_fk = 500001
			left join [epacube].[PRODUCT_ATTRIBUTE] pa_500002 with (nolock) on pi.Product_Structure_FK = pa_500002.PRODUCT_STRUCTURE_FK and pa_500002.data_name_fk = 500002
			where 1 = 1
			and @Report_Only = 1
		) A
	) B Where 1 = 1
		and DRank = 1
		Order by cast(left(node_column_value, charindex('.', node_column_value) - 1) as bigint), node_column_value, item_sort, zone_sort desc

	End
	Else
	Begin
	----Get Alias Headers
	Select * 
	from (
		Select 
		--DENSE_RANK()over(partition by Node_Level, Node_Column_Value, Node_Sort order by isnull(new_cost, cost_change) desc, price_new desc, PRICE_MGMT_TYPE_CR_FK, Qualified_Status_CR_FK) DRank\
		DENSE_RANK()over(partition by Node_Level, Node_Column_Value, node_column_value_parent, case when node_level in (2, 4) then product_structure_fk else null end 
			order by case when node_level in (1, 3) then isnull(new_cost, cost_change) when node_level = 4 then product_structure_fk else 0 end, PRICE_MGMT_TYPE_CR_FK, price_new desc, case node_level when 3 then PRICE_MGMT_TYPE_CR_FK else null end) DRank
		, * 
		from (
			Select 
			1 'NODE_LEVEL'
			, cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE'
			, NULL 'NODE_COLUMN_VALUE_PARENT'
			, PRC.Alias_ID 'NODE_SORT'
			, NULL 'PRICESHEET_RETAIL_CHANGES_ID'
			, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
			, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
			, prc.ITEM_CHANGE_TYPE_CR_FK 'COMPETITOR_CALL_TYPE_CR_FK'
			, case when @Report_Only = 1 and @ind_margin_changes = 1 then 0 when @Report_Only = 1 then qualified_status_cr_fk else null end 'QUALIFIED_STATUS_CR_FK'

			, (select top 1 PRODUCT_STRUCTURE_FK from #displaydata where alias_id = prc.alias_id and ITEM_CHANGE_TYPE_CR_FK not in (656, 662) order by item_sort) 'PRODUCT_STRUCTURE_FK'
			, NULL 'ZONE_ENTITY_STRUCTURE_FK'
			, NULL 'ZONE'
			, NULL 'ZONE_NAME'
			, PRC.SUBGROUP
			, PRC.SUBGROUP_DESCRIPTION
			, PRC.SUBGROUP_DATA_VALUE_FK
			, PRC.Alias_ID	'ITEM'
			, '' 'ITEM_SEARCH'
			, PRC.Alias_ID + ' ' + PRC.ALIAS_DESCRIPTION 'ALIAS_SEARCH'
			, SUBGROUP_SEARCH
			, PRC.ALIAS_DESCRIPTION 'ITEM_DESCRIPTION'
			, PRC.STORE_PACK
			, PRC.SIZE_UOM
			, NULL 'CUR_COST'
			, Null 'CUR_CASE_COST'
			, Null 'NEW_CASE_COST'
			, Null 'CMULT'
			, Null 'CRETAIL'
			, NULL 'PRICE_CUR'
			, NULL 'CUR_GM_PCT'
			, prc.cost_change_grp 'COST_CHANGE'
			, NULL 'PRICE_CHANGE'
			, NULL 'NEW_COST'
			, NULL 'PRICE_NEW'
			, NULL 'NEW_GM_PCT'
			, NULL 'TARGET_MARGIN'
			, NULL 'TM_SOURCE'
			, NULL 'SRP_ADJUSTED'
			, NULL 'GM_ADJUSTED'
			, NULL 'TM_ADJUSTED'
			, prc.HOLD_CUR_HDR 'HOLD_CUR_PRICE'
			, prc.REVIEWED_HDR 'REVIEWED'
			, prc.CIF
			, NULL 'UPDATE_USER_PRICE'
			, NULL 'UPDATE_USER_SET_TM'
			, NULL 'UPDATE_USER_HOLD'
			, NULL 'UPDATE_USER_REVIEWED'
			, NULL 'PRICE_MULTIPLE_NEW'
			, prc.ind_Parity
			, Null 'ind_Margin_Tol'
			, prc.force_rescind_display 'ind_Rescind'
			, '' 'Zone_Number'
			, item_superscript
			, prc.item_sort
			, prc.item_group 'item_group'
			, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort_report'
			, 0 'Zone_Sort'
			, 1 'ind_bold'
			, 'Alias Headers' 'comment'
			, null 'ind_discontinued'
			, prc.price_change_effective_date
			, Null 'ind_launch_audit'
			from #DisplayData prc
			WHERE 1 = 1
			and prc.ITEM_CHANGE_TYPE_CR_FK IN (656, 662)

		UNION
		----Get Item Headers
			Select 
			1 'NODE_LEVEL'
			, cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE'
			, NULL 'NODE_COLUMN_VALUE_PARENT'
			, PRC.Alias_ID 'NODE_SORT'
			, NULL 'PRICESHEET_RETAIL_CHANGES_ID'
			, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
			, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
			, prc.ITEM_CHANGE_TYPE_CR_FK 'COMPETITOR_CALL_TYPE_CR_FK'
			, case when @Report_Only = 1 and @ind_margin_changes = 1 then 0 when @Report_Only = 1 then qualified_status_cr_fk else null end 'QUALIFIED_STATUS_CR_FK'
			, PRC.PRODUCT_STRUCTURE_FK
			, NULL 'ZONE_ENTITY_STRUCTURE_FK'
			, NULL 'ZONE'
			, NULL 'ZONE_NAME'
			, PRC.SUBGROUP
			, PRC.SUBGROUP_DESCRIPTION
			, PRC.SUBGROUP_DATA_VALUE_FK
			, PRC.ITEM	'ITEM'
			, PRC.ITEM + ' ' + PRC.ITEM_DESCRIPTION 'ITEM_SEARCH'
			, PRC.Alias_ID + ' ' + PRC.ALIAS_DESCRIPTION 'ALIAS_SEARCH'
			, SUBGROUP_SEARCH
			, PRC.ITEM_DESCRIPTION 'ITEM_DESCRIPTION'
			, PRC.STORE_PACK
			, PRC.SIZE_UOM
			, NULL 'CUR_COST'
			, Null 'CUR_CASE_COST'
			, Null 'NEW_CASE_COST'
			, Null 'CMULT'
			, Null 'CRETAIL'
			, NULL 'PRICE_CUR'
			, NULL 'CUR_GM_PCT'
			, prc.cost_change_grp 'COST_CHANGE'
			, NULL 'PRICE_CHANGE'
			, NULL 'NEW_COST'
			, NULL 'PRICE_NEW'
			, NULL 'NEW_GM_PCT'
			, NULL 'TARGET_MARGIN'
			, NULL 'TM_SOURCE'
			, NULL 'SRP_ADJUSTED'
			, NULL 'GM_ADJUSTED'
			, NULL 'TM_ADJUSTED'
			, prc.HOLD_CUR_HDR 'HOLD_CUR_PRICE'
			, prc.REVIEWED_HDR 'REVIEWED'

			, prc.CIF
			, NULL 'UPDATE_USER_PRICE'
			, NULL 'UPDATE_USER_SET_TM'
			, NULL 'UPDATE_USER_HOLD'
			, NULL 'UPDATE_USER_REVIEWED'
			, NULL 'PRICE_MULTIPLE_NEW'
			, prc.ind_Parity
			, Null 'ind_Margin_Tol'
			, prc.force_rescind_display 'ind_Rescind'
			, '' 'Zone_Number'
			, item_superscript
			, prc.item_sort
			, prc.item_group 'item_group'
			, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort_report'
			, 0 'Zone_Sort'
			, 1 'ind_bold'
			, 'Item Headers' 'comment'
			, prc.ind_discontinued
			, prc.price_change_effective_date
			, Null 'ind_launch_audit'
			from #DisplayData prc
			WHERE prc.ITEM_CHANGE_TYPE_CR_FK NOT IN (656, 662)
			and prc.alias_id is null

		Union
		----Get Zones and their details
			Select 
			2 'NODE_LEVEL'
			, Cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status + '.' + cast(8600000 - prc.zone_sort as varchar(24)) 'NODE_COLUMN_VALUE'
			,  cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE_PARENT'
			, PRC.Alias_ID 'NODE_SORT'
			, PRC.PRICESHEET_RETAIL_CHANGES_ID
			, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
			, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
			, Null 'COMPETITOR_CALL_TYPE_CR_FK'
			, case when @Report_Only = 1 and @ind_margin_changes = 1 then 0 when @Report_Only = 1 then qualified_status_cr_fk else null end 'QUALIFIED_STATUS_CR_FK'
			, PRC.PRODUCT_STRUCTURE_FK
			, PRC.ZONE_ENTITY_STRUCTURE_FK
			, PRC.ZONE 'ZONE'
			, PRC.ZONE_NAME 'ZONE_NAME'
			, NULL 'SUBGROUP'
			, NULL 'SUBGROUP_DESCRIPTION'
			, NULL 'SUBGROUP_DATA_VALUE_FK'
			, 'ZONE ' + PRC.ZONE 'ITEM'
			, '' 'ITEM_SEARCH'
			, '' 'ALIAS_SEARCH'
			, '' 'SUBGROUP_SEARCH'
			, PRC.ZONE_NAME 'ITEM_DESCRIPTION'
			, NULL 'STORE_PACK'
			, NULL 'SIZE_UOM'
			, PRC.CUR_COST
			, PRC.CUR_CASE_COST
			, PRC.NEW_CASE_COST
			, Case PRC.CMULT when 1 then Null else prc.cmult end 'CMULT'
			, PRC.CRETAIL
			, PRC.PRICE_CUR
			, PRC.CUR_GM_PCT
			, case when isnull(prc.NTZ, '') <> '' then isnull(prc.COST_CHANGE, 0.0001) else prc.COST_CHANGE end 'COST_CHANGE'
			, PRC.PRICE_CHANGE
			, PRC.NEW_COST
			, PRC.PRICE_NEW
			, PRC.NEW_GM_PCT
			, PRC.TARGET_MARGIN
			, PRC.TM_SOURCE
			, PRC.SRP_ADJUSTED
			, PRC.GM_ADJUSTED
			, PRC.TM_ADJUSTED
			, PRC.HOLD_CUR_PRICE
			, PRC.REVIEWED
			, NULL 'CIF'
			, PRC.UPDATE_USER_PRICE
			, PRC.UPDATE_USER_SET_TM
			, PRC.UPDATE_USER_HOLD
			, PRC.UPDATE_USER_REVIEWED
			--, prc.PRICE_MULTIPLE_NEW
			, case when prc.PRICE_MULTIPLE_NEW = 1 then Null else prc.PRICE_MULTIPLE_NEW end 'PRICE_MULTIPLE_NEW'
			, Null ind_Parity
			, ind_Margin_Tol
			, prc.ind_Rescind
			, '' 'Zone_Number'
			, Null 'item_superscript'
			, prc.item_sort
			, prc.item_group 'item_group'
			, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort_report'
			, prc.Zone_Sort
			, 0 'ind_bold'
			, 'Zone Details' 'comment'
			, null 'ind_discontinued'
			, prc.price_change_effective_date
			, PRC.PRICESHEET_RETAIL_CHANGES_ID 'ind_launch_audit'
			from #DisplayData prc
			WHERE 1 = 1
			and (prc.item_change_type_cr_fk in (656, 662) or prc.alias_id is null)

		Union
		----Get Items listing Node
			Select 
			3 'NODE_LEVEL'
			, cast(PRC.item_group as varchar(16)) + '.03' 'NODE_COLUMN_VALUE'
			, cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE_PARENT'
			, PRC.Alias_ID 'NODE_SORT'
			, Null 'PRICESHEET_RETAIL_CHANGES_ID'
			, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
			, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
			, Null 'COMPETITOR_CALL_TYPE_CR_FK'
			, case when @Report_Only = 1 and @ind_margin_changes = 1 then 0 when @Report_Only = 1 then qualified_status_cr_fk else null end 'QUALIFIED_STATUS_CR_FK'
			, NULL 'PRODUCT_STRUCTURE_FK'
			, Null 'ZONE_ENTITY_STRUCTURE_FK'
			, Null  'ZONE'
			, Null 'ZONE_NAME'
			, NULL 'SUBGROUP'
			, NULL 'SUBGROUP_DESCRIPTION'
			, NULL 'SUBGROUP_DATA_VALUE_FK'
			, 'Item Listing' 'ITEM'
			, Null 'ITEM_SEARCH'
			, '' 'ALIAS_SEARCH'
			, '' 'SUBGROUP_SEARCH'
			, Null 'ITEM_DESCRIPTION'
			, Null 'STORE_PACK'
			, Null 'SIZE_UOM'
			, Null 'CUR_COST'
			, Null 'CUR_CASE_COST'
			, Null 'New_CASE_COST'
			, Null 'CMULT'
			, Null 'CRETAIL'
			, Null PRICE_CUR
			, Null CUR_GM_PCT
	--		, case when isnull(prc.NTZ, '') <> '' then isnull(prc.COST_CHANGE, 0.0001) else prc.COST_CHANGE end 'COST_CHANGE'
			, Null 'COST_CHANGE'
			, Null PRICE_CHANGE
			, Null 'NEW_COST'
			, Null PRICE_NEW
			, Null NEW_GM_PCT
			, Null TARGET_MARGIN
			, Null TM_SOURCE
			, Null SRP_ADJUSTED
			, Null GM_ADJUSTED
			, Null TM_ADJUSTED
			, Null HOLD_CUR_PRICE
			, Null REVIEWED
			, NULL 'CIF'
			, Null UPDATE_USER_PRICE
			, Null UPDATE_USER_SET_TM
			, Null UPDATE_USER_HOLD
			, Null UPDATE_USER_REVIEWED
			, Null 'PRICE_MULTIPLE_NEW'
			, Null ind_Parity
			, Null 'Margin_Tol'
			, prc.force_rescind_display 'ind_Rescind'
			, '' 'Zone_Number'
			, case when isnull(prc.ftn_cost_delta, '') <> '' then ' ' + prc.ftn_cost_delta else '' end
				'item_superscript'
			, prc.item_sort
			, prc.item_group 'item_group'
			, 30 'item_sort_report'
			, Null 'Zone_Sort'
			, 0 'ind_bold'
			, 'Zone Details' 'comment'
			, null 'ind_discontinued'
			, prc.price_change_effective_date
			, Null 'ind_launch_audit'
			from #DisplayData prc
			WHERE 1 = 1
			and prc.item_change_type_cr_fk in (656, 662)

		Union

		----Get Items 
			Select 
			4 'NODE_LEVEL'
			, cast(PRC.item_group as varchar(16)) + '.04' 'NODE_COLUMN_VALUE'
			, cast(PRC.item_group as varchar(16)) + '.03' 'NODE_COLUMN_VALUE_PARENT'
			, PRC.Alias_ID 'NODE_SORT'
			, Null 'PRICESHEET_RETAIL_CHANGES_ID'
			, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
			, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
			, prc.ITEM_CHANGE_TYPE_CR_FK 'COMPETITOR_CALL_TYPE_CR_FK'
			, case when @Report_Only = 1 and @ind_margin_changes = 1 then 0 when @Report_Only = 1 then qualified_status_cr_fk else null end 'QUALIFIED_STATUS_CR_FK'
			, PRC.PRODUCT_STRUCTURE_FK
			, Null 'ZONE_ENTITY_STRUCTURE_FK'
			, Null  'ZONE'
			, Null 'ZONE_NAME'
			, PRC.SUBGROUP
			, PRC.SUBGROUP_DESCRIPTION
			, PRC.SUBGROUP_DATA_VALUE_FK
			, Item 'ITEM'
			, Item 'ITEM_SEARCH'
			, '' 'ALIAS_SEARCH'
			, '' 'SUBGROUP_SEARCH'
			, PRC.ITEM_DESCRIPTION 'ITEM_DESCRIPTION'
			, STORE_PACK 'STORE_PACK'
			, SIZE_UOM 'SIZE_UOM'
			, prc.CUR_COST
			, prc.CUR_CASE_COST
			, prc.New_CASE_COST 'NEW_CASE_COST'
			, Null 'CMULT'
			, Null 'CRETAIL'
			, Null PRICE_CUR
			, Null CUR_GM_PCT
			, case when isnull(prc.NTZ, '') <> '' then isnull(prc.COST_CHANGE, 0.0001) else prc.COST_CHANGE end 'COST_CHANGE'
			, Null PRICE_CHANGE
			, prc.NEW_COST
			, Null PRICE_NEW
			, Null NEW_GM_PCT
			, Null TARGET_MARGIN
			, Null TM_SOURCE
			, Null SRP_ADJUSTED
			, Null GM_ADJUSTED
			, Null TM_ADJUSTED
			, Null HOLD_CUR_PRICE
			, Null REVIEWED
			, prc.CIF
			, Null UPDATE_USER_PRICE
			, Null UPDATE_USER_SET_TM
			, Null UPDATE_USER_HOLD
			, Null UPDATE_USER_REVIEWED
			, Null 'PRICE_MULTIPLE_NEW'
			, prc.ind_Parity
			, Null 'Margin_Tol'
			, prc.ind_Rescind
			, '' 'Zone_Number'
			, isnull(Case	when isnull(parity_parent_item, parity_child_item) is not null then ' Parity to ' + isnull(parity_parent_item, parity_child_item) end, '') 'item_superscript'
			, prc.item_sort
			, prc.item_group 'item_group'
			, prc.item_sort 'item_sort_report'
			, Null 'Zone_Sort'
			, 0 'ind_bold'
			, 'Get list of Items' 'comment'
			, prc.ind_discontinued
			, prc.price_change_effective_date
			, (Select top 1 PRICESHEET_RETAIL_CHANGES_ID from #DisplayData where item = prc.item and item_change_type_cr_fk not in (656, 662)) 'ind_launch_audit'
			from #DisplayData prc
			WHERE 1 = 1
			and prc.item_change_type_cr_fk not in (656, 662)
			and prc.alias_id is not null

			Union

			Select 
			case when alias_id is null then 2 else 5 end 'NODE_LEVEL'
			, cast(PRC.item_group as varchar(16)) + case when alias_id is null then '.00.0' else '.04' end 'NODE_COLUMN_VALUE'
			, cast(PRC.item_group as varchar(16)) + case when prc.alias_id is null then '.00' else '.03' end 'NODE_COLUMN_VALUE_PARENT'
			, ALIAS_ID 'NODE_SORT'
			, NULL 'PRICESHEET_RETAIL_CHANGES_ID'
			, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
			, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
			, Null 'COMPETITOR_CALL_TYPE_CR_FK'
			, case when @Report_Only = 1 and @ind_margin_changes = 1 then 0 when @Report_Only = 1 then qualified_status_cr_fk else null end 'QUALIFIED_STATUS_CR_FK'
			, PB.PRODUCT_STRUCTURE_FK
			, Null ZONE_ENTITY_STRUCTURE_FK
			, Null ZONE
			, Null ZONE_NAME
			, Null 'SUBGROUP'
			, Null SUBGROUP_DESCRIPTION
			, Null SUBGROUP_DATA_VALUE_FK
			, pi.value + ' Qty: ' + Cast(Cast(pb.Qty as int) as varchar(8)) 'ITEM'
			, Null 'ITEM_SEARCH'
			, Null 'ALIAS_SEARCH'
			, Null 'SUBGROUP_SEARCH'
			, ltrim(ltrim(rtrim(Case when isnull(PB.State, '') = '' then '' else 'State: ' + ltrim(rtrim(PB.State)) + ' ' end)) + pd.description) 'item_description'
			, ISNULL(pa_500001.attribute_event_data, '''') 'STORE_PACK'
			, ISNULL(pa_500002.attribute_event_data, '''') 'SIZE_UOM'
			, Null 'CUR_COST'
			, Null 'CUR_CASE_COST'
			, Null 'NEW_CASE_COST'
			, Null 'CMULT'
			, Null 'CRETAIL'
			, Null 'PRICE_CUR'
			, Null 'CUR_GM_PCT'
			, Null 'COST_CHANGE'
			, Null 'PRICE_CHANGE'
			, Null 'NEW_COST'
			, Null 'PRICE_NEW'
			, Null 'NEW_GM_PCT'
			, Null 'TARGET_MARGIN'
			, Null 'TM_SOURCE'
			, Null 'SRP_ADJUSTED'
			, Null 'GM_ADJUSTED'
			, Null 'TM_ADJUSTED'
			, Null 'HOLD_CUR_PRICE'
			, Null 'REVIEWED'
			, Null 'CIF'
			, Null 'UPDATE_USER_PRICE'
			, Null 'UPDATE_USER_SET_TM'
			, Null 'UPDATE_USER_HOLD'
			, Null 'UPDATE_USER_REVIEWED'
			, Null 'PRICE_MULTIPLE_NEW'
			, NULL 'ind_Parity'
			, Null 'ind_Margin_Tol'
			, Null 'ind_Rescind'
			, Null 'zone_number'
			, null 'item_superscript'
			, left(prc.item, len(prc.item)  - 1) + 1 'item_sort'
			, prc.item_group 'item_group'
			, prc.item_sort + 1 'item_sort_report'
			, 0 'Zone_Sort'
			, null 'ind_bold'
			, 'get breakdown items' 'comment'
			, null 'ind_discontinued'
			, prc.price_change_effective_date
			, Null 'ind_launch_audit'
			FROM #DisplayData PRC with (nolock)
			inner join #Zones Z on prc.zone_entity_structure_fk = Z.zone_entity_Structure_FK
			inner join epacube.PRODUCT_BOM PB on prc.product_structure_fk = pb.product_structure_fk and pb.data_name_fk = 500213
			inner join epacube.PRODUCT_IDENTIFICATION pi with (nolock) on pb.child_product_structure_fk = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110100
			inner join epacube.product_description pd with (nolock) on pi.PRODUCT_STRUCTURE_FK = pd.PRODUCT_STRUCTURE_FK and pd.DATA_NAME_FK = 110401
			left join [epacube].[PRODUCT_ATTRIBUTE] pa_500001 with (nolock) on pi.Product_Structure_FK = pa_500001.PRODUCT_STRUCTURE_FK and pa_500001.data_name_fk = 500001
			left join [epacube].[PRODUCT_ATTRIBUTE] pa_500002 with (nolock) on pi.Product_Structure_FK = pa_500002.PRODUCT_STRUCTURE_FK and pa_500002.data_name_fk = 500002
			where 1 = 1
			and @Report_Only = 1
		) A	
	) B Where 1 = 1
		and DRank = 1
		Order by cast(left(node_column_value, charindex('.', node_column_value) - 1) as bigint), node_column_value, item_sort, zone_sort desc
	End
