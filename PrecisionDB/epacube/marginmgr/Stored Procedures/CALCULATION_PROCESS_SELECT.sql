﻿
-- Copyright 2009
--
-- Procedure select the EVENT_CALC rows for the Analysis Job
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ---------------------------------------------------------------
-- KS        05/08/2009  Initial Version (V516 Does not include Whatif or Rule Qualified)
-- KS        11/11/2012  Noticed in a job that tool 13 minutes 11 minutes was spent on Insert to Event_Calc
--                            Therefore if the Sales_History_Ind = 1 then run more effectient SQL below
-- GHS		 6/17/2013	 Added ability to filter records to select based on new 'Inner_Join_Filter' column in marginmgr.analysis_job table
-- GHS		05/20/2014	 Line 511: Added criteria to only pull sales qty from MarginMgr.Order_Data_History table when it actually exists there, and is called for.
-- GHS		05/27/2015	 Restructured

CREATE PROCEDURE [marginmgr].[CALCULATION_PROCESS_SELECT]
          @in_analysis_job_fk BIGINT
AS
BEGIN

DECLARE @l_sysdate    datetime
DECLARE @ls_stmt      varchar (1000)
DECLARE @ls_queue     nvarchar (4000)
DECLARE @l_exec_no    bigint
DECLARE @l_rows_processed    BIGINT
DECLARE @ls_exec      nvarchar(MAX)
DECLARE @ls_exec1     nvarchar(MAX)
DECLARE @ls_exec2     nvarchar(MAX)

DECLARE @v_scenario_seq   int
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

DECLARE @job_id_tbl				table (analysis_job_id bigint)
DECLARE @v_analysis_job_fk		BIGINT
DECLARE @v_host_erp_system		varchar(32)

DECLARE @status_desc  varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

BEGIN TRY
SET NOCOUNT ON
SET @l_sysdate = getdate ()

SET @l_exec_no = 0;
SET @ls_stmt =  'started execution of MARGINMGR.CALCULATION_PROCESS_SELECT'
               + ' JOB:: ' + CAST ( ISNULL (@in_analysis_job_fk, 0 ) AS VARCHAR(16) )

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
										@exec_id = @l_exec_no OUTPUT;

BEGIN

-------------------------------------------------------------------------------------
--  Simplest Filtering Match same Events in Previous Job
-------------------------------------------------------------------------------------
If (select count(*) from synchronizer.EVENT_CALC where job_fk = @in_analysis_job_fk) > 0
Goto CostUpdate

IF ( SELECT ISNULL ( AJ.JOB_FK_SELECT, 0 ) 
     FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
     WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk )
   <> 0
BEGIN

INSERT INTO [synchronizer].[EVENT_CALC]
          ( [JOB_FK]
           ,[RESULT_DATA_NAME_FK]           
           ,[RESULT_TYPE_CR_FK]           
           ,[RESULT_EFFECTIVE_DATE]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[CUST_ENTITY_STRUCTURE_FK]
           ,[SUPL_ENTITY_STRUCTURE_FK]
           ,[CALC_GROUP_SEQ]
           ,[RULE_TYPE_CR_FK]                                             
           ,[UPDATE_USER]  ) 
SELECT DISTINCT     
            AJ.ANALYSIS_JOB_ID
           ,DN.DATA_NAME_ID   
           ,AJ.RESULT_TYPE_CR_FK 
           ,ISNULL ( AJ.RESULT_EFFECTIVE_DATE, GETDATE() )  --- should only happen if current
           ,EC.PRODUCT_STRUCTURE_FK
           ,EC.ORG_ENTITY_STRUCTURE_FK
           ,EC.CUST_ENTITY_STRUCTURE_FK	 
           ,ISNULL (
            ( SELECT PAS.ENTITY_STRUCTURE_FK
              FROM epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
              WHERE PAS.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT ) FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK) 
                                         WHERE EEP.APPLICATION_SCOPE_FK = 100 AND EEP.NAME = 'WHSE SUPL ASSOC' )
              AND   PAS.PRODUCT_STRUCTURE_FK = EC.PRODUCT_STRUCTURE_FK  
              AND   PAS.ORG_ENTITY_STRUCTURE_FK = EC.ORG_ENTITY_STRUCTURE_FK
              AND   PAS.RECORD_STATUS_CR_FK = 1 )
           ,( SELECT PAS.ENTITY_STRUCTURE_FK
              FROM epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
              WHERE PAS.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT ) FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK) 
                                         WHERE EEP.APPLICATION_SCOPE_FK = 100 AND EEP.NAME = 'CORP SUPL ASSOC' )
              AND   PAS.PRODUCT_STRUCTURE_FK = EC.PRODUCT_STRUCTURE_FK 
              AND   PAS.RECORD_STATUS_CR_FK = 1 )
             )  
           ,RH.CALC_GROUP_SEQ           
           ,RH.RULE_TYPE_CR_FK                   
           ,AJ.UPDATE_USER
 FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
 INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK) ON DN.DATA_NAME_ID = EC.RESULT_DATA_NAME_FK
 INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
 ON  RH.RECORD_STATUS_CR_FK = 1
 AND RH.RESULT_DATA_NAME_FK = DN.DATA_NAME_ID 
 INNER JOIN marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
  ON ( EC.JOB_FK  = CAST ( AJ.JOB_FK_SELECT AS BIGINT ) )
 WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk 
 
 UPDATE marginmgr.ANALYSIS_JOB
 SET EVENT_CALC_COUNT = ( SELECT COUNT(1)
                          FROM synchronizer.EVENT_CALC WITH (NOLOCK)
                          WHERE JOB_FK = @in_analysis_job_fk )

END
ELSE
BEGIN 



-------------------------------------------------------------------------------------
--  VERIFY THAT IF SALES_HISTORY_IND = 0
--     MAKE SURE EITHER COST_CHANGE = 1 OR A PRODUCT_FILTER EXISTS
--     AND  Customer Filter Exists
-------------------------------------------------------------------------------------

   IF  (	SELECT COUNT(1)
			FROM marginmgr.ANALYSIS_JOB WITH (NOLOCK)
			WHERE 1 = 1
			and ANALYSIS_JOB_ID = @in_analysis_job_fk
			AND   ISNULL ( SALES_HISTORY_IND, 0 ) = 0
			AND   (  ---- CHECK FOR A PRODUCT FILTER
			       ( ( ( ISNULL ( COST_CHANGE_FIRST_IND, 0 ) = 0 ) AND ( ISNULL ( COST_CHANGE_PRIOR_IND, 0 ) = 0 ) )
			       AND ISNULL ( PRODUCT_FILTER, 'NULL DATA' ) = 'NULL DATA' 
			       AND ISNULL ( SUPPLIER_FILTER,'NULL DATA' ) = 'NULL DATA'  )
			      OR ( ISNULL ( CUSTOMER_FILTER, 'NULL DATA' ) = 'NULL DATA' )
			      )
	   ) >  0
   BEGIN 
   
	SET @status_desc = 'MARGINMGR.CALCULATION_PROCESS:: If Not Including History Must Specify Product and Customer Filters '
					 + CAST ( ISNULL ( @in_analysis_job_fk, 0 ) AS VARCHAR (16) )
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
	EXEC exec_monitor.print_status_sp  @l_exec_no;

   END
ELSE
BEGIN



------------------------------------------------------------------------------------------
--  If Cost_Change_Ind = 1;  Then retrieve all the products with future cost changes
------------------------------------------------------------------------------------------

IF ( SELECT COUNT(1) 
     FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK) 
	 WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk 
     AND   ( ( AJ.COST_CHANGE_FIRST_IND = 1 )  OR ( AJ.COST_CHANGE_PRIOR_IND = 1 ) )  ) > 0 
BEGIN     

	EXEC marginmgr.CALCULATION_PROCESS_SELECT_COST_UPDATE
	  @in_analysis_job_fk     WITH RECOMPILE 

END


 DECLARE 
         @v_sales_history_ind     AS int
        ,@v_product_filter        AS VARCHAR(MAX)
        ,@v_customer_filter       AS VARCHAR(MAX)
        ,@v_organization_filter   AS VARCHAR(MAX)
        ,@v_supplier_filter       AS VARCHAR(MAX)        
        ,@v_sales_history_filter  AS VARCHAR(MAX)
        ,@v_rule_type_filter      AS VARCHAR(MAX)
        ,@v_cost_change_filter    AS VARCHAR(MAX)
        ,@v_event_related_filter  AS VARCHAR(MAX)
        ,@v_rule_filter			  AS VARCHAR(MAX)
        ,@v_effective_date_sql    AS VARCHAR(MAX)                
        ,@v_tmp_Inner_Join		  As Varchar(Max)

 SET     @v_sales_history_ind     = ( SELECT ISNULL (AJ.SALES_HISTORY_IND, 0 ) 
                                       FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
                                       WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk )   
        
 SET     @v_product_filter        = ISNULL ( 
									( SELECT
									  ' AND  PS.PRODUCT_STRUCTURE_ID IN ' 
									  + AJ.PRODUCT_FILTER 
									  FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK) 
									  WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk )
									,' ' )
 SET     @v_customer_filter        = ISNULL ( 
									( SELECT 
									  ' AND  ESC.ENTITY_STRUCTURE_ID IN '
									  + AJ.CUSTOMER_FILTER 
									  FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK) 
									  WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk )
									,' ' )
 SET     @v_organization_filter    = ISNULL ( 
									( SELECT 
									  ' AND  ESO.ENTITY_STRUCTURE_ID IN '
									  + AJ.ORGANIZATION_FILTER 
									  FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
									  WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk )
									,' ' )
 SET     @v_supplier_filter    = ISNULL ( 
									( SELECT 
									  ' INNER JOIN epacube.PRODUCT_ASSOCIATION PA WITH (NOLOCK)    ON  PA.DATA_NAME_FK = AJ.SUPL_ASSOC_DN_FK    AND  PA.PRODUCT_STRUCTURE_FK = PS.PRODUCT_STRUCTURE_ID    AND  (   ISNULL ( AJ.SUPL_ASSOC_ORG_IND, 0 ) = 0        OR PA.ORG_ENTITY_STRUCTURE_FK = ESO.ENTITY_STRUCTURE_ID )     AND  PA.RECORD_STATUS_CR_FK = 1  AND   PA.ENTITY_STRUCTURE_FK IN '  
									  + AJ.SUPPLIER_FILTER 
									  FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
									  WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk )	
									 ,' ' )

 SET     @v_rule_type_filter        = ISNULL ( 
									( SELECT AJ.RULE_TYPE_FILTER 
									  FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
									  WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk )
									,' ' )

 SET     @v_sales_history_filter    = ( SELECT CASE ISNULL (AJ.SALES_HISTORY_IND, 0 ) 
                                       WHEN 0 THEN ' '
                                       ELSE ' INNER JOIN marginmgr.ORDER_DATA_HISTORY ODH WITH (NOLOCK)
									  	    ON   DRANK = 1
									  	    AND  ODH.PRODUCT_STRUCTURE_FK = PS.PRODUCT_STRUCTURE_ID 
										    AND  ODH.CUST_ENTITY_STRUCTURE_FK = ESC.ENTITY_STRUCTURE_ID
										    AND  ODH.ORG_ENTITY_STRUCTURE_FK = ESO.ENTITY_STRUCTURE_ID '
								       END
                                       FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
                                       WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk )

 SET    @v_cost_change_filter       = ( SELECT CASE ISNULL ( COALESCE ( AJ.COST_CHANGE_FIRST_IND, AJ.COST_CHANGE_PRIOR_IND   ), 0 ) 
                                       WHEN 0 THEN ' '
                                       ELSE ' INNER JOIN [marginmgr].[ANALYSIS_JOB_COST_CHANGE] AJCC WITH (NOLOCK)    ON ( AJCC.PRODUCT_STRUCTURE_FK = PS.PRODUCT_STRUCTURE_ID ) '
								       END
                                       FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
                                       WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk )
 
 SET    @v_event_related_filter = ' '  --- WILL DO THIS IN V517 this will be with Rules that Trigger Changes similar to Cost Changes
 SET    @v_rule_filter = ' '  --- WILL DO THIS IN V517 when we have a better definition

 
 SET    @v_effective_date_sql      =  ( SELECT CASE ISNULL (AJ.COST_CHANGE_FIRST_IND, 0 )
											   WHEN 0 THEN CASE ISNULL (AJ.RESULT_EFFECTIVE_DATE, '01/01/9999' ) 
													  WHEN '01/01/9999' THEN ' ,( SELECT CONVERT ( VARCHAR, GETDATE (), 112 ) ) ' 
													  ELSE ' ,( SELECT CONVERT ( VARCHAR, AJ.RESULT_EFFECTIVE_DATE, 112 ) ) '
													  END
											   ELSE ' ,( SELECT CONVERT ( VARCHAR, AJCC.COST_EFFECTIVE_DATE, 112 ) ) '
										END
										FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
                                        WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk )

Set		@v_tmp_Inner_Join			=	ISNULL ( 
									( SELECT AJ.Inner_Join_Filter
									  FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK) 
									  WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk )
									,'' )

IF @v_sales_history_ind = 0
BEGIN
SET @ls_exec2 = 
  ' FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
	INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
	 ON DN.RECORD_STATUS_CR_FK = AJ.RECORD_STATUS_CR_FK
	INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
	 ON  RH.RECORD_STATUS_CR_FK = 1
	 AND RH.RESULT_DATA_NAME_FK = DN.DATA_NAME_ID 
	INNER JOIN epacube.PRODUCT_STRUCTURE PS WITH (NOLOCK) 
	 ON PS.RECORD_STATUS_CR_FK = DN.RECORD_STATUS_CR_FK 
	INNER JOIN epacube.ENTITY_STRUCTURE ESO WITH (NOLOCK)
	 ON (   ESO.RECORD_STATUS_CR_FK = DN.RECORD_STATUS_CR_FK AND ESO.ENTITY_CLASS_CR_FK = 10101 
	    AND (  ( ISNULL ( AJ.PRODUCT_WHSE_IND, 0 ) = 0 )
	        OR ( ISNULL ( AJ.PRODUCT_WHSE_IND, 0 ) = 1 AND ESO.ENTITY_STRUCTURE_ID IN ( 
	               SELECT PAS.ORG_ENTITY_STRUCTURE_FK FROM epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
	               WHERE PAS.DATA_NAME_FK = 159100 
	               AND   PAS.PRODUCT_STRUCTURE_FK = PS.PRODUCT_STRUCTURE_ID
	               AND   PAS.RECORD_STATUS_CR_FK = 1 )  )  ) ) '
  +  @v_cost_change_filter	 	 
  + ' INNER JOIN epacube.ENTITY_STRUCTURE ESC WITH (NOLOCK)
	 ON ESC.RECORD_STATUS_CR_FK = DN.RECORD_STATUS_CR_FK AND ESC.ENTITY_CLASS_CR_FK = 10104 '
  +  @v_tmp_Inner_Join
  +  @v_supplier_filter
  + ' WHERE AJ.ANALYSIS_JOB_ID = '
  + CAST ( @in_analysis_job_fk AS VARCHAR(16) )
  + ' AND RH.RULE_TYPE_CR_FK IN ' 
  + @v_rule_type_filter                           
  + @v_product_filter
  + @v_customer_filter
  + @v_organization_filter
  + @v_rule_filter
END


IF @v_sales_history_ind = 1
BEGIN
SET @ls_exec2 = 
  ' FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)   
	INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)    ON DN.RECORD_STATUS_CR_FK = AJ.RECORD_STATUS_CR_FK   
	INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)  ON  RH.RECORD_STATUS_CR_FK = 1    AND RH.RESULT_DATA_NAME_FK = DN.DATA_NAME_ID    
	INNER JOIN  marginmgr.ORDER_DATA_HISTORY ODH WITH (NOLOCK) ON ODH.DRANK = DN.RECORD_STATUS_CR_FK   
	INNER JOIN  epacube.PRODUCT_STRUCTURE PS WITH (NOLOCK)     ON PS.PRODUCT_STRUCTURE_ID = ODH.PRODUCT_STRUCTURE_FK
	INNER JOIN epacube.ENTITY_STRUCTURE ESO WITH (NOLOCK)
	 ON (   ESO.RECORD_STATUS_CR_FK = DN.RECORD_STATUS_CR_FK AND ESO.ENTITY_CLASS_CR_FK = 10101 
	    AND (  ( ISNULL ( AJ.PRODUCT_WHSE_IND, 0 ) = 0 )
	        OR ( ISNULL ( AJ.PRODUCT_WHSE_IND, 0 ) = 1 AND ESO.ENTITY_STRUCTURE_ID IN ( 
	               SELECT PAS.ORG_ENTITY_STRUCTURE_FK FROM epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
	               WHERE PAS.DATA_NAME_FK = 159100 
	               AND   PAS.PRODUCT_STRUCTURE_FK = PS.PRODUCT_STRUCTURE_ID
	               AND   PAS.RECORD_STATUS_CR_FK = 1 )  )  ) ) '
  +  @v_cost_change_filter	 	 
  + ' INNER JOIN epacube.ENTITY_STRUCTURE ESC WITH (NOLOCK)
	 ON ESC.RECORD_STATUS_CR_FK = DN.RECORD_STATUS_CR_FK AND ESC.ENTITY_CLASS_CR_FK = 10104 '
  +  @v_supplier_filter
  + ' WHERE 1=1
	  AND  ODH.PRODUCT_STRUCTURE_FK = PS.PRODUCT_STRUCTURE_ID                
	  AND  ODH.CUST_ENTITY_STRUCTURE_FK = ESC.ENTITY_STRUCTURE_ID              
	  AND  ODH.ORG_ENTITY_STRUCTURE_FK = ESO.ENTITY_STRUCTURE_ID    
      AND   AJ.ANALYSIS_JOB_ID = '
  + CAST ( @in_analysis_job_fk AS VARCHAR(16) )
  + ' AND RH.RULE_TYPE_CR_FK IN ' 
  + @v_rule_type_filter                           
  + @v_product_filter
  + @v_customer_filter
  + @v_organization_filter
  + @v_rule_filter
END




/*

-----    Keep Original until testing all complete
SET @ls_exec2 = 
  ' FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
	INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
	 ON DN.RECORD_STATUS_CR_FK = AJ.RECORD_STATUS_CR_FK
	INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)
	 ON  RH.RECORD_STATUS_CR_FK = 1
	 AND RH.RESULT_DATA_NAME_FK = DN.DATA_NAME_ID 
	INNER JOIN epacube.PRODUCT_STRUCTURE PS WITH (NOLOCK) 
	 ON PS.RECORD_STATUS_CR_FK = DN.RECORD_STATUS_CR_FK 
	INNER JOIN epacube.ENTITY_STRUCTURE ESO WITH (NOLOCK)
	 ON (   ESO.RECORD_STATUS_CR_FK = DN.RECORD_STATUS_CR_FK AND ESO.ENTITY_CLASS_CR_FK = 10101 
	    AND (  ( ISNULL ( AJ.PRODUCT_WHSE_IND, 0 ) = 0 )
	        OR ( ISNULL ( AJ.PRODUCT_WHSE_IND, 0 ) = 1 AND ESO.ENTITY_STRUCTURE_ID IN ( 
	               SELECT PAS.ORG_ENTITY_STRUCTURE_FK FROM epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
	               WHERE PAS.DATA_NAME_FK = 159100 
	               AND   PAS.PRODUCT_STRUCTURE_FK = PS.PRODUCT_STRUCTURE_ID
	               AND   PAS.RECORD_STATUS_CR_FK = 1 )  )  ) ) '
  +  @v_cost_change_filter	 	 
  + ' INNER JOIN epacube.ENTITY_STRUCTURE ESC WITH (NOLOCK)
	 ON ESC.RECORD_STATUS_CR_FK = DN.RECORD_STATUS_CR_FK AND ESC.ENTITY_CLASS_CR_FK = 10104 '
  +  @v_sales_history_filter  
  +  @v_supplier_filter
  + ' WHERE AJ.ANALYSIS_JOB_ID = '
  + CAST ( @in_analysis_job_fk AS VARCHAR(16) )
  + ' AND RH.RULE_TYPE_CR_FK IN ' 
  + @v_rule_type_filter                           
  + @v_product_filter
  + @v_customer_filter
  + @v_organization_filter
  + @v_rule_filter
*/

  
----  @v_event_related_filter  will be an IN ( marginmgr.ANALYSIS_TRIGGER_VALUES; Result DN, PS only ??? )
--      similar to calc since last job



-------------------------------------------------------------------------------------
--  VERIFY THAT THERE ARE NOT TOO MANY EVENT_CALC ROWS
--   … IF SALES_HISTORY_IND <> 1 MAKE SURE # EVENT CALC ROWS IS < = 'CALC BATCH SIZE' 
-------------------------------------------------------------------------------------

 SET @ls_exec = 
 '
UPDATE [marginmgr].[ANALYSIS_JOB] 
SET EVENT_CALC_COUNT = ( SELECT COUNT(1) ' 
+ @ls_exec2
+ ' ) '
+ ' WHERE ANALYSIS_JOB_ID = '
+ CAST ( @in_analysis_job_fk AS VARCHAR(16) )



----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS,  SQL_TEXT )
            VALUES ( GETDATE(), ISNULL ( @ls_exec, 'NULL STRING' ) )

exec sp_executesql 	@ls_exec;   

   SET @v_count = ( SELECT CASE ISNULL ( SALES_HISTORY_IND, 0 ) 
                        WHEN 0 THEN ISNULL ( EVENT_CALC_COUNT, 0 )
                        ELSE 99
                        END  
                 FROM marginmgr.ANALYSIS_JOB WITH (NOLOCK)
                 WHERE ANALYSIS_JOB_ID = @in_analysis_job_fk )
                    
   IF ( ( @v_count > (	SELECT CAST ( EEP.VALUE AS BIGINT )
					    FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
					    WHERE EEP.APPLICATION_SCOPE_FK = 100
					    AND   EEP.NAME = 'CALC BATCH SIZE'  )   )   --- EXCEEDS THE BATCH SIZE
   AND ( ( SELECT ISNULL ( AJ.SALES_HISTORY_IND, 0 ) FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
	        WHERE AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk ) = 0 ) ) --- SALES HISTORY IND NOT SELECTED
	  			
   BEGIN 
   
	SET @status_desc = 'MARGINMGR.CALCULATION_PROCESS:: Number of Rows in Calculation exceed CALC BATCH SIZE '
					 + CAST ( ISNULL ( @in_analysis_job_fk, 0 ) AS VARCHAR (16) )
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
	EXEC exec_monitor.print_status_sp  @l_exec_no;

   END
ELSE
BEGIN


 SET @ls_exec1 = 
 '
INSERT INTO [synchronizer].[EVENT_CALC]
          ( [JOB_FK]
           ,[RESULT_DATA_NAME_FK]           
           ,[RESULT_TYPE_CR_FK]           
           ,[RESULT_EFFECTIVE_DATE]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[CUST_ENTITY_STRUCTURE_FK]
           ,[SUPL_ENTITY_STRUCTURE_FK]
           ,[CALC_GROUP_SEQ]
           ,[RULE_TYPE_CR_FK] 
           ,[PARITY_DV_FK]                                            
           ,[UPDATE_USER]  ) 
SELECT DISTINCT     
            AJ.ANALYSIS_JOB_ID
           ,DN.DATA_NAME_ID   
           ,AJ.RESULT_TYPE_CR_FK '
        +  @v_effective_date_sql
        +  ' ,PS.PRODUCT_STRUCTURE_ID
           ,ESO.ENTITY_STRUCTURE_ID
           ,ESC.ENTITY_STRUCTURE_ID	 
           ,ISNULL (
            ( SELECT PAS.ENTITY_STRUCTURE_FK
              FROM epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
              WHERE PAS.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT ) FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK) 
                                         WHERE EEP.APPLICATION_SCOPE_FK = 100 AND EEP.NAME = ''WHSE SUPL ASSOC'' )
              AND   PAS.PRODUCT_STRUCTURE_FK = PS.PRODUCT_STRUCTURE_ID  
              AND   PAS.ORG_ENTITY_STRUCTURE_FK = ESO.ENTITY_STRUCTURE_ID
              AND   PAS.RECORD_STATUS_CR_FK = 1 )
           ,( SELECT PAS.ENTITY_STRUCTURE_FK
              FROM epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
              WHERE PAS.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT ) FROM EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK) 
                                         WHERE EEP.APPLICATION_SCOPE_FK = 100 AND EEP.NAME = ''CORP SUPL ASSOC'' )
              AND   PAS.PRODUCT_STRUCTURE_FK = PS.PRODUCT_STRUCTURE_ID 
              AND   PAS.RECORD_STATUS_CR_FK = 1 )
             )  
           ,RH.CALC_GROUP_SEQ           
           ,RH.RULE_TYPE_CR_FK
           ,( SELECT PC.DATA_VALUE_FK FROM epacube.PRODUCT_CATEGORY PC WITH (NOLOCK) WHERE PC.DATA_NAME_FK = RH.PARITY_CHECK_DN_FK AND PC.PRODUCT_STRUCTURE_FK = PS.PRODUCT_STRUCTURE_ID )                    
           ,AJ.UPDATE_USER 
'



  SET @ls_exec = @ls_exec1 + ISNULL ( @ls_exec2, 'STRING2 NULL' )

 
 		
------------------------------------------------------------------------------------------
--  Execute Dynamic SQL 
------------------------------------------------------------------------------------------

----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS,  SQL_TEXT )
            VALUES ( GETDATE(), ISNULL ( @ls_exec, 'NULL STRING' ) )


            exec sp_executesql 	@ls_exec;   



------------------------------------------------------------------------------------------
--  Update EVENT_CALC
--    If any BASIS Model_Sales_Qty…. Update Value Here
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_CALC
SET MODEL_SALES_QTY = Case When @v_sales_history_ind = 1 then ODH.Sales_Qty else 1 end
FROM marginmgr.ORDER_DATA_HISTORY ODH WITH (NOLOCK)
WHERE 1 = 1
AND  ODH.PRODUCT_STRUCTURE_FK = synchronizer.EVENT_CALC.PRODUCT_STRUCTURE_FK
AND  ODH.ORG_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.ORG_ENTITY_STRUCTURE_FK
AND  ODH.CUST_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.CUST_ENTITY_STRUCTURE_FK
AND  synchronizer.EVENT_CALC.RESULT_DATA_NAME_FK = 111602  --- PRICING ONLY


CostUpdate:
------------------------------------------------------------------------------------------
--  Update Costs  ( PRICING_BASIS AND MARGIN_BASIS ) if calculating Pricing or Rebates
------------------------------------------------------------------------------------------

IF ( SELECT COUNT(1) 
     FROM synchronizer.EVENT_CALC WITH (NOLOCK)
     WHERE JOB_FK = @in_analysis_job_fk AND RULE_TYPE_CR_FK IN ( 312, 313 ) )
     > 0
BEGIN     

	EXEC marginmgr.CALCULATION_PROCESS_COSTS
	  @in_analysis_job_fk     WITH RECOMPILE 

END



END   --- Count exceeds CALC BATCH SIZE


END   --- If No History is Included then Must specifiy Product and Customer Filter

END   --- SELECT SAME ROWS AND JOB_FK SPECIFIED

END

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of MARGINMGR.CALCULATION_PROCESS_SELECT'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;


END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of MARGINMGR.CALCULATION_PROCESS_SELECT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;

         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
