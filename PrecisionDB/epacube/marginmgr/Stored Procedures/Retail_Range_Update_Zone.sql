﻿-- =============================================
-- Author:		Gary Stone
-- Create date: May 4, 2018
-- Description:	Update Zone_Price_Flag_Settings
-- =============================================
CREATE PROCEDURE [marginmgr].[Retail_Range_Update_Zone]
	@New_Effective_Date Date = getdate
	, @Segment_Settings_ID bigint
	, @Zone_Segment_FK bigint = Null
	, @Prod_Segment_FK bigint = Null
	, @New_Round_To_Value Numeric(18, 2) 
	, @New_Lower_Limit Numeric(18, 2)
	, @New_Upper_Limit Numeric(18, 2)
	, @Price_Type int
	, @Remove int = Null 
	, @User Varchar(64) = ''

AS
	SET NOCOUNT ON;
/*
	Declare @New_Effective_Date Date = '2018-7-24'
	, @Segment_Settings_ID bigint = Null
	, @Zone_Segment_FK bigint = 113246
	, @Prod_Segment_FK bigint = 1014719248
	, @New_Round_To_Value Numeric(18, 2) = 4.0
	, @New_Lower_Limit Numeric(18, 2) = 4.0
	, @New_Upper_Limit Numeric(18, 2) = 4.0
	, @Price_Type int = 2
	, @Remove int = null
	, @user varchar(64) = 'gstone@epacube.com'

	--select * from epacube.segments_settings where data_name_fk = 144891
	--and zone_segment_fk = @Zone_Segment_FK and prod_segment_fk = @Prod_Segment_FK

	--select * from epacube.data_value where data_value_id = @Prod_Segment_FK
	--select * from epacube.entity_identification where ENTITY_STRUCTURE_FK = @Zone_Segment_FK
*/

Declare @Cutoff as time
Declare @Time varchar(20)

Set @Cutoff = (select value from epacube.epacube_params WHERE EPACUBE_PARAMS_ID = 112000)

Set @Time = replace(convert(varchar(20), @Cutoff, 22), ':00 ', ' ')

If cast(getdate() as time) >= @Cutoff and @NEW_EFFECTIVE_DATE <= cast(getdate() as date)
Begin
	Select
	(Select	'You have missed today''s cutoff time of ' + @Time + '.<br><br>The values just entered have not been accepted.<br><br>Please change the effective date to tomorrow or later, refresh your screen, and re-enter your changes') 'USER_MSG'
	, 1 'reset_values'
	, 5000 'user_msg_timeout_ms'
	goto eof
End
	
Set @User = Replace(@User, '.', '_')

If cast(@New_Effective_Date as date) < Cast(getdate() as date)
goto eof

	insert into precision.user_call_statements
	(create_user, ui_name, procedure_called, procedure_parameters, table_name, table_row_id, prod_segment_fk, entity_segment_fk)
	Select @user 'create_user'
	, 'Zone_Price_Settings' 'ui_name'
	, '[marginmgr].[Retail_Range_Update_Zone]' 'procedure_called'
	, '''' + isnull(cast(@New_Effective_Date as varchar(24)), 'Null') + ''',
	 ' + isnull(cast(@Segment_Settings_ID as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@Zone_Segment_FK as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@Prod_Segment_FK as varchar(96)), 'Null') + ',
	 ' + isnull(cast(@New_Round_To_Value as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@New_Lower_Limit as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@New_Upper_Limit as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@Price_Type as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@Remove as varchar(24)), 'Null') + ',
	 ''' + isnull(cast(@user as varchar(24)), 'Null') + '''' 'procedure_parameters'
	, '[epacube].[segments_settings]' 'table_name'
	, @Segment_Settings_ID 'table_row_id', @Prod_Segment_FK 'prod_segment_fk', @Zone_Segment_FK 'entity_segment_fk'

Declare @Price_Type_Entity_Data_Value_FK bigint
Set @Price_Type_Entity_Data_Value_FK = (select entity_data_value_id from epacube.entity_data_value where data_name_fk = 502031 and value = @Price_Type and DATA_VALUE_FK = 
	(select data_value_id from epacube.data_value where data_name_fk = 502041 and value = '10'))

If cast((Select create_timestamp from epacube.epacube.segments_settings where segments_settings_id = @Segment_Settings_ID) as date) = cast(getdate() as date)
Delete from epacube.epacube.segments_settings where segments_settings_id = @Segment_Settings_ID


If isnull(@Remove, 0) <> 1
	Insert into epacube.epacube.segments_settings
	(Effective_Date, Prod_Data_Value_FK, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Data_Name_FK, Attribute_Number, Attribute_Event_Data
	, Precedence, Record_Status_CR_FK, Prod_Segment_FK, Zone_Segment_FK, Lower_Limit, Upper_Limit, ENTITY_DATA_NAME_FK, PRICE_TYPE_ENTITY_DATA_VALUE_FK, Create_User, [DATA_SOURCE])
	Select @New_Effective_Date 'Effective_Date', @Prod_Segment_FK 'Prod_Data_Value_FK', 1 'Org_Entity_Structure_FK', @Zone_Segment_FK 'Zone_Entity_Structure_fk', 144891 'Data_Name_FK', @New_Round_To_Value 'Attribute_Number', @New_Round_To_Value 'Attribute_Event_Data'
	, 20 'Precedence', 1 'Record_Status_CR_FK', @Prod_Segment_FK 'Prod_Segment_FK', @Zone_Segment_FK 'Zone_Segment_FK', @New_Lower_Limit 'Lower_Limit', @New_Upper_Limit 'Upper_Limit', 151000 'ENTITY_DATA_NAME_FK'
	, @PRICE_TYPE_ENTITY_DATA_VALUE_FK 'PRICE_TYPE_ENTITY_DATA_VALUE_FK', @User 'Create_User', 'USER UPDATE' 'DATA_SOURCE'
else
	Update SS Set Record_status_cr_fk = 2, [DATA_SOURCE] = 'USER UPDATE' from epacube.epacube.segments_settings SS where segments_settings_id = @Segment_Settings_ID

eof:
