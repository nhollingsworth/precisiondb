﻿-- =============================================
-- Author:		Gary Stone
-- Create date: May 4, 2018
---- Description:	Update Zone_Price_TMs
-- =============================================
CREATE PROCEDURE [marginmgr].[Retail_Setting_Updates_Zone_TM]
@Pricing_Date Date = getdate
, @Zone_Segment_FK bigint
, @Data_Name Varchar(64) --(Column_Name)
, @Value Numeric(18, 4) = Null
, @Prod_Segment_Data_Name_FK bigint	= Null
, @Prod_Segment_FK bigint = Null
, @Prop_To_Data_Name_FK bigint = Null
, @Immediate int = 0
, @OverWrite int = 0
, @Reinstate_Inheritance int = 0
, @User varchar(64)


AS

/*
----marginmgr.Retail_Setting_Updates_Zone '2020-05-21', 113246, 'tm_pct', '0.55', 110103, 401024, 110103, 0, 0, 1, 'gstone@epacube.com'
--marginmgr.Retail_Setting_Updates_Zone '2020-05-21', 113246, 'tm_pct', '0.48', 110103, 401022, 110103, 0, 0, 1, 'gstone@epacube.com'

	Declare @Pricing_Date Date = '2020-05-21'
	Declare @Zone_Segment_FK bigint = 113246
	Declare @Data_Name Varchar(64) = 'tm_pct'
	Declare @Value Numeric(18, 4) = 0.48
	Declare @Prod_Segment_Data_Name_FK bigint	= 110103
	Declare @Prod_Segment_FK bigint = 401022
	Declare @Prop_To_Data_Name_FK bigint = 110103
	Declare @Immediate int = 0
	Declare @OverWrite int = 0
	Declare @Reinstate_Inheritance int = 1
	Declare @User varchar(64) = 'gstone@epacube.com'

*/

SET NOCOUNT ON;

If exists
(select * from epacube.segments_settings where data_name_fk = 502046 and PROD_SEGMENT_FK = @prod_segment_fk and PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK and effective_date = @Pricing_Date and @Reinstate_Inheritance = 1)
set @value = 0

set @Overwrite = case when @Prod_Segment_Data_Name_FK = @Prop_To_Data_Name_FK then 0 else @OverWrite end

If @Pricing_Date < cast(getdate() as date)
goto eof

If object_id('tempdb..#MC') is not null
drop table #MC

If object_id('tempdb..#segs') is not null
drop table #segs

If object_id('tempdb..#rec_delete') is not null
drop table #rec_delete

if object_id('tempdb..#prodsegments') is not null
drop table #prodsegments

if object_id('tempdb..#overwrites') is not null
drop table #overwrites

if object_id('tempdb..#t_results') is not null
drop table #t_results

	Declare @Price_Change_Effective_Date date
	Declare @Data_Name_FK bigint
	Declare @CurValue Numeric(18, 4)
	Declare @InheritVal Numeric(18, 4)
	Declare @NewVal varchar(8)
	Declare @ConditionToPost int
	Declare @reinstate_inheritance_date date = @pricing_date --dateadd(d, -1, @pricing_date)

	Set @Price_Change_Effective_Date = Cast(cast(@Pricing_Date as datetime) + 6 - DATEPART(DW, cast(@Pricing_Date as datetime)) as date)
	Set @User = Replace(@User, '.', '_')

	Set @Data_Name_FK = 502046
	Set @Data_Name = 'URM_TARGET_MARGIN'

	Set @Value = isnull(@Value, 0)

If @Prop_To_Data_Name_FK is null or @overwrite = 0
	set @Prop_To_Data_Name_FK = @Prod_Segment_Data_Name_FK

Set @Overwrite = case when @Prod_Segment_Data_Name_FK = 110103 then 0 else @overwrite end

	Declare @MaxPrec int = (select precedence from epacube.data_name where data_name_id = @Prod_Segment_Data_Name_FK)
	Declare @MinPrec int = (select precedence from epacube.data_name where data_name_id = isnull(@Prop_To_Data_Name_FK, @Prod_Segment_Data_Name_FK))

If @MaxPrec < @MinPrec
Begin
	
	Select 'You are propagating down to a higher level than you are currently trying to change.' 'MSG'
	goto eof

End

Create table #prodsegments(prod_segment_fk bigint null, parent_prod_segment_fk bigint null, prod_segment_data_name_fk bigint null, precedence int null, alias_id bigint null)

Insert into #prodsegments (prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
	Select a.*, dn.precedence 
	from (
	select dv.data_value_id 'prod_segment_fk', dv.parent_data_value_fk, dv.data_name_fk 'prod_segment_data_name_fk' from epacube.data_value dv where data_value_id = (select dv.PARENT_DATA_VALUE_FK from epacube.data_value dv where data_value_id = (select PARENT_DATA_VALUE_FK from epacube.data_value dv where data_value_id = @prod_segment_fk))
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where data_value_id = (select PARENT_DATA_VALUE_FK from epacube.data_value dv where data_value_id = @prod_segment_fk)
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where data_value_id = @prod_segment_fk
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where data_value_id = @prod_segment_fk
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where parent_data_value_fk in (select dv.data_value_id from epacube.data_value dv where data_value_id = @prod_segment_fk)
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where parent_data_value_fk in (select dv.data_value_id from epacube.data_value dv where parent_data_value_fk = (select dv.data_value_id from epacube.data_value dv where data_value_id = @prod_segment_fk))
	) A inner join epacube.data_name dn with (nolock) on a.prod_segment_data_name_fk = dn.DATA_NAME_ID 

	create index idx_01 on #prodsegments(prod_segment_fk, prod_segment_data_name_fk)

	If isnull(@Prop_To_Data_Name_FK, @Prod_Segment_Data_Name_FK) = 110103
	Begin

		If @Prod_Segment_Data_Name_FK = 110103
		insert into #prodsegments(prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
		select @prod_segment_fk, null 'parent_prod_segment_data_name_fk', 110103 'prod_segment_data_name_fk', 5 'precedence'

		If @Prop_To_Data_Name_FK = 110103 OR @Prod_Segment_Data_Name_FK = 110103
		Begin
			insert into #prodsegments(prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
			select sp.PRODUCT_STRUCTURE_FK, null 'parent_prod_segment_data_name_fk', 110103 'prod_segment_data_name_fk', 5 'precedence' 
			from #prodsegments s
			inner join epacube.segments_product sp with (nolock) on s.prod_segment_fk = sp.PROD_SEGMENT_FK and s.prod_segment_data_name_fk = sp.DATA_NAME_FK
			left join epacube.product_attribute pa_500012 with (nolock) on sp.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
			where sp.data_name_fk = 501045
			--and pa_500012.product_attribute_id is null
		End

			Insert into #prodsegments(prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
			select pmt.product_structure_fk, null 'parent_prod_segment_data_name_fk', 110103 'prod_segment_data_name_fk', 5 'precedence'
			from #prodsegments s
			inner join epacube.product_mult_type pmt1 with (nolock) on s.prod_segment_fk = pmt1.PRODUCT_STRUCTURE_FK and pmt1.ENTITY_STRUCTURE_FK = @Zone_Segment_FK and pmt1.data_name_fk = 500019
			inner join epacube.product_mult_type pmt with (nolock) on pmt1.data_value_fk = pmt.data_value_fk and pmt1.ENTITY_STRUCTURE_FK = pmt.ENTITY_STRUCTURE_FK and pmt.DATA_NAME_FK = 500019
			left join epacube.product_attribute pa_500012 with (nolock) on pmt.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
			where pmt.product_structure_fk not in (select prod_segment_fk from #prodsegments)
			--and pa_500012.product_attribute_id is null
			group by pmt.product_structure_fk
	End

	update ps
	set alias_id = dv.value
	from #prodsegments ps
	inner join epacube.product_mult_type pmt on ps.prod_segment_fk = pmt.PRODUCT_STRUCTURE_FK and pmt.ENTITY_STRUCTURE_FK = @Zone_Segment_FK and pmt.DATA_NAME_FK = 500019
	inner join epacube.data_value dv on pmt.data_value_fk = dv.data_value_id

if @Reinstate_Inheritance = 1 
	and @value <> 0
Begin
Print 'Update Reinstate 1'
	Update ss
	set reinstate_inheritance_date = @reinstate_inheritance_date
	, UPDATE_USER = @User
	, UPDATE_TIMESTAMP = getdate()
	from epacube.segments_settings ss
	inner join #prodsegments ps on ss.prod_segment_data_name_fk = ps.prod_segment_data_name_fk and ss.prod_segment_fk = ps.prod_segment_fk
	where 1 = 1
	and ss.data_name_fk = @Data_Name_FK
	and ss.ZONE_SEGMENT_FK = @Zone_Segment_FK
	and ss.effective_date <= @Pricing_Date
	and ss.PRECEDENCE between @MinPrec and @MaxPrec
	and ss.reinstate_inheritance_date is null
	and ss.PROD_SEGMENT_DATA_NAME_FK = case when @OverWrite = 0 then @Prod_Segment_Data_Name_FK else ss.PROD_SEGMENT_DATA_NAME_FK end
	and ss.PROD_SEGMENT_DATA_NAME_FK <> 501043 --prevents setting inheritance reinstatment on Dept level records - should not be possible.
End

if (@Reinstate_Inheritance = 1 or (@Reinstate_Inheritance = 0 and @overwrite = 1)) and @Value = 0
begin
Print 'Update Reinstate to Null'

	Update ss
	set reinstate_inheritance_date = Null
	, update_user = null
	, UPDATE_TIMESTAMP = null
	from epacube.segments_settings ss
	inner join #prodsegments ps on ss.prod_segment_data_name_fk = ps.prod_segment_data_name_fk and ss.prod_segment_fk = ps.prod_segment_fk and ss.PRECEDENCE between @MinPrec and @MaxPrec
	where 1 = 1
	and ss.data_name_fk = @Data_Name_FK
	and ss.ZONE_SEGMENT_FK = @Zone_Segment_FK
	and effective_date <= @Pricing_Date
	and reinstate_inheritance_date is not null
	and reinstate_inheritance_date = @reinstate_inheritance_date
end

		Delete ss
		from epacube.epacube.segments_settings ss with (nolock)
		inner join #prodsegments ps on ss.prod_segment_fk = ps.prod_segment_fk
									and ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk
		where data_name_fk = @data_name_fk
		and ss.effective_date = @Pricing_Date
		and ss.Effective_Date >= cast(getdate() as date)
		and ss.ZONE_SEGMENT_FK = @Zone_Segment_FK
		and ss.data_source = 'user update'

		Set @CurValue = 
				isnull(
				(	
					select top 1 ss.attribute_number
					from epacube.segments_settings ss
					inner join #prodsegments ps on ss.PROD_SEGMENT_FK = ps.prod_segment_fk and ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk
					where ss.data_name_fk = 502046
					and ps.precedence = (select top 1 precedence from #prodsegments where prod_segment_fk = @Prod_Segment_FK and PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK)
					and ss.reinstate_inheritance_date is null
					and ss.effective_date <= @Pricing_Date
					and ss.zone_segment_fk = @Zone_Segment_FK
					order by ps.PRECEDENCE, Effective_Date desc
				), 0.0000)

		Set @InheritVal =
				isnull(
						(	Select top 1 cast(ss.ATTRIBUTE_NUMBER as Numeric(18, 4)) 'tm_pct'
							from #prodsegments ps
							inner join epacube.segments_product sp1 with (nolock) on ps.prod_segment_data_name_fk = sp1.data_name_fk and ps.prod_segment_fk = sp1.prod_segment_fk and ps.precedence = @MinPrec
							inner join epacube.segments_product sp with (nolock) on sp1.PRODUCT_STRUCTURE_FK = sp.product_structure_fk
							inner join epacube.segments_settings ss with (nolock) on sp.prod_segment_fk = ss.prod_segment_fk and sp.data_name_fk = ss.PROD_SEGMENT_DATA_NAME_FK
							inner join epacube.product_association pa on sp.PRODUCT_STRUCTURE_FK = pa.product_structure_fk and pa.ENTITY_STRUCTURE_FK = @Zone_Segment_FK and pa.DATA_NAME_FK = 159450 
							where 1 = 1
							and ss.Effective_Date < @Pricing_Date
							and pa.RECORD_STATUS_CR_FK = 1
							and ss.data_name_fk = 502046
							and ss.zone_segment_fk = @Zone_Segment_FK
							and (ss.reinstate_inheritance_date > @pricing_date or ss.reinstate_inheritance_date is null)
							and ss.PRECEDENCE > @MaxPrec
							and ss.PROD_SEGMENT_FK in (select parent_prod_segment_fk from #prodsegments)
							order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc
							), 0)

							
		If @InheritVal = 0
		Set @InheritVal =
				isnull(
						(	Select top 1 cast(ss.ATTRIBUTE_NUMBER as Numeric(18, 4)) 'tm_pct'
							from #prodsegments ps
							inner join epacube.segments_settings ss with (nolock) on ps.prod_segment_fk = ss.prod_segment_fk and ps.prod_segment_data_name_fk = ss.PROD_SEGMENT_DATA_NAME_FK
							and ss.Effective_Date < @Pricing_Date
							and ss.data_name_fk = 502046
							and ss.zone_segment_fk = @Zone_Segment_FK
							and (ss.reinstate_inheritance_date > @pricing_date or ss.reinstate_inheritance_date is null)
							and ss.PRECEDENCE > @MaxPrec
							and ss.PROD_SEGMENT_FK in (select parent_prod_segment_fk from #prodsegments)
							order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc
						), 0)

--Declare @SSID bigint
--		Set @SSID =
--				isnull(
--						(	Select top 1 ss.segments_settings_id 'SSID'
--							from #prodsegments ps
--							inner join epacube.segments_product sp1 with (nolock) on ps.prod_segment_data_name_fk = sp1.data_name_fk and ps.prod_segment_fk = sp1.prod_segment_fk and ps.precedence = @MinPrec
--							inner join epacube.segments_product sp with (nolock) on sp1.PRODUCT_STRUCTURE_FK = sp.product_structure_fk
--							inner join epacube.segments_settings ss with (nolock) on sp.prod_segment_fk = ss.prod_segment_fk and sp.data_name_fk = ss.PROD_SEGMENT_DATA_NAME_FK
--							inner join epacube.product_association pa on sp.PRODUCT_STRUCTURE_FK = pa.product_structure_fk and pa.ENTITY_STRUCTURE_FK = @Zone_Segment_FK and pa.DATA_NAME_FK = 159450 
--							left join epacube.product_attribute pa_500012 with (nolock) on sp.PRODUCT_STRUCTURE_FK = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
--							where 1 = 1
--							and ss.Effective_Date < @Pricing_Date
--							--and pa_500012.PRODUCT_ATTRIBUTE_ID is null
--							and pa.RECORD_STATUS_CR_FK = 1
--							and ss.data_name_fk = 502046
--							and ss.zone_segment_fk = @Zone_Segment_FK
--							and (ss.reinstate_inheritance_date > @pricing_date or ss.reinstate_inheritance_date is null)
--							and ss.PRECEDENCE > @MaxPrec
--							and ss.PROD_SEGMENT_FK in (select parent_prod_segment_fk from #prodsegments)
--							order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc
--							), 0)
									
If @Value <> 0 
	And	(
			(@CurValue = 0 and @Value <> @InheritVal and ((@Reinstate_Inheritance = 0 or (@Reinstate_Inheritance = 1 and @Prod_Segment_Data_Name_FK <> 110103 and @Value <> @InheritVal))))
			or
			(@curValue <> @Value and @Value <> @InheritVal and @Reinstate_Inheritance = 0)
			or
			(@CurValue <> 0 and @value = @InheritVal and @Reinstate_Inheritance = 0)
			or
			@Reinstate_Inheritance = 0
			or
			(@Reinstate_Inheritance = 1 and @Prod_Segment_Data_Name_FK = 501043) --will load a changed department level record without turning on inheritance to a department level record. Inheritance, thus "reinstate_inheritance_date" is only for records under the department level.
		)
Begin
	Print 'Insert SS 1'
	Insert into epacube.epacube.segments_settings
	(Effective_Date, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, ZONE_SEGMENT_FK, PROD_SEGMENT_DATA_NAME_FK, PROD_SEGMENT_FK, Data_Name_FK, ENTITY_DATA_NAME_FK
	, Attribute_Number, Attribute_Event_Data
	, Record_Status_CR_FK, Create_User, PRODUCT_STRUCTURE_FK, Prod_Data_Value_FK, Precedence--, InsertRecordLocator
	, [DATA_SOURCE])
			Select 
			cast(@Pricing_Date as date) 'effective_date'
			, 1 'org_entity_structure_fk'
			, @Zone_Segment_FK 'zone_entity_structure_fk'
			, @Zone_Segment_FK 'zone_segment_fk'
			, ps.PROD_SEGMENT_DATA_NAME_FK 'PROD_SEGMENT_DATA_NAME_FK'
			, ps.Prod_Segment_FK 'prod_segment_fk'
			, @Data_Name_FK 'data_name_fk'
			, 151000 'entity_data_name_fk'
			, @VALUE 'attribute_number'
			, @vALUE 'attribute_event_data'
			, 1 'record_status_cr_fk'
			, @User 'create_user'
			, Case when isnull(ps.Prod_Segment_Data_Name_FK, 0) = 110103 then ps.Prod_Segment_FK else Null end 'Product_Structure_FK'
			, Case when isnull(ps.Prod_Segment_Data_Name_FK, 0) = 110103 then Null else ps.Prod_Segment_FK end 'Prod_Data_Value_FK'
			, ps.precedence 'Precedence'
			, 'USER UPDATE' 'DATA_SOURCE'
			from #prodsegments ps
			where 1 = 1
				and precedence = @MaxPrec
End

If @OverWrite = 1
	and @Value <> 0
	Begin
			Print 'Insert SS 2'

			Insert into epacube.segments_settings
			(PROD_SEGMENT_DATA_NAME_FK, Prod_data_value_fk, prod_segment_fk, PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, ZONE_ENTITY_STRUCTURE_FK, zone_segment_fk, ENTITY_DATA_NAME_FK, data_name_fk
			, ATTRIBUTE_NUMBER, attribute_event_data, precedence, RECORD_STATUS_CR_FK, create_user
			, effective_date
			, [DATA_SOURCE])
			select PROD_SEGMENT_DATA_NAME_FK
			, case PROD_SEGMENT_DATA_NAME_FK when 110103 then null else prod_segment_fk end 'Prod_data_value_fk'
			, case PROD_SEGMENT_DATA_NAME_FK when 110103 then PRODUCT_STRUCTURE_FK else prod_segment_fk end 'Prod_segment_fk'
			, case PROD_SEGMENT_DATA_NAME_FK when 110103 then PRODUCT_STRUCTURE_FK else null end 'PRODUCT_STRUCTURE_FK'
			, 1 'ORG_ENTITY_STRUCTURE_FK'
			, zone_segment_fk 'zone_entity_structure_fk'
			, zone_segment_fk
			, 151000 'entity_data_name_fk'
			, 502046 'data_name_fk'
			, @value 'Attribute_Number'
			, @value 'attribute_event_data'
			, precedence
			, 1 'Record_Status_CR_FK'
			, @User 'Create_User'
			, cast(@Pricing_Date as date) 'Effective_Date'
			, 'USER UPDATE' 'DATA_SOURCE'
			from (
			Select ss.ATTRIBUTE_NUMBER, ss.product_structure_fk, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK, ss.precedence
			, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, ss.prod_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank
			, ss.reinstate_inheritance_date
			from epacube.segments_settings ss with (nolock) 
			inner join #prodsegments s on ss.prod_segment_fk = s.prod_segment_fk and ss.prod_segment_data_name_fk = s.prod_segment_data_name_fk and ss.ZONE_SEGMENT_FK = @zone_segment_fk
			where ss.data_name_fk = 502046
			and ss.RECORD_STATUS_CR_FK = 1
			and cast(s.precedence as int) between @MinPrec and @MaxPrec
			and (ss.reinstate_inheritance_date > @Pricing_Date or ss.reinstate_inheritance_date is null or (ss.prod_segment_fk = @prod_segment_fk and ss.PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK))
			) a where Drank = 1
				and PROD_SEGMENT_DATA_NAME_FK <> @Prod_Segment_Data_Name_FK
				and PROD_SEGMENT_FK <> @Prod_Segment_FK
				and a.attribute_number <> @value
	End

----**	Load items into staging table for scheduled processing into level gross.
	If @Immediate = 1 or @overwrite = 1
	Begin

	Set @price_change_effective_date = case when @Pricing_Date >= dateadd(d, -2, @price_change_effective_date) then dateadd(d, 7, @price_change_effective_date) else @price_change_effective_date end

		Delete sfp
		from [precision].stage_for_processing SFP
		inner join #prodsegments ps on sfp.Prod_Segment_Data_Name_FK = ps.prod_segment_data_name_fk
									and sfp.Prod_Segment_FK = ps.prod_segment_fk
		where sfp.price_change_effective_date = @Price_Change_Effective_Date
		and sfp.entity_structure_fk = @Zone_Segment_FK

		If (@Value <> 0 and @MinPrec = 5)
		Insert into [precision].stage_for_processing
		(entity_data_name_fk, processing_type, product_structure_fk, entity_structure_fk, data_name_fk, [value], username, price_change_effective_date, Prod_Segment_Data_Name_FK, Prod_Segment_FK, Prop_To_Data_Name_FK, [Immediate], OverWrite, tm_change_effective_date)
		Select 151000 'entity_data_name_fk'
		, case @Immediate when 1 then 'Level Gross MC' else 'Margin Changes' end 'Processing_Type'
		, ps.prod_segment_fk 'product_structure_fk'
		, @Zone_Segment_FK 'entity_structure_fk'
		, 502046 'Data_Name_FK'
		, case when @Value = 0 then tm_pct else @value end 'tm_pct'
		, @User 'username' 
		, @price_change_effective_date 'price_change_effective_date'
		, @Prod_Segment_Data_Name_FK
		, @Prod_Segment_FK
		, @Prop_To_Data_Name_FK
		, @Immediate
		, @OverWrite
		, @Pricing_Date 
		from #prodsegments ps
		inner join epacube.product_association pa on ps.prod_segment_fk = pa.product_structure_fk and pa.ENTITY_STRUCTURE_FK = @Zone_Segment_FK and pa.DATA_NAME_FK = 159450 
		left join epacube.product_attribute pa_500012 with (nolock) on ps.prod_segment_fk = pa_500012.PRODUCT_STRUCTURE_FK and ps.prod_segment_data_name_fk = 110103 and pa_500012.data_name_fk = 500012
		left join
					(	select * 
						from (
								Select cast(ss.ATTRIBUTE_NUMBER as Numeric(18, 4)) 'tm_pct', sp.product_structure_fk, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
								, dense_rank()over(partition by ss.zone_segment_fk, sp.product_structure_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank
								from epacube.segments_settings ss with (nolock) 
								inner join #prodsegments ps on ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk and ss.PROD_SEGMENT_FK = ps.prod_segment_fk
								inner join epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
								where ss.data_name_fk = 502046
								and ps.prod_segment_data_name_fk = 110103
								and ss.ZONE_SEGMENT_FK = @Zone_Segment_FK
								and ss.effective_date < @pricing_date
								and (ss.reinstate_inheritance_date >= @pricing_date or ss.reinstate_inheritance_date is null)
								and ss.RECORD_STATUS_CR_FK = 1
							) a where Drank = 1
					) tm on ps.prod_segment_fk = tm.PRODUCT_STRUCTURE_FK
			where 1 = 1
			--and pa_500012.PRODUCT_ATTRIBUTE_ID is null
			and pa.RECORD_STATUS_CR_FK = 1
			and ps.prod_segment_fk =	case	when isnull(tm.tm_pct, 0) <> @value then ps.prod_segment_fk else 0 end
		
		If cast(@Value as Numeric(18, 2)) <> 0.00 and @MinPrec > 5
		Insert into [precision].stage_for_processing
		(entity_data_name_fk, processing_type, product_structure_fk, entity_structure_fk, data_name_fk, [value], username, price_change_effective_date, Prod_Segment_Data_Name_FK, Prod_Segment_FK, Prop_To_Data_Name_FK, [Immediate], OverWrite, tm_change_effective_date)
		Select 151000 'entity_data_name_fk'
		, case @Immediate when 1 then 'Level Gross MC' else 'Margin Changes' end 'Processing_Type'
		, a.PRODUCT_STRUCTURE_FK 'product_structure_fk'
		, @Zone_Segment_FK 'entity_structure_fk'
		, 502046 'Data_Name_FK'
		, @Value 'tm_pct'
		, @User 'username' 
		, @price_change_effective_date 'price_change_effective_date'
		, @Prod_Segment_Data_Name_FK
		, @Prod_Segment_FK
		, @Prop_To_Data_Name_FK
		, @Immediate
		, @OverWrite 
		, @Pricing_Date
		from (
			Select cast(ss.ATTRIBUTE_NUMBER as Numeric(18, 4)) 'tm_pct', sp.product_structure_fk, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
							, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
							, ss.precedence
			from #prodsegments ps
			inner join epacube.segments_product sp1 with (nolock) on ps.prod_segment_data_name_fk = sp1.data_name_fk and ps.prod_segment_fk = sp1.prod_segment_fk and ps.precedence = @MinPrec
			inner join epacube.segments_product sp with (nolock) on sp1.PRODUCT_STRUCTURE_FK = sp.product_structure_fk --and sp.DATA_NAME_FK = 110103
			inner join epacube.segments_settings ss with (nolock) on sp.prod_segment_fk = ss.prod_segment_fk and sp.data_name_fk = ss.PROD_SEGMENT_DATA_NAME_FK
			inner join epacube.product_association pa on sp.PRODUCT_STRUCTURE_FK = pa.product_structure_fk and pa.ENTITY_STRUCTURE_FK = @Zone_Segment_FK and pa.DATA_NAME_FK = 159450 
			left join epacube.product_attribute pa_500012 with (nolock) on sp.PRODUCT_STRUCTURE_FK = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
			where 1 = 1
			and ss.Effective_Date < @Pricing_Date
			--and pa_500012.PRODUCT_ATTRIBUTE_ID is null
			and pa.RECORD_STATUS_CR_FK = 1
			and ss.data_name_fk = 502046
			and ss.zone_segment_fk = @Zone_Segment_FK
			and sp1.DATA_NAME_FK = @Prod_Segment_Data_Name_FK
			and sp1.PROD_SEGMENT_FK = @Prod_Segment_FK
			and (ss.reinstate_inheritance_date >= @pricing_date or ss.reinstate_inheritance_date is null)
			) a where DRank = 1	
			and PROD_SEGMENT_DATA_NAME_FK <> 110103
			and a.tm_pct <> @value
			and PRECEDENCE >= case when @overwrite = 1 then 1 else @maxprec end

	End

------Return data to UI to reflect changes		
		Select *
		into #t_results
		from	(
					Select 
					@pricing_date 'pricing_date'
					, zone_segment_fk
					, DATA_NAME_FK 'data_name'
					, case	when @Reinstate_Inheritance = 1 and PRECEDENCE = 5 and @value <> 0.0000 then Null
							when reinstate_inheritance_date is not null then null
							when @value = 0.0000 and ATTRIBUTE_NUMBER is not null then attribute_number
							when @value = 0.0000 and ATTRIBUTE_NUMBER is null then Null
							when @Value = @InheritVal and @Reinstate_Inheritance = 1 then Null
							when @Value = @InheritVal and @OverWrite = 1 and @Reinstate_Inheritance = 0 then Attribute_Number
							when @Reinstate_Inheritance = 1 and PRECEDENCE < @MaxPrec then null
							when @OverWrite = 0 then ATTRIBUTE_NUMBER
							when @CurValue = @Value then Null
							when @Value = @InheritVal then Null
							else @Value end 'value'
					, prod_segment_data_name_fk
					, prod_segment_fk
					, 1 'seq'
					, cast(@value as varchar(64))'sort_value'
					, precedence
					--, 'This is a test of the user message functionality.' 'USER_MSG'
					, reinstate_inheritance_date
					, segments_settings_id
					from (
					Select ss.ATTRIBUTE_NUMBER, ss.prod_segment_fk, ss.ZONE_SEGMENT_FK, ss.SEGMENTS_SETTINGS_ID, ss.PROD_SEGMENT_DATA_NAME_FK
					, dense_rank()over(partition by ss.ZONE_SEGMENT_FK, ss.prod_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank
					, ss.Effective_Date, ss.data_name_fk, ss.precedence
					, ss.reinstate_inheritance_date
					from epacube.segments_settings ss with (nolock) 
					left join epacube.PRODUCT_ASSOCIATION pa with (nolock) on ss.product_structure_fk = pa.product_structure_fk and ss.ZONE_ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK and pa.DATA_NAME_FK = 159450
					inner join #prodsegments S on ss.PROD_SEGMENT_FK = s.prod_segment_fk 
					where 1 = 1
					and		ss.data_name_fk = @data_name_fk
							and ss.Effective_Date <= @Pricing_Date
							and ss.zone_segment_fk = @Zone_Segment_FK
							and isnull(pa.RECORD_STATUS_CR_FK, 0) = case when ss.PROD_SEGMENT_DATA_NAME_FK = 110103 then 1 else isnull(pa.RECORD_STATUS_CR_FK, 0) end
							and ss.RECORD_STATUS_CR_FK = 1
							and (ss.reinstate_inheritance_date >= @pricing_date or ss.reinstate_inheritance_date is null)
							and 
								(
									(cast(s.precedence as int) between @MinPrec and @MaxPrec and @overwrite = 1)
									or
									(ss.PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK and ss.PROD_SEGMENT_FK = @Prod_Segment_FK and @overwrite = 0)
									or
									(ss.PROD_SEGMENT_FK = @Prod_Segment_FK and @Reinstate_Inheritance = 0)
									or
									(s.alias_id in (select alias_id from #prodsegments where PROD_SEGMENT_DATA_NAME_FK = s.Prod_Segment_Data_Name_FK and PROD_SEGMENT_FK = s.Prod_Segment_FK))
								)

					) a where Drank = 1

					Union
					--Send null value back to UI when value = value to inherit
					Select @Pricing_Date 'pricing_date'
					, @Zone_Segment_FK 'zone_segment_fk'
					, @Data_Name_FK 'data_name'
					, Null 'value'
					, @Prod_Segment_Data_Name_FK 'prod_segment_data_name_fk'
					, Prod_Segment_FK 'prod_segment_fk'
					, 1 'seq'
					, cast(@value as varchar(64)) 'sort_value'
					, precedence
					--, 'This is a test of the user message functionality.' 'USER_MSG'
					, @reinstate_inheritance_date 'reinstate_inheritance_date'
					, Null 'Segments_Settings_ID'
					from #prodsegments
					where 1 = 1
					and precedence <= @MaxPrec
					and
						(
							(
								(
									((@value = 0 OR (@value = @InheritVal and @Reinstate_Inheritance = 1)) and precedence = @MaxPrec)
									OR
									(@Value = @InheritVal and precedence < @MaxPrec)
									OR
									(@Value = @InheritVal and @CurValue = 0 and @Reinstate_Inheritance = 1)
								)
							and @OverWrite = 1
							)
						OR
							(
								@Overwrite = 0 and @CurValue = 0 and precedence = @MaxPrec and (@Value = 0 or @value = @InheritVal)
							)
						)
			) results

		If (select count(*) from #t_results) = 0
			Insert into #t_results
			Select @Pricing_Date 'pricing_date'
			, @Zone_Segment_FK 'zone_segment_fk'
			, @Data_Name_FK 'data_name'
			, Null 'value'
			, @Prod_Segment_Data_Name_FK 'prod_segment_data_name_fk'
			, @Prod_Segment_FK 'prod_segment_fk'
			, 1 'seq'
			, cast(@value as varchar(64)) 'sort_value'
			, 5 precedence
			--, 'This is a test of the user message functionality.' 'USER_MSG'
			, @reinstate_inheritance_date 'reinstate_inheritance_date'
			, Null 'Segments_Settings_ID'

	Select * from #t_results

eof:


--Select @Value '@Val', @CurValue '@CurValue', @InheritVal '@InheritVal', @data_name_fk '@data_name_fk', @Prod_Segment_Data_Name_FK '@Prod_Segment_Data_Name_FK', @Prod_Segment_FK '@Prod_Segment_FK', @Reinstate_Inheritance '@Reinstate_Inheritance', (select value from epacube.entity_identification where ENTITY_DATA_NAME_FK = 151000 and ENTITY_STRUCTURE_FK = @Zone_Segment_FK) 'zone_id', @MaxPrec '@MaxPrec'


--For Testing Only  Be sure to uncomment above @SSID section
/*
select @Value '@Value', @CurValue '@CurValue', @InheritVal '@InheritVal', @Immediate '@Immediate', @OverWrite '@OverWrite', @Reinstate_Inheritance '@Reinstate_Inheritance', @MinPrec '@MinPrec', @MaxPrec '@MaxPrec', @SSID '@SSID'
, @Pricing_Date '@Pricing_Date'


Declare @Inherit_prod_segment_fk bigint
Set @Inherit_prod_segment_fk = (select prod_segment_fk from epacube.epacube.segments_settings where segments_settings_id = @SSID)

Select prod_segment_fk, ZONE_SEGMENT_FK, Attribute_Number, Precedence, update_timestamp, update_user, Effective_Date, reinstate_inheritance_date
from epacube.segments_settings where ZONE_SEGMENT_FK = @Zone_Segment_FK and prod_segment_fk = @Inherit_prod_segment_fk and data_name_fk = 502046
order by effective_date desc

Select * from #prodsegments
*/




--Select ss.*
--							from #prodsegments ps
--							inner join epacube.segments_product sp1 with (nolock) on ps.prod_segment_data_name_fk = sp1.data_name_fk and ps.prod_segment_fk = sp1.prod_segment_fk and ps.precedence = 12
--							inner join epacube.segments_product sp with (nolock) on sp1.PRODUCT_STRUCTURE_FK = sp.product_structure_fk
--							inner join epacube.segments_settings ss with (nolock) on sp.prod_segment_fk = ss.prod_segment_fk and sp.data_name_fk = ss.PROD_SEGMENT_DATA_NAME_FK
--							inner join epacube.product_association pa on sp1.PRODUCT_STRUCTURE_FK = pa.product_structure_fk and pa.ENTITY_STRUCTURE_FK = 113234 and pa.DATA_NAME_FK = 159450 
--							--left join epacube.product_attribute pa_500012 with (nolock) on sp.PRODUCT_STRUCTURE_FK = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
--							where 1 = 1
--							and ss.Effective_Date < '2020-04-15'
--							--and pa_500012.PRODUCT_ATTRIBUTE_ID is null
--							--and pa.RECORD_STATUS_CR_FK = 1
--							and ss.data_name_fk = 502046
--							and ss.zone_segment_fk = 113234
--							and (ss.reinstate_inheritance_date > '2020-04-15' or ss.reinstate_inheritance_date is null)
--							and ss.PRECEDENCE > 12
--							and ss.PROD_SEGMENT_FK in (select parent_prod_segment_fk from #prodsegments)

--Select ss.*
--							from #prodsegments ps
--							inner join epacube.segments_settings ss with (nolock) on ps.prod_segment_fk = ss.prod_segment_fk and ps.prod_segment_data_name_fk = ss.PROD_SEGMENT_DATA_NAME_FK
--							where 1 = 1
--							and ss.Effective_Date < '2020-04-15'
--							and ss.data_name_fk = 502046
--							and ss.zone_segment_fk = 113234
--							and (ss.reinstate_inheritance_date > '2020-04-15' or ss.reinstate_inheritance_date is null)
--							and ss.PRECEDENCE > 12
--							and ss.PROD_SEGMENT_FK in (select parent_prod_segment_fk from #prodsegments)
