﻿--A_epaMAUI_PERFORM_MARGIN_RESULTS_UPDATE


-- Copyright 2012
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: Update the Cost Values on ALL the EVENT_CALC in the Job after calculating
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        05/03/2012   Initial SQL Version
-- GS		 09/23/2013	  Changed "IN" clauses to Joins to improve performance
--
CREATE PROCEDURE [marginmgr].[PERFORM_MARGIN_RESULTS_UPDATE] 
   ( @in_analysis_job_fk BIGINT,  @in_result_data_name_fk  INT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of marginmgr.PERFORM_MARGIN_RESULTS_UPDATE.' 
               + ' JOB:: ' + CAST ( ISNULL (@in_analysis_job_fk, 0 ) AS VARCHAR(16) )
               + ' RESULT:: ' + CAST ( ISNULL (@IN_RESULT_DATA_NAME_FK, 0 ) AS VARCHAR(16) )

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;



------------------------------------------------------------------------------------------
--   USING EVENT_CALC result for either 111501 or 111401
--    Update the EVENT_CALC rows within the same JOB that 
--    have the same RESULT_TYPE_CR_FK and product/org/customer
------------------------------------------------------------------------------------------
Update EC
SET	REBATE_CB_AMT = ec_rbt.RESULT
, REBATED_COST_CB_AMT = isnull(ec_rbtd_cst.RESULT, EC.PRICING_COST_BASIS_AMT - ec_rbt.RESULT)
, ORDER_COGS_AMT = isnull(isnull(ec_rbtd_cst.RESULT, EC.PRICING_COST_BASIS_AMT - ec_rbt.RESULT), EC.PRICING_COST_BASIS_AMT)
, REBATE_BUY_AMT = ec_buy.RESULT 
, SELL_PRICE_CUST_AMT = ec_sell.RESULT
from synchronizer.EVENT_CALC EC
Left join synchronizer.event_calc ec_rbt with (nolock) on ec.product_structure_fk = ec_rbt.product_structure_fk and ec.ORG_ENTITY_STRUCTURE_FK = ec_rbt.ORG_ENTITY_STRUCTURE_FK and ec.CUST_ENTITY_STRUCTURE_FK = ec_rbt.CUST_ENTITY_STRUCTURE_FK and ec_rbt.result_data_name_fk = 111501
Left join synchronizer.event_calc ec_rbtd_cst with (nolock) on ec.product_structure_fk = ec_rbtd_cst.product_structure_fk and ec.ORG_ENTITY_STRUCTURE_FK = ec_rbtd_cst.ORG_ENTITY_STRUCTURE_FK and ec.CUST_ENTITY_STRUCTURE_FK = ec_rbtd_cst.CUST_ENTITY_STRUCTURE_FK and ec_rbtd_cst.result_data_name_fk = 111401
Left join synchronizer.event_calc ec_buy with (nolock) on ec.product_structure_fk = ec_buy.product_structure_fk and ec.ORG_ENTITY_STRUCTURE_FK = ec_buy.ORG_ENTITY_STRUCTURE_FK and ec.CUST_ENTITY_STRUCTURE_FK = ec_buy.CUST_ENTITY_STRUCTURE_FK and ec_buy.result_data_name_fk = 111502
Left join synchronizer.event_calc ec_sell with (nolock) on ec.product_structure_fk = ec_sell.product_structure_fk and ec.ORG_ENTITY_STRUCTURE_FK = ec_sell.ORG_ENTITY_STRUCTURE_FK and ec.CUST_ENTITY_STRUCTURE_FK = ec_sell.CUST_ENTITY_STRUCTURE_FK and ec_sell.result_data_name_fk = 111602
WHERE 1 = 1
AND   ec.JOB_FK = @in_analysis_job_fk


--UPDATE synchronizer.EVENT_CALC
--SET    REBATE_CB_AMT     = ( SELECT EC.RESULT
--		                            FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
--		                            WHERE EC.JOB_FK = @in_analysis_job_fk
--		                            AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
--		                            AND   EC.PRODUCT_STRUCTURE_FK = synchronizer.EVENT_CALC.PRODUCT_STRUCTURE_FK
--		                            AND   EC.ORG_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.ORG_ENTITY_STRUCTURE_FK
--		                            AND   EC.CUST_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.CUST_ENTITY_STRUCTURE_FK 				                     
--                            )
--      ,REBATED_COST_CB_AMT  = ( SELECT EC.PRICING_COST_BASIS_AMT - EC.RESULT   
--		                            FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
--		                            WHERE EC.JOB_FK = @in_analysis_job_fk
--		                            AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
--		                            AND   EC.PRODUCT_STRUCTURE_FK = synchronizer.EVENT_CALC.PRODUCT_STRUCTURE_FK
--		                            AND   EC.ORG_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.ORG_ENTITY_STRUCTURE_FK
--		                            AND   EC.CUST_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.CUST_ENTITY_STRUCTURE_FK 
--		                      )
--WHERE 1 = 1
--AND   synchronizer.EVENT_CALC.JOB_FK = @in_analysis_job_fk
--AND   @in_result_data_name_fk = 111501

--UPDATE synchronizer.EVENT_CALC
--SET    REBATE_CB_AMT     = ( SELECT EC.RESULT - EC.PRICING_COST_BASIS_AMT 
--		                            FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
--		                            WHERE EC.JOB_FK = @in_analysis_job_fk
--		                            AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
--		                            AND   EC.PRODUCT_STRUCTURE_FK = synchronizer.EVENT_CALC.PRODUCT_STRUCTURE_FK
--		                            AND   EC.ORG_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.ORG_ENTITY_STRUCTURE_FK
--		                            AND   EC.CUST_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.CUST_ENTITY_STRUCTURE_FK 
--		                   )
--      ,REBATED_COST_CB_AMT  = ( SELECT EC.RESULT  
--		                            FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
--		                            WHERE EC.JOB_FK = @in_analysis_job_fk
--		                            AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
--		                            AND   EC.PRODUCT_STRUCTURE_FK = synchronizer.EVENT_CALC.PRODUCT_STRUCTURE_FK
--		                            AND   EC.ORG_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.ORG_ENTITY_STRUCTURE_FK
--		                            AND   EC.CUST_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.CUST_ENTITY_STRUCTURE_FK 
--		                      )
--WHERE 1 = 1
--AND   synchronizer.EVENT_CALC.JOB_FK = @in_analysis_job_fk
--AND   @in_result_data_name_fk = 111401



--UPDATE synchronizer.EVENT_CALC
--SET    ORDER_COGS_AMT       =  ( SELECT CASE WHEN  ISNULL ( EC.REBATED_COST_CB_AMT, 0 ) = 0
--									 THEN EC.PRICING_COST_BASIS_AMT
--									 ELSE CASE WHEN (  ISNULL ( EC.REBATED_COST_CB_AMT, 0 ) <  ISNULL ( EC.PRICING_COST_BASIS_AMT, 0 ) )
--												THEN  ISNULL ( EC.REBATED_COST_CB_AMT, 0 )
--												ELSE  ISNULL ( EC.PRICING_COST_BASIS_AMT, 0 )      
--												END      
--									END  
--		                            FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
--		                            WHERE EC.JOB_FK = @in_analysis_job_fk
--		                            AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
--		                            AND   EC.PRODUCT_STRUCTURE_FK = synchronizer.EVENT_CALC.PRODUCT_STRUCTURE_FK
--		                            AND   EC.ORG_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.ORG_ENTITY_STRUCTURE_FK
--		                            AND   EC.CUST_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.CUST_ENTITY_STRUCTURE_FK 
--                               )
--WHERE 1 = 1
--AND   synchronizer.EVENT_CALC.JOB_FK = @in_analysis_job_fk
--AND   @in_result_data_name_fk IN ( 111401, 111501 )



--UPDATE synchronizer.EVENT_CALC
--SET    REBATE_BUY_AMT     = ( SELECT EC.RESULT  
--		                            FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
--		                            WHERE EC.JOB_FK = @in_analysis_job_fk
--		                            AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
--		                            AND   EC.PRODUCT_STRUCTURE_FK = synchronizer.EVENT_CALC.PRODUCT_STRUCTURE_FK
--		                            AND   EC.ORG_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.ORG_ENTITY_STRUCTURE_FK
--		                            AND   EC.CUST_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.CUST_ENTITY_STRUCTURE_FK 
--		                   )
--WHERE 1 = 1
--AND   synchronizer.EVENT_CALC.JOB_FK = @in_analysis_job_fk
--AND   @in_result_data_name_fk = 111502




--UPDATE synchronizer.EVENT_CALC
--SET    SELL_PRICE_CUST_AMT     = ( SELECT EC.RESULT  
--		                            FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
--		                            WHERE EC.JOB_FK = @in_analysis_job_fk
--		                            AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
--		                            AND   EC.PRODUCT_STRUCTURE_FK = synchronizer.EVENT_CALC.PRODUCT_STRUCTURE_FK
--		                            AND   EC.ORG_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.ORG_ENTITY_STRUCTURE_FK
--		                            AND   EC.CUST_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.CUST_ENTITY_STRUCTURE_FK 
--		                   )
--WHERE 1 = 1
--AND   synchronizer.EVENT_CALC.JOB_FK = @in_analysis_job_fk
--AND   @in_result_data_name_fk = 111602



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of marginmgr.PERFORM_MARGIN_RESULTS_UPDATE'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of marginmgr.PERFORM_MARGIN_RESULTS_UPDATE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
