﻿-- =============================================
-- Author:		Gary Stone
-- Create date: May 4, 2018
---- Description:	Update Retail_Setting_Updates_Zone
-- =============================================
CREATE PROCEDURE [marginmgr].[Retail_Setting_Updates_Zone]
@Pricing_Date Date --= getdate()
, @Zone_Segment_FK bigint
, @Data_Name Varchar(64) --(Column_Name)
, @Value varchar(16) = Null
, @Prod_Segment_Data_Name_FK bigint	= Null
, @Prod_Segment_FK bigint = Null
, @Prop_To_Data_Name_FK bigint = Null
, @Immediate int = 0
, @OverWrite int = 0
, @Reinstate_Inheritance int = 0
, @User varchar(64)

AS

Declare @Cutoff as time
Declare @Time varchar(20)

Set @Cutoff = (select value from epacube.epacube_params WHERE EPACUBE_PARAMS_ID = 112000)

Set @Time = replace(convert(varchar(20), @Cutoff, 22), ':00 ', ' ')

If cast(getdate() as time) >= @Cutoff and @Pricing_Date <= cast(getdate() as date)
Begin
	Select
	(Select	'You have missed today''s cutoff time of ' + @Time + '.<br><br>The values just entered have not been accepted.<br><br>Please change the effective date to tomorrow or later, refresh your screen, and re-enter your changes') 'USER_MSG'
	, 1 'reset_values'
	, 5000 'user_msg_timeout_ms'
	goto eof
End

Set @Value = Case when @Value = '' then null else @Value end

insert into precision.user_call_statements
(create_user, ui_name, procedure_called, procedure_parameters, prod_segment_fk, entity_segment_fk)
Select @user 'create_user'
, 'Zone Price Settings' 'ui_name'
, '[marginmgr].[Retail_Setting_Updates_Zone]' 'procedure_called'
, '''' + cast(@pricing_date as varchar(24)) + ''', ' + cast(@Zone_Segment_FK as varchar(24)) + ', ''' + @Data_Name + ''', ' + isnull(@value, 'null') + ', ' + isnull(cast(@Prod_Segment_Data_Name_FK as varchar(24)), 'null')
	 + ', ' + isnull(cast(@Prod_Segment_FK as varchar(24)), 'null') + ', ' + isnull(cast(@Prop_To_Data_Name_FK as varchar(24)), 'null') + ', ' + cast(@Immediate as varchar(8)) + ', ' 
	+ cast(@OverWrite as varchar(8)) + ', ' + cast(@Reinstate_Inheritance as varchar(8)) + ', ''' + @User + '''' 'procedure_parameters'
, @Prod_Segment_FK 'prod_segment_fk', @Zone_Segment_FK 'entity_segment_fk'

If @Data_Name in ('LG_FLAG', 'USE_LVL_GRS_PRICE_POINTS_FLAG', 'URM_LVL_GRS_ACTIVATION_FLAG')
Begin
	exec [marginmgr].[Retail_Setting_Updates_Zone_Flags]
	@Pricing_Date
	, @Zone_Segment_FK
	, @Data_Name
	, @Value
	, @Prod_Segment_Data_Name_FK
	, @Prod_Segment_FK
	, @Reinstate_Inheritance
	, @User

	goto eof
End

If @Data_Name like 'PRC_ADJ_DIG_%' or @Data_Name in ('REQUIRED_PRICE_CHANGE_AMOUNT', 'PRENOTIFY_DAYS')
Begin

	Set @Value = Case	when @Value = '' then null 
						else @Value end

	exec [marginmgr].[Retail_Setting_Updates_Zone_Dollar_Amounts]
	@Pricing_Date
	, @Zone_Segment_FK
	, @Data_Name
	, @Value
	, @Prod_Segment_Data_Name_FK
	, @Prod_Segment_FK
	, @Reinstate_Inheritance
	, @User

	goto eof
End

If @Data_Name = 'tm_pct'
Begin

	Set @Value = Case	when isnull(cast(@Value as Numeric(18, 4)), 0) <= 0 then null else @Value end

	exec [marginmgr].[Retail_Setting_Updates_Zone_TM]
	@Pricing_Date
	, @Zone_Segment_FK
	, @Data_Name
	, @Value
	, @Prod_Segment_Data_Name_FK
	, @Prod_Segment_FK
	, @Prop_To_Data_Name_FK
	, @Immediate
	, @OverWrite
	, @Reinstate_Inheritance
	, @User

	goto eof
End

eof:

/*--*****	Code to Reinstate Inheritance ***** ---

	Update ss
	set reinstate_inheritance_date = @reinstate_inheritance_date
	from epacube.segments_settings ss
	where PROD_DATA_VALUE_FK = 1014719248
	and data_name_fk = 144902
	and ZONE_SEGMENT_FK = 113230
	and effective_date <= @Pricing_Date
	and reinstate_inheritance_date is null

*/--*****	Code to Reinstate Inheritance ***** ---

