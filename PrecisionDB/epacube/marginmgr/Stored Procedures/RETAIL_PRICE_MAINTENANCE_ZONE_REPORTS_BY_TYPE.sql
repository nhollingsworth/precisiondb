﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [marginmgr].[RETAIL_PRICE_MAINTENANCE_ZONE_REPORTS_BY_TYPE]
 
	@Item_Change_Type_CR_FK int = 655
	, @Calc_Date Varchar(16) = Null
	, @ShowSQL int = 0

AS

	SET NOCOUNT ON;


/*
	Declare @Price_Mgmt_Type_CR_FK int = 650
	Declare @Item_Change_Type_CR_FK int = 656
	Declare @Qualified_Status_CR_FK int = 653
	Declare @ByZones int = 1 --1 = By Zone; 0 = Multi-Zone
	Declare @ShowSQL int = 9
	Declare @Price_Change_Effective_Date varchar(16) = '2018-09-14'
	Declare @Calc_Date Varchar(16)
*/
	
	Set @Calc_Date = Case when isnull(@Calc_Date, '') = '' then CAST(getdate() + 6 - datepart(dw, getdate()) AS DATE) else @Calc_Date end


Declare @SQL_Code varchar(Max)

---- Regular Items and Margin Changes
If @Item_Change_Type_CR_FK in (655, 661)
Begin
Set @SQL_Code = '
		Select 
		PRC.PRICESHEET_RETAIL_CHANGES_ID
		, PRC.ALIAS_ID
		--, PRC.PRICE_MGMT_TYPE_CR_FK
		, (Select code from epacube.code_ref where code_ref_id = PRC.PRICE_MGMT_TYPE_CR_FK) ''PRICE_MGMT_TYPE''
		--, PRC.ITEM_CHANGE_TYPE_CR_FK
		, (Select code from epacube.code_ref where code_ref_id = PRC.ITEM_CHANGE_TYPE_CR_FK) ''ITEM_CHANGE_TYPE''
		--, PRC.QUALIFIED_STATUS_CR_FK
		, (Select code from epacube.code_ref where code_ref_id = PRC.QUALIFIED_STATUS_CR_FK) ''QUALIFIED_STATUS''
		--, PRC.Parity_Child
		, PRC.Parity_PL_Parent_Structure_FK
		, PRC.Parity_Size_Parent_Structure_FK
		, PRC.Parity_Alias_PL_Parent_Structure_FK
		, PRC.Parity_Alias_SIZE_Parent_Structure_FK
		, PRC.PRODUCT_STRUCTURE_FK
		, PRC.ZONE_ENTITY_STRUCTURE_FK
		, PRC.ZONE
		, PRC.ZONE_NAME
		, PRC.SUBGROUP_ID
		, PRC.SUBGROUP_DESCRIPTION
		, PRC.ITEM
		, PRC.ITEM_DESCRIPTION
		, PRC.STORE_PACK
		, PRC.SIZE_UOM
		, PRC.CUR_COST
		, PRC.PRICE_MULTIPLE_CUR
		, PRC.PRICE_CUR
		, PRC.CUR_GM_PCT
		, PRC.COST_CHANGE
		, PRC.PRICE_CHANGE
		, PRC.NEW_COST
		, PRC.PRICE_MULTIPLE_NEW
		, PRC.PRICE_NEW
		, PRC.NEW_GM_PCT
		, PRC.TARGET_MARGIN
		, PRC.TM_SOURCE
		, PRC.SRP_ADJUSTED
		, PRC.GM_ADJUSTED
		, PRC.TM_ADJUSTED
		, PRC.HOLD_CUR_PRICE
		, PRC.REVIEWED
		, CONVERT(VARCHAR(8), PRC.PRICE_CHANGE_EFFECTIVE_DATE, 1) PRICE_CHANGE_EFFECTIVE_DATE
		, PRC.UPDATE_USER_PRICE
		, PRC.UPDATE_USER_SET_TM
		, PRC.UPDATE_USER_HOLD
		, PRC.UPDATE_USER_REVIEWED
		, PRC.IND_MARGIN_TOL
		, pi_c.value + '' '' + pd.[description] + '' '' + ISNULL(pa_500001.attribute_event_data, '''') + ''/'' + ISNULL(pa_500002.attribute_event_data, '''') '' BREAKDOWN_ITEM''
		, pb.qty '' BREAKDOWN_QTY''
		, PB.STATE '' BREAKDOWN_STATE''
		FROM MARGINMGR.PRICESHEET_RETAIL_CHANGES PRC with (nolock)
		inner join epacube.ENTITY_ATTRIBUTE ea with (nolock) on prc.ZONE_ENTITY_STRUCTURE_FK = ea.ENTITY_STRUCTURE_FK and ea.data_name_fk = 502060
		inner join epacube.data_value dv with (nolock) on ea.DATA_VALUE_FK = dv.data_value_id and dv.value in (''H'', ''R'')
		left join epacube.epacube.product_bom pb with (nolock) on prc.product_structure_fk = pb.product_structure_fk and pb.data_name_fk = 500213
		left join epacube.epacube.product_identification pi_c with (nolock) on pb.child_product_structure_fk = pi_c.product_structure_fk and pi_c.data_name_fk = 110100
		left join epacube.epacube.product_description pd with (nolock) on pi_c.product_structure_fk = pd.product_structure_fk and pd.data_name_fk = 110401
		left join [epacube].[PRODUCT_ATTRIBUTE] pa_500001 with (nolock) on pi_c.Product_Structure_FK = pa_500001.PRODUCT_STRUCTURE_FK and pa_500001.data_name_fk = 500001
		left join [epacube].[PRODUCT_ATTRIBUTE] pa_500002 with (nolock) on pi_c.Product_Structure_FK = pa_500002.PRODUCT_STRUCTURE_FK and pa_500002.data_name_fk = 500002
		where 1 = 1
		and cast(prc.Item_Change_Type_CR_FK as varchar(8)) in (655, 661)
		and cast(prc.PRICE_CHANGE_EFFECTIVE_DATE as date) = ''' + @Calc_Date + ''''

End
--and cast(prc.PRICE_CHANGE_EFFECTIVE_DATE as date) = cast(getdate() + 6 - datepart(dw, getdate()) as date)
ELSE

---- ALIAS ITEMS
If @Item_Change_Type_CR_FK in (656, 662)
Begin
Set @SQL_Code = '
		Select 
		PRICESHEET_RETAIL_CHANGES_ID
		, PRC.ALIAS_ID
		, (Select code from epacube.code_ref where code_ref_id = PRC.PRICE_MGMT_TYPE_CR_FK) ''PRICE_MGMT_TYPE''
		--, PRC.ITEM_CHANGE_TYPE_CR_FK
		, (Select code from epacube.code_ref where code_ref_id = PRC.ITEM_CHANGE_TYPE_CR_FK) ''ITEM_CHANGE_TYPE''
		--, PRC.QUALIFIED_STATUS_CR_FK
		, (Select code from epacube.code_ref where code_ref_id = PRC.QUALIFIED_STATUS_CR_FK) ''QUALIFIED_STATUS''
		--, PRC.Parity_Child
		, PRC.Parity_PL_Parent_Structure_FK
		, PRC.Parity_Size_Parent_Structure_FK
		, PRC.Parity_Alias_PL_Parent_Structure_FK
		, PRC.Parity_Alias_SIZE_Parent_Structure_FK
		, PRC.PRODUCT_STRUCTURE_FK
		, PRC.ZONE_ENTITY_STRUCTURE_FK
		, PRC.ZONE
		, PRC.ZONE_NAME
		, PRC.SUBGROUP_ID
		, PRC.SUBGROUP_DESCRIPTION
		, PRC.ITEM
		, PRC.ITEM_DESCRIPTION
		, PRC.STORE_PACK
		, PRC.SIZE_UOM
		, PRC.CUR_COST
		, PRC.PRICE_MULTIPLE_CUR
		, PRC.PRICE_CUR
		, PRC.CUR_GM_PCT
		, PRC.COST_CHANGE
		, PRC.PRICE_CHANGE
		, PRC.NEW_COST
		, PRC.PRICE_MULTIPLE_NEW
		, PRC.PRICE_NEW
		, PRC.NEW_GM_PCT
		, PRC.TARGET_MARGIN
		, PRC.TM_SOURCE
		, PRC.SRP_ADJUSTED
		, PRC.GM_ADJUSTED
		, PRC.TM_ADJUSTED
		, PRC.HOLD_CUR_PRICE
		, PRC.REVIEWED
		, CONVERT(VARCHAR(8), PRC.PRICE_CHANGE_EFFECTIVE_DATE, 1) PRICE_CHANGE_EFFECTIVE_DATE
		, PRC.UPDATE_USER_PRICE
		, PRC.UPDATE_USER_SET_TM
		, PRC.UPDATE_USER_HOLD
		, PRC.UPDATE_USER_REVIEWED
		, PRC.IND_COST_DELTA
		, PRC.IND_MARGIN_TOL
		, pi_c.value + '' '' + pd.[description] + '' '' + ISNULL(pa_500001.attribute_event_data, '''') + ''/'' + ISNULL(pa_500002.attribute_event_data, '''') '' BREAKDOWN_ITEM''
		, pb.qty '' BREAKDOWN_QTY''
		, PB.STATE '' BREAKDOWN_STATE''
		FROM MARGINMGR.PRICESHEET_RETAIL_CHANGES PRC with (nolock)
		inner join epacube.ENTITY_ATTRIBUTE ea with (nolock) on prc.ZONE_ENTITY_STRUCTURE_FK = ea.ENTITY_STRUCTURE_FK and ea.data_name_fk = 502060
		inner join epacube.data_value dv with (nolock) on ea.DATA_VALUE_FK = dv.data_value_id and dv.value in (''H'', ''R'')
		left join epacube.epacube.product_bom pb with (nolock) on prc.product_structure_fk = pb.product_structure_fk and pb.data_name_fk = 500213
		left join epacube.epacube.product_identification pi_c with (nolock) on pb.child_product_structure_fk = pi_c.product_structure_fk and pi_c.data_name_fk = 110100
		left join epacube.epacube.product_description pd with (nolock) on pi_c.product_structure_fk = pd.product_structure_fk and pd.data_name_fk = 110401
		left join [epacube].[PRODUCT_ATTRIBUTE] pa_500001 with (nolock) on pi_c.Product_Structure_FK = pa_500001.PRODUCT_STRUCTURE_FK and pa_500001.data_name_fk = 500001
		left join [epacube].[PRODUCT_ATTRIBUTE] pa_500002 with (nolock) on pi_c.Product_Structure_FK = pa_500002.PRODUCT_STRUCTURE_FK and pa_500002.data_name_fk = 500002
		where 1 = 1
		and prc.Item_Change_Type_CR_FK in (656, 657, 662)
		and cast(prc.PRICE_CHANGE_EFFECTIVE_DATE as date) = ''' + @Calc_Date + '''
'

--		and cast(prc.PRICE_CHANGE_EFFECTIVE_DATE as date) = cast(getdate() + 6 - datepart(dw, getdate()) as date)

	END
/*
else
---- PARITY ITEMS
If @Item_Change_Type_CR_FK in (658, 659)
BEGIN
Set @SQL_Code = '
	SELECT * FROM (
		Select 
		PRC.PRICESHEET_RETAIL_CHANGES_ID
		, PARITY_PARENT_PRODUCT_STRUCTURE_FK
		, CASE WHEN PRC.PARITY_PARENT_PRODUCT_STRUCTURE_FK = PRC.PRODUCT_STRUCTURE_FK THEN ''PARENT'' WHEN PRC.PARITY_PARENT_PRODUCT_STRUCTURE_FK <> PRC.PRODUCT_STRUCTURE_FK THEN ''CHILD'' END ''PARITY_STATUS''
		, PRC.ALIAS
		, PRC.ALIAS_ID
		, PRC.PRICE_MGMT_TYPE_CR_FK
		, PRC.ITEM_CHANGE_TYPE_CR_FK
		, PRC.QUALIFIED_STATUS_CR_FK
		, PRC.PRODUCT_STRUCTURE_FK
		, PRC.ZONE_ENTITY_STRUCTURE_FK
		, PRC.ZONE
		, PRC.ZONE_NAME
		, PRC.SUBGROUP
		, PRC.SUBGROUP_DESCRIPTION
		, PRC.ITEM_SURROGATE
		, PRC.ITEM_DESCRIPTION
		, PRC.[PK/SIZE]
		, PRC.CUR_COST
		, PRC.PRICE_MULTIPLE_CUR
		, PRC.PRICE_CUR
		, PRC.CUR_GM_PCT
		, PRC.COST_CHANGE
		, PRC.PRICE_CHANGE
		, PRC.NEW_COST
		, PRC.PRICE_MULTIPLE_NEW
		, PRC.PRICE_NEW
		, PRC.NEW_GM_PCT
		, PRC.TARGET_MARGIN
		, PRC.TM_SOURCE
		, PRC.SRP_ADJUSTED
		, PRC.GM_ADJUSTED
		, PRC.TM_ADJUSTED
		, PRC.HOLD_CUR_PRICE
		, PRC.REVIEWED
		, CONVERT(VARCHAR(8), PRC.PRICE_CHANGE_EFFECTIVE_DATE, 1) PRICE_CHANGE_EFFECTIVE_DATE
		, PRC.UPDATE_USER_PRICE
		, PRC.UPDATE_USER_SET_TM
		, PRC.UPDATE_USER_HOLD
		, PRC.UPDATE_USER_REVIEWED
		FROM MARGINMGR.PRICESHEET_RETAIL_CHANGES PRC with (nolock)
		inner join epacube.ENTITY_ATTRIBUTE ea with (nolock) on prc.ZONE_ENTITY_STRUCTURE_FK = ea.ENTITY_STRUCTURE_FK and ea.data_name_fk = 502060
		inner join epacube.data_value dv with (nolock) on ea.DATA_VALUE_FK = dv.data_value_id and dv.value in (''H'', ''R'')
		where 1 = 1
		and prc.Price_Mgmt_Type_CR_FK = ' + cast(@Price_Mgmt_Type_CR_FK as varchar(8)) + '
		and prc.Item_Change_Type_CR_FK in (''660'', ' + cast(@Item_Change_Type_CR_FK as varchar(8)) + ')
		and prc.Qualified_Status_CR_FK = Case when ' + cast(@Price_Mgmt_Type_CR_FK as varchar(8)) + ' <> ''650'' OR prc.Item_Change_Type_CR_FK = 660 then prc.Qualified_Status_CR_FK else ' + cast(@Qualified_Status_CR_FK as varchar(8)) + ' end
		and cast(prc.PRICE_CHANGE_EFFECTIVE_DATE as date) = cast(getdate() + 6 - datepart(dw, getdate()) as date)
		AND PRC.PARITY_PARENT_PRODUCT_STRUCTURE_FK IN
		(
			SELECT PRC.PRODUCT_STRUCTURE_FK
			FROM MARGINMGR.PRICESHEET_RETAIL_CHANGES PRC with (nolock)
			where 1 = 1
			and prc.Price_Mgmt_Type_CR_FK = ' + cast(@Price_Mgmt_Type_CR_FK as varchar(8)) + '
			and prc.Item_Change_Type_CR_FK in (' + cast(@Item_Change_Type_CR_FK as varchar(8)) + ')
			and prc.Qualified_Status_CR_FK = Case when ' + cast(@Price_Mgmt_Type_CR_FK as varchar(8)) + ' <> ''650'' then prc.Qualified_Status_CR_FK else ' + cast(@Qualified_Status_CR_FK as varchar(8)) + ' end
			and cast(prc.PRICE_CHANGE_EFFECTIVE_DATE as date) = cast(getdate() + 6 - datepart(dw, getdate()) as date)
		)
		) A

		order by ' + Case @byZones when 1 then 'CAST(REVERSE(LEFT(REVERSE(ZONE_NAME), CHARINDEX('' '', REVERSE(ZONE_NAME)) - 1)) AS INT), ' else ' PARITY_PARENT_PRODUCT_STRUCTURE_FK ,' end + 
		Case @byZones when 1 then ' PARITY_PARENT_PRODUCT_STRUCTURE_FK, ' else 'CAST(REVERSE(LEFT(REVERSE(ZONE_NAME), CHARINDEX('' '', REVERSE(ZONE_NAME)) - 1)) AS INT), ' end + 'PARITY_STATUS DESC
'
END
*/

If @ShowSQL = 9 
Print(@SQL_Code)
else
Exec(@SQL_Code)
