﻿-- =============================================
-- Author:		Gary Stone
-- Create date: May 1, 2014
-- Description:	Capture Effective Dates by Data Name
-- =============================================

-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -----------------------------------------------
-- CV        03/23/2015  comment out unions for current values



CREATE PROCEDURE [marginmgr].[EVENT_EFFECTIVE_DATES_BY_DATA_NAME_APPEND]
	---@iMPORT_jOB_fk BIGINT
AS
BEGIN
DECLARE @import_job_fk BIGINT

	SET NOCOUNT ON;

	--set in order to call from special sql table
	set @iMPORT_jOB_fk = (select top 1 job_fk from import.import_record_data where IMPORT_PACKAGE_FK = 211000
	order by job_fk desc)


	Insert into MARGINMGR.EFFECTIVE_DATES_BY_DATA_NAME
	([import_job_fk], [Data_Name_Date_FK], [Effective_Date_Label], [Data_Name_To_Update_FK], [Data_Name_Label_to_Update], [Product_Structure_FK], [Org_Entity_Structure_FK], [Event_Effective_Date], [Actual_Effective_Date])
	Select 
		import_job_fk
		, Data_Name_FK 'Data_Name_Date_FK'
		, epacube.getdatanamelabel(Data_Name_FK) 'Effective_Date_Label'
		, Case Data_Name_FK When 100100551 then 100100416 When 100100552 then 231100 When 100100553 then 100100398  end 'Data_Name_To_Update_FK'
		, epacube.getdatanamelabel(Case Data_Name_FK When 100100551 then 100100416 When 100100552 then 231100 When 100100553 then 100100398 end) 'Data_Name_Label_to_Update'
		, Product_Structure_FK
		, Org_Entity_Structure_FK
		, Event_Effective_Date
		, Cast(New_Data as date) Actual_Effective_Date
	from synchronizer.event_data where import_job_fk = @Import_Job_FK and data_name_fk in (100100551, 100100552, 100100553)
	--Union
	--Select 
	--	import_job_fk
	--	, Data_Name_FK 'Data_Name_Date_FK'
	--	, epacube.getdatanamelabel(Data_Name_FK) 'Effective_Date_Label'
	--	, Case Data_Name_FK When 100100552 then 100100530 When 100100553 then 100100392  end 'Data_Name_To_Update_FK'
	--	, epacube.getdatanamelabel(Case Data_Name_FK When 100100552 then 100100530 When 100100553 then 100100392 end) 'Data_Name_Label_to_Update'
	--	, Product_Structure_FK
	--	, Org_Entity_Structure_FK
	--	, Event_Effective_Date
	--	, Cast(New_Data as date) Actual_Effective_Date
	--from synchronizer.event_data where import_job_fk = @Import_Job_FK and data_name_fk in (100100552, 100100553)
	Union
	Select 
		import_job_fk
		, Data_Name_FK 'Data_Name_Date_FK'
		, epacube.getdatanamelabel(Data_Name_FK) 'Effective_Date_Label'
		, 100100404 'Data_Name_To_Update_FK'
		, epacube.getdatanamelabel(100100404) 'Data_Name_Label_to_Update'
		, Product_Structure_FK
		, Org_Entity_Structure_FK
		, Event_Effective_Date
		, Cast(New_Data as date) Actual_Effective_Date
	from synchronizer.event_data where import_job_fk = @Import_Job_FK and data_name_fk in (100100553)
	Union
	Select 
		import_job_fk
		, Data_Name_FK 'Data_Name_Date_FK'
		, epacube.getdatanamelabel(Data_Name_FK) 'Effective_Date_Label'
		, Case Data_Name_FK When 100100551 then 100100416 When 100100552 then 231100 When 100100553 then 100100398 end 'Data_Name_To_Update_FK'
		, epacube.getdatanamelabel(Case Data_Name_FK When 100100551 then 100100416 When 100100552 then 231100 When 100100553 then 100100398 end) 'Data_Name_Label_to_Update'
		, Product_Structure_FK
		, Org_Entity_Structure_FK
		, Event_Effective_Date
		, Cast(New_Data as date) Actual_Effective_Date
	from synchronizer.event_data_history where import_job_fk = @Import_Job_FK and data_name_fk in (100100551, 100100552, 100100553)
	--Union
	--Select 
	--	import_job_fk
	--	, Data_Name_FK 'Data_Name_Date_FK'
	--	, epacube.getdatanamelabel(Data_Name_FK) 'Effective_Date_Label'
	--	, Case Data_Name_FK When 100100552 then 100100530 When 100100553 then 100100392  end 'Data_Name_To_Update_FK'
	--	, epacube.getdatanamelabel(Case Data_Name_FK When 100100552 then 100100530 When 100100553 then 100100392 end) 'Data_Name_Label_to_Update'
	--	, Product_Structure_FK
	--	, Org_Entity_Structure_FK
	--	, Event_Effective_Date
	--	, Cast(New_Data as date) Actual_Effective_Date
	--from synchronizer.event_data_history where import_job_fk = @Import_Job_FK and data_name_fk in (100100552, 100100553)
	Union
	Select 
		import_job_fk
		, Data_Name_FK 'Data_Name_Date_FK'
		, epacube.getdatanamelabel(Data_Name_FK) 'Effective_Date_Label'
		, 100100404 'Data_Name_To_Update_FK'
		, epacube.getdatanamelabel(100100404) 'Data_Name_Label_to_Update'
		, Product_Structure_FK
		, Org_Entity_Structure_FK
		, Event_Effective_Date
		, Cast(New_Data as date) Actual_Effective_Date
	from synchronizer.event_data_history where import_job_fk = @Import_Job_FK and data_name_fk in (100100553)

END
