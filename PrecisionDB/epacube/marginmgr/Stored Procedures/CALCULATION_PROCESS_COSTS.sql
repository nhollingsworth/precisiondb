﻿



-- Copyright 2009
--
-- Procedure set the COSTS on the EVENT_CALC rows for the Analysis Job
--    Pricing Cost and Margin Cost
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -----------------------------------------------
-- KS        05/08/2009  Initial Version
-- KS        10/28/2012  Performance Changes on PRICESHEET Select
-- GHS	     05/27/2015  Restructured

CREATE PROCEDURE [marginmgr].[CALCULATION_PROCESS_COSTS]
          @in_analysis_job_fk BIGINT
AS
BEGIN

DECLARE @l_sysdate    datetime
DECLARE @ls_stmt      varchar (1000)
DECLARE @ls_queue     nvarchar (4000)
DECLARE @l_exec_no    bigint
DECLARE @l_rows_processed    BIGINT

DECLARE @v_scenario_seq   int
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT
DECLARE @v_current_date datetime

DECLARE @job_id_tbl				table (analysis_job_id bigint)
DECLARE @v_analysis_job_fk		BIGINT
DECLARE @v_host_erp_system		varchar(32)

DECLARE @status_desc  varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int

BEGIN TRY
SET NOCOUNT ON
SET @l_sysdate = getdate ()
SET @v_current_date = GETDATE ()

SET @l_exec_no = 0;
SET @ls_stmt =  'started execution of MARGINMGR.CALCULATION_PROCESS_COSTS' 
               + ' JOB:: ' + CAST ( ISNULL (@in_analysis_job_fk, 0 ) AS VARCHAR(16) )

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
										@exec_id = @l_exec_no OUTPUT;


SET @v_host_erp_system = ( SELECT TOP 1 eep.value FROM epacube.epacube_params eep WITH (NOLOCK)
                           WHERE eep.application_scope_fk = 100 AND eep.NAME = 'ERP HOST' )
                           


------------------------------------------------------------------------------------------
--   CREATE  #TS_ORG_XREF
--		This table used for performance to group by warehouse/result for processing
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_ORG_XREF') is not null
	   drop table #TS_ORG_XREF;
	   
	   
	-- create temp table
	CREATE TABLE #TS_ORG_XREF(
	    TS_ORG_XREF_ID    BIGINT IDENTITY(1000,1) NOT NULL,
		ORG_ENTITY_STRUCTURE_FK             BIGINT NULL,	    	    
		XREF_ORG_ENTITY_STRUCTURE_FK        BIGINT NULL,
		 )


------------------------------------------------------------------------------------------
---  temporary solution... will work for everyone but multilevel orgs.. ie Region/Division
------------------------------------------------------------------------------------------

                
		INSERT INTO #TS_ORG_XREF(
			ORG_ENTITY_STRUCTURE_FK    	    	    
		   ,XREF_ORG_ENTITY_STRUCTURE_FK )
		SELECT 
			ES.ENTITY_STRUCTURE_ID
		   ,ES.ENTITY_STRUCTURE_ID 
		FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
		WHERE ES.ENTITY_CLASS_CR_FK = 10101 -- ORG

--- THROW IN THE PARENTS...
		INSERT INTO #TS_ORG_XREF(
			ORG_ENTITY_STRUCTURE_FK    	    	    
		   ,XREF_ORG_ENTITY_STRUCTURE_FK )
		SELECT 
			ES.ENTITY_STRUCTURE_ID
		   ,ES.PARENT_ENTITY_STRUCTURE_FK
		FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
		WHERE ES.ENTITY_CLASS_CR_FK = 10101 -- ORG
		AND   ES.PARENT_ENTITY_STRUCTURE_FK IS NOT NULL 		


--- THROW IN THE ROOTS...
		INSERT INTO #TS_ORG_XREF(
			ORG_ENTITY_STRUCTURE_FK    	    	    
		   ,XREF_ORG_ENTITY_STRUCTURE_FK )
		SELECT 
			ES.ENTITY_STRUCTURE_ID
		   ,1
		FROM EPACUBE.ENTITY_STRUCTURE ES WITH (NOLOCK)
		WHERE ES.ENTITY_CLASS_CR_FK = 10101 -- ORG


--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSOX_TSOXID ON #TS_ORG_XREF 
	(TS_ORG_XREF_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSOX_OESF_IXOESF] ON #TS_ORG_XREF 
	(ORG_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( XREF_ORG_ENTITY_STRUCTURE_FK )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSOX_XOESF_IOESF] ON #TS_ORG_XREF 
	(XREF_ORG_ENTITY_STRUCTURE_FK ASC
	)
	INCLUDE ( ORG_ENTITY_STRUCTURE_FK )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)



-------------------------------------------------------------------------------------
--   TEMP TABLE SELECT ALL FUTURE MARGIN COST BASIS OR PRICING COST BASIS EVENTS
--    Then Rank them by effective date asc select only ONE.
-------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_FUTURE_COST') is not null
	   drop table #TS_FUTURE_COST;
	   
	   
	-- create temp table
	CREATE TABLE #TS_FUTURE_COST(
	     EC_EVENT_FK       BIGINT NOT NULL
	    ,FUTURE_VALUE_FK   BIGINT NOT NULL
        ,DATA_NAME_FK   BIGINT NULL
	    ,COST_VALUE     NUMERIC (18,6) NULL	
	    ,EFFECTIVE_DATE DATETIME 
	    ,DRANK          SMALLINT
		 )


--------------------------------------------------------------------------------------------
--  CREATE INDEXES after INSERT   ---- INCLUDE COST_VALUE ??  REVISIT
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSFE_TSFEID ON #TS_FUTURE_COST
	(EC_EVENT_FK ASC, DATA_NAME_FK ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


--------------------------------------------------------------------------------------------
--  SXE Costs are from SHEET_RESULTS for Current;  and EVENT_DATA for FUTURE
--    V517 will be What IF and we will figure out requirements later
--------------------------------------------------------------------------------------------


IF @v_host_erp_system = 'SXE'
BEGIN


--------------------------------------------------------------------------------------------
--  SXE FUTURE AND WHATIF CHECK VALUES FROM EVENT_DATA .... ( ONLY FUTURE STATUS EVENTS )
--------------------------------------------------------------------------------------------


---- ALL FUTURE SCENARIOS  ( CHECK FOR FUTURE EVENTS )
    INSERT INTO #TS_FUTURE_COST
       ( EC_EVENT_FK, FUTURE_VALUE_FK, DATA_NAME_FK, COST_VALUE )
    SELECT B.EC_EVENT_FK
          ,B.FUTURE_VALUE_FK
          ,B.DATA_NAME_FK
          ,B.COST_VALUE
    FROM (
    SELECT A.EC_EVENT_FK
          ,A.FUTURE_VALUE_FK
          ,A.DATA_NAME_FK
          ,A.COST_VALUE
		  ,( DENSE_RANK() OVER ( PARTITION BY A.EC_EVENT_FK
		   ORDER BY A.PRECEDENCE ASC, A.EVENT_EFFECTIVE_DATE DESC, A.FUTURE_VALUE_FK ASC ) )AS DRANK
       FROM ( 
       SELECT    EC.EVENT_ID AS EC_EVENT_FK,
                 ED.EVENT_ID AS FUTURE_VALUE_FK,
                 DN.DATA_NAME_ID AS DATA_NAME_FK,
                 (SELECT CASE DS.COLUMN_NAME
					   WHEN 'NET_VALUE1' THEN  ED.NET_VALUE1_NEW 
					   WHEN 'NET_VALUE2' THEN  ED.NET_VALUE2_NEW  
					   WHEN 'NET_VALUE3' THEN  ED.NET_VALUE3_NEW  
					   WHEN 'NET_VALUE4' THEN  ED.NET_VALUE4_NEW 
					   WHEN 'NET_VALUE5' THEN  ED.NET_VALUE5_NEW 
					   WHEN 'NET_VALUE6' THEN  ED.NET_VALUE6_NEW 
					   END ) AS COST_VALUE,
			     ED.EVENT_EFFECTIVE_DATE,
				 ECT.PRECEDENCE,
				 ED.PRODUCT_STRUCTURE_FK,
				 ED.ORG_ENTITY_STRUCTURE_FK
			FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK )
			INNER JOIN synchronizer.EVENT_CALC EC WITH (NOLOCK)
			  ON  EC.JOB_FK = AJ.ANALYSIS_JOB_ID
			INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
			  ON  EC.PRODUCT_STRUCTURE_FK = ED.PRODUCT_STRUCTURE_FK 
			INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
			  ON DN.PARENT_DATA_NAME_FK = ED.DATA_NAME_FK AND DN.RECORD_STATUS_CR_FK = 1 
			INNER JOIN epacube.DATA_SET DS WITH (NOLOCK)
			  ON DS.DATA_SET_ID = DN.DATA_SET_FK 			
				--- BASIS VALUES ARE ORG TO ORG;  CORP TO ORG   OR ORG TO CORP 
			INNER JOIN  #TS_ORG_XREF TOX
			ON (     TOX.ORG_ENTITY_STRUCTURE_FK = EC.ORG_ENTITY_STRUCTURE_FK 
				AND  TOX.XREF_ORG_ENTITY_STRUCTURE_FK = ED.ORG_ENTITY_STRUCTURE_FK   )
			INNER JOIN epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
			ON (  ECT.RECORD_STATUS_CR_FK = 1
			AND   ECT.ENTITY_CLASS_CR_FK = 10101
			AND   ECT.DATA_NAME_FK = (  SELECT ES.DATA_NAME_FK 
										FROM epacube.ENTITY_STRUCTURE ES WITH (NOLOCK)
										WHERE ES.ENTITY_STRUCTURE_ID = ED.ORG_ENTITY_STRUCTURE_FK )    )			       
            WHERE 1 = 1
            AND   AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk
            AND   EC.RESULT_TYPE_CR_FK IN ( 711, 712 )  -- FUTURE  OR WHATIF          
            AND   EC.RULE_TYPE_CR_FK IN ( 312, 313 )            
            --AND   ED.EVENT_EFFECTIVE_DATE > ISNULL ( AJ.JOB_DATE, GETDATE () )  --- FUTURE           
            AND   ED.EVENT_EFFECTIVE_DATE > ISNULL ( AJ.RESULT_EFFECTIVE_DATE, GETDATE () )  --- FUTURE             
            AND   ED.EVENT_EFFECTIVE_DATE <= EC.RESULT_EFFECTIVE_DATE   -- <= COST EFF OR ANAL EFF DATES         
            AND   ED.EVENT_STATUS_CR_FK = 84 --- ONLY FUTURE APPROVED
			AND   DN.DATA_NAME_ID IN ( SELECT CAST ( EEP.VALUE AS INT )
			                            FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
			                            WHERE ( EEP.APPLICATION_SCOPE_FK = 100
			                            AND  EEP.NAME IN ( 'MARGIN COST BASIS', 'PRICING COST BASIS' ) )
			                         )
			-------			
		) A
		WHERE ISNULL ( A.COST_VALUE, 0 ) > 0  -- IF NO VALUE IN THE EVENT DO NOT INSERT
		) B
		WHERE B.DRANK = 1



------------------------------------------------------------------------------------------
---  UPDATE FUTURE COSTS FROM EVENT_DATA VALUES
------------------------------------------------------------------------------------------

     UPDATE synchronizer.EVENT_CALC 
     SET PRICING_COST_BASIS_AMT = TSFC.COST_VALUE
     FROM #TS_FUTURE_COST TSFC
     WHERE 1 = 1
     AND   synchronizer.EVENT_CALC.RESULT_TYPE_CR_FK IN ( 711, 712 )  -- FUTURE OR WHATIF
     AND   TSFC.EC_EVENT_FK = synchronizer.EVENT_CALC.EVENT_ID
     AND   TSFC.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
								 FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
			                     WHERE ( EEP.APPLICATION_SCOPE_FK = 100
			                     AND  EEP.NAME = 'PRICING COST BASIS' ) )
			                         

     UPDATE synchronizer.EVENT_CALC 
     SET MARGIN_COST_BASIS_AMT = TSFC.COST_VALUE
     FROM #TS_FUTURE_COST TSFC
     WHERE 1 = 1
     AND   synchronizer.EVENT_CALC.RESULT_TYPE_CR_FK IN ( 711, 712 )  -- FUTURE OR WHATIF
     AND   TSFC.EC_EVENT_FK = synchronizer.EVENT_CALC.EVENT_ID
     AND   TSFC.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
								 FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
			                     WHERE ( EEP.APPLICATION_SCOPE_FK = 100
			                     AND  EEP.NAME = 'MARGIN COST BASIS' ) )



--------------------------------------------------------------------------------------------
--  SXE CURRENT VALUES FROM SHEET RESULTS.... ( IF NO FUTURE EVENTS.. THEN USE SR )
--------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_CALC 
SET PRICING_COST_BASIS_AMT = ISNULL (
     ( SELECT CASE DS.COLUMN_NAME
              WHEN 'NET_VALUE1'  THEN SR.NET_VALUE1
              WHEN 'NET_VALUE2'  THEN SR.NET_VALUE2
              WHEN 'NET_VALUE3'  THEN SR.NET_VALUE3
              WHEN 'NET_VALUE4'  THEN SR.NET_VALUE4
              WHEN 'NET_VALUE5'  THEN SR.NET_VALUE5
              WHEN 'NET_VALUE6'  THEN SR.NET_VALUE6                                                                      
              END
       FROM marginmgr.SHEET_RESULTS_VALUES_FUTURE SR WITH (NOLOCK)
       INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK) ON ( DN.PARENT_DATA_NAME_FK = SR.DATA_NAME_FK )
       INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
       WHERE 1 = 1
       AND  SR.PRODUCT_STRUCTURE_FK = synchronizer.EVENT_CALC.PRODUCT_STRUCTURE_FK
       AND  SR.ORG_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.ORG_ENTITY_STRUCTURE_FK 
       AND  DN.DATA_NAME_ID = ( SELECT CAST ( EEP.VALUE AS INT )
                                   FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
                                   WHERE EEP.APPLICATION_SCOPE_FK = 100
                                   AND   EEP.NAME = 'PRICING COST BASIS' ) )
       ,
     ( SELECT CASE DS.COLUMN_NAME
              WHEN 'NET_VALUE1'  THEN SR.NET_VALUE1
              WHEN 'NET_VALUE2'  THEN SR.NET_VALUE2
              WHEN 'NET_VALUE3'  THEN SR.NET_VALUE3
              WHEN 'NET_VALUE4'  THEN SR.NET_VALUE4
              WHEN 'NET_VALUE5'  THEN SR.NET_VALUE5
              WHEN 'NET_VALUE6'  THEN SR.NET_VALUE6                                                                      
              END
       FROM marginmgr.SHEET_RESULTS_VALUES_FUTURE SR WITH (NOLOCK)
       INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK) ON ( DN.PARENT_DATA_NAME_FK = SR.DATA_NAME_FK )
       INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
       WHERE 1 = 1
       AND  SR.PRODUCT_STRUCTURE_FK = synchronizer.EVENT_CALC.PRODUCT_STRUCTURE_FK
       AND  SR.ORG_ENTITY_STRUCTURE_FK = 1
       AND  DN.DATA_NAME_ID = ( SELECT CAST ( EEP.VALUE AS INT )
                                   FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
                                   WHERE EEP.APPLICATION_SCOPE_FK = 100
                                   AND   EEP.NAME = 'PRICING COST BASIS' ) )       
       )
WHERE 1 = 1
AND   synchronizer.EVENT_CALC.JOB_FK = @in_analysis_job_fk
AND   synchronizer.EVENT_CALC.RULE_TYPE_CR_FK IN ( 312, 313 )
AND   (       synchronizer.EVENT_CALC.RESULT_TYPE_CR_FK = 710  --- CURRENT
      OR (   synchronizer.EVENT_CALC.RESULT_TYPE_CR_FK IN ( 711, 712 )  -- FUTURE OR WHATIF
         AND synchronizer.EVENT_CALC.PRICING_COST_BASIS_AMT IS NULL ) --- WAS NOT SET PRIOR WITH FUTURE
     )
                                                                                                                    


UPDATE synchronizer.EVENT_CALC 
SET MARGIN_COST_BASIS_AMT = ISNULL (
     ( SELECT CASE DS.COLUMN_NAME
              WHEN 'NET_VALUE1'  THEN SR.NET_VALUE1
              WHEN 'NET_VALUE2'  THEN SR.NET_VALUE2
              WHEN 'NET_VALUE3'  THEN SR.NET_VALUE3
              WHEN 'NET_VALUE4'  THEN SR.NET_VALUE4
              WHEN 'NET_VALUE5'  THEN SR.NET_VALUE5
              WHEN 'NET_VALUE6'  THEN SR.NET_VALUE6                                                                      
              END
       FROM marginmgr.SHEET_RESULTS_VALUES_FUTURE SR WITH (NOLOCK)
       INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK) ON ( DN.PARENT_DATA_NAME_FK = SR.DATA_NAME_FK )
       INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
       WHERE 1 = 1
       AND  SR.PRODUCT_STRUCTURE_FK = synchronizer.EVENT_CALC.PRODUCT_STRUCTURE_FK
       AND  SR.ORG_ENTITY_STRUCTURE_FK = synchronizer.EVENT_CALC.ORG_ENTITY_STRUCTURE_FK 
       AND  DN.DATA_NAME_ID = ( SELECT CAST ( EEP.VALUE AS INT )
                                   FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
                                   WHERE EEP.APPLICATION_SCOPE_FK = 100
                                   AND   EEP.NAME = 'MARGIN COST BASIS' ) )
       ,
     ( SELECT CASE DS.COLUMN_NAME
              WHEN 'NET_VALUE1'  THEN SR.NET_VALUE1
              WHEN 'NET_VALUE2'  THEN SR.NET_VALUE2
              WHEN 'NET_VALUE3'  THEN SR.NET_VALUE3
              WHEN 'NET_VALUE4'  THEN SR.NET_VALUE4
              WHEN 'NET_VALUE5'  THEN SR.NET_VALUE5
              WHEN 'NET_VALUE6'  THEN SR.NET_VALUE6                                                                      
              END
       FROM marginmgr.SHEET_RESULTS_VALUES_FUTURE SR WITH (NOLOCK)
       INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK) ON ( DN.PARENT_DATA_NAME_FK = SR.DATA_NAME_FK )
       INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
       WHERE 1 = 1
       AND  SR.PRODUCT_STRUCTURE_FK = synchronizer.EVENT_CALC.PRODUCT_STRUCTURE_FK
       AND  SR.ORG_ENTITY_STRUCTURE_FK = 1
       AND  DN.DATA_NAME_ID = ( SELECT CAST ( EEP.VALUE AS INT )
                                   FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
                                   WHERE EEP.APPLICATION_SCOPE_FK = 100
                                   AND   EEP.NAME = 'MARGIN COST BASIS' ) )       
       )
WHERE 1 = 1
AND   synchronizer.EVENT_CALC.JOB_FK = @in_analysis_job_fk
AND   synchronizer.EVENT_CALC.RULE_TYPE_CR_FK IN ( 312, 313 )
AND   (       synchronizer.EVENT_CALC.RESULT_TYPE_CR_FK = 710  --- CURRENT
      OR (   synchronizer.EVENT_CALC.RESULT_TYPE_CR_FK IN ( 711, 712 )  -- FUTURE OR WHATIF
         AND synchronizer.EVENT_CALC.MARGIN_COST_BASIS_AMT IS NULL ) --- WAS NOT SET PRIOR WITH FUTURE
     )



END  --- SXE



------------------------------------------------------------------------------------------
--   In ECLIPSE Historical, Current and Future Values are all stored in the same
--       table marginmgr.PRICESHEET using EFFECTIVE_DATE to drive selection
------------------------------------------------------------------------------------------


IF @v_host_erp_system = 'ECLIPSE'
BEGIN

------------------------------------------------------------------------------------------
--   CURRENT AND FUTURE COSTS ALL FROM PRICESHEET TABLE
------------------------------------------------------------------------------------------

TRUNCATE TABLE #TS_FUTURE_COST

INSERT INTO #TS_FUTURE_COST(
    EC_EVENT_FK,    
    FUTURE_VALUE_FK,    
    DATA_NAME_FK,
	COST_VALUE,
	EFFECTIVE_DATE,
	DRANK
   )
SELECT A.EC_EVENT_FK
      ,A.FUTURE_VALUE_FK
      ,A.DATA_NAME_FK
      ,A.COST_VALUE
      ,A.EFFECTIVE_DATE
      ,A.DRANK
FROM (
SELECT EC.EVENT_ID AS EC_EVENT_FK 
     , PSHT.PRICESHEET_ID AS FUTURE_VALUE_FK
     , DN.DATA_NAME_ID AS DATA_NAME_FK 
     , CASE DS.COLUMN_NAME
       WHEN 'NET_VALUE1' THEN  ISNULL ( PSHT.NET_VALUE1, 0 ) 
       WHEN 'NET_VALUE2' THEN  ISNULL ( PSHT.NET_VALUE2, 0 ) 
       WHEN 'NET_VALUE3' THEN  ISNULL ( PSHT.NET_VALUE3, 0 ) 
       WHEN 'NET_VALUE4' THEN  ISNULL ( PSHT.NET_VALUE4, 0 )
       WHEN 'NET_VALUE5' THEN  ISNULL ( PSHT.NET_VALUE5, 0 )
       WHEN 'NET_VALUE6' THEN  ISNULL ( PSHT.NET_VALUE6, 0 ) 
       END  AS COST_VALUE
     , PSHT.EFFECTIVE_DATE
     , PSHT.PRODUCT_STRUCTURE_FK
     , PSHT.ORG_ENTITY_STRUCTURE_FK
     , PSHT.ENTITY_STRUCTURE_FK
     , ( DENSE_RANK() OVER ( PARTITION BY EC.EVENT_ID
								ORDER BY  ECT.PRECEDENCE ASC, PSHT.EFFECTIVE_DATE DESC, PSHT.PRICESHEET_ID DESC ) 
         ) AS DRANK
---------------   
FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
  ON ( DN.RECORD_STATUS_CR_FK = AJ.RECORD_STATUS_CR_FK
  AND  DN.DATA_NAME_ID IN ( SELECT CAST ( EEP.VALUE AS INT ) 
                           FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
                           WHERE EEP.APPLICATION_SCOPE_FK = 100
                           AND   EEP.NAME = 'PRICING COST BASIS' ) ) 
INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK ) 
INNER JOIN synchronizer.EVENT_CALC EC WITH (NOLOCK) 
 ON EC.JOB_FK = AJ.ANALYSIS_JOB_ID
    --- BASIS VALUES ARE ORG TO ORG;  CORP TO ORG   OR ORG TO CORP 
INNER JOIN  #TS_ORG_XREF TOX
ON ( TOX.ORG_ENTITY_STRUCTURE_FK = EC.ORG_ENTITY_STRUCTURE_FK )
INNER JOIN marginmgr.PRICESHEET PSHT WITH (NOLOCK)
 ON   PSHT.DATA_NAME_FK = DN.PARENT_DATA_NAME_FK   
 AND  ISNULL ( PSHT.PRODUCT_STRUCTURE_FK, -999 ) = ISNULL ( EC.PRODUCT_STRUCTURE_FK, -999 ) 
 AND  ISNULL ( PSHT.ORG_ENTITY_STRUCTURE_FK, -999 ) = ISNULL ( TOX.XREF_ORG_ENTITY_STRUCTURE_FK, -999 )  
 AND  PSHT.RECORD_STATUS_CR_FK = 1
----
INNER JOIN epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK)
ON (  ECT.RECORD_STATUS_CR_FK = 1
AND   ECT.ENTITY_CLASS_CR_FK = 10101
AND   ECT.DATA_NAME_FK = (  SELECT ES.DATA_NAME_FK 
							FROM epacube.ENTITY_STRUCTURE ES WITH (NOLOCK)
							WHERE ES.ENTITY_STRUCTURE_ID = PSHT.ORG_ENTITY_STRUCTURE_FK )    )
WHERE 1 = 1
AND   AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk
AND   EC.RULE_TYPE_CR_FK IN ( 312, 313 )
AND   (  
         (   EC.RESULT_TYPE_CR_FK = 710
         --AND PSHT.EFFECTIVE_DATE <= ISNULL ( AJ.JOB_DATE, GETDATE () ) )    --- CURRENT VALUE 
         AND PSHT.EFFECTIVE_DATE <= ISNULL ( AJ.RESULT_EFFECTIVE_DATE, GETDATE () ) )    --- CURRENT VALUE 
       OR
         (   EC.RESULT_TYPE_CR_FK IN ( 711, 712 )
         ------AND PSHT.EFFECTIVE_DATE >  AJ.JOB_DATE  --- LEAVE COMMENTED...IF NO FUTURE THAN NEED TO USE CURRENT
         AND PSHT.EFFECTIVE_DATE <= EC.RESULT_EFFECTIVE_DATE  )  --- FUTURE VALUE
       )
---------------
AND ( 
		( DS.COLUMN_NAME = 'NET_VALUE1' AND ISNULL ( PSHT.NET_VALUE1, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE2' AND ISNULL ( PSHT.NET_VALUE2, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE3' AND ISNULL ( PSHT.NET_VALUE3, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE4' AND ISNULL ( PSHT.NET_VALUE4, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE5' AND ISNULL ( PSHT.NET_VALUE5, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE6' AND ISNULL ( PSHT.NET_VALUE6, 0 ) > 0 )       
    )     
    ) A
WHERE A.DRANK = 1


------------------------------------------------------------------------------------------
---  UPDATE PRICING AND MARGIN COSTS 
------------------------------------------------------------------------------------------

     UPDATE synchronizer.EVENT_CALC 
     SET PRICING_COST_BASIS_AMT = TSFC.COST_VALUE
     FROM #TS_FUTURE_COST TSFC
     WHERE 1 = 1
     AND   TSFC.EC_EVENT_FK = synchronizer.EVENT_CALC.EVENT_ID
     AND   TSFC.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
								 FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
			                     WHERE ( EEP.APPLICATION_SCOPE_FK = 100
			                     AND  EEP.NAME = 'PRICING COST BASIS' ) )
			                         

     UPDATE synchronizer.EVENT_CALC 
     SET MARGIN_COST_BASIS_AMT = TSFC.COST_VALUE
     FROM #TS_FUTURE_COST TSFC
     WHERE 1 = 1
     AND   TSFC.EC_EVENT_FK = synchronizer.EVENT_CALC.EVENT_ID
     AND   TSFC.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT )
								 FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
			                     WHERE ( EEP.APPLICATION_SCOPE_FK = 100
			                     AND  EEP.NAME = 'MARGIN COST BASIS' ) )
END   --- ECLIPSE



----------------------------------------------------------------------------------------
--   SET ORDER_COGS_AMT = PRICING_COST_BASIS_AMT 
----------------------------------------------------------------------------------------


UPDATE synchronizer.EVENT_CALC
SET ORDER_COGS_AMT = PRICING_COST_BASIS_AMT
WHERE 1 = 1
AND JOB_FK = @in_analysis_job_fk



-------------------------------------------------------------------------------------
--   INACTIVATE EVENT IF ORDER_COGS_AMT <= 0   --- DO NOT CALCULATE
-------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_CALC
SET  RECORD_STATUS_CR_FK = 2 
WHERE 1 = 1
AND JOB_FK = @in_analysis_job_fk
AND ISNULL ( ORDER_COGS_AMT, 0 ) <= 0



-------------------------------------------------------------------------------------
--   TEMP TABLE DROPS
-------------------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_FUTURE_COST') is not null
	   drop table #TS_FUTURE_COST;


--drop temp table if it exists
	IF object_id('tempdb..#TS_ORG_XREF') is not null
	   drop table #TS_ORG_XREF;



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of MARGINMGR.CALCULATION_PROCESS_COSTS'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;


END TRY
BEGIN CATCH   

		SELECT 
			@ErrorMessage = 'Execution of MARGINMGR.CALCULATION_PROCESS_COSTS has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END






