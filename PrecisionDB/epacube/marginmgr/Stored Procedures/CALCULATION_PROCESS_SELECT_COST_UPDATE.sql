﻿






-- Copyright 2012
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: INSERT rows into EVENT_CALC for ONLY products with Cost Changes
--
--          NOTE:  ERP Specific
--                 IF SXE look for events in EVENT_DATA
--                 IF Eclipse look for future dated values in PRICESHEET table
--
--                 Two Cost Change Methods:  First and Prior
--                 First change after Job_Date ( no EffDt provided on Job -- EffDt becomes Cost EffDt )
--                 Prior change to the Job Effective Date ( EffDt = Job EffDate but store Cost EffDt )
--
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        05/03/2012   Initial SQL Version
-- 
--
CREATE PROCEDURE [marginmgr].[CALCULATION_PROCESS_SELECT_COST_UPDATE] 
   ( @in_analysis_job_fk BIGINT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT
DECLARE  @v_host_erp_system   varchar(64)

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of marginmgr.CALCULATION_PROCESS_SELECT_COST_UPDATE.' 
               + ' JOB:: ' + CAST ( ISNULL (@in_analysis_job_fk, 0 ) AS VARCHAR(16) )

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;



SET @v_host_erp_system = ( SELECT TOP 1 eep.value FROM epacube.epacube_params eep WITH (NOLOCK)
                           WHERE eep.application_scope_fk = 100 AND eep.NAME = 'ERP HOST' )
                           


-----   DELETE ALL PREVIOUS ENTRIES FOR THIS JOB
DELETE
FROM [marginmgr].[ANALYSIS_JOB_COST_CHANGE]
WHERE ANALYSIS_JOB_FK = @in_analysis_job_fk



--------------------------------------------------------------------------------------------
--  SXE Costs are from SHEET_RESULTS for Current;  and EVENT_DATA for FUTURE
--    V517 will be What IF and we will figure out requirements later
--------------------------------------------------------------------------------------------

IF @v_host_erp_system = 'SXE'
BEGIN


---- FUTURE-FIRST
INSERT INTO [marginmgr].[ANALYSIS_JOB_COST_CHANGE]
           ([ANALYSIS_JOB_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[COST_CHANGE_FK]
           ,[COST_EFFECTIVE_DATE]
           )
--
    SELECT    DISTINCT
              @in_analysis_job_fk
             ,A.PRODUCT_STRUCTURE_FK
             ,A.COST_CHANGE_FK
             ,A.COST_EFFECTIVE_DATE
       FROM ( SELECT 
              ED.PRODUCT_STRUCTURE_FK
             ,ED.EVENT_ID AS COST_CHANGE_FK
             ,ED.EVENT_EFFECTIVE_DATE  AS COST_EFFECTIVE_DATE            
			,( DENSE_RANK() OVER ( PARTITION BY ED.PRODUCT_STRUCTURE_FK
						   ORDER BY ED.EVENT_EFFECTIVE_DATE ASC, ED.EVENT_ID ASC ) )AS DRANK
---------
			FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
			INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
			  ON ( AJ.RECORD_STATUS_CR_FK = ED.RECORD_STATUS_CR_FK )
			INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK) 
			  ON ( DN.PARENT_DATA_NAME_FK = ED.DATA_NAME_FK 
			  AND  DN.DATA_NAME_ID IN ( SELECT CAST ( EEP.VALUE AS INT ) 
									   FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
									   WHERE EEP.APPLICATION_SCOPE_FK = 100
									   AND   EEP.NAME IN ( 'MARGIN COST BASIS', 'PRICING COST BASIS', 'REPLACEMENT COST' ) )
			   )
			INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK ) 			  
            WHERE 1 = 1
            AND   AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk
            AND   ISNULL ( AJ.COST_CHANGE_FIRST_IND, 0 ) = 1  -- FUTURE-FIRST
            AND   ED.EVENT_STATUS_CR_FK = 84  -- APPROVED FUTURE
            AND   ED.EVENT_EFFECTIVE_DATE > ISNULL ( AJ.JOB_DATE, GETDATE () ) -- EVENT IS IN THE FUTURE             
		---------------
		AND ( 
				( DS.COLUMN_NAME = 'NET_VALUE1' AND ISNULL ( ED.NET_VALUE1_NEW, 0 ) > 0 )       
			 OR
				( DS.COLUMN_NAME = 'NET_VALUE2' AND ISNULL ( ED.NET_VALUE2_NEW, 0 ) > 0 )       
			 OR
				( DS.COLUMN_NAME = 'NET_VALUE3' AND ISNULL ( ED.NET_VALUE3_NEW, 0 ) > 0 )       
			 OR
				( DS.COLUMN_NAME = 'NET_VALUE4' AND ISNULL ( ED.NET_VALUE4_NEW, 0 ) > 0 )       
			 OR
				( DS.COLUMN_NAME = 'NET_VALUE5' AND ISNULL ( ED.NET_VALUE5_NEW, 0 ) > 0 )       
			 OR
				( DS.COLUMN_NAME = 'NET_VALUE6' AND ISNULL ( ED.NET_VALUE6_NEW, 0 ) > 0 )       
			)     
		-------
			) A
		WHERE A.DRANK = 1
  


---- FUTURE-PRIOR
INSERT INTO [marginmgr].[ANALYSIS_JOB_COST_CHANGE]
           ([ANALYSIS_JOB_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[COST_CHANGE_FK]
           ,[COST_EFFECTIVE_DATE]
           )
--
    SELECT    DISTINCT
              @in_analysis_job_fk
             ,A.PRODUCT_STRUCTURE_FK
             ,A.COST_CHANGE_FK
             ,A.COST_EFFECTIVE_DATE
       FROM ( SELECT 
              ED.PRODUCT_STRUCTURE_FK
             ,ED.EVENT_ID AS COST_CHANGE_FK
             ,ED.EVENT_EFFECTIVE_DATE  AS COST_EFFECTIVE_DATE            
			,( DENSE_RANK() OVER ( PARTITION BY ED.PRODUCT_STRUCTURE_FK
						   ORDER BY ED.EVENT_EFFECTIVE_DATE DESC, ED.EVENT_ID ASC ) )AS DRANK
---------
			FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
			INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
			  ON ( AJ.RECORD_STATUS_CR_FK = ED.RECORD_STATUS_CR_FK )
			INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK) 
			  ON ( DN.PARENT_DATA_NAME_FK = ED.DATA_NAME_FK 
			  AND  DN.DATA_NAME_ID IN ( SELECT CAST ( EEP.VALUE AS INT ) 
									   FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
									   WHERE EEP.APPLICATION_SCOPE_FK = 100
									   AND   EEP.NAME IN ( 'MARGIN COST BASIS', 'PRICING COST BASIS', 'REPLACEMENT COST' ) )
			   )
			INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK ) 			  
            WHERE 1 = 1
            AND   AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk
            AND   ISNULL ( AJ.COST_CHANGE_PRIOR_IND, 0 ) = 1  -- FUTURE-PRIOR
            AND   ED.EVENT_STATUS_CR_FK = 84  -- APPROVED FUTURE
            AND   ED.EVENT_EFFECTIVE_DATE > ISNULL ( AJ.JOB_DATE, GETDATE () ) -- EVENT IS IN THE FUTURE             
            AND   ED.EVENT_EFFECTIVE_DATE <= AJ.RESULT_EFFECTIVE_DATE
		---------------
		AND ( 
				( DS.COLUMN_NAME = 'NET_VALUE1' AND ISNULL ( ED.NET_VALUE1_NEW, 0 ) > 0 )       
			 OR
				( DS.COLUMN_NAME = 'NET_VALUE2' AND ISNULL ( ED.NET_VALUE2_NEW, 0 ) > 0 )       
			 OR
				( DS.COLUMN_NAME = 'NET_VALUE3' AND ISNULL ( ED.NET_VALUE3_NEW, 0 ) > 0 )       
			 OR
				( DS.COLUMN_NAME = 'NET_VALUE4' AND ISNULL ( ED.NET_VALUE4_NEW, 0 ) > 0 )       
			 OR
				( DS.COLUMN_NAME = 'NET_VALUE5' AND ISNULL ( ED.NET_VALUE5_NEW, 0 ) > 0 )       
			 OR
				( DS.COLUMN_NAME = 'NET_VALUE6' AND ISNULL ( ED.NET_VALUE6_NEW, 0 ) > 0 )       
			)     
		-------
			) A
		WHERE A.DRANK = 1




END   ---- SXE



------------------------------------------------------------------------------------------
---  Insert Rows into Analysis Table
------------------------------------------------------------------------------------------




IF @v_host_erp_system = 'ECLIPSE'    --- THIS IS ONLY FOR ECLIPSE
BEGIN


------------------------------------------------------------------------------------------
--   Find Future Effected Rows by looking in PRICESHEET for all prod/org
--     PRICE COST row <= RESULT_EFFECTIVE_DATE but greater than JOB_DATE
------------------------------------------------------------------------------------------


---- FUTURE - FIRST
INSERT INTO [marginmgr].[ANALYSIS_JOB_COST_CHANGE]
           ([ANALYSIS_JOB_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[COST_CHANGE_FK]
           ,[COST_EFFECTIVE_DATE]
           )
SELECT   DISTINCT 
          @in_analysis_job_fk
         ,A.PRODUCT_STRUCTURE_FK
         ,A.COST_CHANGE_FK
         ,A.COST_EFFECTIVE_DATE
FROM (         
SELECT 
       PSHT.PRODUCT_STRUCTURE_FK
     , PSHT.PRICESHEET_ID AS COST_CHANGE_FK
     , PSHT.EFFECTIVE_DATE  AS COST_EFFECTIVE_DATE   
     , ( DENSE_RANK() OVER ( PARTITION BY PSHT.PRODUCT_STRUCTURE_FK
								ORDER BY  PSHT.EFFECTIVE_DATE ASC, PSHT.PRICESHEET_ID DESC ) 
         ) AS DRANK
---------------    
FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
INNER JOIN marginmgr.PRICESHEET PSHT WITH (NOLOCK)
 ON PSHT.RECORD_STATUS_CR_FK = AJ.RECORD_STATUS_CR_FK
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK) ON ( DN.PARENT_DATA_NAME_FK = PSHT.DATA_NAME_FK )    
INNER JOIN epacube.DATA_SET DS WITH (NOLOCK) ON ( DS.DATA_SET_ID = DN.DATA_SET_FK ) 
WHERE 1 = 1
AND   AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk
AND   ISNULL ( AJ.COST_CHANGE_FIRST_IND, 0 ) = 1  -- FUTURE-FIRST
AND   PSHT.EFFECTIVE_DATE > ISNULL ( AJ.JOB_DATE, GETDATE () )
AND   DN.DATA_NAME_ID IN ( SELECT CAST ( EEP.VALUE AS INT ) 
                           FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
                           WHERE EEP.APPLICATION_SCOPE_FK = 100
                           AND   EEP.NAME IN ( 'MARGIN COST BASIS', 'PRICING COST BASIS', 'REPLACEMENT COST' ) )
---------------
AND ( 
		( DS.COLUMN_NAME = 'NET_VALUE1' AND ISNULL ( PSHT.NET_VALUE1, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE2' AND ISNULL ( PSHT.NET_VALUE2, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE3' AND ISNULL ( PSHT.NET_VALUE3, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE4' AND ISNULL ( PSHT.NET_VALUE4, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE5' AND ISNULL ( PSHT.NET_VALUE5, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE6' AND ISNULL ( PSHT.NET_VALUE6, 0 ) > 0 )       
    )     
-------
    ) A
WHERE A.DRANK = 1



---- FUTURE - PRIOR
INSERT INTO [marginmgr].[ANALYSIS_JOB_COST_CHANGE]
           ([ANALYSIS_JOB_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[COST_CHANGE_FK]
           ,[COST_EFFECTIVE_DATE]
           )
SELECT   DISTINCT 
          @in_analysis_job_fk
         ,A.PRODUCT_STRUCTURE_FK
         ,A.COST_CHANGE_FK
         ,A.COST_EFFECTIVE_DATE
FROM (         
SELECT 
       PSHT.PRODUCT_STRUCTURE_FK
     , PSHT.PRICESHEET_ID AS COST_CHANGE_FK
     , PSHT.EFFECTIVE_DATE  AS COST_EFFECTIVE_DATE   
     , ( DENSE_RANK() OVER ( PARTITION BY PSHT.PRODUCT_STRUCTURE_FK
								ORDER BY  PSHT.EFFECTIVE_DATE DESC, PSHT.PRICESHEET_ID DESC ) 
         ) AS DRANK
---------------    
FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK)
INNER JOIN marginmgr.PRICESHEET PSHT WITH (NOLOCK)
 ON PSHT.RECORD_STATUS_CR_FK = AJ.RECORD_STATUS_CR_FK
INNER JOIN epacube.DATA_NAME DN ON ( DN.PARENT_DATA_NAME_FK = PSHT.DATA_NAME_FK )    
INNER JOIN epacube.DATA_SET DS  ON ( DS.DATA_SET_ID = DN.DATA_SET_FK ) 
WHERE 1 = 1
AND   AJ.ANALYSIS_JOB_ID = @in_analysis_job_fk
AND   ISNULL ( AJ.COST_CHANGE_PRIOR_IND, 0 ) = 1  -- FUTURE-PRIOR
AND   PSHT.EFFECTIVE_DATE > ISNULL ( AJ.JOB_DATE, GETDATE () )
AND   PSHT.EFFECTIVE_DATE <= AJ.RESULT_EFFECTIVE_DATE
AND   DN.DATA_NAME_ID IN ( SELECT CAST ( EEP.VALUE AS INT ) 
                           FROM epacube.EPACUBE_PARAMS EEP WITH (NOLOCK)
                           WHERE EEP.APPLICATION_SCOPE_FK = 100
                           AND   EEP.NAME IN ( 'MARGIN COST BASIS', 'PRICING COST BASIS', 'REPLACEMENT COST' ) )
---------------
AND ( 
		( DS.COLUMN_NAME = 'NET_VALUE1' AND ISNULL ( PSHT.NET_VALUE1, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE2' AND ISNULL ( PSHT.NET_VALUE2, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE3' AND ISNULL ( PSHT.NET_VALUE3, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE4' AND ISNULL ( PSHT.NET_VALUE4, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE5' AND ISNULL ( PSHT.NET_VALUE5, 0 ) > 0 )       
     OR
		( DS.COLUMN_NAME = 'NET_VALUE6' AND ISNULL ( PSHT.NET_VALUE6, 0 ) > 0 )       
    )     
-------
    ) A
WHERE A.DRANK = 1





END   --- ECLIPSE 



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of marginmgr.CALCULATION_PROCESS_SELECT_COST_UPDATE'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of marginmgr.CALCULATION_PROCESS_SELECT_COST_UPDATE has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
		 EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END


















