﻿


 ----=============================================
 ----Author:		<Author,,Name>
 ----Create date: <Create Date,,>
 ----Description:	<Description,,>
 ----=============================================
CREATE PROCEDURE [marginmgr].[RETAIL_PRICE_MAINTENANCE_ZONE_SETTINGS]
 
	@Result_Type varchar(64) = 'ZONE'
	, @ZONE as varchar(Max) = NULL
	, @EFFECTIVE_DATE as varchar(16) = NULL
	, @Parent_Prod_Segment_FK varchar(24) = Null
	, @User Varchar(96) = Null

AS

	SET NOCOUNT ON;

	--Declare @Result_Type varchar(64) = 'zic'
	--Declare @ZONE as varchar(64) = '40004'
	----Declare @ZONE as varchar(64) = '30001'
	--Declare @EFFECTIVE_DATE as date = '2019-10-29'
	--Declare @Parent_Prod_Segment_FK varchar(64) = 1014719558 --1014719724 --Null --1014719549 --Null --1014719564 --1000055847
	--Declare @User Varchar(96) = 'gstone@epacube.com'

	If Object_ID('tempdb..#Zones') is not null
	drop table #Zones

	Create table #Zones(zone_entity_Structure_FK bigint not null, Zone Varchar(64) Not Null, Zone_Name Varchar(64) Null)

	Insert into #Zones
	Select ei.entity_structure_fk 'Zone_Entity_Structure_FK', ei.value 'Zone', ein.value 'Zone_Name' 
	from epacube.entity_identification ei with (nolock)
	inner join epacube.entity_ident_nonunique ein with (nolock) on ei.entity_structure_fk = ein.entity_structure_fk and ein.data_name_fk = 151112
	inner join utilities.SplitString(@ZONE, ',') Zns on ei.value = ZNs.listitem and ei.entity_data_name_fk = 151000 and ei.data_name_fk = 151110

	create index idx_01 on #zones(zone_entity_structure_fk)

	Declare @ZoneCount int = (select count(*) from #zones)
	Declare @ZS_FK bigint

If @ZoneCount = 1
Begin

	Set @ZS_FK = (select top 1 zone_entity_structure_fk from #zones)
	
	If @Result_Type = 'ZONE'
	Begin
	Select zone_segment_fk
	, ZONE
	, ZONE_NAME
	, @EFFECTIVE_DATE 'PRICING_DATE'
	, PRENOTIFY_DAYS, [REQUIRED_PRICE_CHANGE_AMOUNT], [URM_LVL_GRS_ACTIVATION_FLAG], [USE_LVL_GRS_PRICE_POINTS_FLAG]
	, [PRC_ADJ_DIG_0], [PRC_ADJ_DIG_1], [PRC_ADJ_DIG_2], [PRC_ADJ_DIG_3], [PRC_ADJ_DIG_4], [PRC_ADJ_DIG_5], [PRC_ADJ_DIG_6], [PRC_ADJ_DIG_7], [PRC_ADJ_DIG_8], [PRC_ADJ_DIG_9]
				FROM(
					Select --ss.effective_date, 
					SS.zone_segment_fk
					, eiz.value 'ZONE'
					, eizn.value 'ZONE_NAME'
					, Attribute_Event_Data  'Adjustment'
					, dn.name 'dataname'
					, dense_rank()over(partition by ss.zone_segment_fk, ss.data_name_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank_Eff
					from epacube.segments_settings ss with (nolock)
					inner join #zones z on ss.ZONE_SEGMENT_FK = z.zone_entity_Structure_FK
					inner join epacube.entity_identification eiz with (nolock) on ss.ZONE_SEGMENT_FK = eiz.entity_structure_fk and eiz.ENTITY_DATA_NAME_FK = 151000
					inner join epacube.entity_ident_nonunique eizn with (nolock) on eiz.ENTITY_STRUCTURE_FK = eizn.ENTITY_STRUCTURE_FK and eiz.ENTITY_DATA_NAME_FK = eizn.ENTITY_DATA_NAME_FK and eizn.data_name_fk = 151112
					inner join epacube.data_name dn with (nolock) on ss.data_name_fk = dn.data_name_id
					left join epacube.data_value dv with (nolock) on ss.data_value_fk = dv.data_value_id
					where 1 = 1
					and ss.ZONE_SEGMENT_FK is not null
					and ss.cust_entity_structure_fk is null
					and ss.prod_segment_fk is null
					and (
							(dn.parent_data_name_fk = 144893
								and ss.entity_data_name_fk = 151000)
							Or 
								ss.data_name_fk in (144868, 144908, 144876, 144862)
						)
					and ss.effective_date <= @EFFECTIVE_DATE
					and (ss.reinstate_inheritance_date > @EFFECTIVE_DATE or ss.reinstate_inheritance_date is null)
					--and isnull(ss.reinstate_inheritance_date, dateadd(d, 1, @EFFECTIVE_DATE)) > @EFFECTIVE_DATE
				) P 
				PIVOT (Max(p.Adjustment) for DataName in
				 (PRENOTIFY_DAYS, [REQUIRED_PRICE_CHANGE_AMOUNT], URM_LVL_GRS_ACTIVATION_FLAG, [USE_LVL_GRS_PRICE_POINTS_FLAG], [PRC_ADJ_DIG_0], [PRC_ADJ_DIG_1], [PRC_ADJ_DIG_2], [PRC_ADJ_DIG_3], [PRC_ADJ_DIG_4], [PRC_ADJ_DIG_5], [PRC_ADJ_DIG_6]
				 , [PRC_ADJ_DIG_7], [PRC_ADJ_DIG_8], [PRC_ADJ_DIG_9]) ) As pivotTable 
				 where DRank_eff = 1
				 order by zone_segment_fk

	End
	else

	If @Result_Type = 'ZIC' and @Parent_Prod_Segment_FK is null
	Begin

		--Set @ZS_FK = (select top 1 zone_entity_structure_fk from #zones)

		If Object_ID('tempdb..#Sub50') is not null
		drop table #Sub50

			Select data_name_fk, prod_segment_fk, zone_segment_fk, count(*) 'Subordinates' 
			into #Sub50 
			from (
			Select sp.data_name_fk, sp.prod_segment_fk, ss.zone_segment_fk, ss.effective_date
			, Dense_rank()over(partition by ss.prod_segment_fk, ss.zone_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank_Effz
			from epacube.segments_product SP with (nolock)
			inner join epacube.product_association pa with (nolock) on sp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and pa.data_name_fk = 159450
			inner join #zones z on pa.ENTITY_STRUCTURE_FK = z.zone_entity_Structure_FK
			inner join epacube.segments_settings ss with (nolock)	on	sp.product_structure_fk = ss.PROD_SEGMENT_FK 
																	and ss.DATA_NAME_FK = 502046
																	and ss.RECORD_STATUS_CR_FK = 1 
																	and ss.effective_date <= @EFFECTIVE_DATE
																	and z.zone_entity_Structure_FK = ss.ZONE_SEGMENT_FK
				where sp.data_name_fk = 501045
				and ss.prod_segment_data_name_fk = 110103
			) A where DRank_Effz = 1
			group by data_name_fk, prod_segment_fk, zone_segment_fk

		create index idx_01 on #Sub50(prod_segment_fk)

		Select 
		A.Scheme_Node
		--, Null Sort2
		--, Null 'r.rules_id'
		, 151110 zone_segment_data_name_fk
		, isnull(adj.ZONE_SEGMENT_FK, @ZS_FK) 'zone_segment_fk'
		, a.data_name_fk 'prod_segment_data_name_fk'
		, a.prod_segment_fk
		, a.parent_prod_segment_fk
		, a.item_class_label
		, a.item_level
		, a.item_class_id
		, a.[Description] 'Item_Class_Description'
		, a.Item_Class_ID + ': ' + a.[Description] + ' (' + isnull(cast(Case when a.scheme_node <> '3' then cast(A.Subordinates as varchar(8)) else cast(isnull(sub50.Subordinates, '0') as varchar(8)) + '/' + cast(isnull(A.subordinates, '0') as varchar(8)) end as varchar(8)), '0') + ')' 'DEPT_GROUP_SUBGROUP_ITEM'
		, dn_144891.URM_RANGE_ROUNDING_TO_VALUE
		, adj.URM_LEVEL_GROSS_FLAG 'lg_flag'
		, adj.[URM_TARGET_MARGIN] 'tm_pct'
		, adj.[REQUIRED_PRICE_CHANGE_AMOUNT]
		, adj.[USE_LVL_GRS_PRICE_POINTS_FLAG]
		, adj.[PRC_ADJ_DIG_0], adj.[PRC_ADJ_DIG_1], adj.[PRC_ADJ_DIG_2], adj.[PRC_ADJ_DIG_3], adj.[PRC_ADJ_DIG_4], adj.[PRC_ADJ_DIG_5], adj.[PRC_ADJ_DIG_6], adj.[PRC_ADJ_DIG_7], adj.[PRC_ADJ_DIG_8], adj.[PRC_ADJ_DIG_9]
		, cast(null as bigint) 'alias'
		from (
		Select 1 Scheme_Node, dv.display_seq 'Item_Level', value 'Item_Class_ID', dv.[Description], data_name_fk, data_value_id Prod_Segment_fk, Null Parent_Prod_Segment_FK, dn.label 'item_class_label', dn.PRECEDENCE
		, (select count(*) from epacube.data_value where data_name_fk = dv.data_name_fk + 1 and parent_data_value_fk = dv.data_value_id) 'Subordinates'
		from epacube.data_value dv with (nolock) 
		inner join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
		where data_name_fk = 501043
		union
		Select 2 Scheme_Node, dv.display_seq 'Item_Level', value 'Item_Class_ID', dv.[Description], data_name_fk, data_value_id Prod_Segment_fk, Parent_Data_Value_FK 'Parent_Prod_Segment_FK', dn.label 'item_class_label', dn.PRECEDENCE
		, (select count(*) from epacube.data_value where data_name_fk = dv.data_name_fk + 1 and parent_data_value_fk = dv.data_value_id) 'Subordinates'
		from epacube.data_value dv with (nolock) 
		inner join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
		where data_name_fk = 501044
		union
		Select 3 Scheme_Node, dv.display_seq 'Item_Level', value 'Item_Class_ID', dv.[Description], dv.data_name_fk, data_value_id Prod_Segment_fk, Parent_Data_Value_FK 'Parent_Prod_Segment_FK', dn.label 'item_class_label', dn.PRECEDENCE
		, (select count(*) from epacube.segments_product sp with (nolock) where data_name_fk = 501045 and prod_segment_fk = dv.data_value_id) 'Subordinates'
		from epacube.data_value dv with (nolock) 
		inner join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
		where dv.data_name_fk = 501045
		) A
		left join #Sub50 Sub50 
			on A.Prod_Segment_fk = Sub50.PROD_SEGMENT_FK
		left join
			(Select 'Y' 'URM_RANGE_ROUNDING_TO_VALUE', zone_segment_fk, prod_segment_fk from epacube.segments_settings ss1 with (nolock) inner join #zones z on ss1.zone_segment_fk = z.zone_entity_Structure_FK
			where data_name_fk = 144891 
			group by zone_segment_fk, prod_segment_fk) dn_144891 on a.Prod_segment_fk = dn_144891.prod_segment_fk

		left join
		(
		Select
		D.zone_segment_fk, D.prod_segment_fk, data_name_fk
		, D.item_level, D.class_id, D.Item_Class, D.[REQUIRED_PRICE_CHANGE_AMOUNT], D.[URM_RANGE_ROUNDING_TO_VALUE], D.[USE_LVL_GRS_PRICE_POINTS_FLAG]
		, D.[PRC_ADJ_DIG_0], D.[PRC_ADJ_DIG_1], D.[PRC_ADJ_DIG_2], D.[PRC_ADJ_DIG_3], D.[PRC_ADJ_DIG_4], D.[PRC_ADJ_DIG_5], D.[PRC_ADJ_DIG_6], D.[PRC_ADJ_DIG_7], D.[PRC_ADJ_DIG_8], D.[PRC_ADJ_DIG_9], D.[URM_LEVEL_GROSS_FLAG]
		, D.[URM_TARGET_MARGIN] 

		from (
		Select zone_segment_fk, PROD_SEGMENT_FK
		, DISPLAY_SEQ 'item_level', value 'class_id', isnull([description], 'All Items') 'item_class', data_name_fk, REQUIRED_PRICE_CHANGE_AMOUNT
		, [URM_RANGE_ROUNDING_TO_VALUE], [USE_LVL_GRS_PRICE_POINTS_FLAG]
		, [PRC_ADJ_DIG_0], [PRC_ADJ_DIG_1], [PRC_ADJ_DIG_2], [PRC_ADJ_DIG_3], [PRC_ADJ_DIG_4], [PRC_ADJ_DIG_5], [PRC_ADJ_DIG_6], [PRC_ADJ_DIG_7], [PRC_ADJ_DIG_8], [PRC_ADJ_DIG_9], [URM_LEVEL_GROSS_FLAG], [URM_TARGET_MARGIN]
					FROM(
						Select
						SS.zone_segment_fk, dv.DISPLAY_SEQ, dv.value, dv.[description]
						, Case	when dn.data_name_id = 144891 then 'Y' 
								when dn.data_name_id = 502046 then Cast(cast(Attribute_Number as Numeric(18, 4)) as varchar(16))
								when ss.attribute_number is not null then Cast(cast(Attribute_Number as Numeric(18, 2)) as varchar(16))
								else ATTRIBUTE_EVENT_DATA end 'Adjustment'
						, dn.name 'dataname'
						, ss.PROD_SEGMENT_FK, dv.data_name_fk
						, dense_rank()over(partition by ss.zone_segment_fk, ss.data_name_fk, ss.prod_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank_val
						from epacube.segments_settings ss with (nolock)
						inner join #zones z on ss.ZONE_SEGMENT_FK = z.zone_entity_Structure_FK
						inner join epacube.data_name dn with (nolock) on ss.data_name_fk = dn.data_name_id
						inner join epacube.data_value dv with (nolock) on ss.prod_segment_fk = dv.data_value_id
						where 1 = 1
						and cast(ss.Effective_Date as date) <= @EFFECTIVE_DATE
						and ss.cust_entity_structure_fk is null
						and ss.ATTRIBUTE_EVENT_DATA is not null
						and (ss.reinstate_inheritance_date > @EFFECTIVE_DATE or ss.reinstate_inheritance_date is null)
						and 
							(
								(dn.parent_data_name_fk = 144893
									and dv.data_name_fk between 501043 and 501045
									and ss.entity_data_name_fk = 151000
									and ss.data_name_fk <> 144891)
								Or 
									ss.data_name_fk in (144908, 500015, 502046, 144876) --144891,
							)
					) P  
					PIVOT (Max(p.Adjustment) for DataName in
					 ([REQUIRED_PRICE_CHANGE_AMOUNT], URM_RANGE_ROUNDING_TO_VALUE, [USE_LVL_GRS_PRICE_POINTS_FLAG]
					 , [PRC_ADJ_DIG_0], [PRC_ADJ_DIG_1], [PRC_ADJ_DIG_2], [PRC_ADJ_DIG_3], [PRC_ADJ_DIG_4], [PRC_ADJ_DIG_5], [PRC_ADJ_DIG_6], [PRC_ADJ_DIG_7], [PRC_ADJ_DIG_8], [PRC_ADJ_DIG_9], [URM_LEVEL_GROSS_FLAG], [URM_TARGET_MARGIN]) ) As pivotTable 
					where  DRank_val = 1
				
		) D
		) adj on isnull(A.prod_segment_fk, 0) = isnull(adj.prod_segment_fk, 0) and a.DATA_NAME_FK = adj.data_name_fk
		order by scheme_node, cast(Item_Class_ID as int)

	end
	else

	If @Result_Type = 'ZIC' and @Parent_Prod_Segment_FK is not null
	Begin
		Select 
		a.scheme_node
		--, Null sort2, Null 'rules_id'
	--	, Case when a.rules_id is null then 1 else 0 end  sort2, a.rules_id
		, 151110 'zone_segment_data_name_fk'
		, isnull(adj.ZONE_SEGMENT_FK, @ZS_FK) 'zone_segment_fk'
		, 110103 'prod_segment_data_name_fk'
		, a.prod_segment_fk
		, a.parent_prod_segment_fk
		, (select label from epacube.DATA_NAME ldn where ldn.DATA_NAME_ID = a.data_name_fk) 'item_class_label'
		, a.item_level
		, a.item_class_id
		, a.item_class_description 
		, a.item_class 'DEPT_GROUP_SUBGROUP_ITEM'
		, adj.[URM_RANGE_ROUNDING_TO_VALUE]
		, adj.[URM_LEVEL_GROSS_FLAG] 'lg_flag'
		, adj.[URM_TARGET_MARGIN] 'tm_pct'
		, adj.[REQUIRED_PRICE_CHANGE_AMOUNT]
		, adj.[PRC_ADJ_DIG_0], adj.[PRC_ADJ_DIG_1], adj.[PRC_ADJ_DIG_2], adj.[PRC_ADJ_DIG_3], adj.[PRC_ADJ_DIG_4], adj.[PRC_ADJ_DIG_5], adj.[PRC_ADJ_DIG_6], adj.[PRC_ADJ_DIG_7], adj.[PRC_ADJ_DIG_8], adj.[PRC_ADJ_DIG_9]
		, alias
		, ind_discontinued
		, A.Authorized
		from (
			select
			4 'scheme_node'
			, sp.data_name_fk
			, Null sort2, Null 'rules_id'
			, sp.PRODUCT_STRUCTURE_FK 'prod_segment_fk'
			, sp.prod_segment_fk 'parent_prod_segment_fk'
			, 60 'item_level'
			, pi.value 'item_class_id'
			, pd.[Description] 'item_class_description'
			, pi.value + ': ' + pd.[Description] 'item_class'
			, sp.data_name_fk 'sp_data_name_fk'
			, dv_500019.value 'alias'
			, case when pa_159450.PRODUCT_ASSOCIATION_ID is null then 0 else 1 end 'Authorized'
			, case when pa_500012.product_attribute_id is not null then '{"renderer":{"color":"red","text-decoration":"underline"}}' else null end 'ind_discontinued'
			from epacube.segments_product SP with (nolock)
			inner join epacube.product_identification pi with (nolock) on sp.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK and pi.data_name_fk = 110100
			inner join epacube.product_association pa with (nolock) on sp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and pa.data_name_fk = 159450
			left join epacube.product_attribute pa_500012 with (nolock) on sp.PRODUCT_STRUCTURE_FK = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
			inner join #zones z on pa.ENTITY_STRUCTURE_FK = z.zone_entity_Structure_FK
			left join epacube.product_mult_type pmt with (nolock) on pa.product_structure_fk = pmt.product_structure_fk and pmt.data_name_fk = 500019 and pmt.ENTITY_STRUCTURE_FK = z.zone_entity_Structure_FK
			left join epacube.data_value dv_500019 on pmt.data_name_fk = dv_500019.data_name_fk and pmt.DATA_VALUE_FK = dv_500019.DATA_VALUE_ID
			left join epacube.product_description pd with (nolock) on sp.PRODUCT_STRUCTURE_FK = pd.PRODUCT_STRUCTURE_FK and pd.DATA_NAME_FK = 110401
			left join epacube.product_association pa_159450 with (nolock) on pa_159450.data_name_fk = 159450 
																				and pa_159450.product_structure_fk = sp.product_structure_fk 
																				and z.zone_entity_structure_fk = pa_159450.entity_structure_fk 
																				and pa_159450.record_status_cr_fk = 1
			where sp.DATA_NAME_FK = 501045
			and sp.prod_segment_fk = @Parent_Prod_Segment_FK
		) A 
			left join
			(
			Select
			D.zone_segment_fk, D.prod_segment_fk, data_name_fk
			, D.item_level, D.class_id, D.Item_Class, D.[REQUIRED_PRICE_CHANGE_AMOUNT], D.[URM_RANGE_ROUNDING_TO_VALUE], D.[PRC_ADJ_DIG_0], D.[PRC_ADJ_DIG_1], D.[PRC_ADJ_DIG_2], D.[PRC_ADJ_DIG_3], D.[PRC_ADJ_DIG_4], D.[PRC_ADJ_DIG_5]
			, D.[PRC_ADJ_DIG_6], D.[PRC_ADJ_DIG_7], D.[PRC_ADJ_DIG_8], D.[PRC_ADJ_DIG_9], D.[URM_LEVEL_GROSS_FLAG], D.[URM_TARGET_MARGIN] 
			from (
			Select zone_segment_fk, PROD_SEGMENT_FK
			, Null 'item_level', Null 'class_id', Null 'item_class', Null 'data_name_fk', REQUIRED_PRICE_CHANGE_AMOUNT
			, Case when URM_RANGE_ROUNDING_TO_VALUE is not null then 'Y' end 'URM_RANGE_ROUNDING_TO_VALUE'
			, [PRC_ADJ_DIG_0], [PRC_ADJ_DIG_1], [PRC_ADJ_DIG_2], [PRC_ADJ_DIG_3], [PRC_ADJ_DIG_4], [PRC_ADJ_DIG_5], [PRC_ADJ_DIG_6], [PRC_ADJ_DIG_7], [PRC_ADJ_DIG_8], [PRC_ADJ_DIG_9], [URM_LEVEL_GROSS_FLAG], [URM_TARGET_MARGIN]
						FROM(
							Select
							SS.zone_segment_fk
							, ATTRIBUTE_EVENT_DATA 'Adjustment'
							, dn.name 'dataname'
							, ss.PROD_SEGMENT_FK
							, dense_rank()over(partition by ss.zone_segment_fk, ss.data_name_fk, ss.prod_segment_fk order by ss.Precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank_Eff
							from epacube.segments_settings ss with (nolock)
							inner join epacube.data_name dn with (nolock) on ss.data_name_fk = dn.data_name_id
							inner join #zones z on ss.ZONE_SEGMENT_FK = z.zone_entity_Structure_FK
							where 1 = 1
							and ss.Effective_Date <= @EFFECTIVE_DATE
							and ss.cust_entity_structure_fk is null
							and ss.PRODUCT_STRUCTURE_FK is not null
							and (ss.reinstate_inheritance_date > @EFFECTIVE_DATE or ss.reinstate_inheritance_date is null)
							and (
									(dn.parent_data_name_fk = 144893
										and ss.entity_data_name_fk = 151000)
									Or 
										ss.data_name_fk in (144891, 144908, 500015, 502046)
								)
						) P 
						PIVOT (Max(p.Adjustment) for DataName in
						 ([REQUIRED_PRICE_CHANGE_AMOUNT], URM_RANGE_ROUNDING_TO_VALUE, [PRC_ADJ_DIG_0], [PRC_ADJ_DIG_1], [PRC_ADJ_DIG_2], [PRC_ADJ_DIG_3], [PRC_ADJ_DIG_4], [PRC_ADJ_DIG_5], [PRC_ADJ_DIG_6], [PRC_ADJ_DIG_7]
						 , [PRC_ADJ_DIG_8], [PRC_ADJ_DIG_9], [URM_LEVEL_GROSS_FLAG], [URM_TARGET_MARGIN]) ) As pivotTable 
						 where DRank_eff = 1
			) D
			) adj on isnull(A.prod_segment_fk, 0) = isnull(adj.prod_segment_fk, 0) --and a.zone_segment_fk = adj.zone_segment_fk 
			order by cast(left(a.item_class_id, Len(a.item_class_id) - 1) as bigint)

	end
end
Else
Begin
	Declare @pvtsql varchar(Max)

	Declare @ZES_FK bigint
	Declare @ZID varchar(64)
	Declare @ZN varchar(64)
	Declare @ZoneCols varchar(Max) = ''
	
	DECLARE Zns Cursor local for
	Select * 
	from #zones z 
	Order by 
		Case isnumeric(reverse(ltrim(rtrim(left(Reverse(Zone_Name ), Charindex(' ',Reverse(Zone_Name ))))))) when 1 then cast(reverse(ltrim(rtrim(left(Reverse(Zone_Name)
		, Charindex(' ',Reverse(Zone_Name)))))) as int) + 50 else Rank()over(order by Zone_Name desc) end desc
				
		OPEN Zns;
		FETCH NEXT FROM Zns INTO @ZES_FK, @ZID, @ZN
		WHILE @@FETCH_STATUS = 0

		Begin

		Set @ZoneCols = @ZoneCols + '[' + @ZID + ']' + ', '

		FETCH NEXT FROM Zns INTO @ZES_FK, @ZID, @ZN

	End

	Close Zns;
	Deallocate Zns;

	Set @ZoneCols = Left(@ZoneCols, len(@ZoneCols) - 1)

	If @Parent_Prod_Segment_FK is null
	Begin
		set @pvtsql = '
		Select 
		A.Scheme_Node
		--, Null Sort2
		--, Null ''r.rules_id''
		, a.data_name_fk ''prod_segment_data_name_fk''
		, a.prod_segment_fk
		, a.parent_prod_segment_fk
		, a.item_class_label
		, a.item_level
		, a.item_class_id
		, a.[Description] ''Item_Class_Description''
		, a.Item_Class_ID + '': '' + a.[Description] ''DEPT_GROUP_SUBGROUP_ITEM''
		, cast(null as bigint) ''alias''
		, ' + @ZoneCols + '
		from (
		Select 1 Scheme_Node, dv.display_seq ''Item_Level'', value ''Item_Class_ID'', dv.[Description], data_name_fk, data_value_id Prod_Segment_fk, Null Parent_Prod_Segment_FK, dn.label ''item_class_label'', dn.PRECEDENCE
		, (select count(*) from epacube.data_value where data_name_fk = dv.data_name_fk + 1 and parent_data_value_fk = dv.data_value_id) ''Subordinates''
		from epacube.data_value dv with (nolock) 
		inner join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
		where data_name_fk = 501043
		union
		Select 2 Scheme_Node, dv.display_seq ''Item_Level'', value ''Item_Class_ID'', dv.[Description], data_name_fk, data_value_id Prod_Segment_fk, Parent_Data_Value_FK ''Parent_Prod_Segment_FK'', dn.label ''item_class_label'', dn.PRECEDENCE
		, (select count(*) from epacube.data_value where data_name_fk = dv.data_name_fk + 1 and parent_data_value_fk = dv.data_value_id) ''Subordinates''
		from epacube.data_value dv with (nolock) 
		inner join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
		where data_name_fk = 501044
		union
		Select 3 Scheme_Node, dv.display_seq ''Item_Level'', value ''Item_Class_ID'', dv.[Description], dv.data_name_fk, data_value_id Prod_Segment_fk, Parent_Data_Value_FK ''Parent_Prod_Segment_FK'', dn.label ''item_class_label'', dn.PRECEDENCE
		, (select count(*) from epacube.segments_product sp with (nolock) where data_name_fk = 501045 and prod_segment_fk = dv.data_value_id) ''Subordinates''
		from epacube.data_value dv with (nolock) 
		inner join epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
		where dv.data_name_fk = 501045
		) A
		left join
		(
		Select
		D.prod_segment_fk, data_name_fk
		, D.item_level, D.class_id, D.Item_Class, ' + @ZoneCols + '
		from (
		Select PROD_SEGMENT_FK
		, DISPLAY_SEQ ''item_level'', value ''class_id'', isnull([description], ''All Items'') ''item_class'', data_name_fk, ' + @ZoneCols + '
			FROM(
						Select
						dv.DISPLAY_SEQ, dv.value, dv.[description]
						, Cast(Attribute_Number as Numeric(18, 4)) ''Adjustment''
						, dn.name ''dataname''
						, ss.PROD_SEGMENT_FK, dv.data_name_fk, eiz.value ''zone_id''
						, dense_rank()over(partition by ss.zone_segment_fk, ss.data_name_fk, ss.prod_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank_val
						from epacube.segments_settings ss with (nolock)
						inner join #zones z on ss.ZONE_SEGMENT_FK = z.zone_entity_Structure_FK
						inner join epacube.data_name dn with (nolock) on ss.data_name_fk = dn.data_name_id
						inner join epacube.data_value dv with (nolock) on ss.prod_segment_fk = dv.data_value_id
						inner join epacube.entity_identification eiz with (nolock) on z.zone_entity_Structure_FK = eiz.ENTITY_STRUCTURE_FK --and eiz.ENTITY_DATA_NAME_FK = 151100
						where 1 = 1
						and cast(ss.Effective_Date as date) <= ''' + cast(@EFFECTIVE_DATE as varchar(16)) + '''
						and ss.cust_entity_structure_fk is null
						and ss.ATTRIBUTE_EVENT_DATA is not null
						and (ss.reinstate_inheritance_date >  ''' + cast(@EFFECTIVE_DATE as varchar(16)) + ''' or ss.reinstate_inheritance_date is null)
						and ss.data_name_fk = 502046
						and dv.data_name_fk between 501043 and 501045
					) P  
					PIVOT (Max(p.adjustment) for zone_id in
					 (' + @ZoneCols + ') ) As pivotTable 
					where  DRank_val = 1
		) D
		) adj on isnull(A.prod_segment_fk, 0) = isnull(adj.prod_segment_fk, 0) and a.DATA_NAME_FK = adj.data_name_fk
		order by scheme_node, cast(Item_Class_ID as int)

	'
	end
	else
		Begin
		set @pvtsql = '
		Select 4 ''Scheme_Node''
		, 110103 ''prod_segment_data_name_fk''
		, product_structure_fk ''prod_segment_fk''
		, D.prod_segment_fk ''parent_prod_segment_fk''
		, ''Item'' ''item_class_label''
		, D.item_level
		, D.item_class_id
		, D.description ''Item_Class_Description''
		, D.Item_Class ''DEPT_GROUP_SUBGROUP_ITEM''
		, (select top 1 value from epacube.data_value dv with (nolock) inner join epacube.product_mult_type pmt with (nolock) on dv.data_value_id = pmt.data_value_fk and pmt.data_name_fk = 500019 and pmt.product_structure_fk = D.product_structure_fk) ''alias''
		, ' + @ZoneCols + '
		, ind_discontinued
		from (
		Select PROD_SEGMENT_FK
		, DISPLAY_SEQ ''item_level'', value ''item_class_id'', product_structure_fk, item + '': '' + [description] ''item_class'', [description], ' + @ZoneCols + ', ind_discontinued
			FROM(
				Select
				60 DISPLAY_SEQ, pi.value, pd.[description]
				, Cast(Case	when isnull(pa.product_association_id, 0) = 0 then -1 
						when dn.data_name_id = 502046 then ss.Attribute_Number
						end as Numeric(18, 4)) ''Adjustment''
				, ''URM_TARGET_MARGIN'' ''dataname''
				, sp.PROD_SEGMENT_FK, 110103 ''data_name_fk'', z.zone ''zone_id'', sp.PRODUCT_STRUCTURE_FK, pi.value ''item''
				, case when pa_500012.product_attribute_id is not null then ''{"renderer":{"color":"red","text-decoration":"underline"}}'' else null end ''ind_discontinued''
			, dense_rank()over(partition by z.zone_entity_structure_fk, ss.prod_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank_val
				from epacube.segments_product sp with (nolock)
				left join epacube.product_attribute pa_500012 with (nolock) on sp.PRODUCT_STRUCTURE_FK = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
				cross join #zones z
				left join epacube.segments_settings ss with (nolock) on sp.PRODUCT_STRUCTURE_FK = ss.PRODUCT_STRUCTURE_FK and ss.PROD_SEGMENT_DATA_NAME_FK = 110103
					and z.zone_entity_Structure_FK = ss.ZONE_SEGMENT_FK
					and cast(ss.Effective_Date as date) <= ''' + cast(@EFFECTIVE_DATE as varchar(16)) + '''
					and (ss.reinstate_inheritance_date >  ''' + cast(@EFFECTIVE_DATE as varchar(16)) + ''' or ss.reinstate_inheritance_date is null)
					and ss.cust_entity_structure_fk is null
					and ss.data_name_fk = 502046
				left join epacube.product_association pa with (nolock) on pa.data_name_fk = 159450 and pa.product_structure_fk = sp.product_structure_fk and z.zone_entity_structure_fk = pa.entity_structure_fk and pa.record_status_cr_fk = 1
				left join epacube.data_name dn with (nolock) on ss.data_name_fk = dn.data_name_id
				inner join epacube.product_identification pi with (nolock) on sp.PRODUCT_STRUCTURE_FK = pi.product_structure_fk and pi.data_name_fk = 110100
				inner join epacube.product_description pd with (nolock) on pi.product_structure_fk = pd.product_structure_fk and pd.data_name_fk = 110401
				where 1 = 1
				and sp.prod_segment_fk = ' + @Parent_Prod_Segment_FK + '
				and sp.DATA_NAME_FK = 501045
			) P  
			PIVOT (Max(p.adjustment) for zone_id in
				(' + @ZoneCols + ') ) As pivotTable 
			where  DRank_val = 1
		) D
		order by cast(left(D.item_class_id, Len(D.item_class_id) - 1) as bigint)
	'
	End

	exec(@pvtsql)
End

