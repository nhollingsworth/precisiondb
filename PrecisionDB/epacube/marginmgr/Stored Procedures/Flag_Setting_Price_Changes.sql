﻿CREATE PROCEDURE [marginmgr].[Flag_Setting_Price_Changes]
	@PRICESHEET_RETAIL_CHANGES_ID bigint = Null
	, @FLAG VARCHAR(16)
	, @VALUE INT
	, @USER VARCHAR(96)
	, @AutocalculateTM int = 0
	, @Alias_ID BIGINT = Null
	, @Product_Structure_FK BIGINT = Null
	, @Qualified_Status_CR_FK int = Null
	, @Price_Mgmt_Type_CR_FK int = Null
	, @Effective_Date date = Null
As

	Set NoCount On

	--Declare @PRICESHEET_RETAIL_CHANGES_ID bigint = 11456456
	--Declare @Alias_ID BIGINT = 76547
	--Declare @Product_Structure_FK BIGINT = Null

	--Declare @FLAG VARCHAR(16) = 'HOLD_CUR_PRICE' --'reviewed'
	----Declare @FLAG VARCHAR(16) = 'reviewed'
	--Declare @VALUE INT = 1
	--Declare @USER VARCHAR(96) = 'gstone@epacube.com'
	--Declare @AutocalculateTM int = 0
	--Declare @Qualified_Status_CR_FK int = Null --653
	--Declare @Price_Mgmt_Type_CR_FK int = Null --650
	--Declare @Effective_Date date = Null

	Declare @Item_Change_Type_CR_FK int = Null
	Declare @Entity_Structure_FK bigint

	declare @PMgmtT_CR_FK int
	Declare @ALT_PRICESHEET_RETAIL_CHANGES_ID bigint
	Declare @Price_Change_Effective_Date date
	Declare @Alias_ID2 bigint

	set @Entity_Structure_FK = (select ZONE_ENTITY_STRUCTURE_FK from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID)
	set @PMgmtT_CR_FK = (select PRICE_MGMT_TYPE_CR_FK from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID)
	set @Alias_ID2 = (select Alias_ID from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID)
	Set @Price_Change_Effective_Date = Case when isnull(@Effective_Date, '') = '' then (select PRICE_CHANGE_EFFECTIVE_DATE from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID) else @Effective_Date end
	set @ALT_PRICESHEET_RETAIL_CHANGES_ID = isnull((select PRICESHEET_RETAIL_CHANGES_ID from marginmgr.PRICESHEET_RETAIL_CHANGES
			Where PRICE_CHANGE_EFFECTIVE_DATE = @Price_Change_Effective_Date 
				and ZONE_ENTITY_STRUCTURE_FK = @Entity_Structure_FK 
				and price_mgmt_type_cr_fk <> @PMgmtT_CR_FK
				and ITEM_CHANGE_TYPE_CR_FK = 656
				and alias_id = @Alias_ID2), 0)

	Declare @Cutoff as time
	Declare @Time varchar(20)

	Set @Cutoff = (select value from epacube.epacube_params WHERE EPACUBE_PARAMS_ID = 112000)

	Set @Time = replace(convert(varchar(20), @Cutoff, 22), ':00 ', ' ')

	If (cast(getdate() as time) >= @Cutoff and @Price_Change_Effective_Date = cast(getdate() as date)) or cast(getdate() as date) > @Price_Change_Effective_Date
	Begin
		Select
		(Select	'You have missed today''s cutoff time of ' + @Time + '.<br><br>The values just entered have not been accepted.<br><br>Please mark down you changes and make them in a View or Zone Price Settings with an effective date of tomorrow or later') 'USER_MSG'
		, 1 'reset_values'
		, 5000 'user_msg_timeout_ms'
		goto eof
	End

	Set @Effective_Date = isnull(@Effective_Date, cast(getdate() + 6 - datepart(dw, getdate()) as date))

	insert into precision.user_call_statements
	(create_user, ui_name, procedure_called, procedure_parameters, table_name, table_row_id, prod_segment_fk, entity_segment_fk)
	Select @user 'create_user'
	, 'Zone_Price_Changes' 'ui_name'
	, '[marginmgr].[Flag_Setting_Price_Changes]' 'procedure_called'
	, isnull(cast(@PRICESHEET_RETAIL_CHANGES_ID as varchar(24)), 'Null') + ',
	 ''' + isnull(cast(@FLAG as varchar(24)), 'Null') + ''',
	 ' + isnull(cast(@VALUE as varchar(24)), 'Null') + ',
	 ''' + isnull(cast(@USER as varchar(96)), 'Null') + ''',
	 ' + isnull(cast(@AutocalculateTM as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@Alias_ID as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@Product_Structure_FK as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@Qualified_Status_CR_FK as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@Price_Mgmt_Type_CR_FK as varchar(24)), 'Null') + ',
	 ''' + isnull(cast(@Effective_Date as varchar(24)), 'Null') + '''' 'procedure_parameters'
	, '[marginmgr].[pricesheet_retail_changes]' 'table_name'
	, @PRICESHEET_RETAIL_CHANGES_ID 'table_row_id', (select top 1 product_structure_fk from [marginmgr].[pricesheet_retail_changes] where pricesheet_retail_changes_id = @PRICESHEET_RETAIL_CHANGES_ID) 'Prod_Structure_fk', (select top 1 ZONE_ENTITY_STRUCTURE_FK from [marginmgr].[pricesheet_retail_changes] where pricesheet_retail_changes_id = @PRICESHEET_RETAIL_CHANGES_ID) 'entity_structure_fk'

	If @PRICESHEET_RETAIL_CHANGES_ID is not null and isnull(@ALT_PRICESHEET_RETAIL_CHANGES_ID, 0) = 0
	Begin
		exec marginmgr.Flag_Setting_Price_Changes_Process  @PRICESHEET_RETAIL_CHANGES_ID, @FLAG, @VALUE, @USER, @AutocalculateTM
		goto eof
	End

	Declare @prc_id bigint

	If object_id('tempdb..#PRCT') is not null
	drop table #PRCT

	Create table #PRCT(PRICESHEET_RETAIL_CHANGES_ID BIGINT NULL, HOLD_CUR_PRICE INT NULL, SRP_ADJUSTED MONEY NULL, GM_ADJUSTED NUMERIC(18, 4) NULL, TM_ADJUSTED NUMERIC(18, 4) NULL, REVIEWED INT NULL, PRICE_MULTIPLE_NEW INT NULL, USER_MSG VARCHAR(128) NULL)

	--Set @Effective_Date = isnull(@Effective_Date, cast(getdate() + 6 - datepart(dw, getdate()) as date))

	DECLARE Pks Cursor local for

	select pricesheet_retail_changes_id from marginmgr.pricesheet_retail_changes 
	where 1 = 1
	and price_change_effective_date = @Effective_Date
	and (
			(Alias_ID = @Alias_ID and @Alias_ID is not null)
			Or
			(Product_Structure_FK = @Product_Structure_FK and alias_id is null)
		)
	and (
			(ITEM_CHANGE_TYPE_CR_FK in (656, 662) and alias_id is not null)
			Or
			(ITEM_CHANGE_TYPE_CR_FK not in (656, 662) and alias_id is null)
		)
	and (PRICE_MGMT_TYPE_CR_FK = @Price_Mgmt_Type_CR_FK or @Price_Mgmt_Type_CR_FK = 0)
	and (QUALIFIED_STATUS_CR_FK = @Qualified_Status_CR_FK or @Qualified_Status_CR_FK = 0)
	and isnull(REVIEWED, 0) = case when @FLAG = 'Reviewed' and @Value = 1 then 0 else isnull(reviewed, 0) end
	and isnull(@ALT_PRICESHEET_RETAIL_CHANGES_ID, 0) = 0
	Union
	Select @PRICESHEET_RETAIL_CHANGES_ID where isnull(@ALT_PRICESHEET_RETAIL_CHANGES_ID, 0) <> 0
	Union
	Select @ALT_PRICESHEET_RETAIL_CHANGES_ID where isnull(@ALT_PRICESHEET_RETAIL_CHANGES_ID, 0) <> 0

		OPEN Pks;
		FETCH NEXT FROM Pks INTO @prc_id
	            
			WHILE @@FETCH_STATUS = 0
			Begin

			INSERT INTO #PRCT
			exec marginmgr.Flag_Setting_Price_Changes_Process  @prc_id, @FLAG, @VALUE, @USER, @AutocalculateTM

		FETCH NEXT FROM Pks INTO @prc_id  
			End
	Close Pks;
	Deallocate Pks

	SELECT * FROM #PRCT
eof:

