﻿---- =========================================================
---- Author:		Gary Stone
---- Create date: May 4, 2018
------ Description:	Retail_Setting_Updates_Zone_Dollar_Amounts
--	Modification
--		5/13/2020	Gary Stone	URM-1852	
---- =========================================================
CREATE PROCEDURE [marginmgr].[Retail_Setting_Updates_Zone_Dollar_Amounts]
@Pricing_Date Date
, @Zone_Segment_FK bigint
, @Data_Name Varchar(64)
, @Value Numeric(18, 2) = Null
, @Prod_Segment_Data_Name_FK bigint	= Null
, @Prod_Segment_FK bigint = Null
, @Reinstate_Inheritance int = 0
, @User varchar(64)

AS

/*

--marginmgr.Retail_Setting_Updates_Zone '2019-11-18', 113230, 'PRENOTIFY_DAYS', '6', null, null, 110103, 0, 0, 0, 'gstone@epacube.com'
--marginmgr.Retail_Setting_Updates_Zone '2019-11-18', 113230, 'REQUIRED_PRICE_CHANGE_AMOUNT', '.04', null, null, 110103, 0, 0, 0, 'gston
--marginmgr.Retail_Setting_Updates_Zone '2019-11-18', 113230, 'PRC_ADJ_DIG_2', '0.07', null, null, 110103, 0, 0, 0, 'gstone@epacube.com
--marginmgr.Retail_Setting_Updates_Zone '2019-11-18', 113230, 'PRC_ADJ_DIG_2', '0.06', 501043, 1014719243, 110103, 0, 0, 0, 'gstone@epacube.com'
--marginmgr.Retail_Setting_Updates_Zone '2019-11-18', 113230, 'PRC_ADJ_DIG_3', '0.05', 110103, 303236, 110103, 0, 0, 0, 'gstone@epacube.com'
--marginmgr.Retail_Setting_Updates_Zone '2019-11-21', 113230, 'PRC_ADJ_DIG_4', '0.04', 110103, 13128, 110103, 0, 0, 0, 'gstone@epacube.com'
--marginmgr.Retail_Setting_Updates_Zone '2019-11-25', 113246, 'REQUIRED_PRICE_CHANGE_AMOUNT', '', 110103, 39674, 110103, 0, 0, 0, 'gstone@epacube.com'
--marginmgr.Retail_Setting_Updates_Zone '2019-11-25', 113246, 'REQUIRED_PRICE_CHANGE_AMOUNT', '0.09', 110103, 13128, 110103, 0, 0, 0, 'gstone@epacube.com'

--marginmgr.Retail_Setting_Updates_Zone '2020-01-16', 113247, 'REQUIRED_PRICE_CHANGE_AMOUNT', '0.03', 501043, 1014719243, 110103, 0, 0, 0, 'gstone@epacube.com'
--marginmgr.Retail_Setting_Updates_Zone '2020-01-16', 113247, 'REQUIRED_PRICE_CHANGE_AMOUNT', '0.04', 501044, 1014719257, 110103, 0, 0, 0, 'gstone@epacube.com'
--marginmgr.Retail_Setting_Updates_Zone '2020-01-19', 113230, 'REQUIRED_PRICE_CHANGE_AMOUNT', '0.02', 501045, 1014719558, 110103, 0, 0, 0, 'gstone@epacube.com'
--marginmgr.Retail_Setting_Updates_Zone '2020-01-25', 113230, 'REQUIRED_PRICE_CHANGE_AMOUNT', '0', 110103, 13128, 110103, 0, 0, 1, 'gstone@epacube.com'
--marginmgr.Retail_Setting_Updates_Zone '2020-04-15', 113234, 'PRC_ADJ_DIG_2', '0.04', 501045, 1014720430, 501044, 0, 0, 0, 'gstone@epacube.com'

	Declare @Pricing_Date Date = '2020-05-13'
	Declare @Zone_Segment_FK bigint = 113246
	Declare @Data_Name Varchar(64) = 'REQUIRED_PRICE_CHANGE_AMOUNT'
	Declare @Value Numeric(18, 2) = 0
	Declare @Prod_Segment_Data_Name_FK bigint = 501043
	Declare @Prod_Segment_FK bigint = 1014719245
	Declare @User varchar(64) = 'gstone@epacube_com'
	Declare @Reinstate_Inheritance int = 0

*/

Set NoCount On

	If Object_id('tempdb..#prodsegments') is not null
		drop table #prodsegments
	If Object_id('tempdb..#prodsegments1') is not null
		drop table #prodsegments1

	Declare @Data_Name_FK bigint
	Declare @CurValue Numeric(18, 2)
	Declare @InheritVal Numeric(18, 4)
	Declare @Precedence int

	Set @Data_Name_FK = (select data_name_id from epacube.data_name where name = @Data_Name)
	Set @Precedence = isnull((select precedence from epacube.data_name where data_name_id = @Prod_Segment_Data_Name_FK), 100)

If @Prod_Segment_Data_Name_FK is null
Begin
--	Zone Level Data	
	Delete from epacube.segments_settings where data_name_fk = @data_name_fk and prod_segment_data_name_fk is null and zone_segment_fk = @zone_segment_fk and effective_date = @pricing_Date

	Set @CurValue = (select top 1 attribute_number from epacube.segments_settings where data_name_fk = @data_name_fk and prod_segment_data_name_fk is null and zone_segment_fk = @zone_segment_fk and effective_date <= @pricing_Date order by effective_date desc)

	--print cast(@CurValue as varchar(16)) + ' - ' + cast(@Value as varchar(16)) 

	If @value is not null and @CurValue <> @Value
		Insert into epacube.epacube.segments_settings
		(Effective_Date, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, ZONE_SEGMENT_FK, Data_Name_FK, ENTITY_DATA_NAME_FK, Attribute_Number, Attribute_Event_Data, Record_Status_CR_FK, Create_User, Precedence, [DATA_SOURCE])
				Select 
				cast(@Pricing_Date as date) 'effective_date'
				, 1 'org_entity_structure_fk'
				, @Zone_Segment_FK 'zone_entity_structure_fk'
				, @Zone_Segment_FK 'zone_segment_fk'
				, dn.data_name_id 'data_name_fk'
				, 151000 'entity_data_name_fk'
				, case when @data_name_fk = 144868 then cast(@Value as Numeric(18, 0)) else @Value end 'attribute_number'
				, case when @data_name_fk = 144868 then cast(@Value as Numeric(18, 0)) else @Value end 'attribute_event_data'
				, 1 'record_status_cr_fk'
				, @User 'create_user'
				, (select precedence from epacube.data_name where data_name_id = @Prod_Segment_Data_Name_FK) 'Precedence'
				, 'USER UPDATE' 'DATA_SOURCE'
				from 
				epacube.epacube.data_name dn with (nolock)
				inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.DATA_SET_ID
				left join epacube.epacube.data_value dv with (nolock) on dv.data_value_id = @Prod_Segment_FK
				where 1 = 1
					AND DN.NAME = @Data_Name

	--Return data to UI
		Select pricing_date
		, zone_segment_fk
		, data_name
		, value
		, prod_segment_data_name_fk
		, prod_segment_fk
		, seq
		, sort_value
		from (
				Select @Pricing_Date 'pricing_date'
				, @zone_segment_fk 'zone_segment_fk'
				, ss.DATA_NAME_FK 'data_name'
				, case when @data_name_fk = 144868 then cast(ss.attribute_number as Numeric(18, 0)) else cast(ss.attribute_number as Numeric(18, 2)) end 'value'
				, cast(ss.prod_segment_data_name_fk as bigint) 'prod_segment_data_name_fk'
				, cast(ss.prod_segment_fk as bigint) 'prod_segment_fk'
				, '1' 'seq'
				, ss.attribute_number 'sort_value'
				, Dense_Rank()over(partition by ss.data_name_fk order by ss.effective_date desc, segments_settings_id desc) DRank
				from epacube.segments_settings ss with (nolock) 
				where ss.data_name_fk = @data_name_fk
				and ss.Effective_Date <= @Pricing_Date
				and ss.zone_segment_fk = @Zone_Segment_FK
				and ss.PROD_SEGMENT_DATA_NAME_FK is null
				and ss.PROD_SEGMENT_FK is null
				and ss.RECORD_STATUS_CR_FK = 1
			) A where DRank = 1

	goto eof
End

--	Zone Item Class level data
Create table #prodsegments1(prod_segment_fk bigint null, parent_prod_segment_fk bigint null, prod_segment_data_name_fk bigint null, precedence int null, alias_id bigint null)
Create table #prodsegments(prod_segment_fk bigint null, parent_prod_segment_fk bigint null, prod_segment_data_name_fk bigint null, precedence int null, alias_id bigint null)

Insert into #prodsegments1 (prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
	select Null 'prod_segment_fk', null 'parent_data_value_fk', null 'prod_segment_data_name_fk', 100 'precedence'
	Union
	Select a.*, dn.precedence
	from (
	select dv.data_value_id 'prod_segment_fk', dv.parent_data_value_fk, dv.data_name_fk 'prod_segment_data_name_fk' from epacube.data_value dv where data_value_id = (select dv.PARENT_DATA_VALUE_FK from epacube.data_value dv where data_value_id = (select PARENT_DATA_VALUE_FK from epacube.data_value dv where data_value_id = @prod_segment_fk))
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where data_value_id = (select PARENT_DATA_VALUE_FK from epacube.data_value dv where data_value_id = @prod_segment_fk)
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where data_value_id = @prod_segment_fk
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where data_value_id = @prod_segment_fk
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where parent_data_value_fk in (select dv.data_value_id from epacube.data_value dv where data_value_id = @prod_segment_fk)
	Union
	select dv.data_value_id, dv.parent_data_value_fk, dv.data_name_fk from epacube.data_value dv where parent_data_value_fk in (select dv.data_value_id from epacube.data_value dv where parent_data_value_fk = (select dv.data_value_id from epacube.data_value dv where data_value_id = @prod_segment_fk))
	) A inner join epacube.data_name dn with (nolock) on a.prod_segment_data_name_fk = dn.DATA_NAME_ID 

	create index idx_01 on #prodsegments1(prod_segment_fk, prod_segment_data_name_fk)
	create index idx_01 on #prodsegments(prod_segment_fk, prod_segment_data_name_fk)

	If @Prod_Segment_Data_Name_FK = 110103
	Begin

		insert into #prodsegments1(prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
		select @prod_segment_fk, null 'parent_prod_segment_fk', 110103 'prod_segment_data_name_fk', 5 'precedence'

			insert into #prodsegments1(prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
			select sp.PRODUCT_STRUCTURE_FK, null 'parent_prod_segment_fk', 110103 'prod_segment_data_name_fk', 5 'precedence' 
			from #prodsegments1 s
			inner join epacube.segments_product sp with (nolock) on s.prod_segment_fk = sp.PROD_SEGMENT_FK and s.prod_segment_data_name_fk = sp.DATA_NAME_FK
			left join epacube.product_attribute pa_500012 with (nolock) on sp.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
			where sp.data_name_fk = 501045
			and pa_500012.product_attribute_id is null

			Insert into #prodsegments1(prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
			select pmt.product_structure_fk, null 'parent_prod_segment_fk', 110103 'prod_segment_data_name_fk', 5 'precedence'
			from #prodsegments1 s
			inner join epacube.product_mult_type pmt1 with (nolock) on s.prod_segment_fk = pmt1.PRODUCT_STRUCTURE_FK and pmt1.ENTITY_STRUCTURE_FK = @Zone_Segment_FK and pmt1.data_name_fk = 500019
			inner join epacube.product_mult_type pmt with (nolock) on pmt1.data_value_fk = pmt.data_value_fk and pmt1.ENTITY_STRUCTURE_FK = pmt.ENTITY_STRUCTURE_FK and pmt.DATA_NAME_FK = 500019
			left join epacube.product_attribute pa_500012 with (nolock) on pmt.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
			where 1 = 1 
			and pa_500012.product_attribute_id is null
			group by pmt.product_structure_fk

			insert into #prodsegments1 
			(prod_segment_fk, parent_prod_segment_fk, prod_segment_data_name_fk, precedence)
			select distinct
			dv.data_value_id 'prod_segment_fk', dv.PARENT_DATA_VALUE_FK,spp.data_name_fk 'prod_segment-_data_name_fk', dn.precedence
			from #prodsegments1 ps
			inner join epacube.segments_product sp with (nolock) on ps.prod_segment_fk = sp.product_structure_fk and ps.prod_segment_data_name_fk = sp.DATA_NAME_FK
			inner join epacube.segments_product spp with (nolock) on sp.product_structure_fk = spp.product_structure_fk and spp.data_name_fk not in (110100, 110103)
			inner join epacube.data_value dv with (nolock) on spp.prod_segment_fk = dv.data_value_id
			inner join epacube.data_name dn with (nolock) on spp.DATA_NAME_FK = dn.DATA_NAME_ID
			--where dv.data_value_id not in (select prod_segment_fk from #prodsegments1)
			
	End

	Insert into #prodsegments
	select distinct * from #prodsegments1

	update ps
	set alias_id = dv.value
	from #prodsegments ps
	inner join epacube.product_mult_type pmt on ps.prod_segment_fk = pmt.PRODUCT_STRUCTURE_FK and pmt.ENTITY_STRUCTURE_FK = @Zone_Segment_FK and pmt.DATA_NAME_FK = 500019
	inner join epacube.data_value dv on pmt.data_value_fk = dv.data_value_id

	
	if @Reinstate_Inheritance = 1 and @Value is null
	Begin
			Delete ss
			from epacube.segments_settings ss
			inner join #prodsegments ps on ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk
										and ss.PROD_SEGMENT_FK = ps.prod_segment_fk
			where 1 = 1
			and ss.data_name_fk = @Data_Name_FK
			and ss.PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
			and ss.ZONE_SEGMENT_FK = @Zone_Segment_FK
			and ss.effective_date = cast(@pricing_date as date)
			
			goto ReturnRecords
	End
	
	Set @CurValue = 
		Isnull(
			(	
				select top 1 ss.attribute_number
				from epacube.segments_settings ss
				where ss.data_name_fk = @Data_Name_FK
				--and ss.reinstate_inheritance_date is null
				and (ss.reinstate_inheritance_date > @Pricing_Date or ss.reinstate_inheritance_date is null)
				and ss.effective_date <= @Pricing_Date
				and ss.zone_segment_fk = @Zone_Segment_FK
				and ss.PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
				and ss.PROD_SEGMENT_FK = @Prod_Segment_FK
				order by Effective_Date desc
			)
			 , -0.99)

	Set @InheritVal = Isnull(
		(Select InheritVal from (
								Select data_name_fk, InheritVal, effective_date, precedence, dense_rank()over(partition by data_name_fk order by isnull(precedence, 100), effective_date desc, segments_settings_id desc) DRank
								from (
								select ss.data_name_fk, ss.ATTRIBUTE_NUMBER 'InheritVal', ss.effective_date, ss.precedence, ss.segments_settings_id
								from epacube.segments_settings ss
								inner join #prodsegments ps on isnull(ss.PROD_SEGMENT_FK, 0) = isnull(ps.prod_segment_fk, 0) and isnull(ss.PROD_SEGMENT_DATA_NAME_FK, 0) = isnull(ps.prod_segment_data_name_fk, 0)
								where 1 = 1
								and ss.ZONE_SEGMENT_FK = @Zone_Segment_FK
								and ss.data_name_fk = @data_name_fk
								and ss.effective_date <= @Pricing_Date
								and isnull(ps.precedence, 100) > isnull((select top 1 precedence from #prodsegments where prod_segment_fk = @Prod_Segment_FK and PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK), 100)
								--and ss.reinstate_inheritance_date is null
								and (ss.reinstate_inheritance_date > @Pricing_Date or ss.reinstate_inheritance_date is null)
								) a 
							) b where DRank = 1)
		, 0.00)

	If @Reinstate_Inheritance = 1
	Begin
		--Update SS
		--Set reinstate_inheritance_date = @Pricing_Date
		--from epacube.segments_settings ss
		--where 1 = 1
		--and data_name_fk = @Data_Name_FK
		--and PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
		--and PROD_SEGMENT_FK = @Prod_Segment_FK
		--and ZONE_SEGMENT_FK = @Zone_Segment_FK
		--and effective_date < @Pricing_Date
		--and reinstate_inheritance_date is null

		--If @Value = 0
		--Update SS
		--Set reinstate_inheritance_date = Null
		--from epacube.segments_settings ss
		--where 1 = 1
		--and data_name_fk = @Data_Name_FK
		--and PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
		--and PROD_SEGMENT_FK = @Prod_Segment_FK
		--and ZONE_SEGMENT_FK = @Zone_Segment_FK
		--and effective_date < @Pricing_Date
		--and reinstate_inheritance_date = @Pricing_Date

		Update SS
		Set reinstate_inheritance_date = @Pricing_Date
		from epacube.segments_settings ss
		inner join #prodsegments ps on ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk
									and ss.PROD_SEGMENT_FK = ps.prod_segment_fk
		where 1 = 1
		and data_name_fk = @Data_Name_FK
		and ss.PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
--		and PROD_SEGMENT_FK = @Prod_Segment_FK
		and ZONE_SEGMENT_FK = @Zone_Segment_FK
		and effective_date < @Pricing_Date
		and reinstate_inheritance_date is null

		If @Value = 0
		Begin
		Update SS
			Set reinstate_inheritance_date = Null
			from epacube.segments_settings ss
			inner join #prodsegments ps on ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk
										and ss.PROD_SEGMENT_FK = ps.prod_segment_fk
			where 1 = 1
			and data_name_fk = @Data_Name_FK
			and ss.PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
			and ZONE_SEGMENT_FK = @Zone_Segment_FK
			and effective_date < @Pricing_Date
			and reinstate_inheritance_date = @Pricing_Date

			--Delete ss
			--from epacube.segments_settings ss
			--inner join #prodsegments ps on ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk
			--							and ss.PROD_SEGMENT_FK = ps.prod_segment_fk
			--where 1 = 1
			--and ss.data_name_fk = @Data_Name_FK
			--and ss.PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
			--and ss.ZONE_SEGMENT_FK = @Zone_Segment_FK
			--and ss.effective_date = cast(@pricing_date as date)

		End
		goto ReturnRecords
	end

	Delete ss 
	from epacube.epacube.segments_settings ss 
	inner join #prodsegments ps on ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk
								and ss.PROD_SEGMENT_FK = ps.prod_segment_fk
	where 1 = 1
		and ss.data_name_fk = @data_name_fk
		and ss.effective_date = cast(@pricing_date as date)
		and ss.ZONE_SEGMENT_FK = @Zone_Segment_FK
		and ss.PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
		and ps.prod_segment_fk is not null

	delete ss 
	from epacube.segments_settings ss
	where 1 = 1
		and ss.DATA_NAME_FK = @Data_Name_FK
		and ss.PROD_SEGMENT_DATA_NAME_FK = @Prod_Segment_Data_Name_FK
		and ss.PROD_SEGMENT_FK = @Prod_Segment_FK
		and ss.zone_segment_fk = @Zone_Segment_FK
		and ss.Effective_Date = cast(@Pricing_Date as date)

	--If @Reinstate_Inheritance = 0 and @Prod_Segment_Data_Name_FK <> 110103 and @value is not null and @value <> @InheritVal
	If @Reinstate_Inheritance = 0 and @Prod_Segment_Data_Name_FK <> 110103 and @value is not null 
		and 
				(@value <> @CurValue)
		
		Insert into epacube.epacube.segments_settings
		(Effective_Date, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, ZONE_SEGMENT_FK, PROD_SEGMENT_DATA_NAME_FK, PROD_SEGMENT_FK, Data_Name_FK, ENTITY_DATA_NAME_FK, Attribute_Number, Attribute_Event_Data
		, Record_Status_CR_FK, Create_User, PRODUCT_STRUCTURE_FK, Prod_Data_Value_FK, Precedence, [DATA_SOURCE])
				Select 
				cast(@Pricing_Date as date) 'effective_date'
				, 1 'org_entity_structure_fk'
				, @Zone_Segment_FK 'zone_entity_structure_fk'
				, @Zone_Segment_FK 'zone_segment_fk'
				, @PROD_SEGMENT_DATA_NAME_FK 'PROD_SEGMENT_DATA_NAME_FK'
				, @Prod_Segment_FK 'prod_segment_fk'
				, @Data_Name_FK 'data_name_fk'
				, 151000 'entity_data_name_fk'
				, @VALUE 'attribute_number'
				, @vALUE 'attribute_event_data'
				, 1 'record_status_cr_fk'
				, @User 'create_user'
				, Case when isnull(@Prod_Segment_Data_Name_FK, 0) = 110103 then @Prod_Segment_FK else Null end 'Product_Structure_FK'
				, Case when isnull(@Prod_Segment_Data_Name_FK, 0) = 110103 then Null else @Prod_Segment_FK end 'Prod_Data_Value_FK'
				, (select precedence from epacube.data_name where data_name_id = @Prod_Segment_Data_Name_FK) 'Precedence'
				, 'USER UPDATE' 'DATA_SOURCE'
	Else
	If @Reinstate_Inheritance = 0 and @Prod_Segment_Data_Name_FK = 110103 and @value is not null and @value <> @InheritVal
	Insert into epacube.epacube.segments_settings
		(Effective_Date, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, ZONE_SEGMENT_FK, PROD_SEGMENT_DATA_NAME_FK, PROD_SEGMENT_FK, Data_Name_FK, ENTITY_DATA_NAME_FK, Attribute_Number, Attribute_Event_Data
		, Record_Status_CR_FK, Create_User, PRODUCT_STRUCTURE_FK, Prod_Data_Value_FK, Precedence, [DATA_SOURCE])
				Select 
				cast(@Pricing_Date as date) 'effective_date'
				, 1 'org_entity_structure_fk'
				, @Zone_Segment_FK 'zone_entity_structure_fk'
				, @Zone_Segment_FK 'zone_segment_fk'
				, ps.PROD_SEGMENT_DATA_NAME_FK 'PROD_SEGMENT_DATA_NAME_FK'
				, ps.Prod_Segment_FK 'prod_segment_fk'
				, @Data_Name_FK 'data_name_fk'
				, 151000 'entity_data_name_fk'
				, @VALUE 'attribute_number'
				, @vALUE 'attribute_event_data'
				, 1 'record_status_cr_fk'
				, @User 'create_user'
				, Case when isnull(ps.Prod_Segment_Data_Name_FK, 0) = 110103 then ps.Prod_Segment_FK else Null end 'Product_Structure_FK'
				, Case when isnull(ps.Prod_Segment_Data_Name_FK, 0) = 110103 then Null else ps.Prod_Segment_FK end 'Prod_Data_Value_FK'
				, ps.precedence 'Precedence'
				, 'USER UPDATE' 'DATA_SOURCE'
				from #prodsegments ps
				where prod_segment_data_name_fk = 110103

--Return appropriate values to UI
ReturnRecords:

	Select 
		@pricing_date 'pricing_date'
		, isnull(zone_segment_fk, @zone_segment_fk) 'zone_segment_fk'
		, @DATA_NAME 'data_name'
		, Cast(Cast(
		  case	when isnull(reinstate_inheritance_date, dateadd(d, 1, @Pricing_Date)) <= @Pricing_Date then Null
				when @reinstate_inheritance = 1 then ATTRIBUTE_NUMBER
				when @Reinstate_Inheritance = 1 then null
				when @value = isnull(@Inheritval, 0) and @value = isnull(@CurValue, 0) then null
				when ATTRIBUTE_NUMBER is not null then ATTRIBUTE_NUMBER
				--when ATTRIBUTE_NUMBER is not null and ATTRIBUTE_NUMBER <> @InheritVal then ATTRIBUTE_NUMBER
				when isnull(@value, 0.00) = 0.00 and ATTRIBUTE_NUMBER is not null then attribute_number
				when @value = 0.00 and ATTRIBUTE_NUMBER is null then Null
				when @CurValue = @Value then Null
				else @Value end as numeric(18, 2)) as varchar(64)) 'value'
		, isnull(prod_segment_data_name_fk, @prod_segment_data_name_fk) 'prod_segment_data_name_fk'
		, isnull(prod_segment_fk, @prod_segment_fk) 'prod_segment_fk'
		, 1 'seq'
		, cast(@value as Numeric(18, 2)) 'sort_value'
		, reinstate_inheritance_date
		--, 'this is a test' 'USER_MSG'
		from (
		Select ss.ATTRIBUTE_NUMBER, ps.prod_segment_fk, @Zone_Segment_FK 'ZONE_SEGMENT_FK', ss.SEGMENTS_SETTINGS_ID, ps.PROD_SEGMENT_DATA_NAME_FK
		, dense_rank()over(partition by ps.prod_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank
		, ss.Effective_Date, @data_name_fk 'data_name_fk', ps.precedence
		, ss.reinstate_inheritance_date
		from #prodsegments ps
		left join epacube.segments_settings ss with (nolock) on ss.PROD_SEGMENT_DATA_NAME_FK = ps.prod_segment_data_name_fk and ss.PROD_SEGMENT_FK = ps.prod_segment_fk and	ss.data_name_fk = @data_name_fk
			and ss.Effective_Date <= @Pricing_Date
				and ss.zone_segment_fk = @Zone_Segment_FK
				and ss.RECORD_STATUS_CR_FK = 1
		where 1 = 1
		and ps.precedence = @Precedence
		) a where 1 = 1
				and Drank = 1

eof:

--Select @Value '@Value', @CurValue '@CurValue', @InheritVal '@InheritVal', @Precedence '@Precedence', @data_name_fk '@data_name_fk', @Prod_Segment_Data_Name_FK '@Prod_Segment_Data_Name_FK', @Prod_Segment_FK '@Prod_Segment_FK', @Reinstate_Inheritance '@Reinstate_Inheritance', (select value from epacube.entity_identification where ENTITY_DATA_NAME_FK = 151000 and ENTITY_STRUCTURE_FK = @Zone_Segment_FK) 'zone_id'
