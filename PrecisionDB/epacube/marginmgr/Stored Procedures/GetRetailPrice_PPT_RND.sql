﻿-- =============================================
-- Author:		Gary Stone
-- Create date: April 9, 2018
-- Description:	Get Retail Price, Rounded and with Price Points
-- =============================================
CREATE PROCEDURE [marginmgr].[GetRetailPrice_PPT_RND]
	@PRICESHEET_RETAIL_CHANGES_ID bigint = Null
	, @GP_New Numeric(18, 4) = Null
	, @Price_New Numeric(18, 4) = Null
	, @TM_Adjusted Numeric(18, 4) = Null
	, @Price_Multiple_New int = 1
	, @User varchar(96)
	, @PPT int = 1
	, @AutoCalcTM int = 0

AS
	SET NOCOUNT ON;

/*
--	exec MARGINMGR.GetRetailPrice_PPT_RND 11572701, null, null, 0.26, 1, 'gstone@epacube.com', 1

	Declare @PRICESHEET_RETAIL_CHANGES_ID bigint = 11572701
	Declare @GP_New Numeric(18, 4) = null --0 --0.22--0.65
	Declare @Price_New Numeric(18, 4) = null--5.39
	Declare @TM_Adjusted Numeric(18, 4) = 0.26
	--Declare @TM_New Numeric(18, 4) = 0
	Declare @Price_Multiple_New int = 1
	Declare @user varchar(64) = 'gstone@epacube_com'
	Declare @PPT int = 1
	Declare @AutoCalcTM int = 1

*/
/*
Insert into marginmgr.PRICESHEET_RETAIL_CHANGES_USER_TRACE
(
[PROCEDURE_NAME]
, DATA_STREAM
, PRICESHEET_RETAIL_CHANGES_FK
, CREATE_USER
, GP_New
, Price_New
, TM_Adjusted
, Price_Multiple_New
, PPT
, AutoCalcTM
)
Values
(
	'marginmgr.GetRetailPrice_PPT_RND'
	, 'In'
	, @PRICESHEET_RETAIL_CHANGES_ID
	, @User
	, @GP_New
	, @Price_New
	, @TM_Adjusted
	, @Price_Multiple_New
	, @PPT
	, @AutoCalcTM
)
*/

	Set @Price_New = Case when isnull(@GP_New, 0) <> 0 then null else @Price_New end

	Declare @Cur_Cost Numeric(18, 4)
	Declare @New_Cost Numeric(18, 4)
	Declare @SRP_CUR Numeric(18, 4)
	Declare @CUR_GM_PCT Numeric(18, 4)
	Declare @Target_Margin Numeric(18, 4)
	Declare @Product_Structure_FK Bigint
	Declare @Entity_Structure_FK Bigint
	Declare @SRP_Initial Numeric(18, 2)
	Declare @SRP_Rounded Numeric(18, 2)
	Declare @SRP_Digit int
	Declare @Digit_DN_FK bigint
	Declare @SRP_Final Numeric(18, 2)
	Declare @GP_Final Numeric(18, 4)
	Declare @CHANGE_TOLERENCE Numeric(18, 4) = 0.05
	Declare @USER_MSG Varchar(128) = ''

	Declare @Item_Change_Type_CR_FK int = Null
	Declare @Alias_ID bigint = Null
	Declare @PRICE_CHANGE_EFFECTIVE_DATE date

	Set @PRICE_CHANGE_EFFECTIVE_DATE = (select price_change_effective_date from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID)
	
	If @PRICE_CHANGE_EFFECTIVE_DATE < Cast(getdate() as date)
	Begin
		Set @USER_MSG = 'Prior weeks'' work cannot be changed.'
		goto eof
	End

	Declare @Cutoff as time
	Declare @Time varchar(20)

	Set @Cutoff = (select value from epacube.epacube_params WHERE EPACUBE_PARAMS_ID = 112000)

	Set @Time = replace(convert(varchar(20), @Cutoff, 22), ':00 ', ' ')

	If (cast(getdate() as time) >= @Cutoff and @Price_Change_Effective_Date = cast(getdate() as date)) or cast(getdate() as date) > @Price_Change_Effective_Date
	Begin
		Select
		(Select	'You have missed today''s cutoff time of ' + @Time + '.<br><br>The values just entered have not been accepted.<br><br>Please mark down any changes you still need to make and enter them in a View or Zone Price Settings with an effective date of tomorrow or later') 'USER_MSG'
		, 1 'reset_values'
		, 5000 'user_msg_timeout_ms'
		goto eof
	End

	insert into precision.user_call_statements
	(create_user, ui_name, procedure_called, procedure_parameters, table_name, table_row_id, prod_segment_fk, entity_segment_fk)
	Select @user 'create_user'
	, 'Zone_Price_Changes' 'ui_name'
	, '[marginmgr].[GetRetailPrice_PPT_RND]' 'procedure_called'
	, isnull(cast(@PRICESHEET_RETAIL_CHANGES_ID as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@GP_New as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@Price_New as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@TM_Adjusted as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@Price_Multiple_New as varchar(24)), 'Null') + ',
	 ''' + isnull(cast(@User as varchar(24)), 'Null') + ''',
	 ' + isnull(cast(@PPT as varchar(24)), 'Null') + ',
	 ' + isnull(cast(@AutoCalcTM as varchar(24)), 'Null') + '' 'procedure_parameters'
	, '[marginmgr].[pricesheet_retail_changes]' 'table_name'
	, @PRICESHEET_RETAIL_CHANGES_ID 'table_row_id', (select top 1 product_structure_fk from [marginmgr].[pricesheet_retail_changes] where pricesheet_retail_changes_id = @PRICESHEET_RETAIL_CHANGES_ID) 'Prod_Structure_fk', (select top 1 ZONE_ENTITY_STRUCTURE_FK from [marginmgr].[pricesheet_retail_changes] where pricesheet_retail_changes_id = @PRICESHEET_RETAIL_CHANGES_ID) 'entity_structure_fk'
	
	If isnull(@GP_New, 0) = 0 and isnull(@Price_New, 0) = 0 
	goto SetNewTM

	If isnull(@GP_New, 0) = 0 and isnull(@Price_New, 0) = 0
	Goto EOF

	DECLARE PricingCursor Cursor local for
	Select unit_cost_cur, unit_cost_new, PRICE_CUR, CUR_GM_PCT, Product_Structure_FK, Zone_Entity_Structure_FK, TARGET_MARGIN, Item_Change_Type_CR_FK, Alias_ID from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID
				
		OPEN PricingCursor;
		FETCH NEXT FROM PricingCursor INTO @Cur_Cost, @New_Cost, @SRP_CUR, @CUR_GM_PCT, @Product_Structure_FK, @Entity_Structure_FK, @Target_Margin, @Item_Change_Type_CR_FK, @Alias_ID
--									WHILE @@FETCH_STATUS = 0
		If @Item_Change_Type_CR_FK in (656, 662)
			Set @Product_Structure_FK = (Select top 1 Product_Structure_FK from epacube.marginmgr.PRICESHEET_RETAIL_CHANGES
										 where 1 = 1
										 and PRICE_CHANGE_EFFECTIVE_DATE = @PRICE_CHANGE_EFFECTIVE_DATE
										 and ZONE_ENTITY_STRUCTURE_FK = @Entity_Structure_FK
										 and Alias_id = @Alias_ID
										 and unit_cost_new = @New_Cost
										 and ITEM_CHANGE_TYPE_CR_FK not in (656, 662))
		Else
			Set @Product_Structure_FK = @Product_Structure_FK

		Begin

			Set @SRP_Initial = isnull(@Price_New, round(Cast((@New_Cost * @Price_Multiple_New) / (1 - @GP_New) as Numeric(18, 6)), 2))

			If @PPT = 0
			Begin
				Set @SRP_FINAL = @SRP_Initial
				goto EndOfCursor
			End

			Set @SRP_Final = [precision].[getretailprices]	(@entity_structure_fk, @product_structure_fk, @SRP_Initial, @price_change_effective_date)

		End

		EndOfCursor:
	Close PricingCursor;
	Deallocate PricingCursor;


	Set @GP_Final = Cast((@SRP_Final - (@New_Cost * @Price_Multiple_New)) / @SRP_Final as Numeric(18, 4))

	Set @USER_MSG = Case	when @SRP_CUR = 0 and @GP_Final > 0.5 then 'HIGH MARGIN WARNING'
							WHEN Abs(@GP_Final - @CUR_GM_PCT) <= @CHANGE_TOLERENCE then '' else Cast(Cast(((@GP_Final - @CUR_GM_PCT) * 100) as Numeric(18, 2)) as varchar(8)) +  '% CHANGE NOT WITHIN ' + Cast(Cast(@CHANGE_TOLERENCE * 100 as Numeric(18, 2)) as varchar(8)) + '% TOLERENCE LEVEL' END

	declare @PRICE_MGMT_TYPE_CR_FK int
	Declare @ALT_PRICESHEET_RETAIL_CHANGES_ID bigint

	set @PRICE_MGMT_TYPE_CR_FK = (select PRICE_MGMT_TYPE_CR_FK from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID)
	set @Alias_ID = (select Alias_ID from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID)
	set @ALT_PRICESHEET_RETAIL_CHANGES_ID = isnull((select PRICESHEET_RETAIL_CHANGES_ID from marginmgr.PRICESHEET_RETAIL_CHANGES
			Where PRICE_CHANGE_EFFECTIVE_DATE = @price_change_effective_date 
				and ZONE_ENTITY_STRUCTURE_FK = @Entity_Structure_FK 
				and price_mgmt_type_cr_fk <> @PRICE_MGMT_TYPE_CR_FK
				and ITEM_CHANGE_TYPE_CR_FK = 656
				and alias_id = @Alias_ID), 0)

	Update PRC
	Set Price_Multiple_New = @Price_Multiple_New
	, SRP_Adjusted = @SRP_Final
	, GM_Adjusted = @GP_Final
	, TM_Adjusted = Case	when TM_Adjusted is not null and isnull(@AutoCalcTM, 0) = 0 then TM_Adjusted
							when isnull(@AutoCalcTM, 0) = 0 then null
							when Round(@GP_Final + 0.000049, 4) = Round(Target_Margin + 0.000049, 4) then Null
							else Cast(@GP_Final as Numeric(18, 4)) end
	, Update_User_Price = @User
	, Update_timestamp = getdate()
	, HOLD_CUR_PRICE = Null
	, Reviewed = 1
	from marginmgr.PRICESHEET_RETAIL_CHANGES PRC where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID

	If @ALT_PRICESHEET_RETAIL_CHANGES_ID <> 0
	Begin
		Update PRC
		Set Price_Multiple_New = @Price_Multiple_New
		, SRP_Adjusted = @SRP_Final
		, GM_Adjusted = Cast((@SRP_Final - (isnull(prc.unit_cost_new, prc.unit_cost_cur) * @Price_Multiple_New)) / @SRP_Final as Numeric(18, 4))
		, TM_Adjusted = case	when isnull(@AutoCalcTM, 0) = 0 then null
								else (select TM_Adjusted from marginmgr.pricesheet_retail_changes where pricesheet_retail_changes_id = @PRICESHEET_RETAIL_CHANGES_ID) end
		, Update_User_Price = @User
		, Update_timestamp = getdate()
		, HOLD_CUR_PRICE = Null
		, Reviewed = 1
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC where PRICESHEET_RETAIL_CHANGES_ID = @ALT_PRICESHEET_RETAIL_CHANGES_ID
	End

Goto EOF

SetNewTM:

	set @PRICE_MGMT_TYPE_CR_FK = (select PRICE_MGMT_TYPE_CR_FK from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID)
	set @Alias_ID = (select Alias_ID from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID)
	set @entity_structure_fk = (select ZONE_ENTITY_STRUCTURE_FK from marginmgr.PRICESHEET_RETAIL_CHANGES where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID)
	set @ALT_PRICESHEET_RETAIL_CHANGES_ID = isnull((select PRICESHEET_RETAIL_CHANGES_ID from marginmgr.PRICESHEET_RETAIL_CHANGES
			Where PRICE_CHANGE_EFFECTIVE_DATE = @price_change_effective_date 
				and ZONE_ENTITY_STRUCTURE_FK = @Entity_Structure_FK 
				and price_mgmt_type_cr_fk <> @PRICE_MGMT_TYPE_CR_FK
				and ITEM_CHANGE_TYPE_CR_FK = 656
				and alias_id = @Alias_ID), 0)

If @TM_Adjusted = 0 or (@TM_Adjusted = 0 and isnull(@AutoCalcTM, 0) = 0)
	Update PRC
	Set TM_Adjusted = Null
	from marginmgr.PRICESHEET_RETAIL_CHANGES PRC 
	where PRICESHEET_RETAIL_CHANGES_ID in (@PRICESHEET_RETAIL_CHANGES_ID, @ALT_PRICESHEET_RETAIL_CHANGES_ID)
	and @TM_Adjusted = 0
Else
	Begin
		Update PRC
		Set TM_Adjusted = @TM_Adjusted
		, UPDATE_USER_SET_TM = @User
		, Reviewed = 1
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC 
		where PRICESHEET_RETAIL_CHANGES_ID = @PRICESHEET_RETAIL_CHANGES_ID
		and target_margin <> @TM_Adjusted
		and @TM_Adjusted > 0
		
		If @ALT_PRICESHEET_RETAIL_CHANGES_ID <> 0
		Begin
			Update PRC
			Set TM_Adjusted = @TM_Adjusted
			, UPDATE_USER_SET_TM = @User
			, Reviewed = 1
			from marginmgr.PRICESHEET_RETAIL_CHANGES PRC 
			where PRICESHEET_RETAIL_CHANGES_ID = @ALT_PRICESHEET_RETAIL_CHANGES_ID
			and target_margin <> @TM_Adjusted
			and @TM_Adjusted > 0
		End
	End

eof:

	Select PRICESHEET_RETAIL_CHANGES_ID
	, SRP_ADJUSTED 
	, GM_ADJUSTED
	, TM_ADJUSTED
	, Case when PRICE_MULTIPLE_NEW = 1 then Null else PRICE_MULTIPLE_NEW end 'PRICE_MULTIPLE_NEW'
	, HOLD_CUR_PRICE
	, REVIEWED
	, @USER_MSG 'USER_MSG'
	from marginmgr.PRICESHEET_RETAIL_CHANGES PRC 
	where PRICESHEET_RETAIL_CHANGES_ID in (@PRICESHEET_RETAIL_CHANGES_ID, @ALT_PRICESHEET_RETAIL_CHANGES_ID)

	Insert into marginmgr.PRICESHEET_RETAIL_CHANGES_USER_TRACE
	(
	[PROCEDURE_NAME]
	, DATA_STREAM
	, PRICESHEET_RETAIL_CHANGES_FK
	, CREATE_USER

	, SRP_ADJUSTED_OUT
	, GM_ADJUSTED_OUT
	, TM_ADJUSTED_OUT
	, PRICE_MULTIPLE_NEW_OUT
	, HOLD_CUR_PRICE_OUT
	, REVIEWED_OUT
	, USER_MSG_OUT
	)
	Select
		'marginmgr.GetRetailPrice_PPT_RND' 'Procedure_Name'
		, 'Out' 'Data_Stream'
		, PRICESHEET_RETAIL_CHANGES_ID
		, @User
		, SRP_ADJUSTED 
		, GM_ADJUSTED
		, TM_ADJUSTED
		, PRICE_MULTIPLE_NEW
		, HOLD_CUR_PRICE
		, REVIEWED
		, @USER_MSG 'USER_MSG'
		from marginmgr.PRICESHEET_RETAIL_CHANGES PRC 
		where PRICESHEET_RETAIL_CHANGES_ID in (@PRICESHEET_RETAIL_CHANGES_ID, @ALT_PRICESHEET_RETAIL_CHANGES_ID)

--select @PRICESHEET_RETAIL_CHANGES_ID '@PRICESHEET_RETAIL_CHANGES_ID', @ALT_PRICESHEET_RETAIL_CHANGES_ID '@ALT_PRICESHEET_RETAIL_CHANGES_ID', @price_change_effective_date '@price_change_effective_date'
--, @Entity_Structure_FK '@Entity_Structure_FK', @PRICE_MGMT_TYPE_CR_FK '@PRICE_MGMT_TYPE_CR_FK', @Alias_ID '@Alias_ID'
