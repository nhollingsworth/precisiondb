﻿
-- ----=============================================
-- ----Author:		<Author,,Name>
-- ----Create date: <Create Date,,>
-- ----Description:	<Description,,>
-- ----=============================================
CREATE PROCEDURE [marginmgr].[RETAIL_PRICE_MAINTENANCE_ZONE_SETTINGS_MZ]
 
	@Result_Type varchar(64) = 'ZONE'
	, @ZONE as varchar(256) = NULL
	, @CalcDate as date = NULL
	, @Parent_Prod_Segment_FK bigint = Null
	, @User Varchar(96) = Null

AS

	SET NOCOUNT ON;

	--Declare @Result_Type varchar(64) = 'zic'
	--Declare @CalcDate as date = '2019-5-30'
	--Declare @ZONE as varchar(64) = '40001,30600,80001'
	--Declare @Parent_Prod_Segment_FK bigint = 1014719558 --1014719560 --1000055847
	--Declare @User Varchar(96) = 'gstone@epacube.com'

	--select * from epacube.data_value where data_name_fk = 501045 and value = '10001'

		Declare @ZES_FK as bigint
		Declare @ZoneCount int

		If Object_ID('tempdb..#Zones') is not null
		drop table #Zones

		Create table #Zones(zone_entity_Structure_FK bigint not null, Zone Varchar(64) Not Null, Zone_Name Varchar(64) Null)

		Insert into #Zones
		Select ei.entity_structure_fk 'Zone_Entity_Structure_FK', ei.value 'Zone', ein.value 'Zone_Name' 
		from epacube.entity_identification ei with (nolock) --where ENTITY_DATA_NAME_FK = 151000
		inner join epacube.epacube.entity_ident_nonunique ein with (nolock) on ei.entity_structure_fk = ein.entity_structure_fk and ein.data_name_fk = 151112
		inner join utilities.SplitString(@ZONE, ',') Zns on ei.value = ZNs.listitem and ei.entity_data_name_fk = 151000 and ei.data_name_fk = 151110

		create index idx_01 on #zones(zone_entity_structure_fk)

		Set @ZoneCount = (select count(*) from #Zones)


	If @Result_Type = 'ZONE'
	Begin
	Select zone_segment_fk
	, ZONE
	, ZONE_NAME
	, @CalcDate 'PRICING_DATE'
	, PRENOTIFY_DAYS, [REQUIRED_PRICE_CHANGE_AMOUNT], [URM_LVL_GRS_ACTIVATION_FLAG], [USE_LVL_GRS_PRICE_POINTS_FLAG]
	, [PRC_ADJ_DIG_0], [PRC_ADJ_DIG_1], [PRC_ADJ_DIG_2], [PRC_ADJ_DIG_3], [PRC_ADJ_DIG_4], [PRC_ADJ_DIG_5], [PRC_ADJ_DIG_6], [PRC_ADJ_DIG_7], [PRC_ADJ_DIG_8], [PRC_ADJ_DIG_9]
				FROM(
					Select --ss.effective_date, 
					SS.zone_segment_fk
					, eiz.value 'ZONE'
					, eizn.value 'ZONE_NAME'
					, Attribute_Event_Data  'Adjustment'
					, dn.name 'dataname'
					, dense_rank()over(partition by ss.zone_segment_fk, ss.data_name_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank_Eff
					from epacube.epacube.segments_settings ss with (nolock)
					inner join epacube.epacube.entity_identification eiz with (nolock) on ss.ZONE_ENTITY_STRUCTURE_FK = eiz.entity_structure_fk and eiz.ENTITY_DATA_NAME_FK = 151000
					inner join epacube.epacube.entity_ident_nonunique eizn with (nolock) on eiz.ENTITY_STRUCTURE_FK = eizn.ENTITY_STRUCTURE_FK and eiz.ENTITY_DATA_NAME_FK = eizn.ENTITY_DATA_NAME_FK and eizn.data_name_fk = 151112
					inner join epacube.data_name dn with (nolock) on ss.data_name_fk = dn.data_name_id
					left join epacube.epacube.data_value dv with (nolock) on ss.data_value_fk = dv.data_value_id
					where 1 = 1
					and ss.zone_entity_structure_fk is not null
					and ss.cust_entity_structure_fk is null
					and ss.prod_segment_fk is null
					and (
							(dn.parent_data_name_fk = 144893
								and ss.entity_data_name_fk = 151000)
							Or 
								ss.data_name_fk in (144868, 144908, 144876, 144862)
						)
					and ss.effective_date <= @CalcDate
					and ss.zone_segment_fk = @ZES_FK
				) P 
				PIVOT (Max(p.Adjustment) for DataName in
				 (PRENOTIFY_DAYS, [REQUIRED_PRICE_CHANGE_AMOUNT], URM_LVL_GRS_ACTIVATION_FLAG, [USE_LVL_GRS_PRICE_POINTS_FLAG], [PRC_ADJ_DIG_0], [PRC_ADJ_DIG_1], [PRC_ADJ_DIG_2], [PRC_ADJ_DIG_3], [PRC_ADJ_DIG_4], [PRC_ADJ_DIG_5], [PRC_ADJ_DIG_6]
				 , [PRC_ADJ_DIG_7], [PRC_ADJ_DIG_8], [PRC_ADJ_DIG_9]) ) As pivotTable 
				 where DRank_eff = 1
				 order by zone_segment_fk
	End
	else
		If @Result_Type = 'ZIC' and @Parent_Prod_Segment_FK is null and @ZoneCount > 1
		Begin
			Select ics.Prod_Segment_FK3 'prod_segment_fk_dept'
			, ics.data_name_fk3 'prod_segment_data_name_dept'
			,ics.Prod_Segment_FK4 'prod_segment_fk_group'
			, ics.data_name_fk4 'prod_segment_data_name_group'
			,ics.Prod_Segment_FK5 'prod_segment_fk_subgroup'
			, ics.data_name_fk5 'prod_segment_data_name_subgroup'
			, Cast(IC_ID3 as varchar(16)) + ': ' + Upper(DV3_Desc) 'dept', Cast(IC_ID4 as varchar(16)) + ': ' + Upper(DV4_Desc) 'group', Cast(IC_ID5 as varchar(16)) + ': ' + Upper(DV5_Desc) 'subgroup'
			, ics.Zone_Segment_FK, z.[zone], z.zone_name
			, dept.Target_Margin_Dept 'tm_pct_dept', [Group].Target_Margin_Group 'tm_pct_group', Target_Margin_Subgroup 'tm_pct_subgroup'
			, cast(null as bigint) 'alias_id'
			from epacube.[precision].item_class_structure ICS with (nolock)
			inner join #zones z on ics.ZONE_SEGMENT_FK = z.zone_entity_structure_fk
				left join
				(
				Select zone_segment_fk, prod_segment_fk, [Zone]
				, DISPLAY_SEQ 'item_level', value 'class_id', isnull([description], 'All Items') 'item_class', data_name_fk, TARGET_MARGIN_DEPT
				FROM(
					Select * from (
					Select
					SS.zone_segment_fk, eiz.value 'Zone', dv.DISPLAY_SEQ, dv.value, dv.[description]
					, ss.ATTRIBUTE_NUMBER 'TARGET_MARGIN_DEPT'
					, dn.[name] 'dataname'
					, ss.PROD_SEGMENT_FK, dv.data_name_fk
					, dense_rank()over(partition by ss.zone_segment_fk, ss.data_name_fk, ss.prod_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank_val
					from epacube.epacube.segments_settings ss with (nolock)
					inner join #zones z on ss.ZONE_SEGMENT_FK = z.zone_entity_structure_fk
					inner join epacube.entity_identification eiz with (nolock) on z.zone_entity_structure_fk = eiz.ENTITY_STRUCTURE_FK and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.DATA_NAME_FK = 151110
					inner join epacube.data_name dn with (nolock) on ss.data_name_fk = dn.data_name_id
					inner join epacube.epacube.data_value dv with (nolock) on ss.prod_segment_fk = dv.data_value_id and ss.PROD_SEGMENT_DATA_NAME_FK = dv.DATA_NAME_FK
					where 1 = 1
					and cast(ss.Effective_Date as date) <= @CalcDate
					and ss.cust_entity_structure_fk is null
					and ss.ATTRIBUTE_EVENT_DATA is not null
					and ss.data_name_fk in (502046)
					and ss.PROD_SEGMENT_DATA_NAME_FK = 501043
					and ss.RECORD_STATUS_CR_FK = 1
					) a where  a.DRank_val = 1
				) D
				) Dept on isnull(ics.prod_segment_fk3, 0) = isnull(dept.prod_segment_fk, 0) and ics.DATA_NAME_FK3 = dept.data_name_fk and ics.zone_segment_fk = Dept.ZONE_SEGMENT_FK
				left join
				(
				Select zone_segment_fk, prod_segment_fk, [Zone]
				, DISPLAY_SEQ 'item_level', value 'class_id', isnull([description], 'All Items') 'item_class', data_name_fk, TARGET_MARGIN_GROUP
				FROM(
					Select * from (
					Select
					SS.zone_segment_fk, eiz.value 'Zone', dv.DISPLAY_SEQ, dv.value, dv.[description]
					, ss.ATTRIBUTE_NUMBER 'TARGET_MARGIN_GROUP'
					, dn.[name] 'dataname'
					, ss.PROD_SEGMENT_FK, dv.data_name_fk
					, dense_rank()over(partition by ss.zone_segment_fk, ss.data_name_fk, ss.prod_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank_val
					from epacube.epacube.segments_settings ss with (nolock)
					inner join #zones z on ss.ZONE_SEGMENT_FK = z.zone_entity_structure_fk
					inner join epacube.entity_identification eiz with (nolock) on z.zone_entity_structure_fk = eiz.ENTITY_STRUCTURE_FK and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.DATA_NAME_FK = 151110
					inner join epacube.data_name dn with (nolock) on ss.data_name_fk = dn.data_name_id
					inner join epacube.epacube.data_value dv with (nolock) on ss.prod_segment_fk = dv.data_value_id and ss.PROD_SEGMENT_DATA_NAME_FK = dv.DATA_NAME_FK
					where 1 = 1
					and cast(ss.Effective_Date as date) <= @CalcDate
					and ss.cust_entity_structure_fk is null
					and ss.ATTRIBUTE_EVENT_DATA is not null
					and ss.data_name_fk in (502046)
					and ss.PROD_SEGMENT_DATA_NAME_FK = 501044
					and ss.RECORD_STATUS_CR_FK = 1
					) a where  a.DRank_val = 1
				) G
				) [GROUP] on isnull(ics.prod_segment_fk4, 0) = isnull([GROUP].prod_segment_fk, 0) and ics.DATA_NAME_FK4 = [Group].data_name_fk and ics.zone_segment_fk = [Group].ZONE_SEGMENT_FK
				left join
				(
				Select zone_segment_fk, prod_segment_fk, [Zone]
				, DISPLAY_SEQ 'item_level', value 'class_id', isnull([description], 'All Items') 'item_class', data_name_fk, TARGET_MARGIN_SUBGROUP
				FROM(
					Select * from (
					Select
					SS.zone_segment_fk, eiz.value 'Zone', dv.DISPLAY_SEQ, dv.value, dv.[description]
					, ss.ATTRIBUTE_NUMBER 'TARGET_MARGIN_SUBGROUP'
					, dn.[name] 'dataname'
					, ss.PROD_SEGMENT_FK, dv.data_name_fk
					, dense_rank()over(partition by ss.zone_segment_fk, ss.data_name_fk, ss.prod_segment_fk order by ss.effective_date desc, ss.segments_settings_id desc) DRank_val
					from epacube.epacube.segments_settings ss with (nolock)
					inner join #zones z on ss.ZONE_SEGMENT_FK = z.zone_entity_structure_fk
					inner join epacube.entity_identification eiz with (nolock) on z.zone_entity_structure_fk = eiz.ENTITY_STRUCTURE_FK and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.DATA_NAME_FK = 151110
					inner join epacube.data_name dn with (nolock) on ss.data_name_fk = dn.data_name_id
					inner join epacube.epacube.data_value dv with (nolock) on ss.prod_segment_fk = dv.data_value_id and ss.PROD_SEGMENT_DATA_NAME_FK = dv.DATA_NAME_FK
					where 1 = 1
					and cast(ss.Effective_Date as date) <= @CalcDate
					and ss.cust_entity_structure_fk is null
					and ss.ATTRIBUTE_EVENT_DATA is not null
					and ss.data_name_fk in (502046)
					and ss.PROD_SEGMENT_DATA_NAME_FK = 501045
					and ss.RECORD_STATUS_CR_FK = 1
					) a where  a.DRank_val = 1
				) SG
				) [SUBGROUP] on isnull(ics.prod_segment_fk5, 0) = isnull([SUBGROUP].prod_segment_fk, 0) and ics.DATA_NAME_FK5 = [SUBGROUP].data_name_fk and ics.zone_segment_fk = [SUBGROUP].ZONE_SEGMENT_FK
				order by cast(ics.ic_id3 as bigint), cast(ics.ic_id4 as bigint), cast(ics.ic_id5 as bigint)
				, left(ics.zone_name, 3) desc, Case isnumeric(reverse(ltrim(rtrim(left(Reverse(ics.zone_name), Charindex(' ',Reverse(ics.zone_name))))))) when 1 then cast(reverse(ltrim(rtrim(left(Reverse(ics.zone_name), Charindex(' ',Reverse(ics.zone_name)))))) as int) else Null end desc, Rank()over(order by ics.zone_name) + 10000 desc

		end
		else

		If @Result_Type = 'ZIC' and @Parent_Prod_Segment_FK is not null and @ZoneCount > 1
		Begin
			Select ics.Prod_Segment_FK3 'prod_segment_fk_dept'
			, ics.data_name_fk3 'prod_segment_data_name_dept'
			, ics.Prod_Segment_FK4 'prod_segment_fk_group'
			, ics.data_name_fk4 'prod_segment_data_name_group'
			, ics.Prod_Segment_FK5 'prod_segment_fk_subgroup'
			, ics.data_name_fk5 'prod_segment_data_name_subgroup'
			, pi.PRODUCT_STRUCTURE_FK 'prod_segment_fk_item'
			, 110103 'prod_segment_data_name_fk'
			, Cast(IC_ID3 as varchar(16)) + ': ' + Upper(DV3_Desc) 'dept', Cast(IC_ID4 as varchar(16)) + ': ' + Upper(DV4_Desc) 'group', Cast(IC_ID5 as varchar(16)) + ': ' + Upper(DV5_Desc) 'subgroup'
			, pi.value + ': ' + isnull(pd.[description], '') + 
				case when exists (Select top 1 1 from epacube.product_mult_type pmt inner join #zones z on pmt.entity_structure_fk = z.zone_entity_structure_fk and pmt.DATA_NAME_FK = 500019
				and product_structure_fk = pi.PRODUCT_STRUCTURE_FK) then ' ***' else '' end  'Item'
			--, pi.value + ': ' + isnull(pd.[description], '') 'Item'
			, ics.Zone_Segment_FK, ics.[zone], ics.zone_name
			, (select top 1 attribute_number from epacube.epacube.segments_settings where zone_segment_fk = ics.zone_segment_fk and data_name_fk = 502046 and prod_segment_fk = ics.Prod_Segment_FK3 and prod_segment_data_name_fk = 501043 and Effective_Date <= @CalcDate order by Effective_Date desc) 'tm_pct_dept'
			, (select top 1 attribute_number from epacube.epacube.segments_settings where zone_segment_fk = ics.zone_segment_fk and data_name_fk = 502046 and prod_segment_fk = ics.Prod_Segment_FK4 and prod_segment_data_name_fk = 501044 and Effective_Date <= @CalcDate order by Effective_Date desc) 'tm_pct_group'
			, (select top 1 attribute_number from epacube.epacube.segments_settings where zone_segment_fk = ics.zone_segment_fk and data_name_fk = 502046 and prod_segment_fk = ics.Prod_Segment_FK5 and prod_segment_data_name_fk = 501045 and Effective_Date <= @CalcDate order by Effective_Date desc) 'tm_pct_subgroup'
			, Item.TARGET_MARGIN_ITEM 'tm_pct_item'
			--, case when pa.product_association_id is null or pa.record_status_cr_fk = 2 then -1 else Item.TARGET_MARGIN_ITEM end 'tm_pct_item'
			, dv_500019.[value] 'alias_id'
			from epacube.[precision].item_class_structure ICS with (nolock) 
			inner join #zones z with (nolock) on ics.Zone_Segment_FK = z.zone_entity_Structure_FK
			inner join epacube.epacube.segments_product sp with (nolock) on ics.prod_segment_fk5 = sp.PROD_SEGMENT_FK and ics.data_name_fk5 = sp.data_name_fk
			left join epacube.product_association pa with (nolock) on sp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and pa.data_name_fk = 159450 and z.zone_entity_Structure_FK = pa.entity_structure_fk
			left join epacube.product_mult_type pmt with (nolock) on sp.product_structure_fk = pmt.product_structure_fk and pmt.data_name_fk = 500019 and z.zone_entity_structure_fk = pmt.ENTITY_STRUCTURE_FK
			left join epacube.data_value dv_500019 on pmt.data_name_fk = dv_500019.data_name_fk and pmt.DATA_VALUE_FK = dv_500019.DATA_VALUE_ID
			left join epacube.product_identification pi with (nolock) on sp.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110100
			left join epacube.product_description pd with (nolock) on sp.product_structure_fk = pd.PRODUCT_STRUCTURE_FK and pd.DATA_NAME_FK = 110401
			left join
				(
				Select zone_segment_fk, prod_segment_fk, PRODUCT_STRUCTURE_FK
				,  TARGET_MARGIN_ITEM
				FROM(
					Select * from (
					Select
					SS.zone_segment_fk
					, ss.ATTRIBUTE_NUMBER 'TARGET_MARGIN_ITEM'
					, sp.PROD_SEGMENT_FK
					, sp.PRODUCT_STRUCTURE_FK
					, dense_rank()over(partition by ss.zone_segment_fk, ss.data_name_fk, ss.prod_segment_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank_val
					from epacube.epacube.segments_settings ss with (nolock)
					inner join epacube.epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.product_structure_fk and ss.prod_segment_data_name_fk = 110103 and sp.data_name_fk = 501045
					inner join #zones z on ss.ZONE_SEGMENT_FK = z.zone_entity_structure_fk
					inner join epacube.product_association pa with (nolock) on sp.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and pa.data_name_fk = 159450 and z.zone_entity_Structure_FK = pa.entity_structure_fk
					where 1 = 1
					and cast(ss.Effective_Date as date) <= @CalcDate
					and ss.cust_entity_structure_fk is null
					and ss.ATTRIBUTE_EVENT_DATA is not null
					and ss.data_name_fk in (502046)
					and ss.RECORD_STATUS_CR_FK = 1
					) a where  a.DRank_val = 1
				) I
				) [Item] on isnull(ics.prod_segment_fk5, 0) = isnull(Item.prod_segment_fk, 0) and ics.zone_segment_fk = [Item].ZONE_SEGMENT_FK and sp.PRODUCT_STRUCTURE_FK = [item].PRODUCT_STRUCTURE_FK
			where ics.prod_segment_fk5 = @Parent_Prod_Segment_FK
			order by isnull(dv_500019.[value], '0'), pi.value
			--order by pi.value desc
			--left(ics.zone_name, 3) desc, Case isnumeric(reverse(ltrim(rtrim(left(Reverse(ics.zone_name), Charindex(' ',Reverse(ics.zone_name))))))) when 1 then cast(reverse(ltrim(rtrim(left(Reverse(ics.zone_name), Charindex(' ',Reverse(ics.zone_name)))))) as int) else Null end desc, Rank()over(order by ics.zone_name) + 10000 desc
			--, dv_500019.[value]
		end
