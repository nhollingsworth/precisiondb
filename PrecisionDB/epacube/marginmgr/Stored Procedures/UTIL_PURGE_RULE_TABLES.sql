﻿





-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To remove rows from RULES TABLES VIA RULE_ID SPECIFIED 
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        09/10/2011   Initial SQL Version
--

CREATE PROCEDURE [marginmgr].[UTIL_PURGE_RULE_TABLES] 
        @IN_RULES_FK BIGINT, @IN_PURGE_STATUS INT
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int

BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of marginmgr.UTIL_PURGE_RULE_TABLES.'
					 + cast(isnull(@IN_RULES_FK, 0) as varchar(30))

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;

SET @l_sysdate = GETDATE()


-- table variable to store rules we will purge; 
DECLARE @TS_PURGE_RULES TABLE
(RULES_FK bigint)


-- populate #TS_PURGE_RULES table with all either specified RULE 
--    or any RULE with Status PURGE if Indicator 1
INSERT INTO @TS_PURGE_RULES
			(RULES_FK)
(
SELECT R.RULES_ID
FROM synchronizer.RULES R
WHERE R.RULES_ID = @IN_RULES_FK
UNION ALL
SELECT R.RULES_ID
FROM synchronizer.RULES R
WHERE R.RECORD_STATUS_CR_FK = 0
AND   @IN_PURGE_STATUS = 1
)

/*  FOR TESTING

INSERT INTO [common].[T_SQL]
           ([TS]
           ,[SQL_TEXT])
SELECT
            @l_sysdate
           ,TSPR.RULES_FK
FROM @TS_PURGE_RULES TSPR

*/


DELETE 
FROM synchronizer.RULES_ACTION_OPERANDS
WHERE RULES_ACTION_FK IN (
SELECT RULES_ACTION_ID
FROM synchronizer.RULES_ACTION
WHERE RULES_FK IN ( SELECT RULES_FK FROM @TS_PURGE_RULES )
)


DELETE 
FROM synchronizer.RULES_ACTION_STRFUNCTION
WHERE RULES_ACTION_FK IN (
SELECT RULES_ACTION_ID
FROM synchronizer.RULES_ACTION
WHERE RULES_FK IN ( SELECT RULES_FK FROM @TS_PURGE_RULES )
)

DELETE 
FROM synchronizer.RULES_ACTION
WHERE RULES_FK IN ( SELECT RULES_FK FROM @TS_PURGE_RULES )

DELETE 
FROM synchronizer.RULES_FILTER_SET
WHERE RULES_FK IN ( SELECT RULES_FK FROM @TS_PURGE_RULES )

DELETE
FROM synchronizer.RULES
WHERE RULES_ID IN ( SELECT RULES_FK FROM @TS_PURGE_RULES )



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of marginmgr.UTIL_PURGE_RULE_TABLES'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of marginmgr.UTIL_PURGE_RULE_TABLES has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
