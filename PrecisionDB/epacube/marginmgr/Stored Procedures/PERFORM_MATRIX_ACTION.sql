﻿--A_epaMAUI_PERFORM_MATRIX_ACTION


-- Copyright 2008
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To determine all qualified rules for Event_Data records in Job
--				Validation and Calculations Rules assigned to Events
--				Derivation Rules will be created with NEW DATA = @
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        11/30/2009   Initial SQL Version
-- KS        04/27/2009   Jasco Enhancement for Royalty and Duty
-- KS        07/24/2012   Added the capability to allow Negative Values for Eclipse Best
-- CV        04/09/2013   Fixed where No Calc rules where not setting Result Rank 
--							Jira 1166
--
--GHS        09/22/2013  Replace "IN" clause with inner join for potential performance improvement

CREATE PROCEDURE [marginmgr].[PERFORM_MATRIX_ACTION] 
   ( @in_analysis_job_fk BIGINT,  @in_result_data_name_fk  INT, @in_calc_group_seq INT )

AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate DATETIME

DECLARE  @v_best_tie_schedule bigint


DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of marginmgr.PERFORM_MATRIX_ACTION.' 
               + ' JOB:: ' + CAST ( ISNULL (@in_analysis_job_fk, 0 ) AS VARCHAR(16) )
               + ' RESULT:: ' + CAST ( ISNULL (@IN_RESULT_DATA_NAME_FK, 0 ) AS VARCHAR(16) )
               + ' CALC SEQ:: ' + CAST ( ISNULL (@IN_CALC_GROUP_SEQ, 0 ) AS VARCHAR(16) )               

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;


------------------------------------------------------------------------------------------
--   Reinitial Results and Ranks
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_CALC
SET RESULT = NULL
WHERE 1 = 1
AND   JOB_FK = @in_analysis_job_fk
AND   RESULT_DATA_NAME_FK = @in_result_data_name_fk
AND   CALC_GROUP_SEQ = @in_calc_group_seq

--GHS Removed "IN" clause and reaplaced with inner join below

UPDATE ECR
SET RESULT_RANK = NULL
   ,SCHEDULE_RANK = NULL
   ,STRUCTURE_RANK = NULL
   ,STRUCTURE_GRP_RANK = NULL
from synchronizer.EVENT_CALC_RULES ECR
inner join synchronizer.event_calc EC on ECR.EVENT_FK = ec.EVENT_ID
WHERE EC.JOB_FK = @in_analysis_job_fk
			AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk
			AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq




------------------------------------------------------------------------------------------
--   Temporary Table for all Qualified Filters for the Event Data
------------------------------------------------------------------------------------------

--drop temp tables
IF object_id('tempdb..#TS_MATRIX_RANK') is not null
   drop table #TS_MATRIX_RANK

CREATE TABLE #TS_MATRIX_RANK(
	[TS_MATRIX_RANK_ID] [bigint] IDENTITY(1000,1) NOT NULL,
    [RESULT_RANK] [smallint] NULL,		
    [STRUCTURE_RANK] [smallint] NULL,		
    [SCHEDULE_RANK] [smallint] NULL,
	[EVENT_RULES_FK] [bigint] NULL,
	[EVENT_FK] [bigint] NULL,	
	[RULES_FK] [bigint] NULL,	
    [RESULT] [numeric] (18,6) NULL,
    [RULES_EFFECTIVE_DATE] [datetime] NULL,	
    [RULE_PRECEDENCE] INT NULL,	    
    [RULE_EVENT_PRECEDENCE] INT NULL,
    [CONTRACT_PRECEDENCE] INT NULL,	
    [SCHEDULE_PRECEDENCE_TIED]  INT NULL,	
    [SCHED_PRECEDENCE] INT NULL,	
    [STRUC_PRECEDENCE] INT NULL,		
    [RESULT_MTX_ACTION] [int] NULL,			
    [STRUC_MTX_ACTION] [int] NULL,		
    [SCHED_MTX_ACTION] [int] NULL,	
    [RULES_SCHEDULE_FK] [bigint] NULL,	            
    [RULES_STRUCTURE_FK] [bigint] NULL,	            
    [RULES_HIERARCHY_FK] [bigint] NULL
)

---------------------------------------------------
-- create all indexes for  #TS_MATRIX_RANK
---------------------------------------------------

CREATE NONCLUSTERED INDEX [IDX_TSMR_ERF_ITSMRI]
ON #TS_MATRIX_RANK
(
	[EVENT_RULES_FK] ASC
)
INCLUDE ( [TS_MATRIX_RANK_ID])


CREATE NONCLUSTERED INDEX [IDX_TSMR_SCHR_IERF_ITSMRI]
ON #TS_MATRIX_RANK
(
	[SCHEDULE_RANK] ASC
)
INCLUDE ( [EVENT_RULES_FK], [TS_MATRIX_RANK_ID] )

CREATE NONCLUSTERED INDEX [IDX_TSMR_STRR_IERF_ITSMRI]
ON #TS_MATRIX_RANK
(
	[STRUCTURE_RANK] ASC
)
INCLUDE ( [EVENT_RULES_FK], [TS_MATRIX_RANK_ID] )

CREATE NONCLUSTERED INDEX [IDX_TSMR_RESULT_IERF_ITSMRI]
ON #TS_MATRIX_RANK
(
	[RESULT_RANK] ASC
)
INCLUDE ( [EVENT_RULES_FK], [TS_MATRIX_RANK_ID] )



CREATE CLUSTERED INDEX [IDX_TSMR_ITSMRI] 
ON #TS_MATRIX_RANK (
	[TS_MATRIX_RANK_ID] ASC
)



------------------------------------------------------------------------------------------
--   CONTRACT AND RULE PRECEDENCE IN THAT ORDER
------------------------------------------------------------------------------------------

		INSERT INTO #TS_MATRIX_RANK (	        
  			 [EVENT_RULES_FK]  
            ,[EVENT_FK] 
			,[RULES_FK] 
            ,[RESULT] 
			,[RULES_EFFECTIVE_DATE]
			,[RULE_PRECEDENCE]
			,[CONTRACT_PRECEDENCE]
			,[SCHED_PRECEDENCE] 
			,[STRUC_PRECEDENCE] 
			,[RESULT_MTX_ACTION]
			,[STRUC_MTX_ACTION] 
			,[SCHED_MTX_ACTION]
			,[RULES_SCHEDULE_FK] 
			,[RULES_STRUCTURE_FK]
			,[RULES_HIERARCHY_FK]
			,[SCHEDULE_RANK]
		)
		SELECT			
			ECR.EVENT_RULES_ID,
			ECR.EVENT_FK,
            ECR.RULES_FK,
            ECR.RESULT,
            ECR.RULES_EFFECTIVE_DATE,
            ECR.RULE_PRECEDENCE,
            ECR.CONTRACT_PRECEDENCE,
            ECR.SCHED_PRECEDENCE,
            ECR.STRUC_PRECEDENCE,
            ECR.RESULT_MTX_ACTION_CR_FK,
            ECR.STRUC_MTX_ACTION_CR_FK,     
			ECR.SCHED_MTX_ACTION_CR_FK,	
            ECR.RULES_SCHEDULE_FK,
            ECR.RULES_STRUCTURE_FK,
            RS.RULES_HIERARCHY_FK,
			CASE ISNULL ( RSCHED.MATRIX_ACTION_CR_FK, -999 )
			WHEN 750 THEN ( DENSE_RANK() OVER ( PARTITION BY ECR.EVENT_FK, R.RULES_SCHEDULE_FK
							ORDER BY ECR.CONTRACT_PRECEDENCE ASC,
							         ECR.RULE_PRECEDENCE ASC, 
							         ECR.PROD_FILTER_HIERARCHY ASC,
							         ECR.PROD_FILTER_ORG_PRECEDENCE ASC,							   
							         ECR.PROD_RESULT_TYPE_CR_FK DESC,
							         ---- FUTURE ADD RESULT TYPE CR FKS
									 ECR.RULES_EFFECTIVE_DATE DESC, ECR.RULES_FK DESC ) )
			WHEN 751 THEN (	DENSE_RANK() OVER ( PARTITION BY ECR.EVENT_FK, R.RULES_SCHEDULE_FK
							ORDER BY ECR.RESULT DESC, ECR.RULES_EFFECTIVE_DATE DESC, ECR.RULES_FK DESC ) )
			WHEN 752 THEN (	DENSE_RANK() OVER ( PARTITION BY ECR.EVENT_FK, R.RULES_SCHEDULE_FK
							ORDER BY ECR.RESULT ASC,  ECR.RULES_EFFECTIVE_DATE DESC, ECR.RULES_FK DESC ) )     
			WHEN 753 THEN 1 --- SET ALL TO ONE                   							              
			ELSE 999              
			END AS SCHEDULE_RANK
		FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
		INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK) 
		  ON ( ECR.EVENT_FK = EC.EVENT_ID )
		INNER JOIN SYNCHRONIZER.RULES R WITH (NOLOCK)
		  ON ( R.RULES_ID = ECR.RULES_FK )
		INNER JOIN SYNCHRONIZER.RULES_SCHEDULE RSCHED WITH (NOLOCK)
		  ON ( RSCHED.RULES_SCHEDULE_ID = R.RULES_SCHEDULE_FK )
		INNER JOIN SYNCHRONIZER.RULES_STRUCTURE RS WITH (NOLOCK)
		  ON ( RS.RULES_STRUCTURE_ID = R.RULES_STRUCTURE_FK )
		WHERE 1 = 1
		AND   EC.JOB_FK = @in_analysis_job_fk
		AND   EC.RESULT_DATA_NAME_FK = @in_result_data_name_fk 
		AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq
--		AND   ECR.RESULT IS NOT NULL  ------ CV JIRA 1166
		AND   ISNULL ( ECR.DISQUALIFIED_IND, 0 ) <> 1
        AND   (   ECR.RESULT IS NOT NULL
               OR ECR.RULES_ACTION_OPERATION1_FK = 1000 )



------------------------------------------------------------------------------------------------
---------  LOOP THROUGH ONLY THE SCHEDULE RANKS = 1 AND THEN PERFORM STRUCTURE RANKS ON THESE
------------------------------------------------------------------------------------------------

      DECLARE   @vm$rules_structure_fk      INT
               ,@vm$struc_mtx_action        INT


      DECLARE  cur_v_struct_group cursor local for
				SELECT DISTINCT rules_structure_fk, struc_mtx_action
				FROM  #TS_MATRIX_RANK 
                WHERE SCHEDULE_RANK = 1
                ORDER BY rules_structure_fk, struc_mtx_action
                    
    OPEN cur_v_struct_group;
    FETCH NEXT FROM cur_v_struct_group INTO  @vm$rules_structure_fk, @vm$struc_mtx_action  
												
     WHILE @@FETCH_STATUS = 0 
     BEGIN

--Potential for Tuning

		UPDATE #TS_MATRIX_RANK 
		SET   STRUCTURE_RANK = 1 
        WHERE STRUC_MTX_ACTION = 750   --- FIRST
        AND   EVENT_RULES_FK IN ( 
			SELECT A.EVENT_RULES_FK
			FROM (
                SELECT
                ECR.EVENT_RULES_FK,
				( DENSE_RANK() OVER ( PARTITION BY ECR.EVENT_FK, ECR.RULES_STRUCTURE_FK
											ORDER BY ECR.SCHED_PRECEDENCE ASC, ECR.RULES_EFFECTIVE_DATE DESC, ECR.RESULT ASC ) 
                     ) AS DRANK
					FROM #TS_MATRIX_RANK ECR WITH (NOLOCK) 
					WHERE 1 = 1
                    AND   SCHEDULE_RANK = 1 
                    AND   RULES_STRUCTURE_FK = @vm$rules_structure_fk
                    AND   STRUC_MTX_ACTION = @vm$struc_mtx_action
                    AND   @vm$struc_mtx_action   = 750   --- FIRST                    
                ) A
            WHERE A.DRANK = 1 )


		UPDATE #TS_MATRIX_RANK 
		SET   STRUCTURE_RANK = 1 
        WHERE STRUC_MTX_ACTION = 751   --- MAXIMUM 
        AND   EVENT_RULES_FK IN ( 
			SELECT A.EVENT_RULES_FK
			FROM (
                SELECT
                ECR.EVENT_RULES_FK,
				(	DENSE_RANK() OVER ( PARTITION BY ECR.EVENT_FK, ECR.RULES_STRUCTURE_FK
											ORDER BY ECR.RESULT DESC, ECR.RULES_EFFECTIVE_DATE DESC, ECR.RULES_FK DESC )
                     ) AS DRANK
					FROM #TS_MATRIX_RANK ECR WITH (NOLOCK) 
					WHERE 1 = 1
                    AND   SCHEDULE_RANK = 1 
                    AND   RULES_STRUCTURE_FK = @vm$rules_structure_fk
                    AND   STRUC_MTX_ACTION = @vm$struc_mtx_action                    
                    AND   @vm$struc_mtx_action   = 751   --- MAXIMUM 
                ) A
            WHERE A.DRANK = 1 )

                

		UPDATE #TS_MATRIX_RANK 
		SET   STRUCTURE_RANK = 1 
        WHERE STRUC_MTX_ACTION  = 752   --- MINIMUM 
        AND   EVENT_RULES_FK IN ( 
			SELECT A.EVENT_RULES_FK
			FROM (
                SELECT
                ECR.EVENT_RULES_FK,
				(	DENSE_RANK() OVER ( PARTITION BY ECR.EVENT_FK, ECR.RULES_STRUCTURE_FK
											ORDER BY ECR.RESULT ASC,  ECR.RULES_EFFECTIVE_DATE DESC, ECR.RULES_FK DESC ) 
                     ) AS DRANK
					FROM #TS_MATRIX_RANK ECR WITH (NOLOCK) 
					WHERE 1 = 1
                    AND   SCHEDULE_RANK = 1 
                    AND   RULES_STRUCTURE_FK = @vm$rules_structure_fk
                    AND   STRUC_MTX_ACTION = @vm$struc_mtx_action                    
                    AND   @vm$struc_mtx_action   = 752   --- MINIMUM 
                ) A
            WHERE A.DRANK = 1 )


    FETCH NEXT FROM cur_v_struct_group INTO  @vm$rules_structure_fk, @vm$struc_mtx_action  
																							
	END --cur_v_struct_group  LOOP
	CLOSE cur_v_struct_group
	DEALLOCATE cur_v_struct_group; 



----------------------------------------------------------------------------------------
---   BECAUSE THE STRUCTURE GROUPING IS OPTIONAL FOR MANY ERPS AND MAY NOT USED... 
---		IF NO STRUCUTRE MXT ACTION EXISTS... USE THE RESULT_MXT_ACTION
----------------------------------------------------------------------------------------


		UPDATE #TS_MATRIX_RANK 
		SET   RESULT_RANK = 1 
        WHERE RESULT_MTX_ACTION = 750   --- FIRST
        AND   EVENT_RULES_FK IN ( 
			SELECT A.EVENT_RULES_FK
			FROM (
                SELECT
                ECR.EVENT_RULES_FK,
				( DENSE_RANK() OVER ( PARTITION BY ECR.EVENT_FK
											ORDER BY ECR.STRUC_PRECEDENCE ASC, ECR.RULES_EFFECTIVE_DATE DESC, ECR.RESULT ASC ) 
                     ) AS DRANK
					FROM #TS_MATRIX_RANK ECR WITH (NOLOCK) 
					WHERE 1 = 1
                    AND   STRUCTURE_RANK = 1 
                    AND   RESULT_MTX_ACTION = 750   --- FIRST
                ) A
            WHERE A.DRANK = 1 )

		UPDATE #TS_MATRIX_RANK 
		SET   RESULT_RANK = 1
        WHERE RESULT_MTX_ACTION = 751   --- MAXIMUM 
        AND   EVENT_RULES_FK IN ( 
			SELECT A.EVENT_RULES_FK
			FROM (
                SELECT
                ECR.EVENT_RULES_FK,
				( DENSE_RANK() OVER ( PARTITION BY ECR.EVENT_FK
											ORDER BY ECR.RESULT DESC, ECR.RULES_EFFECTIVE_DATE DESC, ECR.RULES_FK DESC )
                     ) AS DRANK
					FROM #TS_MATRIX_RANK ECR WITH (NOLOCK) 
					WHERE 1 = 1
                    AND   STRUCTURE_RANK = 1 
                    AND   RESULT_MTX_ACTION = 751   --- MAXIMUM 
                ) A
            WHERE A.DRANK = 1 )

               
		UPDATE #TS_MATRIX_RANK 
		SET   RESULT_RANK = 1
        WHERE RESULT_MTX_ACTION = 752   --- MINIMUM 
        AND   EVENT_RULES_FK IN ( 
			SELECT A.EVENT_RULES_FK
			FROM (
                SELECT
                ECR.EVENT_RULES_FK,
				( DENSE_RANK() OVER ( PARTITION BY ECR.EVENT_FK
											ORDER BY ECR.RESULT ASC,  ECR.RULES_EFFECTIVE_DATE DESC, ECR.RULES_FK DESC ) 
                     ) AS DRANK
					FROM #TS_MATRIX_RANK ECR WITH (NOLOCK) 
					WHERE 1 = 1
                    AND   STRUCTURE_RANK = 1
                    AND   RESULT_MTX_ACTION = 752   --- MINIMUM 
                ) A
            WHERE A.DRANK = 1 )


-----  BEST LOGIC  >>>>> COMPARE BOTH STRUCTURES PICKED ( BEST AND NON-BEST )
-----
-----           IF A TIE OCCURS WHERE BOTH STRUCTURE PICKS ( BEST AND NON-BEST ) ARE AT THE SAME SCHEDULE PRECEDENCE LEVEL ( IE CONTRACTS AND CONTRACTS ) 
-----			THEN PICK THE LOWEST PRECEDENCE OF BOTH SCHEDULES ( PRECEDENCE ORDER IS SCHEDULE, CONTRACT, RULE ) 
-----				 .. IGNORING PREVIOUS BEST MINUMUM BEST PICKS AND GOING BACK TO PURE PRECEDENCE ONLY FOR BOTH BEST AND NON-BEST 
-----           ELSE 
-----           IF LOWEST PRECEDENCE STRUCTURE PICKED IS A "BEST" THEN PICK THE BEST ( IE MIN ) OF ONLY THOSE ASSIGNED TO BEST STRUCTURE
-----           IF LOWEST PRECEDENCE STRUCTURE PICKED IS NON-BEST THEN PICK THE RULE WITHIN THE STRUCTURE WITH THE LOWEST PRECEDENCE
-----			
------          BPC = 0 ....       That means Best Price is  ON or OFF? – 0 = ON and 1 = OFF
------          BCC = 'No' ....   That means Best Cost is  ON or OFF? – Yes = ON and No = OFF


----------------------------------------------------------------------------------------------------------
-----   Loop through Schedule Precedences lowest to highest of all Structure Rank = 1  ECR Rows
-----    At each level compare the structure = 1 Rows 
-----    Choose the lowest precedence of the Events where the RESULT_RANK <> 1
---------------------------------------------------------------------------------------------------------


		UPDATE #TS_MATRIX_RANK 
		SET   RESULT_RANK = 1 
        WHERE RESULT_MTX_ACTION = 755   --- BEST 
        AND   EVENT_RULES_FK IN ( 
			SELECT A.EVENT_RULES_FK
			FROM (
                SELECT
                ECR.EVENT_RULES_FK,
				( DENSE_RANK() OVER ( PARTITION BY ECR.EVENT_FK
											ORDER BY   ECR.SCHED_PRECEDENCE ASC, 
											           ECR.CONTRACT_PRECEDENCE ASC,  
													   ECR.RULE_PRECEDENCE ASC,
													   ECR.RESULT ASC )
                     ) AS DRANK
					FROM #TS_MATRIX_RANK ECR WITH (NOLOCK) 
					WHERE 1 = 1
					AND   ISNULL ( RESULT, 0 ) <> 0   --- KS 07-24-2012 changed from < 0
                    AND   STRUCTURE_RANK = 1    ------ Each Structures Pick
                    AND   RESULT_MTX_ACTION = 755   --- BEST 
                ) A
            WHERE A.DRANK = 1 )



--- JASCO ENHANCEMENT 
--- ADDED SETTING TOP 1 ORDERED BY RULES_FK
--- SUCH THAT SUBQUERY VIEWS WILL NOT FAIL EVEN THOUGH ALL RULES APPLY WITH 759 -ALL MATRIX ACTION


		UPDATE #TS_MATRIX_RANK 
		SET   RESULT_RANK = 1 
        WHERE RESULT_MTX_ACTION = 759   --- SUM UP ALL RULE RESULTS
        AND   EVENT_RULES_FK IN ( 
			SELECT A.EVENT_RULES_FK
			FROM (
                SELECT
                ECR.EVENT_RULES_FK,
				( DENSE_RANK() OVER ( PARTITION BY ECR.EVENT_FK
											ORDER BY ECR.RULES_FK DESC )
                     ) AS DRANK
					FROM #TS_MATRIX_RANK ECR WITH (NOLOCK) 
					WHERE 1 = 1
					AND   ISNULL ( RESULT, 0 ) > 0
                    AND   RESULT_MTX_ACTION = 759   --- SUM UP ALL RULE RESULTS
                ) A
            WHERE A.DRANK = 1 )




------------------------------------------------------------------------------------------
--   UPDATE EVENT_CALC_RULES WITH RANKINGS
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_CALC_RULES
SET    RESULT_RANK					= ISNULL ( TSMR.RESULT_RANK, 999 )
      ,STRUCTURE_RANK				= TSMR.STRUCTURE_RANK      
      ,SCHEDULE_RANK				= TSMR.SCHEDULE_RANK
-----
FROM #TS_MATRIX_RANK TSMR
WHERE TSMR.EVENT_RULES_FK = synchronizer.EVENT_CALC_RULES.EVENT_RULES_ID 



------------------------------------------------------------------------------------------
--   USING EVENT_CALC_RULES WITH RESULT_RANK = 1 UPDATE EVENT_CALC WITH THE FINAL RESULT
--		Round the final result but leave Event_Calc_Rule Result not Rounded
--  ks add here later to retrieve decimal value on EVENT_CALC from data name table
------------------------------------------------------------------------------------------


UPDATE synchronizer.EVENT_CALC
SET    RESULT			   = (   ( CASE ISNULL ( RA.ROUNDING_METHOD_CR_FK, 0 )
					         WHEN 0 THEN (CASE ISNULL ( DN.SCALE, 0 )
										  WHEN 0  THEN  ECR.RESULT
										  ELSE ( CASE ISNULL ( DN.ROUNDING_METHOD_CR_FK, 0 )
												 WHEN 0   THEN ECR.RESULT
												 WHEN 720 THEN ( select common.RoundNearest ( ECR.RESULT, ISNULL ( DN.SCALE, 2 ) ) )
												 WHEN 721 THEN ( select common.RoundUp ( ECR.RESULT, ISNULL ( DN.SCALE, 2 ) ) )
												 WHEN 722 THEN ( select common.RoundDown ( ECR.RESULT, ISNULL ( DN.SCALE, 2 ) ) )
												 ELSE ECR.RESULT
												 END )
										  END )
							 WHEN 720 THEN ( select common.RoundNearest ( ECR.RESULT, ISNULL ( DN.SCALE, 2 ) ) )
							 WHEN 721 THEN ( select common.RoundUp ( ECR.RESULT, ISNULL ( DN.SCALE, 2 ) ) )
							 WHEN 722 THEN ( select common.RoundDown ( ECR.RESULT, ISNULL ( DN.SCALE, 2 ) ) )
							 WHEN 723 THEN  CASE ISNULL ( ra.rounding_to_value, 0 )
							                WHEN 0 THEN ECR.RESULT
							                ELSE ( select common.RoundNearestToValue ( ECR.RESULT, ra.rounding_to_value ) )
							                END
							 WHEN 724 THEN CASE ISNULL ( ra.rounding_to_value, 0 )
							                WHEN 0 THEN ECR.RESULT
							                ELSE ( select common.RoundUpToValue ( ECR.RESULT, ra.rounding_to_value ) )
							                END
							 WHEN 725 THEN CASE ISNULL ( ra.rounding_to_value, 0 )
							                WHEN 0 THEN ECR.RESULT
							                ELSE ( select common.RoundDownToValue ( ECR.RESULT, ra.rounding_to_value ) )
							                END
							 ELSE ECR.RESULT
					        END ) 	+ ISNULL ( RA.ROUNDING_ADDER_VALUE, 0 )  )
      ,RULES_FK				= ECR.RULES_FK
      ,UPDATE_LOG_FK        = 999     --- WHY 999 ???
-----
FROM synchronizer.EVENT_CALC_RULES ECR  WITH (NOLOCK)
INNER JOIN synchronizer.RULES_ACTION RA WITH (NOLOCK)
 ON ( RA.RULES_ACTION_ID = ECR.RULES_ACTION_FK )
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) 
 ON ( DN.DATA_NAME_ID = ECR.RESULT_DATA_NAME_FK )  
WHERE 1 = 1 
AND   ECR.RESULT_MTX_ACTION_CR_FK <> 759
AND   ECR.RESULT_RANK = 1
AND   ECR.EVENT_FK = synchronizer.EVENT_CALC.EVENT_ID 
AND   ECR.EVENT_FK IN ( SELECT EVENT_ID
                        FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
                        WHERE EC.JOB_FK = @in_analysis_job_fk 
                        AND   EC.RESULT_DATA_NAME_FK = @IN_RESULT_DATA_NAME_FK 
                        AND   EC.CALC_GROUP_SEQ = @in_calc_group_seq )



------------------------------------------------------------------------------------------
--   JASCO Enhancement
--		MATRIX ACTION = 759 --- ALL
--      Result = the Sum of all the Event Calc Rule Results
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_CALC
SET    RESULT = (	SELECT SUM ( ECR.RESULT )
					FROM synchronizer.EVENT_CALC_RULES ECR  WITH (NOLOCK)
					WHERE ECR.EVENT_FK = synchronizer.EVENT_CALC.EVENT_ID  )
WHERE 1 = 1
AND   EVENT_ID IN (
          SELECT ECX.EVENT_ID
          FROM   synchronizer.EVENT_CALC ECX WITH (NOLOCK)					
          INNER JOIN synchronizer.EVENT_CALC_RULES ECRX  WITH (NOLOCK)
            ON ECX.EVENT_ID = ECRX.EVENT_FK 
		  WHERE  ECRX.RESULT_MTX_ACTION_CR_FK = 759
          AND    ECX.JOB_FK = @in_analysis_job_fk 
          AND    ECX.RESULT_DATA_NAME_FK = @IN_RESULT_DATA_NAME_FK
          AND    ECX.CALC_GROUP_SEQ = @in_calc_group_seq
)


------------------------------------------------------------------------------------------
--   JASCO Enhancement
--		MATRIX ACTION = 758 --- PARITY
--      Result = the Max Value of all the Event Calc Rule Results in a Parity Group
--               Every Result in that Parity Group will be set to this Max
--               Event Source is set to PARITY
------------------------------------------------------------------------------------------

UPDATE synchronizer.EVENT_CALC
SET    RESULT = (	SELECT MAX ( EC.RESULT )
					FROM synchronizer.EVENT_CALC EC WITH (NOLOCK)
					WHERE EC.JOB_FK = synchronizer.EVENT_CALC.JOB_FK
					AND   EC.PARITY_DV_FK = synchronizer.EVENT_CALC.PARITY_DV_FK  )
	  ,EVENT_SOURCE_CR_FK = 76  -- PARITY
WHERE 1 = 1
AND   EVENT_ID IN (
          SELECT ECX.EVENT_ID
          FROM   synchronizer.EVENT_CALC ECX WITH (NOLOCK)					
          INNER JOIN synchronizer.EVENT_CALC_RULES ECRX  WITH (NOLOCK)
            ON ECX.EVENT_ID = ECRX.EVENT_FK 
		  WHERE  ECRX.RESULT_MTX_ACTION_CR_FK = 758  --- PARITY
          AND    ECX.JOB_FK = @in_analysis_job_fk 
          AND    ECX.RESULT_DATA_NAME_FK = @IN_RESULT_DATA_NAME_FK
          AND    ECX.CALC_GROUP_SEQ = @in_calc_group_seq
)



-----------------------------------------------------------------------------------------
-- Drop all Temporary Tables created in this procedure...
--	DO NOT DROP #TS_EVENT_CALC this table will get truncated and droped elsewhere
-----------------------------------------------------------------------------------------

------drop temp tables
IF object_id('tempdb..#TS_MATRIX_RANK') is not null
   drop table #TS_MATRIX_RANK



------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of marginmgr.PERFORM_MATRIX_ACTION'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of marginmgr.PERFORM_MATRIX_ACTION has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
