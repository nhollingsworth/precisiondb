﻿
-- =============================================
-- Author:		Gary Stone
-- Create date: 2018-04-01
-- Description:	UI and Reporting results for Level Gross Weekly
-- =============================================
CREATE PROCEDURE [marginmgr].[RETAIL_PRICE_MAINTENANCE_ZONE_TEST_CASES]
 
	@Price_Mgmt_Type_CR_FK int = 650
	, @Item_Change_Type_CR_FK int = 655
	, @Qualified_Status_CR_FK int = 653
	, @Zone_Entity_Structure_FK varchar(Max) = ''
	, @PRICE_CHANGE_EFFECTIVE_DATE varchar(10) = NULL
	, @User Varchar(96) = ''
AS

/*

	Declare @PRICE_CHANGE_EFFECTIVE_DATE DATE = GETDATE() + 6 - DATEPART(DW, GETDATE())
	Declare @Price_Mgmt_Type_CR_FK int = 650
	Declare @Item_Change_Type_CR_FK int = 655
	Declare @Qualified_Status_CR_FK int = 654
	Declare @Zone_Entity_Structure_FK varchar(Max) =  'All'
	Declare @User varchar(64) = 'SSRS'
	--Declare @User varchar(64) = 'gstone@epacube.com'
	--Declare @Zone_Entity_Structure_FK varchar(Max) = '113246,113237,113247,113230,113231,113232,113233,113234,113239'

*/
	Set NoCount ON

	Declare @Report_Only int
	Declare @ind_display_as_reg int = Null
	Declare @ind_margin_changes int = Null

	Set @Report_Only = Case when isnull(@User, '') = 'SSRS' then 1 else 0 end
	Set @ind_display_as_reg = case when @item_change_type_cr_fk in (656, 662) then 0 else 1 end
	Set @ind_margin_changes = case when @item_change_type_cr_fk in (661, 662) then 1 else 0 end

	IF @PRICE_CHANGE_EFFECTIVE_DATE IS NULL
		SET @PRICE_CHANGE_EFFECTIVE_DATE = CAST(getdate() + 6 - datepart(dw, getdate()) AS DATE)

	If Object_ID('tempdb..#Zones') is not null
	drop table #Zones

	If Object_ID('tempdb..#Competitors') is not null
	drop table #Competitors

	If Object_ID('tempdb..#DisplayData') is not null
	drop table #DisplayData

	If Object_ID('dbo.LevelGrossDataView') is not null
	drop table dbo.LevelGrossDataView

	Create table #Zones(zone_entity_Structure_FK bigint not null)
	Create table #Competitors(product_structure_fk bigint not null)

	If @Zone_Entity_Structure_FK = 'All'
	Begin
		Insert into #Zones
		SELECT ei.ENTITY_STRUCTURE_FK 'ZONE_ENTITY_STRUCTURE_FK'
		FROM epacube.ENTITY_IDENTIFICATION ei
			INNER JOIN epacube.DATA_NAME dn ON ei.DATA_NAME_FK = dn.DATA_NAME_ID
			inner join epacube.entity_attribute ea with (nolock) on ei.entity_structure_Fk = ea.entity_structure_Fk and ea.data_name_fk = 502060 and ea.record_status_cr_fk = 1
			inner join epacube.data_value dv with (nolock) on ea.data_value_fk = dv.data_value_id and ea.data_name_fk = dv.data_name_fk
		WHERE dn.NAME = 'ZONE_NUMBER'
		and dv.value = 'H'
	End
	else
	Begin
		Insert into #Zones
		Select listitem from utilities.SplitString(@Zone_Entity_Structure_FK, ',')
	End

	Insert into #Competitors
	Select cv.product_structure_fk from [epacube].[COMPETITOR_VALUES] CV with (nolock) 
	inner join [epacube].[getEntitiesByPermission](@User, 544111) A1 on cv.COMP_ENTITY_STRUCTURE_FK = a1.entity_structure_fk
	where cv.PRODUCT_STRUCTURE_FK is not null group by cv.product_structure_fk

	Create index idx_01 on #Zones(zone_entity_structure_fk)
	Create index idx_01 on #Competitors(product_structure_fk)

	Declare @ZoneCount int
	Set @ZoneCount = (select count(*) from #Zones)


--		/*
--		----	START	****	Scenarios To Account For	****	----
--		Single or Multiple Zones
--			Regular Items and Margin Changes
--			Alias Parent
--			Parity Size
--			parity privvate Label

--		----	END	****	Scenarios To Account For	****	----
--*/

	Select 
	PRC.PRICESHEET_RETAIL_CHANGES_ID
	, PRC.PRICE_MGMT_TYPE_CR_FK
	, PRC.ITEM_CHANGE_TYPE_CR_FK
	, PRC.QUALIFIED_STATUS_CR_FK
	, PRC.PRODUCT_STRUCTURE_FK
	, PRC.ZONE_ENTITY_STRUCTURE_FK
	, PRC.ZONE 'ZONE'
	, PRC.ZONE_NAME 'ZONE_NAME'
	, PRC.SUBGROUP_ID 'SUBGROUP'
	, PRC.SUBGROUP_DESCRIPTION
	, PRC.SUBGROUP_DATA_VALUE_FK
	, cast(PRC.SUBGROUP_ID as varchar(64)) + ' ' +  PRC.SUBGROUP_DESCRIPTION 'SUBGROUP_SEARCH'
	, prc.item
	, PRC.ITEM_DESCRIPTION 'ITEM_DESCRIPTION'
	, prc.item + ' ' + PRC.ITEM_DESCRIPTION 'ITEM_SEARCH'
	, PRC.Alias_ID
	, prc.ALIAS_DESCRIPTION
	, PRC.Alias_ID + ' ' + PRC.ALIAS_DESCRIPTION 'ALIAS_SEARCH'
	, PRC.STORE_PACK
	, PRC.SIZE_UOM
	, PRC.unit_cost_cur 'CUR_COST'
	, PRC.case_cost_cur 'CUR_CASE_COST'
	, PRC.case_cost_new 'NEW_CASE_COST'
	, PRC.PRICE_MULTIPLE_CUR
	, PRC.PRICE_CUR
	, PRC.CUR_GM_PCT
	, PRC.COST_CHANGE
	, PRC.PRICE_CHANGE
	, PRC.unit_cost_new 'NEW_COST'
	, isnull(PRC.PRICE_NEW, prc.PRICE_CUR) 'PRICE_NEW'
	, isnull(PRC.NEW_GM_PCT, PRC.CUR_GM_PCT) 'NEW_GM_PCT'
	, PRC.TARGET_MARGIN
	, PRC.TM_SOURCE
	, PRC.SRP_ADJUSTED
	, PRC.GM_ADJUSTED
	, PRC.TM_ADJUSTED
	, PRC.HOLD_CUR_PRICE
	, PRC.REVIEWED
	, Case when C.product_structure_fk is not null then 1 else Null end 'CIF'
	, PRC.UPDATE_USER_PRICE
	, PRC.UPDATE_USER_SET_TM
	, PRC.UPDATE_USER_HOLD
	, PRC.UPDATE_USER_REVIEWED
	, prc.PRICE_MULTIPLE_NEW
	, prc.ind_Parity
	, prc.ind_Margin_Tol
	, ISNULL(prc.ind_Rescind, 0) 'ind_rescind'
	--, case when isnumeric(reverse(ltrim(rtrim(left(Reverse(PRC.ZONE_NAME), Charindex(' ',Reverse((PRC.ZONE_NAME)))))))) = 1 then 
	--	cast(reverse(ltrim(rtrim(left(Reverse(PRC.ZONE_NAME), Charindex(' ',Reverse(PRC.ZONE_NAME)))))) as int) else Null end 
	--	'zone_number'
	, prc.ZONE 'zone_number'
	--, Case	when prc.multiple_parities is not null then ' Parity to ' + prc.multiple_parities + ' ' when prc.parity_alias_id is not null then ' Parity to ' + cast(prc.parity_alias_id as varchar(64)) + ' ' 
	--		when prc.alias_id_child is not null then ' Parity to ' + prc.alias_id_child + ': ' + prc.parity_pl_child_item 
	--		when prc.parity_pl_child_item is not null then ' Parity to ' + prc.parity_pl_child_item
	--		when prc.parity_pl_parent_item is not null then ' Parity to ' + prc.parity_pl_parent_item
	--	else '' end
	, Case	when prc.multiple_parities is not null then ' Parity to ' + prc.multiple_parities + ' ' 
			when related_item_alias_id is not null then ' Parity to ' + cast(related_item_alias_id as varchar(64)) + ': ' + isnull(parity_parent_item, parity_child_item) --+ ' '
			when parity_alias_id is not null then ' Parity to ' + cast(prc.parity_alias_id as varchar(64)) --+ ' ' 
			when isnull(parity_parent_item, parity_child_item) is not null then ' Parity to ' + isnull(parity_parent_item, parity_child_item) --+ ' '
		else '' end
		+ case when coalesce(Cast(prc.multiple_parities as varchar(8)), Cast(prc.related_item_alias_id as varchar(8)), Cast(prc.parity_alias_id as varchar(8))) is not null
			and coalesce(Cast(prc.ftn_Breakdown_Parent as varchar(8)), Cast(prc.ftn_cost_delta as varchar(8))) is not null then '; ' else '' end --+ ' '
		+ isnull(case when prc.ftn_cost_delta is not null and prc.ftn_Breakdown_Parent is not null then Cast(prc.ftn_Breakdown_Parent as varchar(8)) + '; '+  Cast(prc.ftn_cost_delta as varchar(8))
		else coalesce(Cast(prc.ftn_Breakdown_Parent as varchar(8)), Cast(prc.ftn_cost_delta as varchar(8))) end, '') 
		'item_superscript'
	, prc.multiple_parities
	, prc.parity_alias_id 'parity_alias_fk'
	--, (select top 1 ftn_cost_delta from marginmgr.PRICESHEET_RETAIL_CHANGES prc0 where prc0.price_change_effective_date = prc.PRICE_CHANGE_EFFECTIVE_DATE
	--	and isnull(prc0.alias_id, prc0.product_structure_fk) = isnull(prc.alias_id, prc.product_structure_fk) and prc0.ftn_cost_delta is not null) 'ftn_cost_delta'
	, prc.ftn_cost_delta
	, prc.ftn_Breakdown_Parent
	, prc.item_sort
	, prc.item_group
	, prc.item_sort_report
	, Cast(ascii(left(PRC.ZONE_NAME, 1)) as varchar(16)) + right('000' + ltrim(rtrim(reverse(left(Reverse(PRC.ZONE_NAME), charindex(' ', Reverse(PRC.ZONE_NAME)) - 1)))), 3) + '1' 'Zone_Sort'
	, prc.ind_display_as_reg
	, prc.parity_status
	--, case prc.parity_status when 'P' then '01' when 'C' then '02' else '00' end 'ind_parity_status'
	, prc.ind_parity_status
	--, prc.alias_id_child
	--, prc.parity_pl_child_item
	--, prc.parity_pl_parent_item
	
	, parity_parent_item
	, parity_child_item
	
	, case when pa_500012.product_attribute_id is not null then '{"renderer":{"color":"red","text-decoration":"underline"}}' else null end 'ind_discontinued'
	, prc.price_change_effective_date
	into #DisplayData
	from marginmgr.PRICESHEET_RETAIL_CHANGES prc with (nolock)
	inner join #Zones Z on prc.zone_entity_structure_fk = Z.zone_entity_Structure_FK
	left join epacube.product_attribute pa_500012 with (nolock) on prc.product_structure_fk = pa_500012.PRODUCT_STRUCTURE_FK and pa_500012.data_name_fk = 500012
	left join #Competitors C on prc.PRODUCT_STRUCTURE_FK = C.product_structure_fk
	where prc.PRICE_CHANGE_EFFECTIVE_DATE = @PRICE_CHANGE_EFFECTIVE_DATE
	and prc.ind_display_as_reg = @ind_display_as_reg
	and (prc.QUALIFIED_STATUS_CR_FK = @QUALIFIED_STATUS_CR_FK or @QUALIFIED_STATUS_CR_FK = 0)
	and (prc.PRICE_MGMT_TYPE_CR_FK = @Price_Mgmt_Type_CR_FK or @Price_Mgmt_Type_CR_FK = 0)
	and IND_MARGIN_CHANGES = @ind_margin_changes
	--and (item = '86033w' or alias_id = 18715) and zone = 40004
--	select * from #DisplayData


--select *
--from marginmgr.PRICESHEET_RETAIL_CHANGES
--where PRICE_CHANGE_EFFECTIVE_DATE = '2019-08-09'
--and alias_id = 18715
--and zone = 40004

--select *
--from marginmgr.PRICESHEET_RETAIL_CHANGES
--where PRICE_CHANGE_EFFECTIVE_DATE = '2019-08-09'
--and item = '86033w'
--and zone = 40004

	--create index idx_01 on #DisplayData(PRICE_MGMT_TYPE_CR_FK, item_change_type_cr_fk, QUALIFIED_STATUS_CR_FK, alias_id, product_structure_fk)
	create index idx_01 on #DisplayData(QUALIFIED_STATUS_CR_FK, alias_id, product_structure_fk)

	If @ZoneCount = 1
	Begin
	Select @ZoneCount 'ZoneCount', * 
	into dbo.LevelGrossDataView
	from (
		Select 
		1 'NODE_LEVEL'
		, cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE'
		, NULL 'NODE_COLUMN_VALUE_PARENT'
		, PRC.Alias_ID 'NODE_SORT'
		, PRICESHEET_RETAIL_CHANGES_ID
		, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.QUALIFIED_STATUS_CR_FK end 'QUALIFIED_STATUS_CR_FK'
		, prc.PRODUCT_STRUCTURE_FK
		, prc.ZONE_ENTITY_STRUCTURE_FK
		, prc.ZONE
		, prc.ZONE_NAME
		, PRC.SUBGROUP
		, PRC.SUBGROUP_DESCRIPTION
		, PRC.SUBGROUP_DATA_VALUE_FK
		, PRC.Alias_ID	'ITEM'
		, '' 'ITEM_SEARCH'
		, PRC.Alias_ID + ' ' + PRC.ALIAS_DESCRIPTION 'ALIAS_SEARCH'
		, SUBGROUP_SEARCH
		, PRC.ALIAS_DESCRIPTION 'ITEM_DESCRIPTION'
		, PRC.STORE_PACK
		, PRC.SIZE_UOM
		, PRC.CUR_COST
		, PRC.CUR_CASE_COST
		, PRC.NEW_CASE_COST
		, PRICE_MULTIPLE_CUR
		, PRC.PRICE_CUR
		, PRC.CUR_GM_PCT
		, PRC.COST_CHANGE
		, PRC.PRICE_CHANGE
		, PRC.NEW_COST
		, PRC.PRICE_NEW
		, PRC.NEW_GM_PCT
		, PRC.TARGET_MARGIN
		, PRC.TM_SOURCE
		, PRC.SRP_ADJUSTED
		, PRC.GM_ADJUSTED
		, PRC.TM_ADJUSTED
		, PRC.HOLD_CUR_PRICE
		, PRC.REVIEWED
		, prc.CIF --Null 'CIF'
		, PRC.UPDATE_USER_PRICE
		, PRC.UPDATE_USER_SET_TM
		, PRC.UPDATE_USER_HOLD
		, PRC.UPDATE_USER_REVIEWED
		, Case when isnull(prc.PRICE_MULTIPLE_NEW, 1) = 1 then Null else prc.PRICE_MULTIPLE_NEW end 'PRICE_MULTIPLE_NEW'
		, prc.ind_Parity
		, ind_Margin_Tol
		, prc.ind_Rescind		
		, prc.Zone_Number
		, item_superscript
		--, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort'
		, prc.item_sort
		, prc.item_group 'item_group'
		, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort_report'
		, prc.Zone_Sort
		, 1 'ind_bold'
		, 'Alias Header' 'comment'
		, null 'ind_discontinued'
		, prc.price_change_effective_date
		from #DisplayData prc
		WHERE prc.ITEM_CHANGE_TYPE_CR_FK IN (656, 662)
		--and prc.ind_display_as_reg = @ind_display_as_reg

	UNION

		Select 
		1 'NODE_LEVEL'
		, cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE'
		, NULL 'NODE_COLUMN_VALUE_PARENT'
		, PRC.Alias_ID 'NODE_SORT'
		, NULL 'PRICESHEET_RETAIL_CHANGES_ID'
		, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.QUALIFIED_STATUS_CR_FK end 'QUALIFIED_STATUS_CR_FK'
		, PRC.PRODUCT_STRUCTURE_FK
		, prc.ZONE_ENTITY_STRUCTURE_FK
		, prc.ZONE
		, prc.ZONE_NAME
		, PRC.SUBGROUP
		, PRC.SUBGROUP_DESCRIPTION
		, PRC.SUBGROUP_DATA_VALUE_FK
		, PRC.ITEM	'ITEM'
		, PRC.ITEM + ' ' + PRC.ITEM_DESCRIPTION 'ITEM_SEARCH'
		, PRC.Alias_ID + ' ' + PRC.ALIAS_DESCRIPTION 'ALIAS_SEARCH'
		, SUBGROUP_SEARCH
		, PRC.ITEM_DESCRIPTION 'ITEM_DESCRIPTION'
		, PRC.STORE_PACK
		, PRC.SIZE_UOM
		, PRC.CUR_COST
		, PRC.CUR_CASE_COST
		, PRC.NEW_CASE_COST
		, PRICE_MULTIPLE_CUR
		, PRC.PRICE_CUR
		, PRC.CUR_GM_PCT
		, PRC.COST_CHANGE
		, PRC.PRICE_CHANGE
		, PRC.NEW_COST
		, PRC.PRICE_NEW
		, PRC.NEW_GM_PCT
		, PRC.TARGET_MARGIN
		, PRC.TM_SOURCE
		, PRC.SRP_ADJUSTED
		, PRC.GM_ADJUSTED
		, PRC.TM_ADJUSTED
		, prc.HOLD_CUR_PRICE
		, prc.REVIEWED
		, prc.CIF
		, PRC.UPDATE_USER_PRICE
		, PRC.UPDATE_USER_SET_TM
		, PRC.UPDATE_USER_HOLD
		, PRC.UPDATE_USER_REVIEWED
		, Case when isnull(prc.PRICE_MULTIPLE_NEW, 1) = 1 then Null else prc.PRICE_MULTIPLE_NEW end 'PRICE_MULTIPLE_NEW'
		, prc.ind_Parity
		, prc.ind_Margin_Tol
		, prc.ind_Rescind
		, prc.Zone_Number
		, item_superscript
		--, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort'
		, prc.item_sort
		, prc.item_group 'item_group'
		, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort_report'
		, 0 'Zone_Sort'
		, 1 'ind_bold'
		, 'Item Headers' 'comment'
		, prc.ind_discontinued
		, prc.price_change_effective_date
		from #DisplayData prc
		WHERE prc.ITEM_CHANGE_TYPE_CR_FK NOT IN (656, 662)
		and prc.alias_id is null
		--and prc.ind_display_as_reg = @ind_display_as_reg

	Union
	----Get Items listing
		Select 
		3 'NODE_LEVEL'
		, cast(PRC.item_group as varchar(16)) + '.03' 'NODE_COLUMN_VALUE'
		, cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE_PARENT'
		, PRC.Alias_ID 'NODE_SORT'
		, Null 'PRICESHEET_RETAIL_CHANGES_ID'
		, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.QUALIFIED_STATUS_CR_FK end 'QUALIFIED_STATUS_CR_FK'
		, PRC.PRODUCT_STRUCTURE_FK
		, Null 'ZONE_ENTITY_STRUCTURE_FK'
		, Null  'ZONE'
		, Null 'ZONE_NAME'
		, NULL 'SUBGROUP'
		, NULL 'SUBGROUP_DESCRIPTION'
		, NULL 'SUBGROUP_DATA_VALUE_FK'
		, 'Item Listing' 'ITEM'
		, Null 'ITEM_SEARCH'
		, '' 'ALIAS_SEARCH'
		, '' 'SUBGROUP_SEARCH'
		, Null 'ITEM_DESCRIPTION'
		, Null 'STORE_PACK'
		, Null 'SIZE_UOM'
		--, prc.CUR_COST
		--, prc.CUR_CASE_COST
		, Null 'CUR_COST'
		, Null 'CUR_CASE_COST'


		--, prc.New_CASE_COST
		, Null 'New_CASE_COST'
		, Null PRICE_MULTIPLE_CUR
		, Null PRICE_CUR
		, Null CUR_GM_PCT
		, prc.COST_CHANGE
		, Null PRICE_CHANGE
		--, prc.NEW_COST
		, Null 'NEW_COST'
		, Null PRICE_NEW
		, Null NEW_GM_PCT
		, Null TARGET_MARGIN
		, Null TM_SOURCE
		, Null SRP_ADJUSTED
		, Null GM_ADJUSTED
		, Null TM_ADJUSTED
		, Null HOLD_CUR_PRICE
		, Null REVIEWED
		, NULL 'CIF'
		, Null UPDATE_USER_PRICE
		, Null UPDATE_USER_SET_TM
		, Null UPDATE_USER_HOLD
		, Null UPDATE_USER_REVIEWED
		, Null 'PRICE_MULTIPLE_NEW'
		, Null ind_Parity
		, Null 'Margin_Tol'
		, prc.ind_Rescind
		, '' 'Zone_Number'
		--, case when prc.ftn_Breakdown_Parent is not null then ' ' + prc.ftn_Breakdown_Parent else '' end
		--	+ case when (isnull(prc.parity_parent_item, prc.parity_child_item) is not null or prc.ftn_Breakdown_Parent is not null) and prc.ftn_cost_delta is not null then '; ' else '' end
		--	+ case when prc.ftn_cost_delta is not null then ' ' + prc.ftn_cost_delta else '' end
		--	'item_superscript'
		, case when isnull(prc.ftn_cost_delta, '') <> '' then ' ' + prc.ftn_cost_delta else '' end
		--, 30 'item_sort'
		, prc.item_sort
		, prc.item_group 'item_group'
		, 30 'item_sort_report'
		, Null 'Zone_Sort'
		, 0 'ind_bold'
		, 'Zone Details' 'comment'
		, Null 'ind_discontinued'
		, prc.price_change_effective_date
		from #DisplayData prc
		WHERE 1 = 1
		and prc.item_change_type_cr_fk in (656, 662)
		--and prc.ind_display_as_reg = @ind_display_as_reg

	Union

	----Get Items 
		Select 
		4 'NODE_LEVEL'
		, cast(PRC.item_group as varchar(16)) + '.04' 'NODE_COLUMN_VALUE'
		, cast(PRC.item_group as varchar(16)) + '.03' 'NODE_COLUMN_VALUE_PARENT'
		, PRC.Alias_ID 'NODE_SORT'
		, Null 'PRICESHEET_RETAIL_CHANGES_ID'
		, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.QUALIFIED_STATUS_CR_FK end 'QUALIFIED_STATUS_CR_FK'
		, PRC.PRODUCT_STRUCTURE_FK
		, Null 'ZONE_ENTITY_STRUCTURE_FK'
		, Null  'ZONE'
		, Null 'ZONE_NAME'
		, NULL 'SUBGROUP'
		, NULL 'SUBGROUP_DESCRIPTION'
		, NULL 'SUBGROUP_DATA_VALUE_FK'
		, Item 'ITEM'
		, Item 'ITEM_SEARCH'
		, '' 'ALIAS_SEARCH'
		, '' 'SUBGROUP_SEARCH'
		, PRC.ITEM_DESCRIPTION 'ITEM_DESCRIPTION'
		, STORE_PACK 'STORE_PACK'
		, SIZE_UOM 'SIZE_UOM'
		, prc.CUR_COST
		, prc.CUR_CASE_COST
		, prc.NEW_CASE_COST
		, Null PRICE_MULTIPLE_CUR
		, Null PRICE_CUR
		, Null CUR_GM_PCT
		, prc.COST_CHANGE
		, Null PRICE_CHANGE
		, prc.NEW_COST
		, Null PRICE_NEW
		, Null NEW_GM_PCT
		, Null TARGET_MARGIN
		, Null TM_SOURCE
		, Null SRP_ADJUSTED
		, Null GM_ADJUSTED
		, Null TM_ADJUSTED
		, Null HOLD_CUR_PRICE
		, Null REVIEWED
		, prc.CIF --Null 'CIF'
		, Null UPDATE_USER_PRICE
		, Null UPDATE_USER_SET_TM
		, Null UPDATE_USER_HOLD
		, Null UPDATE_USER_REVIEWED
		, Null 'PRICE_MULTIPLE_NEW'
		, prc.ind_Parity
		, Null 'Margin_Tol'
		, prc.ind_Rescind
		, '' 'Zone_Number'
		--, case when isnull(prc.parity_parent_item, prc.parity_child_item) is not null then ' Parity to ' + isnull(prc.parity_parent_item, prc.parity_child_item) else '' end 
		--	+ case when isnull(prc.parity_parent_item, prc.parity_child_item) is not null and prc.ftn_Breakdown_Parent is not null then '; ' else '' end
		--	+ case when prc.ftn_Breakdown_Parent is not null then ' ' + prc.ftn_Breakdown_Parent else '' end
		--'item_superscript'
		, Case	when isnull(parity_parent_item, parity_child_item) is not null then ' Parity to ' + isnull(parity_parent_item, parity_child_item) end 'item_superscript'
		, prc.item_sort
		, prc.item_group 'item_group'
		, prc.item_sort 'item_sort_report'
		, Null 'Zone_Sort'
		, 0 'ind_bold'
		, 'Get list of Items' 'comment'
		, prc.ind_discontinued
		, prc.price_change_effective_date
		from #DisplayData prc
		WHERE 1 = 1
		and prc.item_change_type_cr_fk not in (656, 662)
		and prc.alias_id is not null
		--and prc.ind_display_as_reg = @ind_display_as_reg

		Union

		Select 
		case when alias_id is null then 2 else 5 end 'NODE_LEVEL'
		, cast(PRC.item_group as varchar(16)) + case when alias_id is null then '.00.0' else '.04' end 'NODE_COLUMN_VALUE'
		, cast(PRC.item_group as varchar(16)) + case when prc.alias_id is null then '.00' else '.03' end 'NODE_COLUMN_VALUE_PARENT'
		, ALIAS_ID 'NODE_SORT'
		, NULL 'PRICESHEET_RETAIL_CHANGES_ID'
		, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.QUALIFIED_STATUS_CR_FK end 'QUALIFIED_STATUS_CR_FK'
		, PB.PRODUCT_STRUCTURE_FK
		, Null ZONE_ENTITY_STRUCTURE_FK
		, Null ZONE
		, Null ZONE_NAME
		, Null 'SUBGROUP'
		, Null SUBGROUP_DESCRIPTION
		, Null SUBGROUP_DATA_VALUE_FK
		, pi.value + ' Qty: ' + Cast(Cast(pb.Qty as int) as varchar(8)) 'ITEM'
		, Null 'ITEM_SEARCH'
		, Null 'ALIAS_SEARCH'
		, Null 'SUBGROUP_SEARCH'
		, ltrim(ltrim(rtrim(Case when isnull(PB.State, '') = '' then '' else 'State: ' + ltrim(rtrim(PB.State)) + ' ' end)) + pd.description) 'item_description'
		, ISNULL(pa_500001.attribute_event_data, '''') 'STORE_PACK'
		, ISNULL(pa_500002.attribute_event_data, '''') 'SIZE_UOM'
		, Null 'CUR_COST'
		, Null 'CUR_CASE_COST'
		, Null 'NEW_CASE_COST'
		, Null 'PRICE_MULTIPLE_CUR'
		, Null 'PRICE_CUR'
		, Null 'CUR_GM_PCT'
		, Null 'COST_CHANGE'
		, Null 'PRICE_CHANGE'
		, Null 'NEW_COST'
		, Null 'PRICE_NEW'
		, Null 'NEW_GM_PCT'
		, Null 'TARGET_MARGIN'
		, Null 'TM_SOURCE'
		, Null 'SRP_ADJUSTED'
		, Null 'GM_ADJUSTED'
		, Null 'TM_ADJUSTED'
		, Null 'HOLD_CUR_PRICE'
		, Null 'REVIEWED'
		, Null 'CIF'
		, Null 'UPDATE_USER_PRICE'
		, Null 'UPDATE_USER_SET_TM'
		, Null 'UPDATE_USER_HOLD'
		, Null 'UPDATE_USER_REVIEWED'
		, Null 'PRICE_MULTIPLE_NEW'
		, NULL 'ind_Parity'
		, Null 'ind_Margin_Tol'
		, Null 'ind_Rescind'
		, Null 'zone_number'
		, null 'item_superscript'
		, prc.item_sort + 1 'item_sort'
		, prc.item_group 'item_group'
		, prc.item_sort + 1 'item_sort_report'
		, 0 'Zone_Sort'
		, null 'ind_bold'
		, 'get breakdown items' 'comment'
		, null 'ind_discontinued'
		, prc.price_change_effective_date
		FROM #DisplayData PRC with (nolock)
		inner join #Zones Z on prc.zone_entity_structure_fk = Z.zone_entity_Structure_FK
		inner join epacube.PRODUCT_BOM PB on prc.product_structure_fk = pb.product_structure_fk and pb.data_name_fk = 500213
		inner join epacube.PRODUCT_IDENTIFICATION pi with (nolock) on pb.child_product_structure_fk = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110100
		inner join epacube.product_description pd with (nolock) on pi.PRODUCT_STRUCTURE_FK = pd.PRODUCT_STRUCTURE_FK and pd.DATA_NAME_FK = 110401
		left join [epacube].[PRODUCT_ATTRIBUTE] pa_500001 with (nolock) on pi.Product_Structure_FK = pa_500001.PRODUCT_STRUCTURE_FK and pa_500001.data_name_fk = 500001
		left join [epacube].[PRODUCT_ATTRIBUTE] pa_500002 with (nolock) on pi.Product_Structure_FK = pa_500002.PRODUCT_STRUCTURE_FK and pa_500002.data_name_fk = 500002
		where 1 = 1
		and @Report_Only = 1
	) A
		Order by isnull(ind_Rescind, 0) desc, node_column_value, item_sort, zone_sort desc

	End
	Else
	Begin
	----Get Alias Headers
	Select @ZoneCount 'ZoneCount', * 
	into dbo.LevelGrossDataView
	from (	
		Select 
		1 'NODE_LEVEL'
		, cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE'
		, NULL 'NODE_COLUMN_VALUE_PARENT'
		, PRC.Alias_ID 'NODE_SORT'
		, NULL 'PRICESHEET_RETAIL_CHANGES_ID'
		, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.QUALIFIED_STATUS_CR_FK end 'QUALIFIED_STATUS_CR_FK'
		, (select top 1 PRODUCT_STRUCTURE_FK from #displaydata where alias_id = prc.alias_id and ITEM_CHANGE_TYPE_CR_FK not in (656, 662) order by item_sort) 'PRODUCT_STRUCTURE_FK'
		, NULL 'ZONE_ENTITY_STRUCTURE_FK'
		, NULL 'ZONE'
		, NULL 'ZONE_NAME'
		, PRC.SUBGROUP
		, PRC.SUBGROUP_DESCRIPTION
		, PRC.SUBGROUP_DATA_VALUE_FK
		, PRC.Alias_ID	'ITEM'
		, '' 'ITEM_SEARCH'
		, PRC.Alias_ID + ' ' + PRC.ALIAS_DESCRIPTION 'ALIAS_SEARCH'
		, SUBGROUP_SEARCH
		, PRC.ALIAS_DESCRIPTION 'ITEM_DESCRIPTION'
		, PRC.STORE_PACK
		, PRC.SIZE_UOM
		, NULL 'CUR_COST'
		, Null 'CUR_CASE_COST'
		, Null 'NEW_CASE_COST'
		, prc.PRICE_MULTIPLE_CUR
		, NULL 'PRICE_CUR'
		, NULL 'CUR_GM_PCT'
		, (Select top 1 prc1.COST_CHANGE from #DisplayData prc1 with (nolock) 
			where isnull(prc1.alias_id, prc1.product_structure_fk) = isnull(prc.alias_id, prc.product_structure_fk)
			and prc1.QUALIFIED_STATUS_CR_FK = prc.QUALIFIED_STATUS_CR_FK 
			and prc1.PRICE_MGMT_TYPE_CR_FK = prc.PRICE_MGMT_TYPE_CR_FK 
			order by ABS(prc1.cost_change) desc) 'COST_CHANGE'
		--, prc.COST_CHANGE
		, NULL 'PRICE_CHANGE'
		, NULL 'NEW_COST'
		, NULL 'PRICE_NEW'
		, NULL 'NEW_GM_PCT'
		, NULL 'TARGET_MARGIN'
		, NULL 'TM_SOURCE'
		, NULL 'SRP_ADJUSTED'
		, NULL 'GM_ADJUSTED'
		, NULL 'TM_ADJUSTED'

		, Case when @Report_Only = 1 then null else
			Case when (Select count(*) from #DisplayData prc1 with (nolock) 
				where isnull(prc1.alias_id, prc1.product_structure_fk) = isnull(prc.alias_id, prc.product_structure_fk)
				and (prc1.QUALIFIED_STATUS_CR_FK = prc.QUALIFIED_STATUS_CR_FK or @QUALIFIED_STATUS_CR_FK = 0)
				and prc1.PRICE_MGMT_TYPE_CR_FK = prc.PRICE_MGMT_TYPE_CR_FK 
				and prc1.ITEM_CHANGE_TYPE_CR_FK <> 657
				and isnull(prc1.HOLD_CUR_PRICE, 0) = 0) = 0 then 1 else null end end 'HOLD_CUR_PRICE'

		--, prc.HOLD_CUR_PRICE
		, Case when @Report_Only = 1 then null else
			Case when (Select count(*) from #DisplayData prc1 with (nolock) 
				where isnull(prc1.alias_id, prc1.product_structure_fk) = isnull(prc.alias_id, prc.product_structure_fk)
				and (prc1.QUALIFIED_STATUS_CR_FK = prc.QUALIFIED_STATUS_CR_FK or @QUALIFIED_STATUS_CR_FK = 0)
				and prc1.PRICE_MGMT_TYPE_CR_FK = prc.PRICE_MGMT_TYPE_CR_FK 
				and prc1.ITEM_CHANGE_TYPE_CR_FK <> 657
				and isnull(prc1.REVIEWED, 0) = 0) = 0 then 1 else null end end 'REVIEWED'
	--	, prc.REVIEWED
		, prc.CIF
		, NULL 'UPDATE_USER_PRICE'
		, NULL 'UPDATE_USER_SET_TM'
		, NULL 'UPDATE_USER_HOLD'
		, NULL 'UPDATE_USER_REVIEWED'
		, NULL 'PRICE_MULTIPLE_NEW'
		, prc.ind_Parity
		, Null 'ind_Margin_Tol'
		, prc.ind_Rescind
		, '' 'Zone_Number'
		, item_superscript
--		, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort'
		, prc.item_sort
		, prc.item_group 'item_group'
		, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort_report'
		, 0 'Zone_Sort'
		, 1 'ind_bold'
		, 'Alias Headers' 'comment'
		, null 'ind_discontinued'
		, prc.price_change_effective_date
		from #DisplayData prc
		WHERE 1 = 1
		--and prc.ind_display_as_reg = @ind_display_as_reg
		and prc.ITEM_CHANGE_TYPE_CR_FK IN (656, 662)

	UNION
	----Get Item Headers
		Select 
		1 'NODE_LEVEL'
		, cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE'
		, NULL 'NODE_COLUMN_VALUE_PARENT'
		, PRC.Alias_ID 'NODE_SORT'
		, NULL 'PRICESHEET_RETAIL_CHANGES_ID'
		, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.QUALIFIED_STATUS_CR_FK end 'QUALIFIED_STATUS_CR_FK'
		, PRC.PRODUCT_STRUCTURE_FK
		, NULL 'ZONE_ENTITY_STRUCTURE_FK'
		, NULL 'ZONE'
		, NULL 'ZONE_NAME'
		, PRC.SUBGROUP
		, PRC.SUBGROUP_DESCRIPTION
		, PRC.SUBGROUP_DATA_VALUE_FK
		, PRC.ITEM	'ITEM'
		, PRC.ITEM + ' ' + PRC.ITEM_DESCRIPTION 'ITEM_SEARCH'
		, PRC.Alias_ID + ' ' + PRC.ALIAS_DESCRIPTION 'ALIAS_SEARCH'
		, SUBGROUP_SEARCH
		, PRC.ITEM_DESCRIPTION 'ITEM_DESCRIPTION'
		, PRC.STORE_PACK
		, PRC.SIZE_UOM
		, NULL 'CUR_COST'
		, Null 'CUR_CASE_COST'
		, Null 'NEW_CASE_COST'
		, prc.PRICE_MULTIPLE_CUR
		, NULL 'PRICE_CUR'
		, NULL 'CUR_GM_PCT'
		, (Select top 1 prc1.COST_CHANGE from #DisplayData prc1 with (nolock) 
			where isnull(prc1.alias_id, prc1.product_structure_fk) = isnull(prc.alias_id, prc.product_structure_fk)
			and prc1.QUALIFIED_STATUS_CR_FK = prc.QUALIFIED_STATUS_CR_FK 
			and prc1.PRICE_MGMT_TYPE_CR_FK = prc.PRICE_MGMT_TYPE_CR_FK 
			order by ABS(prc1.cost_change) desc) 'COST_CHANGE'
		--, prc.COST_CHANGE
		, NULL 'PRICE_CHANGE'
		, NULL 'NEW_COST'
		, NULL 'PRICE_NEW'
		, NULL 'NEW_GM_PCT'
		, NULL 'TARGET_MARGIN'
		, NULL 'TM_SOURCE'
		, NULL 'SRP_ADJUSTED'
		, NULL 'GM_ADJUSTED'
		, NULL 'TM_ADJUSTED'
		, Case when @Report_Only = 1 then null else
			Case when (Select count(*) from #DisplayData prc1 with (nolock) 
				where isnull(prc1.alias_id, prc1.product_structure_fk) = isnull(prc.alias_id, prc.product_structure_fk)
				and (prc1.QUALIFIED_STATUS_CR_FK = prc.QUALIFIED_STATUS_CR_FK or @QUALIFIED_STATUS_CR_FK = 0)
				and prc1.PRICE_MGMT_TYPE_CR_FK = prc.PRICE_MGMT_TYPE_CR_FK 
				and isnull(prc1.HOLD_CUR_PRICE, 0) = 0) = 0 then 1 else null end end 'HOLD_CUR_PRICE'
	--	, prc.HOLD_CUR_PRICE
		, Case when @Report_Only = 1 then null else
			Case when (Select count(*) from #DisplayData prc1 with (nolock) 
				where isnull(prc1.alias_id, prc1.product_structure_fk) = isnull(prc.alias_id, prc.product_structure_fk)
				and (prc1.QUALIFIED_STATUS_CR_FK = prc.QUALIFIED_STATUS_CR_FK or @QUALIFIED_STATUS_CR_FK = 0)
				and prc1.PRICE_MGMT_TYPE_CR_FK = prc.PRICE_MGMT_TYPE_CR_FK 
				and isnull(prc1.REVIEWED, 0) = 0) = 0 then 1 else null end end 'REVIEWED'
	--	, prc.REVIEWED
		, prc.CIF
		, NULL 'UPDATE_USER_PRICE'
		, NULL 'UPDATE_USER_SET_TM'
		, NULL 'UPDATE_USER_HOLD'
		, NULL 'UPDATE_USER_REVIEWED'
		, NULL 'PRICE_MULTIPLE_NEW'
		, prc.ind_Parity
		, Null 'ind_Margin_Tol'
		, prc.ind_Rescind
		, '' 'Zone_Number'
		, item_superscript
		--, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort'
		, prc.item_sort
		, prc.item_group 'item_group'
		, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort_report'
		, 0 'Zone_Sort'
		, 1 'ind_bold'
		, 'Item Headers' 'comment'
		, prc.ind_discontinued
		, prc.price_change_effective_date
		from #DisplayData prc
		WHERE prc.ITEM_CHANGE_TYPE_CR_FK NOT IN (656, 662)
		and prc.alias_id is null
		--and prc.ind_display_as_reg = @ind_display_as_reg

	Union
	----Get Zones and their details
		Select 
		2 'NODE_LEVEL'
		--, Cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status + '.' + cast(prc.zone_entity_structure_fk as varchar(24)) 'NODE_COLUMN_VALUE'
		, Cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status + '.' + cast(8600000 - prc.zone_sort as varchar(24)) 'NODE_COLUMN_VALUE'
		,  cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE_PARENT'
		, PRC.Alias_ID 'NODE_SORT'
		, PRC.PRICESHEET_RETAIL_CHANGES_ID
		, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.QUALIFIED_STATUS_CR_FK end 'QUALIFIED_STATUS_CR_FK'
		, PRC.PRODUCT_STRUCTURE_FK
		, PRC.ZONE_ENTITY_STRUCTURE_FK
		, PRC.ZONE 'ZONE'
		, PRC.ZONE_NAME 'ZONE_NAME'
		, NULL 'SUBGROUP'
		, NULL 'SUBGROUP_DESCRIPTION'
		, NULL 'SUBGROUP_DATA_VALUE_FK'
		, 'ZONE ' + PRC.ZONE 'ITEM'
		, '' 'ITEM_SEARCH'
		, '' 'ALIAS_SEARCH'
		, '' 'SUBGROUP_SEARCH'
		, PRC.ZONE_NAME 'ITEM_DESCRIPTION'
		, NULL 'STORE_PACK'
		, NULL 'SIZE_UOM'
		, PRC.CUR_COST
		, PRC.CUR_CASE_COST
		, PRC.NEW_CASE_COST
		, PRICE_MULTIPLE_CUR
		, PRC.PRICE_CUR
		, PRC.CUR_GM_PCT
		, PRC.COST_CHANGE
		, PRC.PRICE_CHANGE
		, PRC.NEW_COST
		, PRC.PRICE_NEW
		, PRC.NEW_GM_PCT
		, PRC.TARGET_MARGIN
		, PRC.TM_SOURCE
		, PRC.SRP_ADJUSTED
		, PRC.GM_ADJUSTED
		, PRC.TM_ADJUSTED
		, PRC.HOLD_CUR_PRICE
		, PRC.REVIEWED
		, NULL 'CIF'
		, PRC.UPDATE_USER_PRICE
		, PRC.UPDATE_USER_SET_TM
		, PRC.UPDATE_USER_HOLD
		, PRC.UPDATE_USER_REVIEWED
		, Case when isnull(prc.PRICE_MULTIPLE_NEW, 1) = 1 then Null else prc.PRICE_MULTIPLE_NEW end 'PRICE_MULTIPLE_NEW'
		, Null ind_Parity
		, ind_Margin_Tol
		, prc.ind_Rescind
		, '' 'Zone_Number'
		, Null 'item_superscript'
		--, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort'
		, prc.item_sort
		, prc.item_group 'item_group'
		, case when isnull(prc.parity_status, 'P') = 'P' then 10 else 20 end 'item_sort_report'
		, prc.Zone_Sort
		, 0 'ind_bold'
		, 'Zone Details' 'comment'
		, null 'ind_discontinued'
		, prc.price_change_effective_date
		from #DisplayData prc
		WHERE 1 = 1
		and (prc.item_change_type_cr_fk in (656, 662) or prc.alias_id is null)
		--and prc.ind_display_as_reg = @ind_display_as_reg

	Union
	----Get Items listing Node
		Select 
		3 'NODE_LEVEL'
		, cast(PRC.item_group as varchar(16)) + '.03' 'NODE_COLUMN_VALUE'
		, cast(PRC.item_group as varchar(16)) + '.' + prc.ind_parity_status 'NODE_COLUMN_VALUE_PARENT'
		, PRC.Alias_ID 'NODE_SORT'
		, Null 'PRICESHEET_RETAIL_CHANGES_ID'
		, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.QUALIFIED_STATUS_CR_FK end 'QUALIFIED_STATUS_CR_FK'
		, NULL 'PRODUCT_STRUCTURE_FK'
		, Null 'ZONE_ENTITY_STRUCTURE_FK'
		, Null  'ZONE'
		, Null 'ZONE_NAME'
		, NULL 'SUBGROUP'
		, NULL 'SUBGROUP_DESCRIPTION'
		, NULL 'SUBGROUP_DATA_VALUE_FK'
		, 'Item Listing' 'ITEM'
		, Null 'ITEM_SEARCH'
		, '' 'ALIAS_SEARCH'
		, '' 'SUBGROUP_SEARCH'
		, Null 'ITEM_DESCRIPTION'
		, Null 'STORE_PACK'
		, Null 'SIZE_UOM'

		--, prc.CUR_COST
		--, prc.CUR_CASE_COST
		, Null 'CUR_COST'
		, Null 'CUR_CASE_COST'


		--, prc.New_CASE_COST
		, Null 'New_CASE_COST'
		, Null PRICE_MULTIPLE_CUR
		, Null PRICE_CUR
		, Null CUR_GM_PCT
		, prc.COST_CHANGE
		, Null PRICE_CHANGE
		--, prc.NEW_COST
		, Null 'NEW_COST'
		, Null PRICE_NEW
		, Null NEW_GM_PCT
		, Null TARGET_MARGIN
		, Null TM_SOURCE
		, Null SRP_ADJUSTED
		, Null GM_ADJUSTED
		, Null TM_ADJUSTED
		, Null HOLD_CUR_PRICE
		, Null REVIEWED
		, NULL 'CIF'
		, Null UPDATE_USER_PRICE
		, Null UPDATE_USER_SET_TM
		, Null UPDATE_USER_HOLD
		, Null UPDATE_USER_REVIEWED
		, Null 'PRICE_MULTIPLE_NEW'
		, Null ind_Parity
		, Null 'Margin_Tol'
		, prc.ind_Rescind
		, '' 'Zone_Number'
		--, case when prc.ftn_Breakdown_Parent is not null then ' ' + prc.ftn_Breakdown_Parent else '' end
		--	+ case when (isnull(prc.parity_parent_item, prc.parity_child_item) is not null or prc.ftn_Breakdown_Parent is not null) and prc.ftn_cost_delta is not null then '; ' else '' end
		--	+ case when prc.ftn_cost_delta is not null then ' ' + prc.ftn_cost_delta else '' end
		, case when isnull(prc.ftn_cost_delta, '') <> '' then ' ' + prc.ftn_cost_delta else '' end
			'item_superscript'
		--, 30 'item_sort'
		, prc.item_sort
		, prc.item_group 'item_group'
		, 30 'item_sort_report'
		, Null 'Zone_Sort'
		, 0 'ind_bold'
		, 'Zone Details' 'comment'
		, null 'ind_discontinued'
		, prc.price_change_effective_date
		from #DisplayData prc
		WHERE 1 = 1
		and prc.item_change_type_cr_fk in (656, 662)
		--and prc.ind_display_as_reg = @ind_display_as_reg

	Union

	----Get Items 
		Select 
		4 'NODE_LEVEL'
		, cast(PRC.item_group as varchar(16)) + '.04' 'NODE_COLUMN_VALUE'
		, cast(PRC.item_group as varchar(16)) + '.03' 'NODE_COLUMN_VALUE_PARENT'
		, PRC.Alias_ID 'NODE_SORT'
		, Null 'PRICESHEET_RETAIL_CHANGES_ID'
		, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.QUALIFIED_STATUS_CR_FK end 'QUALIFIED_STATUS_CR_FK'
		, PRC.PRODUCT_STRUCTURE_FK
		, Null 'ZONE_ENTITY_STRUCTURE_FK'
		, Null  'ZONE'
		, Null 'ZONE_NAME'
		, NULL 'SUBGROUP'
		, NULL 'SUBGROUP_DESCRIPTION'
		, NULL 'SUBGROUP_DATA_VALUE_FK'
		, Item 'ITEM'
		, Item 'ITEM_SEARCH'
		, '' 'ALIAS_SEARCH'
		, '' 'SUBGROUP_SEARCH'
		, PRC.ITEM_DESCRIPTION 'ITEM_DESCRIPTION'
		, STORE_PACK 'STORE_PACK'
		, SIZE_UOM 'SIZE_UOM'
		, prc.CUR_COST
		, prc.CUR_CASE_COST
		, prc.New_CASE_COST 'NEW_CASE_COST'
		, Null PRICE_MULTIPLE_CUR
		, Null PRICE_CUR
		, Null CUR_GM_PCT
		, prc.COST_CHANGE
		, Null PRICE_CHANGE
		, prc.NEW_COST
		, Null PRICE_NEW
		, Null NEW_GM_PCT
		, Null TARGET_MARGIN
		, Null TM_SOURCE
		, Null SRP_ADJUSTED
		, Null GM_ADJUSTED
		, Null TM_ADJUSTED
		, Null HOLD_CUR_PRICE
		, Null REVIEWED
		, prc.CIF
		, Null UPDATE_USER_PRICE
		, Null UPDATE_USER_SET_TM
		, Null UPDATE_USER_HOLD
		, Null UPDATE_USER_REVIEWED
		, Null 'PRICE_MULTIPLE_NEW'
		, prc.ind_Parity
		, Null 'Margin_Tol'
		, prc.ind_Rescind
		, '' 'Zone_Number'
		--, case when isnull(prc.parity_pl_parent_item, prc.parity_pl_child_item) is not null then ' Parity to ' + isnull(prc.parity_pl_parent_item, prc.parity_pl_child_item) else '' end 
		--	+ case when isnull(prc.parity_pl_parent_item, prc.parity_pl_child_item) is not null and prc.ftn_Breakdown_Parent is not null then '; ' else '' end
		--	+ case when prc.ftn_Breakdown_Parent is not null then ' ' + prc.ftn_Breakdown_Parent else '' end
		--'item_superscript'
		--, rtrim(replace(replace(prc.item_superscript, 'mcv', ''), 'brkdwn', '')) 'item_superscript'
		, Case	when isnull(parity_parent_item, parity_child_item) is not null then ' Parity to ' + isnull(parity_parent_item, parity_child_item) end 'item_superscript'
		, prc.item_sort
		, prc.item_group 'item_group'
		, prc.item_sort 'item_sort_report'
		, Null 'Zone_Sort'
		, 0 'ind_bold'
		, 'Get list of Items' 'comment'
		, prc.ind_discontinued
		, prc.price_change_effective_date
		from #DisplayData prc
		WHERE 1 = 1
		and prc.item_change_type_cr_fk not in (656, 662)
		and prc.alias_id is not null
		--and prc.ind_display_as_reg = @ind_display_as_reg

		Union

		Select 
		case when alias_id is null then 2 else 5 end 'NODE_LEVEL'
		, cast(PRC.item_group as varchar(16)) + case when alias_id is null then '.00.0' else '.04' end 'NODE_COLUMN_VALUE'
		, cast(PRC.item_group as varchar(16)) + case when prc.alias_id is null then '.00' else '.03' end 'NODE_COLUMN_VALUE_PARENT'
		, ALIAS_ID 'NODE_SORT'
		, NULL 'PRICESHEET_RETAIL_CHANGES_ID'
		, Case @Report_Only when 0 then null else PRC.PRICE_MGMT_TYPE_CR_FK end 'PRICE_MGMT_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.ITEM_CHANGE_TYPE_CR_FK end 'ITEM_CHANGE_TYPE_CR_FK'
		, Case @Report_Only when 0 then null else PRC.QUALIFIED_STATUS_CR_FK end 'QUALIFIED_STATUS_CR_FK'
		, PB.PRODUCT_STRUCTURE_FK
		, Null ZONE_ENTITY_STRUCTURE_FK
		, Null ZONE
		, Null ZONE_NAME
		, Null 'SUBGROUP'
		, Null SUBGROUP_DESCRIPTION
		, Null SUBGROUP_DATA_VALUE_FK
		, pi.value + ' Qty: ' + Cast(Cast(pb.Qty as int) as varchar(8)) 'ITEM'
		, Null 'ITEM_SEARCH'
		, Null 'ALIAS_SEARCH'
		, Null 'SUBGROUP_SEARCH'
		, ltrim(ltrim(rtrim(Case when isnull(PB.State, '') = '' then '' else 'State: ' + ltrim(rtrim(PB.State)) + ' ' end)) + pd.description) 'item_description'
		, ISNULL(pa_500001.attribute_event_data, '''') 'STORE_PACK'
		, ISNULL(pa_500002.attribute_event_data, '''') 'SIZE_UOM'
		, Null 'CUR_COST'
		, Null 'CUR_CASE_COST'
		, Null 'NEW_CASE_COST'
		, Null 'PRICE_MULTIPLE_CUR'
		, Null 'PRICE_CUR'
		, Null 'CUR_GM_PCT'
		, Null 'COST_CHANGE'
		, Null 'PRICE_CHANGE'
		, Null 'NEW_COST'
		, Null 'PRICE_NEW'
		, Null 'NEW_GM_PCT'
		, Null 'TARGET_MARGIN'
		, Null 'TM_SOURCE'
		, Null 'SRP_ADJUSTED'
		, Null 'GM_ADJUSTED'
		, Null 'TM_ADJUSTED'
		, Null 'HOLD_CUR_PRICE'
		, Null 'REVIEWED'
		, Null 'CIF'
		, Null 'UPDATE_USER_PRICE'
		, Null 'UPDATE_USER_SET_TM'
		, Null 'UPDATE_USER_HOLD'
		, Null 'UPDATE_USER_REVIEWED'
		, Null 'PRICE_MULTIPLE_NEW'
		, NULL 'ind_Parity'
		, Null 'ind_Margin_Tol'
		, Null 'ind_Rescind'
		, Null 'zone_number'
		, null 'item_superscript'
		--, prc.item_sort + 1 'item_sort'
		, left(prc.item, len(prc.item)  - 1) + 1 'item_sort'
		, prc.item_group 'item_group'
		, prc.item_sort + 1 'item_sort_report'
		, 0 'Zone_Sort'
		, null 'ind_bold'
		, 'get breakdown items' 'comment'
		, null 'ind_discontinued'
		, prc.price_change_effective_date
		FROM #DisplayData PRC with (nolock)
		inner join #Zones Z on prc.zone_entity_structure_fk = Z.zone_entity_Structure_FK
		inner join epacube.PRODUCT_BOM PB on prc.product_structure_fk = pb.product_structure_fk and pb.data_name_fk = 500213
		inner join epacube.PRODUCT_IDENTIFICATION pi with (nolock) on pb.child_product_structure_fk = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110100
		inner join epacube.product_description pd with (nolock) on pi.PRODUCT_STRUCTURE_FK = pd.PRODUCT_STRUCTURE_FK and pd.DATA_NAME_FK = 110401
		left join [epacube].[PRODUCT_ATTRIBUTE] pa_500001 with (nolock) on pi.Product_Structure_FK = pa_500001.PRODUCT_STRUCTURE_FK and pa_500001.data_name_fk = 500001
		left join [epacube].[PRODUCT_ATTRIBUTE] pa_500002 with (nolock) on pi.Product_Structure_FK = pa_500002.PRODUCT_STRUCTURE_FK and pa_500002.data_name_fk = 500002
		where 1 = 1
		and @Report_Only = 1
	) A	
		Order by isnull(ind_Rescind, 0) desc, node_column_value, item_sort, zone_sort desc
	End

	Select @Price_Mgmt_Type_CR_FK '@Price_Mgmt_Type_CR_FK', @Item_Change_Type_CR_FK '@Item_Change_Type_CR_FK', @Qualified_Status_CR_FK '@Qualified_Status_CR_FK', @ZoneCount '@ZoneCount'

	Select 'Null Case Costs Multi Zone' 'Test Case', * 
	from dbo.LevelGrossDataView
	where 1 = 1
	and zonecount > 1
	and NODE_LEVEL not in (1, 3, 5)
	and Cur_Case_Cost is null

	Select 'Unqualified Status' 'Test Case', * 
	from dbo.LevelGrossDataView
	where 1 = 1
	and zonecount > 1
	--and NODE_LEVEL not in (1, 3, 5)
	and isnull(price_new, 0) <> isnull(price_cur, 0)
	and isnull(QUALIFIED_STATUS_CR_FK, 0) = 654

	Select Test_Case, Item_Type, Alias_ID, count(*) Recs from (
		Select 'Mismatched Item Groups' 'Test_Case', 'Alias' 'Item_Type', alias_id, item_group
		from marginmgr.pricesheet_retail_changes
		where PRICE_CHANGE_EFFECTIVE_DATE = cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date)
		and alias_id is not null
		group by alias_id, item_group
	) A group by Test_Case, Item_Type, Alias_ID
		having count(*) > 1

	Select Test_Case, Item_Type, Item, count(*) Recs from (
		Select 'Mismatched Item Groups' 'Test_Case', 'item' 'Item_Type', item, item_group
		from marginmgr.pricesheet_retail_changes
		where PRICE_CHANGE_EFFECTIVE_DATE = cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date)
		and item is not null and alias_id is null
		group by item, item_group
	) A group by Test_Case, Item_Type, Item
		having count(*) > 1

	Select Test_Case, Item_Type, Alias_ID, count(*) Recs from (
		Select 'Mismatched Display Page' 'Test_Case', 'Alias' 'Item_Type', alias_id, item_group, ind_display_as_reg
		from marginmgr.pricesheet_retail_changes
		where PRICE_CHANGE_EFFECTIVE_DATE = cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date)
		and alias_id is not null
		group by alias_id, item_group, ind_display_as_reg
	) A group by Test_Case, Item_Type, Alias_ID
		having count(*) > 1

	Select Test_Case, Item_Type, Item, count(*) Recs from (
		Select 'Mismatched Display Page' 'Test_Case', 'item' 'Item_Type', item, item_group, ind_display_as_reg
		from marginmgr.pricesheet_retail_changes
		where PRICE_CHANGE_EFFECTIVE_DATE = cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date)
		and item is not null and alias_id is null
		group by item, item_group, ind_display_as_reg
	) A group by Test_Case, Item_Type, Item
		having count(*) > 1

	Select 'Aliases in Parity Item Group and Display Page' 'Test Case', prc1.alias_id, prc2.alias_id, prc1.item_group, prc1.ind_display_as_reg, prc2.item_group, prc2.ind_display_as_reg
	from marginmgr.pricesheet_retail_changes prc1
	inner join marginmgr.pricesheet_retail_changes prc2 on prc1.price_change_effective_date = prc2.price_change_effective_date
														and prc1.zone_entity_structure_fk = prc2.zone_entity_structure_fk
														and prc1.alias_id = prc2.parity_alias_id
	where 1 = 1
	and prc1.alias_id is not null
	and prc2.alias_id is not null
	and prc1.ITEM_CHANGE_TYPE_CR_FK = 656
	and prc2.ITEM_CHANGE_TYPE_CR_FK = 656
	and prc1.PRICE_CHANGE_EFFECTIVE_DATE = cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date)
	and prc1.parity_status = 'P'
	and prc2.parity_status = 'C'
	and (
			prc2.item_group <> (select top 1 prc3.item_group from marginmgr.pricesheet_retail_changes prc3 where prc3.PRICE_CHANGE_EFFECTIVE_DATE = cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date)
																									and prc3.item_group > prc1.item_group order by prc3.item_group)
		or
			prc1.ind_display_as_reg <> prc2.ind_display_as_reg
		)
	group by prc1.alias_id, prc2.alias_id, prc1.item_group, prc1.ind_display_as_reg, prc2.item_group, prc2.ind_display_as_reg



	Select 'PL Aliases in Parity with National Item = Item Group and Display Page' 'Test Case', prc1.alias_id, prc2.item, prc1.item_group, prc1.ind_display_as_reg, prc2.item_group, prc2.ind_display_as_reg
	from marginmgr.pricesheet_retail_changes prc1
	inner join marginmgr.pricesheet_retail_changes prc2 on prc1.price_change_effective_date = prc2.price_change_effective_date
														and prc1.zone_entity_structure_fk = prc2.zone_entity_structure_fk
														and prc1.alias_id = prc2.related_item_alias_id
	where 1 = 1
	and prc1.alias_id is not null
	and prc2.alias_id is null
	and prc1.ITEM_CHANGE_TYPE_CR_FK = 656
	and prc2.ITEM_CHANGE_TYPE_CR_FK <> 656
	and prc1.PRICE_CHANGE_EFFECTIVE_DATE = cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date)
	and prc1.parity_status = 'P'
	and prc2.parity_status = 'C'
	and (
			prc2.item_group <> (select top 1 prc3.item_group from marginmgr.pricesheet_retail_changes prc3 where prc3.PRICE_CHANGE_EFFECTIVE_DATE = cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date)
																									and prc3.item_group > prc1.item_group order by prc3.item_group)
		or
			prc1.ind_display_as_reg <> prc2.ind_display_as_reg
		)
	group by prc1.alias_id, prc2.item, prc1.item_group, prc1.ind_display_as_reg, prc2.item_group, prc2.ind_display_as_reg

	Select 'PL Item in Parity with National Alias = Item Group and Display Page' 'Test Case', prc1.item, prc2.ALIAS_ID, prc1.item_group, prc1.ind_display_as_reg, prc2.item_group, prc2.ind_display_as_reg
	from marginmgr.pricesheet_retail_changes prc1
	inner join marginmgr.pricesheet_retail_changes prc2 on prc1.price_change_effective_date = prc2.price_change_effective_date
														and prc1.zone_entity_structure_fk = prc2.zone_entity_structure_fk
														and prc1.related_item_alias_id = prc2.alias_id
	where 1 = 1
	and prc1.alias_id is null
	and prc2.alias_id is not null
	and prc1.ITEM_CHANGE_TYPE_CR_FK <> 656
	and prc2.ITEM_CHANGE_TYPE_CR_FK = 656
	and prc1.PRICE_CHANGE_EFFECTIVE_DATE = cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date)
	and prc1.parity_status = 'P'
	and prc2.parity_status = 'C'
	and (
			prc2.item_group <> (select top 1 prc3.item_group from marginmgr.pricesheet_retail_changes prc3 where prc3.PRICE_CHANGE_EFFECTIVE_DATE = cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date)
																									and prc3.item_group > prc1.item_group order by prc3.item_group)
		or
			prc1.ind_display_as_reg <> prc2.ind_display_as_reg
		)
	group by prc1.item, prc2.ALIAS_ID, prc1.item_group, prc1.ind_display_as_reg, prc2.item_group, prc2.ind_display_as_reg


	Select 'PL Item in Parity with National Item = Item Group and Display Page' 'Test Case', prc1.item, prc2.item, prc1.item_group, prc1.ind_display_as_reg, prc2.item_group, prc2.ind_display_as_reg
	from marginmgr.pricesheet_retail_changes prc1
	inner join marginmgr.pricesheet_retail_changes prc2 on prc1.price_change_effective_date = prc2.price_change_effective_date
														and prc1.zone_entity_structure_fk = prc2.zone_entity_structure_fk
														and prc1.PRODUCT_STRUCTURE_FK = prc2.parity_parent_product_structure_fk
	where 1 = 1
	and prc1.alias_id is null
	and prc2.alias_id is null
	and prc1.ITEM_CHANGE_TYPE_CR_FK <> 656
	and prc2.ITEM_CHANGE_TYPE_CR_FK <> 656
	and prc1.PRICE_CHANGE_EFFECTIVE_DATE = cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date)
	and prc1.parity_status = 'P'
	and prc2.parity_status = 'C'
	and (
			prc2.item_group <> (select top 1 prc3.item_group from marginmgr.pricesheet_retail_changes prc3 where prc3.PRICE_CHANGE_EFFECTIVE_DATE = cast(GETDATE() + 6 - DATEPART(DW, GETDATE()) as date)
																									and prc3.item_group > prc1.item_group order by prc3.item_group)
		or
			prc1.ind_display_as_reg <> prc2.ind_display_as_reg
		)
	group by prc1.item, prc2.item, prc1.item_group, prc1.ind_display_as_reg, prc2.item_group, prc2.ind_display_as_reg
