﻿
--A_epaMAUI_UTIL_PURGE_MARGIN_TABLES

-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To remove rows from Margin Analysis Tables
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        10/24/2010   Initial SQL Version
-- GHS		 08/13/2013	  Added synchronizer.EVENT_CALC_RULES_DISQUALIFIED to procedure
--

CREATE PROCEDURE [marginmgr].[UTIL_PURGE_MARGIN_TABLES_NEW] 
        @IN_JOB_FK INT, @IN_REMOVE_ALL_JOBS_IND SMALLINT
        ------, @IN_LEAVE_ANALYSIS_JOB_IND SMALLINT
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int

BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of marginmgr.UTIL_PURGE_MARGIN_TABLES.'
					 + cast(isnull(@IN_JOB_FK, 0) as varchar(30))

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;

SET @l_sysdate = GETDATE()

IF @IN_REMOVE_ALL_JOBS_IND = 1
AND  @IN_JOB_FK IS NULL
BEGIN 


TRUNCATE TABLE  synchronizer.EVENT_CALC_ERRORS

TRUNCATE TABLE  synchronizer.EVENT_CALC_RULES_BASIS

DELETE 
FROM synchronizer.EVENT_CALC_RULES

Truncate Table synchronizer.EVENT_CALC_RULES_DISQUALIFIED

DELETE 
FROM synchronizer.EVENT_CALC


TRUNCATE TABLE marginmgr.ANALYSIS_JOB_COST_CHANGE

DELETE 
FROM marginmgr.ANALYSIS_JOB
WHERE ISNULL ( TEST_JOB_IND, 0 ) = 0



END
ELSE   --- ONLY REMOVE BY JOB ID
BEGIN





----- DELETE EVENT_DATA tables if exists

DELETE synchronizer.EVENT_DATA_ERRORS
FROM synchronizer.EVENT_DATA_ERRORS EDR WITH (NOLOCK) 
INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK) on ED.Event_ID = EDR.Event_fk
where ED.IMPORT_JOB_FK = @IN_JOB_FK
------query above does the same but with a join instead of a subquery with IN
--DELETE 
--FROM synchronizer.EVENT_DATA_ERRORS
--WHERE EVENT_FK IN (
--          SELECT ED.EVENT_ID
--          FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
--          WHERE ED.IMPORT_JOB_FK = @IN_JOB_FK  )


DELETE synchronizer.EVENT_DATA_RULES
from synchronizer.EVENT_DATA_RULES edr WITH (NOLOCK) 
INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK) on edr.Event_fk = ed.event_ID
where  ED.IMPORT_JOB_FK = @IN_JOB_FK

------query above does the same but with a join instead of a subquery with IN
--DELETE 
--FROM synchronizer.EVENT_DATA_RULES
--WHERE EVENT_FK IN (
--          SELECT ED.EVENT_ID
--          FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
--          WHERE ED.IMPORT_JOB_FK = @IN_JOB_FK  )

DELETE 
FROM synchronizer.EVENT_DATA
WHERE IMPORT_JOB_FK = @IN_JOB_FK


-----  DELETE EVENT_CALC tables

DELETE synchronizer.EVENT_CALC_ERRORS
FROM synchronizer.EVENT_CALC_ERRORS ece WITH (NOLOCK)
INNER JOIN synchronizer.EVENT_CALC EC WITH (NOLOCK) on ec.event_id = ece.event_fk
WHERE EC.JOB_FK = @IN_JOB_FK
------query above does the same but with a join instead of a subquery with IN
--DELETE 
--FROM synchronizer.EVENT_CALC_ERRORS
--WHERE EVENT_FK IN (
--          SELECT EC.EVENT_ID
--          FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
--          WHERE EC.JOB_FK = @IN_JOB_FK  )


DELETE synchronizer.EVENT_CALC_RULES_BASIS
FROM synchronizer.EVENT_CALC_RULES_BASIS ecrb WITH (NOLOCK)
INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK) on ecr.EVENT_RULES_ID = ecrb.event_rules_fk
inner join synchronizer.EVENT_CALC EC WITH (NOLOCK) on ec.event_id = ecr.event_fk
WHERE EC.JOB_FK = @IN_JOB_FK

------query above does the same but with a join instead of a subquery with IN
--DELETE 
--FROM synchronizer.EVENT_CALC_RULES_BASIS
--WHERE  EVENT_RULES_FK IN (
--             SELECT ECR.EVENT_RULES_ID
--             FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
--             INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK) 
--             ON ( ECR.EVENT_FK = EC.EVENT_ID )
--             WHERE EC.JOB_FK = @IN_JOB_FK  )


DELETE synchronizer.EVENT_CALC_RULES
FROM synchronizer.EVENT_CALC_RULES ecr WITH (NOLOCK)
inner join synchronizer.EVENT_CALC EC WITH (NOLOCK) on ec.event_id = ecr.event_fk
 WHERE EC.JOB_FK = @IN_JOB_FK

------query above does the same but with a join instead of a subquery with IN
--DELETE 
--FROM synchronizer.EVENT_CALC_RULES
--WHERE EVENT_FK IN (
--          SELECT EC.EVENT_ID
--          FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
--          WHERE EC.JOB_FK = @IN_JOB_FK  )


DELETE synchronizer.EVENT_CALC_RULES_DISQUALIFIED
FROM synchronizer.EVENT_CALC_RULES_DISQUALIFIED ecrd WITH (NOLOCK)
INNER JOIN synchronizer.EVENT_CALC EC WITH (NOLOCK) on ec.event_id = ecrd.event_fk
WHERE EC.JOB_FK = @IN_JOB_FK

------query above does the same but with a join instead of a subquery with IN
--DELETE 
--FROM synchronizer.EVENT_CALC_RULES_DISQUALIFIED
--WHERE EVENT_FK IN (
--          SELECT EC.EVENT_ID
--          FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
--          WHERE EC.JOB_FK = @IN_JOB_FK  )


DELETE 
FROM synchronizer.EVENT_CALC
WHERE JOB_FK = @IN_JOB_FK


DELETE marginmgr.ANALYSIS_JOB_COST_CHANGE
FROM marginmgr.ANALYSIS_JOB_COST_CHANGE ajcc WITH (NOLOCK)
INNER JOIN marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK) on ajcc.analysis_job_fk = aj.analysis_job_id
WHERE AJ.ANALYSIS_JOB_ID = @IN_JOB_FK 

------query above does the same but with a join instead of a subquery with IN
--DELETE 
--FROM marginmgr.ANALYSIS_JOB_COST_CHANGE
--WHERE ANALYSIS_JOB_FK IN (
--           SELECT ANALYSIS_JOB_ID
--           FROM marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK) 
--           WHERE AJ.ANALYSIS_JOB_ID = @IN_JOB_FK   )

--------IF ISNULL ( @IN_LEAVE_ANALYSIS_JOB_IND, 0 ) = 0
DELETE 
FROM marginmgr.ANALYSIS_JOB
WHERE ANALYSIS_JOB_ID = @IN_JOB_FK
AND   ISNULL ( TEST_JOB_IND, 0 ) = 0



END  -- DELETE BY JOB ID


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of marginmgr.UTIL_PURGE_MARGIN_TABLES'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of marginmgr.UTIL_PURGE_MARGIN_TABLES has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
