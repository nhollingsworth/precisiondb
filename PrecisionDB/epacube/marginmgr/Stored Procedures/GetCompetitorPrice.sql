﻿-- =============================================
-- Author:		Gary Stone
-- Create date: May 16, 2018
-- Description:	Get Competitive Data
-- =============================================
CREATE PROCEDURE [marginmgr].[GetCompetitorPrice]
	@Data Varchar(16) = 'Competitors'
	, @Pricing_Date date = Null
	, @Product_Structure_FK varchar(16) = Null
	, @Comp_Entity_Structure_FK varchar(256) = Null
	, @User Varchar(96) = Null
	, @CurrentOnly int = 0
	, @Alias_ID bigint = null

AS
	SET NOCOUNT ON;

	--Declare @Data varchar(64) = 'Item'
	--Declare @Pricing_Date Date = cast(getdate() as date)
	--Declare @Product_Structure_FK varchar(16) = null
	--Declare @Comp_Entity_Structure_FK varchar(256) = '19282,19269,19278,19267,19270,19293,19260,19264,19234,19281,19280,19272,19212,19277,19317,19291,19294,19287,19265,19259,19288,19262,19237,19318,19225,19319,19230,19322,19323,19276,19227,19231'
	--Declare @User varchar(96) = 'gstone@epacube.com'
	--Declare @CurrentOnly int = 0
	--Declare @Alias_ID bigint = 18318

	Declare @Whr varchar(128) = ''
	Declare @SQL varchar(max)

	Set @Whr = Case when @CurrentOnly = 1 then ' and DRank = 1' else '' end
	Set @Alias_ID = case when @alias_id is null then null else (select data_value_id from epacube.data_value where value = @alias_id and data_name_fk = 500019) end

	If @Data = 'Competitors'
	Begin
		select cv.comp_entity_Structure_FK 'COMP_ENTITY_STRUCTURE_FK', eico.value 'COMPETITOR_ID', einco.value 'COMPETITOR' 
		from [epacube].[COMPETITOR_VALUES] cv
		inner join [epacube].[getEntitiesByPermission](@User, 544111) A1 on cv.COMP_ENTITY_STRUCTURE_FK = a1.entity_structure_fk
		inner join epacube.entity_identification eico with (nolock) on cv.COMP_ENTITY_STRUCTURE_FK = eico.entity_structure_fk
		left join epacube.entity_ident_nonunique einco with (nolock) on cv.COMP_ENTITY_STRUCTURE_FK = einco.ENTITY_STRUCTURE_FK
		left Join marginmgr.pricesheet_retail_changes prc with (nolock) on cv.product_structure_fk = prc.product_structure_FK
		group by cv.comp_entity_Structure_FK, eico.value, einco.value
		order by einco.value, eico.value
	End
	Else
	If @Data = 'Item'
	Begin
	Set @SQL = 
	'	
		Select *, case when isnull(unit_price, 0) = 0 or price_type <> ''regular'' then null else cast((unit_price - unit_cost) / unit_price as numeric(18, 4)) end GM_pct from (
		Select
		CV.COMP_ENTITY_STRUCTURE_FK
		, PI.PRODUCT_STRUCTURE_FK 
		, pi.value ''ITEM_NO''
		, pd.[description] ''ITEM_DESCRIPTION'' 
		, pa.attribute_char ''SIZE_DESCRIPTION''
		, eico.value ''COMPETITOR_ID''
		, einco.value ''COMPETITOR''
		, CV.EFFECTIVE_DATE
		--, PRICE_MULTIPLE
		, Case Price_Multiple when ''1'' then ''$'' + CONVERT(varchar(12), Price, 1) else Cast(Price_Multiple as varchar(8)) + '' / '' + ''$'' + CONVERT(varchar(12), Price, 1) end RETAIL
		, Case CV.PRICE_TYPE when 1 then ''Regular'' when 2 then ''Special'' end ''Price_Type''
		, Dense_Rank()over(partition by CV.COMP_ENTITY_STRUCTURE_FK, PI.PRODUCT_STRUCTURE_FK, cv.PRICE_TYPE order by isnull(CV.EFFECTIVE_DATE, cast(getdate() as date)) desc) DRank
		, cast((select top 1 unit_value from marginmgr.pricesheet_basis_values with (nolock) where data_name_fk = 503201 and product_structure_fk = cv.product_structure_fk and effective_date <= cv.effective_date order by effective_date desc) as Numeric(18, 4)) ''unit_cost''
		, cast(price / isnull(price_multiple, 1) as Numeric(18, 4)) ''unit_price''
		from [epacube].[COMPETITOR_VALUES] cv with (nolock)
		--inner join [epacube].[getEntitiesByPermission](@User, 544111) A1 on cv.COMP_ENTITY_STRUCTURE_FK = a1.entity_structure_fk
		inner join epacube.entity_identification eico with (nolock) on cv.COMP_ENTITY_STRUCTURE_FK = eico.entity_structure_fk
		inner join epacube.product_identification pi with (nolock) on cv.product_structure_fk = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110100
		left join epacube.product_attribute pa with (nolock) on cv.product_structure_fk = pa.product_structure_fk and pa.data_name_fk = 500002
		left join epacube.entity_ident_nonunique einco with (nolock) on cv.COMP_ENTITY_STRUCTURE_FK = einco.ENTITY_STRUCTURE_FK
		left join epacube.epacube.product_description pd with (nolock) on pi.product_structure_fk = pd.PRODUCT_STRUCTURE_FK and pd.data_name_fk = 110401
		where '
		+ case	when @Product_Structure_fk is not null then 'cv.product_structure_fk = ' + @Product_Structure_Fk
				else 'cv.product_structure_fk in (select distinct product_structure_fk from epacube.PRODUCT_MULT_TYPE with (nolock) where data_name_fk = 500019 and DATA_VALUE_FK = ' + cast(@Alias_ID as varchar(16)) + ') ' end + '
		'+ 'and cv.COMP_ENTITY_STRUCTURE_FK in (' + @Comp_Entity_Structure_FK + ')
		' + ') A 
		' + 'where 1 = 1 ' + @Whr + '
		Order by competitor, ITEM_NO, EFFECTIVE_DATE desc, Price_Type'
		
		Exec(@SQL)
	End
