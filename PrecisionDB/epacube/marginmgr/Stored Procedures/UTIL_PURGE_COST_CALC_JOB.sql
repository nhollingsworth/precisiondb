﻿







-- Copyright 2012
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: To remove old Cost Calc Jobs
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        11/04/2012   Initial SQL Version
--

CREATE PROCEDURE [marginmgr].[UTIL_PURGE_COST_CALC_JOB] 
        @IN_JOB_FK INT
AS
BEGIN

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int

BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of marginmgr.UTIL_PURGE_COST_CALC_JOB.'
					 + cast(isnull(@IN_JOB_FK, 0) as varchar(30))

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;

SET @l_sysdate = GETDATE()


----- DELETE EVENT_DATA tables if exists


DELETE 
FROM synchronizer.EVENT_DATA_ERRORS
WHERE EVENT_FK IN (
          SELECT ED.EVENT_ID
          FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
          WHERE ED.IMPORT_JOB_FK = @IN_JOB_FK  )


DELETE 
FROM synchronizer.EVENT_DATA_RULES
WHERE EVENT_FK IN (
          SELECT ED.EVENT_ID
          FROM synchronizer.EVENT_DATA ED WITH (NOLOCK) 
          WHERE ED.IMPORT_JOB_FK = @IN_JOB_FK  )

DELETE 
FROM synchronizer.EVENT_DATA
WHERE IMPORT_JOB_FK = @IN_JOB_FK


-----  DELETE EVENT_CALC tables

DELETE 
FROM synchronizer.EVENT_CALC_ERRORS
WHERE EVENT_FK IN (
          SELECT EC.EVENT_ID
          FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
          WHERE EC.JOB_FK = @IN_JOB_FK  )


DELETE 
FROM synchronizer.EVENT_CALC_RULES_BASIS
WHERE  EVENT_RULES_FK IN (
             SELECT ECR.EVENT_RULES_ID
             FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
             INNER JOIN synchronizer.EVENT_CALC_RULES ECR WITH (NOLOCK) 
             ON ( ECR.EVENT_FK = EC.EVENT_ID )
             WHERE EC.JOB_FK = @IN_JOB_FK  )


DELETE 
FROM synchronizer.EVENT_CALC_RULES
WHERE EVENT_FK IN (
          SELECT EC.EVENT_ID
          FROM synchronizer.EVENT_CALC EC WITH (NOLOCK) 
          WHERE EC.JOB_FK = @IN_JOB_FK  )

DELETE 
FROM synchronizer.EVENT_CALC
WHERE JOB_FK = @IN_JOB_FK



DELETE 
FROM common.JOB_ERRORS
WHERE JOB_FK in ( select job_id from common.job where JOB_ID = @IN_JOB_FK )



DELETE 
FROM common.JOB_EXECUTION
WHERE JOB_FK in ( select job_id from common.job where JOB_ID = @IN_JOB_FK )



DELETE 
FROM common.JOB
WHERE JOB_ID = @IN_JOB_FK 


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of marginmgr.UTIL_PURGE_COST_CALC_JOB'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of marginmgr.UTIL_PURGE_COST_CALC_JOB has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END









