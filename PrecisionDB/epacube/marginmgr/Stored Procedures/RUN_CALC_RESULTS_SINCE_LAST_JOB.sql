﻿






-- Copyright 2010
--
-- Procedure created by Kathi Scott
--
--
-- Purpose: If @in_analysis_job_fk is NULL then NEW
--			Else re-running an existing Calc Job
--			Call Procedure on Service Broker
--

--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- KS        08/01/2010   Initial SQL Version
-- KS		 02/16/2011   Job name was changed from 'SINCE LAST JOB' to 'RESULTS SINCE LAST JOB'
--							Causing the job to start at the beginning and recalc everything.
--							Not sure when this occurred because it was certified working prior to release
-- KS        04/19/2011   Updated timestamp at the very end to not pick up calced change rows again.
-- CV        09/14/2011   Added inner join to rules hierarchy
-- KS        05/04/2011   Changed Related Sheet Result Trigger to compare parent dns not result data names
--                        and lay groundwork for Dynamic Calculation Engine changes
-- KS        05/22/2012   JASCO Added logic to include all Products if Parity Calculation is triggered
-- KS        05/25/2012   SUP-487 Changed logic to look at Timestamp on Event_Data_History instead of Eff_date
-- KS        11/07/2012   SUP-654 Added logic to look for Pending Whse Associations when creating Events to Calc
-- CV        09/04/2013   EPA-3466 looking at the rules timestamp-updatetime stamp
-- CV	     01/17/2013   EPA - 3425 Crumb trail not working because related event fk was null.
-- CV        08/14/2014   Adding the last job date as paramater.. found could be wrong if another calc job 
--							gets in que before this one runs from service broker.
-- CV        07/28/2015   Adding to use the Archive period if the last job can not be found.


CREATE PROCEDURE [marginmgr].[RUN_CALC_RESULTS_SINCE_LAST_JOB] 
       ( @in_analysis_job_fk BIGINT = -999 ,  @v_last_job_fk  bigint  )
	   
	   --bigint, @v_last_job_date  datetime)

AS
BEGIN

DECLARE @l_sysdate    datetime
DECLARE @ls_stmt      varchar (1000)
DECLARE @ls_queue     nvarchar (4000)
DECLARE @l_exec_no    bigint
DECLARE @l_rows_processed    BIGINT

DECLARE @v_job_fk         bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_data3      varchar(32)
DECLARE @v_data4      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
--DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime
DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT
DECLARE @v_result_type       VARCHAR(16)

DECLARE @status_desc  varchar (max)
DECLARE @ErrorMessage		 nvarchar(4000)
DECLARE @ErrorSeverity		 int
DECLARE @ErrorState			 int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()



SET @ls_stmt = 'started execution of MARGINMGR.RUN_CALC_RESULTS_SINCE_LAST_JOB' 
				+ ' Job = ' + cast( isnull ( @in_analysis_job_fk, -999 ) as varchar(30))
								
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;

IF  isnull ( @in_analysis_job_fk, -999 ) = -999
BEGIN
	SET @status_desc = 'MARGINMGR.RUN_CALC_RESULTS_SINCE_LAST_JOB:   No Job_FK was passed to procedure '
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
END
ELSE
BEGIN

----SET @v_last_job_fk =  ( SELECT MAX ( JOB_ID )
----						 FROM COMMON.JOB WITH (NOLOCK)
----						 WHERE JOB_CLASS_FK = 400        --- RESULT CALCULATIONS
----						 AND   NAME = 'RESULTS SINCE LAST JOB'
----                         AND   ( @in_analysis_job_fk IS NULL
----                                 OR JOB_ID <> @in_analysis_job_fk ) )

						 
--SET @v_last_job_date = 	ISNULL (
--								( SELECT ISNULL ( JOB_COMPLETE_TIMESTAMP, JOB_CREATE_TIMESTAMP )  -- KS SWITCH FROM CREATE TO COMPLETE TS
--								  FROM COMMON.JOB 
--								  WHERE JOB_ID = @v_last_job_fk )
--                                ,'01/01/2000' )

								
			SET @v_last_job_date =	ISNULL (
								( SELECT ISNULL ( JOB_COMPLETE_TIMESTAMP, JOB_CREATE_TIMESTAMP )  -- KS SWITCH FROM CREATE TO COMPLETE TS
								  FROM COMMON.JOB 
								  WHERE JOB_ID = @v_last_job_fk), cast(getdate() - (select cast(value as int)  from epacube.EPACUBE_PARAMS where name = 'ARCHIVE_PERIOD') as date))

-------------------------------------------------------------------------------------
--  DELETE PREVIOUS JOB ROWS
-------------------------------------------------------------------------------------


	----------------  EVENT DATA
	DELETE 
	FROM [synchronizer].[EVENT_DATA_RULES]
	WHERE EVENT_FK IN (
			SELECT EVENT_ID
			FROM [synchronizer].[EVENT_DATA]  WITH (NOLOCK)
			WHERE IMPORT_JOB_FK = @in_analysis_job_fk   )


	DELETE 
	FROM [synchronizer].[EVENT_DATA_ERRORS]
	WHERE EVENT_FK IN (
			SELECT EVENT_ID
			FROM [synchronizer].[EVENT_DATA]  WITH (NOLOCK)
			WHERE IMPORT_JOB_FK = @in_analysis_job_fk   )

	DELETE 
	FROM [synchronizer].[EVENT_DATA]
	WHERE IMPORT_JOB_FK = @in_analysis_job_fk 

	DELETE 
	FROM [synchronizer].[EVENT_DATA_HISTORY]
	WHERE IMPORT_JOB_FK = @in_analysis_job_fk 

   

    ---------------- EVENT CALC
	DELETE 
	FROM [synchronizer].[EVENT_CALC_RULES_BASIS]
	WHERE EVENT_RULES_FK IN (
			SELECT EVENT_RULES_ID
			FROM [synchronizer].[EVENT_CALC_RULES] ECR WITH (NOLOCK)
			INNER JOIN [synchronizer].[EVENT_CALC] EC  WITH (NOLOCK)
			 ON ( ECR.EVENT_FK = EC.EVENT_ID )
			WHERE JOB_FK = @in_analysis_job_fk   )


	DELETE 
	FROM [synchronizer].[EVENT_CALC_RULES]
	WHERE EVENT_FK IN (
			SELECT EVENT_ID
			FROM [synchronizer].[EVENT_CALC]  WITH (NOLOCK)
			WHERE JOB_FK = @in_analysis_job_fk   )


	DELETE 
	FROM [synchronizer].[EVENT_CALC_ERRORS]
	WHERE EVENT_FK IN (
			SELECT EVENT_ID
			FROM [synchronizer].[EVENT_CALC]  WITH (NOLOCK)
			WHERE JOB_FK = @in_analysis_job_fk   )

	    
	DELETE 
	FROM [synchronizer].[EVENT_CALC]
	WHERE JOB_FK = @in_analysis_job_fk 


    EXEC common.job_execution_create   @in_analysis_job_fk, 'START CALCULATIONS:: ',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
   		
------------------------------------------------------------------------------------------
--   Create Temp Table for Events that trigger Default and Derivation Rules
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_CALC_TRIGGER_EVENTS') is not null
	   drop table #TS_RULE_CALC_TRIGGER_EVENTS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_RULE_CALC_TRIGGER_EVENTS(
	    TS_RULE_CALC_TRIGGER_EVENTS_ID             BIGINT IDENTITY(1000,1) NOT NULL,
		RESULT_PARENT_DATA_NAME_FK  INT NULL,						    
		RESULT_DATA_NAME_FK			INT NULL,	
	    PRODUCT_STRUCTURE_FK		BIGINT NULL,
		END_DATE_CURRENT			date NULL,
	    DRANK						SMALLINT
		)



--------------------------------------------------------------------------------------------
--  CREATE INDEXES FOR DRANK OF #TS_RULE_CALC_TRIGGER_EVENTS 
--------------------------------------------------------------------------------------------

	CREATE CLUSTERED INDEX IDX_TSRDTE_IEID ON #TS_RULE_CALC_TRIGGER_EVENTS 
	(TS_RULE_CALC_TRIGGER_EVENTS_ID ASC)	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)

	CREATE NONCLUSTERED INDEX [IDX_TSRDTE_RPOE_IEID] ON #TS_RULE_CALC_TRIGGER_EVENTS 
	(RESULT_DATA_NAME_FK ASC
	,PRODUCT_STRUCTURE_FK ASC
	)
	INCLUDE ( TS_RULE_CALC_TRIGGER_EVENTS_ID, RESULT_PARENT_DATA_NAME_FK )	
	WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF)


--------------------------------------------------------------------------------------------
--   INSERT INTO TEMP TABLE
--      Filter Data Name changes that may effect Calculation Rules
--------------------------------------------------------------------------------------------

	INSERT INTO #TS_RULE_CALC_TRIGGER_EVENTS(
			 RESULT_DATA_NAME_FK	
			,RESULT_PARENT_DATA_NAME_FK				 			 
			,PRODUCT_STRUCTURE_FK 
			,END_DATE_CURRENT  
			 )
	SELECT DISTINCT
		 VRDN.RESULT_DATA_NAME_FK    
	    ,DN.PARENT_DATA_NAME_FK				 		 
		,ED.PRODUCT_STRUCTURE_FK  
		,ED.END_DATE_CUR 
---------------------------	
FROM  synchronizer.EVENT_DATA_HISTORY ED WITH (NOLOCK)
INNER JOIN  synchronizer.V_RULE_RELATED_DATA_NAMES VRDN
 ON ( VRDN.RULE_TYPE_CR_FK = 311
 AND  VRDN.RELATED_DATA_NAME_FK  = ED.DATA_NAME_FK )
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
 ON ( DN.DATA_NAME_ID = VRDN.RESULT_DATA_NAME_FK )
--------------    
WHERE 1 = 1
AND   ED.EVENT_STATUS_CR_FK IN ( 81,83 )                        --- STATUS APPROVED
------AND   ED.EVENT_EFFECTIVE_DATE > @v_last_job_date
AND   ED.UPDATE_TIMESTAMP > @v_last_job_date
AND   ED.EVENT_SOURCE_CR_FK <> 79  --- NOT FROM HOST
AND   ED.EVENT_ACTION_CR_FK <> 56  --- EVENT_ACTION	NO CHANGE

     

	INSERT INTO #TS_RULE_CALC_TRIGGER_EVENTS(
			 RESULT_DATA_NAME_FK	
			,RESULT_PARENT_DATA_NAME_FK				 			 
			,PRODUCT_STRUCTURE_FK  
			,END_DATE_CURRENT 
			 )
	SELECT DISTINCT
		 VRDN.RESULT_DATA_NAME_FK    
	    ,DN.PARENT_DATA_NAME_FK				 		 
		,ED.PRODUCT_STRUCTURE_FK  
		,ED.END_DATE_CUR 
---------------------------		
FROM  synchronizer.EVENT_DATA ED WITH (NOLOCK)
INNER JOIN  synchronizer.V_RULE_RELATED_DATA_NAMES VRDN
 ON ( VRDN.RULE_TYPE_CR_FK = 311
 AND  VRDN.RELATED_DATA_NAME_FK  = ED.DATA_NAME_FK )
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
 ON ( DN.DATA_NAME_ID = VRDN.RESULT_DATA_NAME_FK ) 
--------------
WHERE 1 = 1
AND   ED.RESULT_TYPE_CR_FK in ( 711, 712 )  --- add this in
AND   ED.EVENT_STATUS_CR_FK IN ( 84 )       --- KS CHANGED IN V516 TO ONLY FUTURE APPROVED 
AND   ED.EVENT_EFFECTIVE_DATE > @v_last_job_date
AND   ED.EVENT_SOURCE_CR_FK <> 79  --- NOT FROM HOST
AND   ED.EVENT_ACTION_CR_FK <> 56  --- EVENT_ACTION	NO CHANGE



--------------------------------------------------------------------------------------------
--   INSERT INTO TEMP TABLE
--      Added The evaluation of Pending Prod/Whse Association Events
--        Because of a Catch-22 situation on Assoc and required Results
--------------------------------------------------------------------------------------------

	INSERT INTO #TS_RULE_CALC_TRIGGER_EVENTS(
			 RESULT_DATA_NAME_FK	
			,RESULT_PARENT_DATA_NAME_FK				 			 
			,PRODUCT_STRUCTURE_FK 
			,END_DATE_CURRENT  
			 )
	SELECT DISTINCT
		 VRDN.RESULT_DATA_NAME_FK    
	    ,DN.PARENT_DATA_NAME_FK				 		 
		,ED.PRODUCT_STRUCTURE_FK 
		,ED.END_DATE_CUR  
---------------------------		
FROM  synchronizer.EVENT_DATA ED WITH (NOLOCK)
INNER JOIN  synchronizer.V_RULE_RELATED_DATA_NAMES VRDN
 ON ( VRDN.RULE_TYPE_CR_FK = 311
 AND  VRDN.RELATED_DATA_NAME_FK  = ED.DATA_NAME_FK )
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
 ON ( DN.DATA_NAME_ID = VRDN.RESULT_DATA_NAME_FK ) 
--------------
WHERE 1 = 1
AND   ED.EVENT_STATUS_CR_FK IN ( 80, 86,87, 88 )       --- pending events
AND   ED.DATA_NAME_FK = 159100  ---  PRODUCT WHSE
AND   ED.EVENT_EFFECTIVE_DATE > @v_last_job_date
AND   ED.EVENT_SOURCE_CR_FK <> 79  --- NOT FROM HOST
AND   ED.EVENT_ACTION_CR_FK <> 56  --- EVENT_ACTION	NO CHANGE



--------------------------------------------------------------------------------------------
--   INSERT INTO TEMP TABLE
--      BASIS changes that may effect Calculation Rules
--------------------------------------------------------------------------------------------

	INSERT INTO #TS_RULE_CALC_TRIGGER_EVENTS(
			 RESULT_DATA_NAME_FK	
			,RESULT_PARENT_DATA_NAME_FK				 			 
			,PRODUCT_STRUCTURE_FK  
			,END_DATE_CURRENT 
			 )
	SELECT DISTINCT
		 VRDN.RESULT_DATA_NAME_FK  
	    ,DN.PARENT_DATA_NAME_FK				 		 
		,ED.PRODUCT_STRUCTURE_FK  
		,ED.END_DATE_CUR 
---------------------------	
FROM  synchronizer.EVENT_DATA_HISTORY ED WITH (NOLOCK)
INNER JOIN  synchronizer.V_RULE_RELATED_DATA_NAMES VRDN
 ON ( VRDN.RULE_TYPE_CR_FK = 311
 AND  VRDN.RELATED_PARENT_DATA_NAME_FK  = ED.DATA_NAME_FK )
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
 ON ( DN.DATA_NAME_ID = VRDN.RESULT_DATA_NAME_FK ) 
--------------    
WHERE 1 = 1
AND   ED.EVENT_STATUS_CR_FK IN ( 81,83 )                        --- STATUS APPROVED
------AND   ED.EVENT_EFFECTIVE_DATE > @v_last_job_date
AND   ED.UPDATE_TIMESTAMP > @v_last_job_date
AND   ED.EVENT_SOURCE_CR_FK <> 79  --- NOT FROM HOST
AND   ED.EVENT_ACTION_CR_FK <> 56  --- EVENT_ACTION	NO CHANGE




	INSERT INTO #TS_RULE_CALC_TRIGGER_EVENTS(
			 RESULT_DATA_NAME_FK	
			,RESULT_PARENT_DATA_NAME_FK				 
			,PRODUCT_STRUCTURE_FK  
			,END_DATE_CURRENT 
			 )
	SELECT DISTINCT
		 VRDN.RESULT_DATA_NAME_FK 
		,DN.PARENT_DATA_NAME_FK   
		,ED.PRODUCT_STRUCTURE_FK  
		,ED.END_DATE_CUR 
---------------------------		
FROM  synchronizer.EVENT_DATA ED WITH (NOLOCK)
INNER JOIN  synchronizer.V_RULE_RELATED_DATA_NAMES VRDN
 ON ( VRDN.RULE_TYPE_CR_FK = 311
 AND   VRDN.RELATED_PARENT_DATA_NAME_FK  = ED.DATA_NAME_FK )
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
 ON ( DN.DATA_NAME_ID = VRDN.RESULT_DATA_NAME_FK ) 
--------------
WHERE 1 = 1
AND   ED.RESULT_TYPE_CR_FK in ( 711, 712 )  --- add this in
AND   ED.EVENT_STATUS_CR_FK IN ( 84 )       --- ks V516 change to only a approved future status
AND   ED.EVENT_EFFECTIVE_DATE > @v_last_job_date
AND   ED.EVENT_SOURCE_CR_FK <> 79  --- NOT FROM HOST
AND   ED.EVENT_ACTION_CR_FK <> 56  --- EVENT_ACTION	NO CHANGE


 
--------------------------------------------------------------------------------------------
--   Find #TS_RULE_CALC_TRIGGER_EVENTS that are related to RESULT_DATE_NAMES
--      05/17/2012 Changed Logic to be a Loop to Capture Multiple Levels
--------------------------------------------------------------------------------------------

SET @v_count = 1

WHILE @v_count > 0 
BEGIN

	INSERT INTO #TS_RULE_CALC_TRIGGER_EVENTS(
			 RESULT_DATA_NAME_FK	
			,RESULT_PARENT_DATA_NAME_FK				 
			,PRODUCT_STRUCTURE_FK  
			,END_DATE_CURRENT 
			 )
	SELECT DISTINCT
		 R.RESULT_DATA_NAME_FK 
		,DN.PARENT_DATA_NAME_FK   
		,TSRCTE.PRODUCT_STRUCTURE_FK 
		,NULL 
---------------------------		
FROM synchronizer.RULES R WITH (NOLOCK)
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID 
	AND  RS.RULE_TYPE_CR_FK IN ( 311  ) )  -- RESULT CALCULATIONS
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON DS.DATA_SET_ID = DN.DATA_SET_FK
INNER JOIN synchronizer.V_RULE_RELATED_DATA_NAMES VRDN
    ON ( R.RESULT_DATA_NAME_FK = VRDN.RESULT_DATA_NAME_FK 
        AND  VRDN.RULE_TYPE_CR_FK = RS.RULE_TYPE_CR_FK  ) 
INNER JOIN #TS_RULE_CALC_TRIGGER_EVENTS TSRCTE
  ON  ( TSRCTE.RESULT_PARENT_DATA_NAME_FK = VRDN.RELATED_PARENT_DATA_NAME_FK )  
--------------
WHERE 1 = 1
-----------
AND   R.RECORD_STATUS_CR_FK = 1
AND   DS.EVENT_TYPE_CR_FK = 152   --- RESULTS ONLY 
AND   R.RESULT_DATA_NAME_FK  NOT IN 
       ( SELECT TSRCTEX.RESULT_DATA_NAME_FK 
         FROM  #TS_RULE_CALC_TRIGGER_EVENTS TSRCTEX
         WHERE TSRCTEX.PRODUCT_STRUCTURE_FK = TSRCTE.PRODUCT_STRUCTURE_FK )

---- CHECK TO SEE IF ANY MORE RELATED RESULT DATA NAMES IN THE LOOP LOGIC
SET @v_count = 
	(	SELECT COUNT(1)
		FROM synchronizer.RULES R WITH (NOLOCK)
		INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
			ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID 
			AND  RS.RULE_TYPE_CR_FK IN ( 311  ) )  -- RESULT CALCULATIONS
		INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK
		INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON DS.DATA_SET_ID = DN.DATA_SET_FK
		INNER JOIN synchronizer.V_RULE_RELATED_DATA_NAMES VRDN
			ON ( R.RESULT_DATA_NAME_FK = VRDN.RESULT_DATA_NAME_FK 
				AND  VRDN.RULE_TYPE_CR_FK = RS.RULE_TYPE_CR_FK  ) 
		INNER JOIN #TS_RULE_CALC_TRIGGER_EVENTS TSRCTE
		  ON  ( TSRCTE.RESULT_PARENT_DATA_NAME_FK = VRDN.RELATED_PARENT_DATA_NAME_FK )  
		--------------
		WHERE 1 = 1
		-----------
		AND   R.RECORD_STATUS_CR_FK = 1
		AND   DS.EVENT_TYPE_CR_FK = 152   --- RESULTS ONLY 
		AND   R.RESULT_DATA_NAME_FK  NOT IN 
			   ( SELECT TSRCTEX.RESULT_DATA_NAME_FK 
				 FROM  #TS_RULE_CALC_TRIGGER_EVENTS TSRCTEX
				 WHERE TSRCTEX.PRODUCT_STRUCTURE_FK = TSRCTE.PRODUCT_STRUCTURE_FK )
        )
 
 END   --- END OF @V_COUNT LOOK.... NO MORE RELATED BASIS EVENTS


 
--------------------------------------------------------------------------------------------
--   INSERT INTO TEMP TABLE
--      Rule additions or changes that may effect Calculation Rules
--------------------------------------------------------------------------------------------
--PROD FILTER

	INSERT INTO #TS_RULE_CALC_TRIGGER_EVENTS(
			 RESULT_DATA_NAME_FK	
			,RESULT_PARENT_DATA_NAME_FK				 			 
			,PRODUCT_STRUCTURE_FK  
			,END_DATE_CURRENT 
			 )
	SELECT DISTINCT
		 R.RESULT_DATA_NAME_FK  
	    ,DN.PARENT_DATA_NAME_FK				 		 
		,VRFP.PRODUCT_STRUCTURE_FK   
		,NULL
---------------------------	
FROM synchronizer.RULES R
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID 
	AND  RS.RULE_TYPE_CR_FK IN ( 311  ) )  -- RESULT CALCULATIONS
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON DS.DATA_SET_ID = DN.DATA_SET_FK
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( R.RULES_ID = RFS.RULES_FK )
INNER JOIN synchronizer.V_RULE_FILTERS_PROD  VRFP 
  ON ( VRFP.RULES_FILTER_FK = RFS.PROD_FILTER_FK )
WHERE 1 = 1
-----------
AND   DS.EVENT_TYPE_CR_FK = 152   --- RESULTS ONLY 
--AND   R.RECORD_STATUS_CR_FK = 1
--AND   R.UPDATE_TIMESTAMP <> R.CREATE_TIMESTAMP
AND   R.UPDATE_TIMESTAMP > @v_last_job_date 


--SUPL FILTER

INSERT INTO #TS_RULE_CALC_TRIGGER_EVENTS(
		 RESULT_DATA_NAME_FK	
		,RESULT_PARENT_DATA_NAME_FK				 			 
		,PRODUCT_STRUCTURE_FK   
		 )
SELECT DISTINCT
		 R.RESULT_DATA_NAME_FK  
	    ,DN.PARENT_DATA_NAME_FK				 		 
		,pa.PRODUCT_STRUCTURE_FK
---------------------------	
FROM synchronizer.RULES R
INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID 
	AND  RS.RULE_TYPE_CR_FK IN ( 311  ) )  -- RESULT CALCULATIONS
INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK
INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON DS.DATA_SET_ID = DN.DATA_SET_FK
INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
  ON ( R.RULES_ID = RFS.RULES_FK )

 INNER JOIN synchronizer.V_RULE_FILTERS_ENTITY VRFE
  ON ( VRFE.RULES_FILTER_FK = RFS.SUPL_FILTER_FK )
 INNER JOIN  epacube.PRODUCT_ASSOCIATION pa
  on (VRFE.ENTITY_STRUCTURE_FK = pa.ENTITY_STRUCTURE_FK
  and pa.DATA_NAME_FK = 159300)
WHERE 1 = 1
-----------
AND   DS.EVENT_TYPE_CR_FK = 152   --- RESULTS ONLY 
--AND   R.RECORD_STATUS_CR_FK = 1
AND   R.UPDATE_TIMESTAMP > @v_last_job_date 


--------------------------------------------------------------------------------------------
--   INSERT INTO TEMP TABLE
--      Rule inactivations that may effect Calculation Rules
--------------------------------------------------------------------------------------------
-----doing this in the code above for supplier filter and prod filter

------	INSERT INTO #TS_RULE_CALC_TRIGGER_EVENTS(
------			 RESULT_DATA_NAME_FK	
------			,RESULT_PARENT_DATA_NAME_FK				 			 
------			,PRODUCT_STRUCTURE_FK   
------			 )
------	SELECT DISTINCT
------		 R.RESULT_DATA_NAME_FK  
------	    ,DN.PARENT_DATA_NAME_FK				 		 
------		,VRFP.PRODUCT_STRUCTURE_FK   
---------------------------------	
------FROM synchronizer.RULES R
------INNER JOIN synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
------	ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID 
------	AND  RS.RULE_TYPE_CR_FK IN ( 311  ) )  -- RESULT CALCULATIONS
------INNER JOIN EPACUBE.DATA_NAME DN WITH (NOLOCK) ON DN.DATA_NAME_ID = R.RESULT_DATA_NAME_FK
------INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK) ON DS.DATA_SET_ID = DN.DATA_SET_FK
------INNER JOIN synchronizer.RULES_FILTER_SET RFS WITH (NOLOCK)
------  ON ( R.RULES_ID = RFS.RULES_FK )
------INNER JOIN synchronizer.V_RULE_FILTERS_PROD  VRFP 
------  ON ( VRFP.RULES_FILTER_FK = RFS.PROD_FILTER_FK )
------WHERE 1 = 1
-----------------
------AND   DS.EVENT_TYPE_CR_FK = 152   --- RESULTS ONLY 
------AND   R.RECORD_STATUS_CR_FK = 2   --- INACTIVE RULES ONLY
--------AND   R.UPDATE_TIMESTAMP <> R.CREATE_TIMESTAMP
------AND   R.UPDATE_TIMESTAMP > @v_last_job_date 



 
--------------------------------------------------------------------------------------------
--   If one of the trigger events requires a Parity Check  EPA-3326
--      Need to include Every product that shares the same Parity Code
--------------------------------------------------------------------------------------------

INSERT INTO #TS_RULE_CALC_TRIGGER_EVENTS(
		 RESULT_DATA_NAME_FK	
		,RESULT_PARENT_DATA_NAME_FK				 			 
		,PRODUCT_STRUCTURE_FK   
		 )
SELECT DISTINCT
	 TSRCTE.RESULT_DATA_NAME_FK  
    ,TSRCTE.RESULT_PARENT_DATA_NAME_FK    ------	CINDY ADDED  the word result to parent data name			 		 
	,PCX.PRODUCT_STRUCTURE_FK   
FROM #TS_RULE_CALC_TRIGGER_EVENTS TSRCTE
INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK) 
 ON RH.RECORD_STATUS_CR_FK = 1
 AND RH.RESULT_DATA_NAME_FK = TSRCTE.RESULT_DATA_NAME_FK
INNER JOIN epacube.PRODUCT_CATEGORY PC WITH (NOLOCK)
 ON PC.PRODUCT_STRUCTURE_FK = TSRCTE.PRODUCT_STRUCTURE_FK
 AND PC.DATA_NAME_FK = RH.PARITY_CHECK_DN_FK 
INNER JOIN epacube.PRODUCT_CATEGORY PCX WITH (NOLOCK)    --- ADD IN ALL PRODUCTS WITH SAME PARITY CODE
 ON PCX.DATA_VALUE_FK = PC.DATA_VALUE_FK                 
 AND PCX.PRODUCT_STRUCTURE_FK <> PC.PRODUCT_STRUCTURE_FK 






--------------------------------------------------------------------------------------------
--   Check if any CALCULATIONS qualified
--------------------------------------------------------------------------------------------

SET @l_rows_processed = ( SELECT COUNT(1) FROM #TS_RULE_CALC_TRIGGER_EVENTS )
	   
IF @l_rows_processed = 0
BEGIN
	SET @status_desc = 'No Result Calculations to be Processed '
	EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc
END
ELSE 
BEGIN  

--------------------------------------------------------------------------------------------
--   RANK #TS_RULE_CALC_TRIGGER_EVENTS
--      SET OLDEST EFFECTIVE DATE EVENT DRANK = 1
--		FOR UNIQUE RESULT DATA NAME FK, PRODUCT/ORG/
--------------------------------------------------------------------------------------------

	UPDATE #TS_RULE_CALC_TRIGGER_EVENTS
	SET DRANK = 1
	WHERE TS_RULE_CALC_TRIGGER_EVENTS_ID IN (
		SELECT A.TS_RULE_CALC_TRIGGER_EVENTS_ID
		FROM (
				SELECT
				TS_RULE_CALC_TRIGGER_EVENTS_ID,
				( DENSE_RANK() OVER ( PARTITION BY RESULT_DATA_NAME_FK
												  ,PRODUCT_STRUCTURE_FK
							   ORDER BY TS_RULE_CALC_TRIGGER_EVENTS_ID DESC ) )AS DRANK
				FROM #TS_RULE_CALC_TRIGGER_EVENTS
				WHERE 1 = 1
			) A
			WHERE A.DRANK = 1
	  )

   DELETE 
   FROM #TS_RULE_CALC_TRIGGER_EVENTS
   WHERE ISNULL ( DRANK, 0 ) <> 1
   
		
------------------------------------------------------------------------------------------
--   Create Temp Table for Parent Events EVENT_DATA 
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_PARENT_TRIGGER_EVENTS') is not null
	   drop table #TS_RULE_PARENT_TRIGGER_EVENTS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_RULE_PARENT_TRIGGER_EVENTS(
	    TS_RULE_PARENT_TRIGGER_EVENTS_ID             BIGINT IDENTITY(1000,1) NOT NULL,
		RESULT_PARENT_DATA_NAME_FK  INT NULL,						    
	    PRODUCT_STRUCTURE_FK		BIGINT NULL,
	    ORG_ENTITY_STRUCTURE_FK		BIGINT NULL,
		)


---- CORP_IND = 1
	INSERT INTO #TS_RULE_PARENT_TRIGGER_EVENTS (
		RESULT_PARENT_DATA_NAME_FK,
		PRODUCT_STRUCTURE_FK,
		ORG_ENTITY_STRUCTURE_FK )
	SELECT DISTINCT 
		TSRCTE.RESULT_PARENT_DATA_NAME_FK,
		TSRCTE.PRODUCT_STRUCTURE_FK,
		1  ---- ORG_ENTITY_STRUCTURE_FK 
	FROM #TS_RULE_CALC_TRIGGER_EVENTS TSRCTE
	INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
	 ON ( DN.DATA_NAME_ID = TSRCTE.RESULT_PARENT_DATA_NAME_FK )
    INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )	 	
	WHERE 1 = 1
	AND   DS.CORP_IND = 1



---- ORG_IND = 1
	INSERT INTO #TS_RULE_PARENT_TRIGGER_EVENTS (
		RESULT_PARENT_DATA_NAME_FK,
		PRODUCT_STRUCTURE_FK,
		ORG_ENTITY_STRUCTURE_FK )
	SELECT DISTINCT 
		TSRCTE.RESULT_PARENT_DATA_NAME_FK,
		PAS.PRODUCT_STRUCTURE_FK,
		PAS.ORG_ENTITY_STRUCTURE_FK 
	FROM #TS_RULE_CALC_TRIGGER_EVENTS TSRCTE
	INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
	 ON ( DN.DATA_NAME_ID = TSRCTE.RESULT_PARENT_DATA_NAME_FK )
    INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )	 	
     INNER JOIN EPACUBE.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
     ON ( PAS.DATA_NAME_FK = 159100  ---  PRODUCT WHSE
     AND  PAS.PRODUCT_STRUCTURE_FK = TSRCTE.PRODUCT_STRUCTURE_FK  )
	WHERE 1 = 1
	AND   DS.ORG_IND = 1



---- ORG_IND = 1  and need to look for future Prod/Whse too Relationships
	INSERT INTO #TS_RULE_PARENT_TRIGGER_EVENTS (
		RESULT_PARENT_DATA_NAME_FK,
		PRODUCT_STRUCTURE_FK,
		ORG_ENTITY_STRUCTURE_FK )
	SELECT DISTINCT 
		TSRCTE.RESULT_PARENT_DATA_NAME_FK,
		ED.PRODUCT_STRUCTURE_FK,
		ED.ORG_ENTITY_STRUCTURE_FK 
	FROM #TS_RULE_CALC_TRIGGER_EVENTS TSRCTE
	INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
	 ON ( DN.DATA_NAME_ID = TSRCTE.RESULT_PARENT_DATA_NAME_FK )
    INNER JOIN EPACUBE.DATA_SET DS WITH (NOLOCK)
     ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )	 	
     INNER JOIN synchronizer.EVENT_DATA ED WITH (NOLOCK)
     ON ( ED.DATA_NAME_FK = 159100  ---  PRODUCT WHSE
     AND  ED.PRODUCT_STRUCTURE_FK = TSRCTE.PRODUCT_STRUCTURE_FK
     AND  ED.EVENT_STATUS_CR_FK IN (80, 86, 87, 88 ) 
      )
	WHERE 1 = 1
	AND   DS.ORG_IND = 1




                         
--------------------------------------------------------------------------------------------
--   INSERT THE EVENT_CALC ROWS
--      One for each of the Results to be Calculated
--------------------------------------------------------------------------------------------


INSERT INTO [synchronizer].[EVENT_CALC]
           ([JOB_FK]
           ,[RESULT_DATA_NAME_FK]
           ,[RESULT_TYPE_CR_FK]
           ,[RESULT_EFFECTIVE_DATE]
		   ,[EFF_END_DATE]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ORG_ENTITY_STRUCTURE_FK]
           ,[CUST_ENTITY_STRUCTURE_FK]
----           ,[SUPL_ENTITY_STRUCTURE_FK]
----           ,[RESULT]
           ,[VALUE_UOM_CODE_FK]
           ,[EVENT_CONDITION_CR_FK]
           ,[CALC_GROUP_SEQ]
----           ,[RULES_FK]
           ,[RELATED_EVENT_FK]
           ,[RULE_TYPE_CR_FK]
           ,[RESULT_PARENT_DATA_NAME_FK]
           ,[COLUMN_NAME]
           ,[UPDATE_USER]
           ,[UPDATE_LOG_FK]
            )
	SELECT
            @in_analysis_job_fk
           ,TSRCTE.RESULT_DATA_NAME_FK
           ,710   -- RESULT_TYPE_CR_FK, int,>
           ,getdate ()  -- RESULT_EFFECTIVE_DATE, datetime,>
		   ,TSRCTE.END_DATE_CURRENT  --- END DATE
           ,TSRPTE.PRODUCT_STRUCTURE_FK
           ,TSRPTE.ORG_ENTITY_STRUCTURE_FK
           ,NULL  -- CUST_ENTITY_STRUCTURE_FK
------		   ,( SELECT PAS.ENTITY_STRUCTURE_FK
------		      FROM EPACUBE.PRODUCT_ASSOCIATION PAS WITH (NOLOCK)
------		      INNER JOIN EPACUBE.DATA_NAME DNX WITH (NOLOCK)
------		        ON ( DNX.DATA_NAME_ID = PAS.DATA_NAME_FK )   
------		      INNER JOIN EPACUBE.DATA_SET DSX WITH (NOLOCK)
------		        ON ( DSX.DATA_SET_iD = DNX.DATA_SET_FK )
------		      WHERE PAS.DATA_NAME_FK = RS.SUPL_ASSOC_DN_FK
------		      AND   PAS.PRODUCT_STRUCTURE_FK = TSRPTE.PRODUCT_STRUCTURE_FK
------		      AND   (    DSX.ORG_IND IS NULL
------                    OR   PAS.ORG_ENTITY_STRUCTURE_FK = TSRPTE.ORG_ENTITY_STRUCTURE_FK  )
------             )  AS SUPL_ENTITY_STRUCTURE_FK 
------           ,NULL  --  <RESULT, numeric,>
		   ,( SELECT TOP 1 PUC.UOM_CODE_FK
			  FROM epacube.PRODUCT_UOM_CLASS PUC WITH (NOLOCK)
			  WHERE PUC.DATA_NAME_FK = DNP.UOM_CLASS_DATA_NAME_FK
			  AND   PUC.PRODUCT_STRUCTURE_FK = TSRPTE.PRODUCT_STRUCTURE_FK
			  AND   ( PUC.ORG_ENTITY_STRUCTURE_FK = TSRPTE.ORG_ENTITY_STRUCTURE_FK 
					  OR PUC.ORG_ENTITY_STRUCTURE_FK = 1 ) 
			) AS VALUE_UOM_CODE_FK          
           ,97  --  <EVENT_CONDITION_CR_FK, int,>
           ,(  ( ISNULL ( RH.CALC_GROUP_SEQ, 0 ) )
             + ( ISNULL ( ECT.CALC_GROUP_SEQ_ADJUSTMENT, 0 ) ) ) AS CALC_GROUP_SEQ
           ----------,NULL        ----- RULES_FK
           ,NULL -----<RELATED_EVENT_FK, bigint,>
           ,( SELECT TOP 1 RS.RULE_TYPE_CR_FK
              FROM synchronizer.RULES_STRUCTURE RS WITH (NOLOCK)
              INNER JOIN synchronizer.RULES R WITH (NOLOCK)
                ON ( R.RULES_STRUCTURE_FK = RS.RULES_STRUCTURE_ID
                AND  R.RESULT_DATA_NAME_FK = TSRCTE.RESULT_DATA_NAME_FK
                AND  R.RECORD_STATUS_CR_FK = 1 ) 
               WHERE 1 = 1
               AND   RS.RULE_TYPE_CR_FK IN ( 311, 312, 313 ) )  -- RULE_TYPE
           ,DN.PARENT_DATA_NAME_FK
           ,DS.COLUMN_NAME
           ,'EPACUBE' --<UPDATE_USER, varchar(64),>
           ,TSRPTE.TS_RULE_PARENT_TRIGGER_EVENTS_ID    --- UPDATE_LOG_FK
-------------
FROM #TS_RULE_PARENT_TRIGGER_EVENTS TSRPTE 
INNER JOIN #TS_RULE_CALC_TRIGGER_EVENTS TSRCTE 
 ON ( TSRPTE.RESULT_PARENT_DATA_NAME_FK = TSRCTE.RESULT_PARENT_DATA_NAME_FK 
 AND  TSRPTE.PRODUCT_STRUCTURE_FK = TSRCTE.PRODUCT_STRUCTURE_FK )
INNER JOIN epacube.DATA_NAME DNP WITH (NOLOCK)
 ON ( DNP.DATA_NAME_ID = TSRCTE.RESULT_PARENT_DATA_NAME_FK ) 
INNER JOIN epacube.DATA_NAME DN WITH (NOLOCK)
 ON ( DN.DATA_NAME_ID = TSRCTE.RESULT_DATA_NAME_FK )
INNER JOIN epacube.DATA_SET DS WITH (NOLOCK)
 ON ( DS.DATA_SET_ID = DN.DATA_SET_FK )
 INNER JOIN synchronizer.RULES_HIERARCHY RH WITH (NOLOCK)  
 on ( TSRCTE.result_data_name_fk = RH.Result_data_name_fk
 AND  RH.RECORD_STATUS_CR_FK = 1 )
INNER JOIN epacube.ENTITY_STRUCTURE ES WITH (NOLOCK)
 ON ( ES.ENTITY_STRUCTURE_ID = TSRPTE.ORG_ENTITY_STRUCTURE_FK )
LEFT JOIN epacube.ENTITY_CLASS_TREE ECT WITH (NOLOCK)    ---- ADD CALC_GROUP_SEQ_ADJUSTMENT FOR ORG LEVEL ( IE CALC COPR FIRST )
 ON ( ECT.ENTITY_CLASS_CR_FK = ES.ENTITY_CLASS_CR_FK
 AND  ECT.DATA_NAME_FK  = ES.DATA_NAME_FK 
 AND  ECT.RECORD_STATUS_CR_FK = 1  )
WHERE 1 = 1

            

--drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_CALC_TRIGGER_EVENTS') is not null
	   drop table #TS_RULE_CALC_TRIGGER_EVENTS;


-------------------------------------------------------------------------------------
--  CALL CALCULATION JOB VIA SERVICE BROKER
-------------------------------------------------------------------------------------

EXEC marginmgr.CALCULATION_ENGINE  @in_analysis_job_fk


------------------------------------------------------------------------------------------
---  UPDATE SYNCHRONIZER.EVENT_DATA FROM EVENT_CALC
------------------------------------------------------------------------------------------

EXEC common.job_execution_create   @in_analysis_job_fk, 'POSTING RESULTS:: ',
	                                 @v_input_rows, @v_output_rows, @v_exception_rows,
									 200, @l_sysdate, @l_sysdate  

EXEC synchronizer.EVENT_POSTING_EVENT_CALC_RESULTS  @in_analysis_job_fk

------set related event FK if null  jira epa 3425
		
--		update synchronizer.EVENT_CALC
--         set RELATED_EVENT_FK = ed.event_id
--		 from synchronizer.EVENT_DATA ed 
--          inner join synchronizer.EVENT_CALC ec
--          on (ec.PRODUCT_STRUCTURE_FK = ed.PRODUCT_STRUCTURE_FK
--          and ec.ORG_ENTITY_STRUCTURE_FK = ed.ORG_ENTITY_STRUCTURE_FK
--          and ec.JOB_FK = ed.IMPORT_JOB_FK
--          and ed.IMPORT_JOB_FK = @in_analysis_job_fk)
--          --where RELATED_EVENT_FK is NULL
          
--          update synchronizer.EVENT_CALC
--         set RELATED_EVENT_FK = ed.event_id
--		 from synchronizer.EVENT_DATA_history ed 
--          inner join synchronizer.EVENT_CALC ec
--          on (ec.PRODUCT_STRUCTURE_FK = ed.PRODUCT_STRUCTURE_FK
--          and ec.ORG_ENTITY_STRUCTURE_FK = ed.ORG_ENTITY_STRUCTURE_FK
--          and ec.JOB_FK = ed.IMPORT_JOB_FK
--          and ed.IMPORT_JOB_FK = @in_analysis_job_fk)
          

------------------------------------------------------------------------------------------
--   Clear Event_Calc Tables this job
------------------------------------------------------------------------------------------

EXEC [marginmgr].[UTIL_PURGE_MARGIN_TABLES] @in_analysis_job_fk, NULL

END --- PROCESS CALCULATIONS

------------------------------------------------------------------------------------------
--   Updated the job timestamp to ensure that rows are not selected next time
------------------------------------------------------------------------------------------


UPDATE COMMON.JOB
SET JOB_COMPLETE_TIMESTAMP = GETDATE ()
WHERE JOB_ID = @in_analysis_job_fk



	
------------------------------------------------------------------------------------------
--   Drop all Temp Tables
------------------------------------------------------------------------------------------


--drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_CALC_TRIGGER_EVENTS') is not null
	   drop table #TS_RULE_CALC_TRIGGER_EVENTS;


--drop temp table if it exists
	IF object_id('tempdb..#TS_RULE_PARENT_TRIGGER_EVENTS') is not null
	   drop table #TS_RULE_PARENT_TRIGGER_EVENTS;
         


    EXEC common.job_execution_create   @in_analysis_job_fk, 'COMPLETED CALCULATIONS',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @l_sysdate, @l_sysdate  


END  ---- @in_analysis_job_fk <> -999


------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of MARGINMGR.RUN_CALC_RESULTS_SINCE_LAST_JOB'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of MARGINMGR.RUN_CALC_RESULTS_SINCE_LAST_JOB has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END






