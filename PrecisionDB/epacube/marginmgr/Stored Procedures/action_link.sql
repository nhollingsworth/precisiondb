﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [marginmgr].[action_link] @Product_Structure_FK bigint = null, @Cust_Entity_Structure_FK bigint = null, @Zone_Entity_Structure_FK bigint = null, @Action Varchar(16)
AS
BEGIN

	SET NOCOUNT ON;

If @Action = 'Add'
Insert into epacube.product_association 
(data_name_fk, Product_structure_fk, Org_Entity_Structure_FK, Entity_Class_CR_FK, Entity_Structure_fk, RECORD_STATUS_CR_FK)
Select 159450, @Product_Structure_FK, 1 Org_Entity_Structure_FK, 10117 Entity_Class_CR_FK, @Zone_Entity_Structure_FK, 1 Record_Status_CR_FK

else
If @Action = 'Remove'
Delete from epacube.product_association where data_name_fk = 159450 and product_structure_fk = @Product_Structure_FK and entity_structure_fk = @Zone_Entity_Structure_FK

END
