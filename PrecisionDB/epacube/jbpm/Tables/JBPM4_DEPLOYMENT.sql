﻿CREATE TABLE [jbpm].[JBPM4_DEPLOYMENT] (
    [DBID_]      NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [NAME_]      TEXT          NULL,
    [TIMESTAMP_] NUMERIC (19)  NULL,
    [STATE_]     VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([DBID_] ASC)
);

