﻿CREATE TABLE [jbpm].[JBPM4_HIST_PROCINST] (
    [DBID_]        NUMERIC (19)  NOT NULL,
    [DBVERSION_]   INT           NOT NULL,
    [ID_]          VARCHAR (255) NULL,
    [PROCDEFID_]   VARCHAR (255) NULL,
    [KEY_]         VARCHAR (255) NULL,
    [START_]       DATETIME      NULL,
    [END_]         DATETIME      NULL,
    [DURATION_]    NUMERIC (19)  NULL,
    [STATE_]       VARCHAR (255) NULL,
    [ENDACTIVITY_] VARCHAR (255) NULL,
    [NEXTIDX_]     INT           NULL,
    PRIMARY KEY CLUSTERED ([DBID_] ASC)
);

