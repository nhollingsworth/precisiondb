﻿CREATE TABLE [jbpm].[JBPM4_PARTICIPATION] (
    [DBID_]      NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [DBVERSION_] INT           NOT NULL,
    [GROUPID_]   VARCHAR (255) NULL,
    [USERID_]    VARCHAR (255) NULL,
    [TYPE_]      VARCHAR (255) NULL,
    [TASK_]      NUMERIC (19)  NULL,
    [SWIMLANE_]  NUMERIC (19)  NULL,
    PRIMARY KEY CLUSTERED ([DBID_] ASC),
    CONSTRAINT [FK_PART_SWIMLANE] FOREIGN KEY ([SWIMLANE_]) REFERENCES [jbpm].[JBPM4_SWIMLANE] ([DBID_]),
    CONSTRAINT [FK_PART_TASK] FOREIGN KEY ([TASK_]) REFERENCES [jbpm].[JBPM4_TASK] ([DBID_])
);


GO
CREATE NONCLUSTERED INDEX [IDX_PART_TASK]
    ON [jbpm].[JBPM4_PARTICIPATION]([TASK_] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

