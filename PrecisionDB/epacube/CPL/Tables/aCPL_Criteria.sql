﻿CREATE TABLE [CPL].[aCPL_Criteria] (
    [Identifier_FK]       BIGINT        NULL,
    [Data_Name_FK]        BIGINT        NULL,
    [Entity_Data_Name_FK] BIGINT        NULL,
    [Data_Name]           VARCHAR (64)  NOT NULL,
    [Entity_Class_CR_FK]  INT           NULL,
    [Exclusion]           VARCHAR (128) NULL,
    [Criteria_Value]      VARCHAR (128) NOT NULL,
    [Value_Description]   VARCHAR (128) NULL,
    [Activity]            VARCHAR (64)  NOT NULL,
    [Job_FK]              BIGINT        NULL,
    [aCPL_Criteria_ID]    BIGINT        NOT NULL
);

