﻿CREATE TABLE [CPL].[aCPL_Template_Data] (
    [Profiles_Job_FK]       INT           NOT NULL,
    [Data_Name]             VARCHAR (64)  NULL,
    [Data_Name_FK]          INT           NULL,
    [Entity_Data_Name_FK]   INT           NULL,
    [Value]                 VARCHAR (128) NULL,
    [Data_Type]             VARCHAR (32)  NULL,
    [Whse]                  VARCHAR (64)  NULL,
    [eMail_Recipients]      VARCHAR (MAX) NULL,
    [Valid]                 INT           NULL,
    [aCPL_Template_Data_ID] INT           NOT NULL,
    [intEMailSend]          INT           NULL,
    [ValueName]             VARCHAR (128) NULL
);

