﻿CREATE TABLE [CPL].[aCPL_Defaults_User] (
    [Process_Name]          VARCHAR (64)  NOT NULL,
    [Data_Name_FK]          BIGINT        NULL,
    [Data_Name]             VARCHAR (64)  NOT NULL,
    [Data_Value]            VARCHAR (MAX) NULL,
    [WinUser]               VARCHAR (64)  NOT NULL,
    [Seq]                   INT           NULL,
    [aCPL_Defaults_User_ID] BIGINT        NOT NULL
);

