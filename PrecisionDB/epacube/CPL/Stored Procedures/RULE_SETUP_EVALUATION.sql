﻿--A_MAUI_RULE_SETUP_EVALUATION
-- =============================================
-- Author:		Gary Stone
-- Create date: October 22, 2013
-- Description:	Price Record Evaluation
-- =============================================
CREATE PROCEDURE [CPL].[RULE_SETUP_EVALUATION]
@Config_ERP_PS Varchar(Max)
	, @Prods Varchar(Max)
	, @Custs Varchar(Max)
	, @ProdCust Varchar(Max)
	, @Identifiers Varchar(Max)
	, @SPC Varchar(Max)
	, @MBC Varchar(Max)
	, @NewRules Varchar(Max)
	, @User Varchar(64)
	, @EffDate datetime
	, @Job_FK_i Varchar(32)
	, @Rbts Varchar(8)
	, @CustValue varchar(128)
	, @Cust_DN_FK Varchar(32)
	, @chkWhseSpecific Varchar(8)
	, @PairLIPitems int

AS
BEGIN

	SET NOCOUNT ON;


Declare @Margin_Cost_Basis BigInt
Declare @Pricing_Cost_Basis BigInt
Declare @RuleType varchar(64)
Declare @UserFilename Varchar(64)
Declare @ResultType as int
Declare @ResultTypeVC as Varchar(3)
Declare @Cost_Change_Prior_Ind Int
Declare @Result_Effective_Date datetime
Declare @Job_FK Bigint
Declare @SQLcd Varchar(Max)
Declare @SQLcd1 Varchar(Max)
Declare @SQLcd2 Varchar(Max)

Set @UserFilename = replace(@User, '.', '_')

Set @SQLcd = '
	IF object_id(''tempdb..##SPC_' + @UserFilename + ''') is not null
	Drop Table ##SPC_' + @UserFilename + '
	IF object_id(''tempdb..##PS_' + @UserFilename + ''') is not null
	Drop Table ##ps_' + @UserFilename + '
	IF object_id(''tempdb..##Prods_' + @UserFilename + ''') is not null
	Drop Table ##Prods_' + @UserFilename + '
	IF object_id(''tempdb..##Custs_' + @UserFilename + ''') is not null
	Drop Table ##Custs_' + @UserFilename + '
	IF object_id(''tempdb..##ProdCust_' + @UserFilename + ''') is not null
	Drop Table ##ProdCust_' + @UserFilename + '
	IF object_id(''tempdb..##Identifiers_' + @UserFilename + ''') is not null
	Drop Table ##Identifiers_' + @UserFilename + '
	IF object_id(''tempdb..##MBC_' + @UserFilename + ''') is not null
	Drop Table ##MBC_' + @UserFilename + '
	IF object_id(''tempdb..##NewRules_' + @UserFilename + ''') is not null
	Drop Table ##NewRules_' + @UserFilename + '
	IF object_id(''tempdb..##MatSetup_' + @UserFilename + ''') is not null
	Drop Table ##MatSetup_' + @UserFilename + '
	IF object_id(''tempdb..##Variations_' + @UserFilename + ''') is not null
	Drop Table ##Variations_' + @UserFilename

Exec(@SQLcd)

Set @Margin_Cost_Basis = (select Value from epacube.epacube.epacube_params where Name = 'MARGIN COST BASIS')
Set @Pricing_Cost_Basis = (select Value from epacube.epacube.epacube_params where Name = 'PRICING COST BASIS')
Set @RuleType = Case When (select count(*) from epacube.synchronizer.RULES_HIERARCHY where result_data_name_Fk in (111501, 111502) and RECORD_STATUS_CR_FK = 1) > 0 then '(312, 313)' else '(313)' end
Set @UserFilename = replace(@User, '.', '_')

Delete from epacube.dbo.MAUI_Rules_Matrix_Setup where CreatedBy = @User

	--Determine Product Status
	Set @Config_ERP_PS = Replace(@Config_ERP_PS, '##PS', '##PS_' + @UserFilename)
	Exec(@Config_ERP_PS)

	--Product Selection
	Set @Prods = Replace(Replace(Replace(@Prods, '##Prods', '##Prods_' + @UserFilename), '##PS', '##PS_' + @UserFilename), '##SPC', '##SPC_' + @UserFilename)
	Exec (@Prods)

	--Customer Selection
	Set @Custs = Replace(@Custs, '##Custs', '##Custs_' + @UserFilename)
	Exec (@Custs)

	--ProdCust Table
	Set @ProdCust = Replace(Replace(Replace(@ProdCust, '##Prods', '##Prods_' + @UserFilename), '##Custs', '##Custs_' + @UserFilename), '##ProdCust', '##ProdCust_' + @UserFilename)
	Exec (@ProdCust)

	--Identifiers Table
	Set @Identifiers = Replace(Replace(@Identifiers, '##ProdCust', '##ProdCust_' + @UserFilename), '##Identifiers', '##Identifiers_' + @UserFilename)
	Exec (@Identifiers)

	--SPC Table - Capture Special Price Costing Units of Measure
	Set @SPC = Replace(Replace(@SPC, '##SPC', '##SPC_' + @UserFilename), '##Prods', '##Prods_' + @UserFilename)
	Exec (@SPC)

	Set @MBC = Replace(Replace(@MBC, '##MBC', '##MBC_' + @UserFilename), '##Identifiers', '##Identifiers_' + @UserFilename)
	Exec (@MBC)

	Set @NewRules = Replace(@NewRules, '##NewRules', '##NewRules_' + @UserFilename)
	Exec (@NewRules)



	--Set @SQLcd = '(Select count(*) recs into ##Test_' + @UserFileName = ' from ##Identifiers_' + @UserFilename + ')'
	--Exec(@SQLcd)

	--If Exec(@SQLcd) > 0 
	--	goto ENDofRPOCEDURE

If Cast((Select Convert(varchar(10), @EffDate, 101)) as datetime) > Cast((Select Convert(varchar(10), getdate(), 101)) as datetime)
Begin
	Set @ResultType = 711
	Set @Cost_Change_Prior_Ind = 1
	Set @Result_Effective_Date = @EffDate
End
	else
Begin	
	Set @ResultType = 710
	Set @Cost_Change_Prior_Ind = NULL
	Set @Result_Effective_Date = Null
End

	Set @ResultTypeVC = Cast(@ResultType as Varchar(3))

	exec epacube.[import].[LOAD_MAUI_0_JOB] 'MAUI_Rules_Matrix_Setup', '0', @ResultTypeVC
	Set @Job_FK = (select top 1 Job_ID from epacube.common.job with (nolock) where Job_Class_FK = 481 and [Name] = 'MAUI Analysis' and Data1 = 'MAUI_Rules_Matrix_Setup' order by Job_ID desc)

	Insert into epacube.marginmgr.ANALYSIS_JOB
		(Analysis_Job_ID, Analysis_Name, result_type_cr_fk, result_effective_date, rule_type_filter, Inner_Join_Filter, Product_Filter, Organization_Filter, Customer_Filter, RECORD_STATUS_CR_FK)
		Select 
		@Job_FK
		, 'MAUI_Rules_Matrix_Setup'
		, @ResultType
		, @Result_Effective_Date
		, @RuleType
		, 'Inner Join ##Identifiers_' + @UserFilename + ' ID on PS.Product_Structure_ID = ID.Product_Structure_FK 
				And ESC.Entity_Structure_ID = ID.Cust_Entity_Structure_FK 
				And ESO.entity_Structure_ID = ID.entity_structure_fk_of_record'
		, '(Select distinct Product_Structure_FK from ##Identifiers_' + @UserFilename + ')'
		, '(Select distinct entity_structure_fk_of_record from ##Identifiers_' + @UserFilename + ')'
		, '(Select distinct cust_entity_structure_fk from ##Identifiers_' + @UserFilename + ')'
		, 1

	EXEC  epacube.[marginmgr].[CALCULATION_PROCESS] @Job_FK

-------------------------------
Set @SQLcd1 = '
Declare @ERP Varchar(16)
Declare @Sheet_Results_DN_FK Bigint
Set @ERP = (Select Value from epacube.epacube.epacube_params with (nolock) where name = ''ERP HOST'')
Set @Sheet_Results_DN_FK = (select data_name_fk from epacube.dbo.MAUI_Config_ERP where usage = ''Data_Name'' and epaCUBE_Column = ''Sheet_Results'' and ERP = @ERP)

Insert into epacube.dbo.MAUI_Rules_Matrix_Setup
	(
	[NR_Margin_Amt], [NR_Margin_Pct], [Margin_Pct_Original], [NR_Sell_Price_Cust_Amt], [Sell_Price_Cust_Amt_cur_Original], [Margin_Amt], [Margin_Pct], [Sell_Price_Cust_Amt_cur], [Pricing_Product_Cost], [Margin_Product_Cost], [Basis_Amt], [Selected], [CustomerReference], [ProductReference], [CustomerFocus], [ProductFocus], [Job_FK_i], [WhatIF_ID], [CreatedBy], [Create_Date], [Approved_By], [Host_Rule_Xref_New], [Rules_FK_New], [Rule_Precedence_New], [WIF_Key], [Product_ID], [CustomerID_ST], [CustomerID_BT], [prod_Description], [CustomerID_Name], [CustomerID_ST_Name], [CustomerID_BT_Name], [Org_Vendor_ID], [Whse], [Sell_Price_Cust_Host_Rule_Xref], [Sell_Price_Cust_Rules_FK], [Rule Precedence], [Rule Precedence Original], [Price_Rule_Type], [Price_Rule_Type_New], [Sales_Qty], [Margin_Cost_Basis_Amt], [Rbts], [What_If_Reference_ID], [Prod_Group3], [Prod_Group1], [Prod_Group2], [Prod_Group4], [Cust_Group2], [Price Rule Level], [Override_Impact_Amt], [Applied], [Qty_Break_Rule_Ind], [Basis_Col], [Basis], [Basis_Col_New], [Basis_Amt_New], [Operation_1], [Operand_1], [Operation_2], [Operand_2], [Rebate_Amount], [Model_Qty], [Base_Price_Cur], [Base_Price_New], [List_Price_Cur], [List_Price_New], [Replacement_Cost_Cur], [Replacement_Cost_New], [Standard_Cost_Cur], [Standard_Cost_New], [Product_Structure_FK], [Org_Entity_Structure_FK], [Cust_Entity_Structure_FK], [SX_Cust_Price_Code], [SX_Cust_Discount_Code], [Stocking_UOM], [Identifier], [NR_Operand_Mult], [NR_Operand_Disc], [NR_Operation1], [NR_Operation2], [NR_Basis_Col], [Effective_Date], [End_Date], [NR_Basis], [Operand_1_Original], [Operand_2_Original], [Operation_1_Original], [Operation_2_Original], [Inactivate], [chkWhseSpecific], [Transferred_To_ERP], [Lookup Name], [Variations], [PriceListName], [Break_Prices], [LIP_Prod_Ind], [Consolidated], [PricesInEffect], [Correct_Record], [Product_Rank], [Promo], [CPT_Primary], [Job_FK]
	)
	Select
	Null NR_Margin_Amt
	, Null NR_Margin_Pct
	, Null NR_Margin_Pct
	, Null NR_Sell_Price_Cust_Amt
	, ec.Sell_Price_Cust_Amt * isnull(spc.spc_uom, 1) Sell_Price_Cust_Amt_Cur_Original
	, (ec.Margin_Amt + isnull(ec.REBATE_CB_AMT, 0)) * isnull(spc.spc_uom, 1) Margin_Amt
	, Case ec.Sell_Price_Cust_Amt when 0 then 0 else (ec.Sell_Price_Cust_Amt - ec.Margin_cost_basis_amt + isnull(ec.REBATE_CB_AMT, 0)) / ec.Sell_Price_Cust_Amt end Margin_Pct
	, ec.Sell_Price_Cust_Amt * isnull(spc.spc_uom, 1) sell_price_cust_amt_cur
	, ec.pricing_cost_basis_amt * isnull(spc.spc_uom, 1) Pricing_Cost_Basis_Amt
	, ec.Margin_cost_basis_amt * isnull(spc.spc_uom, 1) Margin_Cost_Basis_Amt_First
	, ecr.basis_calc * isnull(spc.spc_uom, 1) basis_calc
	, 0 ''Selected''
	, Case When isnull(eic.value, '''') = '''' then ''Customer Bill To'' else ''Customer Ship to'' end CustomerReference
	, ''Product_ID'' Product_Reference
	, isnull(eic.value, eip.value) CustomerFocus
	, pi.value ProductFocus
	, ' + @Job_FK_i + ' Job_FK_i
	, 0 WhatIF_ID
	, ''' + @User + ''' CreatedBy
	, Getdate() Create_Date
	, Null ''Approved_By''
	, Null Host_Rule_Xref_New
	, Null Rules_FK_New
	, (Select top 1 Rule_Precedence_New from ##Prods) Rule_Precedence_New
	, Null WIF_Key
	, pi.value Product_ID
	, eic.value CustomerID_ST
	, eip.value CustomerID_BT
	, Case when isnull(pd2.[description], '''') = '''' then pd1.[description] else pd1.[description] + '': '' + pd2.[description] end ''Prod_Description''
	, isnull(einc.value, einp.value) CustomerID_Name
	, einc.value CustomerID_ST_Name
	, einp.value CustomerID_BT_Name
	, eiv.value Org_Vendor_ID
	, isnull(MBC.Whse, eiw.value) Whse
	, ecr.host_rule_xref sell_price_cust_host_rule_xref
	, ec.rules_fk sell_price_cust_rules_fk
	, ecr.rule_precedence
	, ecr.rule_precedence rule_precedence_original
	, rp.host_precedence_level Price_Rule_Type
	, rp.host_precedence_level Price_Rule_Type_New
	, isnull(mbc.Sales_Qty, 0) Sales_Qty
	, Margin_Cost_Basis_Amt * isnull(spc.spc_uom, 1) Margin_Cost_Basis_Amt
	, ' + @Rbts + ' Rbts
	, Null What_If_Reference_ID

	, (select value from epacube.epacube.data_value dv with (nolock) where data_value_id = (select top 1 data_value_fk from epacube.epacube.PRODUCT_CATEGORY PG3 with (nolock)
		where PG3.Product_Structure_FK = EC.Product_Structure_FK and (PG3.Org_Entity_Structure_FK = EC.ORG_ENTITY_STRUCTURE_FK or PG3.Org_Entity_Structure_FK = 1)
		and PG3.Data_Name_FK = (select data_name_fk from epacube.dbo.MAUI_Config_ERP with (nolock) where usage = ''Product_Grouping'' and epaCUBE_Column = ''Prod_Group3'' and ERP = @ERP) 
		order by PG3.Org_Entity_Structure_FK Desc)) ''Prod_Group3''

	, (select value from epacube.epacube.data_value dv with (nolock) where data_value_id = (select top 1 data_value_fk from epacube.epacube.PRODUCT_CATEGORY PG1 with (nolock)
		where PG1.Product_Structure_FK = EC.Product_Structure_FK and (PG1.Org_Entity_Structure_FK = EC.ORG_ENTITY_STRUCTURE_FK or PG1.Org_Entity_Structure_FK = 1)
		and PG1.Data_Name_FK = (select data_name_fk from epacube.dbo.MAUI_Config_ERP with (nolock) where usage = ''Product_Grouping'' and epaCUBE_Column = ''Prod_Group1'' and ERP = @ERP) 
		order by PG1.Org_Entity_Structure_FK Desc)) ''Prod_Group1''

	, (select value from epacube.epacube.data_value dv with (nolock) where data_value_id = (select top 1 data_value_fk from epacube.epacube.PRODUCT_CATEGORY PG2 with (nolock)
		where PG2.Product_Structure_FK = EC.Product_Structure_FK and (PG2.Org_Entity_Structure_FK = EC.ORG_ENTITY_STRUCTURE_FK or PG2.Org_Entity_Structure_FK = 1)
		and PG2.Data_Name_FK = (select data_name_fk from epacube.dbo.MAUI_Config_ERP with (nolock) where usage = ''Product_Grouping'' and epaCUBE_Column = ''Prod_Group2'' and ERP = @ERP) 
		order by PG2.Org_Entity_Structure_FK Desc)) ''Prod_Group2''
	
	, (select value from epacube.epacube.data_value dv with (nolock) where data_value_id = (select top 1 data_value_fk from epacube.epacube.PRODUCT_CATEGORY PG4 with (nolock)
		where PG4.Product_Structure_FK = EC.Product_Structure_FK and (PG4.Org_Entity_Structure_FK = EC.ORG_ENTITY_STRUCTURE_FK or PG4.Org_Entity_Structure_FK = 1)
		and PG4.Data_Name_FK = (select data_name_fk from epacube.dbo.MAUI_Config_ERP with (nolock) where usage = ''Product_Grouping'' and epaCUBE_Column = ''Prod_Group4'' and ERP = @ERP) 
		order by PG4.Org_Entity_Structure_FK Desc)) ''Prod_Group4''
	, case when ' + @cust_dn_fk + ' = 244900 then ' + @CustValue + ' end ''Cust_Group2''
	, Null ''Price Rule Level''
'
	-----come back to this one

Set @SQLcd2 = '
	, mbc.Override_Impact_Amt
	, 0 Applied

	, Case		(Select Operand_Filter_Operator_CR_FK from epacube.synchronizer.rules_action_operands rao1 with (nolock) where rao1.basis_position = 1 and rao1.rules_action_fk =
				(select rules_action_id from epacube.synchronizer.rules_action ra1 with (nolock) where ra1.rules_fk = r.rules_id))
			When 9020 then 1 else 0 end Qty_Break_Rule_Ind
	, Replace(Replace(ecr.basis_calc_name, ''Ord Cogs'', ''Rebated Cost''), ''Order Cogs'', ''Rebated Cost'') Basis_Col
	, ecr.Basis_Calc * isnull(spc.spc_uom, 1) Basis
	, Null Basis_Col_New
	, Null Basis_Amt_New
	, Case ecr.Operation1
		When ''*'' then ''Multiplier''
		When ''Mult'' then ''Multiplier''
		When ''/'' then ''Discount''
		When ''Disc'' then ''Discount''
		When ''GM'' then ''Gross Profit''
		When ''='' then ''Fixed Value'' end Operation_1
	, Case ecr.Operation1 When ''='' then ecr.Number1 else ecr.Number1 / 100 end Operand_1
	, Case ecr.Operation2 
		When ''*'' then ''Multiplier''
		When ''Mult'' then ''Multiplier''
		When ''/'' then ''Discount''
		When ''Disc'' then ''Discount''
		When ''GM'' then ''Gross Profit''		When ''='' then ''Fixed Value'' end Operation_2
	, Case ecr.Operation2 When ''='' then ecr.Number2 else ecr.Number2 / 100 end Operand_2
	, isnull(ec.REBATE_CB_AMT, 0) Rebate_Amount
	, Case (Select value from epacube.epacube.epacube_params with (nolock) where name = ''MODEL SALES METHOD'')
		When ''Mode'' then mbc.Qty_Mode
		When ''Mean'' then mbc.Qty_Mean
		When ''Median'' then mbc.Qty_Median 
		else mbc.Qty_Mode end Model_Qty
	, SR.NET_VALUE3 * isnull(spc.spc_uom, 1) Base_Price_Cur
	, Null Base_Price_New
	, SR.NET_VALUE4 * isnull(spc.spc_uom, 1) List_Price_Cur
	, Null List_Price_New
	, SR.NET_VALUE1 * isnull(spc.spc_uom, 1) Replacement_Cost_Cur
	, Null Replacement_Cost_New
	, SR.NET_VALUE2 * isnull(spc.spc_uom, 1) Standard_Cost_Cur
	, Null Standard_Cost_New
	, ec.Product_Structure_FK
	, isnull(MBC.Org_Entity_Structure_FK, ec.Org_Entity_structure_fk) Org_Entity_structure_fk
	, ec.cust_entity_structure_fk
	, isnull(Case when ecr.Operation1 <> ''Discount'' then ecrb.basis_operand_position end, 1) SX_Cust_Price_Code
	, isnull(Case when ecr.Operation1 = ''Discount'' then ecrb.basis_operand_position end, 1) SX_Cust_Discount_Code
	, uc.uom_code Stocking_UOM
	, Null Identifier
	, Case ecrb.basis_operand_position When 1 then NR.OP1
		When 2 then NR.OP2
		When 3 then NR.OP3
		When 4 then NR.OP4
		When 5 then NR.OP5
		When 6 then NR.OP6
		When 7 then NR.OP7
		When 8 then NR.OP8
		When 9 then NR.OP9 end NR_Operand_Mult
	, Case ecrb.basis_operand_position When 1 then NR.DiscOP1
		When 2 then NR.DiscOP2
		When 3 then NR.DiscOP3
		When 4 then NR.DiscOP4
		When 5 then NR.DiscOP5
		When 6 then NR.DiscOP6
		When 7 then NR.DiscOP7
		When 8 then NR.DiscOP8
		When 9 then NR.DiscOP9 end NR_Operand_Disc
	, NR.NR_Operation1
	, NR.NR_Operation2
	, NR.NR_Basis_Col
	, ecr.rules_effective_date Effective_Date
	, r.end_date
	, Case	When NR.NR_Basis_Col like ''%List%'' then SR.NET_VALUE4
			When NR.NR_Basis_Col like ''%Base%'' then SR.NET_VALUE3
			When NR.NR_Basis_Col like ''%Std%'' then SR.NET_VALUE2
			When NR.NR_Basis_Col like ''%Rep%'' then SR.NET_VALUE1
			When NR.NR_Basis_Col = ''Rebated Cost'' then ec.pricing_cost_basis_amt
	  end * isnull(spc.spc_uom, 1) NR_Basis
	, ecr.Number1 Operand_1_Original
	, ecr.Number2 Operand_2_Original
	, Case ecr.Operation1
		When ''*'' then ''Multiplier''
		When ''Mult'' then ''Multiplier''
		When ''/'' then ''Discount''
		When ''Disc'' then ''Discount''
		When ''GM'' then ''Gross Profit''
		When ''='' then ''Fixed Value'' end Operation_1_Original
	, Case ecr.Operation2 
		When ''*'' then ''Multiplier''
		When ''Mult'' then ''Multiplier''
		When ''/'' then ''Discount''
		When ''Disc'' then ''Discount''
		When ''GM'' then ''Gross Profit''
		When ''='' then ''Fixed Value'' end Operation_2_Original
	, 0 Inactivate
	, ' + @chkWhseSpecific + ' chkWhseSpecific
	, 1 Transferred_To_ERP
	, pd_ln.[description] ''Lookup Name''
	, Null Variations
	, Null PriceListName
	, Null Break_Prices
	, Null LIP_Prod_Ind
	, 0 Consolidated
	, ''' + Cast(@EffDate as varchar(32)) + ''' PricesInEffect
	, 1 Correct_Record
	, Null Product_Rank
	, Null Promo
	, Null CPT_Primary
	, ' + Cast(@Job_FK as Varchar(32)) + ' Job_FK
	from epacube.synchronizer.event_calc ec with (nolock)
		inner join epacube.synchronizer.event_calc_rules ecr with (nolock) on ec.event_id = ecr.event_fk and ecr.result_rank = 1
		left join epacube.synchronizer.event_calc_rules_basis ecrb with (nolock) on ecr.EVENT_RULES_ID = ecrb.EVENT_RULES_FK and ecrb.BASIS_RANK = 1
		inner join epacube.epacube.product_identification pi with (nolock) on ec.product_structure_fk = pi.product_structure_fk and pi.data_name_fk = 110100
		inner join epacube.epacube.entity_structure es with (nolock) on ec.cust_entity_structure_fk = es.entity_structure_id
		inner join epacube.epacube.entity_identification eip with (nolock) 
			on (es.entity_structure_id = eip.entity_structure_fk and es.data_name_fk = 144010)
			or (es.parent_entity_structure_fk = eip.entity_structure_fk and es.data_name_fk = 144020)
		left join epacube.epacube.entity_identification eic with (nolock)
			on es.entity_structure_id = eic.entity_structure_fk and es.data_name_fk = 144020
		left join epacube.epacube.entity_ident_nonunique einp with (nolock) 
			on (es.entity_structure_id = einp.entity_structure_fk and es.data_name_fk = 144010)
			or (es.parent_entity_structure_fk = einp.entity_structure_fk and es.data_name_fk = 144020)
		left join epacube.epacube.entity_ident_nonunique einc with (nolock)
			on es.entity_structure_id = einc.entity_structure_fk and es.data_name_fk = 144020
		left join epacube.epacube.product_description pd1 with (nolock) on ec.product_structure_fk = pd1.product_structure_fk and pd1.data_name_fk = 110401
		left join epacube.epacube.product_description pd2 with (nolock) on ec.product_structure_fk = pd2.product_structure_fk and pd2.data_name_fk = 110402
		left join epacube.epacube.product_description pd_ln with (nolock) on ec.product_structure_fk = pd_ln.product_structure_fk and pd_ln.data_name_fk = 210404
		left join epacube.epacube.entity_identification eiv with (nolock) on ec.supl_entity_structure_fk = eiv.entity_structure_fk
		left join epacube.epacube.entity_identification eiw with (nolock) on ec.org_entity_structure_fk = eiw.entity_structure_fk
		left join epacube.synchronizer.rules r with (nolock) on ec.rules_fk = r.rules_id
		left join epacube.synchronizer.rules_precedence rp with (nolock) on r.rules_precedence_fk = rp.rules_precedence_id
		left join ##MBC MBC on ec.product_structure_fk = mbc.product_structure_fk and ec.cust_entity_structure_fk = mbc.cust_entity_structure_fk
		left join ##NewRules NR on Replace(ec.event_id, ec.event_id, 1) = Replace(NR_Operation1, NR_Operation1, 1)
		Left Join epacube.marginmgr.SHEET_RESULTS_VALUES_FUTURE SR with (nolock) on SR.Product_Structure_FK = ec.Product_Structure_FK And SR.Org_Entity_Structure_FK = ec.Org_Entity_Structure_FK
			and SR.DATA_NAME_FK = @Sheet_Results_DN_FK
		left join epacube.epacube.PRODUCT_UOM_CLASS puc with (nolock) on ec.product_structure_fk = puc.product_structure_fk and (ec.org_entity_structure_fk = puc.org_entity_structure_fk or puc.org_entity_structure_fk = 1)
			and puc.data_name_fk = 110603
		left join epacube.epacube.uom_code uc with (nolock) on puc.uom_code_fk = uc.UOM_CODE_ID
		left join ##spc spc on ec.product_structure_fk = spc.product_structure_fk
	Where ec.job_fk = ' + Cast(@Job_FK as varchar(32)) + '
		and ec.RESULT_DATA_NAME_FK = 111602
'
Set @SQLcd1 = Replace(@SQLcd1, '##Prods', '##Prods_' + @UserFilename)
Set @SQLcd2 = Replace(Replace(Replace(@SQLcd2, '##MBC', '##MBC_' + @UserFilename), '##NewRules', '##NewRules_' + @UserFilename), '##SPC', '##SPC_' + @UserFilename)
Exec(@SQLcd1 + @SQLcd2)

---------------------------------

--Additional Functions once MRMS is populated

If @PairLIPitems <> 0
Begin
Set @SQLcd1 = '
	Declare @UserFilename VarChar(64)
	Declare @EffDate Varchar(32)
	Declare @Job_FK Varchar(32)
	Declare @User varchar(64)

	Set @UserFilename = ''' + Cast(@UserFilename as varchar(64)) + '''
	Set @EffDate = ''' + Cast(@EffDate as varchar(64))  + '''
	Set @Job_FK = ''' + Cast(@Job_FK as varchar(64))  + '''
	Set @User = ''' + @User + '''

	Select * into ##MatSetup from epacube.dbo.MAUI_Rules_Matrix_Setup with (nolock) where CreatedBy = @User

	Create Table ##Variations (Product_Groupings_FK Int, Variations Varchar(128))
	Create Index idx_vars on ##Variations(Product_Groupings_FK)	

	Declare @Groupings_FK Varchar(max)
	Declare @Grp Varchar(max)

	Set @Groupings_FK = ''''

	DECLARE Groupings Cursor local for
	Select mpv.Product_Groupings_FK
		from epacube.dbo.MAUI_Product_Variations MPV with (Nolock) 
		inner join ##MatSetup PL on MPV.product_structure_FK = PL.Product_Structure_FK
		group by mpv.product_groupings_fk

		 OPEN Groupings;

	 FETCH NEXT FROM Groupings INTO @GRP
		 WHILE @@FETCH_STATUS = 0

	Begin

		Declare @Product_Groupings_FK Int
		Declare @Var Varchar(128)
		Declare @Variations Varchar(Max)
		Set @Variations = ''''

		DECLARE Variations Cursor local for
			Select mpv.Product_Groupings_FK, MPV.Variation
			from epacube.dbo.MAUI_Product_Variations MPV with (Nolock)
			inner join ##MatSetup PL on MPV.product_structure_FK = PL.Product_Structure_FK
			Where mpv.product_groupings_fk = @grp
			group by mpv.product_groupings_fk, mpv.variation
			order by MPV.Variation
	
		OPEN Variations;

		FETCH NEXT FROM Variations INTO @Product_Groupings_FK, @Var
			 WHILE @@FETCH_STATUS = 0

		Begin

			Set @Variations = @Variations + '', '' + @Var

			FETCH NEXT FROM Variations INTO @Product_Groupings_FK, @Var    
		End
			Set @Variations = ''Insert into ##Variations (Variations, Product_Groupings_FK) Select ''''('' + Right(@Variations, Len(@Variations) -2) + '')'''', '' + Cast(@Product_Groupings_FK as Varchar(64))
			Exec (@Variations)
			Close Variations;
			Deallocate Variations;

			FETCH NEXT FROM Groupings INTO @GRP    
	End

	Close Groupings;
	Deallocate Groupings;

	Begin

		Insert into epacube.dbo.MAUI_Rules_Matrix_Setup
		(
		[Margin_Amt], [Margin_Pct], [Margin_Pct_Original], [Sell_Price_Cust_Amt_cur], [Sell_Price_Cust_Amt_cur_Original], [Pricing_Product_Cost], [Margin_Product_Cost], [Basis_Amt], [Selected], [CustomerReference], [ProductReference], [CustomerFocus], [ProductFocus], [Job_FK_i], [WhatIF_ID], [CreatedBy], [Create_Date], [Approved_By], [Host_Rule_Xref_New], [Rules_FK_New], [Rule_Precedence_New], [WIF_Key], [Product_ID], [CustomerID_ST], [CustomerID_BT], [prod_Description], [CustomerID_Name], [CustomerID_ST_Name], [CustomerID_BT_Name], [Org_Vendor_ID], [Whse], [Sell_Price_Cust_Host_Rule_Xref], [Sell_Price_Cust_Rules_FK], [Rule Precedence], [Rule Precedence Original], [Price_Rule_Type], [Price_Rule_Type_New], [Sales_Qty], [Margin_Cost_Basis_Amt], [Rbts], [What_If_Reference_ID], [Prod_Group3], [Prod_Group1], [Prod_Group2], [Prod_Group4], [Cust_Group2], [Price Rule Level], [Override_Impact_Amt], [Applied], [Qty_Break_Rule_Ind], [Basis_Col], [Basis], [Basis_Col_New], [Basis_Amt_New], [Operation_1], [Operation_1_Original], [Operand_1], [Operand_1_Original], [Operation_2], [Operation_2_Original], [Operand_2], [Operand_2_Original], [Rebate_Amount], [Model_Qty], [Base_Price_Cur], [Base_Price_New], [List_Price_Cur], [List_Price_New], [Replacement_Cost_Cur], [Replacement_Cost_New], [Standard_Cost_Cur], [Standard_Cost_New], [Product_Structure_FK], [Org_Entity_Structure_FK], [Cust_Entity_Structure_FK], [SX_Cust_Price_Code], [SX_Cust_Discount_Code], [Stocking_UOM], [Identifier], [NR_Operand_Mult], [NR_Operand_Disc], [NR_Operation1], [NR_Operation2], [NR_Basis_Col], [Effective_Date], [End_Date], [NR_Basis], inactivate, chkWhseSpecific, Transferred_To_ERP, Variations, [Lookup Name], [PricesInEffect], Job_FK
		)
			select 
				--MRMS.[NR_Margin_Amt], MRMS.[NR_Margin_Pct], MRMS.[NR_Sell_Price_Cust_Amt], 
				Max(MRMS.[Margin_Amt]) [Margin_Amt], Max(MRMS.[Margin_Pct]) [Margin_Pct], Max(MRMS.[Margin_Pct]) [Margin_Pct], Max(MRMS.[Sell_Price_Cust_Amt_cur]) [Sell_Price_Cust_Amt_cur], Max(MRMS.[Sell_Price_Cust_Amt_cur]) [Sell_Price_Cust_Amt_cur]
				, MRMS.[Pricing_Product_Cost], MRMS.[Margin_Product_Cost], MRMS.[Basis_Amt], MRMS.[Selected], MRMS.[CustomerReference]
				, Max(Case When isnull(MPG.Product_Groupings_ID, 0) > 0 then ''LIP'' else MRMS.[ProductReference] end) [ProductReference]
				, MRMS.[CustomerFocus]
				, Max(Case When isnull(MPG.Product_Groupings_ID, 0) > 0 then ''MULTI-'' + Cast(MPG.Product_Groupings_ID as Varchar(36)) else MRMS.[ProductFocus] end) [ProductFocus]
				, MRMS.[Job_FK_i], MRMS.[WhatIF_ID], MRMS.[CreatedBy], MRMS.[Create_Date], MRMS.[Approved_By], MRMS.[Host_Rule_Xref_New], MRMS.[Rules_FK_New], MRMS.[Rule_Precedence_New], MRMS.[WIF_Key]
				,  Max(Case When isnull(MPG.Product_Groupings_ID, 0) > 0 then ''MULTI-'' + Cast(MPG.Product_Groupings_ID as Varchar(36)) else MRMS.[Product_ID] end) [Product_ID]
				, MRMS.[CustomerID_ST], MRMS.[CustomerID_BT]
				,  Max(Case When isnull(MPG.Product_Groupings_ID, 0) > 0 then MPG.Description + ''-Multiple Varieties'' else MRMS.[prod_Description] end) [prod_Description]
				, MRMS.[CustomerID_Name], MRMS.[CustomerID_ST_Name], MRMS.[CustomerID_BT_Name], MRMS.[Org_Vendor_ID], MRMS.[Whse], MRMS.[Sell_Price_Cust_Host_Rule_Xref], MRMS.[Sell_Price_Cust_Rules_FK], MRMS.[Rule Precedence], MRMS.[Rule Precedence Original], MRMS.[Price_Rule_Type], MRMS.[Price_Rule_Type_New]
				, Cast(Max(MRMS.[Sales_Qty]) as Numeric(18, 2)) [Sales_Qty]
				, MRMS.[Margin_Cost_Basis_Amt], MRMS.[Rbts], MRMS.[What_If_Reference_ID], MRMS.[Prod_Group3], MRMS.[Prod_Group1], MRMS.[Prod_Group2], MRMS.[Prod_Group4], MRMS.[Cust_Group2], MRMS.[Price Rule Level]
				,  Sum(MRMS.[Override_Impact_Amt]) Override_Impact_Amt
				, MRMS.[Applied], MRMS.[Qty_Break_Rule_Ind], MRMS.[Basis_Col], MRMS.[Basis], MRMS.[Basis_Col_New], MRMS.[Basis_Amt_New], MRMS.[Operation_1], MRMS.[Operation_1], MRMS.[Operand_1], MRMS.[Operand_1], MRMS.[Operation_2], MRMS.[Operation_2], MRMS.[Operand_2], MRMS.[Operand_2], MRMS.[Rebate_Amount]
				, Max(MRMS.[Model_Qty]) Model_Qty
				, MRMS.[Base_Price_Cur], MRMS.[Base_Price_New], MRMS.[List_Price_Cur], MRMS.[List_Price_New], MRMS.[Replacement_Cost_Cur], MRMS.[Replacement_Cost_New], MRMS.[Standard_Cost_Cur], MRMS.[Standard_Cost_New]
				, Min(MRMS.[Product_Structure_FK]) ''Product_Structure_FK''
--				, Max(Case When isnull(MPG.Product_Groupings_ID, 0) > 0 then Null else MRMS.[Product_Structure_FK] end) ''Product_Structure_FK''
				, MRMS.[Org_Entity_Structure_FK], MRMS.[Cust_Entity_Structure_FK], MRMS.[SX_Cust_Price_Code], MRMS.[SX_Cust_Discount_Code], MRMS.[Stocking_UOM]
				, Max(MRMS.[Identifier]) Identifier
				, MRMS.[NR_Operand_Mult], MRMS.[NR_Operand_Disc], MRMS.[NR_Operation1], MRMS.[NR_Operation2], MRMS.[NR_Basis_Col], MRMS.[Effective_Date], MRMS.[End_Date], MRMS.[NR_Basis], MRMS.[Inactivate], MRMS.[chkWhseSpecific], MRMS.[Transferred_To_ERP], V.[Variations]
				, Max(Case When isnull(MPG.Product_Groupings_ID, 0) > 0 then Null else MRMS.[Lookup Name] end) ''Lookup Name''
				, @EffDate, @Job_FK
				from ##MatSetup MRMS with (Nolock)
				Left Join epacube.dbo.MAUI_Products_Grouped Grp with (Nolock)
					on MRMS.Product_Structure_FK = Grp.Product_Structure_FK
				Left join epacube.dbo.MAUI_product_Groupings MPG with (Nolock) on Grp.product_groupings_fk = MPG.product_groupings_id
				Left Join ##Variations V with (Nolock) on MPG.product_groupings_id = V.product_groupings_FK
				Where 1 = 1
				
				Group by
				MRMS.[Pricing_Product_Cost], MRMS.[Margin_Product_Cost], MRMS.[Basis_Amt], MRMS.[Selected], MRMS.[CustomerReference], MRMS.[CustomerFocus]
				, MRMS.[Job_FK_i], MRMS.[WhatIF_ID], MRMS.[CreatedBy], MRMS.[Create_Date], MRMS.[Approved_By], MRMS.[Host_Rule_Xref_New], MRMS.[Rules_FK_New], MRMS.[Rule_Precedence_New], MRMS.[WIF_Key]
				, MRMS.[CustomerID_ST], MRMS.[CustomerID_BT]
				, MRMS.[CustomerID_Name], MRMS.[CustomerID_ST_Name], MRMS.[CustomerID_BT_Name], MRMS.[Org_Vendor_ID], MRMS.[Whse], MRMS.[Sell_Price_Cust_Host_Rule_Xref], MRMS.[Sell_Price_Cust_Rules_FK], MRMS.[Rule Precedence], MRMS.[Rule Precedence Original], MRMS.[Price_Rule_Type], MRMS.[Price_Rule_Type_New]
				, MRMS.[Margin_Cost_Basis_Amt], MRMS.[Rbts], MRMS.[What_If_Reference_ID], MRMS.[Prod_Group3], MRMS.[Prod_Group1], MRMS.[Prod_Group2], MRMS.[Prod_Group4], MRMS.[Cust_Group2], MRMS.[Price Rule Level]
				, MRMS.[Applied], MRMS.[Qty_Break_Rule_Ind], MRMS.[Basis_Col], MRMS.[Basis], MRMS.[Basis_Col_New], MRMS.[Basis_Amt_New], MRMS.[Operation_1], MRMS.[Operation_1], MRMS.[Operand_1], MRMS.[Operand_1], MRMS.[Operation_2], MRMS.[Operation_2], MRMS.[Operand_2], MRMS.[Operand_2], MRMS.[Rebate_Amount]
				, MRMS.[Base_Price_Cur], MRMS.[Base_Price_New], MRMS.[List_Price_Cur], MRMS.[List_Price_New], MRMS.[Replacement_Cost_Cur], MRMS.[Replacement_Cost_New], MRMS.[Standard_Cost_Cur], MRMS.[Standard_Cost_New]
				, MRMS.[Org_Entity_Structure_FK], MRMS.[Cust_Entity_Structure_FK], MRMS.[SX_Cust_Price_Code], MRMS.[SX_Cust_Discount_Code], MRMS.[Stocking_UOM]
				, MRMS.[NR_Operand_Mult], MRMS.[NR_Operand_Disc], MRMS.[NR_Operation1], MRMS.[NR_Operation2], MRMS.[NR_Basis_Col], MRMS.[Effective_Date], MRMS.[End_Date], MRMS.[NR_Basis], MRMS.[Inactivate], MRMS.[chkWhseSpecific], MRMS.[Transferred_To_ERP], V.[Variations]
				Order by productfocus

	End'

	Set @SQLcd1 = Replace(Replace(Replace(Replace(@SQLcd1, '##MatSetup', '##MatSetup_' + @UserFilename), '##NewRules', '##NewRules_' + @UserFilename), '##Variations', '##Variations_' + @UserFilename), '##SPC', '##SPC_' + @UserFilename)
	Exec(@SQLcd1)
End

Set @SQLcd2 = '
	Declare @UserFilename VarChar(64)
	Declare @EffDate Varchar(32)
	Declare @Job_FK Varchar(32)
	Declare @User varchar(64)

	Set @UserFilename = ''' + Cast(@UserFilename as varchar(64)) + '''
	Set @EffDate = ''' + Cast(@EffDate as varchar(64))  + '''
	Set @Job_FK = ''' + Cast(@Job_FK as varchar(64))  + '''
	Set @User = ''' + @User + '''

Update MRMS
Set Promo = Case When RS.Name like ''%Promo%'' then 1 else 0 end
,	CPT_Primary = 
	(
	Select top 1 VALUE from epacube.epacube.data_value dv with (nolock)
	inner join epacube.epacube.entity_mult_type emt with (nolock) on dv.data_name_fk = emt.DATA_NAME_FK and dv.DATA_VALUE_ID = emt.DATA_VALUE_FK
	where dv.data_name_fk = 244900 and emt.ENTITY_STRUCTURE_FK = MRMS.Cust_Entity_Structure_FK order by ENTITY_MULT_TYPE_ID
	)
from epacube.dbo.MAUI_Rules_Matrix_Setup MRMS
	inner join epacube.synchronizer.rules r with (nolock) on MRMS.Sell_Price_Cust_Rules_FK = r.RULES_ID
	inner join epacube.synchronizer.RULES_STRUCTURE rs with (nolock) on r.RULES_STRUCTURE_FK = rs.RULES_STRUCTURE_ID
Where CreatedBy = @User

Update MRMS
Set 
NR_Margin_Amt = Case	When (chg.Qty_Break_Rule_Ind_NR <> 0 and Test_Mod_Qty <> 0) Then Chg.[New Margin Amt]
						When NR_Operation1 = ''Discount'' then Chg.[New Margin Amt]
						When NR_Operation1 <> ''Discount'' then Chg.[New Margin Amt] else NR_Margin_Amt End

, NR_Margin_Pct = Case	When (chg.Qty_Break_Rule_Ind_NR <> 0 and Test_Mod_Qty <> 0) Then Chg.[New Margin Pct]
						When NR_Operation1 = ''Discount'' then Chg.[New Margin Pct]
						When NR_Operation1 <> ''Discount'' then Chg.[New Margin Pct] else NR_Margin_Pct End

, NR_Sell_Price_Cust_Amt = Case	When (chg.Qty_Break_Rule_Ind_NR <> 0 and Test_Mod_Qty <> 0) Then Chg.[New Sell Price]
						When NR_Operation1 = ''Discount'' then Chg.[New Sell Price]
						When NR_Operation1 <> ''Discount'' then Chg.[New Sell Price] else NR_Sell_Price_Cust_Amt End

, Model_Qty = Case isnull(Qty_Break_Rule_Ind_NR, 0) when 0 then Model_Qty else Test_Mod_Qty end
, [Price Rule Level] = BracketPriceLevelCur + '' - '' + Case When NR_Operation1 = ''Discount'' then Cast(chg.SX_Cust_Discount_Code as Varchar(2)) else Cast(chg.SX_Cust_Price_Code as Varchar(2)) End
, Sell_Price_Cust_Amt_Cur = Case When chg.Qty_Break_Rule_Ind_NR <> 0 and Test_Mod_Qty <> 0 then BracketPriceCur else Sell_Price_Cust_Amt_Cur end
, Margin_Amt = Case When chg.Qty_Break_Rule_Ind_NR <> 0 and Test_Mod_Qty <> 0 then BracketPriceCur - Prod_Cost_Cur else Margin_Amt end
, Margin_Pct =  Case When chg.Qty_Break_Rule_Ind_NR <> 0 and Test_Mod_Qty <> 0 then Cast((BracketPriceCur - Prod_Cost_Cur) / BracketPriceCur as Numeric(18, 4)) else Margin_Pct end
, SPC_UOM = chg.SPC_UOM
from epacube.dbo.maui_rules_Matrix_setup MRMS
inner Join
(Select
[New Sell Price]
, Cast([New Sell Price] - [Margin_Product_Cost] + Rebate_Amount as Numeric(18, 4)) ''New Margin Amt''
, Cast(([New Sell Price] - [Margin_Product_Cost] + Rebate_Amount) / [New Sell Price] as Numeric(18, 4)) ''New Margin Pct''
, Rules_Matrix_Setup_ID
, Cast(Case	When Isnull(Q2, 0) = 0 or Model_Qty <= Q1 then BracketPrice1
		When Isnull(Q3, 0) = 0 or Model_Qty <= Q2 then BracketPrice2
		When Isnull(Q4, 0) = 0 or Model_Qty <= Q3 then BracketPrice3
		When Isnull(Q5, 0) = 0 or Model_Qty <= Q4 then BracketPrice4
		When Isnull(Q6, 0) = 0 or Model_Qty <= Q5 then BracketPrice5
		When Isnull(Q7, 0) = 0 or Model_Qty <= Q6 then BracketPrice6
		When Isnull(Q8, 0) = 0 or Model_Qty <= Q7 then BracketPrice7
		When Isnull(Q9, 0) = 0 or Model_Qty <= Q8 then BracketPrice8 else BracketPrice9 end as Numeric(18, 4)) BracketPriceCur

, Cast(Case	When Isnull(Q2, 0) = 0 or Model_Qty <= Q1 then 1
		When Isnull(Q3, 0) = 0 or Model_Qty <= Q2 then 2
		When Isnull(Q4, 0) = 0 or Model_Qty <= Q3 then 3
		When Isnull(Q5, 0) = 0 or Model_Qty <= Q4 then 4
		When Isnull(Q6, 0) = 0 or Model_Qty <= Q5 then 5
		When Isnull(Q7, 0) = 0 or Model_Qty <= Q6 then 6
		When Isnull(Q8, 0) = 0 or Model_Qty <= Q7 then 7
		When Isnull(Q9, 0) = 0 or Model_Qty <= Q8 then 8 else 9 end as Varchar(1)) BracketPriceLevelCur
, Prod_Cost_Cur
, Qty_Break_Rule_Ind_NR
, Test_Mod_Qty
, SX_Cust_Price_Code
, SX_Cust_Discount_Code
, SPC_UOM
from (
Select NR_Sell_Price_Cust_Amt
, Cast(Case NR_Operation1	When ''Gross Margin'' Then (Case When [NR_Basis_Col] = ''Rebated Cost'' and isnull([Rebate_Amount], 0) > 0 then [Rebate_Amount] * -1 else 0 end + NR_Basis) / (1 - Mult)
						When ''Gross Profit'' Then (Case When [NR_Basis_Col] = ''Rebated Cost'' and isnull([Rebate_Amount], 0) > 0 then [Rebate_Amount] * -1 else 0 end + NR_Basis) / (1 - Mult)
						When ''Multiplier'' Then (Case When [NR_Basis_Col] = ''Rebated Cost'' and isnull([Rebate_Amount], 0) > 0 then [Rebate_Amount] * -1 else 0 end + NR_Basis) * Mult
						When ''Fixed Value'' Then Mult
						When ''Discount'' Then (Case When [NR_Basis_Col] = ''Rebated Cost'' and isnull([Rebate_Amount], 0) > 0 then [Rebate_Amount] * -1 else 0 end + NR_Basis) * (1 - Disc) 
						End
*
isnull(Case NR_Operation2	When ''Multiplier'' Then Mult
						When ''Discount'' Then (1 - Disc) end, 1) as Numeric(18, 6))
''New Sell Price''
, Margin_Product_Cost
, Pricing_Product_Cost
, Rules_Matrix_Setup_ID
, Cast(Case when CharIndex(''-'',BracketQty1) <> 0 then substring(BracketQty1,CharIndex(''-'',BracketQty1) + 1, 30) When CharIndex(''+'',BracketQty1) <> 0 then 10000000 end as Numeric(18, 4)) Q1
, Cast(Case when CharIndex(''-'',BracketQty2) <> 0 then substring(BracketQty2,CharIndex(''-'',BracketQty2) + 1, 30) When CharIndex(''+'',BracketQty2) <> 0 then 10000000 end as Numeric(18, 4)) Q2
, Cast(Case when CharIndex(''-'',BracketQty3) <> 0 then substring(BracketQty3,CharIndex(''-'',BracketQty3) + 1, 30) When CharIndex(''+'',BracketQty3) <> 0 then 10000000 end as Numeric(18, 4)) Q3
, Cast(Case when CharIndex(''-'',BracketQty4) <> 0 then substring(BracketQty4,CharIndex(''-'',BracketQty4) + 1, 30) When CharIndex(''+'',BracketQty4) <> 0 then 10000000 end as Numeric(18, 4)) Q4
, Cast(Case when CharIndex(''-'',BracketQty5) <> 0 then substring(BracketQty5,CharIndex(''-'',BracketQty5) + 1, 30) When CharIndex(''+'',BracketQty5) <> 0 then 10000000 end as Numeric(18, 4)) Q5
, Cast(Case when CharIndex(''-'',BracketQty6) <> 0 then substring(BracketQty6,CharIndex(''-'',BracketQty6) + 1, 30) When CharIndex(''+'',BracketQty6) <> 0 then 10000000 end as Numeric(18, 4)) Q6
, Cast(Case when CharIndex(''-'',BracketQty7) <> 0 then substring(BracketQty7,CharIndex(''-'',BracketQty7) + 1, 30) When CharIndex(''+'',BracketQty7) <> 0 then 10000000 end as Numeric(18, 4)) Q7
, Cast(Case when CharIndex(''-'',BracketQty8) <> 0 then substring(BracketQty8,CharIndex(''-'',BracketQty8) + 1, 30) When CharIndex(''+'',BracketQty8) <> 0 then 10000000 end as Numeric(18, 4)) Q8
, Cast(Case when CharIndex(''-'',BracketQty9) <> 0 then substring(BracketQty9,CharIndex(''-'',BracketQty9) + 1, 30) When CharIndex(''+'',BracketQty9) <> 0 then 10000000 end as Numeric(18, 4)) Q9
, BracketPrice1, BracketPrice2, BracketPrice3, BracketPrice4, BracketPrice5, BracketPrice6, BracketPrice7, BracketPrice8, BracketPrice9
, Case When (Select value from epacube.epacube.epacube_params where [Name] = ''MARGIN COST BASIS'') = 231101 then [Replacement_Cost_Cur] else [Standard_Cost_Cur] end Prod_Cost_Cur
, qty_break_rule_ind_NR
, Model_Qty
, Test_Mod_Qty
, SX_Cust_Price_Code
, SX_Cust_Discount_Code
, NR_Operation1
, NR_Operation2
, SPC_UOM
, rebate_amount
from 
(
select 
NR_Sell_Price_Cust_Amt  NR_Sell_Price_Cust_Amt
, NR_Basis  NR_Basis
, isnull([Rebate_Amount], 0) Rebate_Amount
, Margin_Product_Cost  Margin_Product_Cost
, Pricing_Product_Cost  Pricing_Product_Cost
, Rules_Matrix_Setup_ID
, BracketQty1, BracketQty2, BracketQty3, BracketQty4, BracketQty5, BracketQty6, BracketQty7, BracketQty8, BracketQty9
, BracketPrice1, BracketPrice2, BracketPrice3, BracketPrice4, BracketPrice5, BracketPrice6, BracketPrice7, BracketPrice8, BracketPrice9
, [Replacement_Cost_Cur]  Replacement_Cost_Cur
, [Standard_Cost_Cur]  Standard_Cost_Cur
, Model_Qty
, Mod_Qty Test_Mod_Qty
, SX_Cust_Price_Code
, SX_Cust_Discount_Code
, isnull(Case When NR.NR_Operation1 = ''Discount'' or NR.NR_Operation2 = ''Discount'' then
	Case MRMS.SX_Cust_Discount_Code
		When 1 then NR.DiscOp1
		When 2 then Coalesce(NR.DiscOp2, NR.DiscOp1)
		When 3 then Coalesce(NR.DiscOp3, NR.DiscOp2, NR.DiscOp1)
		When 4 then Coalesce(NR.DiscOp4, NR.DiscOp3, NR.DiscOp2, NR.DiscOp1)
		When 5 then Coalesce(NR.DiscOp5, NR.DiscOp4, NR.DiscOp3, NR.DiscOp2, NR.DiscOp1)
		When 6 then Coalesce(NR.DiscOp6, NR.DiscOp5, NR.DiscOp4, NR.DiscOp3, NR.DiscOp2, NR.DiscOp1)
		When 7 then Coalesce(NR.DiscOp7, NR.DiscOp6, NR.DiscOp5, NR.DiscOp4, NR.DiscOp3, NR.DiscOp2, NR.DiscOp1)
		When 8 then Coalesce(NR.DiscOp8, NR.DiscOp7, NR.DiscOp6, NR.DiscOp5, NR.DiscOp4, NR.DiscOp3, NR.DiscOp2, NR.DiscOp1)
		When 9 then Coalesce(NR.DiscOp9, NR.DiscOp8, NR.DiscOp7, NR.DiscOp6, NR.DiscOp5, NR.DiscOp4, NR.DiscOp3, NR.DiscOp2, NR.DiscOp1) end End, 0) ''Disc''
, Case When NR.NR_Operation1 <> ''Discount'' or (NR.NR_Operation1 = ''Discount'' and isnull(NR.NR_Operation2, '''') <> '''') then
	Case MRMS.SX_Cust_Price_Code 
		When 1 then NR.Op1
		When 2 then Coalesce(NR.Op2, NR.Op1)
		When 3 then Coalesce(NR.Op3, NR.Op2, NR.Op1)
		When 4 then Coalesce(NR.Op4, NR.Op3, NR.Op2, NR.Op1)
		When 5 then Coalesce(NR.Op5, NR.Op4, NR.Op3, NR.Op2, NR.Op1)
		When 6 then Coalesce(NR.Op6, NR.Op5, NR.Op4, NR.Op3, NR.Op2, NR.Op1)
		When 7 then Coalesce(NR.Op7, NR.Op6, NR.Op5, NR.Op4, NR.Op3, NR.Op2, NR.Op1)
		When 8 then Coalesce(NR.Op8, NR.Op7, NR.Op6, NR.Op5, NR.Op4, NR.Op3, NR.Op2, NR.Op1)
		When 9 then Coalesce(NR.Op9, NR.Op8, NR.Op7, NR.Op6, NR.Op5, NR.Op4, NR.Op3, NR.Op2, NR.Op1) end End Mult
, NR.*
, SPC.SPC_UOM
from epacube.dbo.maui_rules_matrix_setup MRMS
left join ##NewRules NR on Replace(mrms.Rules_Matrix_Setup_ID, mrms.Rules_Matrix_Setup_ID, 1) = Replace(NR.NR_Operation1, NR.NR_Operation1, 1)
left join ##SPC SPC on MRMS.product_structure_fk = SPC.product_structure_fk
where createdby = @User
) NR_data
) A ) Chg
on MRMS.Rules_Matrix_Setup_ID = Chg.Rules_Matrix_Setup_ID'

Set @SQLcd2 = Replace(Replace(Replace(Replace(@SQLcd2, '##MatSetup', '##MatSetup_' + @UserFilename), '##NewRules', '##NewRules_' + @UserFilename), '##Variations', '##Variations_' + @UserFilename), '##SPC', '##SPC_' + @UserFilename)
Exec(@SQLcd2)
-------------------------------

Exec epacube.[marginmgr].[UTIL_PURGE_MARGIN_TABLES] @Job_FK, Null

Set @SQLcd = '
	IF object_id(''tempdb..##SPC_' + @UserFilename + ''') is not null
	Drop Table ##SPC_' + @UserFilename + '
	IF object_id(''tempdb..##PS_' + @UserFilename + ''') is not null
	Drop Table ##ps_' + @UserFilename + '
	IF object_id(''tempdb..##Prods_' + @UserFilename + ''') is not null
	Drop Table ##Prods_' + @UserFilename + '
	IF object_id(''tempdb..##Custs_' + @UserFilename + ''') is not null
	Drop Table ##Custs_' + @UserFilename + '
	IF object_id(''tempdb..##ProdCust_' + @UserFilename + ''') is not null
	Drop Table ##ProdCust_' + @UserFilename + '
	IF object_id(''tempdb..##Identifiers_' + @UserFilename + ''') is not null
	Drop Table ##Identifiers_' + @UserFilename + '
	IF object_id(''tempdb..##MBC_' + @UserFilename + ''') is not null
	Drop Table ##MBC_' + @UserFilename + '
	IF object_id(''tempdb..##NewRules_' + @UserFilename + ''') is not null
	Drop Table ##NewRules_' + @UserFilename + '
	IF object_id(''tempdb..##MatSetup_' + @UserFilename + ''') is not null
	Drop Table ##MatSetup_' + @UserFilename + '
	IF object_id(''tempdb..##Variations_' + @UserFilename + ''') is not null
	Drop Table ##Variations_' + @UserFilename

Exec(@SQLcd)

--ENDofRPOCEDURE:
END
