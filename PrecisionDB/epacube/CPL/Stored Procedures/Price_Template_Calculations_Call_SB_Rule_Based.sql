﻿ 

--A_MAUI_Price_Template_Calculations_Call_SB_Rule_Based

-- =============================================
-- Author:		<Gary Stone>
-- Create date: <September 28, 2016>
-- =============================================
CREATE PROCEDURE [CPL].[Price_Template_Calculations_Call_SB_Rule_Based]
@Job_FK BigInt
, @Eff datetime = Null
, @Wuser varchar(64) = ''
, @recipients varchar(128) = Null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime


DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of CPL.Price_Template_Calculations_Call_SB_Rule_Based.'
					 + cast(isnull(@Job_FK, 0) as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @Job_FK,
						   				  @exec_id   = @l_exec_no OUTPUT;
--Declare @Job_FK Varchar(65)
--Declare @Eff Varchar(64)
--Declare @recipients varchar(128)
--Set @recipients = 'gstone@epacube.com'
	
Declare @ServiceBrokerCall Varchar(Max)
Declare @body varchar(Max)
Declare @subject varchar(128)

SET @ServiceBrokerCall = 'EXEC [CPL].[Price_Template_Calculations_Generate_Rule_Based] ' 
+ Cast(@Job_FK as varchar(32)) + ', '''
+ Convert(Varchar(10), @Eff, 112) + ''', '''
+ @Wuser + ''''

EXEC queue_manager.enqueue
	@service_name = N'TargetEpaService', --  nvarchar(50)
	@cmd_text = @ServiceBrokerCall, --  nvarchar(4000)
	@epa_job_id = @Job_FK, --  int
	@cmd_type = N'SQL' --  nvarchar(10)
	
Print(@ServiceBrokerCall)

Set @recipients = Replace(isnull((Select eMail from CPL.aCPL_Profiles where Job_FK = 11930), ''), ',', ';')

If isnull(@recipients, '') > ''
Begin

	Set @body = 'Your Customer Price List is ready.'

	Set @subject = '[' + (Select strPRofile_Name from CPL.aCPL_Profiles where Job_FK = @Job_FK) + '] Customer Price Template Results are now ready.'
	
	SET @ServiceBrokerCall = 'EXEC msdb.dbo.sp_send_dbmail ' 
	+ '@recipients = ''' + @recipients + ''', '
	+ '@body = ''' + @body + ''', '
	+ '@subject = ''' + @subject + ''' ;'

	EXEC queue_manager.enqueue
		@service_name = N'TargetEpaService', --  nvarchar(50)
		@cmd_text = @ServiceBrokerCall, --  nvarchar(4000)
		@epa_job_id = @Job_FK, --  int
		@cmd_type = N'SQL' --  nvarchar(10)

	--Print(@ServiceBrokerCall)
		
End

SET @status_desc = 'finished execution of CPL.Price_Template_Calculations_Generate_Rule_Based_Call_SB'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @Job_FK
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of CPL.Price_Template_Calculations_Rule_Based_Call_SB has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
