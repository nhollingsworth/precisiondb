﻿--A_MAUI_Price_Template_Calculations_Call_SB

CREATE PROCEDURE [CPL].[Price_Template_Calculations_Call_SB]
@Job_FK Varchar(64)
, @Job_Type Varchar(64) = Null
, @Eff Varchar(64) = Null
, @recipients varchar(128) = Null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           BIGINT
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @l_rows_processed     bigint
DECLARE  @v_count			  BIGINT

DECLARE  @v_input_rows bigint
DECLARE  @v_output_rows bigint
DECLARE  @v_output_total bigint
DECLARE  @v_exception_rows bigint
DECLARE  @v_job_date datetime
DECLARE  @v_sysdate datetime


DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of CPL.Price_Template_Calculations_Call_SB.'
					 + cast(isnull(@Job_FK, 0) as varchar(30))
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
                                          @epa_batch_no = @Job_FK,
						   				  @exec_id   = @l_exec_no OUTPUT;
--Declare @Job_FK Varchar(65)
--Declare @Job_Type Varchar(64)
--Declare @Eff Varchar(64)
--Declare @Result_Type_CR_FK Varchar(64)
--Declare @Prods Varchar(Max)
--Declare @Custs Varchar(Max)
--Declare @Orgs Varchar(Max)
--Declare @SALES_HISTORY_IND Varchar(64)
--Declare @recipients varchar(128)

--Set @Job_FK = '3105'
--Set @Job_Type = 'Pricing Template'
--Set @Eff = '2012-10-02'
--Set @SALES_HISTORY_IND = '1'
--Set @recipients = 'gstone@epacube.com'
	
Declare @ServiceBrokerCall Varchar(Max)

Declare @body varchar(Max)
Declare @subject varchar(128)

Set @Job_Type = isnull(@Job_Type, 'Null')
Set @Eff = isnull(@Eff, 'Null')

SET @ServiceBrokerCall = 'EXEC [CPL].[Price_Template_Calculations_Generate] ' 
+ Cast(@Job_FK as varchar(32)) + ', '''
+ @Job_Type + ''', '''
+ @Eff + ''''

EXEC queue_manager.enqueue
	@service_name = N'TargetEpaService', --  nvarchar(50)
	@cmd_text = @ServiceBrokerCall, --  nvarchar(4000)
	@epa_job_id = @Job_FK, --  int
	@cmd_type = N'SQL' --  nvarchar(10)
	
Print(@ServiceBrokerCall)

If isnull(@recipients, '') > ''
Begin
Declare @TemplateName as varchar(128)
Declare @Calcs as varchar(16)
Declare @CustsTemplate as varchar(64)
Declare @CustsValid as varchar(16) 
Declare @CustsInvalid as varchar(16)
Declare @ProdsTemplate as varchar(64)
Declare @ProdsValid as varchar(16) 
Declare @ProdsInvalid as varchar(16)

--Template Name
Set @TemplateName = (Select strName from epaCUBE.dbo.MAUI_Customer_Price_List_Profiles where Customer_Price_List_Profiles_ID = @Job_FK)

----Resulting Calculations
--Set @Calcs = (Select COUNT(*) Calculations from epaCUBE.dbo.MAUI_Customer_Price_List_Results where Customer_Price_List_Profiles_FK = @Job_FK)

--Customers
Set @CustsTemplate = (select strname from epaCUBE.dbo.MAUI_Customer_Price_List_Profiles where Customer_Price_List_Profiles_ID = (select Customer_Price_List_Template_FK from epaCUBE.dbo.MAUI_Customer_Price_List_Profiles where Customer_Price_List_Profiles_ID = @Job_FK))
Set @CustsValid = (select COUNT(*) 'Valid Customer Count 'from epaCUBE.dbo.MAUI_Customer_Price_List_Data_Load where Load_valid <> 0 and Price_List_Template_FK = (select Customer_Price_List_Template_FK from epaCUBE.dbo.MAUI_Customer_Price_List_Profiles where Customer_Price_List_Profiles_ID = @Job_FK))
Set @CustsInvalid = (select COUNT(*) 'Invalid Customer Count' from epaCUBE.dbo.MAUI_Customer_Price_List_Data_Load where Load_valid = 0 and Price_List_Template_FK = (select Customer_Price_List_Template_FK from epaCUBE.dbo.MAUI_Customer_Price_List_Profiles where Customer_Price_List_Profiles_ID = @Job_FK))

--Products
Set @ProdsTemplate = (select strname from epaCUBE.dbo.MAUI_Customer_Price_List_Profiles where Customer_Price_List_Profiles_ID = (select Product_Price_List_Template_FK from epaCUBE.dbo.MAUI_Customer_Price_List_Profiles where Customer_Price_List_Profiles_ID = @Job_FK))
Set @ProdsValid = (select COUNT(*) 'Valid Customer Count 'from epaCUBE.dbo.MAUI_Customer_Price_List_Data_Load where Load_valid <> 0 and Price_List_Template_FK = (select Product_Price_List_Template_FK from epaCUBE.dbo.MAUI_Customer_Price_List_Profiles where Customer_Price_List_Profiles_ID = @Job_FK))
Set @ProdsInvalid = (select COUNT(*) 'Invalid Customer Count' from epaCUBE.dbo.MAUI_Customer_Price_List_Data_Load where Load_valid = 0 and Price_List_Template_FK = (select Product_Price_List_Template_FK from epaCUBE.dbo.MAUI_Customer_Price_List_Profiles where Customer_Price_List_Profiles_ID = @Job_FK))


--Set @Body = '''Your Customer Price List is ready. 
--+ char(013) 
--+ char(013) 
--+ ''Customer Price List Name = ' + @TemplateName + '
--+ char(013) 
--+ char(013) 
--+ ''Total Calculations Generated = ' + @Calcs + '
--+ char(013) 
--+ char(013) 
--+ ''Customers Template Name = ' + @CustsTemplate + '
--+ char(013) 
--+ ''  Valid Customers in Template = ' + @CustsValid + '
--+ char(013) 
--+ ''  InValid Customers in Template = ' + @CustsInvalid + '

--+ char(013) 
--+ char(013) 
--+ ''Products Template Name = ' + @ProdsTemplate + '
--+ char(013) 
--+ ''  Valid Products in Template = ' + @ProdsValid + '
--+ char(013) 
--+ ''  InValid Products in Template = ' + @ProdsInvalid + ''

	Set @body = 'Your Customer Price List is ready.'

	Set @subject = '[' + (Select strName from epaCUBE.dbo.MAUI_Customer_Price_List_Profiles where Customer_Price_List_Profiles_ID = @Job_FK) + '] Customer Price List is now ready.'
	
	SET @ServiceBrokerCall = 'EXEC msdb.dbo.sp_send_dbmail ' 
	+ '@recipients = ''' + @recipients + ''', '
	+ '@body = ''' + @body + ''', '
	+ '@subject = ''' + @subject + ''' ;'

	EXEC queue_manager.enqueue
		@service_name = N'TargetEpaService', --  nvarchar(50)
		@cmd_text = @ServiceBrokerCall, --  nvarchar(4000)
		@epa_job_id = @Job_FK, --  int
		@cmd_type = N'SQL' --  nvarchar(10)

	--Print(@ServiceBrokerCall)
		
End

SET @status_desc = 'finished execution of CPL.Price_Template_Calculations_Call_SB'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc,
                                          @epa_batch_no = @Job_FK
Exec exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of CPL.Price_Template_Calculations_Call_SB has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
