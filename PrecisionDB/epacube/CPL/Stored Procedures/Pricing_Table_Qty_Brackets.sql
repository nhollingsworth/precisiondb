﻿--A_MAUI_Pricing_Table_Qty_Brackets

CREATE PROCEDURE [CPL].[Pricing_Table_Qty_Brackets]

	@Job_FK BigInt, @INSERT_TABLE VARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON;

--Declare @Job_FK Bigint
--Declare @INSERT_TABLE VARCHAR(64)
--Set @Job_FK = 10365
--Set @INSERT_TABLE = 'cpl.Price_List_Profile_'

Declare @SQLp1 Varchar(Max)
Declare @SQLp2 Varchar(Max)
Declare @SQLp3 Varchar(Max)
Declare @SQLp4 Varchar(Max)

Declare @CG1 Varchar(64)
Declare @CG2 Varchar(64)
Declare @CG3 Varchar(64)
Declare @PG1 Varchar(64)
Declare @PG2 Varchar(64)
Declare @PG3 Varchar(64)
Declare @PG4 Varchar(64)
Declare @PG5 Varchar(64)

Set @CG1 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Cust_Group1' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @CG2 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Cust_Group2' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @CG3 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Cust_Group3' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @PG1 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Prod_Group1' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @PG2 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Prod_Group2' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @PG3 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Prod_Group3' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @PG4 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Prod_Group4' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @PG5 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Prod_Group5' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''

--Set @INSERT_TABLE = 'cpl.Price_List_Profile_' + Cast(@Job_FK as Varchar(32))

Set @INSERT_TABLE = @INSERT_TABLE + Cast(@Job_FK as Varchar(32))

IF object_id('tempdb..##Tmp') IS NOT NULL
Drop Table ##Tmp

Set @SQLp1 = '

Declare @Job_FK Bigint
Set @Job_FK = ' + Cast(@Job_FK as Varchar(32)) + '

Update CPL_M
Set Break_Prices = BP.Qprices
, BracketQty1 = Case When BP.BracketQty1 = ''1 - 1'' then ''1'' else BP.BracketQty1 end
, BracketQty2 = BP.BracketQty2
, BracketQty3 = BP.BracketQty3
, BracketQty4 = BP.BracketQty4
, BracketQty5 = BP.BracketQty5
, BracketQty6 = BP.BracketQty6
, BracketQty7 = BP.BracketQty7
, BracketQty8 = BP.BracketQty8
, BracketPrice1 = BP.BracketPrice1
, BracketPrice2 = BP.BracketPrice2
, BracketPrice3 = BP.BracketPrice3
, BracketPrice4 = BP.BracketPrice4
, BracketPrice5 = BP.BracketPrice5
, BracketPrice6 = BP.BracketPrice6
, BracketPrice7 = BP.BracketPrice7
, BracketPrice8 = BP.BracketPrice8
from ' + @INSERT_TABLE + ' CPL_M 
Inner Join
(
Select
D.Customer_Price_List_Results_ID
, D.Qprices
, isnull(Case When D.BracketQty1 = ''1 - 0'' and Operation <> ''Discount'' then D.BracketQty2 else Replace(D.BracketQty1, ''1 - 0'', ''0'') end, ''1 +'') BracketQty1
, Case When D.BracketQty1 = ''1 - 0'' then D.BracketQty3 else D.BracketQty2 end BracketQty2
, Case When D.BracketQty1 = ''1 - 0'' then D.BracketQty4 else D.BracketQty3 end BracketQty3
, Case When D.BracketQty1 = ''1 - 0'' then D.BracketQty5 else D.BracketQty4 end BracketQty4
, Case When D.BracketQty1 = ''1 - 0'' then D.BracketQty6 else D.BracketQty5 end BracketQty5
, Case When D.BracketQty1 = ''1 - 0'' then D.BracketQty7 else D.BracketQty6 end BracketQty6
, Case When D.BracketQty1 = ''1 - 0'' then D.BracketQty8 else D.BracketQty7 end BracketQty7
, Case When D.BracketQty1 = ''1 - 0'' then Null else D.BracketQty8 end BracketQty8

, Isnull(Case When D.BracketQty1 = ''1 - 0'' and Operation <> ''Discount'' then D.BracketPrice2 else D.BracketPrice1 end, 
	Coalesce(D.BracketPrice2, D.BracketPrice3, D.BracketPrice4, D.BracketPrice5, D.BracketPrice6, D.BracketPrice7, D.BracketPrice8)) BracketPrice1
, Case When D.BracketQty1 = ''1 - 0'' then D.BracketPrice3 else D.BracketPrice2 end BracketPrice2
, Case When D.BracketQty1 = ''1 - 0'' then D.BracketPrice4 else D.BracketPrice3 end BracketPrice3
, Case When D.BracketQty1 = ''1 - 0'' then D.BracketPrice5 else D.BracketPrice4 end BracketPrice4
, Case When D.BracketQty1 = ''1 - 0'' then D.BracketPrice6 else D.BracketPrice5 end BracketPrice5
, Case When D.BracketQty1 = ''1 - 0'' then D.BracketPrice7 else D.BracketPrice6 end BracketPrice6
, Case When D.BracketQty1 = ''1 - 0'' then D.BracketPrice8 else D.BracketPrice7 end BracketPrice7
, Case When D.BracketQty1 = ''1 - 0'' then Null else D.BracketPrice8 end BracketPrice8
from ' + @INSERT_TABLE + ' CPL
inner Join (
Select Customer_Price_List_Results_ID
, Cast(Q1 as varchar(10)) + ''-$'' + Convert(Varchar(10), Convert(money, P1)) + ''; ''
	+ isnull(Case When isnull(p2, 0) > 0 then '' '' + Cast(Q2 as varchar(10)) + ''-$'' + Convert(Varchar(10), Convert(money, P2)) + ''; '' end, '''')
	+ isnull(Case When isnull(p3, 0) > 0 then '' '' + Cast(Q3 as varchar(10)) + ''-$'' + Convert(Varchar(10), Convert(money, P3)) + ''; '' end, '''')
	+ isnull(Case When isnull(p4, 0) > 0 then '' '' + Cast(Q4 as varchar(10)) + ''-$'' + Convert(Varchar(10), Convert(money, P4)) + ''; '' end, '''')
	+ isnull(Case When isnull(p5, 0) > 0 then '' '' + Cast(Q5 as varchar(10)) + ''-$'' + Convert(Varchar(10), Convert(money, P5)) + ''; '' end, '''')
	+ isnull(Case When isnull(p6, 0) > 0 then '' '' + Cast(Q6 as varchar(10)) + ''-$'' + Convert(Varchar(10), Convert(money, P6)) + ''; '' end, '''')
	+ isnull(Case When isnull(p7, 0) > 0 then '' '' + Cast(Q7 as varchar(10)) + ''-$'' + Convert(Varchar(10), Convert(money, P7)) + ''; '' end, '''')
	+ isnull(Case When isnull(p8, 0) > 0 then '' '' + Cast(Q8 as varchar(10)) + ''-$'' + Convert(Varchar(10), Convert(money, P8)) + ''; '' end, '''')
	+ isnull(Case When isnull(p9, 0) > 0 then '' '' + Cast(Q9 as varchar(10)) + ''-$'' + Convert(Varchar(10), Convert(money, P9)) + ''; '' end, '''') Qprices
, BracketQty1 = Case When isnull(P1, 0) = 0 then Null else Case When isnull(Q2, 0) = 0 then ''1 +'' else Cast(Q1 as Varchar(24)) + '' - '' + Cast(Q2-1 as VarChar(24)) End End
, BracketQty2 = Case When isnull(P2, 0) = 0 then Null When Q2 = 1 and Q3 = 2 then ''1'' When isnull(P3, 0) = 0 then Cast(Q2 as Varchar(24)) + '' +'' else Cast(Q2 as Varchar(24)) + '' - '' + Cast(Q3-1 as VarChar(24)) End
, BracketQty3 = Case When isnull(P3, 0) = 0 then Null When Q4 = 3 and Q3 = 2 then ''2'' When isnull(P4, 0) = 0 then Cast(Q3 as Varchar(24)) + '' +'' else Cast(Q3 as Varchar(24)) + '' - '' + Cast(Q4-1 as VarChar(24)) End
, BracketQty4 = Case When isnull(P4, 0) = 0 then Null When Q5 = 4 and Q4 = 3 then ''3'' When isnull(P5, 0) = 0 then Cast(Q4 as Varchar(24)) + '' +'' else Cast(Q4 as Varchar(24)) + '' - '' + Cast(Q5-1 as VarChar(24)) End
, BracketQty5 = Case When isnull(P5, 0) = 0 then Null When isnull(P6, 0) = 0 or isnull(P7, 0) = 0 then Cast(Q5 as Varchar(24)) + '' +'' else Cast(Q5 as Varchar(24)) + '' - '' + Cast(Q6-1 as VarChar(24)) End
, BracketQty6 = Case When isnull(P6, 0) = 0 or isnull(P7, 0) = 0 then Null When isnull(P7, 0) = 0 then Cast(Q6 as Varchar(24)) + '' +'' else Cast(Q6 as Varchar(24)) + '' - '' + Cast(Q7-1 as VarChar(24)) End
, BracketQty7 = Case When isnull(P7, 0) = 0 then Null When isnull(P8, 0) = 0 then Cast(Q7 as Varchar(24)) + '' +'' else Cast(Q7 as Varchar(24)) + '' - '' + Cast(Q8-1 as VarChar(24)) End
, BracketQty8 = Case When isnull(P8, 0) = 0 then Null When isnull(P9, 0) = 0 then Cast(Q8 as Varchar(24)) + '' +'' else Cast(Q8 as Varchar(24)) + '' - '' + Cast(Q9-1 as VarChar(24)) End
, BracketPrice1 = Convert(money, P1)
, BracketPrice2 = Convert(money, P2)
, BracketPrice3 = Convert(money, P3)
, BracketPrice4 = Convert(money, P4)
, BracketPrice5 = Convert(money, P5)
, BracketPrice6 = Convert(money, P6)
, BracketPrice7 = Convert(money, P7)
, BracketPrice8 = Convert(money, P8)
From
(Select
Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1 then Q2 Else Q1 End Q1
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then Q3 Else Q2 End Q2
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then Q4 Else Q3 End Q3
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then Q5 Else Q4 End Q4
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then Q6 Else Q5 End Q5
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then Q7 Else Q6 End Q6
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then Q8 Else Q7 End Q7
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then Q9 Else Q8 End Q8
, Q9
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then P2 Else P1 End P1
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then P3 Else P2 End P2
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then P4 Else P3 End P3
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then P5 Else P4 End P4
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then P6 Else P5 End P5
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then P7 Else P6 End P6
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then P8 Else P7 End P7
, Case When Model_Qty <= 1 and O1 >0 and Q1 <> 1  then P9 Else P8 End P8
, p9
--, P1, p2, p3, p4, p5, p6, p7, p8, p9
, Customer_Price_List_Results_ID
, [Pricing Record #]
'
Set @SQLp2 = '
From
(Select O1
, 1 ''Q1''
, Case Operation	When ''Gross Margin'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - Isnull(O1, 0)/100)
					When ''% GM'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - Isnull(O1, 0)/100)
					When ''Multiplier'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * Isnull(O1, 0)/100
					When ''Discount'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * (1 - Isnull(O1, 0)/100) 
					When ''Dflt to BASE'' then O1
					When ''Dflt to LIST'' then O1 
					When ''FIXED VALUE'' then O1 End P1
, Q1 ''Q2''
,Case Operation	When ''Gross Margin'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O2/100)
					When ''% GM'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O2/100)
					When ''Multiplier'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * O2/100
					When ''Fixed Value'' Then O2
					When ''Discount'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * (1 - O2/100) 
					When ''Dflt to BASE'' then O2
					When ''Dflt to LIST'' then O2 
					When ''FIXED VALUE'' then O2 End P2
, Q2 ''Q3''
,Case Operation	When ''Gross Margin'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O3/100)
					When ''% GM'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O3/100)
					When ''Multiplier'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * O3/100
					When ''Fixed Value'' Then O3
					When ''Discount'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * (1 - O3/100) 
					When ''Dflt to BASE'' then O3
					When ''Dflt to LIST'' then O3 
					When ''FIXED VALUE'' then O3 End P3
, Q3 ''Q4''
,Case Operation	When ''Gross Margin'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O4/100)
					When ''% GM'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O4/100)
					When ''Multiplier'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * O4/100
					When ''Fixed Value'' Then O4
					When ''Discount'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * (1 - O4/100) 
					When ''Dflt to BASE'' then O4
					When ''Dflt to LIST'' then O4 
					When ''FIXED VALUE'' then O4 End P4
, Q4 ''Q5''
,Case Operation	When ''Gross Margin'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O5/100)
					When ''% GM'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O5/100)
					When ''Multiplier'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * O5/100
					When ''Fixed Value'' Then O5
					When ''Discount'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * (1 - O5/100) 
					When ''Dflt to BASE'' then O5
					When ''Dflt to LIST'' then O5 
					When ''FIXED VALUE'' then O5 End P5
, Q5 ''Q6''
,Case Operation	When ''Gross Margin'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O6/100)
					When ''% GM'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O6/100)
					When ''Multiplier'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * O6/100
					When ''Fixed Value'' Then O6
					When ''Discount'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * (1 - O6/100) 
					When ''Dflt to BASE'' then O6
					When ''Dflt to LIST'' then O6 
					When ''FIXED VALUE'' then O6 End P6
, Q6 ''Q7''
,Case Operation	When ''Gross Margin'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O7/100)
					When ''% GM'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O7/100)
					When ''Multiplier'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * O7/100
					When ''Fixed Value'' Then O7
					When ''Discount'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * (1 - O7/100) 
					When ''Dflt to BASE'' then O7
					When ''Dflt to LIST'' then O7 
					When ''FIXED VALUE'' then O7 End P7
, Q7 ''Q8''
,Case Operation	When ''Gross Margin'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O8/100)
					When ''% GM'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O8/100)
					When ''Multiplier'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * O8/100
					When ''Fixed Value'' Then O8
					When ''Discount'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * (1 - O8/100) 
					When ''Dflt to BASE'' then O8
					When ''Dflt to LIST'' then O8 
					When ''FIXED VALUE'' then O8 End P8
, Q8 ''Q9''
,Case Operation	When ''Gross Margin'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O9/100)
					When ''% GM'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) / (1 - O9/100)
					When ''Multiplier'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * O9/100
					When ''Fixed Value'' Then O9
					When ''Discount'' Then (Case When [Basis_Col] = ''Rbted Cost'' and isnull([Rebate], 0) > 0 then [Rebate] * -1 else 0 end + Basis) * (1 - O9/100) 
					When ''Dflt to BASE'' then O9
					When ''Dflt to LIST'' then O9 
					When ''FIXED VALUE'' then O9 End P9
, Customer_Price_List_Results_ID
, [Pricing Record #]
, Operation
, Model_Qty
'
Set @SQLp3 = '
from
(Select distinct
[Pricing Record #]
, sell_price_cust_rules_fk
, ra.rules_action_id rules_action_fk
, Operation

, Replace((Select Name from epacube.epacube.data_name with (nolock) where data_name_id = RA.Basis_Calc_DN_FK), ''REBATED COST CB AMT'', ''Rbted Cost'') Basis_Col
, Case RA.Basis_Calc_DN_FK
	When 231103 then [Base_Price]
	When 231104 then [List_Price]
	When 231101 then [Replacement_Cost]
	When 231102 then [Standard_Cost]
	When 231105 then [Vendor_Rebate_Cost]
	When 111401 Then
		Case (Select Value from epacube.epacube.epacube_params with (nolock) where [Name] = ''PRICING COST BASIS'') When 231101 then [Replacement_Cost] when 231102 then [Standard_Cost] when 231103 then [Base_Price] when 231104 then [List_Price] end end ''Basis''
, Rebate
, rao.operand1 O1, rao.operand2 O2, rao.operand3 O3, rao.operand4 O4, rao.operand5 O5, rao.operand6 O6, rao.operand7 O7, rao.operand8 O8, rao.operand9 O9
, rao.Filter_Value1 Q1, rao.Filter_Value2 Q2, rao.Filter_Value3 Q3, rao.Filter_Value4 Q4, rao.Filter_Value5 Q5, rao.Filter_Value6 Q6, rao.Filter_Value7 Q7, rao.Filter_Value8 Q8, rao.Filter_Value9 Q9
, Customer_Price_List_Results_ID--, rao.Filter_Value1 Qty_Init
, 1 Model_Qty
from ' + @INSERT_TABLE + ' MCPL with (NoLock)
inner join epacube.synchronizer.rules_action ra with (NoLock) on ra.rules_fk = MCPL.sell_price_cust_rules_fk
inner join epacube.synchronizer.rules_action_operands rao with (NoLock) on ra.rules_action_id = rao.rules_action_fk and rao.operand_filter_operator_cr_fk = 9020
where MCPL.Job_FK = @Job_FK
) A) B) C) D
On CPL.Customer_Price_List_Results_ID = D.Customer_Price_List_Results_ID) BP
On CPL_M.Customer_Price_List_Results_ID = BP.Customer_Price_List_Results_ID

Select * into ##tmp 
from ' + @INSERT_TABLE + ' with (NoLock) where job_fk = @Job_FK and [Product_ID] = ''Multi''

Delete from ' + @INSERT_TABLE + ' where Job_FK = @Job_FK and [Product_ID] = ''Multi''
'

Set @SQLp4 = '
Insert into ' + @INSERT_TABLE + '
([Bill-To Customer ID], [Bill-To Customer Name], [Ship-To Customer ID], [Ship-To Customer Name], [Product_ID], [Product_Description], [Sell Price], [Pricing Cost], [Rebate], [Rebate_Cap_Sell], [Replacement_Cost], [Standard_Cost], [Base_Price], [List_Price], [Vendor_Rebate_Cost], [Whse_ID], [Vendor_ID], [Rebate Record #], [Pricing Record #], [Sell_Price_Cust_Rules_FK], [Operation], [Prices_Calculated_Date], [Product_Structure_FK], [Org_Entity_Structure_FK], [Cust_Entity_Structure_FK], [Supl_Entity_Structure_FK], [WAREHOUSE_RECORD], [SALESREP_RECORD_IN], [SALESREP_RECORD_OUT], [' + @CG1 + '], [' + @CG2 + '], [' + @CG3 + '], [' + @PG1 + '], [' + @PG2 + '], [' + @PG3 + ' ], [' + @PG4 + '], [' + @PG5 + '], [' + @pg1 + '_Desc], [' + @PG2 + '_Desc], [' + @PG3 + '_Desc], [' + @PG4 + '_Desc], [' + @PG5 + '_Desc], [SPC_UOM], [Break_Prices], [BracketQty1], [BracketQty2], [BracketQty3], [BracketQty4], [BracketQty5], [BracketQty6], [BracketQty7], [BracketQty8], [BracketQty9], [BracketPrice1], [BracketPrice2], [BracketPrice3], [BracketPrice4], [BracketPrice5], [BracketPrice6], [BracketPrice7], [BracketPrice8], [BracketPrice9], [CreatedBy], [Customer_Price_List_Sort_FK], [Job_FK], [Stocking_UOM])
Select distinct
MCPL.[Bill-To Customer ID], MCPL.[Bill-To Customer Name], MCPL.[Ship-To Customer ID], MCPL.[Ship-To Customer Name], MCPL.[Product_ID], MCPL.[Product_Description], MCPL.[Sell Price], MCPL.[Pricing Cost], MCPL.[Rebate], MCPL.[Rebate_Cap_Sell], MCPL.[Replacement_Cost], MCPL.[Standard_Cost], MCPL.[Base_Price], MCPL.[List_Price], MCPL.[Vendor_Rebate_Cost], MCPL.[Whse_ID], MCPL.[Vendor_ID], MCPL.[Rebate Record #], MCPL.[Pricing Record #], MCPL.[Sell_Price_Cust_Rules_FK], MCPL.[Operation], MCPL.[Prices_Calculated_Date], MCPL.[Product_Structure_FK], MCPL.[Org_Entity_Structure_FK], MCPL.[Cust_Entity_Structure_FK], MCPL.[Supl_Entity_Structure_FK], MCPL.[WAREHOUSE_RECORD], MCPL.[SALESREP_RECORD_IN], MCPL.[SALESREP_RECORD_OUT], MCPL.[' + @CG1 + '], MCPL.[' + @CG2 + '], MCPL.[' + @CG3 + '], MCPL.[' + @PG1 + '], MCPL.[' + @PG2 + '], MCPL.[' + @PG3 + ' ], MCPL.[' + @PG4 + '], MCPL.[' + @PG5 + '], MCPL.[' + @pg1 + '_Desc], MCPL.[' + @PG2 + '_Desc], MCPL.[' + @PG3 + '_Desc], MCPL.[' + @PG4 + '_Desc], MCPL.[' + @PG5 + '_Desc], MCPL.[SPC_UOM], UPD.[BreakPrice], UPD.[BracketQty1], UPD.[BracketQty2], UPD.[BracketQty3], UPD.[BracketQty4], UPD.[BracketQty5], UPD.[BracketQty6], UPD.[BracketQty7], UPD.[BracketQty8], UPD.[BracketQty9], UPD.[BracketPrice1], UPD.[BracketPrice2], UPD.[BracketPrice3], UPD.[BracketPrice4], UPD.[BracketPrice5], UPD.[BracketPrice6], UPD.[BracketPrice7], UPD.[BracketPrice8], UPD.[BracketPrice9], MCPL.[CreatedBy], MCPL.[Customer_Price_List_Sort_FK], MCPL.[Job_FK], MCPL.[Stocking_UOM]
from ##tmp MCPL
Left Join
(select CPL.[Product_ID], CPL.Product_Structure_fk, PGd.Product_Groupings_FK, PV.Variation, PG.Description, CPL.Product_Description 
, CPL.Break_Prices BreakPrice, CPL.[Sell Price]
, CPL.BracketQty1, CPL.BracketQty2, CPL.BracketQty3, CPL.BracketQty4, CPL.BracketQty5, CPL.BracketQty6, CPL.BracketQty7, CPL.BracketQty8, CPL.BracketQty9, CPL.BracketPrice1, CPL.BracketPrice2, CPL.BracketPrice3, CPL.BracketPrice4, CPL.BracketPrice5, CPL.BracketPrice6, CPL.BracketPrice7, CPL.BracketPrice8, CPL.BracketPrice9
from ' + @INSERT_TABLE + '  CPL with (NoLock)
inner join epacube.dbo.MAUI_Products_Grouped PGd with (NoLock) on CPL.Product_Structure_FK = Pgd.product_structure_FK
inner join epacube.dbo.MAUI_Product_Groupings PG with (NoLock) on PGd.Product_Groupings_FK = PG.Product_Groupings_ID
Left Join epacube.dbo.MAUI_Product_Variations PV with (NoLock) on CPL.Product_Structure_FK = PV.Product_Structure_FK and PGd.Product_Groupings_FK = PV.Product_Groupings_FK) UPD
on MCPL.Product_Description = UPD.Description and isnull(MCPL.Variations, '''') like ''%'' + isnull(isnull(UPD.Variation, MCPL.Variations), '''') + ''%''
Where MCPL.[Product_ID] = ''Multi'' 
--and BreakPrice Is Not Null 
and Job_FK = @Job_FK

Update MCPL
Set Break_Prices = UPD.BreakPrice
from ' + @INSERT_TABLE + '  MCPL
Inner Join
(select CPL.[Product_ID], CPL.Product_Structure_fk, PGd.Product_Groupings_FK, PV.Variation, PG.Description, CPL.Product_Description 
, CPL.Break_Prices BreakPrice, CPL.[Sell Price]
from ' + @INSERT_TABLE + '  CPL with (NoLock)
inner join epacube.dbo.MAUI_Products_Grouped PGd with (NoLock) on CPL.Product_Structure_FK = Pgd.product_structure_FK
inner join epacube.dbo.MAUI_Product_Groupings PG with (NoLock) on PGd.Product_Groupings_FK = PG.Product_Groupings_ID
Left Join epacube.dbo.MAUI_Product_Variations PV with (NoLock) on CPL.Product_Structure_FK = PV.Product_Structure_FK and PGd.Product_Groupings_FK = PV.Product_Groupings_FK) UPD
on MCPL.Product_Description = UPD.Description and MCPL.Variations like ''%'' + isnull(UPD.Variation, MCPL.Variations) + ''%''
Where MCPL.[Product_ID] = ''Multi'' and BreakPrice Is Not Null and isnull(MCPL.Break_Prices, '''') = '''' and Job_FK = @Job_FK

Delete from ' + @INSERT_TABLE + '  where product_structure_fk in 
(
select CPL.Product_Structure_fk
from ' + @INSERT_TABLE + '  CPL with (NoLock)
inner join epacube.dbo.MAUI_Products_Grouped PGd with (NoLock) on CPL.Product_Structure_FK = Pgd.product_structure_FK
inner join epacube.dbo.MAUI_Product_Groupings PG with (NoLock) on PGd.Product_Groupings_FK = PG.Product_Groupings_ID
Left Join epacube.dbo.MAUI_Product_Variations PV with (NoLock) on CPL.Product_Structure_FK = PV.Product_Structure_FK and PGd.Product_Groupings_FK = PV.Product_Groupings_FK
inner join ' + @INSERT_TABLE + '  CPL2 with (NoLock) on PG.Description = CPL2.Product_Description and CPL2.job_fk = @Job_FK
)
and Job_FK = @Job_FK and product_id <> ''Multi''

Delete from ' + @INSERT_TABLE + '  where Job_FK = @Job_FK and Customer_Price_List_Results_ID not in
(select distinct min(Customer_Price_List_Results_ID) Customer_Price_List_Results_ID
from ' + @INSERT_TABLE + '  with (NoLock) where Job_FK = @Job_FK
group by [Product_ID], Product_Description, stocking_uom, isnull([Bill-To Customer ID], [Ship-To Customer ID]), Whse_ID, Case when isnull(break_Prices, '''') = '''' then cast([sell price] as varchar(32)) else break_Prices end)
'
Exec(@SQLp1 + @SQLp2 + @SQLp3 + @SQLp4)

--print(@SQLp1)
--print(@SQLp2)
--print(@SQLp3)
--print(@SQLp4)

End
