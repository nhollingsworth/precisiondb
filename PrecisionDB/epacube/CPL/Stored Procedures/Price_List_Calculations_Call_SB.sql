﻿--A_MAUI_Price_List_Calculations_Call_SB

-- =============================================
-- Author:		Gary Stone
-- Create date: April, 2013
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [CPL].[Price_List_Calculations_Call_SB]
@Job_FK Varchar(64)
, @Job_Type Varchar(64)
, @Eff Varchar(32)
, @Prods Varchar(Max)
, @Custs Varchar(Max)
, @Orgs Varchar(Max)
, @WinUser Varchar(64)
, @eMailAddress Varchar(64)
, @AliasPreparedBy Varchar(256)
, @ServiceBroker int
, @Exclude_Promos_Ind int

AS
BEGIN

Declare @TmpTbl Varchar(96)
Set @TmpTbl = '##MM_CPL_Tbl'
--Set @TmpTbl = '##MM_CPL_Tbl_' + replace(@WinUser, '''', '')

IF object_id('tempdb..' + @TmpTbl) is not null
exec('drop table ' + @TmpTbl)

Exec('Select ''' + @Job_FK + ''' Job_FK, ''''' + @Job_Type + ''''' Job_Type, ''''' + @Eff + ''''' Eff, ''''' + @Prods + ''''' Prods, ''''' + @Custs + ''''' Custs, ''''' + @Orgs + ''''' Orgs, ''''' + @WinUser + ''''' WinUser, ''''' + @eMailAddress + ''''' eMailAddress, ''''' + @AliasPreparedBy + ''''' AliasPreparedBy, ''' + @Exclude_Promos_Ind + ''' Exclude_Promos_Ind into ' + @TmpTbl)

Update CPLP
Set intProcess = 1
, datRecordEffectiveDate = Cast(Replace(@Eff, '''', '') as datetime)
from epacube.dbo.MAUI_Customer_Price_List_Profiles CPLP where Customer_Price_List_Profiles_ID = @Job_FK

Declare @ServiceBrokerCall nvarchar(4000)

Set @ServiceBrokerCall = 'EXEC CPL.Price_List_Calculations_Generate '

If @ServiceBroker = 1 			
Begin
	EXEC queue_manager.enqueue
	@service_name = N'TargetEpaService', --  nvarchar(50)
	@cmd_text = @ServiceBrokerCall, --  nvarchar(4000)
	@epa_job_id = @Job_FK, --  int
	@cmd_type = N'SQL' --  nvarchar(10)

	Declare @Body varchar(128), @subject varchar(128)
	Set @body = 'Your Customer Price List is ready.'
	Set @subject = '[' + (Select strName from epacube.dbo.MAUI_Customer_Price_List_Profiles where Customer_Price_List_Profiles_ID = @Job_FK) + '] Customer Price List is now ready.'
	Set @eMailAddress = Substring(@eMailAddress, 2, Len(@eMailAddress) - 2)

	Exec epacube.eMail_Notification_Thru_Service_Broker @Job_FK, @body, @subject, @eMailAddress

End
Else
	Exec(@ServiceBrokerCall)
END
