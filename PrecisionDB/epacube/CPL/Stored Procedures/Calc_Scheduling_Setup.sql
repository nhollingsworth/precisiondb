﻿--A_MAUI_Reports_Scheduling_Setup

-- =============================================
-- Author:		Gary Stone
-- Create date: July 7, 2014
-- =============================================
CREATE PROCEDURE [CPL].[Calc_Scheduling_Setup] 
	@Job_FK int
AS
BEGIN
	SET NOCOUNT ON;

Declare @sysdate datetime
set @sysdate = Cast(Convert(varchar(10), getdate(), 1) as datetime)
--Declare @Report_ID int
--Set @Report_ID = 1000

Update CPL
Set CalcDateNext = 
	Case
		When CPL.SchedCreateAnnually is not null
		then
			Case When Cast(Cast(DATEPART(m, CPL.SchedCreateAnnually) as varchar(2)) + '/' + Cast(DATEPART(d, CPL.SchedCreateAnnually) as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime) >= @sysdate then
				Cast(Cast(DATEPART(m, CPL.SchedCreateAnnually) as varchar(2)) + '/' + Cast(DATEPART(d, CPL.SchedCreateAnnually) as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime)
				else  Cast(Cast(DATEPART(m, CPL.SchedCreateAnnually) as varchar(2)) + '/' + Cast(DATEPART(d, CPL.SchedCreateAnnually) as varchar(2)) + '/' + Cast(YEAR(@sysdate) + 1 as varchar(4)) AS datetime) end 
		When CPL.SchedCreateDayOfMonth is not null and isnumeric(CPL.SchedCreateDayOfMonth) = 1 then
			Case when Cast(Cast(DATEPART(m, @sysdate) as varchar(2)) + '/' + Cast(CPL.SchedCreateDayOfMonth as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime) >= @sysdate then
				Cast(Cast(DATEPART(m, @sysdate) as varchar(2)) + '/' + Cast(CPL.SchedCreateDayOfMonth as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime)
				else DATEADD(m,1,Cast(Cast(DATEPART(m, @sysdate) as varchar(2)) + '/' + Cast(CPL.SchedCreateDayOfMonth as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime)) end
		When CPL.SchedCreateDayOfMonth is not null and CPL.SchedCreateDayOfMonth = 'Last' then Cast(Convert(varchar(10), Dateadd(m,1,@sysdate) - DAY(Dateadd(m,1,@sysdate)), 1) as datetime)
		When CPL.SchedCreateDay is not null and (CPL.SchedCreateDay = 0 or CPL.SchedCreateDay = DATEPART(dw, @sysdate)) then Cast(Convert(varchar(10), @sysdate, 1) as datetime)
		When CPL.SchedCreateDay is not null then Cast(Convert(varchar(10), @sysdate + CPL.SchedCreateDay - DATEPART(dw, @sysdate) + Case when CPL.SchedCreateDay - DATEPART(dw, @sysdate) < 0 then 7 else 0 end, 1) as datetime)
	end 
from CPL.aCPL_Profiles CPL where Job_FK = @Job_FK

Update CPL
Set PricingEffectiveDateNext = 
	Case
		--When Pricing Date same as Report Date
		When CPL.SchedPricingEffectiveDay = -1 then CPL.CalcDateNext
		--When Pricing Date the next month from  Date
		When CPL.SchedPricingFrequency = 1 then 
			Case CPL.SchedPricingEffectiveDay when 0 then DATEADD(m, 1, CPL.CalcDateNext) - DAY(DATEADD(m, 1, CPL.CalcDateNext)) + 1 
				else DATEADD(m, 1, CPL.CalcDateNext) - DAY(DATEADD(m, 1, CPL.CalcDateNext)) + 1 
					+ CPL.SchedPricingEffectiveDay - DATEPART(dw, DATEADD(m, 1, CPL.CalcDateNext) - DAY(DATEADD(m, 1, CPL.CalcDateNext)) + 1)
					+ Case when CPL.SchedPricingEffectiveDay - DATEPART(dw, DATEADD(m, 1, CPL.CalcDateNext) - DAY(DATEADD(m, 1, CPL.CalcDateNext)) + 1) < 0 then 7 else 0 end
			End
		--When Pricing Date the next week from  Date
		When CPL.SchedPricingFrequency = 2 then 	
			CPL.CalcDateNext - DATEPART(dw, CPL.CalcDateNext) + 7 + Case CPL.SchedPricingEffectiveDay when 0 then 1 else CPL.SchedPricingEffectiveDay end
	end 
from CPL.aCPL_Profiles CPL where Job_FK = @Job_FK

END
