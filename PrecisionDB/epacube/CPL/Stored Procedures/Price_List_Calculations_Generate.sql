﻿--A_MAUI_Price_List_Calculations_Generate

--April 28, 2013	GHS		Updated to Support Service Broker Calls and Email Notification when Complete
--May 12, 2013		GHS		Updated to Support Promos Exclusion

CREATE PROCEDURE [CPL].[Price_List_Calculations_Generate]

AS
BEGIN

	SET NOCOUNT ON;

Declare @Job_FK Int
Declare @Job_Type Varchar(64)
Declare @Eff Varchar(32)
Declare @Eff_Date datetime
Declare @Prods Varchar(Max)
Declare @Custs Varchar(Max)
Declare @Orgs Varchar(Max)
Declare @WinUser Varchar(64)
Declare @eMailAddress Varchar(64)
Declare @AliasPreparedBy Varchar(256)
Declare @Exclude_Promos_Ind Int

Set @Job_FK = (Select Job_FK from ##MM_CPL_Tbl)
Set @Job_Type = (Select Job_Type from ##MM_CPL_Tbl)
Set @Eff = (Select Eff from ##MM_CPL_Tbl)
Set @Eff_Date = Cast(Replace((Select Eff from ##MM_CPL_Tbl), '''', '') as datetime)
Set @Prods = (Select Replace(Replace(Prods, 'epacube.', 'epacube.epacube.'), 'dbo.', 'epacube.dbo.') from ##MM_CPL_Tbl)
Set @Custs = (Select Custs from ##MM_CPL_Tbl)
Set @Orgs = (Select Orgs from ##MM_CPL_Tbl)
Set @WinUser = (Select WinUser from ##MM_CPL_Tbl)
Set @eMailAddress = (Select eMailAddress from ##MM_CPL_Tbl)
Set @AliasPreparedBy = (Select AliasPreparedBy from ##MM_CPL_Tbl)
Set @Exclude_Promos_Ind = (Select Exclude_Promos_Ind from ##MM_CPL_Tbl)

Set @Prods = Replace(substring(@Prods, 2, Len(@Prods) - 2), '''''', '''')
Set @Custs = Replace(substring(@Custs, 2, Len(@Custs) - 2), '''''', '''')
Set @Orgs = Replace(substring(@Orgs, 2, Len(@Orgs) - 2), '''''', '''')

set @WinUser = Replace(@WinUser, '''', '')
set @eMailAddress = Replace(@eMailAddress, '''', '')
set @AliasPreparedBy = Replace(@AliasPreparedBy, '''', '')

Declare @ProfileName Varchar(64)
Declare @Result_Type_CR_FK Int
Declare @INSERT_TABLE VARCHAR(64)
Declare @COST_CHANGE_FIRST_IND Int
Declare @COST_CHANGE_PRIOR_IND Int
Declare @SQLp Varchar(Max)
Declare @StartTime datetime
Set @StartTime = GETDATE()

Update CPLP
	Set intProcess = 2
	from epacube.dbo.MAUI_Customer_Price_List_Profiles CPLP where Customer_Price_List_Profiles_ID = @Job_FK

Set @Result_Type_CR_FK = Case when ISNULL(@Eff_Date, getdate()) > getdate() then 711 else 710 end

Set @COST_CHANGE_FIRST_IND = Case When @Result_Type_CR_FK = 711 and @Eff_Date is Null then 1 else 0 end
Set @COST_CHANGE_PRIOR_IND = Case When @Result_Type_CR_FK = 711 and @Eff_Date is not Null then 1 else 0 end

EXEC marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, Null

Delete from epacube.marginmgr.ANALYSIS_JOB where Analysis_Job_ID = @Job_FK

Insert into epacube.marginmgr.ANALYSIS_JOB
(Analysis_Job_ID, Analysis_Name, result_type_cr_fk, result_effective_date, rule_type_filter, SALES_HISTORY_IND, COST_CHANGE_FIRST_IND, COST_CHANGE_PRIOR_IND, Product_Filter, Product_Whse_Ind, Organization_Filter, Customer_Filter, Exclude_Promos_Ind)
Select @Job_FK, '''MAUI ' + Replace(@Job_Type, '''', '') + '''', @Result_Type_CR_FK, @Eff_Date, '(312, 313)', Null SALES_HISTORY_IND, @COST_CHANGE_FIRST_IND, @COST_CHANGE_PRIOR_IND, @Prods, 1, @Orgs, @Custs, @Exclude_Promos_Ind

EXEC  [marginmgr].[CALCULATION_PROCESS] @Job_FK

Exec [CPL].[Pricing_Table_Create] @Job_FK, @AliasPreparedBy, @WinUser, 'cpl.Price_List_Profile_'

Exec [CPL].[Pricing_Table_Qty_Brackets] @Job_FK, 'cpl.Price_List_Profile_'

exec marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, Null

Update Profiles
Set intProcess = 3
, Template_Process_Date = Getdate()
, strProcessingTime = Convert(Varchar(10),GETDATE() - @StartTime, 108)
, datRecordEffectiveDate = Convert(Varchar(10),GETDATE(), 101)
from epacube.dbo.MAUI_Customer_Price_List_Profiles Profiles
Where 1 = 1
and Customer_Price_List_Profiles_ID = @Job_FK

end
