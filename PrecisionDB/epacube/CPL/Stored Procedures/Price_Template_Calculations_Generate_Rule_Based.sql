﻿--A_MAUI_Price_Template_Calculations_Generate_Rule_Based

CREATE PROCEDURE [CPL].[Price_Template_Calculations_Generate_Rule_Based]
@Job_FK BigInt
, @Eff datetime = Null
, @Wuser varchar(64) = ''
--, @GetCountsOnly Int = 0

AS
BEGIN

--Modified 09/7/2015 update status to 'Processing' once invoked 
--Modified 10/21/2015 to add Customer Price Type as basis
	
	SET NOCOUNT ON;
	
--Declare @Job_FK bigint
--Declare @Eff datetime
--Declare @Wuser varchar(64)

--Set @Job_FK = 11930
--Set @Eff = CAST(getdate() as DATE)
--Set @Wuser = 'gstone'

Declare @Profiles_Job_FK bigint
Set @Profiles_Job_FK = (select customer_template_fk from CPL.aCPL_Profiles where Job_FK = @Job_FK)

Declare @Job_Type Varchar(64)
Declare @StartTime datetime
Declare @SQL_CD varchar(Max)
Declare @COST_CHANGE_FIRST_IND Int
Declare @COST_CHANGE_PRIOR_IND Int
Declare @Result_Type_CR_FK int
Declare @RuleTypeFilter Varchar(16)

DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             VARCHAR (1000)
DECLARE  @l_exec_no           bigint
DECLARE  @status_desc		  varchar (max)

SET @l_sysdate = getdate ()
SET @ls_stmt = 'Started Execution of cpl.Price_Template_Calculations_Generate' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;

IF object_id('tempdb..#Identifiers') is not null
Drop Table #Identifiers

IF object_id('tempdb..#Identifiers1') is not null
Drop Table #Identifiers1
IF object_id('tempdb..#Identifiers2') is not null
Drop Table #Identifiers2
IF object_id('tempdb..#Identifiers3') is not null
Drop Table #Identifiers3
IF object_id('tempdb..#Whses1') is not null
Drop Table #Whses1
IF object_id('tempdb..#Whses') is not null
Drop Table #Whses

Update Profiles
Set intProcess = 2
from cpl.aCPL_Profiles Profiles
Where 1 = 1
and Job_FK = @Job_FK

Set @Eff =				Case when isnull(@Eff, '') = '' or @Eff = '19000101' then isnull((Select Pricing_Effective_Date from cpl.aCPL_Profiles where Job_FK = @Job_FK), GETDATE()) else @EFF End
Set @Wuser =			Replace(Case when isnull(@Wuser, '') = '' then isnull((Select Create_User from cpl.aCPL_Profiles where Job_FK = @Job_FK), 'epacube') else @Wuser End, ' ', '_')
Set @RuleTypeFilter =	Case (Select intRuleTypeFilter from cpl.aCPL_Profiles where Job_FK = @Job_FK)
						When 1 then '(313)'
						else '(312, 313)' end

Select Cast(Null as bigint) Job_FK, r.result_data_name_fk, emt.ENTITY_STRUCTURE_FK Cust_Entity_Structure_FK, ei_v.entity_structure_fk Supl_Entity_Structure_FK
, pi.PRODUCT_STRUCTURE_FK, isnull(ei_o2.entity_structure_fk, ei_o.ENTITY_STRUCTURE_FK) Org_Entity_Structure_FK
into #identifiers
from epacube.epacube.DATA_VALUE dv with (nolock)
inner join epacube.epacube.entity_mult_type emt with (nolock) on dv.DATA_NAME_FK = emt.DATA_NAME_FK and dv.DATA_VALUE_ID = emt.DATA_VALUE_FK
inner join epacube.synchronizer.RULES_FILTER rf_crt with (nolock) on dv.VALUE = rf_crt.VALUE1 and dv.DATA_NAME_FK = rf_crt.DATA_NAME_FK
inner join epacube.synchronizer.RULES_FILTER_SET rfs with (nolock) on rf_crt.RULES_FILTER_ID = rfs.CUST_FILTER_FK
inner join epacube.synchronizer.RULES r with (nolock) on rfs.RULES_FK = r.rules_id
inner join [CPL].[aCPL_Template_Data] CTD with (nolock) on dv.data_name_fk = CTD.data_name_fk and dv.value = CTD.value
left join epacube.synchronizer.RULES_FILTER rf_v with (nolock) on rfs.SUPL_FILTER_FK = rf_v.RULES_FILTER_ID
left join epacube.epacube.ENTITY_IDENTIFICATION ei_v with (nolock) on rf_v.ENTITY_DATA_NAME_FK = ei_v.entity_data_name_fk and rf_v.DATA_NAME_FK = ei_v.DATA_NAME_FK
	and rf_v.VALUE1 = ei_v.VALUE
left join epacube.synchronizer.rules_filter rf_p on rfs.PROD_FILTER_FK = rf_p.rules_filter_id
left join epacube.epacube.PRODUCT_IDENTIFICATION pi with (nolock) on rf_p.data_name_fk = pi.DATA_NAME_FK and rf_p.value1 = pi.value
left join epacube.synchronizer.RULES_FILTER rf_o with (nolock) on rfs.ORG_FILTER_FK = rf_o.RULES_FILTER_ID
left join epacube.epacube.ENTITY_IDENTIFICATION ei_o with (nolock) 
	on rf_o.ENTITY_DATA_NAME_FK = ei_o.ENTITY_DATA_NAME_FK and rf_o.DATA_NAME_FK = ei_o.DATA_NAME_FK 
	and rf_o.entity_data_name_fk = 141000 and rf_o.data_name_fk = 141111 and rf_o.VALUE1 = ei_o.value
left join epacube.epacube.ENTITY_IDENTIFICATION ei_o2 with (nolock) on CTD.Whse = ei_o2.VALUE and ei_o2.ENTITY_DATA_NAME_FK = 141000
where 1 = 1
	And r.result_data_name_fk in (111501, 111502, 111401, 111602) 
	and r.RECORD_STATUS_CR_FK = 1
	and ctd.Profiles_Job_FK = @Profiles_Job_FK
group by r.result_data_name_fk, emt.ENTITY_STRUCTURE_FK, ei_v.entity_structure_fk, isnull(ei_o2.entity_structure_fk, ei_o.ENTITY_STRUCTURE_FK)
, pi.PRODUCT_STRUCTURE_FK

Create index idx_id on #Identifiers(cust_entity_structure_fk, supl_entity_structure_fk, product_structure_fk, org_entity_structure_fk)

--Product Groups in Price/Rebate Records by Customer Price/Rebate Type

Select Cast(Null as bigint) Job_FK, r.result_data_name_fk, emt.ENTITY_STRUCTURE_FK Cust_Entity_Structure_FK, ei_v.entity_structure_fk Supl_Entity_Structure_FK
, pc.PRODUCT_STRUCTURE_FK, isnull(ei_o2.entity_structure_fk, ei_o.ENTITY_STRUCTURE_FK) Org_Entity_Structure_FK
into #identifiers1
from epacube.epacube.DATA_VALUE dv with (nolock)
inner join epacube.epacube.entity_mult_type emt with (nolock) on dv.DATA_NAME_FK = emt.DATA_NAME_FK and dv.DATA_VALUE_ID = emt.DATA_VALUE_FK
inner join epacube.synchronizer.RULES_FILTER rf_crt with (nolock) on dv.VALUE = rf_crt.VALUE1 and dv.DATA_NAME_FK = rf_crt.DATA_NAME_FK
inner join epacube.synchronizer.RULES_FILTER_SET rfs with (nolock) on rf_crt.RULES_FILTER_ID = rfs.CUST_FILTER_FK
inner join epacube.synchronizer.RULES r with (nolock) on rfs.RULES_FK = r.rules_id
inner join [CPL].[aCPL_Template_Data] CTD with (nolock) on dv.data_name_fk = CTD.data_name_fk and dv.value = CTD.value
left join epacube.synchronizer.RULES_FILTER rf_v with (nolock) on rfs.SUPL_FILTER_FK = rf_v.RULES_FILTER_ID
left join epacube.epacube.ENTITY_IDENTIFICATION ei_v with (nolock) on rf_v.ENTITY_DATA_NAME_FK = ei_v.entity_data_name_fk and rf_v.DATA_NAME_FK = ei_v.DATA_NAME_FK
	and rf_v.VALUE1 = ei_v.VALUE
left join epacube.synchronizer.rules_filter rf_p on rfs.PROD_FILTER_FK = rf_p.rules_filter_id
left join epacube.epacube.DATA_VALUE dv_p with (nolock) on rf_p.DATA_NAME_FK = dv_p.DATA_NAME_FK and rf_p.VALUE1 = dv_p.VALUE
left join epacube.epacube.PRODUCT_CATEGORY pc with (nolock) on dv_p.DATA_NAME_FK = pc.DATA_NAME_FK and dv_p.DATA_VALUE_ID = pc.DATA_VALUE_FK
left join epacube.synchronizer.RULES_FILTER rf_o with (nolock) on rfs.ORG_FILTER_FK = rf_o.RULES_FILTER_ID
left join epacube.epacube.ENTITY_IDENTIFICATION ei_o with (nolock) 
	on rf_o.ENTITY_DATA_NAME_FK = ei_o.ENTITY_DATA_NAME_FK and rf_o.DATA_NAME_FK = ei_o.DATA_NAME_FK 
	and rf_o.entity_data_name_fk = 141000 and rf_o.data_name_fk = 141111 and rf_o.VALUE1 = ei_o.value
left join epacube.epacube.ENTITY_IDENTIFICATION ei_o2 with (nolock) on CTD.Whse = ei_o2.VALUE and ei_o2.ENTITY_DATA_NAME_FK = 141000
where 1 = 1
	and r.result_data_name_fk in (111501, 111502, 111401, 111602) 
	and r.RECORD_STATUS_CR_FK = 1
	and ctd.Profiles_Job_FK = @Profiles_Job_FK
group by r.result_data_name_fk, emt.ENTITY_STRUCTURE_FK, ei_v.entity_structure_fk, isnull(ei_o2.entity_structure_fk, ei_o.ENTITY_STRUCTURE_FK)
, pc.PRODUCT_STRUCTURE_FK

Create index idx_id on #Identifiers1(cust_entity_structure_fk, supl_entity_structure_fk, product_structure_fk, org_entity_structure_fk)

----Product IDs in Price/Rebate Records by Customer Bill-to/Ship-to

Select Cast(Null as bigint) Job_FK, r.result_data_name_fk, ei_c.ENTITY_STRUCTURE_FK Cust_Entity_Structure_FK, ei_v.entity_structure_fk Supl_Entity_Structure_FK
, pi.PRODUCT_STRUCTURE_FK, isnull(ei_o2.entity_structure_fk, ei_o.ENTITY_STRUCTURE_FK) Org_Entity_Structure_FK
into #identifiers2
from epacube.synchronizer.rules_filter rf_c with (nolock)
inner join epacube.epacube.ENTITY_IDENTIFICATION ei_c with (nolock) on rf_c.ENTITY_DATA_NAME_FK = ei_c.ENTITY_DATA_NAME_FK and rf_c.DATA_NAME_FK = ei_c.DATA_NAME_FK
	and rf_c.VALUE1 = ei_c.VALUE 
inner join epacube.synchronizer.RULES_FILTER_SET rfs with (nolock) on rf_c.RULES_FILTER_ID = rfs.CUST_FILTER_FK
inner join epacube.synchronizer.RULES r with (nolock) on rfs.RULES_FK = r.rules_id
inner join [CPL].[aCPL_Template_Data] CTD with (nolock) on rf_c.ENTITY_DATA_NAME_FK = CTD.Entity_Data_Name_FK and rf_c.VALUE1 = CTD.value
left join epacube.synchronizer.RULES_FILTER rf_v with (nolock) on rfs.SUPL_FILTER_FK = rf_v.RULES_FILTER_ID
left join epacube.epacube.ENTITY_IDENTIFICATION ei_v with (nolock) on rf_v.ENTITY_DATA_NAME_FK = ei_v.entity_data_name_fk and rf_v.DATA_NAME_FK = ei_v.DATA_NAME_FK
	and rf_v.VALUE1 = ei_v.VALUE
left join epacube.synchronizer.rules_filter rf_p on rfs.PROD_FILTER_FK = rf_p.rules_filter_id
left join epacube.epacube.DATA_VALUE dv_p with (nolock) on rf_p.DATA_NAME_FK = dv_p.DATA_NAME_FK and rf_p.VALUE1 = dv_p.VALUE
left join epacube.epacube.PRODUCT_IDENTIFICATION pi with (nolock) on rf_p.data_name_fk = pi.DATA_NAME_FK and rf_p.value1 = pi.value
left join epacube.synchronizer.RULES_FILTER rf_o with (nolock) on rfs.ORG_FILTER_FK = rf_o.RULES_FILTER_ID
left join epacube.epacube.ENTITY_IDENTIFICATION ei_o with (nolock) 
	on rf_o.ENTITY_DATA_NAME_FK = ei_o.ENTITY_DATA_NAME_FK and rf_o.DATA_NAME_FK = ei_o.DATA_NAME_FK 
	and rf_o.entity_data_name_fk = 141000 and rf_o.data_name_fk = 141111 and rf_o.VALUE1 = ei_o.value
left join epacube.epacube.ENTITY_IDENTIFICATION ei_o2 with (nolock) on CTD.Whse = ei_o2.VALUE and ei_o2.ENTITY_DATA_NAME_FK = 141000
where 1 =1
	and r.result_data_name_fk in (111501, 111502, 111401, 111602) 
	and r.RECORD_STATUS_CR_FK = 1
	and ctd.Profiles_Job_FK = @Profiles_Job_FK
group by r.result_data_name_fk, ei_c.ENTITY_STRUCTURE_FK, ei_v.entity_structure_fk, isnull(ei_o2.entity_structure_fk, ei_o.ENTITY_STRUCTURE_FK)
, pi.PRODUCT_STRUCTURE_FK

Create index idx_id on #Identifiers2(cust_entity_structure_fk, supl_entity_structure_fk, product_structure_fk, org_entity_structure_fk)

----Product Groups in Price/Rebate Records by Customer Bill-to/Ship-to

Select Cast(Null as bigint) Job_FK, r.result_data_name_fk, ei_c.ENTITY_STRUCTURE_FK Cust_Entity_Structure_FK, ei_v.entity_structure_fk Supl_Entity_Structure_FK
, pc.PRODUCT_STRUCTURE_FK, isnull(ei_o2.entity_structure_fk, ei_o.ENTITY_STRUCTURE_FK) Org_Entity_Structure_FK
into #identifiers3
from epacube.synchronizer.rules_filter rf_c with (nolock)
inner join epacube.epacube.ENTITY_IDENTIFICATION ei_c with (nolock) on rf_c.ENTITY_DATA_NAME_FK = ei_c.ENTITY_DATA_NAME_FK and rf_c.DATA_NAME_FK = ei_c.DATA_NAME_FK
	and rf_c.VALUE1 = ei_c.VALUE 
inner join epacube.synchronizer.RULES_FILTER_SET rfs with (nolock) on rf_c.RULES_FILTER_ID = rfs.CUST_FILTER_FK
inner join epacube.synchronizer.RULES r with (nolock) on rfs.RULES_FK = r.rules_id
inner join [CPL].[aCPL_Template_Data] CTD with (nolock) on rf_c.ENTITY_DATA_NAME_FK = CTD.Entity_Data_Name_FK and rf_c.VALUE1 = CTD.value
left join epacube.synchronizer.RULES_FILTER rf_v with (nolock) on rfs.SUPL_FILTER_FK = rf_v.RULES_FILTER_ID
left join epacube.epacube.ENTITY_IDENTIFICATION ei_v with (nolock) on rf_v.ENTITY_DATA_NAME_FK = ei_v.entity_data_name_fk and rf_v.DATA_NAME_FK = ei_v.DATA_NAME_FK
	and rf_v.VALUE1 = ei_v.VALUE
left join epacube.synchronizer.rules_filter rf_p on rfs.PROD_FILTER_FK = rf_p.rules_filter_id
left join epacube.epacube.DATA_VALUE dv_p with (nolock) on rf_p.DATA_NAME_FK = dv_p.DATA_NAME_FK and rf_p.VALUE1 = dv_p.VALUE
left join epacube.epacube.PRODUCT_CATEGORY pc with (nolock) on dv_p.DATA_NAME_FK = pc.DATA_NAME_FK and dv_p.DATA_VALUE_ID = pc.DATA_VALUE_FK
left join epacube.synchronizer.RULES_FILTER rf_o with (nolock) on rfs.ORG_FILTER_FK = rf_o.RULES_FILTER_ID
left join epacube.epacube.ENTITY_IDENTIFICATION ei_o with (nolock) 
	on rf_o.ENTITY_DATA_NAME_FK = ei_o.ENTITY_DATA_NAME_FK and rf_o.DATA_NAME_FK = ei_o.DATA_NAME_FK 
	and rf_o.entity_data_name_fk = 141000 and rf_o.data_name_fk = 141111 and rf_o.VALUE1 = ei_o.value
left join epacube.epacube.ENTITY_IDENTIFICATION ei_o2 with (nolock) on CTD.Whse = ei_o2.VALUE and ei_o2.ENTITY_DATA_NAME_FK = 141000
where 1 = 1
	and r.result_data_name_fk in (111501, 111502, 111401, 111602) 
	and r.RECORD_STATUS_CR_FK = 1
	and ctd.Profiles_Job_FK = @Profiles_Job_FK
group by r.result_data_name_fk, ei_c.ENTITY_STRUCTURE_FK, ei_v.entity_structure_fk, isnull(ei_o2.entity_structure_fk, ei_o.ENTITY_STRUCTURE_FK)
, pc.PRODUCT_STRUCTURE_FK

Create index idx_id on #Identifiers3(cust_entity_structure_fk, supl_entity_structure_fk, product_structure_fk, org_entity_structure_fk)

Insert into #identifiers
Select IDA.*
from #identifiers2 IDA
left Join #identifiers ID on IDA.Cust_Entity_Structure_FK = ID.Cust_Entity_Structure_FK
	and isnull(IDA.Org_Entity_Structure_FK, 0) = isnull(ID.Org_Entity_Structure_FK, 0)
	and isnull(IDA.Supl_Entity_Structure_FK, 0) = isnull(ID.Supl_Entity_Structure_FK, 0)
	and isnull(IDA.PRODUCT_STRUCTURE_FK, 0) = isnull(ID.PRODUCT_STRUCTURE_FK, 0)
Where ID.RESULT_DATA_NAME_FK is null	

Insert into #identifiers
Select IDA.*
from #identifiers3 IDA
left Join #identifiers ID on IDA.Cust_Entity_Structure_FK = ID.Cust_Entity_Structure_FK
	and isnull(IDA.Org_Entity_Structure_FK, 0) = isnull(ID.Org_Entity_Structure_FK, 0)
	and isnull(IDA.Supl_Entity_Structure_FK, 0) = isnull(ID.Supl_Entity_Structure_FK, 0)
	and isnull(IDA.PRODUCT_STRUCTURE_FK, 0) = isnull(ID.PRODUCT_STRUCTURE_FK, 0)
Where ID.RESULT_DATA_NAME_FK is null	

Insert into #identifiers
Select IDA.*
from #identifiers1 IDA
left Join #identifiers ID on IDA.Cust_Entity_Structure_FK = ID.Cust_Entity_Structure_FK
	and isnull(IDA.Org_Entity_Structure_FK, 0) = isnull(ID.Org_Entity_Structure_FK, 0)
	and isnull(IDA.Supl_Entity_Structure_FK, 0) = isnull(ID.Supl_Entity_Structure_FK, 0)
	and isnull(IDA.PRODUCT_STRUCTURE_FK, 0) = isnull(ID.PRODUCT_STRUCTURE_FK, 0)
Where ID.RESULT_DATA_NAME_FK is null	

Update ID
Set Org_Entity_Structure_FK = ei_o.entity_structure_fk
from #Identifiers ID
inner join epacube.epacube.entity_attribute ea with (nolock) on ID.cust_entity_structure_fk = ea.entity_structure_fk and ea.data_name_fk = 144900
inner join epacube.epacube.entity_identification ei_o with (nolock) on ea.attribute_event_data = ei_o.value and ei_o.entity_data_name_fk = 141000 and ei_o.data_name_fk = 141111
where ISNULL(Org_Entity_Structure_FK, 0) = 0

Select custno, shipto, Sum(CAST(netamt as money)) Sales, whse 
into #Whses1 
from epacube.import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE
where CAST(invoicedt as datetime) > getdate() - 366
group by custno, shipto, whse

Create index idx_whse1 on #Whses1(custno, shipto, whse)

Select A.*, ei.ENTITY_STRUCTURE_FK Cust_Entity_Structure_FK, ei_o.ENTITY_STRUCTURE_FK Org_Entity_Structure_FK 
into #Whses
from #Whses1 A
inner join
(Select Custno, ISNULL(shipto, '0') Shipto, MAX(Sales) Sales from #Whses1 group by Custno, ISNULL(shipto, '0')) B
	on A.custno = B.custno and ISNULL(A.shipto, '0') = B.Shipto and A.Sales = B.sales
Left join epacube.epacube.ENTITY_IDENTIFICATION ei with (nolock) 
	on ei.VALUE = Case when A.shipto IS null then A.custno else A.custno + '-' + A.shipto end 
	and ei.ENTITY_DATA_NAME_FK in (144010, 144020)
Left Join epacube.epacube.ENTITY_IDENTIFICATION ei_o with (nolock) on ei_o.VALUE = A.whse and ei_o.ENTITY_DATA_NAME_FK = 141000

Create index idx_whse on #Whses(custno, shipto)

Update ID
Set Org_Entity_Structure_FK = W.Org_Entity_Structure_FK
from #identifiers ID
inner join #Whses W on ID.Cust_Entity_Structure_FK = W.Cust_Entity_Structure_FK
where ID.Org_Entity_Structure_FK is null

Declare @Org_Entity_Structure_FK Bigint
Set @Org_Entity_Structure_FK = (Select entity_structure_fk from epacube.epacube.ENTITY_IDENTIFICATION where ENTITY_DATA_NAME_FK = 141000 and VALUE = 
	(Select top 1 whse from epacube.import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE
		where CAST(invoicedt as datetime) > getdate() - 366
		group by whse
		Order by Sum(CAST(netamt as money)) desc))	

Update ID
Set Org_Entity_Structure_FK = @Org_Entity_Structure_FK
from #identifiers ID
where ID.Org_Entity_Structure_FK is null

Update ID
Set Job_FK = @Job_FK
from #identifiers ID

-----------------------------
SET @status_desc = '#Identifiers Temp Table Created'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

If @Job_Type is null
Set @Job_Type = 'Pricing Template'

Set @Result_Type_CR_FK = Case when ISNULL(@Eff, getdate()) > getdate() then 711 else 710 end

Set @COST_CHANGE_FIRST_IND = Case When @Result_Type_CR_FK = 711 and @EFF is Null then 1 else 0 end
Set @COST_CHANGE_PRIOR_IND = Case When @Result_Type_CR_FK = 711 and @EFF is not Null then 1 else 0 end
Set @StartTime = GETDATE()

Insert into epacube.dbo.maui_log
(Job_fk_i, Activity, Start_Time)
Select @Job_FK, 'MAUI_Generate_CPL_Template_Results-' + CAST(@Job_FK as Varchar(16)), @StartTime

EXEC MarginMgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, 0

Insert into epacube.MarginMgr.ANALYSIS_JOB
(Analysis_Job_ID, Analysis_Name, result_type_cr_fk, result_effective_date, rule_type_filter, Update_User)
Select @Job_FK, 'MAUI ' + @Job_Type + ' - ' + Cast(@Job_FK as Varchar(32)), @Result_Type_CR_FK, @Eff, @RuleTypeFilter, @Wuser

SET @status_desc = '#epaCUBE.MarginMgr.Analysis_Job Table Loaded; Event_Calc Preload Started'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

INSERT INTO [synchronizer].[EVENT_CALC] 
 ( [JOB_FK] ,[RESULT_DATA_NAME_FK] ,[RESULT_TYPE_CR_FK] ,[RESULT_EFFECTIVE_DATE] 
 ,[PRODUCT_STRUCTURE_FK] ,[ORG_ENTITY_STRUCTURE_FK] ,[CUST_ENTITY_STRUCTURE_FK] ,[SUPL_ENTITY_STRUCTURE_FK] 
 ,[CALC_GROUP_SEQ] ,[RULE_TYPE_CR_FK] ,[PARITY_DV_FK] ,[UPDATE_USER] ) 
  Select ID.Job_FK, rh.result_data_name_fk, AJ.Result_Type_CR_FK, AJ.Result_Effective_Date
 , ID.product_Structure_FK, ID.[ORG_ENTITY_STRUCTURE_FK], ID.[CUST_ENTITY_STRUCTURE_FK]
 , ISNULL ( ( SELECT PAS.ENTITY_STRUCTURE_FK FROM epacube.epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK) WHERE PAS.DATA_NAME_FK 
		= ( SELECT CAST ( EEP.VALUE AS INT ) FROM epacube.EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK) WHERE EEP.APPLICATION_SCOPE_FK = 100 AND EEP.NAME = 'WHSE SUPL ASSOC' ) 
			AND PAS.PRODUCT_STRUCTURE_FK = ID.PRODUCT_STRUCTURE_FK AND PAS.ORG_ENTITY_STRUCTURE_FK = ID.ORG_ENTITY_STRUCTURE_FK AND PAS.RECORD_STATUS_CR_FK = 1 ) 
	,( SELECT PAS.ENTITY_STRUCTURE_FK FROM epacube.epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK) WHERE PAS.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT ) 
			FROM epacube.EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK) WHERE EEP.APPLICATION_SCOPE_FK = 100 AND EEP.NAME = 'CORP SUPL ASSOC' ) 
			AND PAS.PRODUCT_STRUCTURE_FK = ID.PRODUCT_STRUCTURE_FK AND PAS.RECORD_STATUS_CR_FK = 1 ) ) Supl_Entity_Structure_FK 
 , rh.CALC_GROUP_SEQ, rh.rule_type_cr_fk 
 , (SELECT PC.DATA_VALUE_FK FROM epacube.epacube.PRODUCT_CATEGORY PC WITH (NOLOCK) WHERE PC.DATA_NAME_FK = RH.PARITY_CHECK_DN_FK AND PC.PRODUCT_STRUCTURE_FK = ID.PRODUCT_STRUCTURE_FK) 'PARITY_DV_FK'
 ,	AJ.UPDATE_USER 
 from #Identifiers ID
 inner join epacube.marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK) on AJ.ANALYSIS_JOB_ID = @Job_FK
 inner join epacube.synchronizer.RULES_HIERARCHY RH WITH (NOLOCK) on RH.RECORD_STATUS_CR_FK = 1 
 Where RH.RULE_TYPE_CR_FK = 313
 
If @RuleTypeFilter like '%312%'
Begin
	INSERT INTO [synchronizer].[EVENT_CALC] 
	 ( [JOB_FK] ,[RESULT_DATA_NAME_FK] ,[RESULT_TYPE_CR_FK] ,[RESULT_EFFECTIVE_DATE] 
	 ,[PRODUCT_STRUCTURE_FK] ,[ORG_ENTITY_STRUCTURE_FK] ,[CUST_ENTITY_STRUCTURE_FK] ,[SUPL_ENTITY_STRUCTURE_FK] 
	 ,[CALC_GROUP_SEQ] ,[RULE_TYPE_CR_FK] ,[PARITY_DV_FK] ,[UPDATE_USER] ) 
	 
	 Select ID.Job_FK, rh.result_data_name_fk, AJ.Result_Type_CR_FK, AJ.Result_Effective_Date
	 , ID.product_Structure_FK, ID.[ORG_ENTITY_STRUCTURE_FK], ID.[CUST_ENTITY_STRUCTURE_FK]
	 , ISNULL ( ( SELECT PAS.ENTITY_STRUCTURE_FK FROM epacube.epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK) WHERE PAS.DATA_NAME_FK 
			= ( SELECT CAST ( EEP.VALUE AS INT ) FROM epacube.EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK) WHERE EEP.APPLICATION_SCOPE_FK = 100 AND EEP.NAME = 'WHSE SUPL ASSOC' ) 
				AND PAS.PRODUCT_STRUCTURE_FK = ID.PRODUCT_STRUCTURE_FK AND PAS.ORG_ENTITY_STRUCTURE_FK = ID.ORG_ENTITY_STRUCTURE_FK AND PAS.RECORD_STATUS_CR_FK = 1 ) 
		,( SELECT PAS.ENTITY_STRUCTURE_FK FROM epacube.epacube.PRODUCT_ASSOCIATION PAS WITH (NOLOCK) WHERE PAS.DATA_NAME_FK = ( SELECT CAST ( EEP.VALUE AS INT ) 
				FROM epacube.EPACUBE.EPACUBE_PARAMS EEP WITH (NOLOCK) WHERE EEP.APPLICATION_SCOPE_FK = 100 AND EEP.NAME = 'CORP SUPL ASSOC' ) 
				AND PAS.PRODUCT_STRUCTURE_FK = ID.PRODUCT_STRUCTURE_FK AND PAS.RECORD_STATUS_CR_FK = 1 ) ) Supl_Entity_Structure_FK 
	 , rh.CALC_GROUP_SEQ, rh.rule_type_cr_fk 
	 , (SELECT PC.DATA_VALUE_FK FROM epacube.epacube.PRODUCT_CATEGORY PC WITH (NOLOCK) WHERE PC.DATA_NAME_FK = RH.PARITY_CHECK_DN_FK AND PC.PRODUCT_STRUCTURE_FK = ID.PRODUCT_STRUCTURE_FK) 'PARITY_DV_FK'
	 ,	AJ.UPDATE_USER 
	 from #Identifiers ID
	 inner join epacube.marginmgr.ANALYSIS_JOB AJ WITH (NOLOCK) On AJ.ANALYSIS_JOB_ID = @Job_FK
	 inner join epacube.synchronizer.RULES_HIERARCHY RH WITH (NOLOCK) on RH.RECORD_STATUS_CR_FK = 1 
	 Where RH.RULE_TYPE_CR_FK = 312
End

SET @status_desc = 'All rows for calculation are preloaded into epaCUBE.Synchronizer.Event_Calc'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

EXEC  epacube.[marginmgr].[CALCULATION_PROCESS] @Job_FK

Exec [CPL].[Pricing_Table_Create] @Job_FK, '', @Wuser, 'cpl.Price_List_Template_'

Exec [CPL].[Pricing_Table_Qty_Brackets] @Job_FK, 'cpl.Price_List_Template_'

Update Profiles
Set intProcess = 3
, LastProcessedDate = Getdate()
, strProcessingTime = Convert(Varchar(10),GETDATE() - @StartTime, 108)
from cpl.aCPL_Profiles Profiles
Where 1 = 1
and Job_FK = @Job_FK


IF object_id('tempdb..#Identifiers') is not null
drop table #Identifiers

EXEC MarginMgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, 0

Set @SQL_CD = '
Update RH
Set Table_Name = ''CPL.Price_List_Template_' + CAST(@Job_FK as Varchar(16)) + '''
from reports.Reports_Header RH
where Job_FK = ' + CAST(@Job_FK as Varchar(16)) + '

Update RD
Set Table_Name = ''CPL.Price_List_Template_' + CAST(@Job_FK as Varchar(16)) + '''
from reports.Reports_Details RD
where Job_FK = ' + CAST(@Job_FK as Varchar(16)) + '

Update RC
Set Table_Name = ''CPL.Price_List_Template_' + CAST(@Job_FK as Varchar(16)) + '''
from reports.Reports_Calcs RC
where Job_FK = ' + CAST(@Job_FK as Varchar(16))

exec(@SQL_CD)

Set @SQL_CD = '
Update ML
Set Duration = ''' + Convert(Varchar(10),GETDATE() - @StartTime, 108) + '''
, Record_Inserts = (select COUNT(*) from cpl.Price_List_Template_' + Cast(@Job_FK as varchar(16)) + ')
from epacube.dbo.MAUI_Log ML where MAUI_Log_ID = (Select top 1 Maui_Log_ID from epacube.dbo.MAUI_Log with (nolock) where Cast(Job_fk_i as bigint) = ' + Cast(@Job_FK as varchar(16)) + ' order by Maui_Log_ID Desc)'
Exec(@SQL_CD)

SET @status_desc = 'Finished Execution of cpl.Price_Template_Calculations_Generate'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;

IF object_id('tempdb..#Identifiers') is not null
Drop Table #Identifiers
IF object_id('tempdb..#Identifiers1') is not null
Drop Table #Identifiers1
IF object_id('tempdb..#Identifiers2') is not null
Drop Table #Identifiers2
IF object_id('tempdb..#Identifiers3') is not null
Drop Table #Identifiers3
IF object_id('tempdb..#Whses1') is not null
Drop Table #Whses1
IF object_id('tempdb..#Whses') is not null
Drop Table #Whses

End
