﻿--A_MAUI_Price_List_Calcs_AutoGenerate

CREATE PROCEDURE [CPL].[Price_List_Calcs_AutoGenerate]
AS
BEGIN

	SET NOCOUNT ON;
	
	--DBCC DBREINDEX ('synchronizer.RULES', ' ', 90);
	----DBCC DBREINDEX ('marginmgr.PRICESHEET', ' ', 90);
	--DBCC DBREINDEX ('synchronizer.RULES_ACTION', ' ', 90);
	--DBCC DBREINDEX ('synchronizer.RULES_FILTER_SET', ' ', 90);
	--DBCC DBREINDEX ('synchronizer.RULES_CONTRACT_CUSTOMER', ' ', 90);	
	--DBCC DBREINDEX ('synchronizer.EVENT_CALC', ' ', 90);	
	--DBCC DBREINDEX ('synchronizer.EVENT_CALC_RULES', ' ', 90);	
	--DBCC DBREINDEX ('synchronizer.EVENT_CALC_RULES_BASIS', ' ', 90);	

	Declare @Job_FK Bigint

	DECLARE PriceLists Cursor local for
	Select Job_FK from CPL.aCPL_Profiles with (nolock) where PricingEffectiveDateNext is not null Order by Job_FK

	OPEN PriceLists
	
	Fetch Next From PriceLists into @Job_FK

		WHILE @@FETCH_STATUS = 0
		Begin

		Exec CPL.Calc_Scheduling_Setup @Job_FK
		
		Update CPL Set Pricing_Effective_Date = PricingEffectiveDateNext from  CPL.aCPL_Profiles CPL Where Job_FK = @Job_FK
			
		Fetch Next From PriceLists into @Job_FK
			
		End
		
	Close PriceLists;
	Deallocate PriceLists;		

		
	Declare @ProfileType int
	DECLARE PriceListCreate Cursor local for
		Select Job_FK, inttype from (
		select Profiles.Job_FK, Profiles.strProfile_Name, Profiles.Pricing_Effective_Date, Profiles.inttype, case Profiles.inttype when 1 then 'Price List Profiles' else 'Price List Template' end 'Price List Type'
		, Profiles.customer_template_fk, Ctemplates.strProfile_Name 'Customer Template Name'
		, Profiles.Product_Template_FK, Ptemplates.strProfile_Name 'Product Template Name', Ctemplates.Template_Count * Ptemplates.Template_Count Calculations 
		from CPL.aCPL_Profiles Profiles with (nolock)
		left join CPL.aCPL_Profiles Ctemplates with (nolock) on Profiles.Customer_Template_FK = Ctemplates.Job_FK
		left join CPL.aCPL_Profiles Ptemplates with (nolock) on Profiles.Product_Template_FK = Ptemplates.Job_FK
		where Profiles.intType in (1, 2)
		and ISNULL(Profiles.intSchedulingActive, 0) <> 0
		and Convert(varchar(10), Profiles.CalcDateNext, 1) = Convert(varchar(10), GETDATE(), 1)) A
		Order by Calculations

	OPEN PriceListCreate
	
	Fetch Next From PriceListCreate into @Job_FK, @ProfileType

		WHILE @@FETCH_STATUS = 0
		Begin
		
		If @ProfileType = 1
		Begin
			Exec CPL.Price_List_Calculations_Call_SB @Job_FK
		End
		Else
		Begin
			Exec CPL.[Price_Template_Calculations_Call_SB] @Job_FK, '', 'Template Scheduler'
		End

	Fetch Next From PriceListCreate into @Job_FK, @ProfileType
	End
		
	Close PriceListCreate;
	Deallocate PriceListCreate;		

	--Set Next Calc Date to Today where it qualifies
	Exec Reports.Report_Scheduling_Update

	IF object_id('Reports.Reports_Scheduled') is not null
	drop table Reports.Reports_Scheduled

	Select
	Report_ID Report_FK, Cast(Job_FK as bigint) Job_FK, ReportCalcDateNext, ExportPath
	, ExportFileName, ExportDataFileName, Fnprefix, Fnsuffix, IncludeDateStamp, Exporttype, eMailAddressRW, exportColumnDelimiter, exportTextDelimiter, exportColumnNames, exportFileNameExt, GETDATE() Create_Timestamp, CAST(Null as datetime) Report_Processed
	into Reports.Reports_Scheduled
	from reports.Reports_Header
	where CONVERT(varchar(10), ReportCalcDateNext, 101) = CONVERT(varchar(10), GETDATE(), 101)
	and CONVERT(varchar(10), isnull(ReportCalcDateLast, getdate() - 1), 101) < CONVERT(varchar(10), GETDATE(), 101)
	and intSchedulingActive = -1

	Create Index idx_rh_id on Reports.Reports_Scheduled(Report_FK)

	Update CPL
	Set [Pricing_Effective_Date] = RS.ReportCalcDateNext
	from cpl.aCPL_Profiles CPL 
	inner join Reports.Reports_Scheduled RS with (Nolock) on CPL.Job_FK = RS.Job_FK

	DECLARE Processing Cursor local for
	Select Job_FK from Reports.Reports_Scheduled with (nolock) Group by Job_FK Order by Job_FK

	OPEN Processing
	
		Fetch Next From Processing into @Job_FK

		WHILE @@FETCH_STATUS = 0
		Begin
		
		Exec CPL.Price_List_Calculations_Call_SB @Job_FK

		Fetch Next From Processing into @Job_FK

		End
		
	Close Processing;
	Deallocate Processing;

	Update RH
	Set ReportCalcDateLast = GETDATE()
	from reports.Reports_Header RH
	inner Join Reports.Reports_Scheduled RS on RH.Report_ID = RS.Report_fk
	
	Exec Reports.Report_Scheduling_Update 1

END
