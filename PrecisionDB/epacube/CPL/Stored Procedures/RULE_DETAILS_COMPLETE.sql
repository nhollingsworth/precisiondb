﻿--A_MAUI_RULE_DETAILS_COMPLETE

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [CPL].[RULE_DETAILS_COMPLETE]
AS
BEGIN
	SET NOCOUNT ON;
	
----*** Include this code with all Tables loading into Special schema

	Declare @Job_FK varchar(16)

	If (select count(*) from INFORMATION_SCHEMA.TABLES where table_schema = 'special' and TABLE_NAME = 'epaCUBE_Rules_Complete') = 0
		Set @Job_FK = isnull((Select RANK() over (order by table_name) Job_Fk from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'Special'), 0) + 1
	Else 
		Set @Job_FK = (Select top 1 Job_FK from Special.epaCUBE_Rules_Complete)

----*** -------------------------------------------------------------

IF object_id('tempdb..##Filters') is not null
	drop table ##Filters;

IF object_id('tempdb..##Rules1') is not null
drop table ##Rules1

IF object_id('tempdb..##RAO1') is not null
drop table ##RAO1

IF object_id('tempdb..##RAO2') is not null
drop table ##RAO2

IF object_id('tempdb..##RAO3') is not null
Drop table ##RAO3


Select 
rfs.rules_fk
, (select label from epacube.epacube.DATA_NAME where  DATA_NAME_ID = rf_p.DATA_NAME_FK) 'Filter_Prod'
, rf_p.VALUE1 'Filter_Prod_Value'

, (select label from epacube.epacube.DATA_NAME where  DATA_NAME_ID = isnull(rf_o.entity_data_name_fk, rf_o.DATA_NAME_FK)) 'Filter_Whse'
, rf_o.VALUE1 'Filter_Whse_Value'

, (select label from epacube.epacube.DATA_NAME where  DATA_NAME_ID = isnull(rf_c.entity_data_name_fk, rf_c.DATA_NAME_FK)) 'Filter_Cust'
, rf_c.VALUE1 'Filter_Cust_Value'

, (select label from epacube.epacube.DATA_NAME where  DATA_NAME_ID = isnull(rf_v.entity_data_name_fk, rf_v.DATA_NAME_FK)) 'Filter_Vndr'
, rf_v.VALUE1 'Filter_Vndr_Value'
into ##Filters
from epacube.synchronizer.rules_filter_set rfs with (nolock)
left join epacube.synchronizer.RULES_FILTER rf_p with (nolock) on rfs.prod_filter_fk = rf_p.RULES_FILTER_ID
left join epacube.synchronizer.RULES_FILTER rf_o with (nolock) on rfs.org_filter_fk = rf_o.RULES_FILTER_ID
left join epacube.synchronizer.RULES_FILTER rf_c with (nolock) on rfs.cust_filter_fk = rf_c.RULES_FILTER_ID
left join epacube.synchronizer.RULES_FILTER rf_v with (nolock) on rfs.supl_filter_fk = rf_v.RULES_FILTER_ID

Create index idx_fltr on ##Filters(Rules_FK)



Declare @RAOops1 Varchar(Max)
Declare @RAOjoin1 Varchar(Max)

Declare @RAOops2 Varchar(Max)
Declare @RAOjoin2 Varchar(Max)

Declare @RAOops3 Varchar(Max)
Declare @RAOjoin3 Varchar(Max)

Set @RAOjoin1 = ''
Set @RAOjoin2 = ''
Set @RAOjoin3 = ''

If (Select COUNT(*) from epacube.synchronizer.rules_action_operands rao with (nolock)
		inner join epacube.synchronizer.RULES_ACTION ra with (nolock) on rao.rules_action_fk = ra.RULES_ACTION_ID) > 0
Begin

	Set @RAOjoin1 = ' Left Join ##RAO1 RAO1 on R.rules_ID = RAO1.Rules_FK '
	Set @RAOops1 = ' , C1_Qty_Break_Ind, C1_Operand1, C1_Operand2, C1_Operand3, C1_Operand4, C1_Operand5, C1_Operand6, C1_Operand7, C1_Operand8, C1_Operand9, C1_Qty1, C1_Qty2, C1_Qty3, C1_Qty4, C1_Qty5, C1_Qty6, C1_Qty7, C1_Qty8 '

	IF object_id('tempdb..##RAO1') is not null
	drop table ##RAO1;

	Select Rules_FK
	, Basis_Position
	, C1_Qty_Break_Ind
	, Operand1 C1_Operand1
	, Operand2 C1_Operand2
	, Operand3 C1_Operand3
	, Operand4 C1_Operand4
	, Operand5 C1_Operand5
	, Operand6 C1_Operand6
	, Operand7 C1_Operand7
	, Operand8 C1_Operand8
	, Operand9 C1_Operand9
	, Case C1_Qty_Break_Ind when 0 then Null else Filter_Value1 end C1_Qty1
	, Case C1_Qty_Break_Ind when 0 then Null else Filter_Value2 end C1_Qty2
	, Case C1_Qty_Break_Ind when 0 then Null else Filter_Value3 end C1_Qty3
	, Case C1_Qty_Break_Ind when 0 then Null else Filter_Value4 end C1_Qty4
	, Case C1_Qty_Break_Ind when 0 then Null else Filter_Value5 end C1_Qty5
	, Case C1_Qty_Break_Ind when 0 then Null else Filter_Value6 end C1_Qty6
	, Case C1_Qty_Break_Ind when 0 then Null else Filter_Value7 end C1_Qty7
	, Case C1_Qty_Break_Ind when 0 then Null else Filter_Value8 end C1_Qty8
	into ##RAO1
	from (
	Select ra.RULES_FK
	, rao.BASIS_POSITION 
	, Case when (select COUNT(*) from epacube.synchronizer.RULES_ACTION_OPERANDS where RULES_ACTION_FK = rao.rules_action_fk and Operand_filter_operator_CR_FK = 9020) > 0 then 1 else 0 end 'C1_Qty_Break_Ind'
	, Cast(rao.Operand1 as money) Operand1
	, Cast(rao.Operand2 as money) Operand2
	, Cast(rao.Operand3 as money) Operand3
	, Cast(rao.Operand4 as money) Operand4
	, Cast(rao.Operand5 as money) Operand5
	, Cast(rao.Operand6 as money) Operand6
	, Cast(rao.Operand7 as money) Operand7
	, Cast(rao.Operand8 as money) Operand8
	, Cast(rao.Operand9 as money) Operand9
	, rao.filter_value1
	, rao.filter_value2
	, rao.filter_value3
	, rao.filter_value4
	, rao.filter_value5
	, rao.filter_value6
	, rao.filter_value7
	, rao.filter_value8
	, rao.filter_value9
	from epacube.synchronizer.rules_action_operands rao with (nolock)
	inner join epacube.synchronizer.RULES_ACTION ra with (nolock) on rao.rules_action_fk = ra.RULES_ACTION_ID
	Where rao.basis_position = 1) A

	Create index idx_fk on ##RAO1(Rules_FK)
End

If (Select COUNT(*) from epacube.synchronizer.rules_action_operands rao with (nolock)
		inner join epacube.synchronizer.RULES_ACTION ra with (nolock) on rao.rules_action_fk = ra.RULES_ACTION_ID where rao.basis_position = 2) > 0
Begin

	Set @RAOjoin2 = ' Left Join ##RAO2 RAO2 on R.rules_ID = RAO2.Rules_FK '
	Set @RAOops2 = ' , C2_Operand1, C2_Operand2, C2_Operand3, C2_Operand4, C2_Operand5, C2_Operand6, C2_Operand7, C2_Operand8, C2_Operand9, C2_Qty1, C2_Qty2, C2_Qty3, C2_Qty4, C2_Qty5, C2_Qty6, C2_Qty7, C2_Qty8 '

	IF object_id('tempdb..##RAO2') is not null
	drop table ##RAO2;

	Select Rules_FK
	, Operand1 C2_Operand1
	, Operand2 C2_Operand2
	, Operand3 C2_Operand3
	, Operand4 C2_Operand4
	, Operand5 C2_Operand5
	, Operand6 C2_Operand6
	, Operand7 C2_Operand7
	, Operand8 C2_Operand8
	, Operand9 C2_Operand9
	, Case Qty_Break_Ind when 0 then Null else Filter_Value1 end C2_Qty1
	, Case Qty_Break_Ind when 0 then Null else Filter_Value2 end C2_Qty2
	, Case Qty_Break_Ind when 0 then Null else Filter_Value3 end C2_Qty3
	, Case Qty_Break_Ind when 0 then Null else Filter_Value4 end C2_Qty4
	, Case Qty_Break_Ind when 0 then Null else Filter_Value5 end C2_Qty5
	, Case Qty_Break_Ind when 0 then Null else Filter_Value6 end C2_Qty6
	, Case Qty_Break_Ind when 0 then Null else Filter_Value7 end C2_Qty7
	, Case Qty_Break_Ind when 0 then Null else Filter_Value8 end C2_Qty8
	into ##RAO2
	from (
	Select ra.RULES_FK
	, Case when (select COUNT(*) from epacube.synchronizer.RULES_ACTION_OPERANDS where RULES_ACTION_FK = rao.rules_action_fk and Operand_filter_operator_CR_FK = 9020) > 0 then 1 else 0 end 'Qty_Break_Ind'
	, Cast(rao.Operand1 as money) Operand1
	, Cast(rao.Operand2 as money) Operand2
	, Cast(rao.Operand3 as money) Operand3
	, Cast(rao.Operand4 as money) Operand4
	, Cast(rao.Operand5 as money) Operand5
	, Cast(rao.Operand6 as money) Operand6
	, Cast(rao.Operand7 as money) Operand7
	, Cast(rao.Operand8 as money) Operand8
	, Cast(rao.Operand9 as money) Operand9
	, rao.filter_value1
	, rao.filter_value2
	, rao.filter_value3
	, rao.filter_value4
	, rao.filter_value5
	, rao.filter_value6
	, rao.filter_value7
	, rao.filter_value8
	, rao.filter_value9
	from epacube.synchronizer.rules_action_operands rao with (nolock)
	inner join epacube.synchronizer.RULES_ACTION ra with (nolock) on rao.rules_action_fk = ra.RULES_ACTION_ID
	Where rao.basis_position = 2) A

	Create index idx_fk on ##RAO2(Rules_FK)
End

If (Select COUNT(*) from epacube.synchronizer.rules_action_operands rao with (nolock)
		inner join epacube.synchronizer.RULES_ACTION ra with (nolock) on rao.rules_action_fk = ra.RULES_ACTION_ID where rao.basis_position = 2) > 0
Begin

	Set @RAOjoin3 = ' Left Join ##RAO3 RAO3 on R.rules_ID = RAO3.Rules_FK '
	Set @RAOops3 = ' , C3_Operand1, C3_Operand2, C3_Operand3, C3_Operand4, C3_Operand5, C3_Operand6, C3_Operand7, C3_Operand8, C3_Operand9, C3_Qty1, C3_Qty2, C3_Qty3, C3_Qty4, C3_Qty5, C3_Qty6, C3_Qty7, C3_Qty8 '

	IF object_id('tempdb..##RAO3') is not null
	drop table ##RAO3;

	Select Rules_FK
	, Operand1 C3_Operand1
	, Operand2 C3_Operand2
	, Operand3 C3_Operand3
	, Operand4 C3_Operand4
	, Operand5 C3_Operand5
	, Operand6 C3_Operand6
	, Operand7 C3_Operand7
	, Operand8 C3_Operand8
	, Operand9 C3_Operand9
	, Case Qty_Break_Ind when 0 then Null else Filter_Value1 end C3_Qty1
	, Case Qty_Break_Ind when 0 then Null else Filter_Value2 end C3_Qty2
	, Case Qty_Break_Ind when 0 then Null else Filter_Value3 end C3_Qty3
	, Case Qty_Break_Ind when 0 then Null else Filter_Value4 end C3_Qty4
	, Case Qty_Break_Ind when 0 then Null else Filter_Value5 end C3_Qty5
	, Case Qty_Break_Ind when 0 then Null else Filter_Value6 end C3_Qty6
	, Case Qty_Break_Ind when 0 then Null else Filter_Value7 end C3_Qty7
	, Case Qty_Break_Ind when 0 then Null else Filter_Value8 end C3_Qty8
	into ##RAO3
	from (
	Select ra.RULES_FK
	, Case when (select COUNT(*) from epacube.synchronizer.RULES_ACTION_OPERANDS where RULES_ACTION_FK = rao.rules_action_fk and Operand_filter_operator_CR_FK = 9020) > 0 then 1 else 0 end 'Qty_Break_Ind'
	, Cast(rao.Operand1 as money) Operand1
	, Cast(rao.Operand2 as money) Operand2
	, Cast(rao.Operand3 as money) Operand3
	, Cast(rao.Operand4 as money) Operand4
	, Cast(rao.Operand5 as money) Operand5
	, Cast(rao.Operand6 as money) Operand6
	, Cast(rao.Operand7 as money) Operand7
	, Cast(rao.Operand8 as money) Operand8
	, Cast(rao.Operand9 as money) Operand9
	, rao.filter_value1
	, rao.filter_value2
	, rao.filter_value3
	, rao.filter_value4
	, rao.filter_value5
	, rao.filter_value6
	, rao.filter_value7
	, rao.filter_value8
	, rao.filter_value9
	from epacube.synchronizer.rules_action_operands rao with (nolock)
	inner join epacube.synchronizer.RULES_ACTION ra with (nolock) on rao.rules_action_fk = ra.RULES_ACTION_ID
	Where rao.basis_position = 3) A

	Create index idx_fk on ##RAO3(Rules_FK)
End

IF object_id('tempdb..##Rules1') is not null
drop table ##Rules1;

Declare @Rls1 varchar(Max)
Set @Rls1 = '
Select 
CAST(getdate() as DATE) ''A0_Creation_Date''
, (select code from epacube.epacube.CODE_REF with (nolock) where CODE_REF_ID = rh.RULE_TYPE_CR_FK) ''A1_Type_Of_Rule''
, dn_rdnfk.LABEL ''A2_Type_OF_Calculation''
, R.[HOST_RULE_XREF] ''' + Case (Select value from epacube.epacube.epacube_params where name = 'ERP HOST') when 'SXE' then 'A3_PD_Record_No'
								When 'Eclipse' then 'A3_Matrix_ID' else 'A3_Host_Record_ID' end
	+ '''

, R.[RULES_ID] ''A4_epaCUBE_Rule_ID''
, rp.host_precedence_level ''A5_Rule_Type'', rp.Host_Precedence_Level_Description ''A6_Rule_Type_Description''
, Rst.[NAME] ''StructureName'', Rsc.[NAME] ''ScheduleName'', R.[NAME] ''epaCUBE_Rule_Name'', R.[RESULT_DATA_NAME_FK], R.[RESULT_DATA_NAME2_FK]
, Cast(R.[EFFECTIVE_DATE] as DATE) ''Effective_Date'', Cast(R.[END_DATE] as DATE) ''End_Date''
, R.[RULE_PRECEDENCE]
, R.[REFERENCE], R.[CONTRACT_NO]
, (Select code from epacube.epacube.code_ref where code_ref_id = R.[RULE_RESULT_TYPE_CR_FK]) ''Current/Future Status''
, Replace(Replace(Replace(Replace(Replace((select name from epacube.synchronizer.rules_action_operation where RULES_ACTION_OPERATION_ID = ra.RULES_ACTION_OPERATION1_FK), ''MARGIN'', ''PROFIT''), '' (N)'', ''''), '' (D)'', ''''), '' (L)'', ''''), '' (O)'', '''') ''C1_Action''
, Replace(Replace(Replace(Replace(Replace((select name from epacube.synchronizer.rules_action_operation where RULES_ACTION_OPERATION_ID = ra.RULES_ACTION_OPERATION2_FK), ''MARGIN'', ''PROFIT''), '' (N)'', ''''), '' (D)'', ''''), '' (L)'', ''''), '' (O)'', '''') ''C2_Action''
, Replace(Replace(Replace(Replace(Replace((select name from epacube.synchronizer.rules_action_operation where RULES_ACTION_OPERATION_ID = ra.RULES_ACTION_OPERATION3_FK), ''MARGIN'', ''PROFIT''), '' (N)'', ''''), '' (D)'', ''''), '' (L)'', ''''), '' (O)'', '''') ''C3_Action''
, dn_bc.LABEL ''C1_Basis_Col'', dn_b1.LABEL ''C2_Basis_Col'', dn_b2.LABEL ''C3_Basis_Col'', dn_b3.LABEL ''C3_Basis_Col2''
, Cast(BASIS1_NUMBER as money) ''C1_Number'', Cast(BASIS2_NUMBER as money) ''C2_Number'', Cast(BASIS3_NUMBER as money) ''C3_Number''
, Cast(BASIS1_Value as money) ''C1_Value'', Cast(BASIS2_Value as money) ''C2_Value'', Cast(BASIS3_Value as money) ''C3_Value''
, BASIS_CALC_PRICESHEET_NAME ''C1_Pricesheet'', Cast(BASIS_CALC_PRICESHEET_DATE as Date) ''C1_Pricesheet_Date''
, BASIS1_PRICESHEET_NAME ''C2_Pricesheet'', Cast(BASIS1_PRICESHEET_DATE as Date) ''C2_Pricesheet_Date''
, BASIS2_PRICESHEET_NAME ''C3_Pricesheet'', Cast(BASIS2_PRICESHEET_DATE as Date) ''C3_Pricesheet_Date''
, BASIS3_PRICESHEET_NAME ''C4_Pricesheet'', Cast(BASIS3_PRICESHEET_DATE as Date) ''C4_Pricesheet_Date''
, (select code from epacube.epacube.CODE_REF where CODE_REF_ID = ra.ROUNDING_METHOD_CR_FK) ''Rounding_Method''
, Cast(ra.ROUNDING_TO_VALUE as money) ''Rounding_To_Value''
, ra.RULES_OPTIMIZATION_FK
, r.promo_ind
, ra.Formula
, ' + @Job_FK + ' Job_FK, ' + @Job_FK + ' Job_FK_i
, F.Filter_Prod, F.Filter_Prod_Value, F.Filter_Whse, F.Filter_Whse_Value, F.Filter_Cust, F.Filter_Cust_Value, F.Filter_Vndr, F.Filter_Vndr_Value
' + isnull(@RAOops1, '') + isnull(@RAOops2, '') + isnull(@RAOops3, '') + '
--, R.[UOM_DN_FK], R.[UOM_Code_FK], R.[TRANSACTION_DV_FK]
--, R.[RESULT_PRICESHEET], R.[RESULT_PRICESHEET_DATE_IND]
--, R.[TRIGGER_EVENT_TYPE_CR_FK], R.[TRIGGER_EVENT_DN_FK], R.[TRIGGER_OPERATOR_CR_FK], R.[TRIGGER_EVENT_VALUE1], R.[TRIGGER_EVENT_VALUE2], R.[TRIGGER_IMPORT_PACKAGE_FK], R.[TRIGGER_EVENT_SOURCE_CR_FK], R.[TRIGGER_EVENT_ACTION_CR_FK], R.[APPLY_TO_ORG_CHILDREN_IND]
--, R.[RULES_PRECEDENCE_FK]
--, R.[RULES_CONTRACT_IND], R.[RULES_CONTRACT_FK], R.[GROUPS_FK], R.[IMPORT_PACKAGE_FK], R.[IMPORT_JOB_FK], R.[IMPORT_RULES_FK], R.[IMPORT_FILENAME], R.[LAST_IMPORT_PACKAGE_FK], R.[LAST_IMPORT_JOB_FK], R.[LAST_IMPORT_FILENAME], R.[UNIQUE_KEY1], R.[DISPLAY_SEQ], R.[EVENT_ACTION_CR_FK], R.[EVENT_CONDITION_CR_FK]
--, R.[SEED_RULE_IND], R.[SYSTEM_ENTITY_STRUCTURE_FK]
into ##Rules1
from epacube.synchronizer.RULES r with (nolock)
inner join epacube.synchronizer.RULES_STRUCTURE rst with (nolock) on r.RULES_STRUCTURE_FK = rst.RULES_STRUCTURE_ID
inner join epacube.synchronizer.rules_precedence rp with (nolock) on r.rules_precedence_fk = rp.rules_precedence_id
inner join epacube.synchronizer.RULES_SCHEDULE rsc with (nolock) on r.RULES_SCHEDULE_FK = rsc.RULES_SCHEDULE_ID
inner join epacube.synchronizer.RULES_HIERARCHY rh with (nolock) on rst.RULES_HIERARCHY_FK = rh.RULES_HIERARCHY_ID
inner join epacube.epacube.DATA_NAME dn_rdnfk with (nolock) on r.RESULT_DATA_NAME_FK = dn_rdnfk.data_name_id
left join epacube.synchronizer.RULES_ACTION ra with (nolock) on r.RULES_ID = ra.rules_fk
left join epacube.epacube.DATA_NAME dn_bc with (nolock) on ra.BASIS_CALC_DN_FK = dn_bc.DATA_NAME_ID
left join epacube.epacube.DATA_NAME dn_b1 with (nolock) on ra.BASIS1_DN_FK = dn_b1.DATA_NAME_ID
left join epacube.epacube.DATA_NAME dn_b2 with (nolock) on ra.BASIS2_DN_FK = dn_b2.DATA_NAME_ID
left join epacube.epacube.DATA_NAME dn_b3 with (nolock) on ra.BASIS3_DN_FK = dn_b3.DATA_NAME_ID
left join ##Filters F on R.RULES_ID = F.RULES_FK '
+ isnull(@RAOjoin1, '') + isnull(@RAOjoin2, '') + isnull(@RAOjoin3, '') + '
where rh.record_status_cr_fk = 1 and r.RECORD_STATUS_CR_FK = 1
'

Exec(@Rls1)

Set @Rls1 = ''

If @RAOjoin1 > ''
Begin
Set @Rls1 = '
	Update R
	Set C1_Number = isnull(C1_Number, Coalesce(C1_Operand1, C1_Operand2, C1_Operand3, C1_Operand4, C1_Operand5, C1_Operand6, C1_Operand7, C1_Operand8, C1_Operand9)) / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int)
	from ##Rules1 R where C1_Action <> ''FIXED VALUE'' and C1_Action <> ''Carrier Zcurve''

	Update R
	Set C1_Number = isnull(C1_Number, Coalesce(C1_Operand1, C1_Operand2, C1_Operand3, C1_Operand4, C1_Operand5, C1_Operand6, C1_Operand7, C1_Operand8, C1_Operand9))
	from ##Rules1 R where C1_Action = ''FIXED VALUE''


	Update R
	Set  C1_Operand1 = Case When isnull(C1_Operand1, 0) = 0 then Null else C1_Operand1 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end
		, C1_Operand2 = Case When isnull(C1_Operand2, 0) = 0 then Null else C1_Operand2 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C1_Operand3 = Case When isnull(C1_Operand3, 0) = 0 then Null else C1_Operand3 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C1_Operand4 = Case When isnull(C1_Operand4, 0) = 0 then Null else C1_Operand4 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C1_Operand5 = Case When isnull(C1_Operand5, 0) = 0 then Null else C1_Operand5 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C1_Operand6 = Case When isnull(C1_Operand6, 0) = 0 then Null else C1_Operand6 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C1_Operand7 = Case When isnull(C1_Operand7, 0) = 0 then Null else C1_Operand7 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C1_Operand8 = Case When isnull(C1_Operand8, 0) = 0 then Null else C1_Operand8 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C1_Operand9 = Case When isnull(C1_Operand9, 0) = 0 then Null else C1_Operand9 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
	from ##Rules1 R where C1_Action <> ''FIXED VALUE'' and C1_Action <> ''Carrier Zcurve''
	'
end
Else
Begin
Set @Rls1 = '
	Update R
	Set C1_Number = Case when isnull(C1_Number, 0) = 0 then Null else C1_Number / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end
	, C2_Number = Case when isnull(C2_Number, 0) = 0 then Null else C2_Number / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end
	, C3_Number = Case when isnull(C3_Number, 0) = 0 then Null else C3_Number / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end
	from ##Rules1 R where C1_Action <> ''FIXED VALUE'' and C1_Action <> ''Carrier Zcurve'' --and C1_Number is not null
	'
End

Exec(@Rls1)
Set @Rls1 = ''

If @RAOjoin2 > ''
Begin
Set @Rls1 = '
	Update R
	Set  C2_Operand1 = Case When isnull(C2_Operand1, 0) = 0 then Null else C2_Operand1 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end
		, C2_Operand2 = Case When isnull(C2_Operand2, 0) = 0 then Null else C2_Operand2 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C2_Operand3 = Case When isnull(C2_Operand3, 0) = 0 then Null else C2_Operand3 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C2_Operand4 = Case When isnull(C2_Operand4, 0) = 0 then Null else C2_Operand4 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C2_Operand5 = Case When isnull(C2_Operand5, 0) = 0 then Null else C2_Operand5 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C2_Operand6 = Case When isnull(C2_Operand6, 0) = 0 then Null else C2_Operand6 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C2_Operand7 = Case When isnull(C2_Operand7, 0) = 0 then Null else C2_Operand7 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C2_Operand8 = Case When isnull(C2_Operand8, 0) = 0 then Null else C2_Operand8 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C2_Operand9 = Case When isnull(C2_Operand9, 0) = 0 then Null else C2_Operand9 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
	from ##Rules1 R where C1_Action <> ''FIXED VALUE'' and C1_Action <> ''Carrier Zcurve''
	'
	Exec(@Rls1)
end

Set @Rls1 = ''

If @RAOjoin3 > ''
Begin
Set @Rls1 = '
	Update R
	Set  C3_Operand1 = Case When isnull(C3_Operand1, 0) = 0 then Null else C3_Operand1 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end
		, C3_Operand2 = Case When isnull(C3_Operand2, 0) = 0 then Null else C3_Operand2 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C3_Operand3 = Case When isnull(C3_Operand3, 0) = 0 then Null else C3_Operand3 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C3_Operand4 = Case When isnull(C3_Operand4, 0) = 0 then Null else C3_Operand4 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C3_Operand5 = Case When isnull(C3_Operand5, 0) = 0 then Null else C3_Operand5 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C3_Operand6 = Case When isnull(C3_Operand6, 0) = 0 then Null else C3_Operand6 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C3_Operand7 = Case When isnull(C3_Operand7, 0) = 0 then Null else C3_Operand7 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C3_Operand8 = Case When isnull(C3_Operand8, 0) = 0 then Null else C3_Operand8 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
		, C3_Operand9 = Case When isnull(C3_Operand9, 0) = 0 then Null else C3_Operand9 / Cast((select value from epacube.epacube.epacube_params where name = ''OPERAND DIVISOR'') as int) end	
	from ##Rules1 R where C1_Action <> ''FIXED VALUE'' and C1_Action <> ''Carrier Zcurve''
	'
Exec(@Rls1)	
end

Set @Rls1 = '
If (Select count(*) from ##Rules1 where [RESULT_DATA_NAME2_FK] is not null) = 0 
Alter Table ##Rules1 drop column [RESULT_DATA_NAME2_FK]

If (Select count(*) from ##Rules1 where [C2_Action] is not null) = 0 
Alter Table ##Rules1 drop column [C2_Action]

If (Select count(*) from ##Rules1 where [C3_Action] is not null) = 0 
Alter Table ##Rules1 drop column [C3_Action]

If (Select count(*) from ##Rules1 where [C2_Basis_Col] is not null) = 0 
Alter Table ##Rules1 drop column [C2_Basis_Col]

If (Select count(*) from ##Rules1 where [C3_Basis_Col] is not null) = 0 
Alter Table ##Rules1 drop column [C3_Basis_Col]

If (Select count(*) from ##Rules1 where [C2_Number] is not null) = 0 
Alter Table ##Rules1 drop column [C2_Number]

If (Select count(*) from ##Rules1 where [C3_Number] is not null) = 0 
Alter Table ##Rules1 drop column [C3_Number]

If (Select count(*) from ##Rules1 where C1_Value is not null) = 0 
Alter Table ##Rules1 drop column C1_Value

If (Select count(*) from ##Rules1 where C2_Value is not null) = 0 
Alter Table ##Rules1 drop column C2_Value

If (Select count(*) from ##Rules1 where C3_Value is not null) = 0 
Alter Table ##Rules1 drop column C3_Value

If (Select count(*) from ##Rules1 where [C1_Pricesheet] is not null) = 0 
Alter Table ##Rules1 drop column [C1_Pricesheet]

If (Select count(*) from ##Rules1 where [C1_Pricesheet_Date] is not null) = 0 
Alter Table ##Rules1 drop column [C1_Pricesheet_Date]

If (Select count(*) from ##Rules1 where [C2_Pricesheet] is not null) = 0 
Alter Table ##Rules1 drop column [C2_Pricesheet]

If (Select count(*) from ##Rules1 where [C2_Pricesheet_Date] is not null) = 0 
Alter Table ##Rules1 drop column [C2_Pricesheet_Date]

If (Select count(*) from ##Rules1 where [C3_Pricesheet] is not null) = 0 
Alter Table ##Rules1 drop column [C3_Pricesheet]

If (Select count(*) from ##Rules1 where [C3_Pricesheet_Date] is not null) = 0 
Alter Table ##Rules1 drop column [C3_Pricesheet_Date]

If (Select count(*) from ##Rules1 where [C4_Pricesheet] is not null) = 0 
Alter Table ##Rules1 drop column [C4_Pricesheet]

If (Select count(*) from ##Rules1 where [C4_Pricesheet_Date] is not null) = 0 
Alter Table ##Rules1 drop column [C4_Pricesheet_Date]

If (Select count(*) from ##Rules1 where [Rounding_Method] is not null) = 0 
Alter Table ##Rules1 drop column [Rounding_Method]

If (Select count(*) from ##Rules1 where [Rounding_To_Value] is not null) = 0 
Alter Table ##Rules1 drop column [Rounding_To_Value]

If (Select count(*) from ##Rules1 where [Rules_Optimization_FK] is not null) = 0 
Alter Table ##Rules1 drop column [Rules_Optimization_FK]
'
Exec(@Rls1)

IF object_id('Special.epaCUBE_Rules_Complete') is not null
drop table Special.epaCUBE_Rules_Complete

Select * into Special.epaCUBE_Rules_Complete from ##Rules1

Set @Rls1 = '
CREATE NONCLUSTERED INDEX [idx_rls] ON Special.epaCUBE_Rules_Complete
(
	A1_Type_Of_Rule Asc,
	' + Case (Select value from epacube.epacube.epacube_params where name = 'ERP HOST') when 'SXE' then 'A3_PD_Record_No'
								When 'Eclipse' then 'A3_Matrix_ID' else 'A3_Host_Record_ID' end
	+ ', 
	A4_epaCUBE_Rule_ID Asc,
	A5_Rule_Type Asc,
	Effective_Date Asc,
	End_Date Asc,
	c1_Action asc,
	c1_Basis_Col asc,
	filter_cust asc,
	filter_cust_value asc,
	filter_prod asc,
	filter_prod_value asc,
	filter_whse asc,
	filter_whse_value asc,
	filter_vndr asc,
	filter_vndr_value asc
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
'
exec(@Rls1)

If (Select COUNT(*) from cpl.aCPL_Profiles where intType = 10 and strProfile_Name = 'epaCUBE_Rules_Complete') = 0
Insert into cpl.aCPL_Profiles
(Job_FK, strProfile_Name, Create_User, Pricing_Effective_Date, intType)
Select @Job_FK, 'epaCUBE_Rules_Complete', 'System_Generated', CAST(getdate() as DATE), 10

Update CPL
Set Pricing_Effective_Date = CAST(getdate() as DATE)
from cpl.aCPL_Profiles CPL where intType = 10 and strProfile_Name = 'epaCUBE_Rules_Complete'

IF object_id('tempdb..##Filters') is not null
	drop table ##Filters;

IF object_id('tempdb..##Rules1') is not null
drop table ##Rules1

IF object_id('tempdb..##RAO1') is not null
drop table ##RAO1

IF object_id('tempdb..##RAO2') is not null
drop table ##RAO2

IF object_id('tempdb..##RAO3') is not null
Drop table ##RAO3
END
