﻿--A_MAUI_Pricing_Table_Create

--April 28, 2013	GHS		Updated to Support Service Broker Calls and Email Notification when Complete
--October 29, 2013 GHS Updated to use Product and Customer Grouping Names specific to ERP

CREATE PROCEDURE [CPL].[Pricing_Table_Create]

	@Job_FK BigInt,
	@AliasPreparedBy Varchar(64),
	@WinUser Varchar(64),
	@INSERT_TABLE VARCHAR(64)

AS
BEGIN
	SET NOCOUNT ON;

--Declare @Job_FK Bigint
--Declare @INSERT_TABLE VARCHAR(64)
--Declare @AliasPreparedBy Varchar(64)
--Declare @WinUser Varchar(64)
--Set @Job_FK = 6112
--Set @INSERT_TABLE = 'cpl.Price_List_Profile_'
--Set @WinUser = 'John Smith'

Declare @PreparedBy Varchar(64)
Declare @SQLp Varchar(Max)
Declare @SQLp1 Varchar(Max)
Declare @SQLp2 Varchar(Max)
Declare @SQLp3 Varchar(Max)
Declare @CG1 Varchar(64)
Declare @CG2 Varchar(64)
Declare @CG3 Varchar(64)
Declare @PG1 Varchar(64)
Declare @PG2 Varchar(64)
Declare @PG3 Varchar(64)
Declare @PG4 Varchar(64)
Declare @PG5 Varchar(64)


Set @INSERT_TABLE = @INSERT_TABLE + Cast(@Job_FK as Varchar(32))

Set @PreparedBy = Case When isnull(@AliasPreparedBy, '') = '' then isnull(@WinUser, '') else isnull(@AliasPreparedBy, '') End

Set @CG1 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Cust_Group1' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @CG2 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Cust_Group2' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @CG3 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Cust_Group3' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @PG1 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Prod_Group1' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @PG2 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Prod_Group2' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @PG3 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Prod_Group3' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @PG4 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Prod_Group4' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''
Set @PG5 = '' + (Select Replace(ColCMBname, ' ', '_') from epacube.dbo.maui_cols with (nolock) where ColCommand = 'Prod_Group5' and ERP = (Select value from epacube.epacube.epacube_params with (nolock) where NAME = 'ERP HOST')) + ''

Set @SQLp = '

	IF object_id(''' + @INSERT_TABLE + ''') is not null
	Drop Table ' + @INSERT_TABLE

Exec(@SQLp)

Set @SQLp = '
CREATE TABLE ' + @INSERT_TABLE + '
(
	[Bill-To Customer ID] [varchar](64) NULL,
	[Bill-To Customer Name] [varchar](128) NULL,
	[Ship-To Customer ID] [varchar](64) NULL,
	[Ship-To Customer Name] [varchar](128) NULL,
	[UPC] [varchar](24) NULL,
	[Product_ID] [varchar](64) NULL,
	[Product_Description] [varchar](128) NULL,
	[Lookup_Name] [varchar](128) NULL,
	[Variations] [varchar](max) NULL,
	[Sell Price] [money] NULL,
	[Pricing Cost] [money] NULL,
	[Rebate] [money] NULL,
	[Rebate_Cap_Sell] [money] NULL,
	[Replacement_Cost] [money] NULL,
	[Standard_Cost] [money] NULL,
	[Base_Price] [money] NULL,
	[List_Price] [money] NULL,
	[Vendor_Rebate_Cost] [money] NULL,
	[Whse_ID] [varchar](64) NULL,
	[Vendor_ID] [varchar](64) NULL,
	[Rebate Record #] [varchar](16) NULL,
	[Pricing Record #] [varchar](16) NULL,
	[Sell_Price_Cust_Rules_FK] [bigint] NULL,
	[Price_Rule_Effective_Date] [datetime] NULL,
	[Price_Rule_End_Date] [datetime] NULL,
	[Operation] [varchar](64) NULL,
	[Prices_Calculated_Date] [datetime] NULL,
	[Product_Structure_FK] [bigint] NULL,
	[Org_Entity_Structure_FK] [bigint] NULL,
	[Cust_Entity_Structure_FK] [bigint] NULL,
	[Supl_Entity_Structure_FK] [bigint] NULL,
	[WAREHOUSE_RECORD] [varchar](64) NULL,
	[SALESREP_RECORD_IN] [varchar](64) NULL,
	[SALESREP_RECORD_OUT] [varchar](64) NULL,
	[Cust_Group1] [varchar](64) NULL,
	[Cust_Group2] [varchar](64) NULL,
	[Cust_Group3] [varchar](64) NULL,
	[Prod_Group1] [varchar](64) NULL,
	[Prod_Group2] [varchar](64) NULL,
	[Prod_Group3] [varchar](64) NULL,
	[Prod_Group4] [varchar](64) NULL,
	[Prod_Group5] [varchar](64) NULL,
	[Cust_Group1_Desc] [varchar](64) NULL,
	[Cust_Group2_Desc] [varchar](64) NULL,
	[Cust_Group3_Desc] [varchar](64) NULL,
	[Prod_Group1_Desc] [varchar](64) NULL,
	[Prod_Group2_Desc] [varchar](64) NULL,
	[Prod_Group3_Desc] [varchar](64) NULL,
	[Prod_Group4_Desc] [varchar](64) NULL,
	[Prod_Group5_Desc] [varchar](64) NULL,
	[Stocking_UOM] [varchar](32) NULL,
	[SPC_UOM] [numeric](18, 6) NULL,
	[Break_Prices] [varchar](255) NULL,
	[BracketQty1] [varchar](24) NULL,
	[BracketQty2] [varchar](24) NULL,
	[BracketQty3] [varchar](24) NULL,
	[BracketQty4] [varchar](24) NULL,
	[BracketQty5] [varchar](24) NULL,
	[BracketQty6] [varchar](24) NULL,
	[BracketQty7] [varchar](24) NULL,
	[BracketQty8] [varchar](24) NULL,
	[BracketQty9] [varchar](24) NULL,
	[BracketPrice1] [numeric](18, 4) NULL,
	[BracketPrice2] [numeric](18, 4) NULL,
	[BracketPrice3] [numeric](18, 4) NULL,
	[BracketPrice4] [numeric](18, 4) NULL,
	[BracketPrice5] [numeric](18, 4) NULL,
	[BracketPrice6] [numeric](18, 4) NULL,
	[BracketPrice7] [numeric](18, 4) NULL,
	[BracketPrice8] [numeric](18, 4) NULL,
	[BracketPrice9] [numeric](18, 4) NULL,
	[CreatedBy] [varchar](64) NULL,
	[Customer_Price_List_Sort_FK] [int] NULL,
	[Job_FK] [bigint] NULL,
	[Customer_Price_List_Results_ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_' + Replace(@INSERT_TABLE, 'cpl.', '') + '] PRIMARY KEY CLUSTERED 
(
	[Customer_Price_List_Results_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
'
Exec(@SQLp)



Set @SQLp1 = '
Declare @Job_FK Bigint
Declare @Eff_Date datetime
Declare @ERP as Varchar(32)
Declare @PG1_FK as Bigint
Declare @PG2_FK as Bigint
Declare @PG3_FK as Bigint
Declare @PG4_FK as Bigint
Declare @PG5_FK as Bigint
Declare @PairLIPitems Int
Declare @User Varchar(64)

Set @Job_FK = ' + Cast(@Job_FK as Varchar(32)) + '
Set @ERP = (Select Value from epacube.epacube.epacube_params with (nolock) where Name = ''ERP HOST'')
Set @PG1_FK = (Select Data_Name_FK from epacube.dbo.maui_config_erp with (nolock) where ERP = @ERP and usage like ''%grouping%'' and epaCUBE_Column = ''Prod_Group1'')
Set @PG2_FK = (Select Data_Name_FK from epacube.dbo.maui_config_erp with (nolock) where ERP = @ERP and usage like ''%grouping%'' and epaCUBE_Column = ''Prod_Group2'')
Set @PG3_FK = (Select Data_Name_FK from epacube.dbo.maui_config_erp with (nolock) where ERP = @ERP and usage like ''%grouping%'' and epaCUBE_Column = ''Prod_Group3'')
Set @PG4_FK = (Select Data_Name_FK from epacube.dbo.maui_config_erp with (nolock) where ERP = @ERP and usage like ''%grouping%'' and epaCUBE_Column = ''Prod_Group4'')
Set @PG5_FK = (Select Data_Name_FK from epacube.dbo.maui_config_erp with (nolock) where ERP = @ERP and usage like ''%grouping%'' and epaCUBE_Column = ''Prod_Group5'')
Set @Eff_Date = (select top 1 result_effective_date from epacube.synchronizer.event_calc with (nolock) where result_data_name_fk = 111602 and job_fk = @Job_FK)
set @PairLIPitems = isnull((select top 1 intLIP from epacube.dbo.maui_customer_price_list_profiles with (nolock) where customer_Price_List_Profiles_ID = @Job_FK), 0)
set @User = ''' + @PreparedBy + '''

IF object_id(''tempdb..#Pricelist'') is not null
drop table #Pricelist;

IF object_id(''tempdb..#MatSetup'') is not null
drop table #MatSetup

IF object_id(''tempdb..#Variations'') is not null
drop table #Variations

Select
Isnull(ei_btid.value, ei_btid2.value) ''Bill-To Customer ID''
, (Select ein.value from epacube.epacube.entity_ident_nonunique ein with (nolock) where ein.data_name_fk = 144112 and ein.entity_structure_fk = isnull(ei_btid.entity_structure_fk, ei_btid2.entity_structure_fk)) ''Bill-To Customer Name''
, ei_stid.value  ''Ship-To Customer ID''
, (Select ein.value from epacube.epacube.entity_ident_nonunique ein with (nolock) where ein.data_name_fk = 144112 and ein.entity_structure_fk = ei_stid.entity_structure_fk) ''Ship-To Customer Name''
, ltrim(rtrim((select top 1 value from epacube.epacube.PRODUCT_IDENTIFICATION with (nolock) where DATA_NAME_FK = 110102 and PRODUCT_STRUCTURE_FK = ec.product_structure_fk))) ''UPC''
, (select value from epacube.epacube.product_identification with (nolock) where data_name_fk = 110100 and product_structure_fk = ec.product_structure_fk) ''Product_ID''
, Ltrim(Rtrim(isnull(Rtrim((Select Description from epacube.epacube.product_description with (nolock) where data_name_fk = 110401 and product_structure_fk = ec.product_structure_fk)), '''')) + '' ''
	 + isnull(Ltrim((Select Description from epacube.epacube.product_description with (nolock) where data_name_fk = 110402 and product_structure_fk = ec.product_structure_fk)), '''')) ''Product_Description''
, Cast(ec.Sell_Price_Cust_Amt as Money) ''Sell Price''
, Cast(ec.Pricing_Cost_Basis_Amt as Money) ''Pricing Cost''
, Cast(ec.Rebate_CB_Amt as Money) ''Rebate''
, Case ecr_cb.Rules_Action_Operation2_FK When 1294 then
		Case When ecr_cb.Basis_calc is null then ecr_cb.NUMBER2
			else isnull(ecr_cb.NUMBER2, 0) * isnull(ecr_cb.Basis1, 0) end end ''Rebate_Sell_Cap''
, ei_o.value Whse_ID
, ei_v.value Vendor_ID
, ecr_cb.HOST_RULE_XREF ''Rebate Record #''
, ecr.HOST_RULE_XREF ''Pricing Record #''
, ec.rules_fk
, ec.result_effective_date Effective_Date
, ec.Product_Structure_FK
, ec.Org_Entity_Structure_FK
, ec.Cust_Entity_Structure_FK
, ec.Supl_Entity_Structure_FK
, (Select Attribute_Event_Data from epacube.epacube.ENTITY_ATTRIBUTE with (nolock) where DATA_NAME_FK = 144900 and ENTITY_STRUCTURE_FK = ec.Cust_Entity_Structure_FK) ''WHSE RECORD''
, (Select Attribute_Event_Data from epacube.epacube.ENTITY_ATTRIBUTE with (nolock) where DATA_NAME_FK = 145010 and ENTITY_STRUCTURE_FK = ec.Cust_Entity_Structure_FK) ''SALESREP IN''
, (Select Attribute_Event_Data from epacube.epacube.ENTITY_ATTRIBUTE with (nolock) where DATA_NAME_FK = 145020 and ENTITY_STRUCTURE_FK = ec.Cust_Entity_Structure_FK) ''SALESREP OUT''
, ec.Job_FK
, Case When ecr.CUST_FILTER like ''cust price type%'' then Replace(ecr.CUST_FILTER, ''Cust Price Type: '', '''') end Cust_Group2
, isnull(pa.Attribute_Number, 1) ''spc_uom''
, (Select value from epacube.epacube.data_value dv with (nolock) inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.epacube.product_category pc with (nolock) on dv.data_name_fk = pc.data_name_fk and dv.data_value_id = pc.data_value_fk and pc.product_structure_fk = ec.product_structure_fk and pc.org_entity_structure_fk = Case When ds.Org_Ind is null then 1 else ec.org_entity_structure_fk end and pc.data_name_fk = @PG1_FK) PG1
, (Select value from epacube.epacube.data_value dv with (nolock) inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.epacube.product_category pc with (nolock) on dv.data_name_fk = pc.data_name_fk and dv.data_value_id = pc.data_value_fk and pc.product_structure_fk = ec.product_structure_fk and pc.org_entity_structure_fk = Case When ds.Org_Ind is null then 1 else ec.org_entity_structure_fk end and pc.data_name_fk = @PG2_FK) PG2
, (Select value from epacube.epacube.data_value dv with (nolock) inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.epacube.product_category pc with (nolock) on dv.data_name_fk = pc.data_name_fk and dv.data_value_id = pc.data_value_fk and pc.product_structure_fk = ec.product_structure_fk and pc.org_entity_structure_fk = Case When ds.Org_Ind is null then 1 else ec.org_entity_structure_fk end and pc.data_name_fk = @PG3_FK) PG3
, (Select value from epacube.epacube.data_value dv with (nolock) inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.epacube.product_category pc with (nolock) on dv.data_name_fk = pc.data_name_fk and dv.data_value_id = pc.data_value_fk and pc.product_structure_fk = ec.product_structure_fk and pc.org_entity_structure_fk = Case When ds.Org_Ind is null then 1 else ec.org_entity_structure_fk end and pc.data_name_fk = @PG4_FK) PG4
, (Select value from epacube.epacube.data_value dv with (nolock) inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.epacube.product_category pc with (nolock) on dv.data_name_fk = pc.data_name_fk and dv.data_value_id = pc.data_value_fk and pc.product_structure_fk = ec.product_structure_fk and pc.org_entity_structure_fk = Case When ds.Org_Ind is null then 1 else ec.org_entity_structure_fk end and pc.data_name_fk = @PG5_FK) PG5
'

Set @SQLp2 = '
, (Select dv.[description] from epacube.epacube.data_value dv with (nolock) inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.epacube.product_category pc with (nolock) on dv.data_name_fk = pc.data_name_fk and dv.data_value_id = pc.data_value_fk and pc.product_structure_fk = ec.product_structure_fk and pc.org_entity_structure_fk = Case When ds.Org_Ind is null then 1 else ec.org_entity_structure_fk end and pc.data_name_fk = @PG1_FK) PG1_Desc
, (Select dv.[description] from epacube.epacube.data_value dv with (nolock) inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.epacube.product_category pc with (nolock) on dv.data_name_fk = pc.data_name_fk and dv.data_value_id = pc.data_value_fk and pc.product_structure_fk = ec.product_structure_fk and pc.org_entity_structure_fk = Case When ds.Org_Ind is null then 1 else ec.org_entity_structure_fk end and pc.data_name_fk = @PG2_FK) PG2_Desc
, (Select dv.[description] from epacube.epacube.data_value dv with (nolock) inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.epacube.product_category pc with (nolock) on dv.data_name_fk = pc.data_name_fk and dv.data_value_id = pc.data_value_fk and pc.product_structure_fk = ec.product_structure_fk and pc.org_entity_structure_fk = Case When ds.Org_Ind is null then 1 else ec.org_entity_structure_fk end and pc.data_name_fk = @PG3_FK) PG3_Desc
, (Select dv.[description] from epacube.epacube.data_value dv with (nolock) inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.epacube.product_category pc with (nolock) on dv.data_name_fk = pc.data_name_fk and dv.data_value_id = pc.data_value_fk and pc.product_structure_fk = ec.product_structure_fk and pc.org_entity_structure_fk = Case When ds.Org_Ind is null then 1 else ec.org_entity_structure_fk end and pc.data_name_fk = @PG4_FK) PG4_Desc
, (Select dv.[description] from epacube.epacube.data_value dv with (nolock) inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id inner join epacube.epacube.data_set ds with (nolock) on dn.data_set_fk = ds.data_set_id inner join epacube.epacube.product_category pc with (nolock) on dv.data_name_fk = pc.data_name_fk and dv.data_value_id = pc.data_value_fk and pc.product_structure_fk = ec.product_structure_fk and pc.org_entity_structure_fk = Case When ds.Org_Ind is null then 1 else ec.org_entity_structure_fk end and pc.data_name_fk = @PG5_FK) PG5_Desc
, @User CreatedBy
, Case ec.rules_fk when 2116021 then ''Dflt to BASE''
	When 2116022 then ''Dflt to LIST'' 
	else Replace(Replace(Replace(Replace(Replace((Select Name from epacube.synchronizer.rules_action_operation with (nolock) where rules_action_operation_id = ra.rules_action_operation1_fk), '' (N)'', ''''), '' (O)'', ''''), '' (D)'', ''''), '' (L)'', ''''), ''Gross Margin'', ''% GM'') end Operation
, Cast(isnull(ed.Net_Value1_New, sr.NET_VALUE1) * isnull(pa.Attribute_Number, 1) as Numeric(18, 4)) ''Replacement_Cost''
, Cast(isnull(ed.Net_Value2_New, sr.NET_VALUE2) * isnull(pa.Attribute_Number, 1) as Numeric(18, 4)) ''Standard_Cost''
, Cast(isnull(ed.Net_Value3_New, sr.NET_VALUE3) * isnull(pa.Attribute_Number, 1) as Numeric(18, 4)) ''Base_Price''
, Cast(isnull(ed.Net_Value4_New, sr.NET_VALUE4) * isnull(pa.Attribute_Number, 1) as Numeric(18, 4)) ''List_Price''
, Cast(isnull(ed.Net_Value5_New, sr.NET_VALUE5) * isnull(pa.Attribute_Number, 1) as Numeric(18, 4)) ''Vendor_Rebate_Cost''
, uc.uom_code ''Stocking_UOM''
, rtrim(isnull((Select top 1 PD.DESCRIPTION from epacube.epacube.PRODUCT_DESCRIPTION PD with (nolock) Where PD.Data_Name_FK = 210404 And PD.Product_Structure_FK = ec.Product_Structure_FK), '''''''')) Lookup_Name
, (select customer_price_list_sort_fk from epacube.dbo.maui_customer_price_list_profiles with (nolock) where customer_price_list_profiles_id = @Job_FK) ''customer_price_list_sort_fk''
, r.effective_date Price_Rule_Effective_Date
, r.end_date Price_Rule_End_Date
into #Pricelist
from epacube.synchronizer.EVENT_CALC ec with (nolock)
Left Join epacube.marginmgr.SHEET_RESULTS_VALUES_FUTURE SR WITH (NOLOCK) on ec.Product_Structure_FK = sr.Product_Structure_FK and ec.Org_Entity_Structure_FK = sr.Org_Entity_Structure_FK	
Left Join epacube.synchronizer.event_data ed with (NoLock) on ec.Product_Structure_FK = ed.Product_Structure_FK and ec.Org_Entity_Structure_FK = ed.Org_Entity_Structure_FK	
	and event_effective_date = (Select Max(event_effective_date) from epacube.synchronizer.event_data ed with (NoLock) where event_effective_date <= @Eff_Date and product_structure_fk = ed.product_structure_fk and org_entity_structure_fk = ed.org_entity_structure_fk) 
inner join epacube.synchronizer.rules r with (nolock) on ec.rules_fk = r.rules_id and r.result_data_name_fk = 111602
inner join epacube.synchronizer.rules_action ra with (nolock) on ec.rules_fk = ra.rules_fk
inner join epacube.synchronizer.EVENT_CALC_RULES ecr with (nolock) on ec.EVENT_ID = ecr.EVENT_FK and ecr.RESULT_RANK = 1
left join epacube.synchronizer.EVENT_CALC ec_cb with (nolock) on ec.job_fk = ec_cb.job_fk and ec.PRODUCT_STRUCTURE_FK = ec_cb.PRODUCT_STRUCTURE_FK and ec.ORG_ENTITY_STRUCTURE_FK = ec_cb.ORG_ENTITY_STRUCTURE_FK and ec.CUST_ENTITY_STRUCTURE_FK = ec_cb.CUST_ENTITY_STRUCTURE_FK
	and ec.SUPL_ENTITY_STRUCTURE_FK = ec_cb.SUPL_ENTITY_STRUCTURE_FK and ec_cb.RESULT_DATA_NAME_FK = 111501
left join epacube.synchronizer.EVENT_CALC_Rules ecr_cb with (nolock) on ec_cb.EVENT_ID = ecr_cb.EVENT_FK and ecr_cb.RESULT_RANK = 1
inner join epacube.epacube.entity_identification ei_o with (nolock) on ec.org_entity_structure_fk = ei_o.entity_structure_fk and ei_o.entity_data_name_fk like ''141%''
inner join epacube.epacube.entity_identification ei_v with (nolock) on ec.supl_entity_structure_fk = ei_v.entity_structure_fk and ei_v.entity_data_name_fk like ''143%''
left join epacube.epacube.entity_identification ei_stid with (nolock) on ec.cust_entity_structure_fk = ei_stid.entity_structure_fk and ei_stid.entity_data_name_fk = 144020
left join epacube.epacube.entity_identification ei_btid with (nolock) on ec.cust_entity_structure_fk = ei_btid.entity_structure_fk and ei_btid.entity_data_name_fk = 144010
left join epacube.epacube.entity_structure es with (nolock) on ec.cust_entity_structure_fk = es.entity_structure_id and es.data_name_fk = 144020
left join epacube.epacube.entity_identification ei_btid2 with (nolock) on es.parent_entity_structure_fk = ei_btid2.entity_structure_fk and ei_btid2.entity_data_name_fk = 144010
Left Join epacube.epacube.product_attribute pa with (nolock) on ec.product_structure_fk = pa.product_structure_fk and pa.data_name_fk = 210846
left join epacube.epacube.PRODUCT_UOM_CLASS UOM with (nolock) on UOM.Product_Structure_FK = ec.Product_Structure_FK and UOM.Data_Name_FK = 110603 
left join epacube.epacube.uom_code UC with (nolock) on  UC.UOM_Code_ID = UOM.UOM_Code_FK 
where ec.RESULT_DATA_NAME_FK = 111602 
and ec.JOB_FK = @Job_FK
'
Set @SQLp3 = '

Create Index idx_ps on #PriceList(product_structure_FK)

	Insert Into ' + @INSERT_TABLE + '
	([Bill-To Customer ID], [Bill-To Customer Name], [Ship-To Customer ID], [Ship-To Customer Name], [UPC], [Product_ID], [Product_Description], [Sell Price], [Pricing Cost], [Rebate], [Rebate_Cap_Sell], [Whse_ID], [Vendor_ID], [Rebate Record #], [Pricing Record #], [Sell_Price_Cust_Rules_FK], [Prices_Calculated_Date], [Product_Structure_FK], [Org_Entity_Structure_FK], [Cust_Entity_Structure_FK], [Supl_Entity_Structure_FK], [WAREHOUSE_RECORD], [SALESREP_RECORD_IN], [SALESREP_RECORD_OUT], [Job_FK], [Cust_Group2], [SPC_UOM], Prod_Group1, Prod_Group2, Prod_Group3, Prod_Group4, Prod_Group5, [Prod_Group1_Desc], [Prod_Group2_Desc], [Prod_Group3_Desc], [Prod_Group4_Desc], [Prod_Group5_Desc], CreatedBy, Operation, Replacement_Cost, Standard_Cost, Base_Price, List_Price, Vendor_Rebate_Cost, [Stocking_UOM], [Lookup_Name], [customer_price_list_sort_fk], Price_Rule_Effective_Date, Price_Rule_End_Date)
	Select * from #Pricelist

If @PairLIPitems <> 0
Begin

	Select * into #MatSetup from #Pricelist where Job_FK = @Job_FK

	Create Table #Variations (Product_Groupings_FK Int, Variations Varchar(128))
	Create Index idx_vars on #Variations(Product_Groupings_FK)	

	Declare @Groupings_FK Varchar(max)
	Declare @Grp Varchar(max)

	Set @Groupings_FK = ''''

				DECLARE Groupings Cursor local for
				Select mpv.Product_Groupings_FK
					from epacube.dbo.MAUI_Product_Variations MPV with (nolock)
					inner join #Pricelist PL on MPV.product_structure_FK = PL.Product_Structure_FK and PL.Job_FK = @Job_FK
					group by mpv.product_groupings_fk
			
					 OPEN Groupings;

				 FETCH NEXT FROM Groupings INTO @GRP
					 WHILE @@FETCH_STATUS = 0

				Begin
				Declare @Product_Groupings_FK Int
				Declare @Var Varchar(128)
				Declare @Variations Varchar(Max)
				Set @Variations = ''''

				DECLARE Variations Cursor local for
				Select mpv.Product_Groupings_FK, MPV.Variation
				from epacube.dbo.MAUI_Product_Variations MPV with (nolock) 
				inner join #Pricelist PL on MPV.product_structure_FK = PL.Product_Structure_FK and PL.Job_FK = @Job_FK
				Where mpv.product_groupings_fk = @grp
				group by mpv.product_groupings_fk, mpv.variation
				order by MPV.Variation
		
				 OPEN Variations;

			 FETCH NEXT FROM Variations INTO @Product_Groupings_FK, @Var
				 WHILE @@FETCH_STATUS = 0

			Begin

				Set @Variations = @Variations + '', '' + @Var

				FETCH NEXT FROM Variations INTO @Product_Groupings_FK, @Var    
			End
				Set @Variations = ''Insert into #Variations (Variations, Product_Groupings_FK) Select ''''('' + Right(@Variations, Len(@Variations) -2) + '')'''', '' + Cast(@Product_Groupings_FK as Varchar(64))
				Exec (@Variations)
	Close Variations;
	Deallocate Variations;

					FETCH NEXT FROM Groupings INTO @GRP    
				End

	Close Groupings;
	Deallocate Groupings;

	Insert Into ' + @INSERT_TABLE + '
	([Bill-To Customer ID], [Bill-To Customer Name], [Ship-To Customer ID], [Ship-To Customer Name], [UPC], [Product_ID], [Product_Description], [Sell Price]
	, [Whse_ID], [Vendor_ID], WAREHOUSE_RECORD, SALESREP_RECORD_IN, SALESREP_RECORD_OUT
	, [Prices_Calculated_Date], [Job_FK], Prod_Group1, Prod_Group2, Prod_Group3, Prod_Group4, Prod_Group5, [Prod_Group1_Desc], [Prod_Group2_Desc], [Prod_Group3_Desc], [Prod_Group4_Desc], [Prod_Group5_Desc], SPC_UOM, CreatedBy, Variations, Stocking_UOM, customer_price_list_sort_fk)
	Select Distinct
	[Bill-To Customer ID], [Bill-To Customer Name], [Ship-To Customer ID], [Ship-To Customer Name], [UPC]
	, Case When isnull(MPG.Product_Groupings_ID, 0) > 0 then ''Multi'' else PL.[Product_ID] end ''Product_ID''
	, Case When isnull(MPG.Product_Groupings_ID, 0) > 0 then mpg.Description else PL.[Product_Description] end [Product_Description]
	, [Sell Price]
	, [Whse_ID], [Vendor_ID], [WHSE RECORD], [SALESREP IN], [SALESREP OUT]
	, [Effective_Date], [Job_FK], PG1, PG2, PG3, PG4, PG5, PG1_Desc, PG2_Desc, PG3_Desc, PG4_Desc, PG5_Desc, SPC_UOM, CreatedBy, Variations, Stocking_UOM, customer_price_list_sort_fk
	from #Pricelist PL
	Left Join epacube.dbo.MAUI_Products_Grouped MPGd with (nolock) on PL.Product_Structure_FK = MPGd.Product_Structure_FK
	Left join epacube.dbo.MAUI_product_Groupings MPG with (nolock) on MPGd.product_groupings_fk = MPG.product_groupings_id
	Left Join #Variations V on MPG.product_groupings_id = V.product_groupings_FK
	where isnull(MPG.product_groupings_id, 0) <> 0
End

IF object_id(''tempdb..#Pricelist'') is not null
drop table #Pricelist;

IF object_id(''tempdb..#MatSetup'') is not null
drop table #MatSetup

IF object_id(''tempdb..#Variations'') is not null
drop table #Variations
'
exec(@SQLp1 + @SQLp2 + @SQLp3)

Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Cust_Group1'',''' +  @CG1 + ''', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Cust_Group2'',''' +  @CG2 + ''', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Cust_Group3'',''' +  @CG3 + ''', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Prod_Group1'',''' +  @PG1 + ''', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Prod_Group2'',''' +  @PG2 + ''', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Prod_Group3'',''' +  @PG3 + ''', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Prod_Group4'',''' +  @PG4 + ''', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Prod_Group5'',''' +  @PG5 + ''', ''column''')

Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Cust_Group1_DESC'',''' +  @CG1 + '_DESC'', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Cust_Group2_DESC'',''' +  @CG2 + '_DESC'', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Cust_Group3_DESC'',''' +  @CG3 + '_DESC'', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Prod_Group1_DESC'',''' +  @PG1 + '_DESC'', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Prod_Group2_DESC'',''' +  @PG2 + '_DESC'', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Prod_Group3_DESC'',''' +  @PG3 + '_DESC'', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Prod_Group4_DESC'',''' +  @PG4 + '_DESC'', ''column''')
Exec('EXEC sp_rename ''' + @INSERT_TABLE + '.Prod_Group5_DESC'',''' +  @PG5 + '_DESC'', ''column''')
--Print(@SQLp1)
--Print(@SQLp2)
--Print(@SQLp3)

end
