﻿ 

--A_MAUI_Price_List_Calcs_AutoIndex

-- =============================================
-- Author:		<Gary Stone>
-- Create date: <September 28, 2016>
-- =============================================
CREATE PROCEDURE [CPL].[Price_List_Calcs_AutoIndex] @Job_FK bigint
AS
BEGIN

	SET NOCOUNT ON;

	Declare @SQLcd Varchar(Max)
	Declare @Column_Name varchar(128)
	Declare @IndexName varchar(64)

	Declare @Table_Name Varchar(128)
	Set @Table_Name = isnull(( select top 1 Table_Name from reports.Reports_Header where Job_FK = @Job_FK and ExportDataFileName is not null), '')
	
	Set @IndexName = 'idx_dn'
	Set @Column_Name = isnull(( select top 1 ExportDataFileName from reports.Reports_Header where Job_FK = @Job_FK and ExportDataFileName is not null), '')

If @Column_Name = '' or @Table_Name = ''
goto eop
	
	Set @SQLcd =  
	'IF EXISTS (select si.name from sysindexes si with (nolock) inner join sysobjects so with (nolock) on si.id = so.id where si.name = ''' + @IndexName + ''' and so.name = ''' + Replace(@Table_Name, 'CPL.', '') + ''')
	Drop index idx_dn on ' + @Table_Name 
	Exec(@SQLcd)

	Set @SQLcd = 
	' Create index ' + @IndexName + ' on ' + @Table_Name + '([' + @Column_Name + '])'

	exec(@SQLcd)
eop:

END
