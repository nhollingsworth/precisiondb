﻿--A_MAUI_Price_Template_Calculations_Generate

CREATE PROCEDURE [CPL].[Price_Template_Calculations_Generate]
@Job_FK BigInt
, @Job_Type Varchar(64) = Null
, @Eff datetime = Null
--, @Prods Varchar(Max) = Null
--, @Custs Varchar(Max) = Null
--, @Orgs Varchar(Max) = Null
--, @ServiceBroker Bigint = Null
--, @SALES_HISTORY_IND Int = Null

AS
BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Declare @Job_FK Varchar(32)
--Declare @Job_Type Varchar(64)
--Declare @Eff datetime
--Set @Job_FK = 3345
--Set @Eff = '2012-11-12'
--Declare @Result_Type_CR_FK Int
--Declare @Prods Varchar(Max) = Null
--Declare @Custs Varchar(Max) = Null
--Declare @Orgs Varchar(Max) = Null
--Declare @SALES_HISTORY_IND Int = 1
--Declare @ServiceBroker Bigint = Null
--Declare @Job_FK bigint

Declare @Customer_Price_List_Template_FK Varchar(16)
Declare @Product_Price_List_Template_FK Varchar(16)
Declare @Result_Type_CR_FK Int
Declare @Whse_Source Varchar(64)
Declare @Region_Sel Varchar(64)
Declare @Whse_Sel Varchar(64)
Declare @Process_Schedule Varchar(16)
Declare @Job Varchar(32)
Declare @SQL_CD Varchar(Max)
Declare @StartTime datetime

Declare @COST_CHANGE_FIRST_IND Int
Declare @COST_CHANGE_PRIOR_IND Int

If @Job_Type is null
Set @Job_Type = 'Pricing Template'

Set @Result_Type_CR_FK = Case when ISNULL(@Eff, getdate()) > getdate() then 711 else 710 end

Set @COST_CHANGE_FIRST_IND = Case When @Result_Type_CR_FK = 711 and @EFF is Null then 1 else 0 end
Set @COST_CHANGE_PRIOR_IND = Case When @Result_Type_CR_FK = 711 and @EFF is not Null then 1 else 0 end
Set @StartTime = GETDATE()

Insert into epacube.dbo.maui_log
(Job_fk_i, Activity, Start_Time)
Select @Job_FK, 'MAUI_Generate_CPL_Template_Results-' + CAST(@Job_FK as Varchar(16)), @StartTime

Declare PriceBook Cursor local for
Select top 1 Customer_Price_List_Profiles_ID
, Customer_Price_List_Template_FK
, Product_Price_List_Template_FK
, Case When Warehouse_Price_List_Template_FK = Customer_Price_List_Template_FK Then 'Customer'
	When Warehouse_Price_List_Template_FK = Product_Price_List_Template_FK Then 'Product'
	When isnull(Whse_Sel, '') > '' Then 'Global Whse'
	When isnull(Region_Sel, '') > '' then 'Global Region' End 'Whse Source'
, Region_Sel
, Whse_Sel
, Process_Schedule
from epacube.dbo.MAUI_Customer_Price_List_Profiles with (nolock)
where Customer_Price_List_Profiles_ID = @Job_FK

Truncate Table epacube.MarginMgr.ORDER_DATA_HISTORY
	
OPEN PriceBook;
FETCH NEXT FROM PriceBook INTO @Job, @Customer_Price_List_Template_FK, @Product_Price_List_Template_FK, @Whse_Source, @Region_Sel, @Whse_Sel, @Process_Schedule
    
WHILE @@FETCH_STATUS = 0
Begin
	
	IF object_id('tempdb..##PL_Recs') is not null
	drop table ##PL_Recs

	IF object_id('tempdb..##Prod_FKs') is not null
	drop table ##Prod_FKs

	IF object_id('tempdb..##Cust_FKs') is not null
	drop table ##Cust_FKs

	IF object_id('tempdb..##Org_FKs') is not null
	drop table ##Org_FKs

	IF object_id('tempdb..##Supl_FKs') is not null
	drop table ##Supl_FKs

	IF object_id('tempdb..##CustOrgs') is not null
	drop table ##CustOrgs
	
	IF object_id('tempdb..##PA_Record') is not null
	drop table ##PA_Record

	Update Profiles
	Set intProcess = 2
	--, Template_Job_FKs = @Job_FK
	, Template_Process_Date = Getdate()
	from epacube.dbo.MAUI_Customer_Price_List_Profiles Profiles
	Where 1 = 1
	and Customer_Price_List_Profiles_ID = @Job

	Select * , IDENTITY(int, 1,1) AS Row_ID
	into ##PL_Recs
	from (
	Select
	Cast(Customer_Price_List_Profiles_ID as int) Customer_Price_List_Profiles_ID
	, isnull(Customers.Load_Entity_Data_Name_FK, Customers.Load_Data_Name_FK) Cust_Data_Name_FK
	, Customers.Load_Host_Value Cust_Host_Value
	, Products.Load_Data_Name_FK Prod_Data_Name_FK
	, Products.Load_Host_Value Prod_Host_Value
	, Case When @Whse_Source = 'Customer' Then Customers.Load_Host_Whse
				When @Whse_Source = 'Product' Then Products.Load_Host_Whse
		else Null end 'Whse'
	, Null 'Product_Structure_FK'
	from epacube.dbo.MAUI_Customer_Price_List_Profiles Profiles with (nolock)
		inner join epacube.dbo.MAUI_Customer_Price_List_Data_Load Customers with (nolock) on Profiles.Customer_Price_List_Template_FK = Customers.Price_List_Template_FK and isnull(Customers.Load_Valid, 0) <> 0
		inner join epacube.dbo.MAUI_Customer_Price_List_Data_Load Products with (nolock) on Profiles.Product_Price_List_Template_FK = Products.Price_List_Template_FK and isnull(Products.Load_Valid, 0) <> 0
	where Customer_Price_List_Profiles_ID = @Job
	) Calcs
	group by Customer_Price_List_Profiles_ID, Cust_Data_Name_FK, Cust_Host_Value, Prod_Data_Name_FK, Prod_Host_Value, Whse, Product_Structure_FK

	Create Unique Index idx_st on ##PL_Recs(Customer_Price_List_Profiles_ID, Row_ID)

	Select Row_ID
	, (Select product_structure_fk from epacube.epacube.product_identification with (nolock) where value = PL.Prod_Host_Value and data_name_fk = PL.Prod_Data_Name_FK) Product_Structure_FK
	into ##Prod_FKs
	from ##PL_Recs PL

	Delete from ##Prod_FKs where product_structure_fk is null
	
	Create Index PFK on ##Prod_FKs(Row_ID, product_structure_fk)

	Update PL
		Set Product_Structure_FK = FK.Product_Structure_FK
		from ##PL_Recs PL 
			inner Join ##Prod_FKs FK on PL.Row_ID = FK.Row_ID
		where Customer_Price_List_Profiles_ID = @Job

	Select Row_ID
	, (Select entity_structure_fk from epacube.epacube.entity_identification with (nolock) where value = PL.Cust_Host_Value and entity_data_name_fk = PL.Cust_Data_Name_FK) Cust_Entity_Structure_FK
	into ##Cust_FKs
	from ##PL_Recs PL

	Delete from ##Cust_FKs where Cust_Entity_structure_fk is null

	Create Index CFK on ##Cust_FKs(Row_ID, Cust_Entity_structure_fk)

	If @Whse_Source in ('Customer', 'Product')
		Begin
			Set @SQL_CD = '
			Select Row_ID
			, PL.Whse
			, (Select entity_structure_fk from epacube.epacube.entity_identification with (nolock) where value = PL.Whse and data_name_fk = 141111) Org_Entity_Structure_FK
			into ##Org_FKs
			from ##PL_Recs PL where Customer_Price_List_Profiles_ID = ' + @Job
		end
	Else if @Whse_Source = 'Global Whse'
		Begin
			Set @SQL_CD = '
			Select Row_ID
			, ' + '''' + @Whse_Sel + '''' + '
			, (Select entity_structure_fk from epacube.epacube.entity_identification with (nolock) where value = ' + '''' + @Whse_Sel + '''' + ' and data_name_fk = 141111) Org_Entity_Structure_FK
			into ##Org_FKs
			From ##PL_Recs PL where Customer_Price_List_Profiles_ID = ' + @Job
		End
	Else if @Whse_Source = 'Global Region'
		Begin
			Set @SQL_CD = '
			Select 
			Whse
			, Row_ID
			, Isnull(
			(select top 1 PA_O.org_entity_structure_fk from epacube.epacube.product_association PA_O with (nolock) 
				inner join epacube.epacube.product_association PA_V with (nolock) on PA_O.product_Structure_FK = PA_V.Product_Structure_FK and PA_O.data_name_FK = 159100 and PA_V.data_Name_FK = 253501
			where PA_O.product_structure_fk = PL.Product_Structure_FK
				and PA_O.ORG_ENTITY_STRUCTURE_FK in 
					(select  es.ENTITY_STRUCTURE_ID from epacube.epacube.entity_identification ei  with (nolock)
						inner join epacube.epacube.ENTITY_STRUCTURE es with (nolock) on ei.ENTITY_STRUCTURE_FK = es.entity_structure_id
						inner join epacube.epacube.entity_ident_nonunique eip with (nolock) on es.parent_entity_structure_fk = eip.entity_structure_fk and eip.ENTITY_DATA_NAME_FK = 141030 and eip.Data_Name_FK = 141112
						where eip.value = ' + '''' + @Region_Sel + '''' + ')) 
			,
			(select top 1 org_entity_structure_fk from epacube.epacube.product_association with (nolock) where data_name_fk = 159100 and product_structure_fk = PL.Product_Structure_FK
				and ORG_ENTITY_STRUCTURE_FK in 
					(select  es.ENTITY_STRUCTURE_ID from epacube.epacube.entity_identification ei  with (nolock)
						inner join epacube.epacube.ENTITY_STRUCTURE es with (nolock) on ei.ENTITY_STRUCTURE_FK = es.entity_structure_id
						inner join epacube.epacube.entity_ident_nonunique eip with (nolock) on es.parent_entity_structure_fk = eip.entity_structure_fk and eip.ENTITY_DATA_NAME_FK = 141030 and eip.Data_Name_FK = 141112
						where eip.value = ' + '''' + @Region_Sel + '''' + ')) 
			) 
			Org_Entity_Structure_FK
			into ##Org_FKs
			From ##PL_Recs PL where PL.Customer_Price_List_Profiles_ID = ' + @Job
		end
	Else
		Begin
			Set @SQL_CD = '
			Select Row_ID
			, ei_o.[value] Whse
			, ei_o.entity_structure_fk Org_Entity_Structure_FK
			into ##Org_FKs
			From ##PL_Recs PL 
				inner join epacube.epacube.entity_identification ei with (nolock) on pl.cust_host_value = ei.value and pl.cust_data_name_fk = ei.entity_data_name_fk
				inner join epacube.epacube.entity_attribute ea with (nolock) on ei.entity_structure_fk = ea.entity_structure_fk and ea.data_name_fk = 144900
				inner join epacube.epacube.entity_identification ei_o with (nolock) on ea.attribute_event_data = ei_o.value and ei_o.entity_data_name_fk = 141000
			where PL.Customer_Price_List_Profiles_ID = ' + @Job
		End
		
	Exec(@SQL_CD)

		Create Index idx_orgFK on ##Org_FKs(Row_ID, Org_Entity_Structure_FK, Whse)
		
	If @Whse_Source = 'Global Region'
		Begin
			Update OFK
			Set Whse = (Select value from epacube.epacube.entity_identification with (nolock) where entity_data_name_fk = 141000 and entity_structure_fk = OFK.Org_Entity_Structure_FK)
			from ##Org_FKs OFK
		End

	Select PL.Row_ID
	, (select top 1 entity_structure_fk from epacube.epacube.product_association with (nolock) 
		where data_name_fk = 253501 and product_structure_fk = PL.Product_Structure_FK and Org_Entity_Structure_FK = OFK.Org_Entity_Structure_FK) Supl_Entity_Structure_FK
	into ##SUPL_FKs
	from ##PL_Recs PL
		inner join ##Org_FKs OFK on PL.Row_ID = OFK.Row_ID

	Delete from ##SUPL_FKs where Supl_Entity_Structure_FK is null

	Create Index SFK on ##SUPL_FKs(Row_ID, Supl_Entity_Structure_FK)
	
	select custs.Cust_Entity_Structure_FK, ei.ENTITY_STRUCTURE_FK Org_Entity_Structure_FK, ei.VALUE Whse 
	into ##CustOrgs
	from ##Cust_FKs Custs 
	inner join epacube.epacube.entity_attribute EA with (nolock) on custs.cust_entity_structure_fk = ea.ENTITY_STRUCTURE_FK and ea.DATA_NAME_FK = 144900
	inner join epacube.epacube.ENTITY_IDENTIFICATION ei with (nolock) on ea.ATTRIBUTE_EVENT_DATA = ei.VALUE and ei.DATA_NAME_FK = 141111 and ei.ENTITY_DATA_NAME_FK = 141000
	group by custs.Cust_Entity_Structure_FK, ei.ENTITY_STRUCTURE_FK, ei.VALUE

	Create Index idx_co on ##CustOrgs(Cust_Entity_Structure_FK, Org_Entity_Structure_FK, Whse)

	Select pa.PRODUCT_STRUCTURE_FK, pa.ORG_ENTITY_STRUCTURE_FK, co.WHSE, co.cust_entity_structure_fk 
	into ##PA_Record
	from ##Prod_FKs prods
	inner join epacube.epacube.PRODUCT_ASSOCIATION pa with (nolock) on prods.product_structure_fk = pa.PRODUCT_STRUCTURE_FK and pa.DATA_NAME_FK = 159100
	inner join ##CustOrgs co on pa.ORG_ENTITY_STRUCTURE_FK = co.Org_Entity_Structure_FK
	group by pa.PRODUCT_STRUCTURE_FK, pa.ORG_ENTITY_STRUCTURE_FK, co.WHSE, co.cust_entity_structure_fk

	Create Index idx_par on ##PA_Record(PRODUCT_STRUCTURE_FK, ORG_ENTITY_STRUCTURE_FK, WHSE, CUST_ENTITY_STRUCTURE_FK)

		Insert into epacube.MarginMgr.ORDER_DATA_HISTORY
		(
		DRANK
		, TRANSACTION_TYPE
		, ORDER_DATE
		, PRODUCT_STRUCTURE_FK
		, ORG_ENTITY_STRUCTURE_FK
		, CUST_ENTITY_STRUCTURE_FK
		, SUPL_ENTITY_STRUCTURE_FK
		, PRODUCT_ID_HOST
		, CUSTOMER_ID_HOST
		, ORG_ID_HOST
		, IMPORT_FILENAME
		, SALES_QTY
		, QTY_MODE
		, QTY_MEDIAN
		, QTY_MEAN
		, RECORD_STATUS_CR_FK
		, IMPORT_JOB_FK
		)
		Select 
		DRANK
		, TRANSACTION_TYPE
		, ORDER_DATE
		, PRODUCT_STRUCTURE_FK
		, ORG_ENTITY_STRUCTURE_FK
		, CUST_ENTITY_STRUCTURE_FK
		, SUPL_ENTITY_STRUCTURE_FK
		, PRODUCT_ID_HOST
		, CUSTOMER_ID_HOST
		, Whse ORG_ID_HOST
		, 'PRICE LIST TEMPLATE ' + CONVERT(VARCHAR(10), GETDATE(), 102) IMPORT_FILENAME
		, 1 SALES_QTY
		, 1 QTY_MODE
		, 1 QTY_MEDIAN
		, 1 QTY_MEAN
		, 1 RECORD_STATUS_CR_FK
		, @Job
		from
		(Select
		1 DRANK
		,'STOCK ORDER' Transaction_Type
		,GETDATE() ORDER_DATE
		,PL.product_structure_fk
		,isnull(par.whse, OFK.Whse) Whse
		, isnull(par.ORG_ENTITY_STRUCTURE_FK, OFK.Org_Entity_Structure_FK) Org_Entity_Structure_FK
		, CFK.Cust_Entity_Structure_fk
		, SFK.Supl_Entity_Structure_FK
		, Prod_Host_Value Product_ID_Host
		, Cust_Host_Value Customer_ID_Host
		from ##PL_Recs PL
			Inner Join ##Org_FKs OFK on PL.Row_ID = OFK.Row_ID
			Inner Join ##Cust_FKs CFK on PL.Row_ID = CFK.Row_ID
			Left Join ##SUPL_FKs SFK on PL.Row_ID = SFK.Row_ID
			Left Join ##PA_Record par on cfk.Cust_Entity_Structure_FK = par.Cust_Entity_Structure_FK and PL.Product_Structure_FK = par.PRODUCT_STRUCTURE_FK
			Where Customer_Price_List_Profiles_ID = @Job
			and PL.product_structure_fk is not null and OFK.Org_Entity_Structure_FK is not null and CFK.Cust_Entity_Structure_fk is not null
		) Sales

FETCH NEXT FROM PriceBook INTO @Job, @Customer_Price_List_Template_FK, @Product_Price_List_Template_FK, @Whse_Source, @Region_Sel, @Whse_Sel, @Process_Schedule
End
Close PriceBook;
Deallocate PriceBook;

IF object_id('tempdb..##PL_Recs') is not null
drop table ##PL_Recs

IF object_id('tempdb..##Prod_FKs') is not null
drop table ##Prod_FKs

IF object_id('tempdb..##Cust_FKs') is not null
drop table ##Cust_FKs

IF object_id('tempdb..##Org_FKs') is not null
drop table ##Org_FKs

IF object_id('tempdb..##Supl_FKs') is not null
drop table ##Supl_FKs

IF object_id('tempdb..##CustOrgs') is not null
drop table ##CustOrgs

IF object_id('tempdb..##PA_Record') is not null
drop table ##PA_Record

------------------------------------------------------------
EXEC MarginMgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, 0

Insert into epacube.MarginMgr.ANALYSIS_JOB
(Analysis_Job_ID, Analysis_Name, result_type_cr_fk, result_effective_date, rule_type_filter, SALES_HISTORY_IND)
--Select @Job_FK, 'MAUI ' + @Job_Type + ' - ' + Cast(@Job_FK as Varchar(32)), @Result_Type_CR_FK, @Eff, '(313)', 1
Select @Job_FK, 'MAUI ' + @Job_Type + ' - ' + Cast(@Job_FK as Varchar(32)), @Result_Type_CR_FK, @Eff, '(312, 313)', 1

EXEC  epacube.[marginmgr].[CALCULATION_PROCESS] @Job_FK

Exec [CPL].[Pricing_Table_Create] @Job_FK, '', '', 'cpl.Price_List_Template_'

Exec [CPL].[Pricing_Table_Qty_Brackets] @Job_FK, 'cpl.Price_List_Template_'

Update Profiles
Set intProcess = 3
, Template_Process_Date = Getdate()
, strProcessingTime = Convert(Varchar(10),GETDATE() - @StartTime, 108)
, datRecordEffectiveDate = Convert(Varchar(10),GETDATE(), 101)
from epacube.dbo.MAUI_Customer_Price_List_Profiles Profiles
Where 1 = 1
and Customer_Price_List_Profiles_ID = @Job_FK

EXEC MarginMgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, 0

Set @SQL_CD = '
Update RH
Set Table_Name = ''CPL.Price_List_Template_' + CAST(@Job_FK as Varchar(16)) + '''
from reports.Reports_Header RH
where Job_FK = ' + CAST(@Job_FK as Varchar(16)) + '

Update RD
Set Table_Name = ''CPL.Price_List_Template_' + CAST(@Job_FK as Varchar(16)) + '''
from reports.Reports_Details RD
where Job_FK = ' + CAST(@Job_FK as Varchar(16)) + '

Update RC
Set Table_Name = ''CPL.Price_List_Template_' + CAST(@Job_FK as Varchar(16)) + '''
from reports.Reports_Calcs RC
where Job_FK = ' + CAST(@Job_FK as Varchar(16))

exec(@SQL_CD)


Set @SQL_CD = '
Update ML
Set Duration = ''' + Convert(Varchar(10),GETDATE() - @StartTime, 108) + '''
, Record_Inserts = (select COUNT(*) from cpl.Price_List_Template_' + Cast(@Job_FK as varchar(16)) + ')
from epacube.dbo.MAUI_Log ML where MAUI_Log_ID = (Select top 1 Maui_Log_ID from epacube.dbo.MAUI_Log with (nolock) where Job_fk_i = ' + Cast(@Job_FK as varchar(16)) + ' order by Maui_Log_ID Desc)'
Exec(@SQL_CD)

IF object_id('tempdb..##spc') is not null
drop table ##spc;

END
