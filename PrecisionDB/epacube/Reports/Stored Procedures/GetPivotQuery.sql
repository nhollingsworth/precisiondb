﻿ 

--A_MAUI_GetPivotQuery

-- =============================================
-- Author:		<Gary Stone>
-- Create date: <September 28, 2016>
-- =============================================
CREATE PROCEDURE [Reports].[GetPivotQuery] 
	-- Add the parameters for the stored procedure here
	@Report_FK bigint, @tbl varchar(64), @Alias varchar(64), @RefCol varchar(64)
AS
BEGIN

	SET NOCOUNT ON;

--Declare @Report_FK bigint
--Declare @tbl varchar(64)
--Declare @Alias varchar(64)
--Declare @RefCol varchar(64)

--Set @Report_FK = 1160
--Set @tbl = '##Recs'
--Set @Alias = 'Recs'
--Set @RefCol = 'RN'
	
If Object_ID('tempdb..##Pivots') is not null 
drop table ##Pivots

Declare @Sel varchar(Max)
Declare @ColName varchar(64)
Declare @PivotAction varchar(32)
Declare @SelPivot varchar(Max)
Declare @Group varchar(Max)
Declare @From Varchar(Max)
Declare @Derivation varchar(Max)
Declare @DataType varchar(64)

Declare @PvtCol varchar(32)
, @PvtValue varchar(32)

Set @Sel = 'Select '
Set @Group =  'Group by [' + @RefCol + '], '
Set @SelPivot = ''
Set @From = ' From ' + @tbl + ' ' + @Alias + ' '

		DECLARE SelBasic Cursor local for
		Select Isnull(ISNULL(rc.Calc_Column_Label, D.column_Name_Alias), D.Column_Label) ColName, D.PivotAction, D.Derivation
		from reports.Reports_Details D
		left join reports.Reports_Calcs RC on D.Report_FK = rc.Report_FK and D.Column_Name = RC.Column_Name
		where D.Report_FK = @Report_FK
		and PivotAction not in ('Exclude')
		and PivotAction not like 'Pivot%'
		Order by d.seq

			 OPEN SelBasic;
			 FETCH NEXT FROM SelBasic INTO @ColName, @PivotAction, @Derivation
	            
			 WHILE @@FETCH_STATUS = 0
		Begin
			Set @Sel = @Sel +	Case  when @PivotAction = 'None' then '[' + @ColName + ']'
										Else @PivotAction + '([' + @ColName + ']) ' End + ' ''' + @ColName + '''' + ', '
										
			Set @Group = @Group + Case when @PivotAction = 'None' then 
									--Case When @Derivation <> '' then @Derivation
									--	else
										'[' + @ColName + '], ' --end 
									else '' 
									end
		
			FETCH NEXT FROM SelBasic INTO @ColName, @PivotAction, @Derivation

		End
		Close SelBasic;
		Deallocate SelBasic;

	Set @PvtCol = 
			(Select top 1
			isnull(ISNULL(rc.Calc_Column_Label, D.column_Name_Alias), D.Column_Label) ColName
			from reports.Reports_Details D with (nolock)
			left join reports.Reports_Calcs RC on D.Report_FK = rc.Report_FK and D.Column_Name = RC.Column_Name
			where D.Report_FK = @Report_FK
			and D.PivotAction = 'Pivot Reference')
	
	Set @DataType = (Select Data_Type from reports.Reports_Details D with (nolock) where D.Report_FK = @Report_FK and D.PivotAction = 'Pivot Reference')
	
	Exec('Select [' + @PvtCol + '] PvtCol into ##Pivots from ' + @tbl + ' group by [' + @PvtCol + '] order by [' + @PvtCol + ']')		
		
	Declare Pivots Cursor local for
	Select PvtCol from ##Pivots order by PvtCol
	
			 OPEN Pivots;
			 FETCH NEXT FROM Pivots INTO @PvtValue
	            
			 WHILE @@FETCH_STATUS = 0
		Begin

		DECLARE SelPivot Cursor local for
		Select isnull(ISNULL(rc.Calc_Column_Label, D.column_Name_Alias), D.Column_Label) ColName
		from reports.Reports_Details D
		left join reports.Reports_Calcs RC on D.Report_FK = rc.Report_FK and D.Column_Name = RC.Column_Name
		where D.Report_FK = @Report_FK
		and PivotAction = 'Pivoted Data'
		Order by d.seq
		
			 OPEN SelPivot;
			 FETCH NEXT FROM SelPivot INTO @ColName
	            
			 WHILE @@FETCH_STATUS = 0
		Begin
			
			
			Set @SelPivot = @SelPivot + 
				Case when @DataType like '%Date%' Or @DataType = 'Varchar' then
						'(Select [' + @ColName + '] from ' + @tbl + ' where ' + @RefCol + ' = ' + @alias + '.' + @RefCol + ' and [' + @PvtCol + '] = ''' + @PvtValue + ''') ' + '''' + @ColName + ' ' + @PvtValue + '''' + ', '
					Else
						'(Select [' + @ColName + '] from ' + @tbl + ' where ' + @RefCol + ' = ' + @alias + '.' + @RefCol + ' and [' + @PvtCol + '] = ' + @PvtValue + ') ' + '''' + @ColName + ' ' + @PvtValue + '''' + ', '
					end
			FETCH NEXT FROM SelPivot INTO @ColName  

		End
		Close SelPivot;
		Deallocate SelPivot;

			FETCH NEXT FROM Pivots INTO @PvtValue   

		End
		Close Pivots;
		Deallocate Pivots;

	Set @Group = Left(@Group, Len(@Group) - 1)
	Set @SelPivot = Left(@SelPivot, Len(@SelPivot) - 1)
	Exec(@Sel + @SelPivot + ' into ##PivotQry ' + @From + @Group)
End
