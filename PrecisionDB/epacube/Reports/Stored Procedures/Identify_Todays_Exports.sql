﻿--A_MAUI_Identify_Todays_Exports

CREATE PROCEDURE [Reports].[Identify_Todays_Exports]
AS
BEGIN

	SET NOCOUNT ON;

	Declare @PlusDays int
	Declare @sysdate datetime
	Set @PlusDays = 0
	set @Sysdate = Cast(Convert(varchar(10), getdate() + @PlusDays , 1) as datetime)

	IF object_id('Reports.Reports_Scheduled') is not null
	drop table Reports.Reports_Scheduled
	
	Select
	Report_ID Report_FK, Cast(Job_FK as bigint) Job_FK, ReportCalcDateNext, ExportPath
	, ExportFileName, ExportDataFileName, Fnprefix, Fnsuffix, IncludeDateStamp, Exporttype, eMailAddressRW, exportColumnDelimiter, exportTextDelimiter, exportColumnNames, exportFileNameExt, GETDATE() Create_Timestamp, CAST(Null as datetime) Report_Processed
	into Reports.Reports_Scheduled
	from reports.Reports_Header RH
	where (rh.SchedReportCreateAnnually is not null or rh.SchedReportCreateDayOfMonth is not null or rh.SchedReportCreateDay is not null)
	And intSchedulingActive = -1
	And
		Case
			When rh.SchedReportCreateAnnually is not null
			then
				Case When Cast(Cast(DATEPART(m, rh.SchedReportCreateAnnually) as varchar(2)) + '/' + Cast(DATEPART(d, rh.SchedReportCreateAnnually) as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime) >= @sysdate then
					Cast(Cast(DATEPART(m, rh.SchedReportCreateAnnually) as varchar(2)) + '/' + Cast(DATEPART(d, rh.SchedReportCreateAnnually) as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime)
					else  Cast(Cast(DATEPART(m, rh.SchedReportCreateAnnually) as varchar(2)) + '/' + Cast(DATEPART(d, rh.SchedReportCreateAnnually) as varchar(2)) + '/' + Cast(YEAR(@sysdate) + 1 as varchar(4)) AS datetime) end 
			When rh.SchedReportCreateDayOfMonth is not null and isnumeric(rh.SchedReportCreateDayOfMonth) = 1 then
				Case when Cast(Cast(DATEPART(m, @sysdate) as varchar(2)) + '/' + Cast(rh.SchedReportCreateDayOfMonth as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime) >= @sysdate then
					Cast(Cast(DATEPART(m, @sysdate) as varchar(2)) + '/' + Cast(rh.SchedReportCreateDayOfMonth as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime)
					else DATEADD(m,1,Cast(Cast(DATEPART(m, @sysdate) as varchar(2)) + '/' + Cast(rh.SchedReportCreateDayOfMonth as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime)) end
			When rh.SchedReportCreateDayOfMonth is not null and rh.SchedReportCreateDayOfMonth = 'Last' then Cast(Convert(varchar(10), Dateadd(m,1,@sysdate) - DAY(Dateadd(m,1,@sysdate)), 1) as datetime)
			When rh.SchedReportCreateDay is not null and (rh.SchedReportCreateDay = 0 or rh.SchedReportCreateDay = DATEPART(dw, @sysdate)) then Cast(Convert(varchar(10), @sysdate, 1) as datetime)
			When rh.SchedReportCreateDay is not null then Cast(Convert(varchar(10), @sysdate + rh.SchedReportCreateDay - DATEPART(dw, @sysdate) + Case when rh.SchedReportCreateDay - DATEPART(dw, @sysdate) < 0 then 7 else 0 end, 1) as datetime)
		end 
		= Cast(Getdate() + @PlusDays as date)
		
	Create Index idx_rh_id on Reports.Reports_Scheduled(Report_FK)
	
End
