﻿--A_Maui_Report_Scheduling_Update

-- =============================================
-- Author:		Gary Stone
-- Create date: July 7, 2014
-- =============================================
CREATE PROCEDURE [Reports].[Report_Scheduling_Update] @PlusDays int = 0

AS
BEGIN
	SET NOCOUNT ON;

Declare @sysdate datetime
set @sysdate = Cast(Convert(varchar(10), getdate() + @PlusDays , 1) as datetime)

Update RH
Set ReportCalcDateNext = 
	Case
		When rh.SchedReportCreateAnnually is not null
		then
			Case When Cast(Cast(DATEPART(m, rh.SchedReportCreateAnnually) as varchar(2)) + '/' + Cast(DATEPART(d, rh.SchedReportCreateAnnually) as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime) >= @sysdate then
				Cast(Cast(DATEPART(m, rh.SchedReportCreateAnnually) as varchar(2)) + '/' + Cast(DATEPART(d, rh.SchedReportCreateAnnually) as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime)
				else  Cast(Cast(DATEPART(m, rh.SchedReportCreateAnnually) as varchar(2)) + '/' + Cast(DATEPART(d, rh.SchedReportCreateAnnually) as varchar(2)) + '/' + Cast(YEAR(@sysdate) + 1 as varchar(4)) AS datetime) end 
		When rh.SchedReportCreateDayOfMonth is not null and isnumeric(rh.SchedReportCreateDayOfMonth) = 1 then
			Case when Cast(Cast(DATEPART(m, @sysdate) as varchar(2)) + '/' + Cast(rh.SchedReportCreateDayOfMonth as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime) >= @sysdate then
				Cast(Cast(DATEPART(m, @sysdate) as varchar(2)) + '/' + Cast(rh.SchedReportCreateDayOfMonth as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime)
				else DATEADD(m,1,Cast(Cast(DATEPART(m, @sysdate) as varchar(2)) + '/' + Cast(rh.SchedReportCreateDayOfMonth as varchar(2)) + '/' + Cast(YEAR(@sysdate) as varchar(4)) AS datetime)) end
		When rh.SchedReportCreateDayOfMonth is not null and rh.SchedReportCreateDayOfMonth = 'Last' then Cast(Convert(varchar(10), Dateadd(m,1,@sysdate) - DAY(Dateadd(m,1,@sysdate)), 1) as datetime)
		When rh.SchedReportCreateDay is not null and (rh.SchedReportCreateDay = 0 or rh.SchedReportCreateDay = DATEPART(dw, @sysdate)) then Cast(Convert(varchar(10), @sysdate, 1) as datetime)
		When rh.SchedReportCreateDay is not null then Cast(Convert(varchar(10), @sysdate + rh.SchedReportCreateDay - DATEPART(dw, @sysdate) + Case when rh.SchedReportCreateDay - DATEPART(dw, @sysdate) < 0 then 7 else 0 end, 1) as datetime)
	end 
from reports.Reports_Header RH where rh.SchedReportCreateAnnually is not null or rh.SchedReportCreateDayOfMonth is not null or rh.SchedReportCreateDay is not null

--Update RH
--Set PricingEffectiveDateNext = 
--	Case
--		--When Pricing Date same as Report Date
--		When rh.SchedReportPricingEffectiveDay = -1 then rh.ReportCalcDateNext
--		--When Pricing Date the next month from Report Date
--		When rh.SchedReportPricingFrequency = 1 then 
--			Case rh.SchedReportPricingEffectiveDay when 0 then DATEADD(m, 1, rh.ReportCalcDateNext) - DAY(DATEADD(m, 1, rh.ReportCalcDateNext)) + 1 
--				else DATEADD(m, 1, rh.ReportCalcDateNext) - DAY(DATEADD(m, 1, rh.ReportCalcDateNext)) + 1 
--					+ rh.SchedReportPricingEffectiveDay - DATEPART(dw, DATEADD(m, 1, rh.ReportCalcDateNext) - DAY(DATEADD(m, 1, rh.ReportCalcDateNext)) + 1)
--					+ Case when rh.SchedReportPricingEffectiveDay - DATEPART(dw, DATEADD(m, 1, rh.ReportCalcDateNext) - DAY(DATEADD(m, 1, rh.ReportCalcDateNext)) + 1) < 0 then 7 else 0 end
--			End
--		--When Pricing Date the next week from Report Date
--		When rh.SchedReportPricingFrequency = 2 then 	
--			rh.ReportCalcDateNext - DATEPART(dw, rh.ReportCalcDateNext) + 7 + Case rh.SchedReportPricingEffectiveDay when 0 then 1 else rh.SchedReportPricingEffectiveDay end
--	end 
--from reports.Reports_Header RH where rh.ReportCalcDateNext is not null and rh.SchedReportPricingEffectiveDay is not null

END
