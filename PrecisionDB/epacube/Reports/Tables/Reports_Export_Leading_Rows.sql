﻿CREATE TABLE [Reports].[Reports_Export_Leading_Rows] (
    [Report_FK]   INT          NOT NULL,
    [Report_Name] VARCHAR (64) NULL,
    [RowNumber]   INT          NOT NULL,
    [DateParam]   INT          NULL,
    [Col1]        VARCHAR (64) NULL,
    [Col2]        VARCHAR (64) NULL,
    [Col3]        VARCHAR (64) NULL,
    [Col4]        VARCHAR (64) NULL,
    [Col5]        VARCHAR (64) NULL,
    [Col6]        VARCHAR (64) NULL,
    [Col7]        VARCHAR (64) NULL,
    [Col8]        VARCHAR (64) NULL,
    [Col9]        VARCHAR (64) NULL,
    [Col10]       VARCHAR (64) NULL,
    [Col11]       VARCHAR (64) NULL,
    [Col12]       VARCHAR (64) NULL,
    [Col13]       VARCHAR (64) NULL,
    [Col14]       VARCHAR (64) NULL,
    [Col15]       VARCHAR (64) NULL,
    [Col16]       VARCHAR (64) NULL,
    [Col17]       VARCHAR (64) NULL,
    [Col18]       VARCHAR (64) NULL,
    [Col19]       VARCHAR (64) NULL,
    [Col20]       VARCHAR (64) NULL,
    [Col21]       VARCHAR (64) NULL,
    [Col22]       VARCHAR (64) NULL,
    [Col23]       VARCHAR (64) NULL,
    [Col24]       VARCHAR (64) NULL,
    [Col25]       VARCHAR (64) NULL,
    [Col26]       VARCHAR (64) NULL,
    [Col27]       VARCHAR (64) NULL,
    [Col28]       VARCHAR (64) NULL,
    [Col29]       VARCHAR (64) NULL,
    [Col30]       VARCHAR (64) NULL,
    [Col31]       VARCHAR (64) NULL,
    [Col32]       VARCHAR (64) NULL,
    [Col33]       VARCHAR (64) NULL,
    [Col34]       VARCHAR (64) NULL,
    [Col35]       VARCHAR (64) NULL,
    [Col36]       VARCHAR (64) NULL,
    [Col37]       VARCHAR (64) NULL,
    [Col38]       VARCHAR (64) NULL,
    [Col39]       VARCHAR (64) NULL,
    [Col40]       VARCHAR (64) NULL,
    [Col41]       VARCHAR (64) NULL,
    [Col42]       VARCHAR (64) NULL,
    [Col43]       VARCHAR (64) NULL,
    [Col44]       VARCHAR (64) NULL,
    [Col45]       VARCHAR (64) NULL,
    [Col46]       VARCHAR (64) NULL,
    [Col47]       VARCHAR (64) NULL,
    [Col48]       VARCHAR (64) NULL,
    [Col49]       VARCHAR (64) NULL,
    [Col50]       VARCHAR (64) NULL
);

