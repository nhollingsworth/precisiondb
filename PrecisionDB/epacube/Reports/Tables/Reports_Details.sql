﻿CREATE TABLE [Reports].[Reports_Details] (
    [Report_FK]           INT             NOT NULL,
    [Table_Name]          VARCHAR (64)    NOT NULL,
    [Column_Name]         VARCHAR (64)    NOT NULL,
    [Data_Type]           VARCHAR (64)    NULL,
    [SEQ]                 INT             NULL,
    [Job_FK]              NUMERIC (10, 2) NULL,
    [Column_Label]        VARCHAR (64)    NULL,
    [Reports_Details_ID]  INT             NOT NULL,
    [Column_Name_Alias]   VARCHAR (64)    NULL,
    [Export_Fixed_Length] INT             NULL,
    [Derivation]          VARCHAR (MAX)   NULL,
    [PivotAction]         VARCHAR (32)    NULL
);

