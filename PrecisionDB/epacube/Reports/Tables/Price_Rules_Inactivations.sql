﻿CREATE TABLE [Reports].[Price_Rules_Inactivations] (
    [Rule ID]                     VARCHAR (128)   NULL,
    [Job_FK_i]                    NUMERIC (18, 2) NULL,
    [Price_Rule_Inactivations_ID] BIGINT          NOT NULL,
    [Insertion_Pass]              INT             NULL,
    [Create_Timestamp]            DATETIME        NULL,
    [Exported]                    INT             NULL
);

