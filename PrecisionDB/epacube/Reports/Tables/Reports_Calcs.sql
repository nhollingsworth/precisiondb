﻿CREATE TABLE [Reports].[Reports_Calcs] (
    [Report_FK]         INT             NOT NULL,
    [Table_Name]        VARCHAR (64)    NOT NULL,
    [Column_Name]       VARCHAR (64)    NOT NULL,
    [Data_Type]         VARCHAR (64)    NULL,
    [Operation]         VARCHAR (64)    NOT NULL,
    [Job_FK]            NUMERIC (10, 2) NULL,
    [Column_Label]      VARCHAR (64)    NOT NULL,
    [Calc_Column_Label] VARCHAR (64)    NOT NULL,
    [Reports_Calcs_ID]  BIGINT          NOT NULL
);

