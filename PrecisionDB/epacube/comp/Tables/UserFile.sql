﻿CREATE TABLE [comp].[UserFile] (
    [Competitor Code]    VARCHAR (5) NULL,
    [URM Item Surrogate] VARCHAR (7) NULL,
    [Price Type Code]    VARCHAR (1) NULL,
    [Price Multiple]     VARCHAR (3) NULL,
    [Price Amount]       VARCHAR (7) NULL,
    [CRLF]               VARCHAR (2) NULL
);

