﻿CREATE TABLE [dbo].[MAUI_Configuration] (
    [Value]               VARCHAR (256) NULL,
    [Description]         VARCHAR (128) NULL,
    [Record_Status_CR_FK] INT           NULL,
    [Class]               VARCHAR (50)  NULL,
    [Value_Seq]           INT           NULL,
    [Configuration_ID]    INT           NULL,
    [Boolean_Status]      INT           NULL,
    [Value_Group]         VARCHAR (256) NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_config]
    ON [dbo].[MAUI_Configuration]([Record_Status_CR_FK] ASC, [Configuration_ID] ASC, [Class] ASC, [Value] ASC, [Value_Seq] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

