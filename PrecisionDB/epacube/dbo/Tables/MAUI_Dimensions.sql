﻿CREATE TABLE [dbo].[MAUI_Dimensions] (
    [Job_FK]               BIGINT        NOT NULL,
    [DimensionLevel]       INT           NOT NULL,
    [DimensionName]        VARCHAR (64)  NOT NULL,
    [DimensionValue]       VARCHAR (64)  NULL,
    [DimensionDescription] VARCHAR (256) NULL,
    [DimensionCriteria]    VARCHAR (64)  NOT NULL,
    [ComponentCust]        VARCHAR (64)  NULL,
    [ComponentProd]        VARCHAR (64)  NULL,
    [DimensionAffiliation] VARCHAR (64)  NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [idx_dims]
    ON [dbo].[MAUI_Dimensions]([Job_FK] ASC, [DimensionLevel] ASC, [DimensionName] ASC, [DimensionValue] ASC, [DimensionCriteria] ASC, [ComponentCust] ASC, [ComponentProd] ASC, [DimensionDescription] ASC);

