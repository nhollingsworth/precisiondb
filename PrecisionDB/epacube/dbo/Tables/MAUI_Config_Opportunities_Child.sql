﻿CREATE TABLE [dbo].[MAUI_Config_Opportunities_Child] (
    [Opportunity_Dimension] VARCHAR (64) NOT NULL,
    [Opportunity_Type]      INT          NOT NULL,
    [Data_Type]             INT          NOT NULL,
    [Opportunity_Sequence]  INT          NULL,
    [Grouping]              INT          NOT NULL,
    [Filter_Contract]       INT          NOT NULL,
    [Opportunity_Parent_FK] INT          NOT NULL,
    [Opportunity_Child_ID]  INT          IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_MAUI_Config_Opportunities_Child] PRIMARY KEY CLUSTERED ([Opportunity_Dimension] ASC, [Opportunity_Type] ASC, [Data_Type] ASC, [Grouping] ASC, [Filter_Contract] ASC, [Opportunity_Parent_FK] ASC)
);

