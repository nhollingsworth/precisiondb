﻿CREATE TABLE [dbo].[MAUI_Rules_Data_Elements] (
    [Data_Elements_ID]           INT          NULL,
    [Data_Name_FK]               INT          NULL,
    [Name]                       VARCHAR (64) NULL,
    [ERP_Value_1]                VARCHAR (64) NULL,
    [ERP_Value_2]                VARCHAR (96) NULL,
    [Class]                      VARCHAR (64) NULL,
    [Data_Criteria]              VARCHAR (64) NULL,
    [Rule_Type]                  VARCHAR (64) NULL,
    [Parent_Value]               VARCHAR (64) NULL,
    [Parent_Name]                VARCHAR (64) NULL,
    [Data_Element_Source_Table]  VARCHAR (64) NULL,
    [Reference_Name]             VARCHAR (64) NULL,
    [Reference_FK]               INT          NULL,
    [Data_Level]                 INT          NULL,
    [Structure_Price]            NUMERIC (18) NULL,
    [Structure_Rebate]           NUMERIC (18) NULL,
    [Schedule_Price]             NUMERIC (18) NULL,
    [Schedule_Rebate]            NUMERIC (18) NULL,
    [Result_Data_Name_FK_Price]  NUMERIC (18) NULL,
    [Result_Data_Name_FK_Rebate] NUMERIC (18) NULL,
    [Matrix_Text]                VARCHAR (64) NULL,
    [Matrix_Text_Customer]       VARCHAR (64) NULL,
    [Matrix_Text_Product]        VARCHAR (64) NULL,
    [Whse]                       VARCHAR (64) NULL,
    [UPDATE_TIMESTAMP]           DATETIME     NULL,
    [UPDATE_USER]                VARCHAR (64) NULL,
    [Seq]                        INT          NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_ids]
    ON [dbo].[MAUI_Rules_Data_Elements]([Data_Elements_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_rule_crit]
    ON [dbo].[MAUI_Rules_Data_Elements]([Name] ASC, [Class] ASC, [Data_Level] ASC, [Rule_Type] ASC, [Data_Name_FK] ASC, [ERP_Value_1] ASC, [Reference_Name] ASC, [Reference_FK] ASC, [Parent_Value] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

