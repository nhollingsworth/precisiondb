﻿CREATE TABLE [dbo].[EntityActionTable] (
    [ID]                   BIGINT         NULL,
    [items]                VARCHAR (8000) NULL,
    [ENTITY_IDENT_MULT_ID] INT            NULL,
    [DataComparedAgainst]  VARCHAR (1000) NULL,
    [Note]                 VARCHAR (32)   NOT NULL,
    [ACTION]               VARCHAR (6)    NOT NULL,
    [processed]            INT            NOT NULL
);

