﻿CREATE TABLE [dbo].[MAUI_Help_Text] (
    [TabName]           VARCHAR (32)  NOT NULL,
    [ColumnName]        VARCHAR (96)  NOT NULL,
    [AssessmentName]    VARCHAR (32)  NOT NULL,
    [ColumnDescription] VARCHAR (32)  NULL,
    [Explanation]       VARCHAR (MAX) NULL,
    [UniqueField]       VARCHAR (160) NOT NULL,
    CONSTRAINT [PK_MAUI_Help_Text] PRIMARY KEY CLUSTERED ([UniqueField] ASC)
);

