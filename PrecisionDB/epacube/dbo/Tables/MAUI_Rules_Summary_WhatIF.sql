﻿CREATE TABLE [dbo].[MAUI_Rules_Summary_WhatIF] (
    [Job_FK_i]                           NUMERIC (10, 2) NOT NULL,
    [Sell_Price_Cust_Host_Rule_XRef]     VARCHAR (64)    NOT NULL,
    [Price_Operand_Pos]                  INT             NOT NULL,
    [WhatIF_ID]                          INT             NOT NULL,
    [Op_Criteria_New]                    NUMERIC (18, 6) NULL,
    [Sales_Qty_New]                      NUMERIC (18, 6) NULL,
    [Sales_Qty_Original]                 NUMERIC (18, 6) NULL,
    [Sell_Price_Cust_Effective_Date_New] DATETIME        NULL,
    [Sell_Price_Cust_End_Date_New]       DATETIME        NULL,
    [Rule_Sell_Price]                    NUMERIC (18, 6) NULL,
    [Rule Sell Price_New]                NUMERIC (18, 6) NULL,
    [Rule_Sell_Price_Change]             NUMERIC (18, 6) NULL,
    [MCB Margin Dollars_New]             NUMERIC (18, 6) NULL,
    [MCB Margin Percent_New]             NUMERIC (18, 6) NULL,
    [MCB Margin Percent_Cur]             NUMERIC (18, 6) NULL,
    [Margin_Pct_Change]                  NUMERIC (18, 6) NULL,
    [Margin_Dollars_Change]              NUMERIC (18, 6) NULL,
    [Rules_Summary_FK]                   BIGINT          NOT NULL,
    [Approved_To_ERP]                    INT             NULL,
    [Approved_By]                        VARCHAR (64)    NULL,
    [Approved_Timestamp]                 DATETIME        NULL,
    [What_If_Reference_ID]               BIGINT          NOT NULL,
    [Calc_Status]                        VARCHAR (16)    NOT NULL,
    CONSTRAINT [PK_MAUI_Rules_Summary_WhatIF] PRIMARY KEY CLUSTERED ([What_If_Reference_ID] ASC, [Calc_Status] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_summ_wif]
    ON [dbo].[MAUI_Rules_Summary_WhatIF]([Job_FK_i] ASC, [Sell_Price_Cust_Host_Rule_XRef] ASC, [Price_Operand_Pos] ASC, [WhatIF_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

