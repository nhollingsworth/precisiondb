﻿CREATE TABLE [dbo].[MAUI_Config_ERP] (
    [Usage]            VARCHAR (128) NULL,
    [Table]            VARCHAR (128) NULL,
    [Temp_Table]       VARCHAR (128) NULL,
    [SQL_CD]           VARCHAR (MAX) NULL,
    [epaCUBE_Column]   VARCHAR (128) NULL,
    [ERP_Alias]        VARCHAR (128) NULL,
    [data_name_fk]     BIGINT        NULL,
    [Data_Name]        VARCHAR (128) NULL,
    [ERP]              VARCHAR (32)  NULL,
    [Config_ERP_ID]    INT           IDENTITY (1, 1) NOT NULL,
    [epaCUBE_Customer] VARCHAR (64)  NULL,
    CONSTRAINT [PK_MAUI_Config_ERP] PRIMARY KEY CLUSTERED ([Config_ERP_ID] ASC)
);

