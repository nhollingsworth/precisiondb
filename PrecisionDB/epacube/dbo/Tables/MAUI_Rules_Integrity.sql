﻿CREATE TABLE [dbo].[MAUI_Rules_Integrity] (
    [Event_ID]                   BIGINT          NOT NULL,
    [Rules_FK_Used]              BIGINT          NULL,
    [Host_Rule_XRef_Used]        VARCHAR (64)    NOT NULL,
    [Result_Used]                NUMERIC (18, 6) NULL,
    [result_rank]                NUMERIC (18)    NULL,
    [Result_Qualifying]          NUMERIC (18, 6) NULL,
    [Rules_FK_Qualifying]        BIGINT          NULL,
    [Host_Rule_XRef_Qualifying]  VARCHAR (64)    NULL,
    [Rule_Precedence_Qualifying] SMALLINT        NULL,
    [operation1]                 VARCHAR (64)    NULL,
    [prod_filter_fk]             BIGINT          NULL,
    [org_filter_fk]              BIGINT          NULL,
    [cust_filter_fk]             BIGINT          NULL,
    [supl_filter_fk]             BIGINT          NULL,
    [Matrix_Used]                VARCHAR (64)    NULL,
    [Matrix_Qualifying]          VARCHAR (64)    NULL,
    [Promo]                      INT             NULL,
    [Update_User]                VARCHAR (64)    NULL,
    CONSTRAINT [PK_MAUI_Rules_Integrity] PRIMARY KEY CLUSTERED ([Event_ID] ASC, [Host_Rule_XRef_Used] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_rv]
    ON [dbo].[MAUI_Rules_Integrity]([Rules_FK_Used] ASC, [Rules_FK_Qualifying] ASC, [prod_filter_fk] ASC, [org_filter_fk] ASC, [cust_filter_fk] ASC, [supl_filter_fk] ASC, [Update_User] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

