﻿CREATE TABLE [dbo].[MAUI_Customer_Price_List_Sort] (
    [SortLevel]                       INT          NULL,
    [SortGroup]                       VARCHAR (64) NULL,
    [DescriptionGroup]                VARCHAR (64) NULL,
    [SortGroupType]                   VARCHAR (64) NULL,
    [OrderKey]                        INT          NULL,
    [OrderGroup]                      INT          NULL,
    [Customer_Price_List_Profiles_FK] INT          NULL,
    [Customer_Price_List_Sort_ID]     INT          IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_MAUI_Customer_Price_List_Sort] PRIMARY KEY CLUSTERED ([Customer_Price_List_Sort_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_sort]
    ON [dbo].[MAUI_Customer_Price_List_Sort]([SortLevel] ASC, [SortGroup] ASC, [OrderKey] ASC, [OrderGroup] ASC, [Customer_Price_List_Profiles_FK] ASC, [Customer_Price_List_Sort_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

