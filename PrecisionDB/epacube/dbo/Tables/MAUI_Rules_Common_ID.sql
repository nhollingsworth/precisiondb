﻿CREATE TABLE [dbo].[MAUI_Rules_Common_ID] (
    [RuleType]         VARCHAR (64) NULL,
    [Update_User]      VARCHAR (64) NULL,
    [ID_Price]         BIGINT       NOT NULL,
    [ID_Rebate]        BIGINT       NULL,
    [ID_Common]        BIGINT       IDENTITY (1000, 1) NOT NULL,
    [Create_Timestamp] DATETIME     CONSTRAINT [DF_MAUI_Rules_Common_ID_Create_Timestamp] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_MAUI_Rules_Common_ID] PRIMARY KEY CLUSTERED ([ID_Price] ASC)
);

