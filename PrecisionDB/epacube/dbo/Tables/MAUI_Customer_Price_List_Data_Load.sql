﻿CREATE TABLE [dbo].[MAUI_Customer_Price_List_Data_Load] (
    [CPL_Data_Load_ID]         INT           IDENTITY (1, 1) NOT NULL,
    [Load_Data_Type]           VARCHAR (64)  NULL,
    [Load_Data_Name_fk]        INT           NULL,
    [Load_Entity_Data_Name_FK] INT           NULL,
    [Load_Data_Name]           VARCHAR (64)  NULL,
    [Load_Host_Value]          VARCHAR (128) NULL,
    [Load_Host_Whse]           VARCHAR (128) NULL,
    [Load_Valid]               INT           NULL,
    [Price_List_Template_FK]   INT           NULL,
    CONSTRAINT [PK_MAUI_Customer_Price_List_Data_Load] PRIMARY KEY CLUSTERED ([CPL_Data_Load_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_Price_List_Data]
    ON [dbo].[MAUI_Customer_Price_List_Data_Load]([Load_Data_Type] ASC, [Load_Data_Name_fk] ASC, [Load_Entity_Data_Name_FK] ASC, [Load_Host_Value] ASC, [Load_Host_Whse] ASC, [Price_List_Template_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

