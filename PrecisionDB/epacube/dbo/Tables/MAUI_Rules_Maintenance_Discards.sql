﻿CREATE TABLE [dbo].[MAUI_Rules_Maintenance_Discards] (
    [Export_Job_FK]                           BIGINT          NULL,
    [Job_FK_i]                                NUMERIC (18, 2) NULL,
    [WhatIF_ID]                               INT             NULL,
    [RuleType]                                VARCHAR (64)    NULL,
    [LevelCode]                               VARCHAR (8)     NULL,
    [Result_Data_Name_FK]                     BIGINT          NULL,
    [Rule_Result_Type_CR_FK]                  INT             NULL,
    [CustCriteria_DN_FK]                      BIGINT          NULL,
    [CustValue]                               VARCHAR (64)    NULL,
    [RULES_ACTION_OPERATION1_FK]              BIGINT          NULL,
    [RULES_ACTION_OPERATION2_FK]              BIGINT          NULL,
    [RULES_ACTION_OPERATION3_FK]              BIGINT          NULL,
    [BASIS_CALC_DN_FK]                        BIGINT          NULL,
    [BASIS1_DN_FK]                            BIGINT          NULL,
    [BASIS2_DN_FK]                            BIGINT          NULL,
    [BASIS3_DN_FK]                            BIGINT          NULL,
    [Rule_Name]                               VARCHAR (238)   NULL,
    [Host_Rule_Xref]                          VARCHAR (64)    NULL,
    [Promo]                                   INT             NULL,
    [Effective_Date]                          DATETIME        NULL,
    [End_Date]                                DATETIME        NULL,
    [Rules_Hierarchy_FK]                      BIGINT          NULL,
    [Rules_Structure_FK]                      BIGINT          NULL,
    [Rules_Schedule_FK]                       BIGINT          NULL,
    [Rules_Precedence_FK]                     BIGINT          NULL,
    [Rule_Precedence]                         VARCHAR (50)    NULL,
    [UOM_DN_FK]                               BIGINT          NULL,
    [UOM_CODE_FK]                             BIGINT          NULL,
    [Reference]                               VARCHAR (64)    NULL,
    [Contract_NO]                             VARCHAR (64)    NULL,
    [Unique_Key1]                             VARCHAR (128)   NULL,
    [ProdCriteria_DN_FK]                      BIGINT          NULL,
    [ProdValue]                               VARCHAR (64)    NULL,
    [chkWhseSpecific]                         INT             NULL,
    [Org_Entity_Structure_FK]                 BIGINT          NULL,
    [WhseValue]                               VARCHAR (64)    NULL,
    [Supl_Entity_Structure_FK]                BIGINT          NULL,
    [Vendor_ID]                               VARCHAR (64)    NULL,
    [ROUNDING_METHOD_CR_FK]                   BIGINT          NULL,
    [ROUNDING_TO_VALUE]                       NUMERIC (18, 6) NULL,
    [Rules_FK]                                BIGINT          NULL,
    [Rules_Action_FK]                         BIGINT          NULL,
    [Qty_Break_Rule_Ind]                      INT             NULL,
    [OPERAND_FILTER1_DN_FK]                   BIGINT          NULL,
    [OPERAND_FILTER1_OPERATOR_CR_FK]          INT             NULL,
    [Operand_1_1]                             NUMERIC (18, 6) NULL,
    [Operand_1_2]                             NUMERIC (18, 6) NULL,
    [Operand_1_3]                             NUMERIC (18, 6) NULL,
    [Operand_1_4]                             NUMERIC (18, 6) NULL,
    [Operand_1_5]                             NUMERIC (18, 6) NULL,
    [Operand_1_6]                             NUMERIC (18, 6) NULL,
    [Operand_1_7]                             NUMERIC (18, 6) NULL,
    [Operand_1_8]                             NUMERIC (18, 6) NULL,
    [Operand_1_9]                             NUMERIC (18, 6) NULL,
    [Filter_Value_1_1]                        INT             NULL,
    [Filter_Value_1_2]                        INT             NULL,
    [Filter_Value_1_3]                        INT             NULL,
    [Filter_Value_1_4]                        INT             NULL,
    [Filter_Value_1_5]                        INT             NULL,
    [Filter_Value_1_6]                        INT             NULL,
    [Filter_Value_1_7]                        INT             NULL,
    [Filter_Value_1_8]                        INT             NULL,
    [Filter_Value_1_9]                        INT             NULL,
    [OPERAND_FILTER2_DN_FK]                   BIGINT          NULL,
    [OPERAND_FILTER2_OPERATOR_CR_FK]          INT             NULL,
    [Operand_2_1]                             NUMERIC (18, 6) NULL,
    [Operand_2_2]                             NUMERIC (18, 6) NULL,
    [Operand_2_3]                             NUMERIC (18, 6) NULL,
    [Operand_2_4]                             NUMERIC (18, 6) NULL,
    [Operand_2_5]                             NUMERIC (18, 6) NULL,
    [Operand_2_6]                             NUMERIC (18, 6) NULL,
    [Operand_2_7]                             NUMERIC (18, 6) NULL,
    [Operand_2_8]                             NUMERIC (18, 6) NULL,
    [Operand_2_9]                             NUMERIC (18, 6) NULL,
    [Filter_Value_2_1]                        INT             NULL,
    [Filter_Value_2_2]                        INT             NULL,
    [Filter_Value_2_3]                        INT             NULL,
    [Filter_Value_2_4]                        INT             NULL,
    [Filter_Value_2_5]                        INT             NULL,
    [Filter_Value_2_6]                        INT             NULL,
    [Filter_Value_2_7]                        INT             NULL,
    [Filter_Value_2_8]                        INT             NULL,
    [Filter_Value_2_9]                        INT             NULL,
    [OPERAND_FILTER3_DN_FK]                   BIGINT          NULL,
    [OPERAND_FILTER3_OPERATOR_CR_FK]          INT             NULL,
    [Operand_3_1]                             NUMERIC (18, 6) NULL,
    [Operand_3_2]                             NUMERIC (18, 6) NULL,
    [Operand_3_3]                             NUMERIC (18, 6) NULL,
    [Operand_3_4]                             NUMERIC (18, 6) NULL,
    [Operand_3_5]                             NUMERIC (18, 6) NULL,
    [Operand_3_6]                             NUMERIC (18, 6) NULL,
    [Operand_3_7]                             NUMERIC (18, 6) NULL,
    [Operand_3_8]                             NUMERIC (18, 6) NULL,
    [Operand_3_9]                             NUMERIC (18, 6) NULL,
    [Filter_Value_3_1]                        INT             NULL,
    [Filter_Value_3_2]                        INT             NULL,
    [Filter_Value_3_3]                        INT             NULL,
    [Filter_Value_3_4]                        INT             NULL,
    [Filter_Value_3_5]                        INT             NULL,
    [Filter_Value_3_6]                        INT             NULL,
    [Filter_Value_3_7]                        INT             NULL,
    [Filter_Value_3_8]                        INT             NULL,
    [Filter_Value_3_9]                        INT             NULL,
    [Record_Status_CR_FK]                     INT             NULL,
    [Update_User]                             VARCHAR (64)    NULL,
    [Create_User]                             VARCHAR (64)    NULL,
    [Approved_By]                             VARCHAR (64)    NULL,
    [Approval_Timestamp]                      DATETIME        NULL,
    [Update_Timestamp]                        DATETIME        NULL,
    [Create_Timestamp]                        DATETIME        CONSTRAINT [DF_MAUI_Rules_Maintenance_Discards_Create_Timestamp] DEFAULT (getdate()) NULL,
    [Transferred_To_ERP]                      INT             NULL,
    [Transfer_To_ERP_Timestamp]               DATETIME        NULL,
    [Transferred_To_Rules]                    INT             NULL,
    [Inactivated_By]                          VARCHAR (64)    NULL,
    [GP_Initial]                              NUMERIC (18, 6) NULL,
    [GP_Final]                                NUMERIC (18, 6) NULL,
    [Sales_Final]                             NUMERIC (18, 6) NULL,
    [Source]                                  VARCHAR (32)    NULL,
    [ID_Common]                               BIGINT          NULL,
    [DropShipType]                            VARCHAR (8)     NULL,
    [DefaultPriceLevel]                       INT             NULL,
    [Comments]                                VARCHAR (1000)  NULL,
    [MAUI_Rules_Maintenance_Discards_ID]      BIGINT          IDENTITY (1, 1) NOT NULL,
    [IMPORT_PRICE_REBATE_MASS_MAINTENANCE_FK] INT             NULL,
    [IMPORT_MASS_MAINTENANCE_PROMO_FK]        INT             NULL,
    [IMPORT_MASS_MAINTENANCE_PRICE_REBATE_FK] INT             NULL,
    [Insert_Date]                             DATETIME        NULL,
    CONSTRAINT [PK_MAUI_Rules_Maintenance_Discards] PRIMARY KEY CLUSTERED ([MAUI_Rules_Maintenance_Discards_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_mrm]
    ON [dbo].[MAUI_Rules_Maintenance_Discards]([LevelCode] ASC, [Result_Data_Name_FK] ASC, [CustCriteria_DN_FK] ASC, [CustValue] ASC, [RULES_ACTION_OPERATION1_FK] ASC, [Promo] ASC, [Effective_Date] ASC, [Contract_NO] ASC, [ProdCriteria_DN_FK] ASC, [ProdValue] ASC, [WhseValue] ASC, [Vendor_ID] ASC, [Qty_Break_Rule_Ind] ASC, [Create_User] ASC, [Create_Timestamp] ASC, [Transferred_To_ERP] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_hrx]
    ON [dbo].[MAUI_Rules_Maintenance_Discards]([Host_Rule_Xref] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

