﻿CREATE TABLE [dbo].[MAUI_Opportunities] (
    [Focus]                       VARCHAR (64)    NOT NULL,
    [lblFocus]                    VARCHAR (64)    NOT NULL,
    [Impact_Negative]             NUMERIC (18, 4) NOT NULL,
    [Impact_Positive]             NUMERIC (18, 4) NULL,
    [Total_Sales]                 NUMERIC (18, 4) NULL,
    [Pct_Transactions_Overridden] NUMERIC (18, 4) NULL,
    [Opportunity_Type]            INT             NOT NULL,
    [Opportunity_Parent_FK]       INT             NOT NULL,
    [Sort_Seq]                    INT             NOT NULL,
    [Under_Contract]              INT             NOT NULL,
    [Job_FK_i]                    NUMERIC (18, 2) NOT NULL,
    CONSTRAINT [PK_MAUI_Opportunities] PRIMARY KEY CLUSTERED ([Focus] ASC, [lblFocus] ASC, [Opportunity_Type] ASC, [Opportunity_Parent_FK] ASC, [Sort_Seq] ASC, [Under_Contract] ASC, [Job_FK_i] ASC)
);

