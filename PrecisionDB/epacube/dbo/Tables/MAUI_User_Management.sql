﻿CREATE TABLE [dbo].[MAUI_User_Management] (
    [NameGroup] VARCHAR (50)  CONSTRAINT [DF_MAUI_User_Management_NameGroup] DEFAULT ('') NOT NULL,
    [NameUser]  VARCHAR (50)  NOT NULL,
    [NameFull]  VARCHAR (50)  NULL,
    [MgmtLvl]   VARCHAR (2)   NOT NULL,
    [eMail]     VARCHAR (128) NULL,
    [Permanent] INT           NULL,
    [Job_FK]    BIGINT        NULL,
    [What_IF]   INT           NULL,
    CONSTRAINT [PK_MAUI_User_Management] PRIMARY KEY CLUSTERED ([NameGroup] ASC, [NameUser] ASC, [MgmtLvl] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_UserMgmt]
    ON [dbo].[MAUI_User_Management]([NameGroup] ASC, [NameUser] ASC, [MgmtLvl] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

