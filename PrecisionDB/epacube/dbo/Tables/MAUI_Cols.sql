﻿CREATE TABLE [dbo].[MAUI_Cols] (
    [ColCMBName]                      VARCHAR (64)  NULL,
    [ColCommand]                      VARCHAR (64)  NOT NULL,
    [ColDisplayCode]                  VARCHAR (256) NULL,
    [ColGroupSec]                     VARCHAR (64)  NULL,
    [Fkcolumn]                        VARCHAR (64)  NULL,
    [Record_Status_CR_FK_Primary]     INT           NULL,
    [Record_Status_CR_FK_Matrix]      INT           NULL,
    [Boolean_Primary]                 INT           NULL,
    [Boolean_Matrix]                  INT           NULL,
    [Record_Status_CR_FK_Basis]       INT           NULL,
    [Record_Status_CR_FK_Basis_Calcs] INT           NULL,
    [ERP]                             VARCHAR (32)  NOT NULL,
    [FilterName]                      VARCHAR (32)  NULL,
    [QueryName]                       VARCHAR (64)  NULL,
    [ColumnNameMAUI]                  VARCHAR (64)  NULL,
    CONSTRAINT [PK_MAUI_Cols] PRIMARY KEY CLUSTERED ([ColCommand] ASC, [ERP] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_maui_basis_cols1]
    ON [dbo].[MAUI_Cols]([ERP] ASC, [ColCommand] ASC, [Record_Status_CR_FK_Basis_Calcs] ASC, [FilterName] ASC, [QueryName] ASC, [ColumnNameMAUI] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

