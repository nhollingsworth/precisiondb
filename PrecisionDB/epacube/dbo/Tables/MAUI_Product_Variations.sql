﻿CREATE TABLE [dbo].[MAUI_Product_Variations] (
    [Variation]            VARCHAR (10) NULL,
    [Product_Structure_FK] INT          NOT NULL,
    [Product_Groupings_FK] INT          NULL,
    [Update_User]          VARCHAR (64) NULL,
    [Update_Timestamp]     DATETIME     NULL,
    [Create_User]          VARCHAR (64) NULL,
    [Create_Timestamp]     DATETIME     NULL,
    CONSTRAINT [PK_MAUI_Product_Variations] PRIMARY KEY CLUSTERED ([Product_Structure_FK] ASC)
);

