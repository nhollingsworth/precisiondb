﻿CREATE TABLE [dbo].[MAUI_Price_Change_Enforcement] (
    [Price_Change_Enforcement_ID] NUMERIC (18)  IDENTITY (1, 1) NOT NULL,
    [Org_Vendor_ID]               NUMERIC (18)  NULL,
    [GroupingCustom]              NUMERIC (18)  NULL,
    [CustomerID_BT]               NUMERIC (18)  NULL,
    [CustomerID_ST]               NUMERIC (18)  NULL,
    [Pricing_Adjustment_Interval] VARCHAR (2)   NULL,
    [Pricing_Adjustment_Date]     DATETIME      NULL,
    [Months]                      NUMERIC (2)   NULL,
    [Explanation]                 VARCHAR (MAX) NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_pce_ID]
    ON [dbo].[MAUI_Price_Change_Enforcement]([Price_Change_Enforcement_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_pce]
    ON [dbo].[MAUI_Price_Change_Enforcement]([Org_Vendor_ID] ASC, [GroupingCustom] ASC, [CustomerID_BT] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

