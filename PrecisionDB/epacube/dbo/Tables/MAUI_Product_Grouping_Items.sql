﻿CREATE TABLE [dbo].[MAUI_Product_Grouping_Items] (
    [Product_ID]           VARCHAR (64) NULL,
    [Product_Structure_FK] BIGINT       NULL,
    [Create_User]          VARCHAR (64) NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_LIP_items]
    ON [dbo].[MAUI_Product_Grouping_Items]([Product_Structure_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

