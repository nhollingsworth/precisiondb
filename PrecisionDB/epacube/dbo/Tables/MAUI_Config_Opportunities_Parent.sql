﻿CREATE TABLE [dbo].[MAUI_Config_Opportunities_Parent] (
    [Opportunity_Name]         VARCHAR (64)  NULL,
    [Opportunity_User_Name]    VARCHAR (64)  NULL,
    [Opportunity_User_Default] INT           CONSTRAINT [DF_MAUI_Config_Opportunities_Parent_Opportunity_User_Default] DEFAULT ((0)) NULL,
    [Opportunity_Filter]       VARCHAR (MAX) NULL,
    [Opportunity_Filter_Name]  VARCHAR (64)  NULL,
    [Opportunity_Parent_ID]    INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_MAUI_Config_Opportunities_Parent] PRIMARY KEY CLUSTERED ([Opportunity_Parent_ID] ASC)
);

