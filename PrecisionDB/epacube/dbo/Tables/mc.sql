﻿CREATE TABLE [dbo].[mc] (
    [product_structure_fk]    BIGINT       NOT NULL,
    [entity_structure_fk]     BIGINT       NOT NULL,
    [stage_for_processing_fk] BIGINT       NOT NULL,
    [new_tm]                  VARCHAR (64) NOT NULL,
    [MC_ID]                   BIGINT       IDENTITY (1, 1) NOT NULL
);

