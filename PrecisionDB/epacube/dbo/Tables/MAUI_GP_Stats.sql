﻿CREATE TABLE [dbo].[MAUI_GP_Stats] (
    [job_fk_i]                      NUMERIC (10, 2) NULL,
    [Class]                         VARCHAR (128)   NULL,
    [Status]                        VARCHAR (128)   NULL,
    [Value_Host]                    VARCHAR (128)   NULL,
    [Value_MM]                      VARCHAR (128)   NULL,
    [Sales_qty]                     NUMERIC (18, 6) NULL,
    [Override Qty]                  NUMERIC (18, 6) NULL,
    [Total Transactions]            NUMERIC (18, 6) NULL,
    [Override Transactions]         NUMERIC (18, 6) NULL,
    [Transactions At Default Price] NUMERIC (18, 6) NULL,
    [Override_Pct_Qty]              NUMERIC (18, 6) NULL,
    [Override_Pct_Trans]            NUMERIC (18, 6) NULL,
    [Projected Override Impact]     NUMERIC (18, 6) NULL,
    [Sales_Dollars_Cur]             NUMERIC (18, 6) NULL,
    [Sales_Dollars_Cur_Net]         NUMERIC (18, 6) NULL,
    [Sales_Dollars_Hist]            NUMERIC (18, 6) NULL,
    [Margin_Pct_Cur]                NUMERIC (18, 6) NULL,
    [Margin_Pct_Hist]               NUMERIC (18, 6) NULL,
    [Margin Net of O/R]             NUMERIC (18, 6) NULL,
    [GP_Stats_ID]                   BIGINT          IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_MAUI_GP_Stats] PRIMARY KEY CLUSTERED ([GP_Stats_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_GP_Stats]
    ON [dbo].[MAUI_GP_Stats]([job_fk_i] ASC, [Class] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

