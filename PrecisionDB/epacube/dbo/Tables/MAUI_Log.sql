﻿CREATE TABLE [dbo].[MAUI_Log] (
    [Job_fk_i]       NUMERIC (18, 2) NULL,
    [Activity]       VARCHAR (MAX)   NULL,
    [Start_Time]     DATETIME        NULL,
    [MAUI_Log_ID]    BIGINT          IDENTITY (1, 1) NOT NULL,
    [Duration]       VARCHAR (20)    NULL,
    [Record_Inserts] BIGINT          NULL,
    CONSTRAINT [PK_MAUI_Log] PRIMARY KEY CLUSTERED ([MAUI_Log_ID] ASC)
);

