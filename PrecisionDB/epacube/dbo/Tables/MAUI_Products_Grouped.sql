﻿CREATE TABLE [dbo].[MAUI_Products_Grouped] (
    [Product_ID]           VARCHAR (64)    NULL,
    [Product_Structure_FK] BIGINT          NULL,
    [Product_Description]  VARCHAR (64)    NULL,
    [Cost]                 NUMERIC (18, 4) NULL,
    [Price]                NUMERIC (18, 4) NULL,
    [Variation]            VARCHAR (50)    NULL,
    [Product_Groupings_FK] BIGINT          NULL,
    [Update_User]          VARCHAR (64)    NULL,
    [Update_Timestamp]     DATETIME        NULL,
    [Products_Grouped_ID]  BIGINT          IDENTITY (1000, 1) NOT NULL,
    CONSTRAINT [PK_MAUI_Products_Grouped] PRIMARY KEY CLUSTERED ([Products_Grouped_ID] ASC)
);

