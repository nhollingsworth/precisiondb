﻿CREATE TABLE [dbo].[MAUI_Product_Groupings] (
    [Description]          VARCHAR (64) NOT NULL,
    [Product_Groupings_ID] BIGINT       IDENTITY (1000, 1) NOT NULL,
    CONSTRAINT [PK_MAUI_Product_Groupings] PRIMARY KEY CLUSTERED ([Description] ASC)
);

