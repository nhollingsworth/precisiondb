﻿CREATE TABLE [dbo].[MAUI_Customer_Price_List_Results] (
    [Bill-To Customer ID]             VARCHAR (64)    NOT NULL,
    [Bill-To Customer Name]           VARCHAR (128)   NOT NULL,
    [Ship-To Customer ID]             VARCHAR (64)    NULL,
    [Ship-To Customer Name]           VARCHAR (128)   NULL,
    [Product_ID]                      VARCHAR (64)    NULL,
    [Product_Description]             VARCHAR (128)   NULL,
    [Sell Price]                      MONEY           NULL,
    [Pricing Cost]                    MONEY           NULL,
    [Rebate]                          MONEY           NULL,
    [Rebate_Cap_Sell]                 MONEY           NULL,
    [Whse_ID]                         VARCHAR (64)    NULL,
    [Vendor_ID]                       VARCHAR (64)    NULL,
    [Rebate Record #]                 VARCHAR (16)    NULL,
    [Pricing Record #]                VARCHAR (16)    NULL,
    [Prices_Calculated_Date]          DATETIME        NULL,
    [Product_Structure_FK]            BIGINT          NULL,
    [Org_Entity_Structure_FK]         BIGINT          NULL,
    [Cust_Entity_Structure_FK]        BIGINT          NULL,
    [Supl_Entity_Structure_FK]        BIGINT          NULL,
    [Customer_Price_List_Profiles_FK] BIGINT          NULL,
    [WAREHOUSE_RECORD]                VARCHAR (64)    NULL,
    [SALESREP_RECORD_IN]              VARCHAR (64)    NULL,
    [SALESREP_RECORD_OUT]             VARCHAR (64)    NULL,
    [Cust_Group1]                     VARCHAR (64)    NULL,
    [Cust_Group2]                     VARCHAR (64)    NULL,
    [Cust_Group3]                     VARCHAR (64)    NULL,
    [Prod_Group1]                     VARCHAR (64)    NULL,
    [Prod_Group2]                     VARCHAR (64)    NULL,
    [Prod_Group3]                     VARCHAR (64)    NULL,
    [Prod_Group4]                     VARCHAR (64)    NULL,
    [Prod_Group5]                     VARCHAR (64)    NULL,
    [Assessment_FK]                   BIGINT          NULL,
    [Job_FK]                          BIGINT          NULL,
    [SPC_UOM]                         NUMERIC (18, 6) NULL,
    [Customer_Price_List_Results_ID]  INT             IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_MAUI_Customer_Price_List_Results] PRIMARY KEY CLUSTERED ([Customer_Price_List_Results_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_IDs]
    ON [dbo].[MAUI_Customer_Price_List_Results]([Customer_Price_List_Profiles_FK] ASC, [Bill-To Customer ID] ASC, [Ship-To Customer ID] ASC, [Product_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

