﻿CREATE TABLE [dbo].[MAUI_Price_Change_Enforcement_Precedence] (
    [Precedence]     INT NULL,
    [Org_Vendor_ID]  INT NULL,
    [GroupingCustom] INT NULL,
    [CustomerID_BT]  INT NULL,
    [CustomerID_ST]  INT NULL,
    [Iteration]      INT NOT NULL,
    [Sub_Iteration]  INT NOT NULL,
    CONSTRAINT [PK_MAUI_Price_Change_Enforcement_Precedence] PRIMARY KEY CLUSTERED ([Iteration] ASC, [Sub_Iteration] ASC)
);

