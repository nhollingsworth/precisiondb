﻿CREATE TABLE [dbo].[MAUI_Basis_Overview] (
    [Focus]                            VARCHAR (128)   NOT NULL,
    [lblFocus]                         VARCHAR (128)   NULL,
    [Projected Override Impact]        NUMERIC (18, 2) NULL,
    [Sales_Qty]                        NUMERIC (10, 1) NULL,
    [Sales_History_Date_Range]         VARCHAR (17)    NULL,
    [Sales_Dollars_Hist]               NUMERIC (18, 2) NULL,
    [Prod_Cost_Dollars_Hist]           NUMERIC (18, 2) NULL,
    [Margin_Dollars_Hist]              NUMERIC (18, 2) NULL,
    [Margin_Pct_Hist]                  NUMERIC (18, 4) NULL,
    [Sales_Dollars_Cur]                NUMERIC (18, 2) NULL,
    [Cost_Dollars_Cur]                 NUMERIC (18, 2) NULL,
    [Margin_Dollars_Cur]               NUMERIC (18, 2) NULL,
    [Margin_Pct_Cur]                   NUMERIC (18, 4) NULL,
    [Sales_Dollars_New]                NUMERIC (18, 2) NULL,
    [Cost_Dollars_New]                 NUMERIC (18, 2) NULL,
    [Margin_Dollars_New]               NUMERIC (18, 2) NULL,
    [Margin_Pct_New]                   NUMERIC (18, 4) NULL,
    [Margin_Dollars_Change]            NUMERIC (18, 2) NULL,
    [Margin_Pct_Change]                NUMERIC (18, 4) NULL,
    [Price_Pct_Change_1]               NUMERIC (18, 4) NULL,
    [Sales_Delta_Cur_to_Hist]          NUMERIC (18, 2) NULL,
    [Prod_Cost_Delta_Cur_To_Hist]      NUMERIC (18, 2) NULL,
    [Margin_Dollars_Delta_Cur_To_Hist] NUMERIC (18, 2) NULL,
    [Margin_Pct_Delta_Cur_To_Hist]     NUMERIC (18, 4) NULL,
    [Price_Pct_Change_Cur_to_Hist]     NUMERIC (18, 4) NULL,
    [Cost_Pct_Change]                  NUMERIC (18, 4) NULL,
    [Cost_Dollars_Change_New_To_Cur]   NUMERIC (18, 2) NULL,
    [Cost_Pct_Change_New_To_Cur]       NUMERIC (18, 4) NULL,
    [Cost_Pct_Change_Cur_To_Hist]      NUMERIC (18, 4) NULL,
    [Cost_Dollars_Change_Cur_To_Hist]  NUMERIC (18, 2) NULL,
    [Cost_Impact_Dollars]              NUMERIC (18, 4) NULL,
    [Price_Recovery_Dollars]           NUMERIC (18, 4) NULL,
    [Total_Impact_Dollars]             NUMERIC (18, 4) NULL,
    [Rebate_Indicator]                 VARCHAR (8)     NULL,
    [Override Qty]                     NUMERIC (18, 2) NULL,
    [Override Transactions]            NUMERIC (18, 2) NULL,
    [Total Transactions]               NUMERIC (18, 2) NULL,
    [Override_Pct_Qty]                 NUMERIC (18, 2) NULL,
    [Override_Pct_Trans]               NUMERIC (18, 2) NULL,
    [Job_FK_i]                         NUMERIC (10, 2) NOT NULL,
    [Rbts]                             INT             NOT NULL,
    [WhatIF_ID]                        INT             NOT NULL,
    [Class]                            VARCHAR (128)   NOT NULL,
    CONSTRAINT [PK_MAUI_Basis_Overview] PRIMARY KEY CLUSTERED ([Focus] ASC, [Job_FK_i] ASC, [Rbts] ASC, [WhatIF_ID] ASC, [Class] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_ov]
    ON [dbo].[MAUI_Basis_Overview]([lblFocus] ASC, [Job_FK_i] ASC, [Rbts] ASC, [WhatIF_ID] ASC, [Class] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

