﻿CREATE TABLE [dbo].[MAUI_Rules_Qualified] (
    [Job_FK]                          BIGINT           NOT NULL,
    [Job_FK_i]                        NUMERIC (18, 6)  NOT NULL,
    [Identifier]                      VARCHAR (131)    NOT NULL,
    [Rule_Rank]                       INT              NOT NULL,
    [Type of Rule]                    VARCHAR (14)     NOT NULL,
    [Rule ID]                         VARCHAR (128)    NOT NULL,
    [Rule Effective Date]             DATETIME         NULL,
    [Hierarchy Level]                 VARCHAR (70)     NULL,
    [Result]                          NUMERIC (18, 4)  NULL,
    [Calculation]                     VARCHAR (135)    NULL,
    [Sell Price]                      NUMERIC (18, 6)  NULL,
    [Pricing Cost]                    NUMERIC (18, 6)  NULL,
    [Margin Amt]                      NUMERIC (18, 6)  NULL,
    [Margin Pct]                      NUMERIC (18, 6)  NULL,
    [Rebate Amount]                   NUMERIC (18, 6)  NULL,
    [Rebated Cost]                    NUMERIC (18, 6)  NULL,
    [Rebated Margin Amt]              NUMERIC (18, 6)  NULL,
    [Rebated Margin Pct]              NUMERIC (18, 6)  NULL,
    [Prod_Filter]                     VARCHAR (128)    NULL,
    [Cust_Filter]                     VARCHAR (128)    NULL,
    [Org_Filter]                      VARCHAR (128)    NULL,
    [Supl_Filter]                     VARCHAR (128)    NULL,
    [RESULT_MTX_ACTION_CR_FK]         INT              NULL,
    [STRUC_MTX_ACTION_CR_FK]          INT              NULL,
    [SCHED_MTX_ACTION_CR_FK]          INT              NULL,
    [RULES_STRUCTURE_FK]              BIGINT           NULL,
    [RULES_SCHEDULE_FK]               BIGINT           NULL,
    [SCHED_PRECEDENCE]                SMALLINT         NULL,
    [STRUC_PRECEDENCE]                SMALLINT         NULL,
    [RULE_PRECEDENCE]                 SMALLINT         NULL,
    [PRODUCT_STRUCTURE_FK]            BIGINT           NULL,
    [ORG_ENTITY_STRUCTURE_FK]         BIGINT           NULL,
    [CUST_ENTITY_STRUCTURE_FK]        BIGINT           NULL,
    [SUPL_ENTITY_STRUCTURE_FK]        BIGINT           NULL,
    [RESULT_DATA_NAME_FK]             INT              NULL,
    [RESULT_TYPE_CR_FK]               INT              NULL,
    [EVENT_ID]                        BIGINT           NOT NULL,
    [EVENT_RULES_ID]                  BIGINT           NOT NULL,
    [DISQUALIFIED_IND]                INT              NULL,
    [MAUI_Rules_Qualified_ID]         BIGINT           IDENTITY (1, 1) NOT NULL,
    [DISQUALIFIED_COMMENT]            VARCHAR (64)     NULL,
    [Basis_Calc]                      NUMERIC (18, 10) NULL,
    [Basis1]                          NUMERIC (18, 10) NULL,
    [Basis2]                          NUMERIC (18, 10) NULL,
    [Basis3]                          NUMERIC (18, 10) NULL,
    [Number1]                         NUMERIC (18, 10) NULL,
    [Number2]                         NUMERIC (18, 10) NULL,
    [Number3]                         NUMERIC (18, 10) NULL,
    [Operation1]                      VARCHAR (32)     NULL,
    [Contract_Precedence]             SMALLINT         NULL,
    [prod_filter_hierarchy]           SMALLINT         NULL,
    [prod_filter_org_precedence]      SMALLINT         NULL,
    [RULES_CONTRACT_CUSTOMER_FK]      BIGINT           NULL,
    [CALC_GROUP_SEQ]                  INT              NULL,
    [RULES_ACTION_OPERATION1_FK]      BIGINT           NULL,
    [RULES_OPTIMIZATION_INCREMENT_FK] BIGINT           NULL,
    [BASIS_SOURCE1]                   VARCHAR (32)     NULL,
    [BASIS_SOURCE2]                   VARCHAR (32)     NULL,
    [BASIS_SOURCE3]                   VARCHAR (32)     NULL,
    [RULES_ACTION_FK]                 BIGINT           NULL,
    [Rules_FK]                        BIGINT           NULL,
    CONSTRAINT [PK_MAUI_Rules_Qualified] PRIMARY KEY CLUSTERED ([Job_FK] ASC, [Job_FK_i] ASC, [Identifier] ASC, [Type of Rule] ASC, [EVENT_RULES_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_rfk]
    ON [dbo].[MAUI_Rules_Qualified]([Job_FK] ASC, [Identifier] ASC, [Rules_FK] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];


GO
CREATE NONCLUSTERED INDEX [idx_rq]
    ON [dbo].[MAUI_Rules_Qualified]([Job_FK_i] ASC, [Identifier] ASC, [RULES_STRUCTURE_FK] ASC, [Rule ID] ASC, [DISQUALIFIED_IND] ASC, [MAUI_Rules_Qualified_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

