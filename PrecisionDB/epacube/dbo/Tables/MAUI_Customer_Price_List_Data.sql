﻿CREATE TABLE [dbo].[MAUI_Customer_Price_List_Data] (
    [Price_List_Template_FK]       INT          NULL,
    [Template_Data_Name_FK]        INT          NULL,
    [Template_Entity_Data_Name_FK] INT          NULL,
    [Host_Value]                   VARCHAR (64) NULL,
    [Price_List_Data_ID]           INT          IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_MAUI_Customer_Price_List_Data] PRIMARY KEY CLUSTERED ([Price_List_Data_ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_uniq_CPLD]
    ON [dbo].[MAUI_Customer_Price_List_Data]([Price_List_Template_FK] ASC, [Host_Value] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

