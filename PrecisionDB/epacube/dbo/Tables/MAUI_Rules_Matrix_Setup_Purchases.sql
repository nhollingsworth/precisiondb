﻿CREATE TABLE [dbo].[MAUI_Rules_Matrix_Setup_Purchases] (
    [Group]                    VARCHAR (48)    NULL,
    [Grouping]                 VARCHAR (643)   NULL,
    [Sales]                    VARCHAR (32)    NULL,
    [% GP]                     VARCHAR (31)    NULL,
    [Equalizer]                VARCHAR (33)    NULL,
    [NumPct]                   NUMERIC (38, 6) NULL,
    [NumSales]                 NUMERIC (38, 4) NULL,
    [NumCost]                  NUMERIC (38, 6) NULL,
    [Sales_History_Start_Date] DATETIME        NULL,
    [Sales_History_End_Date]   DATETIME        NULL,
    [Sales_Ext_Hist]           NUMERIC (38, 8) NULL,
    [GP_Hist]                  NUMERIC (38, 6) NULL,
    [Product_Cost_Ext_Hist]    NUMERIC (38, 8) NULL,
    [FileDateRange]            VARCHAR (50)    NULL,
    [Assessment_Start_Date]    DATETIME        NULL,
    [Assessment_End_Date]      DATETIME        NULL,
    [Sales_Q1]                 NUMERIC (38, 6) NULL,
    [Cost_Net_Q1]              NUMERIC (38, 6) NULL,
    [GP_Q1]                    NUMERIC (38, 6) NULL,
    [Date_Range_Q1]            VARCHAR (32)    NULL,
    [Sales_Q2]                 NUMERIC (38, 6) NULL,
    [Cost_Net_Q2]              NUMERIC (38, 6) NULL,
    [GP_Q2]                    NUMERIC (38, 6) NULL,
    [Date_Range_Q2]            VARCHAR (32)    NULL,
    [Sales_Q3]                 NUMERIC (38, 6) NULL,
    [Cost_Net_Q3]              NUMERIC (38, 6) NULL,
    [GP_Q3]                    NUMERIC (38, 6) NULL,
    [Date_Range_Q3]            VARCHAR (32)    NULL,
    [Sales_Q4]                 NUMERIC (38, 6) NULL,
    [Cost_Net_Q4]              NUMERIC (38, 6) NULL,
    [GP_Q4]                    NUMERIC (38, 6) NULL,
    [Date_Range_Q4]            VARCHAR (32)    NULL,
    [User_Name]                VARCHAR (64)    NULL
);


GO
CREATE CLUSTERED INDEX [ClusteredIndex-20130619-095845]
    ON [dbo].[MAUI_Rules_Matrix_Setup_Purchases]([Group] ASC, [NumPct] ASC, [NumSales] ASC, [User_Name] ASC);

