﻿CREATE TABLE [dbo].[MAUI_Variances] (
    [Job_FK_i]                 NUMERIC (10, 2) NOT NULL,
    [Product_ID_Host]          VARCHAR (64)    NULL,
    [Customer_ID_Host]         VARCHAR (64)    NULL,
    [Whse]                     VARCHAR (64)    NULL,
    [Transaction_Type]         INT             NULL,
    [SPC_UOM]                  NUMERIC (18, 6) NULL,
    [ST_Qty]                   NUMERIC (18, 4) NULL,
    [MM_Qty]                   NUMERIC (18, 4) NULL,
    [ST_Dollars]               NUMERIC (18, 4) NULL,
    [MM_Dollars]               NUMERIC (18, 4) NULL,
    [ST_Sell_Price_Unit]       NUMERIC (18, 4) NULL,
    [MM_Sell_Price_Unit]       NUMERIC (18, 4) NULL,
    [Max_Price_Rule_ST]        VARCHAR (64)    NULL,
    [Max_Price_Rule_MM]        VARCHAR (64)    NULL,
    [Operation]                VARCHAR (64)    NULL,
    [Qty_Break_Rule_Ind]       INT             NULL,
    [Diff]                     NUMERIC (18, 4) NULL,
    [Grp]                      INT             NULL,
    [ProductValid]             INT             NULL,
    [OrgValid]                 INT             NULL,
    [CustomerValid]            INT             NULL,
    [DrillDownValid]           INT             NULL,
    [Product_Status]           VARCHAR (8)     NULL,
    [Kit_Code]                 VARCHAR (8)     NULL,
    [PRODUCT_STRUCTURE_FK]     BIGINT          NULL,
    [ORG_ENTITY_STRUCTURE_FK]  BIGINT          NULL,
    [CUST_ENTITY_STRUCTURE_FK] BIGINT          NULL,
    [SUPL_ENTITY_STRUCTURE_FK] BIGINT          NULL,
    [Identifier]               VARCHAR (64)    NOT NULL,
    CONSTRAINT [PK_MAUI_Variances] PRIMARY KEY CLUSTERED ([Job_FK_i] ASC, [Identifier] ASC)
);

