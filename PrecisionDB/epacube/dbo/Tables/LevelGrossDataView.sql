﻿CREATE TABLE [dbo].[LevelGrossDataView] (
    [ZoneCount]                    INT             NULL,
    [NODE_LEVEL]                   INT             NOT NULL,
    [NODE_COLUMN_VALUE]            VARCHAR (50)    NULL,
    [NODE_COLUMN_VALUE_PARENT]     VARCHAR (25)    NULL,
    [NODE_SORT]                    VARCHAR (16)    NULL,
    [PRICESHEET_RETAIL_CHANGES_ID] BIGINT          NULL,
    [PRICE_MGMT_TYPE_CR_FK]        BIGINT          NULL,
    [ITEM_CHANGE_TYPE_CR_FK]       INT             NULL,
    [QUALIFIED_STATUS_CR_FK]       INT             NULL,
    [PRODUCT_STRUCTURE_FK]         BIGINT          NULL,
    [ZONE_ENTITY_STRUCTURE_FK]     BIGINT          NULL,
    [ZONE]                         VARCHAR (128)   NULL,
    [ZONE_NAME]                    VARCHAR (128)   NULL,
    [SUBGROUP]                     BIGINT          NULL,
    [SUBGROUP_DESCRIPTION]         VARCHAR (256)   NULL,
    [SUBGROUP_DATA_VALUE_FK]       BIGINT          NULL,
    [ITEM]                         VARCHAR (142)   NULL,
    [ITEM_SEARCH]                  VARCHAR (321)   NULL,
    [ALIAS_SEARCH]                 VARCHAR (273)   NULL,
    [SUBGROUP_SEARCH]              VARCHAR (321)   NULL,
    [ITEM_DESCRIPTION]             VARCHAR (280)   NULL,
    [STORE_PACK]                   INT             NULL,
    [SIZE_UOM]                     VARCHAR (256)   NULL,
    [CUR_COST]                     NUMERIC (18, 6) NULL,
    [CUR_CASE_COST]                NUMERIC (18, 6) NULL,
    [NEW_CASE_COST]                NUMERIC (18, 6) NULL,
    [PRICE_MULTIPLE_CUR]           INT             NULL,
    [PRICE_CUR]                    MONEY           NULL,
    [CUR_GM_PCT]                   NUMERIC (18, 4) NULL,
    [COST_CHANGE]                  NUMERIC (19, 6) NULL,
    [PRICE_CHANGE]                 NUMERIC (21, 4) NULL,
    [NEW_COST]                     NUMERIC (18, 6) NULL,
    [PRICE_NEW]                    NUMERIC (18, 2) NULL,
    [NEW_GM_PCT]                   NUMERIC (18, 4) NULL,
    [TARGET_MARGIN]                NUMERIC (18, 6) NULL,
    [TM_SOURCE]                    VARCHAR (8000)  NULL,
    [SRP_ADJUSTED]                 MONEY           NULL,
    [GM_ADJUSTED]                  NUMERIC (18, 4) NULL,
    [TM_ADJUSTED]                  NUMERIC (18, 4) NULL,
    [HOLD_CUR_PRICE]               INT             NULL,
    [REVIEWED]                     INT             NULL,
    [CIF]                          INT             NULL,
    [UPDATE_USER_PRICE]            VARCHAR (64)    NULL,
    [UPDATE_USER_SET_TM]           VARCHAR (64)    NULL,
    [UPDATE_USER_HOLD]             VARCHAR (64)    NULL,
    [UPDATE_USER_REVIEWED]         VARCHAR (64)    NULL,
    [PRICE_MULTIPLE_NEW]           INT             NULL,
    [ind_Parity]                   INT             NULL,
    [ind_Margin_Tol]               INT             NULL,
    [ind_Rescind]                  INT             NULL,
    [Zone_Number]                  VARCHAR (1)     NULL,
    [item_superscript]             VARCHAR (161)   NULL,
    [item_sort]                    BIGINT          NULL,
    [item_group]                   BIGINT          NULL,
    [item_sort_report]             BIGINT          NULL,
    [Zone_Sort]                    INT             NULL,
    [ind_bold]                     INT             NULL,
    [comment]                      VARCHAR (19)    NOT NULL,
    [ind_discontinued]             VARCHAR (58)    NULL,
    [price_change_effective_date]  DATE            NULL
);

