﻿CREATE TABLE [dbo].[MAUI_Rebate_Volume_Dollars] (
    [Org_Vendor_ID]          VARCHAR (50)    NOT NULL,
    [Org_Vendor_Name]        VARCHAR (100)   NULL,
    [PURCHASES_TOTAL_ANNUAL] NUMERIC (18, 4) NULL,
    [Rebate_Dollars_Annual]  NUMERIC (18, 4) NULL,
    [Rebate_Percent_Annual]  NUMERIC (18, 4) NULL,
    [Year]                   NUMERIC (18)    NULL,
    [Comments]               VARCHAR (256)   NULL,
    CONSTRAINT [PK_MAUI_Rebate_Volume_Dollars] PRIMARY KEY CLUSTERED ([Org_Vendor_ID] ASC)
);

