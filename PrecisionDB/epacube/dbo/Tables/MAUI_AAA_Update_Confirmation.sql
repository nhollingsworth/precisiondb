﻿CREATE TABLE [dbo].[MAUI_AAA_Update_Confirmation] (
    [Update_Procedure_Name] VARCHAR (128) NULL,
    [Update_Procedure_Run]  INT           NULL,
    [Sequence]              INT           IDENTITY (1, 1) NOT NULL
);

