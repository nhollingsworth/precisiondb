﻿CREATE TABLE [dbo].[entity_data] (
    [ENTITY_CLASS_CR_FK]      VARCHAR (50) NULL,
    [es_DATA_NAME_FK]         VARCHAR (50) NULL,
    [ei_ENTITY_DATA_NAME_FK]  VARCHAR (50) NULL,
    [ei_DATA_NAME_FK]         VARCHAR (50) NULL,
    [ei_value]                VARCHAR (50) NULL,
    [ein_ENTITY_DATA_NAME_FK] VARCHAR (50) NULL,
    [ein_DATA_NAME_FK]        VARCHAR (50) NULL,
    [ein_value]               VARCHAR (50) NULL
);

