﻿CREATE TABLE [dbo].[pentaho_processing_test_log] (
    [proc_name]                      VARCHAR (64)  NULL,
    [process_time_start]             DATETIME      NULL,
    [process_time_end]               DATETIME      NULL,
    [ProcParameters]                 VARCHAR (128) NULL,
    [pentaho_processing_test_log_id] INT           IDENTITY (1, 1) NOT NULL
);

