﻿CREATE TABLE [dbo].[EXPORT.EXPORT_TAXONOMY_TREE] (
    [taxonomy_tree_fk]        BIGINT        NULL,
    [CATALOG_NAME]            VARCHAR (256) NULL,
    [LANGUAGE_CODE]           VARCHAR (50)  NULL,
    [TAXONOMY_NODE_fk]        VARCHAR (256) NULL,
    [CATEGORY]                VARCHAR (256) NULL,
    [DESCRIPTION]             VARCHAR (256) NULL,
    [DISPLAY_SEQ]             INT           NULL,
    [LEVEL_SEQ]               INT           NULL,
    [PARENT_TAXONOMY_NODE_fk] BIGINT        NULL,
    [PARENT_NAME]             VARCHAR (256) NULL,
    [IMAGE]                   VARCHAR (MAX) NULL,
    [COMMENT]                 VARCHAR (256) NULL,
    [STATUS]                  VARCHAR (256) NULL,
    [CUSTOM1]                 VARCHAR (256) NULL,
    [CUSTOM2]                 VARCHAR (256) NULL,
    [CUSTOM3]                 VARCHAR (256) NULL,
    [CUSTOM4]                 VARCHAR (256) NULL,
    [CUSTOM5]                 VARCHAR (256) NULL
);

