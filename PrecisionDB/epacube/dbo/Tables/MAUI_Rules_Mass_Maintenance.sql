﻿CREATE TABLE [dbo].[MAUI_Rules_Mass_Maintenance] (
    [RowNum]                         BIGINT          NULL,
    [update_user]                    VARCHAR (64)    NULL,
    [create_user]                    VARCHAR (64)    NULL,
    [Date_Of_Update]                 VARCHAR (10)    NULL,
    [Update_Timestamp]               DATETIME        NULL,
    [Create_Timestamp]               DATETIME        NULL,
    [Effective_Date]                 DATETIME        NULL,
    [Result_Data_Name_FK]            BIGINT          NULL,
    [Rules_Action_Operation1_FK]     BIGINT          NULL,
    [CustCriteria_DN_FK]             BIGINT          NULL,
    [CustValue]                      VARCHAR (64)    NULL,
    [ProdCrit]                       VARCHAR (64)    NULL,
    [ProdCriteria_DN_FK]             BIGINT          NULL,
    [Product]                        VARCHAR (96)    NULL,
    [ProdValue]                      VARCHAR (64)    NULL,
    [WhseValue]                      VARCHAR (64)    NULL,
    [Vendor_ID]                      VARCHAR (64)    NULL,
    [Job_FK_i]                       NUMERIC (18, 2) NULL,
    [WhatIF_ID]                      INT             NULL,
    [Host_Rule_Xref]                 VARCHAR (64)    NULL,
    [Assessment]                     VARCHAR (35)    NULL,
    [MAUI_Rules_Maintenance_ID]      BIGINT          NOT NULL,
    [ID_Common]                      BIGINT          NULL,
    [LevelCode]                      VARCHAR (8)     NULL,
    [Op1]                            VARCHAR (32)    NULL,
    [Op2]                            VARCHAR (32)    NULL,
    [Contract_No]                    VARCHAR (64)    NULL,
    [Rules_FK]                       BIGINT          NULL,
    [Record_Status_CR_FK]            INT             NULL,
    [Operand_1_1]                    NUMERIC (18, 6) NULL,
    [Pct GP]                         NUMERIC (18, 6) NULL,
    [Host_Rule_Xref_Used]            VARCHAR (64)    NULL,
    [Promo]                          INT             NULL,
    [Qty_Break_Rule_Ind]             INT             NULL,
    [OP_Code]                        VARCHAR (8)     NOT NULL,
    [Operand]                        NUMERIC (10, 2) NULL,
    [Hierarchy]                      VARCHAR (8)     NULL,
    [Current_User]                   VARCHAR (64)    NULL,
    [Sel]                            INT             NULL,
    [Transferred_To_ERP]             INT             NULL,
    [MAUI_Rules_Mass_Maintenance_ID] BIGINT          IDENTITY (1, 1) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_mu]
    ON [dbo].[MAUI_Rules_Mass_Maintenance]([Sel] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

