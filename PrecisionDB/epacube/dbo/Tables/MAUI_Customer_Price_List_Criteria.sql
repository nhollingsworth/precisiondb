﻿CREATE TABLE [dbo].[MAUI_Customer_Price_List_Criteria] (
    [Customer_Criteria]                    VARCHAR (64) NOT NULL,
    [Customer_Criteria_ID]                 VARCHAR (64) NOT NULL,
    [Product_Criteria]                     VARCHAR (64) NOT NULL,
    [Product_Criteria_ID]                  VARCHAR (64) NOT NULL,
    [Product_Criteria_Description]         VARCHAR (96) NULL,
    [Data_Name_FK]                         BIGINT       NULL,
    [Activity]                             VARCHAR (64) NOT NULL,
    [Customer_Price_List_Profiles_FK]      BIGINT       NOT NULL,
    [MAUI_Customer_Price_List_Criteria_ID] BIGINT       IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_MAUI_Customer_Price_List_Criteria] PRIMARY KEY CLUSTERED ([Customer_Criteria] ASC, [Customer_Criteria_ID] ASC, [Product_Criteria] ASC, [Product_Criteria_ID] ASC, [Activity] ASC, [Customer_Price_List_Profiles_FK] ASC)
);

