﻿create view v_event_data_base as
select
 'EVENT' as EVENT
 ,event_data_name.NAME    as event_data_name
 ,event_data_name.LABEL    as event_data_name_lbl
 ,entity_data_name.NAME    as entity_data_name
 ,entity_data_name.LABEL    as entity_data_name_lbl
 ,event_entity_class.code as event_entity_class
 ,entity_class.code as entity_class
 ,event_type.code as event_type
 ,event_condition.code as event_condition
 ,event_status.code as event_status
 ,event_priority.code as event_priority
 ,event_source.code as event_source
 ,event_action.code as event_action
 ,event_reason.code as event_reason
 ,result_type.code as result_type
 ,ed.*
 from synchronizer.EVENT_DATA ed
      LEFT JOIN epacube.DATA_NAME entity_data_name on (entity_data_name.DATA_NAME_ID=ed.ENTITY_DATA_NAME_FK)
      LEFT JOIN epacube.DATA_NAME event_data_name on (event_data_name.DATA_NAME_ID = ed.DATA_NAME_FK)
      LEFT JOIN epacube.CODE_REF event_entity_class on (event_entity_class.CODE_REF_ID=ed.EVENT_ENTITY_CLASS_CR_FK)
      LEFT JOIN epacube.CODE_REF event_type on (event_type.CODE_REF_ID=ed.EVENT_TYPE_CR_FK)
      LEFT JOIN epacube.CODE_REF entity_class on (entity_class.CODE_REF_ID=ed.ENTITY_CLASS_CR_FK)
      LEFT JOIN epacube.CODE_REF event_condition on (event_condition.CODE_REF_ID=ed.EVENT_CONDITION_CR_FK)
      LEFT JOIN epacube.CODE_REF event_status on (event_status.CODE_REF_ID=ed.EVENT_STATUS_CR_FK)
      LEFT JOIN epacube.CODE_REF event_priority on (event_priority.CODE_REF_ID=ed.EVENT_PRIORITY_CR_FK)
      LEFT JOIN epacube.CODE_REF event_source on (event_source.CODE_REF_ID=ed.EVENT_SOURCE_CR_FK)
      LEFT JOIN epacube.CODE_REF event_action on (event_action.CODE_REF_ID=ed.EVENT_ACTION_CR_FK)
      LEFT JOIN epacube.CODE_REF event_reason on (event_reason.CODE_REF_ID=ed.EVENT_ACTION_CR_FK)
      LEFT JOIN epacube.CODE_REF result_type on (result_type.CODE_REF_ID=ed.EVENT_ACTION_CR_FK)