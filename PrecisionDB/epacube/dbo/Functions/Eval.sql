﻿CREATE FUNCTION [dbo].[Eval]
(@Expression NVARCHAR (255) NULL)
RETURNS DECIMAL (18, 2)
AS
 EXTERNAL NAME [EvalFunction].[EvalFunction.UserDefinedFunctions].[EVAL]

