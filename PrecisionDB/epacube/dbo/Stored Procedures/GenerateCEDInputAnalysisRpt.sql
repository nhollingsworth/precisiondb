﻿


-- =============================================
-- Author:		Michael Nguyen
-- Create date: 
-- Description:	
-- ============================================
CREATE PROCEDURE [dbo].[GenerateCEDInputAnalysisRpt] 
	@JobID int 


	--Purpose: Create Report specific for customer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        06/28/2013   Initial SQL Version
-- CV        07/18/2013   new logic for Vendor's removed header 



AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @Header VARCHAR(MAX), @BODY VARCHAR(MAX)
	DECLARE @PreviousCount VARCHAR(20), @CurrentCount VARCHAR(20), @ImportCount VARCHAR(20)
	DECLARE @PreviousUpdated VARCHAR(20), @CurrentUpdated VARCHAR(20)
	DECLARE @PreviousSPACount VARCHAR(20), @CurrentSPACount VARCHAR(20)
	DECLARE @PreviousVendorCount VARCHAR(20), @CurrentVendorCount VARCHAR(20)
	DECLARE @PreviousWkUpdate VARCHAR(20), @CurrentWkUpdate VARCHAR(20)
	DECLARE @CountVendorDropped VARCHAR(20), @CurrentVendorDropped VARCHAR(1000)
	DECLARE @CountVendorAdded VARCHAR(20), @CurrentVendorAdded VARCHAR(1000)
	DECLARE @Change_in_count  VARCHAR(10)
	DECLARE @UPCADD  VARCHAR(10)
	DECLARE @UPCDROP  VARCHAR(10)
	DECLARE @UPCBLANK  VARCHAR(10)
	DECLARE @UPCDUPLICATE  VARCHAR(10)
	DECLARE @MFRCODECHANGE  VARCHAR(10)


	DECLARE @ls_stmt varchar(1000)
                  , @l_exec_no bigint
                  , @status_desc varchar(max)
                  , @ErrorMessage nvarchar(4000)
                  , @ErrorSeverity int
                  , @ErrorState INT

                  SET @l_exec_no = 0 ;
                  SET @ls_stmt = 'started execution of [dbo].[GenerateCEDInputAnalysisRpt]. ' 

                  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt,
                       @exec_id = @l_exec_no OUTPUT ;


	--Create table to store email content
	
--drop temp table if it exists
	IF object_id('tempdb..EmailContent') is not null
	  drop table tempdb..EmailContent;

	create table tempdb..EmailContent (ID int PRIMARY KEY IDENTITY, EmailStr varchar(max))

	SET @Header = '<table style="width:95%;">
        <tr>
            <td>RUN DATE: ' + CONVERT(VARCHAR(10),GETDATE(),101) + '</td>
            <td>
                <h1>CED, INC</h1>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>RUN TIME: ' + CONVERT(VARCHAR(8),GETDATE(),108) + '</td>
            <td>EDATAFLEX INPUT ANALYSIS REPORT - SUMMARY</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>USER ID: CED MANAGER</td>
            <td>EXCEPTION ONLY: -- Exception in -- Vendor Selected</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><strong>SECTION 1: FILE SUMMARY</strong></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>';

--------------------------------------------------------------------
--setting percentage for header from params table
----------------------------------------------------------------------
SET @Change_in_count = (select value from epacube.CONFIG_PARAMS where scope = 'REPORTING' and name = 'CHANGE IN RECORD COUNT')
SET @UPCADD  = (select value from epacube.CONFIG_PARAMS where scope = 'REPORTING' and name = 'UPCs ADDED')
SET @UPCDROP = (select value from epacube.CONFIG_PARAMS where scope = 'REPORTING' and name = 'UPCs DROPPED')
SET @UPCBLANK = (select value from epacube.CONFIG_PARAMS where scope = 'REPORTING' and name = 'BLANK UPCs')
SET @UPCDUPLICATE = (select value from epacube.CONFIG_PARAMS where scope = 'REPORTING' and name = 'DUPLICATE UPCs')
SET @MFRCODECHANGE  = (select value from epacube.CONFIG_PARAMS where scope = 'REPORTING' and name = 'MFR GROUP CODES CHANGED')

------------------------------------------------------------------------------
	
--Get total rec count AFTER import
select @CurrentCount = cast(count(*) as varchar(20))
from epacube.PRODUCT_STRUCTURE ps with (nolock)
where ps.PRODUCT_STATUS_CR_FK = 100

set @ImportCount = (select cast(count(*) as varchar(20))
from epacube.PRODUCT_STRUCTURE ps with (nolock)
where ps.PRODUCT_STATUS_CR_FK = 100
and ps.job_fk = @JobID)

--Get total rec count BEFORE import
select  @PreviousCount = cast((cast(@CurrentCount as numeric) - cast(@ImportCount as numeric)) as varchar(20))

--UPDATED THROUGH for last time it ran
select top 1 @PreviousUpdated = CONVERT(VARCHAR(10),job_complete_timestamp,101) 
from common.job with (nolock)
where name = 'TRADE_WEEKLY_CHGS'
and job_id <> @JobID -- parameter
order by job_id desc

DECLARE @CurrentUpdatedDate DATETIME

--UPDATED THROUGH for current time it ran
select top 1 @CurrentUpdated = CONVERT(VARCHAR(10),job_create_timestamp,101), @CurrentUpdatedDate = job_create_timestamp   --2013-03-18 11:49:35.050
from common.job with (nolock)
where job_id = @JobID -- job fk parameter

 --MFR GROUP CODE COUNT CURRENT
 select @CurrentSPACount = count(distinct value)
from epacube.entity_data_value dv with (nolock)	--more mfg specific for entity_data_value
where dv.DATA_NAME_FK in (
100143901,
100143902,
100143903,
100143904,
100143905,
100143906,
100143907,
100143908,
100143909,
100143910)


select @PreviousSPACount = (select  ( @CurrentSPACount - count(distinct value))  
from epacube.entity_data_value dv with (nolock)--more mfg specific for entity_data_value
where dv.DATA_NAME_FK in (
100143901,
100143902,
100143903,
100143904,
100143905,
100143906,
100143907,
100143908,
100143909,
100143910)
and job_fk = @JobID)  



--VENDOR UCC COUNT CURRENT (ACTIVE PRODUCT HAVING CURRENT UCC)
SELECT @CurrentVendorCount = COUNT(DISTINCT EI.VALUE) 
FROM EPACUBE.PRODUCT_ASSOCIATION PA with (nolock)
JOIN EPACUBE.ENTITY_IDENTIFICATION EI with (nolock) ON PA.ENTITY_STRUCTURE_FK = EI.ENTITY_STRUCTURE_FK
INNER JOIN EPACUBE.PRODUCT_STRUCTURE PS with (nolock) ON PS.PRODUCT_STRUCTURE_ID = PA.PRODUCT_STRUCTURE_FK
AND PS.PRODUCT_STATUS_CR_FK = 100
WHERE PA.DATA_NAME_FK = 159300 AND EI.DATA_NAME_FK = 100143110

--VENDOR UCC COUNT PREVIOUS (ACTIVE PRODUCT HAVING CURRENT UCC)  --added timestamp check because we want to see if vendor was added not products
SELECT @PreviousVendorCount = @CurrentVendorCount - COUNT(DISTINCT EI.VALUE)	
FROM EPACUBE.PRODUCT_ASSOCIATION PA with (nolock)
JOIN EPACUBE.ENTITY_IDENTIFICATION EI with (nolock) ON PA.ENTITY_STRUCTURE_FK = EI.ENTITY_STRUCTURE_FK
INNER JOIN EPACUBE.PRODUCT_STRUCTURE PS with (nolock)
	ON PS.PRODUCT_STRUCTURE_ID = PA.PRODUCT_STRUCTURE_FK
		AND PS.JOB_FK = @JobID
		AND PS.PRODUCT_STATUS_CR_FK = 100
WHERE PA.DATA_NAME_FK = 159300 AND EI.DATA_NAME_FK = 100143110
and ei.create_timestamp like ps.CREATE_TIMESTAMP

--CURRENT WEEKLY LOAD
SELECT @CurrentWkUpdate = substring(DATA1,32,4) + '  ' +  CONVERT(VARCHAR(10),JOB_CREATE_TIMESTAMP,101) FROM COMMON.JOB with (nolock) WHERE JOB_ID = @JobID

---NOTE NEED TO ADJUST ONCE PUT ON EACH SERVER AS DIRECTORY STRUCTURE DETERMINES STRING LENGTH
--select substring(DATA1,32,4) + '  ' +  CONVERT(VARCHAR(10),JOB_CREATE_TIMESTAMP,101) FROM COMMON.JOB WHERE JOB_ID = 1632	

--PREVIOUS PART 1,2 WEEKLY LOAD  
SELECT TOP 1 @PreviousWkUpdate = substring(DATA1,32,4) + '  ' + CONVERT(VARCHAR(10),JOB_CREATE_TIMESTAMP,101) FROM COMMON.JOB with (nolock)
WHERE NAME = 'TRADE_WEEKLY_CHGS' AND JOB_ID <> @JobID ORDER BY JOB_ID DESC	

--------VENDORS ADDED SECTION
 ----@CountVendorAdded  @CurrentVendorAdded

 SELECT  @CurrentVendorAdded = 
 COALESCE(@CurrentVendorAdded + ', ', '') +  (select distinct  ERROR_DATA2 from stage.STG_RECORD_ERRORS with (nolock)
where ERROR_NAME = 'Entity is UNKNOWN and is REQUIRED for Attribute '
and job_fk = @JobID)

------ Isnull((
------select distinct ERROR_DATA2 from stage.STG_RECORD_ERRORS with (nolock)
------where ERROR_NAME = 'Entity is UNKNOWN and is REQUIRED for Attribute '
------and job_fk = @JobID),'0')


select @CountVendorAdded  = isNULL((select cast(count(distinct ERROR_DATA2) as varchar(20)) from stage.STG_RECORD_ERRORS with (nolock)
where ERROR_NAME = 'Entity is UNKNOWN and is REQUIRED for Attribute '
and job_fk = @JobID),'0')
	
----- VENDORS DROPPED
---get total products by vendor count  @CountVendorDropped VARCHAR(20), @CurrentVendorDropped VARCHAR(20)

SELECT @CurrentVendorDropped = ISNULL(COALESCE(@CurrentVendorDropped + ', ', '') + a.mfr_code + '-' + a.mfr_ucc ,'0')
from (
----total record per mfr
select ei.value MFR_CODE, ein.VALUE MFR_UCC,  ISNULL(count(1),0) Records 
from epacube.product_association pa with (nolock)
inner join epacube.entity_identification ein  with (nolock) 
on (pa.entity_structure_fk = ein.entity_structure_fk
and ein.DATA_NAME_FK = 100143110)
inner join epacube.entity_ident_nonunique ei with (nolock)
on (pa.entity_structure_fk = ei.entity_structure_fk
and Pa.data_name_fk = 159300)
and ei.DATA_NAME_FK =100143113
and ei.ENTITY_STRUCTURE_FK in (select distinct ei.ENTITY_STRUCTURE_FK
from import.IMPORT_RECORD_DATA ird with (nolock)
inner join epacube.entity_identification ei  with (nolock) 
on (ei.value = ird.DATA_POSITION4
and ei.DATA_NAME_FK = 100143110)
where ird.JOB_FK = @JobID)
group by ein.value, ei.value) a
inner join (
--,'4-# UPCs DROPPED per mfr'
select  ein.value MFR_CODE, ei.value MFR_UCC, isnull(count(1),0) item_count
from synchronizer.EVENT_DATA_history ed with (nolock)
inner join epacube.DATA_NAME dn  with (nolock)
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
inner join epacube.PRODUCT_ASSOCIATION pa with (nolock)
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENTIFICATION ei with (nolock)
on (pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein with (nolock)
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
where ed.IMPORT_JOB_FK = @jobid
and ed.DATA_NAME_FK =  820201
and ed.new_data = 'Q'
and EVENT_ACTION_CR_FK = 52
group by ei.value, ein.value) b 
----
on (b.item_count = a.Records
and b.MFR_UCC = a.MFR_UCC)


SET @CountVendorDropped = ISNULL(Count( @CurrentVendorDropped),'0')

------------------------------------------------------------------------------------------------------------------

SET @header = @header + 
	'<tr>
            <td>
                <table style="width:91%;">
                    <tr>
                        <td>&nbsp;</td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; "><strong>PREVIOUS</strong></td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; "><strong>CURRENT</strong></td>
                        
                    </tr>
                    <tr>
                        <td>UPDATED THROUGH:</td>
						<td>' +  @PreviousUpdated + '</td>
                        <td>' +  @CurrentUpdated + '</td>
                    </tr>
                    <tr>
                        <td>RECORD COUNT:</td>
                        <td>' +  @PreviousCount + '</td>
                        <td>' +  @CurrentCount + '</td>
                    </tr>
                    <tr>
                        <td>MFR GROUP CODE COUNT:</td>
                        <td>' +  @PreviousSPACount + '</td>
                        <td>' +  @CurrentSPACount + '</td>
                    </tr>
                    <tr>
                        <td>VENDOR COUNT:</td>
                        <td>' +  @PreviousVendorCount + '</td>
                        <td>' +  @CurrentVendorCount + '</td>
                    </tr>
                    <tr>
                        <td>WEEKLY UPDATES:</td>
                        <td>' +  @PreviousWkUpdate + '</td>
                        <td>' +  @CurrentWkUpdate + '</td>
                    </tr>
					 <tr>
                        <td>EPACUBE JOB ID:</td>
                        <td>&nbsp;</td>
                        <td>' +  cast(@JobID as varchar(10)) + '</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; "><strong>COUNT</strong></td>
                        <td style="border-bottom-width:1px; border-bottom-style:solid; "><strong>ID(s)</strong></td>
                    </tr>
                    <tr>
                        <td><span>VENDORS DROPPED THIS PERIOD:</span></td>
                        <td>' + @CountVendorDropped + '</td>
                        <td><span>' + isnull( @CurrentVendorDropped,'0') + '</span></td>
                    </tr>	
                    <tr>
                        <td><span>VENDORS ADDED THIS PERIOD:</span></td>
                        <td>' + @CountVendorAdded + '</td>
                         <td><span>' + isnull( @CurrentVendorAdded,'0') + '</span></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    </table>
            </td>
            <td style="vertical-align:top">
                
                <table style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td colspan="2"><span>EXCEPTION CRITERIA SETTINGS</span></td>
                    </tr>
                    <tr>
                        <td><span>CHANGE IN RECORD COUNT FROM LAST MONTH</span></td>
                        <td><span>'+ @Change_in_count +'</span></td>
                    </tr>
                    <tr>
                        <td><span># UPCs ADDED</span></td>
                        <td><span>'+ @UPCADD +'</span></td>
                    </tr>
                    <tr>
                        <td><span># UPCs DROPPED</span></td>
                        <td><span>'+ @UPCDROP +'</span></td>
                    </tr>
                    <tr>
                        <td><span># BLANK UPCs</span></td>
                        <td><span>'+ @UPCBLANK +'</span></td>
                    </tr>
                    <tr>
                        <td><span># DUPLICATE UPCs</span></td>
                        <td><span>'+ @UPCDUPLICATE +'</span></td>
                    </tr>
                    <tr>
                        <td><span># MFR GROUP CODES CHANGED</span></td>
                        <td><span>'+ @MFRCODECHANGE +'</span></td>
                    </tr>
                </table>
                
            </td>
            <td>&nbsp;</td>
        </tr>'; 

		--print @header;
		INSERT INTO tempdb..EmailContent VALUES(@Header)


DECLARE @VendorInfo TABLE(MFR_CODE varchar(20), MFR_UCC varchar(20), Description varchar(50), 
Total_Records int, item_count int, mathpct decimal(12,2), to_display int)

insert into @VendorInfo
--------	
select ei.value MFR_CODE, ein.VALUE MFR_UCC,  '1-TOTAL CURRENT RECORDS' Description , ISNULL(count(1),0) Records , 0 item_count , 0 mathpct, 0 to_display
from epacube.product_association pa with (nolock)
inner join epacube.entity_identification ein  with (nolock) 
on (pa.entity_structure_fk = ein.entity_structure_fk
and ein.DATA_NAME_FK = 100143110)
inner join epacube.entity_ident_nonunique ei with (nolock)
on (pa.entity_structure_fk = ei.entity_structure_fk
and Pa.data_name_fk = 159300)
and ei.DATA_NAME_FK =100143113
and ei.ENTITY_STRUCTURE_FK in (select distinct ei.ENTITY_STRUCTURE_FK
from import.IMPORT_RECORD_DATA ird with (nolock)
inner join epacube.entity_identification ei  with (nolock) 
on (ei.value = ird.DATA_POSITION4
and ei.DATA_NAME_FK = 100143110)
where ird.JOB_FK = @JobID)
group by ein.value, ei.value


UNION

select ei.value MFR_CODE, ein.VALUE MFR_UCC,'2-CHANGE IN RECORD COUNT FROM LAST IMPORT' Description, 0 records, isnull(count(1),0) item_count , 0 mathpct, 0 to_display
from epacube.product_association pa with (nolock)    
inner join epacube.PRODUCT_STRUCTURE ps with (nolock)
on (pa.PRODUCT_STRUCTURE_FK = ps.PRODUCT_STRUCTURE_ID
and ps.JOB_FK = @jobid)
inner join epacube.entity_identification ein with (nolock)
on (pa.entity_structure_fk = ein.entity_structure_fk
and ein.DATA_NAME_FK = 100143110)
inner join epacube.entity_ident_nonunique ei with (nolock)
on (pa.entity_structure_fk = ei.entity_structure_fk
and Pa.data_name_fk = 159300)
and ei.DATA_NAME_FK =100143113
group by ein.value, ei.value



UNION

--UPC added

select ein.value MFR_CODE, ei.value MFR_UCC, '3-# UPCs ADDED' Description ,  0 records, isnull(count(1),0) item_count, 0 mathpct, 0 to_display
from synchronizer.EVENT_DATA_history ed with (nolock)
inner join epacube.PRODUCT_ASSOCIATION pa with (nolock)
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.entity_identification ei with (nolock)
on (pa.entity_structure_fk = ei.entity_structure_fk
and ei.DATA_NAME_FK = 100143110)
inner join epacube.entity_ident_nonunique ein with (nolock)
on (pa.entity_structure_fk = ein.entity_structure_fk)
and ein.DATA_NAME_FK =100143113
where ed.IMPORT_JOB_FK = @jobid
and ed.DATA_NAME_FK = 110102
and EVENT_ACTION_CR_FK = 52
group by ei.value, ein.value

--UPC dropped Total number of UPCs that were inactive (Legacy_update_status = Q)
UNION

select  ein.value MFR_CODE, ei.value MFR_UCC,'4-# UPCs DROPPED' Description , 0 records, isnull(count(1),0) item_count, 0 mathpct, 0 to_display
from synchronizer.EVENT_DATA_history ed with (nolock)
inner join epacube.DATA_NAME dn  with (nolock)
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
inner join epacube.PRODUCT_ASSOCIATION pa with (nolock)
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENTIFICATION ei with (nolock)
on (pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein with (nolock)
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
where ed.IMPORT_JOB_FK = @jobid
and ed.DATA_NAME_FK =  820201
and ed.new_data = 'Q'
and EVENT_ACTION_CR_FK = 52
group by ei.value, ein.value

UNION

select ein.value MFR_CODE, ei.value MFR_UCC,'5-# BLANK UPCs' Description , 0 records, isnull(count(1),0) item_count, 0 mathpct, 0 to_display
from synchronizer.EVENT_DATA ed with (nolock)
inner join synchronizer.event_data_errors ede with (nolock)
on (ede.event_fk = ed.event_id
and ed.IMPORT_JOB_FK = @jobid
and ede.error_name = 'PRODUCT COMPLETENESS::UPC' )
inner join epacube.PRODUCT_ASSOCIATION pa with (nolock)
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENTIFICATION ei  with (nolock)
on (pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein with (nolock)
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
and ed.data_name_fk = 159200
group by ein.value, ei.value

UNION
--from cleanser as should get stopped here when report is run

select ein.value MFR_CODE, ei.value MFR_UCC,'6-# DUPLICATE UPCs' Description , 0 records, isnull(count(1),0) item_count, 0 mathpct, 0 to_display
from stage.STG_RECORD_ERRORS sre with (nolock)
inner join stage.STG_RECORD sr with (nolock) 
on (sre.STG_RECORD_FK = sr.STG_RECORD_id
and sr.JOB_FK = @jobid
and sre.error_name = 'Duplicate Unique Identifiers in same import job' )
inner join import.import_record_data ird with (nolock)
on (ird.RECORD_NO = sr.RECORD_NO
and ird.JOB_FK = @jobid)
inner join epacube.ENTITY_IDENTIFICATION ei  with (nolock)
on (ird.DATA_POSITION4 = ei.value
and ei.DATA_NAME_FK = 100143110)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein with (nolock)
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
group by ein.value, ei.value

UNION

---MFR GROUP CODES CHANGE
select  ein.value MFR_CODE, ei.value MFR_UCC, '7-# MFR GROUP CODES CHANGED' Description,  0 records, isnull(count(1),0) item_count, 0 mathpct, 0 to_display
from synchronizer.EVENT_DATA_history ed with (nolock)
inner join epacube.DATA_NAME dn with (nolock)
on (dn.DATA_NAME_ID = ed.DATA_NAME_FK)
inner join epacube.PRODUCT_ASSOCIATION pa with (nolock)
on (ed.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.DATA_NAME_FK = 159300)
inner join epacube.ENTITY_IDENTIFICATION ei with (nolock)
on (pa.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ei.DATA_NAME_FK = 100143110)
inner join epacube.ENTITY_IDENT_NONUNIQUE ein with (nolock)
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 100143113)
where ed.IMPORT_JOB_FK =   @jobid
and ed.DATA_NAME_FK in  ( 100143901, 100143902, 100143903)
and ed.EVENT_ACTION_CR_FK = 52
group by ein.value, ei.value
---------

order by MFR_CODE, MFR_UCC, Description asc

-------------------------------------------------------------------------------------------------------------
---doing math
-------------------------------------------------------------------------------------------------------------

update vs1
	set Total_Records = vs.total_records
from @VendorInfo vs1 
inner join @VendorInfo vs 
on (vs.MFR_UCC = vs1.MFR_UCC
and  vs.Description = '1-TOTAL CURRENT RECORDS') 


update @VendorInfo
set mathpct  = cast( (cast(item_count as decimal(12,2))/cast(Total_Records as decimal(12,2))) * 100 as decimal(12,2))
where Description <> '1-TOTAL CURRENT RECORDS'
and Total_Records <> 0 



--update @VendorInfo
--set mathpct  = cast(item_count/Total_Records * 100 as decimal(12,6))
--where Description <> '1-TOTAL CURRENT RECORDS'
--and Total_Records <> 0 

-------------------------------------------------------------------------------------------------------------
--display or not 
------------------------------------------------------------------------------------------------------------
update @VendorInfo
set to_display = 1
where mathpct >= (select replace(value,'%','') from epacube.CONFIG_PARAMS where name = 'CHANGE IN RECORD COUNT')
and Description = '2-CHANGE IN RECORD COUNT FROM LAST IMPORT'

update @VendorInfo
set to_display = 1
where mathpct >= (select replace(value,'%','') from epacube.CONFIG_PARAMS where name = 'UPCs ADDED')
and Description = '3-# UPCs ADDED'

update @VendorInfo
set to_display = 1
where mathpct >= (select replace(value,'%','') from epacube.CONFIG_PARAMS where name = 'UPCs DROPPED')
and Description = '4-# UPCs DROPPED'

update @VendorInfo
set to_display = 1
where mathpct >= (select replace(value,'%','') from epacube.CONFIG_PARAMS where name = 'BLANK UPCs')
and Description = '5-# BLANK UPCs'


update @VendorInfo
set to_display = 1
where mathpct >= (select replace(value,'%','') from epacube.CONFIG_PARAMS where name = 'DUPLICATE UPCs')
and Description = '6-# DUPLICATE UPCs'

update @VendorInfo
set to_display = 1
where mathpct >= (select replace(value,'%','') from epacube.CONFIG_PARAMS where name = 'MFR GROUP CODES CHANGED')
and Description = '7-# MFR GROUP CODES CHANGED'

--------show total records if any of the to display for that ucc are displayed

update vs1
	set to_display = 1
from @VendorInfo vs1 
inner join @VendorInfo vs 
on (vs.MFR_UCC = vs1.MFR_UCC
and vs.to_display = 1)
where  vs1.Description = '1-TOTAL CURRENT RECORDS'

-------------------------------------------------------------------------------------------------------------------



DECLARE @UCC_Code varchar(20), @MFR_CODE varchar(20), @Count int, @LoopIdx int, @TotalRec int, @item_count int, @Title varchar(50), @mathpct decimal(12,2), @to_display int

SET @body = '<tr>
            <td><strong><span>SECTION 2: VENDOR</span></strong><span><strong> SUMMARY</strong></span></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <table style="width:100%;border-collapse:collapse;">
                    <tr>
                        <td><b>VENDOR</b></td>
                        <td><b>ADD / DROP</b></td>
                        <td><b>METRIC</b></td>
                        <td><b>COUNT</b></td>
                        <td><b>PCT</b></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>'

					INSERT INTO tempdb..EmailContent VALUES(@body)
--Select first Vendor 
----declare @vendor varchar(50)
----select top 1 @Vendor = MFR_CODE + MFR_UCC from @VendorInfo

BEGIN TRY

--DECLARE @pct  decimal(12,6)

DECLARE  cur_v_import cursor local for
			select * from @VendorInfo where to_display = 1

 OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO @UCC_Code, @MFR_Code, @Title, @Count, @item_count, @mathpct, @to_display

WHILE @@FETCH_STATUS = 0 
 BEGIN
    
	
	SET @TotalRec = @Count
		--SET @pct = @mathpct  
		
		--declare @ShowContent bit
		--set @ShowContent = @to_display
		

		--if (@ShowContent = 1) 
		
		--BEGIN   
				
			  if @Title = '1-TOTAL CURRENT RECORDS'   --adding line under this entry
			
			SET @body =   '<tr style="border-top-color: black; border-top-width: 1px; border-top-style: solid;">
							<td style="border-top-width:1px; border-top-style:solid;">' + @UCC_Code + '-' + @MFR_CODE + '</td>
							<td style="border-top-width:1px; border-top-style:solid; ">&nbsp;</td>
							<td style="border-top-width:1px; border-top-style:solid; "><span>' + RIGHT(@Title, LEN(@Title) - 2) + '</span></td>
							<td style="border-top-width:1px; border-top-style:solid; "><span>' + case when @item_count = 0 then cast(@TotalRec as varchar(20)) else cast(@item_Count as varchar(20))  end + '</span></td>
							<td style="border-top-width:1px; border-top-style:solid; "><span>' + isnull(cast(@mathpct as varchar(10)),'0')   + ' %' + '</span></td>
							<td style="border-top-width:1px; border-top-style:solid; ">&nbsp;</td>
							<td style="border-top-width:1px; border-top-style:solid; ">&nbsp;</td>
						</tr>'
						else
			
				SET @body = '<tr>
							<td>' + @UCC_Code + '-' + @MFR_CODE + '</td>
							<td>&nbsp;</td>
							<td><span>' + RIGHT(@Title, LEN(@Title) - 2) + '</span></td>
							<td><span>' + case when @item_count = 0 then cast(@TotalRec as varchar(20)) else cast(@item_Count as varchar(20))  end + '</span></td>
							<td><span>' + isnull(cast(@mathpct as varchar(10)),'0')   + ' %' + '</span></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>'
				
						
	
	
	--store content to table
	INSERT INTO tempdb..EmailContent VALUES(@body)
	--reset body string
	SET @body = ''
	--SET @LoopIdx = @LoopIdx + 1
 FETCH NEXT FROM cur_v_import INTO @UCC_Code, @mfr_code, @Title, @Count, @item_count, @mathpct, @to_display

 END --cur_v_import LOOP

WHILE @@FETCH_STATUS = 0 
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 
	 
	 declare @EmailSub varchar(200), @FileName varchar(200), @To_RecipientsFileName varchar(2000)

	 ----select @To_RecipientsFileName = Recipients from [epacube].[EMAIL_ALERT_SQL] where Email_alert_sql_id = 1002
	 select @To_RecipientsFileName = value from epacube.CONFIG_PARAMS where scope = 'REPORTING' and name = 'EMAIL_TO'

	 set @EmailSub = 'CED TRADE ANALYSIS REPORT ' + CONVERT(VARCHAR(10),GETDATE(),101)
	 set @FileName = 'CED_TRADE_ANALYSIS_REPORT_' + CONVERT(VARCHAR(10),GETDATE(),101) + '.html'

	  --Send email
	 EXEC msdb.dbo.sp_send_dbmail  
		@recipients = @To_RecipientsFileName,
		@query = 'SET NOCOUNT ON;select EmailStr from tempdb..EmailContent;SET NOCOUNT OFF',
		@subject = @EmailSub,
		@query_result_header=0,
		@exclude_query_output=1,
		@body_format = 'HTML',
		@query_attachment_filename = @FileName,
		@query_no_truncate = 1,
		@attach_query_result_as_file = 1 ;

		--select * from tempdb..EmailContent

	 drop table tempdb..EmailContent

------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------


SET @status_desc = 'finished execution of [dbo].[GenerateCEDInputAnalysisRpt]'
Exec exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
Exec exec_monitor.print_status_sp  @l_exec_no;



END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of [dbo].[GenerateCEDInputAnalysisRpt] has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END


