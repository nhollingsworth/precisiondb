﻿
-- =============================================
-- Author:		Gary Stone
-- Create date: 7/11/2018
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [IMPORTS].[IMPORT_URM_ITEM_BREAKDOWN] @Job_FK bigint

AS
	SET NOCOUNT ON;

	--Declare @Job_FK bigint = 0
	
	If Object_id('tempdb..#Breakdown') is not null
	drop table #Breakdown

	Select distinct
	[ITEM_NBR], [ITEM_TYPE], [BRKDWNITM], [BRKDITMTY], [STATE_ID], [BRKDWNPCK], [DTE_CRT], [TME_CRT], [USR_CRT], [DTE_UPD], [TME_UPD], [USR_UPD], [Job_FK]
	into #Breakdown
	from [IMPORTS].[URM_ITEM_BREAKDOWN]
	where job_FK = @Job_FK
	
	Create index idx_01 on #Breakdown(item_nbr, item_type, brkdwnitm, brkditmty, State_ID)

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [PROCEDURE_NAME], [TYPE_OF_DATA], RECORDS_LOADED, JOB_FK)
	select getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ITEM_BREAKDOWN' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.PRODUCT_BOM' 'TARGET_TABLE'
	, 'IMPORTS.IMPORT_URM_ITEM_BREAKDOWN' 'PROCEDURE_NAME'
	, 'BREAKDOWN ITEMS' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM #Breakdown UIB
		inner join epacube.epacube.product_identification pi_p with (nolock) on uib.item_nbr + uib.item_type = pi_p.value and pi_p.data_name_fk = 110100
		inner join epacube.epacube.product_identification pi_c with (nolock) on uib.brkdwnitm + uib.brkditmty = pi_c.value and pi_c.data_name_fk = 110100
		left join epacube.epacube.PRODUCT_BOM PB on pi_p.PRODUCT_STRUCTURE_FK = pb.product_structure_fk and pi_c.PRODUCT_STRUCTURE_FK = pb.CHILD_PRODUCT_STRUCTURE_FK
													and isnull(UIB.State_ID, '') = isnull(pb.[state], '')
		where pb.PRODUCT_BOM_ID is null) 'RECORDS_LOADED'
	, @Job_FK 'Job_FK'

	INSERT INTO epacube.[PRODUCT_BOM]
	(DATA_NAME_FK, PRODUCT_STRUCTURE_FK, CHILD_PRODUCT_STRUCTURE_FK, QTY, [STATE], RECORD_STATUS_CR_FK, CREATE_TIMESTAMP, CREATE_USER, UPDATE_TIMESTAMP, UPDATE_USER, Job_FK)
	select
	500213 'Data_Name_FK'
	, pi_p.product_structure_fk 'PRODUCT_STRUCTURE_FK'
	, pi_c.product_structure_fk 'CHILD_PRODUCT_STRUCTURE_FK'
	, brkdwnpck 'QTY'
	, STATE_ID
	, 1 'Record_Status_CR_FK'
	, [epacube].[URM_DATE_TIME_CONVERSION](dte_crt, tme_crt) 'Create_Timestamp'
	, usr_crt 'Create_User'
	, [epacube].[URM_DATE_TIME_CONVERSION](dte_upd, tme_upd) 'Update_Timestamp'
	, usr_upd 'Update_User'
	, @Job_FK 'Job_FK'
	from #Breakdown UIB
	inner join epacube.epacube.product_identification pi_p with (nolock) on uib.item_nbr + uib.item_type = pi_p.value and pi_p.data_name_fk = 110100
	inner join epacube.epacube.product_identification pi_c with (nolock) on uib.brkdwnitm + uib.brkditmty = pi_c.value and pi_c.data_name_fk = 110100
	left join epacube.epacube.PRODUCT_BOM PB on pi_p.PRODUCT_STRUCTURE_FK = pb.product_structure_fk and pi_c.PRODUCT_STRUCTURE_FK = pb.CHILD_PRODUCT_STRUCTURE_FK
												and isnull(UIB.State_ID, '') = isnull(pb.[state], '')
												and uib.BRKDWNPCK = pb.qty
	where pb.PRODUCT_BOM_ID is null

	Update PB
	Set Qty = uib.brkdwnpck
	, update_user = uib.usr_upd
	, UPDATE_TIMESTAMP = [epacube].[URM_DATE_TIME_CONVERSION](uib.dte_upd, uib.tme_upd)
	from epacube.epacube.PRODUCT_BOM PB
	inner join epacube.epacube.product_identification pi_p on pb.PRODUCT_STRUCTURE_FK = pi_p.PRODUCT_STRUCTURE_FK and pi_p.DATA_NAME_FK = 110100
	inner join epacube.epacube.product_identification pi_c on pb.CHILD_PRODUCT_STRUCTURE_FK = pi_c.PRODUCT_STRUCTURE_FK and pi_c.DATA_NAME_FK = 110100
	inner join #Breakdown UIB on	pi_p.value = uib.item_nbr + uib.item_type
																	and	pi_c.value = uib.brkdwnitm + uib.brkditmty
																	and isnull(pb.[state], '') = isnull(UIB.State_ID, '')
	Where PB.Qty <> uib.brkdwnpck
	and PB.data_name_fk = 500213

	Delete PB
	from epacube.epacube.PRODUCT_BOM PB
	inner join epacube.epacube.product_identification pi_p on pb.PRODUCT_STRUCTURE_FK = pi_p.PRODUCT_STRUCTURE_FK and pi_p.DATA_NAME_FK = 110100
	inner join epacube.epacube.product_identification pi_c on pb.CHILD_PRODUCT_STRUCTURE_FK = pi_c.PRODUCT_STRUCTURE_FK and pi_c.DATA_NAME_FK = 110100
	left join #Breakdown UIB on	pi_p.value = uib.item_nbr + uib.item_type
																	and	pi_c.value = uib.brkdwnitm + uib.brkditmty
																	and isnull(pb.[state], '') = isnull(UIB.State_ID, '')
	where UIB.Dte_CRT is null
	and PB.data_name_fk = 500213
