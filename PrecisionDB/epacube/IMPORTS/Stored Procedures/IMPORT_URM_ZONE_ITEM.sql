﻿
-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        04/27/2018   Initial Version 
-- CV		 09/20/2018   Added the setting of Product and zone structures up front.
-- NH		 01/08/2019   Added data validation error logging JIRA (URM-1176)

CREATE PROCEDURE [IMPORTS].[IMPORT_URM_ZONE_ITEM]
	@Job_FK bigint
	, @import_exec_no AS BIGINT = null OUTPUT   
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
	 DECLARE @l_import_exec_no	 BIGINT;
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint
	 DECLARE @in_job_fk bigint
	 DECLARE @Server varchar(128) 

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
	  SET @l_import_exec_no = 0;
	  SET @Server = (select @@SERVERNAME)



     -- EXEC [AMAZONA-8NUFTI5].common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.URM_ZONE_ITEM', 
					--							@exec_id = @l_exec_no OUTPUT,
					--							@epa_job_id = @in_job_fk;

     --SET @l_sysdate = GETDATE() --used for the beginning timestamp
     --SET @V_START_TIMESTAMP = getdate()  -- used in steps


-------------------------------------------------------------------------------------
UPDATE RD
set product_structure_fk = PI.product_structure_fk
from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] RD
inner join epacube.epacube.PRODUCT_IDENTIFICATION PI on pi.value = RD.ITEM_NBR + RD.ITEM_TYPE
And pi.data_name_fk = 110100
--and rd.product_structure_fk is NULL
where rd.job_FK = @Job_FK

 ---get Zone Structure 

UPDATE RD
set Zone_entity_structure_fk = ei.entity_structure_fk
from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] RD
 inner join epacube.epacube.entity_identification ei on ei.value  = RD.ZONE_NUM
 and ei.DATA_NAME_FK = 151110
 where rd.job_FK = @Job_FK

-- select * from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] RD

----Update Product_Associations to match urm_zone_item table
--select * from epacube.epacube.data_name where DATA_NAME_ID = 159450 --159450	ZONE_ASSOCIATION

--Create Temp Table for Performance
	If Object_ID('tempdb..#ZnPA') is not null
	drop table #ZnPA

	Select distinct zone_num, item_nbr, item_type, item_nbr + item_type 'item', PRODUCT_STRUCTURE_FK, zone_entity_structure_fk
	,epacube.[URM_DATE_CONVERSION](ITMTRMDTE)  End_date 
	,epacube.[URM_DATE_TIME_CONVERSION](DTE_CRT, TME_CRT) 'CREATE_TIMESTAMP'
	,epacube.[URM_DATE_TIME_CONVERSION](DTE_UPD, TME_UPD) 'UPDATE_TIMESTAMP'
	, @Job_FK 'Job_FK'
	into #ZnPA 
	from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] Zn
	where zn.job_FK = @Job_FK
		 
	alter table #ZnPA add [ZnPA_ID] bigint identity(1, 1) not null

	Create index idx_01 on #ZnPA(product_structure_fk, zone_entity_structure_fk)
	Create index idx_03 on #ZnPA(ZnPA_ID)

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, Job_FK)
	select 
	'IMPORTS.[IMPORT_URM_ZONE_ITEM]' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE_ITEM' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.PRODUCT_ASSOCIATION' 'TARGET_TABLE'
	, 'NEW ZONE ITEM ASSOCIATIONS' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM 
		(
				Select 159450 'Data_Name_FK', zn.PRODUCT_STRUCTURE_FK, 1 'org_entity_structure_fk', 10117 'ENTITY_CLASS_CR_FK', zn.zone_entity_structure_fk, 1 'RECORD_STATUS_CR_FK'
				, zn.CREATE_TIMESTAMP, zn.UPDATE_TIMESTAMP
				from #ZnPA Zn
				left join epacube.epacube.product_association pa with (nolock) on zn.zone_entity_structure_fk = pa.ENTITY_STRUCTURE_FK and zn.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and pa.data_name_fk = 159450
				where pa.PRODUCT_ASSOCIATION_ID is null
				group by zn.ZONE_ENTITY_STRUCTURE_FK, zn.PRODUCT_STRUCTURE_FK, zn.CREATE_TIMESTAMP, zn.UPDATE_TIMESTAMP
		) A)  'RECORDS_LOADED'
	, @Job_FK

	SET @l_import_exec_no = SCOPE_IDENTITY()

--Log error rows to table for troubleshooting
	INSERT INTO [PRECISION].DAILY_IMPORT_ERRORS (DAILY_IMPORT_LOG_FK, ZONE_NUM, ITEM_NBR, ITEM_TYPE, PRODUCT_STRUCTURE_FK, ZONE_ENTITY_STRUCTURE_FK, CREATE_TIMESTAMP)
	SELECT @l_import_exec_no, ZONE_NUM, ITEM_NBR, ITEM_TYPE, PRODUCT_STRUCTURE_FK, ZONE_ENTITY_STRUCTURE_FK, GETDATE()  
	FROM #ZnPA 
	WHERE PRODUCT_STRUCTURE_FK IS NULL 
		OR ZONE_ENTITY_STRUCTURE_FK IS NULL

--Add Missing Associations
	Insert into epacube.epacube.PRODUCT_ASSOCIATION
	(data_name_fk, product_structure_fk, org_entity_structure_fk, ENTITY_CLASS_CR_FK, ENTITY_STRUCTURE_FK, RECORD_STATUS_CR_FK, CREATE_TIMESTAMP, UPDATE_TIMESTAMP)
	Select 159450 'Data_Name_FK', zn.PRODUCT_STRUCTURE_FK, 1 'org_entity_structure_fk', 10117 'ENTITY_CLASS_CR_FK', zn.zone_entity_structure_fk, 1 'RECORD_STATUS_CR_FK'
	, zn.CREATE_TIMESTAMP, zn.UPDATE_TIMESTAMP
	from #ZnPA Zn
	left join epacube.epacube.product_association pa with (nolock) on zn.zone_entity_structure_fk = pa.ENTITY_STRUCTURE_FK and zn.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and pa.data_name_fk = 159450
	where pa.PRODUCT_ASSOCIATION_ID is null
	and zn.PRODUCT_STRUCTURE_FK is not null
	and zn.ZONE_ENTITY_STRUCTURE_FK is not null
	group by zn.ZONE_ENTITY_STRUCTURE_FK, zn.PRODUCT_STRUCTURE_FK, zn.CREATE_TIMESTAMP, zn.UPDATE_TIMESTAMP

--Set inactivation date where items have been decided to be removed from zones
	update pa
	set END_DATE = zn.End_date
			from epacube.epacube.product_association pa
		inner join #ZnPA Zn on pa.PRODUCT_STRUCTURE_FK = zn.PRODUCT_STRUCTURE_FK and pa.ENTITY_STRUCTURE_FK = zn.zone_entity_structure_fk
		where 1 = 1
		and pa.DATA_NAME_FK = 159450
		and isNULL(zn.End_date,'0') <> '0'
 
	If Object_ID('tempdb..#ZnPA') is not null
	drop table #ZnPA

	If Object_ID('tempdb..#lg') is not null
	drop table #lg

--Inactivate items from zone once inactivation date is achieved.
	update epacube.epacube.product_association 
	set RECORD_STATUS_CR_FK = 2
	, UPDATE_TIMESTAMP = getdate()
	where 1 = 1
	and data_name_fk = 159450
	and isnull(RECORD_STATUS_CR_FK, 1) = 1
	and cast(END_DATE as date) > cast(getdate() as date)

-------------------------------------

--Need to load data_values and links with following code

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, JOB_FK)
	select 
	'IMPORTS.[IMPORT_URM_ZONE_ITEM]' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE_ITEM' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.PRODUCT_MULT_TYPE' 'TARGET_TABLE'
	, 'NEW ALIAS ITEMS' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM 
		(
			Select Product_Structure_FK, Data_Name_FK, Data_Value_FK, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Record_Status_CR_FK, Update_User, Create_User, Create_Timestamp, Update_Timestamp
			from (
			Select zn.product_structure_fk
			, 500019 data_name_fk, dv.data_value_id 'data_value_fk'
			, 1 'Org_Entity_Structure_FK'
			, zn.Zone_Entity_Structure_FK
			, 1 'Record_Status_CR_FK'
			, Case when isnull(zn.usr_upd, '') <> '' then zn.usr_upd end 'Update_User'
			, Case when isnull(zn.usr_crt, '') <> '' then zn.usr_crt end 'Create_User'
			, Dense_Rank()over(partition by zn.zone_num, zn.item_nbr, zn.item_type order by isnull(epacube.[URM_DATE_TIME_CONVERSION](DTE_UPD, TME_UPD), epacube.[URM_DATE_TIME_CONVERSION](DTE_CRT, TME_CRT)) desc) 'DRank_URM_effective'
			, isnull(epacube.[URM_DATE_TIME_CONVERSION](DTE_UPD, TME_UPD), epacube.[URM_DATE_TIME_CONVERSION](DTE_CRT, TME_CRT)) 'CREATE_TIMESTAMP'
			, epacube.[URM_DATE_TIME_CONVERSION](DTE_UPD, TME_UPD) 'UPDATE_TIMESTAMP'
			from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] ZN
			inner join epacube.epacube.data_value dv with (nolock) on zn.link = dv.value and dv.data_name_fk = 500019
			left join epacube.epacube.product_mult_type pmt with (nolock) on dv.data_value_id = pmt.data_value_fk and zn.ZONE_ENTITY_STRUCTURE_FK = pmt.entity_structure_fk
					and ZN.product_structure_fk = pmt.product_structure_fk and pmt.data_name_fk = 500019
			where 1 = 1
			and pmt.product_mult_type_id is null
			and zn.link <> 0
			) a where 1 = 1
					and DRank_URM_effective = 1
		) A)  'RECORDS_LOADED'
	, @Job_FK

--Remove item where alias has changed so it can be reloaded below.
	Delete PMT
	from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] ZN
	inner join epacube.epacube.data_value dv with (nolock) on zn.link = dv.value and dv.data_name_fk = 500019
	inner join epacube.epacube.product_mult_type pmt with (nolock) on ZN.ZONE_ENTITY_STRUCTURE_FK = pmt.entity_structure_fk and ZN.product_structure_fk = pmt.product_structure_fk and pmt.data_name_fk = 500019
	where 1 = 1
	and pmt.data_value_fk <> dv.data_value_id
	and zn.link <> 0
	and zn.job_FK = @Job_FK

--Remove items no longer in Alias
	Delete PMT
	from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] ZN
	inner join epacube.epacube.product_mult_type pmt with (nolock) on ZN.ZONE_ENTITY_STRUCTURE_FK = pmt.entity_structure_fk and ZN.product_structure_fk = pmt.product_structure_fk and pmt.data_name_fk = 500019
	where 1 = 1
	and zn.link = 0
	and zn.job_FK = @Job_FK

--Add New Aliases
	Insert into epacube.epacube.product_mult_type 
	(Product_Structure_FK, Data_Name_FK, Data_Value_FK, Org_Entity_Structure_FK, Entity_Structure_FK, Record_Status_CR_FK, Update_User, Create_User, Create_Timestamp, Update_Timestamp, Job_FK)
	Select Product_Structure_FK, Data_Name_FK, Data_Value_FK, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Record_Status_CR_FK, Update_User, Create_User, Create_Timestamp, Update_Timestamp
	, Job_FK
	from (
	Select zn.product_structure_fk
	, 500019 data_name_fk, dv.data_value_id 'data_value_fk'
	, 1 'Org_Entity_Structure_FK'
	, zn.Zone_Entity_Structure_FK
	, 1 'Record_Status_CR_FK'
	, Case when isnull(zn.usr_upd, '') <> '' then zn.usr_upd end 'Update_User'
	, Case when isnull(zn.usr_crt, '') <> '' then zn.usr_crt end 'Create_User'
	, Dense_Rank()over(partition by zn.zone_num, zn.item_nbr, zn.item_type order by isnull(epacube.[URM_DATE_TIME_CONVERSION](DTE_UPD, TME_UPD), epacube.[URM_DATE_TIME_CONVERSION](DTE_CRT, TME_CRT)) desc) 'DRank_URM_effective'
	, isnull(epacube.[URM_DATE_TIME_CONVERSION](DTE_UPD, TME_UPD), epacube.[URM_DATE_TIME_CONVERSION](DTE_CRT, TME_CRT)) 'CREATE_TIMESTAMP'
	, epacube.[URM_DATE_TIME_CONVERSION](DTE_UPD, TME_UPD) 'UPDATE_TIMESTAMP'
	, zn.Job_FK
	from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] ZN
	inner join epacube.epacube.data_value dv with (nolock) on zn.link = dv.value and dv.data_name_fk = 500019
	left join epacube.epacube.product_mult_type pmt with (nolock) on dv.data_value_id = pmt.data_value_fk and zn.ZONE_ENTITY_STRUCTURE_FK = pmt.entity_structure_fk
			and ZN.product_structure_fk = pmt.product_structure_fk and pmt.data_name_fk = 500019
	where 1 = 1
	and pmt.product_mult_type_id is null
	and zn.link <> 0
	and zn.job_FK = @Job_FK
	) a where 1 = 1
			and DRank_URM_effective = 1

--Load Item Level Target Margins

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, Job_FK)
	select 
	'IMPORTS.[IMPORT_URM_ZONE_ITEM]' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE_ITEM' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.SEGMENTS_SETTINGS' 'TARGET_TABLE'
	, 'ZONE ITEM TARGET MARGINS' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM 
		(
			Select 
			1 ITM
			from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] ZN
 
				left join 
					(select * from (
									Select ss.ATTRIBUTE_NUMBER, sp.product_structure_fk, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
									, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
									from epacube.epacube.segments_settings ss with (nolock) 
									inner join epacube.epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
									inner join [customer_imports].[IMPORTS].[URM_ZONE_ITEM] UZI with (nolock) on ss.zone_segment_fk = uzi.zone_entity_structure_fk and sp.PRODUCT_STRUCTURE_FK = UZI.product_structure_fk
									where ss.data_name_fk = 502046
									and ss.RECORD_STATUS_CR_FK = 1
									and uzi.Job_FK = @Job_FK
									) a where Drank = 1
					) sz_502046 on ZN.Product_structure_FK = sz_502046.product_structure_fk and ZN.Zone_entity_structure_fk = sz_502046.zone_segment_fk
			Where cast(zn.TM_pct as Numeric(18, 4)) / 100 <> isnull(sz_502046.attribute_number, 0) and cast(zn.TM_pct as Numeric(18, 4)) <> 0
			and zn.job_FK = @Job_FK
		) A)  'RECORDS_LOADED'
	, @Job_FK 'Job_FK'

	If object_id('tempdb..#zn_tm') is not null
	drop table #zn_tm

	Select cast(1 as bigint) 'Org_Entity_Structure_FK'
	, Zone_Entity_Structure_FK
	, cast(502046 as bigint) 'Data_Name_FK'
	, cast(cast(zn.TM_pct as Numeric(18, 4)) / 100 as Numeric(18, 4)) 'TM_PCT'
	, cast(5 as int) 'Precedence'
	, ZN.product_structure_fk 'Prod_Segment_FK'
	, ZN.Zone_entity_structure_fk 'Zone_Segment_FK'
	, ZN.product_structure_FK
	, cast(dateadd(d, -1, import_timestamp) as date) 'Effective_Date'
	, cast(151000 as bigint) 'Entity_Data_Name_FK'
	, cast(110103 as bigint) 'Prod_Segment_Data_Name_Fk'
	, zn.Import_Timestamp
	, 'DAILY UPDATE' 'DATA_SOURCE'
	into #zn_tm
	from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] ZN
	--left join epacube.precision.entities_live el on zn.ZONE_ENTITY_STRUCTURE_FK = el.entity_structure_fk
	where job_fk = @job_fk
	and cast(zn.TM_pct as Numeric(18, 4)) <> 0
	--and el.entities_live_live is null

	create index idx_01 on #zn_tm(product_structure_fk, zone_entity_structure_fk)


	If object_id('tempdb..#CurVals') is not null
	drop table #CurVals

	--Get Current_Values
	Select *
	into #CurVals
	from (
	Select ss.ATTRIBUTE_NUMBER 'TM_CUR', sp.product_structure_fk, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
	, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
	from epacube.epacube.segments_settings ss with (nolock) 
	inner join epacube.epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
	inner join #zn_tm UZI with (nolock) on ss.zone_segment_fk = uzi.zone_entity_structure_fk and sp.PRODUCT_STRUCTURE_FK = UZI.product_structure_fk
	where ss.data_name_fk = 502046
	and ss.RECORD_STATUS_CR_FK = 1
	and ss.effective_date <= cast(uzi.import_timestamp as date)
	) a where Drank = 1

	create index idx_02 on #CurVals(product_structure_fk, zone_segment_fk)

	Insert into epacube.epacube.segments_settings
	(Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Data_Name_FK, Attribute_Number, Attribute_Event_Data, Precedence, Record_Status_CR_FK, Prod_Segment_FK, Zone_Segment_FK
	, Product_STructure_FK, Effective_Date, Entity_Data_Name_FK, Prod_Segment_Data_Name_Fk, Import_Timestamp, create_timestamp, update_timestamp, [DATA_SOURCE])
	Select 
	zn.Org_Entity_Structure_FK
	, zn.Zone_Entity_Structure_FK
	, zn.Data_Name_FK
	, zn.TM_PCT 'Attribute_Number'
	, zn.TM_PCT 'Attribute_Event_Data'
	, zn.Precedence
	, 1 'Record_Status_CR_FK'
	, zn.Prod_Segment_FK
	, zn.Zone_Segment_FK
	, zn.product_structure_FK
	, zn.Effective_Date
	, zn.Entity_Data_Name_FK
	, zn.Prod_Segment_Data_Name_Fk
	, zn.Import_Timestamp
	, dateadd(d, -1, zn.Import_Timestamp) 'create_timestamp'
	, dateadd(d, -1, zn.Import_Timestamp) 'update_timestamp'
	, zn.DATA_SOURCE
	 from #zn_tm zn
	 left join #CurVals cv on zn.PRODUCT_STRUCTURE_FK = cv.PRODUCT_STRUCTURE_FK and zn.ZONE_ENTITY_STRUCTURE_FK = cv.ZONE_SEGMENT_FK and zn.TM_PCT = cv.tm_cur
	 where cv.drank is null

--Load Item_Level_Level_Gross
	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, Job_FK)
	select 
	'IMPORTS.[IMPORT_URM_ZONE_ITEM]' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE_ITEM' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.SEGMENTS_SETTINGS' 'TARGET_TABLE'
	, 'ZONE LEVEL GROSS SETTINGS' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM 
		(
			Select 
			1 ITM
			from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] ZN
 
 					left join 
						(select * from (
										Select distinct ss.ATTRIBUTE_Y_N, sp.product_structure_fk, ss.zone_segment_fk
										, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
										from epacube.epacube.segments_settings ss with (nolock) 
										inner join epacube.epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
										inner join [customer_imports].[IMPORTS].[URM_ZONE_ITEM] UZI with (nolock) on ss.zone_segment_fk = uzi.zone_entity_structure_fk and sp.PRODUCT_STRUCTURE_FK = UZI.product_structure_fk
										where ss.data_name_fk = 500015
										and ss.RECORD_STATUS_CR_FK = 1
										and uzi.job_fk = @Job_FK
										) a where Drank = 1
						) sz_500015 on ZN.product_structure_fk = sz_500015.product_structure_fk and sz_500015.ZONE_SEGMENT_FK = ZN.Zone_entity_structure_fk
			Where isnull(zn.lg_flg, 'N') <> isnull(sz_500015.Attribute_Y_N, 'N')
		) A)  'RECORDS_LOADED'
	, @Job_FK 'Job_FK'


	select * 
	into #lg 
	from (
			Select distinct ss.ATTRIBUTE_Y_N, sp.product_structure_fk, ss.zone_segment_fk
			, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
			, uzi.job_fk
			from epacube.epacube.segments_settings ss with (nolock) 
			inner join epacube.epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
			inner join [customer_imports].[IMPORTS].[URM_ZONE_ITEM] UZI with (nolock) on ss.zone_segment_fk = uzi.zone_entity_structure_fk and sp.PRODUCT_STRUCTURE_FK = UZI.product_structure_fk
			where ss.data_name_fk = 500015
			and uzi.job_FK = @job_fk
			and ss.RECORD_STATUS_CR_FK = 1
			) a where Drank = 1

	create index idx_01 on #lg(product_structure_fk, zone_segment_fk, job_fk)

	Insert into epacube.epacube.segments_settings
	(Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Data_Name_FK, ATTRIBUTE_Y_N, Attribute_Event_Data, Precedence, Record_Status_CR_FK, Prod_Segment_FK, Zone_Segment_FK, Product_STructure_FK, Effective_Date, Entity_Data_Name_FK, Prod_Segment_Data_Name_Fk, Import_Timestamp, [DATA_SOURCE])
	Select
	1 'Org_Entity_Structure_FK'
	, zn.zone_entity_structure_fk 'Zone_Entity_Structure_FK'
	, 500015 'Data_Name_FK'
	, zn.lg_flg 'Attribute_Y_N'
	, zn.lg_flg 'Attribute_Event_Data'
	, 5 'Precedence', 1 'Record_Status_CR_FK'
	, ZN.product_structure_fk 'Prod_Segment_FK'
	, zn.zone_entity_structure_fk 'Zone_Segment_FK'
	, zn.product_structure_FK
	, isnull(epacube.[URM_DATE_CONVERSION](DTE_UPD), epacube.[URM_DATE_CONVERSION](zn.DTE_CRT)) 'Effective_Date'
	, 151000 'Entity_Data_Name_FK'
	, 110103 'Prod_Segment_Data_Name_Fk'
	, zn.Import_Timestamp
	, 'DAILY UPDATE' 'DATA_SOURCE'
	from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] ZN
 
 			left join #lg sz_500015 on ZN.product_structure_fk = sz_500015.product_structure_fk and sz_500015.ZONE_SEGMENT_FK = ZN.Zone_entity_structure_fk and zn.job_fk = sz_500015.Job_FK
				--(select * from (
				--				Select distinct ss.ATTRIBUTE_Y_N, sp.product_structure_fk, ss.zone_segment_fk
				--				, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
				--				, uzi.job_fk
				--				from epacube.epacube.segments_settings ss with (nolock) 
				--				inner join epacube.epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
				--				inner join [customer_imports].[IMPORTS].[URM_ZONE_ITEM] UZI with (nolock) on ss.zone_segment_fk = uzi.zone_entity_structure_fk and sp.PRODUCT_STRUCTURE_FK = UZI.product_structure_fk
				--				where ss.data_name_fk = 500015
				--				and uzi.job_FK = @Job_FK
				--				and ss.RECORD_STATUS_CR_FK = 1
				--				) a where Drank = 1
				--) sz_500015 on ZN.product_structure_fk = sz_500015.product_structure_fk and sz_500015.ZONE_SEGMENT_FK = ZN.Zone_entity_structure_fk and zn.job_fk = sz_500015.Job_FK
	Where 1 = 1
	and isnull(zn.lg_flg, 'N') <> isnull(sz_500015.Attribute_Y_N, 'N')
	and zn.job_FK = @Job_FK

-- ---- Clear up any possibility of duplicated data
--*** NOTE  Change Min SSID to Max SSID if ever reinstating and add join to tm_pct

	--delete ss
	--from epacube.epacube.segments_settings ss
	--inner join
	--(
	--	select min(segments_settings_id) 'min_segments_settings_id', data_name_fk, effective_date, isnull(prod_segment_data_name_fk, 0) 'prod_segment_data_name_fk', isnull(prod_segment_fk, 0) 'prod_segment_fk', zone_segment_fk
	--	, isnull(product_association_fk, 0) 'product_association_fk', isnull(lower_limit, 0) 'lower_limit', isnull(upper_limit, 0) 'upper_limit', count(*) recs
	--	from epacube.epacube.segments_settings with (nolock)
	--	group by data_name_fk, effective_date, isnull(prod_segment_data_name_fk, 0), isnull(prod_segment_fk, 0), zone_segment_fk, isnull(product_association_fk, 0), isnull(lower_limit, 0), isnull(upper_limit, 0)
	--	having count(*) > 1
	--) A on ss.data_name_fk = a.data_name_fk
	--	and ss.effective_date = a.Effective_Date
	--	and isnull(ss.prod_segment_data_name_fk, 0) = isnull(a.prod_segment_data_name_fk, 0)
	--	and isnull(ss.prod_segment_fk, 0) = isnull(a.prod_segment_fk, 0)
	--	and isnull(ss.product_association_fk, 0)  = isnull(a.product_association_fk, 0)
	--	and isnull(ss.lower_limit, 0) = isnull(a.lower_limit, 0)
	--	and isnull(ss.upper_limit, 0) = isnull(a.upper_limit, 0)
	--	and ss.ZONE_SEGMENT_FK = a.ZONE_SEGMENT_FK
	--where ss.segments_settings_id <> a.min_segments_settings_id


--------------------------------------------------------------------------------------------------

  --    SELECT @v_input_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk;

  --    SELECT @v_output_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk
		--AND PRODUCT_STRUCTURE_FK is NOT NULL ;

	 -- SET @v_exception_rows = @v_input_rows - @v_output_rows;
  --    SET @V_END_TIMESTAMP = getdate()
  --    EXEC common.job_execution_create  @in_job_fk, 'IMPORT_COMPLETE',
		--                                 @v_input_rows, @v_output_rows, @v_exception_rows,
		--								 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;

 SET @status_desc =  'finished execution of IMPORT Zone Item'
 
 --Exec [AMAZONA-8NUFTI5].common.exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;

 SET @import_exec_no = @l_import_exec_no

END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT ZONE Item Data has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         --EXEC [AMAZONA-8NUFTI5].common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			--EXEC [AMAZONA-8NUFTI5].common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;

END





