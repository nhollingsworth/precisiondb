﻿






-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        04/27/2018   Initial Version 


CREATE PROCEDURE [IMPORTS].[IMPORT_URM_ITEM_NEW] @Job_FK bigint
AS 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/
--Declare @Job_FK bigint = 1435


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint
	 DECLARE @IN_JOB_FK bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
     -- EXEC [AMAZONA-8NUFTI5].common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.URM_ITEM_NEW', 
					--							@exec_id = @l_exec_no OUTPUT,
					--							@epa_job_id = @in_job_fk;

     --SET @l_sysdate = GETDATE() --used for the beginning timestamp
     --SET @V_START_TIMESTAMP = getdate()  -- used in steps



------------------------------------------------------------------------------------------------
----The ATTFINFLG (Attribute Finished Flag) is a Y/N status flag that is set by our purchasing department.
---- We only show New items that have that flag set to a ‘Y’.  
---- The PRCFINFLG (Pricing Finished Flag) is also a Y/N flag, 
---- it is set to ‘Y’ when the item is marked as ‘Not for sale’ or when the item is priced. 
---- Once that flag is set to ‘Y’ we no longer display the item in the New Item Availability screen. 
---- If an item is flagged as a Non- priced item then we delete it from the Item New file. 
----500213	URM_ATTRIBUTE_FINISHED_FLAG
----500214	URM_PRICING_FINISHED_FLAG

---Procedure needs to Add flags and update flags and remove if in epacube,  but not in file.
----------------------------------------------------------------------------------------------------
--select * from [IMPORTS].[URM_ITEM_NEW]

If Object_id('tempdb..#new') is not null
drop table #new

Select distinct
[ITEM_SURR], [ITEM_NBR], [ITEM_TYPE], [ATTFINFLG], [PRCFINFLG], [DTE_CRT], [ITMSURRPC], [LVLID_ITM], [CLSID_ITM], [PRODUCT_STRUCTURE_FK], [IMPORT_TIMESTAMP], [Job_FK]
into #New from [IMPORTS].[URM_ITEM_NEW] where job_FK = @Job_FK

--Select distinct * into #New from [IMPORTS].[URM_ITEM_NEW] where job_FK = @Job_FK

create index idx_01 on #New(ITEM_NBR, ITEM_TYPE)


--If object_id('tempdb..dbo.new_items') is not null
--drop table dbo.new_items

--Select *, getdate() 'Import_Test'
--into dbo.new_items
--from #New

--goto eof

UPDATE RD
set product_structure_fk = PI.product_structure_fk
from #New RD
inner join epacube.epacube.PRODUCT_IDENTIFICATION PI on pi.value = RD.ITEM_SURR
And pi.data_name_fk = 110103

UPDATE RD
set product_structure_fk = PI.product_structure_fk
from #New RD
inner join epacube.epacube.PRODUCT_IDENTIFICATION PI on pi.value = RD.ITEM_NBR + RD.ITEM_TYPE
And pi.data_name_fk = 110100
and rd.product_structure_fk is NULL

-------------------------------------------------------------------------------------
--select * from epacube.epacube.data_value where data_name_fk = 500010


--select dv.description, rd.* 
--from  #New RD
--inner join epacube.epacube.PRODUCT_status Ps on ps.product_structure_fk = rd.product_structure_fk
--inner join epacube.epacube.data_value dv on dv.data_value_id = ps.data_value_fk 
--where rd.item_type = 'W' 
-- AND dv.description = 'New'


----Status = 1 (New)	Type = W		ATTFINFLG = Y	PRCFINFLG = Y			11: Not for Sale
 
 --select dv.description, rd.* 

update ps
set data_value_fk = (select data_value_id from epacube.epacube.data_value with (nolock) where data_name_fk = 500010 and value = '11')
from  #New RD
inner join epacube.epacube.PRODUCT_status Ps on ps.product_structure_fk = rd.product_structure_fk
inner join epacube.epacube.data_value dv on dv.data_value_id = ps.data_value_fk 
where rd.item_type = 'W' 
and rd.attfinflg = 'Y'
and rd.prcfinflg = 'Y'
AND dv.description = 'New'

----Status = 1 (New)	Type = W		ATTFINFLG = Y	PRCFINFLG <> Y	Does not exist for any zone***		1: New

--Nothing to do here as the status is correct.

--select dv.description, rd.* 
--from  #New RD
--inner join epacube.epacube.PRODUCT_status Ps on ps.product_structure_fk = rd.product_structure_fk
--inner join epacube.epacube.data_value dv on dv.data_value_id = ps.data_value_fk 
--left join epacube.marginmgr.pricesheet_retails pr on pr.product_structure_fk = ps.product_structure_fk and pr.cust_entity_structure_fk is NULL and zone_segment_fk is not NULL
--where rd.item_type = 'W' 
--and rd.attfinflg = 'Y'
--and isNULL(rd.prcfinflg, 'N') = 'N'
--AND dv.description = 'New'
--AND pr.pricesheet_retails_id is NULL


----Status = 1 (New)	Type = W		ATTFINFLG = Y	PRCFINFLG <> Y	Exists^^^		10: New, Don't Display

----select dv.description, rd.* 

update ps
set data_value_fk = (select data_value_id from epacube.epacube.data_value with (nolock) where data_name_fk = 500010 and value = '10')
from  #New RD
inner join epacube.epacube.PRODUCT_status Ps on ps.product_structure_fk = rd.product_structure_fk
inner join epacube.epacube.data_value dv on dv.data_value_id = ps.data_value_fk 
inner join epacube.marginmgr.pricesheet_retails pr on pr.product_structure_fk = ps.product_structure_fk and pr.cust_entity_structure_fk is NULL and zone_entity_structure_fk is not NULL
where rd.item_type = 'W' 
and rd.attfinflg = 'Y'
and isNULL(rd.prcfinflg, 'N') = 'N'
AND dv.description = 'New'
AND pr.pricesheet_retails_id is NULL

----------------------------------------------------------------------------------------------------------	 

------remove flags if product not in the file anymore yet in epacube product attributes.
------want to only do if data in import table.
---------------------------------------------------------------------------------------------------------
----DECLARE @v_check int

----SET @v_check = (select count(1) from Imports.URM_ITEM_NEW)

----IF @v_check > 2

----BEGIN 
----update epacube.epacube.product_attribute
----set RECORD_STATUS_CR_FK = 2
----where PRODUCT_ATTRIBUTE_ID in (
----select PRODUCT_ATTRIBUTE_ID
----from epacube.epacube.PRODUCT_ATTRIBUTE 
----where DATA_NAME_FK in (500214, 500213)
----and PRODUCT_STRUCTURE_FK not in (
----select product_structure_fk from 
----Imports.URM_ITEM_NEW ))

----END 
--------------------------------------------------------------------------------------------------
----create any product associations coming in on the item new file (also created from Item file)
----URM_ITEM_SURROGATE_REPLACE

If object_id('precision.daily_import_log') is not null
Insert into PRECISION.DAILY_IMPORT_LOG
([CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [PROCEDURE_NAME], [TYPE_OF_DATA], RECORDS_LOADED, JOB_FK)
select getdate() 'Create_timestamp'
, 'IMPORTS.URM_ITEM_NEW' 'SOURCE_TABLE'
, 'EPACUBE.EPACUBE.PRODUCT_ASSOCIATION' 'TARGET_TABLE'
, 'IMPORTS.IMPORT_URM_ITEM_NEW' 'PROCEDURE_NAME'
, 'NEW ITEM_SURROGATE ASSOCIATIONS' 'TYPE_OF_DATA'
, (SELECT COUNT(*) FROM #New N
inner join epacube.epacube.product_identification child on child.value = n.itmsurrpc and child.data_name_fk =110103
where isNULL(n.itmsurrpc,'0') <> '0' 
and n.product_structure_fk  in (
	select distinct product_structure_fk from #New where product_structure_fk
	not in (select product_structure_fk from epacube.epacube.product_association where data_name_fk = 500023 ) 
	and itmsurrpc <> '0')) 'RECORDS_LOADED'
, @Job_FK 'Job_FK'

INSERT INTO epacube.[PRODUCT_ASSOCIATION] ([DATA_NAME_FK],[PRODUCT_STRUCTURE_FK],[ORG_ENTITY_STRUCTURE_FK],[ENTITY_CLASS_CR_FK] ,[CHILD_PRODUCT_STRUCTURE_FK],[RECORD_STATUS_CR_FK]
,[CREATE_TIMESTAMP],[CREATE_USER], Job_FK) 
select distinct 500023, n.product_structure_fk parent, 1, 10109,child.product_structure_fk child,1 , getdate(), 'URM_ITEM_NEW', @Job_FK 'Job_FK' 
   
from  #New N
inner join epacube.epacube.product_identification child on child.value = n.itmsurrpc and child.data_name_fk =110103
where isNULL(n.itmsurrpc,'0') <> '0' 
and n.product_structure_fk  in (
 select distinct product_structure_fk from #New where product_structure_fk
  not in (select product_structure_fk from epacube.epacube.product_association where data_name_fk = 500023 ) 
  and itmsurrpc <> '0')




----  INSERT INTO epacube.[PRODUCT_ASSOCIATION] ([DATA_NAME_FK],[PRODUCT_STRUCTURE_FK],[ORG_ENTITY_STRUCTURE_FK],[ENTITY_CLASS_CR_FK] ,[CHILD_PRODUCT_STRUCTURE_FK],[RECORD_STATUS_CR_FK]
----,[CREATE_TIMESTAMP],[CREATE_USER]) 
----select distinct 500023, n.product_structure_fk parent, 1, 10109,pi2.product_structure_fk child,1 , getdate(), 'URM_NEW_ITEM_IMPORT'
----  --  select pi.value, pd.description, pi2.value, pd2.DESCRIPTION , pa.*
----   from [IMPORTS].[URM_ITEM_NEW]  n
----inner join epacube.epacube.product_identification pi 
----on pi.product_structure_fk = n.product_structure_fk and pi.DATA_NAME_FK = 110100 and n.PRODUCT_STRUCTURE_FK is not NULL
----inner join epacube.epacube.PRODUCT_DESCRIPTION pd on pd.product_structure_fk = pi.product_structure_fk and pd.data_name_fk = 110401
----inner join epacube.epacube.product_identification pi2 
----on pi2.value = n.ITMSURRPC and pi2.DATA_NAME_FK = 110103 and isNULL(n.ITMSURRPC,'0') <> '0'
----inner join epacube.epacube.PRODUCT_DESCRIPTION pd2 on pd2.product_structure_fk = pi2.product_structure_fk and pd2.data_name_fk = 110401
----left join epacube.epacube.PRODUCT_ASSOCIATION pa on pa.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK and pa.CHILD_PRODUCT_STRUCTURE_FK = pi2.PRODUCT_STRUCTURE_FK
----and pa.DATA_NAME_FK = 500023
----and pa.PRODUCT_ASSOCIATION_ID is null
----and n.PRODUCT_STRUCTURE_FK is not NULL
----and pi2.PRODUCT_STRUCTURE_FK is not NULL

--delete from epacube.epacube.product_association   where data_name_fk =  500023
 ----------------------------------------------------------------------------------------------

  --    SELECT @v_input_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk;

  --    SELECT @v_output_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk
		--AND PRODUCT_STRUCTURE_FK is NOT NULL ;

	 -- SET @v_exception_rows = @v_input_rows - @v_output_rows;
  --    SET @V_END_TIMESTAMP = getdate()
  --    EXEC common.job_execution_create  @in_job_fk, 'IMPORT_COMPLETE',
		--                                 @v_input_rows, @v_output_rows, @v_exception_rows,
		--								 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



 SET @status_desc =  'finished execution of IMPORT ITEM NEW'
 
 --Exec [AMAZONA-8NUFTI5].common.exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT ITEM NEW Data has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         --EXEC [AMAZONA-8NUFTI5].common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			--EXEC [AMAZONA-8NUFTI5].common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;

--	eof:

