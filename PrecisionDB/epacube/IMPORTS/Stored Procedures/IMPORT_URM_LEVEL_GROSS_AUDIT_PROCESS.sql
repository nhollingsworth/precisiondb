﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [IMPORTS].[IMPORT_URM_LEVEL_GROSS_AUDIT_PROCESS] @Job_FK bigint
AS

	SET NOCOUNT ON;



		Update lga
		set PRODUCT_STRUCTURE_FK = pi.product_structure_fk
		, zone_entity_structure_fk = ei.entity_structure_fk
		from imports.urm_level_gross_audit lga with (nolock) 
		inner join epacube.epacube.product_identification pi with (nolock) on lga.item = pi.value and pi.DATA_NAME_FK = 110100
		inner join epacube.epacube.entity_identification ei with (nolock) on lga.ZONE_NUM = ei.value and ei.ENTITY_DATA_NAME_FK = 151000 and ei.DATA_NAME_FK = 151110
		where lga.PRODUCT_STRUCTURE_FK <> pi.PRODUCT_STRUCTURE_FK or lga.zone_entity_structure_fk <> ei.ENTITY_STRUCTURE_FK

		If (Select count(*) from [IMPORTS].[URM_LEVEL_GROSS_ZONE]
			where isnull(job_fk, 0) = @Job_FK and epacube.[URM_DATE_CONVERSION](Eff_Date) =  Cast(GETDATE() + 6 - (0 * 7) - DATEPART(DW, GETDATE()) as date)) = 0
		goto eop

	--If (Select count(*) from [IMPORTS].[URM_LEVEL_GROSS_ZONE] where isnull(job_FK, 0) in (@Job_FK, 0)) > 0
	--	Delete from IMPORTS.URM_LEVEL_GROSS_AUDIT where effective_date = (select distinct epacube.[URM_DATE_CONVERSION](Eff_Date) from [IMPORTS].[URM_LEVEL_GROSS_ZONE])

--add daily logging
		If object_id('precision.daily_import_log') is not null
		Insert into PRECISION.DAILY_IMPORT_LOG
		([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED)
		select 
		'IMPORTS.IMPORT_URM_LEVEL_GROSS_AUDIT_PROCESS' 'PROCEDURE_NAME'
		, getdate() 'Create_timestamp'
		, 'IMPORTS.URM_LEVEL_GROSS_ZONE' 'SOURCE_TABLE'
		, 'IMPORTS.URM_LEVEL_GROSS_AUDIT' 'TARGET_TABLE'
		, 'NEW DATA CLASS DATA_VALUES' 'TYPE_OF_DATA'
		, (SELECT COUNT(*) FROM (
			Select epacube.[URM_DATE_CONVERSION](z.Eff_Date) 'Effective_Date'
			, pi.PRODUCT_STRUCTURE_FK, eiz.ENTITY_STRUCTURE_FK 'zone_entity_structure_fk'
			, pi.value 'Item'
			, pip.value 'Item_Parity'
			, epacube.[URM_DATE_CONVERSION](z.newcsteff) 'New_Cost_Effective_Date'
			, dv.value 'Alias'
			, dv.DATA_VALUE_ID 'Alias_Data_Value_FK'
			, Cast(z.tm_pct / 100.00 as Numeric(18, 4)) 'TM_PCT_CUR'
			, Cast(z.EXTGMPCT / 100.00 as Numeric(18, 4)) 'GM_PCT_CUR'
			, Cast(z.newgmpct / 100.00 as Numeric(18, 4)) 'GM_PCT_NEW'
			--, Case	when uc.uom_code = 'RWC'	then Round(cast((Cast(z.extcstcas as money) / ((isnull(puc.QUANTITY_MULTIPLE, 1) * cast(pa.ATTRIBUTE_NUMBER as numeric(18, 2))) / 16)) as money) + 0.0049, 2)
			--								else Round(Cast(z.extcstcas / pa.ATTRIBUTE_NUMBER as money) + 0.0049, 2) 
			--		end 'Unit_Cost_Cur'
			--, Case	when uc.uom_code = 'RWC'	then Round(cast((Cast(z.NEWCSTCAS as money) / ((isnull(puc.QUANTITY_MULTIPLE, 1) * cast(pa.ATTRIBUTE_NUMBER as numeric(18, 2))) / 16)) as money) + 0.0049, 2)
			--								else Round(Cast(z.NEWCSTCAS / pa.ATTRIBUTE_NUMBER as money) + 0.0049, 2) 
			--		end 'Unit_Cost_New'
			, Case	when uc.uom_code = 'RWC'	then Round(cast((Cast(z.extcstcas as money) / ((isnull(puc.QUANTITY_MULTIPLE, 1) * cast(pa.ATTRIBUTE_NUMBER as numeric(18, 2))) / 16)) as money), 2)
											else Round(Cast(z.extcstcas / pa.ATTRIBUTE_NUMBER as money), 2) 
					end 'Unit_Cost_Cur'
			, Case	when uc.uom_code = 'RWC'	then Round(cast((Cast(z.NEWCSTCAS as money) / ((isnull(puc.QUANTITY_MULTIPLE, 1) * cast(pa.ATTRIBUTE_NUMBER as numeric(18, 2))) / 16)) as money), 2)
											else Round(Cast(z.NEWCSTCAS / pa.ATTRIBUTE_NUMBER as money), 2) 
					end 'Unit_Cost_New'

			, Z.[ZONE_NUM], Z.[ITEM_NBR], Z.[ITEM_TYPE], Z.[EFF_DATE], Z.[NEWCSTCAS], Z.[PRC_MULT], Z.[PRICE], Z.[NEWPRCMLT], Z.[NEW_PRC], Z.[EXTGMPCT], Z.[TM_PCT], Z.[OVRFLG], Z.[NEWTMPCT], Z.[NEWGMPCT], Z.[EXTCSTCAS], Z.[CLSIDLNIT], Z.[CLSID_ITM], Z.[QUAL_FLG], Z.[HOLD_FLG], Z.[PRT_FLG], Z.[RESCINDED], Z.[NEWCSTEFF], Z.[LINK], Z.[LVLID_PAR], Z.[CLSPARITM], Z.[ITMPARTYP], Z.[PARITEM], Z.[PARITMTYP], Z.[SEQ_NUM6], Z.[DTE_CRT], Z.[TME_CRT], Z.[USR_CRT], Z.[DTE_UPD], Z.[TME_UPD], Z.[USR_UPD], Z.[IMPORT_TIMESTAMP]
			, cast(getdate() as date) 'Create_Timestamp'
			, z.Job_FK
			from [IMPORTS].[URM_LEVEL_GROSS_ZONE] Z with (nolock)
			left join epacube.epacube.PRODUCT_IDENTIFICATION pi with (nolock) on z.item_nbr + z.Item_type = pi.value and pi.data_name_fk = 110100
			left join epacube.epacube.ENTITY_IDENTIFICATION eiz with (nolock) on z.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000
			left join epacube.epacube.PRODUCT_IDENTIFICATION pip with (nolock) on cast(z.paritem as varchar(16)) + cast(z.paritmtyp as varchar(16)) = pip.value and pip.data_name_fk = 110100 and isnull(z.paritem, '0') <> '0'
			left join epacube.epacube.data_value dv with (nolock) on z.link = dv.value and dv.data_name_fk = 500019

			left join epacube.epacube.product_association pa_450 with (nolock) on pi.product_structure_fk = pa_450.product_structure_fk and eiz.entity_structure_fk = pa_450.entity_structure_fk and pa_450.data_name_fk = 159450
			left join epacube.epacube.product_uom_class puc with (nolock) on pi.product_structure_fk = puc.product_structure_fk and puc.data_name_fk = 500025
			left join epacube.epacube.uom_code uc with (nolock) on puc.uom_code_fk = uc.uom_code_id
			inner join epacube.epacube.product_attribute pa with (nolock) on pi.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and pa.DATA_NAME_FK = 500001
			where isnull(z.job_FK, 0) in (@Job_FK, 0)
			) A 
			left join [IMPORTS].URM_LEVEL_GROSS_AUDIT Z2 on A.Effective_Date = Z2.Effective_Date and A.zone_entity_structure_fk = Z2.zone_entity_structure_fk and A.product_structure_fk = Z2.product_structure_fk and A.Job_fk = Z2.Job_FK
			where z2.URM_LEVEL_GROSS_AUDIT_ID is null) 'RECORDS_LOADED'
	
--Load data

	Insert into IMPORTS.URM_LEVEL_GROSS_AUDIT	
	(Effective_Date, product_structure_fk, zone_entity_structure_fk, Item, Item_Parity, New_Cost_Effective_Date, Alias, Alias_Data_Value_FK, TM_PCT_CUR, GM_PCT_CUR, GM_PCT_NEW, Unit_Cost_Cur, Unit_Cost_New
	, [ZONE_NUM], [ITEM_NBR], [ITEM_TYPE], [EFF_DATE], [NEWCSTCAS], [PRC_MULT], [PRICE], [NEWPRCMLT], [NEW_PRC], [EXTGMPCT], [TM_PCT], [OVRFLG], [NEWTMPCT], [NEWGMPCT], [EXTCSTCAS], [CLSIDLNIT], [CLSID_ITM], [QUAL_FLG], [HOLD_FLG], [PRT_FLG], [RESCINDED], [NEWCSTEFF], [LINK], [LVLID_PAR], [CLSPARITM], [ITMPARTYP], [PARITEM], [PARITMTYP], [SEQ_NUM6], [DTE_CRT], [TME_CRT], [USR_CRT], [DTE_UPD], [TME_UPD], [USR_UPD], [IMPORT_TIMESTAMP]
	, Create_Timestamp, Pack, UOM_Code, Job_FK)
	Select A.* from (
	Select epacube.[URM_DATE_CONVERSION](z.Eff_Date) 'Effective_Date'
	, pi.PRODUCT_STRUCTURE_FK, eiz.ENTITY_STRUCTURE_FK 'zone_entity_structure_fk'
	, pi.value 'Item'
	, pip.value 'Item_Parity'
	, epacube.[URM_DATE_CONVERSION](z.newcsteff) 'New_Cost_Effective_Date'
	, dv.value 'Alias'
	, dv.DATA_VALUE_ID 'Alias_Data_Value_FK'
	, Cast(z.tm_pct / 100.00 as Numeric(18, 4)) 'TM_PCT_CUR'
	, Cast(z.EXTGMPCT / 100.00 as Numeric(18, 4)) 'GM_PCT_CUR'
	, Cast(z.newgmpct / 100.00 as Numeric(18, 4)) 'GM_PCT_NEW'
	--, Case	when uc.uom_code = 'RWC'	then Round(cast((Cast(z.extcstcas as money) / ((isnull(puc.QUANTITY_MULTIPLE, 1) * cast(pa.ATTRIBUTE_NUMBER as numeric(18, 2))) / 16)) as money) + 0.0049, 2)
	--								else Round(Cast(z.extcstcas / pa.ATTRIBUTE_NUMBER as money) + 0.0049, 2) 
	--		end 'Unit_Cost_Cur'
	--, Case	when uc.uom_code = 'RWC'	then Round(cast((Cast(z.NEWCSTCAS as money) / ((isnull(puc.QUANTITY_MULTIPLE, 1) * cast(pa.ATTRIBUTE_NUMBER as numeric(18, 2))) / 16)) as money) + 0.0049, 2)
	--								else Round(Cast(z.NEWCSTCAS / pa.ATTRIBUTE_NUMBER as money) + 0.0049, 2) 
	--		end 'Unit_Cost_New'
	, Case	when uc.uom_code = 'RWC'	then Round(cast((Cast(z.extcstcas as money) / ((isnull(puc.QUANTITY_MULTIPLE, 1) * cast(pa.ATTRIBUTE_NUMBER as numeric(18, 2))) / 16)) as money), 2)
									else Round(Cast(z.extcstcas / pa.ATTRIBUTE_NUMBER as money), 2) 
			end 'Unit_Cost_Cur'
	, Case	when uc.uom_code = 'RWC'	then Round(cast((Cast(z.NEWCSTCAS as money) / ((isnull(puc.QUANTITY_MULTIPLE, 1) * cast(pa.ATTRIBUTE_NUMBER as numeric(18, 2))) / 16)) as money), 2)
									else Round(Cast(z.NEWCSTCAS / pa.ATTRIBUTE_NUMBER as money), 2) 
			end 'Unit_Cost_New'
	, Z.[ZONE_NUM], Z.[ITEM_NBR], Z.[ITEM_TYPE], Z.[EFF_DATE], Z.[NEWCSTCAS], Z.[PRC_MULT], Z.[PRICE], Z.[NEWPRCMLT], Z.[NEW_PRC], Z.[EXTGMPCT], Z.[TM_PCT], Z.[OVRFLG], Z.[NEWTMPCT], Z.[NEWGMPCT], Z.[EXTCSTCAS], Z.[CLSIDLNIT], Z.[CLSID_ITM], Z.[QUAL_FLG], Z.[HOLD_FLG], Z.[PRT_FLG], Z.[RESCINDED], Z.[NEWCSTEFF], Z.[LINK], Z.[LVLID_PAR], Z.[CLSPARITM], Z.[ITMPARTYP], Z.[PARITEM], Z.[PARITMTYP], Z.[SEQ_NUM6], Z.[DTE_CRT], Z.[TME_CRT], Z.[USR_CRT], Z.[DTE_UPD], Z.[TME_UPD], Z.[USR_UPD], Z.[IMPORT_TIMESTAMP]
	, cast(getdate() as date) 'Create_Timestamp'
	, pa.ATTRIBUTE_NUMBER 'Pack'
	, uc.uom_code
	, z.Job_FK
	from [IMPORTS].[URM_LEVEL_GROSS_ZONE] Z with (nolock)
	left join epacube.epacube.PRODUCT_IDENTIFICATION pi with (nolock) on z.item_nbr + z.Item_type = pi.value and pi.data_name_fk = 110100
	left join epacube.epacube.ENTITY_IDENTIFICATION eiz with (nolock) on z.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000
	left join epacube.epacube.PRODUCT_IDENTIFICATION pip with (nolock) on cast(z.paritem as varchar(16)) + cast(z.paritmtyp as varchar(16)) = pip.value and pip.data_name_fk = 110100 and isnull(z.paritem, '0') <> '0'
	left join epacube.epacube.data_value dv with (nolock) on z.link = dv.value and dv.data_name_fk = 500019

	left join epacube.epacube.product_association pa_450 with (nolock) on pi.product_structure_fk = pa_450.product_structure_fk and eiz.entity_structure_fk = pa_450.entity_structure_fk and pa_450.data_name_fk = 159450
	left join epacube.epacube.product_uom_class puc with (nolock) on pi.product_structure_fk = puc.product_structure_fk and puc.data_name_fk = 500025
	left join epacube.epacube.uom_code uc with (nolock) on puc.uom_code_fk = uc.uom_code_id
	inner join epacube.epacube.product_attribute pa with (nolock) on pi.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK and pa.DATA_NAME_FK = 500001
	where isnull(z.Job_FK, 0) in (@Job_FK, 0)
	) A 
	left join [IMPORTS].URM_LEVEL_GROSS_AUDIT Z2 on A.Effective_Date = Z2.Effective_Date and A.zone_entity_structure_fk = Z2.zone_entity_structure_fk 
																and A.product_structure_fk = Z2.product_structure_fk and A.job_fk = z2.job_fk
																
	where z2.URM_LEVEL_GROSS_AUDIT_ID is null

Declare @Wk int = 1

	Update pmccf
	set pricesheet_basis_values_id = pbv.Pricesheet_Basis_Values_ID
	from epacube.precision.price_maintenance_cost_change_flags pmccf
	inner join epacube.marginmgr.PRICESHEET_BASIS_VALUES pbv with (nolock) on pmccf.product_structure_fk = pbv.Product_Structure_FK
																and pbv.effective_date = pmccf.effective_date
																and pbv.data_name_fk = pmccf.data_name_fk
																and pbv.RESCIND_TIMESTAMP is null
	where pmccf.pricesheet_basis_values_id <> pbv.Pricesheet_Basis_Values_ID

	Update prc
	set pricesheet_basis_values_fk = pbv.Pricesheet_Basis_Values_ID
	from epacube.marginmgr.PRICESHEET_RETAIL_CHANGES prc
	inner join epacube.marginmgr.PRICESHEET_BASIS_VALUES pbv with (nolock) on prc.product_structure_fk = pbv.Product_Structure_FK
																and pbv.effective_date = prc.cost_change_effective_date
																and pbv.data_name_fk = 503201
																and pbv.RESCIND_TIMESTAMP is null
	where prc.pricesheet_basis_values_fk <> pbv.Pricesheet_Basis_Values_ID

	Insert into epacube.precision.price_maintenance_cost_change_flags
	(pricesheet_basis_values_id, effective_date, product_structure_fk, zone_entity_structure_fk, data_name_fk, value, item, cost_change_processed_date, pre_notify_days)
	select p.* from (
	select prc.PRICESHEET_BASIS_VALUES_FK
	, prc.COST_CHANGE_EFFECTIVE_DATE
	, prc.product_structure_fk
	, 113258 'zone_entity_structure_fk'
	, 503201 'data_name_fk'
	, prc.case_cost_new
	, prc.item
	, prc.PRICE_CHANGE_EFFECTIVE_DATE
	, cast(7 as int) 'pre_notify_days'
	from epacube.marginmgr.pricesheet_retail_changes prc with (nolock)
	where prc.price_change_effective_date = Cast(GETDATE() + 6 - (@wk * 7) - DATEPART(DW, GETDATE()) as date)
	and prc.COST_CHANGE_EFFECTIVE_DATE is not null
	and isnull(prc.ind_rescind, 0) <> 1
	and isnull(prc.IND_COST_CHANGE_ITEM, 0) = 1
	and prc.ITEM_CHANGE_TYPE_CR_FK not in (656, 662)
	group by 
	prc.PRICESHEET_BASIS_VALUES_FK
	, prc.COST_CHANGE_EFFECTIVE_DATE
	, prc.product_structure_fk
	, prc.case_cost_new
	, prc.item
	, prc.PRICE_CHANGE_EFFECTIVE_DATE
	) p 
	left join epacube.precision.price_maintenance_cost_change_flags pmccf with (nolock)  on p.pricesheet_basis_values_fk = pmccf.pricesheet_basis_values_id
		and p.pre_notify_days = pmccf.pre_notify_days
	where pmccf.price_maintenance_cost_change_flags_id is null

	--Exec [IMPORTS].[IMPORT_URM_TM_CATCHUP]
	Exec epacube.[precision].price_maintenance_processing_zone 0 

eop:

