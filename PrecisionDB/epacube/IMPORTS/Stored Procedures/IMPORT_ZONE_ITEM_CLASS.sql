﻿
-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        05/04/2018   Initial Version 

CREATE PROCEDURE [IMPORTS].[IMPORT_ZONE_ITEM_CLASS] @Job_FK bigint
AS 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/
		--Declare @Job_FK bigint = 6376

		DECLARE @l_exec_no          bigint;
		DECLARE @status_desc        varchar(max);

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
     -- EXEC [AMAZONA-8NUFTI5].common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.URM_ZONE_ITEM_CLASS', 
					--							@exec_id = @l_exec_no OUTPUT,
					--							@epa_job_id = @Job_FK;

     --SET @l_sysdate = GETDATE() --used for the beginning timestamp
     --SET @V_START_TIMESTAMP = getdate()  -- used in steps


-------------------------------------------------------------------------------------
----ITEM_CLASS files should be loaded first
 
----Undo any reinstate inheritance settings for target margins in zones that are not yet live.
	update ss
	set reinstate_inheritance_date = null
	from epacube.epacube.segments_settings ss
	where 1 = 1
	and data_name_fk = 502046
	and reinstate_inheritance_date is not null
	and ZONE_ENTITY_STRUCTURE_FK not in (select entity_structure_fk from epacube.precision.entities_live where RECORD_STATUS_CR_FK = 1)

-- **Load Segments_Settings Zone Item Class Begin

	Declare @Effective_Date date = dateadd(d, -1, (select top 1 cast(import_timestamp as date) from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where job_fk = @job_fk))

	If Object_ID('tempdb..#ZIC') is not null
	Drop table #ZIC

	Select a.*, eiz.ENTITY_STRUCTURE_FK 'Zone_Entity_Structure_FK', dv.data_value_id, dv.[description], dv.PARENT_DATA_VALUE_FK, dv.data_name_fk 'Prod_Segment_Data_Name_FK', @Job_FK 'Job_FK' 
	, @Effective_Date 'effective_date'
	into #ZIC 
	from (
	Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d0 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144897 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where job_fk = @job_fk and isnull(cast(prcadj_d0 as Numeric(18, 2)), 0.00) <> 0.00 Union
	Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d1 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144898 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where job_fk = @job_fk and isnull(cast(prcadj_d1 as Numeric(18, 2)), 0.00) <> 0.00 Union
	Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d2 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144899 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where job_fk = @job_fk and isnull(cast(prcadj_d2 as Numeric(18, 2)), 0.00) <> 0.00 Union
	Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d3 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144901 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where job_fk = @job_fk and isnull(cast(prcadj_d3 as Numeric(18, 2)), 0.00) <> 0.00 Union
	Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d4 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144902 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where job_fk = @job_fk and isnull(cast(prcadj_d4 as Numeric(18, 2)), 0.00) <> 0.00 Union
	Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d5 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144903 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where job_fk = @job_fk and isnull(cast(prcadj_d5 as Numeric(18, 2)), 0.00) <> 0.00 Union
	Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d6 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144904 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where job_fk = @job_fk and isnull(cast(prcadj_d6 as Numeric(18, 2)), 0.00) <> 0.00 Union
	Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d7 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144905 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where job_fk = @job_fk and isnull(cast(prcadj_d7 as Numeric(18, 2)), 0.00) <> 0.00 Union
	Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d8 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144906 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where job_fk = @job_fk and isnull(cast(prcadj_d8 as Numeric(18, 2)), 0.00) <> 0.00 Union
	Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d9 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144907 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where job_fk = @job_fk and isnull(cast(prcadj_d9 as Numeric(18, 2)), 0.00) <> 0.00 Union
	Select zone_num, lvlid_itm, clsid_itm, LG_FLG, 500015 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where LG_FLG in ('Y', 'N') and job_fk = @job_fk Union
	Select zone_num, lvlid_itm, clsid_itm, ReqChgAmt, 144908 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(cast(ReqChgAmt as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @job_fk Union
	Select zone_num, lvlid_itm, clsid_itm, Cast(Cast(Cast(TM_PCT as Numeric(18, 4)) / 100 as Numeric(18, 4)) as varchar(24)) 'TM_PCT', 502046 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(cast(TM_PCT as Numeric(18, 4)), 0) <> 0 and job_fk = @job_fk
	) A
	inner join epacube.epacube.entity_identification eiz with (nolock) on A.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.data_name_fk = 151110
	inner join epacube.epacube.data_value dv with (nolock) on a.lvlid_itm = dv.DISPLAY_SEQ and a.clsid_itm = dv.value and dv.data_name_fk between 501043 and 501045

	Create index idx_01 on #ZIC(zone_entity_structure_fk, data_name_fk, data_value_id, Prod_Segment_Data_Name_FK)

--Find records to add that are alpha	

	If Object_ID('tempdb..#Z01A') is not null
	Drop table #Z01A

	Select A.new_value, a.data_name_fk, a.Zone_Entity_Structure_FK, a.data_value_id, a.parent_data_value_fk, a.prod_segment_data_name_fk, ss.attribute_event_data, ss.CREATE_TIMESTAMP, ss.UPDATE_TIMESTAMP, ss.Effective_Date
	, ss.SEGMENTS_SETTINGS_ID
	into #Z01A
	from #ZIC A
	left join epacube.epacube.segments_settings ss with (nolock) on A.data_name_fk = ss.data_name_fk 
		and A.Prod_Segment_Data_Name_FK = ss.PROD_SEGMENT_data_name_FK
		and A.DATA_VALUE_ID = ss.PROD_SEGMENT_FK 
		and A.Zone_Entity_Structure_FK = ss.ZONE_ENTITY_STRUCTURE_FK
		and ss.RECORD_STATUS_CR_FK = 1
		and cast(ss.effective_date as date) <= a.effective_date
	--left join epacube.precision.entities_live el on ss.ZONE_Segment_FK = el.entity_structure_fk
	where 1 = 1
		and a.prod_segment_data_name_fk between 501043 and 501045
		and isnumeric(a.new_value) = 0 
		--and el.entities_live_id is null
		and A.New_Value <> (select attribute_event_data from epacube.epacube.segments_settings 
							where data_name_fk = a.data_name_fk 
							and Zone_Entity_Structure_FK = a.Zone_Entity_Structure_FK 
							and cust_entity_structure_fk is null 
							and prod_segment_fk is null
							and cast(effective_date as date) <= a.effective_date
							and record_status_cr_fk = 1)
	order by a.data_value_id

--Find records to add that are numeric
	If Object_ID('tempdb..#Z01') is not null
	Drop table #Z01

	Select A.new_value, a.effective_date, a.data_name_fk, a.Zone_Entity_Structure_FK, a.data_value_id, a.parent_data_value_fk, a.prod_segment_data_name_fk, ss.attribute_event_data, ss.CREATE_TIMESTAMP, ss.UPDATE_TIMESTAMP, ss.Effective_Date 'ss_effective_date'
	, ss.SEGMENTS_SETTINGS_ID
	into #Z01
	from #ZIC A
	inner join epacube.epacube.segments_settings ss with (nolock) 
		on A.data_name_fk = ss.data_name_fk 
		and A.Prod_Segment_Data_Name_FK = ss.PROD_SEGMENT_data_name_FK
		and A.DATA_VALUE_ID = ss.PROD_SEGMENT_FK 
		and A.Zone_Entity_Structure_FK = ss.ZONE_ENTITY_STRUCTURE_FK
		and ss.RECORD_STATUS_CR_FK = 1
	where 1 = 1
	and a.prod_segment_data_name_fk between 501043 and 501045 and isnumeric(a.new_value) = 1 
	and cast(ss.effective_date as date) <= a.effective_date
	order by a.data_value_id

--add new alpha records
	Insert into epacube.epacube.segments_settings
	(Effective_Date, Prod_Data_Value_FK, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Data_Name_FK, Attribute_Y_N, Attribute_Event_Data, Precedence, Prod_Segment_FK, Prod_Segment_Data_Name_FK, Zone_Segment_FK, Entity_Data_Name_FK, Record_Status_CR_FK, [DATA_SOURCE], Job_FK)
	Select z.effective_date 'Effective_Date', z.data_value_id 'Prod_Data_Value_FK', 1 'Org_Entity_Structure_FK', z.Zone_Entity_Structure_FK, z.data_name_fk, z.new_value, z.new_value, dn.precedence
	, z.data_value_id 'Prod_Segment_FK', z.Prod_Segment_Data_Name_FK, z.Zone_Entity_Structure_FK 'Zone_Segment_FK', 151000 'Entity_Data_Name_FK', 1 'Record_Status_CR_FK', 'DAILY UPDATE' 'DATA_SOURCE', @Job_FK 'Job_FK'
	from #Z01A Z 
	inner join epacube.epacube.data_name dn with (nolock) on z.prod_segment_data_name_fk = dn.data_name_id
	where z.new_value <> z.attribute_event_data

--add new numeric records	
	Insert into epacube.epacube.segments_settings
	(Effective_Date, Prod_Data_Value_FK, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Data_Name_FK, Attribute_Number, Attribute_Event_Data, Precedence, Prod_Segment_FK, Prod_Segment_Data_Name_FK, Zone_Segment_FK, Entity_Data_Name_FK, Record_Status_CR_FK, [DATA_SOURCE], Job_FK)
	Select z.effective_date, z.data_value_id 'Prod_Data_Value_FK', 1 'Org_Entity_Structure_FK', z.Zone_Entity_Structure_FK, z.data_name_fk, cast(z.new_value as Numeric(18, 4)) 'Attribute_Number_New'
	, cast(z.new_value as Numeric(18, 4)) 'Attribute_Number_New', dn.precedence, z.data_value_id 'Prod_Segment_FK', z.Prod_Segment_Data_Name_FK, z.Zone_Entity_Structure_FK 'Zone_Segment_FK', 151000 'Entity_Data_Name_FK', 1 'Record_Status_CR_FK', 'DAILY UPDATE'
	, @Job_FK 'Job_FK'
	from #Z01 Z 
	inner join epacube.epacube.data_name dn with (nolock) on z.prod_segment_data_name_fk = dn.data_name_id
	where 1 = 1
	and cast(z.new_value as Numeric(18, 4)) <> cast(z.attribute_event_data as Numeric(18, 4))
	and cast(z.new_value as numeric(18, 4)) <> 0
	and Cast(z.attribute_event_data as numeric(18, 4)) <> 
		isnull(Cast((select attribute_event_data from epacube.epacube.segments_settings ss
				where ss.data_name_fk = z.data_name_fk 
				and ss.Zone_Entity_Structure_FK = z.Zone_Entity_Structure_FK 
				and ss.cust_entity_structure_fk is null 
				and ss.prod_segment_fk is null
				and cast(ss.effective_date as date) <= z.effective_date
				and ss.record_status_cr_fk = 1) as numeric(18, 4)), 0)
	order by Prod_Segment_FK

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, JOB_FK)
	select 
	'IMPORTS.IMPORT_ZONE_ITEM_CLASS' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE_ITEM_CLASS' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.SEGMENTS_SETTINGS' 'TARGET_TABLE'
	, 'NEW ZONE ITEM CLASS SETTINGS' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM #Z01) 'RECORDS_LOADED'
	, @Job_FK

	If Object_ID('tempdb..#ZIC') is not null
	Drop table #ZIC

 SET @status_desc =  'finished execution of IMPORT Zone Item Class Data'
 
 --Exec [AMAZONA-8NUFTI5].common.exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @Job_FK;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT Zone item class Data has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         --EXEC [AMAZONA-8NUFTI5].common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @Job_FK;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			--EXEC [AMAZONA-8NUFTI5].common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
