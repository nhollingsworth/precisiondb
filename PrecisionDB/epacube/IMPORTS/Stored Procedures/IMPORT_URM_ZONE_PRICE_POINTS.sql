﻿-- =============================================
-- Author:		Gary Stone
-- Create date: August 14, 2018
-- =============================================
CREATE PROCEDURE [IMPORTS].[IMPORT_URM_ZONE_PRICE_POINTS] @Job_FK bigint
AS
	SET NOCOUNT ON;

	If (Select count(*) from [customer_imports].[IMPORTS].[URM_ZONE_PRICE_POINTS] where job_fk = @Job_FK) > 0
		Delete from epacube.epacube.segments_settings where data_name_fk = 144891 and zone_entity_structure_fk is not null and cust_entity_structure_fk is null
	else
		GoTo eop

	If Object_id('tempdb..#Rnd') is not null
	drop table #Rnd

	Select distinct eiz.entity_structure_fk 'Zone_Entity_Structure_FK', dv.data_value_id 'data_value_fk', dv.data_name_fk, dn.precedence, PP.* 
	into #Rnd
	from [customer_imports].[IMPORTS].[URM_ZONE_PRICE_POINTS] PP
	inner join epacube.epacube.entity_identification eiz with (nolock) on PP.ZONE_NUM = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000
	inner join epacube.epacube.data_value dv with (nolock) on PP.CLSID_ITM = dv.value and PP.LVLID_ITM = dv.DISPLAY_SEQ and dv.data_name_fk between 501041 and 501045
	inner join epacube.epacube.data_name dn with (nolock) on dv.DATA_NAME_FK = dn.DATA_NAME_ID
	where PP.job_FK = @Job_FK

	Create index idx_01 on #Rnd(data_name_fk, data_value_fk, RndLowLmt, [RNDUPLMT], RndAmt)

	ALTER TABLE #Rnd ADD [ROW_NUM] BIGINT IDENTITY(1, 1) NOT NULL

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, Job_FK)
	select 
	'IMPORTS.IMPORT_URM_ZONE_PRICE_POINTS' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE_PRICE_POINTS' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.SEGMENTS_SETTINGS' 'TARGET_TABLE'
	, 'RETAIL ROUNDING VALUES WITH UPPER AND LOWER LIMITS' 'TYPE_OF_DATA'	
	,	(Select count(*) 
		from (
			Select distinct cpp.data_value_fk, 1 'Org_Entity_Structure_FK', 144891 Data_Name_FK
			, CPP.RndLowLmt 'Lower_Limit', CPP.[RNDUPLMT] 'Upper_Limit', CPP.RndAmt 'Attribute_Number', CPP.RndAmt 'Attribute_Event_Data'
			, CPP.PRECEDENCE, cpp.zone_entity_structure_fk, cpp.data_name_fk 'Prod_Segment_Data_Name_FK'
			, epacube.[URM_DATE_CONVERSION](cpp.dte_upd) 'effective_date', cpp.usr_upd 'create_user'
			, (Select entity_data_value_id from epacube.epacube.entity_data_value where data_name_fk = 502031 and data_value_fk = 
				(select data_value_id from epacube.epacube.data_value where data_name_fk = 502041 and value = 10)
				and value = CPP.PRC_TYPE) 'Price_Type_Entity_Data_Value_FK'
			, cpp.IMPORT_TIMESTAMP
			from #Rnd CPP with (nolock)
			inner join epacube.epacube.data_name dn with (nolock) on cpp.data_name_fk = dn.data_name_id
			left join
				(
					select * 
					from (
							Select ss.ATTRIBUTE_NUMBER, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID
							, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK, dn.label 'Item_Class_Level', dv.value 'Item_Class_ID'
							, dv.[description] 'Item_Class_Description', Lower_Limit, Upper_Limit
							, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, ss.prod_segment_fk, ss.precedence, Lower_Limit, Upper_Limit 
									order by ss.effective_date desc, ss.segments_settings_id desc) 'DRank', ss.record_status_cr_fk
							from epacube.epacube.segments_settings ss with (nolock) 
							left join epacube.epacube.data_value dv with (nolock) on ss.PROD_SEGMENT_FK = dv.data_value_id and dv.data_name_fk between 501041 and 501045
							left join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
							where ss.data_name_fk = 144891
							and ss.cust_entity_structure_fk is null
							and ss.zone_segment_fk is not null
							and ss.RECORD_STATUS_CR_FK = 1
						) b where Drank = 1
				) A on cpp.data_name_fk = a.prod_segment_data_name_fk
					and cpp.data_value_fk = a.prod_segment_fk
					and CPP.RndLowLmt = a.lower_limit
					and CPP.[RNDUPLMT] = a.upper_limit
					and CPP.RndAmt = a.ATTRIBUTE_NUMBER
				where a.segments_settings_id is null
			) C) 'RECORDS_LOADED'
	, @Job_FK 'Job_FK'

	Insert into epacube.epacube.segments_settings
	(Prod_Data_Value_FK, prod_segment_fk, prod_segment_data_name_fk, ORG_ENTITY_STRUCTURE_FK, Data_Name_FK, Lower_Limit, Upper_Limit
	, Attribute_Number, Attribute_Event_Data, Precedence, zone_entity_structure_fk, zone_segment_fk, entity_data_name_fk, record_status_cr_fk
	, effective_date, create_user, create_timestamp, Price_Type_Entity_Data_Value_FK, IMPORT_TIMESTAMP, Job_FK, DATA_SOURCE)
	Select Data_Value_fk 'Prod_Data_Value_FK', Data_Value_fk 'prod_segment_fk', prod_segment_data_name_fk, ORG_ENTITY_STRUCTURE_FK, Data_Name_FK, Lower_Limit, Upper_Limit
	, Attribute_Number, Attribute_Event_Data, Precedence
	, zone_entity_structure_fk 'zone_entity_structure_fk', zone_entity_structure_fk 'zone_segment_fk', 151000 'entity_data_name_fk', 1 'record_status_cr_fk'
	, effective_date, create_user, getdate() 'create_timestamp', Price_Type_Entity_Data_Value_FK, IMPORT_TIMESTAMP, Job_FK, 'FULL ZONE REFRESH' 'Data_Source'
	from (
	Select distinct cpp.data_value_fk, 1 'Org_Entity_Structure_FK', 144891 Data_Name_FK
	, CPP.RndLowLmt 'Lower_Limit', CPP.[RNDUPLMT] 'Upper_Limit', CPP.RndAmt 'Attribute_Number', CPP.RndAmt 'Attribute_Event_Data'
	, CPP.PRECEDENCE, cpp.zone_entity_structure_fk, cpp.data_name_fk 'Prod_Segment_Data_Name_FK'
	, epacube.[URM_DATE_CONVERSION](cpp.dte_upd) 'effective_date', cpp.usr_upd 'create_user'
	, (Select entity_data_value_id from epacube.epacube.entity_data_value where data_name_fk = 502031 and data_value_fk = 
		(select data_value_id from epacube.epacube.data_value where data_name_fk = 502041 and value = 10)
		and value = CPP.PRC_TYPE) 'Price_Type_Entity_Data_Value_FK'
	, cpp.IMPORT_TIMESTAMP
	, cpp.job_fk
	from #Rnd CPP with (nolock)
	inner join epacube.epacube.data_name dn with (nolock) on cpp.data_name_fk = dn.data_name_id
	left join
		(
			select * 
			from (
					Select ss.ATTRIBUTE_NUMBER, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID
					, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK, dn.label 'Item_Class_Level', dv.value 'Item_Class_ID'
					, dv.[description] 'Item_Class_Description', Lower_Limit, Upper_Limit
					, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, ss.prod_segment_fk, ss.precedence, Lower_Limit, Upper_Limit 
							order by ss.effective_date desc, ss.segments_settings_id desc) 'DRank', ss.record_status_cr_fk
					from epacube.epacube.segments_settings ss with (nolock) 
					left join epacube.epacube.data_value dv with (nolock) on ss.PROD_SEGMENT_FK = dv.data_value_id and dv.data_name_fk between 501041 and 501045
					left join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
					where ss.data_name_fk = 144891
					and ss.cust_entity_structure_fk is null
					and ss.zone_segment_fk is not null
					and ss.RECORD_STATUS_CR_FK = 1
				) b where Drank = 1
		) A on cpp.data_name_fk = a.prod_segment_data_name_fk
			and cpp.data_value_fk = a.prod_segment_fk
			and CPP.RndLowLmt = a.lower_limit
			and CPP.[RNDUPLMT] = a.upper_limit
			and CPP.RndAmt = a.ATTRIBUTE_NUMBER
		where a.segments_settings_id is null
	) C 

	goto eop2

	eop:

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, JOB_FK)
	select 
	'IMPORTS.IMPORT_URM_ZONE_PRICE_POINTS' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE_PRICE_POINTS' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.SEGMENTS_SETTINGS' 'TARGET_TABLE'
	, 'RETAIL ROUNDING VALUES WITH UPPER AND LOWER LIMITS' 'TYPE_OF_DATA'
	, NULL 'RECORDS_LOADED'
	, @Job_FK 'Job_FK'

	eop2:
