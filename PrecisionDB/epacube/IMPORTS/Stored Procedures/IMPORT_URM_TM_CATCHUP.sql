﻿
-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- GS        09/10/2019   Run before level gross is processed to insure all TM's since last level gross processed are in place.

CREATE PROCEDURE [IMPORTS].[IMPORT_URM_TM_CATCHUP]
AS

Declare @Date_Last_Processed date
Declare @dt date
Declare @Job_FK bigint

set @Date_Last_Processed = (select top 1 price_change_effective_date from epacube.marginmgr.PRICESHEET_RETAIL_CHANGES 
							where price_change_effective_date < getdate()
							order by PRICE_CHANGE_EFFECTIVE_DATE desc)
Set @Date_Last_Processed = dateadd(d, -3, @Date_Last_Processed)

		DECLARE TMs Cursor local for
		select cast(import_timestamp as date) dt, job_fk
		from imports.URM_ZONE_ITEM
		where 1 = 1
		and cast(import_timestamp as date) >= @Date_Last_Processed
		--and job_fk = @Job_FK
		group by cast(import_timestamp as date), job_fk
		order by cast(import_timestamp as date) , job_fk

		OPEN TMs;
		FETCH NEXT FROM TMs INTO @dt, @Job_FK
		WHILE @@FETCH_STATUS = 0

			Begin--Fetch

	If object_id('tempdb..#zn_tm') is not null
	drop table #zn_tm

	Select cast(1 as bigint) 'Org_Entity_Structure_FK'
	, Zone_Entity_Structure_FK
	, cast(502046 as bigint) 'Data_Name_FK'
	, cast(cast(zn.TM_pct as Numeric(18, 4)) / 100 as Numeric(18, 4)) 'TM_PCT'
	, cast(5 as int) 'Precedence'
	, ZN.product_structure_fk 'Prod_Segment_FK'
	, ZN.Zone_entity_structure_fk 'Zone_Segment_FK'
	, ZN.product_structure_FK
	, cast(dateadd(d, -1, import_timestamp) as date) 'Effective_Date'
	, cast(151000 as bigint) 'Entity_Data_Name_FK'
	, cast(110103 as bigint) 'Prod_Segment_Data_Name_Fk'
	, zn.Import_Timestamp
	, 'DAILY UPDATE' 'DATA_SOURCE'
	into #zn_tm
	from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] ZN
	--left join epacube.precision.entities_live el on zn.ZONE_ENTITY_STRUCTURE_FK = el.entity_structure_fk
	where job_fk = @job_fk
	and cast(zn.TM_pct as Numeric(18, 4)) <> 0
	--and el.entities_live_live is null

	create index idx_01 on #zn_tm(product_structure_fk, zone_entity_structure_fk)

	If object_id('tempdb..#CurVals') is not null
	drop table #CurVals

	--Get Current_Values
	Select *
	into #CurVals
	from (
	Select ss.ATTRIBUTE_NUMBER 'TM_CUR', sp.product_structure_fk, ss.zone_segment_fk, ss.SEGMENTS_SETTINGS_ID, ss.prod_segment_fk, ss.PROD_SEGMENT_DATA_NAME_FK
	, dense_rank()over(partition by ss.data_name_fk, ss.zone_segment_fk, sp.product_structure_fk order by ss.precedence, ss.effective_date desc, ss.segments_settings_id desc) DRank
	from epacube.epacube.segments_settings ss with (nolock) 
	inner join epacube.epacube.segments_product sp with (nolock) on ss.prod_segment_fk = sp.prod_segment_fk and ss.prod_segment_data_name_fk = sp.data_name_fk
	inner join #zn_tm UZI with (nolock) on ss.zone_segment_fk = uzi.zone_entity_structure_fk and sp.PRODUCT_STRUCTURE_FK = UZI.product_structure_fk
	where ss.data_name_fk = 502046
	and ss.RECORD_STATUS_CR_FK = 1
	and ss.effective_date <= cast(uzi.import_timestamp as date)
	) a where Drank = 1

	create index idx_02 on #CurVals(product_structure_fk, zone_segment_fk)

	Insert into epacube.epacube.segments_settings
	(Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Data_Name_FK, Attribute_Number, Attribute_Event_Data, Precedence, Record_Status_CR_FK, Prod_Segment_FK, Zone_Segment_FK
	, Product_STructure_FK, Effective_Date, Entity_Data_Name_FK, Prod_Segment_Data_Name_Fk, Import_Timestamp, create_timestamp, update_timestamp, [DATA_SOURCE])
	Select 
	zn.Org_Entity_Structure_FK
	, zn.Zone_Entity_Structure_FK

	--, pi.value 'item'
	--, ei.value 'zone_id'

	, zn.Data_Name_FK
	, zn.TM_PCT 'Attribute_Number'
	, zn.TM_PCT 'Attribute_Event_Data'
	, zn.Precedence
	, 1 'Record_Status_CR_FK'
	, zn.Prod_Segment_FK
	, zn.Zone_Segment_FK
	, zn.product_structure_FK
	, zn.Effective_Date
	, zn.Entity_Data_Name_FK
	, zn.Prod_Segment_Data_Name_Fk
	, zn.Import_Timestamp
	, dateadd(d, -1, zn.Import_Timestamp) 'create_timestamp'
	, dateadd(d, -1, zn.Import_Timestamp) 'update_timestamp'
	, zn.DATA_SOURCE
	 from #zn_tm zn
	 left join #CurVals cv on zn.PRODUCT_STRUCTURE_FK = cv.PRODUCT_STRUCTURE_FK and zn.ZONE_ENTITY_STRUCTURE_FK = cv.ZONE_SEGMENT_FK and zn.TM_PCT = cv.tm_cur
	 
	 --left join epacube.epacube.PRODUCT_IDENTIFICATION pi with (nolock) on zn.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK and pi.DATA_NAME_FK = 110100
	 --left join epacube.epacube.ENTITY_IDENTIFICATION ei with (nolock) on zn.ZONE_ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK and ei.ENTITY_DATA_NAME_FK = 151000
	 where cv.drank is null

		FETCH NEXT FROM TMs INTO @dt, @Job_FK
			End--Fetch
		Close TMs;
		Deallocate TMs;
