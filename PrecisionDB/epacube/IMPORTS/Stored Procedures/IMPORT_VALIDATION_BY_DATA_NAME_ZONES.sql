﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [IMPORTS].[IMPORT_VALIDATION_BY_DATA_NAME_ZONES]
AS

	SET NOCOUNT ON;

	Insert into PRECISION.IMPORT_VALIDATION_BY_DATA_NAME
	([BATCH], [Table_Name], [Query_Date], [data_name_fk], [DNL], [Import_Timestamp], [Records])
	Select isnull((Select top 1 Batch from PRECISION.IMPORT_VALIDATION_BY_DATA_NAME order by Batch desc), 0) + 1 'Batch' , * 
	FROM
	(
		Select 'EPACUBE.EPACUBE.SEGMENTS_SETTINGS' 'Table_Name', getdate() 'Query_Date', data_name_fk, epacube.epacube.getdatanamelabel(data_name_fk) 'DNL', Max(Import_Timestamp) 'Import_Timestamp', count(*) 'Records' 
		from EPACUBE.EPACUBE.SEGMENTS_SETTINGS with (nolock) 
		where ZONE_ENTITY_STRUCTURE_FK is not null and CUST_ENTITY_STRUCTURE_FK is null
		group by data_name_fk

		Union

		Select 'EPACUBE.EPACUBE.PRODUCT_ASSOCIATION' 'Table_Name', getdate() 'Query_Date', data_name_fk, epacube.epacube.getdatanamelabel(data_name_fk) 'DNL', Max(Import_Timestamp) 'Import_Timestamp', count(*) 'Records' 
		from EPACUBE.EPACUBE.PRODUCT_ASSOCIATION with (nolock) 
		where ENTITY_STRUCTURE_FK is not null
		and ENTITY_CLASS_CR_FK in (10109, 10117)
		group by data_name_fk

		Union

		Select 'EPACUBE.EPACUBE.PRODUCT_MULT_TYPE' 'Table_Name', getdate() 'Query_Date', data_name_fk, epacube.epacube.getdatanamelabel(data_name_fk) 'DNL', Max(Import_Timestamp) 'Import_Timestamp', count(*) 'Records' 
		from EPACUBE.EPACUBE.PRODUCT_MULT_TYPE with (nolock) 
		group by data_name_fk

		Union

		Select 'EPACUBE.EPACUBE.SEGMENTS_PRODUCT' 'Table_Name', getdate() 'Query_Date', data_name_fk, epacube.epacube.getdatanamelabel(data_name_fk) 'DNL', Max(IMPORT_timestamp) 'Import_Timestamp', count(*) 'Records' 
		from EPACUBE.EPACUBE.SEGMENTS_PRODUCT with (nolock)
		group by data_name_fk

		Union

		Select 'EPACUBE.EPACUBE.PRODUCT_BOM' 'Table_Name', getdate() 'Query_Date', data_name_fk, epacube.epacube.getdatanamelabel(data_name_fk) 'DNL', Max(IMPORT_timestamp) 'Import_Timestamp', count(*) 'Records' 
		from EPACUBE.EPACUBE.PRODUCT_BOM with (nolock)
		group by data_name_fk

		Union

		Select 'EPACUBE.MARGINMGR.PRICESHEET_BASIS_VALUES' 'Table_Name', getdate() 'Query_Date', data_name_fk, epacube.epacube.getdatanamelabel(data_name_fk) 'DNL', Max(Import_Timestamp) 'Import_Timestamp', count(*) 'Records' 
		from EPACUBE.MARGINMGR.PRICESHEET_BASIS_VALUES with (nolock)
		where data_name_fk = 503201
		and Cust_Entity_Structure_FK is null
		and Zone_Entity_Structure_FK is not null
		group by data_name_fk

		Union

		Select 'EPACUBE.MARGINMGR.PRICESHEET_RETAILS' 'Table_Name', getdate() 'Query_Date', data_name_fk, epacube.epacube.getdatanamelabel(data_name_fk) 'DNL', Max(Import_Timestamp) 'Import_Timestamp', count(*) 'Records'
		from EPACUBE.MARGINMGR.PRICESHEET_RETAILS with (nolock)
		group by data_name_fk
	) a

	Update LGA
	Set PRODUCT_STRUCTURE_FK = pi.product_structure_fk
	from [IMPORTS].[URM_LEVEL_GROSS_AUDIT] lga
	inner join epacube.epacube.product_identification pi with (nolock) on lga.item = pi.value and pi.data_name_fk = 110100
	where isnull(lga.product_structure_fk, 0) <> isnull(pi.product_structure_fk, 0) and isnull(pi.product_structure_fk, 0) <> 0

	Update PRC
	Set product_structure_fk = pi.PRODUCT_STRUCTURE_FK
	from epacube.marginmgr.pricesheet_retail_changes prc
	inner join epacube.epacube.product_identification pi with (nolock) on prc.item = pi.value and pi.data_name_fk = 110100
	where isnull(prc.product_structure_fk, 0) <> isnull(pi.product_structure_fk, 0) and isnull(pi.product_structure_fk, 0) <> 0

	Declare @Tbl varchar(128)
	
	DECLARE Tables Cursor local for
	Select * from	(
					Select Table_Name from [PRECISION].[IMPORT_VALIDATION_BY_DATA_NAME] group by Table_Name
					UNION
					Select TARGET_TABLE from [PRECISION].[DAILY_IMPORT_LOG] WHERE TARGET_TABLE NOT LIKE '%CUSTOMER_IMPORTS%' GROUP BY TARGET_TABLE
					Union
					Select 'epacube.epacube.entity_attribute'
					Union
					Select 'epacube.epacube.product_attribute'
					) A 

			OPEN Tables;
			FETCH NEXT FROM Tables INTO @Tbl
	            
			WHILE @@FETCH_STATUS = 0
			Begin

				DBCC DBREINDEX(@Tbl, ' ', 80)
	
			FETCH NEXT FROM Tables INTO @Tbl

	End
	Close Tables;
	Deallocate Tables;
