﻿





-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- GHS       06/06/2018   Initial Version 

CREATE PROCEDURE [IMPORTS].[IMPORT_ZONE_ITEM_CLASS_FULL_SETTINGS_REFRESH] @z_Job_FK bigint = 0, @zic_Job_FK bigint = 0, @zi_job_fk bigint = 0, @Maintain_Live_Entities int = 0
AS 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.

---  THIS PROCEDURE USES TABLES
     URM ZONE
	 URM ZONE ITEM
	 URM ZONE ITEM CLASS 
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/
If isnull(@z_Job_FK, 0) = 0
	Set @z_Job_FK = isnull((select top 1 Job_Fk from imports.urm_zone order by import_timestamp desc, job_fk desc), 0)
	else
	Set @z_Job_FK = @z_Job_FK

If isnull(@zic_Job_FK, 0) = 0
	Set @zic_Job_FK = isnull((select top 1 Job_Fk from imports.urm_zone_item_class order by import_timestamp desc, job_fk desc), 0)
	else
	Set @zic_Job_FK = @zic_Job_FK

If isnull(@zi_Job_FK, 0) = 0
	Set @zi_Job_FK = isnull((select top 1 Job_Fk from imports.urm_zone_item order by import_timestamp desc, job_fk desc), 0)
	else
	Set @zi_Job_FK = @zi_Job_FK


     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint
	 DECLARE @IN_JOB_FK bigint 

SET NOCOUNT ON;

BEGIN TRY
     -- SET @l_exec_no = 0;
     -- EXEC [AMAZONA-8NUFTI5].common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.URM_ZONE_ITEM_CLASS', 
					--							@exec_id = @l_exec_no OUTPUT,
					--							@epa_job_id = @in_job_fk;

     --SET @l_sysdate = GETDATE() --used for the beginning timestamp
     --SET @V_START_TIMESTAMP = getdate()  -- used in steps

	If Object_ID('epacube.precision.item_class_structure') is not null
	drop table epacube.precision.item_class_structure

	Select dv3.data_value_id 'Prod_Segment_FK3', dv3.[Description] 'DV3_Desc', dv3.data_name_fk 'data_name_fk3', cast(dv3.value as int) 'IC_ID3'
	, dv4.data_value_id 'Prod_Segment_FK4', dv4.[Description] 'DV4_Desc', dv4.data_name_fk 'data_name_fk4', cast(dv4.value as int) 'IC_ID4'
	, dv5.data_value_id 'Prod_Segment_FK5', dv5.[Description] 'DV5_Desc', dv5.data_name_fk 'data_name_fk5', cast(dv5.value as int) 'IC_ID5'
	, ei.entity_structure_fk 'Zone_Segment_FK', ei.value 'Zone', ein.value 'Zone_Name'
	into epacube.precision.item_class_structure
	from epacube.epacube.data_value dv3 with (nolock)
	inner join epacube.epacube.data_value dv4 with (nolock) on dv3.DATA_VALUE_ID = dv4.PARENT_DATA_VALUE_FK
	inner join epacube.epacube.data_value dv5 with (nolock) on dv4.data_value_id = dv5.PARENT_DATA_VALUE_FK
	cross join epacube.epacube.entity_identification ei with (nolock)
	left join epacube.epacube.entity_ident_nonunique ein with (nolock) on ei.entity_structure_fk = ein.entity_structure_fk and ein.data_name_fk = 151112
	where dv3.data_name_fk = 501043
	and ei.entity_data_name_fk = 151000 and ei.data_name_fk = 151110

	Create index idx_03 on epacube.precision.item_class_structure(prod_Segment_FK3, Data_Name_FK3, zone_segment_FK)
	Create index idx_04 on epacube.precision.item_class_structure(prod_Segment_FK4, Data_Name_FK4, zone_segment_FK)
	Create index idx_05 on epacube.precision.item_class_structure(prod_Segment_FK5, Data_Name_FK5, zone_segment_FK)

	If Object_Id('tempdb..#Z') is not null
	Drop table #Z

		Select A.*, eiz.entity_structure_fk 'Zone_Segment_FK'
		into #Z
		from 
		(
		Select zone_num, cast(Cast(cast(prcadj_d0 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144897 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(cast(prcadj_d0 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, cast(Cast(cast(prcadj_d1 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144898 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(cast(prcadj_d1 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, cast(Cast(cast(prcadj_d2 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144899 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(cast(prcadj_d2 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, cast(Cast(cast(prcadj_d3 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144901 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(cast(prcadj_d3 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, cast(Cast(cast(prcadj_d4 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144902 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(cast(prcadj_d4 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, cast(Cast(cast(prcadj_d5 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144903 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(cast(prcadj_d5 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, cast(Cast(cast(prcadj_d6 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144904 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(cast(prcadj_d6 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, cast(Cast(cast(prcadj_d7 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144905 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(cast(prcadj_d7 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, cast(Cast(cast(prcadj_d8 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144906 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(cast(prcadj_d8 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, cast(Cast(cast(prcadj_d9 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144907 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(cast(prcadj_d9 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, Prenotday, 144868 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(cast(Prenotday as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, lg_flg, 144862 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(lg_flg, 'N') <> 'N' Union
		Select zone_num, ReqChgAmt, 144908 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(cast(ReqChgAmt as Numeric(18, 2)), 0.00) <> 0.0 Union
		Select zone_num, LG_PP_FLG, 144876 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(job_FK, 0) in (@z_Job_FK, 0) and isnull(LG_PP_FLG, 'N') <> 'N'
		) A
		inner join epacube.epacube.entity_identification eiz with (nolock) on A.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.data_name_fk = 151110
		where isnull(A.New_Value, '') <> ''
		order by zone_num

	If Object_Id('tempdb..#ZIC') is not null
	Drop table #ZIC

		Select a.*, eiz.ENTITY_STRUCTURE_FK 'Zone_Segment_FK', dv.data_value_id 'Prod_Segment_FK', dv.[description], dv.PARENT_DATA_VALUE_FK, dv.data_name_fk 'Prod_Segment_Data_Name_FK' 
		into #ZIC 
		from (
		Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d0 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144897 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and isnull(cast(prcadj_d0 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d1 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144898 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and isnull(cast(prcadj_d1 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d2 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144899 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and isnull(cast(prcadj_d2 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d3 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144901 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and isnull(cast(prcadj_d3 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d4 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144902 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and isnull(cast(prcadj_d4 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d5 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144903 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and isnull(cast(prcadj_d5 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d6 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144904 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and isnull(cast(prcadj_d6 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d7 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144905 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and isnull(cast(prcadj_d7 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d8 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144906 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and isnull(cast(prcadj_d8 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, lvlid_itm, clsid_itm, cast(Cast(cast(prcadj_d9 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144907 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and isnull(cast(prcadj_d9 as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, lvlid_itm, clsid_itm, LG_FLG, 500015 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and LG_FLG in ('Y', 'N') Union
		Select zone_num, lvlid_itm, clsid_itm, ReqChgAmt, 144908 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and isnull(cast(ReqChgAmt as Numeric(18, 2)), 0.00) <> 0.00 Union
		Select zone_num, lvlid_itm, clsid_itm, Cast(Cast(Cast(TM_PCT as Numeric(18, 4)) / 100 as Numeric(18, 4)) as varchar(24)) 'TM_PCT', 502046 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_CLASS] where isnull(job_FK, 0) in (@zic_Job_FK, 0) and isnull(cast(TM_PCT as Numeric(18, 2)), 0.00) <> 0.00 --Union
		) A
		inner join epacube.epacube.entity_identification eiz with (nolock) on A.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.data_name_fk = 151110
		inner join epacube.epacube.data_value dv with (nolock) on a.lvlid_itm = dv.DISPLAY_SEQ and a.clsid_itm = dv.value and dv.data_name_fk between 501043 and 501045
		where isnull(A.New_Value, '') <> ''
		order by zone_num, LVLID_ITM, CLSID_ITM

	If Object_Id('tempdb..#ZI') is not null
	Drop table #ZI

		Select a.*, eiz.ENTITY_STRUCTURE_FK 'Zone_Segment_FK', pi.PRODUCT_STRUCTURE_FK 'Prod_Segment_FK', pd.[description], 110103 'Prod_Segment_Data_Name_FK' 
		into #ZI
		from (
		--Select item_nbr + item_type 'Item', zone_num, LG_FLG 'new_value', 500015 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from urm.rims7.zone_item where LG_FLG in ('Y', 'N') Union
		--Select item_nbr + item_type 'Item', zone_num, Cast(Cast(Cast(TM_PCT as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(24)) 'new_value', 502046 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from urm.rims7.zone_item where isnull(cast(TM_PCT as Numeric(18, 2)), 0.00) <> 0.00 --Union
		Select item_nbr + item_type 'Item', zone_num, LG_FLG 'new_value', 500015 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] where isnull(job_FK, 0) in (@zi_Job_FK, 0) and LG_FLG in ('Y', 'N') Union
		Select item_nbr + item_type 'Item', zone_num, Cast(Cast(Cast(TM_PCT as Numeric(18, 4)) / 100 as Numeric(18, 4)) as varchar(24)) 'new_value', 502046 'data_name_fk', epacube.[URM_DATE_TIME_CONVERSION](Case DTE_UPD when '0' then DTE_CRT else DTE_UPD end, Case DTE_UPD when '0' then TME_CRT else TME_UPD end) 'Effective_Date' from [customer_imports].[IMPORTS].[URM_ZONE_ITEM] where isnull(job_FK, 0) in (@zi_Job_FK, 0) and isnull(cast(TM_PCT as Numeric(18, 2)), 0.00) <> 0.00 --Union
		) A
		inner join epacube.epacube.entity_identification eiz with (nolock) on A.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.data_name_fk = 151110
		inner join epacube.epacube.product_identification pi with (nolock) on A.Item = pi.value and pi.data_name_fk = 110100
		left join epacube.epacube.product_description pd with (nolock) on pi.PRODUCT_STRUCTURE_FK = pd.product_structure_fk and pd.data_name_fk = 110401
		where isnull(A.New_Value, '') not in ('', '0')
		order by a.zone_num, a.item

		Create index idx_z1 on #zi(zone_segment_fk, Prod_segment_fk)

	If Object_Id('tempdb..#SS') is not null
	Drop table #SS

		--Select top 1 * into #SS from epacube.epacube.segments_settings where data_name_fk = 144868 and ZONE_ENTITY_STRUCTURE_FK is not null and PROD_SEGMENT_FK is null and CUST_ENTITY_STRUCTURE_FK is null and 1 = 2 ???

		Select top 1 * into #SS from epacube.epacube.segments_settings where data_name_fk = 144868 and ZONE_ENTITY_STRUCTURE_FK is not null and PROD_SEGMENT_FK is null and CUST_ENTITY_STRUCTURE_FK is null and 1 = 1

		Create index idx_01 on #SS(zone_segment_fk, data_name_fk)
		Create index idx_02 on #SS(zone_segment_fk, prod_segment_fk, data_name_fk)
		create index idx_01 on #zic(zone_Segment_FK, data_name_fk)

		Insert into #SS
		(Effective_Date, Prod_Data_Value_FK, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Data_Name_FK, Attribute_Number, Attribute_Y_N, Attribute_Event_Data, Precedence, Prod_Segment_FK, Zone_Segment_FK
		, Entity_Data_Name_FK, Record_Status_CR_FK, update_timestamp, create_timestamp, Job_FK)
		Select
		z.Effective_Date, Null 'Prod_Data_Value_FK', 1 'Org_Entity_Structure_FK', z.Zone_Segment_FK, z.Data_Name_FK, Case when isnumeric(z.new_value) = 1 then z.new_value else Null end 'Attribute_Number'
		, Case when isnumeric(z.new_value) = 0 then z.new_value else Null end 'Attribute_Y_N', z.new_value 'Attribute_Event_Data', dn.precedence, Null 'Prod_Segment_FK', z.Zone_Segment_FK 'Zone_Segment_FK'
		, 151000 'Entity_Data_Name_FK', 1 'Record_Status_CR_FK', z.Effective_Date 'update_timestamp', z.Effective_Date 'create_timestamp', @z_job_fk 'job_fk'
		from #Z Z
		inner join epacube.epacube.data_name dn on z.data_name_fk = dn.DATA_NAME_ID

		Insert into #SS
		(Effective_Date, Prod_Data_Value_FK, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Data_Name_FK, Attribute_Number, Attribute_Y_N, Attribute_Event_Data, Precedence, Prod_Segment_FK, Zone_Segment_FK
		, Entity_Data_Name_FK, Record_Status_CR_FK, update_timestamp, create_timestamp, Prod_Segment_Data_Name_FK, job_fk)
		Select distinct
		zic3.effective_date 'Effective3'
		, ic.Prod_Segment_FK3 'Prod_Data_Value_FK'
		, 1 'Org_Entity_Structure_FK'
		, zic3.Zone_Segment_FK
		, zic3.Data_Name_FK
		, Case when isnumeric(zic3.new_value) = 1 then zic3.new_value else Null end 'Attribute_Number'
		, Case when isnumeric(zic3.new_value) = 0 then zic3.new_value else Null end 'Attribute_Y_N'
		, zic3.new_value 'Attribute_Event_Data'
		, (select precedence from epacube.epacube.data_name where data_name_id = ic.data_name_fk3) 'Precedence'
		, ic.Prod_Segment_FK3 'Prod_Segment_FK'
		, zic3.Zone_Segment_FK 'Zone_Segment_FK'
		, 151000 Entity_Data_Name_FK
		, 1 'Record_Status_CR_FK'
		, zic3.Effective_Date 'update_timestamp'
		, zic3.Effective_Date 'create_timestamp'
		, ic.Data_Name_FK3
		, @zic_Job_FK 'Job_FK'
		from epacube.[precision].item_class_structure ic
		left join #ZIC ZIC3 on ic.Zone_Segment_FK = zic3.Zone_Segment_FK
				and zic3.Prod_Segment_Data_Name_FK = ic.data_name_fk3 and zic3.prod_segment_fk = ic.prod_segment_fk3
		left join #SS ss on zic3.Zone_Segment_FK = ss.zone_segment_fk and (zic3.data_name_fk = ss.data_name_fk or ss.data_name_fk is null)
		where 1 = 1
			and zic3.new_value <> isnull(ss.attribute_event_data, '99999')
	
		Insert into #SS
		(Effective_Date, Prod_Data_Value_FK, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Data_Name_FK, Attribute_Number, Attribute_Y_N, Attribute_Event_Data, Precedence, Prod_Segment_FK, Zone_Segment_FK
		, Entity_Data_Name_FK, Record_Status_CR_FK, update_timestamp, create_timestamp, Prod_Segment_Data_Name_FK, Job_FK)
		Select distinct
		zic4.effective_date 'Effective4'
		, ic.Prod_Segment_FK4 'Prod_Data_Value_FK'
		, 1 'Org_Entity_Structure_FK'
		, zic4.Zone_Segment_FK
		, zic4.Data_Name_FK
		, Case when isnumeric(zic4.new_value) = 1 then zic4.new_value else Null end 'Attribute_Number'
		, Case when isnumeric(zic4.new_value) = 0 then zic4.new_value else Null end 'Attribute_Y_N'
		, zic4.new_value 'Attribute_Event_Data'
		, (select precedence from epacube.epacube.data_name where data_name_id = ic.data_name_fk4) 'Precedence'
		, ic.Prod_Segment_FK4 'Prod_Segment_FK'
		, zic4.Zone_Segment_FK 'Zone_Segment_FK'
		, 151000 Entity_Data_Name_FK
		, 1 'Record_Status_CR_FK'
		, zic4.Effective_Date 'update_timestamp'
		, zic4.Effective_Date 'create_timestamp'
		, ic.data_name_fk4
		, @zic_Job_FK 'Job_FK'
		from epacube.[precision].item_class_structure ic
		left join #ZIC ZIC4 on ic.Zone_Segment_FK = zic4.Zone_Segment_FK
				and zic4.Prod_Segment_Data_Name_FK = ic.data_name_fk4 and zic4.prod_segment_fk = ic.prod_segment_fk4
		left join #ZIC ZIC3 on ic.Zone_Segment_FK = zic3.Zone_Segment_FK
				and zic3.Prod_Segment_Data_Name_FK = ic.data_name_fk3 and zic3.prod_segment_fk = ic.prod_segment_fk3 and zic4.data_name_fk = zic3.data_name_fk
		left join #SS ss on zic3.Zone_Segment_FK = ss.zone_segment_fk and (zic3.data_name_fk = ss.data_name_fk or ss.data_name_fk is null) and ss.prod_segment_fk is null
		where 1 = 1
			and zic4.new_value <> coalesce(zic3.new_value, isnull(ss.attribute_event_data, '99999'))

		Insert into #SS
		(Effective_Date, Prod_Data_Value_FK, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Data_Name_FK, Attribute_Number, Attribute_Y_N, Attribute_Event_Data, Precedence, Prod_Segment_FK, Zone_Segment_FK
		, Entity_Data_Name_FK, Record_Status_CR_FK, update_timestamp, create_timestamp, Prod_Segment_Data_Name_FK, Job_FK)
		Select distinct
		zic5.effective_date 'Effective5'
		, ic.Prod_Segment_FK5 'Prod_Data_Value_FK'
		, 1 'Org_Entity_Structure_FK'
		, zic5.Zone_Segment_FK
		, zic5.Data_Name_FK
		, Case when isnumeric(zic5.new_value) = 1 then zic5.new_value else Null end 'Attribute_Number'
		, Case when isnumeric(zic5.new_value) = 0 then zic5.new_value else Null end 'Attribute_Y_N'
		, zic5.new_value 'Attribute_Event_Data'
		, (select precedence from epacube.epacube.data_name where data_name_id = ic.data_name_fk5) 'Precedence'
		, ic.Prod_Segment_FK5 'Prod_Segment_FK'
		, zic5.Zone_Segment_FK 'Zone_Segment_FK'
		, 151000 Entity_Data_Name_FK
		, 1 'Record_Status_CR_FK'
		, zic5.Effective_Date 'update_timestamp'
		, zic5.Effective_Date 'create_timestamp'
		, ic.data_name_fk5
		, @zic_Job_FK 'Job_FK'
		from epacube.[precision].item_class_structure ic
		left join #ZIC ZIC5 on ic.Zone_Segment_FK = zic5.Zone_Segment_FK
				and zic5.Prod_Segment_Data_Name_FK = ic.data_name_fk5 and zic5.prod_segment_fk = ic.prod_segment_fk5
		left join #ZIC ZIC4 on ic.Zone_Segment_FK = zic4.Zone_Segment_FK
				and zic4.Prod_Segment_Data_Name_FK = ic.data_name_fk4 and zic4.prod_segment_fk = ic.prod_segment_fk4 and zic5.data_name_fk = zic4.data_name_fk
		left join #ZIC ZIC3 on ic.Zone_Segment_FK = zic3.Zone_Segment_FK
				and zic3.Prod_Segment_Data_Name_FK = ic.data_name_fk3 and zic3.prod_segment_fk = ic.prod_segment_fk3 and zic4.data_name_fk = zic3.data_name_fk
		left join #SS ss on zic3.Zone_Segment_FK = ss.zone_segment_fk and (zic3.data_name_fk = ss.data_name_fk or ss.data_name_fk is null) and ss.prod_segment_fk is null
		where 1 = 1
			and zic5.new_value <> coalesce(zic4.new_value, zic3.new_value, isnull(ss.attribute_event_data, '99999'))

		Insert into #SS
		(Effective_Date, Product_Structure_FK, Org_Entity_Structure_FK, Zone_Entity_Structure_FK, Data_Name_FK, Attribute_Number, Attribute_Y_N, Attribute_Event_Data, Precedence, Prod_Segment_FK, Zone_Segment_FK
		, Entity_Data_Name_FK, Record_Status_CR_FK, update_timestamp, create_timestamp, Prod_Segment_Data_Name_FK, Job_FK)
		Select distinct
		zi6.effective_date 'Effective6'
		, zi6.Prod_Segment_FK 'Product_Structure_FK'
		, 1 'Org_Entity_Structure_FK'
		, zi6.Zone_Segment_FK
		, zi6.Data_Name_FK
		, Case when isnumeric(zi6.new_value) = 1 then zi6.new_value else Null end 'Attribute_Number'
		, Case when isnumeric(zi6.new_value) = 0 then zi6.new_value else Null end 'Attribute_Y_N'
		, zi6.new_value 'Attribute_Event_Data'
		, (select precedence from epacube.epacube.data_name where data_name_id = 110103) 'Precedence'
		, zi6.Prod_Segment_FK 'Prod_Segment_FK'
		, zi6.Zone_Segment_FK 'Zone_Segment_FK'
		, 151000 Entity_Data_Name_FK
		, 1 'Record_Status_CR_FK'
		, zi6.Effective_Date 'update_timestamp'
		, zi6.Effective_Date 'create_timestamp'
		, 110103 'Prod_Segment_Data_Name_FK'
		, @zi_Job_FK 'Job_FK'
		from epacube.[precision].item_class_structure ic
		inner join epacube.epacube.segments_product sp with (nolock) on ic.[Prod_Segment_FK5] = sp.PROD_SEGMENT_FK and sp.data_name_fk = 501045
		inner join #Zi zi6 on sp.PRODUCT_STRUCTURE_FK = zi6.Prod_Segment_FK and ic.Zone_Segment_FK = zi6.Zone_Segment_FK
		left join #ZIC ZIC5 on ic.Zone_Segment_FK = zic5.Zone_Segment_FK
				and zic5.Prod_Segment_Data_Name_FK = ic.data_name_fk5 and zic5.prod_segment_fk = ic.prod_segment_fk5 and zi6.data_name_fk = zic5.data_name_fk
		left join #ZIC ZIC4 on ic.Zone_Segment_FK = zic4.Zone_Segment_FK
				and zic4.Prod_Segment_Data_Name_FK = ic.data_name_fk4 and zic4.prod_segment_fk = ic.prod_segment_fk4 and zic5.data_name_fk = zic4.data_name_fk
		left join #ZIC ZIC3 on ic.Zone_Segment_FK = zic3.Zone_Segment_FK
				and zic3.Prod_Segment_Data_Name_FK = ic.data_name_fk3 and zic3.prod_segment_fk = ic.prod_segment_fk3 and zic4.data_name_fk = zic3.data_name_fk
		left join #SS ss on zic3.Zone_Segment_FK = ss.zone_segment_fk and (zic3.data_name_fk = ss.data_name_fk or ss.data_name_fk is null) and ss.prod_segment_fk is null
		where ss.prod_segment_fk is null 
			and zi6.new_value not in ('0', '0.0', '0.00')
			and zi6.new_value <> coalesce(zic5.new_value, zic4.new_value, zic3.new_value, ss.attribute_event_data)

--/* 
	
	If (Select count(*) from #SS) > 100000
	Begin
		Delete SS from epacube.epacube.segments_settings ss
		--left join epacube.precision.entities_live el on ss.zone_segment_fk = el.entity_structure_fk
		where ss.ZONE_SEGMENT_FK is not null
		and data_name_fk in (select distinct data_name_fk from #SS)
		--and (@Maintain_Live_Entities = 0 or (@Maintain_Live_Entities = 1 and el.entities_live_id is null))

		Insert into epacube.epacube.segments_settings
		(
		DATA_LEVEL_CR_FK, PROD_DATA_VALUE_FK, CUST_DATA_VALUE_FK, RESULT_DATA_NAME_FK, ORG_ENTITY_STRUCTURE_FK, CUST_ENTITY_STRUCTURE_FK, VENDOR_ENTITY_STRUCTURE_FK, ZONE_TYPE_CR_FK, ZONE_ENTITY_STRUCTURE_FK, DATA_NAME_FK
		, ATTRIBUTE_CHAR, ATTRIBUTE_NUMBER, ATTRIBUTE_DATE, ATTRIBUTE_Y_N, DATA_VALUE_FK, ENTITY_DATA_VALUE_FK, ATTRIBUTE_EVENT_DATA, PRECEDENCE, RECORD_STATUS_CR_FK, UPDATE_TIMESTAMP, CREATE_TIMESTAMP, UPDATE_USER, CREATE_USER
		, PROD_SEGMENT_FK, CUST_SEGMENT_FK, ZONE_SEGMENT_FK, PRODUCT_STRUCTURE_FK, LOWER_LIMIT, UPPER_LIMIT, PRICE_TYPE_ENTITY_DATA_VALUE_FK, Effective_Date, ENTITY_DATA_NAME_FK, PROD_SEGMENT_DATA_NAME_FK, [DATA_SOURCE]
		, Job_FK
		)
		Select
		ss.DATA_LEVEL_CR_FK, ss.PROD_DATA_VALUE_FK, ss.CUST_DATA_VALUE_FK, ss.RESULT_DATA_NAME_FK, ss.ORG_ENTITY_STRUCTURE_FK, ss.CUST_ENTITY_STRUCTURE_FK, ss.VENDOR_ENTITY_STRUCTURE_FK, ss.ZONE_TYPE_CR_FK, ss.ZONE_ENTITY_STRUCTURE_FK, ss.DATA_NAME_FK
		, ss.ATTRIBUTE_CHAR, ss.ATTRIBUTE_NUMBER, ss.ATTRIBUTE_DATE, ss.ATTRIBUTE_Y_N, ss.DATA_VALUE_FK, ss.ENTITY_DATA_VALUE_FK, ss.ATTRIBUTE_EVENT_DATA, ss.PRECEDENCE, ss.RECORD_STATUS_CR_FK, ss.UPDATE_TIMESTAMP, ss.CREATE_TIMESTAMP, ss.UPDATE_USER, 'FULL ZONE REFRESH' CREATE_USER
		, ss.PROD_SEGMENT_FK, ss.CUST_SEGMENT_FK, ss.ZONE_SEGMENT_FK, ss.PRODUCT_STRUCTURE_FK, ss.LOWER_LIMIT, ss.UPPER_LIMIT, ss.PRICE_TYPE_ENTITY_DATA_VALUE_FK, ss.Effective_Date, ss.ENTITY_DATA_NAME_FK, ss.PROD_SEGMENT_DATA_NAME_FK, 'FULL ZONE REFRESH' 'DATA_SOURCE'
		, ss.Job_FK
		from #SS SS
		--left join epacube.precision.entities_live el on ss.zone_segment_fk = el.entity_structure_fk
		Where 1 = 1
		--and (@Maintain_Live_Entities = 0 or (@Maintain_Live_Entities = 1 and el.entities_live_id is null))
	End

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED)
	select 
	'IMPORTS.IMPORT_ZONE_ITEM_CLASS_FULL_SETTINGS_REFRESH' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ITEM_CLASS' 'Source_Table'
	, 'EPACUBE.EPACUBE.SEGMENTS_SETTINGS' 'TARGET_TABLE'
	, 'ITEM CLASS SETTINGS' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM #SS) 'RECORDS_LOADED'

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, Job_FK)
	select 
	'IMPORTS.IMPORT_ZONE_ITEM_CLASS_FULL_SETTINGS_REFRESH' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.ZONE' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.SEGMENTS_SETTINGS' 'TARGET_TABLE'
	, 'ZONE SETTINGS' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM #SS where job_fk = @z_job_fk) 'RECORDS_LOADED'
	, @z_job_fk 'Job_fk'

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, Job_FK)
	select 
	'IMPORTS.IMPORT_ZONE_ITEM_CLASS_FULL_SETTINGS_REFRESH' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.ZONE_ITEM_CLASS' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.SEGMENTS_SETTINGS' 'TARGET_TABLE'
	, 'ZONE ITEM CLASS SETTINGS' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM #SS where job_fk = @zic_job_fk) 'RECORDS_LOADED'
	, @zic_Job_FK 'Job_FK'

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, Job_FK)
	select 
	'IMPORTS.IMPORT_ZONE_ITEM_CLASS_FULL_SETTINGS_REFRESH' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'ZONE_Item' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.SEGMENTS_SETTINGS' 'TARGET_TABLE'
	, 'ZONE ITEM SETTINGS AT ITEM LEVEL' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM #SS where job_fk = @zi_Job_FK) 'RECORDS_LOADED'
	, @zi_Job_FK 'Job_FK'


	If Object_Id('tempdb..#Z') is not null
	Drop table #Z

	If Object_Id('tempdb..#ZIC') is not null
	Drop table #ZIC

	If Object_Id('tempdb..#ZI') is not null
	Drop table #ZI

	If Object_Id('tempdb..#SS') is not null
	Drop table #SS
--*/

 SET @status_desc =  'finished execution of IMPORT Zone Item Class Data'
 
 --Exec [AMAZONA-8NUFTI5].common.exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT Zone item class Data has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

   --      EXEC [AMAZONA-8NUFTI5].common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
   --      declare @exceptionRecipients varchar(1024);
		 --set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
			--								where name = 'DBExceptions'),'dbexceptions@epacube.com');
			--EXEC [AMAZONA-8NUFTI5].common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
   --      RAISERROR (@ErrorMessage, -- Message text.
			--	    @ErrorSeverity, -- Severity.
			--	    @ErrorState -- State.
			--	    );
   END CATCH;
