﻿





-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        04/27/2018   Initial Version 


CREATE PROCEDURE [IMPORTS].[IMPORT_URM_ZONE] ( @JOB_FK bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
     -- EXEC [AMAZONA-8NUFTI5].common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.URM_ZONE', 
					--							@exec_id = @l_exec_no OUTPUT,
					--							@epa_job_id = @JOB_FK;

     --SET @l_sysdate = GETDATE() --used for the beginning timestamp
     --SET @V_START_TIMESTAMP = getdate()  -- used in steps

--Add any missing zones, zone_names, and zone_types
--Change any values that are different than what is in epaCUBE

--Declare @JOB_FK bigint
--Set @JOB_FK = (select top 1 job_fk from [customer_imports].[IMPORTS].[URM_ZONE])

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, JOB_FK)
	select 
	'IMPORTS.[IMPORT_URM_ZONE]' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.ENTITY_STRUCTURE' 'TARGET_TABLE'
	, 'NEW ZONES' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM [customer_imports].[IMPORTS].[URM_ZONE] Zn left join epacube.epacube.entity_identification eiz with (nolock) on zn.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.DATA_NAME_FK = 151110 left join epacube.epacube.entity_ident_nonunique einz with (nolock) on eiz.ENTITY_STRUCTURE_FK = einz.entity_structure_fk and einz.data_name_fk = 151112 left join epacube.epacube.entity_data_value edv with (nolock) on eiz.entity_structure_fk = edv.ENTITY_STRUCTURE_FK and edv.data_name_fk = 502060 where 1 = 1 and eiz.ENTITY_IDENTIFICATION_ID is null and  zn.job_fk   =   @JOB_FK) 'RECORDS_LOADED'
	, @JOB_FK 'JOB_FK'
	 
	INSERT INTO epacube.[ENTITY_STRUCTURE]
	([ENTITY_STATUS_CR_FK]
	,[DATA_NAME_FK]
	,[LEVEL_SEQ]
	,[ENTITY_CLASS_CR_FK]
	,JOB_FK
	,[IMPORT_FILENAME]          
	,[RECORD_STATUS_CR_FK]
	,[CREATE_TIMESTAMP]
	)
	SELECT 10200,151000,1, 10117, 
	@JOB_FK, 
	zone_NUM,1,getdate()
	from [customer_imports].[IMPORTS].[URM_ZONE] Zn
	left join epacube.epacube.entity_identification eiz with (nolock) on zn.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.DATA_NAME_FK = 151110
	left join epacube.epacube.entity_ident_nonunique einz with (nolock) on eiz.ENTITY_STRUCTURE_FK = einz.entity_structure_fk and einz.data_name_fk = 151112
	left join epacube.epacube.entity_data_value edv with (nolock) on eiz.entity_structure_fk = edv.ENTITY_STRUCTURE_FK and edv.data_name_fk = 502060
	where 1 = 1
	--and isnull(zn.zone_type, 'C') <> 'C'
	and eiz.ENTITY_IDENTIFICATION_ID is null
	and  zn.job_fk = @JOB_FK

-----------------
--Zone Number
-------------------

	INSERT INTO epacube.[ENTITY_IDENTIFICATION]
    ([ENTITY_STRUCTURE_FK]
    ,[ENTITY_DATA_NAME_FK]
    ,[DATA_NAME_FK]
    ,[VALUE]
    ,[RECORD_STATUS_CR_FK]
    ,[CREATE_TIMESTAMP]
    ,[UPDATE_USER]
    )
	SELECT distinct es.entity_structure_id, 151000,	151110,   zone_NUM, 1, getdate(), 'ZONE_IMPORT'
	from [customer_imports].[IMPORTS].[URM_ZONE] Zn
	inner join epacube.epacube.entity_structure es on es.JOB_FK = zn.JOB_FK and es.import_filename = zn.zone_num
	left join epacube.epacube.entity_identification ei with (nolock) on es.entity_structure_id = ei.entity_structure_fk and ei.value  = zn.zone_num
	where 1 = 1
	--and isnull(zn.zone_type, 'C') <> 'C'
	and ei.entity_identification_id is null
	and  zn.job_fk   =   @JOB_FK
 
----------
-----ZONE NAME
-----------
	INSERT INTO epacube.[ENTITY_IDENT_NONUNIQUE]
	([ENTITY_STRUCTURE_FK]
	,[ENTITY_DATA_NAME_FK]
	,[DATA_NAME_FK]
	,[VALUE]
	,[RECORD_STATUS_CR_FK]
	,[CREATE_TIMESTAMP]
	)
	SELECT es.entity_structure_id, 151000,	151112,  zn.name, 1, getdate() 
	from [customer_imports].[IMPORTS].[URM_ZONE] Zn
	inner join epacube.epacube.entity_structure es on es.import_filename = zn.ZONE_NUM and es.JOB_FK = zn.JOB_FK
	left join epacube.epacube.entity_ident_NONUNIQUE eiN with (nolock) on es.entity_structure_id = ein.entity_structure_fk
	where 1 = 1
	--and isnull(zn.zone_type, 'C') <> 'C'
	and ein.entity_ident_nonunique_id is null
	and  zn.job_fk   =   @JOB_FK

------UPDATE NAME
--	update einz 
--	set value = zn.name
--		from [customer_imports].[IMPORTS].[URM_ZONE] Zn
--	inner join epacube.epacube.entity_identification eiz with (nolock) on zn.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.DATA_NAME_FK = 151110
--	inner join epacube.epacube.entity_ident_nonunique einz with (nolock) on eiz.ENTITY_STRUCTURE_FK = einz.entity_structure_fk and einz.data_name_fk = 151112
--	--inner join epacube.epacube.entity_data_value edv with (nolock) on eiz.entity_structure_fk = edv.ENTITY_STRUCTURE_FK and edv.data_name_fk = 502060
--	where 1 = 1
--	--and isnull(zn.zone_type, 'C') <> 'C'
--	and einz.value <> zn.name
 
---------
--Zone Type
----------

	Insert into epacube.epacube.data_value
	(data_name_fk, Value, Record_Status_CR_FK, Create_User)
	Select 502060, zn.zone_type, 1 record_status_cr_fk, zn.usr_crt
	from [customer_imports].[IMPORTS].[URM_ZONE] zn with (nolock) 
	where 1 = 1
	and zn.zone_type not in (select value from epacube.epacube.data_value where data_name_fk = 502060)
	and zn.job_fk = @JOB_FK

----UPDATE Changes
	update ea 
	set Attribute_Event_Data = zn.ZONE_TYPE
	, data_value_fk = dv.data_value_id
			   from [customer_imports].[IMPORTS].[URM_ZONE] Zn
	inner join epacube.epacube.entity_identification eiz with (nolock) on zn.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.DATA_NAME_FK = 151110
	inner join epacube.epacube.entity_ident_nonunique einz with (nolock) on eiz.ENTITY_STRUCTURE_FK = einz.entity_structure_fk and einz.data_name_fk = 151112
	inner join epacube.epacube.data_value dv with (nolock) on zn.ZONE_TYPE = dv.value and dv.data_name_fk = 502060
	inner join epacube.epacube.entity_attribute ea on eiz.ENTITY_STRUCTURE_FK = ea.entity_structure_fk and ea.DATA_NAME_FK = 502060
	where 1 = 1
	--and isnull(zn.zone_type, 'C') <> 'C'
	and dv.value <> zn.ZONE_TYPE
	and zn.job_fk = @JOB_FK

----ADD New Values
	Insert into epacube.epacube.entity_attribute
	(Entity_Structure_FK, Data_Name_FK, DATA_VALUE_FK, Attribute_Event_Data, Record_Status_CR_FK, Create_Timestamp)
	Select eiz.ENTITY_STRUCTURE_FK, 502060 'Data_Name_FK', dv.DATA_VALUE_ID, zn.zone_type, 1 'Record_Status_CR_FK', [epacube].[URM_DATE_TIME_CONVERSION](zn.DTE_CRT, zn.TME_CRT) 'Create_Timestamp'
	from [customer_imports].[IMPORTS].[URM_ZONE] Zn
	inner join epacube.epacube.entity_identification eiz with (nolock) on zn.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.DATA_NAME_FK = 151110
	inner join epacube.epacube.entity_ident_nonunique einz with (nolock) on eiz.ENTITY_STRUCTURE_FK = einz.entity_structure_fk and einz.data_name_fk = 151112
	inner join epacube.epacube.data_value dv with (nolock) on zn.ZONE_TYPE = dv.value and dv.data_name_fk = 502060
	left join epacube.epacube.entity_attribute ea on eiz.ENTITY_STRUCTURE_FK = ea.entity_structure_fk and ea.DATA_NAME_FK = 502060
	where 1 = 1
	--and isnull(zn.zone_type, 'C') <> 'C'
	and ea.ENTITY_ATTRIBUTE_ID is null
	and zn.job_fk = @JOB_FK

--/*
--Load remaining Zone_Level parameters for new zone only
--There is nothing new with the data below until you get the missing Zone 13 loaded first.
--*/

------Insure there is only 1 active record per data_name_fk/zone_segment_fk
--Update SS
--Set Record_status_cr_fk = 2
--from epacube.epacube.segments_settings SS where segments_settings_id in
--	(
--		select Segments_settings_id from (
--		select Segments_settings_id, data_name_fk, zone_entity_structure_fk, record_status_cr_fk
--		, DENSE_RANK()over(partition by zone_segment_fk, data_name_fk order by effective_date desc, update_timestamp desc) DRank
--		from epacube.epacube.segments_settings
--		where ZONE_ENTITY_STRUCTURE_FK is not null
--		and CUST_ENTITY_STRUCTURE_FK is null
--		and PROD_SEGMENT_FK is null
--		and record_status_cr_fk = 1
--		and cast(effective_date as date) <= cast(getdate() as date)
--		) a where DRank > 1
--	)

If object_id('tempdb..#Changes') is not null
drop table #Changes

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, JOB_FK)
	select 
	'IMPORTS.[IMPORT_URM_ZONE]' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.SEGMENTS_SETTINGS' 'TARGET_TABLE'
	, 'ZONE LEVEL SETTINGS' 'TYPE_OF_DATA'
	, (Select count(*) from (
			Select distinct 1 'Org_Entity_Structure_FK'
	, Zone_Entity_Structure_FK
	, Data_Name_FK
	, case isnumeric(New_Value) when 1 then New_Value end 'Attribute_Number'
	, case when isnumeric(New_Value) = 0 and New_Value in ('Y', 'N') then New_Value end 'Attribute_Y_N'
	--, case isnumeric(New_Value) when 0 then 1000059816 end 'Data_value_fk'
	, New_Value 'Attribute_Event_Data'
	, 1 'Record_Status_CR_FK', Zone_Entity_Structure_FK 'Zone_Segment_FK', Cast(getdate() as date) 'Effective_Date', 151000 'Entity_Data_Name_FK', 'System_Import' 'Update_User' 
	from 
		(Select A.*, eiz.entity_structure_fk 'Zone_Entity_Structure_FK', SS.Attribute_Event_Data 'Current_Value', ss.effective_date 'Current_Effective_Date'
			from 
			(
			Select zone_num, cast(Cast(cast(prcadj_d0 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144897 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d0 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d1 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144898 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d1 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d2 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144899 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d2 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d3 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144901 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d3 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d4 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144902 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d4 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d5 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144903 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d5 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d6 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144904 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d6 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d7 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144905 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d7 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d8 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144906 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d8 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d9 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144907 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d9 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, Prenotday, 144868 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(Prenotday as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, lg_flg, 144862 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(lg_flg, 'N') <> 'N' and job_fk = @JOB_FK Union
			Select zone_num, ReqChgAmt, 144908 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(ReqChgAmt as Numeric(18, 2)), 0.00) <> 0.0 and job_fk = @JOB_FK Union
			Select zone_num, LG_PP_FLG, 144876 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(LG_PP_FLG, 'N') <> 'N' and job_fk = @JOB_FK
			) A
			inner join epacube.epacube.entity_identification eiz with (nolock) on A.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.data_name_fk = 151110
			left join 
					(select * from 
						(select attribute_event_data, zone_entity_structure_fk, data_name_fk, effective_date 
						, dense_rank()over(partition by zone_entity_structure_fk, data_name_fk order by effective_date desc, segments_settings_id desc) DRank
							from epacube.epacube.segments_settings ss with (nolock)
							where PROD_SEGMENT_FK is null
								and CUST_ENTITY_STRUCTURE_FK is null
								and 
									(data_name_fk between 144897 and 144899
									or data_name_fk between 144901 and 144908
									or data_name_fk in (144868, 144862, 144876)
									)
						) b 
						where DRank = 1
					) ss on eiz.ENTITY_STRUCTURE_FK = ss.ZONE_ENTITY_STRUCTURE_FK and a.data_name_fk = ss.data_name_fk and ss.Effective_Date <= cast(getdate() as date) 
	where isnull(a.new_value, '') <> isnull(ss.attribute_event_data, '')
	) B where 1=1
		) C
		) 'RECORDS_LOADED'
	, @JOB_FK 'JOB_FK'

	Select distinct 1 'Org_Entity_Structure_FK'
	, Zone_Entity_Structure_FK
	, Data_Name_FK
	, case isnumeric(New_Value) when 1 then New_Value end 'Attribute_Number'
	, case when isnumeric(New_Value) = 0 and New_Value in ('Y', 'N') then New_Value end 'Attribute_Y_N'
	--, case isnumeric(New_Value) when 0 then 1000059816 end 'Data_value_fk'
	, New_Value 'Attribute_Event_Data'
	, 1 'Record_Status_CR_FK', Zone_Entity_Structure_FK 'Zone_Segment_FK', Cast(getdate() as date) 'Effective_Date', 151000 'Entity_Data_Name_FK', 'System_Import' 'Update_User' 
	into #Changes
	from 
		(Select A.*, eiz.entity_structure_fk 'Zone_Entity_Structure_FK', SS.Attribute_Event_Data 'Current_Value', ss.effective_date 'Current_Effective_Date'
			from 
			(
			Select zone_num, cast(Cast(cast(prcadj_d0 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144897 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d0 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d1 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144898 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d1 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d2 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144899 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d2 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d3 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144901 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d3 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d4 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144902 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d4 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d5 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144903 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d5 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d6 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144904 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d6 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d7 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144905 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d7 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d8 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144906 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d8 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, cast(Cast(cast(prcadj_d9 as Numeric(18, 2)) / 100 as Numeric(18, 2)) as varchar(64)) 'New_Value', 144907 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(prcadj_d9 as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, Prenotday, 144868 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(Prenotday as Numeric(18, 2)), 0.00) <> 0.00 and job_fk = @JOB_FK Union
			Select zone_num, lg_flg, 144862 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(lg_flg, 'N') <> 'N' and job_fk = @JOB_FK Union
			Select zone_num, ReqChgAmt, 144908 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(cast(ReqChgAmt as Numeric(18, 2)), 0.00) <> 0.0 and job_fk = @JOB_FK Union
			Select zone_num, LG_PP_FLG, 144876 'data_name_fk' from [customer_imports].[IMPORTS].[URM_ZONE] where isnull(LG_PP_FLG, 'N') <> 'N' and job_fk = @JOB_FK
			) A
			inner join epacube.epacube.entity_identification eiz with (nolock) on A.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.data_name_fk = 151110
			left join 
					(select * from 
						(select attribute_event_data, zone_entity_structure_fk, data_name_fk, effective_date 
						, dense_rank()over(partition by zone_entity_structure_fk, data_name_fk order by effective_date desc, segments_settings_id desc) DRank
							from epacube.epacube.segments_settings ss with (nolock)
							where PROD_SEGMENT_FK is null
								and CUST_ENTITY_STRUCTURE_FK is null
								and 
									(data_name_fk between 144897 and 144899
									or data_name_fk between 144901 and 144908
									or data_name_fk in (144868, 144862, 144876)
									)
						) b 
						where DRank = 1
					) ss on eiz.ENTITY_STRUCTURE_FK = ss.ZONE_ENTITY_STRUCTURE_FK and a.data_name_fk = ss.data_name_fk and ss.Effective_Date <= cast(getdate() as date) 
	where isnull(a.new_value, '') <> isnull(ss.attribute_event_data, '')
	) B where 1=1


Insert into epacube.epacube.segments_settings
(ORG_ENTITY_STRUCTURE_FK, ZONE_ENTITY_STRUCTURE_FK, DATA_NAME_FK, ATTRIBUTE_NUMBER, ATTRIBUTE_Y_N, ATTRIBUTE_EVENT_DATA, RECORD_STATUS_CR_FK, zone_segment_fk, Effective_Date, ENTITY_DATA_NAME_FK, update_user, [DATA_SOURCE])
Select distinct ORG_ENTITY_STRUCTURE_FK, ZONE_ENTITY_STRUCTURE_FK, DATA_NAME_FK, ATTRIBUTE_NUMBER, ATTRIBUTE_Y_N, ATTRIBUTE_EVENT_DATA, RECORD_STATUS_CR_FK, zone_segment_fk, Effective_Date, ENTITY_DATA_NAME_FK, update_user, 'DAILY UPDATE' 'DATA_SOURCE'
from #Changes

If object_id('tempdb..#Changes') is not null
drop table #Changes
------------------------------------------------------------------------------------------------



--------------------------------------------------------------------------------------------------

  --    SELECT @v_input_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @JOB_FK;

  --    SELECT @v_output_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @JOB_FK
		--AND PRODUCT_STRUCTURE_FK is NOT NULL ;

	 -- SET @v_exception_rows = @v_input_rows - @v_output_rows;
  --    SET @V_END_TIMESTAMP = getdate()
  --    EXEC common.job_execution_create  @JOB_FK, 'IMPORT_COMPLETE',
		--                                 @v_input_rows, @v_output_rows, @v_exception_rows,
		--								 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



 SET @status_desc =  'finished execution of IMPORT Zone Data'
 
 --Exec [AMAZONA-8NUFTI5].common.exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @JOB_FK;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT Competitor Retail Data has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         --EXEC [AMAZONA-8NUFTI5].common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @JOB_FK;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			--EXEC [AMAZONA-8NUFTI5].common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END





