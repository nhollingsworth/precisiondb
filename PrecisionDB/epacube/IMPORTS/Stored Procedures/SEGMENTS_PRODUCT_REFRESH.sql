﻿
-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        05/04/2018   Initial Version 


CREATE PROCEDURE [IMPORTS].[SEGMENTS_PRODUCT_REFRESH]  
AS  

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint


SET NOCOUNT ON;

BEGIN TRY

		Delete UIC from [IMPORTS].[URM_ITEM_CLASS] UIC
		Inner join 
			(Select Job_FK from (	select job_fk, cast(import_timestamp as date) dt, count(*) Recs, Rank()over(order by job_fk desc) 'Rnk'
									from [IMPORTS].[URM_ITEM_CLASS] with (nolock)
									group by job_fk, cast(import_timestamp as date)
									) A where Rnk > 10
			) b ON UIC.JOB_FK = B.JOB_FK


	  DECLARE @Job_FK bigint 
      SET @l_exec_no = 0;
     -- EXEC [AMAZONA-8NUFTI5].common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of SEGMENTS PRODUCT REFRESH', 
					--							@exec_id = @l_exec_no OUTPUT,
					--							@epa_job_id = @Job_FK;

     --SET @l_sysdate = GETDATE() --used for the beginning timestamp
     --SET @V_START_TIMESTAMP = getdate()  -- used in steps

		If object_id('tempdb..#SP') is not null
		drop table #SP

		If object_id('tempdb..#UIC') is not null
		drop table #UIC

		If object_id('tempdb..#Dups') is not null
		drop table #Dups

		Set @Job_FK = (Select top 1 Job_fk from [IMPORTS].[URM_ITEM_CLASS] where cast(IMPORT_TIMESTAMP as date) = cast(getdate() as date) order by IMPORT_TIMESTAMP desc)
		
		Select distinct [LVLID_ITM], [CLSID_ITM], [DESC_30A], [LVLID_PAR], [CLSPARITM], [LVLID_NEW], [CLSIDITMN], [DTE_CRT], [TME_CRT], [USR_CRT], [DTE_UPD], [TME_UPD], [USR_UPD], [Job_FK] 
		into #UIC 
		from [IMPORTS].[URM_ITEM_CLASS] where Job_FK = @Job_FK

		Create index idx_60 on #UIC(LVLID_ITM, clsid_itm)

		Select top 1 * into #SP from epacube.epacube.segments_product where 1 = 2

		alter table #sp drop column [Segments_product_id]

		Insert into #SP
		(PRODUCT_STRUCTURE_FK, Data_Name_FK, Data_Value_FK, Prod_Segment_FK, Org_Entity_Structure_FK, Product_Precedence, RECORD_STATUS_CR_FK)
		Select pi.PRODUCT_STRUCTURE_FK, pi.Data_Name_FK, Null 'Data_Value_FK', pi.Product_Structure_FK 'Prod_Segment_FK', 1 'Org_Entity_Structure_FK', dn.Precedence 'Product_Precedence', 1 'RECORD_STATUS_CR_FK'
		from #UIC IC60 with (nolock)
		inner join epacube.epacube.product_identification pi with (nolock) on ic60.clsid_itm = pi.value and pi.data_name_fk = 110103
		inner join epacube.epacube.data_name dn with (nolock) on pi.data_name_fk = dn.data_name_id
		where ic60.LVLID_ITM = '60'

		Insert into #SP
		(PRODUCT_STRUCTURE_FK, Data_Name_FK, Data_Value_FK, Prod_Segment_FK, Org_Entity_Structure_FK, Product_Precedence, RECORD_STATUS_CR_FK)
		Select pi.PRODUCT_STRUCTURE_FK, dv.Data_Name_FK, dv.DATA_VALUE_ID 'Data_Value_FK', dv.DATA_VALUE_ID 'Prod_Segment_FK', 1 'Org_Entity_Structure_FK', dn.Precedence 'Product_Precedence', 1 'RECORD_STATUS_CR_FK'
		from #UIC IC60 with (nolock)
		inner join epacube.epacube.product_identification pi with (nolock) on ic60.clsid_itm = pi.value and pi.data_name_fk = 110103
		inner join #UIC IC50 with (nolock) on IC60.lvlid_par = ic50.lvlid_itm and ic60.CLSPARITM = ic50.clsid_itm
		inner join epacube.epacube.data_value dv with (nolock) on ic50.clsid_itm = dv.value and dv.data_name_fk = 501045 and dv.display_seq = '50'
		inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
		where ic60.LVLID_ITM = '60'

		Insert into #SP
		(PRODUCT_STRUCTURE_FK, Data_Name_FK, Data_Value_FK, Prod_Segment_FK, Org_Entity_Structure_FK, Product_Precedence, RECORD_STATUS_CR_FK)
		Select pi.PRODUCT_STRUCTURE_FK, dv.Data_Name_FK, dv.DATA_VALUE_ID 'Data_Value_FK', dv.DATA_VALUE_ID 'Prod_Segment_FK', 1 'Org_Entity_Structure_FK', dn.Precedence 'Product_Precedence', 1 'RECORD_STATUS_CR_FK'
		from #UIC IC60 with (nolock)
		inner join epacube.epacube.product_identification pi with (nolock) on ic60.clsid_itm = pi.value and pi.data_name_fk = 110103
		inner join #UIC IC50 with (nolock) on IC60.lvlid_par = ic50.lvlid_itm and ic60.CLSPARITM = ic50.clsid_itm
		inner join #UIC IC40 with (nolock) on IC50.lvlid_par = ic40.lvlid_itm and ic50.CLSPARITM = ic40.clsid_itm
		inner join epacube.epacube.data_value dv with (nolock) on ic40.clsid_itm = dv.value and dv.data_name_fk = 501044 and dv.display_seq = '40'
		inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
		where ic60.LVLID_ITM = '60'

		Insert into #SP
		(PRODUCT_STRUCTURE_FK, Data_Name_FK, Data_Value_FK, Prod_Segment_FK, Org_Entity_Structure_FK, Product_Precedence, RECORD_STATUS_CR_FK)
		Select pi.PRODUCT_STRUCTURE_FK, dv.Data_Name_FK, dv.DATA_VALUE_ID 'Data_Value_FK', dv.DATA_VALUE_ID 'Prod_Segment_FK', 1 'Org_Entity_Structure_FK', dn.Precedence 'Product_Precedence', 1 'RECORD_STATUS_CR_FK'
		from #UIC IC60 with (nolock)
		inner join epacube.epacube.product_identification pi with (nolock) on ic60.clsid_itm = pi.value and pi.data_name_fk = 110103
		inner join #UIC IC50 with (nolock) on IC60.lvlid_par = ic50.lvlid_itm and ic60.CLSPARITM = ic50.clsid_itm
		inner join #UIC IC40 with (nolock) on IC50.lvlid_par = ic40.lvlid_itm and ic50.CLSPARITM = ic40.clsid_itm
		inner join #UIC IC30 with (nolock) on IC40.lvlid_par = ic30.lvlid_itm and ic40.CLSPARITM = ic30.clsid_itm
		inner join epacube.epacube.data_value dv with (nolock) on ic30.clsid_itm = dv.value and dv.data_name_fk = 501043 and dv.display_seq = '30'
		inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
		where ic60.LVLID_ITM = '60'

		Insert into #SP
		(PRODUCT_STRUCTURE_FK, Data_Name_FK, Data_Value_FK, Prod_Segment_FK, Org_Entity_Structure_FK, Product_Precedence, RECORD_STATUS_CR_FK)
		Select pi.PRODUCT_STRUCTURE_FK, dv.Data_Name_FK, dv.DATA_VALUE_ID 'Data_Value_FK', dv.DATA_VALUE_ID 'Prod_Segment_FK', 1 'Org_Entity_Structure_FK', dn.Precedence 'Product_Precedence', 1 'RECORD_STATUS_CR_FK'
		from #UIC IC60 with (nolock)
		inner join epacube.epacube.product_identification pi with (nolock) on ic60.clsid_itm = pi.value and pi.data_name_fk = 110103
		inner join #UIC IC50 with (nolock) on IC60.lvlid_par = ic50.lvlid_itm and ic60.CLSPARITM = ic50.clsid_itm
		inner join #UIC IC40 with (nolock) on IC50.lvlid_par = ic40.lvlid_itm and ic50.CLSPARITM = ic40.clsid_itm
		inner join #UIC IC30 with (nolock) on IC40.lvlid_par = ic30.lvlid_itm and ic40.CLSPARITM = ic30.clsid_itm
		inner join #UIC IC20 with (nolock) on IC30.lvlid_par = ic20.lvlid_itm and ic30.CLSPARITM = ic20.clsid_itm
		inner join epacube.epacube.data_value dv with (nolock) on ic20.clsid_itm = dv.value and dv.data_name_fk = 501042 and dv.display_seq = '20'
		inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
		where ic60.LVLID_ITM = '60'

		Insert into #SP
		(PRODUCT_STRUCTURE_FK, Data_Name_FK, Data_Value_FK, Prod_Segment_FK, Org_Entity_Structure_FK, Product_Precedence, RECORD_STATUS_CR_FK)
		Select pi.PRODUCT_STRUCTURE_FK, dv.Data_Name_FK, dv.DATA_VALUE_ID 'Data_Value_FK', dv.DATA_VALUE_ID 'Prod_Segment_FK', 1 'Org_Entity_Structure_FK', dn.Precedence 'Product_Precedence', 1 'RECORD_STATUS_CR_FK'
		from #UIC IC60 with (nolock)
		inner join epacube.epacube.product_identification pi with (nolock) on ic60.clsid_itm = pi.value and pi.data_name_fk = 110103
		inner join #UIC IC50 with (nolock) on IC60.lvlid_par = ic50.lvlid_itm and ic60.CLSPARITM = ic50.clsid_itm
		inner join #UIC IC40 with (nolock) on IC50.lvlid_par = ic40.lvlid_itm and ic50.CLSPARITM = ic40.clsid_itm
		inner join #UIC IC30 with (nolock) on IC40.lvlid_par = ic30.lvlid_itm and ic40.CLSPARITM = ic30.clsid_itm
		inner join #UIC IC20 with (nolock) on IC30.lvlid_par = ic20.lvlid_itm and ic30.CLSPARITM = ic20.clsid_itm
		inner join #UIC IC10 with (nolock) on IC20.lvlid_par = ic10.lvlid_itm and ic20.CLSPARITM = ic10.clsid_itm
		inner join epacube.epacube.data_value dv with (nolock) on ic10.clsid_itm = dv.value and dv.data_name_fk = 501041 and dv.display_seq = '10'
		inner join epacube.epacube.data_name dn with (nolock) on dv.data_name_fk = dn.data_name_id
		where ic60.LVLID_ITM = '60'

		create index idx_sp on #sp(data_name_fk, product_structure_fk, prod_segment_fk)

		If (Select count(*) from #SP) > 10000
		Begin
			Delete sp
			from epacube.epacube.segments_product sp
			left join #sp spt on sp.product_structure_fk = spt.product_structure_fk and sp.data_name_fk = spt.data_name_fk and sp.prod_segment_fk = spt.prod_segment_fk
			where spt.record_status_cr_fk is null

			Insert into epacube.epacube.segments_product
			(PRODUCT_STRUCTURE_FK, Data_Name_FK, Data_Value_FK, Prod_Segment_FK, Org_Entity_Structure_FK, Product_Precedence, RECORD_STATUS_CR_FK)
			Select spt.PRODUCT_STRUCTURE_FK, spt.Data_Name_FK, spt.Data_Value_FK, spt.Prod_Segment_FK, spt.Org_Entity_Structure_FK, spt.Product_Precedence, spt.RECORD_STATUS_CR_FK
			from #SP spt
			left join epacube.epacube.segments_product sp with (nolock) on sp.product_structure_fk = spt.product_structure_fk and sp.data_name_fk = spt.data_name_fk and sp.prod_segment_fk = spt.prod_segment_fk
			where sp.segments_product_id is null
			order by spt.data_name_fk, spt.prod_segment_fk

			Update PRC
			set SubGroup_Data_Value_FK = dv.data_value_id
			from epacube.marginmgr.PRICESHEET_RETAIL_CHANGES prc
			inner join epacube.epacube.data_value dv on prc.SUBGROUP_ID = dv.value and dv.DATA_NAME_FK = 501045

			UPdate PRC
			set prc.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
			, prod_segment_fk = case prc.prod_segment_data_name_fk when 110103 then pi.PRODUCT_STRUCTURE_FK else PROD_SEGMENT_FK end
			from epacube.marginmgr.PRICESHEET_RETAIL_CHANGES prc
			inner join epacube.epacube.product_identification pi on prc.ITEM = pi.value and pi.DATA_NAME_FK = 110100

			Update PRC
			Set Zone_Entity_Structure_FK = eiz.entity_structure_fk
			from epacube.marginmgr.PRICESHEET_RETAIL_CHANGES prc with (nolock) 
			inner join epacube.epacube.entity_identification eiz with (nolock) on prc.zone = eiz.value and eiz.entity_data_name_fk = 151000 and eiz.data_name_fk = 151110

			Update PR
			Set Retail_Source_FK = eiz.entity_structure_fk
			, ZONE_ENTITY_STRUCTURE_FK = eiz.entity_structure_fk
			from epacube.marginmgr.PRICESHEET_RETAILS PR
			inner join epacube.epacube.entity_identification eiz with (nolock) on pr.retail_source = eiz.value and eiz.entity_data_name_fk = 151000 and eiz.data_name_fk = 151110
		END

		select segments_product_id, product_structure_fk, data_name_fk, PROD_SEGMENT_FK
		, Dense_Rank()over(partition by product_structure_fk, data_name_fk, PROD_SEGMENT_FK order by segments_product_id desc) 'DRank'
		into #Dups
		from epacube.epacube.segments_product 
		order by data_name_fk

		create index idx_01 on #Dups(segments_product_ID)

		delete sp
		from #Dups D
		inner join epacube.epacube.SEGMENTS_PRODUCT sp on D.SEGMENTS_PRODUCT_ID = sp.SEGMENTS_PRODUCT_ID
		where D.DRank <> 1

	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, Job_FK)
	select 
	'IMPORTS.SEGMENTS_PRODUCT_REFRESH' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ITEM_CLASS' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.SEGMENTS_PRODUCT' 'TARGET_TABLE'
	, 'ALL PRODUCT SEGMENTS' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM #SP) 'RECORDS_LOADED'
	, @Job_FK

 SET @status_desc =  'finished execution of IMPORT SEGMENTS PRODUCT REFRESH'
 
 --EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @Job_FK;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT SEGMENTS PRODUCT REFRESH has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         --EXEC [AMAZONA-8NUFTI5].common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @Job_FK;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			--EXEC [AMAZONA-8NUFTI5].common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
