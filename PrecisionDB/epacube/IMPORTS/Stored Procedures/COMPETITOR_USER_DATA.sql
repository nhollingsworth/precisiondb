﻿





-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        04/17/2018   Initial Version 
-- CV        06/03/2019   Convert File name to user


CREATE PROCEDURE [IMPORTS].[COMPETITOR_USER_DATA] ( @IN_JOB_FK bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
     -- EXEC [AMAZONA-8NUFTI5].common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.Completitor_User_DATA', 
					--							@exec_id = @l_exec_no OUTPUT,
					--							@epa_job_id = @in_job_fk;

     --SET @l_sysdate = GETDATE() --used for the beginning timestamp
     --SET @V_START_TIMESTAMP = getdate()  -- used in steps


-------------------------------------------------------------------------------------

--select   * from [IMPORTS].[URM_COMPETITOR_USER_FILES]  with (nolock)

--select * from  [IMPORTS].[URM_COMPETITOR_USER_FILES] RD
--inner join epacube.epacube.PRODUCT_IDENTIFICATION PI on pi.value = RD.URM_ITEM_SURROGATE

--select * from epacube.epacube.product_gtin


UPDATE C
set Comp_User = SUBSTRING(comp_user,2,CHARINDEX('I',comp_user,2)-2)
from [IMPORTS].[URM_COMPETITOR_USER_FILES] C
 where JOB_FK = @IN_JOB_FK


UPDATE RD
set product_structure_fk = PI.product_structure_fk
from [IMPORTS].[URM_COMPETITOR_USER_FILES] RD
inner join epacube.epacube.PRODUCT_IDENTIFICATION PI on pi.value = cast(RD.URM_ITEM_SURROGATE as bigint)
And pi.data_name_fk = 110103
and rd.Product_Structure_fk is null


UPDATE C
set Comp_entity_structure_fk = ei.entity_structure_fk
from [IMPORTS].[URM_COMPETITOR_USER_FILES] C
 inner join epacube.epacube.entity_identification ei on ei.value  = c.competitor_code
 and ei.entity_DATA_NAME_FK = 544010

---------------------------------------------------------------------------------------------------------------------------
----This query needs to be run for loading data 11 times, once for each of the possible cps.seq_num3 values 1 thru 11
---------------------------------------------------------------------------------------------------------------------------
--select * from epacube.[COMPETITOR_VALUES]


INSERT INTO epacube.[COMPETITOR_VALUES]
           ([DATA_SOURCE]
           ,[COMP_ENTITY_STRUCTURE_FK]
           ,[PRODUCT_STRUCTURE_FK]
           ,[ENTITY_DATA_VALUE_FK]
            ,[SIZE_DESCRIPTION]
           ,[ITEM_DESCRIPTION]
           ,[PRICE_MULTIPLE]
           ,[PRICE]
           ,[PRICE_TYPE]
           --,[CHANGE_INDICATOR]
           --,[UPC]
           ,[EFFECTIVE_DATE]
           ,[CREATE_TIMESTAMP]
           ,[CREATE_USER])
  select  'USER DATA', Comp_Entity_Structure_fk, u.Product_Structure_fk
  ,1000058613   -- Entity_Data_Value_FK (data_name_fk = 502041, data_value_fk = 1000058613  ALL CUSTOMERS?) 
 , pa.ATTRIBUTE_EVENT_DATA
 ,pd.description
  ,Price_Multiple
 ,round(cast(Price_Amount as float) /100,4 )
 ,Price_Type_Code, getdate(), getdate(), u.Comp_User
   from [IMPORTS].[URM_COMPETITOR_USER_FILES] U  with (nolock)
  left join epacube.epacube.product_description pd  on pd.PRODUCT_STRUCTURE_FK = u.Product_Structure_fk and pd.DATA_NAME_FK = 110401
  left join epacube.epacube.PRODUCT_ATTRIBUTE pa on pa.PRODUCT_STRUCTURE_FK = u.Product_Structure_fk and pa.DATA_NAME_FK =  500002
   where U.product_structure_fk is not NULL
   and U.comp_entity_structure_fk is not NULL 
   and U.job_fk =  @in_job_fk

EXEC IMPORTS.COMPETITOR_ALIAS_VALUES_INTERPOLATION  
------------------------------------------------------------------------------------------------



--------------------------------------------------------------------------------------------------

  --    SELECT @v_input_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk;

  --    SELECT @v_output_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk
		--AND PRODUCT_STRUCTURE_FK is NOT NULL ;

	 -- SET @v_exception_rows = @v_input_rows - @v_output_rows;
  --    SET @V_END_TIMESTAMP = getdate()
  --    EXEC common.job_execution_create  @in_job_fk, 'IMPORT_COMPLETE',
		--                                 @v_input_rows, @v_output_rows, @v_exception_rows,
		--								 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



 SET @status_desc =  'finished execution of IMPORT.Import_competitor_User_data'
 
 --Exec [AMAZONA-8NUFTI5].common.exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT.Competitor User Data has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         --EXEC [AMAZONA-8NUFTI5].common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			--EXEC [AMAZONA-8NUFTI5].common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END





