﻿






-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person		Date        Comments
-- ---------	----------  -------------------------------------------------
-- CV			04/27/2018  Initial Version 
-- GHS			4/15/19		Revised for SSIS etl


CREATE PROCEDURE [IMPORTS].[IMPORT_ITEM_CLASS] @Job_FK bigint
AS  

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint
	 DECLARE @in_job_fk  bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
     -- EXEC [AMAZONA-8NUFTI5].common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.URM_ITEM_CLASS', 
					--							@exec_id = @l_exec_no OUTPUT,
					--							@epa_job_id = @in_job_fk;

     --SET @l_sysdate = GETDATE() --used for the beginning timestamp
     --SET @V_START_TIMESTAMP = getdate()  -- used in steps


-------------------------------------------------------------------------------------
----------URM_ITEM_CLASS Start

--*****	URM_ITEM_CLASS must be the first import of each day - before any other item level data is loaded.

--Load all new values from URM_ITEM_CLASS into epacube.data_values
--Then set the Parent_Data_Value_FK column in each new record with an update statement

--declare @job_fk bigint = 5613

	If Object_id('tempdb..#UIC_New') is not null
	drop table #UIC_New

	If object_id('tempdb..#UIC') is not null
	drop table #UIC

	Select distinct
	[LVLID_ITM], [CLSID_ITM], [DESC_30A], [LVLID_PAR], [CLSPARITM], [LVLID_NEW], [CLSIDITMN], [DTE_CRT], [TME_CRT], [USR_CRT], [DTE_UPD], [TME_UPD], [USR_UPD], [Job_FK]
	into #UIC 
	from [IMPORTS].[URM_ITEM_CLASS]
	where job_FK = @Job_FK

	Create index idx_60 on #UIC(LVLID_ITM, clsid_itm)

	Select distinct UIC.* 
	into #UIC_New
	from #UIC UIC
	left join epacube.epacube.data_value DV ON UIC.LVLID_ITM = DV.DISPLAY_SEQ AND UIC.CLSID_ITM = DV.VALUE and DV.DATA_NAME_FK BETWEEN 501041 AND 501045
	WHERE 1 = 1
	and isnull(uic.lvlid_itm, '0') not in ('0', '60')
	AND DV.DATA_VALUE_ID IS NULL

	Insert into epacube.epacube.data_value
	(data_name_fk, value, [description], display_seq, create_timestamp, create_user, record_status_cr_fk)
	Select
	case uic.lvlid_itm when '10' then 501041 when '20' then 501042 when '30' then 501043 when '40' then 501044 when '50' then 501045 end Data_Name_FK
	, uic.clsid_itm 'Value'
	, UIC.[DESC_30A] 'Description'
	, uic.lvlid_itm 'Display_Seq'
	, epacube.[URM_DATE_TIME_CONVERSION](uic.dte_crt, tme_crt) 'Create_timestamp'
	, uic.usr_crt 'Create_User'
	, 1 'Record_Status_CR_FK'
	from #UIC_New UIC

	Update dv_child
	Set PARENT_DATA_VALUE_FK = dv_parent.data_value_id
	from #UIC_New UIC
	inner join epacube.epacube.data_value DV_child ON UIC.LVLID_ITM = DV_child.DISPLAY_SEQ AND UIC.CLSID_ITM = DV_child.VALUE and dv_child.DATA_NAME_FK BETWEEN 501041 AND 501045
	inner join epacube.epacube.data_value dv_parent on uic.lvlid_par = dv_parent.display_seq and uic.clsparitm = dv_parent.value and dv_parent.DATA_NAME_FK BETWEEN 501041 AND 501045
	where 1 = 1
	and isnull(dv_child.PARENT_DATA_VALUE_FK, 0) <> dv_parent.data_value_id

--Update any descriptions that have changes
	Update DV
	Set [DESCRIPTION] = UIC.[DESC_30A]
	, [Update_timestamp] = epacube.[URM_DATE_TIME_CONVERSION](uic.dte_upd, tme_upd) 
	, [Update_User] = uic.usr_upd
	from #UIC UIC
	INNER join epacube.epacube.data_value DV ON UIC.LVLID_ITM = DV.DISPLAY_SEQ AND UIC.CLSID_ITM = DV.VALUE and DV.DATA_NAME_FK BETWEEN 501041 AND 501045
	WHERE 1 = 1
	AND DV.[DESCRIPTION] <> UIC.[DESC_30A]

	Insert into epacube.epacube.segments_product
	(PRODUCT_STRUCTURE_FK, data_name_fk, data_value_fk, prod_segment_fk, product_precedence, RECORD_STATUS_CR_FK)
	select 
	pi.product_structure_fk, dv.data_name_fk, dv.data_value_id, dv.data_value_id, dn.precedence, 1 'record_status_cr_fk'
	from #UIC uic
	inner join epacube.epacube.PRODUCT_IDENTIFICATION pi with (nolock) on uic.CLSID_ITM = pi.value and pi.DATA_NAME_FK = 110103
	inner join epacube.epacube.data_value dv with (nolock) on uic.clsparitm = dv.value and dv.DISPLAY_SEQ = 50
	inner join epacube.epacube.data_name dn with (nolock) on dv.DATA_NAME_FK = dn.data_name_id
	left join epacube.epacube.segments_product sp with (nolock) on pi.product_structure_fk = sp.product_structure_fk and sp.DATA_NAME_FK = 501045

	where 1 = 1
	--and uic.job_fk = @Job_FK
	and uic.lvlid_itm = 60
	and sp.segments_product_id is null

	Insert into epacube.epacube.segments_product
	(PRODUCT_STRUCTURE_FK, data_name_fk, data_value_fk, prod_segment_fk, product_precedence, RECORD_STATUS_CR_FK)
	select 
	pi.product_structure_fk, dv4.data_name_fk, dv4.data_value_id, dv4.data_value_id, dn.precedence, 1 'record_status_cr_fk'
	from #UIC uic
	inner join epacube.epacube.PRODUCT_IDENTIFICATION pi with (nolock) on uic.CLSID_ITM = pi.value and pi.DATA_NAME_FK = 110103
	inner join epacube.epacube.data_value dv with (nolock) on uic.clsparitm = dv.value and dv.DISPLAY_SEQ = 50
	inner join epacube.epacube.data_value dv4 with (nolock) on dv.PARENT_DATA_VALUE_FK = dv4.DATA_VALUE_ID and dv4.DATA_NAME_FK = 501044
	inner join epacube.epacube.data_name dn with (nolock) on dv4.DATA_NAME_FK = dn.data_name_id
	left join epacube.epacube.segments_product sp with (nolock) on pi.product_structure_fk = sp.product_structure_fk and sp.DATA_NAME_FK = 501044
	where 1 = 1
	--and uic.job_fk = @Job_FK
	and uic.lvlid_itm = 60
	and sp.segments_product_id is null

	Insert into epacube.epacube.segments_product
	(PRODUCT_STRUCTURE_FK, data_name_fk, data_value_fk, prod_segment_fk, product_precedence, RECORD_STATUS_CR_FK)
	select 
	pi.product_structure_fk, dv3.data_name_fk, dv3.data_value_id, dv3.data_value_id, dn.precedence, 1 'record_status_cr_fk'
	from #UIC uic
	inner join epacube.epacube.PRODUCT_IDENTIFICATION pi with (nolock) on uic.CLSID_ITM = pi.value and pi.DATA_NAME_FK = 110103
	inner join epacube.epacube.data_value dv with (nolock) on uic.clsparitm = dv.value and dv.DISPLAY_SEQ = 50
	inner join epacube.epacube.data_value dv4 with (nolock) on dv.PARENT_DATA_VALUE_FK = dv4.DATA_VALUE_ID and dv4.DATA_NAME_FK = 501044
	inner join epacube.epacube.data_value dv3 with (nolock) on dv.PARENT_DATA_VALUE_FK = dv3.DATA_VALUE_ID and dv4.DATA_NAME_FK = 501043
	inner join epacube.epacube.data_name dn with (nolock) on dv3.DATA_NAME_FK = dn.data_name_id
	left join epacube.epacube.segments_product sp with (nolock) on pi.product_structure_fk = sp.product_structure_fk and sp.DATA_NAME_FK = 501043
	where 1 = 1
	--and uic.job_fk = @Job_FK
	and uic.lvlid_itm = 60
	and sp.segments_product_id is null

If object_id('precision.daily_import_log') is not null
Insert into PRECISION.DAILY_IMPORT_LOG
([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED)
select 
'IMPORTS.IMPORT_ITEM_CLASS' 'PROCEDURE_NAME'
, getdate() 'Create_timestamp'
, 'IMPORTS.URM_ITEM_CLASS' 'SOURCE_TABLE'
, 'EPACUBE.EPACUBE.DATA_VALUE' 'TARGET_TABLE'
, 'NEW DATA CLASS DATA_VALUES' 'TYPE_OF_DATA'
, (SELECT COUNT(*) FROM #UIC_New) 'RECORDS_LOADED'

	If Object_id('tempdb..#UIC_New') is not null
	drop table #UIC_New

----------URM_ITEM_CLASS End



------------------------------------------------------------------------------------------------



--------------------------------------------------------------------------------------------------

  --    SELECT @v_input_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk;

  --    SELECT @v_output_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk
		--AND PRODUCT_STRUCTURE_FK is NOT NULL ;

	 -- SET @v_exception_rows = @v_input_rows - @v_output_rows;
  --    SET @V_END_TIMESTAMP = getdate()
  --    EXEC common.job_execution_create  @in_job_fk, 'IMPORT_COMPLETE',
		--                                 @v_input_rows, @v_output_rows, @v_exception_rows,
		--								 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



 SET @status_desc =  'finished execution of IMPORT ITEM CLASS'
 
 --Exec [AMAZONA-8NUFTI5].common.exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT Competitor Retail Data has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         --EXEC [AMAZONA-8NUFTI5].common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			--EXEC [AMAZONA-8NUFTI5].common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
