﻿

-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        04/17/2018   Initial Version 
-- CV        06/21/2019   if special price flag is blank = 1 '*' = 2 


CREATE PROCEDURE [IMPORTS].[COMPETITOR_RETAIL_DATA] ( @Job_FK bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/
     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
     -- EXEC [AMAZONA-8NUFTI5].common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.Completitor Retail Data', 
					--							@exec_id = @l_exec_no OUTPUT,
					--							@epa_job_id = @Job_FK;

     --SET @l_sysdate = GETDATE() --used for the beginning timestamp
     --SET @V_START_TIMESTAMP = getdate()  -- used in steps


-------------------------------------------------------------------------------------
UPDATE RD
set product_structure_fk = g.product_structure_fk
from [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD
inner join epacube.epacube.product_gtin g on rd.upc_ean13  = g.global_item_Number
where rd.product_structure_fk is null

update R
set product_structure_fk = g.product_structure_fk
from [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] R
inner join epacube.epacube.product_gtin g on substring(r.upc_ean13,3,12) = g.upc_code 
where r.product_structure_fk is null

If object_id('tempdb..#upc_trans') is not null
drop table #upc_trans

Select ucg.comp_upc, ucrd.upc_ean13, ucg.UPC_CODE 'urm_upc' 
into #upc_trans
from [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] ucrd
inner join [IMPORTS].[URM_COMPETITOR_GPC] ucg on ucrd.upc_ean13 like '%' + ucg.comp_upc + '%'
where ucg.upc_code <> '0'
and len(comp_upc) >= 9
group by ucg.comp_upc, ucrd.upc_ean13, ucg.UPC_CODE

create index idx_01 on #upc_trans(upc_ean13, urm_upc)

update ucrd
set product_structure_fk = pg.PRODUCT_STRUCTURE_FK
from #upc_trans ut
inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] ucrd on ut.UPC_EAN13 = ucrd.UPC_EAN13
inner join epacube.epacube.product_gtin pg on ut.urm_upc = pg.UPC_CODE
where ucrd.product_structure_fk is null

--UPDATE RD
--set Comp_entity_structure_fk = ei.entity_structure_fk
--from [IMPORTS].[URM_COMPETITOR_SURVEY_COMPANY] CSC
--inner join [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] CPS on csc.prcsurcmp = cps.prcsurcmp and cps.seq_num3 = 1
--inner join [IMPORTS].[URM_COMPETITOR_SURVEY_AREA] CSA on CPS.prcsurcmp = csa.prcsurcmp and cps.prcsurare = csa.prcsurare
--inner join [IMPORTS].[URM_COMPETITORS] C on CPS.Comp_CDe = C.Comp_cde
--inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD on CSA.prcsurare = rd.price_survey_area
--inner join epacube.epacube.entity_identification ei on ei.value  = c.comp_cde

Update C
set Comp_entity_structure_fk = ei.ENTITY_STRUCTURE_FK
from [IMPORTS].[URM_COMPETITORS] C
inner join epacube.epacube.entity_identification ei with (nolock) on c.comp_cde = ei.value and ei.entity_data_name_fk = 544010 and ei.data_name_fk = 544111
where c.Comp_entity_structure_fk is null

---------------------------------------------------
update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_1  = '1'
where isnull(Special_Price_Flag_1, '') <> '*'

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_1  = '2'
where Special_Price_Flag_1 = '*'

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_2  = '1'
where Special_Price_Flag_2 = ' '

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_2  = '2'
where Special_Price_Flag_2 = '*'

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_3  = '1'
where Special_Price_Flag_3 = ' '

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_3  = '2'
where Special_Price_Flag_3 = '*'

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_4  = '1'
where Special_Price_Flag_4 = ' '

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_4  = '2'
where Special_Price_Flag_4 = '*'

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_5  = '1'
where Special_Price_Flag_5 = ' '

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_5  = '2'
where Special_Price_Flag_5 = '*'
-------------------
update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_6  = '1'
where Special_Price_Flag_6 = ' '

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_6  = '2'
where Special_Price_Flag_6 = '*'

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_7  = '1'
where Special_Price_Flag_7 = ' '

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_7  = '2'
where Special_Price_Flag_7 = '*'

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_8  = '1'
where Special_Price_Flag_8 = ' '

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_8  = '2'
where Special_Price_Flag_8 = '*'

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_9  = '1'
where Special_Price_Flag_9 = ' '

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_9  = '2'
where Special_Price_Flag_9 = '*'

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_10  = '1'
where Special_Price_Flag_10 = ' '

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_10  = '2'
where Special_Price_Flag_10 = '*'

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_11  = '1'
where Special_Price_Flag_11 = ' '

update [IMPORTS].[URM_COMPETITOR_RETAIL_DATA]  
SET Special_Price_Flag_11 = '2'
where Special_Price_Flag_11 = '*'


---------------------------------------------------------------------------------------------------------------------------
----This query needs to be run for loading data 11 times, once for each of the possible cps.seq_num3 values 1 thru 11
---------------------------------------------------------------------------------------------------------------------------
		INSERT INTO epacube.[COMPETITOR_VALUES]
		([DATA_SOURCE]
		,[COMP_ENTITY_STRUCTURE_FK]
		,[PRODUCT_STRUCTURE_FK]
		,[ENTITY_DATA_VALUE_FK]
		,[SIZE_DESCRIPTION]
		,[ITEM_DESCRIPTION]
		,[PRICE_MULTIPLE]
		,[PRICE]
		,[PRICE_TYPE]
		,[CHANGE_INDICATOR]
		,[UPC]
		,[EFFECTIVE_DATE]
		,[CREATE_TIMESTAMP]
		,[CREATE_USER])

		SELECT csc.desc_30A
		,C.comp_entity_structure_fk  
		,RD.product_structure_fk  
		,1000058613   -- Entity_Data_Value_FK (data_name_fk = 502041, data_value_fk = 1000058613  ALL CUSTOMERS?) 
		, RD.Size_Description
		, RD.Item_Description
		, RD.Price_Multiple_1
		, Cast(COALESCE(NULLIF(RD.Price_1,''), 0) as numeric(18, 2)) / 100
		, RD.Special_Price_Flag_1
		, RD.Change_Indicator_1
		,rd.UPC_EAN13
		,substring(rd.FileDate,1,2) + '-' + substring(rd.FileDate,3,2) + '-20' + substring(rd.FileDate,5,2)
		,getdate()
		,csc.prcsurcmp
		from [IMPORTS].[URM_COMPETITOR_SURVEY_COMPANY] CSC
		inner join [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] CPS on csc.prcsurcmp = cps.prcsurcmp and cps.seq_num3 = 1
		inner join [IMPORTS].[URM_COMPETITOR_SURVEY_AREA] CSA on CPS.prcsurcmp = csa.prcsurcmp and cps.prcsurare = csa.prcsurare
		inner join [IMPORTS].[URM_COMPETITORS] C on CPS.Comp_CDe = C.Comp_cde
		inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD on CSA.prcsurare = rd.price_survey_area
		Where RD.product_structure_fk is Not NULL
		And C.comp_entity_structure_fk is not NULL
		and Cast(COALESCE(NULLIF(RD.Price_1,''), 0) as numeric(18, 2)) / 100 <> 0
		and isnumeric(right(rd.Price_1, 1)) = 1
		And RD.job_fk =   @Job_FK

		-------------------------------------------------------------------------------------------------------------------------------------------------
		----2
		-----------------------------------------------------------------------------------------------------------------------------------
		INSERT INTO epacube.[COMPETITOR_VALUES]
		([DATA_SOURCE]
		,[COMP_ENTITY_STRUCTURE_FK]
		,[PRODUCT_STRUCTURE_FK]
		,[ENTITY_DATA_VALUE_FK]
		,[SIZE_DESCRIPTION]
		,[ITEM_DESCRIPTION]
		,[PRICE_MULTIPLE]
		,[PRICE]
		,[PRICE_TYPE]
		,[CHANGE_INDICATOR]
		,[UPC]
		,[EFFECTIVE_DATE]
		,[CREATE_TIMESTAMP]
		,[CREATE_USER])
		SELECT csc.desc_30A 
		,C.comp_entity_structure_fk  
		,RD.product_structure_fk  
		,1000058613   -- Entity_Data_Value_FK (data_name_fk = 502041, data_value_fk = 1000058613  ALL CUSTOMERS?) 
		, RD.Size_Description
		, RD.Item_Description
		, RD.Price_Multiple_2
		, Cast(COALESCE(NULLIF(RD.Price_2,''), 0) as numeric(18, 2)) / 100, RD.Special_Price_Flag_2, RD.Change_Indicator_2
		,rd.UPC_EAN13
		,substring(rd.FileDate,1,2) + '-' + substring(rd.FileDate,3,2) + '-20' + substring(rd.FileDate,5,2)
		,getdate()
		,csc.prcsurcmp
		from [IMPORTS].[URM_COMPETITOR_SURVEY_COMPANY] CSC
		inner join [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] CPS on csc.prcsurcmp = cps.prcsurcmp and cps.seq_num3 = 2
		inner join [IMPORTS].[URM_COMPETITOR_SURVEY_AREA] CSA on CPS.prcsurcmp = csa.prcsurcmp and cps.prcsurare = csa.prcsurare
		inner join [IMPORTS].[URM_COMPETITORS] C on CPS.Comp_CDe = C.Comp_cde
		inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD on CSA.prcsurare = rd.price_survey_area
		Where RD.product_structure_fk is Not NULL
		And C.comp_entity_structure_fk is not NULL
		and Cast(COALESCE(NULLIF(RD.Price_2,''), 0) as numeric(18, 2)) <> 0
		and isnumeric(right(rd.Price_2, 1)) = 1
		And RD.job_fk = @Job_FK
		-------------------------------------------------------------------------------------------------------------------------------------------------
		----3
		-----------------------------------------------------------------------------------------------------------------------------------
		INSERT INTO epacube.[COMPETITOR_VALUES]
		([DATA_SOURCE]
		,[COMP_ENTITY_STRUCTURE_FK]
		,[PRODUCT_STRUCTURE_FK]
		,[ENTITY_DATA_VALUE_FK]
		,[SIZE_DESCRIPTION]
		,[ITEM_DESCRIPTION]
		,[PRICE_MULTIPLE]
		,[PRICE]
		,[PRICE_TYPE]
		,[CHANGE_INDICATOR]
		,[UPC]
		,[EFFECTIVE_DATE]
		,[CREATE_TIMESTAMP]
		,[CREATE_USER])
		SELECT csc.desc_30A 
		,C.comp_entity_structure_fk  
		,RD.product_structure_fk  
		,1000058613   -- Entity_Data_Value_FK (data_name_fk = 502041, data_value_fk = 1000058613  ALL CUSTOMERS?) 
		, RD.Size_Description
		, RD.Item_Description
		, RD.Price_Multiple_3
		, Cast(COALESCE(NULLIF(RD.Price_3,''), 0) as numeric(18, 2)) / 100, RD.Special_Price_Flag_3, RD.Change_Indicator_3
		,rd.UPC_EAN13
		,substring(rd.FileDate,1,2) + '-' + substring(rd.FileDate,3,2) + '-20' + substring(rd.FileDate,5,2)
		,getdate()
		,csc.prcsurcmp
		from [IMPORTS].[URM_COMPETITOR_SURVEY_COMPANY] CSC
		inner join [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] CPS on csc.prcsurcmp = cps.prcsurcmp and cps.seq_num3 = 3
		inner join [IMPORTS].[URM_COMPETITOR_SURVEY_AREA] CSA on CPS.prcsurcmp = csa.prcsurcmp and cps.prcsurare = csa.prcsurare
		inner join [IMPORTS].[URM_COMPETITORS] C on CPS.Comp_CDe = C.Comp_cde
		inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD on CSA.prcsurare = rd.price_survey_area
		Where RD.product_structure_fk is Not NULL
		And C.comp_entity_structure_fk is not NULL
		and Cast(COALESCE(NULLIF(RD.Price_3,''), 0) as numeric(18, 2)) <> 0
		and isnumeric(right(rd.Price_3, 1)) = 1
		And RD.job_fk = @Job_FK
		-------------------------------------------------------------------------------------------------------------------------------------------------
		----4
		-----------------------------------------------------------------------------------------------------------------------------------
		INSERT INTO epacube.[COMPETITOR_VALUES]
		([DATA_SOURCE]
		,[COMP_ENTITY_STRUCTURE_FK]
		,[PRODUCT_STRUCTURE_FK]
		,[ENTITY_DATA_VALUE_FK]
		,[SIZE_DESCRIPTION]
		,[ITEM_DESCRIPTION]
		,[PRICE_MULTIPLE]
		,[PRICE]
		,[PRICE_TYPE]
		,[CHANGE_INDICATOR]
		,[UPC]
		,[EFFECTIVE_DATE]
		,[CREATE_TIMESTAMP]
		,[CREATE_USER])
		SELECT csc.desc_30A 
		,C.comp_entity_structure_fk  
		,RD.product_structure_fk  
		,1000058613   -- Entity_Data_Value_FK (data_name_fk = 502041, data_value_fk = 1000058613  ALL CUSTOMERS?) 
		, RD.Size_Description
		, RD.Item_Description
		, RD.Price_Multiple_4
		, Cast(COALESCE(NULLIF(RD.Price_4,''), 0) as numeric(18, 2)) / 100, RD.Special_Price_Flag_4, RD.Change_Indicator_4
		,rd.UPC_EAN13
		,substring(rd.FileDate,1,2) + '-' + substring(rd.FileDate,3,2) + '-20' + substring(rd.FileDate,5,2)
		,getdate()
		,csc.prcsurcmp
		from [IMPORTS].[URM_COMPETITOR_SURVEY_COMPANY] CSC
		inner join [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] CPS on csc.prcsurcmp = cps.prcsurcmp and cps.seq_num3 = 4
		inner join [IMPORTS].[URM_COMPETITOR_SURVEY_AREA] CSA on CPS.prcsurcmp = csa.prcsurcmp and cps.prcsurare = csa.prcsurare
		inner join [IMPORTS].[URM_COMPETITORS] C on CPS.Comp_CDe = C.Comp_cde
		inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD on CSA.prcsurare = rd.price_survey_area
		Where RD.product_structure_fk is Not NULL
		And C.comp_entity_structure_fk is not NULL
		and Cast(COALESCE(NULLIF(RD.Price_4,''), 0) as numeric(18, 2)) <> 0
		and isnumeric(right(rd.Price_4, 1)) = 1
		And RD.job_fk = @Job_FK
		-------------------------------------------------------------------------------------------------------------------------------------------------
		----5
		-----------------------------------------------------------------------------------------------------------------------------------
		INSERT INTO epacube.[COMPETITOR_VALUES]
		([DATA_SOURCE]
		,[COMP_ENTITY_STRUCTURE_FK]
		,[PRODUCT_STRUCTURE_FK]
		,[ENTITY_DATA_VALUE_FK]
		,[SIZE_DESCRIPTION]
		,[ITEM_DESCRIPTION]
		,[PRICE_MULTIPLE]
		,[PRICE]
		,[PRICE_TYPE]
		,[CHANGE_INDICATOR]
		,[UPC]
		,[EFFECTIVE_DATE]
		,[CREATE_TIMESTAMP]
		,[CREATE_USER])
		SELECT csc.desc_30A 
		,C.comp_entity_structure_fk  
		,RD.product_structure_fk  
		,1000058613   -- Entity_Data_Value_FK (data_name_fk = 502041, data_value_fk = 1000058613  ALL CUSTOMERS?) 
		, RD.Size_Description
		, RD.Item_Description
		, RD.Price_Multiple_5
		, Cast(COALESCE(NULLIF(RD.Price_5,''), 0) as numeric(18, 2)) / 100, RD.Special_Price_Flag_5, RD.Change_Indicator_5
		,rd.UPC_EAN13
		,substring(rd.FileDate,1,2) + '-' + substring(rd.FileDate,3,2) + '-20' + substring(rd.FileDate,5,2)
		,getdate()
		,csc.prcsurcmp
		from [IMPORTS].[URM_COMPETITOR_SURVEY_COMPANY] CSC
		inner join [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] CPS on csc.prcsurcmp = cps.prcsurcmp and cps.seq_num3 = 5
		inner join [IMPORTS].[URM_COMPETITOR_SURVEY_AREA] CSA on CPS.prcsurcmp = csa.prcsurcmp and cps.prcsurare = csa.prcsurare
		inner join [IMPORTS].[URM_COMPETITORS] C on CPS.Comp_CDe = C.Comp_cde
		inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD on CSA.prcsurare = rd.price_survey_area
		Where RD.product_structure_fk is Not NULL
		And C.comp_entity_structure_fk is not NULL
		and Cast(COALESCE(NULLIF(RD.Price_5,''), 0) as numeric(18, 2)) <> 0
		and isnumeric(right(rd.Price_5, 1)) = 1
		And RD.job_fk = @Job_FK
		-------------------------------------------------------------------------------------------------------------------------------------------------
		----6
		-----------------------------------------------------------------------------------------------------------------------------------
		INSERT INTO epacube.[COMPETITOR_VALUES]
		([DATA_SOURCE]
		,[COMP_ENTITY_STRUCTURE_FK]
		,[PRODUCT_STRUCTURE_FK]
		,[ENTITY_DATA_VALUE_FK]
		,[SIZE_DESCRIPTION]
		,[ITEM_DESCRIPTION]
		,[PRICE_MULTIPLE]
		,[PRICE]
		,[PRICE_TYPE]
		,[CHANGE_INDICATOR]
		,[UPC]
		,[EFFECTIVE_DATE]
		,[CREATE_TIMESTAMP]
		,[CREATE_USER])
		SELECT csc.desc_30A 
		,C.comp_entity_structure_fk  
		,RD.product_structure_fk  
		,1000058613   -- Entity_Data_Value_FK (data_name_fk = 502041, data_value_fk = 1000058613  ALL CUSTOMERS?) 
		, RD.Size_Description
		, RD.Item_Description
		, RD.Price_Multiple_6
		, Cast(COALESCE(NULLIF(RD.Price_6,''), 0) as numeric(18, 2)) / 100, RD.Special_Price_Flag_6
		, RD.Change_Indicator_6
		,rd.UPC_EAN13
		,substring(rd.FileDate,1,2) + '-' + substring(rd.FileDate,3,2) + '-20' + substring(rd.FileDate,5,2)
		,getdate()
		,csc.prcsurcmp
		from [IMPORTS].[URM_COMPETITOR_SURVEY_COMPANY] CSC
		inner join [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] CPS on csc.prcsurcmp = cps.prcsurcmp and cps.seq_num3 = 6
		inner join [IMPORTS].[URM_COMPETITOR_SURVEY_AREA] CSA on CPS.prcsurcmp = csa.prcsurcmp and cps.prcsurare = csa.prcsurare
		inner join [IMPORTS].[URM_COMPETITORS] C on CPS.Comp_CDe = C.Comp_cde
		inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD on CSA.prcsurare = rd.price_survey_area
		Where RD.product_structure_fk is Not NULL
		And C.comp_entity_structure_fk is not NULL
		and Cast(COALESCE(NULLIF(RD.Price_6,''), 0) as numeric(18, 2)) <> 0
		and isnumeric(right(rd.Price_6, 1)) = 1
		And RD.job_fk = @Job_FK
		-------------------------------------------------------------------------------------------------------------------------------------------------
		----7
		-----------------------------------------------------------------------------------------------------------------------------------
		INSERT INTO epacube.[COMPETITOR_VALUES]
		([DATA_SOURCE]
		,[COMP_ENTITY_STRUCTURE_FK]
		,[PRODUCT_STRUCTURE_FK]
		,[ENTITY_DATA_VALUE_FK]
		,[SIZE_DESCRIPTION]
		,[ITEM_DESCRIPTION]
		,[PRICE_MULTIPLE]
		,[PRICE]
		,[PRICE_TYPE]
		,[CHANGE_INDICATOR]
		,[UPC]
		,[EFFECTIVE_DATE]
		,[CREATE_TIMESTAMP]
		,[CREATE_USER])
		SELECT csc.desc_30A 
		,C.comp_entity_structure_fk  
		,RD.product_structure_fk  
		,1000058613   -- Entity_Data_Value_FK (data_name_fk = 502041, data_value_fk = 1000058613  ALL CUSTOMERS?) 
		, RD.Size_Description
		, RD.Item_Description
		, RD.Price_Multiple_7
		, Cast(COALESCE(NULLIF(RD.Price_7,''), 0) as numeric(18, 2)) / 100
		, RD.Special_Price_Flag_7, RD.Change_Indicator_7
		,rd.UPC_EAN13
		,substring(rd.FileDate,1,2) + '-' + substring(rd.FileDate,3,2) + '-20' + substring(rd.FileDate,5,2)
		,getdate()
		,csc.prcsurcmp
		from [IMPORTS].[URM_COMPETITOR_SURVEY_COMPANY] CSC
		inner join [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] CPS on csc.prcsurcmp = cps.prcsurcmp and cps.seq_num3 = 7
		inner join [IMPORTS].[URM_COMPETITOR_SURVEY_AREA] CSA on CPS.prcsurcmp = csa.prcsurcmp and cps.prcsurare = csa.prcsurare
		inner join [IMPORTS].[URM_COMPETITORS] C on CPS.Comp_CDe = C.Comp_cde
		inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD on CSA.prcsurare = rd.price_survey_area
		Where RD.product_structure_fk is Not NULL
		And C.comp_entity_structure_fk is not NULL
		And Cast(COALESCE(NULLIF(RD.Price_7,''), 0) as numeric(18, 2)) <> 0
		and isnumeric(right(rd.Price_7, 1)) = 1
		And RD.job_fk = @Job_FK
		-------------------------------------------------------------------------------------------------------------------------------------------------
		----8
		-----------------------------------------------------------------------------------------------------------------------------------
		INSERT INTO epacube.[COMPETITOR_VALUES]
		([DATA_SOURCE]
		,[COMP_ENTITY_STRUCTURE_FK]
		,[PRODUCT_STRUCTURE_FK]
		,[ENTITY_DATA_VALUE_FK]
		,[SIZE_DESCRIPTION]
		,[ITEM_DESCRIPTION]
		,[PRICE_MULTIPLE]
		,[PRICE]
		,[PRICE_TYPE]
		,[CHANGE_INDICATOR]
		,[UPC]
		,[EFFECTIVE_DATE]
		,[CREATE_TIMESTAMP]
		,[CREATE_USER])
		SELECT csc.desc_30A 
		,C.comp_entity_structure_fk  
		,RD.product_structure_fk  
		,1000058613   -- Entity_Data_Value_FK (data_name_fk = 502041, data_value_fk = 1000058613  ALL CUSTOMERS?) 
		, RD.Size_Description
		, RD.Item_Description
		, RD.Price_Multiple_8
		, Cast(COALESCE(NULLIF(RD.Price_8,''), 0) as numeric(18, 2)) / 100
		, RD.Special_Price_Flag_8, RD.Change_Indicator_8
		,rd.UPC_EAN13
		,substring(rd.FileDate,1,2) + '-' + substring(rd.FileDate,3,2) + '-20' + substring(rd.FileDate,5,2)
		,getdate()
		,csc.prcsurcmp
		from [IMPORTS].[URM_COMPETITOR_SURVEY_COMPANY] CSC
		inner join [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] CPS on csc.prcsurcmp = cps.prcsurcmp and cps.seq_num3 = 8
		inner join [IMPORTS].[URM_COMPETITOR_SURVEY_AREA] CSA on CPS.prcsurcmp = csa.prcsurcmp and cps.prcsurare = csa.prcsurare
		inner join [IMPORTS].[URM_COMPETITORS] C on CPS.Comp_CDe = C.Comp_cde
		inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD on CSA.prcsurare = rd.price_survey_area
		Where RD.product_structure_fk is Not NULL
		And C.comp_entity_structure_fk is not NULL
		and Cast(COALESCE(NULLIF(RD.Price_8,''), 0) as numeric(18, 2)) <> 0
		and isnumeric(right(rd.Price_8, 1)) = 1
		And RD.job_fk = @Job_FK
		-------------------------------------------------------------------------------------------------------------------------------------------------
		----9
		-----------------------------------------------------------------------------------------------------------------------------------
		INSERT INTO epacube.[COMPETITOR_VALUES]
		([DATA_SOURCE]
		,[COMP_ENTITY_STRUCTURE_FK]
		,[PRODUCT_STRUCTURE_FK]
		,[ENTITY_DATA_VALUE_FK]
		,[SIZE_DESCRIPTION]
		,[ITEM_DESCRIPTION]
		,[PRICE_MULTIPLE]
		,[PRICE]
		,[PRICE_TYPE]
		,[CHANGE_INDICATOR]
		,[UPC]
		,[EFFECTIVE_DATE]
		,[CREATE_TIMESTAMP]
		,[CREATE_USER])
		SELECT csc.desc_30A 
		,C.comp_entity_structure_fk  
		,RD.product_structure_fk  
		,1000058613   -- Entity_Data_Value_FK (data_name_fk = 502041, data_value_fk = 1000058613  ALL CUSTOMERS?) 
		, RD.Size_Description
		, RD.Item_Description
		, RD.Price_Multiple_9
		, Cast(COALESCE(NULLIF(RD.Price_10,''), 0) as numeric(18, 2)) / 100
		, RD.Special_Price_Flag_9, RD.Change_Indicator_9
		,rd.UPC_EAN13
		,substring(rd.FileDate,1,2) + '-' + substring(rd.FileDate,3,2) + '-20' + substring(rd.FileDate,5,2)
		,getdate()
		,csc.prcsurcmp
		from [IMPORTS].[URM_COMPETITOR_SURVEY_COMPANY] CSC
		inner join [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] CPS on csc.prcsurcmp = cps.prcsurcmp and cps.seq_num3 = 9
		inner join [IMPORTS].[URM_COMPETITOR_SURVEY_AREA] CSA on CPS.prcsurcmp = csa.prcsurcmp and cps.prcsurare = csa.prcsurare
		inner join [IMPORTS].[URM_COMPETITORS] C on CPS.Comp_CDe = C.Comp_cde
		inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD on CSA.prcsurare = rd.price_survey_area
		Where RD.product_structure_fk is Not NULL
		And C.comp_entity_structure_fk is not NULL
		and Cast(COALESCE(NULLIF(RD.Price_9,''), 0) as numeric(18, 2)) <> 0
		and isnumeric(right(rd.Price_9, 1)) = 1
		And RD.job_fk = @Job_FK
		-------------------------------------------------------------------------------------------------------------------------------------------------
		----10
		-----------------------------------------------------------------------------------------------------------------------------------
		INSERT INTO epacube.[COMPETITOR_VALUES]
		([DATA_SOURCE]
		,[COMP_ENTITY_STRUCTURE_FK]
		,[PRODUCT_STRUCTURE_FK]
		,[ENTITY_DATA_VALUE_FK]
		,[SIZE_DESCRIPTION]
		,[ITEM_DESCRIPTION]
		,[PRICE_MULTIPLE]
		,[PRICE]
		,[PRICE_TYPE]
		,[CHANGE_INDICATOR]
		,[UPC]
		,[EFFECTIVE_DATE]
		,[CREATE_TIMESTAMP]
		,[CREATE_USER])
		SELECT csc.desc_30A 
		,C.comp_entity_structure_fk  
		,RD.product_structure_fk  
		,1000058613   -- Entity_Data_Value_FK (data_name_fk = 502041, data_value_fk = 1000058613  ALL CUSTOMERS?) 
		, RD.Size_Description
		, RD.Item_Description
		, RD.Price_Multiple_10
		, Cast(COALESCE(NULLIF(RD.Price_10,''), 0) as numeric(18, 2)) / 100
		, RD.Special_Price_Flag_10, RD.Change_Indicator_10
		,rd.UPC_EAN13
		,substring(rd.FileDate,1,2) + '-' + substring(rd.FileDate,3,2) + '-20' + substring(rd.FileDate,5,2)
		,getdate()
		,csc.prcsurcmp
		from [IMPORTS].[URM_COMPETITOR_SURVEY_COMPANY] CSC
		inner join [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] CPS on csc.prcsurcmp = cps.prcsurcmp and cps.seq_num3 = 10
		inner join [IMPORTS].[URM_COMPETITOR_SURVEY_AREA] CSA on CPS.prcsurcmp = csa.prcsurcmp and cps.prcsurare = csa.prcsurare
		inner join [IMPORTS].[URM_COMPETITORS] C on CPS.Comp_CDe = C.Comp_cde
		inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD on CSA.prcsurare = rd.price_survey_area
		Where RD.product_structure_fk is Not NULL
		And C.comp_entity_structure_fk is not NULL
		and Cast(COALESCE(NULLIF(RD.Price_10,''), 0) as numeric(18, 2)) <> 0
		and isnumeric(right(rd.Price_10, 1)) = 1
		And RD.job_fk = @Job_FK
		-------------------------------------------------------------------------------------------------------------------------------------------------
		----11
		-----------------------------------------------------------------------------------------------------------------------------------
		INSERT INTO epacube.[COMPETITOR_VALUES]
		([DATA_SOURCE]
		,[COMP_ENTITY_STRUCTURE_FK]
		,[PRODUCT_STRUCTURE_FK]
		,[ENTITY_DATA_VALUE_FK]
		,[SIZE_DESCRIPTION]
		,[ITEM_DESCRIPTION]
		,[PRICE_MULTIPLE]
		,[PRICE]
		,[PRICE_TYPE]
		,[CHANGE_INDICATOR]
		,[UPC]
		,[EFFECTIVE_DATE]
		,[CREATE_TIMESTAMP]
		,[CREATE_USER])
		SELECT csc.desc_30A 
		,C.comp_entity_structure_fk  
		,RD.product_structure_fk  
		,1000058613   -- Entity_Data_Value_FK (data_name_fk = 502041, data_value_fk = 1000058613  ALL CUSTOMERS?) 
		, RD.Size_Description
		, RD.Item_Description
		, RD.Price_Multiple_11
		, Cast(COALESCE(NULLIF(RD.Price_11,''), 0) as numeric(18, 2)) / 100
		, RD.Special_Price_Flag_11, RD.Change_Indicator_11
		,rd.UPC_EAN13
		,substring(rd.FileDate,1,2) + '-' + substring(rd.FileDate,3,2) + '-20' + substring(rd.FileDate,5,2)
		,getdate()
		,csc.prcsurcmp
		from [IMPORTS].[URM_COMPETITOR_SURVEY_COMPANY] CSC
		inner join [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] CPS on csc.prcsurcmp = cps.prcsurcmp and cps.seq_num3 = 11
		inner join [IMPORTS].[URM_COMPETITOR_SURVEY_AREA] CSA on CPS.prcsurcmp = csa.prcsurcmp and cps.prcsurare = csa.prcsurare
		inner join [IMPORTS].[URM_COMPETITORS] C on CPS.Comp_CDe = C.Comp_cde
		inner join [IMPORTS].[URM_COMPETITOR_RETAIL_DATA] RD on CSA.prcsurare = rd.price_survey_area
		Where RD.product_structure_fk is Not NULL
		And C.comp_entity_structure_fk is not NULL
		and Cast(COALESCE(NULLIF(RD.Price_11,''), 0) as numeric(18, 2)) <> 0
		and isnumeric(right(rd.Price_11, 1)) = 1
		And RD.job_fk = @Job_FK

Delete from epacube.[COMPETITOR_VALUES] 
where COMPETITOR_VALUES_ID in
								(
									select competitor_values_id from (
									select competitor_values_id 
									, dense_rank()over(partition by product_structure_fk, comp_entity_structure_fk, entity_data_value_fk, price_multiple, price_type, effective_date order by competitor_values_id) DRank
									from epacube.[COMPETITOR_VALUES] where data_source = 'retail data'
									) A where DRank > 1
								)

EXEC IMPORTS.COMPETITOR_ALIAS_VALUES_INTERPOLATION


------------------------------------------------------------------------------------------------



--------------------------------------------------------------------------------------------------

  --    SELECT @v_input_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @Job_FK;

  --    SELECT @v_output_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @Job_FK
		--AND PRODUCT_STRUCTURE_FK is NOT NULL ;

	 -- SET @v_exception_rows = @v_input_rows - @v_output_rows;
  --    SET @V_END_TIMESTAMP = getdate()
  --    EXEC common.job_execution_create  @Job_FK, 'IMPORT_COMPLETE',
		--                                 @v_input_rows, @v_output_rows, @v_exception_rows,
		--								 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



 SET @status_desc =  'finished execution of IMPORT Competitor Retail Data'
 
 --Exec [AMAZONA-8NUFTI5].common.exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @Job_FK;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT Competitor Retail Data has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         --EXEC [AMAZONA-8NUFTI5].common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @Job_FK;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			--EXEC [AMAZONA-8NUFTI5].common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END
