﻿-- =============================================
-- Author:		Gary Stone
-- Create date: 5/10/2019
-- Description:	Load missing competitor data from full URM file
-- =============================================
CREATE PROCEDURE [IMPORTS].[COMPETITOR_FULL_FILE_REFRESH_MISSING] 
AS
	SET NOCOUNT ON;
	
	Declare @Job_FK bigint = (select top 1 job_fk from [IMPORTS].[URM_COMPETITOR_ITEM_PRICE] order by Import_Timestamp desc)

	If object_id('tempdb..#cmp') is not null
	drop table #cmp

	truncate table epacube.[COMPETITOR_VALUES]

	Select distinct
	'BULK LOAD' 'DATA_SOURCE'
	, eic.ENTITY_STRUCTURE_FK 'competitor_entity_structure_fk'
	, PI.PRODUCT_STRUCTURE_FK
	, 1000058613 'ENTITY_DATA_VALUE_FK'
	, pa.attribute_char 'SIZE DESCRIPTION'
	, pd.description 'Item_Description'
	, cip.prc_mult 'price_multiple'
	, cip.price
	, cip.prc_type 'price_type'
	, cip.spcprcflg 'change_indicator'
	, gtin.upc_code
	, dateadd(d, 1, cast(epacube.[URM_DATE_CONVERSION](Eff_Date) as date)) 'effective_date'
	, epacube.[URM_DATE_TIME_CONVERSION](dte_crt, tme_crt) 'create_timestamp'
	, usr_crt 'create_user'
	into #cmp
	from [IMPORTS].[URM_COMPETITOR_ITEM_PRICE] cip with (nolock)
	inner join epacube.epacube.product_identification pi with (nolock) on cip.item_nbr + cip.item_type = pi.value and pi.DATA_NAME_FK = 110100
	inner join epacube.epacube.entity_identification eic with (nolock) on cip.COMP_CDE = eic.value and eic.DATA_NAME_FK = 544111 and eic.ENTITY_DATA_NAME_FK = 544010
	inner join epacube.epacube.product_description pd with (nolock) on pi.product_structure_fk = pd.product_structure_fk and pd.data_name_fk = 110401
	left join epacube.epacube.product_attribute pa with (nolock) on pi.product_structure_fk = pa.PRODUCT_STRUCTURE_FK and pa.DATA_NAME_FK = 500002
	left join epacube.epacube.product_gtin gtin with (nolock) on pi.PRODUCT_STRUCTURE_FK = gtin.PRODUCT_STRUCTURE_FK 
																and gtin.DATA_NAME_FK = 500030 
																and gtin.PRECEDENCE = 1
																and GLOBAL_PACK_CODE = 'E'
	where cip.job_fk = @Job_FK

	create index idx_01 on #cmp(product_structure_FK, competitor_entity_structure_fk, effective_date)

	Insert into epacube.[COMPETITOR_VALUES]
	(data_source, COMP_ENTITY_STRUCTURE_FK, PRODUCT_STRUCTURE_FK, ENTITY_DATA_VALUE_FK, SIZE_DESCRIPTION, ITEM_DESCRIPTION, PRICE_MULTIPLE, PRICE, PRICE_TYPE, CHANGE_INDICATOR
	, UPC, EFFECTIVE_DATE, CREATE_TIMESTAMP, CREATE_USER)
	Select 
	cmp.*
	from #cmp cmp
	order by cmp.effective_date desc

	--EXEC IMPORTS.COMPETITOR_ALIAS_VALUES_INTERPOLATION 

	update cv
	set price_type = 1
	from epacube.epacube.COMPETITOR_VALUES cv where isnull(price_type, 0) = 0
