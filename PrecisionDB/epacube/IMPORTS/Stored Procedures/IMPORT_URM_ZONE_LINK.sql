﻿





-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        04/27/2018   Initial Version 


CREATE PROCEDURE [IMPORTS].[IMPORT_URM_ZONE_LINK] @Job_FK bigint 
AS 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint
	 DECLARE @in_job_fk bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
     -- EXEC [AMAZONA-8NUFTI5].common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.URM_ZONE_LINK', 
					--							@exec_id = @l_exec_no OUTPUT,
					--							@epa_job_id = @in_job_fk;

     --SET @l_sysdate = GETDATE() --used for the beginning timestamp
     --SET @V_START_TIMESTAMP = getdate()  -- used in steps


-------------------------------------------------------------------------------------
 

--select * from [customer_imports].[IMPORTS].[URM_ZONE_LINK]
----	Loads missing Aliases into data_values - dn = 500019
--select * from epacube.epacube.data_value where data_name_fk = 500019

-----------------------------------------------------------------------------------
--- Update any desciptions
---------------------------------------------------------------------------------

update DV
set description = replace(u.desc_30A, '"', '')
from [customer_imports].[IMPORTS].[URM_ZONE_LINK] U
inner join epacube.epacube.data_value dv on dv.value = u.link
 where dv.data_name_fk = 500019
 and isnull(dv.DESCRIPTION, '') <> replace(u.desc_30A, '"', '')
 and U.job_FK = @Job_FK

------------------------------------------------------------------------------------------------
---Insert any new values and use link id as data value id
---------------------------------------------------------------------------------------------------
	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, Job_FK)
	select 
	'IMPORTS.IMPORT_URM_ZONE_LINK' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE_LINK' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.DATA_VALUE' 'TARGET_TABLE'
	, 'NEW ALIAS DATA_VALUES' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM 
		(
			select distinct u.link
			from [customer_imports].[IMPORTS].[URM_ZONE_LINK] U
			left join epacube.epacube.data_value dv on dv.value = u.link and dv.data_name_fk = 500019
			 where 1 = 1
			 and dv.DATA_VALUE_ID is NULL
			 and U.job_FK = @Job_FK
		) A
		) 'RECORDS_LOADED'
	, @Job_FK 'Job_FK'


	SET IDENTITY_INSERT epacube.epacube.data_value ON

INSERT INTO epacube.[DATA_VALUE]
           ( DATA_VALUE_ID
		   ,[DATA_NAME_FK]
           ,[VALUE]
           ,[DESCRIPTION]
           ,[RECORD_STATUS_CR_FK]
           ,[CREATE_TIMESTAMP]
           ,[UPDATE_USER] )

select distinct u.link, 500019, u.LINK,replace(u.desc_30A, '"', ''),1,getdate(), 'ZONE_LINK_IMPORT'
from [customer_imports].[IMPORTS].[URM_ZONE_LINK] U
left join epacube.epacube.data_value dv on dv.value = u.link and dv.data_name_fk = 500019
 where 1 = 1
 and dv.DATA_VALUE_ID is NULL
 and U.job_FK = @Job_FK

 SET IDENTITY_INSERT epacube.epacube.data_value OFF

--------------------------------------------------------------------------------------------------

  --    SELECT @v_input_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk;

  --    SELECT @v_output_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk
		--AND PRODUCT_STRUCTURE_FK is NOT NULL ;

	 -- SET @v_exception_rows = @v_input_rows - @v_output_rows;
  --    SET @V_END_TIMESTAMP = getdate()
  --    EXEC common.job_execution_create  @in_job_fk, 'IMPORT_COMPLETE',
		--                                 @v_input_rows, @v_output_rows, @v_exception_rows,
		--								 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



 SET @status_desc =  'finished execution of IMPORT Zone Link Data Values'
 
 --Exec [AMAZONA-8NUFTI5].common.exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT ZONE LINK Data has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         --EXEC [AMAZONA-8NUFTI5].common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			--EXEC [AMAZONA-8NUFTI5].common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
