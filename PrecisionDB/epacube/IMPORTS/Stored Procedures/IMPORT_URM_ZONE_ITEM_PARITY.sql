﻿

-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        04/27/2018   Initial Version 


CREATE PROCEDURE [IMPORTS].[IMPORT_URM_ZONE_ITEM_PARITY] @Job_FK bigint
As
/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

--Declare @Job_FK bigint = 6286

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint
	 DECLARE @in_job_fk bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
     -- EXEC [AMAZONA-8NUFTI5].common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.URM_ZONE_ITEM_PARITY', 
					--							@exec_id = @l_exec_no OUTPUT,
					--							@epa_job_id = @in_job_fk;

     --SET @l_sysdate = GETDATE() --used for the beginning timestamp
     --SET @V_START_TIMESTAMP = getdate()  -- used in steps


-------------------------------------------------------------------------------------
--Goes to epacube.product_association

--Declare @Job_FK bigint = 3955

	If Object_ID('tempdb..#zip') is not null
	drop table #zip

	select distinct
	[ZONE_NUM], [ITEM_NBR], [ITEM_TYPE], [ITMPARTYP], [PARITEM], [PARITMTYP], [PARLSSAMT], [PARLSSPCT], [PARMULT], [DTE_CRT], [TME_CRT], [USR_CRT], [DTE_UPD], [TME_UPD], [USR_UPD], [Product_structure_fk], [Child_Product_structure_fk], [Zone_entity_structure_fk], [Job_FK]
	into #zip
	from [customer_imports].[IMPORTS].[URM_ZONE_ITEM_PARITY]
	where job_FK = @Job_FK

---get product structure
	UPDATE ZIP
	set product_structure_fk = PI.product_structure_fk
	from #zip ZIP
	inner join epacube.epacube.PRODUCT_IDENTIFICATION PI on pi.value = ZIP.ITEM_NBR + ZIP.ITEM_TYPE
	And pi.data_name_fk = 110100

---get child structure

	 UPDATE ZIP
	set Child_Product_structure_fk = PI.product_structure_fk
	from #zip ZIP
	inner join epacube.epacube.PRODUCT_IDENTIFICATION PI on pi.value = ZIP.PARITEM + ZIP.PARITMTYP
	And pi.data_name_fk = 110100

 ---get Zone Structure 

	UPDATE ZIP
	set Zone_entity_structure_fk = ei.entity_structure_fk
	from #zip ZIP
	 inner join epacube.epacube.entity_identification ei on ei.value  = zip.ZONE_NUM
	 and ei.DATA_NAME_FK = 151110

 --Delete parity no longer in place

	If (select count(*) from #zip where Product_structure_fk is not null and Child_Product_structure_fk is not null and Zone_entity_structure_fk is not null) > 1000
	Begin
		Delete pa
		from epacube.[PRODUCT_ASSOCIATION] pa
		left join #zip zip on (pa.PRODUCT_STRUCTURE_FK = zip.Product_structure_fk and pa.child_PRODUCT_STRUCTURE_FK = zip.Child_Product_structure_fk)
							and pa.ENTITY_STRUCTURE_FK = zip.Zone_entity_structure_fk
		where pa.data_name_fk = 159903
		and zip.item_nbr is null

		Delete pa
		from epacube.[PRODUCT_ASSOCIATION] pa
		inner join #zip zip on (pa.PRODUCT_STRUCTURE_FK = zip.Product_structure_fk and pa.child_PRODUCT_STRUCTURE_FK <> zip.Child_Product_structure_fk)
							and pa.ENTITY_STRUCTURE_FK = zip.Zone_entity_structure_fk
		where pa.data_name_fk = 159903

	End

 --select  distinct dn.name, dn.data_name_id from epacube.epacube.ENTITY_IDENTIFICATION ei
 --inner join epacube.epacube.data_name dn on dn.data_name_id = ei.data_name_fk

 	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, Job_FK)
	select 
	'IMPORTS.IMPORT_URM_ZONE_ITEM_PARITY' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE_ITEM_PARITY' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.PRODUCT_ASSOCIATION' 'TARGET_TABLE'
	, 'NEW PARITY ITEMS' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM 
		(
			select Case when zip.ItmParTyp = 'PRI' then 159903 else 159904 end 'data_name_fk', zip.Product_structure_fk, zip.Zone_entity_structure_fk, zip.Child_Product_structure_fk
			from #zip ZIP
			left join epacube.epacube.PRODUCT_ASSOCIATION pa with (nolock) on ZIP.product_structure_fk = pa.product_structure_fk
				and ZIP.child_product_structure_fk = pa.child_product_structure_fk and ZIP.zone_entity_structure_fk = pa.entity_structure_fk
				and pa.data_name_fk = Case when zip.ItmParTyp = 'PRI' then 159903 else 159904 end
			where pa.product_association_id is NULL
			and zip.Product_structure_fk is not NULL
		) A
		) 'RECORDS_LOADED'
	, @Job_FK 'Job_FK'

  -----------------------------------------------------------------------------------------------
	INSERT INTO epacube.[PRODUCT_ASSOCIATION]
        ([DATA_NAME_FK]
        ,[PRODUCT_STRUCTURE_FK]
        ,[ORG_ENTITY_STRUCTURE_FK]
        ,[ENTITY_CLASS_CR_FK]
        ,[ENTITY_STRUCTURE_FK]
        ,[CHILD_PRODUCT_STRUCTURE_FK]
        ,[RECORD_STATUS_CR_FK]
        ,[CREATE_TIMESTAMP]
		,[UPDATE_TIMESTAMP]
		, CREATE_USER
		, UPDATE_USER
        )
		select Case when zip.ItmParTyp = 'PRI' then 159903 else 159904 end 'data_name_fk', zip.Product_structure_fk,1,10109, zip.Zone_entity_structure_fk,zip.Child_Product_structure_fk, 1
		,epacube.[URM_DATE_TIME_CONVERSION](DTE_CRT, TME_CRT) 'CREATE_TIMESTAMP'
		,epacube.[URM_DATE_TIME_CONVERSION](DTE_UPD, TME_UPD) 'UPDATE_TIMESTAMP'
		, USR_CRT
		, USR_UPD
		from #zip ZIP
		left join epacube.epacube.PRODUCT_ASSOCIATION pa with (nolock) on ZIP.product_structure_fk = pa.product_structure_fk
			and ZIP.child_product_structure_fk = pa.child_product_structure_fk and ZIP.zone_entity_structure_fk = pa.entity_structure_fk
			and pa.data_name_fk = Case when zip.ItmParTyp = 'PRI' then 159903 else 159904 end
		where pa.product_association_id is NULL
		and zip.Product_structure_fk is not NULL

----select * from #zip ZIP
----select pa.* from #zip ZIP
----left join epacube.epacube.PRODUCT_ASSOCIATION pa with (nolock) on ZIP.product_structure_fk = pa.product_structure_fk
----and ZIP.child_product_structure_fk = pa.child_product_structure_fk and ZIP.zone_entity_structure_fk = pa.entity_structure_fk
----and pa.data_name_fk = 159903
----where pa.product_association_id is NOT NULL
--------------------------------------------------------------------------------------------------
	INSERT INTO epacube.[SEGMENTS_SETTINGS]
        (
		--[DATA_LEVEL_CR_FK]
    --      ,[PROD_DATA_VALUE_FK]
    --      ,[CUST_DATA_VALUE_FK]
    --      ,[RESULT_DATA_NAME_FK]
    --      ,[ORG_ENTITY_STRUCTURE_FK]
    --      ,[CUST_ENTITY_STRUCTURE_FK]
    --      ,[VENDOR_ENTITY_STRUCTURE_FK]
    --      ,[ZONE_TYPE_CR_FK]
        [ZONE_ENTITY_STRUCTURE_FK]
        ,[DATA_NAME_FK]
        --,[ATTRIBUTE_CHAR]
        ,[ATTRIBUTE_NUMBER]
        --,[ATTRIBUTE_DATE]
        --,[ATTRIBUTE_Y_N]
        --,[DATA_VALUE_FK]
        --,[ENTITY_DATA_VALUE_FK]
        ,[ATTRIBUTE_EVENT_DATA]
        --,[PRECEDENCE]
        ,[RECORD_STATUS_CR_FK]
        --,[UPDATE_TIMESTAMP]
        ,[CREATE_TIMESTAMP]
        --,[UPDATE_USER]
        ,[CREATE_USER]
        ,[PROD_SEGMENT_FK]
        --,[CUST_SEGMENT_FK]
        ,[ZONE_SEGMENT_FK]
        ,[PRODUCT_STRUCTURE_FK]
        --,[LOWER_LIMIT]
        --,[UPPER_LIMIT]
        --,[PRICE_TYPE_ENTITY_DATA_VALUE_FK]
        ,[Effective_Date]
        --,[ENTITY_DATA_NAME_FK]
		, PRODUCT_ASSOCIATION_FK
		, [DATA_SOURCE]
		)
 		select zip.Zone_entity_structure_fk, 500210, (cast(zip.PARLSSAMT as numeric(18, 4))/100), (cast(zip.PARLSSAMT as numeric(18, 4))/100),1
		, epacube.[URM_DATE_TIME_CONVERSION](zip.dte_crt, zip.tme_crt) 'Create_timestamp' , zip.USr_crt
		, zip.product_structure_fk, zip.Zone_entity_structure_fk,  zip.product_structure_fk
		, epacube.[URM_DATE_CONVERSION](zip.dte_crt) 'effective_date'
		, pa.product_association_id, 'DAILY UPDATE' 'DATA_SOURCE'
		from #zip ZIP
		inner join epacube.epacube.product_association pa with (nolock) on ZIP.product_structure_fk = pa.product_structure_fk
			and ZIP.child_product_structure_fk = pa.child_product_structure_fk and ZIP.zone_entity_structure_fk = pa.entity_structure_fk
			and pa.data_name_fk = Case when zip.ItmParTyp = 'PRI' then 159903 else 159904 end
		left join epacube.epacube.segments_settings sp on sp.product_structure_fk = zip.product_structure_fk and sp.data_name_fk = 500210
		where sp.segments_settings_id is   NULL
		and zip.Product_structure_fk is not NULL
		and zip.zone_entity_structure_fk is Not NULL
		and cast(zip.parlssamt as numeric(18, 2)) <> 0

--500210	URM_PARITY_AMT	Par Amt
--500211	URM_PARITY_PCT	Par Pct
--500212	URM_PARITY_MULT	Par Mult

	INSERT INTO epacube.[SEGMENTS_SETTINGS]
        (
		--[DATA_LEVEL_CR_FK]
    --      ,[PROD_DATA_VALUE_FK]
    --      ,[CUST_DATA_VALUE_FK]
    --      ,[RESULT_DATA_NAME_FK]
    --      ,[ORG_ENTITY_STRUCTURE_FK]
    --      ,[CUST_ENTITY_STRUCTURE_FK]
    --      ,[VENDOR_ENTITY_STRUCTURE_FK]
    --      ,[ZONE_TYPE_CR_FK]
        [ZONE_ENTITY_STRUCTURE_FK]
        ,[DATA_NAME_FK]
        --,[ATTRIBUTE_CHAR]
        ,[ATTRIBUTE_NUMBER]
        --,[ATTRIBUTE_DATE]
        --,[ATTRIBUTE_Y_N]
        --,[DATA_VALUE_FK]
        --,[ENTITY_DATA_VALUE_FK]
        ,[ATTRIBUTE_EVENT_DATA]
        --,[PRECEDENCE]
        ,[RECORD_STATUS_CR_FK]
        --,[UPDATE_TIMESTAMP]
        ,[CREATE_TIMESTAMP]
        --,[UPDATE_USER]
        ,[CREATE_USER]
        ,[PROD_SEGMENT_FK]
        --,[CUST_SEGMENT_FK]
        ,[ZONE_SEGMENT_FK]
        ,[PRODUCT_STRUCTURE_FK]
        --,[LOWER_LIMIT]
        --,[UPPER_LIMIT]
        --,[PRICE_TYPE_ENTITY_DATA_VALUE_FK]
        ,[Effective_Date]
        --,[ENTITY_DATA_NAME_FK]
		, PRODUCT_ASSOCIATION_FK
		, [DATA_SOURCE]
		)
		select zip.Zone_entity_structure_fk, 500211, cast(zip.PARLSSPCT as numeric(18, 2))/100, cast(zip.PARLSSPCT as numeric(18, 2))/100,1
		, epacube.[URM_DATE_TIME_CONVERSION](zip.dte_crt, zip.tme_crt) 'Create_timestamp' , zip.USr_crt
		, zip.product_structure_fk, zip.Zone_entity_structure_fk,  zip.product_structure_fk
		, epacube.[URM_DATE_CONVERSION](zip.dte_crt) 'effective_date'
		, pa.product_association_id, 'DAILY UPDATE' 'DATA_SOURCE'
		from #zip ZIP
		inner join epacube.epacube.product_association pa with (nolock) on ZIP.product_structure_fk = pa.product_structure_fk
			and ZIP.child_product_structure_fk = pa.child_product_structure_fk and ZIP.zone_entity_structure_fk = pa.entity_structure_fk
			and pa.data_name_fk = Case when zip.ItmParTyp = 'PRI' then 159903 else 159904 end
		left join epacube.epacube.segments_settings sp on sp.product_structure_fk = zip.product_structure_fk 
			and zip.zone_entity_structure_fk = sp.zone_segment_fk
			and sp.data_name_fk = 500211
		where sp.segments_settings_id is NULL
		and zip.Product_structure_fk is not NULL
		and zip.zone_entity_structure_fk is Not NULL
		and cast(zip.PARLSSPCT as numeric(18, 2)) <> 0

	INSERT INTO epacube.[SEGMENTS_SETTINGS]
        (
		--[DATA_LEVEL_CR_FK]
    --      ,[PROD_DATA_VALUE_FK]
    --      ,[CUST_DATA_VALUE_FK]
    --      ,[RESULT_DATA_NAME_FK]
    --      ,[ORG_ENTITY_STRUCTURE_FK]
    --      ,[CUST_ENTITY_STRUCTURE_FK]
    --      ,[VENDOR_ENTITY_STRUCTURE_FK]
    --      ,[ZONE_TYPE_CR_FK]
        [ZONE_ENTITY_STRUCTURE_FK]
        ,[DATA_NAME_FK]
        --,[ATTRIBUTE_CHAR]
        ,[ATTRIBUTE_NUMBER]
        --,[ATTRIBUTE_DATE]
        --,[ATTRIBUTE_Y_N]
        --,[DATA_VALUE_FK]
        --,[ENTITY_DATA_VALUE_FK]
        ,[ATTRIBUTE_EVENT_DATA]
        --,[PRECEDENCE]
        ,[RECORD_STATUS_CR_FK]
        --,[UPDATE_TIMESTAMP]
        ,[CREATE_TIMESTAMP]
        --,[UPDATE_USER]
        ,[CREATE_USER]
        ,[PROD_SEGMENT_FK]
        --,[CUST_SEGMENT_FK]
        ,[ZONE_SEGMENT_FK]
        ,[PRODUCT_STRUCTURE_FK]
        --,[LOWER_LIMIT]
        --,[UPPER_LIMIT]
        --,[PRICE_TYPE_ENTITY_DATA_VALUE_FK]
        ,[Effective_Date]
        --,[ENTITY_DATA_NAME_FK]
		, PRODUCT_ASSOCIATION_FK
		, [DATA_SOURCE]
		)
		select zip.Zone_entity_structure_fk, 500212, (cast(zip.PARMULT as numeric(18, 2))/100),(cast(zip.PARMULT as numeric(18, 2))/100),1
		, epacube.[URM_DATE_TIME_CONVERSION](zip.dte_crt, zip.tme_crt) 'Create_timestamp' , zip.USr_crt
		, zip.product_structure_fk, zip.Zone_entity_structure_fk,  zip.product_structure_fk
		, epacube.[URM_DATE_CONVERSION](zip.dte_crt) 'effective_date'
		, pa.product_association_id, 'DAILY UPDATE' 'DATA_SOURCE'
		from #zip ZIP
		inner join epacube.epacube.product_association pa with (nolock) on ZIP.product_structure_fk = pa.product_structure_fk
			and ZIP.child_product_structure_fk = pa.child_product_structure_fk and ZIP.zone_entity_structure_fk = pa.entity_structure_fk
			and pa.data_name_fk = Case when zip.ItmParTyp = 'PRI' then 159903 else 159904 end
		left join epacube.epacube.segments_settings sp on sp.product_structure_fk = zip.product_structure_fk and sp.data_name_fk = 500212
		where sp.segments_settings_id is   NULL
		and zip.Product_structure_fk is not NULL
		and zip.zone_entity_structure_fk is Not NULL
		and cast(zip.PARMULT as numeric(18, 2)) <> 0

		update ss
		set PRODUCT_ASSOCIATION_FK = pa.PRODUCT_ASSOCIATION_ID
		from epacube.epacube.product_association pa
		inner join epacube.epacube.segments_settings ss on pa.product_structure_fk = ss.product_structure_fk and pa.entity_structure_fk = ss.ZONE_SEGMENT_FK
		inner join #zip z on ss.product_structure_fk = z.Product_structure_fk and ss.ZONE_SEGMENT_FK = z.Zone_entity_structure_fk and ss.Effective_Date = epacube.[URM_DATE_CONVERSION](z.dte_crt)
		where pa.data_name_fk = 159903
		and ss.data_name_fk in (500210, 500211, 500212)
		and cast(pa.create_timestamp as date) = ss.Effective_Date
		and pa.PRODUCT_ASSOCIATION_ID <> ss.PRODUCT_ASSOCIATION_FK

 	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, JOB_FK)
	select 
	'IMPORTS.IMPORT_URM_ZONE_ITEM_PARITY' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE_ITEM_PARITY' 'SOURCE_TABLE'
	, 'EPACUBE.EPACUBE.SEGMENTS_SETTINGS' 'TARGET_TABLE'
	, 'PARITY DIFFERENTIALS' 'TYPE_OF_DATA'
	, (SELECT COUNT(*) FROM 
		(
			SELECT * FROM EPACUBE.EPACUBE.SEGMENTS_SETTINGS WHERE DATA_NAME_FK IN (500210, 500211, 500212)
		) A
		) 'RECORDS_LOADED'
	, @Job_FK 'Job_FK'




  --    SELECT @v_input_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk;

  --    SELECT @v_output_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk
		--AND PRODUCT_STRUCTURE_FK is NOT NULL ;

	 -- SET @v_exception_rows = @v_input_rows - @v_output_rows;
  --    SET @V_END_TIMESTAMP = getdate()
  --    EXEC common.job_execution_create  @in_job_fk, 'IMPORT_COMPLETE',
		--                                 @v_input_rows, @v_output_rows, @v_exception_rows,
		--								 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



 SET @status_desc =  'finished execution of IMPORT Zone Item Parity'
 
 --Exec [AMAZONA-8NUFTI5].common.exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;

END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT ZONE Item Parity has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         --EXEC [AMAZONA-8NUFTI5].common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			--EXEC [AMAZONA-8NUFTI5].common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
