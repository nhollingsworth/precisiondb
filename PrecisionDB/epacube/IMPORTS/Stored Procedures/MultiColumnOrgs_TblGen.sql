﻿ 

--DMU_Update_2_DMU_MultiColumnOrgs_TblGen

-- =============================================
-- Author:		<Gary Stone>
-- Create date: <July 27, 2015>
-- =============================================
CREATE PROCEDURE [IMPORTS].[MultiColumnOrgs_TblGen] 
		@QryBldrQueries_Id int = Null, @WS_FK bigint

	AS
	BEGIN

		SET NOCOUNT ON;

	Declare @SourceTableName varchar(128)
	Declare @PivotTableName varchar(128)
	Declare @OrgLabel varchar(96)
	Declare @ValueLabel varchar(96)
	Declare @SQLcd as varchar(Max)
	Declare @Preview as int = 1

	--Declare @QryBldrQueries_Id int
	--Declare @WS_FK bigint
	--Set @QryBldrQueries_Id = 43
	--Set @WS_FK = 7753

	If isnull(@QryBldrQueries_Id, 0) = 0
		Set @Preview = 0

	Set @QryBldrQueries_Id = isnull(isnull(@QryBldrQueries_Id, (Select QryBldrQueries_ID from Config.QryBldrQueries with (nolock) where ws_fk = @WS_FK)), 0)

	If isnull((Select QueryName from config.QryBldrQueries with (nolock) where QryBldrQueries_Id = @QryBldrQueries_Id), '') = ''
		goto eop

	--Set @SourceTableName = 'imports.[' + (Select Table_Name from config.ws with (nolock) where ws_id = @WS_FK) + ']'
	--Set @PivotTableName = 'imports.[' + (Select QueryName from config.QryBldrQueries with (nolock) where QryBldrQueries_Id = @QryBldrQueries_Id) + ']'
	Set @SourceTableName = '[' + Replace((Select Table_Name from config.ws with (nolock) where ws_id = @WS_FK), '.', '].[') + ']'
	Set @PivotTableName = 'imports.[' + (Select QueryName from config.QryBldrQueries with (nolock) where QryBldrQueries_Id = @QryBldrQueries_Id) + ']'
	Set @OrgLabel = (Select PivotOrgLabel from config.QryBldrQueries with (nolock) where QryBldrQueries_Id = @QryBldrQueries_Id)
	Set @ValueLabel = (Select PivotValueLabel from config.QryBldrQueries with (nolock) where QryBldrQueries_Id = @QryBldrQueries_Id)

	If Object_ID(@PivotTableName) is not null
	Begin
		Set @SQLcd = 'Drop table ' + @PivotTableName
		Exec(@SQLcd)
	End

	Declare @ColGrp varchar(96)
	Declare @ColPvt varchar(96)
	Declare @GroupCols Varchar(Max)
	Declare @PivotCols Varchar(Max)

	Set @GroupCols = ''
	Set @PivotCols = ''

			Declare Groups Cursor local for
				Select column_Name_User 
				from config.qryBldrColumns 
				where QryBldrQueries_FK = @QryBldrQueries_Id and [Schema] = 'Group' order by seq
			OPEN Groups;
					FETCH NEXT FROM Groups INTO @ColGrp
					WHILE @@FETCH_STATUS = 0
				Begin

				Set @GroupCols = @GroupCols + '[' + @ColGrp + '], '

					FETCH NEXT FROM Groups INTO @ColGrp 
				End
			Close Groups;
			Deallocate Groups;

			Declare Orgs Cursor local for
				Select column_Name_User 
				from config.qryBldrColumns 
				where QryBldrQueries_FK = @QryBldrQueries_Id and [Schema] = 'Org' order by seq
			OPEN Orgs;
					FETCH NEXT FROM Orgs INTO @ColPvt
					WHILE @@FETCH_STATUS = 0
				Begin

				Set @PivotCols = @PivotCols + 'Select ' + @GroupCols + '''' + @ColPVT + '''' + ' '''+ @OrgLabel + ''', [' + @ColPvt + '] '''+ @ValueLabel + ''' from ' + @SourceTableName + ' where [' + @ColPvt + '] is not null Union '

					FETCH NEXT FROM Orgs INTO @ColPvt 
				End
			Close Orgs;
			Deallocate Orgs;

			Set @PivotCols = Left(@PivotCols, Len(rtrim(@PivotCols)) - 6)

	Set @SQLcd = '
	Select * into ' + @PivotTableName + ' from
	('
	+ @PivotCols
	+ ') A'

	Exec(@SQLcd)

	If @Preview = 1
	Exec(@PivotCols)

	eop:
End
