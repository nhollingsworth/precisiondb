﻿

CREATE PROCEDURE [IMPORTS].[IMPORT_URM_ZONE_RETAILS_WEEKLY] @Job_FK bigint
AS 

-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        04/27/2018   Initial Version 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
-   Check to see if we need to add new  Proposal data
-   Check to see if any updates are needed.  
- do not need to worry about the promo controls table as per Gary or the promo proposal dollars.
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 
	 DECLARE @data_name_id_for_Product	bigint
	 DECLARE @in_job_fk bigint

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
     -- EXEC [AMAZONA-8NUFTI5].common.exec_monitor.start_monitor_sp @exec_desc = 'started execution of Import.URM_ZONE_RETAILS', 
					--							@exec_id = @l_exec_no OUTPUT,
					--							@epa_job_id = @in_job_fk;

     --SET @l_sysdate = GETDATE() --used for the beginning timestamp
     --SET @V_START_TIMESTAMP = getdate()  -- used in steps


-------------------------------------------------------------------------------------
 
----------URM_ZONE_RETAILS Start

--Declare @Job_FK bigint = 6601

If object_id('tempdb..#retails') is not null
drop table #retails

select [ZONE_NUM], [ITEM_NBR], [ITEM_TYPE], [PRC_TYPE], [EFF_DATE], [TERM_DATE], [PRC_MULT], [PRICE], [TM_PCT], [COST_UNIT], [RTL_STS], [PRCCHGRSN], [DTE_CRT], [TME_CRT], [USR_CRT], [DTE_UPD], [TME_UPD], [USR_UPD], IMPORT_TIMESTAMP, [Job_FK]
into #retails
from (
	Select
	[ZONE_NUM], [ITEM_NBR], [ITEM_TYPE], [PRC_TYPE], [EFF_DATE], [TERM_DATE], [PRC_MULT], [PRICE], [TM_PCT], [COST_UNIT], [RTL_STS], [PRCCHGRSN], [DTE_CRT], [TME_CRT], [USR_CRT], [DTE_UPD], [TME_UPD], [USR_UPD], IMPORT_TIMESTAMP, [Job_FK]
	, DENSE_RANK()over(partition by zone_num, item_nbr, eff_date order by import_timestamp desc) DRank
	from [customer_imports].[IMPORTS].[URM_ZONE_RETAILS_WEEKLY] where isnull(job_FK, 0) = @job_fk
) a where DRank = 1

CREATE NONCLUSTERED INDEX idx_01
ON #retails ([ZONE_NUM])
INCLUDE ([ITEM_NBR],[ITEM_TYPE],[PRC_TYPE],[EFF_DATE],[TERM_DATE],[PRC_MULT],[PRICE],[TM_PCT],[DTE_CRT],[TME_CRT],[USR_CRT],[DTE_UPD],[TME_UPD],[USR_UPD],[IMPORT_TIMESTAMP],[Job_FK])

--To Delete all existing zone price records - only when we have a full load from URM
	If (Select count(*) from #retails) > 100000
		--Delete pr from epacube.marginmgr.pricesheet_retails pr 
		--left join epacube.precision.entities_live el on pr.ZONE_ENTITY_STRUCTURE_FK = el.entity_structure_fk
		--where 1 = 1
		--and pr.data_name_fk = 503301 
		--and pr.Zone_Entity_Structure_FK is not null 
		--and pr.cust_entity_structure_fk is null
		--and el.entities_live_id is null
		Delete from epacube.marginmgr.pricesheet_retails where data_name_fk = 503301 and Zone_Entity_Structure_FK is not null and cust_entity_structure_fk is null
	else
	goto eof
	
	Insert into epacube.marginmgr.pricesheet_retails
	(Product_Structure_FK, Zone_Entity_Structure_FK, Price_Multiple
	, PRICE
	, Effective_Date, [Price_Type]
	, TARGET_MARGIN
	--, basis_calc_dn_fk
	--, Prod_Segment_Data_Name_fk
	--, Prod_Segment_FK
	--, ITEM_NUM
	, Retail_Source_Data_Name_FK, Retail_Source_FK, Retail_Source, Create_Timestamp, Create_User, update_timestamp, update_user, End_Date
	--, DRank_Precedence
	, DATA_SOURCE
	, Data_Name_FK
	, IMPORT_TIMESTAMP
	, Job_FK)
	select
	Product_Structure_fk, Zone_Entity_Structure_FK, Prc_Mult
	, Cast(Price as money) 'Price'
	, Effective_Date
	, Prc_Type
	, cast(cast(TM_Pct as Numeric(18, 4)) / 100 as Numeric(18, 4)) 'TM_PCT'
	--, 503201 Basis_Calc_CR_FK
	--, 110103 Prod_Segment_Data_Name_fk
	--, Product_Structure_fk
	--, ITEM_NUM
	, 151110 'Retail_Source_Data_Name_FK', Zone_Entity_Structure_FK 'Retail_Source_FK', Zone_Num, Create_timestamp, create_user, update_timestamp, update_user
	, End_Date
	--, 1 'DRank_Precedence'
	, 'URM DIRECT LOAD ' + cast(Cast(getdate() as date) as varchar(16)) 'DATA_SOURCE'
	, 503301  'Data_Name_FK'
	, IMPORT_TIMESTAMP
	, Job_FK
	from (
	select zn.* , eiz.entity_structure_fk 'Zone_Entity_Structure_FK', pi.product_structure_fk, PI.VALUE 'Item_Num'
	, epacube.[URM_DATE_CONVERSION](zn.Eff_Date) 'Effective_Date'
	, epacube.[URM_DATE_CONVERSION](zn.Term_Date) 'End_Date'
	, epacube.[URM_DATE_TIME_CONVERSION](zn.dte_crt, zn.tme_crt) 'Create_timestamp'
	, usr_crt 'Create_User'
	, epacube.[URM_DATE_TIME_CONVERSION](zn.dte_upd, zn.tme_upd) 'Update_timestamp'
	, usr_upd 'Update_User'
	from #retails zn with (nolock)
	inner join epacube.epacube.entity_identification eiz with (nolock) on zn.zone_num = eiz.value and eiz.ENTITY_DATA_NAME_FK = 151000 and eiz.DATA_NAME_FK = 151110
	inner join epacube.epacube.product_identification pi with (nolock) on zn.item_nbr + zn.item_type = pi.value and pi.data_name_fk = 110100
	left join epacube.marginmgr.pricesheet_retails pr with (nolock) 
		on 1 = 1
		and pr.data_name_fk = 503301
		and pr.Cust_Entity_Structure_FK is null
		and eiz.ENTITY_STRUCTURE_FK = pr.Zone_Entity_Structure_FK 
		and pi.PRODUCT_STRUCTURE_FK = pr.Product_Structure_FK
		and epacube.[URM_DATE_CONVERSION](zn.Eff_Date) = pr.Effective_Date
		and epacube.[URM_DATE_CONVERSION](zn.Term_Date) = pr.end_date 
--	left join epacube.precision.entities_live el on eiz.ENTITY_STRUCTURE_FK = el.entity_structure_fk
	where 1 = 1
	and pr.PRICESHEET_RETAILS_ID is null
--	and el.entities_live_id is null
	and isnull(zn.job_FK, 0) in (@Job_FK, 0)
	) A where 1 = 1
	
	If object_id('precision.daily_import_log') is not null
	Insert into PRECISION.DAILY_IMPORT_LOG
	([PROCEDURE_NAME], [CREATE_TIMESTAMP], [SOURCE_TABLE], [TARGET_TABLE], [TYPE_OF_DATA], RECORDS_LOADED, JOB_FK)
	select 
	'IMPORTS.IMPORT_URM_ZONE_RETAILS_WEEKLY' 'PROCEDURE_NAME'
	, getdate() 'Create_timestamp'
	, 'IMPORTS.URM_ZONE_RETAILS_WEEKLY' 'SOURCE_TABLE'
	, 'EPACUBE.MARGINMGR.PRICESHEET_RETAILS' 'TARGET_TABLE'
	, 'URM ZONE RETAILS' 'TYPE_OF_DATA'
	, (Select count(*) from epacube.marginmgr.pricesheet_retails where Zone_Entity_Structure_FK is not null and CUST_ENTITY_STRUCTURE_FK is null and data_name_fk = 503301) 'RECORDS_LOADED'
	, @Job_FK 'Job_FK'


--	To keep the table from getting too large purge all but the last 3 data sets grouped by date of import
	delete tbl
	from [IMPORTS].[URM_ZONE_RETAILS_WEEKLY]  tbl
	inner join
			(select cast(import_timestamp as date) dt, count(*) recs, rank()over(order by cast(import_timestamp as date) desc) rnk
			from [IMPORTS].[URM_ZONE_RETAILS_WEEKLY]
			group by cast(import_timestamp as date)) dts on cast(import_timestamp as date) = dts.dt
	--left join epacube.precision.entities_live el on tbl.ZONE_ENTITY_STRUCTURE_FK = el.entity_structure_fk
	where 1 = 1
	and dts.rnk > 3
	--and el.entities_live_id is null

	ALTER INDEX ALL ON epacube.marginmgr.pricesheet_retails
	REBUILD WITH (FILLFACTOR = 80, SORT_IN_TEMPDB = ON, STATISTICS_NORECOMPUTE = ON)

eof:
----------URM_ZONE_RETAILS END

--------------------------------------------------------------------------------------------------

  --    SELECT @v_input_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk;

  --    SELECT @v_output_rows = COUNT(1)
  --      FROM [import].import_gtin WITH (NOLOCK)
  --     WHERE job_fk = @in_job_fk
		--AND PRODUCT_STRUCTURE_FK is NOT NULL ;

	 -- SET @v_exception_rows = @v_input_rows - @v_output_rows;
  --    SET @V_END_TIMESTAMP = getdate()
  --    EXEC common.job_execution_create  @in_job_fk, 'IMPORT_COMPLETE',
		--                                 @v_input_rows, @v_output_rows, @v_exception_rows,
		--								 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;



 SET @status_desc =  'finished execution of IMPORT Zone Retails'
 
 --Exec [AMAZONA-8NUFTI5].common.exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of IMPORT ZONE Retail has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         --EXEC [AMAZONA-8NUFTI5].common.exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			--EXEC [AMAZONA-8NUFTI5].common.exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;
