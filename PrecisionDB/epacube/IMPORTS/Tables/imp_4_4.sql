﻿CREATE TABLE [IMPORTS].[imp_4_4] (
    [Item]                    VARCHAR (6)  NULL,
    [Description]             VARCHAR (26) NULL,
    [Description1]            VARCHAR (19) NULL,
    [stocking_UOM]            VARCHAR (12) NULL,
    [commodity_code]          VARCHAR (14) NULL,
    [Import_Date]             DATETIME     NULL,
    [Profiles_Data_Source_FK] BIGINT       NULL,
    [Row_ID]                  INT          NOT NULL
);

