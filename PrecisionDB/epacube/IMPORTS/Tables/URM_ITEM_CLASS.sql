﻿CREATE TABLE [IMPORTS].[URM_ITEM_CLASS] (
    [LVLID_ITM]        VARCHAR (50)  NULL,
    [CLSID_ITM]        VARCHAR (50)  NULL,
    [DESC_30A]         VARCHAR (256) NULL,
    [LVLID_PAR]        VARCHAR (50)  NULL,
    [CLSPARITM]        VARCHAR (50)  NULL,
    [LVLID_NEW]        VARCHAR (50)  NULL,
    [CLSIDITMN]        VARCHAR (50)  NULL,
    [DTE_CRT]          VARCHAR (50)  NULL,
    [TME_CRT]          VARCHAR (50)  NULL,
    [USR_CRT]          VARCHAR (50)  NULL,
    [DTE_UPD]          VARCHAR (50)  NULL,
    [TME_UPD]          VARCHAR (50)  NULL,
    [USR_UPD]          VARCHAR (50)  NULL,
    [IMPORT_TIMESTAMP] DATETIME      NULL,
    [Job_FK]           BIGINT        NULL
);

