﻿CREATE TABLE [IMPORTS].[URM_ITEM_BREAKDOWN] (
    [ITEM_NBR]         VARCHAR (256) NULL,
    [ITEM_TYPE]        VARCHAR (50)  NULL,
    [BRKDWNITM]        VARCHAR (50)  NULL,
    [BRKDITMTY]        VARCHAR (50)  NULL,
    [STATE_ID]         VARCHAR (50)  NULL,
    [BRKDWNPCK]        VARCHAR (50)  NULL,
    [DTE_CRT]          VARCHAR (50)  NULL,
    [TME_CRT]          VARCHAR (50)  NULL,
    [USR_CRT]          VARCHAR (50)  NULL,
    [DTE_UPD]          VARCHAR (50)  NULL,
    [TME_UPD]          VARCHAR (50)  NULL,
    [USR_UPD]          VARCHAR (50)  NULL,
    [IMPORT_TIMESTAMP] DATETIME      NULL,
    [Job_FK]           BIGINT        NULL
);

