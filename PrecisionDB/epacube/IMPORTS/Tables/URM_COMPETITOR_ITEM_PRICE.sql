﻿CREATE TABLE [IMPORTS].[URM_COMPETITOR_ITEM_PRICE] (
    [COMP_CDE]         VARCHAR (50) NULL,
    [ITEM_NBR]         VARCHAR (50) NULL,
    [ITEM_TYPE]        VARCHAR (50) NULL,
    [EFF_DATE]         VARCHAR (50) NULL,
    [PRC_TYPE]         VARCHAR (50) NULL,
    [PRC_MULT]         VARCHAR (50) NULL,
    [PRICE]            VARCHAR (50) NULL,
    [SPCPRCFLG]        VARCHAR (50) NULL,
    [DTE_CRT]          VARCHAR (50) NULL,
    [TME_CRT]          VARCHAR (50) NULL,
    [USR_CRT]          VARCHAR (50) NULL,
    [DTE_UPD]          VARCHAR (50) NULL,
    [TME_UPD]          VARCHAR (50) NULL,
    [USR_UPD]          VARCHAR (50) NULL,
    [JOB_FK]           BIGINT       NULL,
    [Import_Timestamp] DATETIME     NULL
);

