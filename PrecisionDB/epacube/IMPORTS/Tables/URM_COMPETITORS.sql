﻿CREATE TABLE [IMPORTS].[URM_COMPETITORS] (
    [COMP_CDE]                 VARCHAR (50)  NULL,
    [NAME]                     VARCHAR (256) NULL,
    [ADDR1]                    VARCHAR (256) NULL,
    [CITY_18A]                 VARCHAR (256) NULL,
    [ZIP_CODE]                 VARCHAR (256) NULL,
    [NAME_ABBR]                VARCHAR (256) NULL,
    [SHT_NAME]                 VARCHAR (256) NULL,
    [COUNTRY]                  VARCHAR (256) NULL,
    [STATE_ID]                 VARCHAR (256) NULL,
    [COMP_TYPE]                VARCHAR (256) NULL,
    [DTE_CRT]                  VARCHAR (50)  NULL,
    [TME_CRT]                  VARCHAR (50)  NULL,
    [USR_CRT]                  VARCHAR (50)  NULL,
    [DTE_UPD]                  VARCHAR (50)  NULL,
    [TME_UPD]                  VARCHAR (50)  NULL,
    [USR_UPD]                  VARCHAR (50)  NULL,
    [JOB_FK]                   BIGINT        NULL,
    [Import_Timestamp]         DATETIME      NULL,
    [Comp_entity_structure_fk] BIGINT        NULL
);

