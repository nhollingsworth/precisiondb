﻿CREATE TABLE [IMPORTS].[URM_ITEM_NEW] (
    [ITEM_SURR]            VARCHAR (50)  NULL,
    [ITEM_NBR]             VARCHAR (256) NULL,
    [ITEM_TYPE]            VARCHAR (50)  NULL,
    [ATTFINFLG]            VARCHAR (50)  NULL,
    [PRCFINFLG]            VARCHAR (50)  NULL,
    [DTE_CRT]              VARCHAR (50)  NULL,
    [ITMSURRPC]            VARCHAR (50)  NULL,
    [LVLID_ITM]            VARCHAR (50)  NULL,
    [CLSID_ITM]            VARCHAR (50)  NULL,
    [PRODUCT_STRUCTURE_FK] BIGINT        NULL,
    [IMPORT_TIMESTAMP]     DATETIME      NULL,
    [Job_FK]               BIGINT        NULL
);

