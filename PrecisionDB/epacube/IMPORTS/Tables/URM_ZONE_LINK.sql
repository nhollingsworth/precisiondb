﻿CREATE TABLE [IMPORTS].[URM_ZONE_LINK] (
    [LINK]             VARCHAR (50)  NULL,
    [DESC_30A]         VARCHAR (256) NULL,
    [DTE_CRT]          VARCHAR (50)  NULL,
    [TME_CRT]          VARCHAR (50)  NULL,
    [USR_CRT]          VARCHAR (50)  NULL,
    [DTE_UPD]          VARCHAR (50)  NULL,
    [TME_UPD]          VARCHAR (50)  NULL,
    [USR_UPD]          VARCHAR (50)  NULL,
    [IMPORT_TIMESTAMP] DATETIME      NULL,
    [Job_FK]           BIGINT        NULL
);

