﻿CREATE TABLE [IMPORTS].[URM_ZONE_RETAILS] (
    [ZONE_NUM]         VARCHAR (50) NULL,
    [ITEM_NBR]         VARCHAR (50) NULL,
    [ITEM_TYPE]        VARCHAR (50) NULL,
    [PRC_TYPE]         VARCHAR (50) NULL,
    [EFF_DATE]         VARCHAR (50) NULL,
    [TERM_DATE]        VARCHAR (50) NULL,
    [PRC_MULT]         VARCHAR (50) NULL,
    [PRICE]            VARCHAR (50) NULL,
    [TM_PCT]           VARCHAR (50) NULL,
    [COST_UNIT]        VARCHAR (50) NULL,
    [RTL_STS]          VARCHAR (50) NULL,
    [PRCCHGRSN]        VARCHAR (50) NULL,
    [DTE_CRT]          VARCHAR (50) NULL,
    [TME_CRT]          VARCHAR (50) NULL,
    [USR_CRT]          VARCHAR (50) NULL,
    [DTE_UPD]          VARCHAR (50) NULL,
    [TME_UPD]          VARCHAR (50) NULL,
    [USR_UPD]          VARCHAR (50) NULL,
    [IMPORT_TIMESTAMP] DATETIME     NULL,
    [Job_FK]           BIGINT       NULL
);

