﻿CREATE TABLE [IMPORTS].[URM_ZONE_ITEM_PARITY] (
    [ZONE_NUM]                   VARCHAR (50) NULL,
    [ITEM_NBR]                   VARCHAR (50) NULL,
    [ITEM_TYPE]                  VARCHAR (50) NULL,
    [ITMPARTYP]                  VARCHAR (50) NULL,
    [PARITEM]                    VARCHAR (50) NULL,
    [PARITMTYP]                  VARCHAR (50) NULL,
    [PARLSSAMT]                  VARCHAR (50) NULL,
    [PARLSSPCT]                  VARCHAR (50) NULL,
    [PARMULT]                    VARCHAR (50) NULL,
    [DTE_CRT]                    VARCHAR (50) NULL,
    [TME_CRT]                    VARCHAR (50) NULL,
    [USR_CRT]                    VARCHAR (50) NULL,
    [DTE_UPD]                    VARCHAR (50) NULL,
    [TME_UPD]                    VARCHAR (50) NULL,
    [USR_UPD]                    VARCHAR (50) NULL,
    [Product_structure_fk]       BIGINT       NULL,
    [Child_Product_structure_fk] BIGINT       NULL,
    [Zone_entity_structure_fk]   BIGINT       NULL,
    [IMPORT_TIMESTAMP]           DATETIME     NULL,
    [Job_FK]                     BIGINT       NULL
);

