﻿CREATE TABLE [IMPORTS].[URM_COMPETITOR_GPC] (
    [COMP_UPC]         VARCHAR (50) NULL,
    [UPC_CODE]         VARCHAR (50) NULL,
    [CMPUPCDSC]        VARCHAR (50) NULL,
    [COMPSZDSC]        VARCHAR (50) NULL,
    [REVIEWED]         VARCHAR (50) NULL,
    [COMP_TYPE]        VARCHAR (50) NULL,
    [DTE_CRT]          VARCHAR (50) NULL,
    [TME_CRT]          VARCHAR (50) NULL,
    [USR_CRT]          VARCHAR (50) NULL,
    [DTE_UPD]          VARCHAR (50) NULL,
    [TME_UPD]          VARCHAR (50) NULL,
    [USR_UPD]          VARCHAR (50) NULL,
    [JOB_FK]           BIGINT       NULL,
    [Import_Timestamp] DATETIME     NULL
);

