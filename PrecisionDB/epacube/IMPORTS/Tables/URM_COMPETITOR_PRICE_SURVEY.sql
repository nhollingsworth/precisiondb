﻿CREATE TABLE [IMPORTS].[URM_COMPETITOR_PRICE_SURVEY] (
    [PRCSURCMP]        VARCHAR (50) NULL,
    [PRCSURARE]        VARCHAR (50) NULL,
    [SEQ_NUM3]         VARCHAR (50) NULL,
    [CSTNEGAUT]        VARCHAR (50) NULL,
    [COMP_CDE]         VARCHAR (50) NULL,
    [DTE_CRT]          VARCHAR (50) NULL,
    [TME_CRT]          VARCHAR (50) NULL,
    [USR_CRT]          VARCHAR (50) NULL,
    [DTE_UPD]          VARCHAR (50) NULL,
    [TME_UPD]          VARCHAR (50) NULL,
    [USR_UPD]          VARCHAR (50) NULL,
    [JOB_FK]           BIGINT       NULL,
    [Import_Timestamp] DATETIME     NULL
);

