﻿CREATE TABLE [IMPORTS].[URM_ZONE_PRICE_POINTS] (
    [ZONE_NUM]         VARCHAR (50) NULL,
    [LVLID_ITM]        VARCHAR (50) NULL,
    [CLSID_ITM]        VARCHAR (50) NULL,
    [PRC_TYPE]         VARCHAR (50) NULL,
    [RNDLOWLMT]        VARCHAR (50) NULL,
    [RNDUPLMT]         VARCHAR (50) NULL,
    [RNDAMT]           VARCHAR (50) NULL,
    [DTE_CRT]          VARCHAR (50) NULL,
    [TME_CRT]          VARCHAR (50) NULL,
    [USR_CRT]          VARCHAR (50) NULL,
    [DTE_UPD]          VARCHAR (50) NULL,
    [TME_UPD]          VARCHAR (50) NULL,
    [USR_UPD]          VARCHAR (50) NULL,
    [IMPORT_TIMESTAMP] DATETIME     NULL,
    [Job_FK]           BIGINT       NULL
);

