﻿CREATE TABLE [IMPORTS].[URM_COMPETITOR_USER_FILES] (
    [Competitor_Code]          VARCHAR (5)  NULL,
    [URM_Item_Surrogate]       VARCHAR (7)  NULL,
    [Price_Type_Code]          VARCHAR (1)  NULL,
    [Price_Multiple]           VARCHAR (3)  NULL,
    [Price_Amount]             VARCHAR (7)  NULL,
    [Comp_User]                VARCHAR (50) NULL,
    [Product_Structure_fk]     BIGINT       NULL,
    [Comp_Entity_Structure_fk] BIGINT       NULL,
    [JOB_FK]                   BIGINT       NULL,
    [Import_Timestamp]         DATETIME     NULL
);

