﻿CREATE TABLE [IMPORTS].[imp_1_1] (
    [F1]                      VARCHAR (6)  NULL,
    [F2]                      VARCHAR (26) NULL,
    [F3]                      VARCHAR (19) NULL,
    [F4]                      VARCHAR (12) NULL,
    [F5]                      VARCHAR (14) NULL,
    [Import_Date]             DATETIME     NULL,
    [Profiles_Data_Source_FK] BIGINT       NULL,
    [Row_ID]                  INT          NOT NULL
);

