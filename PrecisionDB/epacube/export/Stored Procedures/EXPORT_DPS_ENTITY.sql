﻿





CREATE PROCEDURE [export].[EXPORT_DPS_ENTITY] 
         
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        02/06/2013  Initial SQL Version
-- CV        05/08/2013  Little tweaks
-- CV        05/28/2013  Adding in Eclipseisms
 
  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @v_batch_no   bigint
        
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

DECLARE @ERP_HOST   Varchar(32)        
 
 
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of export.EXPORT_ENTITY_DPS '
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
 

			SET @ERP_HOST = 
			(Select value from epacube.EPACUBE_PARAMS where NAME = 'ERP HOST')

                
truncate table export.export_entity_dps



---------------------------------------------------------------------------------------
----     -----
---------------------------------------------------------------------------------------

---customers - ship to; bill to
INSERT INTO [export].[EXPORT_ENTITY_DPS]
           ([ENTITY_CLASS]
           ,[ENTITY_NAME]
           ,[CODE]
           ,[DESCRIPTION]
           ,[PARENT_ENTITY_NAME]
           ,[PARENT_ENTITY_CODE]
           ,[STATUS])
(select  
cr.code ENTITY_CLASS
,substring(dn1.name,1,50) ENTITY_NAME
, ei.VALUE CODE
,substring(ltrim(rtrim(ein.VALUE)),1,128) DESCRIPTION
,dn2.name  PARENT_ENTITY_NAME
,ei2.VALUE PARENT_ENTITY_CODE
,cr2.code STATUS
from epacube.ENTITY_IDENTIFICATION ei
inner join epacube.DATA_NAME dn 
on (ei.DATA_NAME_FK = dn.data_name_id
and ei.ENTITY_STRUCTURE_FK > 100
and ei.DATA_NAME_FK = 144111)  ----  from map metadata
inner join epacube.DATA_NAME dn1 
on (ei.ENTITY_DATA_NAME_FK = dn1.data_name_id)
left join epacube.ENTITY_STRUCTURE es
on (ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID)
inner join epacube.CODE_REF cr
on (CODE_REF_ID = es.ENTITY_CLASS_CR_FK)
left join epacube.ENTITY_IDENT_NONUNIQUE ein
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK)
left join epacube.ENTITY_IDENTIFICATION ei2
on (es.PARENT_ENTITY_STRUCTURE_FK = ei2.ENTITY_STRUCTURE_FK)
left join epacube.DATA_NAME dn2 
on (ei2.ENTITY_DATA_NAME_FK = dn2.data_name_id)
left join epacube.CODE_REF cr2
on (cr2.CODE_REF_ID = es.ENTITY_STATUS_CR_FK))





IF @ERP_HOST = 'SXE'

BEGIN

--VENDORS number and name
INSERT INTO [export].[EXPORT_ENTITY_DPS]
           ([ENTITY_CLASS]
           ,[ENTITY_NAME]
           ,[CODE]
           ,[DESCRIPTION]
           ,[PARENT_ENTITY_NAME]
           ,[PARENT_ENTITY_CODE]
           ,[STATUS])
(select  
dn1.name ENTITY_CLASS
,substring(dn.name,1,50) ENTITY_NAME 
,ei.VALUE CODE
,ltrim(rtrim(ei1.VALUE)) DESCRIPTION
,NULL  PARENT_ENTITY_NAME
,NULL PARENT_ENTITY_CODE
,cr2.code STATUS
from epacube.ENTITY_IDENTIFICATION ei
inner join epacube.DATA_NAME dn
on (dn.DATA_NAME_ID = ei.DATA_NAME_FK
and ei.DATA_NAME_FK = 143110
and ei.ENTITY_STRUCTURE_FK > 100)
inner join epacube.DATA_NAME dn1 
on (ei.ENTITY_DATA_NAME_FK = dn1.data_name_id)
left join epacube.ENTITY_IDENT_nonunique ei1
--left join epacube.ENTITY_IDENTIFICATION ei1
on (ei1.DATA_NAME_FK = 143112
and ei1.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ei.DATA_NAME_FK = 143110)
left join epacube.ENTITY_STRUCTURE es
on (ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID)
left join epacube.CODE_REF cr2
on (cr2.CODE_REF_ID = es.ENTITY_STATUS_CR_FK)

UNION 
-- WAREHOUSE 
select  
cr.code ENTITY_CLASS
,dn1.name ENTITY_NAME 
, ei.VALUE CODE
,ltrim(rtrim(ein.VALUE)) DESCRIPTION
,dn2.name  PARENT_ENTITY_NAME
,ei2.VALUE PARENT_ENTITY_CODE
,cr2.code STATUS
from epacube.ENTITY_IDENTIFICATION ei
inner join epacube.DATA_NAME dn 
on (ei.DATA_NAME_FK = dn.data_name_id
and ei.ENTITY_STRUCTURE_FK > 100
and ei.DATA_NAME_FK = 141111)
inner join epacube.DATA_NAME dn1 
on (ei.ENTITY_DATA_NAME_FK = dn1.data_name_id)
left join epacube.ENTITY_STRUCTURE es
on (ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID)
inner join epacube.CODE_REF cr
on (CODE_REF_ID = es.ENTITY_CLASS_CR_FK)
left join epacube.ENTITY_IDENT_nonunique ein
--left join epacube.ENTITY_IDENTification ein
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 141112)
left join epacube.ENTITY_IDENTIFICATION ei2
on (es.PARENT_ENTITY_STRUCTURE_FK = ei2.ENTITY_STRUCTURE_FK)
left join epacube.DATA_NAME dn2 
on (ei2.ENTITY_DATA_NAME_FK = dn2.data_name_id)
left join epacube.CODE_REF cr2
on (cr2.CODE_REF_ID = es.ENTITY_STATUS_CR_FK)

)

END


if @ERP_HOST = 'ECLIPSE'
BEGIN

----VENDORS IN ECLIPSE WORLD -- code and name
INSERT INTO [export].[EXPORT_ENTITY_DPS]
           ([ENTITY_CLASS]
           ,[ENTITY_NAME]
           ,[CODE]
           ,[DESCRIPTION]
           ,[PARENT_ENTITY_NAME]
           ,[PARENT_ENTITY_CODE]
           ,[STATUS])
(select  
'VENDOR'
,'VENDOR'
,dv.VALUE CODE
,dv.Description DESCRIPTION
,NULL  PARENT_ENTITY_NAME
,NULL PARENT_ENTITY_CODE
,'ACTIVE'
from epacube.data_value dv
where dv.data_name_fk = 310903)  --- ECL PRICE LINE

UNION --- entity ident nonunique table used for Warehouse name.

select  
cr.code ENTITY_CLASS
,substring(dn1.name,1,50) 
, ei.VALUE CODE
,ltrim(rtrim(ein.VALUE)) DESCRIPTION
,dn2.name  PARENT_ENTITY_NAME
,ei2.VALUE PARENT_ENTITY_CODE
,cr2.code STATUS
from epacube.ENTITY_IDENTIFICATION ei
inner join epacube.DATA_NAME dn 
on (ei.DATA_NAME_FK = dn.data_name_id
and ei.ENTITY_STRUCTURE_FK > 100
and ei.DATA_NAME_FK = 141111)
inner join epacube.DATA_NAME dn1 
on (ei.ENTITY_DATA_NAME_FK = dn1.data_name_id)
left join epacube.ENTITY_STRUCTURE es
on (ei.ENTITY_STRUCTURE_FK = es.ENTITY_STRUCTURE_ID)
inner join epacube.CODE_REF cr
on (CODE_REF_ID = es.ENTITY_CLASS_CR_FK)
left join epacube.ENTITY_IDENT_nonunique ein
--left join epacube.ENTITY_IDENTification ein
on (ein.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ein.DATA_NAME_FK = 141112)
left join epacube.ENTITY_IDENTIFICATION ei2
on (es.PARENT_ENTITY_STRUCTURE_FK = ei2.ENTITY_STRUCTURE_FK)
left join epacube.DATA_NAME dn2 
on (ei2.ENTITY_DATA_NAME_FK = dn2.data_name_id)
left join epacube.CODE_REF cr2
on (cr2.CODE_REF_ID = es.ENTITY_STATUS_CR_FK)


update dps
set parent_entity_code = pe.parent_entity_code
,parent_entity_name = pe.parent_entity_name
from import.ecl_prov_entities  pe
inner join export.EXPORT_ENTITY_DPS dps
on (pe.code = dps.CODE
and pe.parent_entity_name is not NULL )
and dps.ENTITY_NAME = 'CUSTOMER BILL TO'


INSERT INTO [export].[EXPORT_ENTITY_DPS]
           ([ENTITY_CLASS]
           ,[ENTITY_NAME]
           ,[CODE]
           ,[DESCRIPTION]
           ,[PARENT_ENTITY_NAME]
           ,[PARENT_ENTITY_CODE]
           ,[STATUS])
(select ENTITY_CLASS
           ,ENTITY_NAME
           ,CODE
           ,DESCRIPTION
           ,NULL --[PARENT_ENTITY_NAME]
           ,NULL --[PARENT_ENTITY_CODE]
           ,STATUS
           from import.ecl_prov_entities
where entity_name = 'SALES_GROUP')


END

--------------------------------------------------------------------------------------------
----CLEAN UP 
--------------------------------------------------------------------------------------------

update export.export_entity_dps
set entity_name = 'CUSTOMER_BILL_TO'
where entity_name = 'CUSTOMER BILL TO'

update export.export_entity_dps
set entity_name = 'CUSTOMER_SHIP_TO'
where entity_name = 'CUSTOMER SHIP TO'

update export.export_entity_dps
set Parent_entity_name = 'CUSTOMER_BILL_TO'
where entity_name = 'CUSTOMER BILL TO'

update export.export_entity_dps
set entity_name = 'VENDOR'
where entity_name = 'VENDOR NUMBER'

update export.export_entity_dps
set DESCRIPTION = 'NO DESCRIPTION'
where DESCRIPTION is null

update export.export_entity_dps
set DESCRIPTION = 'NO DESCRIPTION'
where DESCRIPTION = 'null'
--------------------------------------------------------------------------------------
---FOR ECLISPE
--------------------------------------------------------------------------------------
update export.export_entity_dps
set entity_name = 'WAREHOUSE GROUP'
where entity_name = 'ECL TERRITORY'

update export.export_entity_dps
set parent_entity_name = 'WAREHOUSE GROUP'
where parent_entity_name = 'ECL TERRITORY'

---------------------------------------------------------------------------------------
------1.	On CUSTOMER_BILL_TO
------a.	Concatenate Description & ’ [‘ & Code & ‘]’   i.e. PROMED CONCEPTS, LLC [200675]

update export.export_entity_dps
set DESCRIPTION = ltrim(rtrim(DESCRIPTION + ' [' + CODE + ']'))
where entity_name = 'CUSTOMER_BILL_TO'
---------------------------------------------------------------------------------------------------
------2.	On CUSTOMER_SHIP_TO
------a.	Concatenate & ’[‘ & PARENT_ENTITY_CODE & ‘] ’   Description  i.e.   [100577] J & J COMPANY

update export.export_entity_dps
set DESCRIPTION = '[' + PARENT_ENTITY_CODE+'] ' + ltrim(rtrim(DESCRIPTION) )
where entity_name = 'CUSTOMER_SHIP_TO'

           
-------------------------------------------------------------------------------
--delete any -99999999 values
--delete where cust bill = cust ship
-------------------------------------------------------------------------------
  delete from export.EXPORT_ENTITY_DPS where CODE = '-999999999'  
  
  delete from export.EXPORT_ENTITY_DPS where ENTITY_NAME ='CUSTOMER_SHIP_TO'
  and CODE = PARENT_ENTITY_CODE
   
-------------------------------------------------------------------------------------------------------------

  SET @status_desc = 'finished execution of Export.EXPORT_ENTITY_DPS'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of Export.EXPORT_ENTITY_DPS has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END














