﻿


-- Copyright 2013
--
-- Procedure created by MARK Schreiber
--
--
-- Purpose: To Export extended attributes for eStorefront
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- MJS       03/18/2013   Initial SQL Version


CREATE PROCEDURE [export].[EXPORT_EXTENDED_ATTRIBUTES] 

AS
BEGIN

 DECLARE @ls_exec            nvarchar (max);
 DECLARE @l_exec_no          bigint;
 DECLARE @l_rows_processed   bigint;
 DECLARE @l_sysdate			 DATETIME
 
 DECLARE  @ls_stmt			  VARCHAR(MAX)
 DECLARE  @v_count			  BIGINT

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int

BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of Export.EXPORT_EXTENDED_ATTRIBUTES' 

EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;

INSERT INTO [dbo].[EXPORT.EXPORT_EXTENDED_ATTRIBUTES]
           ([PRODUCT_ID]
           ,[EPA_ATT_ID]
           ,[ATTRIBUTE_NAME]
           ,[ATTRIBUTE_VALUE])
(SELECT distinct
        pi.value, 
		cdn.data_name_fk,
		dn.name, 
		pa.attribute_event_data
from epacube.product_identification pi,
	epacube.product_attribute pa,
	epacube.config_data_name cdn, 
	epacube.data_name dn
where cdn.config_fk = 90000001
and pi.product_structure_fk = pa.product_structure_fk
and pi.data_name_fk = 110100
and pa.data_name_fk = cdn.data_name_fk
and cdn.data_name_fk = dn.data_name_id
)
SET @status_desc = 'finished execution of EXPORT.EXPORT_EXTENDED_ATTRIBUTES'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY

BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of EXPORT.EXPORT_EXTENDED_ATTRIBUTES has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END

