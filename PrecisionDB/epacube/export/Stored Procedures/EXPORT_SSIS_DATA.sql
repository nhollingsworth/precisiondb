﻿







-- Copyright 2006
--
-- Procedure created by Walt Tucholski
--
--
-- Purpose: Wrapper to execute one or more SSIS Export packages
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------------
-- WT        10/10/2006   Initial SQL Version
-- WT        11/16/2006   Converted to a wrapper procedure so that one or
--						  more export packages can be executed by calling
--						  export.EXEC_SSIS_EXPORT_PACKAGE procedure.
CREATE PROCEDURE [export].[EXPORT_SSIS_DATA] (@in_ssis_config_params_id int)
												
AS

BEGIN

DECLARE @rt int
DECLARE @EXPORT_TYPE VARCHAR (2)

SET NOCOUNT ON

SELECT @EXPORT_TYPE = EXPORT_TYPE	
FROM epacube.export.SSIS_CONFIG_PARAMS
WHERE SSIS_CONFIG_PARAMS_ID = @in_ssis_config_params_id

IF @EXPORT_TYPE = '99' -- EXPORT WRAPPER, CALL ALL OF THE SXE EXPORT PACKAGES for all changes
BEGIN
	DECLARE	SSIS_CONFIG_PARAMS_CURSOR cursor LOCAL
	FOR
	SELECT SSIS_CONFIG_PARAMS_ID
	FROM epacube.export.SSIS_CONFIG_PARAMS
	WHERE SERVER_NAME = @@servername
	  AND EXPORT_TYPE = 3 -- sxe exports for all changes
	ORDER BY PACKAGE_TYPE

	OPEN SSIS_CONFIG_PARAMS_CURSOR

	FETCH next FROM SSIS_CONFIG_PARAMS_CURSOR into @in_ssis_config_params_id

	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC export.EXEC_SSIS_EXPORT_PACKAGE @in_ssis_config_params_id
		FETCH next FROM SSIS_CONFIG_PARAMS_CURSOR into @in_ssis_config_params_id
	END
	CLOSE SSIS_CONFIG_PARAMS_CURSOR
	DEALLOCATE SSIS_CONFIG_PARAMS_CURSOR
END
ELSE -- @EXPORT_TYPE <> 99, CALL A SINGLE SXE EXPORT PACKAGE
	BEGIN
		EXEC export.EXEC_SSIS_EXPORT_PACKAGE @in_ssis_config_params_id
	END

END
























