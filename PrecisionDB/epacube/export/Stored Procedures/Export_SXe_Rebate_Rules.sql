﻿
-- Copyright 2008 epaCUBE, Inc.
--
-- Procedure created by Cindy Voutour 
--
--
-- Purpose: Import data into Synchronizer
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- GHS        04/21/2011   Initial Version 

CREATE PROCEDURE [export].[Export_SXe_Rebate_Rules] ( @IN_JOB_FK bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to   
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure 
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--     
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int 
	 DECLARE @v_import_package_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 

SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.IMPORT_TRANSFORMATION', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps

--drop table if it exists
	IF object_id('dbo.PDSR_xFer') is not null
	   drop table dbo.PDSR_xFer;

	Select distinct
	@IN_JOB_FK 'JOB_FK'
	, (select Host_Precedence_Level_Description from synchronizer.rules_precedence where rules_precedence_id = r.precedence) 'Typecd'
	, Case When (Select NAME from synchronizer.rules_hierarchy where result_data_name_fk = 111501 and record_status_cr_fk = 1) like '%LC%' then 'LEVEL CODE' else 'CUSTOMER' end 'Hierarchy'
	, contract_no 'contractno'
	, rf_cust.value1 'custno'
	, rf_crt.value1 'custrebty'
	, CONVERT(VARCHAR(10), effective_date, 1) 'startdt'
	, CONVERT(VARCHAR(10), isnull(end_date, '12/31/49'), 1) 'enddt'
	, 'w' 'dropshipty' ---*--
	, (select HOST_PRECEDENCE_LEVEL from synchronizer.rules_precedence where rules_precedence_id = r.precedence) 'levelcd'

	, Case When ra.Rules_Action_Operation1_fk = 1293 then
		Case ra.basis_calc_dn_fk When 231103 then 'b'
				When 231104 then 'l'
				When 111602 then 'p'
				When 231101 then 'r'
				When 231102 then 's'
				When 231105 then '3'
				When 100100531 then 'r'
				When 100100417 then 'b'
			end
		end 'margincostty' 
	, Null 'pricesheet' ---*--
	, Case rf_prod.data_name_fk When 211901 then rf_prod.value1 end 'pricetype'
	, Case rf_prod.data_name_fk When 110100 then rf_prod.value1 end 'product'
	, Case rf_prod.data_name_fk When 210901 then rf_prod.value1 end 'prodcat'
	, Case rf_prod.data_name_fk When 211902 then rf_prod.value1 end 'prodline'
	--, Case when ra.Rules_Action_Operation1_fk in (1021, 1022, 1023, 1024, 1291) then ra.BASIS1_NUMBER end 'rebateamt'
	, ra.BASIS1_NUMBER 'rebateamt'
	, 's' 'rebatecd' ---*--
	, Case When ra.Rules_Action_Operation1_fk in (1031, 1032, 1033, 1034, 1291) then
		Case ra.basis_calc_dn_fk When 231103 then 'b'
				When 231104 then 'l'
				When 111602 then 'p'
				When 231101 then 'r'
				When 231102 then 's'
				When 231105 then '3'
				When 100100531 then 'r'
				When 100100417 then 'b'
			end
		end 'rebatecostty' ---*--
	, Case when ra.Rules_Action_Operation1_fk not in (1021, 1022, 1023, 1024, 1291) then ra.BASIS1_NUMBER end 'rebatepct'
	, Case rf_prod.data_name_fk When 211903 then rf_prod.value1 end 'rebatety'
	, Case	when ra.Rules_Action_Operation1_fk in (1021, 1022, 1023, 1024, 1291) then '$'
			when ra.Rules_Action_Operation1_fk in (1031, 1032, 1033, 1034) then '%'
			when ra.Rules_Action_Operation1_fk = 1293 then 'm'
			else 'n' end 'rebcalcty' ---*--
	, Case	when ra.Rules_Action_Operation1_fk = 1291 then 'f'
			when ra.Rules_Action_Operation1_fk =1292 then '' end 'rebdowntoty' ---*-- Verify that 'c' is correct.
	, Case When Host_rule_XRef not like 'New%' then Host_rule_XRef end 'rebrecno'
	, Case rf_prod.data_name_fk When 211904 then rf_prod.value1 end 'rebsubty'
	, Reference 'refer'
	, rf_shipto.value1 'shipto'
	, Null 'user1'
	, Null 'user2'
	, Null 'user3'
	, Null 'user4'
	, Null 'user5'
	, Null 'user6'
	, Null 'user7'
	, Null 'user8'
	, Null 'user9'
	, rf_vndr.value1 'vendno'
	, rf_Org.value1 'whse'
	, Null 'pricesheeetto'
	, Null 'priceeffectivedate'
	, Null 'priceeffectivedateto'
	, r.NAME 'rule_name'
	into dbo.PDSR_xFer
	from synchronizer.rules r
		inner join synchronizer.rules_action ra on r.rules_id = ra.rules_fk
		inner join synchronizer.rules_filter_set rfs on rfs.rules_fk = r.rules_id
			Left join synchronizer.rules_filter rf_cust on rfs.cust_filter_fk = rf_cust.rules_filter_id and rf_cust.entity_data_name_fk = 144010
			Left join synchronizer.rules_filter rf_shipto on rfs.cust_filter_fk = rf_shipto.rules_filter_id and rf_shipto.entity_data_name_fk = 144020
			Left join synchronizer.rules_filter rf_crt on rfs.cust_filter_fk = rf_crt.rules_filter_id and rf_crt.data_name_fk = 244901
			Left Join synchronizer.rules_filter rf_prod on rfs.prod_filter_fk = rf_prod.rules_filter_id
			Left join synchronizer.rules_filter rf_vndr on rfs.supl_filter_fk = rf_vndr.rules_filter_id and rf_vndr.entity_data_name_fk = 143000
			Left join synchronizer.rules_filter rf_Org on rfs.org_filter_fk = rf_Org.rules_filter_id and rf_Org.entity_data_name_fk = 141000
	where	result_data_name_fk in (111501, 111502) --and r.record_status_cr_fk = 1
			and r.name in (select Rule_Name from dbo.MAUI_Rules_Maintenance where Transferred_to_ERP = 0)

	--Update MRM
	--Set Transferred_To_ERP = 1
	--, Transfer_To_ERP_Timestamp = getdate()
	--, Export_Job_FK = @IN_JOB_FK
	--from dbo.MAUI_Rules_Maintenance MRM
	--inner join dbo.PDSR_xFer XFer on MRM.Rule_Name = XFer.rule_name
	--Where Transferred_To_ERP = 0

	Update MRM
	Set Export_Job_FK = @IN_JOB_FK
	from dbo.MAUI_Rules_Maintenance MRM
	inner join dbo.PDSR_xFer XFer on MRM.Rule_Name = XFer.rule_name
	Where Transferred_To_ERP = 0

-------------------------------------------------------------------------
 SELECT @v_input_rows = COUNT(1)
        FROM dbo.PDSR_xFer WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM dbo.PDSR_xFer WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'IDENTIFY RULES TO TRANSFER',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


-------------------------------------------------------------------------------------
-- LAST Step is to Set Job Execution to Complete
-------------------------------------------------------------------------------------
----
----

      SET @V_END_TIMESTAMP = getdate()


--      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
--                              where job_fk =  @in_job_fk
--                              and   name = 'IMPORT PRODUCTS TO STAGE' )
--
--      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
--                              where job_fk =  @in_job_fk
--                              and   name = 'IMPORT PRODUCTS RESOLVED' )

     exec common.job_execution_create  @in_job_fk, 'RULES EXPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;

 SET @status_desc =  'finished execution of export.Export_SXe_Price_Rules'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of export.Export_SXe_Price_Rules has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END
