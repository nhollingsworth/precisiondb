﻿



CREATE PROCEDURE [export].[EXPORT_DPS_SALES_ECL] 
         
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        05/23/2013  Initial SQL Version
-- CV        05/31/2013  Adding more Eclipse Details
-- CV        10/22/2013  Adding new variable for customer type
-- CV        11/18/2013  Adding operator param
 


  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @v_batch_no   bigint
        
DECLARE @v_job_fk     bigint
DECLARE @v_export_Package_fk bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

DECLARE @VENDOR_NUMBER VARCHAR(100)
DECLARE @START_DATE DATETIME
DECLARE @END_DATE DATETIME
DECLARE @v_isFullDateRange     BIT
DECLARE @v_isSingeDateRange  BIT
DECLARE @v_isAllVendor  BIT
DECLARE @Mult_price_type int
DECLARE @Mult_rebate_type int
DECLARE @v_RptMonthsDiff int   
DECLARE @V_CustomerPriceType varchar(100) 
DECLARE @V_Operator varchar(10)
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of export.EXPORT_SALES_DPS_ECL '
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
 
-----------------------------------------------------------------------------------------------------------
-- Set varaibles
-----------------------------------------------------------------------------------------------------------
--select * from epacube.config_params where scope = 'DPS_SALES'
SET @v_export_Package_fk = (Select EXPORT_PACKAGE_ID from export.EXPORT_PACKAGE where name = 'SALES HISTORY')
SET @v_isFullDateRange = 0   
SET @v_isSingeDateRange = 1
SET @v_isAllVendor = 0
SET @v_RptMonthsDiff = 0

SET @Mult_price_type = (select value from epacube.EPACUBE_PARAMS where name = 'SXE MULTIPLE CPT CHECK')
SET @Mult_rebate_type = (select value from epacube.EPACUBE_PARAMS where name = 'SXE MULTIPLE CRT CHECK')         

Set @START_DATE = isnull((select Value from epacube.config_params where scope = 'DPS_SALES'
and Name = 'START_DATE'),'2012-01-01')

DECLARE @v_tmpValue varchar(50)
SET @v_tmpValue = (select Value from epacube.config_params where scope = 'DPS_SALES' and Name = 'END_DATE')


IF @v_tmpValue IS NULL OR LEN(@v_tmpValue) < 10	--No end date provided
BEGIN
	SET @v_isFullDateRange = 0  
	SET @v_isSingeDateRange = 1
	SET @v_RptMonthsDiff = DATEDIFF(m, @START_DATE, getdate())
END ELSE
BEGIN
	SET @v_isFullDateRange = 1  
	SET @v_isSingeDateRange = 0
	Set @END_DATE = (select Value from epacube.config_params where scope = 'DPS_SALES' and Name = 'END_DATE')
	SET @v_RptMonthsDiff = DATEDIFF(m, @START_DATE, @END_DATE)
END

----------------------------------------------------------------------------------------------------------------------
set @V_CustomerPriceType = (select ltrim(rtrim(Value))  from epacube.config_params where scope = 'DPS_SALES'
and Name = 'CUSTOMER TYPE')

Set @V_Operator = (select ltrim(rtrim(Value))  from epacube.config_params where scope = 'DPS_SALES'
and Name = 'OPERATOR')


--drop temp table if it exists
	IF object_id('tempdb..#TS_CUSTOMERS') is not null
	   drop table #TS_CUSTOMERS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_CUSTOMERS(
	[ENTITY_VALUE] [varchar](64)
		)


IF RTRIM(LTRIM(@V_Operator)) = 'like' 
BEGIN


--drop temp table if it exists
	IF object_id('tempdb..#TS_CUST_TYPE_LIST') is not null
	   drop table #TS_CUST_TYPE_LIST;
	   
	   
	-- create temp table
	CREATE TABLE #TS_CUST_TYPE_LIST(
	[TYPE_LIST] [varchar](64)
		)
		
		
		insert into #TS_CUST_TYPE_LIST
		 (Type_list)
		 (select ss.ListItem
		from utilities.SplitString ( @V_CustomerPriceType, ',' ) ss)
		
		DECLARE @v_values   varchar(25) 

		DECLARE  cur_v_import cursor local for
		select type_list from #TS_CUST_TYPE_LIST
		
		OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO 
				@v_values
				
				
         WHILE @@FETCH_STATUS = 0 
         BEGIN

insert into #TS_CUSTOMERS(
ENTITY_VALUE)
select ei.VALUE from epacube.ENTITY_CATEGORY ec with (nolock)
inner join epacube.DATA_VALUE dv  with (nolock)
on (ec.DATA_VALUE_FK = dv.DATA_VALUE_ID
and ec.DATA_NAME_FK = 114996)
inner join epacube.ENTITY_IDENTIFICATION ei
on (ec.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ei.ENTITY_DATA_NAME_FK = 144010)
where dv.DATA_NAME_FK = 114996
and dv.VALUE like @v_values

 FETCH NEXT FROM cur_v_import INTO 
										  @V_values


     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 
     

END

IF RTRIM(LTRIM(@V_Operator)) = 'equal' 

BEGIN
insert into #TS_CUSTOMERS(
ENTITY_VALUE)
select ei.VALUE from epacube.ENTITY_CATEGORY ec with (nolock)
inner join epacube.DATA_VALUE dv  with (nolock)
on (ec.DATA_VALUE_FK = dv.DATA_VALUE_ID
and ec.DATA_NAME_FK = 114996)
inner join epacube.ENTITY_IDENTIFICATION ei
on (ec.ENTITY_STRUCTURE_FK = ei.ENTITY_STRUCTURE_FK
and ei.ENTITY_DATA_NAME_FK = 144010)
where dv.DATA_NAME_FK = 114996
and dv.VALUE --= @V_CustomerPriceType
in (select ss.ListItem
		from utilities.SplitString ( @V_CustomerPriceType, ',' ) ss)



END



--------------------------------------------------------------------------------

set @VENDOR_NUMBER =  (select ltrim(rtrim(Value))  from epacube.config_params where scope = 'DPS_SALES'
and Name = 'ECL PRICE LINE')

-----------------------------------------------------------------------------
---Create temp table to get list of vendors used
----------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_DISTINCT_VENDORS') is not null
	   drop table #TS_DISTINCT_VENDORS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_DISTINCT_VENDORS(
    	[VENDOR] [varchar](50)
		)


IF RTRIM(LTRIM(@VENDOR_NUMBER)) = 'ALL' or RTRIM(LTRIM(@VENDOR_NUMBER)) = 'all'
BEGIN
	SET @v_isAllVendor = 1
            
INSERT INTO #TS_DISTINCT_VENDORS (
             VENDOR)
            ( Select distinct  vendor_id 
from import.import_sales_history_active with (nolock))

END

IF RTRIM(LTRIM(@VENDOR_NUMBER)) <> 'ALL' or RTRIM(LTRIM(@VENDOR_NUMBER)) <> 'all'
BEGIN
	SET @v_isAllVendor = 0

    INSERT INTO #TS_DISTINCT_VENDORS (
             VENDOR)
             Select 
	         ss.ListItem
		from utilities.SplitString ( @VENDOR_NUMBER, ',' ) ss
 
END

--------------------------------------------------------------------------------------------------


                
truncate table export.EXPORT_SALES_DPS

---------------------------------------------------------------------------------------
----CREATE TEMP TABLE FOR PRODUCT LIST-----
---------------------------------------------------------------------------------------

INSERT INTO [export].[EXPORT_SALES_DPS]
           ([CUST_ACCT_BILL_TO_ID]
           ,[CUST_ACCT_SHIP_TO_ID]
           ,[FREIGHT_ADD_ON_COSTS]
--           ,[FREIGHT_CARRIER]
--           ,[FREIGHT_METHOD]
--           ,[IND_ACCT_GOV_MUNICIPAL] ---export SQL
--           ,[IND_ACCT_LOYAL]
--           ,[IND_ACCT_MANAGED_INV]  ---export SQL
--           ,[IND_ACCT_NATIONAL]   ---export SQL
--           ,[IND_ACCT_NEW]
--           ,[IND_ACCT_STRATEGIC]
--           ,[IND_ACCT_TREND_FADING]
--           ,[IND_ACCT_TREND_GROWING]
--           ,[IND_COST_BRANCH_SPECIFIC]
           ,[IND_COST_NET_INTO_STOCK]  
--           ,[IND_COST_QTY_BREAK]
--           ,[IND_EXCLUDE]
--           ,[IND_ITEM_ASSOC_TO_PO]
--           ,[IND_ITEM_COMMODITY]
--           ,[IND_ITEM_HIGH_COMPETITIVE]
--           ,[IND_ITEM_HIGH_COST]
--           ,[IND_ITEM_HIGH_DEMAND]
--           ,[IND_ITEM_KIT_COMP]
--           ,[IND_ITEM_LOSS_LEADER]
--           ,[IND_ITEM_LOW_COST]
--           ,[IND_ITEM_LOW_DEMAND]
--           ,[IND_ITEM_NEW]
--           ,[IND_ITEM_NON_STOCK]
--           ,[IND_ITEM_PREMIUM]
--           ,[IND_ITEM_PROMO]
--           ,[IND_ITEM_SERVICE_LABOR]
--           ,[IND_ORDER_LRG_QTY]
           ,[IND_ORDER_OVERRIDE_COGS]
           ,[IND_ORDER_OVERRIDE_SELL]
--           ,[IND_ORDER_SALES_EDI]
--           ,[IND_ORDER_SPECIAL_INSTR]
--           ,[IND_ORDER_SPECIAL_ITEM]
--           ,[IND_REBATE_CONTRACT_EXIST]
           ,[IND_REBATE_ITEM]
--           ,[IND_REBATE_SPAPPORTUNITY]
           ,[IND_SELL_CONTRACT_USED]
--           ,[IND_VISB_HIGH_GROUP]
--           ,[IND_VISB_HIGH_ITEM]
--           ,[IND_VISB_LOW_GROUP]
--           ,[IND_VISB_LOW_ITEM]
           ,[MTX_RBT_BASIS_COLUMN]
           ,[MTX_RBT_CONTRACT_NO]
           ,[MTX_RBT_FORMULA]
--           ,[MTX_RBT_HIERARCHY]
           ,[MTX_RBT_MATRIX_ID]
           ,[MTX_RBT_REFERENCE]
--           ,[MTX_SELL_BASIS_COLUMN]
--           ,[MTX_SELL_CONTRACT_NO]
--		    ,[MTX_SELL_PRICE_AMT_SOURCE]
--           ,[MTX_SELL_FORMULA]
--           ,[MTX_SELL_HIERARCHY]
           ,[MTX_SELL_MATRIX_ID]
           ,[MTX_SELL_REFERENCE]
           ,[ORDER_AMT_COGS]
           ,[ORDER_AMT_COGS_DEFAULT]
--           ,[ORDER_AMT_COGS_SOURCE]
           ,[ORDER_AMT_REBATE]
           ,[ORDER_AMT_SELL_PRICE]
           ,[ORDER_AMT_SELL_PRICE_DEFAULT]
           ,[ORDER_CURRENCY]
           ,[ORDER_INVOICE_DATE]
           ,[ORDER_INVOICE_LINE]
           ,[ORDER_INVOICE_NUMBER]
           ,[ORDER_INVOICE_SUFFIX]
           ,[ORDER_INVOICE_TRANS_TYPE]
           ,[ORDER_ITEM_SHIPPED_QTY]  ---- must be greater then zero
           ,[ORDER_ITEM_UOM]
           ,[ORDER_ITEM_UOM_CONV]
           ,[ORDER_REP_INSIDE_ID]
           ,[ORDER_REP_OUTSIDE_ID]
           ,[ORDER_REP_TAKEN_BY_ID]
--           ,[PROD_AVG_MO_USAGE]
--           ,[PROD_VELOCITY_RANK]
           ,[PRODUCT_ID]
--           ,[SEGCUST_MRK_HIST]       ---set after customer attribute cust price type from cust attribute procedure.
--           ,[SEGCUST_RBT_MRK_HIST]
--           ,[SEGPROD_MRK_HIST]       ----set after product attribute is built
--           ,[SEGPROD_RBT_MRK_HIST]
           ,[WHSE_GROUP_ID]
           ,[WHSE_ID])
----
(SELECT
		isst.BILL_TO_CUSTOMER_ID  --  [CUST_ACCT_BILL_TO_ID]
		,isst.BILL_TO_CUSTOMER_ID+'-'+isst.SHIP_TO_CUSTOMER_ID  --[CUST_ACCT_SHIP_TO_ID]
       ,NULL  --[FREIGHT_ADD_ON_COSTS]
--           ,[FREIGHT_CARRIER]
--           ,[FREIGHT_METHOD]
--           ,[IND_ACCT_GOV_MUNICIPAL] ---export SQL
--           ,[IND_ACCT_LOYAL]
--           ,[IND_ACCT_MANAGED_INV]  ---export SQL
--           ,[IND_ACCT_NATIONAL]   ---export SQL
--           ,[IND_ACCT_NEW]
--           ,[IND_ACCT_STRATEGIC]
--           ,[IND_ACCT_TREND_FADING]
--           ,[IND_ACCT_TREND_GROWING]
--           ,[IND_COST_BRANCH_SPECIFIC]
           ,CASE WHEN  cast(isst.REPL_COST_AMT AS numeric) <> cast(isst.ORDER_COGS_AMT AS numeric)  THEN  1 ELSE NULL END
 ---[IND_COST_NET_INTO_STOCK]  
--           ,[IND_COST_QTY_BREAK]
--           ,[IND_EXCLUDE]
--           ,[IND_ITEM_ASSOC_TO_PO]
--           ,[IND_ITEM_COMMODITY]
--           ,[IND_ITEM_HIGH_COMPETITIVE]
--           ,[IND_ITEM_HIGH_COST]
--           ,[IND_ITEM_HIGH_DEMAND]
--           ,[IND_ITEM_KIT_COMP]
--           ,[IND_ITEM_LOSS_LEADER]
--           ,[IND_ITEM_LOW_COST]
--           ,[IND_ITEM_LOW_DEMAND]
--           ,[IND_ITEM_NEW]
--           ,[IND_ITEM_NON_STOCK]
--           ,[IND_ITEM_PREMIUM]
--           ,[IND_ITEM_PROMO]
--           ,[IND_ITEM_SERVICE_LABOR]
--           ,[IND_ORDER_LRG_QTY]
,(select CASE when (Cast(isst.ORDER_COGS_OVERRIDE_AMT as numeric(18, 4))) <> 0 then 1 else NULL end) --[IND_ORDER_OVERRIDE_COGS]
,(select CASE when ISNULL(Cast (isst.sell_price_override_amt as numeric(18,4)) ,0) <> 0 then 1 else NULL end )--[IND_ORDER_OVERRIDE_SELL]
--           ,[IND_ORDER_SALES_EDI]
--           ,[IND_ORDER_SPECIAL_INSTR]
--           ,[IND_ORDER_SPECIAL_ITEM]
--           ,[IND_REBATE_CONTRACT_EXIST]
           ,(select case when ISNULL(isst.REBATE_CB_CONTRACT_NO_HISTORICAL,'0') = '0' then NULL else 1 end ) --[IND_REBATE_ITEM]
--           ,[IND_REBATE_SPAPPORTUNITY]
           ,(isnumeric (SUBSTRING(isst.PRICE_MATRIX_ID_HISTORICAL,2
           ,charindex('~',substring(isst.PRICE_MATRIX_ID_HISTORICAL,2,50))-1) ))---[IND_SELL_CONTRACT_USED]
--            ,IND_VISB_HIGH_GROUP]
--           ,[IND_VISB_HIGH_ITEM]
--           ,[IND_VISB_LOW_GROUP]
--           ,[IND_VISB_LOW_ITEM]
           ,isst.REBATE_CB_BASIS_COLMUN_HISTORICAL ----[MTX_RBT_BASIS_COLUMN]
			,isst.Rebate_CB_Contract_No_Historical  ---[MTX_RBT_CONTRACT_NO]
           ,isst.REBATE_CB_FORMULA_HISTORICAL -----[MTX_RBT_FORMULA]
--           ,[MTX_RBT_HIERARCHY]
		,isst.Rebate_CB_Matrix_ID_Historical ------[MTX_RBT_MATRIX_ID]
        ,isst.REBATE_CB_CONTRACT_REFERENCE_HISTORICAL ----[MTX_RBT_REFERENCE]
--           ,[MTX_SELL_BASIS_COLUMN]
--           ,[MTX_SELL_CONTRACT_NO]
--		    ,[MTX_SELL_PRICE_AMT_SOURCE]
--           ,[MTX_SELL_FORMULA]
--           ,[MTX_SELL_HIERARCHY]
          ,isst.PRICE_MATRIX_ID_HISTORICAL ---[MTX_SELL_MATRIX_ID]
           ,isst.PRICE_CONTRACT_REFERENCE_HISTORICAL ---[MTX_SELL_REFERENCE]
           ,Cast(isst.order_cogs_amt as numeric(18, 4)) ---[ORDER_AMT_COGS]
           ,(select CASE WHEN (Cast(isst.ORDER_COGS_OVERRIDE_AMT as numeric(18, 4))) <> 0
			THEN  isst.ORDER_COGS_OVERRIDE_AMT ELSE NULL END) ---[ORDER_AMT_COGS_DEFAULT]
--           ,[ORDER_AMT_COGS_SOURCE]
           ,NULL  --- isnull(Cast(isst.REPL_COST_AMT as numeric(18, 4)),0)--- Cast(isst.order_cogs_amt as numeric(18, 4)) ) , 0) --[ORDER_AMT_REBATE]
           ,Cast(isst.sell_price_amt as numeric(18, 4))----[ORDER_AMT_SELL_PRICE]
           ,(select Case When cast(isst.SELL_PRICE_OVERRIDE_AMT As numeric(18,4) )<> 0 THEN isst.SELL_PRICE_OVERRIDE_AMT ELSE 
           isst.SELL_PRICE_AMT
           END )----[ORDER_AMT_SELL_PRICE_DEFAULT          
           ,isst.CURRENCY ----[ORDER_CURRENCY]
          ,convert(varchar,cast(SALES_ORDER_SHIP_DATE as DATE ), 1) ---[ORDER_INVOICE_DATE]
           ,isst.SALES_ORDER_LINE ---[ORDER_INVOICE_LINE]
           ,isst.SALES_ORDER_NUMBER  ---[ORDER_INVOICE_NUMBER]
           ,isst.SALES_ORDER_SUFFIX  ---[ORDER_INVOICE_SUFFIX]
           ,isst.ANALYSIS_DATA_SET_ABBREV ---[ORDER_INVOICE_TRANS_TYPE]
           ,isst.sales_qty ---[ORDER_ITEM_SHIPPED_QTY]  ---- must be greater then zero
           ,isst.SALES_UOM --[ORDER_ITEM_UOM]
           ,isst.SALES_PRIMARY_UOM_CONVERSION ---[ORDER_ITEM_UOM_CONV]
           ,isst.INSIDE_SALESREP_ID ---[ORDER_REP_INSIDE_ID]
           ,isst.OUTSIDE_SALESREP_ID ---[ORDER_REP_OUTSIDE_ID]
           ,isst.ORDER_TAKER_ID ---[ORDER_REP_TAKEN_BY_ID]
--           ,[PROD_AVG_MO_USAGE]
--           ,[PROD_VELOCITY_RANK]
           ,isst.Product_id -----[PRODUCT_ID]
--           ,[SEGCUST_MRK_HIST]       ---set after customer attribute cust price type from cust attribute procedure.
--           ,[SEGCUST_RBT_MRK_HIST]
--           ,[SEGPROD_MRK_HIST]       ----set after product attribute is built
--if anything other then sell group 1 ... it goes here..  from the product file
--           ,[SEGPROD_RBT_MRK_HIST]
           ,isst.WHSE_GROUP_ID ---[WHSE_GROUP_ID]
           ,isst.whse_id ---[WHSE_ID]
---
from import.IMPORT_SALES_HISTORY_ACTIVE  isst with (nolock)
inner join epacube.product_identification pi with (nolock)
on (pi.value = isst.PRODUCT_ID)
inner join #TS_CUSTOMERS Cust with (nolock)
on (cust.ENTITY_VALUE = isst.BILL_TO_CUSTOMER_ID)
 where 1 = 1
AND isst.SALES_QTY > 0
AND Cast(isst.order_cogs_amt as numeric(18, 4))  > 0
--AND ISNULL(prodcost,0) * ISNULL(prccostmult,0) > 0
AND Cast(isst.sell_price_amt as numeric(18, 4)) > 0
AND isst.VENDOR_ID in (select Vendor from #TS_Distinct_vendors)
--AND ISNULL(isst.SELL_PRICE_AMT,0) * ISNULL(prccostmult,0) > 0
--and  isst.vendno in ('100021', '100019', '300027')
	--AND	((@v_isAllVendor = 0 AND ltrim(rtrim(isst.VENDOR_ID)) <> '0' and Charindex((ltrim(rtrim(isst.VENDOR_ID))), @VENDOR_NUMBER) > 0 ) OR (@v_isAllVendor = 1))
AND	((@v_isFullDateRange = 1 AND isst.SALES_ORDER_SHIP_DATE BETWEEN @start_date AND @end_date)
	OR (@v_isSingeDateRange = 1 AND isst.SALES_ORDER_SHIP_DATE >= @start_date))
)

---------------------------------------------------------------------------------
--1.	CASE WHEN ORDER_INVOICE_TRANS_TYPE = ‘DO’ THEN IND_ITEM_ASSOC_TO_PO = 1 ELSE NULL END
----CASE WHEN ORDER_INVOICE_TRANS_TYPE LIKE (‘D*’) THEN IND_ITEM_ASSOC_TO_PO = 1 ELSE NULL END
---------------------------------------------------------------------------------

update export.EXPORT_SALES_DPS
set IND_ITEM_ASSOC_TO_PO = 1
where ORDER_INVOICE_TRANS_TYPE like 'D%'

-----------------------------------------------------------------------------------
--IND_REBATE_CONTRACT_EXIST ===sum (vendrebateamt) group by customer where > 0, 
--if custno is in list, then 1, else ''
---------------------------------------------------------------------------------------

update export.export_sales_dps
set IND_REBATE_CONTRACT_EXIST = '1'
where CUST_ACCT_BILL_TO_ID in (
select distinct BILL_TO_CUSTOMER_ID from import.IMPORT_SALES_HISTORY_ACTIVE 
where REBATE_CB_CONTRACT_NO_HISTORICAL is not NULL)

------------------------------------------------------------------------------------------
--IND_REBATE_SPAPPORTUNITY====subquery for list of all prod where vendrebateamt <> 0, 
--if prod is on list, and if IND_REBATE_ITEM = 0, then 1, else ''

update export.export_sales_dps
set IND_REBATE_SPAPPORTUNITY = '1'
where product_id  in (
select distinct PRODUCT_ID from import.IMPORT_SALES_HISTORY_ACTIVE where ORDER_AMT_REBATE is not NULL)
and ind_rebate_item is NULL   ---- '0'

------------------------------------------------------------------------------------------
--IND_COST_BRANCH_SPECIFIC====subquery for list of all prod where vendrebateamt <> 0, 
--if prod is on list, and if IND_REBATE_ITEM = 0, then 1, else ''

update export.export_sales_dps
set IND_COST_BRANCH_SPECIFIC = '1'
where product_id  in (
select distinct pi.value from epacube.product_identification pi (nolock)
inner join export.export_sales_dps dps
on (pi.value = dps.product_id)
--and pi.data_name_fk = 
inner join epacube.product_association pa
on (pi.PRODUCT_STRUCTURE_FK = pa.PRODUCT_STRUCTURE_FK
and pa.data_name_fk = 159100)
inner join import.IMPORT_SALES_HISTORY_ACTIVE st (nolock)
on (dps.PRODUCT_ID = st.PRODUCT_ID
and isnull((Cast(st.REPL_COST_AMT as numeric(18, 4)) - Cast(st.order_cogs_amt as numeric(18, 4)) ) , 0)  <> 0 ))

----------------------------------------------------------------------------------------
----[IND_COST_QTY_BREAK]
---------------------------------------------------------------------------------------
UPDATE export.EXPORT_SALES_DPS
SET IND_COST_QTY_BREAK = '1' 
WHERE MTX_SELL_MATRIX_ID in (
select ish.PRICE_MATRIX_ID_HISTORICAL  from synchronizer.rules R
inner join  synchronizer.rules_action RA
on (ra.rules_fk = R.rules_id)
inner join synchronizer.RULES_ACTION_OPERANDS RAO
on (rao.rules_action_fk = Ra.RULES_ACTION_ID
and rao.OPERAND_FILTER_OPERATOR_CR_FK = 9020)
inner join  import.IMPORT_SALES_HISTORY_ACTIVE ish
on (r.host_rule_xref = ish.PRICE_MATRIX_ID_HISTORICAL)
 where RULES_STRUCTURE_FK in (300100,300110))    ---[IND_COST_QTY_BREAK]  --------------ECL PRICE RULES
 ------------------------------------------------------------------------------------------
 
----------------------------------------------------------------------------------------
---- IND_ITEM_PROMO
---------------------------------------------------------------------------------------
UPDATE export.EXPORT_SALES_DPS
SET IND_ITEM_KIT_COMP = '1' 
WHERE  MTX_SELL_MATRIX_ID in
(select distinct HOST_RULE_XREF
from synchronizer.rules R
inner join export.export_sales_dps dps
on ( dps.MTX_SELL_MATRIX_ID  = r. HOST_RULE_XREF) 
inner join  synchronizer.RULES_SCHEDULE rs
on (rs.RULES_SCHEDULE_ID = r.RULES_SCHEDULE_FK
and r.RULES_STRUCTURE_FK in (300100,300110)
and PROMOTIONAL_IND is not null))

--------------------------------------------------------------------------------------------
----IND_SELL_CONTRACT_USED

update export.EXPORT_SALES_DPS
set IND_SELL_CONTRACT_USED = 1 
where MTX_SELL_HIERARCHY in (1,2)
and ISNULL(IND_SELL_CONTRACT_USED,'0') = '0'

update export.EXPORT_SALES_DPS
set IND_SELL_CONTRACT_USED = NULL
where IND_SELL_CONTRACT_USED = '0'


 ----------------------------------------------------------------------------------------
----PROD_AVG_MO_USAGE and then [IND_ORDER_LRG_QTY]
---------------------------------------------------------------------------------------

--select * from import.import_sxe_sales_transactions_active
--select * from export.EXPORT_SALES_DPs

update export.export_sales_dps
set PROD_AVG_MO_USAGE = round(cast( a.usage as numeric(18,2)),1,1)
from 
 (select  distinct PRODUCT_ID, WHSE_ID,
						SUM (cast(sales_qty/@v_RptMonthsDiff as numeric(18,2))) usage
						 from import.IMPORT_SALES_HISTORY_ACTIVE
						GROUP BY PRODUCT_ID, WHSE_ID) A
where a.PRODUCT_ID = export.export_sales_dps.product_id

---- ,[IND_ORDER_LRG_QTY]
----"find out if somone buys 6 mon supply
----'If qtyShift >= (col 77's value * 6) then 1 else 0"
update export.export_sales_dps
set IND_ORDER_LRG_QTY = 1
where PRODUCT_ID in 
 (select distinct PRODUCT_ID from export.EXPORT_SALES_DPS
where cast( ORDER_ITEM_SHIPPED_QTY as numeric(18,2)) > (6 * cast(PROD_AVG_MO_USAGE as numeric(18,2))))
and cast(PROD_AVG_MO_USAGE as numeric(18,2)) >= 1  ----added for Eclipse


----case when qtyship > (6*cast (round(qtyship/@v_RptMonthsDiff,1 ) as numeric(18,1) )  ) then 1 else NULL end   

-----------------------------------------------------------------------------------------------------------
--[IND_ACCT_LOYAL]

--By customer, you are to count number of months that have orders and divide this by the total months in the data.  
--Then by customer you take:       number of months that have orders/total months
--If greater than 75% then IND_ACCT_LOYAL = 1
--dan changed to .50 6/6/2013
------------------------------------------------------------------------------------------------------------

update export.EXPORT_SALES_DPS
set IND_ACCT_LOYAL = 1 
where CUST_ACCT_BILL_TO_ID in (
select c.cust from (
select b.cust, cast(b.num_mon/@v_RptMonthsDiff as numeric) loyal from (
select a.cust cust, count(1) num_mon from 

						(select distinct CUST_ACCT_BILL_TO_ID cust, substring(ORDER_INVOICE_DATE,1,2) num_mon
						 from export.EXPORT_SALES_DPS) A 
					group by a.cust) b) c where loyal > 0.50)
					
----------------------------------------------------------------------------------------------------
---if Custship to = cust bill to then Null cust ship to
----------------------------------------------------------------------------------------------------
update esd
set CUST_ACCT_SHIP_TO_ID = NULL
from  export.EXPORT_SALES_DPS esd
inner join (
select CUST_ACCT_BILL_TO_ID, CUST_ACCT_SHIP_TO_ID,
SUBSTRING(cust_acct_Ship_to_id,charindex('-',CUST_ACCT_SHIP_TO_ID)+1,50) SHIP_TO
from export.EXPORT_SALES_DPS) A
on (a.SHIP_TO = esd.CUST_ACCT_BILL_TO_ID)

------------------------------------------------------------------------------------------------
----additional update of MTX_SELL_MATRIX_ID   --- this is the next best by customer and product
--------------------------------------------------------------------------------------------------

--EXECUTE [export].[EXPORT_DPS_FIND_MISSING_MATRIX_IDS]


--update export.EXPORT_SALES_DPS
--set MTX_SELL_MATRIX_ID = a.mtx
--from (
--select distinct bycust.MTX_SELL_MATRIX_ID mtx, bycust.PRODUCT_ID prod, sales.PRODUCT_ID prod2
--from export.EXPORT_SALES_DPS sales
--inner join export.EXPORT_CUSTOMER_ATTRIBUTES_DPS cust
--on (cust.CUST_ACCT_BILL_TO_ID = sales.CUST_ACCT_BILL_TO_ID)
--inner join export.EXPORT_CUSTOMER_ATTRIBUTES_DPS BYSEG
--on (byseg.SEGCUST_ENT_HIST = cust.SEGCUST_ENT_HIST)
--inner join export.EXPORT_SALES_DPS bycust
--on (bycust.CUST_ACCT_BILL_TO_ID = BYSEG.CUST_ACCT_BILL_TO_ID
--and bycust.PRODUCT_ID = sales.PRODUCT_ID
--and bycust.MTX_SELL_MATRIX_ID is not NULL
--and bycust.IND_SELL_CONTRACT_USED <> 1
--and bycust.IND_ORDER_OVERRIDE_SELL <> 1)
--where isnull(sales.MTX_SELL_MATRIX_ID,'0') <> '0'
--and sales.IND_SELL_CONTRACT_USED <> 1
--and sales.IND_ORDER_OVERRIDE_SELL <> 1) A
--where product_id = a.prod
--and mtx_sell_matrix_id is null


--select * from export.EXPORT_PACKAGE_SQL
-------------------------------------------------------------------------------------------------------------
---CAll at end of export files dps for all special update sql 
----,[IND_ACCT_GOV_MUNICIPAL] ---export SQL
--  ,[IND_ACCT_MANAGED_INV]  ---export SQL
--  ,[IND_ACCT_NATIONAL]   ---export SQL
----------------------------------------------------------------------------------------------------------


			--EXECUTE  Export.[EXECUTE_EXPORT_SQL] 
			--	@v_export_package_fk
				
-----------------------------------------------------------------------------------------------------------------

  SET @status_desc = 'finished execution of Export.EXPORT_DPS_SALES_ECL'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of Export.EXPORT_DPS_SALES_ECL has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END















