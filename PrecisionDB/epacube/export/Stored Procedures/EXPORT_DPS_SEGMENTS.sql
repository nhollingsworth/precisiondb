﻿


CREATE PROCEDURE [export].[EXPORT_DPS_SEGMENTS] 
         
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        02/18/2013  Initial SQL Version
-- CV        04/26/2013  Add No Desc if value is NULL
 
  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @v_batch_no   bigint
        
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT
        
 
 
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of export.EXPORT_SEGMENTS_DPS '
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
 
                
truncate table export.export_SEGMENTS

--select * from export.export_segments

---------------------------------------------------------------------------------------
----SEGMENT -----
---------------------------------------------------------------------------------------


INSERT INTO [export].[EXPORT_SEGMENTS]
           ([CODE]
           ,[DESCRIPTION]
           ,[ENTITY_CLASS]
           ,[SEGMENT_CLASS]
           ,[SEGMENT_TYPE]
           ,[PARENT_CODE]
           ,[PARENT_SEGMENT_CLASS])
(
select distinct  dv.value, 
ISNULL(dv.DESCRIPTION,'NO DESCRIPTION')
,(select PROPERTY_NAME from export.EXPORT_MAP_METADATA m where m.COLUMN_NAME = 'SEGMENT CODE'
and m.DATA_NAME_FK = dv.DATA_NAME_FK)
,'SEGMENT'
,(select DEFAULT_VALUE from export.EXPORT_MAP_METADATA m where m.COLUMN_NAME = 'SEGMENT CODE'
and m.DATA_NAME_FK = dv.DATA_NAME_FK)
,NULL
,NULL
----
 from epacube.data_value dv (nolock)
inner join export.export_map_metadata  emm
on (emm.DATA_NAME_FK = dv.DATA_NAME_FK)
and dv.data_name_fk in (
select DATA_NAME_FK from export.EXPORT_MAP_METADATA
where COLUMN_NAME = 'SEGMENT CODE')
)

--select * from export.EXPORT_MAP_METADATA

--select distinct DATA_NAME_FK from epacube.data_value where DATA_NAME_FK in (244900,
--244901,
--211901)
           
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------

  SET @status_desc = 'finished execution of Export.EXPORT_SEGMENTS_DPS'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of Export.EXPORT_SEGMENTS_DPS has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END














