﻿





-- Copyright 2006
--
-- Procedure created by Walt Tucholski
--
--
-- Purpose: Execute SSIS Export packages
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- WT        11/16/2006   Initial SQL Version
-- YN        10/23/2008   Changed the stored procedure to be executed in a Service Broker queue

CREATE PROCEDURE [export].[EXEC_SSIS_EXPORT_PACKAGE]
      (
        @in_ssis_config_params_id INT
      
      )
AS 
      BEGIN

            DECLARE @rt INT
            DECLARE @cmd VARCHAR(4000)
            DECLARE @PACKAGE_PATH VARCHAR(256)
            DECLARE @PACKAGE_NAME VARCHAR(256)
            DECLARE @EXPORT_FILE_PATH VARCHAR(256)
            DECLARE @SXE_IN_FILE_PATH VARCHAR(256)
            DECLARE @EXPORT_TYPE VARCHAR(2)
            DECLARE @SERVER_NAME VARCHAR(256)
            DECLARE @job_name VARCHAR(250)
            DECLARE @epa_job_id BIGINT

            SET NOCOUNT ON

            SELECT  @PACKAGE_PATH = PACKAGE_PATH
                  , @PACKAGE_NAME = PACKAGE_NAME
                  , @EXPORT_FILE_PATH = EXPORT_FILE_PATH
                  , @SERVER_NAME = SERVER_NAME
                  , @SXE_IN_FILE_PATH = SXE_IN_FILE_PATH
                  , @EXPORT_TYPE = EXPORT_TYPE
            FROM    epacube.export.SSIS_CONFIG_PARAMS
            WHERE   SSIS_CONFIG_PARAMS_ID = @in_ssis_config_params_id

            SELECT  @job_name = 'Executing SSIS package: ' +
                    @PACKAGE_NAME
      

            EXEC [common].[JOB_CREATE] @IN_JOB_FK = NULL,
                 @IN_job_class_fk = 270,
                 @IN_JOB_NAME = @job_name,
                 @IN_DATA1 = NULL,
                 @IN_DATA2 = NULL,
                 @IN_DATA3 = NULL,
                 @IN_JOB_USER = NULL,
                 @OUT_JOB_FK = @epa_job_id OUTPUT,
                 @CLIENT_APP = 'SSIS' ;

            SET @cmd = 'DTEXEC /FILE' +
                ' ' + @PACKAGE_PATH +
                '\' + @PACKAGE_NAME +
                ' ' +
                '/MAXCONCURRENT " -1 "' +
                ' ' +
                '/CHECKPOINTING OFF' +
                ' ' + '/Decrypt epacube' +
                ' ' + '/SET' + ' ' +
                '\Package.Variables[User::V_EXPORT_FILE_DIRECTORY].Properties[Value];' +
                @EXPORT_FILE_PATH + ' ' +
                '/SET' + ' ' +
                '\Package.Variables[User::V_SXE_FILE_DIRECTORY].Properties[Value];' +
                @SXE_IN_FILE_PATH +
				   ' ' + '/SET' + ' ' +
                      '\Package.Connections[ntsdb.epacube].ServerName;' +
                      @SERVER_NAME

                
                
insert into common.t_sql
(sql_text)
select 
 (select @cmd )



--select @cmd
--        EXEC master..xp_cmdshell @cmd, NO_OUTPUT
                 
        --put the SSIS execution into a Service Broker Queue         
            DECLARE @RC INT
            DECLARE @service_name NVARCHAR(50)
            DECLARE @cmd_text NVARCHAR(4000)
            DECLARE @cmd_type NVARCHAR(10)

            SELECT  @service_name = 'TargetEpaService'
                  , @cmd_text = @cmd
                  , @cmd_type = 'CMD'
            EXECUTE @RC = [queue_manager].[enqueue] @service_name,
                    @cmd_text,
                    @epa_job_id,
                    @cmd_type

      END






















