﻿





CREATE PROCEDURE [export].[EXPORT_DPS_FILES] 
         
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        02/06/2013  Initial SQL Version
-- CV        04/05/2013  Make job class for export instead of import.
-- CV        05/15/2013  Moved where the SSIS sales package runs.. to end.
-- CV        05/28/2013  Added Eclipisms 
-- CV        09/26/2013  Added new procedure to call this one using service broker.
 
  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @v_batch_no   bigint
        
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT
DECLARE @ERP_HOST   Varchar(32)
        
 
 
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of export.EXPORT_DPS_FILES '
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
 

                
    --              EXEC seq_manager.db_get_next_sequence_value_impl 'common',
    --                'job_seq', @v_job_fk output
   

    --SET @v_job_class_fk  = 226        --- PRODUCT MAINTENANCE
    --SET @v_job_user = 'EPACUBE'
    --SET @v_job_name = 'EXPORT DPS FILES'
    --SET @v_job_date = @l_sysdate
    --SET @v_data1   = 'LAST JOB:: '
    --                 + CAST ( ISNULL ( @v_last_job_fk, -999 )  AS VARCHAR(32) )
    --                 + ' - '
    --                 + CAST ( ISNULL ( @v_last_job_date, '01/01/2000' ) AS VARCHAR(64) )
    --SET @v_data2   = @v_last_job_date
    
    --EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
				--						   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

    --EXEC common.job_execution_create   @v_job_fk, 'START EXPORT DPS FILES',
		  --                               @v_input_rows, @v_output_rows, @v_exception_rows,
				--						 200, @l_sysdate, @l_sysdate  
  

     SET @ERP_HOST = 
			(Select value from epacube.EPACUBE_PARAMS where NAME = 'ERP HOST')
---------------------------------------------------------------------------------------
----STEP 1 CREATE JOB - CREATE LOGGING
--  STEP 2 CREATE ENTITY - ALL DATA

--  STEP 3 CREATE SALES HISTORY use pramas set by user
--  STEP 4 CREATE PRODUCT ATTRIBUTES off SALES PRODUCTS
--  STEP 5 CREATE CUSTOMER ATTRIBUTES off SALES CUSTOMERS
--  STEP 6 CREATE SEGMENT DATA - ALL DATA
---------------------------------------------------------------------------------------

---RUN ENTITY PROCEDURE AND EXPORT SSIS PACKAGE

EXECUTE export.EXPORT_DPS_ENTITY
       
-------------------------------------------------------------------------------
---RUN SALES PROCEDURE AND EXPORT SSIS PACKAGE
IF 
@ERP_HOST = 'SXE'
BEGIN
EXECUTE export.EXPORT_DPS_SALES_CN
END

-------------------------------------
IF 
@ERP_HOST = 'ECLIPSE'
BEGIN
EXECUTE export.EXPORT_DPS_SALES_ECL
END

-------------------------------------------------------------------------------
---RUN PRODUCT PROCEDURE AND EXPORT SSIS PACKAGE

EXECUTE export.EXPORT_DPS_PROD_ATTRIBUTES
--

-------------------------------------------------------------------------------
---RUN CUSTOMER PROCEDURE AND EXPORT SSIS PACKAGE

EXECUTE export.EXPORT_DPS_CUST_ATTRIBUTES
        
-------------------------------------------------------------------------------
---RUN SEGEMENT PROCEDURE AND EXPORT SSIS PACKAGE

EXECUTE [export].[EXPORT_DPS_SEGMENTS] 

-------------------------------------------------------------------------------

			EXECUTE  Export.[EXECUTE_EXPORT_SQL] 
				1930000
				

-----------------------------------------------------------------------------------

 EXECUTE export.EXEC_SSIS_EXPORT_PACKAGE 1
 EXECUTE export.EXEC_SSIS_EXPORT_PACKAGE 2
 EXECUTE export.EXEC_SSIS_EXPORT_PACKAGE 3
 EXECUTE export.EXEC_SSIS_EXPORT_PACKAGE 4
 EXECUTE export.EXEC_SSIS_EXPORT_PACKAGE 5

set @v_job_fk = (select top 1 job_id from common.JOB where NAME = 'EXPORT DPS FILES'
order by JOB_ID desc)
set @v_output_rows = (select COUNT(1) from export.EXPORT_SALES_DPS)

EXEC common.job_execution_create   @v_job_fk, 'COMPLETED EXPORT DPS FILES',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @l_sysdate, @l_sysdate  
--  


-------------------------------------------------------------------------------------------------------------

  SET @status_desc = 'finished execution of Export.EXPORT_DPS_FILES'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of Export.EXPORT_DPS_FILES has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END














