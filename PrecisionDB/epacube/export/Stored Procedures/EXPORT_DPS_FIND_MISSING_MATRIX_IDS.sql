﻿-- =============================================
-- Author: <Gary Stone>
-- Create date: <June 18, 2003>
-- Description: <Find the Host_Rule_XRef value for DPS data where it is not otherwise available>
-- =============================================
-- MODIFICATION HISTORY
-- Person Date Comments
-- --------- ---------- ------------------------------------------
-- CV 08/05/2013 changed IS NULL to include 0 and isnull for Matrix Sell ID
CREATE PROCEDURE [export].[EXPORT_DPS_FIND_MISSING_MATRIX_IDS] 
AS
BEGIN
SET NOCOUNT ON;
exec [import].[LOAD_MAUI_0_JOB] 'Rules_Matrix_Setup_For_DPS', 0, '710'
Declare @Job_FK bigint
Declare @SQLcd Varchar(Max)
Set @Job_FK = (select top 1 Job_ID from common.job where Name = 'MAUI Analysis' and Data1 = 'Rules_Matrix_Setup_For_DPS' order by JOB_ID desc)
Insert into marginmgr.ANALYSIS_JOB
(Analysis_Job_ID, Analysis_Name, result_type_cr_fk, rule_type_filter, Inner_Join_Filter, Product_Filter, Organization_Filter, Customer_Filter, RECORD_STATUS_CR_FK)
Select @Job_FK
, 'Rules_Matrix_Setup'
, 710
, '(313)'
, 'Inner Join ##Identifiers_DPS ID on PS.Product_Structure_ID = ID.Product_Structure_FK And ESC.Entity_Structure_ID = ID.Cust_Entity_Structure_FK And ESO.entity_Structure_ID = ID.Org_Entity_Structure_FK'
, '(Select distinct Product_Structure_FK from ##Identifiers_DPS)'
, '(Select distinct Org_Entity_Structure_FK from ##Identifiers_DPS)'
, '(Select distinct cust_entity_structure_fk from ##Identifiers_DPS)'
, 1
IF object_id('tempdb..##Identifiers_DPS') is not null 
drop table ##Identifiers_DPS
If (select Value from epacube.epacube_params where NAME = 'ERP HOST') = 'SXE'
Begin
Set @SQLcd = '
Select * into ##Identifiers_DPS from
(select PI.Product_structure_FK, eio.entity_structure_fk org_entity_structure_fk, eic.entity_structure_fk cust_entity_structure_fk, dps.PRODUCT_ID, dps.WHSE_ID, dps.CUST_ACCT_BILL_TO_ID, dps.CUST_ACCT_SHIP_TO_ID
from export.EXPORT_SALES_DPS dps with (Nolock)
inner join epacube.PRODUCT_IDENTIFICATION PI with (Nolock) on dps.Product_ID = PI.value 
and PI.data_name_fk = (select data_name_fk from export.EXPORT_MAP_METADATA with (nolock) where EXPORT_PACKAGE_FK = 1910000 and COLUMN_NAME = ''Product_ID'') 
inner join epacube.ENTITY_IDENTIFICATION eio with (NoLock) on dps.WHSE_ID = eio.VALUE 
and eio.DATA_NAME_FK = (select data_name_fk from export.EXPORT_MAP_METADATA with (nolock) where EXPORT_PACKAGE_FK = 1143000 and SOURCE_FIELD_NAME = ''WAREHOUSE CODE'') 
inner join epacube.ENTITY_IDENTIFICATION eic with (nolock) on eic.VALUE = case when isnull(dps.CUST_ACCT_SHIP_TO_ID, '''') = '''' then dps.CUST_ACCT_BILL_TO_ID else dps.CUST_ACCT_SHIP_TO_ID end
and eic.ENTITY_DATA_NAME_FK = case when isnull(dps.CUST_ACCT_SHIP_TO_ID, '''') = '''' then 144010 else 144020 end
where ISNULL(dps.MTX_SELL_MATRIX_ID, 0) = 0) A
Group by Product_structure_FK, org_entity_structure_fk, cust_entity_structure_fk, PRODUCT_ID, WHSE_ID, CUST_ACCT_BILL_TO_ID, CUST_ACCT_SHIP_TO_ID
'
End
else if (select Value from epacube.epacube_params where NAME = 'ERP HOST') = 'ECLIPSE'
Begin
Set @SQLcd = '
Select * into ##Identifiers_DPS from
(select PI.Product_structure_FK, eio.entity_structure_fk org_entity_structure_fk, eic.entity_structure_fk cust_entity_structure_fk, dps.PRODUCT_ID, dps.WHSE_ID, dps.CUST_ACCT_BILL_TO_ID, RIGHT([CUST_ACCT_SHIP_TO_ID], Len([CUST_ACCT_SHIP_TO_ID]) - CHARINDEX(''-'', [CUST_ACCT_SHIP_TO_ID])) CUST_ACCT_SHIP_TO_ID
from export.EXPORT_SALES_DPS dps with (Nolock)
inner join epacube.PRODUCT_IDENTIFICATION PI with (Nolock) on dps.Product_ID = PI.value 
and PI.data_name_fk = (select data_name_fk from export.EXPORT_MAP_METADATA with (nolock) where EXPORT_PACKAGE_FK = 1910000 and COLUMN_NAME = ''Product_ID'') 
inner join epacube.ENTITY_IDENTIFICATION eio with (NoLock) on dps.WHSE_ID = eio.VALUE 
and eio.DATA_NAME_FK = (select data_name_fk from export.EXPORT_MAP_METADATA with (nolock) where EXPORT_PACKAGE_FK = 1143000 and SOURCE_FIELD_NAME = ''WAREHOUSE CODE'') 
inner join epacube.ENTITY_IDENTIFICATION eic with (nolock) on eic.VALUE = case when isnull(dps.CUST_ACCT_SHIP_TO_ID, '''') = '''' then dps.CUST_ACCT_BILL_TO_ID else RIGHT([CUST_ACCT_SHIP_TO_ID], Len([CUST_ACCT_SHIP_TO_ID]) - CHARINDEX(''-'', [CUST_ACCT_SHIP_TO_ID])) end
and eic.ENTITY_DATA_NAME_FK = case when isnull(dps.CUST_ACCT_SHIP_TO_ID, '''') = '''' then 144010 else 144020 end
where ISNULL(dps.MTX_SELL_MATRIX_ID, 0) = 0) A
Group by Product_structure_FK, org_entity_structure_fk, cust_entity_structure_fk, PRODUCT_ID, WHSE_ID, CUST_ACCT_BILL_TO_ID, CUST_ACCT_SHIP_TO_ID
'
End
Exec(@SQLcd)
Create Index idx_ids on ##Identifiers_DPS(Product_structure_FK, org_entity_structure_fk, cust_entity_structure_fk, PRODUCT_ID, WHSE_ID, CUST_ACCT_BILL_TO_ID, CUST_ACCT_SHIP_TO_ID)
EXEC [marginmgr].[CALCULATION_PROCESS] @Job_FK
If (select Value from epacube.epacube_params where NAME = 'ERP HOST') = 'SXE'
Begin
Set @SQLcd = '
Update DPS
SET MTX_SELL_MATRIX_ID = (select host_rule_xref from synchronizer.RULES where RULES_ID = ec.RULES_FK)
from export.EXPORT_SALES_DPS dps
inner join ##Identifiers_DPS ID with (nolock) on dps.PRODUCT_ID = ID.PRODUCT_ID and dps.WHSE_ID = ID.WHSE_ID and ISNULL(dps.CUST_ACCT_SHIP_TO_ID, dps.CUST_ACCT_BILL_TO_ID) = ISNULL(ID.CUST_ACCT_SHIP_TO_ID, ID.CUST_ACCT_BILL_TO_ID)
inner join synchronizer.EVENT_CALC ec with (nolock) on ID.PRODUCT_STRUCTURE_FK = ec.PRODUCT_STRUCTURE_FK and id.org_entity_structure_fk = ec.ORG_ENTITY_STRUCTURE_FK and id.cust_entity_structure_fk = ec.CUST_ENTITY_STRUCTURE_FK
where ISNULL(dps.MTX_SELL_MATRIX_ID, 0) = 0
'
End
else if (select Value from epacube.epacube_params where NAME = 'ERP HOST') = 'ECLIPSE'
Begin
Set @SQLcd = '
Update DPS
Set MTX_SELL_MATRIX_ID = (select host_rule_xref from synchronizer.RULES where RULES_ID = ec.RULES_FK)
from export.EXPORT_SALES_DPS dps
inner join ##Identifiers_DPS ID with (nolock) on dps.PRODUCT_ID = ID.PRODUCT_ID and dps.WHSE_ID = ID.WHSE_ID 
and ISNULL(RIGHT(dps.[CUST_ACCT_SHIP_TO_ID], Len(dps.[CUST_ACCT_SHIP_TO_ID]) - CHARINDEX(''-'', dps.[CUST_ACCT_SHIP_TO_ID])), dps.CUST_ACCT_BILL_TO_ID)
= ISNULL(ID.CUST_ACCT_SHIP_TO_ID, ID.CUST_ACCT_BILL_TO_ID)
inner join synchronizer.EVENT_CALC ec with (nolock) on ID.PRODUCT_STRUCTURE_FK = ec.PRODUCT_STRUCTURE_FK and id.org_entity_structure_fk = ec.ORG_ENTITY_STRUCTURE_FK and id.cust_entity_structure_fk = ec.CUST_ENTITY_STRUCTURE_FK
where ISNULL(dps.MTX_SELL_MATRIX_ID, 0) = 0
'
End
Exec(@SQLcd)
EXEC marginmgr.UTIL_PURGE_MARGIN_TABLES @Job_FK, 0
IF object_id('tempdb..##Identifiers_DPS') is not null 
drop table ##Identifiers_DPS
END
 
