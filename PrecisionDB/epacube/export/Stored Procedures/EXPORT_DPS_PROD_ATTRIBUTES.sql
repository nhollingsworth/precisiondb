﻿








CREATE PROCEDURE [export].[EXPORT_DPS_PROD_ATTRIBUTES] 
         
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        02/06/2013  Initial SQL Version
-- RG		 03/29/2013	  Additions 
-- CV        04/22/2013  Fixes to data once imported  
-- CV        05/03/2013  update sales if price type different.
-- CV        05/10/2013  update ucc if null
-- CV        05/28/2013  Adding Eclipsisms
-- CV        06/15/2013  Added product lifecycle
-- CV        06/21/2013  Check non unique table for ident
-- CV        07/19/2013  Tighten how we look for data names used
-- CV        09/26/2013  Changed logic for using alternate data name for product groups 
-- CV        10/08/2013  Remove select Top 1 and use variable entity used
-- CV        10/29/2013  Changed primaryVendorDNID to SX ARP VENDOR instead of PRIMARY VENDOR
-- TM	     10/31/2013  removed data_name_fk from Default Warehouse where clause for SegProd_Ent_Hist
-- CV        11/05/2013  use all whse and if null use default warehouse
-- TM        11/14/2013  Prod Group 1 2 3 looks at both Corp and Default Warehouse and picks distinct
-- TM		 11/21/2013  Prod Group 1 2 3 no workey. change back to default whse first. then add update to look at corp below.
-- CV	     12/03/2013  Added to use alternate data name f0r product display if null
-- TM        01/22/2014  Added Logging
-- CV        01/27/2014  Worked on the insert- removed extra join and added more updates for product groups1-3
-- RG		 04/16/2014  Reworking Price Type Update due to poor performance
-- CV        01/13/2017  Rules type cr fk from rules table
  
  
  
  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @v_batch_no   bigint
        
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @in_v_package_fk      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT
DECLARE @primaryVendorDNID int
DECLARE @ERP_HOST   Varchar(32)
DECLARE @v_entity_datavalue   int
DECLARE @v_entity_used   int
DECLARE @v_data_name_used   int
        
 
 
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of export.EXPORT_PRODUCT_ATTRIBUTES_DPS '
      
	  set @primaryVendorDNID = epacube.getDataNameId('SX ARP VENDOR')
	  	  
	  SET @ERP_HOST = 
			(Select value from epacube.EPACUBE_PARAMS where NAME = 'ERP HOST')

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
 

       SET @in_v_package_fk = (select export_Package_id from export.export_package where name = 'PRODUCT ATTRIBUTE')  
	   
	   SET @v_entity_used = (select ENTITY_STRUCTURE_FK from epacube.ENTITY_IDENTIFICATION where value in (
select DEFAULT_VALUE from export.export_map_metadata where COLUMN_NAME = 'DEFAULT WAREHOUSE') and ENTITY_DATA_NAME_FK = 141000)
	   
	    Set @v_entity_datavalue = CASE WHEN  
	   (select ds.COLUMN_NAME from  epacube.DATA_SET ds
		inner join epacube.DATA_NAME dn
	   on (dn.DATA_SET_FK = ds.DATA_SET_ID)
	   where dn.DATA_NAME_ID = (select distinct DATA_NAME_FK from epacube.PRODUCT_CATEGORY where DATA_NAME_FK in 
(Select DATA_NAME_FK  from export.EXPORT_MAP_METADATA
where COLUMN_NAME = 'PROD_GROUP1'))) = 'ENTITY_DATA_VALUE_FK' then 1 Else 0 END

--------------------------------------------------------------------------------------                
truncate table export.EXPORT_PRODUCT_ATTRIBUTE_DPS

---------------------------------------------------------------------------------------
----PRODUCT LIST-----
---------------------------------------------------------------------------------------
if @ERP_HOST = 'SXE'
BEGIN

INSERT INTO [export].[EXPORT_PRODUCT_ATTRIBUTE_DPS]
           ([PROD_DESCRIPTION]
           ,[PROD_DISPLAY]
           ,[PROD_GROUP1]
           ,[PROD_GROUP2]
           ,[PROD_GROUP3]
           ,[PROD_GTIN]
           ,[PROD_LIFECYCLE]
           ,[PROD_RANK]
           ,[PROD_UPC]
           ,[PROD_UCC]
           ,[PRODUCT_ID]
           ,[SEGLIST_PROD_MARKETS]
           ,[SEGLIST_PROD_REBATES]
           ,[SEGPROD_ENT_HIST]
           ,[VENDOR_ID]
           ,[VENDOR_MFR_ID]
           ,[VENDOR_MFG_PART_NO]
           ,[VENDOR_PART_NO])
(SELECT  DISTINCT
(select description from epacube.product_description where product_structure_Fk = pi.product_Structure_fk
and data_name_fk in (select data_name_fk from export.export_map_metadata where column_name = 'DESCRIPTION'
and export_package_fk = @in_v_package_fk ))  --Description
 ,isNULL((select description from epacube.product_description where product_structure_Fk = pi.product_Structure_fk
and data_name_fk in (select data_name_fk from export.export_map_metadata where column_name = 'DISPLAY_DESCRIPTION'
and export_package_fk = @in_v_package_fk )),  esh.PRODUCT_ID ) --Prod Display

,CASE when @v_entity_datavalue = 0 THEN

	   (select  distinct dv.value from epacube.product_category pc 
					inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
					where pc.product_structure_Fk =  pi.product_Structure_fk
					and pc.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
					and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
											where column_name = 'PROD_GROUP1'
							 				and export_package_fk = @in_v_package_fk ))

ELSE
		

	(select  distinct dv.value from epacube.product_category pc 
					inner join epacube.entity_data_value dv on (dv.ENTITY_DATA_VALUE_ID = pc.entity_data_value_fk)
					where pc.product_structure_Fk =  pi.product_Structure_fk
					and pc.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
					and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
											where column_name = 'PROD_GROUP1'
							 				and export_package_fk = @in_v_package_fk )) 
		END    -----product group 1


,CASE when @v_entity_datavalue = 0 THEN

	   (select distinct dv.value from epacube.product_category pc 
					inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
					where pc.product_structure_Fk =  pi.product_Structure_fk
					and pc.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
					and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
											where column_name = 'PROD_GROUP2'
							 				and export_package_fk = @in_v_package_fk ))

ELSE
		

	(select distinct dv.value from epacube.product_category pc 
					inner join epacube.entity_data_value dv on (dv.ENTITY_DATA_VALUE_ID = pc.entity_data_value_fk)
					where pc.product_structure_Fk =  pi.product_Structure_fk
					and pc.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
					and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
											where column_name = 'PROD_GROUP2'
							 				and export_package_fk = @in_v_package_fk )) 
		END    -----product group2



,CASE when @v_entity_datavalue = 0 THEN

	   (select distinct dv.value from epacube.product_category pc 
					inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
					where pc.product_structure_Fk =  pi.product_Structure_fk
					and pc.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
					and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
											where column_name = 'PROD_GROUP3'
							 				and export_package_fk = @in_v_package_fk ))

ELSE
		

	(select distinct dv.value from epacube.product_category pc 
					inner join epacube.entity_data_value dv on (dv.ENTITY_DATA_VALUE_ID = pc.entity_data_value_fk)
					where pc.product_structure_Fk =  pi.product_Structure_fk
					and pc.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
					and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
											where column_name = 'PROD_GROUP3'
							 				and export_package_fk = @in_v_package_fk )) 
		END    -----product group 3


           ,(select value from epacube.product_identification 
		    where product_structure_Fk = pi.product_Structure_fk
			and data_name_fk in (select data_name_fk from export.export_map_metadata 
			where column_name = 'PROD_GTIN' and export_package_fk = @in_v_package_fk ))  
			--[PROD_GTIN]
,CASE when ISNULL((select dv.value from epacube.product_category pc 
					inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
					where pc.product_structure_Fk =  pi.product_Structure_fk
					and pc.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
					and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
											where column_name = 'PROD_LIFECYCLE'
											and export_package_fk = @in_v_package_fk )) , NULL) 
		is NULL
then 
		(select  dv.value from epacube.product_category pc 
		inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
		where pc.product_structure_Fk =  pi.product_Structure_fk
		and pc.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
		and pc.data_name_fk in (select Alternate_data_name_fk from export.export_map_metadata 
		where column_name = 'PROD_LIFECYCLE' and export_package_fk = @in_v_package_fk ) )
ELSE
		(select  dv.value from epacube.product_category pc 
		inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
		where pc.product_structure_Fk =  pi.product_Structure_fk
		and pc.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
		and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
		where column_name = 'PROD_LIFECYCLE' and export_package_fk = @in_v_package_fk ) )
END --[PROD_LIFECYCLE] 
           ,NULL --[PROD_RANK] 110998
           ,(select value from epacube.product_identification 
			 where product_structure_Fk = pi.product_Structure_fk
			 and data_name_fk in (select data_name_fk from export.export_map_metadata 
								  where column_name = 'PROD_UPC'
								  and export_package_fk = @in_v_package_fk )) --[PROD_UPC]
           ,(select dv.value from epacube.product_category pc 
			inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
			where pc.product_structure_Fk =  pi.product_Structure_fk
			and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
									where column_name = 'PROD_UCC'
									and export_package_fk = @in_v_package_fk )) --[PROD_UCC]
			,pi.value 

			,NULL --[SEGLIST_PROD_MARKETS]

			,(select value from epacube.data_value (nolock)
			where data_value_id = (select pc.data_value_fk 
			from epacube.product_category pc (nolock)
			where pc.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
						and pc.ORG_ENTITY_STRUCTURE_FK = 1
						and pc.data_name_fk = (select data_name_fk from export.export_map_metadata 
												where column_name = 'SEGLIST_PROD_REBATES' 
												and export_package_fk = @in_v_package_fk ))
			) --[SEGLIST_PROD_REBATES] 211903
								       
--			,CASE when ISNULL((select dv.value from epacube.product_category pc 
--					inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
--					where pc.product_structure_Fk =  pi.product_Structure_fk
--					and pc.ORG_ENTITY_STRUCTURE_FK = 1--@v_entity_used
--					and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
--											where column_name = 'SEGPROD_ENT_HIST'
--											and export_package_fk = @in_v_package_fk )) , NULL) 
--		is NULL
--then 
--		(select  dv.value from epacube.product_category pc 
--		inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
--		where pc.product_structure_Fk =  pi.product_Structure_fk
--		and pc.ORG_ENTITY_STRUCTURE_FK = 1 --@v_entity_used
--		and pc.data_name_fk in (select Alternate_data_name_fk from export.export_map_metadata 
--		where column_name = 'SEGPROD_ENT_HIST' and export_package_fk = @in_v_package_fk ) )
--ELSE
--		(select  dv.value from epacube.product_category pc 
--		inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
--		where pc.product_structure_Fk =  pi.product_Structure_fk
--		and pc.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
--		and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
--		where column_name = 'SEGPROD_ENT_HIST' and export_package_fk = @in_v_package_fk ) )
--END--    --[SEGPROD_ENT_HIST] 211901

,ISNULL((select dv.value from epacube.product_category pc 
					inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
					where pc.product_structure_Fk =  pi.product_Structure_fk
					and pc.ORG_ENTITY_STRUCTURE_FK = 1--@v_entity_used
					and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
											where column_name = 'SEGPROD_ENT_HIST'
											and export_package_fk = @in_v_package_fk )) , 
		(select  dv.value from epacube.product_category pc 
		inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
		where pc.product_structure_Fk =  pi.product_Structure_fk
		and pc.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
		and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
		where column_name = 'SEGPROD_ENT_HIST' and export_package_fk = @in_v_package_fk ) ))
   --[SEGPROD_ENT_HIST] 211901
           ,(select ei.value from epacube.entity_identification ei (nolock) 
			inner join epacube.product_association pa (nolock) 
			on (pa.entity_structure_fk = ei.entity_structure_fk and pa.data_name_fk = @primaryVendorDNID)
			where pa.product_structure_fk = pi.product_structure_fk
			and pa.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
			and ei.data_name_fk in (select data_name_fk from export.export_map_metadata 
									where column_name = 'VENDOR_ID' and export_package_fk = @in_v_package_fk ) ) --[VENDOR_ID]

            ,isNULL((select ei.value from epacube.ENTITY_IDENTIFICATION ei (nolock) 
			inner join epacube.product_association pa (nolock) 
			on (pa.entity_structure_fk = ei.entity_structure_fk and pa.data_name_fk = @primaryVendorDNID)
			where pa.product_structure_fk = pi.product_structure_fk
			and pa.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
			and ei.data_name_fk in (select data_name_fk from export.export_map_metadata 
									where column_name = 'VENDOR_MFR_ID' and export_package_fk = @in_v_package_fk ))
			,(select ei.value from epacube.ENTITY_IDENT_NONUNIQUE ei (nolock) 
			inner join epacube.product_association pa (nolock) 
			on (pa.entity_structure_fk = ei.entity_structure_fk and pa.data_name_fk = @primaryVendorDNID)
			where pa.product_structure_fk = pi.product_structure_fk
			and pa.ORG_ENTITY_STRUCTURE_FK = @v_entity_used
			and ei.data_name_fk in (select data_name_fk from export.export_map_metadata 
									where column_name = 'VENDOR_MFR_ID' and export_package_fk = @in_v_package_fk ) )) --[VENDOR_MFR_ID] 143112

           ,(select top 1 value from epacube.product_identification (nolock) 
		     where product_structure_Fk = pi.product_Structure_fk
			 and data_name_fk in (select data_name_fk from export.export_map_metadata (nolock) 
								  where column_name = 'VENDOR_MFG_PART_NO'
									and export_package_fk = @in_v_package_fk )) --[VENDOR_MFG_PART_NO] 110101

           ,(select top 1 value from epacube.product_identification (nolock) 
		     where product_structure_Fk = pi.product_Structure_fk
			 and data_name_fk in (select data_name_fk from export.export_map_metadata (nolock) 
									where column_name = 'VENDOR_PART_NO' and export_package_fk = @in_v_package_fk )) --[VENDOR_PART_NO]

from export.export_sales_dps ESH (nolock)
inner join epacube.product_identification pi (nolock)
on (esh.product_id = pi.value
and pi.data_name_fk = 
(select data_name_fk from export.export_map_metadata (nolock) where column_name = 'PRODUCT_ID'
and export_package_fk = @in_v_package_fk))
--inner join import.import_sxe_sales_transactions_Active isst
--on (isst.prod = esh.PRODUCT_ID)
)



	  SET @ls_stmt = 'started export.EXPORT_PRODUCT_ATTRIBUTES_DPS - UPDATE PROD GROUP 123'

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;


------------------------------------------------------------------------------------------
--- adding update to prod groups 1-3 if null this time using alternate data name
------------------------------------------------------------------------------------------
  Set @v_entity_datavalue = NULL  -- clear it out first
 
  Set @v_entity_datavalue = CASE WHEN  
	  (select ds.COLUMN_NAME from  epacube.DATA_SET ds
		inner join epacube.DATA_NAME dn
	   on (dn.DATA_SET_FK = ds.DATA_SET_ID)
	   where dn.DATA_NAME_ID = (select distinct DATA_NAME_FK from epacube.PRODUCT_CATEGORY where DATA_NAME_FK in 
(Select ALTERNATE_DATA_NAME_FK  from export.EXPORT_MAP_METADATA
where COLUMN_NAME = 'PROD_GROUP1'))) = 'ENTITY_DATA_VALUE_FK' then 1 Else 0 END


--drop temp table if it exists
	IF object_id('tempdb..#TS_PRODUCTS') is not null
	   drop table #TS_PRODUCTS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_PRODUCTS(
   	[PRODUCT_ID] [varchar](128),
	[PRODUCT_STRUCTURE_FK] [bigint])
	
	
------------------------------------------------------------------	
	CREATE NONCLUSTERED INDEX [IDX_PRODUCT_ID] ON  #TS_PRODUCTS 
(
	[PRODUCT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

------------------------------------------------------------------


Insert into #TS_PRODUCTS
select pdps.PRODUCT_ID, pi.PRODUCT_STRUCTURE_FK 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS pdps  with (nolock)
 inner join epacube.product_identification pi (nolock)
on (pdps.product_id = pi.value
and pi.data_name_fk = 
(select data_name_fk from export.export_map_metadata (nolock) where column_name = 'PRODUCT_ID'))
where pdps.PROD_GROUP1 is NULL

if  @v_entity_datavalue = 0
begin

update edps
set prod_group1 =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.data_name_fk in (select DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'PROD_GROUP1'))
											inner join epacube.data_value dv with (nolock)
					 on (dv.data_VALUE_id = pc.data_value_fk)
					 where edps.PROD_GROUP1 is NULL

update edps
set prod_group1 =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.data_name_fk in (select ALTERNATE_DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'PROD_GROUP1'))
											inner join epacube.data_value dv with (nolock)
					 on (dv.data_VALUE_id = pc.data_value_fk)
					 where edps.PROD_GROUP1 is NULL


	
	 END 
	 if  @v_entity_datavalue = 1
begin
 



update edps
set prod_group1 =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.data_name_fk in (select ALTERNATE_DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'PROD_GROUP1'))
											inner join epacube.entity_data_value dv with (nolock)
					 on (dv.entity_data_VALUE_id = pc.entity_data_value_fk)
					 where edps.PROD_GROUP1 is NULL
					 
      END  ---alternate data name prod group 1
      
if  @v_entity_datavalue = 0
begin

update edps
set prod_group2 =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.data_name_fk in (select DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'PROD_GROUP2'))
											inner join epacube.data_value dv with (nolock)
					 on (dv.data_VALUE_id = pc.data_value_fk)
					 where edps.PROD_GROUP2 is NULL

update edps
set prod_group2 =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.data_name_fk in (select ALTERNATE_DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'PROD_GROUP2'))
											inner join epacube.data_value dv with (nolock)
					 on (dv.data_VALUE_id = pc.data_value_fk)
					 where edps.PROD_GROUP2 is NULL
	
	 END 
	 if  @v_entity_datavalue = 1
begin
 
update edps
set prod_group2 =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.data_name_fk in (select ALTERNATE_DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'PROD_GROUP2'))
											inner join epacube.entity_data_value dv with (nolock)
					 on (dv.entity_data_VALUE_id = pc.entity_data_value_fk)
					 where edps.PROD_GROUP2 is NULL
					 
      END  ---alternate data name group 2
      
if  @v_entity_datavalue = 0
begin

update edps
set prod_group3 =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.data_name_fk in (select DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'PROD_GROUP3'))
											inner join epacube.data_value dv with (nolock)
					 on (dv.data_VALUE_id = pc.data_value_fk)
					 where edps.PROD_GROUP3 is NULL

update edps
set prod_group3 =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.data_name_fk in (select ALTERNATE_DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'PROD_GROUP3'))
											inner join epacube.data_value dv with (nolock)
					 on (dv.data_VALUE_id = pc.data_value_fk)
					 where edps.PROD_GROUP3 is NULL
	
	 END 
	 if  @v_entity_datavalue = 1
begin
 
update edps
set prod_group3 =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.data_name_fk in (select ALTERNATE_DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'PROD_GROUP3'))
											inner join epacube.entity_data_value dv with (nolock)
					 on (dv.entity_data_VALUE_id = pc.entity_data_value_fk)
					 where edps.PROD_GROUP3 is NULL
					 
      END  ---alternate data name





	  SET @ls_stmt = 'started export.EXPORT_PRODUCT_ATTRIBUTES_DPS - UPDATE PROD GROUP 123 FROM CORP'

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;


-----------------------------------------------------------------
--IF Prod Group is STILL NULL, look at corp level
-------------------------------------------------------------------------

update edps
set prod_group1 =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.ORG_ENTITY_STRUCTURE_FK = 1
and pc.data_name_fk in (select DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'PROD_GROUP1'))
											inner join epacube.entity_data_value dv with (nolock)
					 on (dv.entity_data_VALUE_id = pc.entity_data_value_fk)
					 where edps.PROD_GROUP1 is NULL
------------------------------------------------------------------------
update edps
set prod_group2 =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.ORG_ENTITY_STRUCTURE_FK = 1
and pc.data_name_fk in (select DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'PROD_GROUP2'))
											inner join epacube.entity_data_value dv with (nolock)
					 on (dv.entity_data_VALUE_id = pc.entity_data_value_fk)
					 where edps.PROD_GROUP2 is NULL
------------------------------------------------------------------------
update edps
set prod_group3 =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.ORG_ENTITY_STRUCTURE_FK = 1
and pc.data_name_fk in (select DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'PROD_GROUP3'))
											inner join epacube.entity_data_value dv with (nolock)
					 on (dv.entity_data_VALUE_id = pc.entity_data_value_fk)
					 where edps.PROD_GROUP3 is NULL
------------------------------------------------------------------------
truncate table #ts_products

Insert into #TS_PRODUCTS
select pdps.PRODUCT_ID, pi.PRODUCT_STRUCTURE_FK 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS pdps  with (nolock)
 inner join epacube.product_identification pi (nolock)
on (pdps.product_id = pi.value
and pi.data_name_fk = 
(select data_name_fk from export.export_map_metadata (nolock) where column_name = 'PRODUCT_ID'))
where pdps.SEGPROD_ENT_HIST is NULL

if  @v_entity_datavalue = 0
begin

update edps
set SEGPROD_ENT_HIST =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.data_name_fk in (select ALTERNATE_DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'SEGPROD_ENT_HIST'))
											inner join epacube.data_value dv with (nolock)
					 on (dv.data_VALUE_id = pc.data_value_fk)
					 where edps.SEGPROD_ENT_HIST is NULL
	
	 END 
	 if  @v_entity_datavalue = 1
begin
 
update edps
set SEGPROD_ENT_HIST =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.data_name_fk in (select ALTERNATE_DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'SEGPROD_ENT_HIST'))
											inner join epacube.entity_data_value dv with (nolock)
					 on (dv.entity_data_VALUE_id = pc.entity_data_value_fk)
					 where edps.SEGPROD_ENT_HIST is NULL
					 
      END  ---alternate data name SEGPROD_ENT_HIST



	  SET @ls_stmt = 'started export.EXPORT_PRODUCT_ATTRIBUTES_DPS - UPDATE PROD GROUP 123 FROM ANY WHSE'

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;

  
  -----------------------if still NULL just get value no matter what org.    
 update edps
set SEGPROD_ENT_HIST =  dv.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join #TS_PRODUCTS tp with (nolock)
					 on (edps.PRODUCT_ID = tp.PRODUCT_ID)
					 inner join epacube.PRODUCT_CATEGORY pc with (nolock)
on (pc.PRODUCT_STRUCTURE_FK = tp.PRODUCT_STRUCTURE_FK
and pc.data_name_fk = (select DATA_NAME_FK from export.export_map_metadata 
											where column_name = 'SEGPROD_ENT_HIST'))
											inner join epacube.data_value dv with (nolock)
					 on (dv.data_VALUE_id = pc.data_value_fk)
					 where edps.SEGPROD_ENT_HIST is NULL
      
 END     
---------------------------------------------------------------------------------------------------------------
update edps
set vendor_id  =  ei.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS edps with (nolock)
inner join epacube.product_identification pi with (nolock) 
on (edps.product_id = pi.value
and pi.data_name_fk = 
(select data_name_fk from export.export_map_metadata (nolock) where column_name = 'PRODUCT_ID'
and export_package_fk = @in_v_package_fk))
		inner join epacube.product_association pa (nolock) 
			on ( pa.product_structure_fk = pi.product_structure_fk
			and pa.data_name_fk =  @primaryVendorDNID)
			inner join epacube.entity_identification ei with (nolock)
			on (pa.entity_structure_fk = ei.entity_structure_fk
			and ei.data_name_fk in (select data_name_fk from export.export_map_metadata 
									where column_name = 'VENDOR_ID'))
					 where edps.vendor_id is NULL
					 
-------------------------------------------------------------------------------------------		
 
			 
if @ERP_HOST = 'ECLIPSE'
BEGIN

INSERT INTO [export].[EXPORT_PRODUCT_ATTRIBUTE_DPS]
           ([PROD_DESCRIPTION]
           ,[PROD_DISPLAY]
           ,[PROD_GROUP1]
           ,[PROD_GROUP2]
           ,[PROD_GROUP3]
           ,[PROD_GTIN]
           ,[PROD_LIFECYCLE]
           ,[PROD_RANK]
           ,[PROD_UPC]
           ,[PROD_UCC]
           ,[PRODUCT_ID]
           ,[SEGLIST_PROD_MARKETS]
           ,[SEGLIST_PROD_REBATES]
           ,[SEGPROD_ENT_HIST]
           ,[VENDOR_ID]
           ,[VENDOR_MFR_ID]
           ,[VENDOR_MFG_PART_NO]
           ,[VENDOR_PART_NO])
(SELECT  DISTINCT
(select description from epacube.product_description where product_structure_Fk = pi.product_Structure_fk
and data_name_fk in (select data_name_fk from export.export_map_metadata where column_name = 'DESCRIPTION'
and export_package_fk = @in_v_package_fk ))  --Description
 ,(select value from epacube.product_ident_nonunique (nolock) 
		     where product_structure_Fk = pi.product_Structure_fk
			 and data_name_fk in (select data_name_fk from export.export_map_metadata (nolock) 
									where column_name = 'DISPLAY_DESCRIPTION' and export_package_fk = @in_v_package_fk )) --Prod Display

,(select dv.value from epacube.product_category pc 
		inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
		where pc.product_structure_Fk =  pi.product_Structure_fk
		and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
		where column_name = 'PROD_GROUP1' and export_package_fk = @in_v_package_fk ) )
 --[PROD_GROUP1] 
,		(select dv.value from epacube.product_category pc 
		inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
		where pc.product_structure_Fk =  pi.product_Structure_fk
		and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
		where column_name = 'PROD_GROUP2' and export_package_fk = @in_v_package_fk ) )
--[PROD_GROUP2]
,
		(select dv.value from epacube.product_category pc 
		inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
		where pc.product_structure_Fk =  pi.product_Structure_fk
		and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
		where column_name = 'PROD_GROUP3' and export_package_fk = @in_v_package_fk ) )
 --[PROD_GROUP3]


           ,(select value from epacube.product_identification 
		    where product_structure_Fk = pi.product_Structure_fk
			and data_name_fk in (select data_name_fk from export.export_map_metadata 
			where column_name = 'PROD_GTIN' and export_package_fk = @in_v_package_fk ))  --[PROD_GTIN]
           ,NULL --[PROD_LIFECYCLE] 110981
           ,NULL --[PROD_RANK] 110998
           ,(select value from epacube.product_identification 
			 where product_structure_Fk = pi.product_Structure_fk
			 and data_name_fk in (select data_name_fk from export.export_map_metadata 
								  where column_name = 'PROD_UPC'
								  and export_package_fk = @in_v_package_fk )) --[PROD_UPC]
           ,(select dv.value from epacube.product_category pc 
			inner join epacube.data_value dv on (dv.data_VALUE_id = pc.data_value_fk)
			where pc.product_structure_Fk =  pi.product_Structure_fk
			and pc.data_name_fk in (select data_name_fk from export.export_map_metadata 
									where column_name = 'PROD_UCC'
									and export_package_fk = @in_v_package_fk )) --[PROD_UCC]
			,pi.value 

			,NULL --[SEGLIST_PROD_MARKETS]

			,(select value from epacube.data_value (nolock)
			where data_value_id = (select pc.data_value_fk 
			from epacube.product_category pc (nolock)
			where pc.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
						and pc.ORG_ENTITY_STRUCTURE_FK = 1
						and pc.data_name_fk = (select data_name_fk from export.export_map_metadata 
												where column_name = 'SEGLIST_PROD_REBATES' 
												and export_package_fk = @in_v_package_fk ))
			) --[SEGLIST_PROD_REBATES] 211903
								       
		,(select dv.VALUE from epacube.data_value dv
           inner join epacube.product_category pc
           on (pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
           and dv.DATA_NAME_FK = (select data_name_fk from export.export_map_metadata 
									where column_name = 'SEGPROD_ENT_HIST' and export_package_fk = @in_v_package_fk )
									and pc.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK ))   --[SEGPROD_ENT_HIST] 

           ,(select dv.VALUE from epacube.data_value dv
           inner join epacube.product_category pc
           on (pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
           and dv.DATA_NAME_FK = (select data_name_fk from export.export_map_metadata 
									where column_name = 'VENDOR_ID' and export_package_fk = @in_v_package_fk )
									and pc.PRODUCT_STRUCTURE_FK =  pi.PRODUCT_STRUCTURE_FK )) --[VENDOR_ID]

            ,(select ei.value from epacube.ENTITY_IDENTIFICATION ei (nolock) 
			inner join epacube.product_association pa (nolock) 
			on (pa.entity_structure_fk = ei.entity_structure_fk and pa.data_name_fk = @primaryVendorDNID)
			where pa.product_structure_fk = pi.product_structure_fk
			and ei.data_name_fk in (select data_name_fk from export.export_map_metadata 
									where column_name = 'VENDOR_MFR_ID' and export_package_fk = @in_v_package_fk ) ) --[VENDOR_MFR_ID] 143112

           ,(select top 1 value from epacube.product_identification (nolock) 
		     where product_structure_Fk = pi.product_Structure_fk
			 and data_name_fk in (select data_name_fk from export.export_map_metadata (nolock) 
								  where column_name = 'VENDOR_MFG_PART_NO'
									and export_package_fk = @in_v_package_fk )) --[VENDOR_MFG_PART_NO] 110101

           ,(select value from epacube.product_ident_nonunique (nolock) 
		     where product_structure_Fk = pi.product_Structure_fk
			 and data_name_fk in (select data_name_fk from export.export_map_metadata (nolock) 
									where column_name = 'VENDOR_PART_NO' and export_package_fk = @in_v_package_fk )) --[VENDOR_PART_NO]

from export.export_sales_dps ESH (nolock)
inner join epacube.product_identification pi (nolock)
on (esh.product_id = pi.value
and pi.data_name_fk = 
(select data_name_fk from export.export_map_metadata (nolock) where column_name = 'PRODUCT_ID'
and export_package_fk = @in_v_package_fk))
)

END

	  SET @ls_stmt = 'started export.EXPORT_PRODUCT_ATTRIBUTES_DPS - UPDATE UCC'

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;


------------------------------------------------------------------------------------
---SET UCC IF NULL FROM UPC
------------------------------------------------------------------------------------
update export.EXPORT_PRODUCT_ATTRIBUTE_DPS
set Prod_UCC = substring(PROD_UPC,1,6)
where PROD_UCC is NULL



	  SET @ls_stmt = 'started export.EXPORT_PRODUCT_ATTRIBUTES_DPS - UPDATE PROD_DISPLAY'

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;


--------------------------------------------------------------------------------------
---update if prod_display is still NULL using alternate data name
--------------------------------------------------------------------------------------
update  esd
set prod_display =   pialt.value
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS esd with (nolock)
 inner join epacube.product_identification  pi with (nolock) 
 on (esd.PRODUCT_ID = pi.VALUE
 and pi.DATA_NAME_FK = (select data_name_fk from export.export_map_metadata (nolock) where column_name = 'PRODUCT_ID'
and export_package_fk = @in_v_package_fk))
inner join epacube.PRODUCT_IDENTIFICATION pialt
on ( pi.product_structure_Fk = pialt.product_Structure_fk
			 and pialt.data_name_fk in (select ALTERNATE_DATA_NAME_FK from export.export_map_metadata (nolock) 
				where column_name = 'DISPLAY_DESCRIPTION' and export_package_fk = @in_v_package_fk )) --Prod Display
				where esd.PROD_DISPLAY is NULL

--------------------------------------------------------------------------------
--on Sales History File need to update ---- [PROD_VELOCITY_RANK]
--------------------------------------------------------------------------------

 ----in product attribute file we are created we need company rank from icsw  to go in to - product rank..  
 ----in sales history file   prod_velocity rank looks at product rank in product file and if same then NULL
 ----if different then we want data position111 from icsw

--update export.EXPORT_SALES_DPS
--set PROD_VELOCITY_RANK =  
--when something.


--select dv1.value from epacube.product_category prodrank 
--inner join epacube.data_value dv1
--on (prodrank.DATA_VALUE_FK = dv1.DATA_VALUE_ID)
--where prodrank.DATA_NAME_FK = 291841


--select dv1.value, PRODUCT_STRUCTURE_FK from epacube.product_category WHSErank 
--inner join epacube.data_value dv1
--on (WHSErank.DATA_VALUE_FK = dv1.DATA_VALUE_ID)
--where Whserank.DATA_NAME_FK = 211857

	  SET @ls_stmt = 'started export.EXPORT_PRODUCT_ATTRIBUTES_DPS - PROD PRICE-REBATE TYPE LOOP'

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;


------------------------------------------------------------------------------------
---LOOP TO GET MULITPLE VALUES AND LIST AS STRING FOR PRODUCT REBATE TYPE
-----------------------------------------------------------------------------------
--If @Mult_price_type = 1 

--BEGIN

 
	DECLARE @v_prod_structure bigint
	DECLARE @v_prod_value varchar(50)
	
	DECLARE  cur_v_import cursor local for
select distinct pi.PRODUCT_STRUCTURE_FK, pi.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS pad (nolock)
inner join epacube.PRODUCT_IDENTIFICATION pi (nolock)
on (pad.PRODUCT_ID = pi.value 
and pi.DATA_NAME_FK = 
(select data_name_fk from export.export_map_metadata (nolock) where column_name = 'PRODUCT_ID'
and export_package_fk = @in_v_package_fk)	)

													
		OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO 
				 @v_prod_structure,
				 @v_prod_value
				 				
				
         WHILE @@FETCH_STATUS = 0 
         BEGIN
            
			
--------2nd Loop to string the product rebate type
----------------------------------------------------------------------------------
			DECLARE @v_LIST_VALUES VARCHAR(MAX)
			 
			 SET @v_LIST_VALUES = ''
			 SELECT @v_LIST_VALUES =  T.value + '; ' + @v_LIST_VALUES
			 from 
			 (select distinct dv.value
			FROM epacube.data_value dv (nolock)
			inner join epacube.product_category pc (nolock)
			on (pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
			and pc.product_STRUCTURE_FK = @v_prod_structure )
			inner join export.EXPORT_PRODUCT_ATTRIBUTE_DPS dps (Nolock)
			on (dps.PRODUCT_ID = @v_prod_value)
			 where dv.DATA_NAME_FK =  211903) T

			DECLARE  cur_v_to_string cursor local for

			SELECT    @v_LIST_VALUES
															
				OPEN cur_v_to_string;
				FETCH NEXT FROM cur_v_to_string INTO 
						 @v_LIST_VALUES

					 WHILE @@FETCH_STATUS = 0 
							 BEGIN

					UPDATE export.EXPORT_PRODUCT_ATTRIBUTE_DPS
					SET SEGLIST_PROD_REBATES =  @v_LIST_VALUES
					where PRODUCT_ID = @v_prod_value
					

							
					FETCH NEXT FROM cur_v_to_string INTO 
								 @v_LIST_VALUES



					 END --cur_v_to_String LOOP
					 CLOSE cur_v_to_string;
					 DEALLOCATE cur_v_to_string; 


	
    FETCH NEXT FROM cur_v_import INTO 
				 @v_prod_structure,
				 @v_prod_value



     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 

--END  ---end first loop

------------------------------------------------------------------------------------
---LOOP AGAIN TO GET MULITPLE VALUES AND LIST AS STRING FOR PRODUCT PRICE TYPE
-----------------------------------------------------------------------------------

 
	
	DECLARE  cur_v_import cursor local for
select distinct pi.PRODUCT_STRUCTURE_FK, pi.value 
from export.EXPORT_PRODUCT_ATTRIBUTE_DPS pad (nolock)
inner join epacube.PRODUCT_IDENTIFICATION pi (nolock)
on (pad.PRODUCT_ID = pi.value 
and pi.DATA_NAME_FK = 
(select data_name_fk from export.export_map_metadata (nolock) where column_name = 'PRODUCT_ID'
and export_package_fk = @in_v_package_fk)	
   )

													
		OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO 
				 @v_prod_structure,
				 @v_prod_value
				 				
				
         WHILE @@FETCH_STATUS = 0 
         BEGIN
            
			
--------2nd Loop to string the product rebate type
----------------------------------------------------------------------------------
			--DECLARE @v_LIST_VALUES VARCHAR(MAX)
			 
			 SET @v_LIST_VALUES = ''
			 SELECT @v_LIST_VALUES =  T.value + '; ' + @v_LIST_VALUES
			 from 
			 (select distinct dv.value
			FROM epacube.data_value dv (nolock)
			inner join epacube.product_category pc (nolock)
			on (pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
			and pc.product_STRUCTURE_FK = @v_prod_structure )
			inner join export.EXPORT_PRODUCT_ATTRIBUTE_DPS dps (Nolock)
			on (dps.PRODUCT_ID = @v_prod_value)
			 where dv.DATA_NAME_FK =  211901) T

			DECLARE  cur_v_to_string cursor local for

			SELECT    @v_LIST_VALUES
															
				OPEN cur_v_to_string;
				FETCH NEXT FROM cur_v_to_string INTO 
						 @v_LIST_VALUES

					 WHILE @@FETCH_STATUS = 0 
							 BEGIN

					UPDATE export.EXPORT_PRODUCT_ATTRIBUTE_DPS
					SET SEGLIST_PROD_MARKETS =  @v_LIST_VALUES
					where PRODUCT_ID = @v_prod_value

												

							
					FETCH NEXT FROM cur_v_to_string INTO 
								 @v_LIST_VALUES



					 END --cur_v_to_String LOOP
					 CLOSE cur_v_to_string;
					 DEALLOCATE cur_v_to_string; 


	
    FETCH NEXT FROM cur_v_import INTO 
				 @v_prod_structure,
				 @v_prod_value



     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 

--END  ---end first loop


	  SET @ls_stmt = 'started export.EXPORT_PRODUCT_ATTRIBUTES_DPS - CLEAN UP'

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;


-----------------------------------------------------------------------------------------------------------
--clean up
-----------------------------------------------------------------------------------------------------------
---removes the last comma
					UPDATE export.EXPORT_PRODUCT_ATTRIBUTE_DPS
					SET SEGLIST_PROD_MARKETS =  substring(SEGLIST_PROD_MARKETS,1,len(SEGLIST_PROD_MARKETS)-1)
					where SEGLIST_PROD_MARKETS <> ' ' 


					UPDATE export.EXPORT_PRODUCT_ATTRIBUTE_DPS
					SET SEGLIST_PROD_REBATES =  substring(SEGLIST_PROD_REBATES,1,len(SEGLIST_PROD_REBATES)-1)
					where  SEGLIST_PROD_REBATES <> ' '

--- truncates to 256

	UPDATE export.EXPORT_PRODUCT_ATTRIBUTE_DPS
					SET SEGLIST_PROD_MARKETS =  substring(SEGLIST_PROD_MARKETS,1,256)
					where  len(SEGLIST_PROD_MARKETS) > 256


					UPDATE export.EXPORT_PRODUCT_ATTRIBUTE_DPS
					SET SEGLIST_PROD_REBATES =  substring(SEGLIST_PROD_REBATES,1,256)
					where  len(SEGLIST_PROD_REBATES) > 256



	  SET @ls_stmt = 'started export.EXPORT_PRODUCT_ATTRIBUTES_DPS - UPDATE PROD PRICE TYPE'

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;



--------------------------------------------------------------------------------------------------------------
---update if product price type is different
--------------------------------------------------------------------------------------------------------------
if @ERP_HOST = 'SXE'
BEGIN

-----------------------------------------------------
-- Begin SUP-3025 mod
-----------------------------------------------------


--Make an export sales products temp table with product structure and id
-- index product id
create table #SalesProducts
( ps_id bigint ,
  product_id varchar(50)	  
)

insert into #SalesProducts
( ps_id,
  product_id
)
select distinct pi.product_structure_fk, esd.product_id
from epacube.product_identification pi with (NOLOCK)
inner join export.EXPORT_SALES_DPS esd with (NOLOCK) on (esd.PRODUCT_ID = pi.value)
inner join synchronizer.rules r with (nolock)
on (r.HOST_RULE_XREF = esd.MTX_SELL_MATRIX_ID
and r.UPDATE_USER = 'HOST PRICE RULES LOAD'
and r.RECORD_STATUS_CR_FK = 1)

CREATE CLUSTERED INDEX IDX_ProductStructureID on #SalesProducts(ps_id)

-- Make an export product attributes temp table with product_structure and id, index product id
--only allow those items who have a product structure fk matching what's in export sales
create table #HistoricalEnterpriseSegments
(
 segprod_ent_hist varchar(256) null
)

insert into #HistoricalEnterpriseSegments
( segprod_ent_hist)

 select distinct epa.segprod_ent_hist
 from epacube.product_identification pi with (NOLOCK) 
 inner join export.EXPORT_PRODUCT_ATTRIBUTE_DPS epa with (NOLOCK) on (epa.PRODUCT_ID = pi.value)
 inner join #SalesProducts sp on (sp.ps_id = pi.PRODUCT_STRUCTURE_FK and sp.product_id = epa.PRODUCT_ID)
 where epa.SEGPROD_ENT_HIST is not null

update  esd
set SEGPROD_MRK_HIST =   rf.VALUE1
from export.EXPORT_SALES_DPS esd with (nolock)
inner join synchronizer.rules r with (nolock)
on (r.HOST_RULE_XREF = esd.MTX_SELL_MATRIX_ID
and r.UPDATE_USER = 'HOST PRICE RULES LOAD'
and r.RECORD_STATUS_CR_FK = 1)

inner join synchronizer.RULES_FILTER_SET rfs with (nolock) on (r.rules_id = rfs.rules_fk and rfs.PROD_FILTER_FK is not null)
inner join synchronizer.RULES_FILTER rf with (nolock) on (rfs.prod_filter_fk = rf.rules_filter_id)
--replaced this join
--inner join synchronizer.V_RULES_FILTER_SET rfs with (nolock)
--on (rfs.RULES_FK = r.RULES_ID)
--
inner join export.EXPORT_PRODUCT_ATTRIBUTE_DPS eca with (nolock)
on (eca.PRODUCT_ID = esd.PRODUCT_ID)
where rf.VALUE1 not in (select segprod_ent_hist from #HistoricalEnterpriseSegments)

-----------------------------------------------------
-- End SUP-3025 mod
-----------------------------------------------------
END

if @ERP_HOST = 'ECLIPSE'
BEGIN
 update  esd
set SEGPROD_MRK_HIST = rfs.PRD_VALUE1
from export.EXPORT_SALES_DPS esd with (nolock)
inner join synchronizer.rules r with (nolock)
on (r.HOST_RULE_XREF = esd.MTX_SELL_MATRIX_ID
and r.RULE_TYPE_CR_FK = 313
--and r.RULES_STRUCTURE_FK in (300100,300110)
and r.RECORD_STATUS_CR_FK = 1)
inner join synchronizer.V_RULES_FILTER_SET rfs with (nolock)
on (rfs.RULES_FK = r.RULES_ID)
inner join export.EXPORT_PRODUCT_ATTRIBUTE_DPS eca with (nolock)
on (eca.PRODUCT_ID = esd.PRODUCT_ID)
where rfs.PRD_VALUE1 <> eca.SEGPROD_ENT_HIST


END




	  SET @ls_stmt = 'started export.EXPORT_PRODUCT_ATTRIBUTES_DPS - UPDATE SEGPROD_MRK_HIST'

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;


-------------------------------------------------------------------------------------------------------------
----WHEN sales.SEGPROD_MRK_HIST IS NOT NULL 
----THEN sales.SEGPROD_MRK_HIST = export_product.SEGPROD_ENT_HIST + �:� + export_sales.SEGPROD_MRK_HIST
---------------------------------- ---------------------------------------------------------------------------

update esd
set segprod_mrk_hist = eca.SEGPROD_ENT_HIST + ':' + esd.SEGPROD_MRK_HIST
from export.EXPORT_SALES_DPS esd
inner join export.EXPORT_PRODUCT_ATTRIBUTE_DPS eca
on (eca.PRODUCT_ID = esd.PRODUCT_ID)
where esd.SEGPROD_MRK_HIST is not null

------------------------------------------------------------------------------------------------------------------

  SET @status_desc = 'finished execution of Export.EXPORT_PRODUCT_ATTRIBUTES_DPS'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of Export.EXPORT_PRODUCT_ATTRIBUTES_DPS has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
				EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END

















