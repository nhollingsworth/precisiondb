﻿--A_epaMAUI_Export_Sxe_Price_Rules_PDSC


CREATE PROCEDURE [export].[Export_SXe_Price_Rules] ( @IN_JOB_FK bigint ) 
AS BEGIN 

/*-----------------------------------------------------------------------------------------------
--			*********  START OF PROCESS DOCUMENTATION BLOCK  *************
-------------------------------------------------------------------------------------------------
--   OVERVIEW:  
--   This Procedure is the procedure to   
--
-------------------------------------------------------------------------------------------------
--   CONFIG NOTES:
--   HARDCODED:   
--
-------------------------------------------------------------------------------------------------
--   PRIOR STEPS:  
--   Starting procedure 
--   
-------------------------------------------------------------------------------------------------
--   PROCESS STEP1: 
--     
-------------------------------------------------------------------------------------------------
--   PROCESS STEP2: 
--   
-------------------------------------------------------------------------------------------------
--   FUTURE DEVELOPMENT and other COMMENTS:
--   
--  
-------------------------------------------------------------------------------------------------
--			*********  END OF PROCESS DOCUMENTATION BLOCK  *************
-----------------------------------------------------------------------------------------------*/

     DECLARE @ls_exec            nvarchar (max);
     DECLARE @l_exec_no          bigint;
     DECLARE @l_rows_processed   bigint;
     DECLARE @l_sysdate			 DATETIME
     DECLARE @V_START_TIMESTAMP     DATETIME
     DECLARE @V_END_TIMESTAMP       DATETIME
     DECLARE @v_input_rows       bigint;               
     DECLARE @v_output_rows      bigint; 
	 DECLARE @v_exception_rows		 bigint;         

     DECLARE @status_desc        varchar(max);
     DECLARE @ls_table_name      varchar(max);

     DECLARE @time_before datetime;
	 DECLARE @time_after datetime;
	 DECLARE @elapsed_time numeric(10,4);
	 DECLARE @rows_affected varchar(max);
	 DECLARE @rows_dml bigint;
	 DECLARE @ParmDefinition nvarchar(500);

     DECLARE @v_batch_no             bigint
     DECLARE @v_search_activity_fk   int     
     DECLARE @v_entity_class_cr_fk   int 
	 DECLARE @v_import_package_fk   int  
	 DECLARE @v_column_name   varchar(25)
	 DECLARE @v_primary   varchar(25)   
	 DECLARE @V_UNIQUE1   varchar(25) 
	 DECLARE @V_UNIQUE2   varchar(25) 
	 DECLARE @V_UNIQUE3   varchar(25) 
     	
	 DECLARE @v_effective_date  datetime 
	 DECLARE @v_import_filename varchar(128) 


SET NOCOUNT ON;

BEGIN TRY
      SET @l_exec_no = 0;
      EXEC exec_monitor.start_monitor_sp @exec_desc = 'started execution of stage.IMPORT_TRANSFORMATION', 
												@exec_id = @l_exec_no OUTPUT,
												@epa_job_id = @in_job_fk;

     SET @l_sysdate = GETDATE() --used for the beginning timestamp
     SET @V_START_TIMESTAMP = getdate()  -- used in steps


--GARY START HERE

--drop table if it exists
	IF object_id('dbo.PDSC_xFer') is not null
	   drop table dbo.PDSC_xFer;

	IF object_id('tempdb..##Dels') is not null
	drop table ##Dels

	select host_rule_xref into ##dels from (
	select host_rule_xref from dbo.maui_rules_maintenance where result_data_name_FK = 111602 and custvalue is null and levelcode in (1, 2, 3, 4, 5, 6)
	union
	select host_rule_xref from dbo.maui_rules_maintenance where result_data_name_FK = 111602 and prodvalue is null and levelcode in (1, 2, 3, 4, 7, 8)
	Union
	select r.host_rule_xref 
		from synchronizer.rules_filter_set rfs
		inner join synchronizer.rules r on rfs.rules_fk = r.rules_id
		where prod_filter_fk is null and cust_filter_fk is null 
		and r.result_data_name_fk = 111602 
		and r.host_rule_xref is not null
		and r.rules_id > 2000000000
	) a

	insert into dbo.MAUI_Rules_Maintenance_Discards
	(
	[Export_Job_FK], [Job_FK_i], [WhatIF_ID], [RuleType], [LevelCode], [Result_Data_Name_FK], [Rule_Result_Type_CR_FK], [CustCriteria_DN_FK], [CustValue], [RULES_ACTION_OPERATION1_FK], [RULES_ACTION_OPERATION2_FK], [RULES_ACTION_OPERATION3_FK], [BASIS_CALC_DN_FK], [BASIS1_DN_FK], [BASIS2_DN_FK], [BASIS3_DN_FK], [Rule_Name], [Host_Rule_Xref], [Promo], [Effective_Date], [End_Date], [Rules_Hierarchy_FK], [Rules_Structure_FK], [Rules_Schedule_FK], [Rules_Precedence_FK], [Rule_Precedence], [UOM_DN_FK], [UOM_CODE_FK], [Reference], [Contract_NO], [Unique_Key1], [ProdCriteria_DN_FK], [ProdValue], [chkWhseSpecific], [Org_Entity_Structure_FK], [WhseValue], [Supl_Entity_Structure_FK], [Vendor_ID], [ROUNDING_METHOD_CR_FK], [ROUNDING_TO_VALUE], [Rules_FK], [Rules_Action_FK], [Qty_Break_Rule_Ind], [OPERAND_FILTER1_DN_FK], [OPERAND_FILTER1_OPERATOR_CR_FK], [Operand_1_1], [Operand_1_2], [Operand_1_3], [Operand_1_4], [Operand_1_5], [Operand_1_6], [Operand_1_7], [Operand_1_8], [Operand_1_9], [Filter_Value_1_1], [Filter_Value_1_2], [Filter_Value_1_3], [Filter_Value_1_4], [Filter_Value_1_5], [Filter_Value_1_6], [Filter_Value_1_7], [Filter_Value_1_8], [Filter_Value_1_9], [OPERAND_FILTER2_DN_FK], [OPERAND_FILTER2_OPERATOR_CR_FK], [Operand_2_1], [Operand_2_2], [Operand_2_3], [Operand_2_4], [Operand_2_5], [Operand_2_6], [Operand_2_7], [Operand_2_8], [Operand_2_9], [Filter_Value_2_1], [Filter_Value_2_2], [Filter_Value_2_3], [Filter_Value_2_4], [Filter_Value_2_5], [Filter_Value_2_6], [Filter_Value_2_7], [Filter_Value_2_8], [Filter_Value_2_9], [OPERAND_FILTER3_DN_FK], [OPERAND_FILTER3_OPERATOR_CR_FK], [Operand_3_1], [Operand_3_2], [Operand_3_3], [Operand_3_4], [Operand_3_5], [Operand_3_6], [Operand_3_7], [Operand_3_8], [Operand_3_9], [Filter_Value_3_1], [Filter_Value_3_2], [Filter_Value_3_3], [Filter_Value_3_4], [Filter_Value_3_5], [Filter_Value_3_6], [Filter_Value_3_7], [Filter_Value_3_8], [Filter_Value_3_9], [Record_Status_CR_FK], [Update_User], [Create_User], [Approved_By], [Approval_Timestamp], [Update_Timestamp], [Create_Timestamp], [Transferred_To_ERP], [Transfer_To_ERP_Timestamp], [Transferred_To_Rules], [Inactivated_By], [GP_Initial], [GP_Final], [Sales_Final], [Source], [ID_Common], [DropShipType], [DefaultPriceLevel], [Comments], [Insert_Date]
	)
	Select 
	[Export_Job_FK], [Job_FK_i], [WhatIF_ID], [RuleType], [LevelCode], [Result_Data_Name_FK], [Rule_Result_Type_CR_FK], [CustCriteria_DN_FK], [CustValue], [RULES_ACTION_OPERATION1_FK], [RULES_ACTION_OPERATION2_FK], [RULES_ACTION_OPERATION3_FK], [BASIS_CALC_DN_FK], [BASIS1_DN_FK], [BASIS2_DN_FK], [BASIS3_DN_FK], [Rule_Name], [Host_Rule_Xref], [Promo], [Effective_Date], [End_Date], [Rules_Hierarchy_FK], [Rules_Structure_FK], [Rules_Schedule_FK], [Rules_Precedence_FK], [Rule_Precedence], [UOM_DN_FK], [UOM_CODE_FK], [Reference], [Contract_NO], [Unique_Key1], [ProdCriteria_DN_FK], [ProdValue], [chkWhseSpecific], [Org_Entity_Structure_FK], [WhseValue], [Supl_Entity_Structure_FK], [Vendor_ID], [ROUNDING_METHOD_CR_FK], [ROUNDING_TO_VALUE], [Rules_FK], [Rules_Action_FK], [Qty_Break_Rule_Ind], [OPERAND_FILTER1_DN_FK], [OPERAND_FILTER1_OPERATOR_CR_FK], [Operand_1_1], [Operand_1_2], [Operand_1_3], [Operand_1_4], [Operand_1_5], [Operand_1_6], [Operand_1_7], [Operand_1_8], [Operand_1_9], [Filter_Value_1_1], [Filter_Value_1_2], [Filter_Value_1_3], [Filter_Value_1_4], [Filter_Value_1_5], [Filter_Value_1_6], [Filter_Value_1_7], [Filter_Value_1_8], [Filter_Value_1_9], [OPERAND_FILTER2_DN_FK], [OPERAND_FILTER2_OPERATOR_CR_FK], [Operand_2_1], [Operand_2_2], [Operand_2_3], [Operand_2_4], [Operand_2_5], [Operand_2_6], [Operand_2_7], [Operand_2_8], [Operand_2_9], [Filter_Value_2_1], [Filter_Value_2_2], [Filter_Value_2_3], [Filter_Value_2_4], [Filter_Value_2_5], [Filter_Value_2_6], [Filter_Value_2_7], [Filter_Value_2_8], [Filter_Value_2_9], [OPERAND_FILTER3_DN_FK], [OPERAND_FILTER3_OPERATOR_CR_FK], [Operand_3_1], [Operand_3_2], [Operand_3_3], [Operand_3_4], [Operand_3_5], [Operand_3_6], [Operand_3_7], [Operand_3_8], [Operand_3_9], [Filter_Value_3_1], [Filter_Value_3_2], [Filter_Value_3_3], [Filter_Value_3_4], [Filter_Value_3_5], [Filter_Value_3_6], [Filter_Value_3_7], [Filter_Value_3_8], [Filter_Value_3_9], [Record_Status_CR_FK], [Update_User], [Create_User], [Approved_By], [Approval_Timestamp], [Update_Timestamp], [Create_Timestamp], [Transferred_To_ERP], [Transfer_To_ERP_Timestamp], [Transferred_To_Rules], [Inactivated_By], [GP_Initial], [GP_Final], [Sales_Final], [Source], [ID_Common], [DropShipType], [DefaultPriceLevel], [Comments]--, [MAUI_Rules_Maintenance_ID], [IMPORT_MASS_MAINTENANCE_PRICE_REBATE_FK], [IMPORT_MASS_MAINTENANCE_PROMO_FK]
	, getdate() from dbo.maui_rules_maintenance where result_data_name_FK = 111602 and host_rule_xref in (select host_rule_xref from ##Dels)

	delete from synchronizer.rules_action_operands where rules_action_fk in (select rules_action_id from synchronizer.rules_action where rules_fk in (select rules_id from synchronizer.rules where host_rule_xref in (select host_rule_xref from ##Dels)))
	delete from synchronizer.rules_action where rules_fk in (select rules_id from synchronizer.rules where host_rule_xref in (select host_rule_xref from ##Dels))
	delete from synchronizer.rules_filter_set where rules_fk in (select rules_id from synchronizer.rules where host_rule_xref in (select host_rule_xref from ##Dels))
	delete from synchronizer.rules where rules_id in (select rules_id from synchronizer.rules where host_rule_xref in (select host_rule_xref from ##Dels))
	delete from dbo.maui_rules_maintenance where host_rule_xref in (select host_rule_xref from ##dels)

	IF object_id('tempdb..##Dels') is not null
	drop table ##Dels

Update R
Set End_Date = getdate() - 1
, record_status_cr_fk = 2
from synchronizer.rules r
where RESULT_DATA_NAME_FK = 111602 and host_rule_xref in
(select host_rule_xref
	from dbo.maui_rules_maintenance where 
	1 = 1
	and Inactivated_By is not null
	and end_date > getdate()
)

Update MRM
Set Transferred_To_ERP = 0
, Transfer_To_ERP_Timestamp = Null
, export_job_fk = Null
, end_date = getdate() - 1
, update_timestamp = getdate()
, record_status_cr_fk = 2
from dbo.maui_rules_maintenance MRM where 
1 = 1
and Inactivated_By is not null
and end_date > getdate()

Select
@IN_JOB_FK JOB_FK
, rp.Host_precedence_level_description TypeCD
, Null commtype
, contract_no
, ltrim(rtrim(Case	when rf_cust.value1 is not null and rf_cust.value1 not like '%-%' then  rf_cust.value1
		when rf_cust.value1 is not null and rf_cust.value1 like '%-%' then  left(rf_cust.value1, charindex('-', rf_cust.value1) - 1)
		when rf_shipto.value1 is not null then left(rf_shipto.value1, charindex('-', rf_shipto.value1) - 1)
		end)) custno
, rf_cpt.value1 custtype
, CONVERT(VARCHAR(10), effective_date, 1) 'StartDt'
, CONVERT(VARCHAR(10), isnull(end_date, '12/31/49'), 1) 'EndDt'
, Null JobNo
, rp.host_precedence_level LevelCd
, Null maxqty
, Null minqty
, Case When Host_rule_Xref not like 'New%' then Host_rule_Xref end pdrecno
, Case when Rounding_To_Value = 9 then Rounding_To_Value end 'pexactrnd'
, Case	When Rules_Action_Operation1_fk between 1041 and 1044 then isnull(rao1.operand1, ra.basis1_number)
		When Rules_Action_Operation2_fk between 1041 and 1044 then isnull(rao2.operand1, ra.basis2_number) end prcdisc1
, Case	When Rules_Action_Operation1_fk between 1041 and 1044 then rao1.operand2
		When Rules_Action_Operation2_fk between 1041 and 1044 then rao2.operand2 end prcdisc2
, Case	When Rules_Action_Operation1_fk between 1041 and 1044 then rao1.operand3
		When Rules_Action_Operation2_fk between 1041 and 1044 then rao2.operand3 end prcdisc3
, Case	When Rules_Action_Operation1_fk between 1041 and 1044 then rao1.operand4
		When Rules_Action_Operation2_fk between 1041 and 1044 then rao2.operand4 end prcdisc4
, Case	When Rules_Action_Operation1_fk between 1041 and 1044 then rao1.operand5
		When Rules_Action_Operation2_fk between 1041 and 1044 then rao2.operand1 end prcdisc5
, Case	When Rules_Action_Operation1_fk between 1041 and 1044 then rao1.operand6
		When Rules_Action_Operation2_fk between 1041 and 1044 then rao2.operand6 end prcdisc6
, Case	When Rules_Action_Operation1_fk between 1041 and 1044 then rao1.operand7
		When Rules_Action_Operation2_fk between 1041 and 1044 then rao2.operand7 end prcdisc7
, Case	When Rules_Action_Operation1_fk between 1041 and 1044 then rao1.operand8
		When Rules_Action_Operation2_fk between 1041 and 1044 then rao2.operand8 end prcdisc8
, Case	When Rules_Action_Operation1_fk between 1041 and 1044 then rao1.operand9
		When Rules_Action_Operation2_fk between 1041 and 1044 then rao2.operand9 end prcdisc9
, Case	When Rules_Action_Operation1_fk < 1041 or Rules_Action_Operation1_FK > 1044 then isnull(rao1.operand1, basis1_number)
		When Rules_Action_Operation2_fk < 1041 or Rules_Action_Operation2_FK > 1044 then isnull(rao2.operand1, basis1_number) end prcmult1
, Case	When Rules_Action_Operation1_fk < 1041 or Rules_Action_Operation1_FK > 1044 then rao1.operand2
		When Rules_Action_Operation2_fk < 1041 or Rules_Action_Operation2_FK > 1044 then rao2.operand2 end prcmult2
, Case	When Rules_Action_Operation1_fk < 1041 or Rules_Action_Operation1_FK > 1044 then rao1.operand3
		When Rules_Action_Operation2_fk < 1041 or Rules_Action_Operation2_FK > 1044 then rao2.operand3 end prcmult3
, Case	When Rules_Action_Operation1_fk < 1041 or Rules_Action_Operation1_FK > 1044 then rao1.operand4
		When Rules_Action_Operation2_fk < 1041 or Rules_Action_Operation2_FK > 1044 then rao2.operand4 end prcmult4
, Case	When Rules_Action_Operation1_fk < 1041 or Rules_Action_Operation1_FK > 1044 then rao1.operand5
		When Rules_Action_Operation2_fk < 1041 or Rules_Action_Operation2_FK > 1044 then rao2.operand5 end prcmult5
, Case	When Rules_Action_Operation1_fk < 1041 or Rules_Action_Operation1_FK > 1044 then rao1.operand6
		When Rules_Action_Operation2_fk < 1041 or Rules_Action_Operation2_FK > 1044 then rao2.operand6 end prcmult6
, Case	When Rules_Action_Operation1_fk < 1041 or Rules_Action_Operation1_FK > 1044 then rao1.operand7
		When Rules_Action_Operation2_fk < 1041 or Rules_Action_Operation2_FK > 1044 then rao2.operand7 end prcmult7
, Case	When Rules_Action_Operation1_fk < 1041 or Rules_Action_Operation1_FK > 1044 then rao1.operand8
		When Rules_Action_Operation2_fk < 1041 or Rules_Action_Operation2_FK > 1044 then rao2.operand8 end prcmult8
, Case	When Rules_Action_Operation1_fk < 1041 or Rules_Action_Operation1_FK > 1044 then rao1.operand9
		When Rules_Action_Operation2_fk < 1041 or Rules_Action_Operation2_FK > 1044 then rao2.operand9 end prcmult9
, Case When Rules_Action_Operation1_fk between 1021 and 1024 
	or Rules_Action_Operation2_fk between 1021 and 1024 then '$' else '%' end 'prctype'
, Replace(Replace((Select name from epacube.data_name where data_name_id = 
	(Select Value from epacube.epacube_params where Name = 'Pricing Cost Basis')), 'SX Std', 'Standard Cost'), 'SX REPL COST', 'Replacement Cost') 'pricing cost'
,  Case When (Rules_Action_Operation1_FK between 1051 and 1054 or Rules_Action_Operation2_FK between 1051 and 1054) and (Basis_Calc_DN_FK = 111401 or Basis1_DN_FK = 111401) then 'rm'
		When (Rules_Action_Operation1_FK between 1051 and 1054 or Rules_Action_Operation2_FK between 1051 and 1054) then 'm'
		When Basis_Calc_DN_FK = 231101 or (Basis_Calc_DN_FK = 231102 and Rules_Action_Operation1_fk between 1031 and 1034) then 'c'
		When Basis_Calc_DN_FK = 231102 and Rules_Action_Operation1_fk between 1041 and 1044 then 's'
		when Basis_Calc_DN_FK = 231103 then 'b'
		When Basis_Calc_DN_FK = 231104 then 'l'
		When Basis_Calc_DN_FK = 111401 then 'rc' end 'priceonty'
, Null 'PriceSheet'
, Case rf_prod.data_name_fk When 211901 then rf_prod.value1 end PriceType
, Case rf_prod.data_name_fk When 110100 then rf_prod.value1 end Prod
, Case rf_prod.data_name_fk When 210901 then rf_prod.value1 end ProdCat
, Case rf_prod.data_name_fk When 211902 then rf_prod.value1 end ProdLine
, Case When (Select Name from synchronizer.rules_schedule where rules_schedule_id = r.rules_schedule_fk) like '%Promo%' then 'Y' else 'N' end 'promofl'
, Case When Rounding_To_Value is null then null else Left((Select Code from epacube.Code_ref where Code_Ref_ID = Rounding_Method_CR_FK), 1) end 'pround'
--, Case	When Rounding_To_Value = 100 then 1 
--		When Rounding_To_Value = 10 then 2
--		When Rounding_To_Value = 1 then 3 
--		When Rounding_To_Value = 0.1 then 4 
--		When Rounding_To_Value = 0.01 then 5
--		When Rounding_To_Value = 0.001 then 6
--		When Rounding_To_Value = 0.0001 then 7
--		When Rounding_To_Value = 0.00001 then 8
--		When Rounding_To_Value is not null then 9 end 'ptarget'
, Rounding_To_Value 'ptarget'
, Case	When isnull(rao1.Basis_Position, 0) = 0 then Null 
		When (Rules_action_Operation1_FK between 1041 and 1044) or (Rules_action_Operation2_FK between 1041 and 1044) then 'D' else 'P' end 'qtybreak'
, rao1.filter_value1 qtybrk1
, rao1.filter_value2 qtybrk2
, rao1.filter_value3 qtybrk3
, rao1.filter_value4 qtybrk4
, rao1.filter_value5 qtybrk5
, rao1.filter_value6 qtybrk6
, rao1.filter_value7 qtybrk7
, rao1.filter_value8 qtybrk8
, Null qtytype
, Case rf_prod.data_name_fk When 211903 then rf_prod.value1 end 'rebatetype'
, Reference 'refer'
, ltrim(rtrim(Case	when rf_shipto.value1 is not null then right(rf_shipto.value1, Len(rf_shipto.value1) - charindex('-', rf_shipto.value1)) 
					when rf_cust.value1 is not null and rf_cust.value1 like '%-%' then right(rf_cust.value1, Len(rf_cust.value1) - charindex('-', rf_cust.value1)) end)) 'shipto'
, Null 'termsdisc'
, Null 'termspct'
, Null 'units'
, Null 'user1'
, Null 'user2'
, Null 'user3'
, Null 'user4'
, Null 'user5'
, Null 'user6'
, Null 'user7'
, Null 'user8'
, Null 'user9'
, rf_vndr.value1 'Vendor'
, rf_Org.value1 'Whse'
, Null 'custom1'
, Null 'custom2'
, Null 'custom3'
, Null 'custom4'
, Null 'custom5'
, Null 'custom6'
, Null 'custom7'
, Null 'custom8'
, Null 'custom9'
, Null 'custom10'
, r.NAME rule_name
into dbo.PDSC_xFer
from synchronizer.rules r
	inner join Synchronizer.rules_precedence rp with (nolock) on r.precedence = rp.rules_precedence_id
	inner join synchronizer.rules_action ra on r.rules_id = ra.rules_fk
		left join synchronizer.rules_action_operands rao1 on ra.rules_action_id = rao1.rules_action_fk and rao1.basis_position = 1 
		left join synchronizer.rules_action_operands rao2 on ra.rules_action_id = rao2.rules_action_fk and rao2.basis_position = 2 
		left join synchronizer.rules_action_operands rao3 on ra.rules_action_id = rao3.rules_action_fk and rao3.basis_position = 3 
	inner join synchronizer.rules_filter_set rfs on rfs.rules_fk = r.rules_id
		Left join synchronizer.rules_filter rf_cust on rfs.cust_filter_fk = rf_cust.rules_filter_id and rf_cust.entity_data_name_fk = 144010
		Left join synchronizer.rules_filter rf_shipto on rfs.cust_filter_fk = rf_shipto.rules_filter_id and rf_shipto.entity_data_name_fk = 144020
		Left join synchronizer.rules_filter rf_cpt on rfs.cust_filter_fk = rf_cpt.rules_filter_id and rf_cpt.data_name_fk = 244900
		Left Join synchronizer.rules_filter rf_prod on rfs.prod_filter_fk = rf_prod.rules_filter_id
		Left join synchronizer.rules_filter rf_vndr on rfs.supl_filter_fk = rf_vndr.rules_filter_id and rf_vndr.entity_data_name_fk = 143000
		Left join synchronizer.rules_filter rf_Org on rfs.org_filter_fk = rf_Org.rules_filter_id and rf_Org.entity_data_name_fk = 141000
where	1 = 1
		and result_data_name_fk = 111602
		and CONVERT(VARCHAR(10), effective_date, 1) <> CONVERT(VARCHAR(10), isnull(end_date, '12/31/49'), 1)
--		and r.record_status_cr_fk = 1
		and (
			r.name in			(select Rule_Name from dbo.MAUI_Rules_Maintenance where Transferred_to_ERP = 0)
			or r.rules_id in	(select rules_fk from dbo.maui_rules_maintenance where Transferred_To_ERP = 0)
			or r.unique_key1 in 
								(select r1.unique_key1 
									from synchronizer.rules r1
									inner join dbo.maui_rules_maintenance MRM on r1.unique_key1 = mrm.unique_key1
									where mrm.Transferred_to_ERP = 0)
			)
		

--Update MRM
--Set Transferred_To_ERP = 1
--, Transfer_To_ERP_Timestamp = getdate()
--, Export_Job_FK = @IN_JOB_FK
--from dbo.MAUI_Rules_Maintenance MRM
--inner join synchronizer.rules r on MRM.unique_key1 = r.unique_key1 or r.host_rule_xref = mrm.host_rule_xref
--inner join dbo.PDSC_xFer Xfer on r.Name = Xfer.rule_name
--Where Transferred_To_ERP = 0

Update MRM
Set Export_Job_FK = @IN_JOB_FK
from dbo.MAUI_Rules_Maintenance MRM
inner join synchronizer.rules r on MRM.unique_key1 = r.unique_key1 or r.host_rule_xref = mrm.host_rule_xref
inner join dbo.PDSC_xFer Xfer on r.Name = Xfer.rule_name
Where Transferred_To_ERP = 0

Insert into dbo.PDSC_xFer_Archive
Select *, getdate() from dbo.PDSC_xFer
-------------------------------------------------------------------------
 SELECT @v_input_rows = COUNT(1)
        FROM dbo.PDSC_xFer WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

      SELECT @v_output_rows = COUNT(1)
        FROM dbo.PDSC_xFer WITH (NOLOCK)
       WHERE job_fk = @in_job_fk;

	  SET @v_exception_rows = @v_input_rows - @v_output_rows;
      SET @V_END_TIMESTAMP = getdate()
      EXEC common.job_execution_create  @in_job_fk, 'IDENTIFY RULES TO TRANSFER',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @V_START_TIMESTAMP, @V_END_TIMESTAMP;


-------------------------------------------------------------------------------------
-- LAST Step is to Set Job Execution to Complete
-------------------------------------------------------------------------------------
----
----

      SET @V_END_TIMESTAMP = getdate()


--      SET @v_input_rows =	(select top 1 input_rows from common.job_execution  WITH (NOLOCK)
--                              where job_fk =  @in_job_fk
--                              and   name = 'IMPORT PRODUCTS TO STAGE' )
--
--      SET @v_exception_rows =(select sum(exception_rows)from common.job_execution  WITH (NOLOCK)
--                              where job_fk =  @in_job_fk
--                              and   name = 'IMPORT PRODUCTS RESOLVED' )

     exec common.job_execution_create  @in_job_fk, 'RULES EXPORT COMPLETE',
                                @v_input_rows, @v_output_rows, @v_exception_rows,
								201, @l_sysdate, @V_END_TIMESTAMP;


	   



 SET @status_desc =  'finished execution of export.Export_Sxe_Price_Rules'
 
 EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = @in_job_fk;
      
END TRY
BEGIN CATCH   
      DECLARE @ErrorMessage NVARCHAR(4000);
	  DECLARE @ErrorSeverity INT;
	  DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = 'Execution of export.Export_Sxe_Price_Rules has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

         EXEC exec_monitor.Report_Error_sp @l_exec_no, @epa_job_id = @in_job_fk;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
   END CATCH;


END
