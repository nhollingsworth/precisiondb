﻿-- =============================================
-- Author:		Neal Hollingsworth
-- Create date: 12/20/2019
-- Description:	Retail change history export
-- =============================================
CREATE PROCEDURE [export].[RETAIL_CHANGE_HISTORY_EXPORT] 
	-- Add the parameters for the stored procedure here
	@export_timestamp DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT CUSTOMER 'CUSTOMER_ID'
		, [ZONE] 'ZONE_ID'
		, ZONE_TYPE
		, ITEM 'ITEM_SURROGATE'
		, EFFECTIVE_DATE 'PRICE_EFF_DATE'
		, CASE WHEN ITEM_LEVEL = 60 OR PRICE_MULTIPLE IS NOT NULL 
			THEN ISNULL(PRICE_MULTIPLE, 1) 
		END 'PRICE_MULTIPLE'
		, PRICE
		, ITEM_LEVEL
		, ITEM_CLASS
		, LEVEL_GROSS_FLAG
		, TARGET_MARGIN
		, PRICE_TYPE
	FROM export.RETAIL_CHANGE_HISTORY
	where EXPORT_TIMESTAMP >= @export_timestamp
		AND EXPORT_TIMESTAMP < DATEADD(DAY, 1, @export_timestamp)
		--AND ((item_level = 60 AND Price IS NOT NULL) 
		--	OR (item_level <> 60 and Price IS NULL))
END