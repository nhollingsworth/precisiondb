﻿





-- Copyright 2010
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: To create export data since date passed to populate table /veiw for export.
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  -------------------------------------------------
-- CV        02/03/2012   Initial SQL Version
-- CV		 08/15/2012   Added Paramater to export to EstoreFront or not

--
CREATE PROCEDURE [export].[EXPORT_TAXONOMY]  
AS
BEGIN

-------------------------------------------------------------------------------
----variables to pass Name of Tree to export.  
----Langage code to export - AT the tree level along with the Attribute level
----- changes from date.


DECLARE  @l_sysdate           Datetime
DECLARE  @ls_stmt             varchar (1000)
DECLARE  @ls_exec             nvarchar (max)
DECLARE  @ls_exec2            varchar(100)
DECLARE  @ls_exec3             nvarchar (max)
DECLARE  @l_exec_no           bigint
DECLARE  @l_rows_processed    bigint
DECLARE  @v_count			  bigint
DECLARE  @Export_To_Estorefront int
DECLARE  @Export_Extended_Attributes int

DECLARE  @v_input_rows        bigint
DECLARE  @v_output_rows       bigint
DECLARE  @v_output_total      bigint
DECLARE  @v_exception_rows    bigint
DECLARE  @v_job_date          datetime
DECLARE  @v_sysdate           datetime
DECLARE  @v_import_name       varchar (50)
DECLARE  @v_schema_name       varchar (50)
DECLARE  @v_Future_eff_days int

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int

DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)

DECLARE @tree_name	varchar(128)

DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime
DECLARE @v_out_job_fk BIGINT


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of EXPORT.Export_to_Estorefront' 
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;


                  EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'job_seq', @v_job_fk output
    
    SET @v_job_class_fk  = 100        --- PRODUCT MAINTENANCE
    SET @v_job_user = 'EPACUBE'
    SET @v_job_name = 'EXPORT TAX TREE'
    SET @v_job_date = @l_sysdate
    SET @v_data1   = 'LAST JOB:: '
                     + CAST ( ISNULL ( @v_last_job_fk, -999 )  AS VARCHAR(32) )
                     + ' - '
                     + CAST ( ISNULL ( @v_last_job_date, '01/01/2000' ) AS VARCHAR(64) )
    SET @v_data2   = @v_last_job_date
    
    EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
										   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

    EXEC common.job_execution_create   @v_job_fk, 'START TAXONOMY EXPORT',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  


----------------------------------------------------------------------
---Retrieve Tree name
----------------------------------------------------------------------

set @tree_name = (SELECT value from epacube.config_params
where scope = 'TAX_TREE_EXPORT'
and name = 'TAX_TREE_NAME')

----------------------------------------------------------------------
---FIRST TRUNCATE TABLES --
----------------------------------------------------------------------


 TRUNCATE TABLE [dbo].[EXPORT.EXPORT_TAXONOMY_TREE]
 TRUNCATE TABLE [dbo].[EXPORT.EXPORT_PROD_TAX_ASSIGNMENT]
 TRUNCATE TABLE [dbo].[EXPORT.EXPORT_PROD_ATTRIBUTES]
 TRUNCATE TABLE [dbo].[EXPORT.EXPORT_CATEGORY_ATTRIBUTES]
 TRUNCATE TABLE [dbo].[EXPORT.EXPORT_EXTENDED_ATTRIBUTES]

----------------------------------------------------------------------
---THEN INSERT INTO TABLES  -- View Will look at tables
----------------------------------------------------------------------

----TAX TREE

INSERT INTO [dbo].[EXPORT.EXPORT_TAXONOMY_TREE]
           ([taxonomy_tree_fk]
           ,[CATALOG_NAME]
           ,[LANGUAGE_CODE]
           ,[TAXONOMY_NODE_fk]
           ,[CATEGORY]
           ,[DESCRIPTION]
           ,[DISPLAY_SEQ]
           ,[LEVEL_SEQ]
           ,[PARENT_TAXONOMY_NODE_fk]
           ,[PARENT_NAME]
           ,[IMAGE]
           ,[COMMENT]
           ,[STATUS]
           )
    (SELECT     
tt.taxonomy_tree_id as EPA_CAT_ID    
,TT.NAME AS CATALOG_NAME
, '0' AS LANGUAGE_CODE
, TN.TAXONOMY_NODE_ID AS EPA_CTG_ID, 
                      dv.VALUE AS CATEGORY
, dv.DESCRIPTION
,  dv.display_seq  AS DISPLAY_SEQ
,TN.LEVEL_SEQ AS TAX_LEVEL
, tnp.TAXONOMY_NODE_ID as EPA_PARENT_CTG_ID
, dvp.VALUE AS PARENT_NAME
, tnp.IMAGE
, tnp.COMMENT
, (select code from epacube.code_ref where tn.record_status_cr_fk = code_ref_id )as STATUS
FROM  epacube.TAXONOMY_NODE AS TN INNER JOIN
   epacube.TAXONOMY_TREE AS TT ON TN.TAXONOMY_TREE_FK = TT.TAXONOMY_TREE_ID INNER JOIN
   epacube.DATA_NAME AS dntt ON TT.TAX_NODE_DN_FK = dntt.DATA_NAME_ID INNER JOIN
   epacube.DATA_VALUE AS dv ON TN.TAX_NODE_DV_FK = dv.DATA_VALUE_ID INNER JOIN
   epacube.TAXONOMY_NODE AS tnp ON tnp.TAXONOMY_NODE_ID = TN.PARENT_TAXONOMY_NODE_FK INNER JOIN
  epacube.DATA_VALUE AS dvp ON tnp.TAX_NODE_DV_FK = dvp.DATA_VALUE_ID
where tt.name = @Tree_name)

----------------------ASSIGNMENT

INSERT INTO [dbo].[EXPORT.EXPORT_PROD_TAX_ASSIGNMENT]
           ([EPA_CAT_ID]
           ,[CATALOG_NAME]
           ,[LANGUAGE_CODE]
           ,[PRODUCT_ID]
           ,[EPA_CTG_ID]
           ,[CATEGORY]
			,Company
			,PROD_Display_seq)
(select 
tn.taxonomy_tree_fk epa_cat_id
,tt.name Tree_name
,'0' Language_code ---hard code for now
,pi.value SX_id 
,tn.taxonomy_node_id  Epa_ctg_id 
,dv.value
,ei.value
,psid.attribute_event_data
----------
from epacube.product_category pc
inner join epacube.data_name dn on (pc.data_name_fk = dn.data_name_id)
inner join epacube.data_value dv on (pc.data_value_fk = dv.data_value_id)
inner join epacube.product_identification pi
on (pi.product_structure_fk = pc.product_structure_fk
and pi.data_name_fk = 110100) 
inner join epacube.taxonomy_node tn
on (tn.tax_node_dv_fk = pc.data_value_fk
and tn.record_status_cr_fk = 1)
inner join epacube.taxonomy_tree tt
on (tt.taxonomy_tree_id = tn.taxonomy_tree_fk
and tt.name = @TREE_NAME)
left join epacube.product_association pa With (nolock)
on (pa.product_structure_fk = pc.product_structure_fk
and pa.data_name_fk = 159500) --product_company
left join epacube.entity_identification ei with (nolock)
on (pa.entity_structure_fk = ei.entity_structure_fk
and ei.data_name_fk = 141511)  --company code
left join epacube.product_attribute psid with (nolock)
on (psid.product_structure_fk = pc.product_structure_fk
and psid.data_name_fk = 2002000005)  ---product display seq
where pc.data_name_fk = tt.tax_node_dn_fk)   ---190901

EXEC common.job_execution_create   @v_job_fk, 'INSERT TAXONOMY TREE STRUCTURE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  

update [dbo].[EXPORT.EXPORT_PROD_TAX_ASSIGNMENT]
set COMPANY = (select value from epacube.EPACUBE_PARAMS
where name = 'EXPORT_TO_ESTOREFRONT_COMPANY')

---------------------------------------------------------------------------------------------
--- Need to restrict to only select attributes that are assigned to the Node  ---  
--extra attributes may be on a product because user moved from one Node to another Node

-----------------------------------------------------------------------------------------------

INSERT INTO [dbo].[EXPORT.EXPORT_PROD_ATTRIBUTES]
           ([PRODUCT_ID]
           ,[LANGUAGE_CODE]
           ,[EPA_CTG_ID]
           ,[CATEGORY]
           ,[EPA_ATT_ID]
           ,[ATTRIBUTE_NAME]
           ,[EPA_ATT_DV_ID]
           ,[ATTRIBUTE_VALUE]
			,COMPANY
			,DISPLAY_SEQ)
(SELECT distinct
         pi.value SX_PRODUCT_ID
        ,'0' LANGUAGE_CODE ------Hard code for now
		,tn.taxonomy_node_id 
		,dvn.value CATEGORY
		,pta.data_name_fk
		,dn.name ATTRIBUTE_NAME
		,dva.data_value_id Epa_dv_id
        ,pta.attribute_event_data ATTRIBUTE_VALUE
,ei.value COMPANY
,dn.display_seq
        from epacube.product_tax_attribute pta with (nolock)
INNER JOIN epacube.product_identification pi with (nolock) ON (pta.product_structure_fk = pi.product_structure_fk
and pi.data_name_fk = (SELECT VALUE FROM epacube.epacube_params
                               WHERE NAME = 'PRODUCT'
                               and application_scope_fk = 100))
LEFT JOIN epacube.data_value dva with (nolock) 
on (pta.attribute_event_data = dva.value
and dva.data_name_fk = pta.data_name_fk)
INNER JOIN Epacube.data_name dn 
On (dn.data_name_id = pta.data_name_fk) 
LEFT JOIN epacube.product_category pc 
ON ( pta.product_structure_fk = pc.product_structure_fk)
INNER join epacube.data_value dvn 
on (dvn.data_value_id = pc.data_value_fk)
INNER JOIN epacube.taxonomy_node tn 
ON ( tn.tax_node_dv_fk = dvn.data_value_id 
and tn.record_status_cr_fk = 1)
INNER join epacube.taxonomy_node_attribute tna with (nolock)  ----- added here
on (tn.taxonomy_node_id = tna.taxonomy_node_fk
and pta.DATA_NAME_FK = tna.DATA_NAME_FK)
INNER JOIN epacube.Taxonomy_tree tt ON (tt.taxonomy_tree_id = tn.taxonomy_tree_fk
and tt.name = @TREE_NAME
and pc.data_name_fk = tt.tax_node_dn_fk)   ---190901
left join epacube.product_association pa With (nolock)
on (pa.product_structure_fk = pi.product_structure_fk
and pa.data_name_fk = 159500) --product_company
left join epacube.entity_identification ei with (nolock)
on (pa.entity_structure_fk = ei.entity_structure_fk
and ei.data_name_fk = 141511)  --company code
where pta.record_status_cr_fk = 1
)

update [dbo].[EXPORT.EXPORT_PROD_ATTRIBUTES]
set COMPANY = (select value from epacube.EPACUBE_PARAMS
where name = 'EXPORT_TO_ESTOREFRONT_COMPANY')

-------------------------------------------------------------------
----NEW TABLE TO HANDLE Attributes assigned to Categories but not to 
---products  - Uncommented select gets all attributes regardless if
-- they have restricted values.  There DV_ID is NULL
--------------------------------------------------------------------

INSERT INTO [dbo].[EXPORT.EXPORT_CATEGORY_ATTRIBUTES]
           ([LANGUAGE_CODE]
           ,[EPA_CTG_ID]
           ,[CATEGORY]
           ,[EPA_ATT_ID]
           ,[ATTRIBUTE_NAME]
           ,[EPA_ATT_DV_ID]
           ,[ATTRIBUTE_VALUE]
           ,[RESTRICTED_IND]
           ,[DISPLAY_SEQ])
select
    '0' LANGUAGE_CODE ------Hard code for now
		,tn.taxonomy_node_id 
		,dvn.value CATEGORY
		,dn.data_name_id
		,dn.name ATTRIBUTE_NAME
		,tnavdv.data_value_id Epa_dv_id
        ,tnavdv.VALUE ATTRIBUTE_VALUE
		,tna.restricted_Data_Values_ind RESTRICTED_IND
		,dn.display_seq
from epacube.taxonomy_node_attribute tna
left outer join epacube.TAXONOMY_NODE_ATTRIBUTE_DV tnav
ON (tnav.TAXONOMY_NODE_ATTRIBUTE_FK = tna.TAXONOMY_NODE_ATTRIBUTE_ID)
left outer join epacube.data_value tnavdv
ON (tnav.DATA_VALUE_FK = tnavdv.DATA_VALUE_ID)
inner join epacube.data_name dn
ON (dn.data_name_id = tna.data_name_fk)
inner join epacube.data_value dva
on (dva.data_value_id = tna.data_value_fk)
inner join epacube.taxonomy_node tn
On (tn.taxonomy_node_id = tna.taxonomy_node_fk)
inner join epacube.data_value dvn
on (dvn.data_value_id = tn.tax_node_dv_fk)
INNER JOIN epacube.Taxonomy_tree tt 
ON (tt.taxonomy_tree_id = tn.taxonomy_tree_fk
and tt.name = @TREE_NAME)


--  NOTE - This select will only get those attributes that have restricted values.
--         If there are no restricted values the attributes are missing from results.

--select
--    '0' LANGUAGE_CODE ------Hard code for now
--		,tn.taxonomy_node_id 
--		,dvn.value CATEGORY
--		,dn.data_name_id
--		,dn.name ATTRIBUTE_NAME
--		,tnavdv.data_value_id Epa_dv_id
--        ,tnavdv.VALUE ATTRIBUTE_VALUE
--		,tna.restricted_Data_Values_ind RESTRICTED_IND
--		,dn.display_seq 
--from epacube.TAXONOMY_NODE_ATTRIBUTE_DV tnav
--inner join epacube.data_value tnavdv
--ON (tnav.DATA_VALUE_FK = tnavdv.DATA_VALUE_ID)
--inner join epacube.taxonomy_node_attribute tna
--ON (tnav.TAXONOMY_NODE_ATTRIBUTE_FK = tna.TAXONOMY_NODE_ATTRIBUTE_ID)
--inner join epacube.data_name dn
--ON (dn.data_name_id = tna.data_name_fk)
--inner join epacube.data_value dva
--on (dva.data_value_id = tna.data_value_fk)
--inner join epacube.taxonomy_node tn
--On (tn.taxonomy_node_id = tna.taxonomy_node_fk)
--inner join epacube.data_value dvn
--on (dvn.data_value_id = tn.tax_node_dv_fk)
--INNER JOIN epacube.Taxonomy_tree tt 
--ON (tt.taxonomy_tree_id = tn.taxonomy_tree_fk
--and tt.name = @TREE_NAME)

--select
--    '0' LANGUAGE_CODE ------Hard code for now
--		,tn.taxonomy_node_id 
--		,dvn.value CATEGORY
--		,dn.data_name_id
--		,dn.name ATTRIBUTE_NAME
--		,dva.data_value_id Epa_dv_id
--        ,dva.VALUE ATTRIBUTE_VALUE
--,tna.restricted_Data_Values_ind RESTRICTED_IND
--,dn.display_seq from epacube.taxonomy_node_attribute tna
--inner join epacube.data_name dn
--ON (dn.data_name_id = tna.data_name_fk)
--inner join epacube.data_value dva
--on (dva.data_value_id = tna.data_value_fk)
--inner join epacube.taxonomy_node tn
--On (tn.taxonomy_node_id = tna.taxonomy_node_fk)
--inner join epacube.data_value dvn
--on (dvn.data_value_id = tn.tax_node_dv_fk)
--INNER JOIN epacube.Taxonomy_tree tt ON (tt.taxonomy_tree_id = tn.taxonomy_tree_fk
--and tt.name = @TREE_NAME)



EXEC common.job_execution_create   @v_job_fk, 'INSERT TAXONOMY PRODUCT',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  
  
------------------------------------------------------------------------------
--EXECUTE CUSTOM SQL
------------------------------------------------------------------------------

   
EXECUTE [export].[EXECUTE_Export_Taxonomy_SQL] 
   @Tree_name


-------------------------------------------------------------------------------
--Determine if extended attributes are being exported.
-------------------------------------------------------------------------------
  
---use epacube params to export to estorefront. 
		SET @Export_Extended_Attributes = (select value from epacube.epacube_params
								where name = 'EXPORT_EXTENDED_ATTRIBUTES')
--------------------------------------------------------------------------------

    if @Export_Extended_Attributes = 1

     BEGIN

		--EXEC common.job_execution_create   @v_job_fk, 'INSERT EXTENDED ATTRIBUTES',
		--                                 @v_input_rows, @v_output_rows, @v_exception_rows,
		--								 200, @l_sysdate, @l_sysdate  

		--SET NOCOUNT ON;
		--SET @l_sysdate = getdate ()
		--SET @ls_stmt = 'started execution of epacube.EpaSFUpdate' 
		--EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
		--				   				  @exec_id   = @l_exec_no OUTPUT;

-------------------------------------------------------------------------------

		EXECUTE [export].[EXPORT_EXTENDED_ATTRIBUTES] 

	END


-------------------------------------------------------------------------------
--Determine if exporting to file or call to Estorefront.
-------------------------------------------------------------------------------
  
---use epacube params to export to estorefront. 
		SET @Export_To_Estorefront = (select value from epacube.epacube_params
								where name = 'EXPORT_TO_ESTOREFRONT')
--------------------------------------------------------------------------------

    if @Export_To_Estorefront = 1

     BEGIN

		EXEC common.job_execution_create   @v_job_fk, 'CALL ESTOREFRONT PROCEDURE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 200, @l_sysdate, @l_sysdate  

		SET NOCOUNT ON;
		SET @l_sysdate = getdate ()
		SET @ls_stmt = 'started execution of epacube.EpaSFUpdate' 
		EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;

-------------------------------------------------------------------------------

				EXEC epacube.[EpaSFUpdate] 
				   @tree_name

DECLARE @recipients varchar(128) 

--SET @recipients = (select value from epacube.epacube_params
--								where name = 'EXPORT_TO_ESTOREFRONT_RECIPIENTS')

set @recipients  = (SELECT value from epacube.config_params
where scope = 'TAX_TREE_EXPORT'
and name = 'TAX_EMAIL_RECIPIENTS')

--set @recipients = 'mschreiber@epacube.com;cvoutour@epacube.com'

DECLARE @Subject nvarchar(256)
Set @Subject = 'Storefront has been updated from Epacube'
DECLARE @Body nvarchar(512)
Set @Body = 'Your changes from epacube have been moved to Storefront. Please Publish the changes in Storefront.'
DECLARE @importance varchar(128) 
set @importance = 'High'


EXEC msdb.dbo.sp_send_dbmail
@profile_name = 'epacubeDBMail',
    @recipients = @recipients,
    @body = @body,
    @subject = @subject ,
    @importance = @importance;

-----------------------------------------------------------------------------------

				SET NOCOUNT ON;
				SET @l_sysdate = getdate ()
				SET @status_desc  = 'finish execution of epacube.EpaSFUpdate' 
				EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
				EXEC exec_monitor.print_status_sp  @l_exec_no;



		END 

EXEC common.job_execution_create   @v_job_fk, 'TAXONOMY EXPORT COMPLETE',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @l_sysdate, @l_sysdate  
  
------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of EXPORT.EXPORT_TO_ESTOREFRONT'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of EXPORT.EXPORT_TO_ESTOREFRONT has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END





