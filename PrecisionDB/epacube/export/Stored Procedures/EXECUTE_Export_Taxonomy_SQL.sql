﻿








-- Copyright 2008
--
-- Procedure created by Cindy Voutour
--
--
-- Purpose: To determine any special sql for specific Export taxonomy trees
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        07/30/2012   Initial SQL Version


CREATE PROCEDURE [export].[EXECUTE_Export_Taxonomy_SQL] 
               ( @Tree_name varchar(64) )

AS
BEGIN

 DECLARE @ls_exec            nvarchar (max);
 DECLARE @l_exec_no          bigint;
 DECLARE @l_rows_processed   bigint;
 DECLARE @l_sysdate			 DATETIME
 
 DECLARE  @ls_stmt			  VARCHAR(MAX)
 DECLARE  @v_count			  BIGINT

DECLARE  @status_desc		  varchar (max)
DECLARE  @ErrorMessage		  nvarchar(4000)
DECLARE  @ErrorSeverity		  int
DECLARE  @ErrorState		  int


BEGIN TRY
SET NOCOUNT ON;
SET @l_sysdate = getdate ()
SET @ls_stmt = 'started execution of Export.EXECUTE_Export_Taxonomy_SQL ' 
              + CAST (@tree_name  AS VARCHAR(64) )
EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   				  @exec_id   = @l_exec_no OUTPUT;




------------------------------------------------------------------------------------------
---  Perform Dynamic SQL on ACTION 
------------------------------------------------------------------------------------------
---Need to loop by seq id

DECLARE 
              @vm$seq_id            varchar(10), 
              @vm$Action1_sql       varchar(Max),
			  @vm$Action2_sql       varchar(Max)

 DECLARE  cur_v_import cursor local for
			SELECT SEQ_ID, ACTION1_SQL, ACTION2_SQL 
						FROM EXPORT.EXPORT_TAXONOMY_sql with (nolock)
						WHERE TREE_NAME = @Tree_name
						AND Record_status_cr_fk = 1
						order by seq_id asc
--    

 OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO
				@vm$seq_id, 
				@vm$Action1_sql,
				@vm$Action2_sql

WHILE @@FETCH_STATUS = 0 
         BEGIN
    
            SET @status_desc =  'Export_Taxonomy_SQL: inserting - '
                                + @vm$seq_id
                                + ' '
                                + @vm$Action1_sql
								+ ' '
                                + ISNULL (@vm$Action2_sql, ' AND 1 = 1')


            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc, @epa_job_id = 3;


------------------------------------------------------------------------------------------
---  Perform Dynamic SQL on ACTION1 and ACTION2
------------------------------------------------------------------------------------------

--      	
		SET @ls_exec = @vm$Action1_sql 
--						+ ' AND Tree_name =' 
--						+  cast(isnull (@tree_name, -999) as varchar(50))
						
----    
						+ ' ' +
						ISNULL (@vm$Action2_sql, ' AND 1 = 1')
----                    
----
---- ( SELECT ISNULL(ACTION2_SQL, ' AND 1 = 1' ) FROM Import.import_package_sql
----						WHERE Import_package_FK = @in_import_package_fk 
----						AND Record_status_cr_fk = 1  )


----           TRUNCATE TABLE COMMON.T_SQL
            INSERT INTO COMMON.T_SQL
                ( TS, SQL_TEXT )
            VALUES ( GETDATE(), ISNULL(@ls_exec,'Export Taxonomy SQL PART') )

      EXEC sp_executesql 	@ls_exec;

----

 FETCH NEXT FROM cur_v_import INTO 
										  @vm$seq_id,
										  @vm$action1_sql,
										  @vm$Action2_sql

 END --cur_v_import LOOP

WHILE @@FETCH_STATUS = 0 
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 




------------------------------------------------------------------------------------------
--   Finish Execution Log and END CATCH
------------------------------------------------------------------------------------------

SET @status_desc = 'finished execution of EXPORT.EXECUTE_EXPORT_TAXONOMY_SQL'
EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc;
EXEC exec_monitor.print_status_sp  @l_exec_no;

END TRY
BEGIN CATCH 

		SELECT 
			@ErrorMessage = 'Execution of EXPORT.EXECUTE_EXPORT_TAXONOMY_SQL has failed ' + ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();


         EXEC exec_monitor.Report_Error_sp @l_exec_no;
         EXEC exec_monitor.email_error_sp @l_exec_no, 'dbexceptions@epacube.com';
         RAISERROR (@ErrorMessage, -- Message text.
				    @ErrorSeverity, -- Severity.
				    @ErrorState -- State.
				    );
END CATCH

END
























































