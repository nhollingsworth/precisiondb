﻿







CREATE PROCEDURE [export].[EXPORT_DPS_CUST_ATTRIBUTES] 
         
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        02/06/2013  Initial SQL Version
-- RG		 03/21/2013  Finishing up remaining available fields 
-- CV        04/02/2013  Added loop to update total sales
-- CV        04/15/2013  Adding logic to pull first prod price type
-- CV        05/21/2013  Add logging to tell if missing customer raw data.
-- CV        05/22/2013  Clean up section if mult not used.
-- CV        05/30/2013  Added Eclipsisms
-- CV        06/18/2013  Changing Table to last import customer table
-- CV        07/08/2013  remove cariage return
-- CV        08/05/2013  Fixed bad substring logic
-- TM        08/13/2013  Removed hardcode -0 replaced with customer Disc_Code
-- TM	     08/15/2013  comment out: rp.HOST_PRECEDENCE_LEVEL is not the level code attribute we are looking to append
-- CV        01/22/2014  added logging :-)

  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @v_batch_no   bigint
        
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT
DECLARE @customerAttributePackageFK bigint
DECLARE @custAcctBillToFKDN int
DECLARE @Mult_price_type int
DECLARE @Mult_rebate_type int
DECLARE @ERP_HOST   Varchar(32)
        
                
 
 
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of export.EXPORT_DPS_CUST_ATTRIBUTES '
	  
	   SET @ERP_HOST = 
			(Select value from epacube.EPACUBE_PARAMS where NAME = 'ERP HOST')

	  SET @customerAttributePackageFK = (select EXPORT_PACKAGE_ID
										from export.export_package
										where name = 'CUSTOMER ATTRIBUTE')

	  SET @custAcctBillToFKDN =epacube.getDataNameId('CUSTOMER BILL TO')
	  --144010

	  SET @Mult_price_type = (select ISNULL(value,0) from epacube.EPACUBE_PARAMS where name = 'SXE MULTIPLE CPT CHECK')
	  SET @Mult_rebate_type = (select ISNULL(value,0) from epacube.EPACUBE_PARAMS where name = 'SXE MULTIPLE CRT CHECK')
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
 

      
	  --select * from import.IMPORT_SXE_SALES_TRANSACTIONS
--select * from export.export_sales_dps
----select * from export.export_map_metadata
--select * from export.EXPORT_CUSTOMER_ATTRIBUTES_DPS where CUST_ACCT_BILL_TO_ID = '100032'
--select * from import.import_sxe_customers_last_import where CUST_NO = '100032' and SHIP_TO is null
--select * from import.IMPORT_SXE_TABLE_DATA where value2 = '100032'

--IF ISNULL((select count(1) from import.import_sxe_customers_last_import),0) > 0 
--BEGIN

                
truncate table export.EXPORT_CUSTOMER_ATTRIBUTES_DPS


---------------------------------------------------------------------------------------
----CUSTOMER LIST-----
---------------------------------------------------------------------------------------

INSERT INTO [export].[EXPORT_CUSTOMER_ATTRIBUTES_DPS]
           ([CUST_ACCT_BILL_TO_ID]
           ,[CUST_DSO]
           ,[CUST_GROUP1]
           ,[CUST_GROUP2]
--           ,[CUST_GROUP3]
--           ,[CUST_LIFECYCLE]
--           ,[CUST_NAICS]
           ,[CUST_RANK] 
           ,[CUST_SIC] 
           ,[CUST_TOTAL_SALES]
           ,[CUST_WHSE_DEFAULT] 
           ,[SEGCUST_ENT_HIST] 
           ,[SEGCUST_RBT_ENT_HIST] 
           ,[SEGLIST_CUST_MARKETS]
--           ,[SEGLIST_CUST_REBATES]
)

(select DISTINCT  esd.CUST_ACCT_BILL_TO_ID 

,(select attribute_event_data from epacube.entity_attribute (nolock)
  where entity_structure_fk = ei.entity_structure_fk
  and data_name_fk = 
		(select data_name_fk from export.export_map_metadata (nolock)
	     where export_package_Fk =  @customerAttributePackageFK
		 and column_name = 'CUST_DSO'))
----END CUST_DSO
,(select value from epacube.data_value (nolock)
	where data_value_Id = 
		(select data_value_fk from epacube.entity_Category (nolock)
		 where  entity_structure_fk = ei.entity_structure_fk
			and data_name_fk = 
					(select data_name_fk from export.export_map_metadata (nolock)
					 where export_package_Fk =  @customerAttributePackageFK
					 and column_name = 'CUST_GROUP1'))) 
----END CUST_GROUP1
,(select value from epacube.data_value (nolock)
	where data_value_Id = 
		(select data_value_fk from epacube.entity_Category (nolock)
		 where  entity_structure_fk = ei.entity_structure_fk
			and data_name_fk = 
					(select data_name_fk from export.export_map_metadata (nolock)
					 where export_package_Fk =  @customerAttributePackageFK
					 and column_name = 'CUST_GROUP2'))) 
----END CUST_GROUP2
,(select value from epacube.data_value (nolock)
	where data_value_Id = 
		(select data_value_fk from epacube.entity_Category (nolock)
		 where  entity_structure_fk = ei.entity_structure_fk
			and data_name_fk = 
					(select data_name_fk from export.export_map_metadata (nolock)
					 where export_package_Fk =  @customerAttributePackageFK
					 and column_name = 'CUST_RANK'))) 		 
----END CUST_RANK
,(select value from epacube.data_value (nolock)
	where data_value_Id = 
		(select data_value_fk from epacube.entity_Category (nolock)
		 where  entity_structure_fk = ei.entity_structure_fk
			and data_name_fk = 
					(select data_name_fk from export.export_map_metadata (nolock)
					 where export_package_Fk =  @customerAttributePackageFK
					 and column_name = 'CUST_SIC'))) 
----END CUST_SIC
,NULL
----END CUST_TOTAL_SALES
,(select attribute_event_data from epacube.entity_attribute (nolock)
		 where  entity_structure_fk = ei.entity_structure_fk
			and data_name_fk = 
					(select data_name_fk from export.export_map_metadata (nolock)
					 where export_package_Fk =  @customerAttributePackageFK
					 and column_name = 'CUST_WHSE_DEFAULT')) 
----END CUST_WHSE_DEFAULT
,NULL
	--				 ,(select value from epacube.data_value (nolock)
	--where data_value_Id = 
	--	(select top 1 data_value_fk from epacube.entity_mult_type (nolock)
	--	 where  entity_structure_fk = ei.entity_structure_fk
	--		and data_name_fk = 
	--				(select data_name_fk from export.export_map_metadata (nolock)
	--				 where export_package_Fk =  @customerAttributePackageFK
	--				 and column_name = 'SEGCUST_ENT_HIST')))
--END SEGCUST_ENT_HIST
,case when @Mult_rebate_type = 1 then NULL else (select value from epacube.data_value (nolock)
	where data_value_Id = 
		(select top 1 data_value_fk from epacube.entity_mult_type (nolock)
		 where  entity_structure_fk = ei.entity_structure_fk
			and data_name_fk = 
					(select data_name_fk from export.export_map_metadata (nolock)
					 where export_package_Fk =  @customerAttributePackageFK
					 and column_name = 'SEGCUST_RBT_ENT_HIST'))) end

----END SEGCUST_RBT_ENT_HIST
,NULL
----END SEGLIST_CUST_MARKETS
from export.export_sales_dps esd (nolock)
inner join epacube.entity_identification ei (nolock)
on (esd.CUST_ACCT_BILL_TO_ID = ei.value
and ei.entity_data_name_fk =  @custAcctBillToFKDN
)
)


	    SET @ls_stmt = 'started export.EXPORT_DPS_CUST_ATTRIBUTES - SXE PRICE TYPE LOOP '
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;




IF @ERP_HOST = 'SXE'
BEGIN

--SXE MULTIPLE CPT CHECK = 1
--SXE MULTIPLE CRT CHECK = 1
------------------------------------------------------------------------------------
---LOOP TO GET MULITPLE VALUES AND LIST AS STRING FOR CUSTOMER PRICE TYPE
-----------------------------------------------------------------------------------
--If @Mult_price_type in ( 1,0)  

--BEGIN

 
	DECLARE @v_cust_structure bigint
	DECLARE @v_cust_bill varchar(50)
	
	DECLARE  cur_v_import cursor local for
		select distinct emt.entity_structure_fk , ei.VALUE
		from epacube.ENTITY_MULT_TYPE emt (nolock)
		inner join epacube.entity_identification ei (nolock)
		on (ei.ENTITY_STRUCTURE_FK = emt.ENTITY_STRUCTURE_FK)
		--and  ei.entity_data_name_fk = @custAcctBillToFKDN)
		inner join export.export_sales_dps esd (nolock)
		on (esd.CUST_ACCT_BILL_TO_ID = ei.value)
		where emt.DATA_NAME_FK = ---244900
		(select data_name_fk from export.EXPORT_MAP_METADATA where COLUMN_NAME = 'SEGCUST_ENT_HIST')

													
		OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO 
				@v_cust_structure,
				 @v_cust_bill
				 				
				
         WHILE @@FETCH_STATUS = 0 
         BEGIN
            
			
--2nd Loop to string the CUST price type
--------------------------------------------------------------------------------
			DECLARE @v_LIST_VALUES VARCHAR(MAX)
			 
			 SET @v_LIST_VALUES = ''
			  SELECT @v_LIST_VALUES =  td.value1 + '-' + td.value4 + '; ' + @v_LIST_VALUES
			FROM epacube.data_value dv
			inner join epacube.entity_mult_type emt
			on (emt.DATA_VALUE_FK = dv.DATA_VALUE_ID
			and emt.ENTITY_STRUCTURE_FK = @v_cust_structure )
			left join import.IMPORT_SXE_TABLE_DATA TD
			on (td.DATA_NAME = 'customer ptype'
			and VALUE1 = dv.value)
			inner join epacube.entity_identification ei
			on (td.VALUE2 = ei.value
			and  ei.ENTITY_STRUCTURE_FK = @v_cust_structure)
			  where dv.DATA_NAME_FK =-- 244900
			  (select data_name_fk from export.EXPORT_MAP_METADATA where COLUMN_NAME = 'SEGCUST_ENT_HIST')
			 

			DECLARE  cur_v_to_string cursor local for

			SELECT     case when @v_LIST_VALUES = '' then 'NO_SEGMENTT' else @v_list_values end
																	
				OPEN cur_v_to_string;
				FETCH NEXT FROM cur_v_to_string INTO 
						 @v_LIST_VALUES

					 WHILE @@FETCH_STATUS = 0 
							 BEGIN

					UPDATE [export].[EXPORT_CUSTOMER_ATTRIBUTES_DPS]
					SET seglist_cust_markets =  substring(@v_LIST_VALUES,1,len(@v_list_values)-1)
					WHERE CUST_ACCT_BILL_TO_ID = @v_cust_bill

					 update import.import_sxe_customers_last_import
					 set CUST_PRICE_TYPE = NULL
					 where cust_price_type = ','

					 update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
					set SEGCUST_ENT_HIST = seglist_cust_markets
					where SEGLIST_CUST_MARKETS = 'NO_SEGMENT'

					update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
					set SEGCUST_ENT_HIST = (
			--select substring(cust_price_Type, 1,(CHARINDEX(',',cust_price_type))-1) + '-'+ price_code +'-0'
			------------ TAM edited the hardcode -0 to be the discount code from the ARSC record
			select substring(cust_price_Type, 1,(CHARINDEX(',',cust_price_type))-1) + '-'+ price_code +'-' + DISC_CODE
			from import.import_sxe_customers_last_import where SHIP_TO is NULL and cust_price_type like '%,%' and CUST_NO = @v_cust_bill)
			WHERE CUST_ACCT_BILL_TO_ID = @v_cust_bill
			 

			update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
					set SEGCUST_ENT_HIST = (
			--select 'NO_SEGMENT' + '-'+ price_code +'-0'
			------------ TAM edited the hardcode -0 to be the discount code from the ARSC record
			select 'NO_SEGMENT' + '-'+ price_code +'-' + DISC_CODE
			from import.import_sxe_customers_last_import where SHIP_TO is NULL and cust_price_type like ',%' and CUST_NO = @v_cust_bill)
			WHERE CUST_ACCT_BILL_TO_ID = @v_cust_bill
			AND SEGCUST_ENT_HIST like '-%'

			update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
					set SEGCUST_ENT_HIST = (
			select 'NO_SEGMENT' 
			from import.import_sxe_customers_last_import where SHIP_TO is NULL and cust_price_type is NULL and CUST_NO = @v_cust_bill)
			WHERE CUST_ACCT_BILL_TO_ID = @v_cust_bill
			AND SEGCUST_ENT_HIST IS NULL


					 update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
					 set SEGLIST_CUST_MARKETS = SEGCUST_ENT_HIST + ';' + SEGLIST_CUST_MARKETS
					 where segcust_ent_hist <> 'NO_SEGMENT'
					 and CUST_ACCT_BILL_TO_ID = @v_cust_bill
	 
	 ----when no pricetypes in sx table data.. and one price type in customer file.  we hard code the -0 because we are keeping the same as waht the value 4 in sxe table data formated.  We believe
	 ----that value4 in sx table data is the equivalent to concantonation of Price_Code and Disc_Code from the import.import_sxe_customers file.
					
					UPDATE [export].[EXPORT_CUSTOMER_ATTRIBUTES_DPS]
					--SET seglist_cust_markets =  (select cust_price_type + '-'+ price_code +'-0' from import.import_sxe_customers_last_import where SHIP_TO is NULL and CUST_PRICE_TYPE not like '%,%'  and CUST_NO = @v_cust_bill)
					------------ TAM edited the hardcode -0 to be the discount code from the ARSC record
					SET seglist_cust_markets =  (select cust_price_type + '-'+ price_code +'-' + DISC_CODE from import.import_sxe_customers_last_import where SHIP_TO is NULL and CUST_PRICE_TYPE not like '%,%'  and CUST_NO = @v_cust_bill)
					WHERE CUST_ACCT_BILL_TO_ID = @v_cust_bill
					AND seglist_cust_markets = 'NO_SEGMENT'

					UPDATE [export].[EXPORT_CUSTOMER_ATTRIBUTES_DPS]
					SET SEGCUST_ENT_HIST = seglist_cust_markets
					WHERE CUST_ACCT_BILL_TO_ID = @v_cust_bill
					AND SEGCUST_ENT_HIST is NULL

------ where there is not price type in customer or table data set to no segments		
					
					UPDATE [export].[EXPORT_CUSTOMER_ATTRIBUTES_DPS]
					SET SEGCUST_ENT_HIST = 'NO_SEGMENT'
					,seglist_cust_markets = 'NO_SEGMENT'
					WHERE SEGCUST_ENT_HIST is NULL
					AND SEGLIST_CUST_MARKETS is NULL

			
					FETCH NEXT FROM cur_v_to_string INTO 
								 @v_LIST_VALUES



					 END --cur_v_to_String LOOP
					 CLOSE cur_v_to_string;
					 DEALLOCATE cur_v_to_string; 


	
    FETCH NEXT FROM cur_v_import INTO 
				
				  @v_cust_structure,
				   @v_cust_bill



     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 

--END  ---end first loop

 SET @ls_stmt = 'started export.EXPORT_DPS_CUST_ATTRIBUTES - SXE REBATE TYPE LOOP '
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;

-------------------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------
---REBATE TYPE LOOP TO GET MULITPLE VALUES AND LIST AS STRING
-----------------------------------------------------------------------------------
--If @Mult_rebate_type in ( 1, 0) 

--BEGIN

 
	--DECLARE @v_cust_structure bigint
	--DECLARE @v_cust_bill varchar(50)
	
	DECLARE  cur_v_import cursor local for
		select distinct emt.entity_structure_fk , ei.VALUE
		from epacube.ENTITY_MULT_TYPE emt (nolock)
		inner join epacube.entity_identification ei (nolock)
		on (ei.ENTITY_STRUCTURE_FK = emt.ENTITY_STRUCTURE_FK)
		--and  ei.entity_data_name_fk = @custAcctBillToFKDN)
		inner join export.export_sales_dps esd (nolock)
		on (esd.CUST_ACCT_BILL_TO_ID = ei.value)
		where emt.DATA_NAME_FK = ---244901
		(select DATA_NAME_FK from export.EXPORT_MAP_METADATA where COLUMN_NAME = 'SEGCUST_RBT_ENT_HIST')

													
		OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO 
				@v_cust_structure,
				 @v_cust_bill
				 				
				
         WHILE @@FETCH_STATUS = 0 
         BEGIN
            
			

--2nd Loop to string the CUST rebate type
--------------------------------------------------------------------------------
			--DECLARE @v_LIST_VALUES VARCHAR(MAX)
			 
			 SET @v_LIST_VALUES = ''
			 SELECT @v_LIST_VALUES =  dv.value + '; ' + @v_LIST_VALUES
			FROM epacube.data_value dv
			inner join epacube.entity_mult_type emt
			on (emt.DATA_VALUE_FK = dv.DATA_VALUE_ID
			and emt.ENTITY_STRUCTURE_FK = @v_cust_structure )
			 where dv.DATA_NAME_FK = -- 244901
			 (select DATA_NAME_FK from export.EXPORT_MAP_METADATA where COLUMN_NAME = 'SEGCUST_RBT_ENT_HIST')

			DECLARE  cur_v_to_string cursor local for

			SELECT    @v_LIST_VALUES
																	
				OPEN cur_v_to_string;
				FETCH NEXT FROM cur_v_to_string INTO 
						 @v_LIST_VALUES

					 WHILE @@FETCH_STATUS = 0 
							 BEGIN

					UPDATE [export].[EXPORT_CUSTOMER_ATTRIBUTES_DPS]
					SET SEGLIST_CUST_REBATES = substring(@v_LIST_VALUES,1, len(@v_list_Values)-1)
					WHERE CUST_ACCT_BILL_TO_ID = @v_cust_bill
			
					FETCH NEXT FROM cur_v_to_string INTO 
								 @v_LIST_VALUES



					 END --cur_v_to_String LOOP
					 CLOSE cur_v_to_string;
					 DEALLOCATE cur_v_to_string; 


	
					
    FETCH NEXT FROM cur_v_import INTO 
				
				  @v_cust_structure,
				   @v_cust_bill



     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 

--END  ---end first loop

 
 
 
 END   ----ERP HOST = SXE
-------------------------------------------------------------------------------------------------------------

 SET @ls_stmt = 'started export.EXPORT_DPS_CUST_ATTRIBUTES - ECL UPDATES'
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;

IF @ERP_HOST = 'ECLIPSE'
BEGIN

Update  eca
set SEGCUST_ENT_HIST = dv.value
from  export.EXPORT_CUSTOMER_ATTRIBUTES_DPS eca (nolock)
inner join epacube.ENTITY_IDENTIFICATION ei (nolock)
on (ei.VALUE = eca.CUST_ACCT_BILL_TO_ID
and ei.ENTITY_DATA_NAME_FK = 144010)
inner join  epacube.entity_Category ec (nolock)
on ( ec.entity_structure_fk = ei.entity_structure_fk
			and ec.data_name_fk = 
					(select data_name_fk from export.export_map_metadata (nolock)
						 where column_name = 'SEGCUST_ENT_HIST'))
						 inner join epacube.DATA_VALUE dv (nolock)
						 on (dv.DATA_VALUE_ID = ec.DATA_VALUE_FK)
						 
						 
update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
set SEGCUST_ENT_HIST = 'NO_SEGMENT'
where SEGCUST_ENT_HIST is NULL


update dps
set CUST_WHSE_DEFAULT = pe.parent_entity_code
from import.ecl_prov_entities  pe
inner join export.EXPORT_CUSTOMER_ATTRIBUTES_DPS dps
on (pe.code = dps.CUST_ACCT_BILL_TO_ID)



END ----ERP HOST  = ECLIPSE


 SET @ls_stmt = 'started export.EXPORT_DPS_CUST_ATTRIBUTES - TOTAL SALES LOOP'
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;


------------------------------------------------------------------------------------
---LOOP TO UPDATE TOTAL SALES
-----------------------------------------------------------------------------------
--DECLARE @v_cust_bill   varchar(50)  --declared above
DECLARE @v_total_sales bigint

DECLARE  cur_v_import cursor local for
		select DISTINCT CUST_ACCT_BILL_TO_ID,
						SUM (cast(order_item_shipped_qty as decimal(18,2)) * order_amt_sell_price )
						 from export.export_sales_dps
						GROUP BY CUST_ACCT_BILL_TO_ID

												
		OPEN cur_v_import;
        FETCH NEXT FROM cur_v_import INTO 
				 @v_cust_bill,
				@v_total_sales
				
				
         WHILE @@FETCH_STATUS = 0 
         BEGIN
          
					
UPDATE [export].[EXPORT_CUSTOMER_ATTRIBUTES_DPS]
SET CUST_TOTAL_SALES = @v_total_sales
WHERE CUST_ACCT_BILL_TO_ID = @v_cust_bill

					
    FETCH NEXT FROM cur_v_import INTO 
				 @v_cust_bill,
				@v_total_sales


     END --cur_v_import LOOP
     CLOSE cur_v_import;
	 DEALLOCATE cur_v_import; 

	 
	  SET @ls_stmt = 'started export.EXPORT_DPS_CUST_ATTRIBUTES - CLEAN UP'
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;

----------------------------------------------------------------------------------------------------
--CLEAN UP SECTION   
----------------------------------------------------------------------------------------------------
update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
set CUST_DSO = NULL
where CUST_DSO = '0'

update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
set CUST_GROUP2 = NULL
where CUST_GROUP2 = '0'

update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
set CUST_SIC = NULL
where CUST_SIC = '0'

update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
set SEGLIST_CUST_MARKETS = SUBSTRING(seglist_cust_markets,1,256)
where len(SEGLIST_CUST_MARKETS) > 256

update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
set SEGLIST_CUST_REBATES = SUBSTRING(SEGLIST_CUST_REBATES,1,256)
where len(SEGLIST_CUST_REBATES) > 256

------List not used
if @Mult_price_type = 0
begin
update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
set SEGLIST_CUST_MARKETS = NULL
end 

if @Mult_rebate_type = 0
Begin
update export.EXPORT_CUSTOMER_ATTRIBUTES_DPS
set SEGLIST_CUST_REBATES = NULL

end



 SET @ls_stmt = 'started export.EXPORT_DPS_CUST_ATTRIBUTES - UPDATE SALESHIST PRICE TYPE'
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;

---------------------------------------------------------------------------------------------------
--UPDATE Sales History with cust price type data from rules if different then on customer file
 -------------------------------------------------------------------------------------------------------------
 
 if @ERP_HOST = 'SXE'
 BEGIN
 
 update  esd
set SEGCUST_MRK_HIST = rfs.cust_value1
from export.EXPORT_SALES_DPS esd 
inner join synchronizer.rules r With (nolock)
on (r.HOST_RULE_XREF = esd.MTX_SELL_MATRIX_ID
and r.UPDATE_USER = 'HOST PRICE RULES LOAD'
and r.RECORD_STATUS_CR_FK = 1)
inner join synchronizer.V_RULES_FILTER_SET rfs With (nolock)
on (rfs.RULES_FK = r.RULES_ID)
inner join export.EXPORT_CUSTOMER_ATTRIBUTES_DPS eca With (nolock)
on (eca.CUST_ACCT_BILL_TO_ID = esd.CUST_ACCT_BILL_TO_ID)
where rfs.cust_value1 <> substring(esd.segcust_mrk_hist,1,CHARINDEX('-',esd.SEGCUST_MRK_HIST,1)-1)
--where rfs.cust_value1 <> substring(eca.SEGCUST_ENT_HIST,1,1) 

------------------- This is the SEGCUST_MRK_HIST piece of code that is inaccurate.  rp.HOST_PRECEDENCE_LEVEL is not the level code attribute we are looking to append.
------------------- The PD record has buckets for prices in level 1, level 2, etc. to level 9.  The ARSC.Price_Code defines which price level to use.  The sales person
------------------- can alter the level on the header of the sales transaction in SXe.  This is OEEH.PriceCD.  It does not appear we get this data from ICAEP. But I am
------------------- sure Gary can tell us where to find it based on the PD record. 
--update  esd
--set SEGCUST_MRK_HIST = SEGCUST_MRK_HIST + '-' + rp.HOST_PRECEDENCE_LEVEL
--from export.EXPORT_SALES_DPS esd
--inner join synchronizer.rules r With (nolock)
--on (r.HOST_RULE_XREF = esd.MTX_SELL_MATRIX_ID
--and r.UPDATE_USER = 'HOST PRICE RULES LOAD'
--and r.RECORD_STATUS_CR_FK = 1)
--inner join synchronizer.RULES_PRECEDENCE rp With (nolock)
--on (rp.RULES_PRECEDENCE_ID = r.RULES_PRECEDENCE_FK)
--inner join export.EXPORT_CUSTOMER_ATTRIBUTES_DPS eca With (nolock)
--on (eca.CUST_ACCT_BILL_TO_ID = esd.CUST_ACCT_BILL_TO_ID)

----update if the value is = to customer number then use the value from customer file 
update esd
set SEGCUST_MRK_HIST = NULL
from  export.EXPORT_SALES_DPS esd
where substring(esd.segcust_mrk_hist,1,CHARINDEX('-',esd.SEGCUST_MRK_HIST,1)-1) = esd.CUST_ACCT_BILL_TO_ID

update export.export_customer_Attributes_dps
set seglist_cust_markets = replace(seglist_cust_markets,CHAR(13),'')

END

--END ------top if on customer data being there...


---------------------------------------------------------------------------------------------------------


  SET @status_desc = 'finished execution of Export.EXPORT_DPS_CUST_ATTRIBUTES'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of Export.EXPORT_DPS_CUST_ATTRIBUTES has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END
















