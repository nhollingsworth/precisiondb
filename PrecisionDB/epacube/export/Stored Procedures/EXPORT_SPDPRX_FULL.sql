﻿









-- Copyright 2013
--
-- Procedure created by Robert Gannon
--
--
-- Purpose: Execute SSIS Import packages
--
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- RG        07/28/2013   Initial SQL Version
-- CV        08/06/2013   Added update to MfR group code and Country of Origin
-- RG		 08/07/2013   Parameterization for publishing


CREATE PROCEDURE [export].[EXPORT_SPDPRX_FULL]  
AS 
      BEGIN

            DECLARE @rt int
            DECLARE @cmd varchar(4000)                      
            DECLARE @ls_stmt varchar(1000)
                  , @l_exec_no bigint
                  , @status_desc varchar(max)
                  , @ErrorMessage nvarchar(4000)
                  , @ErrorSeverity int
                  , @ErrorState INT
				  , @ProductExportJobClass bigint
            DECLARE @job_name VARCHAR(250)

			DECLARE @l_sysdate DATETIME       
DECLARE @v_job_fk     bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime
DECLARE @v_job_processing_status int



--------------------------------------
--Declare Data Name variables used in Query
--------------------------------------
DECLARE  @VendorUOM_DNID int
	    ,@CashDiscountPercent_DNID int
		,@UNSPSC_DNID int		
		,@CommodityCode_DNID int
		,@TStemNumer_DNID int
		,@TSPSLgcyQty3_DNID int
		,@TSCountryOfOrigin_DNID int
		,@TSPercentMfgdInUSA_DNID int
		,@TSTradeColumnCosts_DNID int
		,@CEDNetCatalog_DNID int
		,@CEDNetDescription_DNID int
		,@CEDNetDescUPCPrior_DNID int
		,@CEDRefUOM_DNID int
		,@CEDProductStdMfrCode_DNID int
		,@CEDMfrUCC_DNID int
--------------------------------------		


--------------------------------------
--Declare variables for determining export path
--------------------------------------
DECLARE @PACKAGE_NAME varchar(256)           
       ,@SERVER_NAME varchar(256)
	   ,@SSIS_PATH varchar(256)	   
	   ,@PATH_START INT
	   ,@PATH_END INT
	   ,@PATH_LENGTH INT
	   ,@FILE_DATE_STRING varchar(32)


DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

BEGIN TRY
	SET NOCOUNT ON



  SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of export.EXPORT_SPDPRX_FULL'             

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
 

--Initialize Data Name variables used in Query
--------------------------------------------
		set @VendorUOM_DNID = (select epacube.getDataNameId('VENDOR UOM'))
	    set @CashDiscountPercent_DNID = (select epacube.getDataNameId('CASH DISCOUNT PERCENT'))
		set @UNSPSC_DNID = (select epacube.getDataNameId('UNSPSC'))		
		set @CommodityCode_DNID = (select epacube.getDataNameId('COMMODITY CODE'))
		set @TStemNumer_DNID = (select epacube.getDataNameId('TS ITEM NUMBER'))
		set @TSPSLgcyQty3_DNID = (select epacube.getDataNameId('TS PS LGCY QTY3'))
		set @TSCountryOfOrigin_DNID = (select epacube.getDataNameId('TS COUNTRY OF ORIGIN'))
		set @TSPercentMfgdInUSA_DNID = (select epacube.getDataNameId('TS PERCENT MFGD IN USA'))
		set @TSTradeColumnCosts_DNID = (select epacube.getDataNameId('TS TRADE COLUMN COSTS'))
		set @CEDNetCatalog_DNID = (select epacube.getDataNameId('CED NET CATALOG'))
		set @CEDNetDescription_DNID = (select epacube.getDataNameId('CED NET DESCRIPTION'))
		set @CEDNetDescUPCPrior_DNID = (select epacube.getDataNameId('CED NET DESC UPC PRIOR'))
		set @CEDRefUOM_DNID = (select epacube.getDataNameId('CED REF UOM'))
		set @CEDProductStdMfrCode_DNID = (select epacube.getDataNameId('CED PRODUCT STD MFR CODE'))
		set @CEDMfrUCC_DNID = (select epacube.getDataNameId('CED MFR UCC'))
----------------------------------------------

--Initialize Publishing/package path variables
----------------------------------------------
set @FILE_DATE_STRING = (select replace(replace(replace(convert(varchar(20),getdate(),120),'-',''),':',''),' ','')) 
set @PACKAGE_NAME = 'Export_SPDPRX_FULL.dtsx'   
set @PATH_START = (select charindex('\\',value) from epacube.config_property 
						where config_fk = (select config_id from epacube.config 
										   where name = 'SPDPRX Export FULL')
						and name = 'publisher')
set @PATH_END = (select charindex('IN',value) from epacube.config_property
					    where config_fk = (select config_id from epacube.config 
										   where name = 'SPDPRX Export FULL')
						and name = 'publisher')              
set @PATH_LENGTH = (select ((@PATH_END)-(@PATH_START)))
set @SERVER_NAME = (select @@SERVERNAME)
set @SSIS_PATH = (select substring(value, @PATH_START ,@PATH_LENGTH)
				  from epacube.CONFIG_PROPERTY
				  where config_fk = (select config_id from epacube.config 
								     where name = 'SPDPRX Export FULL')
				  and name = 'publisher') 

---------------------------------------------------------------------------------
-- Fetch the Job ID
---------------------------------------------------------------------------------
	EXEC seq_manager.db_get_next_sequence_value_impl 'common',
                    'job_seq', @v_job_fk output

    SET @v_job_class_fk  = (select job_class_id from common.job_class where name = 'EXPORT PRODUCTS')        --- Export class
    SET @v_job_user = 'EPACUBE'
    SET @v_job_name = 'EXPORT SPDPRX FULL'
    SET @v_job_date = @l_sysdate
    SET @v_data1   = 'LAST JOB:: '
                     + CAST ( ISNULL ( @v_last_job_fk, -999 )  AS VARCHAR(32) )
                     + ' - '
                     + CAST ( ISNULL ( @v_last_job_date, '01/01/2000' ) AS VARCHAR(64) )
    SET @v_data2   = @v_last_job_date

	set @v_job_processing_status = (select epacube.getCodeRefId('JOB_STATUS','PROCESSING'))
    
    EXEC common.job_create  @v_job_fk, @V_job_class_fk , @V_JOB_NAME, @V_DATA1, @V_DATA2, NULL,
										   @V_JOB_USER,  @V_OUT_JOB_FK OUTPUT;

    EXEC common.job_execution_create   @v_job_fk, 'START EXPORT SPDPRX FULL',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 @v_job_processing_status, @l_sysdate, @l_sysdate  
  
			
-----------------------------------------------------------------------------------
---- Fetch the data into export product 200
-----------------------------------------------------------------------------------			
INSERT INTO export.export_product_200 (

job_fk,product_structure_fk,export_filename,export_package_fk
,data_position2 -- CED NET CATALOG#(4101001)
,data_position4 -- TS ITEM NUMBER#(820101)
,data_position5 -- CED NET DESCRIPTION#(4104001)
,data_position6 -- COMMODITY CODE#(110902)
,data_position8 -- VENDOR UOM#(110606)
,data_position9 -- TS COST#(822101)
,data_position10 -- TS TRADE LIST#(822105)
,data_position11 -- TS COL1#(822104)
,data_position12 -- TS COL2#(822103)
,data_position13 -- TS COL3#(822102)
,data_position15 -- CASH DISCOUNT PERCENT#(110819)
,data_position16 -- TS PS LGCY QTY3#(820814)
,data_position17 -- CED MFR GROUP CODE 1#(100143901)
,data_position18 -- UNSPSC#(110901)
,data_position19 -- TS COUNTRY OF ORIGIN#(820816)
,data_position20 -- TS PERCENT MFGD IN USA#(820817)
,data_position21 -- CED PO REF COST#(4301002)
,data_position22 -- CED REF UOM#(4106001)
,data_position23 -- TS LEGACY UPDATE STATUS#(820201)
,data_position7 -- EXPORT RECORD TYPE#(100050)
,data_position1 -- CED PRODUCT STD MFR CODE#(100110901)
,data_position3 -- PRIMARY VENDOR#(159300)
,data_position24 -- EXPORT RECORD TYPE#(100050)
,data_position25 -- JOB ID#(100010)
)
(
SELECT CAST(@v_job_fk AS VARCHAR(30)), ProductKey, '', 0, [CEDNetCatalog],[Mfr Item No],[CEDNetDesc],-- [CED Net Desc Upc Prior],
 [Comm Cd], [Vdr UOM],
[TS COST#],[TS TRADE LIST#], [TS COL1#], [TS COL2#],[TS COL3#],[Cash Disc %], [Trade Lgcy Qty 3],'' As [CEDGOESHERE], [UNSPSC],
substring([Trade Cntry of Origin],1,3)
,[Trade % Mfgd in USA],[REFCOST#],[Ref UOM], NULL as [TS LEGACY UPDATE STATUS]
, ' ' as [EXPORT RECORD TYPE#_1], [MFR CD],[CED MFR UCC] as 'PRIMARY VENDOR', '*' as [EXPORT RECORD TYPE#_2], @V_JOB_FK As [Job ID]
FROM (Select ProductKey, Dataname, DataValue FROM
(select Product_Structure_FK as ProductKey, dn.Short_name as Dataname, value as DataValue from epacube.product_ident_nonunique pin
inner join epacube.DATA_NAME dn on pin.DATA_NAME_FK = dn.DATA_NAME_ID where data_name_fk in (@CEDNetCatalog_DNID,@TStemNumer_DNID) 
UNION select PRODUCT_STRUCTURE_FK, dn.short_name, pd.DESCRIPTION
from epacube.PRODUCT_DESCRIPTION pd 
inner join epacube.data_name dn on dn.data_name_id = pd.data_name_fk where DATA_NAME_FK in (@CEDNetDescription_DNID,@CEDNetDescUPCPrior_DNID) 
UNION
select PRODUCT_STRUCTURE_FK, dn.short_name, ATTRIBUTE_EVENT_DATA 
from epacube.PRODUCT_ATTRIBUTE pa 
INNER JOIN epacube.DATA_NAME dn on pa.DATA_NAME_FK = dn.DATA_NAME_ID where data_name_fk in (@CashDiscountPercent_DNID, @TSPSLgcyQty3_DNID,@TSCountryOfOrigin_DNID,@TSPercentMfgdInUSA_DNID)
UNION 
select puc.PRODUCT_STRUCTURE_FK, dn.short_name, uc.UOM_CODE 
from epacube.PRODUCT_UOM_CLASS puc 
inner join epacube.DATA_NAME dn on puc.DATA_NAME_FK = dn.DATA_NAME_ID 
inner join epacube.UOM_CODE uc on puc.UOM_CODE_FK = uc.UOM_CODE_ID
where puc.Data_name_FK IN (@VendorUOM_DNID,@CEDRefUOM_DNID)
UNION 
select pa.Product_structure_FK, dn.short_name, value 
from epacube.ENTITY_IDENTIFICATION ei
inner join epacube.product_association pa on pa.entity_structure_fk = ei.entity_structure_fk  
inner join epacube.DATA_NAME dn on ei.DATA_NAME_FK = dn.DATA_NAME_ID 
WHERE ei.DATA_NAME_FK in (@CEDMfrUCC_DNID)
UNION
SELECT pc.PRODUCT_STRUCTURE_FK, dn.SHORT_NAME, CASE WHEN len(dv.VALUE) > 20 THEN NULL ELSE dv.value END 
FROM epacube.PRODUCT_CATEGORY pc inner join 
epacube.data_name dn on pc.data_name_fk = dn.data_name_id 
inner join epacube.DATA_VALUE dv on pc.DATA_VALUE_FK = dv.DATA_VALUE_ID
 where pc.DATA_NAME_FK in (@CommodityCode_DNID,@UNSPSC_DNID,@CEDProductStdMfrCode_DNID)
UNION select PRODUCT_STRUCTURE_FK, 'TS COST#', cast(NET_VALUE1 as varchar(1000)) from epacube.PRODUCT_VALUES where data_name_fk = @TSTradeColumnCosts_DNID
UNION select PRODUCT_STRUCTURE_FK, 'TS TRADE LIST#', cast(NET_VALUE5 as varchar(1000)) from epacube.PRODUCT_VALUES where data_name_fk = @TSTradeColumnCosts_DNID
UNION select PRODUCT_STRUCTURE_FK, 'TS COL1#', cast(NET_VALUE4 as varchar(1000)) from epacube.PRODUCT_VALUES where data_name_fk = @TSTradeColumnCosts_DNID
UNION select PRODUCT_STRUCTURE_FK, 'TS COL2#', cast(NET_VALUE3 as varchar(1000)) from epacube.PRODUCT_VALUES where data_name_fk = @TSTradeColumnCosts_DNID
UNION select PRODUCT_STRUCTURE_FK, 'TS COL3#', cast(NET_VALUE2 as varchar(1000)) from epacube.PRODUCT_VALUES where data_name_fk = @TSTradeColumnCosts_DNID
UNION select PRODUCT_STRUCTURE_FK, 'REFCOST#', cast(NET_VALUE1 as varchar(1000)) from epacube.PRODUCT_VALUES where data_name_fk = 4301000 --RG can't find this data name id in March2013 db
 ) As P
 INNER JOIN (select product_structure_fk from export.V_PAXSON_PRX_INCLUDE) PAX on p.ProductKey = PAX.Product_Structure_FK) I
PIVOT (
Max(I.DataValue) FOR DataName IN ([CEDNetDesc], [CED Net Desc Upc Prior], [TS COST#], [TS TRADE LIST#], [TS COL1#], [TS COL2#],[TS COL3#],
[Vdr UOM],[Mfr Item No],[CEDNetCatalog],[Comm Cd],[Trade Lgcy Qty 3],[Cash Disc %],[UNSPSC],[Trade % Mfgd in USA],[Trade Cntry of Origin],[REFCOST#],[Ref UOM],[MFR CD],[CED MFR UCC])
) As PivotTable
)			
			
			
			
			
			
			
			
			
			
			
			
			
--------------------------------------------------------------------------					
-- Setup + call SSIS package
--------------------------------------------------------------------------
			
				  --set @ProductExportJobClass = (select job_class_id from common.job_class where name = 'EXPORT PRODUCTS');

      --            SELECT    @PACKAGE_PATH = PACKAGE_PATH
      --                    , @PACKAGE_NAME = PACKAGE_NAME
      --                    , @EXPORT_FILE_PATH = IMPORT_FILE_PATH
      --                    , @SERVER_NAME = SERVER_NAME
      --            FROM      epacube.import.SSIS_CONFIG_PARAMS
      --            WHERE     SSIS_CONFIG_PARAMS_ID = @in_ssis_config_params_id

      --            SELECT    @job_name = 'Executing SSIS package: ' + @PACKAGE_NAME
      

                  


--DTEXEC /FILE "c:\Home\SSIS\Export_SPDPRX.dtsx" /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /Decrypt epacube 
--/SET \Package.Connections[ntsdb.epacube].ServerName;RGDEV /SET \Package.Connections[CUSTOMER_OUTPUT].ConnectionString;C:\Home\workForOnce.txt 
--/SET \Package.Variables[User::V_JOB_ID].Properties[Value];1749


                  SET @cmd = 'DTEXEC /FILE' +
                      ' ' + @SSIS_PATH + 'Package' + '\' + @PACKAGE_NAME +
                      ' ' +
                      '/MAXCONCURRENT " -1 "' +
                      ' ' +
                      '/CHECKPOINTING OFF' +
                      ' ' +
                      '/Decrypt epacube' +
                      ' ' + '/SET' + ' ' +
                      '\Package.Variables[User::V_EXPORT_FILE_DIRECTORY].Properties[Value];' +   --[User::V_EXPORT_FILE_DIRECTORY]
					  + @SSIS_PATH +
                      ' '+
					  '/SET ' + '\Package.Variables[User::V_EXPORT_FILE_NAME].Properties[Value];' + 'spdprx_' + @file_date_string +'_IN.lsq'+
					  ' ' + 
					  
					  '/SET' + ' ' +
                      '\Package.Connections[ntsdb.epacube].ServerName;' +
                      @SERVER_NAME +
                      ' ' + '/SET' + ' ' +
                      '\Package.Variables[User::V_JOB_ID].Properties[Value];' +
                      CAST( @v_job_fk AS VARCHAR(30))


insert into common.t_sql
(sql_text)
select 
 (select @cmd )


--select @cmd
--                  EXEC master..xp_cmdshell @cmd,
--                       NO_OUTPUT

 --put the SSIS execution into a Service Broker Queue         
                  DECLARE @RC INT
                  DECLARE @service_name NVARCHAR(50)
                  DECLARE @cmd_text NVARCHAR(4000)
                  DECLARE @cmd_type NVARCHAR(10)

                  SELECT    @service_name = 'TargetEpaService'
                          , @cmd_text = @cmd
                          , @cmd_type = 'CMD'
                  EXECUTE @RC = [queue_manager].[enqueue] @service_name, @cmd_text,  @v_job_fk, @cmd_type

--execute master..xp_cmdshell 'DTEXEC /FILE "\\ntsdb\c$\dev\SSIS\I2\Package\I2_Import_To_SqlServer.dtsx" /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /Decrypt epacube /SET "\Package.Variables[User::V_IMPORT_FILE_DIRECTORY].Properties[Value];\\ntsdb\c$\dev\SSIS\I2" /SET "\Package.Connections[ntsdb.epacube].ServerName;NTSDB"'
--\Package.Variables[User::V_DBname].Properties[Value];ntsdbqa.epacube
--------

EXEC common.job_execution_create   @v_job_fk, 'COMPLETED EXPORT SPDPRX FULL',
		                                 @v_input_rows, @v_output_rows, @v_exception_rows,
										 201, @l_sysdate, @l_sysdate  
--



                  SET @status_desc = 'finished execution of export.export_spdprx_full'
                  EXEC exec_monitor.Report_Status_sp @l_exec_no,
                       @status_desc ;
            END TRY
            BEGIN CATCH 

                  SELECT    @ErrorMessage = 'Execution of export.export_spdprx_full has failed ' + ERROR_MESSAGE()
                          , @ErrorSeverity = ERROR_SEVERITY()
                          , @ErrorState = ERROR_STATE() ;

                  EXEC exec_monitor.Report_Error_sp @l_exec_no ;
                  declare @exceptionRecipients varchar(1024);
		 set @exceptionRecipients = isnull((select email_address from msdb.dbo.sysoperators
											where name = 'DBExceptions'),'dbexceptions@epacube.com');
			EXEC exec_monitor.email_error_sp @l_exec_no, @exceptionRecipients;
                  RAISERROR ( @ErrorMessage, -- Message text.
                            @ErrorSeverity, -- Severity.
                            @ErrorState -- State.
				    ) ;
            END CATCH
      END

























