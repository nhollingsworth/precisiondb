﻿



CREATE PROCEDURE [export].[EXPORT_DPS_SALES_CN] 
         
       
    AS
BEGIN     
 
 --
-- MODIFICATION HISTORY
-- Person     Date        Comments
-- --------- ----------  ------------------------------------------
-- CV        02/06/2013  Initial SQL Version
-- MN		 03/12/2013	 
-- CV        04/04/2013  Add more detail
-- CV        04/18/2013  working on export sql call -- data validation from test
-- CV        04/25/2013  Add cust price/rebate type
-- CV        05/07/2013  Finial touches and details 
---CV        06/07/2013  change loyal percent
-- CV        06/10/2013  add code for order amt cog
-- CV        06/15/2013  upper case product id
-- CV        06/18/2013  adding call to missing matrix id     
-- CV        06/21/2013  changing way we get end date to use newest date on sales   
-- CV		 08/15/2013  [MTX_SELL_PRICE_AMT_SOURCE] update below, added MTX_SELL_FORMULA
-- TM		 08/20/2013  ORDER_AMT_COGS & CostOverFl, AveUsage, ShipVia, RushFl, OrderType (TiedtoPO) Added from new ICAEP
-- CV        08/26/2013  Re-arrange updates for anything using Matrix id until Matrix id is found. Order_Amt_Rebate add UnitConv
-- TM        08/27/2013  math correction for ORDER_AMT_SELL_PRICE
-- TM        10/04/2013  change logic for IND_SELL_CONTRACT_USED to be set from rule heirarchy
-- TM        11/06/2013  ORDER_AMT_REBATE edit for divide by zero error
-- TM        11/07/2013  MTX_SELL_HIERARCHY update for inactive rules added
-- TM        11/07/2013  IND_REBATE_SPAPPORTUNITY correct to look at Sales_Trans_ACTIVE
-- CV        11/08/2013  Use Raw pdsc import table for finding missing precedence
-- CV        11/11/2013  @vendor needed to be increased to varchar(max)
-- TM		 11/13/2013  delete where cogs and price = 0
-- MN		 12/04/2013	 Update statement for branch IND 
-- TM	     01/22/2014  Added Logging
-- TM	     04/15/2014  math correction for ORDER_AMT_SELL_PRICE, ORDER_AMT_COGS, and ORDER_AMT_REBATE and remove prccostmult from cost&sell > 0 
-- TM	     04/15/2014  simplified @END_DATE set command, removed IF_THEN logic from below
-- TM	     04/15/2014  Kit Components - duplicated items removed from insert statement
-- TM	     05/13/2014  Another MATH correction for ORDER_AMT_SELL_PRICE, ORDER_AMT_COGS
-- TM	     05/13/2014  Limit initial insert pi.DATA_NAME_FK = product ID only
-- TM		 05/27/2014  IND_ITEM_PROMO is only true if promofl is also checked
-- TM		 06/30/2014  ORDER_AMT_SELL_PRICE, ORDER_AMT_COGS divided by unitconv 
-- TM		 10/01/2014  Incorporated Math changes developed by Albert
-- AJCC      10/22/2014  Included additional filter to remove sales lines where the combined discount is creater than the price.  This would make the price = 0


  DECLARE @l_sysdate DATETIME
        DECLARE @ls_stmt VARCHAR(256)
        DECLARE @l_exec_no BIGINT
        DECLARE @l_rows_processed BIGINT
        DECLARE @status_desc VARCHAR(MAX)
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT
        DECLARE @v_batch_no   bigint
        
DECLARE @v_job_fk     bigint
DECLARE @v_export_Package_fk bigint
DECLARE @v_job_class_fk   int
DECLARE @v_job_name   varchar(32)
DECLARE @v_data1      varchar(32)
DECLARE @v_data2      varchar(32)
DECLARE @v_job_user   varchar(32)
DECLARE @v_job_date   DATETIME
DECLARE @v_last_job_fk     bigint
DECLARE @v_last_job_date   datetime

DECLARE @v_input_rows      int
DECLARE @v_output_rows     int
DECLARE @v_exception_rows  int
DECLARE @v_out_job_fk BIGINT
DECLARE @v_count      INT

DECLARE @VENDOR_NUMBER VARCHAR(max)
DECLARE @START_DATE DATETIME
DECLARE @END_DATE DATETIME
DECLARE @v_isFullDateRange     BIT
DECLARE @v_isSingeDateRange  BIT
DECLARE @v_isAllVendor  BIT
DECLARE @Mult_price_type int
DECLARE @Mult_rebate_type int
DECLARE @v_RptMonthsDiff int   
DECLARE @V_USE_CALC varchar(5)  
 
 BEGIN TRY
   SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of export.EXPORT_SALES_DPS '
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
 
-----------------------------------------------------------------------------------------------------------
-- Set varaibles
-----------------------------------------------------------------------------------------------------------
--select * from epacube.config_params where scope = 'DPS_SALES'
SET @v_export_Package_fk = (Select EXPORT_PACKAGE_ID from export.EXPORT_PACKAGE where name = 'SALES HISTORY')
SET @v_isFullDateRange = 0   
SET @v_isSingeDateRange = 1
SET @v_isAllVendor = 0
--SET @v_RptMonthsDiff = 0  replaced below

SET @Mult_price_type = (select value from epacube.EPACUBE_PARAMS where name = 'SXE MULTIPLE CPT CHECK')
SET @Mult_rebate_type = (select value from epacube.EPACUBE_PARAMS where name = 'SXE MULTIPLE CRT CHECK')         

SET @V_USE_CALC = (select ACTION1_SQL from export.EXPORT_PACKAGE_SQL where name = 'CALC PARAMETER')

Set @START_DATE = isnull((select cast(Value as date) from epacube.config_params where scope = 'DPS_SALES'
and Name = 'START_DATE'),'2012-01-01')

Set @END_DATE = (select case when value = '' then cast(getdate() as date) 
else cast(Value as date) end from epacube.config_params where scope = 'DPS_SALES' and Name = 'END_DATE')  -- added here, removed the IF_THEN statements below.

SET @v_RptMonthsDiff = DATEDIFF(m, @START_DATE, @END_DATE) + 1

DECLARE @v_tmpValue varchar(50)
SET @v_tmpValue = (select top 1 invoicedt from import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE order by invoicedt desc)

set @VENDOR_NUMBER =  (select ltrim(rtrim(Value))  from epacube.config_params where scope = 'DPS_SALES'
and Name = 'VENDOR_NUMBER')

-----------------------------------------------------------------------------
---Create temp table to get list of vendors used
----------------------------------------------------------------------------

--drop temp table if it exists
	IF object_id('tempdb..#TS_DISTINCT_VENDORS') is not null
	   drop table #TS_DISTINCT_VENDORS;
	   
	   
	-- create temp table
	CREATE TABLE #TS_DISTINCT_VENDORS(
    	[VENDOR] [varchar](50)
		)


IF RTRIM(LTRIM(@VENDOR_NUMBER)) = 'ALL' or RTRIM(LTRIM(@VENDOR_NUMBER)) = 'all'
BEGIN
	SET @v_isAllVendor = 1
            
INSERT INTO #TS_DISTINCT_VENDORS (
             VENDOR)
             (Select distinct vendno 
from import.import_sxe_sales_transactions_active with (nolock))

END

IF RTRIM(LTRIM(@VENDOR_NUMBER)) <> 'ALL' or RTRIM(LTRIM(@VENDOR_NUMBER)) <> 'all'
BEGIN
	SET @v_isAllVendor = 0

    INSERT INTO #TS_DISTINCT_VENDORS (
             VENDOR)
             Select 
	         ss.ListItem
		from utilities.SplitString ( @VENDOR_NUMBER, ',' ) ss
 
END
----------------------------------------------------------------------------
---- replaced @END_DATE logic IF_THEN with simple set above.  TAM

--IF @v_tmpValue IS NULL OR LEN(@v_tmpValue) < 10	--No end date provided
--BEGIN
--	SET @v_isFullDateRange = 0  
--	SET @v_isSingeDateRange = 1
--	SET @v_RptMonthsDiff = DATEDIFF(m, @START_DATE, getdate()) + 1
--END ELSE
--BEGIN
--	SET @v_isFullDateRange = 1  
--	SET @v_isSingeDateRange = 0
--	--Set @END_DATE = (select Value from epacube.config_params where scope = 'DPS_SALES' and Name = 'END_DATE')
--	Set @END_DATE = (select top 1 invoicedt from import.IMPORT_SXE_SALES_TRANSACTIONS_ACTIVE order by invoicedt desc) -- this ignores the UI input. 
--	SET @v_RptMonthsDiff = DATEDIFF(m, @START_DATE, @END_DATE) + 1
--END


                
truncate table export.EXPORT_SALES_DPS



---------------------------------------------------------------------------------------
----CREATE TEMP TABLE FOR PRODUCT LIST-----
---------------------------------------------------------------------------------------

INSERT INTO [export].[EXPORT_SALES_DPS]
           ([CUST_ACCT_BILL_TO_ID]
           ,[CUST_ACCT_SHIP_TO_ID]
           --,[FREIGHT_ADD_ON_COSTS]
--           ,[FREIGHT_CARRIER]
           ,[FREIGHT_METHOD]            ---export SQL
--           ,[IND_ACCT_GOV_MUNICIPAL]  ---export SQL
           ,[IND_ACCT_LOYAL]
--           ,[IND_ACCT_MANAGED_INV]    ---export SQL
--           ,[IND_ACCT_NATIONAL]       ---export SQL
--           ,[IND_ACCT_NEW]
--           ,[IND_ACCT_STRATEGIC]
--           ,[IND_ACCT_TREND_FADING]
--           ,[IND_ACCT_TREND_GROWING]
--           ,[IND_COST_BRANCH_SPECIFIC]
           ,[IND_COST_NET_INTO_STOCK]  
           ,[IND_COST_QTY_BREAK]
--           ,[IND_EXCLUDE]
           ,[IND_ITEM_ASSOC_TO_PO]      ---export SQL
--           ,[IND_ITEM_COMMODITY]
--           ,[IND_ITEM_HIGH_COMPETITIVE]
--           ,[IND_ITEM_HIGH_COST]
--           ,[IND_ITEM_HIGH_DEMAND]
           ,[IND_ITEM_KIT_COMP]
--           ,[IND_ITEM_LOSS_LEADER]
--           ,[IND_ITEM_LOW_COST]
--           ,[IND_ITEM_LOW_DEMAND]
--           ,[IND_ITEM_NEW]
           ,[IND_ITEM_NON_STOCK]
--           ,[IND_ITEM_PREMIUM]
           ,[IND_ITEM_PROMO]
           ,[IND_ITEM_SERVICE_LABOR]
           ,[IND_ORDER_LRG_QTY]
           ,[IND_ORDER_OVERRIDE_COGS]  ---export SQL
           ,[IND_ORDER_OVERRIDE_SELL]
--           ,[IND_ORDER_SALES_EDI]
           ,[IND_ORDER_SPECIAL_INSTR]  ---export SQL
           ,[IND_ORDER_SPECIAL_ITEM]
           ,[IND_REBATE_CONTRACT_EXIST]
           ,[IND_REBATE_ITEM]
           ,[IND_REBATE_SPAPPORTUNITY]
           ,[IND_SELL_CONTRACT_USED]
--           ,[IND_VISB_HIGH_GROUP]
--           ,[IND_VISB_HIGH_ITEM]
--           ,[IND_VISB_LOW_GROUP]
--           ,[IND_VISB_LOW_ITEM]
----IND_ACCT_MATRIX    --------possible new column
--           ,[MTX_RBT_BASIS_COLUMN]
           ,[MTX_RBT_CONTRACT_NO]
--           ,[MTX_RBT_FORMULA]
--           ,[MTX_RBT_HIERARCHY]
--           ,[MTX_RBT_MATRIX_ID]
           ,[MTX_RBT_REFERENCE]
--           ,[MTX_SELL_BASIS_COLUMN]
           ,[MTX_SELL_CONTRACT_NO]
		    ,[MTX_SELL_PRICE_AMT_SOURCE]
--           ,[MTX_SELL_FORMULA]
           ,[MTX_SELL_HIERARCHY]
           ,[MTX_SELL_MATRIX_ID]
           ,[MTX_SELL_REFERENCE]
           ,[ORDER_AMT_COGS]
--           ,[ORDER_AMT_COGS_DEFAULT]
--           ,[ORDER_AMT_COGS_SOURCE]
           ,[ORDER_AMT_REBATE]
           ,[ORDER_AMT_SELL_PRICE]
--           ,[ORDER_AMT_SELL_PRICE_DEFAULT]
--           ,[ORDER_CURRENCY]
           ,[ORDER_INVOICE_DATE]
           ,[ORDER_INVOICE_LINE]
           ,[ORDER_INVOICE_NUMBER]
           ,[ORDER_INVOICE_SUFFIX]
           ,[ORDER_INVOICE_TRANS_TYPE]
           ,[ORDER_ITEM_SHIPPED_QTY]  ---- must be greater then zero
           ,[ORDER_ITEM_UOM]
           ,[ORDER_ITEM_UOM_CONV]
           ,[ORDER_REP_INSIDE_ID]
           ,[ORDER_REP_OUTSIDE_ID]
           ,[ORDER_REP_TAKEN_BY_ID]
           ,[PROD_AVG_MO_USAGE]        --- calculated or set from export SQL
           ,[PROD_VELOCITY_RANK]
           ,[PRODUCT_ID]
--           ,[SEGCUST_MRK_HIST]       ---set after customer attribute cust price type from cust attribute procedure.
--           ,[SEGCUST_RBT_MRK_HIST]
--           ,[SEGPROD_MRK_HIST]       ----set after product attribute is built
--           ,[SEGPROD_RBT_MRK_HIST]
--           ,[WHSE_GROUP_ID]
           ,[WHSE_ID])
(
select custno
,custno+'-'+shipto
,NULL												         -----[FREIGHT_METHOD]   updated from Export_SQL
,NULL															----[IND_ACCT_LOYAL]
,NULL															----[IND_COST_NET_INTO_STOCK] 
														        --	 (for dca did not have net into stock data name - 'DAN - asked to use NULL not '0')
,NULL														---[IND_COST_QTY_BREAK]
,NULL				                                        ---[IND_ITEM_ASSOC_TO_PO] updated from Export_SQL
,case when (select ATTRIBUTE_EVENT_DATA from epacube.PRODUCT_ATTRIBUTE  ps
where ps.DATA_NAME_FK = 210820
and ps.PRODUCT_STRUCTURE_FK = pi.product_structure_fk) is not NULL then 1 else NULL end     ---[IND_ITEM_KIT_COMP]
,case when SPECIAL_STOCK_IND = '%N%' then '1' else NULL end								---IND_ITEM_NON_STOCK
,NULL																						--[IND_ITEM_PROMO]
,case when (select dv.value from epacube.product_Status ps
				inner join epacube.DATA_VALUE dv
				on (dv.DATA_VALUE_ID = ps.DATA_VALUE_FK
				and dv.VALUE = 'L')
				where ps.DATA_NAME_FK = 210202
				and ps.PRODUCT_STRUCTURE_FK = pi.product_structure_fk) = 'L' then 1 else NULL 
				end																		---------[IND_ITEM_SERVICE_LABOR]
,NULL																					--at bottom [IND_ORDER_LRG_QTY]
,NULL                                                    --[IND_ORDER_OVERRIDE_COGS] updated from Export_SQL  
,case when priceoverfl = 'Y' then '1' else NULL end        --IND_ORDER_OVERRIDE_SELL
,NULL                                                     --[IND_ORDER_SPECIAL_INSTR] updated from Export_SQL
,case when SPECIAL_STOCK_IND = '%S%' then '1' else NULL end ---IND_ORDER_SPECIAL_ITEM
,NULL														---IND_REBATE_CONTRACT_EXIST  
,case when  ISNULL(vendrebamt,'0') = '0' then NULL else '1' end --IND_REBATE_ITEM
,NULL															--IND_REBATE_SPAPPORTUNITY
,NULL                                                        --IND_SELL_CONTRACT_USED
     --,case when ISNULL([pdsc contract],'0') = '0' then NULL else '1' end--IND_SELL_CONTRACT_USED old
,[pdsr contract]    --MTX_RBT_CONTRACT_NO
,[pdsr contract]  ----MTX_RBT_REFERENCE
,[pdsc contract]  ----MTX_SELL_CONTRACT_NO
	,NULL  ---[MTX_SELL_PRICE_AMT_SOURCE]
	--(select top 1 rp.HOST_PRECEDENCE_LEVEL_DESCRIPTION from  synchronizer.rules R
	--						inner join  synchronizer.RULES_PRECEDENCE  RP
	--						on (r.RULES_PRECEDENCE_FK = rp.RULES_PRECEDENCE_ID)
	--						where isst.pdrecno = R.HOST_RULE_XREF
	--						and r.UPDATE_USER = 'HOST PRICE RULES LOAD'
	--						and r.RECORD_STATUS_CR_FK = 1  )								---[MTX_SELL_PRICE_AMT_SOURCE]

,(select top 1 rp.HOST_PRECEDENCE_LEVEL from  synchronizer.rules R
						inner join  synchronizer.RULES_PRECEDENCE  RP
						on (r.RULES_PRECEDENCE_FK = rp.RULES_PRECEDENCE_ID)
						where isst.pdrecno = R.HOST_RULE_XREF
						and r.UPDATE_USER = 'HOST PRICE RULES LOAD'
						and r.RECORD_STATUS_CR_FK = 1)                     ----[MTX_SELL_HIERARCHY]
 ,pdrecno       ---MTX_SELL_MATRIX_ID
 ,[pdsc refer]  ---MTX_SELL_REFERENCE


,(((isnull(cast(prodcost as numeric(18,6)),0) - ISNULL(cast(vendrebamt as numeric(18,4)),0)))) -- ORDER_AMT_COGS from Albert
 ----------,(((ISNULL(prodcost,0) - ISNULL(cast(vendrebamt as numeric(18,4)),0)) * ISNULL(prccostmult,1)))  --ORDER_AMT_COGS
 --------,(((ISNULL(prodcost,0) - ISNULL(cast(vendrebamt as numeric(18,4)),0)) * ISNULL(prccostmult,0)) / ISNULL(unitconv,1))  --old code 4
 --------,(ISNULL(prodcost,0) - ISNULL(cast(vendrebamt as numeric(18,4)),0)) * ISNULL(prccostmult,0)  --ORDER_AMT_COGS -- old code3
 --------,ISNULL(prodcost,0) - ISNULL(cast(vendrebamt as numeric(18,4)),0))  --ORDER_AMT_COGS --old code2
 --------,ISNULL(prodcost,0) * ISNULL(prccostmult,0) --ORDER_AMT_COGS --old code

 --, --ORDER_AMT_COGS_DEFAULT
 --, --ORDER_AMT_COGS_SOURCE

 ,((ISNULL(cast(vendrebamt as numeric(18,2)), 0)) * ISNULL(( cast(prccostmult as numeric( 18,6) ) ),1))	--  ORDER_AMT_REBATE from Albert
----------,((ISNULL(cast(vendrebamt as numeric(18,2)), 0)) * ISNULL(prccostmult,1))	--ORDER_AMT_REBATE
 ---------,((ISNULL(cast(vendrebamt as numeric(18,2)), 0)) * ISNULL(prccostmult,0)) / ISNULL(unitconv,1)	--old code3
 ---------,(ISNULL(cast(vendrebamt as numeric(18,2)), 0)) * ISNULL(prccostmult,0)	--ORDER_AMT_REBATE  -- old code2
 ---------,ISNULL(cast(vendrebamt as numeric(18,2)), 0)	--ORDER_AMT_REBATE old code1

,isnull(cast(price as numeric(18,6)),0) - (isnull( cast([line discount] as numeric( 18,6) ),0 ) + (isnull( cast([otherdisc] as numeric( 18,6) ),0 ) ) )  -- [ORDER_AMT_SELL_PRICE] from Albert
---------,((ISNULL(price,0) * ISNULL(cast(unitconv as numeric(18,4)),1))) --[ORDER_AMT_SELL_PRICE]
---------,((ISNULL(price,0) * ISNULL(prccostmult,0)) / ISNULL(unitconv,1)) --old code2
--------- ,ISNULL(price,0) * ISNULL(prccostmult,0)	--[ORDER_AMT_SELL_PRICE] ----old code1
--           --[ORDER_AMT_SELL_PRICE_DEFAULT]
--           --[ORDER_CURRENCY]
,CASE WHEN ISDATE(invoicedt) = 1 THEN convert (varchar(10),invoicedt, 101)  ELSE NULL END	--	[ORDER_INVOICE_DATE]
, [lineno]		--[ORDER_INVOICE_LINE]
, [orderno]		--[ORDER_INVOICE_NUMBER]
, [ordersuf]	--[ORDER_INVOICE_SUFFIX]
, [transtype]	--[ORDER_INVOICE_TRANS_TYPE]
, [qtyship]		--[ORDER_ITEM_SHIPPED_QTY]
, [priceunit]	--[ORDER_ITEM_UOM]
, (isnull(cast(netamt as numeric(18,6)),0) / (isnull(cast(price as numeric(18,6)),0) - (isnull( cast([line discount] as numeric( 18,6) ),0 ) + (isnull( cast([otherdisc] as numeric( 18,6) ),0 ) ) ))) / cast(qtyship as numeric(18,6))	 -- [ORDER_ITEM_UOM_CONV] From Albert
--------------, [prccostmult]	--[ORDER_ITEM_UOM_CONV]  old code
, Upper([SALESREP_IN])	--[ORDER_REP_INSIDE_ID]
, upper([slsrepout])	--[ORDER_REP_OUTSIDE_ID]
, Upper([ORDER_TAKER])	--[ORDER_REP_TAKEN_BY_ID]
, NULL      ---[PROD_AVG_MO_USAGE]    --- update in Export_SQL otherwise, calculated value below.
, NULL       --[PROD_VELOCITY_RANK]   ---update after product file created
,(upper(prod)) ---PRODUCT_iD
,Ltrim(rtrim(whse))	--whse_id
--------


from import.import_sxe_sales_transactions_active  isst with (nolock)
inner join epacube.product_identification pi with (nolock)
on (pi.value = isst.prod)
--inner join #TS_Distinct_vendors on (isst.vendno = #TS_Distinct_vendors.vendor)
where pi.DATA_NAME_FK = (select DATA_NAME_FK FROM [export].[EXPORT_MAP_METADATA] where COLUMN_NAME = 'PRODUCT_ID')
and qtyship > 0
AND ISNULL(cast (prodcost as numeric(18,4)),0.00) > 0
                       --AND ISNULL(prodcost,0) * ISNULL(prccostmult,0) > 0 old code
AND ISNULL(cast (price as numeric(18,4)),0.00) > 0
--The actual sell price is the price minus discounts.  We need to check for a calculated sell price that is creater then 0
AND (isnull(cast(price as numeric(18,6)),0) - (isnull( cast([line discount] as numeric( 18,6) ),0 ) + (isnull( cast([otherdisc] as numeric( 18,6) ),0 ) ) )) > 0
                       --AND ISNULL(price,0) * ISNULL(prccostmult,0) > 0 old code
                       --AND isst.vendno in (171537901,125218101,110611701,111095901,123145101,112415501,140432001,122199001,113955001,154228501,164697601,157700401,160815401,156620101,136585002,104161001,156308101,163310001,189625901,159270701,182625001,146752701,124978801,109630101,161705401,135483101,131086301,151215301,165660009,121807801,198950001,163030401,111444301,157593201,161075401,163030201,113548701,192003801,142695001,171107001,197725501,163029301,113022901,108292901,1164886014,120458501,163031701,115840001,199782501,134820801,165660003,157477401,147848001,126976001,150610202,183550201,171770701,167720101,121149001,102143001,154209501,148700001,113059001,189910001,138462501,112126301,155851201,100515501,157478501,143942001,112824601,161160401,108951401,156712501,156030001,164553401,144281201,169599201,184767001,148715001,162900401,194492501,172759201,198260101,136153201,162945401,157651101,102201001,142598801,100110101,162610401,103272702,119610101,199999901,192511101,187878001,153647001,163030101,180386701,121090001)
AND isst.vendno in (select Vendor from #TS_Distinct_vendors)
                       --and  isst.vendno in ('100021', '100019', '300027')
                       --AND	((@v_isAllVendor = 0 AND ltrim(rtrim(vendno)) <> '0' and Charindex(@VENDOR_NUMBER, (ltrim(rtrim(vendno))) ) > 0 )  OR (@v_isAllVendor = 1))
						--AND	((@v_isFullDateRange = 1 AND invoicedt BETWEEN @start_date AND @end_date)
						--	OR (@v_isSingeDateRange = 1 AND invoicedt >= @start_date))
and (invoicedt BETWEEN @start_date AND @end_date)
and isst.transtype <> 'Kit Component'
)




	  SET @ls_stmt = 'started export.EXPORT_SALES_DPS - UPDATE INDICATORS 1'
                
	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;

---------------------------------------------------------------------------------
--1.	CASE WHEN ORDER_INVOICE_TRANS_TYPE = ‘DO’ THEN IND_ITEM_ASSOC_TO_PO = 1 ELSE NULL END
----CASE WHEN ORDER_INVOICE_TRANS_TYPE LIKE (‘D*’) THEN IND_ITEM_ASSOC_TO_PO = 1 ELSE NULL END
---------------------------------------------------------------------------------

update export.EXPORT_SALES_DPS
set IND_ITEM_ASSOC_TO_PO = 1
where ORDER_INVOICE_TRANS_TYPE like 'D%'

-----------------------------------------------------------------------------------
--IND_REBATE_CONTRACT_EXIST ===sum (vendrebateamt) group by customer where > 0, 
--if custno is in list, then 1, else ''
---------------------------------------------------------------------------------------

update export.export_sales_dps
set IND_REBATE_CONTRACT_EXIST = '1'
where CUST_ACCT_BILL_TO_ID in (
select distinct custno from import.import_sxe_sales_transactions_active where vendrebamt is not NULL)

------------------------------------------------------------------------------------------
--IND_REBATE_SPAPPORTUNITY====subquery for list of all prod where vendrebateamt <> 0, 
--if prod is on list, and if IND_REBATE_ITEM = 0, then 1, else ''

update export.export_sales_dps
set IND_REBATE_SPAPPORTUNITY = '1'
where product_id  in (
select distinct prod  from import.import_sxe_sales_transactions_active where vendrebamt is not NULL)
and ind_rebate_item is NULL   ---- '0'



------------------------------------------------------------------------------------------------
----additional update of MTX_SELL_MATRIX_ID
--------------------------------------------------------------------------------------------------

IF @V_USE_CALC = 'NO'
BEGIN

update export.EXPORT_SALES_DPS 
set MTX_SELL_MATRIX_ID = a.MTX_SELL_MATRIX_ID 
 from export.EXPORT_SALES_DPS inner join (
select MTX_SELL_MATRIX_ID, CUST_ACCT_BILL_TO_ID+PRODUCT_ID match  from export.EXPORT_SALES_DPS  where CUST_ACCT_BILL_TO_ID+PRODUCT_ID in (
select distinct CUST_ACCT_BILL_TO_ID+PRODUCT_ID  from export.EXPORT_SALES_DPS  
where IND_ORDER_OVERRIDE_SELL is not null  and ISNULL(MTX_sell_MATRIX_ID,0) ='0')
and MTX_SELL_MATRIX_ID is not null 
) a
on  (export.EXPORT_SALES_DPS.CUST_ACCT_BILL_TO_ID+ PRODUCT_ID = a.match)
and ISNULL(export.EXPORT_SALES_DPS.MTX_SELL_MATRIX_ID,0) ='0'

END

IF @V_USE_CALC = 'YES'
BEGIN
 

 SET @l_exec_no = 0;
	  SET @ls_stmt = 'started execution of export.EXPORT_DPS_FIND_MISSING_MATRIX '
                

	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;


EXECUTE [export].[EXPORT_DPS_FIND_MISSING_MATRIX_IDS]

SET @status_desc = 'finished execution of Export.EXPORT_DPS_FIND_MISSING_MATRIX'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;

END


	  SET @ls_stmt = 'started export.EXPORT_SALES_DPS - UPDATE INDICATORS 2'
                
	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
--------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--IND_COST_BRANCH_SPECIFIC====subquery for list of all prod IF NET_VALUE1 is different within product org, update the flag

;WITH srUnique as
(
select distinct sr1.PRODUCT_STRUCTURE_FK
from (
select sr.PRODUCT_STRUCTURE_FK,net_value1 
from marginmgr.SHEET_RESULTS_VALUES_FUTURE sr with (nolock) 
where sr.data_name_fk = 231100
group by sr.PRODUCT_STRUCTURE_FK,net_value1
) sr1 
cross join (
select sr4.PRODUCT_STRUCTURE_FK,net_value1 
from marginmgr.SHEET_RESULTS_VALUES_FUTURE sr4 with (nolock) 
where sr4.data_name_fk = 231100
group by sr4.PRODUCT_STRUCTURE_FK,net_value1
) sr2  
where 
 sr2.net_value1 = (select min(net_value1) from marginmgr.SHEET_RESULTS_VALUES_FUTURE sr3 with (nolock) where sr3.net_value1 <> sr1.net_value1
 and sr3.data_name_fk = 231100
and sr3.PRODUCT_STRUCTURE_FK = sr1.PRODUCT_STRUCTURE_FK)
)
update export.export_sales_dps
set IND_COST_BRANCH_SPECIFIC = '1'
where product_id in (
select distinct pi.value from epacube.product_identification pi (nolock)
inner join export.export_sales_dps dps
on (pi.value = dps.product_id)
inner join srUnique
on srUnique.PRODUCT_STRUCTURE_FK = pi.PRODUCT_STRUCTURE_FK
)



----------------------------------------------------------------------------------------
----[IND_COST_QTY_BREAK]
---------------------------------------------------------------------------------------
UPDATE export.EXPORT_SALES_DPS
SET IND_COST_QTY_BREAK = '1' 
WHERE MTX_SELL_MATRIX_ID in (
select ish.[pdrecno]  from synchronizer.rules R
inner join  synchronizer.rules_action RA
on (ra.rules_fk = R.rules_id)
inner join synchronizer.RULES_ACTION_OPERANDS RAO
on (rao.rules_action_fk = Ra.RULES_ACTION_ID
and rao.OPERAND_FILTER_OPERATOR_CR_FK = 9020)
inner join  import.import_sxe_sales_transactions_active ish
on (r.host_rule_xref = ish.[pdrecno])
 where r.create_user = 'HOST PRICE RULES LOAD')    ---[IND_COST_QTY_BREAK]



 	  SET @ls_stmt = 'started export.EXPORT_SALES_DPS - CALC PROD AVG MO USAGE'
                
	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;
 ------------------------------------------------------------------------------------------
 ----------------------------------------------------------------------------------------
----PROD_AVG_MO_USAGE and then [IND_ORDER_LRG_QTY]
-----------Note:  Calculated, then actual value updated by Export_SQL, if available.
---------------------------------------------------------------------------------------

update export.export_sales_dps
set PROD_AVG_MO_USAGE = round(cast( a.usage as numeric(18,2)),1,1)
from 
 (select  distinct prod, whse,
						SUM (cast(qtyship/@v_RptMonthsDiff as numeric(18,2))) usage
						 from import.import_sxe_sales_transactions_active
						GROUP BY prod, whse) A
where a.prod = export.export_sales_dps.product_id
and a.whse = export.export_sales_dps.WHSE_ID

-----------------------------------------------------------------------------------------------------
---- ,[IND_ORDER_LRG_QTY]
----find out if somone buys 6 mon supply
----'If qtyShift >= (col 77's value * 6) then 1 else 0
-----------------------------------------------------------------------------------------------------
update export.export_sales_dps
set IND_ORDER_LRG_QTY = 1
--where PRODUCT_ID in 
-- (select distinct PRODUCT_ID from export.EXPORT_SALES_DPS
--where cast( ORDER_ITEM_SHIPPED_QTY as numeric(18,2)) > (6 * cast(PROD_AVG_MO_USAGE as numeric(18,2))))
where cast( ORDER_ITEM_SHIPPED_QTY as numeric(18,2)) > (6 * cast(PROD_AVG_MO_USAGE as numeric(18,2)))


----case when qtyship > (6*cast (round(qtyship/@v_RptMonthsDiff,1 ) as numeric(18,1) )  ) then 1 else NULL end  

-----------------------------------------------------------------------------------------------------------
--[IND_ACCT_LOYAL]

--By customer, you are to count number of months that have orders and divide this by the total months in the data.  
--Then by customer you take:       number of months that have orders/total months
--If greater than 75% then IND_ACCT_LOYAL = 1
------------------------------------------------------------------------------------------------------------

update export.EXPORT_SALES_DPS
set IND_ACCT_LOYAL = 1 
where CUST_ACCT_BILL_TO_ID in (
select c.cust from (
select b.cust, cast(b.num_mon/@v_RptMonthsDiff as numeric) loyal from (
select a.cust cust, count(1) num_mon from 

						(select distinct CUST_ACCT_BILL_TO_ID cust, substring(ORDER_INVOICE_DATE,1,2) num_mon
						 from export.EXPORT_SALES_DPS) A 
					group by a.cust) b) c where loyal > 0.50)
					
--------------------------------------------------------------------------------------------------------
-----------------------    ORDER_AMT_REBATE
--------------------------------------------------------------------------------------------------------

----update export.EXPORT_SALES_DPS
----set ORDER_AMT_REBATE = cast(cast(ORDER_AMT_REBATE as numeric)/cast(ORDER_ITEM_SHIPPED_QTY as numeric) as numeric(18,2))


update  DPS
set ORDER_AMT_REBATE = Case when isnull(cast(ORDER_ITEM_SHIPPED_QTY as numeric(18, 6)), 0) = 0 
then 0 else cast(cast(ORDER_AMT_REBATE as numeric(18, 6))/cast(ORDER_ITEM_SHIPPED_QTY as numeric(18, 6)) as numeric(18,6)) end
from export.EXPORT_SALES_DPS DPS


	  SET @ls_stmt = 'started export.EXPORT_SALES_DPS - UPDATE MATRIX ATTRIBUTES'
                
	  EXEC exec_monitor.start_monitor_sp @exec_desc = @ls_stmt, 
						   					  @exec_id   = @l_exec_no OUTPUT;

------------------------------------------------------------------------------------
------------ MTX_SELL_FORMULA
-------------------------------------------------------------------------------

UPDATE export.export_sales_dps
set MTX_SELL_FORMULA = a.formula
from 
( select  ra.formula formula,  esd.MTX_SELL_MATRIX_ID MATRIX_ID from synchronizer.rules_action ra with (nolock)
 inner join synchronizer.rules r with (nolock)
 on (r.RULES_ID = ra.RULES_FK)
 inner join export.EXPORT_SALES_DPS esd with (nolock)
 on (r.HOST_RULE_XREF = esd.MTX_SELL_MATRIX_ID)
 where ra.UPDATE_USER = 'host price rules load' and ra.formula is not null
 and ra.RULES_ACTION_OPERATION1_FK not  in (1021,
1022,
1023,
1024) ) a ---These are operations for FIXED VALUE formulas
where MTX_SELL_MATRIX_ID = a.MATRIX_ID

----------------------------------------------------------------------------------------
------------------------------MTX_SELL_REFERENCE
-----------------------------------------------------------------------------------
update export.EXPORT_SALES_DPS
set MTX_SELL_REFERENCE = r.reference
from  synchronizer.rules r with (nolock)
 inner join export.EXPORT_SALES_DPS esd with (nolock)
 on (r.HOST_RULE_XREF = esd.MTX_SELL_MATRIX_ID
 and r.UPDATE_USER = 'host price rules load')
where  MTX_SELL_REFERENCE IS NULL

-------------------------------------------------------------------------------------------
---------------------------------MTX_SELL_CONTRACT_NO
--------------------------------------------------------------------------------------------

update export.EXPORT_SALES_DPS
set MTX_SELL_CONTRACT_NO = r.contract_no
from  synchronizer.rules r with (nolock)
 inner join export.EXPORT_SALES_DPS esd with (nolock)
 on (r.HOST_RULE_XREF = esd.MTX_SELL_MATRIX_ID)
 where r.UPDATE_USER = 'host price rules load'
 and  MTX_SELL_CONTRACT_NO IS NULL

-----------------------------------------------------------------------------------------------------
----[MTX_SELL_PRICE_AMT_SOURCE]
----------------------------------------------------------------------------------------------------------------

update export.export_sales_dps
set MTX_SELL_PRICE_AMT_SOURCE = (select Case when ISNULL(MTX_SELL_CONTRACT_NO,'0' ) = '0' 
then 'MATRIX' else 'CONTRACT' End) 

---[MTX_SELL_PRICE_AMT_SOURCE]
    --when MTX_SELL_CONTRACT_NO TBD update export.EXPORT_SALES_DPS set MTX_SELL_PRICE_AMT_SOURCE = 'QUOTE'  
-- future functionality

-------------------------------------------------------------------------------------------
---------------------------------MTX_SELL_HIERARCHY
-------------------------------------------------------------------------------------------
------- update for active rules
/*
update export.EXPORT_SALES_DPS
set MTX_SELL_HIERARCHY = a.RULELEVEL
from 
(select distinct rp.HOST_PRECEDENCE_LEVEL RULELEVEL, esd.MTX_SELL_MATRIX_ID MATRIX_ID  from  synchronizer.rules R
						inner join  synchronizer.RULES_PRECEDENCE  RP
						on (r.RULES_PRECEDENCE_FK = rp.RULES_PRECEDENCE_ID)
						inner join export.EXPORT_SALES_DPS esd 
						on (esd.MTX_SELL_MATRIX_ID = r.HOST_RULE_XREF)
						where esd.MTX_SELL_MATRIX_ID = R.HOST_RULE_XREF
						and r.UPDATE_USER = 'HOST PRICE RULES LOAD'
						and r.RECORD_STATUS_CR_FK = 1) a
where MTX_SELL_MATRIX_ID = a.MATRIX_ID
AND MTX_SELL_HIERARCHY IS NULL
*/

------- update for inactive rules

update export.EXPORT_SALES_DPS
set MTX_SELL_HIERARCHY = a.RULELEVEL
from 
(select distinct rp.HOST_PRECEDENCE_LEVEL RULELEVEL, esd.MTX_SELL_MATRIX_ID MATRIX_ID  from  synchronizer.rules R
						inner join  synchronizer.RULES_PRECEDENCE  RP
						on (r.PRECEDENCE = rp.RULES_PRECEDENCE_ID)
						inner join export.EXPORT_SALES_DPS esd 
						on (esd.MTX_SELL_MATRIX_ID = r.HOST_RULE_XREF)
						where esd.MTX_SELL_MATRIX_ID = R.HOST_RULE_XREF
						and r.UPDATE_USER = 'HOST PRICE RULES LOAD'
						and r.RECORD_STATUS_CR_FK = 2) a
where MTX_SELL_MATRIX_ID = a.MATRIX_ID
AND MTX_SELL_HIERARCHY IS NULL

------  Added to check the Raw import pdsc table if still missing 11/08

update export.EXPORT_SALES_DPS
set MTX_SELL_HIERARCHY = a.RULELEVEL
from 
(select distinct pdsc.levelcd RULELEVEL, pdsc.pdrecno MATRIX_ID  
from  import.import_sxe_pdsc pdsc ) a
where MTX_SELL_MATRIX_ID = a.MATRIX_ID
AND MTX_SELL_HIERARCHY IS NULL


----------------------------------------------------------------------------------------
---- IND_ITEM_PROMO
---------------------------------------------------------------------------------------
update export.EXPORT_SALES_DPS
set IND_ITEM_PROMO = 1 
from (select distinct pdsc.pdrecno MATRIX_ID from  import.import_sxe_pdsc pdsc
where levelcd in (7,8) and promofl = 'Y') a
where MTX_SELL_MATRIX_ID = a.MATRIX_ID


--update export.EXPORT_SALES_DPS 
--set IND_ITEM_PROMO = 1 
--where MTX_SELL_HIERARCHY in (7,8)
------------------------------------------------------------------------------------
------------ MTX_SELL_BASIS_COLUMN
-------------------------------------------------------------------------------

UPDATE export.export_sales_dps
set MTX_SELL_BASIS_COLUMN = a.basis
from 
( select dn.name basis,  esd.MTX_SELL_MATRIX_ID MATRIX_ID from synchronizer.rules_action ra with (nolock)
 inner join synchronizer.rules r with (nolock)
 on (r.RULES_ID = ra.RULES_FK)
 inner join epacube.data_name dn with (nolock)
 on (dn.DATA_NAME_ID = ra.BASIS_CALC_DN_FK)
 inner join export.EXPORT_SALES_DPS esd with (nolock)
 on (r.HOST_RULE_XREF = esd.MTX_SELL_MATRIX_ID)
 where ra.UPDATE_USER = 'host price rules load'
 ) a
where MTX_SELL_MATRIX_ID = a.MATRIX_ID


--------------------------------------------------------------------------------------------
----IND_SELL_CONTRACT_USED  this must be done after the Missing Matrix procedure runs AND 
--                          after the MTX_SELL_HIERARCHY is updated.
------------------------------------------------------------------------------------------
update export.EXPORT_SALES_DPS
set IND_SELL_CONTRACT_USED = 1 
where MTX_SELL_HIERARCHY in (1,2)
and ISNULL(IND_SELL_CONTRACT_USED,0) ='0'

-----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
----IND_COST_NET_INTO_STOCKv  --- TO DO look up values.
--WHERE product_replacement_cost netvalue1 <> product_standard_cost netvalue2
---------------------------------------------------------------------------------------
UPDATE export.export_sales_dps
SET IND_COST_NET_INTO_STOCK = '1'
where PRODUCT_ID  in (select PRODUCT_ID
from export.export_sales_dps esd with (nolock)
inner join epacube.product_identification pi with (nolock)
on (pi.value = esd.PRODUCT_ID)
inner join marginmgr.SHEET_RESULTS_VALUES_FUTURE sr with (nolock)
on ( pi.product_structure_fk = sr.product_structure_fk
 and sr.net_value1 <> sr.net_value2
 and sr.data_name_fk = 231100)
inner join epacube.entity_identification ei with (nolock)
on (ei.entity_structure_fk = sr.org_entity_structure_fk 
and ei.value = esd.WHSE_id))


--------------------------------------------------------------------------------------------------
--------------------------  delete cogs and price is = 0
--------------------------------------------------------------------------------------------------

delete from export.EXPORT_SALES_DPS where cast((ORDER_AMT_COGS) as numeric (18,6)) = 0
delete from export.EXPORT_SALES_DPS where cast((ORDER_AMT_SELL_PRICE) as numeric (18,6)) = 0

-------------------------------------------------------------------------------------------------



  SET @status_desc = 'finished execution of Export.EXPORT_DPS_SALES'
            EXEC exec_monitor.Report_Status_sp @l_exec_no, @status_desc ;


  END TRY

 BEGIN CATCH   

            SELECT  @ErrorMessage = 'Execution of Export.EXPORT_DPS_SALES has failed '
                    + ERROR_MESSAGE(),
                    @ErrorSeverity = ERROR_SEVERITY(),
                    @ErrorState = ERROR_STATE() ;


            EXEC exec_monitor.Report_Error_sp @l_exec_no ;
            EXEC exec_monitor.email_error_sp @l_exec_no,
                'dbexceptions@epacube.com' ;
            RAISERROR ( @ErrorMessage, -- Message text.
                @ErrorSeverity, -- Severity.
                @ErrorState -- State.
				    ) ;
        END CATCH


END
















