﻿












CREATE VIEW [export].[V_PAXSON_PRX_INCLUDE_NEW]
AS

 ----SPDPRx.LSQ – Standard Product Prices applicable to all PCs: 
 ----a. Delimiter = “|” 
 ----b. Select all products from SPFMaster to a work table where SpecVndZonePricesFlag NOT equal to ‘1’ or ‘2’. 
 ----c. Based on UPC, update the col prices, cost, UOM and cash discount columns for all products in work table 
 ----with products from SPFMaster where SpecVndZonePricesFlag = ‘2’, and Insert the first SpecVndZonePricesFlag = ‘2’
 ---- product (for a given UPC) when no match is found in the work table. d. Create SPDPRX.LSQ from the work table.  
  

--Go thru the list of INCLUDED products and if the product shares UPC another product ---NOW UPC IS UNIQUE
 
--FIXED AND CLARIFIED MORE!!

--Go thru the list of INCLUDED products and if the product shares another product 
--with the same UPC but has a different TS_ZONE on it, 
----------------------------look at all products again and find any 
---vendor level data always the same on same upc.------take any products with Vendor Restricted NE 3 over Vendor Restricted = 3 
------------------------------and show on the PRX export with the list, col prices, cost and cash discount columns 
------------------------------the same as the like item UPC's included. 
-- If there are two with Vendor Restricted = 3 included with differing costs,  take the first one that is restricted.
----- Start Zone Code > 00000 AND End Zone Code =< 99999

---- added 12/31/2012 from Mark email
----There are some products that have a Zone that is found on the Spec Price Zone column (Price Zone)
---- AND the Zone has a start and end zip range = 00000 and 99999 (Zip Zone).  
 
----  In this logic, such products could only be found in the SPDPZN file. 
----   But they are showing up in both in the SPDPRX and SPDPZN exports out of epaCUBE.  
----   An example: UPC = 07498319591.   The Zone is PNT.  So, because of the order the below logic is implemented,
----    once it is flagged to be included in SPDPZN due to Zone being found in SpecPriceZone,
----	 the same product would not be in the SPDPRX even 
----though the start and end zip range for the zone is 00000 and 99999.  

select distinct pazone.product_structure_fk 
from  epacube.product_association pazone with (nolock)
inner join epacube.entity_identification ei with (nolock)
on (pazone.org_entity_structure_fk = ei.entity_structure_fk )
inner join (select pricezone from import.paxson_zone_zip_codes pzzc
inner join epacube.entity_identification ei on pzzc.pricezone = ei.value
inner join epacube.product_association pa on ei.entity_structure_fk = pa.org_entity_structure_fk and pa.data_name_fk = 159100 
where ei.value <> '000' and pzzc.startzipcode = '00000' and endzipcode = '99999' ) p on p.pricezone = ei.value
 where  pazone.product_structure_fk not in (select PRODUCT_STRUCTURE_FK from export.V_PAXSON_PZN_INCLUDE_NEW)


--select distinct pazone.product_structure_fk 
--from  epacube.product_association pazone with (nolock)
--inner join epacube.entity_identification ei with (nolock)
--on (pazone.org_entity_structure_fk = ei.entity_structure_fk )
--inner join import.paxson_zone_zip_codes pzzc with (nolock)
--on (ei.value = pzzc.PriceZone
--  and pzzc.startzipcode = 00000 and pzzc.endzipcode = 99999)
--and pazone.data_name_fk = 159100

UNION
------- Where Zone is 000
----not found in Zone table provided by Trade Service
select distinct  pazone.product_structure_fk 
from  epacube.product_association pazone with (nolock)
inner join epacube.entity_identification ei with (nolock)
on (pazone.org_entity_structure_fk = ei.entity_structure_fk 
and ei.value not in (
select pricezone from import.paxson_zone_zip_codes pzzc with (nolock))
)
and pazone.data_name_fk = 159100

UNION

----a) Manufacturer is a Special Vendor AND Restriction is Vendor Restricted
--If vendor (143000/143110) on product is Special Vendor (100143805) = Y 
--and Restriction is Vendor Restricted (100143806) = Y-- 
select distinct
pa.product_structure_fk from epacube.product_association pa 
inner join epacube.entity_attribute ea on pa.entity_structure_fk = ea.entity_structure_fk and ea.data_name_fk = 100143805 and ea.attribute_event_data ='Y'
inner join epacube.entity_attribute ea1 on pa.entity_structure_fk = ea1.entity_structure_fk and ea.data_name_fk = 100143806 and ea.attribute_event_data not in ('1', '2')
where pa.data_name_fk = 159300 














