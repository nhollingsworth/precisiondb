﻿









CREATE
VIEW [export].[V_PAXSON_PZN_INCLUDE] 
(
PRODUCT_STRUCTURE_FK
)
AS

--SPDPZN.LSQ - Standard Product Prices for Special Vendors or Price Zones not Associated with all PCs: 
--a. Delimiter = “|” 
--b. All Products from SPFMaster where SpecVndZonedPriceFlag = ‘1’: 
--- NO 000 zone included.
----get all zones in zone association then check zip zone table
--and if StartZipCode > 00000 and end zip code =< 99999 
select  pazone.product_structure_fk 
from  epacube.product_association pazone with (nolock)
inner join epacube.entity_identification ei with (nolock)
on (pazone.org_entity_structure_fk = ei.entity_structure_fk )
and ei.value in (
select distinct pricezone from import.paxson_zone_zip_codes WITH (NOLOCK)
 where pricezone in (
 select value from epacube.entity_identification  WITH (NOLOCK) where entity_structure_fk  in (
  select distinct org_entity_structure_fk from epacube.product_association  WITH (NOLOCK) where data_name_fk =   159100
  and product_structure_fk  in(
 select product_structure_fk from epacube.product_association  WITH (NOLOCK)
 where entity_structure_fk in (select distinct entity_structure_fk 
 from epacube.product_association pa  WITH (NOLOCK) where pa.data_name_fk = 159300)
 and value <> '000'
 ))) and startzipcode > '00000' and endzipcode <= '99999')
----
and pazone.data_name_fk = 159100


UNION
----a) Manufacturer is a Special Vendor AND Restriction is Vendor Restricted
----If vendor (143000/143110) on product is Special Vendor (100143805) = Y 
----and Restriction is Vendor Restricted (100143806) = Y-- 
--select
--pa.product_structure_fk from epacube.product_association pa where pa.data_name_fk = 159300 
--and pa.entity_structure_fk in (select entity_structure_fk from epacube.entity_attribute where
--data_name_fk =100143805 and attribute_event_data = 'Y') 
--AND
--pa.entity_structure_fk in (select entity_structure_fk from epacube.entity_attribute 
--where data_name_fk = 100143806 and attribute_event_data = '2') 

----I should find the 153 would be on the product association 
----zones from Trade server are zip zone codes
----price zone here import.Paxson_Zone_Zip_Codes = zone on product association and then use start and end 

----special vendor is y or 1 type restriction 2 and zone is not 000
----and the product association zone is not an special prize zone off the Ucc attribute 
----attribute off UCC .  when the UCC of the product = 

----when the product zone = the special price zone for the MFRCODE on the MFR then use in the PZN export.

select pazone.product_structure_fk 
----, eizone.value zone
----,eimfr.value mfr 
----,eazone.attribute_event_data
from epacube.product_association pazone
inner join epacube.product_association pamfr
on (pazone.product_structure_fk = pamfr.product_structure_fk
and pamfr.data_name_fk = 159300)
inner join epacube.entity_identification eizone
on (eizone.entity_structure_fk = pazone.org_entity_structure_fk
and eizone.value <> '000')
inner join epacube.entity_identification eimfr
on (eimfr.entity_structure_fk = pamfr.entity_structure_fk)
inner join epacube.entity_Attribute EAZONE 
on (pamfr.entity_structure_fk = eazone.entity_structure_fk
and eazone.data_name_fk in 
(100143807,
100143808,
100143809))
where pazone.data_name_fk = 159100
and pazone.PRODUCT_STRUCTURE_FK in (
select
pa.product_structure_fk from epacube.product_association pa where pa.data_name_fk = 159300 
and pa.entity_structure_fk in (select entity_structure_fk from epacube.entity_attribute where
data_name_fk =100143805 and attribute_event_data = 'Y') 
AND
pa.entity_structure_fk in (select entity_structure_fk from epacube.entity_attribute 
where data_name_fk = 100143806 and attribute_event_data = '2') )




