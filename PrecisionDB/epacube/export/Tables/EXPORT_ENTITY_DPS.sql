﻿CREATE TABLE [export].[EXPORT_ENTITY_DPS] (
    [ENTITY_CLASS]       VARCHAR (50)  NULL,
    [ENTITY_NAME]        VARCHAR (50)  NULL,
    [CODE]               VARCHAR (50)  NULL,
    [DESCRIPTION]        VARCHAR (128) NULL,
    [PARENT_ENTITY_NAME] VARCHAR (50)  NULL,
    [PARENT_ENTITY_CODE] VARCHAR (50)  NULL,
    [STATUS]             VARCHAR (50)  NULL
);

