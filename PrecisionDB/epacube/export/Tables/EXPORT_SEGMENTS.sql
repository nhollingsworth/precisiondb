﻿CREATE TABLE [export].[EXPORT_SEGMENTS] (
    [CODE]                 NVARCHAR (256) NULL,
    [DESCRIPTION]          NVARCHAR (256) NULL,
    [ENTITY_CLASS]         NVARCHAR (256) NULL,
    [SEGMENT_CLASS]        NVARCHAR (256) NULL,
    [SEGMENT_TYPE]         NVARCHAR (256) NULL,
    [PARENT_CODE]          NVARCHAR (256) NULL,
    [PARENT_SEGMENT_CLASS] NVARCHAR (256) NULL
);

