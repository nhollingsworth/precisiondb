﻿CREATE TABLE [export].[RETAIL_CHANGE_HISTORY] (
    [RETAIL_CHANGE_HISTORY_ID] BIGINT          IDENTITY (1, 1) NOT NULL,
    [EXPORT_TIMESTAMP]         DATETIME        CONSTRAINT [DF_RETAIL_CHANGE_HISTORY_EXPORT_TIMESTAMP] DEFAULT (getdate()) NULL,
    [PROCESS]                  VARCHAR (64)    NULL,
    [TABLE_NAME]               VARCHAR (96)    NULL,
    [TABLE_ID]                 BIGINT          NULL,
    [CUSTOMER]                 VARCHAR (64)    NULL,
    [ZONE]                     VARCHAR (64)    NULL,
    [ZONE_TYPE]                VARCHAR (8)     NULL,
    [ITEM]                     VARCHAR (64)    NULL,
    [EFFECTIVE_DATE]           DATE            NULL,
    [PRICE_MULTIPLE]           INT             NULL,
    [PRICE]                    NUMERIC (18, 2) NULL,
    [ITEM_LEVEL]               NUMERIC (8)     NULL,
    [ITEM_CLASS]               NUMERIC (16)    NULL,
    [LEVEL_GROSS_FLAG]         VARCHAR (1)     NULL,
    [TARGET_MARGIN]            NUMERIC (18, 4) NULL,
    [PRICE_TYPE]               INT             NULL,
    [product_structure_fk]     BIGINT          NULL,
    [zone_entity_structure_fk] BIGINT          NULL,
    [data_value_fk]            BIGINT          NULL
);


GO
CREATE NONCLUSTERED INDEX [idx_01]
    ON [export].[RETAIL_CHANGE_HISTORY]([TABLE_NAME] ASC, [TABLE_ID] ASC, [PROCESS] ASC, [RETAIL_CHANGE_HISTORY_ID] ASC) WITH (FILLFACTOR = 80)
    ON [INDEX];

