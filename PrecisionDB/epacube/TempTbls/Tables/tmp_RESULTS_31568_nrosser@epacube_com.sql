﻿CREATE TABLE [TempTbls].[tmp_RESULTS_31568_nrosser@epacube_com] (
    [PRODUCT_STRUCTURE_FK]       BIGINT        NULL,
    [PRODUCT_SEGMENT_FK]         BIGINT        NULL,
    [CUST_ENTITY_STRUCTURE_FK]   INT           NULL,
    [VENDOR_ENTITY_STRUCTURE_FK] INT           NULL,
    [ZONE_ENTITY_STRUCTURE_FK]   INT           NULL,
    [COMP_ENTITY_STRUCTURE_FK]   INT           NULL,
    [ASSOC_ENTITY_STRUCTURE_FK]  INT           NULL,
    [DN_110100]                  BIGINT        NULL,
    [DN_110401]                  BIGINT        NULL,
    [DN_550113]                  BIGINT        NULL,
    [DN_550010]                  BIGINT        NULL,
    [DN_550011]                  BIGINT        NULL,
    [DN_550012]                  BIGINT        NULL,
    [DN_550115]                  BIGINT        NULL,
    [DN_550114]                  BIGINT        NULL,
    [DATA_LEVEL_CR_FK]           INT           NULL,
    [PRODUCT_ID]                 VARCHAR (128) NULL,
    [DESCRIPTION_LINE_1]         VARCHAR (256) NULL,
    [V_UPC_EFFECTIVE_DATE]       DATE          NULL,
    [V_UPC_SYS]                  VARCHAR (16)  NULL,
    [V_UPC_MFR]                  VARCHAR (16)  NULL,
    [V_UPC_ITEM]                 VARCHAR (16)  NULL,
    [V_DISCONTINUE_DATE]         DATE          NULL,
    [V_UPC_PRECEDENCE]           INT           NULL
);



